//actions.js file 
function AS_Button_b232e3445bda4e7795fdb6e62534234c(eventobject) {
    var self = this;
    this.selectedTab(0);
}

function AS_Button_d00e6be0d3134fb7a3d0707efd4ec5e7(eventobject) {
    var self = this;
    this.selectedTab(1);
}

function AS_Button_e1685424d4c6426b95ee8300041e29ac(eventobject) {
    var self = this;
    var self = this;
    var parentid = eventobject.parent.parent.parent.id;
    var editId = parentid.replace("view", "edit");
    if ($KG.__currentForm.flxViewLoanDetails) $KG.__currentForm.flxViewLoanDetails.setVisibility(false);
    if ($KG.__currentForm.flxEditLoanInformation) {
        $KG.__currentForm.flxEditLoanInformation.setVisibility(true);
        // $KG.__currentForm.flxEditLoanInformation.flxEditLoanInformation1.scrollToWidget($KG.__currentForm.flxEditLoanInformation.flxEditLoanInformation1[editId]);
    }
}

function AS_Button_e794fbc96df7470f98d8b0154dfb9b34(eventobject) {
    var self = this;
}

function AS_Button_g296478d75b04440a808e297fce40695(eventobject) {
    var self = this;
}

function AS_Button_ie6cc81a8c23486c885f5ddc529b1939(eventobject) {
    var self = this;
    this.selectedTab(2);
}

function AS_FlexContainer_a0401bdee1ec4cc9a14184d4ef8782f8(eventobject) {
    var self = this;
    return self.simulationTemplatePreShow.call(this);
}

function AS_FlexContainer_a08a6097d3c44826a29e7d98169e7428(eventobject) {
    var self = this;
    this.flxPerm_onClick();
}

function AS_FlexContainer_a219ccf19eb34861a0b7a45f3e23a093(eventobject) {
    var self = this;
    kony.application.getCurrentForm().toastMessage.parent.setVisibility(false);
}

function AS_FlexContainer_a235e95a2684419e941f389359657759(eventobject) {
    var self = this;
    this.flxFAQsLMenu_onClick();
}

function AS_FlexContainer_a44d23a376ef4b74afaad8170bc45392(eventobject) {
    var self = this;
    this.callPreshow();
}

function AS_FlexContainer_a5cd41c52f6d4ff6aafa0ddbcd72933d(eventobject) {
    var self = this;
    this.setCompFlowActions();
}

function AS_FlexContainer_a715ce69303f4f219318b08c81fcb0f0(eventobject) {
    var self = this;
    return self.manageProductsPreShow.call(this);
}

function AS_FlexContainer_a74a80a28cf84a7194adc9149c578a33(eventobject) {
    var self = this;
    return self.ondeleteFourthIncome.call(this);
}

function AS_FlexContainer_a9547d16b5074f5cbd43b318809332cd(eventobject) {
    var self = this;
    var navObj = new kony.mvc.Navigation("frmLocations");
    navObj.navigate();
}

function AS_FlexContainer_ab1e178129194d0ea3e29dc1a2cdff70(eventobject) {
    var self = this;
    this.flxAdminUsers_onClick();
}

function AS_FlexContainer_ab2e67b6a33d41fb9a7bfe3ada231b88(eventobject) {
    var self = this;
    this.setCompFlowActions();
}

function AS_FlexContainer_ab7d0f743ba94ff98d2abb57bb65cbaa(eventobject) {
    var self = this;
    this.setFlowActions();
}

function AS_FlexContainer_ab82dc3781f14807b3a7012e7d4aef47(eventobject) {
    var self = this;
    return self.showOrHideContent.call(this, null);
}

function AS_FlexContainer_abf4233351fb4d0e859e0acf28338865(eventobject) {
    return self.viewFeatureDetailsPreShow.call(this);
}

function AS_FlexContainer_acc7d34519494e69ad70a4c809369008(eventobject) {
    var self = this;
}

function AS_FlexContainer_ad2f60a295af4dbab21e537eab83ce26(eventobject) {
    var self = this;
    this.deleteRowPreshow();
}

function AS_FlexContainer_ad64cca7a784425b85c5b1eaeab02e5d(eventobject) {
    var self = this;
    this.flxRoles_onClick();
}

function AS_FlexContainer_ae2e9ad80bad4816b5c0462b0df1bc0a(eventobject) {
    var self = this;
    this.flxPrem_onClick();
}

function AS_FlexContainer_b0c0b03602c1499eb6900f1b828d7bf0(eventobject) {
    var self = this;
    this.requestHelpCenterPreshow();
}

function AS_FlexContainer_b1027fa1137e4323acf5247a826c3172(eventobject) {
    var self = this;
    this.flxServiceManagementLink_onClick();
}

function AS_FlexContainer_b1a3158c443a4c42acb8d6a705fbc11f(eventobject) {
    var self = this;
    this.setFlowActions();
}

function AS_FlexContainer_b4e90537c75e4045abca09f1448ebeeb(eventobject) {
    var self = this;
    this.setFlowActions();
}

function AS_FlexContainer_b5ee4fa0710c43f2a2cc31751cc338b7(eventobject) {
    var self = this;
    this.flxPerm_onClick();
}

function AS_FlexContainer_b6ba001f60984318b4080b167afb859c(eventobject) {
    var self = this;
    //asdfg
}

function AS_FlexContainer_b77ed70e85c74302a1bdf325f729d0b7(eventobject) {
    var self = this;
    return self.ondeleteThirdIncome.call(this);
}

function AS_FlexContainer_b831542a93a7490da951e8d929425ec2(eventobject) {
    var self = this;
    return self.onClickOfAddMoreThird.call(this);
}

function AS_FlexContainer_b930b9a5cf984849ab2455f8a6b4ddae(eventobject) {
    var self = this;
    return self.ondeleteFirstIncome.call(this);
}

function AS_FlexContainer_b9660cf28edb4d2bbb479d48cacb6670(eventobject) {
    return self.viewProductPreShow.call(this);
}

function AS_FlexContainer_ba7818631ed740b9b8f871667b666d56(eventobject) {
    var self = this;
    this.editGeneralInfoPreshow();
}

function AS_FlexContainer_bbe5f349746e471db9d3a8189e9f09c5(eventobject) {
    var self = this;
    this.flxPerm_onClick();
}

function AS_FlexContainer_bc27f1d8c52f4b319a3a88651b592f11(eventobject) {
    var self = this;
    this.flxTnCLMenu_onClick();
}

function AS_FlexContainer_be0cdb6290fe411fa70d45aa45b4497d(eventobject) {
    var self = this;
    this.callPreshow();
}

function AS_FlexContainer_beb73bea0c774a32b8b7c0decd24021a(eventobject) {
    var self = this;
    this.flxPerm_onClick();
}

function AS_FlexContainer_bfe2bf6254a4485788dadaa487d50483(eventobject) {
    var self = this;
    this.linkProfilePreshow();
}

function AS_FlexContainer_bffb333410a042d683103c172e078d54(eventobject) {
    var self = this;
    this.view.tbxSearchBox.text = "";
    this.view.flxSearchCancel.isVisible = false;
}

function AS_FlexContainer_c04ce5fac6ea488e87b5b4c24cbf2050(eventobject) {
    var self = this;
    this.flxSecureImage_onClick();
}

function AS_FlexContainer_c0be6b4ef53c4d068127bc73197be828(eventobject) {
    var self = this;
    return self.viewInternalExternalEventsPreShow.call(this);
}

function AS_FlexContainer_c2edcc3f80c645d5b73788b47eaa4bf4(eventobject) {
    var self = this;
    this.flxAdminUsers_onClick();
}

function AS_FlexContainer_c32e76eaff8549d5bf1fe683a5235941(eventobject) {
    var self = this;
    return self.ondeleteFiveIncome.call(this);
}

function AS_FlexContainer_c44a677cfa71454da74841ec9b59ab31(eventobject) {
    this.preshow();
}

function AS_FlexContainer_c48c03ee574b4e5a8e933e010a9fdafa(eventobject) {
    var self = this;
    this.onSelection(this.view.flxLbl3);
    this.view.lbl3.skin = "sknLblffffff13px";
}

function AS_FlexContainer_c4c5fc056b5f4ab9b7030c131ad1b4ae(eventobject) {
    var self = this;
    this.flxAdminUsers_onClick();
}

function AS_FlexContainer_c585c715f06042fdb6b1537a934f34cb(eventobject) {
    var self = this;
    return self.addProductPreShow.call(this);
}

function AS_FlexContainer_c640921d587c4781914a4605c42306b8(eventobject) {
    var self = this;
    if (this.view.flxImage.src === "img_down_arrow.png") {
        this.view.flxImage.src = "img_desc_arrow.png";
        this.view.flxFilters.setVisibility(false);
    } else {
        this.view.flxImage.src = "img_down_arrow.png";
        this.view.flxFilters.setVisibility(true);
    }
}

function AS_FlexContainer_c6b61fe1c4c9441d8694eec976dedf80(eventobject) {
    var self = this;
    this.setFlowActions();
}

function AS_FlexContainer_c6c8c0c50e2d48e8b832578d41ed8c06(eventobject) {
    var self = this;
    this.flxPerm_onClick();
}

function AS_FlexContainer_c78d1f858e544936abc5c796b05753ab(eventobject) {
    var self = this;
}

function AS_FlexContainer_cc4fba48e08d489ba5dbdd239def942d(eventobject) {
    var self = this;
    return self.onClickOfAddMoreFourth.call(this);
}

function AS_FlexContainer_cd6abbdad0784a19bafa675ba0ae0d0c(eventobject) {
    var self = this;
    this.flxServicesOutageLMenu_onClick();
}

function AS_FlexContainer_cdc847d47b7a4303824c23e2dde9002a(eventobject) {
    var self = this;
    this.preShow();
}

function AS_FlexContainer_ce738bae91614533a035f7216c3471e4(eventobject) {
    var self = this;
    this.setFlowActions();
}

function AS_FlexContainer_cf089b5615474e6f9d6087c98551050e(eventobject) {
    var self = this;
}

function AS_FlexContainer_d0cf27a9ae1c4feaa673337ab0de6396(eventobject) {
    var self = this;
    this.flxPerm_onClick();
}

function AS_FlexContainer_d1bb929b869f4a648d93176ae2210072(eventobject) {
    var self = this;
    this.onSelection(this.view.flxLbl0);
    this.view.lbl0.skin = "sknLblffffff13px";
}

function AS_FlexContainer_d2f3ddd2a6554a3f9532e0383726d76a(eventobject) {
    var self = this;
    this.setFlowActions();
}

function AS_FlexContainer_d410be7967584d79bedd9a7d21c8697f(eventobject) {
    var self = this;
    return self.preshow.call(this);
}

function AS_FlexContainer_d4976f8044ca4ead83c3ffed9ebf6288(eventobject) {
    var self = this;
    return self.popupDetailsPreShow.call(this);
}

function AS_FlexContainer_d5c760d573e94af6b91ba69e0e2c1c19(eventobject) {
    var self = this;
}

function AS_FlexContainer_d6168da3718447f1b5b309f21c14e29e(eventobject) {
    var self = this;
    this.flxAdminUsers_onClick();
}

function AS_FlexContainer_d679d108c5a349f8b1a851b0cd6a09ef(eventobject) {
    var self = this;
    this.setFlowActions();
}

function AS_FlexContainer_d68cefc7cecb4bd4b82b639770095822(eventobject) {
    var self = this;
    return self.onClickOfAddMoreFirst.call(this);
}

function AS_FlexContainer_d6adcb0b75ac49e9ad9041177384e4f3(eventobject) {
    var self = this;
    this.flxAdminLink_onClick();
}

function AS_FlexContainer_d805f279f1d848e38f10afdd1fb4762b(eventobject) {
    var self = this;
    this.flxPerm_onClick();
}

function AS_FlexContainer_d82573481f1e455d8c18a46c0ceadf04(eventobject) {
    var self = this;
    this.flxPerm_onClick();
}

function AS_FlexContainer_d8b89807d2434573b950e17ef6c12115(eventobject) {
    var self = this;
    return self.ondeleteSevenIncome.call(this);
}

function AS_FlexContainer_d8e34dc14ed74c8a901427411dd46daf(eventobject) {
    var self = this;
    this.onSelection(this.view.flxLbl4);
    this.view.lbl4.skin = "sknLblffffff13px";
}

function AS_FlexContainer_de214b99c6d6433994a27c6bf70d47a4(eventobject) {
    var self = this;
    this.flxServicesLMenu_onClick();
}

function AS_FlexContainer_de9be5c29c094de1902dba128b9822ca(eventobject) {
    var self = this;
    this.callPreshow();
}

function AS_FlexContainer_dea3fe86193542b2a014a922afd7c882(eventobject) {
    var self = this;
    this.setFlowActions();
}

function AS_FlexContainer_df83650c21f04fea99b9cd0ae0e26e4c(eventobject) {
    var self = this;
    this.setCompFlowActions();
}

function AS_FlexContainer_e28889b0992e4d44a55ae430d9b97c44(eventobject) {
    var self = this;
    this.flxSecurityQuestionsLMenu_onClick();
}

function AS_FlexContainer_e2fa48651f6042f2b029764f9298d1e4(eventobject) {
    var self = this;
    this.flxAdminLink_onClick();
}

function AS_FlexContainer_e3325f619a57467d8ced35976ca07b03(eventobject) {
    var self = this;
    this.verticalTabsPreShow();
    kony.print("verticalTabs preshow called");
}

function AS_FlexContainer_e37a2d2f44044fd59d35c285bdc974b5(eventobject) {
    var self = this;
}

function AS_FlexContainer_e418ed5865574ae0a6806ca8202884f2(eventobject) {
    var self = this;
}

function AS_FlexContainer_e58ef4c2540648f88ca8a73f95c3bdb3(eventobject) {
    var self = this;
    return self.onClickOfAddNewIncome.call(this);
}

function AS_FlexContainer_e66fef994f8b44e1844b82a3da7a983c(eventobject) {
    var self = this;
    this.flxPerm_onClick();
}

function AS_FlexContainer_e79e4e23daf147fb9e42e405315e4ea1(eventobject) {
    var self = this;
}

function AS_FlexContainer_e996b729b1ca486993299d4e5ecf8700(eventobject) {
    var self = this;
    this.flxPerm_onClick();
}

function AS_FlexContainer_ea699b34f506498eb19e009216757bf0(eventobject) {
    var self = this;
    this.flxPerm_onClick();
}

function AS_FlexContainer_eaf0f8266eec422f897d57f3156b9051(eventobject) {
    var self = this;
    // this.view.tbxSearchBox.text="";
    // this.view.flxClearSearchImage.setVisibility(false);
    // this.view.imgClearSearch.setVisibility(true);
    // this.view.forceLayout();
}

function AS_FlexContainer_ebc0a16d61204cde8670a0cb87b1262d(eventobject) {
    var self = this;
}

function AS_FlexContainer_eccdc33540ca452f8a46c855aeddd5b8(eventobject) {
    var self = this;
    return self.ondeleteSecondIncome.call(this);
}

function AS_FlexContainer_ed824b012e1847b89f735ba85a1de434(eventobject) {
    var self = this;
    this.flowActions();
}

function AS_FlexContainer_ee2fd27792a24daa8e3eae0e37f6a8e4(eventobject) {
    var self = this;
    this.flxPwdPoliciesLMenu_onClick();
}

function AS_FlexContainer_ee34dd4ec65b4ba0b39fce3f8a987dee(eventobject) {
    var self = this;
    this.flxRoles_onClick();
}

function AS_FlexContainer_ef86433e7e8c4396afe7e9873d50990c(eventobject) {
    var self = this;
    this.setFlowActionsForCSRAssist();
}

function AS_FlexContainer_f05a2e590ee94c3aac4d050dbb9bac48(eventobject) {
    var self = this;
}

function AS_FlexContainer_f060a06b2a454e61a0577b070e30e355(eventobject, context) {
    var self = this;
    var self = this;
    var isHover = context.event !== "mouseout";
    var formName = eventobject.pf;
    var code = "";
    if (formName && formName === "frmDepositsDashboard") {
        code = eventobject.id.replace("depositsInfo", "");
    } else {
        code = eventobject.id.replace("loaninfo", "");
    }
    eventobject[code + "flxLoanAprDetails"].setVisibility(!isHover);
    eventobject[code + "flxApplyLoan"].setVisibility(isHover);
    eventobject[code + "lblLearnMore"].setVisibility(isHover);
    eventobject[code + "btnApplyLoan"].setVisibility(isHover);
    eventobject.skin = isHover ? "sknFlxBorder117eb0CustomRadius5px" : "sknFlxBordere1e5eedCustomRadius5px";
    self.view.forceLayout();
}

function AS_FlexContainer_f103c4f10de64a5ab1f62966ca1bb57e(eventobject) {
    var self = this;
    this.addLocationsToMap();
}

function AS_FlexContainer_f11d59456811475ba309998f08be5a1b(eventobject) {
    var self = this;
    this.setFlowActions();
}

function AS_FlexContainer_f22058894b5d4fe0b68d0ef403776999(eventobject) {
    var self = this;
    this.flxSecureImage_onClick();
}

function AS_FlexContainer_f2e5479eb93746499bdf3ecbe81f0e67(eventobject) {
    var self = this;
    this.flxRoles_onClick();
}

function AS_FlexContainer_f35b89a5eac048fba0df59afd1d1657f(eventobject) {
    var self = this;
    return self.preShow.call(this);
}

function AS_FlexContainer_f51b403e601644ddb93c6d49d5a13bd4(eventobject) {
    var self = this;
    this.flxServiceManagementLink_onClick();
}

function AS_FlexContainer_f6c40c3a56934b58881f3f3375338f1c(eventobject) {
    var self = this;
    this.flxSecureImage_onClick();
}

function AS_FlexContainer_f7722c2eab344577a81d63facebe898e(eventobject) {
    var self = this;
    this.setFlowActions();
}

function AS_FlexContainer_f7b18b49cbb04a138c4153362a2fed86(eventobject) {
    var self = this;
    this.flxPrivacyPoliciesLMenu_onClick();
}

function AS_FlexContainer_f861d67e4d0e4f109d85004bd1deb82d(eventobject) {
    var self = this;
    this.setPreShow();
}

function AS_FlexContainer_fbcc708e03414e1398a146e820f3ea2a(eventobject) {
    var self = this;
    this.flxPerm_onClick();
}

function AS_FlexContainer_fd4b6994bf604ee78bff55ba90fcb858(eventobject) {
    var self = this;
    this.viewConfigureCSRPreshow();
}

function AS_FlexContainer_fda01a57b0424844bdb0c812b3acf292(eventobject) {
    var self = this;
    return self.onClickOfAddMoreSix.call(this);
}

function AS_FlexContainer_fdd27c0633c14c7e8cc9f106c0e50480(eventobject) {
    var self = this;
    this.setCompFlowActions();
}

function AS_FlexContainer_feef057e6b2249b7b888dacf8397096e(eventobject) {
    var self = this;
    this.LimitPreshow();
}

function AS_FlexContainer_ffa4cfda051047e5a9dd13707f73f958(eventobject) {
    var self = this;
    this.onSelection(this.view.flxLbl1);
    this.view.lbl1.skin = "sknLblffffff13px";
}

function AS_FlexContainer_g09f0b2a942f4001bbc6ca93de64c08c(eventobject) {
    var self = this;
}

function AS_FlexContainer_g1d0791195004eb1846659b8e4f169c6(eventobject) {
    var self = this;
}

function AS_FlexContainer_g2a601a510e14c4fa2664ac91d111eeb(eventobject) {
    var self = this;
    this.callPreShow();
}

function AS_FlexContainer_g435b753a90b4a15b1fec7daa5aeaae0(eventobject) {
    var self = this;
    this.renderCalendarPostShow();
}

function AS_FlexContainer_g53a220b6d5e4c5a832c4bb7ec7bd176(eventobject) {
    var self = this;
    this.setCompFlowActions();
}

function AS_FlexContainer_g7e4ec720ea1426293b0be13c630ba5f(eventobject) {
    var self = this;
    this.flxAdminUsers_onClick();
}

function AS_FlexContainer_ga3e88e710cb4e3ebaf98186349569bc(eventobject) {
    var self = this;
    this.onSelection(this.view.flxLbl2);
    this.view.lbl2.skin = "sknLblffffff13px";
}

function AS_FlexContainer_gd36cabed06d463cb80493b6dfe08033(eventobject) {
    var self = this;
    this.setCompFlowActions();
}

function AS_FlexContainer_h2b8509bb390429eb4d7da66c74ecd1a(eventobject) {
    var self = this;
    this.flxServiceManagementLink_onClick();
}

function AS_FlexContainer_h4446017aafa4a989a21082c7b4a4310(eventobject) {
    var self = this;
    return self.ondeleteSixIncome.call(this);
}

function AS_FlexContainer_h44e27af0a1d4672b69a516817ada1cc(eventobject) {
    var self = this;
    this.flxPerm_onClick();
}

function AS_FlexContainer_h4a6762bc017462b901ecdbc11468cb7(eventobject) {
    var self = this;
    this.setCompFlowActions();
}

function AS_FlexContainer_h51c4108daae491285c376f7c0665cd8(eventobject) {
    var self = this;
    return self.onClickOfAddMoreFive.call(this);
}

function AS_FlexContainer_h9b35f84d4e44ac2927877f9ebd95f5d(eventobject) {
    var self = this;
    return self.preShow.call(this);
}

function AS_FlexContainer_habff468b8c445bc95d80f845ac5f69c(eventobject) {
    var self = this;
    this.preShowFun();
}

function AS_FlexContainer_hddf6ece62b0438fa74c1cd81a02568f(eventobject) {
    var self = this;
    this.setPreShow();
}

function AS_FlexContainer_he82ddc60f0a44c5b91d4e0e1d5482e9(eventobject) {
    var self = this;
    this.editContactPreShow();
}

function AS_FlexContainer_hf96e82a3529484182fa09f59a9be2b6(eventobject) {
    var self = this;
    this.renderCalendarPostShow();
}

function AS_FlexContainer_i00ef24af0fe44e9832e03e00f71477c(eventobject) {
    var self = this;
    this.dropdownPreshow();
}

function AS_FlexContainer_i16f2c33414c4c42b14cd2176dc29508(eventobject) {
    var self = this;
    this.flxServiceManagementLink_onClick();
}

function AS_FlexContainer_i2856c0af5344ff7a89d232e263c2efb(eventobject) {
    var self = this;
    return self.showOrHideContent.call(this, null);
}

function AS_FlexContainer_i374785a23c140e3b491464e58392fca(eventobject) {
    var self = this;
    this.onSelection(this.view.flxLbl5);
    this.view.lbl5.skin = "sknLblffffff13px";
}

function AS_FlexContainer_i3c64aa3bf954cdc83ab33ccf94c6f1f(eventobject) {
    var self = this;
    return self.onClickOfAddMoreSecond.call(this);
}

function AS_FlexContainer_i686549ea6614cec80d91df4eb10f58d(eventobject) {
    var self = this;
    this.flxRoles_onClick();
}

function AS_FlexContainer_ia8fc1ad4b674b798374d5ef2d77b494(eventobject) {
    var self = this;
    return self.deviceAuthenticatorPreShow.call(this);
}

function AS_FlexContainer_iac1b50fdcd94437aa24ea88a15cf66f(eventobject) {
    var self = this;
    this.flxPerm_onClick();
}

function AS_FlexContainer_if2e2512797f4b31a17f2d88766e943e(eventobject) {
    var self = this;
    this.flxRoles_onClick();
}

function AS_FlexContainer_if81ffb9cf1c4d008b3ea0c026641c7a(eventobject) {
    return self.closePopup.call(this);
}

function AS_FlexContainer_j03bd2d0f88e4e579a06c20218606cde(eventobject) {
    var self = this;
}

function AS_FlexContainer_j06de391c8ed45338732225ebb4947f7(eventobject) {
    var self = this;
    return self.viewPopupEventsPreShow.call(this);
}

function AS_FlexContainer_j1a6b556b4e842839c809061c5560949(eventobject) {
    var self = this;
}

function AS_FlexContainer_j38cb78661d54e15a106fac8d207e4f1(eventobject) {
    var self = this;
    this.setFlowActions();
}

function AS_FlexContainer_j5d31046235542f0bcdc5d5f2452d3a2(eventobject) {
    var self = this;
    this.callPreshow();
}

function AS_FlexContainer_jb1e5ad37cf44c0a8d5cbece4f744a5a(eventobject) {
    var self = this;
    this.flxPerm_onClick();
}

function AS_FlexContainer_jb37e8f7283c41c0a0f404790a3eb963(eventobject) {
    var self = this;
}

function AS_FlexContainer_jbced4acd60546a6a3f8757e8eef1055(eventobject) {
    var self = this;
    this.view.timePicker.lstbxAMPM.skin = "sknlbxNobgNoBorderPagination";
    this.view.timePicker.flxOuterBorder.skin = "slFbox";
}

function AS_FlexContainer_jfbe463fab65484d9f1d4795d5c021aa(eventobject) {
    var self = this;
    this.flxPerm_onClick();
}

function AS_Image_a29d2d861fae4f199b0ad251628f6a49(eventobject, x, y) {
    var self = this;
    this.showOptionFlx();
}

function AS_Image_f13cddba0f8c4d9ca80d7502eacf3bcf(eventobject, x, y) {
    var self = this;
}

function AS_Image_h9a6f7c2d55648a1bb3955286cf43397(eventobject, x, y) {
    var self = this;
    this.showOptionFlx();
}

function AS_Image_j15f5719709148459da2ca8b6b9f27da(eventobject, x, y) {
    var self = this;
    this.imgLogout_onTouchStart();
}

function AS_RadioButtonGroup_b6f7d3e0b3bb4f9b9664831ae29fdeff(eventobject) {
    var self = this;
    if (this.view.rbgYearsCoA.selectedKey == "rbg2") {
        this.view.flxAddress2CoA.isVisible = true
    } else {
        this.view.flxAddress2CoA.isVisible = false;
    }
}

function AS_RadioButtonGroup_d3ecc4939c434e068ca9bd4d818765b5(eventobject) {
    var self = this;
    if (this.view.rbgYears.selectedKey == "rbg2") {
        this.view.flxAddress2.isVisible = true;
    } else {
        this.view.flxAddress2.isVisible = false;
    }
}

function AS_Segment_b767df7c8e0647a087391c42e1b9d453(eventobject, sectionNumber, rowNumber) {
    var self = this;
    this.showSchedulerDetails();
}

function AS_TextArea_c708c44f39cf4c0b82b43ff1b7e72655(eventobject) {
    var self = this;
    return self.onDescriptionKeyUp.call(this, null);
}

function AS_TextField_g8cd2b1cc65c467b8ff124e7e99f9809(eventobject, x, y) {
    var self = this;
    kony.print("dsfsdf" + this.view.flxSearchCancel.isVisible);
    this.view.flxSearchCancel.isVisible = true;
}

function AS_TextField_h16715bb231c48e08a309fc5f4e77e94(eventobject, x, y) {
    var self = this;
    this.view.imgSearchCancel.isVisible = true;
}

function AS_TextField_jb095e09de01466983d100ef4c631c27(eventobject, changedtext) {
    var self = this;
}