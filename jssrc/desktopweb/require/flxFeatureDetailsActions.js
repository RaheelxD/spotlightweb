define("flxFeatureDetailsActions", function() {
    return function(controller) {
        var flxFeatureDetailsActions = new kony.ui.FlexContainer({
            "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
            "clipBounds": true,
            "focusSkin": "sknflxffffffop100",
            "id": "flxFeatureDetailsActions",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "isModalContainer": false,
            "skin": "sknflxffffffop100"
        }, {}, {});
        flxFeatureDetailsActions.setDefaultUnit(kony.flex.DP);
        var lblActionName = new kony.ui.Label({
            "bottom": "15px",
            "id": "lblActionName",
            "isVisible": true,
            "left": "30dp",
            "skin": "sknlblLatoBold35475f14px",
            "text": "Mobile App",
            "top": "15px",
            "width": "30%",
            "zIndex": 1
        }, {
            "contentAlignment": constants.CONTENT_ALIGN_TOP_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, {});
        var lblActionDescription = new kony.ui.Label({
            "bottom": "15px",
            "id": "lblActionDescription",
            "isVisible": true,
            "left": "40%",
            "skin": "sknlblLato5bc06cBold14px",
            "text": "Handle your business Payables  efficiently",
            "top": "15px",
            "width": "52%",
            "zIndex": 1
        }, {
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, {});
        var lblSeparator = new kony.ui.Label({
            "bottom": "0px",
            "centerX": "50%",
            "height": "1dp",
            "id": "lblSeparator",
            "isVisible": true,
            "left": "20dp",
            "right": "20dp",
            "skin": "sknlblSeperator",
            "text": "Label",
            "zIndex": 2
        }, {
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, {});
        flxFeatureDetailsActions.add(lblActionName, lblActionDescription, lblSeparator);
        return flxFeatureDetailsActions;
    }
})