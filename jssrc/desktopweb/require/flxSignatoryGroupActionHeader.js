define("flxSignatoryGroupActionHeader", function() {
    return function(controller) {
        var flxSignatoryGroupActionHeader = new kony.ui.FlexContainer({
            "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
            "clipBounds": true,
            "id": "flxSignatoryGroupActionHeader",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "left": "0dp",
            "isModalContainer": false,
            "skin": "sknFlxFFFFFF1000",
            "top": "0dp",
            "width": "100%"
        }, {}, {});
        flxSignatoryGroupActionHeader.setDefaultUnit(kony.flex.DP);
        var flxHeader = new kony.ui.FlexContainer({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "clipBounds": true,
            "height": "65dp",
            "id": "flxHeader",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "left": "15px",
            "isModalContainer": false,
            "right": "20px",
            "skin": "slFbox",
            "top": "0px",
            "zIndex": 1
        }, {}, {});
        flxHeader.setDefaultUnit(kony.flex.DP);
        var flxActionDetails = new kony.ui.FlexContainer({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "clipBounds": true,
            "height": "65dp",
            "id": "flxActionDetails",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "left": "0dp",
            "isModalContainer": false,
            "skin": "slFbox",
            "top": "0dp",
            "width": "100%"
        }, {}, {});
        flxActionDetails.setDefaultUnit(kony.flex.DP);
        var flxRow1 = new kony.ui.FlexContainer({
            "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
            "centerY": "50%",
            "clipBounds": true,
            "id": "flxRow1",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "left": "0dp",
            "isModalContainer": false,
            "skin": "slFbox",
            "top": "12px",
            "width": "100%"
        }, {}, {});
        flxRow1.setDefaultUnit(kony.flex.DP);
        var flxToggle = new kony.ui.FlexContainer({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "clipBounds": true,
            "height": "13dp",
            "id": "flxToggle",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "left": "0dp",
            "isModalContainer": false,
            "right": "0px",
            "skin": "slFbox",
            "top": "3dp",
            "width": "13dp",
            "zIndex": 1
        }, {}, {
            "hoverSkin": "sknCursor"
        });
        flxToggle.setDefaultUnit(kony.flex.DP);
        var lblToggle = new kony.ui.Label({
            "centerX": "50%",
            "centerY": "50%",
            "height": "100%",
            "id": "lblToggle",
            "isVisible": true,
            "skin": "sknLblIIcoMoon485c7511px",
            "i18n_text": "kony.i18n.getLocalizedString(\"i18n.userwidgetmodel.fontIconBreadcrumbsDown\")",
            "width": "100%",
            "zIndex": 1
        }, {
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, {});
        flxToggle.add(lblToggle);
        var lblFeatureName = new kony.ui.Label({
            "id": "lblFeatureName",
            "isVisible": true,
            "left": "25px",
            "skin": "sknLbl192B45LatoRegular14px",
            "text": "International Transfers",
            "top": "0",
            "width": "70%",
            "zIndex": 1
        }, {
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, {});
        var flxRight = new kony.ui.FlexContainer({
            "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
            "clipBounds": true,
            "id": "flxRight",
            "isVisible": true,
            "layoutType": kony.flex.FLOW_HORIZONTAL,
            "reverseLayoutDirection": false,
            "isModalContainer": false,
            "right": 0,
            "skin": "slFbox",
            "top": "0dp",
            "width": "10%"
        }, {}, {});
        flxRight.setDefaultUnit(kony.flex.DP);
        var statusIcon = new kony.ui.Label({
            "id": "statusIcon",
            "isVisible": true,
            "left": "0dp",
            "skin": "sknFontIconActivate",
            "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmAlertsManagement.lblViewAlertStatusKey\")",
            "top": 2,
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, {
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, {});
        var statusValue = new kony.ui.Label({
            "id": "statusValue",
            "isVisible": true,
            "left": "5dp",
            "skin": "sknlblLato5bc06cBold14px",
            "i18n_text": "kony.i18n.getLocalizedString(\"i18n.secureimage.Active\")",
            "top": "0px",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, {
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, {});
        flxRight.add(statusIcon, statusValue);
        flxRow1.add(flxToggle, lblFeatureName, flxRight);
        var flxRow2 = new kony.ui.FlexContainer({
            "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
            "clipBounds": true,
            "id": "flxRow2",
            "isVisible": true,
            "layoutType": kony.flex.FLOW_HORIZONTAL,
            "left": "0dp",
            "isModalContainer": false,
            "skin": "slFbox",
            "top": "40dp",
            "width": "100%"
        }, {}, {});
        flxRow2.setDefaultUnit(kony.flex.DP);
        var lblMonetaryActions = new kony.ui.Label({
            "id": "lblMonetaryActions",
            "isVisible": true,
            "left": "25dp",
            "skin": "sknLbl485C75LatoRegular13Px",
            "text": "Monetary Actions:",
            "top": "0dp",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, {
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, {});
        var lblCountActions = new kony.ui.Label({
            "id": "lblCountActions",
            "isVisible": true,
            "left": "10dp",
            "skin": "sknLbl192b45LatoBold13px",
            "text": "2",
            "top": "0dp",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, {
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, {});
        flxRow2.add(lblMonetaryActions, lblCountActions);
        flxActionDetails.add(flxRow1, flxRow2);
        flxHeader.add(flxActionDetails);
        var flxViewAccountsHeader = new kony.ui.FlexContainer({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "clipBounds": true,
            "height": "40px",
            "id": "flxViewAccountsHeader",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "left": "10px",
            "isModalContainer": false,
            "right": "10px",
            "skin": "slFbox",
            "top": "65px"
        }, {}, {});
        flxViewAccountsHeader.setDefaultUnit(kony.flex.DP);
        var flxAccountNameHeader = new kony.ui.FlexContainer({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "centerY": "50%",
            "clipBounds": true,
            "height": "100%",
            "id": "flxAccountNameHeader",
            "isVisible": true,
            "layoutType": kony.flex.FLOW_HORIZONTAL,
            "left": "30px",
            "isModalContainer": false,
            "onClick": controller.AS_FlexContainer_a5084e29a733477596892b84c89d16f9,
            "skin": "slFbox",
            "top": 0,
            "width": "25%",
            "zIndex": 1
        }, {}, {});
        flxAccountNameHeader.setDefaultUnit(kony.flex.DP);
        var lblAccountNameHeader = new kony.ui.Label({
            "centerY": "50%",
            "id": "lblAccountNameHeader",
            "isVisible": true,
            "left": "0px",
            "skin": "sknlblLato696c7311px",
            "text": "ACCOUNT NAME",
            "top": 0,
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, {
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, {});
        var fontIconAccNameSort = new kony.ui.Label({
            "centerY": "50%",
            "id": "fontIconAccNameSort",
            "isVisible": true,
            "left": "10dp",
            "skin": "sknIcon13px",
            "text": "",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, {
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, {});
        flxAccountNameHeader.add(lblAccountNameHeader, fontIconAccNameSort);
        var flxAccountNumberHeader = new kony.ui.FlexContainer({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "centerY": "50%",
            "clipBounds": true,
            "height": "100%",
            "id": "flxAccountNumberHeader",
            "isVisible": true,
            "layoutType": kony.flex.FLOW_HORIZONTAL,
            "left": "30%",
            "isModalContainer": false,
            "onClick": controller.AS_FlexContainer_a5084e29a733477596892b84c89d16f9,
            "skin": "slFbox",
            "top": 0,
            "width": "25%",
            "zIndex": 1
        }, {}, {});
        flxAccountNumberHeader.setDefaultUnit(kony.flex.DP);
        var lblAccountNumberHeader = new kony.ui.Label({
            "centerY": "50%",
            "id": "lblAccountNumberHeader",
            "isVisible": true,
            "left": "0px",
            "skin": "sknlblLato696c7311px",
            "text": "ACCOUNT NUMBER",
            "top": 0,
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, {
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, {});
        flxAccountNumberHeader.add(lblAccountNumberHeader);
        var flxAccountTypeHeader = new kony.ui.FlexContainer({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "centerY": "50%",
            "clipBounds": true,
            "height": "100%",
            "id": "flxAccountTypeHeader",
            "isVisible": true,
            "layoutType": kony.flex.FLOW_HORIZONTAL,
            "left": "60%",
            "isModalContainer": false,
            "skin": "slFbox",
            "top": 0,
            "width": "20%",
            "zIndex": 1
        }, {}, {});
        flxAccountTypeHeader.setDefaultUnit(kony.flex.DP);
        var lblAccountTypeHeader = new kony.ui.Label({
            "centerY": "50%",
            "id": "lblAccountTypeHeader",
            "isVisible": true,
            "left": "0px",
            "skin": "sknlblLato696c7311px",
            "text": "ACCOUNT TYPE",
            "top": 0,
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, {
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, {});
        var fontIconAccTypeFilter = new kony.ui.Label({
            "centerY": "50%",
            "id": "fontIconAccTypeFilter",
            "isVisible": true,
            "left": "5dp",
            "skin": "sknIcon13px",
            "text": "",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, {
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, {});
        flxAccountTypeHeader.add(lblAccountTypeHeader, fontIconAccTypeFilter);
        var lblFASeperator2 = new kony.ui.Label({
            "bottom": 0,
            "height": "1px",
            "id": "lblFASeperator2",
            "isVisible": true,
            "left": "10px",
            "right": "10px",
            "skin": "sknlblSeperatorD7D9E0",
            "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmCSR.lblSeperator\")",
            "zIndex": 1
        }, {
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, {});
        flxViewAccountsHeader.add(flxAccountNameHeader, flxAccountNumberHeader, flxAccountTypeHeader, lblFASeperator2);
        var lblFASeperator1 = new kony.ui.Label({
            "height": "1px",
            "id": "lblFASeperator1",
            "isVisible": true,
            "left": "0px",
            "right": "0dp",
            "skin": "sknLblSeparatore7e7e7",
            "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmCSR.lblSeperator\")",
            "top": "65dp",
            "zIndex": 1
        }, {
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, {});
        var lblFASeperatorTop = new kony.ui.Label({
            "height": "1px",
            "id": "lblFASeperatorTop",
            "isVisible": true,
            "left": "0px",
            "right": "0dp",
            "skin": "sknLblSeparatore7e7e7",
            "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmCSR.lblSeperator\")",
            "top": "0dp",
            "zIndex": 1
        }, {
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, {});
        flxSignatoryGroupActionHeader.add(flxHeader, flxViewAccountsHeader, lblFASeperator1, lblFASeperatorTop);
        return flxSignatoryGroupActionHeader;
    }
})