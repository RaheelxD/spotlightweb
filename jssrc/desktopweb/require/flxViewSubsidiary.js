define("flxViewSubsidiary", function() {
    return function(controller) {
        var flxViewSubsidiary = new kony.ui.FlexContainer({
            "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
            "clipBounds": true,
            "id": "flxViewSubsidiary",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "isModalContainer": false,
            "skin": "sknflxffffffop100"
        }, {}, {
            "hoverSkin": "sknfbfcfc"
        });
        flxViewSubsidiary.setDefaultUnit(kony.flex.DP);
        var lblCompanyName = new kony.ui.Label({
            "bottom": "15px",
            "id": "lblCompanyName",
            "isVisible": true,
            "left": "20px",
            "skin": "sknLbl485C75LatoRegular13Px",
            "text": "XYZ Company",
            "top": "15px",
            "width": "20%",
            "zIndex": 100
        }, {
            "contentAlignment": constants.CONTENT_ALIGN_TOP_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, {});
        var lblBusinessTypes = new kony.ui.Label({
            "id": "lblBusinessTypes",
            "isVisible": true,
            "left": "30%",
            "skin": "sknLbl485C75LatoRegular13Px",
            "text": "ACH_Collection_1",
            "top": "15px",
            "width": "20%",
            "zIndex": 1
        }, {
            "contentAlignment": constants.CONTENT_ALIGN_TOP_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, {});
        var lblSubmittedOn = new kony.ui.Label({
            "bottom": "15dp",
            "id": "lblSubmittedOn",
            "isVisible": true,
            "left": "55%",
            "skin": "sknLbl485C75LatoRegular13Px",
            "text": "17/08/2017 10:30PM",
            "top": "15px",
            "width": "20%",
            "zIndex": 1
        }, {
            "contentAlignment": constants.CONTENT_ALIGN_TOP_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, {});
        var lblSubmittedBy = new kony.ui.Label({
            "bottom": "15dp",
            "id": "lblSubmittedBy",
            "isVisible": true,
            "left": "80%",
            "skin": "sknLbl485C75LatoRegular13Px",
            "text": "JOHN PETER",
            "top": "15px",
            "width": "20%",
            "zIndex": 1
        }, {
            "contentAlignment": constants.CONTENT_ALIGN_TOP_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, {});
        var lblSeperator = new kony.ui.Label({
            "bottom": 0,
            "height": "1px",
            "id": "lblSeperator",
            "isVisible": true,
            "left": "20dp",
            "right": "20dp",
            "skin": "sknlblSeperator",
            "text": ".",
            "zIndex": 1
        }, {
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, {});
        flxViewSubsidiary.add(lblCompanyName, lblBusinessTypes, lblSubmittedOn, lblSubmittedBy, lblSeperator);
        return flxViewSubsidiary;
    }
})