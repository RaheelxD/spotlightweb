define(function() {
    return function(controller) {
        var policies = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
            "clipBounds": true,
            "isMaster": true,
            "id": "policies",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "left": "0dp",
            "isModalContainer": false,
            "skin": "slFbox",
            "top": "0dp",
            "width": "100%"
        }, controller.args[0], "policies"), extendConfig({}, controller.args[1], "policies"), extendConfig({}, controller.args[2], "policies"));
        policies.setDefaultUnit(kony.flex.DP);
        var flxPoliciesWrapper = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
            "clipBounds": true,
            "id": "flxPoliciesWrapper",
            "isVisible": true,
            "layoutType": kony.flex.FLOW_VERTICAL,
            "left": "0dp",
            "isModalContainer": false,
            "right": "0dp",
            "skin": "sknflxffffffOp100Border5e90cbRadius3Px",
            "top": "0dp",
            "width": "460px",
            "zIndex": 1
        }, controller.args[0], "flxPoliciesWrapper"), extendConfig({}, controller.args[1], "flxPoliciesWrapper"), extendConfig({}, controller.args[2], "flxPoliciesWrapper"));
        flxPoliciesWrapper.setDefaultUnit(kony.flex.DP);
        var flxCustomerHeader = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "clipBounds": true,
            "height": "82dp",
            "id": "flxCustomerHeader",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "left": "0dp",
            "isModalContainer": false,
            "skin": "sknPoliciesHeader",
            "top": "0dp",
            "width": "100%",
            "zIndex": 1
        }, controller.args[0], "flxCustomerHeader"), extendConfig({}, controller.args[1], "flxCustomerHeader"), extendConfig({}, controller.args[2], "flxCustomerHeader"));
        flxCustomerHeader.setDefaultUnit(kony.flex.DP);
        var lblCustomerHeader = new kony.ui.Label(extendConfig({
            "id": "lblCustomerHeader",
            "isVisible": true,
            "left": "37dp",
            "skin": "sknLbl192b45LatoReg16px",
            "i18n_text": "kony.i18n.getLocalizedString(\"i18n.leftmenu.customers\")",
            "top": "32dp",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, controller.args[0], "lblCustomerHeader"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblCustomerHeader"), extendConfig({}, controller.args[2], "lblCustomerHeader"));
        var lblCustomerIcon = new kony.ui.Label(extendConfig({
            "id": "lblCustomerIcon",
            "isVisible": true,
            "right": "52dp",
            "skin": "lblIconBlueCustomers",
            "text": "",
            "top": "32dp",
            "width": "70dp",
            "zIndex": 1
        }, controller.args[0], "lblCustomerIcon"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_CENTER,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblCustomerIcon"), extendConfig({}, controller.args[2], "lblCustomerIcon"));
        flxCustomerHeader.add(lblCustomerHeader, lblCustomerIcon);
        var flxUsernamePolicies = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
            "clipBounds": true,
            "id": "flxUsernamePolicies",
            "isVisible": true,
            "layoutType": kony.flex.FLOW_VERTICAL,
            "left": "20dp",
            "isModalContainer": false,
            "right": "20dp",
            "skin": "sknflxBorder",
            "top": "20dp",
            "width": "96%"
        }, controller.args[0], "flxUsernamePolicies"), extendConfig({}, controller.args[1], "flxUsernamePolicies"), extendConfig({
            "hoverSkin": "blocksHover"
        }, controller.args[2], "flxUsernamePolicies"));
        flxUsernamePolicies.setDefaultUnit(kony.flex.DP);
        var flxPoliciesDataWrapper = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
            "clipBounds": true,
            "id": "flxPoliciesDataWrapper",
            "isVisible": true,
            "layoutType": kony.flex.FLOW_VERTICAL,
            "left": "20dp",
            "isModalContainer": false,
            "skin": "slFbox",
            "top": "21dp",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, controller.args[0], "flxPoliciesDataWrapper"), extendConfig({}, controller.args[1], "flxPoliciesDataWrapper"), extendConfig({}, controller.args[2], "flxPoliciesDataWrapper"));
        flxPoliciesDataWrapper.setDefaultUnit(kony.flex.DP);
        var flxUsernamePoliciesName = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "clipBounds": true,
            "height": "20px",
            "id": "flxUsernamePoliciesName",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "left": "0dp",
            "isModalContainer": false,
            "right": "0dp",
            "skin": "slFbox",
            "top": "0dp",
            "width": "96%"
        }, controller.args[0], "flxUsernamePoliciesName"), extendConfig({}, controller.args[1], "flxUsernamePoliciesName"), extendConfig({}, controller.args[2], "flxUsernamePoliciesName"));
        flxUsernamePoliciesName.setDefaultUnit(kony.flex.DP);
        var lblUsernamePolicies = new kony.ui.Label(extendConfig({
            "id": "lblUsernamePolicies",
            "isVisible": true,
            "left": "0px",
            "skin": "sknLblLatoRegular9CA9BA11Px",
            "text": "USERNAME POLICIES",
            "top": "0dp",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, controller.args[0], "lblUsernamePolicies"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblUsernamePolicies"), extendConfig({}, controller.args[2], "lblUsernamePolicies"));
        var flxEdituserNameContainer = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "clipBounds": true,
            "height": "20dp",
            "id": "flxEdituserNameContainer",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "isModalContainer": false,
            "right": "0px",
            "skin": "sknCursor",
            "top": "0dp",
            "width": "20dp"
        }, controller.args[0], "flxEdituserNameContainer"), extendConfig({}, controller.args[1], "flxEdituserNameContainer"), extendConfig({}, controller.args[2], "flxEdituserNameContainer"));
        flxEdituserNameContainer.setDefaultUnit(kony.flex.DP);
        var lblEditIconUsername = new kony.ui.Label(extendConfig({
            "height": "20px",
            "id": "lblEditIconUsername",
            "isVisible": true,
            "left": "0px",
            "skin": "sknIcon20pxWhiteHover",
            "i18n_text": "kony.i18n.getLocalizedString(\"i18n.userwidgetmodel.lblIconOption1\")",
            "top": "0px",
            "width": "20dp",
            "zIndex": 1
        }, controller.args[0], "lblEditIconUsername"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblEditIconUsername"), extendConfig({}, controller.args[2], "lblEditIconUsername"));
        flxEdituserNameContainer.add(lblEditIconUsername);
        flxUsernamePoliciesName.add(lblUsernamePolicies, flxEdituserNameContainer);
        var lblUsernamePoliciesValue = new kony.ui.RichText(extendConfig({
            "bottom": "20dp",
            "id": "lblUsernamePoliciesValue",
            "isVisible": true,
            "left": "0dp",
            "linkSkin": "defRichTextLink",
            "skin": "sknrtxLato485c7514px",
            "text": "Length of username must be 8 characters long but should not be more than 64 characters. Username must contain a symbol (.,-,_,@,!,#,$).",
            "top": "20dp",
            "width": "91.30%",
            "zIndex": 1
        }, controller.args[0], "lblUsernamePoliciesValue"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [2, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblUsernamePoliciesValue"), extendConfig({}, controller.args[2], "lblUsernamePoliciesValue"));
        flxPoliciesDataWrapper.add(flxUsernamePoliciesName, lblUsernamePoliciesValue);
        flxUsernamePolicies.add(flxPoliciesDataWrapper);
        var flxPasswordPolicies = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
            "bottom": "20dp",
            "clipBounds": true,
            "focusSkin": "blocksHover",
            "id": "flxPasswordPolicies",
            "isVisible": true,
            "layoutType": kony.flex.FLOW_VERTICAL,
            "left": "20dp",
            "isModalContainer": false,
            "right": "20dp",
            "skin": "sknflxBorder",
            "top": "20dp",
            "width": "96%"
        }, controller.args[0], "flxPasswordPolicies"), extendConfig({}, controller.args[1], "flxPasswordPolicies"), extendConfig({
            "hoverSkin": "blocksHover"
        }, controller.args[2], "flxPasswordPolicies"));
        flxPasswordPolicies.setDefaultUnit(kony.flex.DP);
        var flxPasswordPoliciesDataWrapper = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
            "clipBounds": true,
            "id": "flxPasswordPoliciesDataWrapper",
            "isVisible": true,
            "layoutType": kony.flex.FLOW_VERTICAL,
            "left": "20dp",
            "isModalContainer": false,
            "skin": "slFbox",
            "top": "21dp",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, controller.args[0], "flxPasswordPoliciesDataWrapper"), extendConfig({}, controller.args[1], "flxPasswordPoliciesDataWrapper"), extendConfig({}, controller.args[2], "flxPasswordPoliciesDataWrapper"));
        flxPasswordPoliciesDataWrapper.setDefaultUnit(kony.flex.DP);
        var flxPasswordPoliciesWrapper = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "clipBounds": true,
            "height": "20px",
            "id": "flxPasswordPoliciesWrapper",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "left": "0dp",
            "isModalContainer": false,
            "right": "0dp",
            "skin": "slFbox",
            "top": "0dp",
            "width": "96%"
        }, controller.args[0], "flxPasswordPoliciesWrapper"), extendConfig({}, controller.args[1], "flxPasswordPoliciesWrapper"), extendConfig({}, controller.args[2], "flxPasswordPoliciesWrapper"));
        flxPasswordPoliciesWrapper.setDefaultUnit(kony.flex.DP);
        var lblPasswordPolicies = new kony.ui.Label(extendConfig({
            "id": "lblPasswordPolicies",
            "isVisible": true,
            "left": "0px",
            "skin": "sknLblLatoRegular9CA9BA11Px",
            "text": "USERNAME POLICIES",
            "top": "0dp",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, controller.args[0], "lblPasswordPolicies"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblPasswordPolicies"), extendConfig({}, controller.args[2], "lblPasswordPolicies"));
        var flxEditPasswordContainer = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "clipBounds": true,
            "height": "20dp",
            "id": "flxEditPasswordContainer",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "isModalContainer": false,
            "right": "0px",
            "skin": "sknCursor",
            "top": "0dp",
            "width": "20dp"
        }, controller.args[0], "flxEditPasswordContainer"), extendConfig({}, controller.args[1], "flxEditPasswordContainer"), extendConfig({}, controller.args[2], "flxEditPasswordContainer"));
        flxEditPasswordContainer.setDefaultUnit(kony.flex.DP);
        var lblEditIconPassword = new kony.ui.Label(extendConfig({
            "height": "20px",
            "id": "lblEditIconPassword",
            "isVisible": true,
            "left": "0px",
            "skin": "sknIcon20pxWhiteHover",
            "i18n_text": "kony.i18n.getLocalizedString(\"i18n.userwidgetmodel.lblIconOption1\")",
            "top": "0px",
            "width": "20dp",
            "zIndex": 1
        }, controller.args[0], "lblEditIconPassword"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblEditIconPassword"), extendConfig({}, controller.args[2], "lblEditIconPassword"));
        flxEditPasswordContainer.add(lblEditIconPassword);
        flxPasswordPoliciesWrapper.add(lblPasswordPolicies, flxEditPasswordContainer);
        var lblPasswordPoliciesValue = new kony.ui.RichText(extendConfig({
            "bottom": "20dp",
            "id": "lblPasswordPoliciesValue",
            "isVisible": true,
            "left": "0dp",
            "linkSkin": "defRichTextLink",
            "skin": "sknrtxLato485c7514px",
            "text": "Length of username must be 8 characters long but should not be more than 64 characters. Username must contain a symbol (.,-,_,@,!,#,$).",
            "top": "20dp",
            "width": "91.30%",
            "zIndex": 1
        }, controller.args[0], "lblPasswordPoliciesValue"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [2, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblPasswordPoliciesValue"), extendConfig({}, controller.args[2], "lblPasswordPoliciesValue"));
        flxPasswordPoliciesDataWrapper.add(flxPasswordPoliciesWrapper, lblPasswordPoliciesValue);
        flxPasswordPolicies.add(flxPasswordPoliciesDataWrapper);
        flxPoliciesWrapper.add(flxCustomerHeader, flxUsernamePolicies, flxPasswordPolicies);
        policies.add(flxPoliciesWrapper);
        return policies;
    }
})