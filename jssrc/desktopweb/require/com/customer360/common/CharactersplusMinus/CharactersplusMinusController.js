define("com/customer360/common/CharactersplusMinus/userCharactersplusMinusController", function() {
    return {};
});
define("com/customer360/common/CharactersplusMinus/CharactersplusMinusControllerActions", {
    /*
          This is an auto generated file and any modifications to it may result in corruption of the action sequence.
        */
});
define("com/customer360/common/CharactersplusMinus/CharactersplusMinusController", ["com/customer360/common/CharactersplusMinus/userCharactersplusMinusController", "com/customer360/common/CharactersplusMinus/CharactersplusMinusControllerActions"], function() {
    var controller = require("com/customer360/common/CharactersplusMinus/userCharactersplusMinusController");
    var actions = require("com/customer360/common/CharactersplusMinus/CharactersplusMinusControllerActions");
    for (var key in actions) {
        controller[key] = actions[key];
    }
    return controller;
});
