define(function() {
    return function(controller) {
        var CharactersplusMinus = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
            "clipBounds": true,
            "isMaster": true,
            "id": "CharactersplusMinus",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "left": "0dp",
            "isModalContainer": false,
            "skin": "slFbox",
            "top": "0dp",
            "width": "181dp"
        }, controller.args[0], "CharactersplusMinus"), extendConfig({}, controller.args[1], "CharactersplusMinus"), extendConfig({}, controller.args[2], "CharactersplusMinus"));
        CharactersplusMinus.setDefaultUnit(kony.flex.DP);
        var tbxcharcterSize = new kony.ui.TextBox2(extendConfig({
            "autoCapitalize": constants.TEXTBOX_AUTO_CAPITALIZE_NONE,
            "focusSkin": "defTextBoxFocus",
            "height": "40dp",
            "id": "tbxcharcterSize",
            "isVisible": true,
            "keyBoardStyle": constants.TEXTBOX_KEY_BOARD_STYLE_DEFAULT,
            "left": "0dp",
            "maxTextLength": 2,
            "secureTextEntry": false,
            "skin": "skntbxLato35475f14px",
            "textInputMode": constants.TEXTBOX_INPUT_MODE_NUMERIC,
            "top": "0dp",
            "width": "181dp",
            "zIndex": 1
        }, controller.args[0], "tbxcharcterSize"), extendConfig({
            "containerHeightMode": constants.TEXTBOX_FONT_METRICS_DRIVEN_HEIGHT,
            "contentAlignment": constants.CONTENT_ALIGN_CENTER,
            "padding": [3, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "tbxcharcterSize"), extendConfig({
            "autoCorrect": false,
            "placeholderSkin": "defTextBoxPlaceholder"
        }, controller.args[2], "tbxcharcterSize"));
        var flxPlus = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "bottom": "0dp",
            "clipBounds": true,
            "focusSkin": "sknflxffffffBorderLeftRound1293cc",
            "height": "40dp",
            "id": "flxPlus",
            "isVisible": true,
            "layoutType": kony.flex.FLOW_HORIZONTAL,
            "left": "0dp",
            "isModalContainer": false,
            "skin": "slFbox",
            "top": "0dp",
            "width": "54dp",
            "zIndex": 2
        }, controller.args[0], "flxPlus"), extendConfig({}, controller.args[1], "flxPlus"), extendConfig({}, controller.args[2], "flxPlus"));
        flxPlus.setDefaultUnit(kony.flex.DP);
        var lblPlus = new kony.ui.Label(extendConfig({
            "centerX": "50%",
            "centerY": "50%",
            "height": "37dp",
            "id": "lblPlus",
            "isVisible": true,
            "left": "0dp",
            "skin": "sknlbl696C7320pxLatoReg",
            "text": "-",
            "top": "0dp",
            "width": "52dp",
            "zIndex": 1
        }, controller.args[0], "lblPlus"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_CENTER,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblPlus"), extendConfig({}, controller.args[2], "lblPlus"));
        flxPlus.add(lblPlus);
        var flxPlusDisable = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "clipBounds": true,
            "focusSkin": "sknflxffffffBorderd6dbe7Radius4pxFocus",
            "height": "38dp",
            "id": "flxPlusDisable",
            "isVisible": false,
            "layoutType": kony.flex.FREE_FORM,
            "left": "1dp",
            "isModalContainer": false,
            "skin": "sknFlxEAEBEFunits",
            "top": "1dp",
            "width": "52dp",
            "zIndex": 2
        }, controller.args[0], "flxPlusDisable"), extendConfig({}, controller.args[1], "flxPlusDisable"), extendConfig({
            "hoverSkin": "sknCursorDisabled"
        }, controller.args[2], "flxPlusDisable"));
        flxPlusDisable.setDefaultUnit(kony.flex.DP);
        var lblPlusDisable = new kony.ui.Label(extendConfig({
            "centerX": "50%",
            "centerY": "50%",
            "id": "lblPlusDisable",
            "isVisible": true,
            "left": "12dp",
            "skin": "sknlblb7b7b720pxLatoReg",
            "text": "-",
            "top": "0dp",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, controller.args[0], "lblPlusDisable"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_CENTER,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblPlusDisable"), extendConfig({}, controller.args[2], "lblPlusDisable"));
        flxPlusDisable.add(lblPlusDisable);
        var flxSeperator1 = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "clipBounds": true,
            "height": "39dp",
            "id": "flxSeperator1",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "left": "53dp",
            "isModalContainer": false,
            "skin": "sknflxd6dbe7",
            "top": "0dp",
            "width": "2dp",
            "zIndex": 1
        }, controller.args[0], "flxSeperator1"), extendConfig({}, controller.args[1], "flxSeperator1"), extendConfig({}, controller.args[2], "flxSeperator1"));
        flxSeperator1.setDefaultUnit(kony.flex.DP);
        flxSeperator1.add();
        var flxMinus = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "bottom": "0dp",
            "clipBounds": true,
            "focusSkin": "sknflxffffffBorderRightRound1293cc",
            "height": "40dp",
            "id": "flxMinus",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "isModalContainer": false,
            "right": "0dp",
            "skin": "slFbox",
            "top": "0dp",
            "width": "54dp",
            "zIndex": 2
        }, controller.args[0], "flxMinus"), extendConfig({}, controller.args[1], "flxMinus"), extendConfig({}, controller.args[2], "flxMinus"));
        flxMinus.setDefaultUnit(kony.flex.DP);
        var flxHideNumericIncrementor = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "bottom": "2dp",
            "clipBounds": true,
            "id": "flxHideNumericIncrementor",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "left": "2dp",
            "isModalContainer": false,
            "right": "2dp",
            "skin": "sknflxConfigurationBundlesNoBorder",
            "top": "2dp"
        }, controller.args[0], "flxHideNumericIncrementor"), extendConfig({}, controller.args[1], "flxHideNumericIncrementor"), extendConfig({}, controller.args[2], "flxHideNumericIncrementor"));
        flxHideNumericIncrementor.setDefaultUnit(kony.flex.DP);
        var lblMinus = new kony.ui.Label(extendConfig({
            "centerX": "50%",
            "centerY": "50%",
            "height": "37dp",
            "id": "lblMinus",
            "isVisible": true,
            "left": "0dp",
            "skin": "sknlbl696C7320pxLatoReg",
            "text": "+",
            "top": "0dp",
            "width": "52dp",
            "zIndex": 1
        }, controller.args[0], "lblMinus"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_CENTER,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblMinus"), extendConfig({}, controller.args[2], "lblMinus"));
        flxHideNumericIncrementor.add(lblMinus);
        flxMinus.add(flxHideNumericIncrementor);
        var flxMinusDisable = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "clipBounds": true,
            "focusSkin": "sknflxffffffBorderd6dbe7Radius4pxFocus",
            "height": "38dp",
            "id": "flxMinusDisable",
            "isVisible": false,
            "layoutType": kony.flex.FREE_FORM,
            "isModalContainer": false,
            "right": "1dp",
            "skin": "sknFlxEAEBEFunits",
            "top": "1dp",
            "width": "52dp",
            "zIndex": 2
        }, controller.args[0], "flxMinusDisable"), extendConfig({}, controller.args[1], "flxMinusDisable"), extendConfig({
            "hoverSkin": "sknCursorDisabled"
        }, controller.args[2], "flxMinusDisable"));
        flxMinusDisable.setDefaultUnit(kony.flex.DP);
        var lblMinusDisable = new kony.ui.Label(extendConfig({
            "centerX": "50%",
            "centerY": "50%",
            "id": "lblMinusDisable",
            "isVisible": true,
            "left": "0dp",
            "skin": "sknlblb7b7b720pxLatoReg",
            "text": "+",
            "top": "0dp",
            "width": "52dp",
            "zIndex": 1
        }, controller.args[0], "lblMinusDisable"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_CENTER,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblMinusDisable"), extendConfig({}, controller.args[2], "lblMinusDisable"));
        flxMinusDisable.add(lblMinusDisable);
        var flxSeperator2 = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "clipBounds": true,
            "height": "39dp",
            "id": "flxSeperator2",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "isModalContainer": false,
            "right": "53dp",
            "skin": "sknflxd6dbe7",
            "top": "0dp",
            "width": "2dp",
            "zIndex": 1
        }, controller.args[0], "flxSeperator2"), extendConfig({}, controller.args[1], "flxSeperator2"), extendConfig({}, controller.args[2], "flxSeperator2"));
        flxSeperator2.setDefaultUnit(kony.flex.DP);
        flxSeperator2.add();
        CharactersplusMinus.add(tbxcharcterSize, flxPlus, flxPlusDisable, flxSeperator1, flxMinus, flxMinusDisable, flxSeperator2);
        return CharactersplusMinus;
    }
})