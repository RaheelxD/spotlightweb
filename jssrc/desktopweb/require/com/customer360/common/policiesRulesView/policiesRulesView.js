define(function() {
    return function(controller) {
        var policiesRulesView = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
            "clipBounds": true,
            "isMaster": true,
            "id": "policiesRulesView",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "left": "0dp",
            "isModalContainer": false,
            "skin": "slFbox",
            "top": "0dp",
            "width": "100%"
        }, controller.args[0], "policiesRulesView"), extendConfig({}, controller.args[1], "policiesRulesView"), extendConfig({}, controller.args[2], "policiesRulesView"));
        policiesRulesView.setDefaultUnit(kony.flex.DP);
        var flxPoliciesWrapper = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
            "clipBounds": true,
            "id": "flxPoliciesWrapper",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "left": "0dp",
            "isModalContainer": false,
            "right": "0dp",
            "skin": "sknflxffffffOp100Border5e90cbRadius3Px",
            "top": "0dp",
            "width": "100%",
            "zIndex": 1
        }, controller.args[0], "flxPoliciesWrapper"), extendConfig({}, controller.args[1], "flxPoliciesWrapper"), extendConfig({}, controller.args[2], "flxPoliciesWrapper"));
        flxPoliciesWrapper.setDefaultUnit(kony.flex.DP);
        var flxPoliciesDataWrapper = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
            "clipBounds": true,
            "id": "flxPoliciesDataWrapper",
            "isVisible": true,
            "layoutType": kony.flex.FLOW_VERTICAL,
            "left": "20dp",
            "isModalContainer": false,
            "right": "20dp",
            "skin": "slFbox",
            "top": "22dp",
            "width": "97.50%",
            "zIndex": 1
        }, controller.args[0], "flxPoliciesDataWrapper"), extendConfig({}, controller.args[1], "flxPoliciesDataWrapper"), extendConfig({}, controller.args[2], "flxPoliciesDataWrapper"));
        flxPoliciesDataWrapper.setDefaultUnit(kony.flex.DP);
        var lblPoliciesRuleName = new kony.ui.Label(extendConfig({
            "id": "lblPoliciesRuleName",
            "isVisible": true,
            "left": "0px",
            "skin": "LblLatoRegular485c7516px",
            "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmPolicies.passwordPoliciesForCustomers\")",
            "top": "0dp",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, controller.args[0], "lblPoliciesRuleName"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblPoliciesRuleName"), extendConfig({}, controller.args[2], "lblPoliciesRuleName"));
        var lblSeperator = new kony.ui.Label(extendConfig({
            "bottom": "1dp",
            "height": "1px",
            "id": "lblSeperator",
            "isVisible": true,
            "left": "0dp",
            "right": "0px",
            "skin": "HeaderSeparator",
            "top": "22px",
            "width": "100%",
            "zIndex": 1
        }, controller.args[0], "lblSeperator"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblSeperator"), extendConfig({}, controller.args[2], "lblSeperator"));
        var lblLengthOfPassword = new kony.ui.Label(extendConfig({
            "id": "lblLengthOfPassword",
            "isVisible": true,
            "left": "0px",
            "skin": "sknLbl192b45LatoBold12px",
            "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmPolicies.lengthOfPassword\")",
            "top": "22dp",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, controller.args[0], "lblLengthOfPassword"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblLengthOfPassword"), extendConfig({}, controller.args[2], "lblLengthOfPassword"));
        var flxLengthOfPasswordValues = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "clipBounds": true,
            "height": "44dp",
            "id": "flxLengthOfPasswordValues",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "left": "0dp",
            "isModalContainer": false,
            "skin": "slFbox",
            "top": "20dp",
            "width": "100%",
            "zIndex": 1
        }, controller.args[0], "flxLengthOfPasswordValues"), extendConfig({}, controller.args[1], "flxLengthOfPasswordValues"), extendConfig({}, controller.args[2], "flxLengthOfPasswordValues"));
        flxLengthOfPasswordValues.setDefaultUnit(kony.flex.DP);
        var flxCharacters = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "clipBounds": true,
            "height": "100%",
            "id": "flxCharacters",
            "isVisible": true,
            "layoutType": kony.flex.FLOW_HORIZONTAL,
            "left": "0dp",
            "isModalContainer": false,
            "skin": "slFbox",
            "top": "0dp",
            "width": "100%"
        }, controller.args[0], "flxCharacters"), extendConfig({}, controller.args[1], "flxCharacters"), extendConfig({}, controller.args[2], "flxCharacters"));
        flxCharacters.setDefaultUnit(kony.flex.DP);
        var flxMinCharacters = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "clipBounds": true,
            "height": "100%",
            "id": "flxMinCharacters",
            "isVisible": true,
            "layoutType": kony.flex.FLOW_VERTICAL,
            "left": "0dp",
            "isModalContainer": false,
            "skin": "slFbox",
            "top": "0dp",
            "width": "165dp"
        }, controller.args[0], "flxMinCharacters"), extendConfig({}, controller.args[1], "flxMinCharacters"), extendConfig({}, controller.args[2], "flxMinCharacters"));
        flxMinCharacters.setDefaultUnit(kony.flex.DP);
        var lblMinNumberChars = new kony.ui.Label(extendConfig({
            "id": "lblMinNumberChars",
            "isVisible": true,
            "left": "0dp",
            "skin": "sknlblLato696c7311px",
            "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmPolicies.minNumberOfcharacters\")",
            "top": "0dp",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, controller.args[0], "lblMinNumberChars"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblMinNumberChars"), extendConfig({}, controller.args[2], "lblMinNumberChars"));
        var lblMinNumberCharsValue = new kony.ui.Label(extendConfig({
            "id": "lblMinNumberCharsValue",
            "isVisible": true,
            "left": "0dp",
            "skin": "sknlbl485C75LatoSemiBold13px",
            "text": "8",
            "top": "14dp",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, controller.args[0], "lblMinNumberCharsValue"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblMinNumberCharsValue"), extendConfig({}, controller.args[2], "lblMinNumberCharsValue"));
        flxMinCharacters.add(lblMinNumberChars, lblMinNumberCharsValue);
        var flxMaxCharacters = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "clipBounds": true,
            "height": "100%",
            "id": "flxMaxCharacters",
            "isVisible": true,
            "layoutType": kony.flex.FLOW_VERTICAL,
            "left": "72dp",
            "isModalContainer": false,
            "skin": "slFbox",
            "top": "0dp",
            "width": "168dp"
        }, controller.args[0], "flxMaxCharacters"), extendConfig({}, controller.args[1], "flxMaxCharacters"), extendConfig({}, controller.args[2], "flxMaxCharacters"));
        flxMaxCharacters.setDefaultUnit(kony.flex.DP);
        var lblMaxNumberChars = new kony.ui.Label(extendConfig({
            "id": "lblMaxNumberChars",
            "isVisible": true,
            "left": "0dp",
            "skin": "sknlblLato696c7311px",
            "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmPolicies.maxNumberOfcharacters\")",
            "top": "0dp",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, controller.args[0], "lblMaxNumberChars"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblMaxNumberChars"), extendConfig({}, controller.args[2], "lblMaxNumberChars"));
        var lblMaxNumberCharsValue = new kony.ui.Label(extendConfig({
            "id": "lblMaxNumberCharsValue",
            "isVisible": true,
            "left": "0dp",
            "skin": "sknlbl485C75LatoSemiBold13px",
            "text": "64",
            "top": "14dp",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, controller.args[0], "lblMaxNumberCharsValue"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblMaxNumberCharsValue"), extendConfig({}, controller.args[2], "lblMaxNumberCharsValue"));
        flxMaxCharacters.add(lblMaxNumberChars, lblMaxNumberCharsValue);
        var flxSpecialCharacters = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "clipBounds": true,
            "height": "100%",
            "id": "flxSpecialCharacters",
            "isVisible": true,
            "layoutType": kony.flex.FLOW_VERTICAL,
            "left": "72dp",
            "isModalContainer": false,
            "skin": "slFbox",
            "top": "0dp",
            "width": "204dp"
        }, controller.args[0], "flxSpecialCharacters"), extendConfig({}, controller.args[1], "flxSpecialCharacters"), extendConfig({}, controller.args[2], "flxSpecialCharacters"));
        flxSpecialCharacters.setDefaultUnit(kony.flex.DP);
        var lblSpecialChars = new kony.ui.Label(extendConfig({
            "id": "lblSpecialChars",
            "isVisible": true,
            "left": "0dp",
            "right": "0dp",
            "skin": "sknlblLato696c7311px",
            "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmPolicies.specialCharcaters\")",
            "top": "0dp",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, controller.args[0], "lblSpecialChars"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblSpecialChars"), extendConfig({}, controller.args[2], "lblSpecialChars"));
        var lblSpecialCharsValue = new kony.ui.Label(extendConfig({
            "id": "lblSpecialCharsValue",
            "isVisible": true,
            "left": "0dp",
            "right": "0dp",
            "skin": "sknlbl485C75LatoSemiBold13px",
            "text": "Yes",
            "top": "14dp",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, controller.args[0], "lblSpecialCharsValue"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblSpecialCharsValue"), extendConfig({}, controller.args[2], "lblSpecialCharsValue"));
        flxSpecialCharacters.add(lblSpecialChars, lblSpecialCharsValue);
        flxCharacters.add(flxMinCharacters, flxMaxCharacters, flxSpecialCharacters);
        flxLengthOfPasswordValues.add(flxCharacters);
        var flxPasswordAdditionalRules = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "clipBounds": true,
            "height": "110dp",
            "id": "flxPasswordAdditionalRules",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "left": "0dp",
            "isModalContainer": false,
            "skin": "slFbox",
            "top": "22dp",
            "width": "100%",
            "zIndex": 1
        }, controller.args[0], "flxPasswordAdditionalRules"), extendConfig({}, controller.args[1], "flxPasswordAdditionalRules"), extendConfig({}, controller.args[2], "flxPasswordAdditionalRules"));
        flxPasswordAdditionalRules.setDefaultUnit(kony.flex.DP);
        var flxPasswordRules = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "clipBounds": true,
            "height": "100%",
            "id": "flxPasswordRules",
            "isVisible": true,
            "layoutType": kony.flex.FLOW_HORIZONTAL,
            "left": "0dp",
            "isModalContainer": false,
            "skin": "slFbox",
            "top": "0dp",
            "width": "100%"
        }, controller.args[0], "flxPasswordRules"), extendConfig({}, controller.args[1], "flxPasswordRules"), extendConfig({}, controller.args[2], "flxPasswordRules"));
        flxPasswordRules.setDefaultUnit(kony.flex.DP);
        var flxPasswordMustContain = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "clipBounds": true,
            "height": "100%",
            "id": "flxPasswordMustContain",
            "isVisible": true,
            "layoutType": kony.flex.FLOW_VERTICAL,
            "left": "0dp",
            "isModalContainer": false,
            "skin": "slFbox",
            "top": "0dp",
            "width": "275dp"
        }, controller.args[0], "flxPasswordMustContain"), extendConfig({}, controller.args[1], "flxPasswordMustContain"), extendConfig({}, controller.args[2], "flxPasswordMustContain"));
        flxPasswordMustContain.setDefaultUnit(kony.flex.DP);
        var lblPasswordMustContain = new kony.ui.Label(extendConfig({
            "id": "lblPasswordMustContain",
            "isVisible": true,
            "left": "0dp",
            "skin": "sknLbl192b45LatoBold12px",
            "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmPolicies.passwordMustContain\")",
            "top": "0dp",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, controller.args[0], "lblPasswordMustContain"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblPasswordMustContain"), extendConfig({}, controller.args[2], "lblPasswordMustContain"));
        var lblSelectedOptions = new kony.ui.Label(extendConfig({
            "id": "lblSelectedOptions",
            "isVisible": true,
            "left": "0dp",
            "skin": "sknlblLato696c7311px",
            "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmPolicies.selectedOptions\")",
            "top": "20dp",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, controller.args[0], "lblSelectedOptions"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblSelectedOptions"), extendConfig({}, controller.args[2], "lblSelectedOptions"));
        var lblSelectedOptionsValue = new kony.ui.Label(extendConfig({
            "id": "lblSelectedOptionsValue",
            "isVisible": true,
            "left": "0dp",
            "skin": "sknlbl485C75LatoSemiBold13px",
            "text": "Uppercase letters, Lowercase letters, Numbers, Special characters (., - , _, @, !, #, $)",
            "top": "14dp",
            "width": "275dp",
            "zIndex": 1
        }, controller.args[0], "lblSelectedOptionsValue"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblSelectedOptionsValue"), extendConfig({}, controller.args[2], "lblSelectedOptionsValue"));
        flxPasswordMustContain.add(lblPasswordMustContain, lblSelectedOptions, lblSelectedOptionsValue);
        var flxCharacterRepetition = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "clipBounds": true,
            "height": "100%",
            "id": "flxCharacterRepetition",
            "isVisible": true,
            "layoutType": kony.flex.FLOW_VERTICAL,
            "left": "38dp",
            "isModalContainer": false,
            "skin": "slFbox",
            "top": "0dp",
            "width": "238dp"
        }, controller.args[0], "flxCharacterRepetition"), extendConfig({}, controller.args[1], "flxCharacterRepetition"), extendConfig({}, controller.args[2], "flxCharacterRepetition"));
        flxCharacterRepetition.setDefaultUnit(kony.flex.DP);
        var lblCharacterRepetition = new kony.ui.Label(extendConfig({
            "id": "lblCharacterRepetition",
            "isVisible": true,
            "left": "0dp",
            "skin": "sknLbl192b45LatoBold12px",
            "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmPolicies.characterRepetition\")",
            "top": "0dp",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, controller.args[0], "lblCharacterRepetition"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblCharacterRepetition"), extendConfig({}, controller.args[2], "lblCharacterRepetition"));
        var lblMaxConsecutiveRepetition = new kony.ui.Label(extendConfig({
            "id": "lblMaxConsecutiveRepetition",
            "isVisible": true,
            "left": "0dp",
            "skin": "sknlblLato696c7311px",
            "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmPolicies.maxNumberOfConsecutiveRepetition\")",
            "top": "20dp",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, controller.args[0], "lblMaxConsecutiveRepetition"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblMaxConsecutiveRepetition"), extendConfig({}, controller.args[2], "lblMaxConsecutiveRepetition"));
        var lblMaxConsecutiveRepetitionValue = new kony.ui.Label(extendConfig({
            "id": "lblMaxConsecutiveRepetitionValue",
            "isVisible": true,
            "left": "0dp",
            "skin": "sknlbl485C75LatoSemiBold13px",
            "text": "No restriction",
            "top": "14dp",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, controller.args[0], "lblMaxConsecutiveRepetitionValue"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblMaxConsecutiveRepetitionValue"), extendConfig({}, controller.args[2], "lblMaxConsecutiveRepetitionValue"));
        flxCharacterRepetition.add(lblCharacterRepetition, lblMaxConsecutiveRepetition, lblMaxConsecutiveRepetitionValue);
        flxPasswordRules.add(flxPasswordMustContain, flxCharacterRepetition);
        flxPasswordAdditionalRules.add(flxPasswordRules);
        flxPoliciesDataWrapper.add(lblPoliciesRuleName, lblSeperator, lblLengthOfPassword, flxLengthOfPasswordValues, flxPasswordAdditionalRules);
        var flxEditIcon = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "clipBounds": true,
            "height": "20px",
            "id": "flxEditIcon",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "isModalContainer": false,
            "right": "20px",
            "skin": "hoverhandSkin",
            "top": "20px",
            "width": "20px"
        }, controller.args[0], "flxEditIcon"), extendConfig({}, controller.args[1], "flxEditIcon"), extendConfig({}, controller.args[2], "flxEditIcon"));
        flxEditIcon.setDefaultUnit(kony.flex.DP);
        var lblIconOption1 = new kony.ui.Label(extendConfig({
            "height": "20px",
            "id": "lblIconOption1",
            "isVisible": true,
            "right": "0px",
            "skin": "sknIcon20px",
            "i18n_text": "kony.i18n.getLocalizedString(\"i18n.userwidgetmodel.lblIconOption1\")",
            "top": "0px",
            "width": "20px",
            "zIndex": 1
        }, controller.args[0], "lblIconOption1"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblIconOption1"), extendConfig({}, controller.args[2], "lblIconOption1"));
        flxEditIcon.add(lblIconOption1);
        flxPoliciesWrapper.add(flxPoliciesDataWrapper, flxEditIcon);
        policiesRulesView.add(flxPoliciesWrapper);
        return policiesRulesView;
    }
})