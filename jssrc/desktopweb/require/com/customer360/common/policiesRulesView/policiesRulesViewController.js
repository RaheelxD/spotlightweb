define("com/customer360/common/policiesRulesView/userpoliciesRulesViewController", function() {
    return {};
});
define("com/customer360/common/policiesRulesView/policiesRulesViewControllerActions", {
    /*
          This is an auto generated file and any modifications to it may result in corruption of the action sequence.
        */
});
define("com/customer360/common/policiesRulesView/policiesRulesViewController", ["com/customer360/common/policiesRulesView/userpoliciesRulesViewController", "com/customer360/common/policiesRulesView/policiesRulesViewControllerActions"], function() {
    var controller = require("com/customer360/common/policiesRulesView/userpoliciesRulesViewController");
    var actions = require("com/customer360/common/policiesRulesView/policiesRulesViewControllerActions");
    for (var key in actions) {
        controller[key] = actions[key];
    }
    return controller;
});
