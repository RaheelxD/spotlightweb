define(function() {
    return function(controller) {
        var inputNumber = new kony.ui.FlexContainer(extendConfig({
            "clipBounds": true,
            "isMaster": true,
            "height": "100%",
            "id": "inputNumber",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "left": "0dp",
            "isModalContainer": false,
            "postShow": controller.AS_FlexContainer_j65657caf46c4dad82ca0ad179d5913b,
            "skin": "sknflxffffffop100Bordercbcdd1Radius3px",
            "top": "0dp",
            "width": "100%"
        }, controller.args[0], "inputNumber"), extendConfig({}, controller.args[1], "inputNumber"), extendConfig({}, controller.args[2], "inputNumber"));
        inputNumber.setDefaultUnit(kony.flex.DP);
        var lblValue = new kony.ui.Label(extendConfig({
            "centerX": "50%",
            "centerY": "50%",
            "id": "lblValue",
            "isVisible": true,
            "skin": "sknlblLato5d6c7f12px",
            "text": "0",
            "width": "50%",
            "zIndex": 1
        }, controller.args[0], "lblValue"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_CENTER,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblValue"), extendConfig({}, controller.args[2], "lblValue"));
        var flxSeperator1 = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "centerY": "50%",
            "clipBounds": true,
            "height": "100%",
            "id": "flxSeperator1",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "left": "25%",
            "isModalContainer": false,
            "skin": "sknflxd6dbe7",
            "top": "166dp",
            "width": "1px",
            "zIndex": 100
        }, controller.args[0], "flxSeperator1"), extendConfig({}, controller.args[1], "flxSeperator1"), extendConfig({}, controller.args[2], "flxSeperator1"));
        flxSeperator1.setDefaultUnit(kony.flex.DP);
        flxSeperator1.add();
        var flxSeperator2 = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "centerY": "50%",
            "clipBounds": true,
            "height": "100%",
            "id": "flxSeperator2",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "isModalContainer": false,
            "right": "25%",
            "skin": "sknflxd6dbe7",
            "top": "0dp",
            "width": "1px",
            "zIndex": 100
        }, controller.args[0], "flxSeperator2"), extendConfig({}, controller.args[1], "flxSeperator2"), extendConfig({}, controller.args[2], "flxSeperator2"));
        flxSeperator2.setDefaultUnit(kony.flex.DP);
        flxSeperator2.add();
        var btnMinus = new kony.ui.Button(extendConfig({
            "focusSkin": "btnIcon",
            "height": "100%",
            "id": "btnMinus",
            "isVisible": true,
            "left": "0dp",
            "skin": "btnIcon",
            "text": "",
            "top": "0dp",
            "width": "25%",
            "zIndex": 1
        }, controller.args[0], "btnMinus"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_CENTER,
            "displayText": true,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "btnMinus"), extendConfig({}, controller.args[2], "btnMinus"));
        var btnPlus = new kony.ui.Button(extendConfig({
            "focusSkin": "btnIcon",
            "height": "100%",
            "id": "btnPlus",
            "isVisible": true,
            "right": "0dp",
            "skin": "btnIcon",
            "text": "",
            "top": "0dp",
            "width": "25%",
            "zIndex": 1
        }, controller.args[0], "btnPlus"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_CENTER,
            "displayText": true,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "btnPlus"), extendConfig({}, controller.args[2], "btnPlus"));
        inputNumber.add(lblValue, flxSeperator1, flxSeperator2, btnMinus, btnPlus);
        return inputNumber;
    }
})