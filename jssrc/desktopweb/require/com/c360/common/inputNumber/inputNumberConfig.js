define(function() {
    return {
        "properties": [{
            "name": "maxLimit",
            "enumerable": true,
            "configurable": false,
            "writable": true
        }, {
            "name": "minLimit",
            "enumerable": true,
            "configurable": false,
            "writable": true
        }, {
            "name": "defaultValue",
            "enumerable": true,
            "configurable": false,
            "writable": true
        }],
        "apis": ["getValue", "enableFocus", "disableFocus"],
        "events": ["onChange", "onLimitExceeds", "onLimitLowers"]
    }
});