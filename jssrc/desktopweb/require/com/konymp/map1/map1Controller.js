define("com/konymp/map1/usermap1Controller", function() {
    return {
        addLocationsToMap: function() {}
    };
});
define("com/konymp/map1/map1ControllerActions", {
    /*
        This is an auto generated file and any modifications to it may result in corruption of the action sequence.
    */
    AS_FlexContainer_f103c4f10de64a5ab1f62966ca1bb57e: function AS_FlexContainer_f103c4f10de64a5ab1f62966ca1bb57e(eventobject) {
        var self = this;
        this.addLocationsToMap();
    }
});
define("com/konymp/map1/map1Controller", ["com/konymp/map1/usermap1Controller", "com/konymp/map1/map1ControllerActions"], function() {
    var controller = require("com/konymp/map1/usermap1Controller");
    var actions = require("com/konymp/map1/map1ControllerActions");
    for (var key in actions) {
        controller[key] = actions[key];
    }
    return controller;
});
