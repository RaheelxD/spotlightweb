define(function() {
    return function(controller) {
        var popUp = new kony.ui.FlexContainer(extendConfig({
            "clipBounds": true,
            "isMaster": true,
            "height": "100%",
            "id": "popUp",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "left": "0dp",
            "isModalContainer": false,
            "skin": "sknflxBg000000Op50",
            "top": "0dp",
            "width": "100%"
        }, controller.args[0], "popUp"), extendConfig({}, controller.args[1], "popUp"), extendConfig({}, controller.args[2], "popUp"));
        popUp.setDefaultUnit(kony.flex.DP);
        var flxPopUp = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
            "centerX": "50%",
            "clipBounds": false,
            "id": "flxPopUp",
            "isVisible": true,
            "layoutType": kony.flex.FLOW_VERTICAL,
            "isModalContainer": false,
            "skin": "slFbox0j9f841cc563e4e",
            "top": "250px",
            "width": "600px",
            "zIndex": 10
        }, controller.args[0], "flxPopUp"), extendConfig({}, controller.args[1], "flxPopUp"), extendConfig({}, controller.args[2], "flxPopUp"));
        flxPopUp.setDefaultUnit(kony.flex.DP);
        var flxPopUpTopColor = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "clipBounds": true,
            "height": "10px",
            "id": "flxPopUpTopColor",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "left": "0dp",
            "isModalContainer": false,
            "skin": "sknFlxTopColor4A77A0",
            "top": "0dp",
            "width": "100%",
            "zIndex": 1
        }, controller.args[0], "flxPopUpTopColor"), extendConfig({}, controller.args[1], "flxPopUpTopColor"), extendConfig({}, controller.args[2], "flxPopUpTopColor"));
        flxPopUpTopColor.setDefaultUnit(kony.flex.DP);
        flxPopUpTopColor.add();
        var flxPopupHeader = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
            "clipBounds": true,
            "id": "flxPopupHeader",
            "isVisible": true,
            "layoutType": kony.flex.FLOW_VERTICAL,
            "left": "0dp",
            "isModalContainer": false,
            "skin": "slFbox0d9c3974835234d",
            "top": "0px",
            "width": "100%",
            "zIndex": 1
        }, controller.args[0], "flxPopupHeader"), extendConfig({}, controller.args[1], "flxPopupHeader"), extendConfig({}, controller.args[2], "flxPopupHeader"));
        flxPopupHeader.setDefaultUnit(kony.flex.DP);
        var flxPopUpClose = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "clipBounds": true,
            "height": "25px",
            "id": "flxPopUpClose",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "isModalContainer": false,
            "right": 15,
            "skin": "CopyslFbox0efc73ba91cad4b",
            "top": "15dp",
            "width": "25px",
            "zIndex": 20
        }, controller.args[0], "flxPopUpClose"), extendConfig({}, controller.args[1], "flxPopUpClose"), extendConfig({}, controller.args[2], "flxPopUpClose"));
        flxPopUpClose.setDefaultUnit(kony.flex.DP);
        var lblPopUpClose = new kony.ui.Label(extendConfig({
            "centerY": "50%",
            "id": "lblPopUpClose",
            "isVisible": true,
            "left": "4dp",
            "skin": "sknIcon18pxGray",
            "i18n_text": "kony.i18n.getLocalizedString(\"i18n.userwidgetmodel.lblClearSearch\")",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, controller.args[0], "lblPopUpClose"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblPopUpClose"), extendConfig({}, controller.args[2], "lblPopUpClose"));
        var fontIconImgCLose = new kony.ui.Label(extendConfig({
            "centerX": "50%",
            "centerY": "50.00%",
            "height": "15dp",
            "id": "fontIconImgCLose",
            "isVisible": true,
            "skin": "sknFontIconSearchCross16px",
            "i18n_text": "kony.i18n.getLocalizedString(\"i18n.userwidgetmodel.lblClearSearch\")",
            "width": "15dp",
            "zIndex": 2
        }, controller.args[0], "fontIconImgCLose"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "fontIconImgCLose"), extendConfig({}, controller.args[2], "fontIconImgCLose"));
        flxPopUpClose.add(lblPopUpClose, fontIconImgCLose);
        var flxPopupIcon = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
            "clipBounds": true,
            "id": "flxPopupIcon",
            "isVisible": true,
            "layoutType": kony.flex.FLOW_VERTICAL,
            "left": "0dp",
            "isModalContainer": false,
            "skin": "slFbox",
            "top": "0dp",
            "width": "100%"
        }, controller.args[0], "flxPopupIcon"), extendConfig({}, controller.args[1], "flxPopupIcon"), extendConfig({}, controller.args[2], "flxPopupIcon"));
        flxPopupIcon.setDefaultUnit(kony.flex.DP);
        var lblPopUpIcon = new kony.ui.Label(extendConfig({
            "centerX": "50%",
            "height": "50dp",
            "id": "lblPopUpIcon",
            "isVisible": true,
            "skin": "sknIcomoonOnboardingSuccess",
            "text": "",
            "width": "50dp",
            "zIndex": 1
        }, controller.args[0], "lblPopUpIcon"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_CENTER,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblPopUpIcon"), extendConfig({}, controller.args[2], "lblPopUpIcon"));
        flxPopupIcon.add(lblPopUpIcon);
        var lblPopupMsg = new kony.ui.Label(extendConfig({
            "centerX": "50%",
            "id": "lblPopupMsg",
            "isVisible": true,
            "skin": "sknlblLatoRegular484b5216px",
            "text": "Congratulations for Successful Onboarding",
            "top": "10dp",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 20
        }, controller.args[0], "lblPopupMsg"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_CENTER,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblPopupMsg"), extendConfig({}, controller.args[2], "lblPopupMsg"));
        var rtxPopUpDisclaimer = new kony.ui.RichText(extendConfig({
            "bottom": "30px",
            "id": "rtxPopUpDisclaimer",
            "isVisible": true,
            "left": "20px",
            "right": "20px",
            "skin": "sknrtxLato0df8337c414274d",
            "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmAssistedOnboarding.approveRtx\")",
            "top": "30px",
            "width": "93%",
            "zIndex": 1
        }, controller.args[0], "rtxPopUpDisclaimer"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_CENTER,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "rtxPopUpDisclaimer"), extendConfig({}, controller.args[2], "rtxPopUpDisclaimer"));
        flxPopupHeader.add(flxPopUpClose, flxPopupIcon, lblPopupMsg, rtxPopUpDisclaimer);
        var flxPopUpButtons = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "clipBounds": true,
            "height": "80px",
            "id": "flxPopUpButtons",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "left": "0%",
            "isModalContainer": false,
            "skin": "slFbox0dc900c4572aa40",
            "top": "0px",
            "width": "100%",
            "zIndex": 1
        }, controller.args[0], "flxPopUpButtons"), extendConfig({}, controller.args[1], "flxPopUpButtons"), extendConfig({}, controller.args[2], "flxPopUpButtons"));
        flxPopUpButtons.setDefaultUnit(kony.flex.DP);
        var btnPopUpCancel = new kony.ui.Button(extendConfig({
            "centerY": "50%",
            "focusSkin": "sknBtnLatoRegulara5abc413pxKA",
            "height": "40px",
            "id": "btnPopUpCancel",
            "isVisible": true,
            "right": "170px",
            "skin": "sknBtnLatoRegulara5abc413pxKA",
            "text": "BACK TO SEARCH",
            "top": "0%",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 20
        }, controller.args[0], "btnPopUpCancel"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_CENTER,
            "displayText": true,
            "padding": [3, 0, 3, 0],
            "paddingInPixel": false
        }, controller.args[1], "btnPopUpCancel"), extendConfig({
            "hoverSkin": "sknbtnffffffLatoBolda5abc413Pxd7d8e01pxBorderRadius20px"
        }, controller.args[2], "btnPopUpCancel"));
        var btnPopUpDelete = new kony.ui.Button(extendConfig({
            "centerY": "50%",
            "focusSkin": "sknBtn003E75LatoRegular13pxFFFFFFRad20px",
            "height": "40px",
            "id": "btnPopUpDelete",
            "isVisible": true,
            "minWidth": "100px",
            "right": "20px",
            "skin": "sknBtn003E75LatoRegular13pxFFFFFFRad20px",
            "text": "OPEN PROFILE",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 20
        }, controller.args[0], "btnPopUpDelete"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_CENTER,
            "displayText": true,
            "padding": [3, 0, 3, 0],
            "paddingInPixel": false
        }, controller.args[1], "btnPopUpDelete"), extendConfig({
            "hoverSkin": "sknBtn005198LatoRegular13pxFFFFFFRad20px"
        }, controller.args[2], "btnPopUpDelete"));
        flxPopUpButtons.add(btnPopUpCancel, btnPopUpDelete);
        flxPopUp.add(flxPopUpTopColor, flxPopupHeader, flxPopUpButtons);
        popUp.add(flxPopUp);
        return popUp;
    }
})