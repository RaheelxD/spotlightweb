define("com/adminConsole/onBoarding/popUp/userpopUpController", function() {
    return {};
});
define("com/adminConsole/onBoarding/popUp/popUpControllerActions", {
    /*
          This is an auto generated file and any modifications to it may result in corruption of the action sequence.
        */
});
define("com/adminConsole/onBoarding/popUp/popUpController", ["com/adminConsole/onBoarding/popUp/userpopUpController", "com/adminConsole/onBoarding/popUp/popUpControllerActions"], function() {
    var controller = require("com/adminConsole/onBoarding/popUp/userpopUpController");
    var actions = require("com/adminConsole/onBoarding/popUp/popUpControllerActions");
    for (var key in actions) {
        controller[key] = actions[key];
    }
    return controller;
});
