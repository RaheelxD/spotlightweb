define(function() {
    return function(controller) {
        var linkProfilesPopup = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "clipBounds": true,
            "isMaster": true,
            "height": "100%",
            "id": "linkProfilesPopup",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "left": "0dp",
            "isModalContainer": false,
            "preShow": function(eventobject) {
                controller.AS_FlexContainer_bfe2bf6254a4485788dadaa487d50483(eventobject);
            },
            "skin": "sknFlxBg222B35Op50PopupBg",
            "top": "0dp",
            "width": "100%"
        }, controller.args[0], "linkProfilesPopup"), extendConfig({}, controller.args[1], "linkProfilesPopup"), extendConfig({}, controller.args[2], "linkProfilesPopup"));
        linkProfilesPopup.setDefaultUnit(kony.flex.DP);
        var flxPopUp = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "centerX": "50%",
            "centerY": "50%",
            "clipBounds": false,
            "height": "85%",
            "id": "flxPopUp",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "isModalContainer": false,
            "skin": "slFbox0j9f841cc563e4e",
            "width": "960px",
            "zIndex": 10
        }, controller.args[0], "flxPopUp"), extendConfig({}, controller.args[1], "flxPopUp"), extendConfig({}, controller.args[2], "flxPopUp"));
        flxPopUp.setDefaultUnit(kony.flex.DP);
        var flxPopUpTopColor = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "clipBounds": true,
            "height": "10px",
            "id": "flxPopUpTopColor",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "left": "0dp",
            "isModalContainer": false,
            "skin": "sknFlxInfoToastBg357C9ENoRadius",
            "top": "0dp",
            "width": "100%",
            "zIndex": 3
        }, controller.args[0], "flxPopUpTopColor"), extendConfig({}, controller.args[1], "flxPopUpTopColor"), extendConfig({}, controller.args[2], "flxPopUpTopColor"));
        flxPopUpTopColor.setDefaultUnit(kony.flex.DP);
        flxPopUpTopColor.add();
        var flxPopupHeader = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "centerX": "50%",
            "clipBounds": true,
            "height": "25dp",
            "id": "flxPopupHeader",
            "isVisible": true,
            "layoutType": kony.flex.FLOW_VERTICAL,
            "isModalContainer": false,
            "skin": "slFbox0d9c3974835234d",
            "top": "25dp",
            "width": "100%",
            "zIndex": 1
        }, controller.args[0], "flxPopupHeader"), extendConfig({}, controller.args[1], "flxPopupHeader"), extendConfig({}, controller.args[2], "flxPopupHeader"));
        flxPopupHeader.setDefaultUnit(kony.flex.DP);
        var flxPopUpClose = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "clipBounds": true,
            "height": "25px",
            "id": "flxPopUpClose",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "isModalContainer": false,
            "right": 15,
            "skin": "CopyslFbox0efc73ba91cad4b",
            "top": "0dp",
            "width": "25px",
            "zIndex": 20
        }, controller.args[0], "flxPopUpClose"), extendConfig({}, controller.args[1], "flxPopUpClose"), extendConfig({}, controller.args[2], "flxPopUpClose"));
        flxPopUpClose.setDefaultUnit(kony.flex.DP);
        var lblPopUpClose = new kony.ui.Label(extendConfig({
            "centerY": "50%",
            "id": "lblPopUpClose",
            "isVisible": true,
            "left": "4dp",
            "skin": "sknIcon18pxGray",
            "i18n_text": "kony.i18n.getLocalizedString(\"i18n.userwidgetmodel.lblClearSearch\")",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, controller.args[0], "lblPopUpClose"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblPopUpClose"), extendConfig({}, controller.args[2], "lblPopUpClose"));
        var fontIconImgCLose = new kony.ui.Label(extendConfig({
            "centerX": "50%",
            "centerY": "50.00%",
            "height": "15dp",
            "id": "fontIconImgCLose",
            "isVisible": true,
            "skin": "sknFontIconSearchCross16px",
            "i18n_text": "kony.i18n.getLocalizedString(\"i18n.userwidgetmodel.lblClearSearch\")",
            "width": "15dp",
            "zIndex": 2
        }, controller.args[0], "fontIconImgCLose"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "fontIconImgCLose"), extendConfig({}, controller.args[2], "fontIconImgCLose"));
        flxPopUpClose.add(lblPopUpClose, fontIconImgCLose);
        flxPopupHeader.add(flxPopUpClose);
        var flxProfilesListContainer = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "bottom": "0dp",
            "clipBounds": true,
            "id": "flxProfilesListContainer",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "left": "0dp",
            "isModalContainer": false,
            "skin": "slFbox",
            "top": "50dp",
            "width": "100%",
            "zIndex": 1
        }, controller.args[0], "flxProfilesListContainer"), extendConfig({}, controller.args[1], "flxProfilesListContainer"), extendConfig({}, controller.args[2], "flxProfilesListContainer"));
        flxProfilesListContainer.setDefaultUnit(kony.flex.DP);
        var flxProfileListHeader = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
            "clipBounds": true,
            "id": "flxProfileListHeader",
            "isVisible": true,
            "layoutType": kony.flex.FLOW_VERTICAL,
            "left": "20dp",
            "isModalContainer": false,
            "right": "20dp",
            "skin": "slFbox",
            "top": "0dp",
            "zIndex": 1
        }, controller.args[0], "flxProfileListHeader"), extendConfig({}, controller.args[1], "flxProfileListHeader"), extendConfig({}, controller.args[2], "flxProfileListHeader"));
        flxProfileListHeader.setDefaultUnit(kony.flex.DP);
        var lblProfileLinking = new kony.ui.Label(extendConfig({
            "id": "lblProfileLinking",
            "isVisible": true,
            "left": "0dp",
            "skin": "sknLbl192b45LatoReg16px",
            "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmCustomerManagement.LinkProfiles\")",
            "top": "0dp",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, controller.args[0], "lblProfileLinking"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblProfileLinking"), extendConfig({}, controller.args[2], "lblProfileLinking"));
        var lblSubHeading = new kony.ui.Label(extendConfig({
            "id": "lblSubHeading",
            "isVisible": true,
            "left": "0dp",
            "skin": "sknLbl192B45LatoRegular14px",
            "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmCustomerManagement.BusinessProfiles\")",
            "top": "20dp",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, controller.args[0], "lblSubHeading"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblSubHeading"), extendConfig({}, controller.args[2], "lblSubHeading"));
        var lblSeperator = new kony.ui.Label(extendConfig({
            "height": "1dp",
            "id": "lblSeperator",
            "isVisible": true,
            "left": "0dp",
            "skin": "sknLble1e5edOp100O",
            "text": "-",
            "top": "20dp",
            "width": "100%",
            "zIndex": 1
        }, controller.args[0], "lblSeperator"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblSeperator"), extendConfig({}, controller.args[2], "lblSeperator"));
        flxProfileListHeader.add(lblProfileLinking, lblSubHeading, lblSeperator);
        var flxError = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "clipBounds": true,
            "height": "60dp",
            "id": "flxError",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "left": "0dp",
            "isModalContainer": false,
            "skin": "slFbox",
            "top": "80dp",
            "width": "100%",
            "zIndex": 3
        }, controller.args[0], "flxError"), extendConfig({}, controller.args[1], "flxError"), extendConfig({}, controller.args[2], "flxError"));
        flxError.setDefaultUnit(kony.flex.DP);
        var errorProfileList = new com.adminConsole.common.errorFlagMessage(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
            "centerY": "50%",
            "id": "errorProfileList",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "left": "20dp",
            "masterType": constants.MASTER_TYPE_DEFAULT,
            "isModalContainer": false,
            "right": "20dp",
            "skin": "slFbox",
            "overrides": {
                "errorFlagMessage": {
                    "centerY": "50%",
                    "left": "20dp",
                    "right": "20dp",
                    "top": "viz.val_cleared",
                    "width": "viz.val_cleared"
                }
            }
        }, controller.args[0], "errorProfileList"), extendConfig({
            "overrides": {}
        }, controller.args[1], "errorProfileList"), extendConfig({
            "overrides": {}
        }, controller.args[2], "errorProfileList"));
        flxError.add(errorProfileList);
        var flxSegmentContainer = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "bottom": "80dp",
            "clipBounds": true,
            "id": "flxSegmentContainer",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "left": "0dp",
            "isModalContainer": false,
            "right": "0dp",
            "skin": "slFbox",
            "top": "130dp",
            "zIndex": 1
        }, controller.args[0], "flxSegmentContainer"), extendConfig({}, controller.args[1], "flxSegmentContainer"), extendConfig({}, controller.args[2], "flxSegmentContainer"));
        flxSegmentContainer.setDefaultUnit(kony.flex.DP);
        var segProfilesList = new kony.ui.SegmentedUI2(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "groupCells": false,
            "height": "100%",
            "id": "segProfilesList",
            "isVisible": true,
            "left": "0dp",
            "needPageIndicator": true,
            "pageOffDotImage": "pageoffdot.png",
            "pageOnDotImage": "pageondot.png",
            "retainSelection": false,
            "rowFocusSkin": "seg2Focus",
            "rowSkin": "seg2Normal",
            "sectionHeaderSkin": "sliPhoneSegmentHeader",
            "selectionBehavior": constants.SEGUI_SINGLE_SELECT_BEHAVIOR,
            "selectionBehaviorConfig": {
                "imageIdentifier": "imgRadioBtn",
                "selectedStateImage": "radio_selected.png",
                "unselectedStateImage": "radio_notselected.png"
            },
            "separatorColor": "aaaaaa00",
            "separatorRequired": false,
            "separatorThickness": 1,
            "showScrollbars": false,
            "top": "0dp",
            "viewType": constants.SEGUI_VIEW_TYPE_TABLEVIEW,
            "widgetDataMap": {},
            "width": "100%",
            "zIndex": 1
        }, controller.args[0], "segProfilesList"), extendConfig({
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "segProfilesList"), extendConfig({}, controller.args[2], "segProfilesList"));
        flxSegmentContainer.add(segProfilesList);
        flxProfilesListContainer.add(flxProfileListHeader, flxError, flxSegmentContainer);
        var flxLinkProfileContainer = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "bottom": "0dp",
            "clipBounds": true,
            "id": "flxLinkProfileContainer",
            "isVisible": false,
            "layoutType": kony.flex.FREE_FORM,
            "left": "0dp",
            "isModalContainer": false,
            "skin": "slFbox",
            "top": "50dp",
            "width": "100%",
            "zIndex": 1
        }, controller.args[0], "flxLinkProfileContainer"), extendConfig({}, controller.args[1], "flxLinkProfileContainer"), extendConfig({}, controller.args[2], "flxLinkProfileContainer"));
        flxLinkProfileContainer.setDefaultUnit(kony.flex.DP);
        var flxLinkProfileHeader = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
            "clipBounds": true,
            "id": "flxLinkProfileHeader",
            "isVisible": true,
            "layoutType": kony.flex.FLOW_VERTICAL,
            "left": "20dp",
            "isModalContainer": false,
            "right": "20dp",
            "skin": "slFbox",
            "top": "0dp",
            "zIndex": 1
        }, controller.args[0], "flxLinkProfileHeader"), extendConfig({}, controller.args[1], "flxLinkProfileHeader"), extendConfig({}, controller.args[2], "flxLinkProfileHeader"));
        flxLinkProfileHeader.setDefaultUnit(kony.flex.DP);
        var lblProfileLinkHeader = new kony.ui.Label(extendConfig({
            "id": "lblProfileLinkHeader",
            "isVisible": true,
            "left": "0dp",
            "skin": "sknLbl192b45LatoReg16px",
            "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmCustomerManagement.LinkProfiles\")",
            "top": "0dp",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, controller.args[0], "lblProfileLinkHeader"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblProfileLinkHeader"), extendConfig({}, controller.args[2], "lblProfileLinkHeader"));
        var lblSubHeader = new kony.ui.Label(extendConfig({
            "id": "lblSubHeader",
            "isVisible": true,
            "left": "0dp",
            "skin": "sknLbl192B45LatoRegular14px",
            "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmCustomerManagement.SelectUsernameToLinkProfile\")",
            "top": "20dp",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, controller.args[0], "lblSubHeader"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblSubHeader"), extendConfig({}, controller.args[2], "lblSubHeader"));
        var lblHeaderSeperator = new kony.ui.Label(extendConfig({
            "height": "1dp",
            "id": "lblHeaderSeperator",
            "isVisible": true,
            "left": "0dp",
            "skin": "sknLble1e5edOp100O",
            "text": "-",
            "top": "20dp",
            "width": "100%",
            "zIndex": 1
        }, controller.args[0], "lblHeaderSeperator"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblHeaderSeperator"), extendConfig({}, controller.args[2], "lblHeaderSeperator"));
        flxLinkProfileHeader.add(lblProfileLinkHeader, lblSubHeader, lblHeaderSeperator);
        var flxErrorMessage = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "clipBounds": true,
            "height": "60dp",
            "id": "flxErrorMessage",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "left": "0dp",
            "isModalContainer": false,
            "skin": "slFbox",
            "top": "80dp",
            "width": "100%",
            "zIndex": 1
        }, controller.args[0], "flxErrorMessage"), extendConfig({}, controller.args[1], "flxErrorMessage"), extendConfig({}, controller.args[2], "flxErrorMessage"));
        flxErrorMessage.setDefaultUnit(kony.flex.DP);
        var errorSelectProfile = new com.adminConsole.common.errorFlagMessage(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
            "centerY": "50%",
            "id": "errorSelectProfile",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "left": "20dp",
            "masterType": constants.MASTER_TYPE_DEFAULT,
            "isModalContainer": false,
            "right": "20dp",
            "skin": "slFbox",
            "overrides": {
                "errorFlagMessage": {
                    "centerY": "50%",
                    "left": "20dp",
                    "right": "20dp",
                    "top": "viz.val_cleared",
                    "width": "viz.val_cleared"
                }
            }
        }, controller.args[0], "errorSelectProfile"), extendConfig({
            "overrides": {}
        }, controller.args[1], "errorSelectProfile"), extendConfig({
            "overrides": {}
        }, controller.args[2], "errorSelectProfile"));
        flxErrorMessage.add(errorSelectProfile);
        var flxSelectSegment = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "bottom": "80dp",
            "clipBounds": true,
            "id": "flxSelectSegment",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "left": "0dp",
            "isModalContainer": false,
            "skin": "slFbox",
            "top": "80dp",
            "width": "100%",
            "zIndex": 1
        }, controller.args[0], "flxSelectSegment"), extendConfig({}, controller.args[1], "flxSelectSegment"), extendConfig({}, controller.args[2], "flxSelectSegment"));
        flxSelectSegment.setDefaultUnit(kony.flex.DP);
        var segSelectProfile = new kony.ui.SegmentedUI2(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "groupCells": false,
            "height": "100%",
            "id": "segSelectProfile",
            "isVisible": true,
            "left": "0dp",
            "needPageIndicator": true,
            "pageOffDotImage": "pageoffdot.png",
            "pageOnDotImage": "pageondot.png",
            "retainSelection": false,
            "right": "0dp",
            "rowFocusSkin": "seg2Focus",
            "rowSkin": "seg2Normal",
            "sectionHeaderSkin": "sliPhoneSegmentHeader",
            "selectionBehavior": constants.SEGUI_SINGLE_SELECT_BEHAVIOR,
            "selectionBehaviorConfig": {
                "imageIdentifier": "imgRadioBtn",
                "selectedStateImage": "radio_selected.png",
                "unselectedStateImage": "radio_notselected.png"
            },
            "separatorColor": "aaaaaa00",
            "separatorRequired": false,
            "separatorThickness": 1,
            "showScrollbars": false,
            "top": "0dp",
            "viewType": constants.SEGUI_VIEW_TYPE_TABLEVIEW,
            "widgetDataMap": {},
            "zIndex": 1
        }, controller.args[0], "segSelectProfile"), extendConfig({
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "segSelectProfile"), extendConfig({}, controller.args[2], "segSelectProfile"));
        flxSelectSegment.add(segSelectProfile);
        flxLinkProfileContainer.add(flxLinkProfileHeader, flxErrorMessage, flxSelectSegment);
        var flxPopUpButtons = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "bottom": "0dp",
            "clipBounds": true,
            "height": "80px",
            "id": "flxPopUpButtons",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "left": "0%",
            "isModalContainer": false,
            "skin": "sknflxBgE4E6EC",
            "width": "100%",
            "zIndex": 3
        }, controller.args[0], "flxPopUpButtons"), extendConfig({}, controller.args[1], "flxPopUpButtons"), extendConfig({}, controller.args[2], "flxPopUpButtons"));
        flxPopUpButtons.setDefaultUnit(kony.flex.DP);
        var btnPopUpCancel = new kony.ui.Button(extendConfig({
            "centerY": "50%",
            "focusSkin": "sknbtnf7f7faLatoRegular4f555dBorder1px8b96a5",
            "height": "40px",
            "id": "btnPopUpCancel",
            "isVisible": true,
            "left": "20dp",
            "skin": "sknbtnf7f7faLatoRegular4f555dBorder1px8b96a5",
            "i18n_text": "kony.i18n.getLocalizedString(\"i18n.permission.CANCEL\")",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 20
        }, controller.args[0], "btnPopUpCancel"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_CENTER,
            "displayText": true,
            "padding": [3, 0, 3, 0],
            "paddingInPixel": false
        }, controller.args[1], "btnPopUpCancel"), extendConfig({
            "hoverSkin": "sknBtnLatoRegular8b96a513pxKAhover"
        }, controller.args[2], "btnPopUpCancel"));
        var btnPopUpNext = new kony.ui.Button(extendConfig({
            "centerY": "50%",
            "focusSkin": "sknBtn003E75LatoRegular13pxFFFFFFRad20px",
            "height": "40px",
            "id": "btnPopUpNext",
            "isVisible": true,
            "right": "20px",
            "skin": "sknBtn003E75LatoRegular13pxFFFFFFRad20px",
            "i18n_text": "kony.i18n.getLocalizedString(\"i18n.SecurityQuestions.OK\")",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 20
        }, controller.args[0], "btnPopUpNext"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_CENTER,
            "displayText": true,
            "padding": [3, 0, 3, 0],
            "paddingInPixel": false
        }, controller.args[1], "btnPopUpNext"), extendConfig({
            "hoverSkin": "sknBtn005198LatoRegular13pxFFFFFFRad20px"
        }, controller.args[2], "btnPopUpNext"));
        flxPopUpButtons.add(btnPopUpCancel, btnPopUpNext);
        flxPopUp.add(flxPopUpTopColor, flxPopupHeader, flxProfilesListContainer, flxLinkProfileContainer, flxPopUpButtons);
        linkProfilesPopup.add(flxPopUp);
        return linkProfilesPopup;
    }
})