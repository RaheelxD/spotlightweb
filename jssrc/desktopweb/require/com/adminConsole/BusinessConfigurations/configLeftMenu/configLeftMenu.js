define(function() {
    return function(controller) {
        var configLeftMenu = new kony.ui.FlexContainer(extendConfig({
            "clipBounds": true,
            "isMaster": true,
            "height": "100%",
            "id": "configLeftMenu",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "left": "0dp",
            "isModalContainer": false,
            "skin": "slFbox",
            "top": "0dp",
            "width": "100%"
        }, controller.args[0], "configLeftMenu"), extendConfig({}, controller.args[1], "configLeftMenu"), extendConfig({}, controller.args[2], "configLeftMenu"));
        configLeftMenu.setDefaultUnit(kony.flex.DP);
        var flxDetailHeader = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "clipBounds": true,
            "height": "100%",
            "id": "flxDetailHeader",
            "isVisible": true,
            "layoutType": kony.flex.FLOW_VERTICAL,
            "left": "0dp",
            "isModalContainer": false,
            "right": "0px",
            "skin": "sknBackgroundFFFFFF",
            "top": "0dp",
            "width": "100%",
            "zIndex": 1
        }, controller.args[0], "flxDetailHeader"), extendConfig({}, controller.args[1], "flxDetailHeader"), extendConfig({}, controller.args[2], "flxDetailHeader"));
        flxDetailHeader.setDefaultUnit(kony.flex.DP);
        var flxHeader1 = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "clipBounds": true,
            "height": "52px",
            "id": "flxHeader1",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "left": "0dp",
            "isModalContainer": false,
            "skin": "sknCursor",
            "top": "0dp",
            "width": "100%",
            "zIndex": 1
        }, controller.args[0], "flxHeader1"), extendConfig({}, controller.args[1], "flxHeader1"), extendConfig({
            "hoverSkin": "sknCursorHoverDark"
        }, controller.args[2], "flxHeader1"));
        flxHeader1.setDefaultUnit(kony.flex.DP);
        var lblConfigurationName1 = new kony.ui.Label(extendConfig({
            "centerY": "50.00%",
            "id": "lblConfigurationName1",
            "isVisible": true,
            "left": "25px",
            "right": "25px",
            "text": "ELIGIBILITY CRITERIA",
            "zIndex": 1
        }, controller.args[0], "lblConfigurationName1"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblConfigurationName1"), extendConfig({}, controller.args[2], "lblConfigurationName1"));
        var flxSelected = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "bottom": "0px",
            "clipBounds": true,
            "height": "52px",
            "id": "flxSelected",
            "isVisible": true,
            "layoutType": kony.flex.FLOW_HORIZONTAL,
            "left": "0px",
            "isModalContainer": false,
            "right": "0px",
            "skin": "sknFlx006CCA0pxRad",
            "width": "5px",
            "zIndex": 3
        }, controller.args[0], "flxSelected"), extendConfig({}, controller.args[1], "flxSelected"), extendConfig({}, controller.args[2], "flxSelected"));
        flxSelected.setDefaultUnit(kony.flex.DP);
        flxSelected.add();
        var flxseparator1 = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "bottom": "0dp",
            "clipBounds": true,
            "height": "1px",
            "id": "flxseparator1",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "isModalContainer": false,
            "right": "0px",
            "skin": "sknSeparatorCsr",
            "width": "100%",
            "zIndex": 1
        }, controller.args[0], "flxseparator1"), extendConfig({}, controller.args[1], "flxseparator1"), extendConfig({}, controller.args[2], "flxseparator1"));
        flxseparator1.setDefaultUnit(kony.flex.DP);
        flxseparator1.add();
        var flxSeperator2 = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "clipBounds": true,
            "height": "1px",
            "id": "flxSeperator2",
            "isVisible": false,
            "layoutType": kony.flex.FREE_FORM,
            "isModalContainer": false,
            "right": "0px",
            "skin": "sknflx115678",
            "top": "0dp",
            "width": "100%",
            "zIndex": 1
        }, controller.args[0], "flxSeperator2"), extendConfig({}, controller.args[1], "flxSeperator2"), extendConfig({}, controller.args[2], "flxSeperator2"));
        flxSeperator2.setDefaultUnit(kony.flex.DP);
        flxSeperator2.add();
        flxHeader1.add(lblConfigurationName1, flxSelected, flxseparator1, flxSeperator2);
        var flxHeader2 = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "clipBounds": true,
            "height": "74px",
            "id": "flxHeader2",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "left": "0dp",
            "isModalContainer": false,
            "skin": "sknCursor",
            "top": "0dp",
            "width": "100%",
            "zIndex": 1
        }, controller.args[0], "flxHeader2"), extendConfig({}, controller.args[1], "flxHeader2"), extendConfig({
            "hoverSkin": "sknCursorHoverDark"
        }, controller.args[2], "flxHeader2"));
        flxHeader2.setDefaultUnit(kony.flex.DP);
        var lblConfigurationName22 = new kony.ui.Label(extendConfig({
            "centerY": "50.00%",
            "id": "lblConfigurationName22",
            "isVisible": true,
            "left": "25px",
            "right": "25px",
            "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmBusinessConfigurations.businessConfig\")",
            "zIndex": 1
        }, controller.args[0], "lblConfigurationName22"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblConfigurationName22"), extendConfig({}, controller.args[2], "lblConfigurationName22"));
        var flxSelected2 = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "bottom": "0px",
            "clipBounds": true,
            "height": "74px",
            "id": "flxSelected2",
            "isVisible": true,
            "layoutType": kony.flex.FLOW_HORIZONTAL,
            "left": "0px",
            "isModalContainer": false,
            "right": "0px",
            "skin": "sknFlx006CCA0pxRad",
            "width": "5px",
            "zIndex": 3
        }, controller.args[0], "flxSelected2"), extendConfig({}, controller.args[1], "flxSelected2"), extendConfig({}, controller.args[2], "flxSelected2"));
        flxSelected2.setDefaultUnit(kony.flex.DP);
        flxSelected2.add();
        var flxseparator21 = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "bottom": "0dp",
            "clipBounds": true,
            "height": "1px",
            "id": "flxseparator21",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "isModalContainer": false,
            "right": "0px",
            "skin": "sknSeparatorCsr",
            "width": "100%",
            "zIndex": 1
        }, controller.args[0], "flxseparator21"), extendConfig({}, controller.args[1], "flxseparator21"), extendConfig({}, controller.args[2], "flxseparator21"));
        flxseparator21.setDefaultUnit(kony.flex.DP);
        flxseparator21.add();
        var flxSeperator22 = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "clipBounds": true,
            "height": "1px",
            "id": "flxSeperator22",
            "isVisible": false,
            "layoutType": kony.flex.FREE_FORM,
            "isModalContainer": false,
            "right": "0px",
            "skin": "sknflx115678",
            "top": "0dp",
            "width": "100%",
            "zIndex": 1
        }, controller.args[0], "flxSeperator22"), extendConfig({}, controller.args[1], "flxSeperator22"), extendConfig({}, controller.args[2], "flxSeperator22"));
        flxSeperator22.setDefaultUnit(kony.flex.DP);
        flxSeperator22.add();
        flxHeader2.add(lblConfigurationName22, flxSelected2, flxseparator21, flxSeperator22);
        var flxHeader3 = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "clipBounds": true,
            "height": "52px",
            "id": "flxHeader3",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "left": "0dp",
            "isModalContainer": false,
            "skin": "sknCursor",
            "top": "0dp",
            "width": "100%",
            "zIndex": 1
        }, controller.args[0], "flxHeader3"), extendConfig({}, controller.args[1], "flxHeader3"), extendConfig({
            "hoverSkin": "sknCursorHoverDark"
        }, controller.args[2], "flxHeader3"));
        flxHeader3.setDefaultUnit(kony.flex.DP);
        var lblConfigurationName33 = new kony.ui.Label(extendConfig({
            "centerY": "50.00%",
            "id": "lblConfigurationName33",
            "isVisible": true,
            "left": "25px",
            "right": "25px",
            "text": "ALERT CONFIGURATIONS",
            "zIndex": 1
        }, controller.args[0], "lblConfigurationName33"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblConfigurationName33"), extendConfig({}, controller.args[2], "lblConfigurationName33"));
        var flxSelected3 = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "bottom": "0px",
            "clipBounds": true,
            "height": "52px",
            "id": "flxSelected3",
            "isVisible": true,
            "layoutType": kony.flex.FLOW_HORIZONTAL,
            "left": "0px",
            "isModalContainer": false,
            "right": "0px",
            "skin": "sknFlx006CCA0pxRad",
            "width": "5px",
            "zIndex": 3
        }, controller.args[0], "flxSelected3"), extendConfig({}, controller.args[1], "flxSelected3"), extendConfig({}, controller.args[2], "flxSelected3"));
        flxSelected3.setDefaultUnit(kony.flex.DP);
        flxSelected3.add();
        var flxseparator31 = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "bottom": "0dp",
            "clipBounds": true,
            "height": "1px",
            "id": "flxseparator31",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "isModalContainer": false,
            "right": "0px",
            "skin": "sknSeparatorCsr",
            "width": "100%",
            "zIndex": 1
        }, controller.args[0], "flxseparator31"), extendConfig({}, controller.args[1], "flxseparator31"), extendConfig({}, controller.args[2], "flxseparator31"));
        flxseparator31.setDefaultUnit(kony.flex.DP);
        flxseparator31.add();
        var flxSeperator32 = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "clipBounds": true,
            "height": "1px",
            "id": "flxSeperator32",
            "isVisible": false,
            "layoutType": kony.flex.FREE_FORM,
            "isModalContainer": false,
            "right": "0px",
            "skin": "sknflx115678",
            "top": "0dp",
            "width": "100%",
            "zIndex": 1
        }, controller.args[0], "flxSeperator32"), extendConfig({}, controller.args[1], "flxSeperator32"), extendConfig({}, controller.args[2], "flxSeperator32"));
        flxSeperator32.setDefaultUnit(kony.flex.DP);
        flxSeperator32.add();
        flxHeader3.add(lblConfigurationName33, flxSelected3, flxseparator31, flxSeperator32);
        flxDetailHeader.add(flxHeader1, flxHeader2, flxHeader3);
        configLeftMenu.add(flxDetailHeader);
        return configLeftMenu;
    }
})