define(function() {
    return function(controller) {
        var viewProducts = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "clipBounds": true,
            "isMaster": true,
            "height": "100%",
            "id": "viewProducts",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "left": "0px",
            "isModalContainer": false,
            "preShow": function(eventobject) {
                controller.AS_FlexContainer_b9660cf28edb4d2bbb479d48cacb6670(eventobject);
            },
            "skin": "slFbox",
            "top": "0px",
            "width": "100%"
        }, controller.args[0], "viewProducts"), extendConfig({}, controller.args[1], "viewProducts"), extendConfig({}, controller.args[2], "viewProducts"));
        viewProducts.setDefaultUnit(kony.flex.DP);
        var flxViewProducts = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "bottom": "0px",
            "clipBounds": true,
            "id": "flxViewProducts",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "left": "0px",
            "isModalContainer": false,
            "right": "0px",
            "skin": "slFbox",
            "top": "0px",
            "zIndex": 5
        }, controller.args[0], "flxViewProducts"), extendConfig({}, controller.args[1], "flxViewProducts"), extendConfig({}, controller.args[2], "flxViewProducts"));
        flxViewProducts.setDefaultUnit(kony.flex.DP);
        var flxProducts = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "bottom": "20px",
            "clipBounds": true,
            "id": "flxProducts",
            "isVisible": false,
            "layoutType": kony.flex.FREE_FORM,
            "left": "0px",
            "isModalContainer": false,
            "right": "0px",
            "skin": "slFbox",
            "top": "0px",
            "zIndex": 1
        }, controller.args[0], "flxProducts"), extendConfig({}, controller.args[1], "flxProducts"), extendConfig({}, controller.args[2], "flxProducts"));
        flxProducts.setDefaultUnit(kony.flex.DP);
        var subHeader = new com.adminConsole.header.subHeader(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "height": "60px",
            "id": "subHeader",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "left": "0dp",
            "masterType": constants.MASTER_TYPE_DEFAULT,
            "isModalContainer": false,
            "skin": "CopyslFbox2",
            "top": "0dp",
            "width": "100%",
            "overrides": {
                "flxMenu": {
                    "isVisible": false,
                    "left": "35dp"
                },
                "flxSearch": {
                    "right": "0px",
                    "top": "0px",
                    "width": "350px"
                },
                "flxSearchContainer": {
                    "centerY": "50%",
                    "left": "0dp",
                    "right": "0dp",
                    "top": "viz.val_cleared"
                },
                "flxSubHeader": {
                    "bottom": "0dp",
                    "height": "100%",
                    "top": "0dp"
                },
                "subHeader": {
                    "centerY": "viz.val_cleared",
                    "height": "60px",
                    "isVisible": true,
                    "top": "0dp"
                },
                "tbxSearchBox": {
                    "placeholder": "Search by Product Name, Line and Group"
                }
            }
        }, controller.args[0], "subHeader"), extendConfig({
            "overrides": {}
        }, controller.args[1], "subHeader"), extendConfig({
            "overrides": {}
        }, controller.args[2], "subHeader"));
        var flxProductList = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "bottom": "0px",
            "clipBounds": true,
            "id": "flxProductList",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "left": "0dp",
            "isModalContainer": false,
            "right": "0dp",
            "skin": "sknflxffffffop100Bordercbcdd1Radius3px",
            "top": "61dp"
        }, controller.args[0], "flxProductList"), extendConfig({}, controller.args[1], "flxProductList"), extendConfig({}, controller.args[2], "flxProductList"));
        flxProductList.setDefaultUnit(kony.flex.DP);
        var flxProductsHeader = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "clipBounds": true,
            "height": "60px",
            "id": "flxProductsHeader",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "left": 0,
            "isModalContainer": false,
            "skin": "sknflxffffffop100",
            "top": "0",
            "width": "100%",
            "zIndex": 1
        }, controller.args[0], "flxProductsHeader"), extendConfig({}, controller.args[1], "flxProductsHeader"), extendConfig({}, controller.args[2], "flxProductsHeader"));
        flxProductsHeader.setDefaultUnit(kony.flex.DP);
        var flxProductName = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "centerY": "50%",
            "clipBounds": true,
            "height": "100%",
            "id": "flxProductName",
            "isVisible": true,
            "layoutType": kony.flex.FLOW_HORIZONTAL,
            "left": "3%",
            "isModalContainer": false,
            "onClick": controller.AS_FlexContainer_i87bd852f9f74801ae5ceff0e9ee81d1,
            "skin": "slFbox",
            "width": "130px",
            "zIndex": 1
        }, controller.args[0], "flxProductName"), extendConfig({}, controller.args[1], "flxProductName"), extendConfig({}, controller.args[2], "flxProductName"));
        flxProductName.setDefaultUnit(kony.flex.DP);
        var lblProductName = new kony.ui.Label(extendConfig({
            "centerY": "50%",
            "id": "lblProductName",
            "isVisible": true,
            "left": "0px",
            "skin": "sknlblLato696c7312px",
            "text": "PRODUCT NAME",
            "width": "100px",
            "zIndex": 1
        }, controller.args[0], "lblProductName"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblProductName"), extendConfig({
            "hoverSkin": "sknlblLatoBold00000012px"
        }, controller.args[2], "lblProductName"));
        var fontIconSortProductName = new kony.ui.Label(extendConfig({
            "centerY": "50%",
            "id": "fontIconSortProductName",
            "isVisible": true,
            "left": "10dp",
            "skin": "sknIcon15px",
            "text": "",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, controller.args[0], "fontIconSortProductName"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "fontIconSortProductName"), extendConfig({
            "hoverSkin": "sknlblCursorFont"
        }, controller.args[2], "fontIconSortProductName"));
        flxProductName.add(lblProductName, fontIconSortProductName);
        var flxProductGroup = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "centerY": "50%",
            "clipBounds": true,
            "height": "100%",
            "id": "flxProductGroup",
            "isVisible": true,
            "layoutType": kony.flex.FLOW_HORIZONTAL,
            "left": "35%",
            "isModalContainer": false,
            "skin": "slFbox",
            "width": "130px",
            "zIndex": 1
        }, controller.args[0], "flxProductGroup"), extendConfig({}, controller.args[1], "flxProductGroup"), extendConfig({}, controller.args[2], "flxProductGroup"));
        flxProductGroup.setDefaultUnit(kony.flex.DP);
        var lblProductGroup = new kony.ui.Label(extendConfig({
            "centerY": "50%",
            "id": "lblProductGroup",
            "isVisible": true,
            "left": "0dp",
            "skin": "sknlblLato696c7312px",
            "text": "PRODUCT GROUP",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, controller.args[0], "lblProductGroup"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblProductGroup"), extendConfig({
            "hoverSkin": "sknlblLatoBold00000012px"
        }, controller.args[2], "lblProductGroup"));
        var fontIconFilterGroup = new kony.ui.Label(extendConfig({
            "centerY": "50%",
            "id": "fontIconFilterGroup",
            "isVisible": true,
            "left": "10dp",
            "skin": "sknIcon15px",
            "text": "",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, controller.args[0], "fontIconFilterGroup"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "fontIconFilterGroup"), extendConfig({
            "hoverSkin": "sknlblCursorFont"
        }, controller.args[2], "fontIconFilterGroup"));
        flxProductGroup.add(lblProductGroup, fontIconFilterGroup);
        var flxProductLine = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "centerY": "50%",
            "clipBounds": true,
            "height": "100%",
            "id": "flxProductLine",
            "isVisible": true,
            "layoutType": kony.flex.FLOW_HORIZONTAL,
            "left": "63%",
            "isModalContainer": false,
            "skin": "slFbox",
            "width": "120px",
            "zIndex": 1
        }, controller.args[0], "flxProductLine"), extendConfig({}, controller.args[1], "flxProductLine"), extendConfig({}, controller.args[2], "flxProductLine"));
        flxProductLine.setDefaultUnit(kony.flex.DP);
        var lblProductLine = new kony.ui.Label(extendConfig({
            "centerY": "50%",
            "id": "lblProductLine",
            "isVisible": true,
            "left": "0dp",
            "skin": "sknlblLato696c7312px",
            "text": "PRODUCT LINE",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, controller.args[0], "lblProductLine"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblProductLine"), extendConfig({
            "hoverSkin": "sknlblLatoBold00000012px"
        }, controller.args[2], "lblProductLine"));
        var fontIconFilterLine = new kony.ui.Label(extendConfig({
            "centerY": "50%",
            "id": "fontIconFilterLine",
            "isVisible": true,
            "left": "10dp",
            "skin": "sknIcon15px",
            "text": "",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, controller.args[0], "fontIconFilterLine"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "fontIconFilterLine"), extendConfig({
            "hoverSkin": "sknlblCursorFont"
        }, controller.args[2], "fontIconFilterLine"));
        flxProductLine.add(lblProductLine, fontIconFilterLine);
        var flxProductStatus = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "centerY": "50%",
            "clipBounds": true,
            "height": "100%",
            "id": "flxProductStatus",
            "isVisible": true,
            "layoutType": kony.flex.FLOW_HORIZONTAL,
            "left": "85%",
            "isModalContainer": false,
            "onClick": controller.AS_FlexContainer_i88e2961592c4b338de61134a3117f51,
            "skin": "slFbox",
            "width": "75px",
            "zIndex": 1
        }, controller.args[0], "flxProductStatus"), extendConfig({}, controller.args[1], "flxProductStatus"), extendConfig({}, controller.args[2], "flxProductStatus"));
        flxProductStatus.setDefaultUnit(kony.flex.DP);
        var lblProductStatus = new kony.ui.Label(extendConfig({
            "centerY": "50%",
            "id": "lblProductStatus",
            "isVisible": true,
            "left": "0px",
            "skin": "sknlblLato696c7312px",
            "i18n_text": "kony.i18n.getLocalizedString(\"i18n.permission.STATUS\")",
            "top": 0,
            "width": "46px",
            "zIndex": 1
        }, controller.args[0], "lblProductStatus"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblProductStatus"), extendConfig({
            "hoverSkin": "sknlblLatoBold00000012px"
        }, controller.args[2], "lblProductStatus"));
        var fontIconFilterStatus = new kony.ui.Label(extendConfig({
            "centerY": "50%",
            "id": "fontIconFilterStatus",
            "isVisible": true,
            "left": "10dp",
            "skin": "sknIcon15px",
            "text": "",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, controller.args[0], "fontIconFilterStatus"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "fontIconFilterStatus"), extendConfig({
            "hoverSkin": "sknlblCursorFont"
        }, controller.args[2], "fontIconFilterStatus"));
        flxProductStatus.add(lblProductStatus, fontIconFilterStatus);
        var lblHeaderSeperator = new kony.ui.Label(extendConfig({
            "bottom": "1px",
            "centerX": "50%",
            "height": "1px",
            "id": "lblHeaderSeperator",
            "isVisible": true,
            "left": "20dp",
            "right": "20px",
            "skin": "sknLblTableHeaderLine",
            "text": "-",
            "zIndex": 1
        }, controller.args[0], "lblHeaderSeperator"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblHeaderSeperator"), extendConfig({}, controller.args[2], "lblHeaderSeperator"));
        flxProductsHeader.add(flxProductName, flxProductGroup, flxProductLine, flxProductStatus, lblHeaderSeperator);
        var segProducts = new kony.ui.SegmentedUI2(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "bottom": "0px",
            "data": [{
                "fonticonActive": "",
                "lblOptions": "",
                "lblProductGroup": "Mobile App",
                "lblProductLine": "Mobile App",
                "lblProductName": "Jompay",
                "lblSeparator": "Label",
                "lblStatus": "Active"
            }, {
                "fonticonActive": "",
                "lblOptions": "",
                "lblProductGroup": "Mobile App",
                "lblProductLine": "Mobile App",
                "lblProductName": "Jompay",
                "lblSeparator": "Label",
                "lblStatus": "Active"
            }, {
                "fonticonActive": "",
                "lblOptions": "",
                "lblProductGroup": "Mobile App",
                "lblProductLine": "Mobile App",
                "lblProductName": "Jompay",
                "lblSeparator": "Label",
                "lblStatus": "Active"
            }],
            "groupCells": false,
            "id": "segProducts",
            "isVisible": false,
            "left": "0px",
            "needPageIndicator": true,
            "onRowClick": controller.AS_Segment_a938b0b3348e41a1a31e0a5eaafb7671,
            "pageOffDotImage": "pageoffdot.png",
            "pageOnDotImage": "pageondot.png",
            "retainSelection": false,
            "rowFocusSkin": "seg2Focus",
            "rowSkin": "seg2Normal",
            "rowTemplate": "flxProducts",
            "sectionHeaderSkin": "sliPhoneSegmentHeader",
            "selectionBehavior": constants.SEGUI_DEFAULT_BEHAVIOR,
            "separatorRequired": false,
            "showScrollbars": true,
            "top": "60px",
            "viewType": constants.SEGUI_VIEW_TYPE_TABLEVIEW,
            "widgetDataMap": {
                "flxOptions": "flxOptions",
                "flxProducts": "flxProducts",
                "flxStatus": "flxStatus",
                "fonticonActive": "fonticonActive",
                "lblOptions": "lblOptions",
                "lblProductGroup": "lblProductGroup",
                "lblProductLine": "lblProductLine",
                "lblProductName": "lblProductName",
                "lblSeparator": "lblSeparator",
                "lblStatus": "lblStatus"
            },
            "width": "100%",
            "zIndex": 1
        }, controller.args[0], "segProducts"), extendConfig({
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "segProducts"), extendConfig({}, controller.args[2], "segProducts"));
        var flxNoRecordsFound = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "bottom": "0px",
            "clipBounds": true,
            "id": "flxNoRecordsFound",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "left": "0px",
            "isModalContainer": false,
            "skin": "slFbox",
            "top": "60px",
            "width": "100%",
            "zIndex": 1
        }, controller.args[0], "flxNoRecordsFound"), extendConfig({}, controller.args[1], "flxNoRecordsFound"), extendConfig({}, controller.args[2], "flxNoRecordsFound"));
        flxNoRecordsFound.setDefaultUnit(kony.flex.DP);
        var lblNoResultFound = new kony.ui.Label(extendConfig({
            "centerX": "50%",
            "centerY": "50%",
            "id": "lblNoResultFound",
            "isVisible": true,
            "skin": "sknLbl485C75LatoRegular13Px",
            "text": "No result found.",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, controller.args[0], "lblNoResultFound"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblNoResultFound"), extendConfig({}, controller.args[2], "lblNoResultFound"));
        flxNoRecordsFound.add(lblNoResultFound);
        var flxPagination = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
            "bottom": "5px",
            "clipBounds": false,
            "id": "flxPagination",
            "isVisible": false,
            "layoutType": kony.flex.FLOW_HORIZONTAL,
            "isModalContainer": false,
            "right": "35dp",
            "skin": "slFbox0d9c3974835234d",
            "top": 0,
            "width": "222px",
            "zIndex": 1
        }, controller.args[0], "flxPagination"), extendConfig({}, controller.args[1], "flxPagination"), extendConfig({}, controller.args[2], "flxPagination"));
        flxPagination.setDefaultUnit(kony.flex.DP);
        var flxPaginationWrapper = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "bottom": 20,
            "clipBounds": false,
            "height": "30px",
            "id": "flxPaginationWrapper",
            "isVisible": true,
            "layoutType": kony.flex.FLOW_HORIZONTAL,
            "isModalContainer": false,
            "right": "35dp",
            "skin": "slFbox0d9c3974835234d",
            "top": 20,
            "width": "222px",
            "zIndex": 1
        }, controller.args[0], "flxPaginationWrapper"), extendConfig({}, controller.args[1], "flxPaginationWrapper"), extendConfig({}, controller.args[2], "flxPaginationWrapper"));
        flxPaginationWrapper.setDefaultUnit(kony.flex.DP);
        var lbxPagination = new kony.ui.ListBox(extendConfig({
            "height": "30dp",
            "id": "lbxPagination",
            "isVisible": true,
            "left": "0dp",
            "masterData": [
                ["lb1", "Page 1 of 20"],
                ["lb2", "Page 2 of 20"],
                ["lb3", "Page 3 of 20"],
                ["lb4", "Page 4 of 20"]
            ],
            "selectedKey": "lb1",
            "skin": "sknlbxNobgNoBorderPagination",
            "top": "0dp",
            "width": "120dp",
            "zIndex": 1
        }, controller.args[0], "lbxPagination"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lbxPagination"), extendConfig({
            "hoverSkin": "lstboxcursor",
            "multiSelect": false
        }, controller.args[2], "lbxPagination"));
        var flxPaginationSeperator = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "clipBounds": true,
            "height": "100%",
            "id": "flxPaginationSeperator",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "left": "15dp",
            "isModalContainer": false,
            "skin": "sknflxd6dbe7",
            "top": "2dp",
            "width": "1px",
            "zIndex": 1
        }, controller.args[0], "flxPaginationSeperator"), extendConfig({}, controller.args[1], "flxPaginationSeperator"), extendConfig({}, controller.args[2], "flxPaginationSeperator"));
        flxPaginationSeperator.setDefaultUnit(kony.flex.DP);
        flxPaginationSeperator.add();
        var flxPrevious = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "clipBounds": true,
            "height": "32dp",
            "id": "flxPrevious",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "left": "15dp",
            "isModalContainer": false,
            "right": "3dp",
            "skin": "slFbox",
            "top": "0dp",
            "width": "32px",
            "zIndex": 1
        }, controller.args[0], "flxPrevious"), extendConfig({}, controller.args[1], "flxPrevious"), extendConfig({}, controller.args[2], "flxPrevious"));
        flxPrevious.setDefaultUnit(kony.flex.DP);
        var lblIconPrevious = new kony.ui.Label(extendConfig({
            "centerX": "50%",
            "centerY": "50%",
            "id": "lblIconPrevious",
            "isVisible": true,
            "skin": "sknFontIconPrevNextDisable",
            "i18n_text": "kony.i18n.getLocalizedString(\"i18n.userwidgetmodel.fontIconPrevious\")",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, controller.args[0], "lblIconPrevious"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblIconPrevious"), extendConfig({}, controller.args[2], "lblIconPrevious"));
        flxPrevious.add(lblIconPrevious);
        var flxNext = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "clipBounds": true,
            "height": "32dp",
            "id": "flxNext",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "isModalContainer": false,
            "skin": "slFbox",
            "top": "0dp",
            "width": "32px",
            "zIndex": 1
        }, controller.args[0], "flxNext"), extendConfig({}, controller.args[1], "flxNext"), extendConfig({}, controller.args[2], "flxNext"));
        flxNext.setDefaultUnit(kony.flex.DP);
        var lblIconNext = new kony.ui.Label(extendConfig({
            "centerX": "50%",
            "centerY": "50%",
            "id": "lblIconNext",
            "isVisible": true,
            "skin": "sknFontIconPrevNextDisable",
            "i18n_text": "kony.i18n.getLocalizedString(\"i18n.userwidgetmodel.fontIconNext\")",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, controller.args[0], "lblIconNext"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblIconNext"), extendConfig({}, controller.args[2], "lblIconNext"));
        flxNext.add(lblIconNext);
        flxPaginationWrapper.add(lbxPagination, flxPaginationSeperator, flxPrevious, flxNext);
        flxPagination.add(flxPaginationWrapper);
        var flxProductStatusFilter = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
            "clipBounds": true,
            "id": "flxProductStatusFilter",
            "isVisible": false,
            "layoutType": kony.flex.FREE_FORM,
            "isModalContainer": false,
            "right": "75px",
            "skin": "slFbox",
            "top": "33dp",
            "width": "120px",
            "zIndex": 11
        }, controller.args[0], "flxProductStatusFilter"), extendConfig({}, controller.args[1], "flxProductStatusFilter"), extendConfig({}, controller.args[2], "flxProductStatusFilter"));
        flxProductStatusFilter.setDefaultUnit(kony.flex.DP);
        var statusFilterMenu = new com.adminConsole.common.statusFilterMenu(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
            "id": "statusFilterMenu",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "left": "0dp",
            "masterType": constants.MASTER_TYPE_DEFAULT,
            "isModalContainer": false,
            "skin": "slFbox",
            "top": "5dp",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 5,
            "overrides": {
                "imgUpArrow": {
                    "left": "viz.val_cleared",
                    "right": "15dp",
                    "src": "uparrow_2x.png"
                },
                "segStatusFilterDropdown": {
                    "data": [{
                        "imgCheckBox": "checkbox.png",
                        "lblDescription": "Active"
                    }, {
                        "imgCheckBox": "checkbox.png",
                        "lblDescription": "Inactive"
                    }, {
                        "imgCheckBox": "checkbox.png",
                        "lblDescription": "Suspended"
                    }]
                },
                "statusFilterMenu": {
                    "width": kony.flex.USE_PREFFERED_SIZE
                }
            }
        }, controller.args[0], "statusFilterMenu"), extendConfig({
            "overrides": {}
        }, controller.args[1], "statusFilterMenu"), extendConfig({
            "overrides": {}
        }, controller.args[2], "statusFilterMenu"));
        flxProductStatusFilter.add(statusFilterMenu);
        var flxProductGroupFilter = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
            "clipBounds": true,
            "id": "flxProductGroupFilter",
            "isVisible": false,
            "layoutType": kony.flex.FREE_FORM,
            "isModalContainer": false,
            "right": "270dp",
            "skin": "slFbox",
            "top": "33dp",
            "width": "150dp",
            "zIndex": 11
        }, controller.args[0], "flxProductGroupFilter"), extendConfig({}, controller.args[1], "flxProductGroupFilter"), extendConfig({}, controller.args[2], "flxProductGroupFilter"));
        flxProductGroupFilter.setDefaultUnit(kony.flex.DP);
        var productGroupFilterMenu = new com.adminConsole.Products.productTypeFilterMenu(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
            "id": "productGroupFilterMenu",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "left": "0dp",
            "masterType": constants.MASTER_TYPE_DEFAULT,
            "isModalContainer": false,
            "right": "0dp",
            "skin": "slFbox",
            "top": "5dp",
            "zIndex": 5,
            "overrides": {
                "flxArrowImage": {
                    "right": "0dp",
                    "width": "viz.val_cleared"
                },
                "flxCheckboxInner": {
                    "right": "0dp",
                    "width": "viz.val_cleared"
                },
                "flxFilterSort": {
                    "isVisible": false
                },
                "imgUpArrow": {
                    "src": "uparrow_2x.png"
                },
                "lblSeperator1": {
                    "isVisible": false
                },
                "productTypeFilterMenu": {
                    "right": "0dp",
                    "width": "viz.val_cleared"
                },
                "segStatusFilterDropdown": {
                    "data": [{
                        "imgCheckBox": "checkbox.png",
                        "lblDescription": "Active"
                    }, {
                        "imgCheckBox": "checkbox.png",
                        "lblDescription": "Inactive"
                    }, {
                        "imgCheckBox": "checkbox.png",
                        "lblDescription": "Suspended"
                    }],
                    "right": "0dp"
                }
            }
        }, controller.args[0], "productGroupFilterMenu"), extendConfig({
            "overrides": {}
        }, controller.args[1], "productGroupFilterMenu"), extendConfig({
            "overrides": {}
        }, controller.args[2], "productGroupFilterMenu"));
        flxProductGroupFilter.add(productGroupFilterMenu);
        var flxProductLineFilter = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
            "clipBounds": true,
            "id": "flxProductLineFilter",
            "isVisible": false,
            "layoutType": kony.flex.FREE_FORM,
            "isModalContainer": false,
            "right": "130dp",
            "skin": "slFbox",
            "top": "33dp",
            "width": "200dp",
            "zIndex": 11
        }, controller.args[0], "flxProductLineFilter"), extendConfig({}, controller.args[1], "flxProductLineFilter"), extendConfig({}, controller.args[2], "flxProductLineFilter"));
        flxProductLineFilter.setDefaultUnit(kony.flex.DP);
        var productLineFilterMenu = new com.adminConsole.Products.productTypeFilterMenu(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
            "id": "productLineFilterMenu",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "left": "0dp",
            "masterType": constants.MASTER_TYPE_DEFAULT,
            "isModalContainer": false,
            "skin": "slFbox",
            "top": "5dp",
            "width": "100%",
            "zIndex": 5,
            "overrides": {
                "flxFilterSort": {
                    "isVisible": false
                },
                "imgUpArrow": {
                    "src": "uparrow_2x.png"
                },
                "lblSeperator1": {
                    "isVisible": false
                },
                "segStatusFilterDropdown": {
                    "data": [{
                        "imgCheckBox": "checkbox.png",
                        "lblDescription": "Active"
                    }, {
                        "imgCheckBox": "checkbox.png",
                        "lblDescription": "Inactive"
                    }, {
                        "imgCheckBox": "checkbox.png",
                        "lblDescription": "Suspended"
                    }]
                }
            }
        }, controller.args[0], "productLineFilterMenu"), extendConfig({
            "overrides": {}
        }, controller.args[1], "productLineFilterMenu"), extendConfig({
            "overrides": {}
        }, controller.args[2], "productLineFilterMenu"));
        flxProductLineFilter.add(productLineFilterMenu);
        flxProductList.add(flxProductsHeader, segProducts, flxNoRecordsFound, flxPagination, flxProductStatusFilter, flxProductGroupFilter, flxProductLineFilter);
        flxProducts.add(subHeader, flxProductList);
        var flxProductDetails = new kony.ui.FlexScrollContainer(extendConfig({
            "allowHorizontalBounce": false,
            "allowVerticalBounce": true,
            "bottom": "20px",
            "bounces": true,
            "clipBounds": true,
            "enableScrolling": true,
            "horizontalScrollIndicator": true,
            "id": "flxProductDetails",
            "isVisible": false,
            "layoutType": kony.flex.FREE_FORM,
            "left": "0px",
            "pagingEnabled": false,
            "right": "0px",
            "scrollDirection": kony.flex.SCROLL_VERTICAL,
            "skin": "sknscrollflxf5f6f8Op100",
            "top": "0px",
            "verticalScrollIndicator": true,
            "zIndex": 1
        }, controller.args[0], "flxProductDetails"), extendConfig({}, controller.args[1], "flxProductDetails"), extendConfig({}, controller.args[2], "flxProductDetails"));
        flxProductDetails.setDefaultUnit(kony.flex.DP);
        var flxViewDetails = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
            "clipBounds": true,
            "id": "flxViewDetails",
            "isVisible": true,
            "layoutType": kony.flex.FLOW_VERTICAL,
            "left": "0px",
            "isModalContainer": false,
            "right": "0px",
            "skin": "slFbox",
            "top": "0px",
            "zIndex": 1
        }, controller.args[0], "flxViewDetails"), extendConfig({}, controller.args[1], "flxViewDetails"), extendConfig({}, controller.args[2], "flxViewDetails"));
        flxViewDetails.setDefaultUnit(kony.flex.DP);
        var flxTopDetails = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
            "clipBounds": true,
            "id": "flxTopDetails",
            "isVisible": true,
            "layoutType": kony.flex.FLOW_VERTICAL,
            "left": "0px",
            "isModalContainer": false,
            "skin": "sknFlxBgFFFFFFBorE4E6ECsz1pxR3px",
            "top": "0px",
            "width": "100%",
            "zIndex": 1
        }, controller.args[0], "flxTopDetails"), extendConfig({}, controller.args[1], "flxTopDetails"), extendConfig({}, controller.args[2], "flxTopDetails"));
        flxTopDetails.setDefaultUnit(kony.flex.DP);
        var flxHeadRow = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "clipBounds": true,
            "height": "56dp",
            "id": "flxHeadRow",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "left": "0dp",
            "isModalContainer": false,
            "skin": "slFbox",
            "top": "0dp",
            "width": "100%"
        }, controller.args[0], "flxHeadRow"), extendConfig({}, controller.args[1], "flxHeadRow"), extendConfig({}, controller.args[2], "flxHeadRow"));
        flxHeadRow.setDefaultUnit(kony.flex.DP);
        var lblProductNameSelected = new kony.ui.Label(extendConfig({
            "id": "lblProductNameSelected",
            "isVisible": true,
            "left": "20dp",
            "skin": "sknLatoRegular192B4518px",
            "text": "SapphireCreditCard_Aug19",
            "top": "17dp",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, controller.args[0], "lblProductNameSelected"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblProductNameSelected"), extendConfig({}, controller.args[2], "lblProductNameSelected"));
        var flxStatus = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "clipBounds": true,
            "height": "16dp",
            "id": "flxStatus",
            "isVisible": true,
            "layoutType": kony.flex.FLOW_HORIZONTAL,
            "reverseLayoutDirection": true,
            "isModalContainer": false,
            "right": "50dp",
            "skin": "slFbox",
            "top": "20dp",
            "width": "80dp",
            "zIndex": 1
        }, controller.args[0], "flxStatus"), extendConfig({}, controller.args[1], "flxStatus"), extendConfig({}, controller.args[2], "flxStatus"));
        flxStatus.setDefaultUnit(kony.flex.DP);
        var lblStatus = new kony.ui.Label(extendConfig({
            "centerY": "50%",
            "height": "16px",
            "id": "lblStatus",
            "isVisible": true,
            "right": "0px",
            "skin": "sknlblLatoReg485c7513px",
            "text": "Active",
            "top": "0px",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, controller.args[0], "lblStatus"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblStatus"), extendConfig({}, controller.args[2], "lblStatus"));
        var fontIconStatus = new kony.ui.Label(extendConfig({
            "centerY": "50%",
            "id": "fontIconStatus",
            "isVisible": true,
            "right": "5px",
            "skin": "sknfontIconInactive",
            "text": "",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, controller.args[0], "fontIconStatus"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "fontIconStatus"), extendConfig({}, controller.args[2], "fontIconStatus"));
        flxStatus.add(lblStatus, fontIconStatus);
        var flxOptions = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "clipBounds": true,
            "height": "25dp",
            "id": "flxOptions",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "isModalContainer": false,
            "right": "20dp",
            "skin": "sknFlxBorffffff1pxRound",
            "top": "17dp",
            "width": "25dp",
            "zIndex": 1
        }, controller.args[0], "flxOptions"), extendConfig({}, controller.args[1], "flxOptions"), extendConfig({
            "hoverSkin": "sknflxffffffop100Border424242Radius100px"
        }, controller.args[2], "flxOptions"));
        flxOptions.setDefaultUnit(kony.flex.DP);
        var lblIconOptions = new kony.ui.Label(extendConfig({
            "centerX": "50%",
            "centerY": "50%",
            "id": "lblIconOptions",
            "isVisible": true,
            "skin": "sknFontIconOptionMenu",
            "text": "",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, controller.args[0], "lblIconOptions"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblIconOptions"), extendConfig({}, controller.args[2], "lblIconOptions"));
        flxOptions.add(lblIconOptions);
        var lblSeparatorTopDetails = new kony.ui.Label(extendConfig({
            "bottom": "0px",
            "centerX": "50%",
            "height": "1px",
            "id": "lblSeparatorTopDetails",
            "isVisible": true,
            "left": "20px",
            "right": "20px",
            "skin": "sknlblSeperator",
            "text": "Label",
            "zIndex": 2
        }, controller.args[0], "lblSeparatorTopDetails"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblSeparatorTopDetails"), extendConfig({}, controller.args[2], "lblSeparatorTopDetails"));
        flxHeadRow.add(lblProductNameSelected, flxStatus, flxOptions, lblSeparatorTopDetails);
        var flxFirstRow = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
            "clipBounds": true,
            "id": "flxFirstRow",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "left": "0px",
            "isModalContainer": false,
            "skin": "slFbox",
            "top": "30px",
            "width": "100%"
        }, controller.args[0], "flxFirstRow"), extendConfig({}, controller.args[1], "flxFirstRow"), extendConfig({}, controller.args[2], "flxFirstRow"));
        flxFirstRow.setDefaultUnit(kony.flex.DP);
        var lblProductLineHeader = new kony.ui.Label(extendConfig({
            "id": "lblProductLineHeader",
            "isVisible": true,
            "left": "20px",
            "skin": "sknlblLato11px696C73",
            "text": "PRODUCT LINE",
            "top": "0px",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, controller.args[0], "lblProductLineHeader"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblProductLineHeader"), extendConfig({}, controller.args[2], "lblProductLineHeader"));
        var lblProductLineDetail = new kony.ui.Label(extendConfig({
            "id": "lblProductLineDetail",
            "isVisible": true,
            "left": "20px",
            "skin": "sknlbl485C75LatoSemiBold13px",
            "text": "ProductLine",
            "top": "25px",
            "width": "34%",
            "zIndex": 1
        }, controller.args[0], "lblProductLineDetail"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblProductLineDetail"), extendConfig({}, controller.args[2], "lblProductLineDetail"));
        var lblProductGroupHeader = new kony.ui.Label(extendConfig({
            "id": "lblProductGroupHeader",
            "isVisible": true,
            "left": "39%",
            "skin": "sknlblLato11px696C73",
            "text": "PRODUCT GROUP",
            "top": "0px",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, controller.args[0], "lblProductGroupHeader"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblProductGroupHeader"), extendConfig({}, controller.args[2], "lblProductGroupHeader"));
        var lblProductGroupDetail = new kony.ui.Label(extendConfig({
            "id": "lblProductGroupDetail",
            "isVisible": true,
            "left": "39%",
            "skin": "sknlbl485C75LatoSemiBold13px",
            "text": "ProductGroup",
            "top": "25px",
            "width": "34%",
            "zIndex": 1
        }, controller.args[0], "lblProductGroupDetail"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblProductGroupDetail"), extendConfig({}, controller.args[2], "lblProductGroupDetail"));
        var lblProductRefHeader = new kony.ui.Label(extendConfig({
            "id": "lblProductRefHeader",
            "isVisible": true,
            "left": "75%",
            "skin": "sknlblLato11px696C73",
            "text": "PRODUCT REFERENCE",
            "top": "0px",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, controller.args[0], "lblProductRefHeader"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblProductRefHeader"), extendConfig({}, controller.args[2], "lblProductRefHeader"));
        var lblProductRefDetail = new kony.ui.Label(extendConfig({
            "id": "lblProductRefDetail",
            "isVisible": true,
            "left": "75%",
            "skin": "sknlbl485C75LatoSemiBold13px",
            "text": "ProductRef",
            "top": "25px",
            "width": "25%",
            "zIndex": 1
        }, controller.args[0], "lblProductRefDetail"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblProductRefDetail"), extendConfig({}, controller.args[2], "lblProductRefDetail"));
        flxFirstRow.add(lblProductLineHeader, lblProductLineDetail, lblProductGroupHeader, lblProductGroupDetail, lblProductRefHeader, lblProductRefDetail);
        var flxSecondRow = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
            "clipBounds": true,
            "id": "flxSecondRow",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "left": "0px",
            "isModalContainer": false,
            "skin": "slFbox",
            "top": "30px",
            "width": "100%"
        }, controller.args[0], "flxSecondRow"), extendConfig({}, controller.args[1], "flxSecondRow"), extendConfig({}, controller.args[2], "flxSecondRow"));
        flxSecondRow.setDefaultUnit(kony.flex.DP);
        var lblPurposeHeader = new kony.ui.Label(extendConfig({
            "id": "lblPurposeHeader",
            "isVisible": true,
            "left": "20px",
            "skin": "sknlblLato11px696C73",
            "text": "PURPOSE",
            "top": "0px",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, controller.args[0], "lblPurposeHeader"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblPurposeHeader"), extendConfig({}, controller.args[2], "lblPurposeHeader"));
        var lblPurpose = new kony.ui.Label(extendConfig({
            "id": "lblPurpose",
            "isVisible": true,
            "left": "20px",
            "skin": "sknlbl485C75LatoSemiBold13px",
            "text": "lblPurpose",
            "top": "25px",
            "width": "30%",
            "zIndex": 1
        }, controller.args[0], "lblPurpose"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblPurpose"), extendConfig({}, controller.args[2], "lblPurpose"));
        var lblAvailableFromDateHeader = new kony.ui.Label(extendConfig({
            "id": "lblAvailableFromDateHeader",
            "isVisible": true,
            "left": "39%",
            "skin": "sknlblLato11px696C73",
            "text": "AVAILABLE FROM DATE",
            "top": "0px",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, controller.args[0], "lblAvailableFromDateHeader"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblAvailableFromDateHeader"), extendConfig({}, controller.args[2], "lblAvailableFromDateHeader"));
        var lblAvailableFromDate = new kony.ui.Label(extendConfig({
            "id": "lblAvailableFromDate",
            "isVisible": true,
            "left": "39%",
            "skin": "sknlbl485C75LatoSemiBold13px",
            "text": "lblAvailableFromDate",
            "top": "25px",
            "width": "30%",
            "zIndex": 1
        }, controller.args[0], "lblAvailableFromDate"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblAvailableFromDate"), extendConfig({}, controller.args[2], "lblAvailableFromDate"));
        var lblAvailableToDateHeader = new kony.ui.Label(extendConfig({
            "id": "lblAvailableToDateHeader",
            "isVisible": true,
            "left": "75%",
            "skin": "sknlblLato11px696C73",
            "text": "AVAILABLE TO DATE",
            "top": "0px",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, controller.args[0], "lblAvailableToDateHeader"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblAvailableToDateHeader"), extendConfig({}, controller.args[2], "lblAvailableToDateHeader"));
        var lblAvailableToDate = new kony.ui.Label(extendConfig({
            "id": "lblAvailableToDate",
            "isVisible": true,
            "left": "75%",
            "skin": "sknlbl485C75LatoSemiBold13px",
            "text": "lblAvailableToDate",
            "top": "25px",
            "width": "30%",
            "zIndex": 1
        }, controller.args[0], "lblAvailableToDate"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblAvailableToDate"), extendConfig({}, controller.args[2], "lblAvailableToDate"));
        flxSecondRow.add(lblPurposeHeader, lblPurpose, lblAvailableFromDateHeader, lblAvailableFromDate, lblAvailableToDateHeader, lblAvailableToDate);
        var flxThirdRow = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
            "clipBounds": true,
            "id": "flxThirdRow",
            "isVisible": false,
            "layoutType": kony.flex.FLOW_VERTICAL,
            "left": "0px",
            "isModalContainer": false,
            "skin": "slFbox",
            "top": "30px",
            "width": "100%"
        }, controller.args[0], "flxThirdRow"), extendConfig({}, controller.args[1], "flxThirdRow"), extendConfig({}, controller.args[2], "flxThirdRow"));
        flxThirdRow.setDefaultUnit(kony.flex.DP);
        var flxRow1 = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
            "clipBounds": true,
            "id": "flxRow1",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "left": "0px",
            "isModalContainer": false,
            "skin": "slFbox",
            "top": "0px",
            "width": "100%"
        }, controller.args[0], "flxRow1"), extendConfig({}, controller.args[1], "flxRow1"), extendConfig({}, controller.args[2], "flxRow1"));
        flxRow1.setDefaultUnit(kony.flex.DP);
        var lblLabel1 = new kony.ui.Label(extendConfig({
            "id": "lblLabel1",
            "isVisible": true,
            "left": "20px",
            "skin": "sknlblLato11px696C73",
            "text": "LABEL 1",
            "top": "0px",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, controller.args[0], "lblLabel1"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblLabel1"), extendConfig({}, controller.args[2], "lblLabel1"));
        var lblValue1 = new kony.ui.Label(extendConfig({
            "id": "lblValue1",
            "isVisible": true,
            "left": "20px",
            "skin": "sknlbl485C75LatoSemiBold13px",
            "text": "Value 1",
            "top": "25px",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, controller.args[0], "lblValue1"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblValue1"), extendConfig({}, controller.args[2], "lblValue1"));
        var lblLabel2 = new kony.ui.Label(extendConfig({
            "id": "lblLabel2",
            "isVisible": true,
            "left": "39%",
            "skin": "sknlblLato11px696C73",
            "text": "LABEL 2",
            "top": "0px",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, controller.args[0], "lblLabel2"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblLabel2"), extendConfig({}, controller.args[2], "lblLabel2"));
        var lblValue2 = new kony.ui.Label(extendConfig({
            "id": "lblValue2",
            "isVisible": true,
            "left": "39%",
            "skin": "sknlbl485C75LatoSemiBold13px",
            "text": "Value 2",
            "top": "25px",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, controller.args[0], "lblValue2"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblValue2"), extendConfig({}, controller.args[2], "lblValue2"));
        var lblLabel3 = new kony.ui.Label(extendConfig({
            "id": "lblLabel3",
            "isVisible": true,
            "left": "75%",
            "skin": "sknlblLato11px696C73",
            "text": "LABEL 3",
            "top": "0px",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, controller.args[0], "lblLabel3"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblLabel3"), extendConfig({}, controller.args[2], "lblLabel3"));
        var lblValue3 = new kony.ui.Label(extendConfig({
            "id": "lblValue3",
            "isVisible": true,
            "left": "75%",
            "skin": "sknlbl485C75LatoSemiBold13px",
            "text": "Value 3",
            "top": "25px",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, controller.args[0], "lblValue3"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblValue3"), extendConfig({}, controller.args[2], "lblValue3"));
        flxRow1.add(lblLabel1, lblValue1, lblLabel2, lblValue2, lblLabel3, lblValue3);
        var flxRow2 = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
            "clipBounds": true,
            "id": "flxRow2",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "left": "0px",
            "isModalContainer": false,
            "skin": "slFbox",
            "top": "30px",
            "width": "100%"
        }, controller.args[0], "flxRow2"), extendConfig({}, controller.args[1], "flxRow2"), extendConfig({}, controller.args[2], "flxRow2"));
        flxRow2.setDefaultUnit(kony.flex.DP);
        var lblLabel4 = new kony.ui.Label(extendConfig({
            "id": "lblLabel4",
            "isVisible": true,
            "left": "20px",
            "skin": "sknlblLato11px696C73",
            "text": "LABEL 4",
            "top": "0px",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, controller.args[0], "lblLabel4"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblLabel4"), extendConfig({}, controller.args[2], "lblLabel4"));
        var lblValue4 = new kony.ui.Label(extendConfig({
            "id": "lblValue4",
            "isVisible": true,
            "left": "20px",
            "skin": "sknlbl485C75LatoSemiBold13px",
            "text": "Value 4",
            "top": "25px",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, controller.args[0], "lblValue4"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblValue4"), extendConfig({}, controller.args[2], "lblValue4"));
        var lblLabel5 = new kony.ui.Label(extendConfig({
            "id": "lblLabel5",
            "isVisible": true,
            "left": "39%",
            "skin": "sknlblLato11px696C73",
            "text": "LABEL 6",
            "top": "0px",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, controller.args[0], "lblLabel5"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblLabel5"), extendConfig({}, controller.args[2], "lblLabel5"));
        var lblValue5 = new kony.ui.Label(extendConfig({
            "id": "lblValue5",
            "isVisible": true,
            "left": "39%",
            "skin": "sknlbl485C75LatoSemiBold13px",
            "text": "Value 5",
            "top": "25px",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, controller.args[0], "lblValue5"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblValue5"), extendConfig({}, controller.args[2], "lblValue5"));
        var lblLabel6 = new kony.ui.Label(extendConfig({
            "id": "lblLabel6",
            "isVisible": true,
            "left": "75%",
            "skin": "sknlblLato11px696C73",
            "text": "LABEL 6",
            "top": "0px",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, controller.args[0], "lblLabel6"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblLabel6"), extendConfig({}, controller.args[2], "lblLabel6"));
        var lblValue6 = new kony.ui.Label(extendConfig({
            "id": "lblValue6",
            "isVisible": true,
            "left": "75%",
            "skin": "sknlbl485C75LatoSemiBold13px",
            "text": "Value 6",
            "top": "25px",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, controller.args[0], "lblValue6"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblValue6"), extendConfig({}, controller.args[2], "lblValue6"));
        flxRow2.add(lblLabel4, lblValue4, lblLabel5, lblValue5, lblLabel6, lblValue6);
        var flxRow3 = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
            "clipBounds": true,
            "id": "flxRow3",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "left": "0px",
            "isModalContainer": false,
            "skin": "slFbox",
            "top": "30px",
            "width": "100%"
        }, controller.args[0], "flxRow3"), extendConfig({}, controller.args[1], "flxRow3"), extendConfig({}, controller.args[2], "flxRow3"));
        flxRow3.setDefaultUnit(kony.flex.DP);
        var lblLabel7 = new kony.ui.Label(extendConfig({
            "id": "lblLabel7",
            "isVisible": true,
            "left": "20px",
            "skin": "sknlblLato11px696C73",
            "text": "LABEL 7",
            "top": "0px",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, controller.args[0], "lblLabel7"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblLabel7"), extendConfig({}, controller.args[2], "lblLabel7"));
        var lblValue7 = new kony.ui.Label(extendConfig({
            "id": "lblValue7",
            "isVisible": true,
            "left": "20px",
            "skin": "sknlbl485C75LatoSemiBold13px",
            "text": "Value 7",
            "top": "25px",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, controller.args[0], "lblValue7"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblValue7"), extendConfig({}, controller.args[2], "lblValue7"));
        var lblLabel8 = new kony.ui.Label(extendConfig({
            "id": "lblLabel8",
            "isVisible": true,
            "left": "39%",
            "skin": "sknlblLato11px696C73",
            "text": "LABEL 8",
            "top": "0px",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, controller.args[0], "lblLabel8"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblLabel8"), extendConfig({}, controller.args[2], "lblLabel8"));
        var lblValue8 = new kony.ui.Label(extendConfig({
            "id": "lblValue8",
            "isVisible": true,
            "left": "39%",
            "skin": "sknlbl485C75LatoSemiBold13px",
            "text": "Value 8",
            "top": "25px",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, controller.args[0], "lblValue8"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblValue8"), extendConfig({}, controller.args[2], "lblValue8"));
        var lblLabel9 = new kony.ui.Label(extendConfig({
            "id": "lblLabel9",
            "isVisible": true,
            "left": "75%",
            "skin": "sknlblLato11px696C73",
            "text": "LABEL 9",
            "top": "0px",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, controller.args[0], "lblLabel9"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblLabel9"), extendConfig({}, controller.args[2], "lblLabel9"));
        var lblValue9 = new kony.ui.Label(extendConfig({
            "id": "lblValue9",
            "isVisible": true,
            "left": "75%",
            "skin": "sknlbl485C75LatoSemiBold13px",
            "text": "Value 9",
            "top": "25px",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, controller.args[0], "lblValue9"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblValue9"), extendConfig({}, controller.args[2], "lblValue9"));
        flxRow3.add(lblLabel7, lblValue7, lblLabel8, lblValue8, lblLabel9, lblValue9);
        var flxRow4 = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
            "clipBounds": true,
            "id": "flxRow4",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "left": "0px",
            "isModalContainer": false,
            "skin": "slFbox",
            "top": "30px",
            "width": "100%"
        }, controller.args[0], "flxRow4"), extendConfig({}, controller.args[1], "flxRow4"), extendConfig({}, controller.args[2], "flxRow4"));
        flxRow4.setDefaultUnit(kony.flex.DP);
        var lblLabel10 = new kony.ui.Label(extendConfig({
            "id": "lblLabel10",
            "isVisible": true,
            "left": "20px",
            "skin": "sknlblLato11px696C73",
            "text": "LABEL 10",
            "top": "0px",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, controller.args[0], "lblLabel10"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblLabel10"), extendConfig({}, controller.args[2], "lblLabel10"));
        var lblValue10 = new kony.ui.Label(extendConfig({
            "id": "lblValue10",
            "isVisible": true,
            "left": "20px",
            "skin": "sknlbl485C75LatoSemiBold13px",
            "text": "Value 10",
            "top": "25px",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, controller.args[0], "lblValue10"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblValue10"), extendConfig({}, controller.args[2], "lblValue10"));
        var lblLabel11 = new kony.ui.Label(extendConfig({
            "id": "lblLabel11",
            "isVisible": true,
            "left": "39%",
            "skin": "sknlblLato11px696C73",
            "text": "LABEL 11",
            "top": "0px",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, controller.args[0], "lblLabel11"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblLabel11"), extendConfig({}, controller.args[2], "lblLabel11"));
        var lblValue11 = new kony.ui.Label(extendConfig({
            "id": "lblValue11",
            "isVisible": true,
            "left": "39%",
            "skin": "sknlbl485C75LatoSemiBold13px",
            "text": "Value 11",
            "top": "25px",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, controller.args[0], "lblValue11"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblValue11"), extendConfig({}, controller.args[2], "lblValue11"));
        var lblLabel12 = new kony.ui.Label(extendConfig({
            "id": "lblLabel12",
            "isVisible": true,
            "left": "75%",
            "skin": "sknlblLato11px696C73",
            "text": "LABEL 12",
            "top": "0px",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, controller.args[0], "lblLabel12"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblLabel12"), extendConfig({}, controller.args[2], "lblLabel12"));
        var lblValue12 = new kony.ui.Label(extendConfig({
            "id": "lblValue12",
            "isVisible": true,
            "left": "75%",
            "skin": "sknlbl485C75LatoSemiBold13px",
            "text": "Value 12",
            "top": "25px",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, controller.args[0], "lblValue12"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblValue12"), extendConfig({}, controller.args[2], "lblValue12"));
        flxRow4.add(lblLabel10, lblValue10, lblLabel11, lblValue11, lblLabel12, lblValue12);
        var flxRow5 = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
            "clipBounds": true,
            "id": "flxRow5",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "left": "0px",
            "isModalContainer": false,
            "skin": "slFbox",
            "top": "30px",
            "width": "100%"
        }, controller.args[0], "flxRow5"), extendConfig({}, controller.args[1], "flxRow5"), extendConfig({}, controller.args[2], "flxRow5"));
        flxRow5.setDefaultUnit(kony.flex.DP);
        var lblLabel13 = new kony.ui.Label(extendConfig({
            "id": "lblLabel13",
            "isVisible": true,
            "left": "20px",
            "skin": "sknlblLato11px696C73",
            "text": "LABEL 13",
            "top": "0px",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, controller.args[0], "lblLabel13"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblLabel13"), extendConfig({}, controller.args[2], "lblLabel13"));
        var lblValue13 = new kony.ui.Label(extendConfig({
            "id": "lblValue13",
            "isVisible": true,
            "left": "20px",
            "skin": "sknlbl485C75LatoSemiBold13px",
            "text": "Value 13",
            "top": "25px",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, controller.args[0], "lblValue13"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblValue13"), extendConfig({}, controller.args[2], "lblValue13"));
        var lblLabel14 = new kony.ui.Label(extendConfig({
            "id": "lblLabel14",
            "isVisible": true,
            "left": "39%",
            "skin": "sknlblLato11px696C73",
            "text": "LABEL 14",
            "top": "0px",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, controller.args[0], "lblLabel14"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblLabel14"), extendConfig({}, controller.args[2], "lblLabel14"));
        var lblValue14 = new kony.ui.Label(extendConfig({
            "id": "lblValue14",
            "isVisible": true,
            "left": "39%",
            "skin": "sknlbl485C75LatoSemiBold13px",
            "text": "Value 14",
            "top": "25px",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, controller.args[0], "lblValue14"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblValue14"), extendConfig({}, controller.args[2], "lblValue14"));
        var lblLabel15 = new kony.ui.Label(extendConfig({
            "id": "lblLabel15",
            "isVisible": true,
            "left": "75%",
            "skin": "sknlblLato11px696C73",
            "text": "LABEL 15",
            "top": "0px",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, controller.args[0], "lblLabel15"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblLabel15"), extendConfig({}, controller.args[2], "lblLabel15"));
        var lblValue15 = new kony.ui.Label(extendConfig({
            "id": "lblValue15",
            "isVisible": true,
            "left": "75%",
            "skin": "sknlbl485C75LatoSemiBold13px",
            "text": "Value 15",
            "top": "25px",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, controller.args[0], "lblValue15"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblValue15"), extendConfig({}, controller.args[2], "lblValue15"));
        flxRow5.add(lblLabel13, lblValue13, lblLabel14, lblValue14, lblLabel15, lblValue15);
        flxThirdRow.add(flxRow1, flxRow2, flxRow3, flxRow4, flxRow5);
        var flxFourthRow = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
            "bottom": "20px",
            "clipBounds": true,
            "id": "flxFourthRow",
            "isVisible": true,
            "layoutType": kony.flex.FLOW_VERTICAL,
            "left": "0px",
            "isModalContainer": false,
            "skin": "slFbox",
            "top": "30px",
            "width": "100%"
        }, controller.args[0], "flxFourthRow"), extendConfig({}, controller.args[1], "flxFourthRow"), extendConfig({}, controller.args[2], "flxFourthRow"));
        flxFourthRow.setDefaultUnit(kony.flex.DP);
        var flxProductDescHeader = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "clipBounds": true,
            "height": "25px",
            "id": "flxProductDescHeader",
            "isVisible": true,
            "layoutType": kony.flex.FLOW_HORIZONTAL,
            "left": "0px",
            "isModalContainer": false,
            "skin": "slFbox",
            "top": "5px",
            "width": "100%"
        }, controller.args[0], "flxProductDescHeader"), extendConfig({}, controller.args[1], "flxProductDescHeader"), extendConfig({}, controller.args[2], "flxProductDescHeader"));
        flxProductDescHeader.setDefaultUnit(kony.flex.DP);
        var lblProductDescHeader = new kony.ui.Label(extendConfig({
            "centerY": "50%",
            "id": "lblProductDescHeader",
            "isVisible": true,
            "left": "20dp",
            "skin": "sknLbl192b45LatoReg16px",
            "text": "Product Description",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, controller.args[0], "lblProductDescHeader"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblProductDescHeader"), extendConfig({}, controller.args[2], "lblProductDescHeader"));
        var flxProductDescDropdown = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "clipBounds": true,
            "height": "16px",
            "id": "flxProductDescDropdown",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "left": "10px",
            "isModalContainer": false,
            "onClick": controller.AS_FlexContainer_aeb8168ea3624740bf06d583316c5596,
            "skin": "sknCursor",
            "top": "6px",
            "width": "16px",
            "zIndex": 2
        }, controller.args[0], "flxProductDescDropdown"), extendConfig({}, controller.args[1], "flxProductDescDropdown"), extendConfig({}, controller.args[2], "flxProductDescDropdown"));
        flxProductDescDropdown.setDefaultUnit(kony.flex.DP);
        var fonticonArrow = new kony.ui.Label(extendConfig({
            "centerX": "50%",
            "centerY": "50%",
            "height": "16dp",
            "id": "fonticonArrow",
            "isVisible": true,
            "skin": "sknIcon6E7178Sz15px",
            "text": "",
            "width": "16dp",
            "zIndex": 1
        }, controller.args[0], "fonticonArrow"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "fonticonArrow"), extendConfig({}, controller.args[2], "fonticonArrow"));
        flxProductDescDropdown.add(fonticonArrow);
        flxProductDescHeader.add(lblProductDescHeader, flxProductDescDropdown);
        var flxProductDescBody = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
            "clipBounds": true,
            "id": "flxProductDescBody",
            "isVisible": true,
            "layoutType": kony.flex.FLOW_VERTICAL,
            "left": "0px",
            "isModalContainer": false,
            "skin": "slFbox",
            "top": "20px",
            "width": "100%"
        }, controller.args[0], "flxProductDescBody"), extendConfig({}, controller.args[1], "flxProductDescBody"), extendConfig({}, controller.args[2], "flxProductDescBody"));
        flxProductDescBody.setDefaultUnit(kony.flex.DP);
        var flxDescription = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
            "clipBounds": true,
            "id": "flxDescription",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "left": "0px",
            "isModalContainer": false,
            "skin": "slFbox",
            "top": "0px",
            "width": "100%"
        }, controller.args[0], "flxDescription"), extendConfig({}, controller.args[1], "flxDescription"), extendConfig({}, controller.args[2], "flxDescription"));
        flxDescription.setDefaultUnit(kony.flex.DP);
        var lblDescriptionHeader = new kony.ui.Label(extendConfig({
            "id": "lblDescriptionHeader",
            "isVisible": true,
            "left": "20px",
            "skin": "sknlblLato11px696C73",
            "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmAdManagement.DescriptionCAPS\")",
            "top": "0px",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, controller.args[0], "lblDescriptionHeader"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblDescriptionHeader"), extendConfig({}, controller.args[2], "lblDescriptionHeader"));
        var lblDescription = new kony.ui.Label(extendConfig({
            "id": "lblDescription",
            "isVisible": true,
            "left": "20px",
            "right": 20,
            "skin": "sknlbl485C75LatoSemiBold13px",
            "text": "Promotion for Sapphire Credit Card, targeted  at all customers who live in Austin, TX Promotion for Sapphire Credit Card, targeted  at all customers who live in Austin, TX Promotion for Sapphire Credit Card, targeted  at all customers who live in Austin, TX Promotion for Sapphire Credit Card, targeted  at all customers who live in Austin, TX Promotion for Sapphire Credit Card, targeted  at all customers who live in Austin, TX Promotion for Sapphire Credit Card, targeted  at all customers who live in Austin, TX",
            "top": "27px",
            "zIndex": 1
        }, controller.args[0], "lblDescription"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblDescription"), extendConfig({}, controller.args[2], "lblDescription"));
        flxDescription.add(lblDescriptionHeader, lblDescription);
        var flxDetailedDesc = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
            "clipBounds": true,
            "id": "flxDetailedDesc",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "left": "0px",
            "isModalContainer": false,
            "skin": "slFbox",
            "top": "30px",
            "width": "100%"
        }, controller.args[0], "flxDetailedDesc"), extendConfig({}, controller.args[1], "flxDetailedDesc"), extendConfig({}, controller.args[2], "flxDetailedDesc"));
        flxDetailedDesc.setDefaultUnit(kony.flex.DP);
        var lblDetailedDescHeader = new kony.ui.Label(extendConfig({
            "id": "lblDetailedDescHeader",
            "isVisible": true,
            "left": "20px",
            "skin": "sknlblLato11px696C73",
            "text": "DETAILED DESCRIPTION",
            "top": "0px",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, controller.args[0], "lblDetailedDescHeader"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblDetailedDescHeader"), extendConfig({}, controller.args[2], "lblDetailedDescHeader"));
        var lblDetailedDesc = new kony.ui.Label(extendConfig({
            "id": "lblDetailedDesc",
            "isVisible": true,
            "left": "20px",
            "right": 20,
            "skin": "sknlbl485C75LatoSemiBold13px",
            "text": "Promotion for Sapphire Credit Card, targeted  at all customers who live in Austin, TX Promotion for Sapphire Credit Card, targeted  at all customers who live in Austin, TX Promotion for Sapphire Credit Card, targeted  at all customers who live in Austin, TX Promotion for Sapphire Credit Card, targeted  at all customers who live in Austin, TX Promotion for Sapphire Credit Card, targeted  at all customers who live in Austin, TX Promotion for Sapphire Credit Card, targeted  at all customers who live in Austin, TX",
            "top": "27px",
            "zIndex": 1
        }, controller.args[0], "lblDetailedDesc"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblDetailedDesc"), extendConfig({}, controller.args[2], "lblDetailedDesc"));
        flxDetailedDesc.add(lblDetailedDescHeader, lblDetailedDesc);
        var flxNotes = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
            "clipBounds": true,
            "id": "flxNotes",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "left": "0px",
            "isModalContainer": false,
            "skin": "slFbox",
            "top": "30px",
            "width": "100%"
        }, controller.args[0], "flxNotes"), extendConfig({}, controller.args[1], "flxNotes"), extendConfig({}, controller.args[2], "flxNotes"));
        flxNotes.setDefaultUnit(kony.flex.DP);
        var lblNotesHeader = new kony.ui.Label(extendConfig({
            "id": "lblNotesHeader",
            "isVisible": true,
            "left": "20px",
            "skin": "sknlblLato11px696C73",
            "text": "NOTES",
            "top": "0px",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, controller.args[0], "lblNotesHeader"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblNotesHeader"), extendConfig({}, controller.args[2], "lblNotesHeader"));
        var lblNotes = new kony.ui.Label(extendConfig({
            "id": "lblNotes",
            "isVisible": true,
            "left": "20px",
            "right": 20,
            "skin": "sknlbl485C75LatoSemiBold13px",
            "text": "Promotion for Sapphire Credit Card, targeted  at all customers who live in Austin, TX Promotion for Sapphire Credit Card, targeted  at all customers who live in Austin, TX Promotion for Sapphire Credit Card, targeted  at all customers who live in Austin, TX Promotion for Sapphire Credit Card, targeted  at all customers who live in Austin, TX Promotion for Sapphire Credit Card, targeted  at all customers who live in Austin, TX Promotion for Sapphire Credit Card, targeted  at all customers who live in Austin, TX",
            "top": "27px",
            "zIndex": 1
        }, controller.args[0], "lblNotes"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblNotes"), extendConfig({}, controller.args[2], "lblNotes"));
        flxNotes.add(lblNotesHeader, lblNotes);
        var flxDisclosure = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
            "clipBounds": true,
            "id": "flxDisclosure",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "left": "0px",
            "isModalContainer": false,
            "skin": "slFbox",
            "top": "30px",
            "width": "100%"
        }, controller.args[0], "flxDisclosure"), extendConfig({}, controller.args[1], "flxDisclosure"), extendConfig({}, controller.args[2], "flxDisclosure"));
        flxDisclosure.setDefaultUnit(kony.flex.DP);
        var lblDisclosureHeader = new kony.ui.Label(extendConfig({
            "id": "lblDisclosureHeader",
            "isVisible": true,
            "left": "20px",
            "skin": "sknlblLato11px696C73",
            "text": "DISCLOSURE",
            "top": "0px",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, controller.args[0], "lblDisclosureHeader"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblDisclosureHeader"), extendConfig({}, controller.args[2], "lblDisclosureHeader"));
        var lblDisclosure = new kony.ui.Label(extendConfig({
            "bottom": "19px",
            "id": "lblDisclosure",
            "isVisible": true,
            "left": "20px",
            "right": 20,
            "skin": "sknlbl485C75LatoSemiBold13px",
            "text": "Promotion for Sapphire Credit Card, targeted  at all customers who live in Austin, TX Promotion for Sapphire Credit Card, targeted  at all customers who live in Austin, TX Promotion for Sapphire Credit Card, targeted  at all customers who live in Austin, TX Promotion for Sapphire Credit Card, targeted  at all customers who live in Austin, TX Promotion for Sapphire Credit Card, targeted  at all customers who live in Austin, TX Promotion for Sapphire Credit Card, targeted  at all customers who live in Austin, TX",
            "top": "27px",
            "zIndex": 1
        }, controller.args[0], "lblDisclosure"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblDisclosure"), extendConfig({}, controller.args[2], "lblDisclosure"));
        var flxReadMoreDisclosure = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
            "bottom": "0px",
            "clipBounds": true,
            "id": "flxReadMoreDisclosure",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "left": "20px",
            "isModalContainer": false,
            "skin": "hoverhandSkin",
            "width": "65px"
        }, controller.args[0], "flxReadMoreDisclosure"), extendConfig({}, controller.args[1], "flxReadMoreDisclosure"), extendConfig({}, controller.args[2], "flxReadMoreDisclosure"));
        flxReadMoreDisclosure.setDefaultUnit(kony.flex.DP);
        var lblReadMoreDisclosure = new kony.ui.Label(extendConfig({
            "bottom": "0px",
            "id": "lblReadMoreDisclosure",
            "isVisible": true,
            "left": "0px",
            "skin": "sknLblLatoReg117eb013px",
            "text": "Read More",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, controller.args[0], "lblReadMoreDisclosure"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblReadMoreDisclosure"), extendConfig({}, controller.args[2], "lblReadMoreDisclosure"));
        flxReadMoreDisclosure.add(lblReadMoreDisclosure);
        flxDisclosure.add(lblDisclosureHeader, lblDisclosure, flxReadMoreDisclosure);
        var flxTermsAndConditions = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
            "clipBounds": true,
            "id": "flxTermsAndConditions",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "left": "0px",
            "isModalContainer": false,
            "skin": "slFbox",
            "top": "30px",
            "width": "100%"
        }, controller.args[0], "flxTermsAndConditions"), extendConfig({}, controller.args[1], "flxTermsAndConditions"), extendConfig({}, controller.args[2], "flxTermsAndConditions"));
        flxTermsAndConditions.setDefaultUnit(kony.flex.DP);
        var lblTermsAndConditionsHeader = new kony.ui.Label(extendConfig({
            "id": "lblTermsAndConditionsHeader",
            "isVisible": true,
            "left": "20px",
            "skin": "sknlblLato11px696C73",
            "text": "TERMS AND CONDITIONS",
            "top": "0px",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, controller.args[0], "lblTermsAndConditionsHeader"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblTermsAndConditionsHeader"), extendConfig({}, controller.args[2], "lblTermsAndConditionsHeader"));
        var lblTermsAndConditions = new kony.ui.Label(extendConfig({
            "bottom": "19px",
            "id": "lblTermsAndConditions",
            "isVisible": true,
            "left": "20px",
            "right": 20,
            "skin": "sknlbl485C75LatoSemiBold13px",
            "text": "Promotion for Sapphire Credit Card, targeted  at all customers who live in Austin, TX Promotion for Sapphire Credit Card, targeted  at all customers who live in Austin, TX Promotion for Sapphire Credit Card, targeted  at all customers who live in Austin, TX Promotion for Sapphire Credit Card, targeted  at all customers who live in Austin, TX Promotion for Sapphire Credit Card, targeted  at all customers who live in Austin, TX Promotion for Sapphire Credit Card, targeted  at all customers who live in Austin, TX",
            "top": "27px",
            "zIndex": 1
        }, controller.args[0], "lblTermsAndConditions"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblTermsAndConditions"), extendConfig({}, controller.args[2], "lblTermsAndConditions"));
        var flxReadMoreTC = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
            "bottom": "0px",
            "clipBounds": true,
            "id": "flxReadMoreTC",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "left": "20px",
            "isModalContainer": false,
            "skin": "hoverhandSkin",
            "width": "65px"
        }, controller.args[0], "flxReadMoreTC"), extendConfig({}, controller.args[1], "flxReadMoreTC"), extendConfig({}, controller.args[2], "flxReadMoreTC"));
        flxReadMoreTC.setDefaultUnit(kony.flex.DP);
        var lblReadMoreTC = new kony.ui.Label(extendConfig({
            "bottom": "0px",
            "id": "lblReadMoreTC",
            "isVisible": true,
            "left": "0px",
            "skin": "sknLblLatoReg117eb013px",
            "text": "Read More",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, controller.args[0], "lblReadMoreTC"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblReadMoreTC"), extendConfig({}, controller.args[2], "lblReadMoreTC"));
        flxReadMoreTC.add(lblReadMoreTC);
        flxTermsAndConditions.add(lblTermsAndConditionsHeader, lblTermsAndConditions, flxReadMoreTC);
        flxProductDescBody.add(flxDescription, flxDetailedDesc, flxNotes, flxDisclosure, flxTermsAndConditions);
        flxFourthRow.add(flxProductDescHeader, flxProductDescBody);
        flxTopDetails.add(flxHeadRow, flxFirstRow, flxSecondRow, flxThirdRow, flxFourthRow);
        var flxBottomDetails = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
            "clipBounds": true,
            "id": "flxBottomDetails",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "left": "0px",
            "isModalContainer": false,
            "skin": "sknFlxBgFFFFFFBorE4E6ECsz1pxR3px",
            "top": "20px",
            "width": "100%",
            "zIndex": 1
        }, controller.args[0], "flxBottomDetails"), extendConfig({}, controller.args[1], "flxBottomDetails"), extendConfig({}, controller.args[2], "flxBottomDetails"));
        flxBottomDetails.setDefaultUnit(kony.flex.DP);
        var flxViewTabs = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "clipBounds": true,
            "height": "50px",
            "id": "flxViewTabs",
            "isVisible": true,
            "layoutType": kony.flex.FLOW_HORIZONTAL,
            "left": "0px",
            "isModalContainer": false,
            "right": "0px",
            "skin": "slFbox",
            "top": "0px",
            "zIndex": 4
        }, controller.args[0], "flxViewTabs"), extendConfig({}, controller.args[1], "flxViewTabs"), extendConfig({}, controller.args[2], "flxViewTabs"));
        flxViewTabs.setDefaultUnit(kony.flex.DP);
        var flxTab1 = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "clipBounds": true,
            "height": "100%",
            "id": "flxTab1",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "left": "20dp",
            "isModalContainer": false,
            "skin": "sknFlxffffffCursorPointer",
            "top": "0dp",
            "width": "122dp",
            "zIndex": 1
        }, controller.args[0], "flxTab1"), extendConfig({}, controller.args[1], "flxTab1"), extendConfig({}, controller.args[2], "flxTab1"));
        flxTab1.setDefaultUnit(kony.flex.DP);
        var lblTab1 = new kony.ui.Label(extendConfig({
            "centerX": "50%",
            "centerY": "50%",
            "height": "100%",
            "id": "lblTab1",
            "isVisible": true,
            "skin": "sknLblTabUtilActive",
            "text": "PRODUCT FEATURES",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, controller.args[0], "lblTab1"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_CENTER,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblTab1"), extendConfig({}, controller.args[2], "lblTab1"));
        flxTab1.add(lblTab1);
        var flxTab2 = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "clipBounds": true,
            "height": "100%",
            "id": "flxTab2",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "left": "20dp",
            "isModalContainer": false,
            "skin": "sknFlxffffffCursorPointer",
            "top": "0dp",
            "width": "90dp",
            "zIndex": 1
        }, controller.args[0], "flxTab2"), extendConfig({}, controller.args[1], "flxTab2"), extendConfig({}, controller.args[2], "flxTab2"));
        flxTab2.setDefaultUnit(kony.flex.DP);
        var lblTab2 = new kony.ui.Label(extendConfig({
            "centerY": "50%",
            "height": "100%",
            "id": "lblTab2",
            "isVisible": true,
            "left": "0dp",
            "skin": "sknlblLatoRegular485c7512px",
            "text": "IMAGE DETAILS",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, controller.args[0], "lblTab2"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_CENTER,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblTab2"), extendConfig({}, controller.args[2], "lblTab2"));
        flxTab2.add(lblTab2);
        var flxTab3 = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "clipBounds": true,
            "height": "100%",
            "id": "flxTab3",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "left": "20dp",
            "isModalContainer": false,
            "skin": "sknFlxffffffCursorPointer",
            "top": "0dp",
            "width": "150dp",
            "zIndex": 1
        }, controller.args[0], "flxTab3"), extendConfig({}, controller.args[1], "flxTab3"), extendConfig({}, controller.args[2], "flxTab3"));
        flxTab3.setDefaultUnit(kony.flex.DP);
        var lblTab3 = new kony.ui.Label(extendConfig({
            "centerY": "50%",
            "height": "100%",
            "id": "lblTab3",
            "isVisible": true,
            "left": "0dp",
            "skin": "sknlblLatoRegular485c7512px",
            "text": "ADDITIONAL ATTRIBUTES",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, controller.args[0], "lblTab3"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_CENTER,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblTab3"), extendConfig({}, controller.args[2], "lblTab3"));
        flxTab3.add(lblTab3);
        flxViewTabs.add(flxTab1, flxTab2, flxTab3);
        var lblSeparatorBottomDetails = new kony.ui.Label(extendConfig({
            "centerX": "50%",
            "height": "1px",
            "id": "lblSeparatorBottomDetails",
            "isVisible": true,
            "left": "0px",
            "right": "0px",
            "skin": "sknlblSeperator",
            "text": "Label",
            "top": "50px",
            "zIndex": 2
        }, controller.args[0], "lblSeparatorBottomDetails"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblSeparatorBottomDetails"), extendConfig({}, controller.args[2], "lblSeparatorBottomDetails"));
        var flxProductFeatures = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
            "clipBounds": true,
            "id": "flxProductFeatures",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "left": "0dp",
            "isModalContainer": false,
            "skin": "slFbox",
            "top": "51dp",
            "width": "100%",
            "zIndex": 5
        }, controller.args[0], "flxProductFeatures"), extendConfig({}, controller.args[1], "flxProductFeatures"), extendConfig({}, controller.args[2], "flxProductFeatures"));
        flxProductFeatures.setDefaultUnit(kony.flex.DP);
        var flxProductFeaturesList = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
            "bottom": "20px",
            "clipBounds": true,
            "id": "flxProductFeaturesList",
            "isVisible": true,
            "layoutType": kony.flex.FLOW_VERTICAL,
            "left": "20px",
            "isModalContainer": false,
            "right": "20px",
            "skin": "slFbox",
            "top": "0px",
            "zIndex": 1
        }, controller.args[0], "flxProductFeaturesList"), extendConfig({}, controller.args[1], "flxProductFeaturesList"), extendConfig({}, controller.args[2], "flxProductFeaturesList"));
        flxProductFeaturesList.setDefaultUnit(kony.flex.DP);
        var ViewProductFeatureDetails = new com.adminConsole.Products.ViewProductFeatureDetails(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
            "id": "ViewProductFeatureDetails",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "left": "0px",
            "masterType": constants.MASTER_TYPE_DEFAULT,
            "isModalContainer": false,
            "skin": "slFbox",
            "top": "20px",
            "width": "100%",
            "overrides": {
                "ViewProductFeatureDetails": {
                    "left": "0px",
                    "top": "20px"
                }
            }
        }, controller.args[0], "ViewProductFeatureDetails"), extendConfig({
            "overrides": {}
        }, controller.args[1], "ViewProductFeatureDetails"), extendConfig({
            "overrides": {}
        }, controller.args[2], "ViewProductFeatureDetails"));
        flxProductFeaturesList.add(ViewProductFeatureDetails);
        var flxNoResultFoundPF = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "clipBounds": true,
            "height": "200px",
            "id": "flxNoResultFoundPF",
            "isVisible": false,
            "layoutType": kony.flex.FREE_FORM,
            "left": "0px",
            "isModalContainer": false,
            "skin": "slFbox",
            "top": "0px",
            "width": "100%",
            "zIndex": 1
        }, controller.args[0], "flxNoResultFoundPF"), extendConfig({}, controller.args[1], "flxNoResultFoundPF"), extendConfig({}, controller.args[2], "flxNoResultFoundPF"));
        flxNoResultFoundPF.setDefaultUnit(kony.flex.DP);
        var lblNoResultFoundPF = new kony.ui.Label(extendConfig({
            "centerX": "50%",
            "centerY": "50%",
            "id": "lblNoResultFoundPF",
            "isVisible": true,
            "skin": "sknLbl485C75LatoRegular13Px",
            "text": "No features found.",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, controller.args[0], "lblNoResultFoundPF"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblNoResultFoundPF"), extendConfig({}, controller.args[2], "lblNoResultFoundPF"));
        flxNoResultFoundPF.add(lblNoResultFoundPF);
        flxProductFeatures.add(flxProductFeaturesList, flxNoResultFoundPF);
        var flxImgDetails = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
            "clipBounds": true,
            "id": "flxImgDetails",
            "isVisible": false,
            "layoutType": kony.flex.FREE_FORM,
            "left": "0px",
            "isModalContainer": false,
            "skin": "slFbox",
            "top": "51px",
            "width": "100%",
            "zIndex": 1
        }, controller.args[0], "flxImgDetails"), extendConfig({}, controller.args[1], "flxImgDetails"), extendConfig({}, controller.args[2], "flxImgDetails"));
        flxImgDetails.setDefaultUnit(kony.flex.DP);
        var flxImgDetailsHeaders = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "clipBounds": true,
            "height": "50px",
            "id": "flxImgDetailsHeaders",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "left": "20px",
            "isModalContainer": false,
            "right": "20px",
            "skin": "slFbox",
            "top": "0px",
            "width": kony.flex.USE_PREFFERED_SIZE
        }, controller.args[0], "flxImgDetailsHeaders"), extendConfig({}, controller.args[1], "flxImgDetailsHeaders"), extendConfig({}, controller.args[2], "flxImgDetailsHeaders"));
        flxImgDetailsHeaders.setDefaultUnit(kony.flex.DP);
        var flxImgTypeHeader = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "clipBounds": true,
            "height": "100%",
            "id": "flxImgTypeHeader",
            "isVisible": true,
            "layoutType": kony.flex.FLOW_HORIZONTAL,
            "left": "10px",
            "isModalContainer": false,
            "skin": "slFbox",
            "top": "0px",
            "width": "85px"
        }, controller.args[0], "flxImgTypeHeader"), extendConfig({}, controller.args[1], "flxImgTypeHeader"), extendConfig({}, controller.args[2], "flxImgTypeHeader"));
        flxImgTypeHeader.setDefaultUnit(kony.flex.DP);
        var lblImgTypeHeader = new kony.ui.Label(extendConfig({
            "centerY": "50%",
            "id": "lblImgTypeHeader",
            "isVisible": true,
            "left": "0",
            "skin": "sknlblLato696c7311px",
            "text": "IMAGE TYPE",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, controller.args[0], "lblImgTypeHeader"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblImgTypeHeader"), extendConfig({}, controller.args[2], "lblImgTypeHeader"));
        var fontIconSortImgType = new kony.ui.Label(extendConfig({
            "centerY": "50%",
            "id": "fontIconSortImgType",
            "isVisible": false,
            "left": "7dp",
            "skin": "sknIcon15px",
            "text": "",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, controller.args[0], "fontIconSortImgType"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "fontIconSortImgType"), extendConfig({
            "hoverSkin": "sknlblCursorFont"
        }, controller.args[2], "fontIconSortImgType"));
        flxImgTypeHeader.add(lblImgTypeHeader, fontIconSortImgType);
        var flxImgUrlHeader = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "clipBounds": true,
            "height": "100%",
            "id": "flxImgUrlHeader",
            "isVisible": true,
            "layoutType": kony.flex.FLOW_HORIZONTAL,
            "left": "41%",
            "isModalContainer": false,
            "skin": "slFbox",
            "top": "0px",
            "width": "85px"
        }, controller.args[0], "flxImgUrlHeader"), extendConfig({}, controller.args[1], "flxImgUrlHeader"), extendConfig({}, controller.args[2], "flxImgUrlHeader"));
        flxImgUrlHeader.setDefaultUnit(kony.flex.DP);
        var lblImgUrlHeader = new kony.ui.Label(extendConfig({
            "centerY": "50%",
            "id": "lblImgUrlHeader",
            "isVisible": true,
            "left": "0dp",
            "skin": "sknlblLato696c7311px",
            "text": "IMAGE URL",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, controller.args[0], "lblImgUrlHeader"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblImgUrlHeader"), extendConfig({}, controller.args[2], "lblImgUrlHeader"));
        var fontIconSortImgUrl = new kony.ui.Label(extendConfig({
            "centerY": "50%",
            "id": "fontIconSortImgUrl",
            "isVisible": false,
            "left": "7dp",
            "skin": "sknIcon15px",
            "text": "",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, controller.args[0], "fontIconSortImgUrl"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "fontIconSortImgUrl"), extendConfig({
            "hoverSkin": "sknlblCursorFont"
        }, controller.args[2], "fontIconSortImgUrl"));
        flxImgUrlHeader.add(lblImgUrlHeader, fontIconSortImgUrl);
        var lblSeparatorImgDetailsHeader = new kony.ui.Label(extendConfig({
            "bottom": "1px",
            "height": "1px",
            "id": "lblSeparatorImgDetailsHeader",
            "isVisible": true,
            "left": "0px",
            "right": "0px",
            "skin": "sknLblTableHeaderLine",
            "text": "-",
            "zIndex": 1
        }, controller.args[0], "lblSeparatorImgDetailsHeader"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblSeparatorImgDetailsHeader"), extendConfig({}, controller.args[2], "lblSeparatorImgDetailsHeader"));
        flxImgDetailsHeaders.add(flxImgTypeHeader, flxImgUrlHeader, lblSeparatorImgDetailsHeader);
        var segImgDetails = new kony.ui.SegmentedUI2(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
            "data": [{
                "lblImgType": "Product Line",
                "lblImgUrl": "Product Reference",
                "lblSeparator": "Label"
            }],
            "groupCells": false,
            "id": "segImgDetails",
            "isVisible": true,
            "left": "20px",
            "needPageIndicator": true,
            "pageOffDotImage": "pageoffdot.png",
            "pageOnDotImage": "pageondot.png",
            "retainSelection": false,
            "right": "20px",
            "rowFocusSkin": "seg2Focus",
            "rowSkin": "seg2Normal",
            "rowTemplate": "flxViewImgDetails",
            "sectionHeaderSkin": "sliPhoneSegmentHeader",
            "selectionBehavior": constants.SEGUI_DEFAULT_BEHAVIOR,
            "separatorColor": "d7d9e000",
            "separatorRequired": true,
            "showScrollbars": false,
            "top": "51px",
            "viewType": constants.SEGUI_VIEW_TYPE_TABLEVIEW,
            "widgetDataMap": {
                "flxViewImgDetails": "flxViewImgDetails",
                "lblImgType": "lblImgType",
                "lblImgUrl": "lblImgUrl",
                "lblSeparator": "lblSeparator"
            },
            "zIndex": 1
        }, controller.args[0], "segImgDetails"), extendConfig({
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "segImgDetails"), extendConfig({}, controller.args[2], "segImgDetails"));
        var flxNoResultFoundImgDetails = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "clipBounds": true,
            "height": "200px",
            "id": "flxNoResultFoundImgDetails",
            "isVisible": false,
            "layoutType": kony.flex.FREE_FORM,
            "left": "0px",
            "isModalContainer": false,
            "skin": "slFbox",
            "top": "51px",
            "width": "100%",
            "zIndex": 1
        }, controller.args[0], "flxNoResultFoundImgDetails"), extendConfig({}, controller.args[1], "flxNoResultFoundImgDetails"), extendConfig({}, controller.args[2], "flxNoResultFoundImgDetails"));
        flxNoResultFoundImgDetails.setDefaultUnit(kony.flex.DP);
        var lblNoResultFoundImgDetails = new kony.ui.Label(extendConfig({
            "centerX": "50%",
            "centerY": "50%",
            "id": "lblNoResultFoundImgDetails",
            "isVisible": true,
            "skin": "sknLbl485C75LatoRegular13Px",
            "text": "No images found.",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, controller.args[0], "lblNoResultFoundImgDetails"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblNoResultFoundImgDetails"), extendConfig({}, controller.args[2], "lblNoResultFoundImgDetails"));
        flxNoResultFoundImgDetails.add(lblNoResultFoundImgDetails);
        flxImgDetails.add(flxImgDetailsHeaders, segImgDetails, flxNoResultFoundImgDetails);
        var flxAdditionalAttributes = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
            "clipBounds": true,
            "id": "flxAdditionalAttributes",
            "isVisible": false,
            "layoutType": kony.flex.FREE_FORM,
            "left": "0dp",
            "isModalContainer": false,
            "skin": "slFbox",
            "top": "51dp",
            "width": "100%",
            "zIndex": 1
        }, controller.args[0], "flxAdditionalAttributes"), extendConfig({}, controller.args[1], "flxAdditionalAttributes"), extendConfig({}, controller.args[2], "flxAdditionalAttributes"));
        flxAdditionalAttributes.setDefaultUnit(kony.flex.DP);
        var flxAdditionalAttributesHeaders = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "clipBounds": true,
            "height": "51px",
            "id": "flxAdditionalAttributesHeaders",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "left": "20px",
            "isModalContainer": false,
            "right": "20px",
            "skin": "slFbox",
            "top": "0px",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, controller.args[0], "flxAdditionalAttributesHeaders"), extendConfig({}, controller.args[1], "flxAdditionalAttributesHeaders"), extendConfig({}, controller.args[2], "flxAdditionalAttributesHeaders"));
        flxAdditionalAttributesHeaders.setDefaultUnit(kony.flex.DP);
        var lblAttribute1Header = new kony.ui.Label(extendConfig({
            "centerY": "50%",
            "id": "lblAttribute1Header",
            "isVisible": true,
            "left": "12px",
            "skin": "sknlblLato696c7311px",
            "text": "ATTRIBUTE1",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, controller.args[0], "lblAttribute1Header"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblAttribute1Header"), extendConfig({}, controller.args[2], "lblAttribute1Header"));
        var lblAttribute2Header = new kony.ui.Label(extendConfig({
            "centerY": "50%",
            "id": "lblAttribute2Header",
            "isVisible": true,
            "left": "41%",
            "skin": "sknlblLato696c7311px",
            "text": "ATTRIBUTE2",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, controller.args[0], "lblAttribute2Header"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblAttribute2Header"), extendConfig({}, controller.args[2], "lblAttribute2Header"));
        var lblSeparatorAttributeHeader = new kony.ui.Label(extendConfig({
            "bottom": "1px",
            "height": "1px",
            "id": "lblSeparatorAttributeHeader",
            "isVisible": true,
            "left": "0px",
            "right": "0px",
            "skin": "sknLblTableHeaderLine",
            "text": "Label",
            "zIndex": 2
        }, controller.args[0], "lblSeparatorAttributeHeader"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_TOP_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblSeparatorAttributeHeader"), extendConfig({}, controller.args[2], "lblSeparatorAttributeHeader"));
        flxAdditionalAttributesHeaders.add(lblAttribute1Header, lblAttribute2Header, lblSeparatorAttributeHeader);
        var segAdditionalAttributes = new kony.ui.SegmentedUI2(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
            "data": [{
                "lblImgType": "Product Line",
                "lblImgUrl": "Product Reference",
                "lblSeparator": "Label"
            }, {
                "lblImgType": "Product Line",
                "lblImgUrl": "Product Reference",
                "lblSeparator": "Label"
            }, {
                "lblImgType": "Product Line",
                "lblImgUrl": "Product Reference",
                "lblSeparator": "Label"
            }],
            "groupCells": false,
            "id": "segAdditionalAttributes",
            "isVisible": true,
            "left": "20px",
            "needPageIndicator": true,
            "pageOffDotImage": "pageoffdot.png",
            "pageOnDotImage": "pageondot.png",
            "retainSelection": false,
            "right": "20px",
            "rowFocusSkin": "seg2Focus",
            "rowSkin": "sknseg3pxRadius",
            "rowTemplate": "flxViewImgDetails",
            "sectionHeaderSkin": "sliPhoneSegmentHeader",
            "selectionBehavior": constants.SEGUI_DEFAULT_BEHAVIOR,
            "separatorRequired": false,
            "showScrollbars": false,
            "top": "51px",
            "viewType": constants.SEGUI_VIEW_TYPE_TABLEVIEW,
            "widgetDataMap": {
                "flxViewImgDetails": "flxViewImgDetails",
                "lblImgType": "lblImgType",
                "lblImgUrl": "lblImgUrl",
                "lblSeparator": "lblSeparator"
            },
            "zIndex": 1
        }, controller.args[0], "segAdditionalAttributes"), extendConfig({
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "segAdditionalAttributes"), extendConfig({}, controller.args[2], "segAdditionalAttributes"));
        var flxNoResultFoundAdditionalAttributes = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "clipBounds": true,
            "height": "200px",
            "id": "flxNoResultFoundAdditionalAttributes",
            "isVisible": false,
            "layoutType": kony.flex.FREE_FORM,
            "left": "0px",
            "isModalContainer": false,
            "skin": "slFbox",
            "top": "51px",
            "width": "100%",
            "zIndex": 1
        }, controller.args[0], "flxNoResultFoundAdditionalAttributes"), extendConfig({}, controller.args[1], "flxNoResultFoundAdditionalAttributes"), extendConfig({}, controller.args[2], "flxNoResultFoundAdditionalAttributes"));
        flxNoResultFoundAdditionalAttributes.setDefaultUnit(kony.flex.DP);
        var lblNoResultFoundAdditionalAttributes = new kony.ui.Label(extendConfig({
            "centerX": "50%",
            "centerY": "50%",
            "id": "lblNoResultFoundAdditionalAttributes",
            "isVisible": true,
            "skin": "sknLbl485C75LatoRegular13Px",
            "text": "No additional attributes found.",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, controller.args[0], "lblNoResultFoundAdditionalAttributes"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblNoResultFoundAdditionalAttributes"), extendConfig({}, controller.args[2], "lblNoResultFoundAdditionalAttributes"));
        flxNoResultFoundAdditionalAttributes.add(lblNoResultFoundAdditionalAttributes);
        flxAdditionalAttributes.add(flxAdditionalAttributesHeaders, segAdditionalAttributes, flxNoResultFoundAdditionalAttributes);
        flxBottomDetails.add(flxViewTabs, lblSeparatorBottomDetails, flxProductFeatures, flxImgDetails, flxAdditionalAttributes);
        flxViewDetails.add(flxTopDetails, flxBottomDetails);
        flxProductDetails.add(flxViewDetails);
        var flxSelectOptions = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
            "clipBounds": false,
            "id": "flxSelectOptions",
            "isVisible": false,
            "layoutType": kony.flex.FLOW_VERTICAL,
            "left": "760px",
            "isModalContainer": false,
            "skin": "slFbox",
            "top": "100px",
            "width": "140px",
            "zIndex": 100
        }, controller.args[0], "flxSelectOptions"), extendConfig({}, controller.args[1], "flxSelectOptions"), extendConfig({}, controller.args[2], "flxSelectOptions"));
        flxSelectOptions.setDefaultUnit(kony.flex.DP);
        var selectOptions = new com.adminConsole.adManagement.selectOptions(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
            "id": "selectOptions",
            "isVisible": true,
            "layoutType": kony.flex.FLOW_VERTICAL,
            "left": "0dp",
            "masterType": constants.MASTER_TYPE_DEFAULT,
            "isModalContainer": false,
            "skin": "slFbox",
            "top": "0dp",
            "width": "100%",
            "overrides": {
                "flxDownArrowImage": {
                    "isVisible": false
                },
                "flxOption3": {
                    "isVisible": false
                },
                "flxOption4": {
                    "isVisible": false
                },
                "fontIconOption2": {
                    "i18n_text": "kony.i18n.getLocalizedString(\"i18n.userwidgetmodel.lblIconOption2\")"
                },
                "fontIconOption3": {
                    "i18n_text": "kony.i18n.getLocalizedString(\"i18n.userwidgetmodel.lblIconOption2\")"
                },
                "imgDownArrow": {
                    "src": "downarrow_2x.png"
                },
                "imgUpArrow": {
                    "src": "uparrow_2x.png"
                },
                "lblOption2": {
                    "i18n_text": "kony.i18n.getLocalizedString(\"i18n.common.delete\")",
                    "width": "63px"
                },
                "lblOption3": {
                    "i18n_text": "kony.i18n.getLocalizedString(\"i18n.common.delete\")"
                },
                "lblOption4": {
                    "i18n_text": "kony.i18n.getLocalizedString(\"i18n.ProfileManagement.copySegment\")"
                },
                "lblSeperator": {
                    "isVisible": false
                }
            }
        }, controller.args[0], "selectOptions"), extendConfig({
            "overrides": {}
        }, controller.args[1], "selectOptions"), extendConfig({
            "overrides": {}
        }, controller.args[2], "selectOptions"));
        flxSelectOptions.add(selectOptions);
        flxViewProducts.add(flxProducts, flxProductDetails, flxSelectOptions);
        var flxPopup = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "clipBounds": true,
            "height": "100%",
            "id": "flxPopup",
            "isVisible": false,
            "layoutType": kony.flex.FREE_FORM,
            "left": "0px",
            "masterType": constants.MASTER_TYPE_USERWIDGET,
            "isModalContainer": false,
            "skin": "skn222b35",
            "top": "0px",
            "width": "100%",
            "zIndex": 10
        }, controller.args[0], "flxPopup"), extendConfig({}, controller.args[1], "flxPopup"), extendConfig({}, controller.args[2], "flxPopup"));
        flxPopup.setDefaultUnit(kony.flex.DP);
        var flxReadMorePopup = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "centerY": "50%",
            "clipBounds": false,
            "height": "90%",
            "id": "flxReadMorePopup",
            "isVisible": false,
            "layoutType": kony.flex.FREE_FORM,
            "left": "14%",
            "isModalContainer": false,
            "right": "14%",
            "skin": "flxNoShadowNoBorder",
            "zIndex": 10
        }, controller.args[0], "flxReadMorePopup"), extendConfig({}, controller.args[1], "flxReadMorePopup"), extendConfig({}, controller.args[2], "flxReadMorePopup"));
        flxReadMorePopup.setDefaultUnit(kony.flex.DP);
        var flxReadMoreTopBar = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "clipBounds": true,
            "height": "10px",
            "id": "flxReadMoreTopBar",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "left": "0px",
            "isModalContainer": false,
            "skin": "sknFlxInfoToastBg357C9ENoRadius",
            "top": "0px",
            "width": "100%",
            "zIndex": 1
        }, controller.args[0], "flxReadMoreTopBar"), extendConfig({}, controller.args[1], "flxReadMoreTopBar"), extendConfig({}, controller.args[2], "flxReadMoreTopBar"));
        flxReadMoreTopBar.setDefaultUnit(kony.flex.DP);
        flxReadMoreTopBar.add();
        var flxCloseReadMore = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "clipBounds": true,
            "height": "15px",
            "id": "flxCloseReadMore",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "isModalContainer": false,
            "right": 20,
            "skin": "hoverhandSkin",
            "top": "21dp",
            "width": "15px",
            "zIndex": 1
        }, controller.args[0], "flxCloseReadMore"), extendConfig({}, controller.args[1], "flxCloseReadMore"), extendConfig({
            "hoverSkin": "sknCursor"
        }, controller.args[2], "flxCloseReadMore"));
        flxCloseReadMore.setDefaultUnit(kony.flex.DP);
        var fontIconCloseReadMore = new kony.ui.Label(extendConfig({
            "height": "15dp",
            "id": "fontIconCloseReadMore",
            "isVisible": true,
            "left": "0dp",
            "skin": "sknfontIconListbox15px",
            "i18n_text": "kony.i18n.getLocalizedString(\"i18n.userwidgetmodel.lblClearSearch\")",
            "top": "0dp",
            "width": "15dp",
            "zIndex": 1
        }, controller.args[0], "fontIconCloseReadMore"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "fontIconCloseReadMore"), extendConfig({}, controller.args[2], "fontIconCloseReadMore"));
        flxCloseReadMore.add(fontIconCloseReadMore);
        var lbReadMoreTopHeader = new kony.ui.Label(extendConfig({
            "height": "19dp",
            "id": "lbReadMoreTopHeader",
            "isVisible": true,
            "left": "20px",
            "skin": "sknLbl192b45LatoReg16px",
            "text": "Read More",
            "top": "45px",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, controller.args[0], "lbReadMoreTopHeader"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lbReadMoreTopHeader"), extendConfig({}, controller.args[2], "lbReadMoreTopHeader"));
        var lblSeparatorReadMoreHeader = new kony.ui.Label(extendConfig({
            "height": "1px",
            "id": "lblSeparatorReadMoreHeader",
            "isVisible": true,
            "left": "20px",
            "right": "20px",
            "skin": "sknlblSeperatorD7D9E0",
            "text": "Label",
            "top": "80px",
            "zIndex": 2
        }, controller.args[0], "lblSeparatorReadMoreHeader"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_TOP_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblSeparatorReadMoreHeader"), extendConfig({}, controller.args[2], "lblSeparatorReadMoreHeader"));
        var lblAgreementTerms = new kony.ui.Label(extendConfig({
            "height": "19px",
            "id": "lblAgreementTerms",
            "isVisible": true,
            "left": "20px",
            "skin": "sknLatoRegular192B4518px",
            "text": "AGREEMENT TO TERMS",
            "top": "105px",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, controller.args[0], "lblAgreementTerms"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblAgreementTerms"), extendConfig({}, controller.args[2], "lblAgreementTerms"));
        var flxReadMoreDetails = new kony.ui.FlexScrollContainer(extendConfig({
            "allowHorizontalBounce": false,
            "allowVerticalBounce": true,
            "bottom": "100px",
            "bounces": true,
            "clipBounds": true,
            "enableScrolling": true,
            "horizontalScrollIndicator": true,
            "id": "flxReadMoreDetails",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "left": "0px",
            "pagingEnabled": false,
            "scrollDirection": kony.flex.SCROLL_VERTICAL,
            "skin": "slFSbox",
            "top": "145px",
            "verticalScrollIndicator": true,
            "width": "100%"
        }, controller.args[0], "flxReadMoreDetails"), extendConfig({}, controller.args[1], "flxReadMoreDetails"), extendConfig({}, controller.args[2], "flxReadMoreDetails"));
        flxReadMoreDetails.setDefaultUnit(kony.flex.DP);
        var lblReadMoreDetails = new kony.ui.Label(extendConfig({
            "id": "lblReadMoreDetails",
            "isVisible": true,
            "left": "20px",
            "right": "20px",
            "skin": "sknLatoRegular192B4518px",
            "text": "These Terms and Conditions constitute a legally binding agreement made between you, whether personally or on behalf of an entity (“you”) and [business entity name] (“we,” “us” or “our”), concerning your access to and use of the [website name.com] website as well as any other media form, media channel, mobile website or mobile application related, linked, or otherwise connected thereto (collectively, the “Site”).  You agree that by accessing the Site, you have read, understood, and agree to be bound by all of these Terms and Conditions. If you do not agree with all of these Terms and Conditions, then you are expressly prohibited from using the Site and you must discontinue use immediately.  Supplemental terms and conditions or documents that may be posted on the Site from time to time are hereby expressly incorporated herein by reference. We reserve the right, in our sole discretion, to make changes or modifications to these Terms and Conditions at any time and for any reason.  We will alert you about any changes by updating the “Last updated” date of these Terms and Conditions, and you waive any right to receive specific notice of each such change.  It is your responsibility to periodically review these Terms and Conditions to stay informed of updates. You will be subject to, and will be deemed to have been made aware of and to have accepted, the changes in any revised Terms and Conditions by your continued use of the Site after the date such revised Terms and Conditions are posted. It is your responsibility to periodically review these Terms and Conditions to stay informed of updates. You will be subject to, and will be deemed to have been made aware of and to have accepted, the changes in any revised Terms and Conditions by your continued use of the Site after the date such revised Terms and Conditions are posted.It is your responsibility to periodically review these Terms and Conditions to stay informed of updates. You will be subject to, and will be deemed to have been made aware of and to have accepted, the changes in any revised Terms and Conditions by your continued use of the Site after the date such revised Terms and Conditions are posted.  AGREEMENT TO TERMS  These Terms and Conditions constitute a legally binding agreement made between you, whether personally or on behalf of an entity (“you”) and [business entity name] (“we,” “us” or “our”), concerning your access to and use of the [website name.com] website as well as any other media form, media channel, mobile website or mobile application related, linked, or otherwise connected thereto (collectively, the “Site”).  You agree that by accessing the Site, you have read, understood, and agree to be bound by all of these Terms and Conditions. If you do not agree with all of these Terms and Conditions, then you are expressly prohibited from using the Site and you must discontinue use immediately.  Supplemental terms and conditions or documents that may be posted on the Site from time to time are hereby expressly incorporated herein by reference. We reserve the right, in our sole discretion, to make changes or modifications to these Terms and Conditions at any time and for any reason.  We will alert you about any changes by updating the “Last updated” date of these Terms and Conditions, and you waive any right to receive specific notice of each such change.  It is your responsibility to periodically review these Terms and Conditions to stay informed of updates. You will be subject to, and will be deemed to have been made aware of and to have accepted, the changes in any revised Terms and Conditions by your continued use of the Site after the date such revised Terms and Conditions are posted. It is your responsibility to periodically review these Terms and Conditions to stay informed of updates. You will be subject to, and will be deemed to have been made aware of and to have accepted, the changes in any revised Terms and Conditions by your continued use of the Site after the date such revised Terms and Conditions are posted.It is your responsibility to periodically review these Terms and Conditions to stay informed of updates. You will be subject to, and will be deemed to have been made aware of and to have accepted, the changes in any revised Terms and Conditions by your continued use of the Site after the date such revised Terms and Conditions are posted.",
            "top": "0px",
            "zIndex": 1
        }, controller.args[0], "lblReadMoreDetails"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblReadMoreDetails"), extendConfig({}, controller.args[2], "lblReadMoreDetails"));
        flxReadMoreDetails.add(lblReadMoreDetails);
        var flxReadMorePopupButtons = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "bottom": "0dp",
            "clipBounds": true,
            "height": "80px",
            "id": "flxReadMorePopupButtons",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "left": "0dp",
            "isModalContainer": false,
            "skin": "sknflxBgE4E6EC",
            "width": "100%",
            "zIndex": 1
        }, controller.args[0], "flxReadMorePopupButtons"), extendConfig({}, controller.args[1], "flxReadMorePopupButtons"), extendConfig({}, controller.args[2], "flxReadMorePopupButtons"));
        flxReadMorePopupButtons.setDefaultUnit(kony.flex.DP);
        var btnCloseReadMore = new kony.ui.Button(extendConfig({
            "centerY": "50%",
            "focusSkin": "sknBtn003E75LatoRegular13pxFFFFFFRad20px",
            "height": "40px",
            "id": "btnCloseReadMore",
            "isVisible": true,
            "minWidth": "100px",
            "right": "20px",
            "skin": "sknBtn003E75LatoRegular13pxFFFFFFRad20px",
            "text": "CLOSE",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 20
        }, controller.args[0], "btnCloseReadMore"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_CENTER,
            "displayText": true,
            "padding": [3, 0, 3, 0],
            "paddingInPixel": false
        }, controller.args[1], "btnCloseReadMore"), extendConfig({
            "hoverSkin": "sknBtn005198LatoRegular13pxFFFFFFRad20px"
        }, controller.args[2], "btnCloseReadMore"));
        flxReadMorePopupButtons.add(btnCloseReadMore);
        flxReadMorePopup.add(flxReadMoreTopBar, flxCloseReadMore, lbReadMoreTopHeader, lblSeparatorReadMoreHeader, lblAgreementTerms, flxReadMoreDetails, flxReadMorePopupButtons);
        var popUp = new com.adminConsole.common.popUp(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
            "centerX": "50%",
            "centerY": "50%",
            "id": "popUp",
            "isVisible": false,
            "layoutType": kony.flex.FREE_FORM,
            "masterType": constants.MASTER_TYPE_DEFAULT,
            "isModalContainer": false,
            "skin": "sknflxBg000000Op50",
            "width": "680px",
            "zIndex": 1,
            "overrides": {
                "btnPopUpCancel": {
                    "i18n_text": "kony.i18n.getLocalizedString(\"i18n.PopUp.NoLeaveAsIS\")",
                    "left": "viz.val_cleared",
                    "minWidth": "100px",
                    "right": "20dp",
                    "top": "0%",
                    "width": kony.flex.USE_PREFFERED_SIZE
                },
                "btnPopUpDelete": {
                    "i18n_text": "kony.i18n.getLocalizedString(\"i18n.PopUp.YesProceed\")"
                },
                "flxPopUp": {
                    "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                    "centerX": "viz.val_cleared",
                    "left": "0px",
                    "top": "0px",
                    "width": "100%"
                },
                "lblPopUpMainMessage": {
                    "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmAdManagement.ReduceAdPlaceholders\")"
                },
                "popUp": {
                    "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                    "centerX": "50%",
                    "centerY": "50%",
                    "isVisible": false,
                    "left": "viz.val_cleared",
                    "top": "viz.val_cleared",
                    "width": "680px"
                },
                "rtxPopUpDisclaimer": {
                    "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmAdManagement.ReduceAdPlaceholdersMsg\")",
                    "width": "640dp"
                }
            }
        }, controller.args[0], "popUp"), extendConfig({
            "overrides": {}
        }, controller.args[1], "popUp"), extendConfig({
            "overrides": {}
        }, controller.args[2], "popUp"));
        flxPopup.add(flxReadMorePopup, popUp);
        var flxDummyToOpenPopup = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "clipBounds": true,
            "height": "100%",
            "id": "flxDummyToOpenPopup",
            "isVisible": false,
            "layoutType": kony.flex.FREE_FORM,
            "left": "0dp",
            "isModalContainer": false,
            "skin": "slFbox",
            "top": "0dp",
            "width": "100%",
            "zIndex": 1
        }, controller.args[0], "flxDummyToOpenPopup"), extendConfig({}, controller.args[1], "flxDummyToOpenPopup"), extendConfig({}, controller.args[2], "flxDummyToOpenPopup"));
        flxDummyToOpenPopup.setDefaultUnit(kony.flex.DP);
        flxDummyToOpenPopup.add();
        viewProducts.add(flxViewProducts, flxPopup, flxDummyToOpenPopup);
        return viewProducts;
    }
})