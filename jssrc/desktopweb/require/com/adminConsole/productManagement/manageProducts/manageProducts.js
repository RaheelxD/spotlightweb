define(function() {
    return function(controller) {
        var manageProducts = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "clipBounds": true,
            "isMaster": true,
            "height": "100%",
            "id": "manageProducts",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "left": "0px",
            "isModalContainer": false,
            "preShow": function(eventobject) {
                controller.AS_FlexContainer_a715ce69303f4f219318b08c81fcb0f0(eventobject);
            },
            "skin": "slFbox",
            "top": "0px",
            "width": "100%"
        }, controller.args[0], "manageProducts"), extendConfig({}, controller.args[1], "manageProducts"), extendConfig({}, controller.args[2], "manageProducts"));
        manageProducts.setDefaultUnit(kony.flex.DP);
        var flxManageProducts = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "bottom": "0px",
            "clipBounds": true,
            "id": "flxManageProducts",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "left": "0px",
            "isModalContainer": false,
            "right": "0px",
            "skin": "sknFlxBgFFFFFFBorE4E6ECsz1pxR3px",
            "top": "0px",
            "zIndex": 1
        }, controller.args[0], "flxManageProducts"), extendConfig({}, controller.args[1], "flxManageProducts"), extendConfig({}, controller.args[2], "flxManageProducts"));
        flxManageProducts.setDefaultUnit(kony.flex.DP);
        var flxProductTabs = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "clipBounds": true,
            "height": "45px",
            "id": "flxProductTabs",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "left": "0px",
            "isModalContainer": false,
            "right": "0px",
            "skin": "slFbox",
            "top": "0px",
            "zIndex": 4
        }, controller.args[0], "flxProductTabs"), extendConfig({}, controller.args[1], "flxProductTabs"), extendConfig({}, controller.args[2], "flxProductTabs"));
        flxProductTabs.setDefaultUnit(kony.flex.DP);
        var flxTabProductLine = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "clipBounds": true,
            "height": "100%",
            "id": "flxTabProductLine",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "left": "0dp",
            "isModalContainer": false,
            "skin": "sknBorderTop006CCA3px",
            "top": "0dp",
            "width": "127dp",
            "zIndex": 1
        }, controller.args[0], "flxTabProductLine"), extendConfig({}, controller.args[1], "flxTabProductLine"), extendConfig({}, controller.args[2], "flxTabProductLine"));
        flxTabProductLine.setDefaultUnit(kony.flex.DP);
        var flxTabProductLineInner = new kony.ui.FlexContainer(extendConfig({
            "clipBounds": true,
            "height": "100%",
            "id": "flxTabProductLineInner",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "left": "0dp",
            "isModalContainer": false,
            "skin": "hoverhandSkin",
            "top": "0dp",
            "width": "100%"
        }, controller.args[0], "flxTabProductLineInner"), extendConfig({}, controller.args[1], "flxTabProductLineInner"), extendConfig({}, controller.args[2], "flxTabProductLineInner"));
        flxTabProductLineInner.setDefaultUnit(kony.flex.DP);
        var lblProductLineCount = new kony.ui.Label(extendConfig({
            "centerX": "50%",
            "id": "lblProductLineCount",
            "isVisible": true,
            "skin": "sknLbl485c75LatoBold13px",
            "text": "0",
            "top": "9px",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, controller.args[0], "lblProductLineCount"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblProductLineCount"), extendConfig({}, controller.args[2], "lblProductLineCount"));
        var lblProductLineTab = new kony.ui.Label(extendConfig({
            "centerX": "50%",
            "id": "lblProductLineTab",
            "isVisible": true,
            "skin": "skn485C75LatoRegular11Px",
            "text": "Product Lines",
            "top": "25px",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, controller.args[0], "lblProductLineTab"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_CENTER,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblProductLineTab"), extendConfig({}, controller.args[2], "lblProductLineTab"));
        flxTabProductLineInner.add(lblProductLineCount, lblProductLineTab);
        flxTabProductLine.add(flxTabProductLineInner);
        var flxTabProductGroup = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "clipBounds": true,
            "height": "100%",
            "id": "flxTabProductGroup",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "left": "126dp",
            "isModalContainer": false,
            "skin": "sknBorderE4E6EC1px",
            "top": "0dp",
            "width": "138dp",
            "zIndex": 1
        }, controller.args[0], "flxTabProductGroup"), extendConfig({}, controller.args[1], "flxTabProductGroup"), extendConfig({}, controller.args[2], "flxTabProductGroup"));
        flxTabProductGroup.setDefaultUnit(kony.flex.DP);
        var flxTabProductGroupInner = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "clipBounds": true,
            "height": "100%",
            "id": "flxTabProductGroupInner",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "left": "0dp",
            "isModalContainer": false,
            "skin": "hoverhandSkin",
            "top": "0dp",
            "width": "100%"
        }, controller.args[0], "flxTabProductGroupInner"), extendConfig({}, controller.args[1], "flxTabProductGroupInner"), extendConfig({}, controller.args[2], "flxTabProductGroupInner"));
        flxTabProductGroupInner.setDefaultUnit(kony.flex.DP);
        var lblProductGroupCount = new kony.ui.Label(extendConfig({
            "centerX": "50%",
            "id": "lblProductGroupCount",
            "isVisible": true,
            "skin": "sknLbl485c75LatoBold13px",
            "text": "0",
            "top": "9px",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, controller.args[0], "lblProductGroupCount"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblProductGroupCount"), extendConfig({}, controller.args[2], "lblProductGroupCount"));
        var lblProductGroupTab = new kony.ui.Label(extendConfig({
            "centerX": "50%",
            "id": "lblProductGroupTab",
            "isVisible": true,
            "skin": "skn485C75LatoRegular11Px",
            "text": "Product Groups",
            "top": "25px",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, controller.args[0], "lblProductGroupTab"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_CENTER,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblProductGroupTab"), extendConfig({}, controller.args[2], "lblProductGroupTab"));
        flxTabProductGroupInner.add(lblProductGroupCount, lblProductGroupTab);
        flxTabProductGroup.add(flxTabProductGroupInner);
        var flxTabProductFeature = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "clipBounds": true,
            "height": "100%",
            "id": "flxTabProductFeature",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "left": "263dp",
            "isModalContainer": false,
            "skin": "sknBorderE4E6EC1px",
            "top": "0dp",
            "width": "138dp",
            "zIndex": 1
        }, controller.args[0], "flxTabProductFeature"), extendConfig({}, controller.args[1], "flxTabProductFeature"), extendConfig({}, controller.args[2], "flxTabProductFeature"));
        flxTabProductFeature.setDefaultUnit(kony.flex.DP);
        var flxTabProductFeatureInner = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "clipBounds": true,
            "height": "100%",
            "id": "flxTabProductFeatureInner",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "left": "0dp",
            "isModalContainer": false,
            "skin": "hoverhandSkin",
            "top": "0dp",
            "width": "100%"
        }, controller.args[0], "flxTabProductFeatureInner"), extendConfig({}, controller.args[1], "flxTabProductFeatureInner"), extendConfig({}, controller.args[2], "flxTabProductFeatureInner"));
        flxTabProductFeatureInner.setDefaultUnit(kony.flex.DP);
        var lblProductFeatureCount = new kony.ui.Label(extendConfig({
            "centerX": "50%",
            "id": "lblProductFeatureCount",
            "isVisible": true,
            "skin": "sknLbl485c75LatoBold13px",
            "text": "0",
            "top": "9px",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, controller.args[0], "lblProductFeatureCount"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblProductFeatureCount"), extendConfig({}, controller.args[2], "lblProductFeatureCount"));
        var lblProductFeatureTab = new kony.ui.Label(extendConfig({
            "centerX": "50%",
            "id": "lblProductFeatureTab",
            "isVisible": true,
            "skin": "skn485C75LatoRegular11Px",
            "text": "Product Features",
            "top": "25px",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, controller.args[0], "lblProductFeatureTab"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_CENTER,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblProductFeatureTab"), extendConfig({}, controller.args[2], "lblProductFeatureTab"));
        flxTabProductFeatureInner.add(lblProductFeatureCount, lblProductFeatureTab);
        flxTabProductFeature.add(flxTabProductFeatureInner);
        var flxSeparatorTabs = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "bottom": "0px",
            "clipBounds": true,
            "height": "1px",
            "id": "flxSeparatorTabs",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "left": "400px",
            "isModalContainer": false,
            "right": "0px",
            "skin": "sknflxSepratorE1E5ED",
            "zIndex": 1
        }, controller.args[0], "flxSeparatorTabs"), extendConfig({}, controller.args[1], "flxSeparatorTabs"), extendConfig({}, controller.args[2], "flxSeparatorTabs"));
        flxSeparatorTabs.setDefaultUnit(kony.flex.DP);
        flxSeparatorTabs.add();
        flxProductTabs.add(flxTabProductLine, flxTabProductGroup, flxTabProductFeature, flxSeparatorTabs);
        var flxManageProductLine = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "bottom": "0px",
            "clipBounds": true,
            "id": "flxManageProductLine",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "left": "0px",
            "isModalContainer": false,
            "skin": "slFbox",
            "top": "45px",
            "width": "100%",
            "zIndex": 4
        }, controller.args[0], "flxManageProductLine"), extendConfig({}, controller.args[1], "flxManageProductLine"), extendConfig({}, controller.args[2], "flxManageProductLine"));
        flxManageProductLine.setDefaultUnit(kony.flex.DP);
        var lblProductLineTopHeader = new kony.ui.Label(extendConfig({
            "id": "lblProductLineTopHeader",
            "isVisible": true,
            "left": "20px",
            "skin": "sknLatoRegular192B4516px",
            "text": "Product Lines",
            "top": "20px",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, controller.args[0], "lblProductLineTopHeader"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblProductLineTopHeader"), extendConfig({}, controller.args[2], "lblProductLineTopHeader"));
        var btnAddProductLines = new kony.ui.Button(extendConfig({
            "focusSkin": "sknbtnffffffLatoRegular4f555dBorder1px485c75",
            "height": "22px",
            "id": "btnAddProductLines",
            "isVisible": true,
            "right": "20px",
            "skin": "sknbtnf7f7faLatoReg13px485c75Rad20px",
            "text": "Add Product Line",
            "top": "20px",
            "width": "121px",
            "zIndex": 1
        }, controller.args[0], "btnAddProductLines"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_CENTER,
            "displayText": true,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "btnAddProductLines"), extendConfig({
            "hoverSkin": "sknbtnffffffLatoRegular4f555dBorder1px485c75"
        }, controller.args[2], "btnAddProductLines"));
        var lblSeparatorSearchPL = new kony.ui.Label(extendConfig({
            "height": "1px",
            "id": "lblSeparatorSearchPL",
            "isVisible": true,
            "left": "20px",
            "right": "20px",
            "skin": "sknlblSeperatorD7D9E0",
            "text": "Label",
            "top": "119px",
            "zIndex": 2
        }, controller.args[0], "lblSeparatorSearchPL"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_TOP_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblSeparatorSearchPL"), extendConfig({}, controller.args[2], "lblSeparatorSearchPL"));
        var flxProductLineSegHeaders = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "clipBounds": true,
            "height": "52px",
            "id": "flxProductLineSegHeaders",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "left": "20px",
            "isModalContainer": false,
            "right": "20px",
            "skin": "slFbox",
            "top": "120px",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, controller.args[0], "flxProductLineSegHeaders"), extendConfig({}, controller.args[1], "flxProductLineSegHeaders"), extendConfig({}, controller.args[2], "flxProductLineSegHeaders"));
        flxProductLineSegHeaders.setDefaultUnit(kony.flex.DP);
        var flxProductLineHeader = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
            "centerY": "50%",
            "clipBounds": true,
            "id": "flxProductLineHeader",
            "isVisible": true,
            "layoutType": kony.flex.FLOW_HORIZONTAL,
            "left": "10px",
            "isModalContainer": false,
            "skin": "slFbox",
            "width": "110px"
        }, controller.args[0], "flxProductLineHeader"), extendConfig({}, controller.args[1], "flxProductLineHeader"), extendConfig({}, controller.args[2], "flxProductLineHeader"));
        flxProductLineHeader.setDefaultUnit(kony.flex.DP);
        var lblProductLineHeader = new kony.ui.Label(extendConfig({
            "id": "lblProductLineHeader",
            "isVisible": true,
            "left": "0px",
            "skin": "sknlblLato696c7311px",
            "text": "PRODUCT LINES",
            "top": "1px",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, controller.args[0], "lblProductLineHeader"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblProductLineHeader"), extendConfig({}, controller.args[2], "lblProductLineHeader"));
        var fontIconOptionsProductLine = new kony.ui.Label(extendConfig({
            "id": "fontIconOptionsProductLine",
            "isVisible": true,
            "left": "5px",
            "skin": "sknIcon15px",
            "text": "",
            "top": "0px",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, controller.args[0], "fontIconOptionsProductLine"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "fontIconOptionsProductLine"), extendConfig({
            "hoverSkin": "sknlblCursorFont"
        }, controller.args[2], "fontIconOptionsProductLine"));
        flxProductLineHeader.add(lblProductLineHeader, fontIconOptionsProductLine);
        var flxProductReferenceHeader = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
            "centerY": "50%",
            "clipBounds": true,
            "id": "flxProductReferenceHeader",
            "isVisible": true,
            "layoutType": kony.flex.FLOW_HORIZONTAL,
            "left": "41%",
            "isModalContainer": false,
            "skin": "hoverhandSkin",
            "width": "140px"
        }, controller.args[0], "flxProductReferenceHeader"), extendConfig({}, controller.args[1], "flxProductReferenceHeader"), extendConfig({}, controller.args[2], "flxProductReferenceHeader"));
        flxProductReferenceHeader.setDefaultUnit(kony.flex.DP);
        var lblProductReferenceHeader = new kony.ui.Label(extendConfig({
            "id": "lblProductReferenceHeader",
            "isVisible": true,
            "left": "0px",
            "skin": "sknlblLato696c7311px",
            "text": "PRODUCT REFERENCE",
            "top": "1px",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, controller.args[0], "lblProductReferenceHeader"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblProductReferenceHeader"), extendConfig({}, controller.args[2], "lblProductReferenceHeader"));
        var fontIconOptionsProductReference = new kony.ui.Label(extendConfig({
            "id": "fontIconOptionsProductReference",
            "isVisible": true,
            "left": "5px",
            "skin": "sknIcon15px",
            "text": "",
            "top": "0px",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, controller.args[0], "fontIconOptionsProductReference"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "fontIconOptionsProductReference"), extendConfig({
            "hoverSkin": "sknlblCursorFont"
        }, controller.args[2], "fontIconOptionsProductReference"));
        flxProductReferenceHeader.add(lblProductReferenceHeader, fontIconOptionsProductReference);
        var lblSeparatorHeaderPL = new kony.ui.Label(extendConfig({
            "bottom": "0px",
            "height": "1px",
            "id": "lblSeparatorHeaderPL",
            "isVisible": true,
            "left": "0px",
            "right": "0px",
            "skin": "sknLblSeparator696C73",
            "text": "Label",
            "zIndex": 2
        }, controller.args[0], "lblSeparatorHeaderPL"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_TOP_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblSeparatorHeaderPL"), extendConfig({}, controller.args[2], "lblSeparatorHeaderPL"));
        flxProductLineSegHeaders.add(flxProductLineHeader, flxProductReferenceHeader, lblSeparatorHeaderPL);
        var flxSegProductLine = new kony.ui.FlexScrollContainer(extendConfig({
            "allowHorizontalBounce": false,
            "allowVerticalBounce": true,
            "bottom": "0px",
            "bounces": true,
            "clipBounds": true,
            "enableScrolling": true,
            "horizontalScrollIndicator": true,
            "id": "flxSegProductLine",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "left": "0px",
            "pagingEnabled": false,
            "scrollDirection": kony.flex.SCROLL_VERTICAL,
            "skin": "slFSbox",
            "top": "173px",
            "verticalScrollIndicator": true,
            "width": "100%"
        }, controller.args[0], "flxSegProductLine"), extendConfig({}, controller.args[1], "flxSegProductLine"), extendConfig({}, controller.args[2], "flxSegProductLine"));
        flxSegProductLine.setDefaultUnit(kony.flex.DP);
        var segProductLine = new kony.ui.SegmentedUI2(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
            "data": [{
                "lblEdit": "",
                "lblProductLine": "Product Line",
                "lblProductReference": "Product Reference",
                "lblSeparator": "Label"
            }, {
                "lblEdit": "",
                "lblProductLine": "Product Line",
                "lblProductReference": "Product Reference",
                "lblSeparator": "Label"
            }, {
                "lblEdit": "",
                "lblProductLine": "Product Line",
                "lblProductReference": "Product Reference",
                "lblSeparator": "Label"
            }],
            "groupCells": false,
            "id": "segProductLine",
            "isVisible": true,
            "left": "20px",
            "needPageIndicator": true,
            "pageOffDotImage": "pageoffdot.png",
            "pageOnDotImage": "pageondot.png",
            "retainSelection": false,
            "right": "20px",
            "rowFocusSkin": "seg2Focus",
            "rowSkin": "sknseg3pxRadius",
            "rowTemplate": "flxProductLine",
            "sectionHeaderSkin": "sliPhoneSegmentHeader",
            "selectionBehavior": constants.SEGUI_DEFAULT_BEHAVIOR,
            "separatorRequired": false,
            "showScrollbars": false,
            "top": "0px",
            "viewType": constants.SEGUI_VIEW_TYPE_TABLEVIEW,
            "widgetDataMap": {
                "flxEdit": "flxEdit",
                "flxProductLine": "flxProductLine",
                "lblEdit": "lblEdit",
                "lblProductLine": "lblProductLine",
                "lblProductReference": "lblProductReference",
                "lblSeparator": "lblSeparator"
            },
            "zIndex": 1
        }, controller.args[0], "segProductLine"), extendConfig({
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "segProductLine"), extendConfig({}, controller.args[2], "segProductLine"));
        flxSegProductLine.add(segProductLine);
        var flxNoResultFoundPL = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "clipBounds": true,
            "height": "200px",
            "id": "flxNoResultFoundPL",
            "isVisible": false,
            "layoutType": kony.flex.FREE_FORM,
            "left": "0px",
            "isModalContainer": false,
            "skin": "slFbox",
            "top": "173px",
            "width": "100%",
            "zIndex": 1
        }, controller.args[0], "flxNoResultFoundPL"), extendConfig({}, controller.args[1], "flxNoResultFoundPL"), extendConfig({}, controller.args[2], "flxNoResultFoundPL"));
        flxNoResultFoundPL.setDefaultUnit(kony.flex.DP);
        var lblNoResultFoundPL = new kony.ui.Label(extendConfig({
            "centerX": "50%",
            "centerY": "50%",
            "id": "lblNoResultFoundPL",
            "isVisible": true,
            "skin": "sknLbl485C75LatoRegular13Px",
            "text": "No result found.",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, controller.args[0], "lblNoResultFoundPL"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblNoResultFoundPL"), extendConfig({}, controller.args[2], "lblNoResultFoundPL"));
        flxNoResultFoundPL.add(lblNoResultFoundPL);
        var searchBoxProductLine = new com.adminConsole.common.searchBox(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "height": "40px",
            "id": "searchBoxProductLine",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "masterType": constants.MASTER_TYPE_DEFAULT,
            "isModalContainer": false,
            "right": "20px",
            "skin": "slFbox",
            "top": "60px",
            "width": "400px",
            "overrides": {
                "searchBox": {
                    "left": "viz.val_cleared",
                    "right": "20px",
                    "top": "60px",
                    "width": "400px"
                },
                "tbxSearchBox": {
                    "placeholder": "Search by product line"
                }
            }
        }, controller.args[0], "searchBoxProductLine"), extendConfig({
            "overrides": {}
        }, controller.args[1], "searchBoxProductLine"), extendConfig({
            "overrides": {}
        }, controller.args[2], "searchBoxProductLine"));
        flxManageProductLine.add(lblProductLineTopHeader, btnAddProductLines, lblSeparatorSearchPL, flxProductLineSegHeaders, flxSegProductLine, flxNoResultFoundPL, searchBoxProductLine);
        var flxManageProductGroup = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "bottom": "0px",
            "clipBounds": true,
            "id": "flxManageProductGroup",
            "isVisible": false,
            "layoutType": kony.flex.FREE_FORM,
            "left": "0px",
            "isModalContainer": false,
            "skin": "slFbox",
            "top": "45px",
            "width": "100%",
            "zIndex": 4
        }, controller.args[0], "flxManageProductGroup"), extendConfig({}, controller.args[1], "flxManageProductGroup"), extendConfig({}, controller.args[2], "flxManageProductGroup"));
        flxManageProductGroup.setDefaultUnit(kony.flex.DP);
        var lblProductGroupTopHeader = new kony.ui.Label(extendConfig({
            "id": "lblProductGroupTopHeader",
            "isVisible": true,
            "left": "20px",
            "skin": "sknLatoRegular192B4516px",
            "text": "Product Groups",
            "top": "20px",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, controller.args[0], "lblProductGroupTopHeader"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblProductGroupTopHeader"), extendConfig({}, controller.args[2], "lblProductGroupTopHeader"));
        var btnAddProductGroups = new kony.ui.Button(extendConfig({
            "focusSkin": "sknbtnffffffLatoRegular4f555dBorder1px485c75",
            "height": "22px",
            "id": "btnAddProductGroups",
            "isVisible": true,
            "right": "20px",
            "skin": "sknbtnf7f7faLatoReg13px485c75Rad20px",
            "text": "Add Product Group",
            "top": "20px",
            "width": "141px",
            "zIndex": 1
        }, controller.args[0], "btnAddProductGroups"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_CENTER,
            "displayText": true,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "btnAddProductGroups"), extendConfig({
            "hoverSkin": "sknbtnffffffLatoRegular4f555dBorder1px485c75"
        }, controller.args[2], "btnAddProductGroups"));
        var lblSeparatorSearchPG = new kony.ui.Label(extendConfig({
            "height": "1px",
            "id": "lblSeparatorSearchPG",
            "isVisible": true,
            "left": "20px",
            "right": "20px",
            "skin": "sknlblSeperatorD7D9E0",
            "text": "Label",
            "top": "119px",
            "zIndex": 2
        }, controller.args[0], "lblSeparatorSearchPG"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_TOP_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblSeparatorSearchPG"), extendConfig({}, controller.args[2], "lblSeparatorSearchPG"));
        var flxProductGroupSegHeaders = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "clipBounds": true,
            "height": "52px",
            "id": "flxProductGroupSegHeaders",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "left": "20px",
            "isModalContainer": false,
            "right": "20px",
            "skin": "slFbox",
            "top": "120px",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, controller.args[0], "flxProductGroupSegHeaders"), extendConfig({}, controller.args[1], "flxProductGroupSegHeaders"), extendConfig({}, controller.args[2], "flxProductGroupSegHeaders"));
        flxProductGroupSegHeaders.setDefaultUnit(kony.flex.DP);
        var flxProductGroupHeader = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
            "centerY": "50%",
            "clipBounds": true,
            "id": "flxProductGroupHeader",
            "isVisible": true,
            "layoutType": kony.flex.FLOW_HORIZONTAL,
            "left": "10px",
            "isModalContainer": false,
            "skin": "slFbox",
            "width": "121px"
        }, controller.args[0], "flxProductGroupHeader"), extendConfig({}, controller.args[1], "flxProductGroupHeader"), extendConfig({}, controller.args[2], "flxProductGroupHeader"));
        flxProductGroupHeader.setDefaultUnit(kony.flex.DP);
        var lblProductGroupHeader = new kony.ui.Label(extendConfig({
            "id": "lblProductGroupHeader",
            "isVisible": true,
            "left": "0px",
            "skin": "sknlblLato696c7311px",
            "text": "PRODUCT GROUPS",
            "top": "1px",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, controller.args[0], "lblProductGroupHeader"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblProductGroupHeader"), extendConfig({}, controller.args[2], "lblProductGroupHeader"));
        var fontIconOptionsProductGroup = new kony.ui.Label(extendConfig({
            "id": "fontIconOptionsProductGroup",
            "isVisible": true,
            "left": "5px",
            "skin": "sknIcon15px",
            "text": "",
            "top": "0px",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, controller.args[0], "fontIconOptionsProductGroup"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "fontIconOptionsProductGroup"), extendConfig({
            "hoverSkin": "sknlblCursorFont"
        }, controller.args[2], "fontIconOptionsProductGroup"));
        flxProductGroupHeader.add(lblProductGroupHeader, fontIconOptionsProductGroup);
        var flxProductGroupReferenceHeader = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
            "centerY": "50%",
            "clipBounds": true,
            "id": "flxProductGroupReferenceHeader",
            "isVisible": true,
            "layoutType": kony.flex.FLOW_HORIZONTAL,
            "left": "30%",
            "isModalContainer": false,
            "skin": "hoverhandSkin",
            "width": "130px"
        }, controller.args[0], "flxProductGroupReferenceHeader"), extendConfig({}, controller.args[1], "flxProductGroupReferenceHeader"), extendConfig({}, controller.args[2], "flxProductGroupReferenceHeader"));
        flxProductGroupReferenceHeader.setDefaultUnit(kony.flex.DP);
        var lblProductGroupReferenceHeader = new kony.ui.Label(extendConfig({
            "id": "lblProductGroupReferenceHeader",
            "isVisible": true,
            "left": "0px",
            "skin": "sknlblLato696c7311px",
            "text": "GROUP REFERENCE",
            "top": "1px",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, controller.args[0], "lblProductGroupReferenceHeader"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblProductGroupReferenceHeader"), extendConfig({}, controller.args[2], "lblProductGroupReferenceHeader"));
        var fontIconOptionsGroupReference = new kony.ui.Label(extendConfig({
            "id": "fontIconOptionsGroupReference",
            "isVisible": true,
            "left": "5px",
            "skin": "sknIcon15px",
            "text": "",
            "top": "0px",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, controller.args[0], "fontIconOptionsGroupReference"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "fontIconOptionsGroupReference"), extendConfig({
            "hoverSkin": "sknlblCursorFont"
        }, controller.args[2], "fontIconOptionsGroupReference"));
        flxProductGroupReferenceHeader.add(lblProductGroupReferenceHeader, fontIconOptionsGroupReference);
        var flxProductLineHeaderPG = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
            "centerY": "50%",
            "clipBounds": true,
            "id": "flxProductLineHeaderPG",
            "isVisible": true,
            "layoutType": kony.flex.FLOW_HORIZONTAL,
            "left": "66%",
            "isModalContainer": false,
            "skin": "hoverhandSkin",
            "width": "110px"
        }, controller.args[0], "flxProductLineHeaderPG"), extendConfig({}, controller.args[1], "flxProductLineHeaderPG"), extendConfig({}, controller.args[2], "flxProductLineHeaderPG"));
        flxProductLineHeaderPG.setDefaultUnit(kony.flex.DP);
        var lblProductLineHeaderPG = new kony.ui.Label(extendConfig({
            "id": "lblProductLineHeaderPG",
            "isVisible": true,
            "left": "0px",
            "skin": "sknlblLato696c7311px",
            "text": "PRODUCT LINES",
            "top": "1px",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, controller.args[0], "lblProductLineHeaderPG"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblProductLineHeaderPG"), extendConfig({}, controller.args[2], "lblProductLineHeaderPG"));
        var fontIconOptionsProductLinePG = new kony.ui.Label(extendConfig({
            "id": "fontIconOptionsProductLinePG",
            "isVisible": true,
            "left": "5px",
            "skin": "sknIcon15px",
            "text": "",
            "top": "0px",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, controller.args[0], "fontIconOptionsProductLinePG"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "fontIconOptionsProductLinePG"), extendConfig({
            "hoverSkin": "sknlblCursorFont"
        }, controller.args[2], "fontIconOptionsProductLinePG"));
        flxProductLineHeaderPG.add(lblProductLineHeaderPG, fontIconOptionsProductLinePG);
        var lblSeparatorHeaderPG = new kony.ui.Label(extendConfig({
            "bottom": "0dp",
            "height": "1px",
            "id": "lblSeparatorHeaderPG",
            "isVisible": true,
            "left": "0dp",
            "right": "0dp",
            "skin": "sknlblSeperatorD7D9E0",
            "text": "Label",
            "zIndex": 2
        }, controller.args[0], "lblSeparatorHeaderPG"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_TOP_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblSeparatorHeaderPG"), extendConfig({}, controller.args[2], "lblSeparatorHeaderPG"));
        flxProductGroupSegHeaders.add(flxProductGroupHeader, flxProductGroupReferenceHeader, flxProductLineHeaderPG, lblSeparatorHeaderPG);
        var flxSegProductGroup = new kony.ui.FlexScrollContainer(extendConfig({
            "allowHorizontalBounce": false,
            "allowVerticalBounce": true,
            "bottom": "0px",
            "bounces": true,
            "clipBounds": true,
            "enableScrolling": true,
            "horizontalScrollIndicator": true,
            "id": "flxSegProductGroup",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "left": "0px",
            "pagingEnabled": false,
            "scrollDirection": kony.flex.SCROLL_VERTICAL,
            "skin": "slFSbox",
            "top": "173px",
            "verticalScrollIndicator": true,
            "width": "100%"
        }, controller.args[0], "flxSegProductGroup"), extendConfig({}, controller.args[1], "flxSegProductGroup"), extendConfig({}, controller.args[2], "flxSegProductGroup"));
        flxSegProductGroup.setDefaultUnit(kony.flex.DP);
        var segProductGroup = new kony.ui.SegmentedUI2(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
            "data": [{
                "lblEdit": "",
                "lblGroupReference": "Group Reference",
                "lblProductGroup": "Product Group",
                "lblProductLine": "Product Line",
                "lblSeparator": "Label"
            }, {
                "lblEdit": "",
                "lblGroupReference": "Group Reference",
                "lblProductGroup": "Product Group",
                "lblProductLine": "Product Line",
                "lblSeparator": "Label"
            }, {
                "lblEdit": "",
                "lblGroupReference": "Group Reference",
                "lblProductGroup": "Product Group",
                "lblProductLine": "Product Line",
                "lblSeparator": "Label"
            }],
            "groupCells": false,
            "id": "segProductGroup",
            "isVisible": true,
            "left": "20px",
            "needPageIndicator": true,
            "pageOffDotImage": "pageoffdot.png",
            "pageOnDotImage": "pageondot.png",
            "retainSelection": false,
            "right": "20px",
            "rowFocusSkin": "seg2Focus",
            "rowSkin": "sknseg3pxRadius",
            "rowTemplate": "flxProductGroup",
            "sectionHeaderSkin": "sliPhoneSegmentHeader",
            "selectionBehavior": constants.SEGUI_DEFAULT_BEHAVIOR,
            "separatorRequired": false,
            "showScrollbars": false,
            "top": "0px",
            "viewType": constants.SEGUI_VIEW_TYPE_TABLEVIEW,
            "widgetDataMap": {
                "flxEdit": "flxEdit",
                "flxProductGroup": "flxProductGroup",
                "lblEdit": "lblEdit",
                "lblGroupReference": "lblGroupReference",
                "lblProductGroup": "lblProductGroup",
                "lblProductLine": "lblProductLine",
                "lblSeparator": "lblSeparator"
            },
            "zIndex": 1
        }, controller.args[0], "segProductGroup"), extendConfig({
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "segProductGroup"), extendConfig({}, controller.args[2], "segProductGroup"));
        flxSegProductGroup.add(segProductGroup);
        var flxNoResultFoundPG = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "clipBounds": true,
            "height": "200px",
            "id": "flxNoResultFoundPG",
            "isVisible": false,
            "layoutType": kony.flex.FREE_FORM,
            "left": "0px",
            "isModalContainer": false,
            "skin": "slFbox",
            "top": "173px",
            "width": "100%",
            "zIndex": 1
        }, controller.args[0], "flxNoResultFoundPG"), extendConfig({}, controller.args[1], "flxNoResultFoundPG"), extendConfig({}, controller.args[2], "flxNoResultFoundPG"));
        flxNoResultFoundPG.setDefaultUnit(kony.flex.DP);
        var lblNoResultFoundPG = new kony.ui.Label(extendConfig({
            "centerX": "50%",
            "centerY": "50%",
            "id": "lblNoResultFoundPG",
            "isVisible": true,
            "skin": "sknLbl485C75LatoRegular13Px",
            "text": "No result found.",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, controller.args[0], "lblNoResultFoundPG"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblNoResultFoundPG"), extendConfig({}, controller.args[2], "lblNoResultFoundPG"));
        flxNoResultFoundPG.add(lblNoResultFoundPG);
        var searchBoxProductGroup = new com.adminConsole.common.searchBox(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "height": "40px",
            "id": "searchBoxProductGroup",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "masterType": constants.MASTER_TYPE_DEFAULT,
            "isModalContainer": false,
            "right": "20px",
            "skin": "slFbox",
            "top": "60px",
            "width": "400px",
            "overrides": {
                "searchBox": {
                    "left": "viz.val_cleared",
                    "right": "20px",
                    "top": "60px",
                    "width": "400px"
                },
                "tbxSearchBox": {
                    "placeholder": "Search by product group"
                }
            }
        }, controller.args[0], "searchBoxProductGroup"), extendConfig({
            "overrides": {}
        }, controller.args[1], "searchBoxProductGroup"), extendConfig({
            "overrides": {}
        }, controller.args[2], "searchBoxProductGroup"));
        flxManageProductGroup.add(lblProductGroupTopHeader, btnAddProductGroups, lblSeparatorSearchPG, flxProductGroupSegHeaders, flxSegProductGroup, flxNoResultFoundPG, searchBoxProductGroup);
        var flxManageProductFeature = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "bottom": "0px",
            "clipBounds": true,
            "id": "flxManageProductFeature",
            "isVisible": false,
            "layoutType": kony.flex.FREE_FORM,
            "left": "0px",
            "isModalContainer": false,
            "skin": "slFbox",
            "top": "45px",
            "width": "100%",
            "zIndex": 4
        }, controller.args[0], "flxManageProductFeature"), extendConfig({}, controller.args[1], "flxManageProductFeature"), extendConfig({}, controller.args[2], "flxManageProductFeature"));
        flxManageProductFeature.setDefaultUnit(kony.flex.DP);
        var lblProductFeaureTopHeader = new kony.ui.Label(extendConfig({
            "id": "lblProductFeaureTopHeader",
            "isVisible": true,
            "left": "20px",
            "skin": "sknLatoRegular192B4516px",
            "text": "Product Features",
            "top": "20px",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, controller.args[0], "lblProductFeaureTopHeader"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblProductFeaureTopHeader"), extendConfig({}, controller.args[2], "lblProductFeaureTopHeader"));
        var btnAddProductFeatures = new kony.ui.Button(extendConfig({
            "focusSkin": "sknbtnffffffLatoRegular4f555dBorder1px485c75",
            "height": "22px",
            "id": "btnAddProductFeatures",
            "isVisible": true,
            "right": "20px",
            "skin": "sknbtnf7f7faLatoReg13px485c75Rad20px",
            "text": "Add Product Featues",
            "top": "20px",
            "width": "145px",
            "zIndex": 1
        }, controller.args[0], "btnAddProductFeatures"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_CENTER,
            "displayText": true,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "btnAddProductFeatures"), extendConfig({
            "hoverSkin": "sknbtnffffffLatoRegular4f555dBorder1px485c75"
        }, controller.args[2], "btnAddProductFeatures"));
        var lblSeparatorSearchPF = new kony.ui.Label(extendConfig({
            "height": "1px",
            "id": "lblSeparatorSearchPF",
            "isVisible": true,
            "left": "20px",
            "right": "20px",
            "skin": "sknlblSeperatorD7D9E0",
            "text": "Label",
            "top": "119px",
            "zIndex": 2
        }, controller.args[0], "lblSeparatorSearchPF"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_TOP_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblSeparatorSearchPF"), extendConfig({}, controller.args[2], "lblSeparatorSearchPF"));
        var flxProductFeatureSegHeaders = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "clipBounds": true,
            "height": "52px",
            "id": "flxProductFeatureSegHeaders",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "left": "20px",
            "isModalContainer": false,
            "right": "20px",
            "skin": "slFbox",
            "top": "120px",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, controller.args[0], "flxProductFeatureSegHeaders"), extendConfig({}, controller.args[1], "flxProductFeatureSegHeaders"), extendConfig({}, controller.args[2], "flxProductFeatureSegHeaders"));
        flxProductFeatureSegHeaders.setDefaultUnit(kony.flex.DP);
        var flxFeatureNameHeader = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
            "centerY": "50%",
            "clipBounds": true,
            "id": "flxFeatureNameHeader",
            "isVisible": true,
            "layoutType": kony.flex.FLOW_HORIZONTAL,
            "left": "10px",
            "isModalContainer": false,
            "skin": "slFbox",
            "width": "110px"
        }, controller.args[0], "flxFeatureNameHeader"), extendConfig({}, controller.args[1], "flxFeatureNameHeader"), extendConfig({}, controller.args[2], "flxFeatureNameHeader"));
        flxFeatureNameHeader.setDefaultUnit(kony.flex.DP);
        var lblFeatureNameHeader = new kony.ui.Label(extendConfig({
            "id": "lblFeatureNameHeader",
            "isVisible": true,
            "left": "0px",
            "skin": "sknlblLato696c7311px",
            "text": "FEATURE  NAME",
            "top": "1px",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, controller.args[0], "lblFeatureNameHeader"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblFeatureNameHeader"), extendConfig({}, controller.args[2], "lblFeatureNameHeader"));
        var fontIconOptionsFeatureName = new kony.ui.Label(extendConfig({
            "id": "fontIconOptionsFeatureName",
            "isVisible": true,
            "left": "5px",
            "skin": "sknIcon15px",
            "text": "",
            "top": "0px",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, controller.args[0], "fontIconOptionsFeatureName"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "fontIconOptionsFeatureName"), extendConfig({
            "hoverSkin": "sknlblCursorFont"
        }, controller.args[2], "fontIconOptionsFeatureName"));
        flxFeatureNameHeader.add(lblFeatureNameHeader, fontIconOptionsFeatureName);
        var flxFeatureGroupHeader = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
            "centerY": "50%",
            "clipBounds": true,
            "id": "flxFeatureGroupHeader",
            "isVisible": true,
            "layoutType": kony.flex.FLOW_HORIZONTAL,
            "left": "30%",
            "isModalContainer": false,
            "skin": "hoverhandSkin",
            "width": "120px"
        }, controller.args[0], "flxFeatureGroupHeader"), extendConfig({}, controller.args[1], "flxFeatureGroupHeader"), extendConfig({}, controller.args[2], "flxFeatureGroupHeader"));
        flxFeatureGroupHeader.setDefaultUnit(kony.flex.DP);
        var lblFeatureGroupHeader = new kony.ui.Label(extendConfig({
            "id": "lblFeatureGroupHeader",
            "isVisible": true,
            "left": "0px",
            "skin": "sknlblLato696c7311px",
            "text": "FEATURE  GROUP",
            "top": "1px",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, controller.args[0], "lblFeatureGroupHeader"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblFeatureGroupHeader"), extendConfig({}, controller.args[2], "lblFeatureGroupHeader"));
        var fontIconOptionsFeatureGroup = new kony.ui.Label(extendConfig({
            "id": "fontIconOptionsFeatureGroup",
            "isVisible": true,
            "left": "5px",
            "skin": "sknIcon15px",
            "text": "",
            "top": "0px",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, controller.args[0], "fontIconOptionsFeatureGroup"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "fontIconOptionsFeatureGroup"), extendConfig({
            "hoverSkin": "sknlblCursorFont"
        }, controller.args[2], "fontIconOptionsFeatureGroup"));
        flxFeatureGroupHeader.add(lblFeatureGroupHeader, fontIconOptionsFeatureGroup);
        var flxFeatureTypeHeader = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
            "centerY": "50%",
            "clipBounds": true,
            "id": "flxFeatureTypeHeader",
            "isVisible": true,
            "layoutType": kony.flex.FLOW_HORIZONTAL,
            "left": "66%",
            "isModalContainer": false,
            "skin": "hoverhandSkin",
            "width": "100px"
        }, controller.args[0], "flxFeatureTypeHeader"), extendConfig({}, controller.args[1], "flxFeatureTypeHeader"), extendConfig({}, controller.args[2], "flxFeatureTypeHeader"));
        flxFeatureTypeHeader.setDefaultUnit(kony.flex.DP);
        var lblFeatureTypeHeader = new kony.ui.Label(extendConfig({
            "id": "lblFeatureTypeHeader",
            "isVisible": true,
            "left": "0px",
            "skin": "sknlblLato696c7311px",
            "text": "FEATURE TYPE",
            "top": "1px",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, controller.args[0], "lblFeatureTypeHeader"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblFeatureTypeHeader"), extendConfig({}, controller.args[2], "lblFeatureTypeHeader"));
        var fontIconOptionsFeatureType = new kony.ui.Label(extendConfig({
            "id": "fontIconOptionsFeatureType",
            "isVisible": true,
            "left": "5px",
            "skin": "sknIcon15px",
            "text": "",
            "top": "0px",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, controller.args[0], "fontIconOptionsFeatureType"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "fontIconOptionsFeatureType"), extendConfig({
            "hoverSkin": "sknlblCursorFont"
        }, controller.args[2], "fontIconOptionsFeatureType"));
        flxFeatureTypeHeader.add(lblFeatureTypeHeader, fontIconOptionsFeatureType);
        var lblSeparatorHeaderPF = new kony.ui.Label(extendConfig({
            "bottom": "0dp",
            "height": "1px",
            "id": "lblSeparatorHeaderPF",
            "isVisible": true,
            "left": "0dp",
            "right": "0dp",
            "skin": "sknlblSeperatorD7D9E0",
            "text": "Label",
            "zIndex": 2
        }, controller.args[0], "lblSeparatorHeaderPF"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_TOP_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblSeparatorHeaderPF"), extendConfig({}, controller.args[2], "lblSeparatorHeaderPF"));
        flxProductFeatureSegHeaders.add(flxFeatureNameHeader, flxFeatureGroupHeader, flxFeatureTypeHeader, lblSeparatorHeaderPF);
        var flxSegFeature = new kony.ui.FlexScrollContainer(extendConfig({
            "allowHorizontalBounce": false,
            "allowVerticalBounce": true,
            "bottom": "0px",
            "bounces": true,
            "clipBounds": true,
            "enableScrolling": true,
            "horizontalScrollIndicator": true,
            "id": "flxSegFeature",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "left": "0px",
            "pagingEnabled": false,
            "scrollDirection": kony.flex.SCROLL_VERTICAL,
            "skin": "slFSbox",
            "top": "173px",
            "verticalScrollIndicator": true,
            "width": "100%"
        }, controller.args[0], "flxSegFeature"), extendConfig({}, controller.args[1], "flxSegFeature"), extendConfig({}, controller.args[2], "flxSegFeature"));
        flxSegFeature.setDefaultUnit(kony.flex.DP);
        var segProductFeature = new kony.ui.SegmentedUI2(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
            "data": [{
                "lblEdit": "",
                "lblGroup": "Group Reference",
                "lblName": "Product Group",
                "lblSeparator": "Label",
                "lblType": "Product Line"
            }, {
                "lblEdit": "",
                "lblGroup": "Group Reference",
                "lblName": "Product Group",
                "lblSeparator": "Label",
                "lblType": "Product Line"
            }, {
                "lblEdit": "",
                "lblGroup": "Group Reference",
                "lblName": "Product Group",
                "lblSeparator": "Label",
                "lblType": "Product Line"
            }],
            "groupCells": false,
            "id": "segProductFeature",
            "isVisible": true,
            "left": "20px",
            "needPageIndicator": true,
            "pageOffDotImage": "pageoffdot.png",
            "pageOnDotImage": "pageondot.png",
            "retainSelection": false,
            "right": "20px",
            "rowFocusSkin": "seg2Focus",
            "rowSkin": "sknseg3pxRadius",
            "rowTemplate": "flxProductFeature",
            "sectionHeaderSkin": "sliPhoneSegmentHeader",
            "selectionBehavior": constants.SEGUI_DEFAULT_BEHAVIOR,
            "separatorRequired": false,
            "showScrollbars": false,
            "top": "0px",
            "viewType": constants.SEGUI_VIEW_TYPE_TABLEVIEW,
            "widgetDataMap": {
                "flxEdit": "flxEdit",
                "flxProductFeature": "flxProductFeature",
                "lblEdit": "lblEdit",
                "lblGroup": "lblGroup",
                "lblName": "lblName",
                "lblSeparator": "lblSeparator",
                "lblType": "lblType"
            },
            "zIndex": 1
        }, controller.args[0], "segProductFeature"), extendConfig({
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "segProductFeature"), extendConfig({}, controller.args[2], "segProductFeature"));
        flxSegFeature.add(segProductFeature);
        var flxNoResultFoundPF = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "clipBounds": true,
            "height": "200px",
            "id": "flxNoResultFoundPF",
            "isVisible": false,
            "layoutType": kony.flex.FREE_FORM,
            "left": "0px",
            "isModalContainer": false,
            "skin": "slFbox",
            "top": "173px",
            "width": "100%",
            "zIndex": 1
        }, controller.args[0], "flxNoResultFoundPF"), extendConfig({}, controller.args[1], "flxNoResultFoundPF"), extendConfig({}, controller.args[2], "flxNoResultFoundPF"));
        flxNoResultFoundPF.setDefaultUnit(kony.flex.DP);
        var lblNoResultFoundPF = new kony.ui.Label(extendConfig({
            "centerX": "50%",
            "centerY": "50%",
            "id": "lblNoResultFoundPF",
            "isVisible": true,
            "skin": "sknLbl485C75LatoRegular13Px",
            "text": "No result found.",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, controller.args[0], "lblNoResultFoundPF"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblNoResultFoundPF"), extendConfig({}, controller.args[2], "lblNoResultFoundPF"));
        flxNoResultFoundPF.add(lblNoResultFoundPF);
        var searchBoxProductFeature = new com.adminConsole.common.searchBox(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "height": "40px",
            "id": "searchBoxProductFeature",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "masterType": constants.MASTER_TYPE_DEFAULT,
            "isModalContainer": false,
            "right": "20px",
            "skin": "slFbox",
            "top": "60px",
            "width": "400px",
            "overrides": {
                "searchBox": {
                    "left": "viz.val_cleared",
                    "right": "20px",
                    "top": "60px",
                    "width": "400px"
                },
                "tbxSearchBox": {
                    "placeholder": "Search by product feature"
                }
            }
        }, controller.args[0], "searchBoxProductFeature"), extendConfig({
            "overrides": {}
        }, controller.args[1], "searchBoxProductFeature"), extendConfig({
            "overrides": {}
        }, controller.args[2], "searchBoxProductFeature"));
        flxManageProductFeature.add(lblProductFeaureTopHeader, btnAddProductFeatures, lblSeparatorSearchPF, flxProductFeatureSegHeaders, flxSegFeature, flxNoResultFoundPF, searchBoxProductFeature);
        flxManageProducts.add(flxProductTabs, flxManageProductLine, flxManageProductGroup, flxManageProductFeature);
        var flxPopup = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "clipBounds": true,
            "height": "100%",
            "id": "flxPopup",
            "isVisible": false,
            "layoutType": kony.flex.FREE_FORM,
            "left": "0px",
            "masterType": constants.MASTER_TYPE_USERWIDGET,
            "isModalContainer": false,
            "skin": "skn222b35",
            "top": "0px",
            "width": "100%",
            "zIndex": 10
        }, controller.args[0], "flxPopup"), extendConfig({}, controller.args[1], "flxPopup"), extendConfig({}, controller.args[2], "flxPopup"));
        flxPopup.setDefaultUnit(kony.flex.DP);
        var flxAddProductLine = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "centerX": "50%",
            "centerY": "50%",
            "clipBounds": false,
            "height": "430px",
            "id": "flxAddProductLine",
            "isVisible": false,
            "layoutType": kony.flex.FREE_FORM,
            "isModalContainer": false,
            "skin": "flxNoShadowNoBorder",
            "width": "520px",
            "zIndex": 10
        }, controller.args[0], "flxAddProductLine"), extendConfig({}, controller.args[1], "flxAddProductLine"), extendConfig({}, controller.args[2], "flxAddProductLine"));
        flxAddProductLine.setDefaultUnit(kony.flex.DP);
        var flxAddProductLinePopupTopBar = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "clipBounds": true,
            "height": "10px",
            "id": "flxAddProductLinePopupTopBar",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "left": "0dp",
            "isModalContainer": false,
            "skin": "sknFlxInfoToastBg357C9ENoRadius",
            "top": "0dp",
            "width": "100%",
            "zIndex": 1
        }, controller.args[0], "flxAddProductLinePopupTopBar"), extendConfig({}, controller.args[1], "flxAddProductLinePopupTopBar"), extendConfig({}, controller.args[2], "flxAddProductLinePopupTopBar"));
        flxAddProductLinePopupTopBar.setDefaultUnit(kony.flex.DP);
        flxAddProductLinePopupTopBar.add();
        var flxAddProductLinePopupClose = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "clipBounds": true,
            "height": "15px",
            "id": "flxAddProductLinePopupClose",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "isModalContainer": false,
            "right": 20,
            "skin": "hoverhandSkin",
            "top": "30dp",
            "width": "15px",
            "zIndex": 1
        }, controller.args[0], "flxAddProductLinePopupClose"), extendConfig({}, controller.args[1], "flxAddProductLinePopupClose"), extendConfig({
            "hoverSkin": "sknCursor"
        }, controller.args[2], "flxAddProductLinePopupClose"));
        flxAddProductLinePopupClose.setDefaultUnit(kony.flex.DP);
        var lblAddProductLinePopupClose = new kony.ui.Label(extendConfig({
            "height": "15dp",
            "id": "lblAddProductLinePopupClose",
            "isVisible": true,
            "left": "0dp",
            "skin": "sknfontIconListbox15px",
            "i18n_text": "kony.i18n.getLocalizedString(\"i18n.userwidgetmodel.lblClearSearch\")",
            "top": "0dp",
            "width": "15dp",
            "zIndex": 1
        }, controller.args[0], "lblAddProductLinePopupClose"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblAddProductLinePopupClose"), extendConfig({}, controller.args[2], "lblAddProductLinePopupClose"));
        flxAddProductLinePopupClose.add(lblAddProductLinePopupClose);
        var lblAddProductLinePopupTopHeader = new kony.ui.Label(extendConfig({
            "height": "19dp",
            "id": "lblAddProductLinePopupTopHeader",
            "isVisible": true,
            "left": "20px",
            "skin": "sknLbl192b45LatoReg16px",
            "text": "Add Product Line",
            "top": "37px",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, controller.args[0], "lblAddProductLinePopupTopHeader"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblAddProductLinePopupTopHeader"), extendConfig({}, controller.args[2], "lblAddProductLinePopupTopHeader"));
        var flxProductLineNameHeader = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
            "clipBounds": true,
            "id": "flxProductLineNameHeader",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "left": "20dp",
            "isModalContainer": false,
            "right": "20dp",
            "skin": "slFbox",
            "top": "80dp"
        }, controller.args[0], "flxProductLineNameHeader"), extendConfig({}, controller.args[1], "flxProductLineNameHeader"), extendConfig({}, controller.args[2], "flxProductLineNameHeader"));
        flxProductLineNameHeader.setDefaultUnit(kony.flex.DP);
        var lblProductNameHeader = new kony.ui.Label(extendConfig({
            "id": "lblProductNameHeader",
            "isVisible": true,
            "left": "0dp",
            "skin": "sknLblLato485c7513px",
            "text": "Product Line Name",
            "top": "0dp",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, controller.args[0], "lblProductNameHeader"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblProductNameHeader"), extendConfig({}, controller.args[2], "lblProductNameHeader"));
        var lblProductLineNameSize = new kony.ui.Label(extendConfig({
            "id": "lblProductLineNameSize",
            "isVisible": false,
            "right": "0dp",
            "skin": "sknLblLato485c7513px",
            "text": "0/50",
            "top": "0dp",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, controller.args[0], "lblProductLineNameSize"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblProductLineNameSize"), extendConfig({}, controller.args[2], "lblProductLineNameSize"));
        flxProductLineNameHeader.add(lblProductNameHeader, lblProductLineNameSize);
        var txtProductLineName = new kony.ui.TextBox2(extendConfig({
            "autoCapitalize": constants.TEXTBOX_AUTO_CAPITALIZE_NONE,
            "focusSkin": "skntxtbxDetailsFocus14px",
            "height": "40dp",
            "id": "txtProductLineName",
            "isVisible": true,
            "keyBoardStyle": constants.TEXTBOX_KEY_BOARD_STYLE_DEFAULT,
            "left": "20dp",
            "maxTextLength": 50,
            "placeholder": "Product Line Name",
            "right": "20dp",
            "secureTextEntry": false,
            "skin": "txtD7d9e0",
            "textInputMode": constants.TEXTBOX_INPUT_MODE_ANY,
            "top": "110dp",
            "zIndex": 1
        }, controller.args[0], "txtProductLineName"), extendConfig({
            "containerHeightMode": constants.TEXTBOX_FONT_METRICS_DRIVEN_HEIGHT,
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [2, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "txtProductLineName"), extendConfig({
            "autoCorrect": false,
            "placeholderSkin": "sknTextGreyb2bdcd"
        }, controller.args[2], "txtProductLineName"));
        var ProductLineNameError = new com.adminConsole.loans.applications.errorMsg(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
            "id": "ProductLineNameError",
            "isVisible": true,
            "layoutType": kony.flex.FLOW_HORIZONTAL,
            "left": "20dp",
            "masterType": constants.MASTER_TYPE_DEFAULT,
            "isModalContainer": false,
            "right": "20dp",
            "skin": "slFbox",
            "top": "159dp",
            "overrides": {
                "errorMsg": {
                    "isVisible": true,
                    "left": "20dp",
                    "right": "20dp",
                    "top": "159dp",
                    "width": "viz.val_cleared"
                },
                "lblErrorText": {
                    "text": "Please enter Product Line Name"
                }
            }
        }, controller.args[0], "ProductLineNameError"), extendConfig({
            "overrides": {}
        }, controller.args[1], "ProductLineNameError"), extendConfig({
            "overrides": {}
        }, controller.args[2], "ProductLineNameError"));
        var flxProductLineRefHeader = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
            "clipBounds": true,
            "id": "flxProductLineRefHeader",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "left": "20dp",
            "isModalContainer": false,
            "right": "20dp",
            "skin": "slFbox",
            "top": "185dp"
        }, controller.args[0], "flxProductLineRefHeader"), extendConfig({}, controller.args[1], "flxProductLineRefHeader"), extendConfig({}, controller.args[2], "flxProductLineRefHeader"));
        flxProductLineRefHeader.setDefaultUnit(kony.flex.DP);
        var lblProductLineRefHeader = new kony.ui.Label(extendConfig({
            "id": "lblProductLineRefHeader",
            "isVisible": true,
            "left": "0dp",
            "skin": "sknLblLato485c7513px",
            "text": "Product Line Reference",
            "top": "0dp",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, controller.args[0], "lblProductLineRefHeader"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblProductLineRefHeader"), extendConfig({}, controller.args[2], "lblProductLineRefHeader"));
        var lblProductLineRefSize = new kony.ui.Label(extendConfig({
            "id": "lblProductLineRefSize",
            "isVisible": false,
            "right": "0dp",
            "skin": "sknLblLato485c7513px",
            "text": "0/50",
            "top": "0dp",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, controller.args[0], "lblProductLineRefSize"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblProductLineRefSize"), extendConfig({}, controller.args[2], "lblProductLineRefSize"));
        flxProductLineRefHeader.add(lblProductLineRefHeader, lblProductLineRefSize);
        var txtProductLineReference = new kony.ui.TextBox2(extendConfig({
            "autoCapitalize": constants.TEXTBOX_AUTO_CAPITALIZE_NONE,
            "focusSkin": "skntxtbxDetailsFocus14px",
            "height": "40dp",
            "id": "txtProductLineReference",
            "isVisible": true,
            "keyBoardStyle": constants.TEXTBOX_KEY_BOARD_STYLE_DEFAULT,
            "left": "20dp",
            "maxTextLength": 50,
            "placeholder": "Product Line Reference",
            "right": "20dp",
            "secureTextEntry": false,
            "skin": "txtD7d9e0",
            "textInputMode": constants.TEXTBOX_INPUT_MODE_ANY,
            "top": "215dp",
            "zIndex": 1
        }, controller.args[0], "txtProductLineReference"), extendConfig({
            "containerHeightMode": constants.TEXTBOX_FONT_METRICS_DRIVEN_HEIGHT,
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [2, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "txtProductLineReference"), extendConfig({
            "autoCorrect": false,
            "placeholderSkin": "sknTextGreyb2bdcd"
        }, controller.args[2], "txtProductLineReference"));
        var ProductLineRefError = new com.adminConsole.loans.applications.errorMsg(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
            "id": "ProductLineRefError",
            "isVisible": false,
            "layoutType": kony.flex.FLOW_HORIZONTAL,
            "left": "20dp",
            "masterType": constants.MASTER_TYPE_DEFAULT,
            "isModalContainer": false,
            "right": "20dp",
            "skin": "slFbox",
            "top": "262dp",
            "overrides": {
                "errorMsg": {
                    "isVisible": false,
                    "left": "20dp",
                    "right": "20dp",
                    "top": "262dp",
                    "width": "viz.val_cleared"
                },
                "lblErrorText": {
                    "text": "Please enter Product Line Reference"
                }
            }
        }, controller.args[0], "ProductLineRefError"), extendConfig({
            "overrides": {}
        }, controller.args[1], "ProductLineRefError"), extendConfig({
            "overrides": {}
        }, controller.args[2], "ProductLineRefError"));
        var flxAddProductLinePopupButtons = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "bottom": "0dp",
            "clipBounds": true,
            "height": "80px",
            "id": "flxAddProductLinePopupButtons",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "left": "0dp",
            "isModalContainer": false,
            "skin": "sknflxBgE4E6EC",
            "width": "100%",
            "zIndex": 1
        }, controller.args[0], "flxAddProductLinePopupButtons"), extendConfig({}, controller.args[1], "flxAddProductLinePopupButtons"), extendConfig({}, controller.args[2], "flxAddProductLinePopupButtons"));
        flxAddProductLinePopupButtons.setDefaultUnit(kony.flex.DP);
        var btnAddProductLineCancel = new kony.ui.Button(extendConfig({
            "centerY": "50%",
            "height": "40px",
            "id": "btnAddProductLineCancel",
            "isVisible": true,
            "left": "20px",
            "skin": "sknbtnf7f7faLatoRegular4f555dBorder1px8b96a5",
            "i18n_text": "kony.i18n.getLocalizedString(\"i18n.permission.CANCEL\")",
            "top": "0%",
            "width": "105px",
            "zIndex": 20
        }, controller.args[0], "btnAddProductLineCancel"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_CENTER,
            "displayText": true,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "btnAddProductLineCancel"), extendConfig({
            "hoverSkin": "sknbtnffffffLatoRegular4f555dBorder1px485c75"
        }, controller.args[2], "btnAddProductLineCancel"));
        var btnAddProductLine = new kony.ui.Button(extendConfig({
            "centerY": "50%",
            "focusSkin": "sknBtn003E75LatoRegular13pxFFFFFFRad20px",
            "height": "40px",
            "id": "btnAddProductLine",
            "isVisible": true,
            "minWidth": "100px",
            "right": "20px",
            "skin": "sknBtn003E75LatoRegular13pxFFFFFFRad20px",
            "text": "ADD PRODUCT LINE",
            "width": "180px",
            "zIndex": 20
        }, controller.args[0], "btnAddProductLine"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_CENTER,
            "displayText": true,
            "padding": [3, 0, 3, 0],
            "paddingInPixel": false
        }, controller.args[1], "btnAddProductLine"), extendConfig({
            "hoverSkin": "sknBtn005198LatoRegular13pxFFFFFFRad20px"
        }, controller.args[2], "btnAddProductLine"));
        flxAddProductLinePopupButtons.add(btnAddProductLineCancel, btnAddProductLine);
        flxAddProductLine.add(flxAddProductLinePopupTopBar, flxAddProductLinePopupClose, lblAddProductLinePopupTopHeader, flxProductLineNameHeader, txtProductLineName, ProductLineNameError, flxProductLineRefHeader, txtProductLineReference, ProductLineRefError, flxAddProductLinePopupButtons);
        var flxAddProductGroup = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "centerY": "50%",
            "clipBounds": false,
            "height": "80%",
            "id": "flxAddProductGroup",
            "isVisible": false,
            "layoutType": kony.flex.FREE_FORM,
            "left": "14%",
            "isModalContainer": false,
            "right": "14%",
            "skin": "flxNoShadowNoBorder",
            "zIndex": 10
        }, controller.args[0], "flxAddProductGroup"), extendConfig({}, controller.args[1], "flxAddProductGroup"), extendConfig({}, controller.args[2], "flxAddProductGroup"));
        flxAddProductGroup.setDefaultUnit(kony.flex.DP);
        var flxAddProductGroupPopupTopBar = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "clipBounds": true,
            "height": "10px",
            "id": "flxAddProductGroupPopupTopBar",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "left": "0dp",
            "isModalContainer": false,
            "skin": "sknFlxInfoToastBg357C9ENoRadius",
            "top": "0dp",
            "width": "100%",
            "zIndex": 1
        }, controller.args[0], "flxAddProductGroupPopupTopBar"), extendConfig({}, controller.args[1], "flxAddProductGroupPopupTopBar"), extendConfig({}, controller.args[2], "flxAddProductGroupPopupTopBar"));
        flxAddProductGroupPopupTopBar.setDefaultUnit(kony.flex.DP);
        flxAddProductGroupPopupTopBar.add();
        var flxAddProductGroupPopupClose = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "clipBounds": true,
            "height": "15px",
            "id": "flxAddProductGroupPopupClose",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "isModalContainer": false,
            "right": 20,
            "skin": "hoverhandSkin",
            "top": "30dp",
            "width": "15px",
            "zIndex": 1
        }, controller.args[0], "flxAddProductGroupPopupClose"), extendConfig({}, controller.args[1], "flxAddProductGroupPopupClose"), extendConfig({
            "hoverSkin": "sknCursor"
        }, controller.args[2], "flxAddProductGroupPopupClose"));
        flxAddProductGroupPopupClose.setDefaultUnit(kony.flex.DP);
        var lblAddProductGroupPopupClose = new kony.ui.Label(extendConfig({
            "height": "15dp",
            "id": "lblAddProductGroupPopupClose",
            "isVisible": true,
            "left": "0dp",
            "skin": "sknfontIconListbox15px",
            "i18n_text": "kony.i18n.getLocalizedString(\"i18n.userwidgetmodel.lblClearSearch\")",
            "top": "0dp",
            "width": "15dp",
            "zIndex": 1
        }, controller.args[0], "lblAddProductGroupPopupClose"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblAddProductGroupPopupClose"), extendConfig({}, controller.args[2], "lblAddProductGroupPopupClose"));
        flxAddProductGroupPopupClose.add(lblAddProductGroupPopupClose);
        var lblAddProductGroupPopupHeader = new kony.ui.Label(extendConfig({
            "height": "19dp",
            "id": "lblAddProductGroupPopupHeader",
            "isVisible": true,
            "left": "20px",
            "skin": "sknLbl192b45LatoReg16px",
            "text": "Add Product Group",
            "top": "37px",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, controller.args[0], "lblAddProductGroupPopupHeader"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblAddProductGroupPopupHeader"), extendConfig({}, controller.args[2], "lblAddProductGroupPopupHeader"));
        var flxAddProductGroupScrollConatiner = new kony.ui.FlexScrollContainer(extendConfig({
            "allowHorizontalBounce": false,
            "allowVerticalBounce": true,
            "bottom": "110px",
            "bounces": true,
            "clipBounds": true,
            "enableScrolling": true,
            "horizontalScrollIndicator": true,
            "id": "flxAddProductGroupScrollConatiner",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "left": "0px",
            "pagingEnabled": false,
            "scrollDirection": kony.flex.SCROLL_VERTICAL,
            "skin": "slFSbox",
            "top": "90px",
            "verticalScrollIndicator": true,
            "width": "100%"
        }, controller.args[0], "flxAddProductGroupScrollConatiner"), extendConfig({}, controller.args[1], "flxAddProductGroupScrollConatiner"), extendConfig({}, controller.args[2], "flxAddProductGroupScrollConatiner"));
        flxAddProductGroupScrollConatiner.setDefaultUnit(kony.flex.DP);
        var flxAddProductGroupTop = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
            "clipBounds": true,
            "id": "flxAddProductGroupTop",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "left": "20px",
            "isModalContainer": false,
            "right": "20px",
            "skin": "slFbox",
            "top": "0px",
            "zIndex": 1
        }, controller.args[0], "flxAddProductGroupTop"), extendConfig({}, controller.args[1], "flxAddProductGroupTop"), extendConfig({}, controller.args[2], "flxAddProductGroupTop"));
        flxAddProductGroupTop.setDefaultUnit(kony.flex.DP);
        var flxProductGroupName = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
            "clipBounds": true,
            "id": "flxProductGroupName",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "left": "0px",
            "isModalContainer": false,
            "skin": "slFbox",
            "top": "0px",
            "width": "34%"
        }, controller.args[0], "flxProductGroupName"), extendConfig({}, controller.args[1], "flxProductGroupName"), extendConfig({}, controller.args[2], "flxProductGroupName"));
        flxProductGroupName.setDefaultUnit(kony.flex.DP);
        var flxAddProductGroupNameHeader = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
            "clipBounds": true,
            "id": "flxAddProductGroupNameHeader",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "left": "0px",
            "isModalContainer": false,
            "right": "15px",
            "skin": "slFbox",
            "top": "0px"
        }, controller.args[0], "flxAddProductGroupNameHeader"), extendConfig({}, controller.args[1], "flxAddProductGroupNameHeader"), extendConfig({}, controller.args[2], "flxAddProductGroupNameHeader"));
        flxAddProductGroupNameHeader.setDefaultUnit(kony.flex.DP);
        var lblAddProductGroupNameHeader = new kony.ui.Label(extendConfig({
            "id": "lblAddProductGroupNameHeader",
            "isVisible": true,
            "left": "0px",
            "skin": "sknLblLato485c7513px",
            "text": "Product Group Name",
            "top": "0px",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, controller.args[0], "lblAddProductGroupNameHeader"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblAddProductGroupNameHeader"), extendConfig({}, controller.args[2], "lblAddProductGroupNameHeader"));
        var lblProductGroupNameSize = new kony.ui.Label(extendConfig({
            "id": "lblProductGroupNameSize",
            "isVisible": false,
            "right": "0px",
            "skin": "sknLblLato485c7513px",
            "text": "0/50",
            "top": "0px",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, controller.args[0], "lblProductGroupNameSize"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblProductGroupNameSize"), extendConfig({}, controller.args[2], "lblProductGroupNameSize"));
        flxAddProductGroupNameHeader.add(lblAddProductGroupNameHeader, lblProductGroupNameSize);
        var txtProductGroupName = new kony.ui.TextBox2(extendConfig({
            "autoCapitalize": constants.TEXTBOX_AUTO_CAPITALIZE_NONE,
            "focusSkin": "skntxtbxDetailsFocus14px",
            "height": "40dp",
            "id": "txtProductGroupName",
            "isVisible": true,
            "keyBoardStyle": constants.TEXTBOX_KEY_BOARD_STYLE_DEFAULT,
            "left": "0px",
            "maxTextLength": 50,
            "placeholder": "Product Group Name",
            "right": "15px",
            "secureTextEntry": false,
            "skin": "txtD7d9e0",
            "textInputMode": constants.TEXTBOX_INPUT_MODE_ANY,
            "top": "30px",
            "zIndex": 1
        }, controller.args[0], "txtProductGroupName"), extendConfig({
            "containerHeightMode": constants.TEXTBOX_FONT_METRICS_DRIVEN_HEIGHT,
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [2, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "txtProductGroupName"), extendConfig({
            "autoCorrect": false,
            "placeholderSkin": "sknTextGreyb2bdcd"
        }, controller.args[2], "txtProductGroupName"));
        var ProductGroupNameError = new com.adminConsole.loans.applications.errorMsg(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
            "id": "ProductGroupNameError",
            "isVisible": false,
            "layoutType": kony.flex.FLOW_HORIZONTAL,
            "left": "0px",
            "masterType": constants.MASTER_TYPE_DEFAULT,
            "isModalContainer": false,
            "right": "15px",
            "skin": "slFbox",
            "top": "77px",
            "overrides": {
                "errorMsg": {
                    "isVisible": false,
                    "left": "0px",
                    "right": "15px",
                    "top": "77px",
                    "width": "viz.val_cleared"
                },
                "lblErrorText": {
                    "text": "Enter Product Group Name"
                }
            }
        }, controller.args[0], "ProductGroupNameError"), extendConfig({
            "overrides": {}
        }, controller.args[1], "ProductGroupNameError"), extendConfig({
            "overrides": {}
        }, controller.args[2], "ProductGroupNameError"));
        flxProductGroupName.add(flxAddProductGroupNameHeader, txtProductGroupName, ProductGroupNameError);
        var flxAssociatedProductLine = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
            "clipBounds": true,
            "id": "flxAssociatedProductLine",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "left": "34%",
            "isModalContainer": false,
            "skin": "slFbox",
            "top": "0px",
            "width": "34%"
        }, controller.args[0], "flxAssociatedProductLine"), extendConfig({}, controller.args[1], "flxAssociatedProductLine"), extendConfig({}, controller.args[2], "flxAssociatedProductLine"));
        flxAssociatedProductLine.setDefaultUnit(kony.flex.DP);
        var flxAssociatedProductLineHeader = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
            "clipBounds": true,
            "id": "flxAssociatedProductLineHeader",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "left": "0px",
            "isModalContainer": false,
            "right": "15px",
            "skin": "slFbox",
            "top": "0px"
        }, controller.args[0], "flxAssociatedProductLineHeader"), extendConfig({}, controller.args[1], "flxAssociatedProductLineHeader"), extendConfig({}, controller.args[2], "flxAssociatedProductLineHeader"));
        flxAssociatedProductLineHeader.setDefaultUnit(kony.flex.DP);
        var lblAssociatedProductLineHeader = new kony.ui.Label(extendConfig({
            "id": "lblAssociatedProductLineHeader",
            "isVisible": true,
            "left": "0px",
            "skin": "sknLblLato485c7513px",
            "text": "Associated Product Line",
            "top": "0px",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, controller.args[0], "lblAssociatedProductLineHeader"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblAssociatedProductLineHeader"), extendConfig({}, controller.args[2], "lblAssociatedProductLineHeader"));
        flxAssociatedProductLineHeader.add(lblAssociatedProductLineHeader);
        var lstbxAssociatedProductLine = new kony.ui.ListBox(extendConfig({
            "focusSkin": "sknlbxBgffffffBorder11abebRadius3Px14px",
            "height": "40px",
            "id": "lstbxAssociatedProductLine",
            "isVisible": true,
            "left": "0px",
            "masterData": [
                ["lb1", "Select"],
                ["lb2", "Telengana"],
                ["lb3", "Maharastra"]
            ],
            "right": "15px",
            "selectedKey": "lb1",
            "skin": "sknLbxborderd7d9e03pxradius",
            "top": "30px",
            "zIndex": 1
        }, controller.args[0], "lstbxAssociatedProductLine"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [2, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lstbxAssociatedProductLine"), extendConfig({
            "hoverSkin": "lstboxcursor",
            "multiSelect": false
        }, controller.args[2], "lstbxAssociatedProductLine"));
        var AssociatedProductLineError = new com.adminConsole.loans.applications.errorMsg(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
            "id": "AssociatedProductLineError",
            "isVisible": false,
            "layoutType": kony.flex.FLOW_HORIZONTAL,
            "left": "0px",
            "masterType": constants.MASTER_TYPE_DEFAULT,
            "isModalContainer": false,
            "right": "15px",
            "skin": "slFbox",
            "top": "77px",
            "overrides": {
                "errorMsg": {
                    "isVisible": false,
                    "left": "0px",
                    "right": "15px",
                    "top": "77px",
                    "width": "viz.val_cleared"
                },
                "lblErrorText": {
                    "text": "Select Associated Product Line"
                }
            }
        }, controller.args[0], "AssociatedProductLineError"), extendConfig({
            "overrides": {}
        }, controller.args[1], "AssociatedProductLineError"), extendConfig({
            "overrides": {}
        }, controller.args[2], "AssociatedProductLineError"));
        flxAssociatedProductLine.add(flxAssociatedProductLineHeader, lstbxAssociatedProductLine, AssociatedProductLineError);
        var flxProductGroupRef = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
            "clipBounds": true,
            "id": "flxProductGroupRef",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "left": "68%",
            "isModalContainer": false,
            "right": "0px",
            "skin": "slFbox",
            "top": "0px"
        }, controller.args[0], "flxProductGroupRef"), extendConfig({}, controller.args[1], "flxProductGroupRef"), extendConfig({}, controller.args[2], "flxProductGroupRef"));
        flxProductGroupRef.setDefaultUnit(kony.flex.DP);
        var flxProductGroupRefHeader = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
            "clipBounds": true,
            "id": "flxProductGroupRefHeader",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "left": "0px",
            "isModalContainer": false,
            "right": "0px",
            "skin": "slFbox",
            "top": "0px"
        }, controller.args[0], "flxProductGroupRefHeader"), extendConfig({}, controller.args[1], "flxProductGroupRefHeader"), extendConfig({}, controller.args[2], "flxProductGroupRefHeader"));
        flxProductGroupRefHeader.setDefaultUnit(kony.flex.DP);
        var lblProductGroupRefHeader = new kony.ui.Label(extendConfig({
            "id": "lblProductGroupRefHeader",
            "isVisible": true,
            "left": "0dp",
            "skin": "sknLblLato485c7513px",
            "text": "Product Group Reference",
            "top": "0dp",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, controller.args[0], "lblProductGroupRefHeader"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblProductGroupRefHeader"), extendConfig({}, controller.args[2], "lblProductGroupRefHeader"));
        var lblProductGroupRefSize = new kony.ui.Label(extendConfig({
            "id": "lblProductGroupRefSize",
            "isVisible": false,
            "right": "0dp",
            "skin": "sknLblLato485c7513px",
            "text": "0/50",
            "top": "0dp",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, controller.args[0], "lblProductGroupRefSize"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblProductGroupRefSize"), extendConfig({}, controller.args[2], "lblProductGroupRefSize"));
        flxProductGroupRefHeader.add(lblProductGroupRefHeader, lblProductGroupRefSize);
        var txtProductGroupRef = new kony.ui.TextBox2(extendConfig({
            "autoCapitalize": constants.TEXTBOX_AUTO_CAPITALIZE_NONE,
            "focusSkin": "skntxtbxDetailsFocus14px",
            "height": "40dp",
            "id": "txtProductGroupRef",
            "isVisible": true,
            "keyBoardStyle": constants.TEXTBOX_KEY_BOARD_STYLE_DEFAULT,
            "left": "0px",
            "maxTextLength": 50,
            "placeholder": "Product Group Reference",
            "right": "0px",
            "secureTextEntry": false,
            "skin": "txtD7d9e0",
            "textInputMode": constants.TEXTBOX_INPUT_MODE_ANY,
            "top": "30px",
            "zIndex": 1
        }, controller.args[0], "txtProductGroupRef"), extendConfig({
            "containerHeightMode": constants.TEXTBOX_FONT_METRICS_DRIVEN_HEIGHT,
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [2, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "txtProductGroupRef"), extendConfig({
            "autoCorrect": false,
            "placeholderSkin": "sknTextGreyb2bdcd"
        }, controller.args[2], "txtProductGroupRef"));
        var ProductGroupRefError = new com.adminConsole.loans.applications.errorMsg(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
            "id": "ProductGroupRefError",
            "isVisible": false,
            "layoutType": kony.flex.FLOW_HORIZONTAL,
            "left": "0px",
            "masterType": constants.MASTER_TYPE_DEFAULT,
            "isModalContainer": false,
            "right": "0px",
            "skin": "slFbox",
            "top": "77px",
            "overrides": {
                "errorMsg": {
                    "isVisible": false,
                    "left": "0px",
                    "right": "0px",
                    "top": "77px",
                    "width": "viz.val_cleared"
                },
                "lblErrorText": {
                    "text": "Enter Product Group Reference"
                }
            }
        }, controller.args[0], "ProductGroupRefError"), extendConfig({
            "overrides": {}
        }, controller.args[1], "ProductGroupRefError"), extendConfig({
            "overrides": {}
        }, controller.args[2], "ProductGroupRefError"));
        flxProductGroupRef.add(flxProductGroupRefHeader, txtProductGroupRef, ProductGroupRefError);
        flxAddProductGroupTop.add(flxProductGroupName, flxAssociatedProductLine, flxProductGroupRef);
        var flxAddProductGroupDesc = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
            "clipBounds": true,
            "id": "flxAddProductGroupDesc",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "left": "20px",
            "isModalContainer": false,
            "right": "20px",
            "skin": "slFbox",
            "top": "100px"
        }, controller.args[0], "flxAddProductGroupDesc"), extendConfig({}, controller.args[1], "flxAddProductGroupDesc"), extendConfig({}, controller.args[2], "flxAddProductGroupDesc"));
        flxAddProductGroupDesc.setDefaultUnit(kony.flex.DP);
        var flxProductGroupDescHeader = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
            "clipBounds": true,
            "id": "flxProductGroupDescHeader",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "left": "0px",
            "isModalContainer": false,
            "skin": "slFbox",
            "top": "0px",
            "width": "100%"
        }, controller.args[0], "flxProductGroupDescHeader"), extendConfig({}, controller.args[1], "flxProductGroupDescHeader"), extendConfig({}, controller.args[2], "flxProductGroupDescHeader"));
        flxProductGroupDescHeader.setDefaultUnit(kony.flex.DP);
        var lblProductGroupDescHeader = new kony.ui.Label(extendConfig({
            "id": "lblProductGroupDescHeader",
            "isVisible": true,
            "left": "0dp",
            "skin": "sknLblLato485c7513px",
            "i18n_text": "kony.i18n.getLocalizedString(\"i18n.common.description\")",
            "top": "0dp",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, controller.args[0], "lblProductGroupDescHeader"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblProductGroupDescHeader"), extendConfig({}, controller.args[2], "lblProductGroupDescHeader"));
        var lblProductGroupDescSize = new kony.ui.Label(extendConfig({
            "id": "lblProductGroupDescSize",
            "isVisible": false,
            "right": "0dp",
            "skin": "sknLblLato485c7513px",
            "text": "0/150",
            "top": "0dp",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, controller.args[0], "lblProductGroupDescSize"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblProductGroupDescSize"), extendConfig({}, controller.args[2], "lblProductGroupDescSize"));
        flxProductGroupDescHeader.add(lblProductGroupDescHeader, lblProductGroupDescSize);
        var txtProductGroupDesc = new kony.ui.TextArea2(extendConfig({
            "autoCapitalize": constants.TEXTAREA_AUTO_CAPITALIZE_NONE,
            "focusSkin": "sknFocus",
            "height": "70px",
            "id": "txtProductGroupDesc",
            "isVisible": true,
            "keyBoardStyle": constants.TEXTAREA_KEY_BOARD_STYLE_DEFAULT,
            "left": "0px",
            "maxTextLength": 150,
            "numberOfVisibleLines": 3,
            "i18n_placeholder": "kony.i18n.getLocalizedString(\"i18n.common.description\")",
            "skin": "skntxtAread7d9e0",
            "textInputMode": constants.TEXTAREA_INPUT_MODE_ANY,
            "top": "30px",
            "width": "100%",
            "zIndex": 1
        }, controller.args[0], "txtProductGroupDesc"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_TOP_LEFT,
            "padding": [2, 2, 2, 2],
            "paddingInPixel": false
        }, controller.args[1], "txtProductGroupDesc"), extendConfig({
            "autoCorrect": false,
            "placeholderSkin": "defTextAreaFocus"
        }, controller.args[2], "txtProductGroupDesc"));
        var ProductGroupDescError = new com.adminConsole.loans.applications.errorMsg(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
            "id": "ProductGroupDescError",
            "isVisible": false,
            "layoutType": kony.flex.FLOW_HORIZONTAL,
            "left": "0px",
            "masterType": constants.MASTER_TYPE_DEFAULT,
            "isModalContainer": false,
            "skin": "slFbox",
            "top": "110px",
            "width": "100%",
            "overrides": {
                "errorMsg": {
                    "isVisible": false,
                    "left": "0px",
                    "right": "viz.val_cleared",
                    "top": "110px",
                    "width": "100%"
                },
                "lblErrorText": {
                    "i18n_text": "kony.i18n.getLocalizedString(\"i18n.ProfileManagement.errMsgFieldCantEmpty\")"
                }
            }
        }, controller.args[0], "ProductGroupDescError"), extendConfig({
            "overrides": {}
        }, controller.args[1], "ProductGroupDescError"), extendConfig({
            "overrides": {}
        }, controller.args[2], "ProductGroupDescError"));
        flxAddProductGroupDesc.add(flxProductGroupDescHeader, txtProductGroupDesc, ProductGroupDescError);
        var flxProductGroupDetailedDesc = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
            "clipBounds": true,
            "id": "flxProductGroupDetailedDesc",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "left": "20px",
            "isModalContainer": false,
            "right": "20px",
            "skin": "slFbox",
            "top": "230px"
        }, controller.args[0], "flxProductGroupDetailedDesc"), extendConfig({}, controller.args[1], "flxProductGroupDetailedDesc"), extendConfig({}, controller.args[2], "flxProductGroupDetailedDesc"));
        flxProductGroupDetailedDesc.setDefaultUnit(kony.flex.DP);
        var flxProductGroupDetailedDescHeader = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
            "clipBounds": true,
            "id": "flxProductGroupDetailedDescHeader",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "left": "0px",
            "isModalContainer": false,
            "skin": "slFbox",
            "top": "0px",
            "width": "100%"
        }, controller.args[0], "flxProductGroupDetailedDescHeader"), extendConfig({}, controller.args[1], "flxProductGroupDetailedDescHeader"), extendConfig({}, controller.args[2], "flxProductGroupDetailedDescHeader"));
        flxProductGroupDetailedDescHeader.setDefaultUnit(kony.flex.DP);
        var lblProductGroupDetailedDescHeader = new kony.ui.Label(extendConfig({
            "id": "lblProductGroupDetailedDescHeader",
            "isVisible": true,
            "left": "0dp",
            "skin": "sknLblLato485c7513px",
            "text": "Detailed Description ",
            "top": "0dp",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, controller.args[0], "lblProductGroupDetailedDescHeader"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblProductGroupDetailedDescHeader"), extendConfig({}, controller.args[2], "lblProductGroupDetailedDescHeader"));
        var lblProductGroupDetailedDescSize = new kony.ui.Label(extendConfig({
            "id": "lblProductGroupDetailedDescSize",
            "isVisible": false,
            "right": "0dp",
            "skin": "sknLblLato485c7513px",
            "text": "0/250",
            "top": "0dp",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, controller.args[0], "lblProductGroupDetailedDescSize"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblProductGroupDetailedDescSize"), extendConfig({}, controller.args[2], "lblProductGroupDetailedDescSize"));
        flxProductGroupDetailedDescHeader.add(lblProductGroupDetailedDescHeader, lblProductGroupDetailedDescSize);
        var txtProductGroupDetailedDesc = new kony.ui.TextArea2(extendConfig({
            "autoCapitalize": constants.TEXTAREA_AUTO_CAPITALIZE_NONE,
            "focusSkin": "sknFocus",
            "height": "130px",
            "id": "txtProductGroupDetailedDesc",
            "isVisible": true,
            "keyBoardStyle": constants.TEXTAREA_KEY_BOARD_STYLE_DEFAULT,
            "left": "0px",
            "maxTextLength": 250,
            "numberOfVisibleLines": 3,
            "placeholder": "Detailed Description ",
            "skin": "skntxtAread7d9e0",
            "textInputMode": constants.TEXTAREA_INPUT_MODE_ANY,
            "top": "30px",
            "width": "100%",
            "zIndex": 1
        }, controller.args[0], "txtProductGroupDetailedDesc"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_TOP_LEFT,
            "padding": [2, 2, 2, 2],
            "paddingInPixel": false
        }, controller.args[1], "txtProductGroupDetailedDesc"), extendConfig({
            "autoCorrect": false,
            "placeholderSkin": "defTextAreaFocus"
        }, controller.args[2], "txtProductGroupDetailedDesc"));
        var ProductGroupDetailedDescError = new com.adminConsole.loans.applications.errorMsg(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
            "id": "ProductGroupDetailedDescError",
            "isVisible": false,
            "layoutType": kony.flex.FLOW_HORIZONTAL,
            "left": "0px",
            "masterType": constants.MASTER_TYPE_DEFAULT,
            "isModalContainer": false,
            "skin": "slFbox",
            "top": "167px",
            "width": "100%",
            "overrides": {
                "errorMsg": {
                    "isVisible": false,
                    "left": "0px",
                    "right": "viz.val_cleared",
                    "top": "167px",
                    "width": "100%"
                },
                "lblErrorText": {
                    "i18n_text": "kony.i18n.getLocalizedString(\"i18n.ProfileManagement.errMsgFieldCantEmpty\")"
                }
            }
        }, controller.args[0], "ProductGroupDetailedDescError"), extendConfig({
            "overrides": {}
        }, controller.args[1], "ProductGroupDetailedDescError"), extendConfig({
            "overrides": {}
        }, controller.args[2], "ProductGroupDetailedDescError"));
        flxProductGroupDetailedDesc.add(flxProductGroupDetailedDescHeader, txtProductGroupDetailedDesc, ProductGroupDetailedDescError);
        var flxProductGroupImageDetails = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
            "clipBounds": true,
            "id": "flxProductGroupImageDetails",
            "isVisible": true,
            "layoutType": kony.flex.FLOW_VERTICAL,
            "left": "20px",
            "isModalContainer": false,
            "right": "20px",
            "skin": "slFbox",
            "top": "420px",
            "zIndex": 1
        }, controller.args[0], "flxProductGroupImageDetails"), extendConfig({}, controller.args[1], "flxProductGroupImageDetails"), extendConfig({}, controller.args[2], "flxProductGroupImageDetails"));
        flxProductGroupImageDetails.setDefaultUnit(kony.flex.DP);
        var flxImgDetailsHeader = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
            "clipBounds": true,
            "id": "flxImgDetailsHeader",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "left": "0dp",
            "isModalContainer": false,
            "skin": "slFbox",
            "top": "0dp",
            "width": "100%"
        }, controller.args[0], "flxImgDetailsHeader"), extendConfig({}, controller.args[1], "flxImgDetailsHeader"), extendConfig({}, controller.args[2], "flxImgDetailsHeader"));
        flxImgDetailsHeader.setDefaultUnit(kony.flex.DP);
        var lblImgType = new kony.ui.Label(extendConfig({
            "id": "lblImgType",
            "isVisible": true,
            "left": "0px",
            "skin": "sknLblLato485c7513px",
            "text": "Image Type",
            "top": "0px",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, controller.args[0], "lblImgType"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblImgType"), extendConfig({}, controller.args[2], "lblImgType"));
        var lblImgURL = new kony.ui.Label(extendConfig({
            "id": "lblImgURL",
            "isVisible": true,
            "left": "305px",
            "skin": "sknLblLato485c7513px",
            "text": "Image URL",
            "top": "0px",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, controller.args[0], "lblImgURL"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblImgURL"), extendConfig({}, controller.args[2], "lblImgURL"));
        var fontIconInfo = new kony.ui.Label(extendConfig({
            "id": "fontIconInfo",
            "isVisible": false,
            "left": "45%",
            "skin": "sknfontIcon33333316px",
            "text": "",
            "top": "0px",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, controller.args[0], "fontIconInfo"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "fontIconInfo"), extendConfig({}, controller.args[2], "fontIconInfo"));
        flxImgDetailsHeader.add(lblImgType, lblImgURL, fontIconInfo);
        var segImgDetails = new kony.ui.SegmentedUI2(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
            "data": [{
                "lblDelete": "kony.i18n.getLocalizedString(\"i18n.frmFAQ.fonticonDelete\")",
                "lblImgTypeErrorIcon": "",
                "lblImgTypeErrorText": "Error Message",
                "lblImgURLErrorIcon": "",
                "lblImgURLErrorText": "Error Message",
                "lstbxImgType": {
                    "masterData": [
                        ["lb1", "Select"],
                        ["lb2", "Telengana"],
                        ["lb3", "Maharastra"]
                    ],
                    "selectedKey": "lb1",
                    "selectedKeys": null,
                    "selectedKeyValue": ["lb1", "Select"]
                },
                "txtImgURL": ""
            }, {
                "lblDelete": "kony.i18n.getLocalizedString(\"i18n.frmFAQ.fonticonDelete\")",
                "lblImgTypeErrorIcon": "",
                "lblImgTypeErrorText": "Error Message",
                "lblImgURLErrorIcon": "",
                "lblImgURLErrorText": "Error Message",
                "lstbxImgType": {
                    "masterData": [
                        ["lb1", "Select"],
                        ["lb2", "Telengana"],
                        ["lb3", "Maharastra"]
                    ],
                    "selectedKey": "lb1",
                    "selectedKeys": null,
                    "selectedKeyValue": ["lb1", "Select"]
                },
                "txtImgURL": ""
            }, {
                "lblDelete": "kony.i18n.getLocalizedString(\"i18n.frmFAQ.fonticonDelete\")",
                "lblImgTypeErrorIcon": "",
                "lblImgTypeErrorText": "Error Message",
                "lblImgURLErrorIcon": "",
                "lblImgURLErrorText": "Error Message",
                "lstbxImgType": {
                    "masterData": [
                        ["lb1", "Select"],
                        ["lb2", "Telengana"],
                        ["lb3", "Maharastra"]
                    ],
                    "selectedKey": "lb1",
                    "selectedKeys": null,
                    "selectedKeyValue": ["lb1", "Select"]
                },
                "txtImgURL": ""
            }],
            "groupCells": false,
            "id": "segImgDetails",
            "isVisible": true,
            "left": "0px",
            "needPageIndicator": true,
            "pageOffDotImage": "pageoffdot.png",
            "pageOnDotImage": "pageondot.png",
            "retainSelection": false,
            "right": "0px",
            "rowFocusSkin": "seg2Focus",
            "rowSkin": "sknseg3pxRadius",
            "rowTemplate": "flxImgDetails",
            "sectionHeaderSkin": "sliPhoneSegmentHeader",
            "selectionBehavior": constants.SEGUI_DEFAULT_BEHAVIOR,
            "separatorRequired": false,
            "showScrollbars": false,
            "top": "30px",
            "viewType": constants.SEGUI_VIEW_TYPE_TABLEVIEW,
            "widgetDataMap": {
                "flxDelete": "flxDelete",
                "flxImgDetails": "flxImgDetails",
                "flxImgType": "flxImgType",
                "flxImgTypeError": "flxImgTypeError",
                "flxImgURL": "flxImgURL",
                "flxImgURLError": "flxImgURLError",
                "lblDelete": "lblDelete",
                "lblImgTypeErrorIcon": "lblImgTypeErrorIcon",
                "lblImgTypeErrorText": "lblImgTypeErrorText",
                "lblImgURLErrorIcon": "lblImgURLErrorIcon",
                "lblImgURLErrorText": "lblImgURLErrorText",
                "lstbxImgType": "lstbxImgType",
                "txtImgURL": "txtImgURL"
            },
            "zIndex": 1
        }, controller.args[0], "segImgDetails"), extendConfig({
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "segImgDetails"), extendConfig({}, controller.args[2], "segImgDetails"));
        var flxAddImgDetail = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
            "clipBounds": true,
            "id": "flxAddImgDetail",
            "isVisible": true,
            "layoutType": kony.flex.FLOW_VERTICAL,
            "left": "0dp",
            "isModalContainer": false,
            "skin": "hoverhandSkin",
            "top": "7dp",
            "width": "80px"
        }, controller.args[0], "flxAddImgDetail"), extendConfig({}, controller.args[1], "flxAddImgDetail"), extendConfig({}, controller.args[2], "flxAddImgDetail"));
        flxAddImgDetail.setDefaultUnit(kony.flex.DP);
        var lblAddImgDetail = new kony.ui.Label(extendConfig({
            "id": "lblAddImgDetail",
            "isVisible": true,
            "left": "0px",
            "skin": "sknLblLatoReg117eb013px",
            "text": "+ Add Image",
            "top": "0px",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, controller.args[0], "lblAddImgDetail"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblAddImgDetail"), extendConfig({}, controller.args[2], "lblAddImgDetail"));
        flxAddImgDetail.add(lblAddImgDetail);
        flxProductGroupImageDetails.add(flxImgDetailsHeader, segImgDetails, flxAddImgDetail);
        flxAddProductGroupScrollConatiner.add(flxAddProductGroupTop, flxAddProductGroupDesc, flxProductGroupDetailedDesc, flxProductGroupImageDetails);
        var flxAddProductGroupPopupButtons = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "bottom": "0dp",
            "clipBounds": true,
            "height": "80px",
            "id": "flxAddProductGroupPopupButtons",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "left": "0dp",
            "isModalContainer": false,
            "skin": "sknflxBgE4E6EC",
            "width": "100%",
            "zIndex": 1
        }, controller.args[0], "flxAddProductGroupPopupButtons"), extendConfig({}, controller.args[1], "flxAddProductGroupPopupButtons"), extendConfig({}, controller.args[2], "flxAddProductGroupPopupButtons"));
        flxAddProductGroupPopupButtons.setDefaultUnit(kony.flex.DP);
        var btnAddProductGroupCancel = new kony.ui.Button(extendConfig({
            "centerY": "50%",
            "height": "40px",
            "id": "btnAddProductGroupCancel",
            "isVisible": true,
            "left": "20px",
            "skin": "sknbtnf7f7faLatoRegular4f555dBorder1px8b96a5",
            "i18n_text": "kony.i18n.getLocalizedString(\"i18n.permission.CANCEL\")",
            "top": "0%",
            "width": "105px",
            "zIndex": 20
        }, controller.args[0], "btnAddProductGroupCancel"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_CENTER,
            "displayText": true,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "btnAddProductGroupCancel"), extendConfig({
            "hoverSkin": "sknbtnffffffLatoRegular4f555dBorder1px485c75"
        }, controller.args[2], "btnAddProductGroupCancel"));
        var btnAddProductGroup = new kony.ui.Button(extendConfig({
            "centerY": "50%",
            "focusSkin": "sknBtn003E75LatoRegular13pxFFFFFFRad20px",
            "height": "40px",
            "id": "btnAddProductGroup",
            "isVisible": true,
            "right": "20px",
            "skin": "sknBtn003E75LatoRegular13pxFFFFFFRad20px",
            "text": "ADD PRODUCT GROUP",
            "width": "210px",
            "zIndex": 20
        }, controller.args[0], "btnAddProductGroup"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_CENTER,
            "displayText": true,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "btnAddProductGroup"), extendConfig({
            "hoverSkin": "sknBtn005198LatoRegular13pxFFFFFFRad20px"
        }, controller.args[2], "btnAddProductGroup"));
        flxAddProductGroupPopupButtons.add(btnAddProductGroupCancel, btnAddProductGroup);
        flxAddProductGroup.add(flxAddProductGroupPopupTopBar, flxAddProductGroupPopupClose, lblAddProductGroupPopupHeader, flxAddProductGroupScrollConatiner, flxAddProductGroupPopupButtons);
        var flxViewProductGroup = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "centerY": "50%",
            "clipBounds": false,
            "height": "90%",
            "id": "flxViewProductGroup",
            "isVisible": false,
            "layoutType": kony.flex.FREE_FORM,
            "left": "20%",
            "isModalContainer": false,
            "right": "20%",
            "skin": "flxNoShadowNoBorder",
            "zIndex": 10
        }, controller.args[0], "flxViewProductGroup"), extendConfig({}, controller.args[1], "flxViewProductGroup"), extendConfig({}, controller.args[2], "flxViewProductGroup"));
        flxViewProductGroup.setDefaultUnit(kony.flex.DP);
        var flxViewProductGroupTopBar = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "clipBounds": true,
            "height": "10px",
            "id": "flxViewProductGroupTopBar",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "left": "0dp",
            "isModalContainer": false,
            "skin": "sknFlxInfoToastBg357C9ENoRadius",
            "top": "0dp",
            "width": "100%",
            "zIndex": 1
        }, controller.args[0], "flxViewProductGroupTopBar"), extendConfig({}, controller.args[1], "flxViewProductGroupTopBar"), extendConfig({}, controller.args[2], "flxViewProductGroupTopBar"));
        flxViewProductGroupTopBar.setDefaultUnit(kony.flex.DP);
        flxViewProductGroupTopBar.add();
        var flxViewProductGroupPopupClose = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "clipBounds": true,
            "height": "15px",
            "id": "flxViewProductGroupPopupClose",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "isModalContainer": false,
            "right": 20,
            "skin": "hoverhandSkin",
            "top": "21dp",
            "width": "15px",
            "zIndex": 1
        }, controller.args[0], "flxViewProductGroupPopupClose"), extendConfig({}, controller.args[1], "flxViewProductGroupPopupClose"), extendConfig({
            "hoverSkin": "sknCursor"
        }, controller.args[2], "flxViewProductGroupPopupClose"));
        flxViewProductGroupPopupClose.setDefaultUnit(kony.flex.DP);
        var lblViewPGPopupClose = new kony.ui.Label(extendConfig({
            "height": "15dp",
            "id": "lblViewPGPopupClose",
            "isVisible": true,
            "left": "0dp",
            "skin": "sknfontIconListbox15px",
            "i18n_text": "kony.i18n.getLocalizedString(\"i18n.userwidgetmodel.lblClearSearch\")",
            "top": "0dp",
            "width": "15dp",
            "zIndex": 1
        }, controller.args[0], "lblViewPGPopupClose"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblViewPGPopupClose"), extendConfig({}, controller.args[2], "lblViewPGPopupClose"));
        flxViewProductGroupPopupClose.add(lblViewPGPopupClose);
        var lbViewProductGroupPopupHeader = new kony.ui.Label(extendConfig({
            "height": "19dp",
            "id": "lbViewProductGroupPopupHeader",
            "isVisible": true,
            "left": "20px",
            "skin": "sknLbl192b45LatoReg16px",
            "text": "Product Group Name",
            "top": "52px",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, controller.args[0], "lbViewProductGroupPopupHeader"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lbViewProductGroupPopupHeader"), extendConfig({}, controller.args[2], "lbViewProductGroupPopupHeader"));
        var btnEditProductGroup = new kony.ui.Button(extendConfig({
            "focusSkin": "sknbtnffffffLatoRegular4f555dBorder1px485c75",
            "height": "22px",
            "id": "btnEditProductGroup",
            "isVisible": true,
            "right": "20px",
            "skin": "sknbtnf7f7faLatoReg13px485c75Rad20px",
            "text": "Edit",
            "top": "50px",
            "width": "53px",
            "zIndex": 1
        }, controller.args[0], "btnEditProductGroup"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_CENTER,
            "displayText": true,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "btnEditProductGroup"), extendConfig({
            "hoverSkin": "sknbtnffffffLatoRegular4f555dBorder1px485c75"
        }, controller.args[2], "btnEditProductGroup"));
        var lblSeparatorViewPGHeader = new kony.ui.Label(extendConfig({
            "height": "1px",
            "id": "lblSeparatorViewPGHeader",
            "isVisible": true,
            "left": "20px",
            "right": "20px",
            "skin": "sknlblSeperatorD7D9E0",
            "text": "Label",
            "top": "90px",
            "zIndex": 2
        }, controller.args[0], "lblSeparatorViewPGHeader"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_TOP_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblSeparatorViewPGHeader"), extendConfig({}, controller.args[2], "lblSeparatorViewPGHeader"));
        var flxProductGroupDetailsScrollContainer = new kony.ui.FlexScrollContainer(extendConfig({
            "allowHorizontalBounce": false,
            "allowVerticalBounce": true,
            "bottom": "0px",
            "bounces": true,
            "clipBounds": true,
            "enableScrolling": true,
            "horizontalScrollIndicator": true,
            "id": "flxProductGroupDetailsScrollContainer",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "left": "0px",
            "pagingEnabled": false,
            "scrollDirection": kony.flex.SCROLL_VERTICAL,
            "skin": "slFSbox",
            "top": "115px",
            "verticalScrollIndicator": true,
            "width": "100%"
        }, controller.args[0], "flxProductGroupDetailsScrollContainer"), extendConfig({}, controller.args[1], "flxProductGroupDetailsScrollContainer"), extendConfig({}, controller.args[2], "flxProductGroupDetailsScrollContainer"));
        flxProductGroupDetailsScrollContainer.setDefaultUnit(kony.flex.DP);
        var flxProductGroupDetailsInner = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
            "clipBounds": true,
            "id": "flxProductGroupDetailsInner",
            "isVisible": true,
            "layoutType": kony.flex.FLOW_VERTICAL,
            "left": "20px",
            "isModalContainer": false,
            "right": "20px",
            "skin": "slFbox",
            "top": "0px"
        }, controller.args[0], "flxProductGroupDetailsInner"), extendConfig({}, controller.args[1], "flxProductGroupDetailsInner"), extendConfig({}, controller.args[2], "flxProductGroupDetailsInner"));
        flxProductGroupDetailsInner.setDefaultUnit(kony.flex.DP);
        var flxViewProductGroupTop = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
            "clipBounds": true,
            "id": "flxViewProductGroupTop",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "left": "0px",
            "isModalContainer": false,
            "skin": "slFbox",
            "top": "0px",
            "width": "100%",
            "zIndex": 1
        }, controller.args[0], "flxViewProductGroupTop"), extendConfig({}, controller.args[1], "flxViewProductGroupTop"), extendConfig({}, controller.args[2], "flxViewProductGroupTop"));
        flxViewProductGroupTop.setDefaultUnit(kony.flex.DP);
        var flxViewAssociatedProductLine = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
            "clipBounds": true,
            "id": "flxViewAssociatedProductLine",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "left": "0px",
            "isModalContainer": false,
            "skin": "slFbox",
            "top": "0px",
            "width": "50%"
        }, controller.args[0], "flxViewAssociatedProductLine"), extendConfig({}, controller.args[1], "flxViewAssociatedProductLine"), extendConfig({}, controller.args[2], "flxViewAssociatedProductLine"));
        flxViewAssociatedProductLine.setDefaultUnit(kony.flex.DP);
        var lblViewAssociatedProductLineHeader = new kony.ui.Label(extendConfig({
            "id": "lblViewAssociatedProductLineHeader",
            "isVisible": true,
            "left": "0dp",
            "skin": "sknlblLato696c7311px",
            "text": "ASSOCIATED PRODUCT LINE",
            "top": "0dp",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, controller.args[0], "lblViewAssociatedProductLineHeader"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblViewAssociatedProductLineHeader"), extendConfig({}, controller.args[2], "lblViewAssociatedProductLineHeader"));
        var lblViewAssociatedProductLine = new kony.ui.Label(extendConfig({
            "id": "lblViewAssociatedProductLine",
            "isVisible": true,
            "left": "0px",
            "right": "15px",
            "skin": "sknLblLato485c7513px",
            "text": "Associated Product Line",
            "top": "30px",
            "zIndex": 1
        }, controller.args[0], "lblViewAssociatedProductLine"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblViewAssociatedProductLine"), extendConfig({}, controller.args[2], "lblViewAssociatedProductLine"));
        flxViewAssociatedProductLine.add(lblViewAssociatedProductLineHeader, lblViewAssociatedProductLine);
        var flxViewProductGroupRef = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
            "clipBounds": true,
            "id": "flxViewProductGroupRef",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "left": "50%",
            "isModalContainer": false,
            "right": "0px",
            "skin": "slFbox",
            "top": "0px"
        }, controller.args[0], "flxViewProductGroupRef"), extendConfig({}, controller.args[1], "flxViewProductGroupRef"), extendConfig({}, controller.args[2], "flxViewProductGroupRef"));
        flxViewProductGroupRef.setDefaultUnit(kony.flex.DP);
        var lblViewProductGroupRefHeader = new kony.ui.Label(extendConfig({
            "id": "lblViewProductGroupRefHeader",
            "isVisible": true,
            "left": "0dp",
            "skin": "sknlblLato696c7311px",
            "text": "PRODUCT GROUP REFERENCE",
            "top": "0dp",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, controller.args[0], "lblViewProductGroupRefHeader"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblViewProductGroupRefHeader"), extendConfig({}, controller.args[2], "lblViewProductGroupRefHeader"));
        var lblViewProductGroupRef = new kony.ui.Label(extendConfig({
            "id": "lblViewProductGroupRef",
            "isVisible": true,
            "left": "0px",
            "right": "15px",
            "skin": "sknLblLato485c7513px",
            "text": "Product Group Reference",
            "top": "30px",
            "zIndex": 1
        }, controller.args[0], "lblViewProductGroupRef"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblViewProductGroupRef"), extendConfig({}, controller.args[2], "lblViewProductGroupRef"));
        flxViewProductGroupRef.add(lblViewProductGroupRefHeader, lblViewProductGroupRef);
        flxViewProductGroupTop.add(flxViewAssociatedProductLine, flxViewProductGroupRef);
        var flxViewProductGroupDesc = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
            "clipBounds": true,
            "id": "flxViewProductGroupDesc",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "left": "0px",
            "isModalContainer": false,
            "skin": "slFbox",
            "top": "35px",
            "width": "100%"
        }, controller.args[0], "flxViewProductGroupDesc"), extendConfig({}, controller.args[1], "flxViewProductGroupDesc"), extendConfig({}, controller.args[2], "flxViewProductGroupDesc"));
        flxViewProductGroupDesc.setDefaultUnit(kony.flex.DP);
        var lblViewProductGroupDescHeader = new kony.ui.Label(extendConfig({
            "id": "lblViewProductGroupDescHeader",
            "isVisible": true,
            "left": "0dp",
            "skin": "sknlblLato696c7311px",
            "text": "DESCRIPTION",
            "top": "0dp",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, controller.args[0], "lblViewProductGroupDescHeader"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblViewProductGroupDescHeader"), extendConfig({}, controller.args[2], "lblViewProductGroupDescHeader"));
        var lblViewProductGroupDesc = new kony.ui.Label(extendConfig({
            "id": "lblViewProductGroupDesc",
            "isVisible": true,
            "left": "0px",
            "right": "15px",
            "skin": "sknLblLato485c7513px",
            "text": "Home equity loans allow homeowners to borrow against the equity in their residence.Home equity loan amounts are based on the difference between a home's current market value and the mortgage balance due.",
            "top": "30px",
            "zIndex": 1
        }, controller.args[0], "lblViewProductGroupDesc"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblViewProductGroupDesc"), extendConfig({}, controller.args[2], "lblViewProductGroupDesc"));
        flxViewProductGroupDesc.add(lblViewProductGroupDescHeader, lblViewProductGroupDesc);
        var flxViewProductGroupDetailedDesc = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
            "clipBounds": true,
            "id": "flxViewProductGroupDetailedDesc",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "left": "0px",
            "isModalContainer": false,
            "skin": "slFbox",
            "top": "35px",
            "width": "100%"
        }, controller.args[0], "flxViewProductGroupDetailedDesc"), extendConfig({}, controller.args[1], "flxViewProductGroupDetailedDesc"), extendConfig({}, controller.args[2], "flxViewProductGroupDetailedDesc"));
        flxViewProductGroupDetailedDesc.setDefaultUnit(kony.flex.DP);
        var lblViewProductGroupDetailedDescHeader = new kony.ui.Label(extendConfig({
            "id": "lblViewProductGroupDetailedDescHeader",
            "isVisible": true,
            "left": "0dp",
            "skin": "sknlblLato696c7311px",
            "text": "DETAILED DESCRIPTION",
            "top": "0dp",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, controller.args[0], "lblViewProductGroupDetailedDescHeader"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblViewProductGroupDetailedDescHeader"), extendConfig({}, controller.args[2], "lblViewProductGroupDetailedDescHeader"));
        var lblViewProductGroupDetailedDesc = new kony.ui.Label(extendConfig({
            "id": "lblViewProductGroupDetailedDesc",
            "isVisible": true,
            "left": "0px",
            "right": "15px",
            "skin": "sknLblLato485c7513px",
            "text": "Home equity loans allow homeowners to borrow against the equity in their residence.Home equity loan amounts are based on the difference between a home's current market value and the mortgage balance due.",
            "top": "30px",
            "zIndex": 1
        }, controller.args[0], "lblViewProductGroupDetailedDesc"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblViewProductGroupDetailedDesc"), extendConfig({}, controller.args[2], "lblViewProductGroupDetailedDesc"));
        flxViewProductGroupDetailedDesc.add(lblViewProductGroupDetailedDescHeader, lblViewProductGroupDetailedDesc);
        var flxViewImgDetails = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
            "bottom": "35px",
            "clipBounds": true,
            "id": "flxViewImgDetails",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "left": "0dp",
            "isModalContainer": false,
            "skin": "sknFlxBgFFFFFFBorE4E6ECsz1pxR3px",
            "top": "35dp",
            "width": "100%",
            "zIndex": 1
        }, controller.args[0], "flxViewImgDetails"), extendConfig({}, controller.args[1], "flxViewImgDetails"), extendConfig({}, controller.args[2], "flxViewImgDetails"));
        flxViewImgDetails.setDefaultUnit(kony.flex.DP);
        var flxViewImgDetailsHeaders = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
            "clipBounds": true,
            "id": "flxViewImgDetailsHeaders",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "left": "0px",
            "isModalContainer": false,
            "right": "0px",
            "skin": "slFbox",
            "top": "0px",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, controller.args[0], "flxViewImgDetailsHeaders"), extendConfig({}, controller.args[1], "flxViewImgDetailsHeaders"), extendConfig({}, controller.args[2], "flxViewImgDetailsHeaders"));
        flxViewImgDetailsHeaders.setDefaultUnit(kony.flex.DP);
        var lblImgTypeHeader = new kony.ui.Label(extendConfig({
            "id": "lblImgTypeHeader",
            "isVisible": true,
            "left": "12px",
            "skin": "sknlblLato696c7311px",
            "text": "IMAGE TYPE",
            "top": "16px",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, controller.args[0], "lblImgTypeHeader"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblImgTypeHeader"), extendConfig({}, controller.args[2], "lblImgTypeHeader"));
        var lblImgURLHeader = new kony.ui.Label(extendConfig({
            "id": "lblImgURLHeader",
            "isVisible": true,
            "left": "26%",
            "skin": "sknlblLato696c7311px",
            "text": "IMAGE URL",
            "top": "16px",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, controller.args[0], "lblImgURLHeader"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblImgURLHeader"), extendConfig({}, controller.args[2], "lblImgURLHeader"));
        var lblSeparatorImgDetails = new kony.ui.Label(extendConfig({
            "height": "1px",
            "id": "lblSeparatorImgDetails",
            "isVisible": true,
            "left": "0px",
            "right": "0px",
            "skin": "sknlblSeperatorD7D9E0",
            "text": "Label",
            "top": "43px",
            "zIndex": 2
        }, controller.args[0], "lblSeparatorImgDetails"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_TOP_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblSeparatorImgDetails"), extendConfig({}, controller.args[2], "lblSeparatorImgDetails"));
        flxViewImgDetailsHeaders.add(lblImgTypeHeader, lblImgURLHeader, lblSeparatorImgDetails);
        var segViewImgDetails = new kony.ui.SegmentedUI2(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
            "data": [{
                "lblImgType": "Product Line",
                "lblImgUrl": "Product Reference",
                "lblSeparator": "Label"
            }, {
                "lblImgType": "Product Line",
                "lblImgUrl": "Product Reference",
                "lblSeparator": "Label"
            }, {
                "lblImgType": "Product Line",
                "lblImgUrl": "Product Reference",
                "lblSeparator": "Label"
            }],
            "groupCells": false,
            "id": "segViewImgDetails",
            "isVisible": true,
            "left": "0px",
            "needPageIndicator": true,
            "pageOffDotImage": "pageoffdot.png",
            "pageOnDotImage": "pageondot.png",
            "retainSelection": false,
            "right": "0px",
            "rowFocusSkin": "seg2Focus",
            "rowSkin": "sknseg3pxRadius",
            "rowTemplate": "flxViewImgDetails",
            "sectionHeaderSkin": "sliPhoneSegmentHeader",
            "selectionBehavior": constants.SEGUI_DEFAULT_BEHAVIOR,
            "separatorRequired": false,
            "showScrollbars": false,
            "top": "44px",
            "viewType": constants.SEGUI_VIEW_TYPE_TABLEVIEW,
            "widgetDataMap": {
                "flxViewImgDetails": "flxViewImgDetails",
                "lblImgType": "lblImgType",
                "lblImgUrl": "lblImgUrl",
                "lblSeparator": "lblSeparator"
            },
            "zIndex": 1
        }, controller.args[0], "segViewImgDetails"), extendConfig({
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "segViewImgDetails"), extendConfig({}, controller.args[2], "segViewImgDetails"));
        var flxNoResultFoundImgDetails = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "clipBounds": true,
            "height": "100px",
            "id": "flxNoResultFoundImgDetails",
            "isVisible": false,
            "layoutType": kony.flex.FREE_FORM,
            "left": "0px",
            "isModalContainer": false,
            "skin": "slFbox",
            "top": "44px",
            "width": "100%",
            "zIndex": 1
        }, controller.args[0], "flxNoResultFoundImgDetails"), extendConfig({}, controller.args[1], "flxNoResultFoundImgDetails"), extendConfig({}, controller.args[2], "flxNoResultFoundImgDetails"));
        flxNoResultFoundImgDetails.setDefaultUnit(kony.flex.DP);
        var lblNoResultFoundImgDetails = new kony.ui.Label(extendConfig({
            "centerX": "50%",
            "centerY": "50%",
            "id": "lblNoResultFoundImgDetails",
            "isVisible": true,
            "skin": "sknLbl485C75LatoRegular13Px",
            "text": "No images found.",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, controller.args[0], "lblNoResultFoundImgDetails"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblNoResultFoundImgDetails"), extendConfig({}, controller.args[2], "lblNoResultFoundImgDetails"));
        flxNoResultFoundImgDetails.add(lblNoResultFoundImgDetails);
        flxViewImgDetails.add(flxViewImgDetailsHeaders, segViewImgDetails, flxNoResultFoundImgDetails);
        flxProductGroupDetailsInner.add(flxViewProductGroupTop, flxViewProductGroupDesc, flxViewProductGroupDetailedDesc, flxViewImgDetails);
        flxProductGroupDetailsScrollContainer.add(flxProductGroupDetailsInner);
        flxViewProductGroup.add(flxViewProductGroupTopBar, flxViewProductGroupPopupClose, lbViewProductGroupPopupHeader, btnEditProductGroup, lblSeparatorViewPGHeader, flxProductGroupDetailsScrollContainer);
        var flxAddFeature = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "centerY": "50%",
            "clipBounds": false,
            "height": "470dp",
            "id": "flxAddFeature",
            "isVisible": false,
            "layoutType": kony.flex.FREE_FORM,
            "left": "14%",
            "isModalContainer": false,
            "right": "14%",
            "skin": "flxNoShadowNoBorder",
            "zIndex": 10
        }, controller.args[0], "flxAddFeature"), extendConfig({}, controller.args[1], "flxAddFeature"), extendConfig({}, controller.args[2], "flxAddFeature"));
        flxAddFeature.setDefaultUnit(kony.flex.DP);
        var flxAddFeaturePopupTopBar = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "clipBounds": true,
            "height": "10px",
            "id": "flxAddFeaturePopupTopBar",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "left": "0dp",
            "isModalContainer": false,
            "skin": "sknFlxInfoToastBg357C9ENoRadius",
            "top": "0dp",
            "width": "100%",
            "zIndex": 1
        }, controller.args[0], "flxAddFeaturePopupTopBar"), extendConfig({}, controller.args[1], "flxAddFeaturePopupTopBar"), extendConfig({}, controller.args[2], "flxAddFeaturePopupTopBar"));
        flxAddFeaturePopupTopBar.setDefaultUnit(kony.flex.DP);
        flxAddFeaturePopupTopBar.add();
        var flxAddFeaturePopupClose = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "clipBounds": true,
            "height": "15px",
            "id": "flxAddFeaturePopupClose",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "isModalContainer": false,
            "right": 20,
            "skin": "hoverhandSkin",
            "top": "30dp",
            "width": "15px",
            "zIndex": 1
        }, controller.args[0], "flxAddFeaturePopupClose"), extendConfig({}, controller.args[1], "flxAddFeaturePopupClose"), extendConfig({
            "hoverSkin": "sknCursor"
        }, controller.args[2], "flxAddFeaturePopupClose"));
        flxAddFeaturePopupClose.setDefaultUnit(kony.flex.DP);
        var lblAddFeaturePopupClose = new kony.ui.Label(extendConfig({
            "height": "15dp",
            "id": "lblAddFeaturePopupClose",
            "isVisible": true,
            "left": "0dp",
            "skin": "sknfontIconListbox15px",
            "i18n_text": "kony.i18n.getLocalizedString(\"i18n.userwidgetmodel.lblClearSearch\")",
            "top": "0dp",
            "width": "15dp",
            "zIndex": 1
        }, controller.args[0], "lblAddFeaturePopupClose"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblAddFeaturePopupClose"), extendConfig({}, controller.args[2], "lblAddFeaturePopupClose"));
        flxAddFeaturePopupClose.add(lblAddFeaturePopupClose);
        var lblAddFeaturePopupHeader = new kony.ui.Label(extendConfig({
            "height": "19dp",
            "id": "lblAddFeaturePopupHeader",
            "isVisible": true,
            "left": "20px",
            "skin": "sknLbl192b45LatoReg16px",
            "text": "Add Product Features",
            "top": "37px",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, controller.args[0], "lblAddFeaturePopupHeader"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblAddFeaturePopupHeader"), extendConfig({}, controller.args[2], "lblAddFeaturePopupHeader"));
        var flxAddFeatureTopDetails = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
            "clipBounds": true,
            "id": "flxAddFeatureTopDetails",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "left": "20px",
            "isModalContainer": false,
            "right": "20px",
            "skin": "slFbox",
            "top": "100px",
            "zIndex": 1
        }, controller.args[0], "flxAddFeatureTopDetails"), extendConfig({}, controller.args[1], "flxAddFeatureTopDetails"), extendConfig({}, controller.args[2], "flxAddFeatureTopDetails"));
        flxAddFeatureTopDetails.setDefaultUnit(kony.flex.DP);
        var flxFeatureName = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
            "clipBounds": true,
            "id": "flxFeatureName",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "left": "0px",
            "isModalContainer": false,
            "skin": "slFbox",
            "top": "0px",
            "width": "34%"
        }, controller.args[0], "flxFeatureName"), extendConfig({}, controller.args[1], "flxFeatureName"), extendConfig({}, controller.args[2], "flxFeatureName"));
        flxFeatureName.setDefaultUnit(kony.flex.DP);
        var flxAddFeatureNameHeaderPopup = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
            "clipBounds": true,
            "id": "flxAddFeatureNameHeaderPopup",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "left": "0px",
            "isModalContainer": false,
            "right": "15px",
            "skin": "slFbox",
            "top": "0px"
        }, controller.args[0], "flxAddFeatureNameHeaderPopup"), extendConfig({}, controller.args[1], "flxAddFeatureNameHeaderPopup"), extendConfig({}, controller.args[2], "flxAddFeatureNameHeaderPopup"));
        flxAddFeatureNameHeaderPopup.setDefaultUnit(kony.flex.DP);
        var lblAddFeatureNameHeader = new kony.ui.Label(extendConfig({
            "id": "lblAddFeatureNameHeader",
            "isVisible": true,
            "left": "0dp",
            "skin": "sknLblLato485c7513px",
            "text": "Feature Name",
            "top": "0dp",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, controller.args[0], "lblAddFeatureNameHeader"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblAddFeatureNameHeader"), extendConfig({}, controller.args[2], "lblAddFeatureNameHeader"));
        var lblFeatureNameSize = new kony.ui.Label(extendConfig({
            "id": "lblFeatureNameSize",
            "isVisible": false,
            "right": "0dp",
            "skin": "sknLblLato485c7513px",
            "text": "0/50",
            "top": "0dp",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, controller.args[0], "lblFeatureNameSize"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblFeatureNameSize"), extendConfig({}, controller.args[2], "lblFeatureNameSize"));
        flxAddFeatureNameHeaderPopup.add(lblAddFeatureNameHeader, lblFeatureNameSize);
        var txtFeatureName = new kony.ui.TextBox2(extendConfig({
            "autoCapitalize": constants.TEXTBOX_AUTO_CAPITALIZE_NONE,
            "focusSkin": "skntxtbxDetailsFocus14px",
            "height": "40dp",
            "id": "txtFeatureName",
            "isVisible": true,
            "keyBoardStyle": constants.TEXTBOX_KEY_BOARD_STYLE_DEFAULT,
            "left": "0px",
            "maxTextLength": 50,
            "placeholder": "Feature Name",
            "right": "15px",
            "secureTextEntry": false,
            "skin": "txtD7d9e0",
            "textInputMode": constants.TEXTBOX_INPUT_MODE_ANY,
            "top": "30px",
            "zIndex": 1
        }, controller.args[0], "txtFeatureName"), extendConfig({
            "containerHeightMode": constants.TEXTBOX_FONT_METRICS_DRIVEN_HEIGHT,
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [2, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "txtFeatureName"), extendConfig({
            "autoCorrect": false,
            "placeholderSkin": "sknTextGreyb2bdcd"
        }, controller.args[2], "txtFeatureName"));
        var FeatureNameError = new com.adminConsole.loans.applications.errorMsg(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
            "id": "FeatureNameError",
            "isVisible": false,
            "layoutType": kony.flex.FLOW_HORIZONTAL,
            "left": "0px",
            "masterType": constants.MASTER_TYPE_DEFAULT,
            "isModalContainer": false,
            "right": "15px",
            "skin": "slFbox",
            "top": "77px",
            "overrides": {
                "errorMsg": {
                    "isVisible": false,
                    "left": "0px",
                    "right": "15px",
                    "top": "77px",
                    "width": "viz.val_cleared"
                },
                "lblErrorText": {
                    "text": "Enter Feature Name"
                }
            }
        }, controller.args[0], "FeatureNameError"), extendConfig({
            "overrides": {}
        }, controller.args[1], "FeatureNameError"), extendConfig({
            "overrides": {}
        }, controller.args[2], "FeatureNameError"));
        flxFeatureName.add(flxAddFeatureNameHeaderPopup, txtFeatureName, FeatureNameError);
        var flxFeatureGroup = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
            "clipBounds": true,
            "id": "flxFeatureGroup",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "left": "34%",
            "isModalContainer": false,
            "skin": "slFbox",
            "top": "0px",
            "width": "34%"
        }, controller.args[0], "flxFeatureGroup"), extendConfig({}, controller.args[1], "flxFeatureGroup"), extendConfig({}, controller.args[2], "flxFeatureGroup"));
        flxFeatureGroup.setDefaultUnit(kony.flex.DP);
        var flxAddFeatureGroupHeader = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
            "clipBounds": true,
            "id": "flxAddFeatureGroupHeader",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "left": "0px",
            "isModalContainer": false,
            "right": "15px",
            "skin": "slFbox",
            "top": "0px"
        }, controller.args[0], "flxAddFeatureGroupHeader"), extendConfig({}, controller.args[1], "flxAddFeatureGroupHeader"), extendConfig({}, controller.args[2], "flxAddFeatureGroupHeader"));
        flxAddFeatureGroupHeader.setDefaultUnit(kony.flex.DP);
        var lblAddFeatureGroupHeader = new kony.ui.Label(extendConfig({
            "id": "lblAddFeatureGroupHeader",
            "isVisible": true,
            "left": "0dp",
            "skin": "sknLblLato485c7513px",
            "text": "Feature Group",
            "top": "0dp",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, controller.args[0], "lblAddFeatureGroupHeader"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblAddFeatureGroupHeader"), extendConfig({}, controller.args[2], "lblAddFeatureGroupHeader"));
        var lblFeatureGroupSize = new kony.ui.Label(extendConfig({
            "id": "lblFeatureGroupSize",
            "isVisible": false,
            "right": "0dp",
            "skin": "sknLblLato485c7513px",
            "text": "0/50",
            "top": "0dp",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, controller.args[0], "lblFeatureGroupSize"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblFeatureGroupSize"), extendConfig({}, controller.args[2], "lblFeatureGroupSize"));
        flxAddFeatureGroupHeader.add(lblAddFeatureGroupHeader, lblFeatureGroupSize);
        var txtFeatureGroup = new kony.ui.TextBox2(extendConfig({
            "autoCapitalize": constants.TEXTBOX_AUTO_CAPITALIZE_NONE,
            "focusSkin": "skntxtbxDetailsFocus14px",
            "height": "40dp",
            "id": "txtFeatureGroup",
            "isVisible": true,
            "keyBoardStyle": constants.TEXTBOX_KEY_BOARD_STYLE_DEFAULT,
            "left": "0px",
            "maxTextLength": 50,
            "placeholder": "Feature Group",
            "right": "15px",
            "secureTextEntry": false,
            "skin": "txtD7d9e0",
            "textInputMode": constants.TEXTBOX_INPUT_MODE_ANY,
            "top": "30px",
            "zIndex": 1
        }, controller.args[0], "txtFeatureGroup"), extendConfig({
            "containerHeightMode": constants.TEXTBOX_FONT_METRICS_DRIVEN_HEIGHT,
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [2, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "txtFeatureGroup"), extendConfig({
            "autoCorrect": false,
            "placeholderSkin": "sknTextGreyb2bdcd"
        }, controller.args[2], "txtFeatureGroup"));
        var FeatureGroupError = new com.adminConsole.loans.applications.errorMsg(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
            "id": "FeatureGroupError",
            "isVisible": false,
            "layoutType": kony.flex.FLOW_HORIZONTAL,
            "left": "0px",
            "masterType": constants.MASTER_TYPE_DEFAULT,
            "isModalContainer": false,
            "right": "15px",
            "skin": "slFbox",
            "top": "77px",
            "overrides": {
                "errorMsg": {
                    "isVisible": false,
                    "left": "0px",
                    "right": "15px",
                    "top": "77px",
                    "width": "viz.val_cleared"
                },
                "lblErrorText": {
                    "text": "Enter Feature Group "
                }
            }
        }, controller.args[0], "FeatureGroupError"), extendConfig({
            "overrides": {}
        }, controller.args[1], "FeatureGroupError"), extendConfig({
            "overrides": {}
        }, controller.args[2], "FeatureGroupError"));
        flxFeatureGroup.add(flxAddFeatureGroupHeader, txtFeatureGroup, FeatureGroupError);
        var flxFeatureType = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
            "clipBounds": true,
            "id": "flxFeatureType",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "left": "68%",
            "isModalContainer": false,
            "right": "0px",
            "skin": "slFbox",
            "top": "0px"
        }, controller.args[0], "flxFeatureType"), extendConfig({}, controller.args[1], "flxFeatureType"), extendConfig({}, controller.args[2], "flxFeatureType"));
        flxFeatureType.setDefaultUnit(kony.flex.DP);
        var flxAddFeatureTypeHeader = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
            "clipBounds": true,
            "id": "flxAddFeatureTypeHeader",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "left": "0px",
            "isModalContainer": false,
            "right": "0px",
            "skin": "slFbox",
            "top": "0px"
        }, controller.args[0], "flxAddFeatureTypeHeader"), extendConfig({}, controller.args[1], "flxAddFeatureTypeHeader"), extendConfig({}, controller.args[2], "flxAddFeatureTypeHeader"));
        flxAddFeatureTypeHeader.setDefaultUnit(kony.flex.DP);
        var lblAddFeatureTypeHeader = new kony.ui.Label(extendConfig({
            "id": "lblAddFeatureTypeHeader",
            "isVisible": true,
            "left": "0dp",
            "skin": "sknLblLato485c7513px",
            "text": "Feature Type",
            "top": "0dp",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, controller.args[0], "lblAddFeatureTypeHeader"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblAddFeatureTypeHeader"), extendConfig({}, controller.args[2], "lblAddFeatureTypeHeader"));
        var lblFeatureTypeSize = new kony.ui.Label(extendConfig({
            "id": "lblFeatureTypeSize",
            "isVisible": false,
            "right": "0dp",
            "skin": "sknLblLato485c7513px",
            "text": "0/50",
            "top": "0dp",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, controller.args[0], "lblFeatureTypeSize"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblFeatureTypeSize"), extendConfig({}, controller.args[2], "lblFeatureTypeSize"));
        flxAddFeatureTypeHeader.add(lblAddFeatureTypeHeader, lblFeatureTypeSize);
        var txtFeatureType = new kony.ui.TextBox2(extendConfig({
            "autoCapitalize": constants.TEXTBOX_AUTO_CAPITALIZE_NONE,
            "focusSkin": "skntxtbxDetailsFocus14px",
            "height": "40dp",
            "id": "txtFeatureType",
            "isVisible": true,
            "keyBoardStyle": constants.TEXTBOX_KEY_BOARD_STYLE_DEFAULT,
            "left": "0px",
            "maxTextLength": 50,
            "placeholder": "Feature Type",
            "right": "0px",
            "secureTextEntry": false,
            "skin": "txtD7d9e0",
            "textInputMode": constants.TEXTBOX_INPUT_MODE_ANY,
            "top": "30px",
            "zIndex": 1
        }, controller.args[0], "txtFeatureType"), extendConfig({
            "containerHeightMode": constants.TEXTBOX_FONT_METRICS_DRIVEN_HEIGHT,
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [2, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "txtFeatureType"), extendConfig({
            "autoCorrect": false,
            "placeholderSkin": "sknTextGreyb2bdcd"
        }, controller.args[2], "txtFeatureType"));
        var FeatureTypeError = new com.adminConsole.loans.applications.errorMsg(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
            "id": "FeatureTypeError",
            "isVisible": false,
            "layoutType": kony.flex.FLOW_HORIZONTAL,
            "left": "0px",
            "masterType": constants.MASTER_TYPE_DEFAULT,
            "isModalContainer": false,
            "right": "0px",
            "skin": "slFbox",
            "top": "77px",
            "overrides": {
                "errorMsg": {
                    "isVisible": false,
                    "left": "0px",
                    "right": "0px",
                    "top": "77px",
                    "width": "viz.val_cleared"
                },
                "lblErrorText": {
                    "text": "Enter Feature Type"
                }
            }
        }, controller.args[0], "FeatureTypeError"), extendConfig({
            "overrides": {}
        }, controller.args[1], "FeatureTypeError"), extendConfig({
            "overrides": {}
        }, controller.args[2], "FeatureTypeError"));
        flxFeatureType.add(flxAddFeatureTypeHeader, txtFeatureType, FeatureTypeError);
        flxAddFeatureTopDetails.add(flxFeatureName, flxFeatureGroup, flxFeatureType);
        var flxAddFeatureDescription = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
            "clipBounds": true,
            "id": "flxAddFeatureDescription",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "left": "20px",
            "isModalContainer": false,
            "right": "20px",
            "skin": "slFbox",
            "top": "200px"
        }, controller.args[0], "flxAddFeatureDescription"), extendConfig({}, controller.args[1], "flxAddFeatureDescription"), extendConfig({}, controller.args[2], "flxAddFeatureDescription"));
        flxAddFeatureDescription.setDefaultUnit(kony.flex.DP);
        var flxFeatureDescriptionHeader = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
            "clipBounds": true,
            "id": "flxFeatureDescriptionHeader",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "left": "0px",
            "isModalContainer": false,
            "skin": "slFbox",
            "top": "0px",
            "width": "100%"
        }, controller.args[0], "flxFeatureDescriptionHeader"), extendConfig({}, controller.args[1], "flxFeatureDescriptionHeader"), extendConfig({}, controller.args[2], "flxFeatureDescriptionHeader"));
        flxFeatureDescriptionHeader.setDefaultUnit(kony.flex.DP);
        var lblFeatureDescHeader = new kony.ui.Label(extendConfig({
            "id": "lblFeatureDescHeader",
            "isVisible": true,
            "left": "0dp",
            "skin": "sknLblLato485c7513px",
            "i18n_text": "kony.i18n.getLocalizedString(\"i18n.common.description\")",
            "top": "0dp",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, controller.args[0], "lblFeatureDescHeader"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblFeatureDescHeader"), extendConfig({}, controller.args[2], "lblFeatureDescHeader"));
        var lblFeatureDescSize = new kony.ui.Label(extendConfig({
            "id": "lblFeatureDescSize",
            "isVisible": false,
            "right": "0dp",
            "skin": "sknLblLato485c7513px",
            "text": "0/150",
            "top": "0dp",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, controller.args[0], "lblFeatureDescSize"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblFeatureDescSize"), extendConfig({}, controller.args[2], "lblFeatureDescSize"));
        flxFeatureDescriptionHeader.add(lblFeatureDescHeader, lblFeatureDescSize);
        var txtFeatureDescription = new kony.ui.TextArea2(extendConfig({
            "autoCapitalize": constants.TEXTAREA_AUTO_CAPITALIZE_NONE,
            "focusSkin": "sknFocus",
            "height": "70px",
            "id": "txtFeatureDescription",
            "isVisible": true,
            "keyBoardStyle": constants.TEXTAREA_KEY_BOARD_STYLE_DEFAULT,
            "left": "0px",
            "maxTextLength": 250,
            "numberOfVisibleLines": 3,
            "i18n_placeholder": "kony.i18n.getLocalizedString(\"i18n.common.description\")",
            "skin": "skntxtAread7d9e0",
            "textInputMode": constants.TEXTAREA_INPUT_MODE_ANY,
            "top": "30px",
            "width": "100%",
            "zIndex": 1
        }, controller.args[0], "txtFeatureDescription"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_TOP_LEFT,
            "padding": [2, 2, 2, 2],
            "paddingInPixel": false
        }, controller.args[1], "txtFeatureDescription"), extendConfig({
            "autoCorrect": false,
            "placeholderSkin": "defTextAreaFocus"
        }, controller.args[2], "txtFeatureDescription"));
        var FeatureDescError = new com.adminConsole.loans.applications.errorMsg(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
            "id": "FeatureDescError",
            "isVisible": false,
            "layoutType": kony.flex.FLOW_HORIZONTAL,
            "left": "0px",
            "masterType": constants.MASTER_TYPE_DEFAULT,
            "isModalContainer": false,
            "skin": "slFbox",
            "top": "110px",
            "width": "100%",
            "overrides": {
                "errorMsg": {
                    "isVisible": false,
                    "left": "0px",
                    "right": "viz.val_cleared",
                    "top": "110px",
                    "width": "100%"
                },
                "lblErrorText": {
                    "i18n_text": "kony.i18n.getLocalizedString(\"i18n.ProfileManagement.errMsgFieldCantEmpty\")"
                }
            }
        }, controller.args[0], "FeatureDescError"), extendConfig({
            "overrides": {}
        }, controller.args[1], "FeatureDescError"), extendConfig({
            "overrides": {}
        }, controller.args[2], "FeatureDescError"));
        flxAddFeatureDescription.add(flxFeatureDescriptionHeader, txtFeatureDescription, FeatureDescError);
        var flxAddFeaturePopupButtons = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "bottom": "0dp",
            "clipBounds": true,
            "height": "80px",
            "id": "flxAddFeaturePopupButtons",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "left": "0dp",
            "isModalContainer": false,
            "skin": "sknflxBgE4E6EC",
            "width": "100%",
            "zIndex": 1
        }, controller.args[0], "flxAddFeaturePopupButtons"), extendConfig({}, controller.args[1], "flxAddFeaturePopupButtons"), extendConfig({}, controller.args[2], "flxAddFeaturePopupButtons"));
        flxAddFeaturePopupButtons.setDefaultUnit(kony.flex.DP);
        var btnAddFeatureCancel = new kony.ui.Button(extendConfig({
            "centerY": "50%",
            "height": "40px",
            "id": "btnAddFeatureCancel",
            "isVisible": true,
            "left": "20px",
            "skin": "sknbtnf7f7faLatoRegular4f555dBorder1px8b96a5",
            "i18n_text": "kony.i18n.getLocalizedString(\"i18n.permission.CANCEL\")",
            "top": "0%",
            "width": "105px",
            "zIndex": 20
        }, controller.args[0], "btnAddFeatureCancel"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_CENTER,
            "displayText": true,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "btnAddFeatureCancel"), extendConfig({
            "hoverSkin": "sknbtnffffffLatoRegular4f555dBorder1px485c75"
        }, controller.args[2], "btnAddFeatureCancel"));
        var btnAddFeature = new kony.ui.Button(extendConfig({
            "centerY": "50%",
            "focusSkin": "sknBtn003E75LatoRegular13pxFFFFFFRad20px",
            "height": "40px",
            "id": "btnAddFeature",
            "isVisible": true,
            "right": "20px",
            "skin": "sknBtn003E75LatoRegular13pxFFFFFFRad20px",
            "text": "ADD FEATURE",
            "width": "150px",
            "zIndex": 20
        }, controller.args[0], "btnAddFeature"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_CENTER,
            "displayText": true,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "btnAddFeature"), extendConfig({
            "hoverSkin": "sknBtn005198LatoRegular13pxFFFFFFRad20px"
        }, controller.args[2], "btnAddFeature"));
        flxAddFeaturePopupButtons.add(btnAddFeatureCancel, btnAddFeature);
        flxAddFeature.add(flxAddFeaturePopupTopBar, flxAddFeaturePopupClose, lblAddFeaturePopupHeader, flxAddFeatureTopDetails, flxAddFeatureDescription, flxAddFeaturePopupButtons);
        var flxViewFeature = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "centerY": "50%",
            "clipBounds": false,
            "height": "360px",
            "id": "flxViewFeature",
            "isVisible": false,
            "layoutType": kony.flex.FREE_FORM,
            "left": "20%",
            "isModalContainer": false,
            "right": "20%",
            "skin": "flxNoShadowNoBorder",
            "zIndex": 10
        }, controller.args[0], "flxViewFeature"), extendConfig({}, controller.args[1], "flxViewFeature"), extendConfig({}, controller.args[2], "flxViewFeature"));
        flxViewFeature.setDefaultUnit(kony.flex.DP);
        var flxViewFeaturePopupTopBar = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "clipBounds": true,
            "height": "10px",
            "id": "flxViewFeaturePopupTopBar",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "left": "0dp",
            "isModalContainer": false,
            "skin": "sknFlxInfoToastBg357C9ENoRadius",
            "top": "0dp",
            "width": "100%",
            "zIndex": 1
        }, controller.args[0], "flxViewFeaturePopupTopBar"), extendConfig({}, controller.args[1], "flxViewFeaturePopupTopBar"), extendConfig({}, controller.args[2], "flxViewFeaturePopupTopBar"));
        flxViewFeaturePopupTopBar.setDefaultUnit(kony.flex.DP);
        flxViewFeaturePopupTopBar.add();
        var flxViewFeaturePopupClose = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "clipBounds": true,
            "height": "15px",
            "id": "flxViewFeaturePopupClose",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "isModalContainer": false,
            "right": 20,
            "skin": "hoverhandSkin",
            "top": "21dp",
            "width": "15px",
            "zIndex": 1
        }, controller.args[0], "flxViewFeaturePopupClose"), extendConfig({}, controller.args[1], "flxViewFeaturePopupClose"), extendConfig({
            "hoverSkin": "sknCursor"
        }, controller.args[2], "flxViewFeaturePopupClose"));
        flxViewFeaturePopupClose.setDefaultUnit(kony.flex.DP);
        var lblViewFeaturePopupClose = new kony.ui.Label(extendConfig({
            "height": "15dp",
            "id": "lblViewFeaturePopupClose",
            "isVisible": true,
            "left": "0dp",
            "skin": "sknfontIconListbox15px",
            "i18n_text": "kony.i18n.getLocalizedString(\"i18n.userwidgetmodel.lblClearSearch\")",
            "top": "0dp",
            "width": "15dp",
            "zIndex": 1
        }, controller.args[0], "lblViewFeaturePopupClose"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblViewFeaturePopupClose"), extendConfig({}, controller.args[2], "lblViewFeaturePopupClose"));
        flxViewFeaturePopupClose.add(lblViewFeaturePopupClose);
        var lblSeparatorViewFeatureHeader = new kony.ui.Label(extendConfig({
            "height": "1px",
            "id": "lblSeparatorViewFeatureHeader",
            "isVisible": true,
            "left": "20px",
            "right": "20px",
            "skin": "sknlblSeperatorD7D9E0",
            "text": "Label",
            "top": "90px",
            "zIndex": 2
        }, controller.args[0], "lblSeparatorViewFeatureHeader"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_TOP_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblSeparatorViewFeatureHeader"), extendConfig({}, controller.args[2], "lblSeparatorViewFeatureHeader"));
        var btnEditFeature = new kony.ui.Button(extendConfig({
            "focusSkin": "sknbtnffffffLatoRegular4f555dBorder1px485c75",
            "height": "22px",
            "id": "btnEditFeature",
            "isVisible": true,
            "right": "20px",
            "skin": "sknbtnf7f7faLatoReg13px485c75Rad20px",
            "text": "Edit",
            "top": "50px",
            "width": "53px",
            "zIndex": 1
        }, controller.args[0], "btnEditFeature"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_CENTER,
            "displayText": true,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "btnEditFeature"), extendConfig({
            "hoverSkin": "sknbtnffffffLatoRegular4f555dBorder1px485c75"
        }, controller.args[2], "btnEditFeature"));
        var lbViewFeaturePopupHeader = new kony.ui.Label(extendConfig({
            "height": "19dp",
            "id": "lbViewFeaturePopupHeader",
            "isVisible": true,
            "left": "20px",
            "skin": "sknLbl192b45LatoReg16px",
            "text": "Feature Name",
            "top": "52px",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, controller.args[0], "lbViewFeaturePopupHeader"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lbViewFeaturePopupHeader"), extendConfig({}, controller.args[2], "lbViewFeaturePopupHeader"));
        var flxViewFeatureTopDetails = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
            "clipBounds": true,
            "id": "flxViewFeatureTopDetails",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "left": "20px",
            "isModalContainer": false,
            "right": "20px",
            "skin": "slFbox",
            "top": "115px",
            "zIndex": 1
        }, controller.args[0], "flxViewFeatureTopDetails"), extendConfig({}, controller.args[1], "flxViewFeatureTopDetails"), extendConfig({}, controller.args[2], "flxViewFeatureTopDetails"));
        flxViewFeatureTopDetails.setDefaultUnit(kony.flex.DP);
        var flxViewFeatureName = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
            "clipBounds": true,
            "id": "flxViewFeatureName",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "left": "0px",
            "isModalContainer": false,
            "skin": "slFbox",
            "top": "0px",
            "width": "34%"
        }, controller.args[0], "flxViewFeatureName"), extendConfig({}, controller.args[1], "flxViewFeatureName"), extendConfig({}, controller.args[2], "flxViewFeatureName"));
        flxViewFeatureName.setDefaultUnit(kony.flex.DP);
        var lblViewFeatureNameHeader = new kony.ui.Label(extendConfig({
            "id": "lblViewFeatureNameHeader",
            "isVisible": true,
            "left": "0dp",
            "skin": "sknlblLato696c7311px",
            "text": "FEATURE NAME",
            "top": "0dp",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, controller.args[0], "lblViewFeatureNameHeader"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblViewFeatureNameHeader"), extendConfig({}, controller.args[2], "lblViewFeatureNameHeader"));
        var lblFeatureName = new kony.ui.Label(extendConfig({
            "id": "lblFeatureName",
            "isVisible": true,
            "left": "0px",
            "right": "15px",
            "skin": "sknLblLato485c7513px",
            "text": "Feature Name",
            "top": "30px",
            "zIndex": 1
        }, controller.args[0], "lblFeatureName"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblFeatureName"), extendConfig({}, controller.args[2], "lblFeatureName"));
        flxViewFeatureName.add(lblViewFeatureNameHeader, lblFeatureName);
        var flxViewFeatureGroup = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
            "clipBounds": true,
            "id": "flxViewFeatureGroup",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "left": "34%",
            "isModalContainer": false,
            "skin": "slFbox",
            "top": "0px",
            "width": "34%"
        }, controller.args[0], "flxViewFeatureGroup"), extendConfig({}, controller.args[1], "flxViewFeatureGroup"), extendConfig({}, controller.args[2], "flxViewFeatureGroup"));
        flxViewFeatureGroup.setDefaultUnit(kony.flex.DP);
        var lblViewFeatureGroupHeader = new kony.ui.Label(extendConfig({
            "id": "lblViewFeatureGroupHeader",
            "isVisible": true,
            "left": "0dp",
            "skin": "sknlblLato696c7311px",
            "text": "FEATURE GROUP",
            "top": "0dp",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, controller.args[0], "lblViewFeatureGroupHeader"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblViewFeatureGroupHeader"), extendConfig({}, controller.args[2], "lblViewFeatureGroupHeader"));
        var lblFeatureGroup = new kony.ui.Label(extendConfig({
            "id": "lblFeatureGroup",
            "isVisible": true,
            "left": "0px",
            "right": "15px",
            "skin": "sknLblLato485c7513px",
            "text": "Feature Group",
            "top": "30px",
            "zIndex": 1
        }, controller.args[0], "lblFeatureGroup"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblFeatureGroup"), extendConfig({}, controller.args[2], "lblFeatureGroup"));
        flxViewFeatureGroup.add(lblViewFeatureGroupHeader, lblFeatureGroup);
        var flxViewFeatureType = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
            "clipBounds": true,
            "id": "flxViewFeatureType",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "left": "68%",
            "isModalContainer": false,
            "right": "0px",
            "skin": "slFbox",
            "top": "0px"
        }, controller.args[0], "flxViewFeatureType"), extendConfig({}, controller.args[1], "flxViewFeatureType"), extendConfig({}, controller.args[2], "flxViewFeatureType"));
        flxViewFeatureType.setDefaultUnit(kony.flex.DP);
        var lblViewFeatureTypeHeader = new kony.ui.Label(extendConfig({
            "id": "lblViewFeatureTypeHeader",
            "isVisible": true,
            "left": "0dp",
            "skin": "sknlblLato696c7311px",
            "text": "FEATURE TYPE",
            "top": "0dp",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, controller.args[0], "lblViewFeatureTypeHeader"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblViewFeatureTypeHeader"), extendConfig({}, controller.args[2], "lblViewFeatureTypeHeader"));
        var lblFeatureType = new kony.ui.Label(extendConfig({
            "id": "lblFeatureType",
            "isVisible": true,
            "left": "0px",
            "right": "15px",
            "skin": "sknLblLato485c7513px",
            "text": "Feature Type",
            "top": "30px",
            "zIndex": 1
        }, controller.args[0], "lblFeatureType"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblFeatureType"), extendConfig({}, controller.args[2], "lblFeatureType"));
        flxViewFeatureType.add(lblViewFeatureTypeHeader, lblFeatureType);
        flxViewFeatureTopDetails.add(flxViewFeatureName, flxViewFeatureGroup, flxViewFeatureType);
        var lblSeparatorFeatureDetails = new kony.ui.Label(extendConfig({
            "height": "1px",
            "id": "lblSeparatorFeatureDetails",
            "isVisible": true,
            "left": "20px",
            "right": "20px",
            "skin": "sknlblSeperatorD7D9E0",
            "text": "Label",
            "top": "195px",
            "zIndex": 2
        }, controller.args[0], "lblSeparatorFeatureDetails"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_TOP_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblSeparatorFeatureDetails"), extendConfig({}, controller.args[2], "lblSeparatorFeatureDetails"));
        var flxViewFeatureDescription = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
            "clipBounds": true,
            "id": "flxViewFeatureDescription",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "left": "20px",
            "isModalContainer": false,
            "right": "20px",
            "skin": "slFbox",
            "top": "240px"
        }, controller.args[0], "flxViewFeatureDescription"), extendConfig({}, controller.args[1], "flxViewFeatureDescription"), extendConfig({}, controller.args[2], "flxViewFeatureDescription"));
        flxViewFeatureDescription.setDefaultUnit(kony.flex.DP);
        var lblFeatureDescriptionHeader = new kony.ui.Label(extendConfig({
            "id": "lblFeatureDescriptionHeader",
            "isVisible": true,
            "left": "0dp",
            "skin": "sknlblLato696c7311px",
            "text": "DESCRIPTION",
            "top": "0dp",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, controller.args[0], "lblFeatureDescriptionHeader"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblFeatureDescriptionHeader"), extendConfig({}, controller.args[2], "lblFeatureDescriptionHeader"));
        var lblFeatureDescription = new kony.ui.Label(extendConfig({
            "id": "lblFeatureDescription",
            "isVisible": true,
            "left": "0px",
            "right": "15px",
            "skin": "sknLblLato485c7513px",
            "text": "Home equity loans allow homeowners to borrow against the equity in their residence.Home equity loan amounts are based on the difference between a home's current market value and the mortgage balance due.",
            "top": "30px",
            "zIndex": 1
        }, controller.args[0], "lblFeatureDescription"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblFeatureDescription"), extendConfig({}, controller.args[2], "lblFeatureDescription"));
        flxViewFeatureDescription.add(lblFeatureDescriptionHeader, lblFeatureDescription);
        flxViewFeature.add(flxViewFeaturePopupTopBar, flxViewFeaturePopupClose, lblSeparatorViewFeatureHeader, btnEditFeature, lbViewFeaturePopupHeader, flxViewFeatureTopDetails, lblSeparatorFeatureDetails, flxViewFeatureDescription);
        flxPopup.add(flxAddProductLine, flxAddProductGroup, flxViewProductGroup, flxAddFeature, flxViewFeature);
        var manageProductToolTip = new com.adminConsole.Customers.ToolTip(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
            "id": "manageProductToolTip",
            "isVisible": false,
            "layoutType": kony.flex.FREE_FORM,
            "left": "0dp",
            "masterType": constants.MASTER_TYPE_DEFAULT,
            "isModalContainer": false,
            "skin": "slFbox",
            "top": "50dp",
            "width": "110px",
            "zIndex": 10,
            "overrides": {
                "ToolTip": {
                    "isVisible": false,
                    "top": "50dp",
                    "width": "110px",
                    "zIndex": 10
                },
                "flxToolTipMessage": {
                    "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                    "width": "100%"
                },
                "lblNoConcentToolTip": {
                    "bottom": "5px",
                    "height": kony.flex.USE_PREFFERED_SIZE,
                    "left": "10px",
                    "text": "Add image URLs",
                    "top": "5px",
                    "width": "210px"
                },
                "lblarrow": {
                    "centerX": "viz.val_cleared",
                    "left": "10px"
                }
            }
        }, controller.args[0], "manageProductToolTip"), extendConfig({
            "overrides": {}
        }, controller.args[1], "manageProductToolTip"), extendConfig({
            "overrides": {}
        }, controller.args[2], "manageProductToolTip"));
        manageProducts.add(flxManageProducts, flxPopup, manageProductToolTip);
        return manageProducts;
    }
})