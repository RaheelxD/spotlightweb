define(function() {
    return function(controller) {
        var accountCentricAccounts = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "clipBounds": true,
            "isMaster": true,
            "height": "200dp",
            "id": "accountCentricAccounts",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "left": "0dp",
            "isModalContainer": false,
            "skin": "slFbox",
            "top": "0dp",
            "width": "100%"
        }, controller.args[0], "accountCentricAccounts"), extendConfig({}, controller.args[1], "accountCentricAccounts"), extendConfig({}, controller.args[2], "accountCentricAccounts"));
        accountCentricAccounts.setDefaultUnit(kony.flex.DP);
        var lblAccountId = new kony.ui.Label(extendConfig({
            "centerY": "50%",
            "id": "lblAccountId",
            "isVisible": true,
            "left": "10dp",
            "skin": "sknlblLatoBold35475f14px",
            "text": "12340000000043",
            "top": "0",
            "width": kony.flex.USE_PREFFERED_SIZE
        }, controller.args[0], "lblAccountId"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblAccountId"), extendConfig({}, controller.args[2], "lblAccountId"));
        var flxAccountCustVerticalLine = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "bottom": "0dp",
            "clipBounds": true,
            "height": "100%",
            "id": "flxAccountCustVerticalLine",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "isModalContainer": false,
            "right": "0%",
            "skin": "sknflxd5d9ddop100",
            "top": "0dp",
            "width": "1dp",
            "zIndex": 1
        }, controller.args[0], "flxAccountCustVerticalLine"), extendConfig({}, controller.args[1], "flxAccountCustVerticalLine"), extendConfig({}, controller.args[2], "flxAccountCustVerticalLine"));
        flxAccountCustVerticalLine.setDefaultUnit(kony.flex.DP);
        flxAccountCustVerticalLine.add();
        var lblSeperatorLine = new kony.ui.Label(extendConfig({
            "bottom": "0dp",
            "height": "1dp",
            "id": "lblSeperatorLine",
            "isVisible": true,
            "left": "0dp",
            "right": "0dp",
            "skin": "sknLblD5D9DD1000",
            "text": "Label",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, controller.args[0], "lblSeperatorLine"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblSeperatorLine"), extendConfig({}, controller.args[2], "lblSeperatorLine"));
        accountCentricAccounts.add(lblAccountId, flxAccountCustVerticalLine, lblSeperatorLine);
        return accountCentricAccounts;
    }
})