define("com/adminConsole/authorizedSignatories/accountCentricAccounts/useraccountCentricAccountsController", function() {
    return {
        setFlowActions: function() {}
    };
});
define("com/adminConsole/authorizedSignatories/accountCentricAccounts/accountCentricAccountsControllerActions", {
    /*
      This is an auto generated file and any modifications to it may result in corruption of the action sequence.
    */
    /** preShow defined for accountCentricAccounts **/
    AS_FlexContainer_f0e528f33a754931b38459d3aa150239: function AS_FlexContainer_f0e528f33a754931b38459d3aa150239(eventobject) {
        var self = this;
        this.setFlowActions();
    }
});
define("com/adminConsole/authorizedSignatories/accountCentricAccounts/accountCentricAccountsController", ["com/adminConsole/authorizedSignatories/accountCentricAccounts/useraccountCentricAccountsController", "com/adminConsole/authorizedSignatories/accountCentricAccounts/accountCentricAccountsControllerActions"], function() {
    var controller = require("com/adminConsole/authorizedSignatories/accountCentricAccounts/useraccountCentricAccountsController");
    var actions = require("com/adminConsole/authorizedSignatories/accountCentricAccounts/accountCentricAccountsControllerActions");
    for (var key in actions) {
        controller[key] = actions[key];
    }
    return controller;
});
