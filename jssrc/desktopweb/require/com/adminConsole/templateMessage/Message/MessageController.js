define("com/adminConsole/templateMessage/Message/userMessageController", function() {
    return {};
});
define("com/adminConsole/templateMessage/Message/MessageControllerActions", {
    /*
        This is an auto generated file and any modifications to it may result in corruption of the action sequence.
    */
});
define("com/adminConsole/templateMessage/Message/MessageController", ["com/adminConsole/templateMessage/Message/userMessageController", "com/adminConsole/templateMessage/Message/MessageControllerActions"], function() {
    var controller = require("com/adminConsole/templateMessage/Message/userMessageController");
    var actions = require("com/adminConsole/templateMessage/Message/MessageControllerActions");
    for (var key in actions) {
        controller[key] = actions[key];
    }
    return controller;
});
