define(function() {
    return function(controller) {
        var advanceSelection = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
            "clipBounds": true,
            "isMaster": true,
            "id": "advanceSelection",
            "isVisible": true,
            "layoutType": kony.flex.FLOW_VERTICAL,
            "left": "0dp",
            "isModalContainer": false,
            "skin": "sknFlxbgFFFFFFbdrD7D9E0rd3px",
            "top": "0dp",
            "width": "100%"
        }, controller.args[0], "advanceSelection"), extendConfig({}, controller.args[1], "advanceSelection"), extendConfig({}, controller.args[2], "advanceSelection"));
        advanceSelection.setDefaultUnit(kony.flex.DP);
        var flxAdvancedSelectionHeader = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
            "clipBounds": true,
            "id": "flxAdvancedSelectionHeader",
            "isVisible": true,
            "layoutType": kony.flex.FLOW_VERTICAL,
            "left": "0dp",
            "isModalContainer": false,
            "skin": "slFbox",
            "top": "0dp",
            "width": "100%"
        }, controller.args[0], "flxAdvancedSelectionHeader"), extendConfig({}, controller.args[1], "flxAdvancedSelectionHeader"), extendConfig({}, controller.args[2], "flxAdvancedSelectionHeader"));
        flxAdvancedSelectionHeader.setDefaultUnit(kony.flex.DP);
        var flxFeatureNameContainer = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "clipBounds": true,
            "height": "75px",
            "id": "flxFeatureNameContainer",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "left": 0,
            "isModalContainer": false,
            "skin": "sknf5f6f8border0px",
            "top": 0,
            "width": "100%",
            "zIndex": 3
        }, controller.args[0], "flxFeatureNameContainer"), extendConfig({}, controller.args[1], "flxFeatureNameContainer"), extendConfig({
            "hoverSkin": "sknflxffffffBorderE1E5EERadius3pxPointer"
        }, controller.args[2], "flxFeatureNameContainer"));
        flxFeatureNameContainer.setDefaultUnit(kony.flex.DP);
        var flxDropdown = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "clipBounds": true,
            "height": "30px",
            "id": "flxDropdown",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "left": "1%",
            "isModalContainer": false,
            "onClick": controller.AS_FlexContainer_b1057bf841aa44f4aa0f68532b65c2ed,
            "right": "10px",
            "skin": "sknCursor",
            "top": "10px",
            "width": "2%",
            "zIndex": 2
        }, controller.args[0], "flxDropdown"), extendConfig({}, controller.args[1], "flxDropdown"), extendConfig({}, controller.args[2], "flxDropdown"));
        flxDropdown.setDefaultUnit(kony.flex.DP);
        var fonticonArrow = new kony.ui.Label(extendConfig({
            "centerY": "50%",
            "height": "14px",
            "id": "fonticonArrow",
            "isVisible": true,
            "left": "0dp",
            "skin": "sknfontIconDescRightArrow14px",
            "text": "",
            "top": "0dp",
            "width": "14px",
            "zIndex": 1
        }, controller.args[0], "fonticonArrow"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "fonticonArrow"), extendConfig({}, controller.args[2], "fonticonArrow"));
        flxDropdown.add(fonticonArrow);
        var lblFeatureName = new kony.ui.Label(extendConfig({
            "id": "lblFeatureName",
            "isVisible": true,
            "left": "4%",
            "skin": "sknLbl16pxLato192B45",
            "text": "Bill Payments",
            "top": "15px",
            "width": "80%",
            "zIndex": 1
        }, controller.args[0], "lblFeatureName"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_TOP_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblFeatureName"), extendConfig({}, controller.args[2], "lblFeatureName"));
        var flxFeatureStatus = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "clipBounds": true,
            "height": "20dp",
            "id": "flxFeatureStatus",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "left": "85%",
            "isModalContainer": false,
            "skin": "slFbox",
            "top": "15px",
            "width": "100dp"
        }, controller.args[0], "flxFeatureStatus"), extendConfig({}, controller.args[1], "flxFeatureStatus"), extendConfig({}, controller.args[2], "flxFeatureStatus"));
        flxFeatureStatus.setDefaultUnit(kony.flex.DP);
        var lblIconStatus = new kony.ui.Label(extendConfig({
            "centerY": "52%",
            "height": "12dp",
            "id": "lblIconStatus",
            "isVisible": true,
            "left": "0dp",
            "skin": "sknFontIconActivate",
            "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmAlertsManagement.lblViewAlertStatusKey\")",
            "width": "12dp",
            "zIndex": 1
        }, controller.args[0], "lblIconStatus"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblIconStatus"), extendConfig({}, controller.args[2], "lblIconStatus"));
        var lblFeatureStatusValue = new kony.ui.Label(extendConfig({
            "centerY": "50%",
            "id": "lblFeatureStatusValue",
            "isVisible": true,
            "left": 15,
            "skin": "sknLbl485C75LatoRegular13Px",
            "text": "Active",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, controller.args[0], "lblFeatureStatusValue"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblFeatureStatusValue"), extendConfig({}, controller.args[2], "lblFeatureStatusValue"));
        flxFeatureStatus.add(lblIconStatus, lblFeatureStatusValue);
        var flxActionHeader = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "clipBounds": true,
            "height": "30px",
            "id": "flxActionHeader",
            "isVisible": true,
            "layoutType": kony.flex.FLOW_HORIZONTAL,
            "left": 0,
            "isModalContainer": false,
            "skin": "sknf5f6f8border0px",
            "top": "40px",
            "width": "100%",
            "zIndex": 3
        }, controller.args[0], "flxActionHeader"), extendConfig({}, controller.args[1], "flxActionHeader"), extendConfig({
            "hoverSkin": "sknflxffffffBorderE1E5EERadius3pxPointer"
        }, controller.args[2], "flxActionHeader"));
        flxActionHeader.setDefaultUnit(kony.flex.DP);
        var lblSelectedActions = new kony.ui.Label(extendConfig({
            "centerY": "50%",
            "height": "20px",
            "id": "lblSelectedActions",
            "isVisible": true,
            "left": "1.20%",
            "skin": "sknlblLato696c7312px",
            "text": "Selected Actions:   ",
            "top": "1dp",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, controller.args[0], "lblSelectedActions"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblSelectedActions"), extendConfig({}, controller.args[2], "lblSelectedActions"));
        var lblSelectedValue = new kony.ui.Label(extendConfig({
            "centerY": "50%",
            "height": "20px",
            "id": "lblSelectedValue",
            "isVisible": true,
            "left": "10dp",
            "skin": "sknLbl696c73LatoBold",
            "text": "0",
            "top": "1dp",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, controller.args[0], "lblSelectedValue"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblSelectedValue"), extendConfig({}, controller.args[2], "lblSelectedValue"));
        var lblOf = new kony.ui.Label(extendConfig({
            "centerY": "50%",
            "height": "20px",
            "id": "lblOf",
            "isVisible": true,
            "left": "5dp",
            "skin": "sknlblLato696c7312px",
            "text": " of ",
            "top": "1dp",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, controller.args[0], "lblOf"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblOf"), extendConfig({}, controller.args[2], "lblOf"));
        var lblActionsValue = new kony.ui.Label(extendConfig({
            "centerY": "50%",
            "height": "20px",
            "id": "lblActionsValue",
            "isVisible": true,
            "left": "5dp",
            "skin": "sknlblLato696c7312px",
            "text": "0",
            "top": "1dp",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, controller.args[0], "lblActionsValue"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblActionsValue"), extendConfig({}, controller.args[2], "lblActionsValue"));
        flxActionHeader.add(lblSelectedActions, lblSelectedValue, lblOf, lblActionsValue);
        flxFeatureNameContainer.add(flxDropdown, lblFeatureName, flxFeatureStatus, flxActionHeader);
        var lblSeparator1 = new kony.ui.Label(extendConfig({
            "height": "1px",
            "id": "lblSeparator1",
            "isVisible": true,
            "left": "0dp",
            "skin": "sknlblSeperator",
            "text": ".",
            "top": "0dp",
            "width": "100%",
            "zIndex": 1
        }, controller.args[0], "lblSeparator1"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblSeparator1"), extendConfig({}, controller.args[2], "lblSeparator1"));
        flxAdvancedSelectionHeader.add(flxFeatureNameContainer, lblSeparator1);
        var flxAdvancedSelectionContent = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
            "clipBounds": true,
            "id": "flxAdvancedSelectionContent",
            "isVisible": true,
            "layoutType": kony.flex.FLOW_VERTICAL,
            "left": "0dp",
            "isModalContainer": false,
            "skin": "slFbox",
            "top": "0dp",
            "width": "100%"
        }, controller.args[0], "flxAdvancedSelectionContent"), extendConfig({}, controller.args[1], "flxAdvancedSelectionContent"), extendConfig({}, controller.args[2], "flxAdvancedSelectionContent"));
        flxAdvancedSelectionContent.setDefaultUnit(kony.flex.DP);
        var flxSelectAll = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "clipBounds": true,
            "focusSkin": "sknflxffffffop100",
            "height": "40px",
            "id": "flxSelectAll",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "left": "0",
            "isModalContainer": false,
            "skin": "slFbox",
            "top": "0",
            "width": "100%"
        }, controller.args[0], "flxSelectAll"), extendConfig({}, controller.args[1], "flxSelectAll"), extendConfig({
            "hoverSkin": "sknFlxffffffOp100Cursor"
        }, controller.args[2], "flxSelectAll"));
        flxSelectAll.setDefaultUnit(kony.flex.DP);
        var flxCheckBox = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "centerY": "50%",
            "clipBounds": true,
            "height": "12px",
            "id": "flxCheckBox",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "left": "1.20%",
            "isModalContainer": false,
            "skin": "slFbox",
            "top": "7dp",
            "width": "12px",
            "zIndex": 2
        }, controller.args[0], "flxCheckBox"), extendConfig({}, controller.args[1], "flxCheckBox"), extendConfig({}, controller.args[2], "flxCheckBox"));
        flxCheckBox.setDefaultUnit(kony.flex.DP);
        var imgCheckBox = new kony.ui.Image2(extendConfig({
            "centerY": "50%",
            "height": "100%",
            "id": "imgCheckBox",
            "isVisible": true,
            "left": "0%",
            "skin": "slImage",
            "src": "checkboxpartial.png",
            "width": "100%",
            "zIndex": 2
        }, controller.args[0], "imgCheckBox"), extendConfig({
            "imageScaleMode": constants.IMAGE_SCALE_MODE_MAINTAIN_ASPECT_RATIO,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "imgCheckBox"), extendConfig({}, controller.args[2], "imgCheckBox"));
        flxCheckBox.add(imgCheckBox);
        var lblDescription = new kony.ui.Label(extendConfig({
            "bottom": "5dp",
            "centerY": "50%",
            "id": "lblDescription",
            "isVisible": true,
            "left": "4%",
            "skin": "sknLbl485C75LatoRegular13Px",
            "text": "ACTIONS",
            "top": "5dp",
            "width": "80%",
            "zIndex": 1
        }, controller.args[0], "lblDescription"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblDescription"), extendConfig({}, controller.args[2], "lblDescription"));
        var lblStatus = new kony.ui.Label(extendConfig({
            "bottom": "5dp",
            "centerY": "50%",
            "id": "lblStatus",
            "isVisible": true,
            "left": "85%",
            "skin": "sknLbl485C75LatoRegular13Px",
            "text": "STATUS",
            "top": "5dp",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, controller.args[0], "lblStatus"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblStatus"), extendConfig({}, controller.args[2], "lblStatus"));
        flxSelectAll.add(flxCheckBox, lblDescription, lblStatus);
        var lblSeperator = new kony.ui.Label(extendConfig({
            "height": "1px",
            "id": "lblSeperator",
            "isVisible": true,
            "left": "0dp",
            "skin": "sknlblSeperator",
            "text": ".",
            "top": "0dp",
            "width": "100%",
            "zIndex": 1
        }, controller.args[0], "lblSeperator"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblSeperator"), extendConfig({}, controller.args[2], "lblSeperator"));
        var flxActionList = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
            "bottom": "5px",
            "clipBounds": true,
            "id": "flxActionList",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "left": 0,
            "isModalContainer": false,
            "skin": "sknflxffffffop100",
            "top": "5px",
            "zIndex": 3
        }, controller.args[0], "flxActionList"), extendConfig({}, controller.args[1], "flxActionList"), extendConfig({
            "hoverSkin": "sknflxffffffBorderE1E5EERadius3pxPointer"
        }, controller.args[2], "flxActionList"));
        flxActionList.setDefaultUnit(kony.flex.DP);
        var SegActions = new kony.ui.SegmentedUI2(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
            "data": [{
                "imgCheckBox": "checkboxnormal.png",
                "lblDescription": "View Recepient",
                "lblFeatureStatusValue": "Active",
                "lblIconStatus": "kony.i18n.getLocalizedString(\"i18n.frmAlertsManagement.lblViewAlertStatusKey\")"
            }, {
                "imgCheckBox": "checkboxnormal.png",
                "lblDescription": "View Recepient",
                "lblFeatureStatusValue": "Active",
                "lblIconStatus": "kony.i18n.getLocalizedString(\"i18n.frmAlertsManagement.lblViewAlertStatusKey\")"
            }, {
                "imgCheckBox": "checkboxnormal.png",
                "lblDescription": "View Recepient",
                "lblFeatureStatusValue": "Active",
                "lblIconStatus": "kony.i18n.getLocalizedString(\"i18n.frmAlertsManagement.lblViewAlertStatusKey\")"
            }],
            "groupCells": false,
            "id": "SegActions",
            "isVisible": true,
            "left": "0dp",
            "needPageIndicator": true,
            "pageOffDotImage": "pageoffdot.png",
            "pageOnDotImage": "pageondot.png",
            "retainSelection": false,
            "rowFocusSkin": "seg2Focus",
            "rowSkin": "seg2Normal",
            "rowTemplate": "flxActionsList",
            "sectionHeaderSkin": "sliPhoneSegmentHeader",
            "selectionBehavior": constants.SEGUI_DEFAULT_BEHAVIOR,
            "separatorRequired": false,
            "showScrollbars": false,
            "top": "0dp",
            "viewType": constants.SEGUI_VIEW_TYPE_TABLEVIEW,
            "widgetDataMap": {
                "flxActionsList": "flxActionsList",
                "flxCheckBox": "flxCheckBox",
                "flxFeatureStatus": "flxFeatureStatus",
                "imgCheckBox": "imgCheckBox",
                "lblDescription": "lblDescription",
                "lblFeatureStatusValue": "lblFeatureStatusValue",
                "lblIconStatus": "lblIconStatus"
            },
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 10
        }, controller.args[0], "SegActions"), extendConfig({
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "SegActions"), extendConfig({}, controller.args[2], "SegActions"));
        flxActionList.add(SegActions);
        flxAdvancedSelectionContent.add(flxSelectAll, lblSeperator, flxActionList);
        advanceSelection.add(flxAdvancedSelectionHeader, flxAdvancedSelectionContent);
        return advanceSelection;
    }
})