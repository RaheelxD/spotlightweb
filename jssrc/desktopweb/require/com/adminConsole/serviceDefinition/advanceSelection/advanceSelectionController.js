define("com/adminConsole/serviceDefinition/advanceSelection/useradvanceSelectionController", function() {
    return {
        setFlowActions: function() {
            var scopeObj = this;
            this.view.flxFeatureNameContainer.onClick = function() {
                scopeObj.toggleContent();
            };
        },
        toggleContent: function() {
            if (this.view.flxAdvancedSelectionContent.isVisible) {
                this.view.fonticonArrow.text = "\ue922";
                this.view.flxAdvancedSelectionContent.setVisibility(false);
            } else {
                this.view.fonticonArrow.text = "\ue915";
                this.view.flxAdvancedSelectionContent.setVisibility(true);
            }
        }
    };
});
define("com/adminConsole/serviceDefinition/advanceSelection/advanceSelectionControllerActions", {
    /*
        This is an auto generated file and any modifications to it may result in corruption of the action sequence.
    */
});
define("com/adminConsole/serviceDefinition/advanceSelection/advanceSelectionController", ["com/adminConsole/serviceDefinition/advanceSelection/useradvanceSelectionController", "com/adminConsole/serviceDefinition/advanceSelection/advanceSelectionControllerActions"], function() {
    var controller = require("com/adminConsole/serviceDefinition/advanceSelection/useradvanceSelectionController");
    var actions = require("com/adminConsole/serviceDefinition/advanceSelection/advanceSelectionControllerActions");
    for (var key in actions) {
        controller[key] = actions[key];
    }
    return controller;
});
