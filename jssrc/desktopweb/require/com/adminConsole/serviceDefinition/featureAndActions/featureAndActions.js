define(function() {
    return function(controller) {
        var featureAndActions = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "clipBounds": true,
            "isMaster": true,
            "height": "100%",
            "id": "featureAndActions",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "left": "0dp",
            "isModalContainer": false,
            "skin": "slFbox",
            "top": "0dp",
            "width": "100%"
        }, controller.args[0], "featureAndActions"), extendConfig({}, controller.args[1], "featureAndActions"), extendConfig({}, controller.args[2], "featureAndActions"));
        featureAndActions.setDefaultUnit(kony.flex.DP);
        var flxFeatures = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "clipBounds": true,
            "height": "100%",
            "id": "flxFeatures",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "left": "0dp",
            "isModalContainer": false,
            "skin": "slFbox",
            "top": "0dp",
            "width": "100%",
            "zIndex": 1
        }, controller.args[0], "flxFeatures"), extendConfig({}, controller.args[1], "flxFeatures"), extendConfig({}, controller.args[2], "flxFeatures"));
        flxFeatures.setDefaultUnit(kony.flex.DP);
        var flxHeader = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "clipBounds": false,
            "height": "130dp",
            "id": "flxHeader",
            "isVisible": true,
            "layoutType": kony.flex.FLOW_VERTICAL,
            "left": "0dp",
            "isModalContainer": false,
            "skin": "slFbox",
            "top": "0dp",
            "width": "100%",
            "zIndex": 10
        }, controller.args[0], "flxHeader"), extendConfig({}, controller.args[1], "flxHeader"), extendConfig({}, controller.args[2], "flxHeader"));
        flxHeader.setDefaultUnit(kony.flex.DP);
        var flxHeading = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "clipBounds": true,
            "height": "50dp",
            "id": "flxHeading",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "left": "0dp",
            "isModalContainer": false,
            "skin": "slFbox",
            "top": "0dp",
            "width": "100%",
            "zIndex": 1
        }, controller.args[0], "flxHeading"), extendConfig({}, controller.args[1], "flxHeading"), extendConfig({}, controller.args[2], "flxHeading"));
        flxHeading.setDefaultUnit(kony.flex.DP);
        var lblHeading = new kony.ui.Label(extendConfig({
            "id": "lblHeading",
            "isVisible": true,
            "left": "20dp",
            "skin": "sknLbl192B4518pxLatoReg",
            "text": "Label",
            "top": "15dp",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, controller.args[0], "lblHeading"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblHeading"), extendConfig({}, controller.args[2], "lblHeading"));
        var btnSelection = new kony.ui.Button(extendConfig({
            "height": "22dp",
            "id": "btnSelection",
            "isVisible": true,
            "right": "15dp",
            "skin": "sknbtnf7f7faLatoRegular12Px485c75Border1px485C75Radius20px",
            "text": "Advanced Selection",
            "top": "20dp",
            "width": "135dp",
            "zIndex": 1
        }, controller.args[0], "btnSelection"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_CENTER,
            "displayText": true,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "btnSelection"), extendConfig({
            "hoverSkin": "sknbtnffffffLatoRegular12Px485c75Border1px485C75Radius20px"
        }, controller.args[2], "btnSelection"));
        flxHeading.add(lblHeading, btnSelection);
        var flxPolicies = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "clipBounds": false,
            "height": "70dp",
            "id": "flxPolicies",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "left": "10dp",
            "isModalContainer": false,
            "skin": "slFbox",
            "top": "0dp",
            "width": "100%",
            "zIndex": 10
        }, controller.args[0], "flxPolicies"), extendConfig({}, controller.args[1], "flxPolicies"), extendConfig({}, controller.args[2], "flxPolicies"));
        flxPolicies.setDefaultUnit(kony.flex.DP);
        var flxFeatureSearch = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "clipBounds": false,
            "height": "60dp",
            "id": "flxFeatureSearch",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "left": "0dp",
            "isModalContainer": false,
            "skin": "slFbox",
            "top": "0dp",
            "width": "100%",
            "zIndex": 10
        }, controller.args[0], "flxFeatureSearch"), extendConfig({}, controller.args[1], "flxFeatureSearch"), extendConfig({}, controller.args[2], "flxFeatureSearch"));
        flxFeatureSearch.setDefaultUnit(kony.flex.DP);
        var subHeader = new com.adminConsole.header.subHeader(extendConfig({
            "centerY": "50%",
            "height": "48dp",
            "id": "subHeader",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "left": "0dp",
            "masterType": constants.MASTER_TYPE_DEFAULT,
            "isModalContainer": false,
            "skin": "CopyslFbox2",
            "top": "0dp",
            "width": "100%",
            "overrides": {
                "flxMenu": {
                    "isVisible": false,
                    "left": "0dp"
                },
                "flxSearch": {
                    "right": "15dp"
                },
                "subHeader": {
                    "centerY": "50%"
                },
                "tbxSearchBox": {
                    "i18n_placeholder": "kony.i18n.getLocalizedString(\"i18n.frmCreateCustomer.Search_By_Feature\")"
                }
            }
        }, controller.args[0], "subHeader"), extendConfig({
            "overrides": {}
        }, controller.args[1], "subHeader"), extendConfig({
            "overrides": {}
        }, controller.args[2], "subHeader"));
        var flxActions = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "clipBounds": false,
            "height": "40dp",
            "id": "flxActions",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "left": "10dp",
            "isModalContainer": false,
            "skin": "slFbox",
            "top": "11dp",
            "width": "30%",
            "zIndex": 10
        }, controller.args[0], "flxActions"), extendConfig({}, controller.args[1], "flxActions"), extendConfig({}, controller.args[2], "flxActions"));
        flxActions.setDefaultUnit(kony.flex.DP);
        var imgFeatureCheckbox = new kony.ui.Image2(extendConfig({
            "centerY": "50%",
            "height": "15dp",
            "id": "imgFeatureCheckbox",
            "isVisible": true,
            "left": "0dp",
            "skin": "slImage",
            "src": "checkboxnormal.png",
            "width": "15dp",
            "zIndex": 1
        }, controller.args[0], "imgFeatureCheckbox"), extendConfig({
            "imageScaleMode": constants.IMAGE_SCALE_MODE_MAINTAIN_ASPECT_RATIO,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "imgFeatureCheckbox"), extendConfig({}, controller.args[2], "imgFeatureCheckbox"));
        var lblSelect = new kony.ui.Label(extendConfig({
            "centerY": "50%",
            "id": "lblSelect",
            "isVisible": true,
            "left": "25dp",
            "skin": "sknlblLatoregular02020213px",
            "text": "Label",
            "top": "6dp",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, controller.args[0], "lblSelect"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblSelect"), extendConfig({}, controller.args[2], "lblSelect"));
        var flxAssignActions = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "centerY": "50%",
            "clipBounds": false,
            "height": "22dp",
            "id": "flxAssignActions",
            "isVisible": false,
            "layoutType": kony.flex.FREE_FORM,
            "left": "120dp",
            "isModalContainer": false,
            "skin": "sknflxbrdr485C75rad3px",
            "top": "4dp",
            "width": "140dp",
            "zIndex": 10
        }, controller.args[0], "flxAssignActions"), extendConfig({}, controller.args[1], "flxAssignActions"), extendConfig({}, controller.args[2], "flxAssignActions"));
        flxAssignActions.setDefaultUnit(kony.flex.DP);
        var flxDropDown = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
            "clipBounds": false,
            "id": "flxDropDown",
            "isVisible": false,
            "layoutType": kony.flex.FLOW_VERTICAL,
            "left": "0dp",
            "isModalContainer": false,
            "skin": "sknflxD7D9E0rad3pxshadow20px",
            "top": "25dp",
            "width": "140dp",
            "zIndex": 10
        }, controller.args[0], "flxDropDown"), extendConfig({}, controller.args[1], "flxDropDown"), extendConfig({}, controller.args[2], "flxDropDown"));
        flxDropDown.setDefaultUnit(kony.flex.DP);
        var flxAll = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "clipBounds": true,
            "height": "20dp",
            "id": "flxAll",
            "isVisible": true,
            "layoutType": kony.flex.FLOW_HORIZONTAL,
            "left": "0dp",
            "isModalContainer": false,
            "skin": "slFbox",
            "top": "0dp",
            "width": "100%",
            "zIndex": 10
        }, controller.args[0], "flxAll"), extendConfig({}, controller.args[1], "flxAll"), extendConfig({}, controller.args[2], "flxAll"));
        flxAll.setDefaultUnit(kony.flex.DP);
        var flxCheckBox = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "clipBounds": true,
            "height": "12px",
            "id": "flxCheckBox",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "left": "15px",
            "isModalContainer": false,
            "skin": "slFbox",
            "top": "7dp",
            "width": "12px",
            "zIndex": 2
        }, controller.args[0], "flxCheckBox"), extendConfig({}, controller.args[1], "flxCheckBox"), extendConfig({}, controller.args[2], "flxCheckBox"));
        flxCheckBox.setDefaultUnit(kony.flex.DP);
        var imgCheckBox = new kony.ui.Image2(extendConfig({
            "centerY": "50%",
            "height": "100%",
            "id": "imgCheckBox",
            "isVisible": true,
            "left": "0%",
            "skin": "slImage",
            "src": "checkboxnormal.png",
            "width": "100%",
            "zIndex": 2
        }, controller.args[0], "imgCheckBox"), extendConfig({
            "imageScaleMode": constants.IMAGE_SCALE_MODE_MAINTAIN_ASPECT_RATIO,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "imgCheckBox"), extendConfig({}, controller.args[2], "imgCheckBox"));
        flxCheckBox.add(imgCheckBox);
        var lblDescription = new kony.ui.Label(extendConfig({
            "bottom": "5dp",
            "id": "lblDescription",
            "isVisible": true,
            "left": "10px",
            "skin": "sknlblLatoBold35475f14px",
            "text": "All",
            "top": "5dp",
            "width": "60%",
            "zIndex": 1
        }, controller.args[0], "lblDescription"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblDescription"), extendConfig({}, controller.args[2], "lblDescription"));
        flxAll.add(flxCheckBox, lblDescription);
        var segDropdown = new kony.ui.SegmentedUI2(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
            "data": [{
                "imgCheckBox": "checkboxnormal.png",
                "lblDescription": "All"
            }, {
                "imgCheckBox": "checkboxnormal.png",
                "lblDescription": "View"
            }, {
                "imgCheckBox": "checkboxnormal.png",
                "lblDescription": "Create"
            }, {
                "imgCheckBox": "checkboxnormal.png",
                "lblDescription": "Approve"
            }, {
                "imgCheckBox": "checkboxnormal.png",
                "lblDescription": "Delete"
            }, {
                "imgCheckBox": "checkboxnormal.png",
                "lblDescription": "View - Bulk"
            }, {
                "imgCheckBox": "checkboxnormal.png",
                "lblDescription": "Create - Bulk"
            }, {
                "imgCheckBox": "checkboxnormal.png",
                "lblDescription": "Approve - Bulk"
            }, {
                "imgCheckBox": "checkboxnormal.png",
                "lblDescription": "Delete - Bulk"
            }],
            "groupCells": false,
            "id": "segDropdown",
            "isVisible": true,
            "left": "0dp",
            "needPageIndicator": true,
            "pageOffDotImage": "pageoffdot.png",
            "pageOnDotImage": "pageondot.png",
            "retainSelection": false,
            "rowFocusSkin": "seg2Focus",
            "rowSkin": "seg2Normal",
            "rowTemplate": "flxSearchDropDown",
            "sectionHeaderSkin": "sliPhoneSegmentHeader",
            "selectionBehavior": constants.SEGUI_DEFAULT_BEHAVIOR,
            "separatorColor": "aaaaaa00",
            "separatorRequired": false,
            "separatorThickness": 1,
            "showScrollbars": true,
            "top": "6dp",
            "viewType": constants.SEGUI_VIEW_TYPE_TABLEVIEW,
            "widgetDataMap": {
                "flxCheckBox": "flxCheckBox",
                "flxSearchDropDown": "flxSearchDropDown",
                "imgCheckBox": "imgCheckBox",
                "lblDescription": "lblDescription"
            },
            "width": "100%",
            "zIndex": 10
        }, controller.args[0], "segDropdown"), extendConfig({
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "segDropdown"), extendConfig({}, controller.args[2], "segDropdown"));
        var flxApply = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "clipBounds": false,
            "height": "40dp",
            "id": "flxApply",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "left": "0dp",
            "isModalContainer": false,
            "skin": "slFbox",
            "top": "5dp",
            "width": "100%",
            "zIndex": 10
        }, controller.args[0], "flxApply"), extendConfig({}, controller.args[1], "flxApply"), extendConfig({
            "hoverSkin": "sknContextualMenuEntryHover2"
        }, controller.args[2], "flxApply"));
        flxApply.setDefaultUnit(kony.flex.DP);
        var flxSep = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "clipBounds": true,
            "height": "1dp",
            "id": "flxSep",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "left": "0dp",
            "isModalContainer": false,
            "skin": "sknflxD5D9DD",
            "top": "0dp",
            "width": "100%",
            "zIndex": 10
        }, controller.args[0], "flxSep"), extendConfig({}, controller.args[1], "flxSep"), extendConfig({}, controller.args[2], "flxSep"));
        flxSep.setDefaultUnit(kony.flex.DP);
        flxSep.add();
        var btnApply = new kony.ui.Button(extendConfig({
            "centerX": "50%",
            "centerY": "50%",
            "height": "100%",
            "id": "btnApply",
            "isVisible": true,
            "left": "0dp",
            "skin": "sknBtnUtilRest696C73Lato11px",
            "text": "APPLY",
            "top": "10dp",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 10
        }, controller.args[0], "btnApply"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_CENTER,
            "displayText": true,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "btnApply"), extendConfig({}, controller.args[2], "btnApply"));
        flxApply.add(flxSep, btnApply);
        flxDropDown.add(flxAll, segDropdown, flxApply);
        var lblActions = new kony.ui.Label(extendConfig({
            "centerY": "50%",
            "height": "100%",
            "id": "lblActions",
            "isVisible": true,
            "left": "10dp",
            "skin": "sknLbl485C75LatoRegular12Px",
            "text": "Assign Access Level",
            "top": "4dp",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, controller.args[0], "lblActions"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblActions"), extendConfig({}, controller.args[2], "lblActions"));
        var lblIconDownArrow = new kony.ui.Label(extendConfig({
            "centerY": "55%",
            "id": "lblIconDownArrow",
            "isVisible": true,
            "right": 7,
            "skin": "sknFontIconBreadcrumb",
            "text": "",
            "top": "3dp",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, controller.args[0], "lblIconDownArrow"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblIconDownArrow"), extendConfig({
            "hoverSkin": "sknFontIconBreadcrumbHoverCursor"
        }, controller.args[2], "lblIconDownArrow"));
        flxAssignActions.add(flxDropDown, lblActions, lblIconDownArrow);
        flxActions.add(imgFeatureCheckbox, lblSelect, flxAssignActions);
        flxFeatureSearch.add(subHeader, flxActions);
        flxPolicies.add(flxFeatureSearch);
        var flxSeperator = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "clipBounds": true,
            "height": "1dp",
            "id": "flxSeperator",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "left": "10dp",
            "isModalContainer": false,
            "skin": "sknflxD5D9DD",
            "top": "5dp",
            "width": "98%",
            "zIndex": 1
        }, controller.args[0], "flxSeperator"), extendConfig({}, controller.args[1], "flxSeperator"), extendConfig({}, controller.args[2], "flxSeperator"));
        flxSeperator.setDefaultUnit(kony.flex.DP);
        flxSeperator.add();
        flxHeader.add(flxHeading, flxPolicies, flxSeperator);
        var flxFeaturesSegment = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "bottom": "82dp",
            "clipBounds": true,
            "id": "flxFeaturesSegment",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "left": "0dp",
            "isModalContainer": false,
            "skin": "slFbox",
            "top": "130dp",
            "width": "100%",
            "zIndex": 1
        }, controller.args[0], "flxFeaturesSegment"), extendConfig({}, controller.args[1], "flxFeaturesSegment"), extendConfig({}, controller.args[2], "flxFeaturesSegment"));
        flxFeaturesSegment.setDefaultUnit(kony.flex.DP);
        var segFeatures = new kony.ui.SegmentedUI2(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "bottom": 0,
            "data": [{
                "imgCheckBox": "checkboxnormal.png",
                "lblFeatureName": "Bill Payments",
                "lblFeatureStatusValue": "Active",
                "lblIconStatus": "kony.i18n.getLocalizedString(\"i18n.frmAlertsManagement.lblViewAlertStatusKey\")",
                "lblSeparator": "Label"
            }, {
                "imgCheckBox": "checkboxnormal.png",
                "lblFeatureName": "Bill Payments",
                "lblFeatureStatusValue": "Active",
                "lblIconStatus": "kony.i18n.getLocalizedString(\"i18n.frmAlertsManagement.lblViewAlertStatusKey\")",
                "lblSeparator": "Label"
            }, {
                "imgCheckBox": "checkboxnormal.png",
                "lblFeatureName": "Bill Payments",
                "lblFeatureStatusValue": "Active",
                "lblIconStatus": "kony.i18n.getLocalizedString(\"i18n.frmAlertsManagement.lblViewAlertStatusKey\")",
                "lblSeparator": "Label"
            }, {
                "imgCheckBox": "checkboxnormal.png",
                "lblFeatureName": "Bill Payments",
                "lblFeatureStatusValue": "Active",
                "lblIconStatus": "kony.i18n.getLocalizedString(\"i18n.frmAlertsManagement.lblViewAlertStatusKey\")",
                "lblSeparator": "Label"
            }, {
                "imgCheckBox": "checkboxnormal.png",
                "lblFeatureName": "Bill Payments",
                "lblFeatureStatusValue": "Active",
                "lblIconStatus": "kony.i18n.getLocalizedString(\"i18n.frmAlertsManagement.lblViewAlertStatusKey\")",
                "lblSeparator": "Label"
            }, {
                "imgCheckBox": "checkboxnormal.png",
                "lblFeatureName": "Bill Payments",
                "lblFeatureStatusValue": "Active",
                "lblIconStatus": "kony.i18n.getLocalizedString(\"i18n.frmAlertsManagement.lblViewAlertStatusKey\")",
                "lblSeparator": "Label"
            }, {
                "imgCheckBox": "checkboxnormal.png",
                "lblFeatureName": "Bill Payments",
                "lblFeatureStatusValue": "Active",
                "lblIconStatus": "kony.i18n.getLocalizedString(\"i18n.frmAlertsManagement.lblViewAlertStatusKey\")",
                "lblSeparator": "Label"
            }, {
                "imgCheckBox": "checkboxnormal.png",
                "lblFeatureName": "Bill Payments",
                "lblFeatureStatusValue": "Active",
                "lblIconStatus": "kony.i18n.getLocalizedString(\"i18n.frmAlertsManagement.lblViewAlertStatusKey\")",
                "lblSeparator": "Label"
            }, {
                "imgCheckBox": "checkboxnormal.png",
                "lblFeatureName": "Bill Payments",
                "lblFeatureStatusValue": "Active",
                "lblIconStatus": "kony.i18n.getLocalizedString(\"i18n.frmAlertsManagement.lblViewAlertStatusKey\")",
                "lblSeparator": "Label"
            }, {
                "imgCheckBox": "checkboxnormal.png",
                "lblFeatureName": "Bill Payments",
                "lblFeatureStatusValue": "Active",
                "lblIconStatus": "kony.i18n.getLocalizedString(\"i18n.frmAlertsManagement.lblViewAlertStatusKey\")",
                "lblSeparator": "Label"
            }],
            "groupCells": false,
            "height": "100%",
            "id": "segFeatures",
            "isVisible": true,
            "left": "10dp",
            "needPageIndicator": true,
            "pageOffDotImage": "pageoffdot.png",
            "pageOnDotImage": "pageondot.png",
            "retainSelection": false,
            "rowFocusSkin": "seg2Focus",
            "rowSkin": "seg2Normal",
            "rowTemplate": "flxFeaturesList",
            "sectionHeaderSkin": "sliPhoneSegmentHeader",
            "selectionBehavior": constants.SEGUI_DEFAULT_BEHAVIOR,
            "separatorColor": "d5d9dd64",
            "separatorRequired": true,
            "separatorThickness": 1,
            "showScrollbars": false,
            "top": "0dp",
            "viewType": constants.SEGUI_VIEW_TYPE_TABLEVIEW,
            "widgetDataMap": {
                "flxFeatureCheckbox": "flxFeatureCheckbox",
                "flxFeatureNameContainer": "flxFeatureNameContainer",
                "flxFeatureStatus": "flxFeatureStatus",
                "flxFeaturesList": "flxFeaturesList",
                "imgCheckBox": "imgCheckBox",
                "lblFeatureName": "lblFeatureName",
                "lblFeatureStatusValue": "lblFeatureStatusValue",
                "lblIconStatus": "lblIconStatus",
                "lblSeparator": "lblSeparator"
            },
            "width": "98%",
            "zIndex": 1
        }, controller.args[0], "segFeatures"), extendConfig({
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "segFeatures"), extendConfig({}, controller.args[2], "segFeatures"));
        flxFeaturesSegment.add(segFeatures);
        var flxSeperator1 = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "bottom": "80dp",
            "clipBounds": true,
            "height": "1dp",
            "id": "flxSeperator1",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "left": "10dp",
            "isModalContainer": false,
            "skin": "sknflxD5D9DD",
            "width": "98%",
            "zIndex": 1
        }, controller.args[0], "flxSeperator1"), extendConfig({}, controller.args[1], "flxSeperator1"), extendConfig({}, controller.args[2], "flxSeperator1"));
        flxSeperator1.setDefaultUnit(kony.flex.DP);
        flxSeperator1.add();
        var flxCommonButtons = new kony.ui.FlexContainer(extendConfig({
            "bottom": "0dp",
            "clipBounds": true,
            "height": "80px",
            "id": "flxCommonButtons",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "left": "0dp",
            "isModalContainer": false,
            "skin": "slFbox",
            "width": "100%"
        }, controller.args[0], "flxCommonButtons"), extendConfig({}, controller.args[1], "flxCommonButtons"), extendConfig({}, controller.args[2], "flxCommonButtons"));
        flxCommonButtons.setDefaultUnit(kony.flex.DP);
        var flxRightButtons = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "centerY": "50%",
            "clipBounds": true,
            "height": "100%",
            "id": "flxRightButtons",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "isModalContainer": false,
            "right": "0px",
            "skin": "slFbox",
            "width": "300dp",
            "zIndex": 1
        }, controller.args[0], "flxRightButtons"), extendConfig({}, controller.args[1], "flxRightButtons"), extendConfig({}, controller.args[2], "flxRightButtons"));
        flxRightButtons.setDefaultUnit(kony.flex.DP);
        var btnSave = new kony.ui.Button(extendConfig({
            "centerY": "50%",
            "height": "40dp",
            "id": "btnSave",
            "isVisible": true,
            "right": "20dp",
            "skin": "sknBtn003E75LatoRegular13pxFFFFFFRad20px",
            "text": "SAVE",
            "top": "0%",
            "width": "100px",
            "zIndex": 1
        }, controller.args[0], "btnSave"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_CENTER,
            "displayText": true,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "btnSave"), extendConfig({}, controller.args[2], "btnSave"));
        var btnCancel = new kony.ui.Button(extendConfig({
            "centerY": "50%",
            "focusSkin": "sknBtnLatoRegular8b96a513pxKAhover",
            "height": "40dp",
            "id": "btnCancel",
            "isVisible": true,
            "right": "135dp",
            "skin": "sknbtnf7f7faLatoRegular4f555dBorder1px8b96a5",
            "i18n_text": "kony.i18n.getLocalizedString(\"i18n.permission.CANCEL\")",
            "top": "0%",
            "width": "100px",
            "zIndex": 2
        }, controller.args[0], "btnCancel"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_CENTER,
            "displayText": true,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "btnCancel"), extendConfig({
            "hoverSkin": "sknBtnLatoRegular8b96a513pxKAhover"
        }, controller.args[2], "btnCancel"));
        flxRightButtons.add(btnSave, btnCancel);
        flxCommonButtons.add(flxRightButtons);
        flxFeatures.add(flxHeader, flxFeaturesSegment, flxSeperator1, flxCommonButtons);
        var flxReview = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "clipBounds": true,
            "height": "100%",
            "id": "flxReview",
            "isVisible": false,
            "layoutType": kony.flex.FREE_FORM,
            "left": "0dp",
            "isModalContainer": false,
            "skin": "slFbox",
            "top": "0dp",
            "width": "100%",
            "zIndex": 1
        }, controller.args[0], "flxReview"), extendConfig({}, controller.args[1], "flxReview"), extendConfig({}, controller.args[2], "flxReview"));
        flxReview.setDefaultUnit(kony.flex.DP);
        var flxReviewHeader = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "clipBounds": true,
            "height": "62dp",
            "id": "flxReviewHeader",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "left": "0dp",
            "isModalContainer": false,
            "skin": "slFbox",
            "top": "0dp",
            "width": "100%",
            "zIndex": 1
        }, controller.args[0], "flxReviewHeader"), extendConfig({}, controller.args[1], "flxReviewHeader"), extendConfig({}, controller.args[2], "flxReviewHeader"));
        flxReviewHeader.setDefaultUnit(kony.flex.DP);
        var lblReview = new kony.ui.Label(extendConfig({
            "centerY": "50%",
            "id": "lblReview",
            "isVisible": true,
            "left": "20dp",
            "skin": "sknLatoRegular192B4518px",
            "text": "Features",
            "top": "23dp",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, controller.args[0], "lblReview"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblReview"), extendConfig({}, controller.args[2], "lblReview"));
        var btnAdvancedSelection = new kony.ui.Button(extendConfig({
            "centerY": "50%",
            "height": "22dp",
            "id": "btnAdvancedSelection",
            "isVisible": true,
            "right": "20dp",
            "skin": "sknbtnbrdr485C75rad15px",
            "text": "Advanced Selection",
            "top": "6dp",
            "width": "134dp",
            "zIndex": 1
        }, controller.args[0], "btnAdvancedSelection"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_CENTER,
            "displayText": true,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "btnAdvancedSelection"), extendConfig({}, controller.args[2], "btnAdvancedSelection"));
        var btnAddPermission = new kony.ui.Button(extendConfig({
            "centerY": "50%",
            "height": "22dp",
            "id": "btnAddPermission",
            "isVisible": true,
            "right": "170dp",
            "skin": "sknbtnbrdr485C75rad15px",
            "text": "Add Permissions",
            "top": "6dp",
            "width": "134dp",
            "zIndex": 1
        }, controller.args[0], "btnAddPermission"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_CENTER,
            "displayText": true,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "btnAddPermission"), extendConfig({}, controller.args[2], "btnAddPermission"));
        var flxSeparator = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "bottom": "2dp",
            "clipBounds": true,
            "height": "1dp",
            "id": "flxSeparator",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "left": "10dp",
            "isModalContainer": false,
            "skin": "sknflxD5D9DD",
            "width": "98%",
            "zIndex": 1
        }, controller.args[0], "flxSeparator"), extendConfig({}, controller.args[1], "flxSeparator"), extendConfig({}, controller.args[2], "flxSeparator"));
        flxSeparator.setDefaultUnit(kony.flex.DP);
        flxSeparator.add();
        flxReviewHeader.add(lblReview, btnAdvancedSelection, btnAddPermission, flxSeparator);
        var segReview = new kony.ui.SegmentedUI2(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "bottom": 82,
            "data": [{
                "fontIconCross": "kony.i18n.getLocalizedString(\"i18n.userwidgetmodel.lblClearSearch\")",
                "fontIconCross2": "kony.i18n.getLocalizedString(\"i18n.userwidgetmodel.lblClearSearch\")",
                "fontIconCross3": "kony.i18n.getLocalizedString(\"i18n.userwidgetmodel.lblClearSearch\")",
                "fontIconCross4": "kony.i18n.getLocalizedString(\"i18n.userwidgetmodel.lblClearSearch\")",
                "lblAction1": "Label",
                "lblAction2": "Label",
                "lblAction3": "Label",
                "lblAction4": "Label",
                "lblFeatureName": "Bill Payments",
                "lblFeatureStatusValue": "Active",
                "lblIcon1": "kony.i18n.getLocalizedString(\"i18n.frmAlertsManagement.lblViewAlertStatusKey\")",
                "lblIcon2": "kony.i18n.getLocalizedString(\"i18n.frmAlertsManagement.lblViewAlertStatusKey\")",
                "lblIcon3": "kony.i18n.getLocalizedString(\"i18n.frmAlertsManagement.lblViewAlertStatusKey\")",
                "lblIcon4": "kony.i18n.getLocalizedString(\"i18n.frmAlertsManagement.lblViewAlertStatusKey\")",
                "lblIconStatus": "kony.i18n.getLocalizedString(\"i18n.frmAlertsManagement.lblViewAlertStatusKey\")"
            }, {
                "fontIconCross": "kony.i18n.getLocalizedString(\"i18n.userwidgetmodel.lblClearSearch\")",
                "fontIconCross2": "kony.i18n.getLocalizedString(\"i18n.userwidgetmodel.lblClearSearch\")",
                "fontIconCross3": "kony.i18n.getLocalizedString(\"i18n.userwidgetmodel.lblClearSearch\")",
                "fontIconCross4": "kony.i18n.getLocalizedString(\"i18n.userwidgetmodel.lblClearSearch\")",
                "lblAction1": "Label",
                "lblAction2": "Label",
                "lblAction3": "Label",
                "lblAction4": "Label",
                "lblFeatureName": "Bill Payments",
                "lblFeatureStatusValue": "Active",
                "lblIcon1": "kony.i18n.getLocalizedString(\"i18n.frmAlertsManagement.lblViewAlertStatusKey\")",
                "lblIcon2": "kony.i18n.getLocalizedString(\"i18n.frmAlertsManagement.lblViewAlertStatusKey\")",
                "lblIcon3": "kony.i18n.getLocalizedString(\"i18n.frmAlertsManagement.lblViewAlertStatusKey\")",
                "lblIcon4": "kony.i18n.getLocalizedString(\"i18n.frmAlertsManagement.lblViewAlertStatusKey\")",
                "lblIconStatus": "kony.i18n.getLocalizedString(\"i18n.frmAlertsManagement.lblViewAlertStatusKey\")"
            }, {
                "fontIconCross": "kony.i18n.getLocalizedString(\"i18n.userwidgetmodel.lblClearSearch\")",
                "fontIconCross2": "kony.i18n.getLocalizedString(\"i18n.userwidgetmodel.lblClearSearch\")",
                "fontIconCross3": "kony.i18n.getLocalizedString(\"i18n.userwidgetmodel.lblClearSearch\")",
                "fontIconCross4": "kony.i18n.getLocalizedString(\"i18n.userwidgetmodel.lblClearSearch\")",
                "lblAction1": "Label",
                "lblAction2": "Label",
                "lblAction3": "Label",
                "lblAction4": "Label",
                "lblFeatureName": "Bill Payments",
                "lblFeatureStatusValue": "Active",
                "lblIcon1": "kony.i18n.getLocalizedString(\"i18n.frmAlertsManagement.lblViewAlertStatusKey\")",
                "lblIcon2": "kony.i18n.getLocalizedString(\"i18n.frmAlertsManagement.lblViewAlertStatusKey\")",
                "lblIcon3": "kony.i18n.getLocalizedString(\"i18n.frmAlertsManagement.lblViewAlertStatusKey\")",
                "lblIcon4": "kony.i18n.getLocalizedString(\"i18n.frmAlertsManagement.lblViewAlertStatusKey\")",
                "lblIconStatus": "kony.i18n.getLocalizedString(\"i18n.frmAlertsManagement.lblViewAlertStatusKey\")"
            }, {
                "fontIconCross": "kony.i18n.getLocalizedString(\"i18n.userwidgetmodel.lblClearSearch\")",
                "fontIconCross2": "kony.i18n.getLocalizedString(\"i18n.userwidgetmodel.lblClearSearch\")",
                "fontIconCross3": "kony.i18n.getLocalizedString(\"i18n.userwidgetmodel.lblClearSearch\")",
                "fontIconCross4": "kony.i18n.getLocalizedString(\"i18n.userwidgetmodel.lblClearSearch\")",
                "lblAction1": "Label",
                "lblAction2": "Label",
                "lblAction3": "Label",
                "lblAction4": "Label",
                "lblFeatureName": "Bill Payments",
                "lblFeatureStatusValue": "Active",
                "lblIcon1": "kony.i18n.getLocalizedString(\"i18n.frmAlertsManagement.lblViewAlertStatusKey\")",
                "lblIcon2": "kony.i18n.getLocalizedString(\"i18n.frmAlertsManagement.lblViewAlertStatusKey\")",
                "lblIcon3": "kony.i18n.getLocalizedString(\"i18n.frmAlertsManagement.lblViewAlertStatusKey\")",
                "lblIcon4": "kony.i18n.getLocalizedString(\"i18n.frmAlertsManagement.lblViewAlertStatusKey\")",
                "lblIconStatus": "kony.i18n.getLocalizedString(\"i18n.frmAlertsManagement.lblViewAlertStatusKey\")"
            }, {
                "fontIconCross": "kony.i18n.getLocalizedString(\"i18n.userwidgetmodel.lblClearSearch\")",
                "fontIconCross2": "kony.i18n.getLocalizedString(\"i18n.userwidgetmodel.lblClearSearch\")",
                "fontIconCross3": "kony.i18n.getLocalizedString(\"i18n.userwidgetmodel.lblClearSearch\")",
                "fontIconCross4": "kony.i18n.getLocalizedString(\"i18n.userwidgetmodel.lblClearSearch\")",
                "lblAction1": "Label",
                "lblAction2": "Label",
                "lblAction3": "Label",
                "lblAction4": "Label",
                "lblFeatureName": "Bill Payments",
                "lblFeatureStatusValue": "Active",
                "lblIcon1": "kony.i18n.getLocalizedString(\"i18n.frmAlertsManagement.lblViewAlertStatusKey\")",
                "lblIcon2": "kony.i18n.getLocalizedString(\"i18n.frmAlertsManagement.lblViewAlertStatusKey\")",
                "lblIcon3": "kony.i18n.getLocalizedString(\"i18n.frmAlertsManagement.lblViewAlertStatusKey\")",
                "lblIcon4": "kony.i18n.getLocalizedString(\"i18n.frmAlertsManagement.lblViewAlertStatusKey\")",
                "lblIconStatus": "kony.i18n.getLocalizedString(\"i18n.frmAlertsManagement.lblViewAlertStatusKey\")"
            }, {
                "fontIconCross": "kony.i18n.getLocalizedString(\"i18n.userwidgetmodel.lblClearSearch\")",
                "fontIconCross2": "kony.i18n.getLocalizedString(\"i18n.userwidgetmodel.lblClearSearch\")",
                "fontIconCross3": "kony.i18n.getLocalizedString(\"i18n.userwidgetmodel.lblClearSearch\")",
                "fontIconCross4": "kony.i18n.getLocalizedString(\"i18n.userwidgetmodel.lblClearSearch\")",
                "lblAction1": "Label",
                "lblAction2": "Label",
                "lblAction3": "Label",
                "lblAction4": "Label",
                "lblFeatureName": "Bill Payments",
                "lblFeatureStatusValue": "Active",
                "lblIcon1": "kony.i18n.getLocalizedString(\"i18n.frmAlertsManagement.lblViewAlertStatusKey\")",
                "lblIcon2": "kony.i18n.getLocalizedString(\"i18n.frmAlertsManagement.lblViewAlertStatusKey\")",
                "lblIcon3": "kony.i18n.getLocalizedString(\"i18n.frmAlertsManagement.lblViewAlertStatusKey\")",
                "lblIcon4": "kony.i18n.getLocalizedString(\"i18n.frmAlertsManagement.lblViewAlertStatusKey\")",
                "lblIconStatus": "kony.i18n.getLocalizedString(\"i18n.frmAlertsManagement.lblViewAlertStatusKey\")"
            }, {
                "fontIconCross": "kony.i18n.getLocalizedString(\"i18n.userwidgetmodel.lblClearSearch\")",
                "fontIconCross2": "kony.i18n.getLocalizedString(\"i18n.userwidgetmodel.lblClearSearch\")",
                "fontIconCross3": "kony.i18n.getLocalizedString(\"i18n.userwidgetmodel.lblClearSearch\")",
                "fontIconCross4": "kony.i18n.getLocalizedString(\"i18n.userwidgetmodel.lblClearSearch\")",
                "lblAction1": "Label",
                "lblAction2": "Label",
                "lblAction3": "Label",
                "lblAction4": "Label",
                "lblFeatureName": "Bill Payments",
                "lblFeatureStatusValue": "Active",
                "lblIcon1": "kony.i18n.getLocalizedString(\"i18n.frmAlertsManagement.lblViewAlertStatusKey\")",
                "lblIcon2": "kony.i18n.getLocalizedString(\"i18n.frmAlertsManagement.lblViewAlertStatusKey\")",
                "lblIcon3": "kony.i18n.getLocalizedString(\"i18n.frmAlertsManagement.lblViewAlertStatusKey\")",
                "lblIcon4": "kony.i18n.getLocalizedString(\"i18n.frmAlertsManagement.lblViewAlertStatusKey\")",
                "lblIconStatus": "kony.i18n.getLocalizedString(\"i18n.frmAlertsManagement.lblViewAlertStatusKey\")"
            }, {
                "fontIconCross": "kony.i18n.getLocalizedString(\"i18n.userwidgetmodel.lblClearSearch\")",
                "fontIconCross2": "kony.i18n.getLocalizedString(\"i18n.userwidgetmodel.lblClearSearch\")",
                "fontIconCross3": "kony.i18n.getLocalizedString(\"i18n.userwidgetmodel.lblClearSearch\")",
                "fontIconCross4": "kony.i18n.getLocalizedString(\"i18n.userwidgetmodel.lblClearSearch\")",
                "lblAction1": "Label",
                "lblAction2": "Label",
                "lblAction3": "Label",
                "lblAction4": "Label",
                "lblFeatureName": "Bill Payments",
                "lblFeatureStatusValue": "Active",
                "lblIcon1": "kony.i18n.getLocalizedString(\"i18n.frmAlertsManagement.lblViewAlertStatusKey\")",
                "lblIcon2": "kony.i18n.getLocalizedString(\"i18n.frmAlertsManagement.lblViewAlertStatusKey\")",
                "lblIcon3": "kony.i18n.getLocalizedString(\"i18n.frmAlertsManagement.lblViewAlertStatusKey\")",
                "lblIcon4": "kony.i18n.getLocalizedString(\"i18n.frmAlertsManagement.lblViewAlertStatusKey\")",
                "lblIconStatus": "kony.i18n.getLocalizedString(\"i18n.frmAlertsManagement.lblViewAlertStatusKey\")"
            }, {
                "fontIconCross": "kony.i18n.getLocalizedString(\"i18n.userwidgetmodel.lblClearSearch\")",
                "fontIconCross2": "kony.i18n.getLocalizedString(\"i18n.userwidgetmodel.lblClearSearch\")",
                "fontIconCross3": "kony.i18n.getLocalizedString(\"i18n.userwidgetmodel.lblClearSearch\")",
                "fontIconCross4": "kony.i18n.getLocalizedString(\"i18n.userwidgetmodel.lblClearSearch\")",
                "lblAction1": "Label",
                "lblAction2": "Label",
                "lblAction3": "Label",
                "lblAction4": "Label",
                "lblFeatureName": "Bill Payments",
                "lblFeatureStatusValue": "Active",
                "lblIcon1": "kony.i18n.getLocalizedString(\"i18n.frmAlertsManagement.lblViewAlertStatusKey\")",
                "lblIcon2": "kony.i18n.getLocalizedString(\"i18n.frmAlertsManagement.lblViewAlertStatusKey\")",
                "lblIcon3": "kony.i18n.getLocalizedString(\"i18n.frmAlertsManagement.lblViewAlertStatusKey\")",
                "lblIcon4": "kony.i18n.getLocalizedString(\"i18n.frmAlertsManagement.lblViewAlertStatusKey\")",
                "lblIconStatus": "kony.i18n.getLocalizedString(\"i18n.frmAlertsManagement.lblViewAlertStatusKey\")"
            }, {
                "fontIconCross": "kony.i18n.getLocalizedString(\"i18n.userwidgetmodel.lblClearSearch\")",
                "fontIconCross2": "kony.i18n.getLocalizedString(\"i18n.userwidgetmodel.lblClearSearch\")",
                "fontIconCross3": "kony.i18n.getLocalizedString(\"i18n.userwidgetmodel.lblClearSearch\")",
                "fontIconCross4": "kony.i18n.getLocalizedString(\"i18n.userwidgetmodel.lblClearSearch\")",
                "lblAction1": "Label",
                "lblAction2": "Label",
                "lblAction3": "Label",
                "lblAction4": "Label",
                "lblFeatureName": "Bill Payments",
                "lblFeatureStatusValue": "Active",
                "lblIcon1": "kony.i18n.getLocalizedString(\"i18n.frmAlertsManagement.lblViewAlertStatusKey\")",
                "lblIcon2": "kony.i18n.getLocalizedString(\"i18n.frmAlertsManagement.lblViewAlertStatusKey\")",
                "lblIcon3": "kony.i18n.getLocalizedString(\"i18n.frmAlertsManagement.lblViewAlertStatusKey\")",
                "lblIcon4": "kony.i18n.getLocalizedString(\"i18n.frmAlertsManagement.lblViewAlertStatusKey\")",
                "lblIconStatus": "kony.i18n.getLocalizedString(\"i18n.frmAlertsManagement.lblViewAlertStatusKey\")"
            }],
            "groupCells": false,
            "id": "segReview",
            "isVisible": true,
            "left": "0dp",
            "needPageIndicator": true,
            "pageOffDotImage": "pageoffdot.png",
            "pageOnDotImage": "pageondot.png",
            "retainSelection": false,
            "rowFocusSkin": "seg2Focus",
            "rowSkin": "seg2Normal",
            "rowTemplate": "flxFeaturesListSelected",
            "sectionHeaderSkin": "sliPhoneSegmentHeader",
            "selectionBehavior": constants.SEGUI_DEFAULT_BEHAVIOR,
            "separatorColor": "aaaaaa00",
            "separatorRequired": false,
            "separatorThickness": 1,
            "showScrollbars": false,
            "top": "60dp",
            "viewType": constants.SEGUI_VIEW_TYPE_TABLEVIEW,
            "widgetDataMap": {
                "flxAction1": "flxAction1",
                "flxAction2": "flxAction2",
                "flxAction3": "flxAction3",
                "flxAction4": "flxAction4",
                "flxActions": "flxActions",
                "flxFeatureNameContainer": "flxFeatureNameContainer",
                "flxFeatureStatus": "flxFeatureStatus",
                "flxFeaturesListSelected": "flxFeaturesListSelected",
                "flxSeperator": "flxSeperator",
                "fontIconCross": "fontIconCross",
                "fontIconCross2": "fontIconCross2",
                "fontIconCross3": "fontIconCross3",
                "fontIconCross4": "fontIconCross4",
                "lblAction1": "lblAction1",
                "lblAction2": "lblAction2",
                "lblAction3": "lblAction3",
                "lblAction4": "lblAction4",
                "lblFeatureName": "lblFeatureName",
                "lblFeatureStatusValue": "lblFeatureStatusValue",
                "lblIcon1": "lblIcon1",
                "lblIcon2": "lblIcon2",
                "lblIcon3": "lblIcon3",
                "lblIcon4": "lblIcon4",
                "lblIconStatus": "lblIconStatus"
            },
            "width": "99%",
            "zIndex": 1
        }, controller.args[0], "segReview"), extendConfig({
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "segReview"), extendConfig({}, controller.args[2], "segReview"));
        var flxSeperator2 = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "bottom": "80dp",
            "clipBounds": true,
            "height": "1dp",
            "id": "flxSeperator2",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "left": "10dp",
            "isModalContainer": false,
            "skin": "sknflxD5D9DD",
            "width": "98%",
            "zIndex": 1
        }, controller.args[0], "flxSeperator2"), extendConfig({}, controller.args[1], "flxSeperator2"), extendConfig({}, controller.args[2], "flxSeperator2"));
        flxSeperator2.setDefaultUnit(kony.flex.DP);
        flxSeperator2.add();
        var commonButtons = new com.adminConsole.common.commonButtons(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "bottom": "0dp",
            "height": "80px",
            "id": "commonButtons",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "left": "0dp",
            "masterType": constants.MASTER_TYPE_DEFAULT,
            "isModalContainer": false,
            "skin": "slFbox",
            "width": "100%",
            "overrides": {
                "btnCancel": {
                    "isVisible": true,
                    "left": "20dp"
                },
                "btnNext": {
                    "isVisible": true
                },
                "btnSave": {
                    "right": "20dp",
                    "text": "ADD SERVICE DEFINITION",
                    "width": "200px"
                },
                "commonButtons": {
                    "bottom": "0dp",
                    "top": "viz.val_cleared"
                },
                "flxRightButtons": {
                    "width": "330px"
                }
            }
        }, controller.args[0], "commonButtons"), extendConfig({
            "overrides": {}
        }, controller.args[1], "commonButtons"), extendConfig({
            "overrides": {}
        }, controller.args[2], "commonButtons"));
        flxReview.add(flxReviewHeader, segReview, flxSeperator2, commonButtons);
        featureAndActions.add(flxFeatures, flxReview);
        return featureAndActions;
    }
})