define("com/adminConsole/serviceDefinition/featureAndActions/userfeatureAndActionsController", function() {
    return {
        showReviewPage: function() {
            scopeObj = this;
            scopeObj.view.flxFeatures.setVisibility(false);
            scopeObj.view.flxReview.setVisibility(true);
        }
    };
});
define("com/adminConsole/serviceDefinition/featureAndActions/featureAndActionsControllerActions", {
    /*
          This is an auto generated file and any modifications to it may result in corruption of the action sequence.
        */
});
define("com/adminConsole/serviceDefinition/featureAndActions/featureAndActionsController", ["com/adminConsole/serviceDefinition/featureAndActions/userfeatureAndActionsController", "com/adminConsole/serviceDefinition/featureAndActions/featureAndActionsControllerActions"], function() {
    var controller = require("com/adminConsole/serviceDefinition/featureAndActions/userfeatureAndActionsController");
    var actions = require("com/adminConsole/serviceDefinition/featureAndActions/featureAndActionsControllerActions");
    for (var key in actions) {
        controller[key] = actions[key];
    }
    return controller;
});
