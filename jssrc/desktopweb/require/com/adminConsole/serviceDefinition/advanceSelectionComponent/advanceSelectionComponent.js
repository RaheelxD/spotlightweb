define(function() {
    return function(controller) {
        var advanceSelectionComponent = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "clipBounds": true,
            "isMaster": true,
            "height": "100%",
            "id": "advanceSelectionComponent",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "left": "0dp",
            "isModalContainer": false,
            "skin": "slFbox",
            "top": "0dp",
            "width": "100%"
        }, controller.args[0], "advanceSelectionComponent"), extendConfig({}, controller.args[1], "advanceSelectionComponent"), extendConfig({}, controller.args[2], "advanceSelectionComponent"));
        advanceSelectionComponent.setDefaultUnit(kony.flex.DP);
        var flxAdvancedSelection = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "bottom": "0dp",
            "clipBounds": true,
            "id": "flxAdvancedSelection",
            "isVisible": true,
            "layoutType": kony.flex.FLOW_VERTICAL,
            "left": "0dp",
            "isModalContainer": false,
            "right": "0dp",
            "skin": "slFbox",
            "top": "0dp",
            "zIndex": 1
        }, controller.args[0], "flxAdvancedSelection"), extendConfig({}, controller.args[1], "flxAdvancedSelection"), extendConfig({}, controller.args[2], "flxAdvancedSelection"));
        flxAdvancedSelection.setDefaultUnit(kony.flex.DP);
        var flxAdvanceSelectionHeader = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "bottom": 0,
            "clipBounds": true,
            "height": "14%",
            "id": "flxAdvanceSelectionHeader",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "left": "0dp",
            "isModalContainer": false,
            "right": 0,
            "skin": "slFbox",
            "top": "0dp",
            "zIndex": 1
        }, controller.args[0], "flxAdvanceSelectionHeader"), extendConfig({}, controller.args[1], "flxAdvanceSelectionHeader"), extendConfig({}, controller.args[2], "flxAdvanceSelectionHeader"));
        flxAdvanceSelectionHeader.setDefaultUnit(kony.flex.DP);
        var lblHeading = new kony.ui.Label(extendConfig({
            "id": "lblHeading",
            "isVisible": true,
            "left": "20dp",
            "skin": "sknLbl192B4518pxLatoReg",
            "text": "Advanced Selection",
            "top": "20px",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, controller.args[0], "lblHeading"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblHeading"), extendConfig({}, controller.args[2], "lblHeading"));
        var flxSearch = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "clipBounds": true,
            "height": "100%",
            "id": "flxSearch",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "isModalContainer": false,
            "right": "20dp",
            "skin": "CopyslFbox2",
            "top": "15px",
            "width": "350px",
            "zIndex": 1
        }, controller.args[0], "flxSearch"), extendConfig({}, controller.args[1], "flxSearch"), extendConfig({}, controller.args[2], "flxSearch"));
        flxSearch.setDefaultUnit(kony.flex.DP);
        var flxSearchContainer = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "clipBounds": true,
            "height": "35px",
            "id": "flxSearchContainer",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "left": "0dp",
            "isModalContainer": false,
            "skin": "sknflxd5d9ddop100",
            "top": "8dp",
            "width": "350px",
            "zIndex": 1
        }, controller.args[0], "flxSearchContainer"), extendConfig({}, controller.args[1], "flxSearchContainer"), extendConfig({}, controller.args[2], "flxSearchContainer"));
        flxSearchContainer.setDefaultUnit(kony.flex.DP);
        var fontIconSearchImg = new kony.ui.Label(extendConfig({
            "centerY": "50%",
            "id": "fontIconSearchImg",
            "isVisible": true,
            "left": "12px",
            "skin": "sknFontIconSearchImg20px",
            "i18n_text": "kony.i18n.getLocalizedString(\"i18n.userwidgetmodel.lblSearchIcon\")",
            "top": "0dp",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, controller.args[0], "fontIconSearchImg"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "fontIconSearchImg"), extendConfig({}, controller.args[2], "fontIconSearchImg"));
        var tbxSearchBox = new kony.ui.TextBox2(extendConfig({
            "autoCapitalize": constants.TEXTBOX_AUTO_CAPITALIZE_NONE,
            "centerY": "50%",
            "height": "30px",
            "id": "tbxSearchBox",
            "isVisible": true,
            "keyBoardStyle": constants.TEXTBOX_KEY_BOARD_STYLE_DEFAULT,
            "left": "30dp",
            "onTouchStart": controller.AS_TextField_cfab06859f0f43eca99495891ee290d5,
            "placeholder": "Search By Feature",
            "right": "35px",
            "secureTextEntry": false,
            "skin": "skntbxffffffNoBorderlato35475f14px",
            "textInputMode": constants.TEXTBOX_INPUT_MODE_ANY,
            "zIndex": 1
        }, controller.args[0], "tbxSearchBox"), extendConfig({
            "containerHeightMode": constants.TEXTBOX_FONT_METRICS_DRIVEN_HEIGHT,
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [2, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "tbxSearchBox"), extendConfig({
            "autoCorrect": false
        }, controller.args[2], "tbxSearchBox"));
        var flxClearSearchImage = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "clipBounds": true,
            "height": "100%",
            "id": "flxClearSearchImage",
            "isVisible": false,
            "layoutType": kony.flex.FREE_FORM,
            "isModalContainer": false,
            "right": "0px",
            "skin": "sknCursor",
            "top": "0dp",
            "width": "35px",
            "zIndex": 1
        }, controller.args[0], "flxClearSearchImage"), extendConfig({}, controller.args[1], "flxClearSearchImage"), extendConfig({}, controller.args[2], "flxClearSearchImage"));
        flxClearSearchImage.setDefaultUnit(kony.flex.DP);
        var fontIconCross = new kony.ui.Label(extendConfig({
            "centerY": "50%",
            "id": "fontIconCross",
            "isVisible": true,
            "left": "5px",
            "skin": "sknFontIconSearchCross16px",
            "i18n_text": "kony.i18n.getLocalizedString(\"i18n.userwidgetmodel.lblClearSearch\")",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, controller.args[0], "fontIconCross"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "fontIconCross"), extendConfig({}, controller.args[2], "fontIconCross"));
        flxClearSearchImage.add(fontIconCross);
        flxSearchContainer.add(fontIconSearchImg, tbxSearchBox, flxClearSearchImage);
        flxSearch.add(flxSearchContainer);
        flxAdvanceSelectionHeader.add(lblHeading, flxSearch);
        var flxAdvanceSelectionContent = new kony.ui.FlexScrollContainer(extendConfig({
            "allowHorizontalBounce": false,
            "allowVerticalBounce": true,
            "bounces": true,
            "centerX": "50%",
            "clipBounds": true,
            "enableScrolling": true,
            "height": "72%",
            "horizontalScrollIndicator": true,
            "id": "flxAdvanceSelectionContent",
            "isVisible": true,
            "layoutType": kony.flex.FLOW_VERTICAL,
            "left": "0dp",
            "pagingEnabled": false,
            "right": 0,
            "scrollDirection": kony.flex.SCROLL_VERTICAL,
            "skin": "slFSbox",
            "top": "0dp",
            "verticalScrollIndicator": true,
            "width": "94%",
            "zIndex": 1
        }, controller.args[0], "flxAdvanceSelectionContent"), extendConfig({}, controller.args[1], "flxAdvanceSelectionContent"), extendConfig({}, controller.args[2], "flxAdvanceSelectionContent"));
        flxAdvanceSelectionContent.setDefaultUnit(kony.flex.DP);
        flxAdvanceSelectionContent.add();
        var flxCommonButtons = new kony.ui.FlexContainer(extendConfig({
            "bottom": "0dp",
            "clipBounds": true,
            "height": "14%",
            "id": "flxCommonButtons",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "left": "0dp",
            "isModalContainer": false,
            "skin": "slFbox",
            "width": "100%"
        }, controller.args[0], "flxCommonButtons"), extendConfig({}, controller.args[1], "flxCommonButtons"), extendConfig({}, controller.args[2], "flxCommonButtons"));
        flxCommonButtons.setDefaultUnit(kony.flex.DP);
        var flxSeparator4 = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "clipBounds": true,
            "height": "1dp",
            "id": "flxSeparator4",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "left": "0dp",
            "isModalContainer": false,
            "skin": "sknflxD5D9DD",
            "top": "5dp",
            "width": "100%",
            "zIndex": 1
        }, controller.args[0], "flxSeparator4"), extendConfig({}, controller.args[1], "flxSeparator4"), extendConfig({}, controller.args[2], "flxSeparator4"));
        flxSeparator4.setDefaultUnit(kony.flex.DP);
        flxSeparator4.add();
        var flxRightButtons = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "centerY": "50%",
            "clipBounds": true,
            "height": "100%",
            "id": "flxRightButtons",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "isModalContainer": false,
            "right": "20dp",
            "skin": "slFbox",
            "width": "250px",
            "zIndex": 1
        }, controller.args[0], "flxRightButtons"), extendConfig({}, controller.args[1], "flxRightButtons"), extendConfig({}, controller.args[2], "flxRightButtons"));
        flxRightButtons.setDefaultUnit(kony.flex.DP);
        var btnSave = new kony.ui.Button(extendConfig({
            "centerY": "50%",
            "focusSkin": "sknBtn003E75LatoRegular13pxFFFFFFRad20px",
            "height": "40dp",
            "id": "btnSave",
            "isVisible": true,
            "right": "0px",
            "skin": "sknBtn003E75LatoRegular13pxFFFFFFRad20px",
            "text": "SAVE",
            "top": "0%",
            "width": "125px",
            "zIndex": 1
        }, controller.args[0], "btnSave"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_CENTER,
            "displayText": true,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "btnSave"), extendConfig({}, controller.args[2], "btnSave"));
        var btnCancel = new kony.ui.Button(extendConfig({
            "centerY": "50%",
            "focusSkin": "sknBtnLatoRegular8b96a513pxKAhover",
            "height": "40dp",
            "id": "btnCancel",
            "isVisible": true,
            "right": "145dp",
            "skin": "sknbtnf7f7faLatoRegular4f555dBorder1px8b96a5",
            "i18n_text": "kony.i18n.getLocalizedString(\"i18n.permission.CANCEL\")",
            "top": "0%",
            "width": "100px",
            "zIndex": 2
        }, controller.args[0], "btnCancel"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_CENTER,
            "displayText": true,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "btnCancel"), extendConfig({}, controller.args[2], "btnCancel"));
        flxRightButtons.add(btnSave, btnCancel);
        flxCommonButtons.add(flxSeparator4, flxRightButtons);
        flxAdvancedSelection.add(flxAdvanceSelectionHeader, flxAdvanceSelectionContent, flxCommonButtons);
        advanceSelectionComponent.add(flxAdvancedSelection);
        return advanceSelectionComponent;
    }
})