define("com/adminConsole/serviceDefinition/advanceSelectionComponent/useradvanceSelectionComponentController", function() {
    return {};
});
define("com/adminConsole/serviceDefinition/advanceSelectionComponent/advanceSelectionComponentControllerActions", {
    /*
        This is an auto generated file and any modifications to it may result in corruption of the action sequence.
    */
});
define("com/adminConsole/serviceDefinition/advanceSelectionComponent/advanceSelectionComponentController", ["com/adminConsole/serviceDefinition/advanceSelectionComponent/useradvanceSelectionComponentController", "com/adminConsole/serviceDefinition/advanceSelectionComponent/advanceSelectionComponentControllerActions"], function() {
    var controller = require("com/adminConsole/serviceDefinition/advanceSelectionComponent/useradvanceSelectionComponentController");
    var actions = require("com/adminConsole/serviceDefinition/advanceSelectionComponent/advanceSelectionComponentControllerActions");
    for (var key in actions) {
        controller[key] = actions[key];
    }
    return controller;
});
