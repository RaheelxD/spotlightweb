define(function() {
    return function(controller) {
        var associatedRoles = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "clipBounds": true,
            "isMaster": true,
            "height": "100%",
            "id": "associatedRoles",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "left": "0dp",
            "isModalContainer": false,
            "skin": "slFbox",
            "top": "0dp",
            "width": "100%"
        }, controller.args[0], "associatedRoles"), extendConfig({}, controller.args[1], "associatedRoles"), extendConfig({}, controller.args[2], "associatedRoles"));
        associatedRoles.setDefaultUnit(kony.flex.DP);
        var flxAssociatedRoles = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "bottom": "78dp",
            "clipBounds": true,
            "id": "flxAssociatedRoles",
            "isVisible": true,
            "layoutType": kony.flex.FLOW_VERTICAL,
            "left": "0dp",
            "isModalContainer": false,
            "right": "0dp",
            "skin": "slFbox",
            "top": "0dp",
            "zIndex": 1
        }, controller.args[0], "flxAssociatedRoles"), extendConfig({}, controller.args[1], "flxAssociatedRoles"), extendConfig({}, controller.args[2], "flxAssociatedRoles"));
        flxAssociatedRoles.setDefaultUnit(kony.flex.DP);
        var flxRolesHeader = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "bottom": 0,
            "clipBounds": true,
            "height": "60dp",
            "id": "flxRolesHeader",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "left": "0dp",
            "isModalContainer": false,
            "right": 0,
            "skin": "slFbox",
            "top": "0dp",
            "zIndex": 1
        }, controller.args[0], "flxRolesHeader"), extendConfig({}, controller.args[1], "flxRolesHeader"), extendConfig({}, controller.args[2], "flxRolesHeader"));
        flxRolesHeader.setDefaultUnit(kony.flex.DP);
        var lblHeading = new kony.ui.Label(extendConfig({
            "id": "lblHeading",
            "isVisible": true,
            "left": "20dp",
            "skin": "sknLbl192B4518pxLatoReg",
            "text": "Roles",
            "top": "20px",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, controller.args[0], "lblHeading"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblHeading"), extendConfig({}, controller.args[2], "lblHeading"));
        var flxSearch = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "clipBounds": true,
            "height": "100%",
            "id": "flxSearch",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "isModalContainer": false,
            "right": "20dp",
            "skin": "CopyslFbox2",
            "top": "15px",
            "width": "350px",
            "zIndex": 1
        }, controller.args[0], "flxSearch"), extendConfig({}, controller.args[1], "flxSearch"), extendConfig({}, controller.args[2], "flxSearch"));
        flxSearch.setDefaultUnit(kony.flex.DP);
        var flxSearchContainer = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "clipBounds": true,
            "height": "35px",
            "id": "flxSearchContainer",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "left": "0dp",
            "isModalContainer": false,
            "skin": "sknflxd5d9ddop100",
            "top": "8dp",
            "width": "350px",
            "zIndex": 1
        }, controller.args[0], "flxSearchContainer"), extendConfig({}, controller.args[1], "flxSearchContainer"), extendConfig({}, controller.args[2], "flxSearchContainer"));
        flxSearchContainer.setDefaultUnit(kony.flex.DP);
        var fontIconSearchImg = new kony.ui.Label(extendConfig({
            "centerY": "50%",
            "id": "fontIconSearchImg",
            "isVisible": true,
            "left": "12px",
            "skin": "sknFontIconSearchImg20px",
            "i18n_text": "kony.i18n.getLocalizedString(\"i18n.userwidgetmodel.lblSearchIcon\")",
            "top": "0dp",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, controller.args[0], "fontIconSearchImg"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "fontIconSearchImg"), extendConfig({}, controller.args[2], "fontIconSearchImg"));
        var tbxSearchBox = new kony.ui.TextBox2(extendConfig({
            "autoCapitalize": constants.TEXTBOX_AUTO_CAPITALIZE_NONE,
            "centerY": "50%",
            "height": "30px",
            "id": "tbxSearchBox",
            "isVisible": true,
            "keyBoardStyle": constants.TEXTBOX_KEY_BOARD_STYLE_DEFAULT,
            "left": "30dp",
            "onTouchStart": controller.AS_TextField_cfab06859f0f43eca99495891ee290d5,
            "placeholder": "Search By Role Name",
            "right": "35px",
            "secureTextEntry": false,
            "skin": "skntbxffffffNoBorderlato35475f14px",
            "textInputMode": constants.TEXTBOX_INPUT_MODE_ANY,
            "zIndex": 1
        }, controller.args[0], "tbxSearchBox"), extendConfig({
            "containerHeightMode": constants.TEXTBOX_FONT_METRICS_DRIVEN_HEIGHT,
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [2, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "tbxSearchBox"), extendConfig({
            "autoCorrect": false
        }, controller.args[2], "tbxSearchBox"));
        var flxClearSearchImage = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "clipBounds": true,
            "height": "100%",
            "id": "flxClearSearchImage",
            "isVisible": false,
            "layoutType": kony.flex.FREE_FORM,
            "isModalContainer": false,
            "right": "0px",
            "skin": "sknCursor",
            "top": "0dp",
            "width": "35px",
            "zIndex": 1
        }, controller.args[0], "flxClearSearchImage"), extendConfig({}, controller.args[1], "flxClearSearchImage"), extendConfig({}, controller.args[2], "flxClearSearchImage"));
        flxClearSearchImage.setDefaultUnit(kony.flex.DP);
        var fontIconCross = new kony.ui.Label(extendConfig({
            "centerY": "50%",
            "id": "fontIconCross",
            "isVisible": true,
            "left": "5px",
            "skin": "sknFontIconSearchCross16px",
            "i18n_text": "kony.i18n.getLocalizedString(\"i18n.userwidgetmodel.lblClearSearch\")",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, controller.args[0], "fontIconCross"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "fontIconCross"), extendConfig({}, controller.args[2], "fontIconCross"));
        flxClearSearchImage.add(fontIconCross);
        flxSearchContainer.add(fontIconSearchImg, tbxSearchBox, flxClearSearchImage);
        flxSearch.add(flxSearchContainer);
        flxRolesHeader.add(lblHeading, flxSearch);
        var flxRolesEdit = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "clipBounds": true,
            "height": "45px",
            "id": "flxRolesEdit",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "left": "0dp",
            "isModalContainer": false,
            "skin": "slFbox",
            "top": "0dp",
            "width": "100%",
            "zIndex": 1
        }, controller.args[0], "flxRolesEdit"), extendConfig({}, controller.args[1], "flxRolesEdit"), extendConfig({}, controller.args[2], "flxRolesEdit"));
        flxRolesEdit.setDefaultUnit(kony.flex.DP);
        var btnChangeDefaultRole = new kony.ui.Button(extendConfig({
            "bottom": "0px",
            "centerY": "50%",
            "focusSkin": "sknbtnf7f7faLatoRegular12Px485c75Border1px485C75Radius20px",
            "height": "22dp",
            "id": "btnChangeDefaultRole",
            "isVisible": true,
            "right": "20px",
            "skin": "sknbtnf7f7faLatoRegular12Px485c75Border1px485C75Radius20px",
            "text": "Change Default Role",
            "width": "150dp",
            "zIndex": 1
        }, controller.args[0], "btnChangeDefaultRole"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_CENTER,
            "displayText": true,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "btnChangeDefaultRole"), extendConfig({
            "hoverSkin": "sknbtnffffffLatoRegular12Px485c75Border1px485C75Radius20px"
        }, controller.args[2], "btnChangeDefaultRole"));
        flxRolesEdit.add(btnChangeDefaultRole);
        var flxRoleUndoHeader = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "clipBounds": true,
            "height": "50px",
            "id": "flxRoleUndoHeader",
            "isVisible": false,
            "layoutType": kony.flex.FREE_FORM,
            "left": "20dp",
            "isModalContainer": false,
            "right": "20dp",
            "skin": "slFbox",
            "top": "0dp",
            "zIndex": 1
        }, controller.args[0], "flxRoleUndoHeader"), extendConfig({}, controller.args[1], "flxRoleUndoHeader"), extendConfig({}, controller.args[2], "flxRoleUndoHeader"));
        flxRoleUndoHeader.setDefaultUnit(kony.flex.DP);
        var lblDefaultRole = new kony.ui.Label(extendConfig({
            "height": "30px",
            "id": "lblDefaultRole",
            "isVisible": true,
            "left": "0dp",
            "skin": "sknLbl192B4518pxLatoReg",
            "text": "Change Default Role",
            "top": "10dp",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, controller.args[0], "lblDefaultRole"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblDefaultRole"), extendConfig({}, controller.args[2], "lblDefaultRole"));
        var backToRolesList = new com.adminConsole.customerMang.backToPageHeader(extendConfig({
            "height": "20px",
            "id": "backToRolesList",
            "isVisible": true,
            "layoutType": kony.flex.FLOW_HORIZONTAL,
            "masterType": constants.MASTER_TYPE_DEFAULT,
            "isModalContainer": false,
            "right": "20dp",
            "skin": "slFbox",
            "top": "10dp",
            "width": "80dp",
            "overrides": {
                "backToPageHeader": {
                    "left": "viz.val_cleared",
                    "right": "20dp",
                    "top": "10dp",
                    "width": "80dp"
                },
                "btnBack": {
                    "text": "Back"
                },
                "flxBack": {
                    "left": "0px"
                }
            }
        }, controller.args[0], "backToRolesList"), extendConfig({
            "overrides": {}
        }, controller.args[1], "backToRolesList"), extendConfig({
            "overrides": {}
        }, controller.args[2], "backToRolesList"));
        flxRoleUndoHeader.add(lblDefaultRole, backToRolesList);
        var flxRolesSegment = new kony.ui.FlexScrollContainer(extendConfig({
            "allowHorizontalBounce": false,
            "allowVerticalBounce": true,
            "bounces": true,
            "centerX": "50%",
            "clipBounds": true,
            "enableScrolling": true,
            "height": "75%",
            "horizontalScrollIndicator": true,
            "id": "flxRolesSegment",
            "isVisible": true,
            "layoutType": kony.flex.FLOW_VERTICAL,
            "left": "0dp",
            "pagingEnabled": false,
            "right": 0,
            "scrollDirection": kony.flex.SCROLL_VERTICAL,
            "skin": "slFSbox",
            "top": "0dp",
            "verticalScrollIndicator": true,
            "width": "100%",
            "zIndex": 1
        }, controller.args[0], "flxRolesSegment"), extendConfig({}, controller.args[1], "flxRolesSegment"), extendConfig({}, controller.args[2], "flxRolesSegment"));
        flxRolesSegment.setDefaultUnit(kony.flex.DP);
        var segAssociatedRoles = new kony.ui.SegmentedUI2(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
            "bottom": 0,
            "data": [{
                "btnViewDetails": "kony.i18n.getLocalizedString(\"i18n.frmGroupsController.viewDetails\")",
                "imgRoleCheckbox": "checkboxnormal.png",
                "lblRoleDesc": "kony.i18n.getLocalizedString(\"i18n.frmAlertsManagement.LoremIpsumA\")",
                "lblRoleName": "Bill Payments"
            }],
            "groupCells": false,
            "id": "segAssociatedRoles",
            "isVisible": true,
            "left": "0dp",
            "needPageIndicator": true,
            "pageOffDotImage": "pageoffdot.png",
            "pageOnDotImage": "pageondot.png",
            "retainSelection": false,
            "rowFocusSkin": "seg2Focus",
            "rowSkin": "seg2Normal",
            "rowTemplate": "flxCustomerProfileRoles",
            "sectionHeaderSkin": "sliPhoneSegmentHeader",
            "selectionBehavior": constants.SEGUI_DEFAULT_BEHAVIOR,
            "separatorRequired": false,
            "showScrollbars": false,
            "top": 0,
            "viewType": constants.SEGUI_VIEW_TYPE_TABLEVIEW,
            "widgetDataMap": {
                "btnViewDetails": "btnViewDetails",
                "flxCustomerProfileRoles": "flxCustomerProfileRoles",
                "flxDefaultRoleButton": "flxDefaultRoleButton",
                "flxRoleCheckbox": "flxRoleCheckbox",
                "flxRoleInfo": "flxRoleInfo",
                "flxRoleName": "flxRoleName",
                "flxRoleNameContainer": "flxRoleNameContainer",
                "flxRoleRadio": "flxRoleRadio",
                "imgRoleCheckbox": "imgRoleCheckbox",
                "imgRoleRadio": "imgRoleRadio",
                "lblRoleDesc": "lblRoleDesc",
                "lblRoleName": "lblRoleName"
            },
            "width": "100%",
            "zIndex": 1
        }, controller.args[0], "segAssociatedRoles"), extendConfig({
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "segAssociatedRoles"), extendConfig({}, controller.args[2], "segAssociatedRoles"));
        flxRolesSegment.add(segAssociatedRoles);
        flxAssociatedRoles.add(flxRolesHeader, flxRolesEdit, flxRoleUndoHeader, flxRolesSegment);
        var flxCommonButtonsRolesSave = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "bottom": "0dp",
            "clipBounds": true,
            "height": "80dp",
            "id": "flxCommonButtonsRolesSave",
            "isVisible": false,
            "layoutType": kony.flex.FREE_FORM,
            "left": "0dp",
            "isModalContainer": false,
            "skin": "sknFlxFFFFFF100O",
            "width": "100%",
            "zIndex": 1
        }, controller.args[0], "flxCommonButtonsRolesSave"), extendConfig({}, controller.args[1], "flxCommonButtonsRolesSave"), extendConfig({}, controller.args[2], "flxCommonButtonsRolesSave"));
        flxCommonButtonsRolesSave.setDefaultUnit(kony.flex.DP);
        var flxCommonButtonsRolesSeparator = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "clipBounds": true,
            "height": "1dp",
            "id": "flxCommonButtonsRolesSeparator",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "left": "0dp",
            "isModalContainer": false,
            "right": "0dp",
            "skin": "sknflxe1e5edop100",
            "top": "0dp",
            "zIndex": 1
        }, controller.args[0], "flxCommonButtonsRolesSeparator"), extendConfig({}, controller.args[1], "flxCommonButtonsRolesSeparator"), extendConfig({}, controller.args[2], "flxCommonButtonsRolesSeparator"));
        flxCommonButtonsRolesSeparator.setDefaultUnit(kony.flex.DP);
        flxCommonButtonsRolesSeparator.add();
        var commonButtonsRoles = new com.adminConsole.common.commonButtons(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "centerY": "50%",
            "height": "80px",
            "id": "commonButtonsRoles",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "left": "0dp",
            "masterType": constants.MASTER_TYPE_DEFAULT,
            "isModalContainer": false,
            "skin": "slFbox",
            "width": "100%",
            "overrides": {
                "btnCancel": {
                    "left": "viz.val_cleared",
                    "right": "140px"
                },
                "btnNext": {
                    "isVisible": false,
                    "left": "viz.val_cleared",
                    "right": "20dp",
                    "width": "100dp"
                },
                "btnSave": {
                    "i18n_text": "kony.i18n.getLocalizedString(\"i18n.permission.SAVE\")",
                    "isVisible": true,
                    "right": "20dp"
                },
                "commonButtons": {
                    "bottom": "viz.val_cleared",
                    "centerY": "50%",
                    "isVisible": true,
                    "top": "viz.val_cleared"
                },
                "flxRightButtons": {
                    "width": "140px"
                }
            }
        }, controller.args[0], "commonButtonsRoles"), extendConfig({
            "overrides": {}
        }, controller.args[1], "commonButtonsRoles"), extendConfig({
            "overrides": {}
        }, controller.args[2], "commonButtonsRoles"));
        flxCommonButtonsRolesSave.add(flxCommonButtonsRolesSeparator, commonButtonsRoles);
        var flxCommonButtons = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "bottom": "0dp",
            "clipBounds": true,
            "height": "80dp",
            "id": "flxCommonButtons",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "left": "0%",
            "isModalContainer": false,
            "skin": "slFbox",
            "width": "100%",
            "zIndex": 1
        }, controller.args[0], "flxCommonButtons"), extendConfig({}, controller.args[1], "flxCommonButtons"), extendConfig({}, controller.args[2], "flxCommonButtons"));
        flxCommonButtons.setDefaultUnit(kony.flex.DP);
        var flxSeparator = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "clipBounds": true,
            "height": "1dp",
            "id": "flxSeparator",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "left": "0dp",
            "isModalContainer": false,
            "skin": "sknflxD5D9DD",
            "top": "5dp",
            "width": "100%",
            "zIndex": 1
        }, controller.args[0], "flxSeparator"), extendConfig({}, controller.args[1], "flxSeparator"), extendConfig({}, controller.args[2], "flxSeparator"));
        flxSeparator.setDefaultUnit(kony.flex.DP);
        flxSeparator.add();
        var btnCancel = new kony.ui.Button(extendConfig({
            "centerY": "50%",
            "focusSkin": "sknBtnLatoRegulara5abc413pxKA",
            "height": "40dp",
            "id": "btnCancel",
            "isVisible": true,
            "left": "20px",
            "right": "29.50%",
            "skin": "sknbtnf7f7faLatoRegular4f555dBorder1px8b96a5",
            "i18n_text": "kony.i18n.getLocalizedString(\"i18n.permission.CANCEL\")",
            "top": "0%",
            "width": "100px",
            "zIndex": 1
        }, controller.args[0], "btnCancel"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_CENTER,
            "displayText": true,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "btnCancel"), extendConfig({
            "hoverSkin": "sknbtnffffffLatoRegular4f555dBorder1px485c75"
        }, controller.args[2], "btnCancel"));
        var flxRightButtons = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "centerY": "50%",
            "clipBounds": true,
            "height": "100%",
            "id": "flxRightButtons",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "reverseLayoutDirection": false,
            "isModalContainer": false,
            "right": "0px",
            "skin": "slFbox",
            "width": "280px",
            "zIndex": 1
        }, controller.args[0], "flxRightButtons"), extendConfig({}, controller.args[1], "flxRightButtons"), extendConfig({}, controller.args[2], "flxRightButtons"));
        flxRightButtons.setDefaultUnit(kony.flex.DP);
        var btnUpdate = new kony.ui.Button(extendConfig({
            "centerY": "50%",
            "focusSkin": "sknBtn003E75LatoRegular13pxFFFFFFRad20px",
            "height": "40dp",
            "id": "btnUpdate",
            "isVisible": false,
            "right": "20px",
            "skin": "sknBtn003E75LatoRegular13pxFFFFFFRad20px",
            "i18n_text": "kony.i18n.getLocalizedString(\"i18n.ConfigurationBundles.buttonUpdate\")",
            "top": "0%",
            "width": "93dp",
            "zIndex": 1
        }, controller.args[0], "btnUpdate"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_CENTER,
            "displayText": true,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "btnUpdate"), extendConfig({
            "hoverSkin": "sknBtn005198LatoRegular13pxFFFFFFRad20px"
        }, controller.args[2], "btnUpdate"));
        var btnAdd = new kony.ui.Button(extendConfig({
            "centerY": "50%",
            "focusSkin": "sknBtn003E75LatoRegular13pxFFFFFFRad20px",
            "height": "40dp",
            "id": "btnAdd",
            "isVisible": true,
            "right": "20px",
            "skin": "sknBtn003E75LatoRegular13pxFFFFFFRad20px",
            "text": "ADD SERVICE DEFINITION",
            "top": "0%",
            "width": "219dp",
            "zIndex": 1
        }, controller.args[0], "btnAdd"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_CENTER,
            "displayText": true,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "btnAdd"), extendConfig({
            "hoverSkin": "sknBtn005198LatoRegular13pxFFFFFFRad20px"
        }, controller.args[2], "btnAdd"));
        flxRightButtons.add(btnUpdate, btnAdd);
        flxCommonButtons.add(flxSeparator, btnCancel, flxRightButtons);
        associatedRoles.add(flxAssociatedRoles, flxCommonButtonsRolesSave, flxCommonButtons);
        return associatedRoles;
    }
})