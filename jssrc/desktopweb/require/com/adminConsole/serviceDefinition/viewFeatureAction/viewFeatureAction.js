define(function() {
    return function(controller) {
        var viewFeatureAction = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
            "clipBounds": true,
            "isMaster": true,
            "id": "viewFeatureAction",
            "isVisible": true,
            "layoutType": kony.flex.FLOW_VERTICAL,
            "left": "0dp",
            "isModalContainer": false,
            "skin": "sknFlxbgFFFFFFbdrD7D9E0rd3px",
            "top": "0dp",
            "width": "100%"
        }, controller.args[0], "viewFeatureAction"), extendConfig({}, controller.args[1], "viewFeatureAction"), extendConfig({}, controller.args[2], "viewFeatureAction"));
        viewFeatureAction.setDefaultUnit(kony.flex.DP);
        var flxHeader = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "clipBounds": true,
            "height": "61dp",
            "id": "flxHeader",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "left": "0px",
            "isModalContainer": false,
            "right": "0px",
            "skin": "sknf5f6f8border0px",
            "top": "0px",
            "zIndex": 1
        }, controller.args[0], "flxHeader"), extendConfig({}, controller.args[1], "flxHeader"), extendConfig({}, controller.args[2], "flxHeader"));
        flxHeader.setDefaultUnit(kony.flex.DP);
        var flxTopRow1 = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
            "clipBounds": true,
            "id": "flxTopRow1",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "left": "0dp",
            "isModalContainer": false,
            "skin": "slFbox",
            "top": "12dp",
            "width": "100%"
        }, controller.args[0], "flxTopRow1"), extendConfig({}, controller.args[1], "flxTopRow1"), extendConfig({}, controller.args[2], "flxTopRow1"));
        flxTopRow1.setDefaultUnit(kony.flex.DP);
        var flxRight = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
            "clipBounds": true,
            "id": "flxRight",
            "isVisible": true,
            "layoutType": kony.flex.FLOW_HORIZONTAL,
            "reverseLayoutDirection": false,
            "isModalContainer": false,
            "right": 0,
            "skin": "slFbox",
            "top": "0dp",
            "width": "10%"
        }, controller.args[0], "flxRight"), extendConfig({}, controller.args[1], "flxRight"), extendConfig({}, controller.args[2], "flxRight"));
        flxRight.setDefaultUnit(kony.flex.DP);
        var statusIcon = new kony.ui.Label(extendConfig({
            "id": "statusIcon",
            "isVisible": true,
            "left": "0dp",
            "skin": "sknFontIconActivate",
            "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmAlertsManagement.lblViewAlertStatusKey\")",
            "top": 2,
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, controller.args[0], "statusIcon"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "statusIcon"), extendConfig({}, controller.args[2], "statusIcon"));
        var statusValue = new kony.ui.Label(extendConfig({
            "id": "statusValue",
            "isVisible": true,
            "left": "5dp",
            "skin": "sknlblLato5bc06cBold14px",
            "i18n_text": "kony.i18n.getLocalizedString(\"i18n.secureimage.Active\")",
            "top": "0px",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, controller.args[0], "statusValue"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "statusValue"), extendConfig({}, controller.args[2], "statusValue"));
        flxRight.add(statusIcon, statusValue);
        var flxLeft = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
            "clipBounds": true,
            "id": "flxLeft",
            "isVisible": true,
            "layoutType": kony.flex.FLOW_HORIZONTAL,
            "left": "0dp",
            "isModalContainer": false,
            "skin": "slFbox",
            "top": "0dp",
            "width": "90%"
        }, controller.args[0], "flxLeft"), extendConfig({}, controller.args[1], "flxLeft"), extendConfig({}, controller.args[2], "flxLeft"));
        flxLeft.setDefaultUnit(kony.flex.DP);
        var flxToggle = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "clipBounds": true,
            "height": "13dp",
            "id": "flxToggle",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "left": "10dp",
            "isModalContainer": false,
            "right": "0px",
            "skin": "slFbox",
            "top": "3dp",
            "width": "13dp",
            "zIndex": 1
        }, controller.args[0], "flxToggle"), extendConfig({}, controller.args[1], "flxToggle"), extendConfig({
            "hoverSkin": "sknCursor"
        }, controller.args[2], "flxToggle"));
        flxToggle.setDefaultUnit(kony.flex.DP);
        var lblToggle = new kony.ui.Label(extendConfig({
            "centerX": "50%",
            "centerY": "50%",
            "height": "100%",
            "id": "lblToggle",
            "isVisible": true,
            "skin": "sknIcon12pxBlack",
            "i18n_text": "kony.i18n.getLocalizedString(\"i18n.userwidgetmodel.fontIconBreadcrumbsDown\")",
            "width": "100%",
            "zIndex": 1
        }, controller.args[0], "lblToggle"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblToggle"), extendConfig({}, controller.args[2], "lblToggle"));
        flxToggle.add(lblToggle);
        var lblFeatureName = new kony.ui.Label(extendConfig({
            "id": "lblFeatureName",
            "isVisible": true,
            "left": "7dp",
            "skin": "sknLbl192B45LatoRegular14px",
            "i18n_text": "kony.i18n.getLocalizedString(\"i18n.permission.DESCRIPTION\")",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, controller.args[0], "lblFeatureName"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblFeatureName"), extendConfig({}, controller.args[2], "lblFeatureName"));
        flxLeft.add(flxToggle, lblFeatureName);
        flxTopRow1.add(flxRight, flxLeft);
        var flxTopRow2 = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
            "clipBounds": true,
            "id": "flxTopRow2",
            "isVisible": true,
            "layoutType": kony.flex.FLOW_HORIZONTAL,
            "left": "0dp",
            "isModalContainer": false,
            "skin": "slFbox",
            "top": "35dp",
            "width": "100%"
        }, controller.args[0], "flxTopRow2"), extendConfig({}, controller.args[1], "flxTopRow2"), extendConfig({}, controller.args[2], "flxTopRow2"));
        flxTopRow2.setDefaultUnit(kony.flex.DP);
        var lblAvailableActions = new kony.ui.Label(extendConfig({
            "id": "lblAvailableActions",
            "isVisible": true,
            "left": "30dp",
            "skin": "sknLbl485C75LatoRegular13Px",
            "text": "Available Actions:",
            "top": "0dp",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, controller.args[0], "lblAvailableActions"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblAvailableActions"), extendConfig({}, controller.args[2], "lblAvailableActions"));
        var lblCountActions = new kony.ui.Label(extendConfig({
            "id": "lblCountActions",
            "isVisible": true,
            "left": "10dp",
            "skin": "sknLbl192B45LatoSemiBold13px",
            "text": "2",
            "top": "0dp",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, controller.args[0], "lblCountActions"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblCountActions"), extendConfig({}, controller.args[2], "lblCountActions"));
        var lblTotalActions = new kony.ui.Label(extendConfig({
            "id": "lblTotalActions",
            "isVisible": true,
            "left": "3dp",
            "skin": "sknLbl485C75LatoRegular13Px",
            "text": "of 2",
            "top": "0dp",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, controller.args[0], "lblTotalActions"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblTotalActions"), extendConfig({}, controller.args[2], "lblTotalActions"));
        flxTopRow2.add(lblAvailableActions, lblCountActions, lblTotalActions);
        flxHeader.add(flxTopRow1, flxTopRow2);
        var flxHideDisplayContent = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
            "bottom": "5dp",
            "clipBounds": true,
            "id": "flxHideDisplayContent",
            "isVisible": true,
            "layoutType": kony.flex.FLOW_VERTICAL,
            "left": "0dp",
            "isModalContainer": false,
            "skin": "slFbox",
            "top": "0dp",
            "width": "100%"
        }, controller.args[0], "flxHideDisplayContent"), extendConfig({}, controller.args[1], "flxHideDisplayContent"), extendConfig({}, controller.args[2], "flxHideDisplayContent"));
        flxHideDisplayContent.setDefaultUnit(kony.flex.DP);
        var flxFeatureActionsHeader = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "clipBounds": true,
            "height": "40px",
            "id": "flxFeatureActionsHeader",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "left": "0dp",
            "isModalContainer": false,
            "skin": "sknflxffffffop100",
            "top": "0",
            "width": "100%"
        }, controller.args[0], "flxFeatureActionsHeader"), extendConfig({}, controller.args[1], "flxFeatureActionsHeader"), extendConfig({
            "hoverSkin": "sknfbfcfc"
        }, controller.args[2], "flxFeatureActionsHeader"));
        flxFeatureActionsHeader.setDefaultUnit(kony.flex.DP);
        var lblActionName = new kony.ui.Label(extendConfig({
            "centerY": "50%",
            "id": "lblActionName",
            "isVisible": true,
            "left": "30px",
            "skin": "sknlblLato696c7312px",
            "text": "ACTION",
            "width": "28%",
            "zIndex": 1
        }, controller.args[0], "lblActionName"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblActionName"), extendConfig({}, controller.args[2], "lblActionName"));
        var lblDescription = new kony.ui.Label(extendConfig({
            "centerY": "50%",
            "id": "lblDescription",
            "isVisible": true,
            "left": "32%",
            "skin": "sknlblLato696c7312px",
            "i18n_text": "kony.i18n.getLocalizedString(\"i18n.ConfigurationBundles.descriptionInCaps\")",
            "width": "58%",
            "zIndex": 1
        }, controller.args[0], "lblDescription"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblDescription"), extendConfig({}, controller.args[2], "lblDescription"));
        var lblView = new kony.ui.Label(extendConfig({
            "centerY": "50%",
            "id": "lblView",
            "isVisible": true,
            "left": "90%",
            "right": 0,
            "skin": "sknlblLato696c7312px",
            "i18n_text": "kony.i18n.getLocalizedString(\"i18n.common.statusCaps\")",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, controller.args[0], "lblView"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblView"), extendConfig({}, controller.args[2], "lblView"));
        flxFeatureActionsHeader.add(lblActionName, lblDescription, lblView);
        var lblSeperator = new kony.ui.Label(extendConfig({
            "height": "1px",
            "id": "lblSeperator",
            "isVisible": true,
            "left": "0dp",
            "skin": "sknlblSeperator",
            "text": ".",
            "top": "0dp",
            "width": "100%",
            "zIndex": 1
        }, controller.args[0], "lblSeperator"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblSeperator"), extendConfig({}, controller.args[2], "lblSeperator"));
        var flxSegActions = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
            "clipBounds": true,
            "id": "flxSegActions",
            "isVisible": true,
            "layoutType": kony.flex.FLOW_VERTICAL,
            "left": "0dp",
            "isModalContainer": false,
            "skin": "slFbox",
            "top": "0dp",
            "width": "100%"
        }, controller.args[0], "flxSegActions"), extendConfig({}, controller.args[1], "flxSegActions"), extendConfig({}, controller.args[2], "flxSegActions"));
        flxSegActions.setDefaultUnit(kony.flex.DP);
        var SegActions = new kony.ui.SegmentedUI2(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
            "data": [{
                "lblActionName": "Admin Role",
                "lblDescription": "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do",
                "lblSeperator": ".",
                "statusIcon": "kony.i18n.getLocalizedString(\"i18n.frmAlertsManagement.lblViewAlertStatusKey\")",
                "statusValue": "kony.i18n.getLocalizedString(\"i18n.secureimage.Active\")"
            }, {
                "lblActionName": "Admin Role",
                "lblDescription": "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do",
                "lblSeperator": ".",
                "statusIcon": "kony.i18n.getLocalizedString(\"i18n.frmAlertsManagement.lblViewAlertStatusKey\")",
                "statusValue": "kony.i18n.getLocalizedString(\"i18n.secureimage.Active\")"
            }, {
                "lblActionName": "Admin Role",
                "lblDescription": "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do",
                "lblSeperator": ".",
                "statusIcon": "kony.i18n.getLocalizedString(\"i18n.frmAlertsManagement.lblViewAlertStatusKey\")",
                "statusValue": "kony.i18n.getLocalizedString(\"i18n.secureimage.Active\")"
            }],
            "groupCells": false,
            "id": "SegActions",
            "isVisible": true,
            "left": "0dp",
            "needPageIndicator": true,
            "pageOffDotImage": "pageoffdot.png",
            "pageOnDotImage": "pageondot.png",
            "retainSelection": false,
            "rowFocusSkin": "seg2Focus",
            "rowSkin": "seg2Normal",
            "rowTemplate": "flxActionStatus",
            "sectionHeaderSkin": "sliPhoneSegmentHeader",
            "selectionBehavior": constants.SEGUI_SINGLE_SELECT_BEHAVIOR,
            "selectionBehaviorConfig": {
                "imageIdentifier": "imgCheckBoxMsg",
                "selectedStateImage": "checkboxselected.png",
                "unselectedStateImage": "checkboxnormal.png"
            },
            "separatorRequired": false,
            "showScrollbars": false,
            "top": "0dp",
            "viewType": constants.SEGUI_VIEW_TYPE_TABLEVIEW,
            "widgetDataMap": {
                "flxActionStatus": "flxActionStatus",
                "flxStatus": "flxStatus",
                "lblActionName": "lblActionName",
                "lblDescription": "lblDescription",
                "lblSeperator": "lblSeperator",
                "statusIcon": "statusIcon",
                "statusValue": "statusValue"
            },
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 10
        }, controller.args[0], "SegActions"), extendConfig({
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "SegActions"), extendConfig({}, controller.args[2], "SegActions"));
        flxSegActions.add(SegActions);
        flxHideDisplayContent.add(flxFeatureActionsHeader, lblSeperator, flxSegActions);
        viewFeatureAction.add(flxHeader, flxHideDisplayContent);
        return viewFeatureAction;
    }
})