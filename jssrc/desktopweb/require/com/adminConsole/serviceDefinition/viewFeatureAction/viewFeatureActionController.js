define("com/adminConsole/serviceDefinition/viewFeatureAction/userviewFeatureActionController", function() {
    return {
        setFlowActions: function() {
            var scopeObj = this;
            this.view.flxToggle.onClick = function() {
                scopeObj.toggleContent();
            };
            this.view.lblFeatureName.onClick = function() {
                scopeObj.toggleContent();
            };
        },
        toggleContent: function() {
            if (this.view.flxHideDisplayContent.isVisible) {
                this.view.lblToggle.text = "\ue922";
                this.view.flxHideDisplayContent.setVisibility(false);
            } else {
                this.view.lblToggle.text = "\ue915";
                this.view.flxHideDisplayContent.setVisibility(true);
            }
        }
    };
});
define("com/adminConsole/serviceDefinition/viewFeatureAction/viewFeatureActionControllerActions", {
    /*
          This is an auto generated file and any modifications to it may result in corruption of the action sequence.
        */
});
define("com/adminConsole/serviceDefinition/viewFeatureAction/viewFeatureActionController", ["com/adminConsole/serviceDefinition/viewFeatureAction/userviewFeatureActionController", "com/adminConsole/serviceDefinition/viewFeatureAction/viewFeatureActionControllerActions"], function() {
    var controller = require("com/adminConsole/serviceDefinition/viewFeatureAction/userviewFeatureActionController");
    var actions = require("com/adminConsole/serviceDefinition/viewFeatureAction/viewFeatureActionControllerActions");
    for (var key in actions) {
        controller[key] = actions[key];
    }
    return controller;
});
