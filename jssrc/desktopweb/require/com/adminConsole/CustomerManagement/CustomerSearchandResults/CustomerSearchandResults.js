define(function() {
    return function(controller) {
        var CustomerSearchandResults = new kony.ui.FlexScrollContainer(extendConfig({
            "allowHorizontalBounce": false,
            "allowVerticalBounce": true,
            "bounces": true,
            "clipBounds": true,
            "enableScrolling": false,
            "isMaster": true,
            "height": "800px",
            "horizontalScrollIndicator": true,
            "id": "CustomerSearchandResults",
            "isVisible": true,
            "layoutType": kony.flex.FLOW_VERTICAL,
            "left": "0dp",
            "pagingEnabled": false,
            "scrollDirection": kony.flex.SCROLL_NONE,
            "skin": "sknflxCustomerSearchComponent",
            "top": "0px",
            "verticalScrollIndicator": true,
            "width": "100%",
            "zIndex": 1
        }, controller.args[0], "CustomerSearchandResults"), extendConfig({}, controller.args[1], "CustomerSearchandResults"), extendConfig({}, controller.args[2], "CustomerSearchandResults"));
        CustomerSearchandResults.setDefaultUnit(kony.flex.DP);
        var flxInner = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
            "clipBounds": true,
            "id": "flxInner",
            "isVisible": true,
            "layoutType": kony.flex.FLOW_VERTICAL,
            "left": "3%",
            "isModalContainer": false,
            "right": "0px",
            "skin": "sknflxCustomerSearchInner",
            "top": "15px",
            "width": "94%"
        }, controller.args[0], "flxInner"), extendConfig({}, controller.args[1], "flxInner"), extendConfig({}, controller.args[2], "flxInner"));
        flxInner.setDefaultUnit(kony.flex.DP);
        var flxSearchContainer = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "clipBounds": true,
            "height": "445px",
            "id": "flxSearchContainer",
            "isVisible": true,
            "layoutType": kony.flex.FLOW_VERTICAL,
            "left": "0dp",
            "isModalContainer": false,
            "skin": "slFbox",
            "top": "0dp",
            "width": "100%"
        }, controller.args[0], "flxSearchContainer"), extendConfig({}, controller.args[1], "flxSearchContainer"), extendConfig({}, controller.args[2], "flxSearchContainer"));
        flxSearchContainer.setDefaultUnit(kony.flex.DP);
        var flxSearchHeader = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "clipBounds": true,
            "height": "15px",
            "id": "flxSearchHeader",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "left": "0dp",
            "isModalContainer": false,
            "skin": "slFbox",
            "top": "35px",
            "width": "100%"
        }, controller.args[0], "flxSearchHeader"), extendConfig({}, controller.args[1], "flxSearchHeader"), extendConfig({}, controller.args[2], "flxSearchHeader"));
        flxSearchHeader.setDefaultUnit(kony.flex.DP);
        var lblDefaultSearchHeader = new kony.ui.Label(extendConfig({
            "centerY": "50%",
            "id": "lblDefaultSearchHeader",
            "isVisible": true,
            "left": "20dp",
            "skin": "sknlblLatobold11px485C75",
            "i18n_text": "kony.i18n.getLocalizedString(\"i18n.userwidgetmodel.lblDefaultSearchHeader\")",
            "top": "0px",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, controller.args[0], "lblDefaultSearchHeader"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblDefaultSearchHeader"), extendConfig({}, controller.args[2], "lblDefaultSearchHeader"));
        var lblSearchError = new kony.ui.Label(extendConfig({
            "centerY": "50%",
            "id": "lblSearchError",
            "isVisible": true,
            "left": "185px",
            "skin": "sknlblError",
            "i18n_text": "kony.i18n.getLocalizedString(\"i18n.userwidgetmodel.lblSearchError\")",
            "top": "0px",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, controller.args[0], "lblSearchError"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblSearchError"), extendConfig({}, controller.args[2], "lblSearchError"));
        var imgSearchError = new kony.ui.Image2(extendConfig({
            "centerY": "51%",
            "height": "12dp",
            "id": "imgSearchError",
            "isVisible": true,
            "left": "169dp",
            "skin": "slImage",
            "src": "error_2x.png",
            "top": "2dp",
            "width": "12px",
            "zIndex": 1
        }, controller.args[0], "imgSearchError"), extendConfig({
            "imageScaleMode": constants.IMAGE_SCALE_MODE_MAINTAIN_ASPECT_RATIO,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "imgSearchError"), extendConfig({}, controller.args[2], "imgSearchError"));
        flxSearchHeader.add(lblDefaultSearchHeader, lblSearchError, imgSearchError);
        var flxSearch = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "clipBounds": true,
            "height": "385px",
            "id": "flxSearch",
            "isVisible": true,
            "layoutType": kony.flex.FLOW_VERTICAL,
            "left": "0dp",
            "isModalContainer": false,
            "right": "0dp",
            "skin": "slFbox",
            "top": "2%",
            "zIndex": 1
        }, controller.args[0], "flxSearch"), extendConfig({}, controller.args[1], "flxSearch"), extendConfig({}, controller.args[2], "flxSearch"));
        flxSearch.setDefaultUnit(kony.flex.DP);
        var flxFirstRow = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "clipBounds": true,
            "height": "68dp",
            "id": "flxFirstRow",
            "isVisible": true,
            "layoutType": kony.flex.FLOW_HORIZONTAL,
            "left": "0px",
            "isModalContainer": false,
            "right": "0px",
            "skin": "slFbox",
            "top": "25px",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, controller.args[0], "flxFirstRow"), extendConfig({}, controller.args[1], "flxFirstRow"), extendConfig({}, controller.args[2], "flxFirstRow"));
        flxFirstRow.setDefaultUnit(kony.flex.DP);
        var flxSearchCriteria1 = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "clipBounds": true,
            "height": "68dp",
            "id": "flxSearchCriteria1",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "left": "20dp",
            "isModalContainer": false,
            "skin": "slFbox",
            "top": "0px",
            "width": "30%",
            "zIndex": 1
        }, controller.args[0], "flxSearchCriteria1"), extendConfig({}, controller.args[1], "flxSearchCriteria1"), extendConfig({}, controller.args[2], "flxSearchCriteria1"));
        flxSearchCriteria1.setDefaultUnit(kony.flex.DP);
        var lblSearchParam1 = new kony.ui.Label(extendConfig({
            "id": "lblSearchParam1",
            "isVisible": true,
            "left": "0px",
            "skin": "sknLbl485C75LatoRegular13Px",
            "i18n_text": "kony.i18n.getLocalizedString(\"i18n.userwidgetmodel.lblSearchParam4\")",
            "top": "0px",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, controller.args[0], "lblSearchParam1"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblSearchParam1"), extendConfig({}, controller.args[2], "lblSearchParam1"));
        var txtSearchParam1 = new kony.ui.TextBox2(extendConfig({
            "autoCapitalize": constants.TEXTBOX_AUTO_CAPITALIZE_NONE,
            "focusSkin": "skntxtbxDetailsFocus14px",
            "height": "40dp",
            "id": "txtSearchParam1",
            "isVisible": true,
            "keyBoardStyle": constants.TEXTBOX_KEY_BOARD_STYLE_DEFAULT,
            "left": "0px",
            "i18n_placeholder": "kony.i18n.getLocalizedString(\"i18n.userwidgetmodel.lblSearchParam4\")",
            "right": "0px",
            "secureTextEntry": false,
            "skin": "skntxtbxDetails0bbf1235271384a",
            "textInputMode": constants.TEXTBOX_INPUT_MODE_ANY,
            "top": "25px",
            "zIndex": 1
        }, controller.args[0], "txtSearchParam1"), extendConfig({
            "containerHeightMode": constants.TEXTBOX_FONT_METRICS_DRIVEN_HEIGHT,
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [3, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "txtSearchParam1"), extendConfig({
            "autoCorrect": false
        }, controller.args[2], "txtSearchParam1"));
        flxSearchCriteria1.add(lblSearchParam1, txtSearchParam1);
        var flxSearchCriteria4 = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "clipBounds": true,
            "height": "68dp",
            "id": "flxSearchCriteria4",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "left": "2%",
            "isModalContainer": false,
            "skin": "slFbox",
            "top": "0px",
            "width": "30%",
            "zIndex": 1
        }, controller.args[0], "flxSearchCriteria4"), extendConfig({}, controller.args[1], "flxSearchCriteria4"), extendConfig({}, controller.args[2], "flxSearchCriteria4"));
        flxSearchCriteria4.setDefaultUnit(kony.flex.DP);
        var lblSearchParam4 = new kony.ui.Label(extendConfig({
            "id": "lblSearchParam4",
            "isVisible": true,
            "left": "0px",
            "skin": "sknLbl485C75LatoRegular13Px",
            "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmCustomerManagementController.EmailID\")",
            "top": "0px",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, controller.args[0], "lblSearchParam4"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblSearchParam4"), extendConfig({}, controller.args[2], "lblSearchParam4"));
        var txtSearchParam4 = new kony.ui.TextBox2(extendConfig({
            "autoCapitalize": constants.TEXTBOX_AUTO_CAPITALIZE_NONE,
            "focusSkin": "skntxtbxDetailsFocus14px",
            "height": "40dp",
            "id": "txtSearchParam4",
            "isVisible": true,
            "keyBoardStyle": constants.TEXTBOX_KEY_BOARD_STYLE_DEFAULT,
            "left": "0px",
            "i18n_placeholder": "kony.i18n.getLocalizedString(\"i18n.frmCustomerManagementController.EmailID\")",
            "right": "0px",
            "secureTextEntry": false,
            "skin": "skntxtbxDetails0bbf1235271384a",
            "textInputMode": constants.TEXTBOX_INPUT_MODE_ANY,
            "top": "25px",
            "zIndex": 1
        }, controller.args[0], "txtSearchParam4"), extendConfig({
            "containerHeightMode": constants.TEXTBOX_FONT_METRICS_DRIVEN_HEIGHT,
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [3, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "txtSearchParam4"), extendConfig({
            "autoCorrect": false
        }, controller.args[2], "txtSearchParam4"));
        flxSearchCriteria4.add(lblSearchParam4, txtSearchParam4);
        var flxSearchCriteria6 = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "clipBounds": true,
            "height": "68dp",
            "id": "flxSearchCriteria6",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "left": "4%",
            "isModalContainer": false,
            "skin": "slFbox",
            "top": "0px",
            "width": "30%",
            "zIndex": 1
        }, controller.args[0], "flxSearchCriteria6"), extendConfig({}, controller.args[1], "flxSearchCriteria6"), extendConfig({}, controller.args[2], "flxSearchCriteria6"));
        flxSearchCriteria6.setDefaultUnit(kony.flex.DP);
        var lblSearchParam6 = new kony.ui.Label(extendConfig({
            "id": "lblSearchParam6",
            "isVisible": true,
            "left": "0px",
            "skin": "sknLbl485C75LatoRegular13Px",
            "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmCustomerManagementController.User_ID\")",
            "top": "0px",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, controller.args[0], "lblSearchParam6"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblSearchParam6"), extendConfig({}, controller.args[2], "lblSearchParam6"));
        var txtSearchParam6 = new kony.ui.TextBox2(extendConfig({
            "autoCapitalize": constants.TEXTBOX_AUTO_CAPITALIZE_NONE,
            "focusSkin": "skntxtbxDetailsFocus14px",
            "height": "40dp",
            "id": "txtSearchParam6",
            "isVisible": true,
            "keyBoardStyle": constants.TEXTBOX_KEY_BOARD_STYLE_DEFAULT,
            "left": "0px",
            "i18n_placeholder": "kony.i18n.getLocalizedString(\"i18n.frmCustomerManagementController.User_ID\")",
            "right": "0px",
            "secureTextEntry": false,
            "skin": "skntxtbxDetails0bbf1235271384a",
            "textInputMode": constants.TEXTBOX_INPUT_MODE_ANY,
            "top": "25px",
            "zIndex": 1
        }, controller.args[0], "txtSearchParam6"), extendConfig({
            "containerHeightMode": constants.TEXTBOX_FONT_METRICS_DRIVEN_HEIGHT,
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [3, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "txtSearchParam6"), extendConfig({
            "autoCorrect": false
        }, controller.args[2], "txtSearchParam6"));
        flxSearchCriteria6.add(lblSearchParam6, txtSearchParam6);
        flxFirstRow.add(flxSearchCriteria1, flxSearchCriteria4, flxSearchCriteria6);
        var flxSecondRow = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "clipBounds": true,
            "height": "90dp",
            "id": "flxSecondRow",
            "isVisible": true,
            "layoutType": kony.flex.FLOW_HORIZONTAL,
            "left": "0px",
            "isModalContainer": false,
            "right": "0px",
            "skin": "slFbox",
            "top": "20px",
            "zIndex": 1
        }, controller.args[0], "flxSecondRow"), extendConfig({}, controller.args[1], "flxSecondRow"), extendConfig({}, controller.args[2], "flxSecondRow"));
        flxSecondRow.setDefaultUnit(kony.flex.DP);
        var flxSearchCriteria2 = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "clipBounds": true,
            "height": "68dp",
            "id": "flxSearchCriteria2",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "left": "20dp",
            "isModalContainer": false,
            "skin": "slFbox",
            "top": "0px",
            "width": "30%",
            "zIndex": 1
        }, controller.args[0], "flxSearchCriteria2"), extendConfig({}, controller.args[1], "flxSearchCriteria2"), extendConfig({}, controller.args[2], "flxSearchCriteria2"));
        flxSearchCriteria2.setDefaultUnit(kony.flex.DP);
        var lblSearchParam2 = new kony.ui.Label(extendConfig({
            "id": "lblSearchParam2",
            "isVisible": true,
            "left": "0px",
            "skin": "sknLbl485C75LatoRegular13Px",
            "i18n_text": "kony.i18n.getLocalizedString(\"i18n.customerSearch.MemberID\")",
            "top": "0px",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, controller.args[0], "lblSearchParam2"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblSearchParam2"), extendConfig({}, controller.args[2], "lblSearchParam2"));
        var txtSearchParam2 = new kony.ui.TextBox2(extendConfig({
            "autoCapitalize": constants.TEXTBOX_AUTO_CAPITALIZE_NONE,
            "focusSkin": "skntxtbxDetailsFocus14px",
            "height": "40dp",
            "id": "txtSearchParam2",
            "isVisible": true,
            "keyBoardStyle": constants.TEXTBOX_KEY_BOARD_STYLE_DEFAULT,
            "left": "0px",
            "i18n_placeholder": "kony.i18n.getLocalizedString(\"i18n.customerSearch.MemberID\")",
            "right": "0px",
            "secureTextEntry": false,
            "skin": "skntxtbxDetails0bbf1235271384a",
            "textInputMode": constants.TEXTBOX_INPUT_MODE_ANY,
            "top": "25px",
            "zIndex": 1
        }, controller.args[0], "txtSearchParam2"), extendConfig({
            "containerHeightMode": constants.TEXTBOX_FONT_METRICS_DRIVEN_HEIGHT,
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [3, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "txtSearchParam2"), extendConfig({
            "autoCorrect": false
        }, controller.args[2], "txtSearchParam2"));
        flxSearchCriteria2.add(lblSearchParam2, txtSearchParam2);
        var flxSearchCriteria3 = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "clipBounds": true,
            "height": "90dp",
            "id": "flxSearchCriteria3",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "left": "20dp",
            "isModalContainer": false,
            "right": "0px",
            "skin": "slFbox",
            "top": "0px",
            "width": "30%",
            "zIndex": 1
        }, controller.args[0], "flxSearchCriteria3"), extendConfig({}, controller.args[1], "flxSearchCriteria3"), extendConfig({}, controller.args[2], "flxSearchCriteria3"));
        flxSearchCriteria3.setDefaultUnit(kony.flex.DP);
        var lblSearchParam3 = new kony.ui.Label(extendConfig({
            "id": "lblSearchParam3",
            "isVisible": true,
            "left": "0px",
            "skin": "sknLbl485C75LatoRegular13Px",
            "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmCustomerManagementController.Customer_Name\")",
            "top": "0px",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, controller.args[0], "lblSearchParam3"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblSearchParam3"), extendConfig({}, controller.args[2], "lblSearchParam3"));
        var txtSearchParam3 = new kony.ui.TextBox2(extendConfig({
            "autoCapitalize": constants.TEXTBOX_AUTO_CAPITALIZE_NONE,
            "focusSkin": "skntxtbxDetailsFocus14px",
            "height": "40dp",
            "id": "txtSearchParam3",
            "isVisible": true,
            "keyBoardStyle": constants.TEXTBOX_KEY_BOARD_STYLE_DEFAULT,
            "left": "0px",
            "i18n_placeholder": "kony.i18n.getLocalizedString(\"i18n.frmCustomerManagementController.Customer_Name\")",
            "right": "0px",
            "secureTextEntry": false,
            "skin": "skntxtbxDetails0bbf1235271384a",
            "textInputMode": constants.TEXTBOX_INPUT_MODE_ANY,
            "top": "25px",
            "zIndex": 1
        }, controller.args[0], "txtSearchParam3"), extendConfig({
            "containerHeightMode": constants.TEXTBOX_FONT_METRICS_DRIVEN_HEIGHT,
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [3, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "txtSearchParam3"), extendConfig({
            "autoCorrect": false
        }, controller.args[2], "txtSearchParam3"));
        var lblCompanyBusinessTag = new kony.ui.Label(extendConfig({
            "bottom": "0dp",
            "id": "lblCompanyBusinessTag",
            "isVisible": false,
            "left": "0px",
            "skin": "sknlblLatoRegular12px485c75",
            "text": "# Only for Business Customer",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, controller.args[0], "lblCompanyBusinessTag"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblCompanyBusinessTag"), extendConfig({}, controller.args[2], "lblCompanyBusinessTag"));
        flxSearchCriteria3.add(lblSearchParam3, txtSearchParam3, lblCompanyBusinessTag);
        var flxSearchCriteria7 = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "clipBounds": true,
            "height": "68dp",
            "id": "flxSearchCriteria7",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "left": "4%",
            "isModalContainer": false,
            "skin": "slFbox",
            "top": "0px",
            "width": "30%",
            "zIndex": 1
        }, controller.args[0], "flxSearchCriteria7"), extendConfig({}, controller.args[1], "flxSearchCriteria7"), extendConfig({}, controller.args[2], "flxSearchCriteria7"));
        flxSearchCriteria7.setDefaultUnit(kony.flex.DP);
        var lblSearchParam7 = new kony.ui.Label(extendConfig({
            "id": "lblSearchParam7",
            "isVisible": true,
            "left": "0px",
            "skin": "sknLbl485C75LatoRegular13Px",
            "text": "Card/Account Number",
            "top": "0px",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, controller.args[0], "lblSearchParam7"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblSearchParam7"), extendConfig({}, controller.args[2], "lblSearchParam7"));
        var txtSearchParam7 = new kony.ui.TextBox2(extendConfig({
            "autoCapitalize": constants.TEXTBOX_AUTO_CAPITALIZE_NONE,
            "focusSkin": "skntxtbxDetailsFocus14px",
            "height": "40dp",
            "id": "txtSearchParam7",
            "isVisible": true,
            "keyBoardStyle": constants.TEXTBOX_KEY_BOARD_STYLE_DEFAULT,
            "left": "0px",
            "placeholder": "Card/Account Number",
            "right": "0px",
            "secureTextEntry": false,
            "skin": "skntxtbxDetails0bbf1235271384a",
            "textInputMode": constants.TEXTBOX_INPUT_MODE_ANY,
            "top": "25px",
            "zIndex": 1
        }, controller.args[0], "txtSearchParam7"), extendConfig({
            "containerHeightMode": constants.TEXTBOX_FONT_METRICS_DRIVEN_HEIGHT,
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [3, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "txtSearchParam7"), extendConfig({
            "autoCorrect": false
        }, controller.args[2], "txtSearchParam7"));
        flxSearchCriteria7.add(lblSearchParam7, txtSearchParam7);
        flxSecondRow.add(flxSearchCriteria2, flxSearchCriteria3, flxSearchCriteria7);
        var flxThirdRow = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "clipBounds": true,
            "height": "90dp",
            "id": "flxThirdRow",
            "isVisible": true,
            "layoutType": kony.flex.FLOW_HORIZONTAL,
            "left": "0px",
            "isModalContainer": false,
            "right": "0px",
            "skin": "slFbox",
            "top": "0px",
            "zIndex": 1
        }, controller.args[0], "flxThirdRow"), extendConfig({}, controller.args[1], "flxThirdRow"), extendConfig({}, controller.args[2], "flxThirdRow"));
        flxThirdRow.setDefaultUnit(kony.flex.DP);
        var flxSearchCriteria8 = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "clipBounds": true,
            "height": "90dp",
            "id": "flxSearchCriteria8",
            "isVisible": false,
            "layoutType": kony.flex.FREE_FORM,
            "left": "2%",
            "isModalContainer": false,
            "skin": "slFbox",
            "top": "0px",
            "width": "30%",
            "zIndex": 1
        }, controller.args[0], "flxSearchCriteria8"), extendConfig({}, controller.args[1], "flxSearchCriteria8"), extendConfig({}, controller.args[2], "flxSearchCriteria8"));
        flxSearchCriteria8.setDefaultUnit(kony.flex.DP);
        var lblSearchParam8 = new kony.ui.Label(extendConfig({
            "id": "lblSearchParam8",
            "isVisible": true,
            "left": "0px",
            "skin": "sknlblLatoRegular12px485c75",
            "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmCustomerManagementController.Application_ID\")",
            "top": "0px",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, controller.args[0], "lblSearchParam8"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblSearchParam8"), extendConfig({}, controller.args[2], "lblSearchParam8"));
        var txtSearchParam8 = new kony.ui.TextBox2(extendConfig({
            "autoCapitalize": constants.TEXTBOX_AUTO_CAPITALIZE_NONE,
            "focusSkin": "skntxtbxDetailsFocus14px",
            "height": "40dp",
            "id": "txtSearchParam8",
            "isVisible": true,
            "keyBoardStyle": constants.TEXTBOX_KEY_BOARD_STYLE_DEFAULT,
            "left": "0px",
            "i18n_placeholder": "kony.i18n.getLocalizedString(\"i18n.frmCustomerManagementController.Application_ID\")",
            "right": "0px",
            "secureTextEntry": false,
            "skin": "skntxtbxDetails0bbf1235271384a",
            "textInputMode": constants.TEXTBOX_INPUT_MODE_ANY,
            "top": "25px",
            "zIndex": 1
        }, controller.args[0], "txtSearchParam8"), extendConfig({
            "containerHeightMode": constants.TEXTBOX_FONT_METRICS_DRIVEN_HEIGHT,
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [3, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "txtSearchParam8"), extendConfig({
            "autoCorrect": false
        }, controller.args[2], "txtSearchParam8"));
        flxSearchCriteria8.add(lblSearchParam8, txtSearchParam8);
        var flxSearchCriteria5 = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "clipBounds": true,
            "height": "90dp",
            "id": "flxSearchCriteria5",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "left": "2%",
            "isModalContainer": false,
            "skin": "slFbox",
            "top": "0px",
            "width": "30%",
            "zIndex": 1
        }, controller.args[0], "flxSearchCriteria5"), extendConfig({}, controller.args[1], "flxSearchCriteria5"), extendConfig({}, controller.args[2], "flxSearchCriteria5"));
        flxSearchCriteria5.setDefaultUnit(kony.flex.DP);
        var lblSearchParam5 = new kony.ui.Label(extendConfig({
            "id": "lblSearchParam5",
            "isVisible": true,
            "left": "0px",
            "skin": "sknLbl485C75LatoRegular13Px",
            "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmCustomerManagementController.Identification\")",
            "top": "0px",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, controller.args[0], "lblSearchParam5"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblSearchParam5"), extendConfig({}, controller.args[2], "lblSearchParam5"));
        var FlexGroupCriteria5 = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "clipBounds": true,
            "height": "40dp",
            "id": "FlexGroupCriteria5",
            "isVisible": true,
            "layoutType": kony.flex.FLOW_HORIZONTAL,
            "left": "0dp",
            "isModalContainer": false,
            "skin": "slFbox",
            "top": "25dp",
            "width": "100%"
        }, controller.args[0], "FlexGroupCriteria5"), extendConfig({}, controller.args[1], "FlexGroupCriteria5"), extendConfig({}, controller.args[2], "FlexGroupCriteria5"));
        FlexGroupCriteria5.setDefaultUnit(kony.flex.DP);
        var listboxSearchParam5 = new kony.ui.ListBox(extendConfig({
            "height": "40dp",
            "id": "listboxSearchParam5",
            "isVisible": true,
            "left": "0dp",
            "masterData": [
                ["ID_DRIVING_LICENSE", "kony.i18n.getLocalizedString(\"i18n.frmCustomerManagementController.DriversLicense\")"],
                ["ID_SSN", "kony.i18n.getLocalizedString(\"i18n.frmCustomerManagementController.SSN_HEADER\")"],
                ["ID_PASSPORT", "kony.i18n.getLocalizedString(\"i18n.frmCustomerManagementController.Passport\")"],
                ["ID_TIN", "kony.i18n.getLocalizedString(\"i18n.frmCustomerManagement.TIN\")"]
            ],
            "skin": "sknLstBGffffffBRe1e5edR1pxTBLSide",
            "top": "0dp",
            "width": "43%",
            "zIndex": 10
        }, controller.args[0], "listboxSearchParam5"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [2, 0, 1, 0],
            "paddingInPixel": false
        }, controller.args[1], "listboxSearchParam5"), extendConfig({
            "multiSelect": false
        }, controller.args[2], "listboxSearchParam5"));
        var txtSearchParam5 = new kony.ui.TextBox2(extendConfig({
            "autoCapitalize": constants.TEXTBOX_AUTO_CAPITALIZE_NONE,
            "focusSkin": "sknTbx485c75Reg13pxBR1293ccTBRSideFocus",
            "height": "40dp",
            "id": "txtSearchParam5",
            "isVisible": true,
            "keyBoardStyle": constants.TEXTBOX_KEY_BOARD_STYLE_DEFAULT,
            "left": 0,
            "maxTextLength": 15,
            "i18n_placeholder": "kony.i18n.getLocalizedString(\"i18n.frmCustomerManagementController.EnterLicenseNo\")",
            "secureTextEntry": false,
            "skin": "sknTbx485c75Reg13pxBRe1e5edR1pxTBRSide",
            "textInputMode": constants.TEXTBOX_INPUT_MODE_ANY,
            "top": "0px",
            "width": "57%",
            "zIndex": 11
        }, controller.args[0], "txtSearchParam5"), extendConfig({
            "containerHeightMode": constants.TEXTBOX_FONT_METRICS_DRIVEN_HEIGHT,
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [3, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "txtSearchParam5"), extendConfig({
            "autoCorrect": false
        }, controller.args[2], "txtSearchParam5"));
        FlexGroupCriteria5.add(listboxSearchParam5, txtSearchParam5);
        var lblTINBusinessTag = new kony.ui.Label(extendConfig({
            "bottom": "0dp",
            "id": "lblTINBusinessTag",
            "isVisible": true,
            "left": "0px",
            "skin": "sknlblLatoRegular12px485c75",
            "text": "# Only for Business Customer",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, controller.args[0], "lblTINBusinessTag"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblTINBusinessTag"), extendConfig({}, controller.args[2], "lblTINBusinessTag"));
        flxSearchCriteria5.add(lblSearchParam5, FlexGroupCriteria5, lblTINBusinessTag);
        flxThirdRow.add(flxSearchCriteria8, flxSearchCriteria5);
        var flxAdvSearchHeaderandSearchBtn = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "bottom": "0px",
            "clipBounds": true,
            "height": "80px",
            "id": "flxAdvSearchHeaderandSearchBtn",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "left": "0px",
            "isModalContainer": false,
            "skin": "slFbox",
            "top": "10dp",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, controller.args[0], "flxAdvSearchHeaderandSearchBtn"), extendConfig({}, controller.args[1], "flxAdvSearchHeaderandSearchBtn"), extendConfig({}, controller.args[2], "flxAdvSearchHeaderandSearchBtn"));
        flxAdvSearchHeaderandSearchBtn.setDefaultUnit(kony.flex.DP);
        var flxAdvSearch = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "clipBounds": true,
            "height": "20px",
            "id": "flxAdvSearch",
            "isVisible": true,
            "layoutType": kony.flex.FLOW_HORIZONTAL,
            "left": "0dp",
            "isModalContainer": false,
            "skin": "slFbox",
            "top": "0dp",
            "width": kony.flex.USE_PREFFERED_SIZE
        }, controller.args[0], "flxAdvSearch"), extendConfig({}, controller.args[1], "flxAdvSearch"), extendConfig({}, controller.args[2], "flxAdvSearch"));
        flxAdvSearch.setDefaultUnit(kony.flex.DP);
        var btnAdvSearch = new kony.ui.Button(extendConfig({
            "centerY": "50%",
            "id": "btnAdvSearch",
            "isVisible": true,
            "left": "20dp",
            "skin": "sknBtnLato11ABEB11Px",
            "i18n_text": "kony.i18n.getLocalizedString(\"i18n.userwidgetmodel.btnAdvSearch\")",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, controller.args[0], "btnAdvSearch"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_CENTER,
            "displayText": true,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "btnAdvSearch"), extendConfig({}, controller.args[2], "btnAdvSearch"));
        var flxImg = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "clipBounds": true,
            "height": "20px",
            "id": "flxImg",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "left": "5px",
            "isModalContainer": false,
            "skin": "sknCursor",
            "top": "0",
            "width": "30px",
            "zIndex": 1
        }, controller.args[0], "flxImg"), extendConfig({}, controller.args[1], "flxImg"), extendConfig({}, controller.args[2], "flxImg"));
        flxImg.setDefaultUnit(kony.flex.DP);
        var fonticonrightarrow = new kony.ui.Label(extendConfig({
            "centerX": "50%",
            "centerY": "50%",
            "id": "fonticonrightarrow",
            "isVisible": true,
            "left": "5px",
            "skin": "sknfontIconDescDownArrow12px",
            "i18n_text": "kony.i18n.getLocalizedString(\"i18n.userwidgetmodel.fontIconBreadcrumbsRight\")",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, controller.args[0], "fonticonrightarrow"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "fonticonrightarrow"), extendConfig({}, controller.args[2], "fonticonrightarrow"));
        flxImg.add(fonticonrightarrow);
        flxAdvSearch.add(btnAdvSearch, flxImg);
        var btnSave = new kony.ui.Button(extendConfig({
            "bottom": "0px",
            "focusSkin": "sknBtn003E75LatoRegular13pxFFFFFFRad20px",
            "height": "40dp",
            "id": "btnSave",
            "isVisible": true,
            "right": "20dp",
            "skin": "sknBtn003E75LatoRegular13pxFFFFFFRad20px",
            "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmCustomerManagementController.SEARCH\")",
            "width": "100px",
            "zIndex": 1
        }, controller.args[0], "btnSave"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_CENTER,
            "displayText": true,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "btnSave"), extendConfig({
            "hoverSkin": "sknBtn005198LatoRegular13pxFFFFFFRad20px"
        }, controller.args[2], "btnSave"));
        var btnReset = new kony.ui.Button(extendConfig({
            "bottom": "0px",
            "focusSkin": "sknBtnLatoRegulara5abc413pxKA",
            "height": "40dp",
            "id": "btnReset",
            "isVisible": true,
            "left": "20dp",
            "skin": "sknBtnLatoRegulara5abc413pxKA",
            "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmEnrollCustomer.ClearAll_UC\")",
            "width": "124px",
            "zIndex": 1
        }, controller.args[0], "btnReset"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_CENTER,
            "displayText": true,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "btnReset"), extendConfig({
            "hoverSkin": "sknbtnffffffLatoBolda5abc413Pxd7d8e01pxBorderRadius20px"
        }, controller.args[2], "btnReset"));
        flxAdvSearchHeaderandSearchBtn.add(flxAdvSearch, btnSave, btnReset);
        flxSearch.add(flxFirstRow, flxSecondRow, flxThirdRow, flxAdvSearchHeaderandSearchBtn);
        flxSearchContainer.add(flxSearchHeader, flxSearch);
        var flxResults = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
            "clipBounds": true,
            "id": "flxResults",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "left": "0dp",
            "isModalContainer": false,
            "skin": "slFbox",
            "top": "20px",
            "width": "100%",
            "zIndex": 1
        }, controller.args[0], "flxResults"), extendConfig({}, controller.args[1], "flxResults"), extendConfig({}, controller.args[2], "flxResults"));
        flxResults.setDefaultUnit(kony.flex.DP);
        var flxHeaders = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "clipBounds": true,
            "height": "50px",
            "id": "flxHeaders",
            "isVisible": true,
            "layoutType": kony.flex.FLOW_VERTICAL,
            "left": "0dp",
            "isModalContainer": false,
            "skin": "slFbox",
            "top": "0dp",
            "width": "100%"
        }, controller.args[0], "flxHeaders"), extendConfig({}, controller.args[1], "flxHeaders"), extendConfig({}, controller.args[2], "flxHeaders"));
        flxHeaders.setDefaultUnit(kony.flex.DP);
        var flxSearchSeperator = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "clipBounds": true,
            "height": "1px",
            "id": "flxSearchSeperator",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "left": "0px",
            "isModalContainer": false,
            "right": "0px",
            "skin": "sknflxd6dbe7",
            "top": "0dp",
            "zIndex": 1
        }, controller.args[0], "flxSearchSeperator"), extendConfig({}, controller.args[1], "flxSearchSeperator"), extendConfig({}, controller.args[2], "flxSearchSeperator"));
        flxSearchSeperator.setDefaultUnit(kony.flex.DP);
        flxSearchSeperator.add();
        var flxResultsHeader = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "centerY": "50%",
            "clipBounds": true,
            "height": "25px",
            "id": "flxResultsHeader",
            "isVisible": true,
            "layoutType": kony.flex.FLOW_HORIZONTAL,
            "left": "0dp",
            "isModalContainer": false,
            "skin": "slFbox",
            "top": "0px",
            "width": kony.flex.USE_PREFFERED_SIZE
        }, controller.args[0], "flxResultsHeader"), extendConfig({}, controller.args[1], "flxResultsHeader"), extendConfig({}, controller.args[2], "flxResultsHeader"));
        flxResultsHeader.setDefaultUnit(kony.flex.DP);
        var flxLeftContainer = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "bottom": "0px",
            "clipBounds": true,
            "height": "25px",
            "id": "flxLeftContainer",
            "isVisible": true,
            "layoutType": kony.flex.FLOW_HORIZONTAL,
            "left": "20dp",
            "isModalContainer": false,
            "skin": "slFbox",
            "top": "0dp",
            "width": "30%"
        }, controller.args[0], "flxLeftContainer"), extendConfig({}, controller.args[1], "flxLeftContainer"), extendConfig({}, controller.args[2], "flxLeftContainer"));
        flxLeftContainer.setDefaultUnit(kony.flex.DP);
        var lblResultsHeader = new kony.ui.Label(extendConfig({
            "centerY": "50%",
            "id": "lblResultsHeader",
            "isVisible": true,
            "left": "0%",
            "skin": "sknlblLatobold11px485C75",
            "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmCustomerManagementController.RESULTS\")",
            "top": "0px",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, controller.args[0], "lblResultsHeader"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblResultsHeader"), extendConfig({}, controller.args[2], "lblResultsHeader"));
        flxLeftContainer.add(lblResultsHeader);
        var flxRightContainer = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "bottom": "0px",
            "clipBounds": true,
            "height": "25px",
            "id": "flxRightContainer",
            "isVisible": true,
            "layoutType": kony.flex.FLOW_HORIZONTAL,
            "reverseLayoutDirection": true,
            "left": "21dp",
            "isModalContainer": false,
            "right": 0,
            "skin": "slFbox",
            "top": "0px",
            "width": "64%"
        }, controller.args[0], "flxRightContainer"), extendConfig({}, controller.args[1], "flxRightContainer"), extendConfig({}, controller.args[2], "flxRightContainer"));
        flxRightContainer.setDefaultUnit(kony.flex.DP);
        var flxSortIconWrapper = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "centerY": "50%",
            "clipBounds": true,
            "height": "22px",
            "id": "flxSortIconWrapper",
            "isVisible": true,
            "layoutType": kony.flex.FLOW_HORIZONTAL,
            "left": "0dp",
            "isModalContainer": false,
            "skin": "sknCustomerSortWrapper",
            "top": "0dp",
            "width": "25px"
        }, controller.args[0], "flxSortIconWrapper"), extendConfig({}, controller.args[1], "flxSortIconWrapper"), extendConfig({
            "hoverSkin": "sknCustomerSortWrapperHover"
        }, controller.args[2], "flxSortIconWrapper"));
        flxSortIconWrapper.setDefaultUnit(kony.flex.DP);
        var fonticonSortByName = new kony.ui.Label(extendConfig({
            "centerX": "46%",
            "centerY": "46%",
            "id": "fonticonSortByName",
            "isVisible": true,
            "left": "0",
            "skin": "sknCustomerSearchSortIcon",
            "text": "",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, controller.args[0], "fonticonSortByName"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "fonticonSortByName"), extendConfig({
            "toolTip": "Sort"
        }, controller.args[2], "fonticonSortByName"));
        flxSortIconWrapper.add(fonticonSortByName);
        var lblSortByName = new kony.ui.Label(extendConfig({
            "centerY": "50%",
            "id": "lblSortByName",
            "isVisible": true,
            "left": "0px",
            "right": "10px",
            "skin": "sknlblLatoRegular12px485c75",
            "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmCustomerManagementController.SortByName\")",
            "top": "0px",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, controller.args[0], "lblSortByName"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblSortByName"), extendConfig({}, controller.args[2], "lblSortByName"));
        var listboxCustomerType = new kony.ui.ListBox(extendConfig({
            "centerY": "50%",
            "focusSkin": "defListBoxFocus",
            "height": "24px",
            "id": "listboxCustomerType",
            "isVisible": true,
            "left": "0dp",
            "masterData": [
                ["lb1", "All Customers"],
                ["lb2", "Retail"],
                ["lb3", "Corporate"],
                ["lb4", "Applicant"]
            ],
            "right": "25px",
            "skin": "sknLstboxCustomerType",
            "top": "5dp",
            "width": "120px",
            "zIndex": 1
        }, controller.args[0], "listboxCustomerType"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [1, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "listboxCustomerType"), extendConfig({
            "hoverSkin": "sknLstboxCustomerTypeHover",
            "multiSelect": false
        }, controller.args[2], "listboxCustomerType"));
        var lblFIlterBy = new kony.ui.Label(extendConfig({
            "centerY": "50%",
            "id": "lblFIlterBy",
            "isVisible": true,
            "left": "0px",
            "right": "10px",
            "skin": "sknlblLatoRegular12px485c75",
            "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmCustomerManagementController.Filterby\")",
            "top": "0px",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, controller.args[0], "lblFIlterBy"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblFIlterBy"), extendConfig({}, controller.args[2], "lblFIlterBy"));
        flxRightContainer.add(flxSortIconWrapper, lblSortByName, listboxCustomerType, lblFIlterBy);
        flxResultsHeader.add(flxLeftContainer, flxRightContainer);
        flxHeaders.add(flxSearchSeperator, flxResultsHeader);
        var segCustomerResults = new kony.ui.SegmentedUI2(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
            "data": [{
                "fontIconCircle1": "kony.i18n.getLocalizedString(\"i18n.userwidgetmodel.lblFontIconStatus1\")",
                "fontIconCircle2": "kony.i18n.getLocalizedString(\"i18n.userwidgetmodel.lblFontIconStatus1\")",
                "fontIconCircle3": "kony.i18n.getLocalizedString(\"i18n.userwidgetmodel.lblFontIconStatus1\")",
                "fontIconCircle4": "kony.i18n.getLocalizedString(\"i18n.userwidgetmodel.lblFontIconStatus1\")",
                "fontIconCircle5": "kony.i18n.getLocalizedString(\"i18n.userwidgetmodel.lblFontIconStatus1\")",
                "imgRadioBtn": "imagedrag.png",
                "lblContent1": "Retail Customer",
                "lblContent2": "Applicant",
                "lblContent3": "Small Business User",
                "lblContent4": "kony.i18n.getLocalizedString(\"i18n.frmCustomerProfile.BusinessUser\")",
                "lblContent5": "Lead",
                "lblCustomerName": "Label",
                "lblData1": "TRN232422",
                "lblData2": "*****2212",
                "lblData3": "02/12/1995",
                "lblData4": "Active",
                "lblData5": "Active",
                "lblData6": "Active",
                "lblData7": "Active",
                "lblData8": "Active",
                "lblHeading1": "CUSTOMER ID",
                "lblHeading2": "SSN",
                "lblHeading3": "DOB",
                "lblHeading4": "STATUS",
                "lblHeading5": "STATUS",
                "lblHeading6": "STATUS",
                "lblHeading7": "kony.i18n.getLocalizedString(\"i18n.permission.EMAILID\")",
                "lblHeading8": "kony.i18n.getLocalizedString(\"i18n.permission.EMAILID\")",
                "lblIconStatus": "kony.i18n.getLocalizedString(\"i18n.userwidgetmodel.lblHeaderSeparator\")",
                "lblRowSeparator": "kony.i18n.getLocalizedString(\"i18n.userwidgetmodel.lblHeaderSeparator\")",
                "lblStatusValue": "Jonh.Bailey"
            }],
            "groupCells": false,
            "id": "segCustomerResults",
            "isVisible": true,
            "left": "0dp",
            "needPageIndicator": true,
            "pageOffDotImage": "pageoffdot.png",
            "pageOnDotImage": "pageondot.png",
            "retainSelection": false,
            "rowFocusSkin": "seg2Focus",
            "rowSkin": "seg2Normal",
            "rowTemplate": "flxCustomerResults",
            "sectionHeaderSkin": "sliPhoneSegmentHeader",
            "selectionBehavior": constants.SEGUI_DEFAULT_BEHAVIOR,
            "separatorColor": "aaaaaa00",
            "separatorRequired": true,
            "separatorThickness": 0,
            "showScrollbars": false,
            "top": "55px",
            "viewType": constants.SEGUI_VIEW_TYPE_TABLEVIEW,
            "widgetDataMap": {
                "flxApplicantTag": "flxApplicantTag",
                "flxCustomerResults": "flxCustomerResults",
                "flxDataContainer1": "flxDataContainer1",
                "flxDataContainer2": "flxDataContainer2",
                "flxDataContainer3": "flxDataContainer3",
                "flxDataContainer4": "flxDataContainer4",
                "flxDataContainer5": "flxDataContainer5",
                "flxDataContainer6": "flxDataContainer6",
                "flxDataContainer7": "flxDataContainer7",
                "flxDataContainer8": "flxDataContainer8",
                "flxInnerContainer": "flxInnerContainer",
                "flxLeadTag": "flxLeadTag",
                "flxLeftContainer": "flxLeftContainer",
                "flxLowerContainer": "flxLowerContainer",
                "flxMicroBusinessTag": "flxMicroBusinessTag",
                "flxRadioButton": "flxRadioButton",
                "flxRetailTag": "flxRetailTag",
                "flxRightContainer": "flxRightContainer",
                "flxRow1": "flxRow1",
                "flxRow2": "flxRow2",
                "flxSmallBusinessTag": "flxSmallBusinessTag",
                "flxUpperContainer": "flxUpperContainer",
                "fontIconCircle1": "fontIconCircle1",
                "fontIconCircle2": "fontIconCircle2",
                "fontIconCircle3": "fontIconCircle3",
                "fontIconCircle4": "fontIconCircle4",
                "fontIconCircle5": "fontIconCircle5",
                "imgRadioBtn": "imgRadioBtn",
                "lblContent1": "lblContent1",
                "lblContent2": "lblContent2",
                "lblContent3": "lblContent3",
                "lblContent4": "lblContent4",
                "lblContent5": "lblContent5",
                "lblCustomerName": "lblCustomerName",
                "lblData1": "lblData1",
                "lblData2": "lblData2",
                "lblData3": "lblData3",
                "lblData4": "lblData4",
                "lblData5": "lblData5",
                "lblData6": "lblData6",
                "lblData7": "lblData7",
                "lblData8": "lblData8",
                "lblHeading1": "lblHeading1",
                "lblHeading2": "lblHeading2",
                "lblHeading3": "lblHeading3",
                "lblHeading4": "lblHeading4",
                "lblHeading5": "lblHeading5",
                "lblHeading6": "lblHeading6",
                "lblHeading7": "lblHeading7",
                "lblHeading8": "lblHeading8",
                "lblIconStatus": "lblIconStatus",
                "lblRowSeparator": "lblRowSeparator",
                "lblStatusValue": "lblStatusValue"
            },
            "width": "100%",
            "zIndex": 1
        }, controller.args[0], "segCustomerResults"), extendConfig({
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "segCustomerResults"), extendConfig({}, controller.args[2], "segCustomerResults"));
        var flxNoResultsFound = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "clipBounds": true,
            "height": "300px",
            "id": "flxNoResultsFound",
            "isVisible": false,
            "layoutType": kony.flex.FLOW_HORIZONTAL,
            "left": "0dp",
            "isModalContainer": false,
            "skin": "slFbox",
            "top": "30px",
            "width": "100%",
            "zIndex": 1
        }, controller.args[0], "flxNoResultsFound"), extendConfig({}, controller.args[1], "flxNoResultsFound"), extendConfig({}, controller.args[2], "flxNoResultsFound"));
        flxNoResultsFound.setDefaultUnit(kony.flex.DP);
        var lblNoResult = new kony.ui.Label(extendConfig({
            "centerX": "47%",
            "id": "lblNoResult",
            "isVisible": true,
            "skin": "sknLblLatoReg78818A13px",
            "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmCustomerManagementController.NoresultsSearchForaCustomer\")",
            "top": "110dp",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, controller.args[0], "lblNoResult"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblNoResult"), extendConfig({}, controller.args[2], "lblNoResult"));
        var lblCreateCustomer = new kony.ui.Label(extendConfig({
            "id": "lblCreateCustomer",
            "isVisible": false,
            "left": "3px",
            "right": "0px",
            "skin": "sknLblLato117eb0Sz12px",
            "text": "CREATE CUSTOMER",
            "top": "110px",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 5
        }, controller.args[0], "lblCreateCustomer"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblCreateCustomer"), extendConfig({
            "hoverSkin": "sknLblLato117eb0Sz12px"
        }, controller.args[2], "lblCreateCustomer"));
        flxNoResultsFound.add(lblNoResult, lblCreateCustomer);
        flxResults.add(flxHeaders, segCustomerResults, flxNoResultsFound);
        flxInner.add(flxSearchContainer, flxResults);
        CustomerSearchandResults.add(flxInner);
        return CustomerSearchandResults;
    }
})