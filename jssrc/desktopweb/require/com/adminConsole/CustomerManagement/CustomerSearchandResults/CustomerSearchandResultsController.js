define("com/adminConsole/CustomerManagement/CustomerSearchandResults/userCustomerSearchandResultsController", function() {
    return {};
});
define("com/adminConsole/CustomerManagement/CustomerSearchandResults/CustomerSearchandResultsControllerActions", {
    /*
          This is an auto generated file and any modifications to it may result in corruption of the action sequence.
        */
});
define("com/adminConsole/CustomerManagement/CustomerSearchandResults/CustomerSearchandResultsController", ["com/adminConsole/CustomerManagement/CustomerSearchandResults/userCustomerSearchandResultsController", "com/adminConsole/CustomerManagement/CustomerSearchandResults/CustomerSearchandResultsControllerActions"], function() {
    var controller = require("com/adminConsole/CustomerManagement/CustomerSearchandResults/userCustomerSearchandResultsController");
    var actions = require("com/adminConsole/CustomerManagement/CustomerSearchandResults/CustomerSearchandResultsControllerActions");
    for (var key in actions) {
        controller[key] = actions[key];
    }
    return controller;
});
