define("com/adminConsole/alerts/alertGroupLayout/useralertGroupLayoutController", function() {
    return {
        setFlowActions: function() {
            var scopeObj = this;
            this.view.lblToggle.skin = "sknIcon6E7178Sz15px";
            this.view.lblToggle.text = "\ue922";
            this.view.flxHideDisplayContent.setVisibility(false);
            this.view.lblHeaderSeperator.setVisibility(false);
            this.view.flxToggle.onClick = function() {
                scopeObj.toggleContent();
            };
        },
        toggleContent: function() {
            if (this.view.flxHideDisplayContent.isVisible) {
                this.view.lblToggle.skin = "sknIcon6E7178Sz15px";
                this.view.lblToggle.text = "\ue922";
                this.view.flxHideDisplayContent.setVisibility(false);
                this.view.lblHeaderSeperator.setVisibility(false);
            } else {
                this.view.lblToggle.skin = "sknIcon6E7178Sz13px";
                this.view.lblToggle.text = "\ue915";
                this.view.flxHideDisplayContent.setVisibility(true);
                this.view.lblHeaderSeperator.setVisibility(true);
            }
            this.view.forceLayout();
        }
    };
});
define("com/adminConsole/alerts/alertGroupLayout/alertGroupLayoutControllerActions", {
    /*
        This is an auto generated file and any modifications to it may result in corruption of the action sequence.
    */
    AS_FlexContainer_f7722c2eab344577a81d63facebe898e: function AS_FlexContainer_f7722c2eab344577a81d63facebe898e(eventobject) {
        var self = this;
        this.setFlowActions();
    }
});
define("com/adminConsole/alerts/alertGroupLayout/alertGroupLayoutController", ["com/adminConsole/alerts/alertGroupLayout/useralertGroupLayoutController", "com/adminConsole/alerts/alertGroupLayout/alertGroupLayoutControllerActions"], function() {
    var controller = require("com/adminConsole/alerts/alertGroupLayout/useralertGroupLayoutController");
    var actions = require("com/adminConsole/alerts/alertGroupLayout/alertGroupLayoutControllerActions");
    for (var key in actions) {
        controller[key] = actions[key];
    }
    return controller;
});
