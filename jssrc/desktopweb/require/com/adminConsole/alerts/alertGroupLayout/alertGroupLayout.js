define(function() {
    return function(controller) {
        var alertGroupLayout = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
            "clipBounds": true,
            "isMaster": true,
            "id": "alertGroupLayout",
            "isVisible": true,
            "layoutType": kony.flex.FLOW_VERTICAL,
            "left": "0dp",
            "isModalContainer": false,
            "preShow": function(eventobject) {
                controller.AS_FlexContainer_f7722c2eab344577a81d63facebe898e(eventobject);
            },
            "skin": "sknFlxbgFFFFFFbdrD7D9E0rd3px",
            "top": "0dp",
            "width": "100%"
        }, controller.args[0], "alertGroupLayout"), extendConfig({}, controller.args[1], "alertGroupLayout"), extendConfig({}, controller.args[2], "alertGroupLayout"));
        alertGroupLayout.setDefaultUnit(kony.flex.DP);
        var flxHeader = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "clipBounds": true,
            "height": "61dp",
            "id": "flxHeader",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "left": "0px",
            "isModalContainer": false,
            "right": "0px",
            "skin": "sknflxf5f6f8Op100",
            "top": "0px",
            "zIndex": 1
        }, controller.args[0], "flxHeader"), extendConfig({}, controller.args[1], "flxHeader"), extendConfig({
            "hoverSkin": "sknflxf5f6f8Op100Pointer"
        }, controller.args[2], "flxHeader"));
        flxHeader.setDefaultUnit(kony.flex.DP);
        var flxHeadingsLeft = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
            "clipBounds": true,
            "id": "flxHeadingsLeft",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "left": "0dp",
            "isModalContainer": false,
            "right": "150dp",
            "skin": "slFbox",
            "top": "12dp"
        }, controller.args[0], "flxHeadingsLeft"), extendConfig({}, controller.args[1], "flxHeadingsLeft"), extendConfig({}, controller.args[2], "flxHeadingsLeft"));
        flxHeadingsLeft.setDefaultUnit(kony.flex.DP);
        var flxAlertGroupName = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
            "clipBounds": true,
            "id": "flxAlertGroupName",
            "isVisible": true,
            "layoutType": kony.flex.FLOW_HORIZONTAL,
            "left": "0dp",
            "isModalContainer": false,
            "skin": "slFbox",
            "top": "0dp",
            "width": "100%"
        }, controller.args[0], "flxAlertGroupName"), extendConfig({}, controller.args[1], "flxAlertGroupName"), extendConfig({}, controller.args[2], "flxAlertGroupName"));
        flxAlertGroupName.setDefaultUnit(kony.flex.DP);
        var flxToggle = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "clipBounds": true,
            "height": "13dp",
            "id": "flxToggle",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "left": "10dp",
            "isModalContainer": false,
            "right": "0px",
            "skin": "sknCursor",
            "top": "3dp",
            "width": "15dp",
            "zIndex": 1
        }, controller.args[0], "flxToggle"), extendConfig({}, controller.args[1], "flxToggle"), extendConfig({
            "hoverSkin": "sknCursor"
        }, controller.args[2], "flxToggle"));
        flxToggle.setDefaultUnit(kony.flex.DP);
        var lblToggle = new kony.ui.Label(extendConfig({
            "centerX": "50%",
            "centerY": "50%",
            "height": "100%",
            "id": "lblToggle",
            "isVisible": true,
            "skin": "sknIcon12pxBlack",
            "i18n_text": "kony.i18n.getLocalizedString(\"i18n.userwidgetmodel.fontIconBreadcrumbsDown\")",
            "width": "100%",
            "zIndex": 1
        }, controller.args[0], "lblToggle"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblToggle"), extendConfig({}, controller.args[2], "lblToggle"));
        flxToggle.add(lblToggle);
        var lblFeatureName = new kony.ui.Label(extendConfig({
            "id": "lblFeatureName",
            "isVisible": true,
            "left": "5dp",
            "skin": "sknLbl192B45LatoRegular14px",
            "i18n_text": "kony.i18n.getLocalizedString(\"i18n.permission.DESCRIPTION\")",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, controller.args[0], "lblFeatureName"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblFeatureName"), extendConfig({}, controller.args[2], "lblFeatureName"));
        flxAlertGroupName.add(flxToggle, lblFeatureName);
        var flxAlertCount = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "clipBounds": true,
            "height": "25dp",
            "id": "flxAlertCount",
            "isVisible": true,
            "layoutType": kony.flex.FLOW_HORIZONTAL,
            "left": "25dp",
            "isModalContainer": false,
            "skin": "slFbox",
            "top": "20dp",
            "width": "100%"
        }, controller.args[0], "flxAlertCount"), extendConfig({}, controller.args[1], "flxAlertCount"), extendConfig({}, controller.args[2], "flxAlertCount"));
        flxAlertCount.setDefaultUnit(kony.flex.DP);
        var flxAlertGroupTypeIcon = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "centerY": "50%",
            "clipBounds": true,
            "height": "18dp",
            "id": "flxAlertGroupTypeIcon",
            "isVisible": true,
            "layoutType": kony.flex.FLOW_HORIZONTAL,
            "left": "5dp",
            "isModalContainer": false,
            "skin": "sknCursor",
            "top": "0dp",
            "width": "22dp"
        }, controller.args[0], "flxAlertGroupTypeIcon"), extendConfig({}, controller.args[1], "flxAlertGroupTypeIcon"), extendConfig({}, controller.args[2], "flxAlertGroupTypeIcon"));
        flxAlertGroupTypeIcon.setDefaultUnit(kony.flex.DP);
        var lblIconAlertType = new kony.ui.Label(extendConfig({
            "centerX": "50%",
            "centerY": "50%",
            "height": "18dp",
            "id": "lblIconAlertType",
            "isVisible": true,
            "skin": "sknLblIcomoon20px485c75",
            "text": "",
            "width": "20dp",
            "zIndex": 2
        }, controller.args[0], "lblIconAlertType"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblIconAlertType"), extendConfig({}, controller.args[2], "lblIconAlertType"));
        flxAlertGroupTypeIcon.add(lblIconAlertType);
        var lblAlertsHeader = new kony.ui.Label(extendConfig({
            "centerY": "50%",
            "id": "lblAlertsHeader",
            "isVisible": true,
            "left": "10dp",
            "skin": "sknlblLato696c7311px",
            "text": "GLOBAL ALERTS:",
            "top": "2dp",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, controller.args[0], "lblAlertsHeader"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblAlertsHeader"), extendConfig({}, controller.args[2], "lblAlertsHeader"));
        var lblAlertsCount = new kony.ui.Label(extendConfig({
            "id": "lblAlertsCount",
            "isVisible": true,
            "left": "0dp",
            "skin": "sknLbl192B45LatoSemiBold13px",
            "text": "2",
            "top": "0dp",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, controller.args[0], "lblAlertsCount"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [1, 0, 1, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblAlertsCount"), extendConfig({}, controller.args[2], "lblAlertsCount"));
        var lblHorizSeperator1 = new kony.ui.Label(extendConfig({
            "centerY": "50%",
            "height": "13dp",
            "id": "lblHorizSeperator1",
            "isVisible": true,
            "left": "0dp",
            "right": "5dp",
            "skin": "sknLblSeparator696C73",
            "text": ".",
            "top": 0,
            "width": "1dp"
        }, controller.args[0], "lblHorizSeperator1"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblHorizSeperator1"), extendConfig({}, controller.args[2], "lblHorizSeperator1"));
        var flxUserAlert = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "centerY": "50%",
            "clipBounds": true,
            "height": "18dp",
            "id": "flxUserAlert",
            "isVisible": true,
            "layoutType": kony.flex.FLOW_HORIZONTAL,
            "left": "0dp",
            "isModalContainer": false,
            "skin": "sknCursor",
            "top": "0dp",
            "width": "18dp"
        }, controller.args[0], "flxUserAlert"), extendConfig({}, controller.args[1], "flxUserAlert"), extendConfig({}, controller.args[2], "flxUserAlert"));
        flxUserAlert.setDefaultUnit(kony.flex.DP);
        var lblUsrAlertsIcon = new kony.ui.Label(extendConfig({
            "centerX": "50%",
            "centerY": "50%",
            "height": "18dp",
            "id": "lblUsrAlertsIcon",
            "isVisible": true,
            "skin": "sknLblIcomoon20px485c75",
            "text": "",
            "width": "20dp",
            "zIndex": 2
        }, controller.args[0], "lblUsrAlertsIcon"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblUsrAlertsIcon"), extendConfig({}, controller.args[2], "lblUsrAlertsIcon"));
        flxUserAlert.add(lblUsrAlertsIcon);
        var lblUserAlertsTxt = new kony.ui.Label(extendConfig({
            "centerY": "50%",
            "id": "lblUserAlertsTxt",
            "isVisible": true,
            "left": "10dp",
            "skin": "sknlblLato696c7311px",
            "text": "USER ALERTS:",
            "top": "2dp",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, controller.args[0], "lblUserAlertsTxt"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblUserAlertsTxt"), extendConfig({}, controller.args[2], "lblUserAlertsTxt"));
        var lblUsrAlertsCount = new kony.ui.Label(extendConfig({
            "centerY": "50%",
            "id": "lblUsrAlertsCount",
            "isVisible": true,
            "left": "0dp",
            "skin": "sknLbl192B45LatoSemiBold13px",
            "text": "2",
            "top": "0dp",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, controller.args[0], "lblUsrAlertsCount"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [1, 0, 1, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblUsrAlertsCount"), extendConfig({}, controller.args[2], "lblUsrAlertsCount"));
        var lblHorizSeperator2 = new kony.ui.Label(extendConfig({
            "centerY": "50%",
            "height": "13dp",
            "id": "lblHorizSeperator2",
            "isVisible": true,
            "left": "0dp",
            "right": "5dp",
            "skin": "sknLblSeparator696C73",
            "text": ".",
            "top": "0",
            "width": "1dp"
        }, controller.args[0], "lblHorizSeperator2"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblHorizSeperator2"), extendConfig({}, controller.args[2], "lblHorizSeperator2"));
        var lblAutoSubscribeTxt = new kony.ui.Label(extendConfig({
            "centerY": "50%",
            "id": "lblAutoSubscribeTxt",
            "isVisible": true,
            "left": "0dp",
            "skin": "sknlblLato696c7311px",
            "text": "AUTO SUBSCRIBED ALERTS:",
            "top": "2dp",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, controller.args[0], "lblAutoSubscribeTxt"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblAutoSubscribeTxt"), extendConfig({}, controller.args[2], "lblAutoSubscribeTxt"));
        var lblAutoSubscribeCount = new kony.ui.Label(extendConfig({
            "centerY": "50%",
            "id": "lblAutoSubscribeCount",
            "isVisible": true,
            "left": "0dp",
            "skin": "sknLbl192B45LatoSemiBold13px",
            "text": "2",
            "top": "0dp",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, controller.args[0], "lblAutoSubscribeCount"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [1, 0, 1, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblAutoSubscribeCount"), extendConfig({}, controller.args[2], "lblAutoSubscribeCount"));
        var lblHorizSeperator3 = new kony.ui.Label(extendConfig({
            "centerY": "50%",
            "height": "13dp",
            "id": "lblHorizSeperator3",
            "isVisible": true,
            "left": "0dp",
            "right": "5dp",
            "skin": "sknLblSeparator696C73",
            "text": ".",
            "top": "0",
            "width": "1dp"
        }, controller.args[0], "lblHorizSeperator3"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblHorizSeperator3"), extendConfig({}, controller.args[2], "lblHorizSeperator3"));
        var lblDefFreqText = new kony.ui.Label(extendConfig({
            "centerY": "50%",
            "id": "lblDefFreqText",
            "isVisible": true,
            "left": "0dp",
            "skin": "sknlblLato696c7311px",
            "text": "FREQUENCY ENABLED ALERTS:",
            "top": "2dp",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, controller.args[0], "lblDefFreqText"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblDefFreqText"), extendConfig({}, controller.args[2], "lblDefFreqText"));
        var lblDefFreqCount = new kony.ui.Label(extendConfig({
            "centerY": "50%",
            "id": "lblDefFreqCount",
            "isVisible": true,
            "left": "0dp",
            "skin": "sknLbl192B45LatoSemiBold13px",
            "text": "2",
            "top": "0dp",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, controller.args[0], "lblDefFreqCount"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [1, 0, 1, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblDefFreqCount"), extendConfig({}, controller.args[2], "lblDefFreqCount"));
        flxAlertCount.add(flxAlertGroupTypeIcon, lblAlertsHeader, lblAlertsCount, lblHorizSeperator1, flxUserAlert, lblUserAlertsTxt, lblUsrAlertsCount, lblHorizSeperator2, lblAutoSubscribeTxt, lblAutoSubscribeCount, lblHorizSeperator3, lblDefFreqText, lblDefFreqCount);
        flxHeadingsLeft.add(flxAlertGroupName, flxAlertCount);
        var flxStatusOptions = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "centerY": "50%",
            "clipBounds": true,
            "height": "30dp",
            "id": "flxStatusOptions",
            "isVisible": true,
            "layoutType": kony.flex.FLOW_HORIZONTAL,
            "reverseLayoutDirection": true,
            "isModalContainer": false,
            "right": "0dp",
            "skin": "slFbox",
            "width": "150dp",
            "zIndex": 2
        }, controller.args[0], "flxStatusOptions"), extendConfig({}, controller.args[1], "flxStatusOptions"), extendConfig({}, controller.args[2], "flxStatusOptions"));
        flxStatusOptions.setDefaultUnit(kony.flex.DP);
        var flxOptions = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "centerY": "50%",
            "clipBounds": true,
            "height": "30px",
            "id": "flxOptions",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "isModalContainer": false,
            "right": "20px",
            "skin": "slFbox",
            "width": "30px",
            "zIndex": 2
        }, controller.args[0], "flxOptions"), extendConfig({}, controller.args[1], "flxOptions"), extendConfig({
            "hoverSkin": "sknflxffffffop100Border424242Radius100px"
        }, controller.args[2], "flxOptions"));
        flxOptions.setDefaultUnit(kony.flex.DP);
        var lblIconOptions = new kony.ui.Label(extendConfig({
            "centerX": "50%",
            "centerY": "50%",
            "height": "16px",
            "id": "lblIconOptions",
            "isVisible": true,
            "left": "14dp",
            "skin": "sknFontIconOptionMenu",
            "text": "",
            "width": "16px",
            "zIndex": 1
        }, controller.args[0], "lblIconOptions"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblIconOptions"), extendConfig({}, controller.args[2], "lblIconOptions"));
        flxOptions.add(lblIconOptions);
        var lblStatusName = new kony.ui.Label(extendConfig({
            "centerY": "50%",
            "id": "lblStatusName",
            "isVisible": true,
            "right": "20dp",
            "skin": "sknlblLato5bc06cBold14px",
            "i18n_text": "kony.i18n.getLocalizedString(\"i18n.secureimage.Active\")",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, controller.args[0], "lblStatusName"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblStatusName"), extendConfig({}, controller.args[2], "lblStatusName"));
        var lblIconStatus = new kony.ui.Label(extendConfig({
            "centerY": "51%",
            "id": "lblIconStatus",
            "isVisible": true,
            "right": "5dp",
            "skin": "sknFontIconActivate",
            "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmAlertsManagement.lblViewAlertStatusKey\")",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, controller.args[0], "lblIconStatus"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblIconStatus"), extendConfig({}, controller.args[2], "lblIconStatus"));
        var lblVerticalLine = new kony.ui.Label(extendConfig({
            "height": "100%",
            "id": "lblVerticalLine",
            "isVisible": false,
            "right": "15dp",
            "skin": "sknlblSeperatorD7D9E0",
            "text": ".",
            "top": "0dp",
            "width": "1dp",
            "zIndex": 2
        }, controller.args[0], "lblVerticalLine"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblVerticalLine"), extendConfig({}, controller.args[2], "lblVerticalLine"));
        flxStatusOptions.add(flxOptions, lblStatusName, lblIconStatus, lblVerticalLine);
        var lblHeaderSeperator = new kony.ui.Label(extendConfig({
            "bottom": "0dp",
            "height": "1px",
            "id": "lblHeaderSeperator",
            "isVisible": true,
            "left": "0dp",
            "right": "0dp",
            "skin": "sknlblSeperator",
            "text": ".",
            "zIndex": 1
        }, controller.args[0], "lblHeaderSeperator"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblHeaderSeperator"), extendConfig({}, controller.args[2], "lblHeaderSeperator"));
        flxHeader.add(flxHeadingsLeft, flxStatusOptions, lblHeaderSeperator);
        var flxHideDisplayContent = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
            "bottom": "5dp",
            "clipBounds": true,
            "id": "flxHideDisplayContent",
            "isVisible": false,
            "layoutType": kony.flex.FLOW_VERTICAL,
            "left": "0dp",
            "isModalContainer": false,
            "skin": "slFbox",
            "top": "0dp",
            "width": "100%"
        }, controller.args[0], "flxHideDisplayContent"), extendConfig({}, controller.args[1], "flxHideDisplayContent"), extendConfig({}, controller.args[2], "flxHideDisplayContent"));
        flxHideDisplayContent.setDefaultUnit(kony.flex.DP);
        var flxAlertGroupHeader = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "clipBounds": true,
            "height": "40px",
            "id": "flxAlertGroupHeader",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "left": "0dp",
            "isModalContainer": false,
            "skin": "sknflxffffffop100",
            "top": "0",
            "width": "100%"
        }, controller.args[0], "flxAlertGroupHeader"), extendConfig({}, controller.args[1], "flxAlertGroupHeader"), extendConfig({}, controller.args[2], "flxAlertGroupHeader"));
        flxAlertGroupHeader.setDefaultUnit(kony.flex.DP);
        var flxAlertGroupsHeaderCont = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "clipBounds": true,
            "height": "100%",
            "id": "flxAlertGroupsHeaderCont",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "left": "10dp",
            "isModalContainer": false,
            "right": "10dp",
            "skin": "slFbox",
            "top": "0dp"
        }, controller.args[0], "flxAlertGroupsHeaderCont"), extendConfig({}, controller.args[1], "flxAlertGroupsHeaderCont"), extendConfig({}, controller.args[2], "flxAlertGroupsHeaderCont"));
        flxAlertGroupsHeaderCont.setDefaultUnit(kony.flex.DP);
        var lblAlertGroupSeperator = new kony.ui.Label(extendConfig({
            "bottom": "0dp",
            "height": "1px",
            "id": "lblAlertGroupSeperator",
            "isVisible": false,
            "left": "10dp",
            "right": "10dp",
            "skin": "sknlblSeperator",
            "text": ".",
            "zIndex": 1
        }, controller.args[0], "lblAlertGroupSeperator"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblAlertGroupSeperator"), extendConfig({}, controller.args[2], "lblAlertGroupSeperator"));
        var lblAlertGroupStatus = new kony.ui.Label(extendConfig({
            "centerY": "50%",
            "id": "lblAlertGroupStatus",
            "isVisible": true,
            "left": "90%",
            "skin": "sknlblLato696c7312px",
            "text": "STATUS",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, controller.args[0], "lblAlertGroupStatus"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblAlertGroupStatus"), extendConfig({}, controller.args[2], "lblAlertGroupStatus"));
        var lblAlertGroupDescription = new kony.ui.Label(extendConfig({
            "centerY": "50%",
            "id": "lblAlertGroupDescription",
            "isVisible": true,
            "left": "50%",
            "skin": "sknlblLato696c7312px",
            "text": "ALERT TYPE",
            "width": "10%",
            "zIndex": 1
        }, controller.args[0], "lblAlertGroupDescription"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblAlertGroupDescription"), extendConfig({}, controller.args[2], "lblAlertGroupDescription"));
        var lblAlertType = new kony.ui.Label(extendConfig({
            "centerY": "50%",
            "id": "lblAlertType",
            "isVisible": true,
            "left": "70%",
            "skin": "sknlblLato696c7312px",
            "text": "FREQUENCY ENABLED",
            "width": "200dp",
            "zIndex": 1
        }, controller.args[0], "lblAlertType"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblAlertType"), extendConfig({}, controller.args[2], "lblAlertType"));
        var lblAlertGroupCode = new kony.ui.Label(extendConfig({
            "centerY": "50%",
            "id": "lblAlertGroupCode",
            "isVisible": true,
            "left": "21%",
            "skin": "sknlblLato696c7312px",
            "text": "ALERT CODE",
            "width": "20%",
            "zIndex": 1
        }, controller.args[0], "lblAlertGroupCode"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblAlertGroupCode"), extendConfig({}, controller.args[2], "lblAlertGroupCode"));
        var lblAlertGroupName = new kony.ui.Label(extendConfig({
            "centerY": "50%",
            "id": "lblAlertGroupName",
            "isVisible": true,
            "left": "30px",
            "skin": "sknlblLato696c7312px",
            "text": "ALERT NAME",
            "width": "20%",
            "zIndex": 1
        }, controller.args[0], "lblAlertGroupName"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblAlertGroupName"), extendConfig({}, controller.args[2], "lblAlertGroupName"));
        flxAlertGroupsHeaderCont.add(lblAlertGroupSeperator, lblAlertGroupStatus, lblAlertGroupDescription, lblAlertType, lblAlertGroupCode, lblAlertGroupName);
        flxAlertGroupHeader.add(flxAlertGroupsHeaderCont);
        var flxAlertsList = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
            "clipBounds": true,
            "id": "flxAlertsList",
            "isVisible": true,
            "layoutType": kony.flex.FLOW_VERTICAL,
            "left": "0dp",
            "isModalContainer": false,
            "skin": "slFbox",
            "top": "0dp",
            "width": "100%"
        }, controller.args[0], "flxAlertsList"), extendConfig({}, controller.args[1], "flxAlertsList"), extendConfig({}, controller.args[2], "flxAlertsList"));
        flxAlertsList.setDefaultUnit(kony.flex.DP);
        var segAlertsList = new kony.ui.SegmentedUI2(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
            "data": [{
                "btnAutoSubscribed": "Auto Subscribed",
                "btnExt": "Button",
                "lblAlertCode": "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do",
                "lblAlertName": "Admin Role",
                "lblAlertStatus": "Active",
                "lblFreqEnabled": "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do",
                "lblIcon": "",
                "lblIconStatus": "",
                "lblSeperator": "."
            }, {
                "btnAutoSubscribed": "Auto Subscribed",
                "btnExt": "Button",
                "lblAlertCode": "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do",
                "lblAlertName": "Admin Role",
                "lblAlertStatus": "Active",
                "lblFreqEnabled": "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do",
                "lblIcon": "",
                "lblIconStatus": "",
                "lblSeperator": "."
            }, {
                "btnAutoSubscribed": "Auto Subscribed",
                "btnExt": "Button",
                "lblAlertCode": "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do",
                "lblAlertName": "Admin Role",
                "lblAlertStatus": "Active",
                "lblFreqEnabled": "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do",
                "lblIcon": "",
                "lblIconStatus": "",
                "lblSeperator": "."
            }],
            "groupCells": false,
            "id": "segAlertsList",
            "isVisible": true,
            "left": "0dp",
            "needPageIndicator": true,
            "pageOffDotImage": "pageoffdot.png",
            "pageOnDotImage": "pageondot.png",
            "retainSelection": false,
            "rowFocusSkin": "seg2Focus",
            "rowSkin": "seg2Normal",
            "rowTemplate": "flxAlertGroupCardList",
            "sectionHeaderSkin": "sliPhoneSegmentHeader",
            "selectionBehavior": constants.SEGUI_SINGLE_SELECT_BEHAVIOR,
            "selectionBehaviorConfig": {
                "imageIdentifier": "imgCheckBoxMsg",
                "selectedStateImage": "checkboxselected.png",
                "unselectedStateImage": "checkboxnormal.png"
            },
            "separatorRequired": false,
            "showScrollbars": false,
            "viewType": constants.SEGUI_VIEW_TYPE_TABLEVIEW,
            "widgetDataMap": {
                "btnAutoSubscribed": "btnAutoSubscribed",
                "btnExt": "btnExt",
                "flxAlertGroupCardList": "flxAlertGroupCardList",
                "flxGroupCardListHoverContainer": "flxGroupCardListHoverContainer",
                "flxStatus": "flxStatus",
                "flxTagsGroup": "flxTagsGroup",
                "lblAlertCode": "lblAlertCode",
                "lblAlertName": "lblAlertName",
                "lblAlertStatus": "lblAlertStatus",
                "lblFreqEnabled": "lblFreqEnabled",
                "lblIcon": "lblIcon",
                "lblIconStatus": "lblIconStatus",
                "lblSeperator": "lblSeperator"
            },
            "width": "100%",
            "zIndex": 10
        }, controller.args[0], "segAlertsList"), extendConfig({
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "segAlertsList"), extendConfig({}, controller.args[2], "segAlertsList"));
        flxAlertsList.add(segAlertsList);
        var flxNoAlertsList = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
            "clipBounds": true,
            "id": "flxNoAlertsList",
            "isVisible": false,
            "layoutType": kony.flex.FLOW_VERTICAL,
            "left": "0dp",
            "isModalContainer": false,
            "skin": "sknFlxFFFFFF100O",
            "top": "0dp",
            "width": "100%"
        }, controller.args[0], "flxNoAlertsList"), extendConfig({}, controller.args[1], "flxNoAlertsList"), extendConfig({}, controller.args[2], "flxNoAlertsList"));
        flxNoAlertsList.setDefaultUnit(kony.flex.DP);
        var noAlertsWithButton = new com.adminConsole.common.noResultsWithButton(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
            "id": "noAlertsWithButton",
            "isVisible": true,
            "layoutType": kony.flex.FLOW_VERTICAL,
            "left": "0dp",
            "masterType": constants.MASTER_TYPE_DEFAULT,
            "isModalContainer": false,
            "skin": "sknFlxFFFFFF100O",
            "top": "0dp",
            "width": "100%",
            "overrides": {
                "btnAddRecord": {
                    "text": "Add Alerts",
                    "width": "81dp"
                }
            }
        }, controller.args[0], "noAlertsWithButton"), extendConfig({
            "overrides": {}
        }, controller.args[1], "noAlertsWithButton"), extendConfig({
            "overrides": {}
        }, controller.args[2], "noAlertsWithButton"));
        flxNoAlertsList.add(noAlertsWithButton);
        flxHideDisplayContent.add(flxAlertGroupHeader, flxAlertsList, flxNoAlertsList);
        alertGroupLayout.add(flxHeader, flxHideDisplayContent);
        return alertGroupLayout;
    }
})