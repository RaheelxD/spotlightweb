define(function() {
    return function(controller) {
        var alertCategoryCard = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
            "clipBounds": true,
            "isMaster": true,
            "id": "alertCategoryCard",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "left": "0dp",
            "isModalContainer": false,
            "skin": "sknFlxBgFFFFFFBorE4E6ECsz1pxR3px",
            "top": "0dp",
            "width": "100%"
        }, controller.args[0], "alertCategoryCard"), extendConfig({}, controller.args[1], "alertCategoryCard"), extendConfig({}, controller.args[2], "alertCategoryCard"));
        alertCategoryCard.setDefaultUnit(kony.flex.DP);
        var flxCardContainer = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
            "clipBounds": true,
            "id": "flxCardContainer",
            "isVisible": true,
            "layoutType": kony.flex.FLOW_VERTICAL,
            "left": "0dp",
            "isModalContainer": false,
            "right": "0dp",
            "skin": "sknCursor",
            "top": "0dp",
            "width": "100%",
            "zIndex": 1
        }, controller.args[0], "flxCardContainer"), extendConfig({}, controller.args[1], "flxCardContainer"), extendConfig({
            "hoverSkin": "sknCursor"
        }, controller.args[2], "flxCardContainer"));
        flxCardContainer.setDefaultUnit(kony.flex.DP);
        var lblCategoryName = new kony.ui.Label(extendConfig({
            "id": "lblCategoryName",
            "isVisible": true,
            "left": "20dp",
            "skin": "sknLbl192B45LatoReg20Px",
            "text": "Security",
            "top": "20dp",
            "width": "88%",
            "zIndex": 1
        }, controller.args[0], "lblCategoryName"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblCategoryName"), extendConfig({}, controller.args[2], "lblCategoryName"));
        var flxCategoryStatus = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "clipBounds": true,
            "height": "15px",
            "id": "flxCategoryStatus",
            "isVisible": true,
            "layoutType": kony.flex.FLOW_HORIZONTAL,
            "reverseLayoutDirection": false,
            "left": "20dp",
            "isModalContainer": false,
            "skin": "slFbox",
            "top": "10dp",
            "width": "120px",
            "zIndex": 1
        }, controller.args[0], "flxCategoryStatus"), extendConfig({}, controller.args[1], "flxCategoryStatus"), extendConfig({}, controller.args[2], "flxCategoryStatus"));
        flxCategoryStatus.setDefaultUnit(kony.flex.DP);
        var lblIconStatus = new kony.ui.Label(extendConfig({
            "centerY": "52%",
            "id": "lblIconStatus",
            "isVisible": true,
            "left": "0dp",
            "skin": "sknFontIconActivate",
            "text": "",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 2
        }, controller.args[0], "lblIconStatus"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblIconStatus"), extendConfig({}, controller.args[2], "lblIconStatus"));
        var lblCategoryStatus = new kony.ui.Label(extendConfig({
            "centerY": "50%",
            "id": "lblCategoryStatus",
            "isVisible": true,
            "left": "5dp",
            "skin": "sknlblLato5bc06cBold14px",
            "text": "Active",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, controller.args[0], "lblCategoryStatus"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblCategoryStatus"), extendConfig({}, controller.args[2], "lblCategoryStatus"));
        flxCategoryStatus.add(lblIconStatus, lblCategoryStatus);
        var flxCountContainer = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "bottom": "20dp",
            "clipBounds": true,
            "height": "15dp",
            "id": "flxCountContainer",
            "isVisible": true,
            "layoutType": kony.flex.FLOW_HORIZONTAL,
            "left": "20dp",
            "isModalContainer": false,
            "skin": "slFbox",
            "top": "15dp",
            "width": "80%",
            "zIndex": 1
        }, controller.args[0], "flxCountContainer"), extendConfig({}, controller.args[1], "flxCountContainer"), extendConfig({}, controller.args[2], "flxCountContainer"));
        flxCountContainer.setDefaultUnit(kony.flex.DP);
        var lblGroupsCount = new kony.ui.Label(extendConfig({
            "centerY": "50%",
            "id": "lblGroupsCount",
            "isVisible": true,
            "left": "0dp",
            "skin": "sknlblLato696c7311px",
            "text": "GROUPS- 3",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, controller.args[0], "lblGroupsCount"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblGroupsCount"), extendConfig({}, controller.args[2], "lblGroupsCount"));
        var lblAlertsCount = new kony.ui.Label(extendConfig({
            "centerY": "50%",
            "id": "lblAlertsCount",
            "isVisible": true,
            "left": "30dp",
            "skin": "sknlblLato696c7311px",
            "text": "ALERTS - 5",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, controller.args[0], "lblAlertsCount"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblAlertsCount"), extendConfig({}, controller.args[2], "lblAlertsCount"));
        flxCountContainer.add(lblGroupsCount, lblAlertsCount);
        flxCardContainer.add(lblCategoryName, flxCategoryStatus, flxCountContainer);
        alertCategoryCard.add(flxCardContainer);
        return alertCategoryCard;
    }
})