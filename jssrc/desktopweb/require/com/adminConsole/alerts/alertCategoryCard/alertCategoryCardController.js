define("com/adminConsole/alerts/alertCategoryCard/useralertCategoryCardController", function() {
    return {};
});
define("com/adminConsole/alerts/alertCategoryCard/alertCategoryCardControllerActions", {
    /*
          This is an auto generated file and any modifications to it may result in corruption of the action sequence.
        */
});
define("com/adminConsole/alerts/alertCategoryCard/alertCategoryCardController", ["com/adminConsole/alerts/alertCategoryCard/useralertCategoryCardController", "com/adminConsole/alerts/alertCategoryCard/alertCategoryCardControllerActions"], function() {
    var controller = require("com/adminConsole/alerts/alertCategoryCard/useralertCategoryCardController");
    var actions = require("com/adminConsole/alerts/alertCategoryCard/alertCategoryCardControllerActions");
    for (var key in actions) {
        controller[key] = actions[key];
    }
    return controller;
});
