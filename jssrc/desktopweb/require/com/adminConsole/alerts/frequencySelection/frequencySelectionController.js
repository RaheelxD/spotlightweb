define("com/adminConsole/alerts/frequencySelection/userfrequencySelectionController", function() {
    return {};
});
define("com/adminConsole/alerts/frequencySelection/frequencySelectionControllerActions", {
    /*
        This is an auto generated file and any modifications to it may result in corruption of the action sequence.
    */
    AS_FlexContainer_jbced4acd60546a6a3f8757e8eef1055: function AS_FlexContainer_jbced4acd60546a6a3f8757e8eef1055(eventobject) {
        var self = this;
        this.view.timePicker.lstbxAMPM.skin = "sknlbxNobgNoBorderPagination";
        this.view.timePicker.flxOuterBorder.skin = "slFbox";
    }
});
define("com/adminConsole/alerts/frequencySelection/frequencySelectionController", ["com/adminConsole/alerts/frequencySelection/userfrequencySelectionController", "com/adminConsole/alerts/frequencySelection/frequencySelectionControllerActions"], function() {
    var controller = require("com/adminConsole/alerts/frequencySelection/userfrequencySelectionController");
    var actions = require("com/adminConsole/alerts/frequencySelection/frequencySelectionControllerActions");
    for (var key in actions) {
        controller[key] = actions[key];
    }
    return controller;
});
