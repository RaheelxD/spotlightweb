define(function() {
    return function(controller) {
        var frequencySelection = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
            "clipBounds": true,
            "isMaster": true,
            "id": "frequencySelection",
            "isVisible": true,
            "layoutType": kony.flex.FLOW_HORIZONTAL,
            "left": "0dp",
            "isModalContainer": false,
            "preShow": function(eventobject) {
                controller.AS_FlexContainer_jbced4acd60546a6a3f8757e8eef1055(eventobject);
            },
            "skin": "sknFlxBgF8F9FABrE1E5EER3px",
            "top": "0dp",
            "width": "100%"
        }, controller.args[0], "frequencySelection"), extendConfig({}, controller.args[1], "frequencySelection"), extendConfig({}, controller.args[2], "frequencySelection"));
        frequencySelection.setDefaultUnit(kony.flex.DP);
        var flxFrequencyColumn1 = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
            "bottom": "20dp",
            "clipBounds": true,
            "id": "flxFrequencyColumn1",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "left": "15dp",
            "isModalContainer": false,
            "skin": "slFbox",
            "top": "20dp",
            "width": "30%",
            "zIndex": 1
        }, controller.args[0], "flxFrequencyColumn1"), extendConfig({}, controller.args[1], "flxFrequencyColumn1"), extendConfig({}, controller.args[2], "flxFrequencyColumn1"));
        flxFrequencyColumn1.setDefaultUnit(kony.flex.DP);
        var lblFrequencyHeader1 = new kony.ui.Label(extendConfig({
            "id": "lblFrequencyHeader1",
            "isVisible": true,
            "left": "0dp",
            "skin": "sknlblLatoReg485c7513px",
            "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmMFAScenarios.Frequency\")",
            "top": "0dp",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, controller.args[0], "lblFrequencyHeader1"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblFrequencyHeader1"), extendConfig({}, controller.args[2], "lblFrequencyHeader1"));
        var lstBoxFrequencyCol1 = new kony.ui.ListBox(extendConfig({
            "focusSkin": "defListBoxFocus",
            "height": "40dp",
            "id": "lstBoxFrequencyCol1",
            "isVisible": true,
            "left": "0dp",
            "masterData": [
                ["SELECT", "Select a Frequency"],
                ["MONTH", "Month"],
                ["WEEKLY", "Week"],
                ["DAY", "Day"],
                ["NONE", "None"]
            ],
            "right": "1dp",
            "selectedKey": "SELECT",
            "skin": "sknLbxborderd7d9e03pxradius",
            "top": "28dp",
            "zIndex": 1
        }, controller.args[0], "lstBoxFrequencyCol1"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [3, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lstBoxFrequencyCol1"), extendConfig({
            "multiSelect": false
        }, controller.args[2], "lstBoxFrequencyCol1"));
        var flxInlineError1 = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
            "clipBounds": true,
            "id": "flxInlineError1",
            "isVisible": false,
            "layoutType": kony.flex.FLOW_HORIZONTAL,
            "left": "0dp",
            "isModalContainer": false,
            "right": "0dp",
            "skin": "slFbox",
            "top": "73dp",
            "width": "95%"
        }, controller.args[0], "flxInlineError1"), extendConfig({}, controller.args[1], "flxInlineError1"), extendConfig({}, controller.args[2], "flxInlineError1"));
        flxInlineError1.setDefaultUnit(kony.flex.DP);
        var lblErrorIcon1 = new kony.ui.Label(extendConfig({
            "id": "lblErrorIcon1",
            "isVisible": true,
            "left": "0dp",
            "skin": "sknFontIconError",
            "text": "",
            "top": "1dp",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, controller.args[0], "lblErrorIcon1"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblErrorIcon1"), extendConfig({}, controller.args[2], "lblErrorIcon1"));
        var lblErrorText1 = new kony.ui.Label(extendConfig({
            "id": "lblErrorText1",
            "isVisible": true,
            "left": "5dp",
            "skin": "sknlblError",
            "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmAlertsManagement.PleaseSelectFrequency\")",
            "top": "0dp",
            "width": "85%",
            "zIndex": 1
        }, controller.args[0], "lblErrorText1"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblErrorText1"), extendConfig({}, controller.args[2], "lblErrorText1"));
        flxInlineError1.add(lblErrorIcon1, lblErrorText1);
        flxFrequencyColumn1.add(lblFrequencyHeader1, lstBoxFrequencyCol1, flxInlineError1);
        var flxFrequencyColumn2 = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
            "bottom": "20dp",
            "clipBounds": true,
            "id": "flxFrequencyColumn2",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "left": "15dp",
            "isModalContainer": false,
            "skin": "slFbox",
            "top": "20dp",
            "width": "30%",
            "zIndex": 1
        }, controller.args[0], "flxFrequencyColumn2"), extendConfig({}, controller.args[1], "flxFrequencyColumn2"), extendConfig({}, controller.args[2], "flxFrequencyColumn2"));
        flxFrequencyColumn2.setDefaultUnit(kony.flex.DP);
        var lblFrequencyHeader2 = new kony.ui.Label(extendConfig({
            "id": "lblFrequencyHeader2",
            "isVisible": true,
            "left": "0dp",
            "skin": "sknlblLatoReg485c7513px",
            "text": "Date",
            "top": "0dp",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, controller.args[0], "lblFrequencyHeader2"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblFrequencyHeader2"), extendConfig({}, controller.args[2], "lblFrequencyHeader2"));
        var lstBoxFrequencyCol2 = new kony.ui.ListBox(extendConfig({
            "focusSkin": "defListBoxFocus",
            "height": "40dp",
            "id": "lstBoxFrequencyCol2",
            "isVisible": true,
            "left": "0dp",
            "masterData": [
                ["lb1", "Placeholder One"],
                ["lb2", "Placeholder Two"],
                ["lb3", "Placeholder Three"]
            ],
            "right": "1dp",
            "skin": "sknLbxborderd7d9e03pxradius",
            "top": "28dp",
            "zIndex": 1
        }, controller.args[0], "lstBoxFrequencyCol2"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [3, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lstBoxFrequencyCol2"), extendConfig({
            "multiSelect": false
        }, controller.args[2], "lstBoxFrequencyCol2"));
        var flxInlineError2 = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
            "clipBounds": true,
            "id": "flxInlineError2",
            "isVisible": false,
            "layoutType": kony.flex.FLOW_HORIZONTAL,
            "left": "0dp",
            "isModalContainer": false,
            "right": "0dp",
            "skin": "slFbox",
            "top": "75dp",
            "width": "95%"
        }, controller.args[0], "flxInlineError2"), extendConfig({}, controller.args[1], "flxInlineError2"), extendConfig({}, controller.args[2], "flxInlineError2"));
        flxInlineError2.setDefaultUnit(kony.flex.DP);
        var lblErrorIcon2 = new kony.ui.Label(extendConfig({
            "id": "lblErrorIcon2",
            "isVisible": true,
            "left": "0dp",
            "skin": "sknFontIconError",
            "text": "",
            "top": "1dp",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, controller.args[0], "lblErrorIcon2"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblErrorIcon2"), extendConfig({}, controller.args[2], "lblErrorIcon2"));
        var lblErrorText2 = new kony.ui.Label(extendConfig({
            "id": "lblErrorText2",
            "isVisible": true,
            "left": "5dp",
            "skin": "sknlblError",
            "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmAlertsManagement.PleaseSelectDate\")",
            "top": "0dp",
            "width": "85%",
            "zIndex": 1
        }, controller.args[0], "lblErrorText2"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblErrorText2"), extendConfig({}, controller.args[2], "lblErrorText2"));
        flxInlineError2.add(lblErrorIcon2, lblErrorText2);
        flxFrequencyColumn2.add(lblFrequencyHeader2, lstBoxFrequencyCol2, flxInlineError2);
        var flxFrequencyColumn3 = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
            "bottom": "20dp",
            "clipBounds": true,
            "id": "flxFrequencyColumn3",
            "isVisible": false,
            "layoutType": kony.flex.FREE_FORM,
            "left": "15dp",
            "isModalContainer": false,
            "skin": "slFbox",
            "top": "20dp",
            "width": "30%",
            "zIndex": 1
        }, controller.args[0], "flxFrequencyColumn3"), extendConfig({}, controller.args[1], "flxFrequencyColumn3"), extendConfig({}, controller.args[2], "flxFrequencyColumn3"));
        flxFrequencyColumn3.setDefaultUnit(kony.flex.DP);
        var lblFrequencyHeader3 = new kony.ui.Label(extendConfig({
            "id": "lblFrequencyHeader3",
            "isVisible": true,
            "left": "0dp",
            "skin": "sknlblLatoReg485c7513px",
            "text": "Day",
            "top": "0dp",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, controller.args[0], "lblFrequencyHeader3"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblFrequencyHeader3"), extendConfig({}, controller.args[2], "lblFrequencyHeader3"));
        var lstBoxFrequencyCol3 = new kony.ui.ListBox(extendConfig({
            "focusSkin": "defListBoxFocus",
            "height": "40dp",
            "id": "lstBoxFrequencyCol3",
            "isVisible": true,
            "left": "0dp",
            "masterData": [
                ["SELECT", "Select a Day"],
                ["MONDAY", "Monday"],
                ["TUESDAY", "Tuesday"],
                ["WEDNESDAY", "Wednesday"],
                ["THURSDAY", "Thursday"],
                ["FRIDAY", "Friday"],
                ["SATURDAY", "Saturday"],
                ["SUNDAY", "Sunday"]
            ],
            "right": "1dp",
            "selectedKey": "SELECT",
            "skin": "sknLbxborderd7d9e03pxradius",
            "top": "28dp",
            "zIndex": 1
        }, controller.args[0], "lstBoxFrequencyCol3"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [3, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lstBoxFrequencyCol3"), extendConfig({
            "multiSelect": false
        }, controller.args[2], "lstBoxFrequencyCol3"));
        var flxInlineError3 = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
            "clipBounds": true,
            "id": "flxInlineError3",
            "isVisible": false,
            "layoutType": kony.flex.FLOW_HORIZONTAL,
            "left": "0dp",
            "isModalContainer": false,
            "right": "0dp",
            "skin": "slFbox",
            "top": "73dp",
            "width": "95%"
        }, controller.args[0], "flxInlineError3"), extendConfig({}, controller.args[1], "flxInlineError3"), extendConfig({}, controller.args[2], "flxInlineError3"));
        flxInlineError3.setDefaultUnit(kony.flex.DP);
        var lblErrorIcon3 = new kony.ui.Label(extendConfig({
            "id": "lblErrorIcon3",
            "isVisible": true,
            "left": "0dp",
            "skin": "sknFontIconError",
            "text": "",
            "top": "1dp",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, controller.args[0], "lblErrorIcon3"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblErrorIcon3"), extendConfig({}, controller.args[2], "lblErrorIcon3"));
        var lblErrorText3 = new kony.ui.Label(extendConfig({
            "id": "lblErrorText3",
            "isVisible": true,
            "left": "5dp",
            "skin": "sknlblError",
            "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmAlertsManagement.PleaseSelectDay\")",
            "top": "0dp",
            "width": "85%",
            "zIndex": 1
        }, controller.args[0], "lblErrorText3"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblErrorText3"), extendConfig({}, controller.args[2], "lblErrorText3"));
        flxInlineError3.add(lblErrorIcon3, lblErrorText3);
        flxFrequencyColumn3.add(lblFrequencyHeader3, lstBoxFrequencyCol3, flxInlineError3);
        var flxFrequencyColumn4 = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
            "bottom": "20dp",
            "clipBounds": true,
            "id": "flxFrequencyColumn4",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "left": "15dp",
            "isModalContainer": false,
            "skin": "slFbox",
            "top": "20dp",
            "width": "30%",
            "zIndex": 1
        }, controller.args[0], "flxFrequencyColumn4"), extendConfig({}, controller.args[1], "flxFrequencyColumn4"), extendConfig({}, controller.args[2], "flxFrequencyColumn4"));
        flxFrequencyColumn4.setDefaultUnit(kony.flex.DP);
        var lblFrequencyHeader4 = new kony.ui.Label(extendConfig({
            "id": "lblFrequencyHeader4",
            "isVisible": true,
            "left": "0dp",
            "skin": "sknlblLatoReg485c7513px",
            "text": "Time",
            "top": "0dp",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, controller.args[0], "lblFrequencyHeader4"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblFrequencyHeader4"), extendConfig({}, controller.args[2], "lblFrequencyHeader4"));
        var flxFrequencyTime = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
            "clipBounds": true,
            "id": "flxFrequencyTime",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "left": "0dp",
            "isModalContainer": false,
            "skin": "sknFlxbgFFFFFFbdrD7D9E0rd3px",
            "top": "28dp",
            "width": "214dp"
        }, controller.args[0], "flxFrequencyTime"), extendConfig({}, controller.args[1], "flxFrequencyTime"), extendConfig({}, controller.args[2], "flxFrequencyTime"));
        flxFrequencyTime.setDefaultUnit(kony.flex.DP);
        var timePicker = new com.adminConsole.common.timePicker(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
            "id": "timePicker",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "left": "0dp",
            "masterType": constants.MASTER_TYPE_DEFAULT,
            "isModalContainer": false,
            "skin": "CopyslFbox0b6ab2ac21ea948",
            "top": "0dp",
            "width": "214dp",
            "overrides": {
                "flxOuterBorder": {
                    "top": "0dp",
                    "zIndex": 2
                },
                "lstbxAMPM": {
                    "left": "146px",
                    "top": "0dp",
                    "width": "66px"
                },
                "timePicker": {
                    "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                    "top": "0dp",
                    "width": "214dp"
                }
            }
        }, controller.args[0], "timePicker"), extendConfig({
            "overrides": {}
        }, controller.args[1], "timePicker"), extendConfig({
            "overrides": {}
        }, controller.args[2], "timePicker"));
        flxFrequencyTime.add(timePicker);
        var flxInlineError4 = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
            "clipBounds": true,
            "id": "flxInlineError4",
            "isVisible": false,
            "layoutType": kony.flex.FLOW_HORIZONTAL,
            "left": "0dp",
            "isModalContainer": false,
            "right": "0dp",
            "skin": "slFbox",
            "top": "73dp",
            "width": "95%"
        }, controller.args[0], "flxInlineError4"), extendConfig({}, controller.args[1], "flxInlineError4"), extendConfig({}, controller.args[2], "flxInlineError4"));
        flxInlineError4.setDefaultUnit(kony.flex.DP);
        var lblErrorIcon4 = new kony.ui.Label(extendConfig({
            "id": "lblErrorIcon4",
            "isVisible": true,
            "left": "0dp",
            "skin": "sknFontIconError",
            "text": "",
            "top": "1dp",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, controller.args[0], "lblErrorIcon4"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblErrorIcon4"), extendConfig({}, controller.args[2], "lblErrorIcon4"));
        var lblErrorText4 = new kony.ui.Label(extendConfig({
            "id": "lblErrorText4",
            "isVisible": true,
            "left": "4dp",
            "skin": "sknlblError",
            "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmAlertsManagement.PleaseSelectTime\")",
            "top": "0dp",
            "width": "85%",
            "zIndex": 1
        }, controller.args[0], "lblErrorText4"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblErrorText4"), extendConfig({}, controller.args[2], "lblErrorText4"));
        flxInlineError4.add(lblErrorIcon4, lblErrorText4);
        flxFrequencyColumn4.add(lblFrequencyHeader4, flxFrequencyTime, flxInlineError4);
        frequencySelection.add(flxFrequencyColumn1, flxFrequencyColumn2, flxFrequencyColumn3, flxFrequencyColumn4);
        return frequencySelection;
    }
})