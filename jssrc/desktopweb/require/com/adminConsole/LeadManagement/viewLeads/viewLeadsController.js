define("com/adminConsole/LeadManagement/viewLeads/userviewLeadsController", function() {
    return {};
});
define("com/adminConsole/LeadManagement/viewLeads/viewLeadsControllerActions", {
    /*
          This is an auto generated file and any modifications to it may result in corruption of the action sequence.
        */
});
define("com/adminConsole/LeadManagement/viewLeads/viewLeadsController", ["com/adminConsole/LeadManagement/viewLeads/userviewLeadsController", "com/adminConsole/LeadManagement/viewLeads/viewLeadsControllerActions"], function() {
    var controller = require("com/adminConsole/LeadManagement/viewLeads/userviewLeadsController");
    var actions = require("com/adminConsole/LeadManagement/viewLeads/viewLeadsControllerActions");
    for (var key in actions) {
        controller[key] = actions[key];
    }
    return controller;
});
