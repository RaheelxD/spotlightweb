define(function() {
    return function(controller) {
        var ToolTip = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
            "clipBounds": true,
            "isMaster": true,
            "id": "ToolTip",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "left": "0dp",
            "isModalContainer": false,
            "skin": "slFbox",
            "top": "0dp",
            "width": "230px"
        }, controller.args[0], "ToolTip"), extendConfig({}, controller.args[1], "ToolTip"), extendConfig({}, controller.args[2], "ToolTip"));
        ToolTip.setDefaultUnit(kony.flex.DP);
        var lblarrow = new kony.ui.Label(extendConfig({
            "centerX": "80%",
            "height": "10dp",
            "id": "lblarrow",
            "isVisible": true,
            "left": "0px",
            "skin": "sknfonticonCustomersTooltip",
            "i18n_text": "kony.i18n.getLocalizedString(\"i18n.userwidgetmodel.fontIconUp\")",
            "top": "0px",
            "width": "17px",
            "zIndex": 20
        }, controller.args[0], "lblarrow"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblarrow"), extendConfig({}, controller.args[2], "lblarrow"));
        var flxToolTipMessage = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "centerX": "50%",
            "clipBounds": true,
            "height": "62px",
            "id": "flxToolTipMessage",
            "isVisible": true,
            "layoutType": kony.flex.FLOW_HORIZONTAL,
            "left": "0px",
            "isModalContainer": false,
            "skin": "sknflxTooltip",
            "top": "9px",
            "width": "210px",
            "zIndex": 1
        }, controller.args[0], "flxToolTipMessage"), extendConfig({}, controller.args[1], "flxToolTipMessage"), extendConfig({}, controller.args[2], "flxToolTipMessage"));
        flxToolTipMessage.setDefaultUnit(kony.flex.DP);
        var lblNoConcentToolTip = new kony.ui.Label(extendConfig({
            "height": "54px",
            "id": "lblNoConcentToolTip",
            "isVisible": true,
            "left": "10px",
            "skin": "sknlblCSRAssist",
            "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmCustomerManagementController.CSRAssistDisabled\")",
            "top": "3px",
            "width": "190px",
            "zIndex": 1
        }, controller.args[0], "lblNoConcentToolTip"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblNoConcentToolTip"), extendConfig({}, controller.args[2], "lblNoConcentToolTip"));
        flxToolTipMessage.add(lblNoConcentToolTip);
        var lblDownArrow = new kony.ui.Label(extendConfig({
            "centerX": "80%",
            "height": "10dp",
            "id": "lblDownArrow",
            "isVisible": false,
            "left": "0px",
            "skin": "sknfonticonCustomersTooltip",
            "text": "",
            "top": "70dp",
            "width": "17px",
            "zIndex": 20
        }, controller.args[0], "lblDownArrow"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblDownArrow"), extendConfig({}, controller.args[2], "lblDownArrow"));
        var imgUpArrow = new kony.ui.Image2(extendConfig({
            "centerX": "85%",
            "height": "12dp",
            "id": "imgUpArrow",
            "isVisible": false,
            "skin": "slImage",
            "src": "uparrow_2x.png",
            "top": "-1dp",
            "width": "15dp",
            "zIndex": 1
        }, controller.args[0], "imgUpArrow"), extendConfig({
            "imageScaleMode": constants.IMAGE_SCALE_MODE_FIT_TO_DIMENSIONS,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "imgUpArrow"), extendConfig({}, controller.args[2], "imgUpArrow"));
        ToolTip.add(lblarrow, flxToolTipMessage, lblDownArrow, imgUpArrow);
        return ToolTip;
    }
})