define(function() {
    return function(controller) {
        var MessageFilter = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
            "clipBounds": true,
            "isMaster": true,
            "id": "MessageFilter",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "left": "0dp",
            "isModalContainer": false,
            "skin": "slFbox",
            "top": "0dp",
            "width": "100%",
            "zIndex": 10
        }, controller.args[0], "MessageFilter"), extendConfig({}, controller.args[1], "MessageFilter"), extendConfig({}, controller.args[2], "MessageFilter"));
        MessageFilter.setDefaultUnit(kony.flex.DP);
        var flxBody = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
            "clipBounds": true,
            "id": "flxBody",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "left": "0px",
            "isModalContainer": false,
            "skin": "slFbox",
            "top": "0px",
            "width": "100%",
            "zIndex": 1
        }, controller.args[0], "flxBody"), extendConfig({}, controller.args[1], "flxBody"), extendConfig({}, controller.args[2], "flxBody"));
        flxBody.setDefaultUnit(kony.flex.DP);
        var flxMain = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
            "clipBounds": false,
            "id": "flxMain",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "left": "0dp",
            "isModalContainer": false,
            "skin": "sknf8f9faBorder100op",
            "top": "0dp",
            "width": "100%",
            "zIndex": 200
        }, controller.args[0], "flxMain"), extendConfig({}, controller.args[1], "flxMain"), extendConfig({}, controller.args[2], "flxMain"));
        flxMain.setDefaultUnit(kony.flex.DP);
        var flxSearchContainer = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "clipBounds": true,
            "height": "40px",
            "id": "flxSearchContainer",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "left": "20px",
            "isModalContainer": false,
            "right": "20px",
            "skin": "sknflxBgffffffBorderc1c9ceRadius30px",
            "top": "10px",
            "zIndex": 1
        }, controller.args[0], "flxSearchContainer"), extendConfig({}, controller.args[1], "flxSearchContainer"), extendConfig({}, controller.args[2], "flxSearchContainer"));
        flxSearchContainer.setDefaultUnit(kony.flex.DP);
        var imgSearchIcon = new kony.ui.Image2(extendConfig({
            "centerY": "50%",
            "height": "15px",
            "id": "imgSearchIcon",
            "isVisible": false,
            "left": "15px",
            "skin": "CopyslImage2",
            "src": "search_1x.png",
            "width": "15px",
            "zIndex": 1
        }, controller.args[0], "imgSearchIcon"), extendConfig({
            "imageScaleMode": constants.IMAGE_SCALE_MODE_FIT_TO_DIMENSIONS,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "imgSearchIcon"), extendConfig({}, controller.args[2], "imgSearchIcon"));
        var lblSearchIcon = new kony.ui.Label(extendConfig({
            "centerY": "50%",
            "height": "19dp",
            "id": "lblSearchIcon",
            "isVisible": true,
            "left": "14dp",
            "skin": "sknFontIconSearchImg20px",
            "i18n_text": "kony.i18n.getLocalizedString(\"i18n.userwidgetmodel.lblSearchIcon\")",
            "width": "19dp",
            "zIndex": 1
        }, controller.args[0], "lblSearchIcon"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblSearchIcon"), extendConfig({}, controller.args[2], "lblSearchIcon"));
        var tbxSearchBox = new kony.ui.TextBox2(extendConfig({
            "autoCapitalize": constants.TEXTBOX_AUTO_CAPITALIZE_NONE,
            "centerY": "50%",
            "height": "30px",
            "id": "tbxSearchBox",
            "isVisible": true,
            "keyBoardStyle": constants.TEXTBOX_KEY_BOARD_STYLE_DEFAULT,
            "left": "35dp",
            "onTouchStart": controller.AS_TextField_f90fbf5228c54b1bb97b994a46bbf289,
            "placeholder": "Search by Name, Username, Customer ID, Subject",
            "right": "35px",
            "secureTextEntry": false,
            "skin": "skntbxffffffNoBorderlato35475f14px",
            "textInputMode": constants.TEXTBOX_INPUT_MODE_ANY,
            "zIndex": 1
        }, controller.args[0], "tbxSearchBox"), extendConfig({
            "containerHeightMode": constants.TEXTBOX_FONT_METRICS_DRIVEN_HEIGHT,
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "tbxSearchBox"), extendConfig({
            "autoCorrect": false
        }, controller.args[2], "tbxSearchBox"));
        var flxClearSearchImage = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "centerY": "50%",
            "clipBounds": true,
            "height": "20px",
            "id": "flxClearSearchImage",
            "isVisible": false,
            "layoutType": kony.flex.FREE_FORM,
            "isModalContainer": false,
            "right": "15px",
            "skin": "sknCursor",
            "width": "17px",
            "zIndex": 1
        }, controller.args[0], "flxClearSearchImage"), extendConfig({}, controller.args[1], "flxClearSearchImage"), extendConfig({}, controller.args[2], "flxClearSearchImage"));
        flxClearSearchImage.setDefaultUnit(kony.flex.DP);
        var imgClearSearch = new kony.ui.Image2(extendConfig({
            "centerY": "50%",
            "height": "15px",
            "id": "imgClearSearch",
            "isVisible": false,
            "right": "15px",
            "skin": "CopyslImage2",
            "src": "close_blue.png",
            "width": "15px",
            "zIndex": 1
        }, controller.args[0], "imgClearSearch"), extendConfig({
            "imageScaleMode": constants.IMAGE_SCALE_MODE_FIT_TO_DIMENSIONS,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "imgClearSearch"), extendConfig({}, controller.args[2], "imgClearSearch"));
        var lblClearSearch = new kony.ui.Label(extendConfig({
            "centerY": "50%",
            "height": "17dp",
            "id": "lblClearSearch",
            "isVisible": true,
            "left": "0dp",
            "skin": "sknFontIconSearchCross16px",
            "i18n_text": "kony.i18n.getLocalizedString(\"i18n.userwidgetmodel.lblClearSearch\")",
            "width": "17dp",
            "zIndex": 1
        }, controller.args[0], "lblClearSearch"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblClearSearch"), extendConfig({}, controller.args[2], "lblClearSearch"));
        flxClearSearchImage.add(imgClearSearch, lblClearSearch);
        flxSearchContainer.add(imgSearchIcon, lblSearchIcon, tbxSearchBox, flxClearSearchImage);
        var lblSeparator2 = new kony.ui.Label(extendConfig({
            "height": "1px",
            "id": "lblSeparator2",
            "isVisible": true,
            "left": "0px",
            "skin": "sknlblSeperator",
            "i18n_text": "kony.i18n.getLocalizedString(\"i18n.userwidgetmodel.lblCustHeaderSeperator\")",
            "top": "145dp",
            "width": "100%",
            "zIndex": 2
        }, controller.args[0], "lblSeparator2"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblSeparator2"), extendConfig({}, controller.args[2], "lblSeparator2"));
        var flxSearchFilters = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "clipBounds": false,
            "height": "70px",
            "id": "flxSearchFilters",
            "isVisible": true,
            "layoutType": kony.flex.FLOW_HORIZONTAL,
            "left": "20px",
            "isModalContainer": false,
            "right": "20px",
            "skin": "slFbox",
            "top": "65px",
            "zIndex": 40
        }, controller.args[0], "flxSearchFilters"), extendConfig({}, controller.args[1], "flxSearchFilters"), extendConfig({}, controller.args[2], "flxSearchFilters"));
        flxSearchFilters.setDefaultUnit(kony.flex.DP);
        var flxColumn1 = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
            "clipBounds": false,
            "id": "flxColumn1",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "left": "0dp",
            "isModalContainer": false,
            "skin": "slFbox",
            "top": "1dp",
            "width": "22%",
            "zIndex": 1
        }, controller.args[0], "flxColumn1"), extendConfig({}, controller.args[1], "flxColumn1"), extendConfig({
            "hoverSkin": "sknCursor"
        }, controller.args[2], "flxColumn1"));
        flxColumn1.setDefaultUnit(kony.flex.DP);
        var lblCol1 = new kony.ui.Label(extendConfig({
            "id": "lblCol1",
            "isVisible": true,
            "left": "0dp",
            "skin": "sknlblLatoRegular484b5213px",
            "i18n_text": "kony.i18n.getLocalizedString(\"i18n.userwidgetmodel.lblCol1\")",
            "top": "5px",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, controller.args[0], "lblCol1"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblCol1"), extendConfig({}, controller.args[2], "lblCol1"));
        var flxDropDown01 = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "clipBounds": false,
            "height": "40px",
            "id": "flxDropDown01",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "left": "0dp",
            "isModalContainer": false,
            "skin": "sknflxffffffop100f0c7192186904d",
            "top": "28dp",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, controller.args[0], "flxDropDown01"), extendConfig({}, controller.args[1], "flxDropDown01"), extendConfig({}, controller.args[2], "flxDropDown01"));
        flxDropDown01.setDefaultUnit(kony.flex.DP);
        var lblSelectedRows = new kony.ui.Label(extendConfig({
            "centerY": "50%",
            "id": "lblSelectedRows",
            "isVisible": true,
            "left": "0px",
            "skin": "sknlbl0h2ff0d6b13f947AD",
            "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmLogsController.Select_date\")",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 20
        }, controller.args[0], "lblSelectedRows"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [3, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblSelectedRows"), extendConfig({}, controller.args[2], "lblSelectedRows"));
        var flxSearchDownImg = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "centerY": "50%",
            "clipBounds": true,
            "height": "20px",
            "id": "flxSearchDownImg",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "isModalContainer": false,
            "right": "10px",
            "skin": "slFbox",
            "width": "20px",
            "zIndex": 1
        }, controller.args[0], "flxSearchDownImg"), extendConfig({}, controller.args[1], "flxSearchDownImg"), extendConfig({}, controller.args[2], "flxSearchDownImg"));
        flxSearchDownImg.setDefaultUnit(kony.flex.DP);
        var imgCalender = new kony.ui.Image2(extendConfig({
            "centerY": "50%",
            "height": "20px",
            "id": "imgCalender",
            "isVisible": false,
            "right": "0px",
            "skin": "CopyslImage2",
            "src": "calicon.png",
            "width": "20px",
            "zIndex": 2
        }, controller.args[0], "imgCalender"), extendConfig({
            "imageScaleMode": constants.IMAGE_SCALE_MODE_FIT_TO_DIMENSIONS,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "imgCalender"), extendConfig({}, controller.args[2], "imgCalender"));
        var lblCalender = new kony.ui.Label(extendConfig({
            "centerY": "50%",
            "id": "lblCalender",
            "isVisible": true,
            "left": "0dp",
            "skin": "sknIcon20px",
            "i18n_text": "kony.i18n.getLocalizedString(\"i18n.userwidgetmodel.lblCalender\")",
            "top": "7dp",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 2
        }, controller.args[0], "lblCalender"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblCalender"), extendConfig({}, controller.args[2], "lblCalender"));
        flxSearchDownImg.add(imgCalender, lblCalender);
        flxDropDown01.add(lblSelectedRows, flxSearchDownImg);
        flxColumn1.add(lblCol1, flxDropDown01);
        var flxColumn2 = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
            "clipBounds": false,
            "id": "flxColumn2",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "left": "1%",
            "isModalContainer": false,
            "skin": "slFbox",
            "top": "0dp",
            "width": "22%",
            "zIndex": 1
        }, controller.args[0], "flxColumn2"), extendConfig({}, controller.args[1], "flxColumn2"), extendConfig({}, controller.args[2], "flxColumn2"));
        flxColumn2.setDefaultUnit(kony.flex.DP);
        var lblCol2 = new kony.ui.Label(extendConfig({
            "id": "lblCol2",
            "isVisible": true,
            "left": "0dp",
            "skin": "sknlblLatoRegular484b5213px",
            "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmServiceManagement.lblCategory\")",
            "top": "5px",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, controller.args[0], "lblCol2"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblCol2"), extendConfig({}, controller.args[2], "lblCol2"));
        var ListBoxCategory = new kony.ui.ListBox(extendConfig({
            "focusSkin": "sknlstbx13abebHover",
            "height": "40px",
            "id": "ListBoxCategory",
            "isVisible": true,
            "left": "0dp",
            "masterData": [
                ["Select Category", "Select Category"],
                ["General Banking", "General Banking"],
                ["Deposits", "Deposits"],
                ["Mobile Banking", "Mobile Banking"],
                ["ATM", "ATM"],
                ["Loans", "Loans"]
            ],
            "selectedKey": "Select Category",
            "skin": "sknlstbxNormal0f9abd8e88aa64a",
            "top": "28dp",
            "width": "100%",
            "zIndex": 1
        }, controller.args[0], "ListBoxCategory"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [3, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "ListBoxCategory"), extendConfig({
            "hoverSkin": "lstboxcursor",
            "multiSelect": false
        }, controller.args[2], "ListBoxCategory"));
        flxColumn2.add(lblCol2, ListBoxCategory);
        var flxColumn3 = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
            "clipBounds": true,
            "id": "flxColumn3",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "left": "1%",
            "isModalContainer": false,
            "skin": "slFbox",
            "top": "1dp",
            "width": "22%",
            "zIndex": 1
        }, controller.args[0], "flxColumn3"), extendConfig({}, controller.args[1], "flxColumn3"), extendConfig({}, controller.args[2], "flxColumn3"));
        flxColumn3.setDefaultUnit(kony.flex.DP);
        var lblCol3 = new kony.ui.Label(extendConfig({
            "id": "lblCol3",
            "isVisible": true,
            "left": "0dp",
            "skin": "sknlblLatoRegular484b5213px",
            "i18n_text": "kony.i18n.getLocalizedString(\"i18n.userwidgetmodel.lblCol3\")",
            "top": "5px",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, controller.args[0], "lblCol3"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblCol3"), extendConfig({}, controller.args[2], "lblCol3"));
        var flxAssignedToTxtcont = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "clipBounds": true,
            "height": "40dp",
            "id": "flxAssignedToTxtcont",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "left": "0dp",
            "isModalContainer": false,
            "skin": "sknflxffffffope1e5edcsr",
            "top": "28dp",
            "width": "100%"
        }, controller.args[0], "flxAssignedToTxtcont"), extendConfig({}, controller.args[1], "flxAssignedToTxtcont"), extendConfig({}, controller.args[2], "flxAssignedToTxtcont"));
        flxAssignedToTxtcont.setDefaultUnit(kony.flex.DP);
        var txtfldAssignTo = new kony.ui.TextBox2(extendConfig({
            "autoCapitalize": constants.TEXTBOX_AUTO_CAPITALIZE_NONE,
            "height": "37px",
            "id": "txtfldAssignTo",
            "isVisible": true,
            "keyBoardStyle": constants.TEXTBOX_KEY_BOARD_STYLE_DEFAULT,
            "left": "1dp",
            "placeholder": "Select CSR Name",
            "right": "40dp",
            "secureTextEntry": false,
            "skin": "sknTbxBgFFFFFFf485c7513pxNoBorder",
            "textInputMode": constants.TEXTBOX_INPUT_MODE_ANY,
            "top": "1dp",
            "zIndex": 2
        }, controller.args[0], "txtfldAssignTo"), extendConfig({
            "containerHeightMode": constants.TEXTBOX_FONT_METRICS_DRIVEN_HEIGHT,
            "contentAlignment": constants.CONTENT_ALIGN_TOP_LEFT,
            "padding": [3, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "txtfldAssignTo"), extendConfig({
            "autoCorrect": false
        }, controller.args[2], "txtfldAssignTo"));
        var flxClearAssignTo = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "clipBounds": true,
            "height": "40dp",
            "id": "flxClearAssignTo",
            "isVisible": false,
            "layoutType": kony.flex.FREE_FORM,
            "isModalContainer": false,
            "right": "5dp",
            "skin": "slFbox",
            "top": "0dp",
            "width": "30dp",
            "zIndex": 2
        }, controller.args[0], "flxClearAssignTo"), extendConfig({}, controller.args[1], "flxClearAssignTo"), extendConfig({
            "hoverSkin": "sknCursor"
        }, controller.args[2], "flxClearAssignTo"));
        flxClearAssignTo.setDefaultUnit(kony.flex.DP);
        var lblIconClearAssignedTo = new kony.ui.Label(extendConfig({
            "centerY": "50%",
            "id": "lblIconClearAssignedTo",
            "isVisible": true,
            "right": "10dp",
            "skin": "sknFontIconSearchCross16px",
            "i18n_text": "kony.i18n.getLocalizedString(\"i18n.userwidgetmodel.lblClearSearch\")",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, controller.args[0], "lblIconClearAssignedTo"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblIconClearAssignedTo"), extendConfig({}, controller.args[2], "lblIconClearAssignedTo"));
        flxClearAssignTo.add(lblIconClearAssignedTo);
        flxAssignedToTxtcont.add(txtfldAssignTo, flxClearAssignTo);
        flxColumn3.add(lblCol3, flxAssignedToTxtcont);
        var flxColumn4 = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
            "clipBounds": true,
            "id": "flxColumn4",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "left": "1%",
            "isModalContainer": false,
            "skin": "slFbox",
            "top": "1dp",
            "width": "22%",
            "zIndex": 1
        }, controller.args[0], "flxColumn4"), extendConfig({}, controller.args[1], "flxColumn4"), extendConfig({}, controller.args[2], "flxColumn4"));
        flxColumn4.setDefaultUnit(kony.flex.DP);
        var lblCol4 = new kony.ui.Label(extendConfig({
            "id": "lblCol4",
            "isVisible": true,
            "left": "0dp",
            "skin": "sknlblLatoRegular484b5213px",
            "i18n_text": "kony.i18n.getLocalizedString(\"i18n.userwidgetmodel.lblCol4\")",
            "top": "5px",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, controller.args[0], "lblCol4"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblCol4"), extendConfig({}, controller.args[2], "lblCol4"));
        var flxRepliedByTxtcont = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "clipBounds": true,
            "height": "40dp",
            "id": "flxRepliedByTxtcont",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "left": "0dp",
            "isModalContainer": false,
            "skin": "sknflxffffffope1e5edcsr",
            "top": "28dp",
            "width": "100%"
        }, controller.args[0], "flxRepliedByTxtcont"), extendConfig({}, controller.args[1], "flxRepliedByTxtcont"), extendConfig({}, controller.args[2], "flxRepliedByTxtcont"));
        flxRepliedByTxtcont.setDefaultUnit(kony.flex.DP);
        var txtfldRepliedby = new kony.ui.TextBox2(extendConfig({
            "autoCapitalize": constants.TEXTBOX_AUTO_CAPITALIZE_NONE,
            "height": "37px",
            "id": "txtfldRepliedby",
            "isVisible": true,
            "keyBoardStyle": constants.TEXTBOX_KEY_BOARD_STYLE_DEFAULT,
            "left": "1dp",
            "placeholder": "Select CSR Name",
            "right": "40dp",
            "secureTextEntry": false,
            "skin": "sknTbxBgFFFFFFf485c7513pxNoBorder",
            "textInputMode": constants.TEXTBOX_INPUT_MODE_ANY,
            "top": "1px",
            "zIndex": 2
        }, controller.args[0], "txtfldRepliedby"), extendConfig({
            "containerHeightMode": constants.TEXTBOX_FONT_METRICS_DRIVEN_HEIGHT,
            "contentAlignment": constants.CONTENT_ALIGN_TOP_LEFT,
            "padding": [3, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "txtfldRepliedby"), extendConfig({
            "autoCorrect": false
        }, controller.args[2], "txtfldRepliedby"));
        var flxClearRepliedBy = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "clipBounds": true,
            "height": "40dp",
            "id": "flxClearRepliedBy",
            "isVisible": false,
            "layoutType": kony.flex.FREE_FORM,
            "isModalContainer": false,
            "right": "5dp",
            "skin": "slFbox",
            "top": "0dp",
            "width": "30dp",
            "zIndex": 2
        }, controller.args[0], "flxClearRepliedBy"), extendConfig({}, controller.args[1], "flxClearRepliedBy"), extendConfig({
            "hoverSkin": "sknCursor"
        }, controller.args[2], "flxClearRepliedBy"));
        flxClearRepliedBy.setDefaultUnit(kony.flex.DP);
        var lblIconClearRepliedBy = new kony.ui.Label(extendConfig({
            "centerY": "50%",
            "id": "lblIconClearRepliedBy",
            "isVisible": true,
            "right": "10dp",
            "skin": "sknFontIconSearchCross16px",
            "i18n_text": "kony.i18n.getLocalizedString(\"i18n.userwidgetmodel.lblClearSearch\")",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, controller.args[0], "lblIconClearRepliedBy"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblIconClearRepliedBy"), extendConfig({}, controller.args[2], "lblIconClearRepliedBy"));
        flxClearRepliedBy.add(lblIconClearRepliedBy);
        flxRepliedByTxtcont.add(txtfldRepliedby, flxClearRepliedBy);
        flxColumn4.add(lblCol4, flxRepliedByTxtcont);
        var flxColumn5 = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
            "clipBounds": false,
            "id": "flxColumn5",
            "isVisible": false,
            "layoutType": kony.flex.FREE_FORM,
            "left": "1%",
            "isModalContainer": false,
            "skin": "slFbox",
            "top": "0dp",
            "width": "22%",
            "zIndex": 1
        }, controller.args[0], "flxColumn5"), extendConfig({}, controller.args[1], "flxColumn5"), extendConfig({}, controller.args[2], "flxColumn5"));
        flxColumn5.setDefaultUnit(kony.flex.DP);
        var lblCol5 = new kony.ui.Label(extendConfig({
            "id": "lblCol5",
            "isVisible": true,
            "left": "0dp",
            "skin": "sknlblLatoRegular484b5213px",
            "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmAlertsManagement.lblEditAlertStatus\")",
            "top": "5px",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, controller.args[0], "lblCol5"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblCol5"), extendConfig({}, controller.args[2], "lblCol5"));
        var ListBoxStatus = new kony.ui.ListBox(extendConfig({
            "focusSkin": "sknlstbx13abebHover",
            "height": "40px",
            "id": "ListBoxStatus",
            "isVisible": true,
            "left": "0dp",
            "masterData": [
                ["Select Status", "Select Status"],
                ["SID_OPEN", "New"],
                ["SID_INPROGRESS", "InProgress"]
            ],
            "selectedKey": "Select Status",
            "skin": "sknlstbxNormal0f9abd8e88aa64a",
            "top": "28dp",
            "width": "100%",
            "zIndex": 1
        }, controller.args[0], "ListBoxStatus"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [3, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "ListBoxStatus"), extendConfig({
            "hoverSkin": "lstboxcursor",
            "multiSelect": false
        }, controller.args[2], "ListBoxStatus"));
        flxColumn5.add(lblCol5, ListBoxStatus);
        var btnAdd = new kony.ui.Button(extendConfig({
            "bottom": "15px",
            "focusSkin": "sknbtnf7f7faLatoRegular12Px485c75Border1px485C75Radius20px",
            "height": "22px",
            "id": "btnAdd",
            "isVisible": true,
            "left": "2%",
            "skin": "sknbtnf7f7faLatoRegular12Px485c75Border1px485C75Radius20px",
            "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmCustomerManagement.btnApply\")",
            "top": "35px",
            "width": "64px",
            "zIndex": 1
        }, controller.args[0], "btnAdd"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_CENTER,
            "displayText": true,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "btnAdd"), extendConfig({
            "hoverSkin": "sknbtnffffffLatoRegular12Px485c75Border1px485C75Radius20px"
        }, controller.args[2], "btnAdd"));
        flxSearchFilters.add(flxColumn1, flxColumn2, flxColumn3, flxColumn4, flxColumn5, btnAdd);
        var lblSeparator = new kony.ui.Label(extendConfig({
            "centerX": "50%",
            "height": "1px",
            "id": "lblSeparator",
            "isVisible": true,
            "left": "20px",
            "skin": "sknlblSeperator",
            "i18n_text": "kony.i18n.getLocalizedString(\"i18n.userwidgetmodel.lblCustHeaderSeperator\")",
            "top": "60dp",
            "width": "100%",
            "zIndex": 2
        }, controller.args[0], "lblSeparator"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblSeparator"), extendConfig({}, controller.args[2], "lblSeparator"));
        flxMain.add(flxSearchContainer, lblSeparator2, flxSearchFilters, lblSeparator);
        var FlexResult = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
            "clipBounds": false,
            "id": "FlexResult",
            "isVisible": true,
            "layoutType": kony.flex.FLOW_VERTICAL,
            "left": "20px",
            "isModalContainer": false,
            "right": "0px",
            "skin": "slFbox",
            "top": "146px",
            "zIndex": 10
        }, controller.args[0], "FlexResult"), extendConfig({}, controller.args[1], "FlexResult"), extendConfig({}, controller.args[2], "FlexResult"));
        FlexResult.setDefaultUnit(kony.flex.DP);
        var flxMessagesHeader = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
            "clipBounds": true,
            "id": "flxMessagesHeader",
            "isVisible": true,
            "layoutType": kony.flex.FLOW_HORIZONTAL,
            "left": "0dp",
            "isModalContainer": false,
            "skin": "slFbox",
            "top": "0dp",
            "width": "100%"
        }, controller.args[0], "flxMessagesHeader"), extendConfig({}, controller.args[1], "flxMessagesHeader"), extendConfig({}, controller.args[2], "flxMessagesHeader"));
        flxMessagesHeader.setDefaultUnit(kony.flex.DP);
        var lblRowHeader1 = new kony.ui.Label(extendConfig({
            "id": "lblRowHeader1",
            "isVisible": true,
            "left": "0px",
            "skin": "sknLblLatoBold485c7513px",
            "i18n_text": "kony.i18n.getLocalizedString(\"i18n.userwidgetmodel.lblRowHeader1\")",
            "top": "20px",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 2
        }, controller.args[0], "lblRowHeader1"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblRowHeader1"), extendConfig({}, controller.args[2], "lblRowHeader1"));
        var flxAssignTo = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "clipBounds": false,
            "height": "25dp",
            "id": "flxAssignTo",
            "isVisible": false,
            "layoutType": kony.flex.FLOW_HORIZONTAL,
            "left": "15px",
            "isModalContainer": false,
            "skin": "sknFlxBGF7F7FAFC485C75R20pxBor485C75",
            "top": "15px",
            "width": "110dp",
            "zIndex": 111
        }, controller.args[0], "flxAssignTo"), extendConfig({}, controller.args[1], "flxAssignTo"), extendConfig({}, controller.args[2], "flxAssignTo"));
        flxAssignTo.setDefaultUnit(kony.flex.DP);
        var lblActions = new kony.ui.Label(extendConfig({
            "centerY": "50%",
            "id": "lblActions",
            "isVisible": true,
            "left": "15dp",
            "skin": "sknllbl485c75Lato13px",
            "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmCSR.lblPopUpMainMessage\")",
            "width": kony.flex.USE_PREFFERED_SIZE
        }, controller.args[0], "lblActions"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblActions"), extendConfig({}, controller.args[2], "lblActions"));
        var lblListIcon = new kony.ui.Label(extendConfig({
            "centerY": "50%",
            "height": "17dp",
            "id": "lblListIcon",
            "isVisible": true,
            "left": 10,
            "right": "15dp",
            "skin": "sknIcon17px",
            "text": "",
            "top": "0",
            "width": kony.flex.USE_PREFFERED_SIZE
        }, controller.args[0], "lblListIcon"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblListIcon"), extendConfig({}, controller.args[2], "lblListIcon"));
        flxAssignTo.add(lblActions, lblListIcon);
        flxMessagesHeader.add(lblRowHeader1, flxAssignTo);
        var lblSeparator3 = new kony.ui.Label(extendConfig({
            "height": "1px",
            "id": "lblSeparator3",
            "isVisible": true,
            "left": "0px",
            "skin": "sknlblSeperator",
            "i18n_text": "kony.i18n.getLocalizedString(\"i18n.userwidgetmodel.lblCustHeaderSeperator\")",
            "top": "20dp",
            "width": "100%",
            "zIndex": 2
        }, controller.args[0], "lblSeparator3"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblSeparator3"), extendConfig({}, controller.args[2], "lblSeparator3"));
        var flxScrollMessages = new kony.ui.FlexScrollContainer(extendConfig({
            "allowHorizontalBounce": false,
            "allowVerticalBounce": true,
            "bounces": true,
            "clipBounds": true,
            "enableScrolling": true,
            "height": "450dp",
            "horizontalScrollIndicator": true,
            "id": "flxScrollMessages",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "left": "0dp",
            "pagingEnabled": false,
            "scrollDirection": kony.flex.SCROLL_HORIZONTAL,
            "skin": "slFSbox",
            "top": "0dp",
            "verticalScrollIndicator": true,
            "width": "100%"
        }, controller.args[0], "flxScrollMessages"), extendConfig({}, controller.args[1], "flxScrollMessages"), extendConfig({}, controller.args[2], "flxScrollMessages"));
        flxScrollMessages.setDefaultUnit(kony.flex.DP);
        var flxMessagesResult = new kony.ui.FlexContainer(extendConfig({
            "clipBounds": false,
            "height": "100%",
            "id": "flxMessagesResult",
            "isVisible": true,
            "layoutType": kony.flex.FLOW_VERTICAL,
            "left": "0dp",
            "isModalContainer": false,
            "skin": "slFbox",
            "top": "0dp",
            "width": kony.flex.USE_PREFFERED_SIZE
        }, controller.args[0], "flxMessagesResult"), extendConfig({}, controller.args[1], "flxMessagesResult"), extendConfig({}, controller.args[2], "flxMessagesResult"));
        flxMessagesResult.setDefaultUnit(kony.flex.DP);
        var flxHeader = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "clipBounds": true,
            "height": "50px",
            "id": "flxHeader",
            "isVisible": true,
            "layoutType": kony.flex.FLOW_HORIZONTAL,
            "left": "0px",
            "isModalContainer": false,
            "right": "0px",
            "skin": "sknflxffffffop100",
            "top": "0px",
            "width": "1114dp"
        }, controller.args[0], "flxHeader"), extendConfig({}, controller.args[1], "flxHeader"), extendConfig({}, controller.args[2], "flxHeader"));
        flxHeader.setDefaultUnit(kony.flex.DP);
        var flxCheckbox = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
            "bottom": "20dp",
            "clipBounds": true,
            "id": "flxCheckbox",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "left": "20px",
            "isModalContainer": false,
            "skin": "sknCursor",
            "top": "20dp",
            "width": "30px",
            "zIndex": 2
        }, controller.args[0], "flxCheckbox"), extendConfig({}, controller.args[1], "flxCheckbox"), extendConfig({}, controller.args[2], "flxCheckbox"));
        flxCheckbox.setDefaultUnit(kony.flex.DP);
        var imgCheckBox = new kony.ui.Image2(extendConfig({
            "height": "12px",
            "id": "imgCheckBox",
            "isVisible": true,
            "left": "0px",
            "skin": "slImage",
            "src": "checkbox.png",
            "top": "0dp",
            "width": "12px",
            "zIndex": 2
        }, controller.args[0], "imgCheckBox"), extendConfig({
            "imageScaleMode": constants.IMAGE_SCALE_MODE_MAINTAIN_ASPECT_RATIO,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "imgCheckBox"), extendConfig({}, controller.args[2], "imgCheckBox"));
        flxCheckbox.add(imgCheckBox);
        var flxRequestIdHederName = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "clipBounds": true,
            "height": "100%",
            "id": "flxRequestIdHederName",
            "isVisible": true,
            "layoutType": kony.flex.FLOW_HORIZONTAL,
            "left": "20px",
            "isModalContainer": false,
            "skin": "slFbox",
            "top": "0dp",
            "width": "120dp",
            "zIndex": 1
        }, controller.args[0], "flxRequestIdHederName"), extendConfig({}, controller.args[1], "flxRequestIdHederName"), extendConfig({
            "hoverSkin": "sknCursor"
        }, controller.args[2], "flxRequestIdHederName"));
        flxRequestIdHederName.setDefaultUnit(kony.flex.DP);
        var lblRequestIdName = new kony.ui.Label(extendConfig({
            "centerY": "50%",
            "id": "lblRequestIdName",
            "isVisible": true,
            "left": "0",
            "skin": "sknlblLato696c7312px",
            "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmCustomerManagement.lblRequestNumber\")",
            "top": 0,
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, controller.args[0], "lblRequestIdName"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblRequestIdName"), extendConfig({
            "hoverSkin": "sknlblLato00000011px"
        }, controller.args[2], "lblRequestIdName"));
        var lblIconRequestIdSortName = new kony.ui.Label(extendConfig({
            "centerY": "50%",
            "height": "15dp",
            "id": "lblIconRequestIdSortName",
            "isVisible": true,
            "left": "0dp",
            "skin": "sknIcon15px",
            "i18n_text": "kony.i18n.getLocalizedString(\"i18n.userwidgetmodel.lblIconAssignedTo\")",
            "width": "15dp",
            "zIndex": 1
        }, controller.args[0], "lblIconRequestIdSortName"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblIconRequestIdSortName"), extendConfig({}, controller.args[2], "lblIconRequestIdSortName"));
        var imgRequestIdSortName = new kony.ui.Image2(extendConfig({
            "centerY": "50%",
            "height": "13dp",
            "id": "imgRequestIdSortName",
            "isVisible": false,
            "left": "7dp",
            "skin": "slImage",
            "src": "dropdownarrow.png",
            "top": "0dp",
            "width": "13dp",
            "zIndex": 10
        }, controller.args[0], "imgRequestIdSortName"), extendConfig({
            "imageScaleMode": constants.IMAGE_SCALE_MODE_MAINTAIN_ASPECT_RATIO,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "imgRequestIdSortName"), extendConfig({}, controller.args[2], "imgRequestIdSortName"));
        flxRequestIdHederName.add(lblRequestIdName, lblIconRequestIdSortName, imgRequestIdSortName);
        var flxCustomerId = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "clipBounds": true,
            "height": "100%",
            "id": "flxCustomerId",
            "isVisible": true,
            "layoutType": kony.flex.FLOW_HORIZONTAL,
            "left": "20dp",
            "isModalContainer": false,
            "skin": "slFbox",
            "top": "0dp",
            "width": "120dp",
            "zIndex": 1
        }, controller.args[0], "flxCustomerId"), extendConfig({}, controller.args[1], "flxCustomerId"), extendConfig({
            "hoverSkin": "sknCursor"
        }, controller.args[2], "flxCustomerId"));
        flxCustomerId.setDefaultUnit(kony.flex.DP);
        var lblCustHeaderId = new kony.ui.Label(extendConfig({
            "centerY": "50%",
            "id": "lblCustHeaderId",
            "isVisible": true,
            "left": "0px",
            "skin": "sknlblLato696c7312px",
            "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmCustomerManagementController.RetailCustomerId\")",
            "top": 0,
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, controller.args[0], "lblCustHeaderId"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblCustHeaderId"), extendConfig({
            "hoverSkin": "sknlblLato00000011px"
        }, controller.args[2], "lblCustHeaderId"));
        var lblIconCustSortId = new kony.ui.Label(extendConfig({
            "centerY": "50%",
            "height": "15dp",
            "id": "lblIconCustSortId",
            "isVisible": true,
            "left": "0dp",
            "skin": "sknIcon15px",
            "i18n_text": "kony.i18n.getLocalizedString(\"i18n.userwidgetmodel.lblIconAssignedTo\")",
            "width": "15dp",
            "zIndex": 1
        }, controller.args[0], "lblIconCustSortId"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblIconCustSortId"), extendConfig({}, controller.args[2], "lblIconCustSortId"));
        var imgCustSortId = new kony.ui.Image2(extendConfig({
            "centerY": "50%",
            "height": "13dp",
            "id": "imgCustSortId",
            "isVisible": false,
            "left": "7dp",
            "skin": "slImage",
            "src": "dropdownarrow.png",
            "top": "0dp",
            "width": "13dp",
            "zIndex": 1
        }, controller.args[0], "imgCustSortId"), extendConfig({
            "imageScaleMode": constants.IMAGE_SCALE_MODE_MAINTAIN_ASPECT_RATIO,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "imgCustSortId"), extendConfig({}, controller.args[2], "imgCustSortId"));
        flxCustomerId.add(lblCustHeaderId, lblIconCustSortId, imgCustSortId);
        var flxCustomername = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "clipBounds": true,
            "height": "100%",
            "id": "flxCustomername",
            "isVisible": true,
            "layoutType": kony.flex.FLOW_HORIZONTAL,
            "left": "20dp",
            "isModalContainer": false,
            "skin": "slFbox",
            "top": "0dp",
            "width": "150dp",
            "zIndex": 1
        }, controller.args[0], "flxCustomername"), extendConfig({}, controller.args[1], "flxCustomername"), extendConfig({
            "hoverSkin": "sknCursor"
        }, controller.args[2], "flxCustomername"));
        flxCustomername.setDefaultUnit(kony.flex.DP);
        var lblCustHeaderUsername = new kony.ui.Label(extendConfig({
            "centerY": "50%",
            "id": "lblCustHeaderUsername",
            "isVisible": true,
            "left": "0px",
            "skin": "sknlblLato696c7312px",
            "i18n_text": "kony.i18n.getLocalizedString(\"i18n.userwidgetmodel.lblCustHeaderUsername\")",
            "top": 0,
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, controller.args[0], "lblCustHeaderUsername"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblCustHeaderUsername"), extendConfig({
            "hoverSkin": "sknlblLato00000011px"
        }, controller.args[2], "lblCustHeaderUsername"));
        var lblIconCustSortUsername = new kony.ui.Label(extendConfig({
            "centerY": "50%",
            "height": "15dp",
            "id": "lblIconCustSortUsername",
            "isVisible": true,
            "left": "0dp",
            "skin": "sknIcon15px",
            "i18n_text": "kony.i18n.getLocalizedString(\"i18n.userwidgetmodel.lblIconAssignedTo\")",
            "width": "15dp",
            "zIndex": 1
        }, controller.args[0], "lblIconCustSortUsername"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblIconCustSortUsername"), extendConfig({}, controller.args[2], "lblIconCustSortUsername"));
        var imgCustSortUsername = new kony.ui.Image2(extendConfig({
            "centerY": "50%",
            "height": "13dp",
            "id": "imgCustSortUsername",
            "isVisible": false,
            "left": "7dp",
            "skin": "slImage",
            "src": "dropdownarrow.png",
            "top": "0dp",
            "width": "13dp",
            "zIndex": 1
        }, controller.args[0], "imgCustSortUsername"), extendConfig({
            "imageScaleMode": constants.IMAGE_SCALE_MODE_MAINTAIN_ASPECT_RATIO,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "imgCustSortUsername"), extendConfig({}, controller.args[2], "imgCustSortUsername"));
        flxCustomername.add(lblCustHeaderUsername, lblIconCustSortUsername, imgCustSortUsername);
        var flxSubject = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "clipBounds": true,
            "height": "100%",
            "id": "flxSubject",
            "isVisible": true,
            "layoutType": kony.flex.FLOW_HORIZONTAL,
            "left": "20dp",
            "isModalContainer": false,
            "skin": "slFbox",
            "top": "0dp",
            "width": "180dp",
            "zIndex": 1
        }, controller.args[0], "flxSubject"), extendConfig({}, controller.args[1], "flxSubject"), extendConfig({}, controller.args[2], "flxSubject"));
        flxSubject.setDefaultUnit(kony.flex.DP);
        var lblSubject = new kony.ui.Label(extendConfig({
            "centerY": "50%",
            "id": "lblSubject",
            "isVisible": true,
            "left": "0px",
            "skin": "sknlblLato696c7312px",
            "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmCustomerManagement.lblSubject\")",
            "top": 0,
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, controller.args[0], "lblSubject"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblSubject"), extendConfig({}, controller.args[2], "lblSubject"));
        var lblIconSubject = new kony.ui.Label(extendConfig({
            "centerY": "50%",
            "height": "15dp",
            "id": "lblIconSubject",
            "isVisible": true,
            "left": "0dp",
            "skin": "sknIcon15px",
            "i18n_text": "kony.i18n.getLocalizedString(\"i18n.userwidgetmodel.lblIconAssignedTo\")",
            "width": "15dp",
            "zIndex": 1
        }, controller.args[0], "lblIconSubject"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblIconSubject"), extendConfig({}, controller.args[2], "lblIconSubject"));
        var imgSubject = new kony.ui.Image2(extendConfig({
            "centerY": "50%",
            "height": "13dp",
            "id": "imgSubject",
            "isVisible": false,
            "left": "10dp",
            "skin": "slImage",
            "src": "dropdownarrow.png",
            "top": "0dp",
            "width": "13dp",
            "zIndex": 1
        }, controller.args[0], "imgSubject"), extendConfig({
            "imageScaleMode": constants.IMAGE_SCALE_MODE_MAINTAIN_ASPECT_RATIO,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "imgSubject"), extendConfig({}, controller.args[2], "imgSubject"));
        flxSubject.add(lblSubject, lblIconSubject, imgSubject);
        var flxCategory = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "clipBounds": true,
            "height": "100%",
            "id": "flxCategory",
            "isVisible": true,
            "layoutType": kony.flex.FLOW_HORIZONTAL,
            "left": "20dp",
            "isModalContainer": false,
            "skin": "sknCursor",
            "top": "0dp",
            "width": "110dp",
            "zIndex": 1
        }, controller.args[0], "flxCategory"), extendConfig({}, controller.args[1], "flxCategory"), extendConfig({}, controller.args[2], "flxCategory"));
        flxCategory.setDefaultUnit(kony.flex.DP);
        var lblCategory = new kony.ui.Label(extendConfig({
            "centerY": "50%",
            "id": "lblCategory",
            "isVisible": true,
            "left": "0px",
            "skin": "sknlblLato696c7312px",
            "i18n_text": "kony.i18n.getLocalizedString(\"i18n.Group.CATEGORY\")",
            "top": 0,
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, controller.args[0], "lblCategory"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblCategory"), extendConfig({
            "hoverSkin": "sknlblLato00000011px"
        }, controller.args[2], "lblCategory"));
        var lblIconCategory = new kony.ui.Label(extendConfig({
            "centerY": "50%",
            "height": "15dp",
            "id": "lblIconCategory",
            "isVisible": true,
            "left": "0dp",
            "skin": "sknIcon15px",
            "i18n_text": "kony.i18n.getLocalizedString(\"i18n.userwidgetmodel.lblIconAssignedTo\")",
            "width": "15dp",
            "zIndex": 1
        }, controller.args[0], "lblIconCategory"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblIconCategory"), extendConfig({}, controller.args[2], "lblIconCategory"));
        var imgCategory = new kony.ui.Image2(extendConfig({
            "centerY": "50%",
            "height": "13dp",
            "id": "imgCategory",
            "isVisible": false,
            "left": "7dp",
            "skin": "slImage",
            "src": "dropdownarrow.png",
            "top": "0dp",
            "width": "13dp",
            "zIndex": 1
        }, controller.args[0], "imgCategory"), extendConfig({
            "imageScaleMode": constants.IMAGE_SCALE_MODE_MAINTAIN_ASPECT_RATIO,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "imgCategory"), extendConfig({}, controller.args[2], "imgCategory"));
        flxCategory.add(lblCategory, lblIconCategory, imgCategory);
        var flxDate = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "clipBounds": true,
            "height": "100%",
            "id": "flxDate",
            "isVisible": true,
            "layoutType": kony.flex.FLOW_HORIZONTAL,
            "left": "20dp",
            "isModalContainer": false,
            "skin": "slFbox",
            "top": "0dp",
            "width": "80dp",
            "zIndex": 1
        }, controller.args[0], "flxDate"), extendConfig({}, controller.args[1], "flxDate"), extendConfig({
            "hoverSkin": "sknCursor"
        }, controller.args[2], "flxDate"));
        flxDate.setDefaultUnit(kony.flex.DP);
        var lblDate = new kony.ui.Label(extendConfig({
            "centerY": "50%",
            "id": "lblDate",
            "isVisible": true,
            "left": "0px",
            "skin": "sknlblLato696c7312px",
            "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmCustomerManagement.lblRequestDate\")",
            "top": 0,
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, controller.args[0], "lblDate"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblDate"), extendConfig({
            "hoverSkin": "sknlblLato00000011px"
        }, controller.args[2], "lblDate"));
        var lblIconDate = new kony.ui.Label(extendConfig({
            "centerY": "50%",
            "height": "15dp",
            "id": "lblIconDate",
            "isVisible": true,
            "left": "0dp",
            "skin": "sknIcon15px",
            "i18n_text": "kony.i18n.getLocalizedString(\"i18n.userwidgetmodel.lblIconAssignedTo\")",
            "width": "15dp",
            "zIndex": 1
        }, controller.args[0], "lblIconDate"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblIconDate"), extendConfig({}, controller.args[2], "lblIconDate"));
        var imgDate = new kony.ui.Image2(extendConfig({
            "centerY": "50%",
            "height": "13dp",
            "id": "imgDate",
            "isVisible": false,
            "left": "7dp",
            "skin": "slImage",
            "src": "dropdownarrow.png",
            "top": "0dp",
            "width": "13dp",
            "zIndex": 1
        }, controller.args[0], "imgDate"), extendConfig({
            "imageScaleMode": constants.IMAGE_SCALE_MODE_MAINTAIN_ASPECT_RATIO,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "imgDate"), extendConfig({}, controller.args[2], "imgDate"));
        flxDate.add(lblDate, lblIconDate, imgDate);
        var flxAssignedTo = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "clipBounds": true,
            "height": "100%",
            "id": "flxAssignedTo",
            "isVisible": true,
            "layoutType": kony.flex.FLOW_HORIZONTAL,
            "left": "20dp",
            "isModalContainer": false,
            "skin": "sknCursor",
            "top": "0dp",
            "width": "100dp",
            "zIndex": 1
        }, controller.args[0], "flxAssignedTo"), extendConfig({}, controller.args[1], "flxAssignedTo"), extendConfig({
            "hoverSkin": "sknCursor"
        }, controller.args[2], "flxAssignedTo"));
        flxAssignedTo.setDefaultUnit(kony.flex.DP);
        var lblAssignedTo = new kony.ui.Label(extendConfig({
            "centerY": "50%",
            "id": "lblAssignedTo",
            "isVisible": true,
            "left": "0px",
            "skin": "sknlblLato696c7312px",
            "i18n_text": "kony.i18n.getLocalizedString(\"i18n.userwidgetmodel.lblAssignedTo\")",
            "top": 0,
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, controller.args[0], "lblAssignedTo"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblAssignedTo"), extendConfig({
            "hoverSkin": "sknlblLato00000011px"
        }, controller.args[2], "lblAssignedTo"));
        var lblIconAssignedTo = new kony.ui.Label(extendConfig({
            "centerY": "50%",
            "height": "15dp",
            "id": "lblIconAssignedTo",
            "isVisible": true,
            "left": "0dp",
            "skin": "sknIcon15px",
            "i18n_text": "kony.i18n.getLocalizedString(\"i18n.userwidgetmodel.lblIconAssignedTo\")",
            "width": "15dp",
            "zIndex": 1
        }, controller.args[0], "lblIconAssignedTo"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblIconAssignedTo"), extendConfig({}, controller.args[2], "lblIconAssignedTo"));
        var imgAssignedTo = new kony.ui.Image2(extendConfig({
            "centerY": "50%",
            "height": "13dp",
            "id": "imgAssignedTo",
            "isVisible": false,
            "left": "7dp",
            "skin": "slImage",
            "src": "dropdownarrow.png",
            "top": "0dp",
            "width": "13dp",
            "zIndex": 1
        }, controller.args[0], "imgAssignedTo"), extendConfig({
            "imageScaleMode": constants.IMAGE_SCALE_MODE_MAINTAIN_ASPECT_RATIO,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "imgAssignedTo"), extendConfig({}, controller.args[2], "imgAssignedTo"));
        flxAssignedTo.add(lblAssignedTo, lblIconAssignedTo, imgAssignedTo);
        flxHeader.add(flxCheckbox, flxRequestIdHederName, flxCustomerId, flxCustomername, flxSubject, flxCategory, flxDate, flxAssignedTo);
        var lblCustHeaderSeperator = new kony.ui.Label(extendConfig({
            "bottom": 0,
            "height": "1px",
            "id": "lblCustHeaderSeperator",
            "isVisible": true,
            "left": "0px",
            "right": "20px",
            "skin": "sknlblSeperator",
            "i18n_text": "kony.i18n.getLocalizedString(\"i18n.userwidgetmodel.lblCustHeaderSeperator\")",
            "top": 0,
            "width": "1094dp",
            "zIndex": 1
        }, controller.args[0], "lblCustHeaderSeperator"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblCustHeaderSeperator"), extendConfig({}, controller.args[2], "lblCustHeaderSeperator"));
        var flxSegSearchResult = new kony.ui.FlexScrollContainer(extendConfig({
            "allowHorizontalBounce": false,
            "allowVerticalBounce": true,
            "bounces": true,
            "clipBounds": true,
            "enableScrolling": true,
            "height": "400px",
            "horizontalScrollIndicator": true,
            "id": "flxSegSearchResult",
            "isVisible": true,
            "layoutType": kony.flex.FLOW_VERTICAL,
            "left": "0dp",
            "pagingEnabled": false,
            "scrollDirection": kony.flex.SCROLL_VERTICAL,
            "skin": "slFSbox",
            "top": "0dp",
            "verticalScrollIndicator": true,
            "width": "1114dp"
        }, controller.args[0], "flxSegSearchResult"), extendConfig({}, controller.args[1], "flxSegSearchResult"), extendConfig({}, controller.args[2], "flxSegSearchResult"));
        flxSegSearchResult.setDefaultUnit(kony.flex.DP);
        var SegSearchResult = new kony.ui.SegmentedUI2(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
            "data": [{
                "imgCheckBoxMsg": "checkbox.png",
                "lblAssignto": "Edward Shwartz",
                "lblCategory": "General banking",
                "lblCustomerId": "John Doe(9), ",
                "lblCustomerName": "John Doe(9), ",
                "lblDate": "12.10.2017",
                "lblDraft": "Draft",
                "lblIconOptions": "",
                "lblReply": "",
                "lblRequestID": " RI333456",
                "lblSeparator": "'",
                "lblSubject": "Lorem ipsum dolor sitconsectetu.. (9).orem ipsum dolor sitconsectetu.. (orem ipsum dolor sitconsectetu.. (9)."
            }, {
                "imgCheckBoxMsg": "checkbox.png",
                "lblAssignto": "Edward Shwartz",
                "lblCategory": "General banking",
                "lblCustomerId": "John Doe(9), ",
                "lblCustomerName": "John Doe(9), ",
                "lblDate": "12.10.2017",
                "lblDraft": "Draft",
                "lblIconOptions": "",
                "lblReply": "",
                "lblRequestID": " RI333456",
                "lblSeparator": "'",
                "lblSubject": "Lorem ipsum dolor sitconsectetu.. (9).orem ipsum dolor sitconsectetu.. (orem ipsum dolor sitconsectetu.. (9)."
            }, {
                "imgCheckBoxMsg": "checkbox.png",
                "lblAssignto": "Edward Shwartz",
                "lblCategory": "General banking",
                "lblCustomerId": "John Doe(9), ",
                "lblCustomerName": "John Doe(9), ",
                "lblDate": "12.10.2017",
                "lblDraft": "Draft",
                "lblIconOptions": "",
                "lblReply": "",
                "lblRequestID": " RI333456",
                "lblSeparator": "'",
                "lblSubject": "Lorem ipsum dolor sitconsectetu.. (9).orem ipsum dolor sitconsectetu.. (orem ipsum dolor sitconsectetu.. (9)."
            }],
            "groupCells": false,
            "id": "SegSearchResult",
            "isVisible": true,
            "left": "0dp",
            "needPageIndicator": true,
            "pageOffDotImage": "pageoffdot.png",
            "pageOnDotImage": "pageondot.png",
            "retainSelection": false,
            "rowFocusSkin": "seg2Focus",
            "rowSkin": "seg2Normal",
            "rowTemplate": "flxCSRMsgList",
            "sectionHeaderSkin": "sliPhoneSegmentHeader",
            "selectionBehavior": constants.SEGUI_MULTI_SELECT_BEHAVIOR,
            "selectionBehaviorConfig": {
                "imageIdentifier": "imgCheckBoxMsg",
                "selectedStateImage": "checkboxselected.png",
                "unselectedStateImage": "checkboxnormal.png"
            },
            "separatorRequired": false,
            "showScrollbars": false,
            "top": "0dp",
            "viewType": constants.SEGUI_VIEW_TYPE_TABLEVIEW,
            "widgetDataMap": {
                "flxCSRMsgList": "flxCSRMsgList",
                "flxCheckboxMsg": "flxCheckboxMsg",
                "flxMsgSegMain": "flxMsgSegMain",
                "flxOptions": "flxOptions",
                "flxReqId": "flxReqId",
                "flxSegMain": "flxSegMain",
                "flxUserName": "flxUserName",
                "imgCheckBoxMsg": "imgCheckBoxMsg",
                "lblAssignto": "lblAssignto",
                "lblCategory": "lblCategory",
                "lblCustomerId": "lblCustomerId",
                "lblCustomerName": "lblCustomerName",
                "lblDate": "lblDate",
                "lblDraft": "lblDraft",
                "lblIconOptions": "lblIconOptions",
                "lblReply": "lblReply",
                "lblRequestID": "lblRequestID",
                "lblSeparator": "lblSeparator",
                "lblSubject": "lblSubject"
            },
            "width": "100%",
            "zIndex": 10
        }, controller.args[0], "SegSearchResult"), extendConfig({
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "SegSearchResult"), extendConfig({}, controller.args[2], "SegSearchResult"));
        flxSegSearchResult.add(SegSearchResult);
        var flxPagination = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "bottom": "0px",
            "clipBounds": true,
            "height": "70px",
            "id": "flxPagination",
            "isVisible": false,
            "layoutType": kony.flex.FREE_FORM,
            "left": "0dp",
            "isModalContainer": false,
            "skin": "slFbox",
            "top": "0px",
            "width": "100%",
            "zIndex": 1
        }, controller.args[0], "flxPagination"), extendConfig({}, controller.args[1], "flxPagination"), extendConfig({}, controller.args[2], "flxPagination"));
        flxPagination.setDefaultUnit(kony.flex.DP);
        var pagination = new com.adminConsole.common.pagination1(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
            "id": "pagination",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "left": "0dp",
            "masterType": constants.MASTER_TYPE_DEFAULT,
            "isModalContainer": false,
            "skin": "slFbox",
            "top": "0dp",
            "width": "100%",
            "overrides": {
                "pagination1": {
                    "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                    "bottom": "viz.val_cleared",
                    "centerX": "viz.val_cleared",
                    "centerY": "viz.val_cleared",
                    "left": "0dp",
                    "maxHeight": "viz.val_cleared",
                    "maxWidth": "viz.val_cleared",
                    "minHeight": "viz.val_cleared",
                    "minWidth": "viz.val_cleared",
                    "right": "viz.val_cleared",
                    "top": "0dp",
                    "width": "100%"
                }
            }
        }, controller.args[0], "pagination"), extendConfig({
            "overrides": {}
        }, controller.args[1], "pagination"), extendConfig({
            "overrides": {}
        }, controller.args[2], "pagination"));
        flxPagination.add(pagination);
        flxMessagesResult.add(flxHeader, lblCustHeaderSeperator, flxSegSearchResult, flxPagination);
        flxScrollMessages.add(flxMessagesResult);
        FlexResult.add(flxMessagesHeader, lblSeparator3, flxScrollMessages);
        var FlexNoResult = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "clipBounds": false,
            "height": "500px",
            "id": "FlexNoResult",
            "isVisible": false,
            "layoutType": kony.flex.FREE_FORM,
            "left": "20dp",
            "isModalContainer": false,
            "right": "20px",
            "skin": "slFbox",
            "top": "146dp",
            "zIndex": 10
        }, controller.args[0], "FlexNoResult"), extendConfig({}, controller.args[1], "FlexNoResult"), extendConfig({}, controller.args[2], "FlexNoResult"));
        FlexNoResult.setDefaultUnit(kony.flex.DP);
        var richtextNoResult = new kony.ui.RichText(extendConfig({
            "centerX": "50%",
            "centerY": "50%",
            "id": "richtextNoResult",
            "isVisible": true,
            "left": "0dp",
            "skin": "sknRtxLatoReg84939e13px",
            "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmCSRController.prop773\")",
            "width": "100%",
            "zIndex": 1
        }, controller.args[0], "richtextNoResult"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_CENTER,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "richtextNoResult"), extendConfig({}, controller.args[2], "richtextNoResult"));
        FlexNoResult.add(richtextNoResult);
        var FlexContextMenu = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
            "clipBounds": true,
            "id": "FlexContextMenu",
            "isVisible": false,
            "layoutType": kony.flex.FREE_FORM,
            "isModalContainer": false,
            "right": "15px",
            "skin": "slFbox",
            "top": "300dp",
            "width": "200px",
            "zIndex": 10
        }, controller.args[0], "FlexContextMenu"), extendConfig({}, controller.args[1], "FlexContextMenu"), extendConfig({}, controller.args[2], "FlexContextMenu"));
        FlexContextMenu.setDefaultUnit(kony.flex.DP);
        var contextualMenu = new com.adminConsole.common.contextualMenu1(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
            "id": "contextualMenu",
            "isVisible": true,
            "layoutType": kony.flex.FLOW_VERTICAL,
            "left": "0dp",
            "masterType": constants.MASTER_TYPE_DEFAULT,
            "isModalContainer": false,
            "skin": "slFbox",
            "top": "0dp",
            "width": "100%",
            "overrides": {
                "contextualMenu1": {
                    "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                    "bottom": "viz.val_cleared",
                    "centerX": "viz.val_cleared",
                    "centerY": "viz.val_cleared",
                    "left": "0dp",
                    "maxHeight": "viz.val_cleared",
                    "maxWidth": "viz.val_cleared",
                    "minHeight": "viz.val_cleared",
                    "minWidth": "viz.val_cleared",
                    "right": "viz.val_cleared",
                    "top": "0dp",
                    "width": "100%"
                },
                "imgDownArrow": {
                    "src": "downarrow_2x.png"
                },
                "imgOption1": {
                    "src": "imagedrag.png"
                },
                "imgOption2": {
                    "src": "edit2x.png"
                },
                "imgOption3": {
                    "src": "imagedrag.png"
                },
                "imgOption4": {
                    "src": "deactive_2x.png"
                },
                "imgUpArrow": {
                    "src": "uparrow_2x.png"
                }
            }
        }, controller.args[0], "contextualMenu"), extendConfig({
            "overrides": {}
        }, controller.args[1], "contextualMenu"), extendConfig({
            "overrides": {}
        }, controller.args[2], "contextualMenu"));
        FlexContextMenu.add(contextualMenu);
        var flxFilterDropdown = new kony.ui.FlexScrollContainer(extendConfig({
            "allowHorizontalBounce": false,
            "allowVerticalBounce": true,
            "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
            "bounces": true,
            "clipBounds": true,
            "enableScrolling": true,
            "horizontalScrollIndicator": true,
            "id": "flxFilterDropdown",
            "isVisible": false,
            "layoutType": kony.flex.FREE_FORM,
            "left": "56%",
            "maxHeight": "150px",
            "pagingEnabled": false,
            "scrollDirection": kony.flex.SCROLL_VERTICAL,
            "skin": "sknFlxffffffbordere1e5ed",
            "top": "250dp",
            "verticalScrollIndicator": true,
            "width": "220px",
            "zIndex": 200
        }, controller.args[0], "flxFilterDropdown"), extendConfig({}, controller.args[1], "flxFilterDropdown"), extendConfig({}, controller.args[2], "flxFilterDropdown"));
        flxFilterDropdown.setDefaultUnit(kony.flex.DP);
        var segDropDown = new kony.ui.SegmentedUI2(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
            "data": [{
                "imgCheckBox": "checkboxnormal.png",
                "lblDescription": "KL098234 - Westheimer"
            }, {
                "imgCheckBox": "checkboxnormal.png",
                "lblDescription": "KL098234 - Westheimer"
            }, {
                "imgCheckBox": "checkboxnormal.png",
                "lblDescription": "KL098234 - Westheimer"
            }],
            "groupCells": false,
            "id": "segDropDown",
            "isVisible": true,
            "left": "0dp",
            "needPageIndicator": true,
            "pageOffDotImage": "pageoffdot.png",
            "pageOnDotImage": "pageondot.png",
            "retainSelection": false,
            "right": "10px",
            "rowFocusSkin": "seg2Focus",
            "rowTemplate": "flxSearchDropDown",
            "sectionHeaderSkin": "sliPhoneSegmentHeader",
            "selectionBehavior": constants.SEGUI_MULTI_SELECT_BEHAVIOR,
            "selectionBehaviorConfig": {
                "imageIdentifier": "imgCheckBox",
                "selectedStateImage": "checkboxselected.png",
                "unselectedStateImage": "checkbox.png"
            },
            "separatorRequired": false,
            "showScrollbars": false,
            "top": "10dp",
            "viewType": constants.SEGUI_VIEW_TYPE_TABLEVIEW,
            "widgetDataMap": {
                "flxCheckBox": "flxCheckBox",
                "flxSearchDropDown": "flxSearchDropDown",
                "imgCheckBox": "imgCheckBox",
                "lblDescription": "lblDescription"
            },
            "zIndex": 500
        }, controller.args[0], "segDropDown"), extendConfig({
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "segDropDown"), extendConfig({}, controller.args[2], "segDropDown"));
        flxFilterDropdown.add(segDropDown);
        var flxAssignDropdown = new kony.ui.FlexScrollContainer(extendConfig({
            "allowHorizontalBounce": false,
            "allowVerticalBounce": true,
            "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
            "bounces": true,
            "clipBounds": true,
            "enableScrolling": true,
            "horizontalScrollIndicator": true,
            "id": "flxAssignDropdown",
            "isVisible": false,
            "layoutType": kony.flex.FREE_FORM,
            "left": "46.30%",
            "maxHeight": "150px",
            "pagingEnabled": false,
            "scrollDirection": kony.flex.SCROLL_VERTICAL,
            "skin": "sknFlxffffffbordere1e5ed",
            "top": "135dp",
            "verticalScrollIndicator": true,
            "width": "21%",
            "zIndex": 200
        }, controller.args[0], "flxAssignDropdown"), extendConfig({}, controller.args[1], "flxAssignDropdown"), extendConfig({
            "hoverSkin": "sknFlxHandCursor"
        }, controller.args[2], "flxAssignDropdown"));
        flxAssignDropdown.setDefaultUnit(kony.flex.DP);
        var segAssignDropdown = new kony.ui.SegmentedUI2(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
            "bottom": "10px",
            "data": [{
                "lblViewFullName": "Edward"
            }, {
                "lblViewFullName": "Edward"
            }, {
                "lblViewFullName": "Edward"
            }],
            "groupCells": false,
            "id": "segAssignDropdown",
            "isVisible": true,
            "left": "0dp",
            "needPageIndicator": true,
            "pageOffDotImage": "pageoffdot.png",
            "pageOnDotImage": "pageondot.png",
            "retainSelection": false,
            "right": "10px",
            "rowFocusSkin": "seg2Focus",
            "rowTemplate": "flxAssignUsers",
            "sectionHeaderSkin": "sliPhoneSegmentHeader",
            "selectionBehavior": constants.SEGUI_MULTI_SELECT_BEHAVIOR,
            "selectionBehaviorConfig": {
                "imageIdentifier": "imgCheckBox",
                "selectedStateImage": "checkboxselected.png",
                "unselectedStateImage": "checkbox.png"
            },
            "separatorRequired": false,
            "showScrollbars": false,
            "top": "10dp",
            "viewType": constants.SEGUI_VIEW_TYPE_TABLEVIEW,
            "widgetDataMap": {
                "flxAssignUsers": "flxAssignUsers",
                "lblViewFullName": "lblViewFullName"
            },
            "zIndex": 500
        }, controller.args[0], "segAssignDropdown"), extendConfig({
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "segAssignDropdown"), extendConfig({}, controller.args[2], "segAssignDropdown"));
        var richtextNoResult1 = new kony.ui.RichText(extendConfig({
            "centerX": "50%",
            "centerY": "50%",
            "id": "richtextNoResult1",
            "isVisible": false,
            "left": "0dp",
            "skin": "sknRtxLatoReg84939e13px",
            "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmCSRController.prop773\")",
            "top": "0dp",
            "width": "100%",
            "zIndex": 1
        }, controller.args[0], "richtextNoResult1"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_CENTER,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "richtextNoResult1"), extendConfig({}, controller.args[2], "richtextNoResult1"));
        flxAssignDropdown.add(segAssignDropdown, richtextNoResult1);
        var flxRepliedOnDropdown = new kony.ui.FlexScrollContainer(extendConfig({
            "allowHorizontalBounce": false,
            "allowVerticalBounce": true,
            "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
            "bounces": true,
            "clipBounds": true,
            "enableScrolling": true,
            "horizontalScrollIndicator": true,
            "id": "flxRepliedOnDropdown",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "left": "68.60%",
            "maxHeight": "150px",
            "pagingEnabled": false,
            "scrollDirection": kony.flex.SCROLL_VERTICAL,
            "skin": "sknFlxffffffbordere1e5ed",
            "top": "135dp",
            "verticalScrollIndicator": true,
            "width": "21%",
            "zIndex": 200
        }, controller.args[0], "flxRepliedOnDropdown"), extendConfig({}, controller.args[1], "flxRepliedOnDropdown"), extendConfig({
            "hoverSkin": "sknFlxHandCursor"
        }, controller.args[2], "flxRepliedOnDropdown"));
        flxRepliedOnDropdown.setDefaultUnit(kony.flex.DP);
        var segRepliedOnDropdown = new kony.ui.SegmentedUI2(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
            "bottom": "10px",
            "data": [{
                "lblViewFullName": "Edward"
            }, {
                "lblViewFullName": "Edward"
            }, {
                "lblViewFullName": "Edward"
            }],
            "groupCells": false,
            "id": "segRepliedOnDropdown",
            "isVisible": true,
            "left": "0dp",
            "needPageIndicator": true,
            "pageOffDotImage": "pageoffdot.png",
            "pageOnDotImage": "pageondot.png",
            "retainSelection": false,
            "right": "10px",
            "rowFocusSkin": "seg2Focus",
            "rowTemplate": "flxAssignUsers",
            "sectionHeaderSkin": "sliPhoneSegmentHeader",
            "selectionBehavior": constants.SEGUI_MULTI_SELECT_BEHAVIOR,
            "selectionBehaviorConfig": {
                "imageIdentifier": "imgCheckBox",
                "selectedStateImage": "checkboxselected.png",
                "unselectedStateImage": "checkbox.png"
            },
            "separatorRequired": false,
            "showScrollbars": false,
            "top": "10dp",
            "viewType": constants.SEGUI_VIEW_TYPE_TABLEVIEW,
            "widgetDataMap": {
                "flxAssignUsers": "flxAssignUsers",
                "lblViewFullName": "lblViewFullName"
            },
            "zIndex": 500
        }, controller.args[0], "segRepliedOnDropdown"), extendConfig({
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "segRepliedOnDropdown"), extendConfig({}, controller.args[2], "segRepliedOnDropdown"));
        var richtextNoResult2 = new kony.ui.RichText(extendConfig({
            "centerX": "50%",
            "centerY": "50%",
            "id": "richtextNoResult2",
            "isVisible": false,
            "left": "0dp",
            "skin": "sknRtxLatoReg84939e13px",
            "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmCSRController.prop773\")",
            "top": "0dp",
            "width": "100%",
            "zIndex": 1
        }, controller.args[0], "richtextNoResult2"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_CENTER,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "richtextNoResult2"), extendConfig({}, controller.args[2], "richtextNoResult2"));
        flxRepliedOnDropdown.add(segRepliedOnDropdown, richtextNoResult2);
        var flxDropDownDetail1 = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "clipBounds": false,
            "height": "380px",
            "id": "flxDropDownDetail1",
            "isVisible": false,
            "layoutType": kony.flex.FREE_FORM,
            "left": "20px",
            "isModalContainer": false,
            "skin": "skntxtbxNormald7d9e0",
            "top": "135px",
            "width": "21.40%",
            "zIndex": 1
        }, controller.args[0], "flxDropDownDetail1"), extendConfig({}, controller.args[1], "flxDropDownDetail1"), extendConfig({
            "hoverSkin": "skntxtbxhover11abeb"
        }, controller.args[2], "flxDropDownDetail1"));
        flxDropDownDetail1.setDefaultUnit(kony.flex.DP);
        var selectDate = new com.adminConsole.selectDate.selectDate(extendConfig({
            "height": "100%",
            "id": "selectDate",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "left": "0dp",
            "masterType": constants.MASTER_TYPE_DEFAULT,
            "isModalContainer": false,
            "skin": "slFbox",
            "top": "0dp",
            "width": "100%",
            "zIndex": 1,
            "overrides": {
                "btnApply": {
                    "centerX": "viz.val_cleared"
                },
                "calFrom": {
                    "centerX": "viz.val_cleared",
                    "left": "10px"
                },
                "calTo": {
                    "centerX": "viz.val_cleared",
                    "left": "10px"
                },
                "flxDate": {
                    "right": "viz.val_cleared",
                    "width": "100%"
                },
                "selectDate": {
                    "isVisible": true,
                    "right": "viz.val_cleared",
                    "width": "100%",
                    "zIndex": 1
                }
            }
        }, controller.args[0], "selectDate"), extendConfig({
            "overrides": {}
        }, controller.args[1], "selectDate"), extendConfig({
            "overrides": {}
        }, controller.args[2], "selectDate"));
        flxDropDownDetail1.add(selectDate);
        flxBody.add(flxMain, FlexResult, FlexNoResult, FlexContextMenu, flxFilterDropdown, flxAssignDropdown, flxRepliedOnDropdown, flxDropDownDetail1);
        MessageFilter.add(flxBody);
        return MessageFilter;
    }
})