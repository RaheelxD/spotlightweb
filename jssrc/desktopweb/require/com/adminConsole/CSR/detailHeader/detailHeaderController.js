define("com/adminConsole/CSR/detailHeader/userdetailHeaderController", function() {
    return {};
});
define("com/adminConsole/CSR/detailHeader/detailHeaderControllerActions", {
    /*
          This is an auto generated file and any modifications to it may result in corruption of the action sequence.
        */
});
define("com/adminConsole/CSR/detailHeader/detailHeaderController", ["com/adminConsole/CSR/detailHeader/userdetailHeaderController", "com/adminConsole/CSR/detailHeader/detailHeaderControllerActions"], function() {
    var controller = require("com/adminConsole/CSR/detailHeader/userdetailHeaderController");
    var actions = require("com/adminConsole/CSR/detailHeader/detailHeaderControllerActions");
    for (var key in actions) {
        controller[key] = actions[key];
    }
    return controller;
});
