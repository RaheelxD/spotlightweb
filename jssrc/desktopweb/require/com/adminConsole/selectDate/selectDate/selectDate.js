define(function() {
    return function(controller) {
        var selectDate = new kony.ui.FlexContainer(extendConfig({
            "clipBounds": true,
            "isMaster": true,
            "height": "100%",
            "id": "selectDate",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "left": "0dp",
            "isModalContainer": false,
            "skin": "slFbox",
            "top": "0dp",
            "width": "100%"
        }, controller.args[0], "selectDate"), extendConfig({}, controller.args[1], "selectDate"), extendConfig({}, controller.args[2], "selectDate"));
        selectDate.setDefaultUnit(kony.flex.DP);
        var flxDate = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
            "clipBounds": true,
            "id": "flxDate",
            "isVisible": true,
            "layoutType": kony.flex.FLOW_VERTICAL,
            "left": "0px",
            "isModalContainer": false,
            "skin": "slFbox",
            "top": "0dp",
            "width": "150px",
            "zIndex": 1
        }, controller.args[0], "flxDate"), extendConfig({}, controller.args[1], "flxDate"), extendConfig({}, controller.args[2], "flxDate"));
        flxDate.setDefaultUnit(kony.flex.DP);
        var flxDays = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
            "clipBounds": true,
            "id": "flxDays",
            "isVisible": true,
            "layoutType": kony.flex.FLOW_VERTICAL,
            "left": "0dp",
            "isModalContainer": false,
            "skin": "sknflxffffffop0a",
            "top": "0dp",
            "width": "100%",
            "zIndex": 1
        }, controller.args[0], "flxDays"), extendConfig({}, controller.args[1], "flxDays"), extendConfig({}, controller.args[2], "flxDays"));
        flxDays.setDefaultUnit(kony.flex.DP);
        var flxToday = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
            "clipBounds": true,
            "id": "flxToday",
            "isVisible": true,
            "layoutType": kony.flex.FLOW_VERTICAL,
            "left": "0dp",
            "isModalContainer": false,
            "skin": "slFbox",
            "top": "0dp",
            "width": "100%"
        }, controller.args[0], "flxToday"), extendConfig({}, controller.args[1], "flxToday"), extendConfig({}, controller.args[2], "flxToday"));
        flxToday.setDefaultUnit(kony.flex.DP);
        var lblToday = new kony.ui.Label(extendConfig({
            "id": "lblToday",
            "isVisible": true,
            "left": "10dp",
            "skin": "sknlblLatoBold35475f14px",
            "i18n_text": "kony.i18n.getLocalizedString(\"i18n.userwidgetmodel.lblToday\")",
            "top": "20dp",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, controller.args[0], "lblToday"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblToday"), extendConfig({}, controller.args[2], "lblToday"));
        flxToday.add(lblToday);
        var flxYesterday = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
            "clipBounds": true,
            "id": "flxYesterday",
            "isVisible": true,
            "layoutType": kony.flex.FLOW_VERTICAL,
            "left": "0dp",
            "isModalContainer": false,
            "skin": "slFbox",
            "top": "0dp",
            "width": "100%"
        }, controller.args[0], "flxYesterday"), extendConfig({}, controller.args[1], "flxYesterday"), extendConfig({}, controller.args[2], "flxYesterday"));
        flxYesterday.setDefaultUnit(kony.flex.DP);
        var lblYesterday = new kony.ui.Label(extendConfig({
            "id": "lblYesterday",
            "isVisible": true,
            "left": "10dp",
            "skin": "sknlblLatoBold35475f14px",
            "i18n_text": "kony.i18n.getLocalizedString(\"i18n.userwidgetmodel.lblYesterday\")",
            "top": "15dp",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, controller.args[0], "lblYesterday"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblYesterday"), extendConfig({}, controller.args[2], "lblYesterday"));
        flxYesterday.add(lblYesterday);
        var flx7days = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
            "clipBounds": true,
            "id": "flx7days",
            "isVisible": true,
            "layoutType": kony.flex.FLOW_VERTICAL,
            "left": "0dp",
            "isModalContainer": false,
            "skin": "slFbox",
            "top": "0dp",
            "width": "100%"
        }, controller.args[0], "flx7days"), extendConfig({}, controller.args[1], "flx7days"), extendConfig({}, controller.args[2], "flx7days"));
        flx7days.setDefaultUnit(kony.flex.DP);
        var lbl7days = new kony.ui.Label(extendConfig({
            "id": "lbl7days",
            "isVisible": true,
            "left": "10dp",
            "skin": "sknlblLatoBold35475f14px",
            "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmCustomerManagementController.Last_7_days\")",
            "top": "15dp",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, controller.args[0], "lbl7days"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lbl7days"), extendConfig({}, controller.args[2], "lbl7days"));
        flx7days.add(lbl7days);
        var flx30days = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
            "clipBounds": true,
            "id": "flx30days",
            "isVisible": true,
            "layoutType": kony.flex.FLOW_VERTICAL,
            "left": "0dp",
            "isModalContainer": false,
            "skin": "slFbox",
            "top": "0dp",
            "width": "100%"
        }, controller.args[0], "flx30days"), extendConfig({}, controller.args[1], "flx30days"), extendConfig({}, controller.args[2], "flx30days"));
        flx30days.setDefaultUnit(kony.flex.DP);
        var lbl30days = new kony.ui.Label(extendConfig({
            "bottom": "20dp",
            "id": "lbl30days",
            "isVisible": true,
            "left": "10dp",
            "skin": "sknlblLatoBold35475f14px",
            "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmCustomerManagementController.Last_30_days\")",
            "top": "15dp",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, controller.args[0], "lbl30days"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lbl30days"), extendConfig({}, controller.args[2], "lbl30days"));
        flx30days.add(lbl30days);
        var lblSeperator = new kony.ui.Label(extendConfig({
            "bottom": 0,
            "height": "1px",
            "id": "lblSeperator",
            "isVisible": true,
            "left": "0px",
            "right": "0dp",
            "skin": "sknSeparator",
            "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmAlertsManagement.lblAlertTypeDescriptionCount\")",
            "width": "100%",
            "zIndex": 1
        }, controller.args[0], "lblSeperator"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblSeperator"), extendConfig({}, controller.args[2], "lblSeperator"));
        flxDays.add(flxToday, flxYesterday, flx7days, flx30days, lblSeperator);
        var flxDatepicker = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
            "clipBounds": true,
            "id": "flxDatepicker",
            "isVisible": true,
            "layoutType": kony.flex.FLOW_VERTICAL,
            "left": "0dp",
            "isModalContainer": false,
            "skin": "sknfbfcfc",
            "top": "0dp",
            "width": "100%",
            "zIndex": 1
        }, controller.args[0], "flxDatepicker"), extendConfig({}, controller.args[1], "flxDatepicker"), extendConfig({}, controller.args[2], "flxDatepicker"));
        flxDatepicker.setDefaultUnit(kony.flex.DP);
        var lblCustomRange = new kony.ui.Label(extendConfig({
            "id": "lblCustomRange",
            "isVisible": true,
            "left": "10dp",
            "skin": "sknlblLatoBold35475f14px",
            "i18n_text": "kony.i18n.getLocalizedString(\"i18n.userwidgetmodel.lblCustomRange\")",
            "top": "20dp",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, controller.args[0], "lblCustomRange"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblCustomRange"), extendConfig({}, controller.args[2], "lblCustomRange"));
        var lblFrom = new kony.ui.Label(extendConfig({
            "id": "lblFrom",
            "isVisible": true,
            "left": "10dp",
            "skin": "sknlblLatoBold35475f14px",
            "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmLocations.lblFrom\")",
            "top": "10dp",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, controller.args[0], "lblFrom"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblFrom"), extendConfig({}, controller.args[2], "lblFrom"));
        var calFrom = new kony.ui.Calendar(extendConfig({
            "calendarIcon": "calbtn.png",
            "centerX": "50%",
            "dateFormat": "MM/dd/yyyy",
            "height": "40dp",
            "id": "calFrom",
            "isVisible": true,
            "placeholder": "From",
            "skin": "sknCalendar",
            "top": "10dp",
            "viewType": constants.CALENDAR_VIEW_TYPE_DEFAULT,
            "width": "130px",
            "zIndex": 1
        }, controller.args[0], "calFrom"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [3, 0, 3, 0],
            "paddingInPixel": false
        }, controller.args[1], "calFrom"), extendConfig({
            "noOfMonths": 1
        }, controller.args[2], "calFrom"));
        var lblTo = new kony.ui.Label(extendConfig({
            "id": "lblTo",
            "isVisible": true,
            "left": "10dp",
            "skin": "sknlblLatoBold35475f14px",
            "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmLocations.lblTo\")",
            "top": "10dp",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, controller.args[0], "lblTo"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblTo"), extendConfig({}, controller.args[2], "lblTo"));
        var calTo = new kony.ui.Calendar(extendConfig({
            "bottom": "20px",
            "calendarIcon": "calbtn.png",
            "centerX": "50%",
            "dateFormat": "MM/dd/yyyy",
            "height": "40dp",
            "id": "calTo",
            "isVisible": true,
            "placeholder": "To",
            "right": "10px",
            "skin": "sknCalendar",
            "top": "10dp",
            "viewType": constants.CALENDAR_VIEW_TYPE_DEFAULT,
            "width": "130px",
            "zIndex": 1
        }, controller.args[0], "calTo"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [3, 0, 3, 0],
            "paddingInPixel": false
        }, controller.args[1], "calTo"), extendConfig({
            "noOfMonths": 1
        }, controller.args[2], "calTo"));
        var btnApply = new kony.ui.Button(extendConfig({
            "bottom": "5dp",
            "centerX": "50%",
            "focusSkin": "sknbtnf7f7faLatoRegular12Px485c75Border1px485C75Radius20px",
            "height": "22px",
            "id": "btnApply",
            "isVisible": true,
            "right": "33%",
            "skin": "sknbtnf7f7faLatoRegular12Px485c75Border1px485C75Radius20px",
            "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmLogsController.Select\")",
            "top": "0dp",
            "width": "60px",
            "zIndex": 1
        }, controller.args[0], "btnApply"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_CENTER,
            "displayText": true,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "btnApply"), extendConfig({
            "hoverSkin": "sknbtnffffffLatoRegular12Px485c75Border1px485C75Radius20px"
        }, controller.args[2], "btnApply"));
        flxDatepicker.add(lblCustomRange, lblFrom, calFrom, lblTo, calTo, btnApply);
        flxDate.add(flxDays, flxDatepicker);
        selectDate.add(flxDate);
        return selectDate;
    }
})