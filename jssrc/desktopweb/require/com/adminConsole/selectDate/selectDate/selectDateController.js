define("com/adminConsole/selectDate/selectDate/userselectDateController", function() {
    return {};
});
define("com/adminConsole/selectDate/selectDate/selectDateControllerActions", {
    /*
          This is an auto generated file and any modifications to it may result in corruption of the action sequence.
        */
});
define("com/adminConsole/selectDate/selectDate/selectDateController", ["com/adminConsole/selectDate/selectDate/userselectDateController", "com/adminConsole/selectDate/selectDate/selectDateControllerActions"], function() {
    var controller = require("com/adminConsole/selectDate/selectDate/userselectDateController");
    var actions = require("com/adminConsole/selectDate/selectDate/selectDateControllerActions");
    for (var key in actions) {
        controller[key] = actions[key];
    }
    return controller;
});
