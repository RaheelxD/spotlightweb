define(function() {
    return function(controller) {
        var imageContainer = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
            "bottom": "30dp",
            "clipBounds": true,
            "isMaster": true,
            "id": "imageContainer",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "left": "0dp",
            "isModalContainer": false,
            "skin": "slFbox",
            "top": "0dp",
            "width": kony.flex.USE_PREFFERED_SIZE
        }, controller.args[0], "imageContainer"), extendConfig({}, controller.args[1], "imageContainer"), extendConfig({}, controller.args[2], "imageContainer"));
        imageContainer.setDefaultUnit(kony.flex.DP);
        var flxImageContainer = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
            "clipBounds": true,
            "id": "flxImageContainer",
            "isVisible": true,
            "layoutType": kony.flex.FLOW_VERTICAL,
            "left": "20dp",
            "isModalContainer": false,
            "right": "20dp",
            "skin": "slFbox",
            "top": "0dp",
            "width": kony.flex.USE_PREFFERED_SIZE
        }, controller.args[0], "flxImageContainer"), extendConfig({}, controller.args[1], "flxImageContainer"), extendConfig({}, controller.args[2], "flxImageContainer"));
        flxImageContainer.setDefaultUnit(kony.flex.DP);
        var flxImgSource = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
            "clipBounds": true,
            "id": "flxImgSource",
            "isVisible": true,
            "layoutType": kony.flex.FLOW_VERTICAL,
            "left": "0dp",
            "isModalContainer": false,
            "skin": "slFbox",
            "top": "20dp",
            "width": kony.flex.USE_PREFFERED_SIZE
        }, controller.args[0], "flxImgSource"), extendConfig({}, controller.args[1], "flxImgSource"), extendConfig({}, controller.args[2], "flxImgSource"));
        flxImgSource.setDefaultUnit(kony.flex.DP);
        var lblImgConfig = new kony.ui.Label(extendConfig({
            "id": "lblImgConfig",
            "isVisible": true,
            "left": "0dp",
            "skin": "sknLbl192B45LatoRegular14px",
            "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmAdManagement.ImageConfiguration\")",
            "top": "0dp",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, controller.args[0], "lblImgConfig"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblImgConfig"), extendConfig({}, controller.args[2], "lblImgConfig"));
        var lblLine = new kony.ui.Label(extendConfig({
            "bottom": "0dp",
            "height": "1dp",
            "id": "lblLine",
            "isVisible": true,
            "left": "0dp",
            "right": "0dp",
            "skin": "sknlblSeperatorD7D9E0",
            "text": "l",
            "top": "15dp",
            "width": "100%",
            "zIndex": 1
        }, controller.args[0], "lblLine"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblLine"), extendConfig({}, controller.args[2], "lblLine"));
        var lblImgSource = new kony.ui.Label(extendConfig({
            "id": "lblImgSource",
            "isVisible": true,
            "left": "0dp",
            "skin": "sknLblLato485c7513px",
            "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmAdManagement.ImgURL\")",
            "top": "15dp",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, controller.args[0], "lblImgSource"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblImgSource"), extendConfig({}, controller.args[2], "lblImgSource"));
        var flxImageSource = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
            "clipBounds": true,
            "id": "flxImageSource",
            "isVisible": true,
            "layoutType": kony.flex.FLOW_VERTICAL,
            "left": "0dp",
            "isModalContainer": false,
            "skin": "slFbox",
            "width": kony.flex.USE_PREFFERED_SIZE
        }, controller.args[0], "flxImageSource"), extendConfig({}, controller.args[1], "flxImageSource"), extendConfig({}, controller.args[2], "flxImageSource"));
        flxImageSource.setDefaultUnit(kony.flex.DP);
        flxImageSource.add();
        flxImgSource.add(lblImgConfig, lblLine, lblImgSource, flxImageSource);
        var flxTargetSource = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
            "clipBounds": true,
            "id": "flxTargetSource",
            "isVisible": true,
            "layoutType": kony.flex.FLOW_VERTICAL,
            "left": "0",
            "isModalContainer": false,
            "skin": "slFbox",
            "top": "20dp",
            "width": kony.flex.USE_PREFFERED_SIZE
        }, controller.args[0], "flxTargetSource"), extendConfig({}, controller.args[1], "flxTargetSource"), extendConfig({}, controller.args[2], "flxTargetSource"));
        flxTargetSource.setDefaultUnit(kony.flex.DP);
        var lblTargetSource = new kony.ui.Label(extendConfig({
            "id": "lblTargetSource",
            "isVisible": true,
            "left": "0dp",
            "skin": "sknLblLato485c7513px",
            "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmAdManagement.TargetURL\")",
            "top": "0dp",
            "width": "100%",
            "zIndex": 1
        }, controller.args[0], "lblTargetSource"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblTargetSource"), extendConfig({}, controller.args[2], "lblTargetSource"));
        var flxTargetSourceValue = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "clipBounds": true,
            "height": "40dp",
            "id": "flxTargetSourceValue",
            "isVisible": true,
            "layoutType": kony.flex.FLOW_HORIZONTAL,
            "left": "0dp",
            "isModalContainer": false,
            "skin": "slFbox",
            "top": "15dp",
            "width": "100%",
            "zIndex": 1
        }, controller.args[0], "flxTargetSourceValue"), extendConfig({}, controller.args[1], "flxTargetSourceValue"), extendConfig({}, controller.args[2], "flxTargetSourceValue"));
        flxTargetSourceValue.setDefaultUnit(kony.flex.DP);
        var txtTargetSource = new kony.ui.TextBox2(extendConfig({
            "autoCapitalize": constants.TEXTBOX_AUTO_CAPITALIZE_NONE,
            "focusSkin": "skntxtbxDetailsFocus14px",
            "height": "100%",
            "id": "txtTargetSource",
            "isVisible": true,
            "keyBoardStyle": constants.TEXTBOX_KEY_BOARD_STYLE_PHONE_PAD,
            "left": "0",
            "i18n_placeholder": "kony.i18n.getLocalizedString(\"i18n.frmAdManagement.EnterURL\")",
            "secureTextEntry": false,
            "skin": "txtD7d9e0",
            "textInputMode": constants.TEXTBOX_INPUT_MODE_NUMERIC,
            "top": "0",
            "width": "80%",
            "zIndex": 10
        }, controller.args[0], "txtTargetSource"), extendConfig({
            "containerHeightMode": constants.TEXTBOX_FONT_METRICS_DRIVEN_HEIGHT,
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [2, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "txtTargetSource"), extendConfig({
            "autoCorrect": false,
            "placeholderSkin": "defTextBoxPlaceholder"
        }, controller.args[2], "txtTargetSource"));
        var btnTargetVerify = new kony.ui.Button(extendConfig({
            "height": "100%",
            "id": "btnTargetVerify",
            "isVisible": true,
            "left": "0dp",
            "skin": "sknBtnd7d9e0",
            "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmAdManagement.Verify\")",
            "top": "0dp",
            "width": "20%",
            "zIndex": 1
        }, controller.args[0], "btnTargetVerify"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_CENTER,
            "displayText": true,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "btnTargetVerify"), extendConfig({
            "hoverSkin": "sknBtnd7d9e0"
        }, controller.args[2], "btnTargetVerify"));
        flxTargetSourceValue.add(txtTargetSource, btnTargetVerify);
        var imgTargetError = new com.adminConsole.loans.applications.errorMsg(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
            "id": "imgTargetError",
            "isVisible": false,
            "layoutType": kony.flex.FLOW_HORIZONTAL,
            "left": "0dp",
            "masterType": constants.MASTER_TYPE_DEFAULT,
            "isModalContainer": false,
            "skin": "slFbox",
            "top": "10dp",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "overrides": {
                "errorMsg": {
                    "isVisible": false
                },
                "lblErrorText": {
                    "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmAdManagement.ErrorTargetURL\")"
                }
            }
        }, controller.args[0], "imgTargetError"), extendConfig({
            "overrides": {}
        }, controller.args[1], "imgTargetError"), extendConfig({
            "overrides": {}
        }, controller.args[2], "imgTargetError"));
        flxTargetSource.add(lblTargetSource, flxTargetSourceValue, imgTargetError);
        flxImageContainer.add(flxImgSource, flxTargetSource);
        imageContainer.add(flxImageContainer);
        return imageContainer;
    }
})