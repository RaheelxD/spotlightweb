define("com/adminConsole/adManagement/imageContainer/userimageContainerController", function() {
    return {};
});
define("com/adminConsole/adManagement/imageContainer/imageContainerControllerActions", {
    /*
      This is an auto generated file and any modifications to it may result in corruption of the action sequence.
    */
});
define("com/adminConsole/adManagement/imageContainer/imageContainerController", ["com/adminConsole/adManagement/imageContainer/userimageContainerController", "com/adminConsole/adManagement/imageContainer/imageContainerControllerActions"], function() {
    var controller = require("com/adminConsole/adManagement/imageContainer/userimageContainerController");
    var actions = require("com/adminConsole/adManagement/imageContainer/imageContainerControllerActions");
    for (var key in actions) {
        controller[key] = actions[key];
    }
    return controller;
});
