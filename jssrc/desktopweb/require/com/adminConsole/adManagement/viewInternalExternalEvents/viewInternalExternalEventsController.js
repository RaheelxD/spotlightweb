define("com/adminConsole/adManagement/viewInternalExternalEvents/userviewInternalExternalEventsController", function() {
    return {
        viewInternalExternalEventsPreShow: function() {
            this.setFlowActions();
        },
        showEvents: function() {
            //setting segment visibility off and changing icon to collapse
            if (this.view.flxEventsBody.isVisible) {
                this.view.flxEventsBody.setVisibility(false);
                this.view.fontIconEventsExpandCollapse.text = "\ue922";
            } else {
                this.view.flxEventsBody.setVisibility(true);
                this.view.fontIconEventsExpandCollapse.text = "\ue915";
            }
            this.view.forceLayout();
        },
        setFlowActions: function() {
            var scopeObj = this;
            this.view.flxEventsExpandCollapse.onClick = function() {
                scopeObj.showEvents();
            };
        }
    };
});
define("com/adminConsole/adManagement/viewInternalExternalEvents/viewInternalExternalEventsControllerActions", {
    /*
        This is an auto generated file and any modifications to it may result in corruption of the action sequence.
    */
    AS_FlexContainer_c0be6b4ef53c4d068127bc73197be828: function AS_FlexContainer_c0be6b4ef53c4d068127bc73197be828(eventobject) {
        var self = this;
        return self.viewInternalExternalEventsPreShow.call(this);
    }
});
define("com/adminConsole/adManagement/viewInternalExternalEvents/viewInternalExternalEventsController", ["com/adminConsole/adManagement/viewInternalExternalEvents/userviewInternalExternalEventsController", "com/adminConsole/adManagement/viewInternalExternalEvents/viewInternalExternalEventsControllerActions"], function() {
    var controller = require("com/adminConsole/adManagement/viewInternalExternalEvents/userviewInternalExternalEventsController");
    var actions = require("com/adminConsole/adManagement/viewInternalExternalEvents/viewInternalExternalEventsControllerActions");
    for (var key in actions) {
        controller[key] = actions[key];
    }
    return controller;
});
