define(function() {
    return function(controller) {
        var viewInternalExternalEvents = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
            "clipBounds": true,
            "isMaster": true,
            "id": "viewInternalExternalEvents",
            "isVisible": true,
            "layoutType": kony.flex.FLOW_VERTICAL,
            "left": "0dp",
            "isModalContainer": false,
            "preShow": function(eventobject) {
                controller.AS_FlexContainer_c0be6b4ef53c4d068127bc73197be828(eventobject);
            },
            "skin": "sknFlxbgFFFFFFbdrD7D9E0rd4px",
            "top": "0dp",
            "width": "100%"
        }, controller.args[0], "viewInternalExternalEvents"), extendConfig({}, controller.args[1], "viewInternalExternalEvents"), extendConfig({}, controller.args[2], "viewInternalExternalEvents"));
        viewInternalExternalEvents.setDefaultUnit(kony.flex.DP);
        var flxEventsHeader = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "clipBounds": true,
            "height": "45dp",
            "id": "flxEventsHeader",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "left": "0dp",
            "isModalContainer": false,
            "skin": "sknflxf5f6f8Op100",
            "top": "0dp",
            "width": "100%",
            "zIndex": 1
        }, controller.args[0], "flxEventsHeader"), extendConfig({}, controller.args[1], "flxEventsHeader"), extendConfig({}, controller.args[2], "flxEventsHeader"));
        flxEventsHeader.setDefaultUnit(kony.flex.DP);
        var flxEventsExpandCollapse = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "centerY": "50%",
            "clipBounds": true,
            "height": "16dp",
            "id": "flxEventsExpandCollapse",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "left": "10dp",
            "isModalContainer": false,
            "skin": "hoverhandSkin",
            "width": "16dp"
        }, controller.args[0], "flxEventsExpandCollapse"), extendConfig({}, controller.args[1], "flxEventsExpandCollapse"), extendConfig({}, controller.args[2], "flxEventsExpandCollapse"));
        flxEventsExpandCollapse.setDefaultUnit(kony.flex.DP);
        var fontIconEventsExpandCollapse = new kony.ui.Label(extendConfig({
            "centerY": "50%",
            "id": "fontIconEventsExpandCollapse",
            "isVisible": true,
            "left": "0dp",
            "skin": "sknLblIconMoon16px192b45",
            "text": "",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, controller.args[0], "fontIconEventsExpandCollapse"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "fontIconEventsExpandCollapse"), extendConfig({}, controller.args[2], "fontIconEventsExpandCollapse"));
        flxEventsExpandCollapse.add(fontIconEventsExpandCollapse);
        var flxEventsHeaderInner = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
            "centerY": "50%",
            "clipBounds": true,
            "id": "flxEventsHeaderInner",
            "isVisible": true,
            "layoutType": kony.flex.FLOW_HORIZONTAL,
            "left": "36dp",
            "isModalContainer": false,
            "right": "60dp",
            "skin": "slFbox"
        }, controller.args[0], "flxEventsHeaderInner"), extendConfig({}, controller.args[1], "flxEventsHeaderInner"), extendConfig({}, controller.args[2], "flxEventsHeaderInner"));
        flxEventsHeaderInner.setDefaultUnit(kony.flex.DP);
        var lblEventsHeader = new kony.ui.Label(extendConfig({
            "id": "lblEventsHeader",
            "isVisible": true,
            "left": "0dp",
            "skin": "sknLatoRegular192B4516px",
            "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmAdManagement.internalEvents\")",
            "top": 0,
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, controller.args[0], "lblEventsHeader"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblEventsHeader"), extendConfig({}, controller.args[2], "lblEventsHeader"));
        var fontIconEventsInfo = new kony.ui.Label(extendConfig({
            "id": "fontIconEventsInfo",
            "isVisible": true,
            "left": "10dp",
            "skin": "sknfontIcon33333316px",
            "text": "",
            "top": 2,
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, controller.args[0], "fontIconEventsInfo"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "fontIconEventsInfo"), extendConfig({}, controller.args[2], "fontIconEventsInfo"));
        flxEventsHeaderInner.add(lblEventsHeader, fontIconEventsInfo);
        var btnAddEvents = new kony.ui.Button(extendConfig({
            "centerY": "50%",
            "height": "22dp",
            "id": "btnAddEvents",
            "isVisible": true,
            "right": "12dp",
            "skin": "sknbtnf7f7faLatoRegular12Px485c75Border1px485C75Radius20px",
            "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmPermissionsController.Add\")",
            "width": "52dp",
            "zIndex": 1
        }, controller.args[0], "btnAddEvents"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_CENTER,
            "displayText": true,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "btnAddEvents"), extendConfig({
            "hoverSkin": "sknbtnffffffLatoRegular12Px485c75Border1px485C75Radius20px"
        }, controller.args[2], "btnAddEvents"));
        flxEventsHeader.add(flxEventsExpandCollapse, flxEventsHeaderInner, btnAddEvents);
        var flxEventsBody = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
            "clipBounds": true,
            "id": "flxEventsBody",
            "isVisible": true,
            "layoutType": kony.flex.FLOW_VERTICAL,
            "left": "0dp",
            "isModalContainer": false,
            "skin": "slFbox",
            "top": "0dp",
            "width": "100%"
        }, controller.args[0], "flxEventsBody"), extendConfig({}, controller.args[1], "flxEventsBody"), extendConfig({}, controller.args[2], "flxEventsBody"));
        flxEventsBody.setDefaultUnit(kony.flex.DP);
        var flxEventsSegHeader = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "clipBounds": true,
            "height": "50dp",
            "id": "flxEventsSegHeader",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "left": "0dp",
            "isModalContainer": false,
            "skin": "sknBackgroundFFFFFF",
            "top": "0dp",
            "width": "100%"
        }, controller.args[0], "flxEventsSegHeader"), extendConfig({}, controller.args[1], "flxEventsSegHeader"), extendConfig({}, controller.args[2], "flxEventsSegHeader"));
        flxEventsSegHeader.setDefaultUnit(kony.flex.DP);
        var lblEventNameHeader = new kony.ui.Label(extendConfig({
            "centerY": "50%",
            "id": "lblEventNameHeader",
            "isVisible": true,
            "left": "10dp",
            "skin": "sknlblLato696c7313px",
            "text": "EVENT NAME",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, controller.args[0], "lblEventNameHeader"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblEventNameHeader"), extendConfig({}, controller.args[2], "lblEventNameHeader"));
        var lblEventSourceHeader = new kony.ui.Label(extendConfig({
            "centerY": "50%",
            "id": "lblEventSourceHeader",
            "isVisible": true,
            "left": "38%",
            "skin": "sknlblLato696c7313px",
            "text": "EVENT SOURCE",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, controller.args[0], "lblEventSourceHeader"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblEventSourceHeader"), extendConfig({}, controller.args[2], "lblEventSourceHeader"));
        var lblEventCodeHeader = new kony.ui.Label(extendConfig({
            "centerY": "50%",
            "id": "lblEventCodeHeader",
            "isVisible": true,
            "left": "71%",
            "skin": "sknlblLato696c7313px",
            "text": "EVENT CODE",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, controller.args[0], "lblEventCodeHeader"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblEventCodeHeader"), extendConfig({}, controller.args[2], "lblEventCodeHeader"));
        var lblSeparator = new kony.ui.Label(extendConfig({
            "bottom": "0dp",
            "height": "1px",
            "id": "lblSeparator",
            "isVisible": true,
            "left": "0dp",
            "right": "0dp",
            "skin": "sknlblSeperatorD7D9E0",
            "text": "Label",
            "zIndex": 2
        }, controller.args[0], "lblSeparator"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_TOP_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblSeparator"), extendConfig({}, controller.args[2], "lblSeparator"));
        flxEventsSegHeader.add(lblEventNameHeader, lblEventSourceHeader, lblEventCodeHeader, lblSeparator);
        var segEvents = new kony.ui.SegmentedUI2(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
            "data": [{
                "lblDelete": "kony.i18n.getLocalizedString(\"i18n.frmFAQ.fonticonDelete\")",
                "lblEvent": "Events Name",
                "lblEventCode": "Events Name",
                "lblEventSource": "Events Name",
                "lblSeparator": "Label"
            }, {
                "lblDelete": "",
                "lblEvent": "Events Name",
                "lblEventCode": "Events Name",
                "lblEventSource": "Events Name",
                "lblSeparator": "Label"
            }, {
                "lblDelete": "",
                "lblEvent": "Events Name",
                "lblEventCode": "Events Name",
                "lblEventSource": "Events Name",
                "lblSeparator": "Label"
            }],
            "groupCells": false,
            "id": "segEvents",
            "isVisible": true,
            "left": "0dp",
            "needPageIndicator": true,
            "pageOffDotImage": "pageoffdot.png",
            "pageOnDotImage": "pageondot.png",
            "retainSelection": false,
            "rowFocusSkin": "seg2Focus",
            "rowSkin": "sknseg3pxRadius",
            "rowTemplate": "flxEventsView",
            "sectionHeaderSkin": "sliPhoneSegmentHeader",
            "selectionBehavior": constants.SEGUI_DEFAULT_BEHAVIOR,
            "separatorRequired": false,
            "showScrollbars": false,
            "top": "0dp",
            "viewType": constants.SEGUI_VIEW_TYPE_TABLEVIEW,
            "widgetDataMap": {
                "flxDelete": "flxDelete",
                "flxEventsView": "flxEventsView",
                "lblDelete": "lblDelete",
                "lblEvent": "lblEvent",
                "lblEventCode": "lblEventCode",
                "lblEventSource": "lblEventSource",
                "lblSeparator": "lblSeparator"
            },
            "width": "100%",
            "zIndex": 1
        }, controller.args[0], "segEvents"), extendConfig({
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "segEvents"), extendConfig({}, controller.args[2], "segEvents"));
        var flxNoResultFound = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "clipBounds": true,
            "height": "150dp",
            "id": "flxNoResultFound",
            "isVisible": false,
            "layoutType": kony.flex.FREE_FORM,
            "left": "0dp",
            "isModalContainer": false,
            "skin": "slFbox",
            "top": "0dp",
            "width": "100%",
            "zIndex": 1
        }, controller.args[0], "flxNoResultFound"), extendConfig({}, controller.args[1], "flxNoResultFound"), extendConfig({}, controller.args[2], "flxNoResultFound"));
        flxNoResultFound.setDefaultUnit(kony.flex.DP);
        var rtxSearchMesg = new kony.ui.RichText(extendConfig({
            "centerX": "50%",
            "centerY": "50%",
            "id": "rtxSearchMesg",
            "isVisible": true,
            "left": "0",
            "skin": "sknRtxLato84939e12Px",
            "text": "No Results found",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, controller.args[0], "rtxSearchMesg"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_CENTER,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "rtxSearchMesg"), extendConfig({}, controller.args[2], "rtxSearchMesg"));
        flxNoResultFound.add(rtxSearchMesg);
        flxEventsBody.add(flxEventsSegHeader, segEvents, flxNoResultFound);
        viewInternalExternalEvents.add(flxEventsHeader, flxEventsBody);
        return viewInternalExternalEvents;
    }
})