define(function() {
    return function(controller) {
        var campaignPopupMobile = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
            "clipBounds": true,
            "isMaster": true,
            "id": "campaignPopupMobile",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "left": "0dp",
            "isModalContainer": false,
            "skin": "slFbox",
            "top": "0dp",
            "width": "100%"
        }, controller.args[0], "campaignPopupMobile"), extendConfig({}, controller.args[1], "campaignPopupMobile"), extendConfig({}, controller.args[2], "campaignPopupMobile"));
        campaignPopupMobile.setDefaultUnit(kony.flex.DP);
        var flxPopup = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
            "centerX": "50%",
            "clipBounds": true,
            "id": "flxPopup",
            "isVisible": true,
            "layoutType": kony.flex.FLOW_VERTICAL,
            "left": "0dp",
            "isModalContainer": false,
            "skin": "slFbox",
            "top": "0dp",
            "width": "300dp",
            "zIndex": 1
        }, controller.args[0], "flxPopup"), extendConfig({}, controller.args[1], "flxPopup"), extendConfig({}, controller.args[2], "flxPopup"));
        flxPopup.setDefaultUnit(kony.flex.DP);
        var flxHeader = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "clipBounds": true,
            "height": "23dp",
            "id": "flxHeader",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "left": "0dp",
            "isModalContainer": false,
            "skin": "slFbox",
            "top": "0dp",
            "width": "100%",
            "zIndex": 1
        }, controller.args[0], "flxHeader"), extendConfig({}, controller.args[1], "flxHeader"), extendConfig({}, controller.args[2], "flxHeader"));
        flxHeader.setDefaultUnit(kony.flex.DP);
        var flxClose = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "clipBounds": true,
            "height": "12dp",
            "id": "flxClose",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "isModalContainer": false,
            "right": "11dp",
            "skin": "slFbox",
            "top": "11dp",
            "width": "12dp",
            "zIndex": 1
        }, controller.args[0], "flxClose"), extendConfig({}, controller.args[1], "flxClose"), extendConfig({}, controller.args[2], "flxClose"));
        flxClose.setDefaultUnit(kony.flex.DP);
        var lblClose = new kony.ui.Label(extendConfig({
            "height": "100%",
            "id": "lblClose",
            "isVisible": true,
            "left": "0px",
            "skin": "sknLblIcon484b5218px",
            "text": "",
            "top": "0dp",
            "width": "100%",
            "zIndex": 1
        }, controller.args[0], "lblClose"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_CENTER,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblClose"), extendConfig({}, controller.args[2], "lblClose"));
        flxClose.add(lblClose);
        flxHeader.add(flxClose);
        var flxCampaignHeader = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
            "clipBounds": true,
            "id": "flxCampaignHeader",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "left": "0",
            "isModalContainer": false,
            "skin": "slFbox",
            "top": "1dp",
            "width": "100%",
            "zIndex": 1
        }, controller.args[0], "flxCampaignHeader"), extendConfig({}, controller.args[1], "flxCampaignHeader"), extendConfig({}, controller.args[2], "flxCampaignHeader"));
        flxCampaignHeader.setDefaultUnit(kony.flex.DP);
        var lblHeading = new kony.ui.Label(extendConfig({
            "id": "lblHeading",
            "isVisible": true,
            "left": "20dp",
            "right": "30dp",
            "skin": "sknLbl003e7540px",
            "text": "Spend less, earn more Interest",
            "textStyle": {},
            "top": "0dp",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, controller.args[0], "lblHeading"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblHeading"), extendConfig({}, controller.args[2], "lblHeading"));
        flxCampaignHeader.add(lblHeading);
        var flxCampaignDescription = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
            "clipBounds": true,
            "id": "flxCampaignDescription",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "left": "0dp",
            "isModalContainer": false,
            "skin": "slFbox",
            "top": "6dp",
            "width": "100%",
            "zIndex": 1
        }, controller.args[0], "flxCampaignDescription"), extendConfig({}, controller.args[1], "flxCampaignDescription"), extendConfig({}, controller.args[2], "flxCampaignDescription"));
        flxCampaignDescription.setDefaultUnit(kony.flex.DP);
        var lblDescription = new kony.ui.Label(extendConfig({
            "id": "lblDescription",
            "isVisible": true,
            "left": "20dp",
            "right": "30dp",
            "skin": "sknLbl20px424242",
            "text": "Get 1.95% APY on your savings with the new Infinity savings account. ",
            "textStyle": {},
            "top": "0dp",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, controller.args[0], "lblDescription"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblDescription"), extendConfig({}, controller.args[2], "lblDescription"));
        flxCampaignDescription.add(lblDescription);
        var flxCampaignImage = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
            "clipBounds": true,
            "id": "flxCampaignImage",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "left": "0dp",
            "isModalContainer": false,
            "skin": "slFbox",
            "top": "20dp",
            "width": "100%",
            "zIndex": 1
        }, controller.args[0], "flxCampaignImage"), extendConfig({}, controller.args[1], "flxCampaignImage"), extendConfig({}, controller.args[2], "flxCampaignImage"));
        flxCampaignImage.setDefaultUnit(kony.flex.DP);
        var imgPopup = new kony.ui.Image2(extendConfig({
            "height": "170dp",
            "id": "imgPopup",
            "isVisible": true,
            "left": "0dp",
            "skin": "slImage",
            "src": "imagedrag.png",
            "top": "0dp",
            "width": "100%",
            "zIndex": 1
        }, controller.args[0], "imgPopup"), extendConfig({
            "imageScaleMode": constants.IMAGE_SCALE_MODE_MAINTAIN_ASPECT_RATIO,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "imgPopup"), extendConfig({}, controller.args[2], "imgPopup"));
        var lblCampaignLink = new kony.ui.Label(extendConfig({
            "id": "lblCampaignLink",
            "isVisible": false,
            "left": "0dp",
            "skin": "slLabel",
            "text": "Campaign Link to be Given Here",
            "textStyle": {},
            "top": "0dp",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, controller.args[0], "lblCampaignLink"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblCampaignLink"), extendConfig({}, controller.args[2], "lblCampaignLink"));
        flxCampaignImage.add(imgPopup, lblCampaignLink);
        var flxButtons = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
            "bottom": "20dp",
            "clipBounds": true,
            "id": "flxButtons",
            "isVisible": true,
            "layoutType": kony.flex.FLOW_VERTICAL,
            "left": "0dp",
            "isModalContainer": false,
            "skin": "slFbox",
            "top": "21dp",
            "width": "300dp",
            "zIndex": 1
        }, controller.args[0], "flxButtons"), extendConfig({}, controller.args[1], "flxButtons"), extendConfig({}, controller.args[2], "flxButtons"));
        flxButtons.setDefaultUnit(kony.flex.DP);
        var btnYes = new kony.ui.Button(extendConfig({
            "height": "40dp",
            "id": "btnYes",
            "isVisible": true,
            "left": "20dp",
            "skin": "sknBtnNormalSSPFFFFFF15Px",
            "text": "Read Now",
            "top": "20dp",
            "width": "265dp",
            "zIndex": 1
        }, controller.args[0], "btnYes"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_CENTER,
            "displayText": true,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "btnYes"), extendConfig({
            "hoverSkin": "sknBtnNormalSSPFFFFFFHover15Px"
        }, controller.args[2], "btnYes"));
        var btnNo = new kony.ui.Button(extendConfig({
            "height": "40dp",
            "id": "btnNo",
            "isVisible": true,
            "left": "20dp",
            "skin": "sknBtnffffffBorder0273e31pxRadius2px",
            "text": "Read Later",
            "top": "10dp",
            "width": "265dp",
            "zIndex": 1
        }, controller.args[0], "btnNo"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_CENTER,
            "displayText": true,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "btnNo"), extendConfig({
            "hoverSkin": "sknBtnffffffBorder0273e31pxRadius2px"
        }, controller.args[2], "btnNo"));
        flxButtons.add(btnYes, btnNo);
        flxPopup.add(flxHeader, flxCampaignHeader, flxCampaignDescription, flxCampaignImage, flxButtons);
        campaignPopupMobile.add(flxPopup);
        return campaignPopupMobile;
    }
})