define("com/adminConsole/adManagement/campaignPopupMobile/usercampaignPopupMobileController", function() {
    return {};
});
define("com/adminConsole/adManagement/campaignPopupMobile/campaignPopupMobileControllerActions", {
    /*
      This is an auto generated file and any modifications to it may result in corruption of the action sequence.
    */
});
define("com/adminConsole/adManagement/campaignPopupMobile/campaignPopupMobileController", ["com/adminConsole/adManagement/campaignPopupMobile/usercampaignPopupMobileController", "com/adminConsole/adManagement/campaignPopupMobile/campaignPopupMobileControllerActions"], function() {
    var controller = require("com/adminConsole/adManagement/campaignPopupMobile/usercampaignPopupMobileController");
    var actions = require("com/adminConsole/adManagement/campaignPopupMobile/campaignPopupMobileControllerActions");
    for (var key in actions) {
        controller[key] = actions[key];
    }
    return controller;
});
