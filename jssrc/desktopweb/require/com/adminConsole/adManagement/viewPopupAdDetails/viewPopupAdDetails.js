define(function() {
    return function(controller) {
        var viewPopupAdDetails = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
            "clipBounds": true,
            "isMaster": true,
            "id": "viewPopupAdDetails",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "left": "0dp",
            "isModalContainer": false,
            "skin": "sknBackgroundFFFFFF",
            "top": "0dp",
            "width": "100%"
        }, controller.args[0], "viewPopupAdDetails"), extendConfig({}, controller.args[1], "viewPopupAdDetails"), extendConfig({}, controller.args[2], "viewPopupAdDetails"));
        viewPopupAdDetails.setDefaultUnit(kony.flex.DP);
        var lblNopopupDatafound = new kony.ui.Label(extendConfig({
            "centerX": "50%",
            "height": "30dp",
            "id": "lblNopopupDatafound",
            "isVisible": false,
            "skin": "sknLblLato485c7513px",
            "text": "No Data Found",
            "top": "50dp",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, controller.args[0], "lblNopopupDatafound"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblNopopupDatafound"), extendConfig({}, controller.args[2], "lblNopopupDatafound"));
        var flxContent = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
            "clipBounds": true,
            "id": "flxContent",
            "isVisible": true,
            "layoutType": kony.flex.FLOW_VERTICAL,
            "left": "20dp",
            "isModalContainer": false,
            "right": "20dp",
            "skin": "slFbox",
            "top": "0dp",
            "zIndex": 1
        }, controller.args[0], "flxContent"), extendConfig({}, controller.args[1], "flxContent"), extendConfig({}, controller.args[2], "flxContent"));
        flxContent.setDefaultUnit(kony.flex.DP);
        var lblBanner = new kony.ui.Label(extendConfig({
            "id": "lblBanner",
            "isVisible": true,
            "left": "0px",
            "skin": "sknlbl333333LatoSemiBold13px",
            "text": "Banner On-Click Target URL",
            "top": "20dp",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, controller.args[0], "lblBanner"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblBanner"), extendConfig({}, controller.args[2], "lblBanner"));
        var lblBannerValue = new kony.ui.Label(extendConfig({
            "id": "lblBannerValue",
            "isVisible": true,
            "left": "0dp",
            "skin": "sknLblLato13px117eb0",
            "text": "Label",
            "top": "15dp",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, controller.args[0], "lblBannerValue"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblBannerValue"), extendConfig({}, controller.args[2], "lblBannerValue"));
        var lblTarget = new kony.ui.Label(extendConfig({
            "id": "lblTarget",
            "isVisible": true,
            "left": "0px",
            "skin": "sknlbl333333LatoSemiBold13px",
            "text": "TARGET URL - CTA Label (Right)",
            "top": "20dp",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, controller.args[0], "lblTarget"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblTarget"), extendConfig({}, controller.args[2], "lblTarget"));
        var lblTargetValue = new kony.ui.Label(extendConfig({
            "id": "lblTargetValue",
            "isVisible": true,
            "left": "0dp",
            "skin": "sknLblLato13px117eb0",
            "text": "Label",
            "top": "15dp",
            "width": "100%",
            "zIndex": 1
        }, controller.args[0], "lblTargetValue"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblTargetValue"), extendConfig({}, controller.args[2], "lblTargetValue"));
        var lblSeperator = new kony.ui.Label(extendConfig({
            "bottom": "0px",
            "height": "1px",
            "id": "lblSeperator",
            "isVisible": true,
            "left": "0px",
            "skin": "sknLblTableHeaderLine",
            "text": "-",
            "top": "20dp",
            "width": "100%",
            "zIndex": 1
        }, controller.args[0], "lblSeperator"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblSeperator"), extendConfig({}, controller.args[2], "lblSeperator"));
        var flxImageContainer = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
            "bottom": "20dp",
            "centerX": "50%",
            "clipBounds": true,
            "id": "flxImageContainer",
            "isVisible": true,
            "layoutType": kony.flex.FLOW_VERTICAL,
            "left": "0dp",
            "isModalContainer": false,
            "skin": "sknborderd7d9e0bgfff0px",
            "top": "30dp",
            "width": "560dp",
            "zIndex": 1
        }, controller.args[0], "flxImageContainer"), extendConfig({}, controller.args[1], "flxImageContainer"), extendConfig({}, controller.args[2], "flxImageContainer"));
        flxImageContainer.setDefaultUnit(kony.flex.DP);
        var flxHeaderFreeForm = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
            "clipBounds": true,
            "id": "flxHeaderFreeForm",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "left": "0dp",
            "isModalContainer": false,
            "skin": "slFbox",
            "top": "0dp",
            "width": "100%"
        }, controller.args[0], "flxHeaderFreeForm"), extendConfig({}, controller.args[1], "flxHeaderFreeForm"), extendConfig({}, controller.args[2], "flxHeaderFreeForm"));
        flxHeaderFreeForm.setDefaultUnit(kony.flex.DP);
        var flxHeader = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
            "clipBounds": true,
            "id": "flxHeader",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "left": "20dp",
            "isModalContainer": false,
            "right": "20dp",
            "skin": "slFbox",
            "top": "20dp",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, controller.args[0], "flxHeader"), extendConfig({}, controller.args[1], "flxHeader"), extendConfig({}, controller.args[2], "flxHeader"));
        flxHeader.setDefaultUnit(kony.flex.DP);
        var lblHeading = new kony.ui.Label(extendConfig({
            "id": "lblHeading",
            "isVisible": true,
            "left": "0dp",
            "right": "40dp",
            "skin": "sknLabel003e7540px",
            "text": "Take control of your financial future",
            "top": "0dp",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, controller.args[0], "lblHeading"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblHeading"), extendConfig({}, controller.args[2], "lblHeading"));
        var lblcross = new kony.ui.Label(extendConfig({
            "height": "20dp",
            "id": "lblcross",
            "isVisible": true,
            "right": "0dp",
            "skin": "sknfontIcon192b4b14px",
            "text": "",
            "top": "0dp",
            "width": "20dp",
            "zIndex": 1
        }, controller.args[0], "lblcross"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblcross"), extendConfig({}, controller.args[2], "lblcross"));
        flxHeader.add(lblHeading, lblcross);
        flxHeaderFreeForm.add(flxHeader);
        var flxMsgContainer = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
            "clipBounds": true,
            "id": "flxMsgContainer",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "left": "0dp",
            "isModalContainer": false,
            "skin": "slFbox",
            "top": "0dp",
            "width": "100%"
        }, controller.args[0], "flxMsgContainer"), extendConfig({}, controller.args[1], "flxMsgContainer"), extendConfig({}, controller.args[2], "flxMsgContainer"));
        flxMsgContainer.setDefaultUnit(kony.flex.DP);
        var lblmessage = new kony.ui.Label(extendConfig({
            "id": "lblmessage",
            "isVisible": true,
            "left": "20dp",
            "right": 60,
            "skin": "sknLabel42424220px",
            "text": "Get 1.95% APY on your savings with the new Infinity savings account.",
            "top": "0dp",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, controller.args[0], "lblmessage"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblmessage"), extendConfig({}, controller.args[2], "lblmessage"));
        flxMsgContainer.add(lblmessage);
        var imgCampaign = new kony.ui.Image2(extendConfig({
            "height": "323dp",
            "id": "imgCampaign",
            "isVisible": true,
            "left": "0dp",
            "skin": "slImage",
            "src": "imagedrag.png",
            "top": "20dp",
            "width": "100%",
            "zIndex": 1
        }, controller.args[0], "imgCampaign"), extendConfig({
            "imageScaleMode": constants.IMAGE_SCALE_MODE_MAINTAIN_ASPECT_RATIO,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "imgCampaign"), extendConfig({}, controller.args[2], "imgCampaign"));
        var flxButtons = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
            "bottom": "20dp",
            "clipBounds": true,
            "id": "flxButtons",
            "isVisible": true,
            "layoutType": kony.flex.FLOW_HORIZONTAL,
            "reverseLayoutDirection": true,
            "left": "0dp",
            "isModalContainer": false,
            "skin": "slFbox",
            "top": "20dp",
            "width": "100%",
            "zIndex": 1
        }, controller.args[0], "flxButtons"), extendConfig({}, controller.args[1], "flxButtons"), extendConfig({}, controller.args[2], "flxButtons"));
        flxButtons.setDefaultUnit(kony.flex.DP);
        var btnYes = new kony.ui.Button(extendConfig({
            "height": "40dp",
            "id": "btnYes",
            "isVisible": true,
            "right": "20dp",
            "skin": "defBtnNormal",
            "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmAdManagement.primary\")",
            "top": "0dp",
            "width": "30%",
            "zIndex": 1
        }, controller.args[0], "btnYes"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_CENTER,
            "displayText": true,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "btnYes"), extendConfig({}, controller.args[2], "btnYes"));
        var btnNo = new kony.ui.Button(extendConfig({
            "height": "40dp",
            "id": "btnNo",
            "isVisible": true,
            "right": "20dp",
            "skin": "defBtnNormal",
            "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmAdManagement.ReadLater\")",
            "top": "0dp",
            "width": "30%",
            "zIndex": 1
        }, controller.args[0], "btnNo"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_CENTER,
            "displayText": true,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "btnNo"), extendConfig({}, controller.args[2], "btnNo"));
        flxButtons.add(btnYes, btnNo);
        flxImageContainer.add(flxHeaderFreeForm, flxMsgContainer, imgCampaign, flxButtons);
        flxContent.add(lblBanner, lblBannerValue, lblTarget, lblTargetValue, lblSeperator, flxImageContainer);
        viewPopupAdDetails.add(lblNopopupDatafound, flxContent);
        return viewPopupAdDetails;
    }
})