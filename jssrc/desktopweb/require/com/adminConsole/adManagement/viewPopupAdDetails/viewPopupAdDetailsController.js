define("com/adminConsole/adManagement/viewPopupAdDetails/userviewPopupAdDetailsController", function() {
    return {};
});
define("com/adminConsole/adManagement/viewPopupAdDetails/viewPopupAdDetailsControllerActions", {
    /*
          This is an auto generated file and any modifications to it may result in corruption of the action sequence.
        */
});
define("com/adminConsole/adManagement/viewPopupAdDetails/viewPopupAdDetailsController", ["com/adminConsole/adManagement/viewPopupAdDetails/userviewPopupAdDetailsController", "com/adminConsole/adManagement/viewPopupAdDetails/viewPopupAdDetailsControllerActions"], function() {
    var controller = require("com/adminConsole/adManagement/viewPopupAdDetails/userviewPopupAdDetailsController");
    var actions = require("com/adminConsole/adManagement/viewPopupAdDetails/viewPopupAdDetailsControllerActions");
    for (var key in actions) {
        controller[key] = actions[key];
    }
    return controller;
});
