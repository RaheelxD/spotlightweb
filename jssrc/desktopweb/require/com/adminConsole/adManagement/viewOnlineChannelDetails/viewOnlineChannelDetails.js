define(function() {
    return function(controller) {
        var viewOnlineChannelDetails = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
            "clipBounds": true,
            "isMaster": true,
            "id": "viewOnlineChannelDetails",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "left": "0dp",
            "isModalContainer": false,
            "skin": "slFbox",
            "top": "0dp",
            "width": "100%"
        }, controller.args[0], "viewOnlineChannelDetails"), extendConfig({}, controller.args[1], "viewOnlineChannelDetails"), extendConfig({}, controller.args[2], "viewOnlineChannelDetails"));
        viewOnlineChannelDetails.setDefaultUnit(kony.flex.DP);
        var flxOnlineSection = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
            "clipBounds": true,
            "id": "flxOnlineSection",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "left": "20dp",
            "isModalContainer": false,
            "right": "20dp",
            "skin": "sknFlxbgFFFFFFbdrD7D9E0rd3px",
            "top": "10dp",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, controller.args[0], "flxOnlineSection"), extendConfig({}, controller.args[1], "flxOnlineSection"), extendConfig({}, controller.args[2], "flxOnlineSection"));
        flxOnlineSection.setDefaultUnit(kony.flex.DP);
        var flxHeader = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "clipBounds": true,
            "height": "40dp",
            "id": "flxHeader",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "left": "0dp",
            "isModalContainer": false,
            "skin": "sknf5f6f8border0px",
            "top": "0dp",
            "width": "100%"
        }, controller.args[0], "flxHeader"), extendConfig({}, controller.args[1], "flxHeader"), extendConfig({}, controller.args[2], "flxHeader"));
        flxHeader.setDefaultUnit(kony.flex.DP);
        var flxChannelArrow = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "centerY": "50%",
            "clipBounds": true,
            "height": "15dp",
            "id": "flxChannelArrow",
            "isVisible": true,
            "layoutType": kony.flex.FLOW_HORIZONTAL,
            "left": "20dp",
            "isModalContainer": false,
            "onClick": controller.AS_FlexContainer_i2856c0af5344ff7a89d232e263c2efb,
            "skin": "sknCursor",
            "width": "15dp"
        }, controller.args[0], "flxChannelArrow"), extendConfig({}, controller.args[1], "flxChannelArrow"), extendConfig({}, controller.args[2], "flxChannelArrow"));
        flxChannelArrow.setDefaultUnit(kony.flex.DP);
        var lblChannelArrow = new kony.ui.Label(extendConfig({
            "height": "15dp",
            "id": "lblChannelArrow",
            "isVisible": true,
            "left": "0dp",
            "skin": "sknfontIcon192b4b14px",
            "text": "",
            "top": "0dp",
            "width": "15dp",
            "zIndex": 1
        }, controller.args[0], "lblChannelArrow"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblChannelArrow"), extendConfig({}, controller.args[2], "lblChannelArrow"));
        flxChannelArrow.add(lblChannelArrow);
        var lblHeader = new kony.ui.Label(extendConfig({
            "centerY": "50%",
            "id": "lblHeader",
            "isVisible": true,
            "left": "45dp",
            "skin": "sknLbl16pxLato192B45",
            "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmAdManagement.NativeMobileApp\")",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, controller.args[0], "lblHeader"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblHeader"), extendConfig({}, controller.args[2], "lblHeader"));
        flxHeader.add(flxChannelArrow, lblHeader);
        var flxContent = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
            "clipBounds": true,
            "id": "flxContent",
            "isVisible": true,
            "layoutType": kony.flex.FLOW_VERTICAL,
            "left": 0,
            "isModalContainer": false,
            "skin": "slFbox",
            "top": "40dp",
            "width": kony.flex.USE_PREFFERED_SIZE
        }, controller.args[0], "flxContent"), extendConfig({}, controller.args[1], "flxContent"), extendConfig({}, controller.args[2], "flxContent"));
        flxContent.setDefaultUnit(kony.flex.DP);
        flxContent.add();
        flxOnlineSection.add(flxHeader, flxContent);
        viewOnlineChannelDetails.add(flxOnlineSection);
        return viewOnlineChannelDetails;
    }
})