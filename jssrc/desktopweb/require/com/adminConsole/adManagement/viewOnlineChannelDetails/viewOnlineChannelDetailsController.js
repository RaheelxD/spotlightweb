define("com/adminConsole/adManagement/viewOnlineChannelDetails/userviewOnlineChannelDetailsController", function() {
    return {
        showOrHideContent: function(parentObject) {
            let isHide = parentObject.lblChannelArrow.text === "\ue915";
            parentObject.lblChannelArrow.text = isHide ? "\ue922" : "\ue915";
            parentObject.flxContent.isVisible = !isHide;
            parentObject.forceLayout();
        },
    };
});
define("com/adminConsole/adManagement/viewOnlineChannelDetails/viewOnlineChannelDetailsControllerActions", {
    /*
        This is an auto generated file and any modifications to it may result in corruption of the action sequence.
    */
    AS_FlexContainer_i2856c0af5344ff7a89d232e263c2efb: function AS_FlexContainer_i2856c0af5344ff7a89d232e263c2efb(eventobject) {
        var self = this;
        return self.showOrHideContent.call(this, null);
    }
});
define("com/adminConsole/adManagement/viewOnlineChannelDetails/viewOnlineChannelDetailsController", ["com/adminConsole/adManagement/viewOnlineChannelDetails/userviewOnlineChannelDetailsController", "com/adminConsole/adManagement/viewOnlineChannelDetails/viewOnlineChannelDetailsControllerActions"], function() {
    var controller = require("com/adminConsole/adManagement/viewOnlineChannelDetails/userviewOnlineChannelDetailsController");
    var actions = require("com/adminConsole/adManagement/viewOnlineChannelDetails/viewOnlineChannelDetailsControllerActions");
    for (var key in actions) {
        controller[key] = actions[key];
    }
    return controller;
});
