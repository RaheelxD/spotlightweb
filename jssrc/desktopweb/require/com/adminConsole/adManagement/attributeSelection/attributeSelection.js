define(function() {
    return function(controller) {
        var attributeSelection = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "clipBounds": true,
            "isMaster": true,
            "height": "81dp",
            "id": "attributeSelection",
            "isVisible": true,
            "layoutType": kony.flex.FLOW_VERTICAL,
            "left": 0,
            "isModalContainer": false,
            "skin": "slFbox",
            "top": "0dp",
            "width": kony.flex.USE_PREFFERED_SIZE
        }, controller.args[0], "attributeSelection"), extendConfig({}, controller.args[1], "attributeSelection"), extendConfig({
            "hoverSkin": "sknFlxf9fbfd1000"
        }, controller.args[2], "attributeSelection"));
        attributeSelection.setDefaultUnit(kony.flex.DP);
        var flxAttributeSelection = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "clipBounds": true,
            "height": "80dp",
            "id": "flxAttributeSelection",
            "isVisible": true,
            "layoutType": kony.flex.FLOW_HORIZONTAL,
            "left": "0",
            "isModalContainer": false,
            "skin": "slFbox",
            "top": "0dp",
            "width": "100%",
            "zIndex": 1
        }, controller.args[0], "flxAttributeSelection"), extendConfig({}, controller.args[1], "flxAttributeSelection"), extendConfig({}, controller.args[2], "flxAttributeSelection"));
        flxAttributeSelection.setDefaultUnit(kony.flex.DP);
        var flxName = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "clipBounds": true,
            "height": "100%",
            "id": "flxName",
            "isVisible": true,
            "layoutType": kony.flex.FLOW_HORIZONTAL,
            "isModalContainer": false,
            "skin": "slFbox",
            "width": "31%",
            "zIndex": 1
        }, controller.args[0], "flxName"), extendConfig({}, controller.args[1], "flxName"), extendConfig({}, controller.args[2], "flxName"));
        flxName.setDefaultUnit(kony.flex.DP);
        var fonticonCheckBox = new kony.ui.Label(extendConfig({
            "height": "25dp",
            "id": "fonticonCheckBox",
            "isVisible": true,
            "skin": "sknFontIconCheckBoxUnselected",
            "text": "",
            "top": "28dp",
            "width": "25dp",
            "zIndex": 2
        }, controller.args[0], "fonticonCheckBox"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "fonticonCheckBox"), extendConfig({}, controller.args[2], "fonticonCheckBox"));
        var lblName = new kony.ui.Label(extendConfig({
            "id": "lblName",
            "isVisible": true,
            "left": "10dp",
            "right": "20dp",
            "skin": "sknLbl485C75LatoRegular13Px",
            "text": "Attribute Name",
            "top": "32dp",
            "width": "85%",
            "zIndex": 1
        }, controller.args[0], "lblName"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblName"), extendConfig({}, controller.args[2], "lblName"));
        var lblId = new kony.ui.Label(extendConfig({
            "centerY": "50%",
            "id": "lblId",
            "isVisible": false,
            "left": "30dp",
            "right": "10dp",
            "skin": "sknLbl485C75LatoRegular13Px",
            "text": "Attribute Name",
            "top": "19dp",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, controller.args[0], "lblId"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblId"), extendConfig({}, controller.args[2], "lblId"));
        var lblType = new kony.ui.Label(extendConfig({
            "centerY": "50%",
            "id": "lblType",
            "isVisible": false,
            "left": "30dp",
            "right": "10dp",
            "skin": "sknLbl485C75LatoRegular13Px",
            "text": "Attribute Name",
            "top": "19dp",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, controller.args[0], "lblType"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblType"), extendConfig({}, controller.args[2], "lblType"));
        var lblRange = new kony.ui.Label(extendConfig({
            "centerY": "50%",
            "id": "lblRange",
            "isVisible": false,
            "left": "30dp",
            "right": "10dp",
            "skin": "sknLbl485C75LatoRegular13Px",
            "text": "Attribute Name",
            "top": "19dp",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, controller.args[0], "lblRange"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblRange"), extendConfig({}, controller.args[2], "lblRange"));
        flxName.add(fonticonCheckBox, lblName, lblId, lblType, lblRange);
        var lstCriteria = new kony.ui.ListBox(extendConfig({
            "focusSkin": "defListBoxFocus",
            "height": "40dp",
            "id": "lstCriteria",
            "isVisible": true,
            "left": "2%",
            "masterData": [
                ["lb1", "Placeholder One"],
                ["lb2", "Placeholder Two"],
                ["lb3", "Placeholder Three"]
            ],
            "skin": "sknLbxborderd7d9e03pxradius",
            "top": "20dp",
            "width": "31%",
            "zIndex": 1
        }, controller.args[0], "lstCriteria"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [1, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lstCriteria"), extendConfig({
            "multiSelect": false
        }, controller.args[2], "lstCriteria"));
        var flxValues = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
            "clipBounds": true,
            "id": "flxValues",
            "isVisible": true,
            "layoutType": kony.flex.FLOW_VERTICAL,
            "left": "2%",
            "isModalContainer": false,
            "skin": "slFbox",
            "top": "20dp",
            "width": "31%",
            "zIndex": 1
        }, controller.args[0], "flxValues"), extendConfig({}, controller.args[1], "flxValues"), extendConfig({}, controller.args[2], "flxValues"));
        flxValues.setDefaultUnit(kony.flex.DP);
        var txtValue = new kony.ui.TextBox2(extendConfig({
            "autoCapitalize": constants.TEXTBOX_AUTO_CAPITALIZE_NONE,
            "focusSkin": "defTextBoxFocus",
            "height": "40dp",
            "id": "txtValue",
            "isVisible": true,
            "keyBoardStyle": constants.TEXTBOX_KEY_BOARD_STYLE_DEFAULT,
            "left": "0dp",
            "placeholder": "Input Value",
            "secureTextEntry": false,
            "skin": "txtD7d9e0",
            "textInputMode": constants.TEXTBOX_INPUT_MODE_ANY,
            "top": "0dp",
            "width": "100%",
            "zIndex": 1
        }, controller.args[0], "txtValue"), extendConfig({
            "containerHeightMode": constants.TEXTBOX_FONT_METRICS_DRIVEN_HEIGHT,
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [3, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "txtValue"), extendConfig({
            "autoCorrect": false,
            "placeholderSkin": "defTextBoxPlaceholder"
        }, controller.args[2], "txtValue"));
        var lstValues = new kony.ui.ListBox(extendConfig({
            "focusSkin": "defListBoxFocus",
            "height": "40dp",
            "id": "lstValues",
            "isVisible": false,
            "left": "0dp",
            "masterData": [
                ["lb1", "Placeholder One"],
                ["lb2", "Placeholder Two"],
                ["lb3", "Placeholder Three"]
            ],
            "skin": "sknLbxborderd7d9e03pxradius",
            "top": "0dp",
            "width": "100%",
            "zIndex": 1
        }, controller.args[0], "lstValues"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [3, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lstValues"), extendConfig({
            "multiSelect": false
        }, controller.args[2], "lstValues"));
        var attrError = new com.adminConsole.loans.applications.errorMsg(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
            "id": "attrError",
            "isVisible": false,
            "layoutType": kony.flex.FLOW_HORIZONTAL,
            "left": "0dp",
            "masterType": constants.MASTER_TYPE_DEFAULT,
            "isModalContainer": false,
            "skin": "slFbox",
            "top": "0dp",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "overrides": {
                "errorMsg": {
                    "isVisible": false,
                    "top": "0dp"
                },
                "lblErrorText": {
                    "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmAdManagement.ErrorImageSourceURL\")"
                }
            }
        }, controller.args[0], "attrError"), extendConfig({
            "overrides": {}
        }, controller.args[1], "attrError"), extendConfig({
            "overrides": {}
        }, controller.args[2], "attrError"));
        flxValues.add(txtValue, lstValues, attrError);
        flxAttributeSelection.add(flxName, lstCriteria, flxValues);
        var lblLine = new kony.ui.Label(extendConfig({
            "bottom": "0dp",
            "height": "1dp",
            "id": "lblLine",
            "isVisible": true,
            "left": "0dp",
            "right": "0dp",
            "skin": "sknlblSeperatorD7D9E0",
            "text": "l",
            "width": "100%",
            "zIndex": 1
        }, controller.args[0], "lblLine"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblLine"), extendConfig({}, controller.args[2], "lblLine"));
        attributeSelection.add(flxAttributeSelection, lblLine);
        return attributeSelection;
    }
})