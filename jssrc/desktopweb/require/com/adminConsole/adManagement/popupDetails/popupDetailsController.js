define("com/adminConsole/adManagement/popupDetails/userpopupDetailsController", function() {
    return {
        popupDetailsPreShow: function() {
            this.setFlowActions();
        },
        showPopupDetails: function() {
            //setting flx details visibility off and changing icon to collapse
            if (this.view.flxPopupDetailsBody.isVisible) {
                this.view.flxPopupDetailsBody.setVisibility(false);
                this.view.fonticonArrow.text = "\ue922";
            } else {
                this.view.flxPopupDetailsBody.setVisibility(true);
                this.view.fonticonArrow.text = "\ue915";
            }
        },
        setFlowActions: function() {
            var scopeObj = this;
            this.view.flxDropdown.onClick = function() {
                scopeObj.showPopupDetails();
            };
        }
    };
});
define("com/adminConsole/adManagement/popupDetails/popupDetailsControllerActions", {
    /*
        This is an auto generated file and any modifications to it may result in corruption of the action sequence.
    */
    AS_FlexContainer_d4976f8044ca4ead83c3ffed9ebf6288: function AS_FlexContainer_d4976f8044ca4ead83c3ffed9ebf6288(eventobject) {
        var self = this;
        return self.popupDetailsPreShow.call(this);
    }
});
define("com/adminConsole/adManagement/popupDetails/popupDetailsController", ["com/adminConsole/adManagement/popupDetails/userpopupDetailsController", "com/adminConsole/adManagement/popupDetails/popupDetailsControllerActions"], function() {
    var controller = require("com/adminConsole/adManagement/popupDetails/userpopupDetailsController");
    var actions = require("com/adminConsole/adManagement/popupDetails/popupDetailsControllerActions");
    for (var key in actions) {
        controller[key] = actions[key];
    }
    return controller;
});
