define(function() {
    return function(controller) {
        var popupDetails = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
            "clipBounds": true,
            "isMaster": true,
            "id": "popupDetails",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "left": "0dp",
            "isModalContainer": false,
            "preShow": function(eventobject) {
                controller.AS_FlexContainer_d4976f8044ca4ead83c3ffed9ebf6288(eventobject);
            },
            "skin": "sknFlxbgFFFFFFbdrD7D9E0rd4px",
            "top": "0dp",
            "width": "100%"
        }, controller.args[0], "popupDetails"), extendConfig({}, controller.args[1], "popupDetails"), extendConfig({}, controller.args[2], "popupDetails"));
        popupDetails.setDefaultUnit(kony.flex.DP);
        var flxPopupDetailsHeader = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "clipBounds": true,
            "height": "50dp",
            "id": "flxPopupDetailsHeader",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "left": "0dp",
            "isModalContainer": false,
            "skin": "sknflxf5f6f8Op100",
            "top": "0dp",
            "width": "100%"
        }, controller.args[0], "flxPopupDetailsHeader"), extendConfig({}, controller.args[1], "flxPopupDetailsHeader"), extendConfig({}, controller.args[2], "flxPopupDetailsHeader"));
        flxPopupDetailsHeader.setDefaultUnit(kony.flex.DP);
        var flxDropdown = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "centerY": "50%",
            "clipBounds": true,
            "height": "14dp",
            "id": "flxDropdown",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "left": "15dp",
            "isModalContainer": false,
            "onClick": controller.AS_FlexContainer_aeb8168ea3624740bf06d583316c5596,
            "skin": "sknCursor",
            "width": "14dp",
            "zIndex": 2
        }, controller.args[0], "flxDropdown"), extendConfig({}, controller.args[1], "flxDropdown"), extendConfig({}, controller.args[2], "flxDropdown"));
        flxDropdown.setDefaultUnit(kony.flex.DP);
        var fonticonArrow = new kony.ui.Label(extendConfig({
            "centerX": "50%",
            "centerY": "50%",
            "height": "14dp",
            "id": "fonticonArrow",
            "isVisible": true,
            "skin": "sknfontIconDescRightArrow14px",
            "text": "",
            "width": "14dp",
            "zIndex": 1
        }, controller.args[0], "fonticonArrow"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "fonticonArrow"), extendConfig({}, controller.args[2], "fonticonArrow"));
        flxDropdown.add(fonticonArrow);
        var flxPopupTypeHeader = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
            "clipBounds": true,
            "id": "flxPopupTypeHeader",
            "isVisible": true,
            "layoutType": kony.flex.FLOW_HORIZONTAL,
            "left": "38dp",
            "isModalContainer": false,
            "skin": "slFbox",
            "top": "15dp",
            "width": "170dp"
        }, controller.args[0], "flxPopupTypeHeader"), extendConfig({}, controller.args[1], "flxPopupTypeHeader"), extendConfig({}, controller.args[2], "flxPopupTypeHeader"));
        flxPopupTypeHeader.setDefaultUnit(kony.flex.DP);
        var lblPopupTypeHeader = new kony.ui.Label(extendConfig({
            "id": "lblPopupTypeHeader",
            "isVisible": true,
            "left": "0dp",
            "skin": "sknLbl192B45LatoRegular14px",
            "text": "Web Popup Ad Banner",
            "top": "0dp",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, controller.args[0], "lblPopupTypeHeader"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblPopupTypeHeader"), extendConfig({}, controller.args[2], "lblPopupTypeHeader"));
        var fontIconInfo = new kony.ui.Label(extendConfig({
            "id": "fontIconInfo",
            "isVisible": true,
            "left": "10dp",
            "skin": "sknfontIcon33333316px",
            "text": "",
            "top": "0dp",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, controller.args[0], "fontIconInfo"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "fontIconInfo"), extendConfig({}, controller.args[2], "fontIconInfo"));
        flxPopupTypeHeader.add(lblPopupTypeHeader, fontIconInfo);
        var btnPreview = new kony.ui.Button(extendConfig({
            "centerY": "50%",
            "height": "22dp",
            "id": "btnPreview",
            "isVisible": true,
            "right": "12dp",
            "skin": "sknbtnf7f7faLatoRegular12Px485c75Border1px485C75Radius20px",
            "text": "Preview",
            "width": "70dp",
            "zIndex": 1
        }, controller.args[0], "btnPreview"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_CENTER,
            "displayText": true,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "btnPreview"), extendConfig({
            "hoverSkin": "sknbtnffffffLatoRegular12Px485c75Border1px485C75Radius20px"
        }, controller.args[2], "btnPreview"));
        flxPopupDetailsHeader.add(flxDropdown, flxPopupTypeHeader, btnPreview);
        var flxPopupDetailsBody = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
            "clipBounds": true,
            "id": "flxPopupDetailsBody",
            "isVisible": true,
            "layoutType": kony.flex.FLOW_VERTICAL,
            "left": "0dp",
            "isModalContainer": false,
            "skin": "slFbox",
            "top": "50dp",
            "width": "100%"
        }, controller.args[0], "flxPopupDetailsBody"), extendConfig({}, controller.args[1], "flxPopupDetailsBody"), extendConfig({}, controller.args[2], "flxPopupDetailsBody"));
        flxPopupDetailsBody.setDefaultUnit(kony.flex.DP);
        var flxDispalyHideBanner = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
            "clipBounds": true,
            "id": "flxDispalyHideBanner",
            "isVisible": true,
            "layoutType": kony.flex.FLOW_VERTICAL,
            "left": "0dp",
            "isModalContainer": false,
            "right": "0dp",
            "skin": "slFbox",
            "top": "20dp",
            "width": "100%"
        }, controller.args[0], "flxDispalyHideBanner"), extendConfig({}, controller.args[1], "flxDispalyHideBanner"), extendConfig({}, controller.args[2], "flxDispalyHideBanner"));
        flxDispalyHideBanner.setDefaultUnit(kony.flex.DP);
        var flxDisplayAds = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
            "clipBounds": true,
            "id": "flxDisplayAds",
            "isVisible": true,
            "layoutType": kony.flex.FLOW_HORIZONTAL,
            "left": "20dp",
            "isModalContainer": false,
            "skin": "slFbox",
            "top": "0dp",
            "width": "150dp"
        }, controller.args[0], "flxDisplayAds"), extendConfig({}, controller.args[1], "flxDisplayAds"), extendConfig({}, controller.args[2], "flxDisplayAds"));
        flxDisplayAds.setDefaultUnit(kony.flex.DP);
        var lblDisplayAds = new kony.ui.Label(extendConfig({
            "id": "lblDisplayAds",
            "isVisible": true,
            "left": "0dp",
            "skin": "sknLbl485C75LatoRegular13Px",
            "text": "Display Ads",
            "top": "3dp",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, controller.args[0], "lblDisplayAds"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblDisplayAds"), extendConfig({}, controller.args[2], "lblDisplayAds"));
        var switchAds = new kony.ui.Switch(extendConfig({
            "height": "20dp",
            "id": "switchAds",
            "isVisible": true,
            "left": "20dp",
            "selectedIndex": 0,
            "skin": "sknSwitchServiceManagement",
            "top": "0",
            "width": "35dp",
            "zIndex": 1
        }, controller.args[0], "switchAds"), extendConfig({
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "switchAds"), extendConfig({}, controller.args[2], "switchAds"));
        flxDisplayAds.add(lblDisplayAds, switchAds);
        var flxHideAdContainer = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
            "clipBounds": true,
            "id": "flxHideAdContainer",
            "isVisible": false,
            "layoutType": kony.flex.FREE_FORM,
            "left": "0dp",
            "isModalContainer": false,
            "skin": "slFbox",
            "top": "0dp",
            "width": kony.flex.USE_PREFFERED_SIZE
        }, controller.args[0], "flxHideAdContainer"), extendConfig({}, controller.args[1], "flxHideAdContainer"), extendConfig({}, controller.args[2], "flxHideAdContainer"));
        flxHideAdContainer.setDefaultUnit(kony.flex.DP);
        var flxHideAd = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "clipBounds": true,
            "height": "150dp",
            "id": "flxHideAd",
            "isVisible": true,
            "layoutType": kony.flex.FLOW_VERTICAL,
            "left": "0dp",
            "isModalContainer": false,
            "skin": "slFbox",
            "top": "20dp",
            "width": kony.flex.USE_PREFFERED_SIZE
        }, controller.args[0], "flxHideAd"), extendConfig({}, controller.args[1], "flxHideAd"), extendConfig({}, controller.args[2], "flxHideAd"));
        flxHideAd.setDefaultUnit(kony.flex.DP);
        var lblNoAd = new kony.ui.Label(extendConfig({
            "centerX": "50%",
            "height": "30dp",
            "id": "lblNoAd",
            "isVisible": true,
            "skin": "sknLblIcon485c7535px",
            "text": "",
            "top": "50dp",
            "width": "35dp",
            "zIndex": 1
        }, controller.args[0], "lblNoAd"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblNoAd"), extendConfig({}, controller.args[2], "lblNoAd"));
        var lblNoImageConfig = new kony.ui.Label(extendConfig({
            "centerX": "50%",
            "id": "lblNoImageConfig",
            "isVisible": true,
            "left": "0dp",
            "skin": "sknLblLato485c7513px",
            "text": "No Web Popup Ad Banner configuration ",
            "top": "10dp",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, controller.args[0], "lblNoImageConfig"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblNoImageConfig"), extendConfig({}, controller.args[2], "lblNoImageConfig"));
        flxHideAd.add(lblNoAd, lblNoImageConfig);
        flxHideAdContainer.add(flxHideAd);
        flxDispalyHideBanner.add(flxDisplayAds, flxHideAdContainer);
        var flxPopupDetails = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
            "clipBounds": true,
            "id": "flxPopupDetails",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "left": "0dp",
            "isModalContainer": false,
            "skin": "slFbox",
            "top": "0dp",
            "width": "100%"
        }, controller.args[0], "flxPopupDetails"), extendConfig({}, controller.args[1], "flxPopupDetails"), extendConfig({}, controller.args[2], "flxPopupDetails"));
        flxPopupDetails.setDefaultUnit(kony.flex.DP);
        var flxPopupDetailsInner = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
            "clipBounds": true,
            "id": "flxPopupDetailsInner",
            "isVisible": true,
            "layoutType": kony.flex.FLOW_VERTICAL,
            "left": "15dp",
            "isModalContainer": false,
            "right": "15dp",
            "skin": "slFbox",
            "top": "0dp",
            "zIndex": 2
        }, controller.args[0], "flxPopupDetailsInner"), extendConfig({}, controller.args[1], "flxPopupDetailsInner"), extendConfig({}, controller.args[2], "flxPopupDetailsInner"));
        flxPopupDetailsInner.setDefaultUnit(kony.flex.DP);
        var flxCopyBannerDetails = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "clipBounds": true,
            "height": "25dp",
            "id": "flxCopyBannerDetails",
            "isVisible": true,
            "layoutType": kony.flex.FLOW_HORIZONTAL,
            "left": "0dp",
            "isModalContainer": false,
            "skin": "slFbox",
            "top": "20dp",
            "width": "100%"
        }, controller.args[0], "flxCopyBannerDetails"), extendConfig({}, controller.args[1], "flxCopyBannerDetails"), extendConfig({}, controller.args[2], "flxCopyBannerDetails"));
        flxCopyBannerDetails.setDefaultUnit(kony.flex.DP);
        var lblCopyBannerDetails = new kony.ui.Label(extendConfig({
            "centerY": "50%",
            "id": "lblCopyBannerDetails",
            "isVisible": true,
            "left": "0dp",
            "skin": "sknLblLato485c7513px",
            "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmAdManagement.copyWebPopupBannerDetails\")",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, controller.args[0], "lblCopyBannerDetails"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblCopyBannerDetails"), extendConfig({}, controller.args[2], "lblCopyBannerDetails"));
        var flxSelectOption = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
            "clipBounds": true,
            "id": "flxSelectOption",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "left": "15dp",
            "isModalContainer": false,
            "skin": "hoverhandSkin",
            "top": "0dp",
            "width": "25dp"
        }, controller.args[0], "flxSelectOption"), extendConfig({}, controller.args[1], "flxSelectOption"), extendConfig({}, controller.args[2], "flxSelectOption"));
        flxSelectOption.setDefaultUnit(kony.flex.DP);
        var fontIconSelectOption = new kony.ui.Label(extendConfig({
            "id": "fontIconSelectOption",
            "isVisible": true,
            "left": "0dp",
            "skin": "sknFontIconCheckBoxSelected",
            "text": "",
            "top": "0dp",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, controller.args[0], "fontIconSelectOption"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "fontIconSelectOption"), extendConfig({}, controller.args[2], "fontIconSelectOption"));
        flxSelectOption.add(fontIconSelectOption);
        flxCopyBannerDetails.add(lblCopyBannerDetails, flxSelectOption);
        var lblBannerConfig = new kony.ui.Label(extendConfig({
            "id": "lblBannerConfig",
            "isVisible": true,
            "left": "0dp",
            "skin": "sknLbl192B45LatoRegular14px",
            "text": "Banner Configuration",
            "top": "20dp",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 2
        }, controller.args[0], "lblBannerConfig"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblBannerConfig"), extendConfig({}, controller.args[2], "lblBannerConfig"));
        var flxBannerHeadline = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
            "clipBounds": true,
            "id": "flxBannerHeadline",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "left": "0dp",
            "isModalContainer": false,
            "skin": "slFbox",
            "top": "20dp",
            "width": "100%"
        }, controller.args[0], "flxBannerHeadline"), extendConfig({}, controller.args[1], "flxBannerHeadline"), extendConfig({}, controller.args[2], "flxBannerHeadline"));
        flxBannerHeadline.setDefaultUnit(kony.flex.DP);
        var lblBannerHeadline = new kony.ui.Label(extendConfig({
            "id": "lblBannerHeadline",
            "isVisible": true,
            "left": "0dp",
            "skin": "sknLblLato485c7513px",
            "text": "Banner Headline",
            "top": "0dp",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, controller.args[0], "lblBannerHeadline"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblBannerHeadline"), extendConfig({}, controller.args[2], "lblBannerHeadline"));
        var lblBannerHeadlineSize = new kony.ui.Label(extendConfig({
            "id": "lblBannerHeadlineSize",
            "isVisible": false,
            "right": "0dp",
            "skin": "sknLblLato485c7513px",
            "text": "0/50",
            "top": "0dp",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, controller.args[0], "lblBannerHeadlineSize"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblBannerHeadlineSize"), extendConfig({}, controller.args[2], "lblBannerHeadlineSize"));
        flxBannerHeadline.add(lblBannerHeadline, lblBannerHeadlineSize);
        var txtBannerHeadline = new kony.ui.TextBox2(extendConfig({
            "autoCapitalize": constants.TEXTBOX_AUTO_CAPITALIZE_NONE,
            "focusSkin": "skntxtbxDetailsFocus14px",
            "height": "40dp",
            "id": "txtBannerHeadline",
            "isVisible": true,
            "keyBoardStyle": constants.TEXTBOX_KEY_BOARD_STYLE_DEFAULT,
            "left": "0",
            "maxTextLength": 15,
            "placeholder": "Enter Banner Headline  ",
            "secureTextEntry": false,
            "skin": "txtD7d9e0",
            "textInputMode": constants.TEXTBOX_INPUT_MODE_ANY,
            "top": "15dp",
            "width": "100%",
            "zIndex": 1
        }, controller.args[0], "txtBannerHeadline"), extendConfig({
            "containerHeightMode": constants.TEXTBOX_FONT_METRICS_DRIVEN_HEIGHT,
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [2, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "txtBannerHeadline"), extendConfig({
            "autoCorrect": false,
            "placeholderSkin": "sknTextGreyb2bdcd"
        }, controller.args[2], "txtBannerHeadline"));
        var BannerHeadlineError = new com.adminConsole.loans.applications.errorMsg(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
            "id": "BannerHeadlineError",
            "isVisible": false,
            "layoutType": kony.flex.FLOW_HORIZONTAL,
            "left": "0dp",
            "masterType": constants.MASTER_TYPE_DEFAULT,
            "isModalContainer": false,
            "skin": "slFbox",
            "top": "10dp",
            "width": "100%",
            "overrides": {
                "errorMsg": {
                    "isVisible": false,
                    "width": "100%"
                },
                "lblErrorText": {
                    "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmAdManagement.ErrorBannerHeadlineEmpty\")"
                }
            }
        }, controller.args[0], "BannerHeadlineError"), extendConfig({
            "overrides": {}
        }, controller.args[1], "BannerHeadlineError"), extendConfig({
            "overrides": {}
        }, controller.args[2], "BannerHeadlineError"));
        var flxBannerDescription = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
            "clipBounds": true,
            "id": "flxBannerDescription",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "left": "0dp",
            "isModalContainer": false,
            "skin": "slFbox",
            "top": "26dp",
            "width": "100%"
        }, controller.args[0], "flxBannerDescription"), extendConfig({}, controller.args[1], "flxBannerDescription"), extendConfig({}, controller.args[2], "flxBannerDescription"));
        flxBannerDescription.setDefaultUnit(kony.flex.DP);
        var lblBannerDescription = new kony.ui.Label(extendConfig({
            "id": "lblBannerDescription",
            "isVisible": true,
            "left": "0dp",
            "skin": "sknLblLato485c7513px",
            "text": "Banner Description",
            "top": "0dp",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, controller.args[0], "lblBannerDescription"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblBannerDescription"), extendConfig({}, controller.args[2], "lblBannerDescription"));
        var lblBannerDescriptionSize = new kony.ui.Label(extendConfig({
            "id": "lblBannerDescriptionSize",
            "isVisible": false,
            "right": "0dp",
            "skin": "sknLblLato485c7513px",
            "text": "0/150",
            "top": "0dp",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, controller.args[0], "lblBannerDescriptionSize"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblBannerDescriptionSize"), extendConfig({}, controller.args[2], "lblBannerDescriptionSize"));
        flxBannerDescription.add(lblBannerDescription, lblBannerDescriptionSize);
        var txtBannerDescription = new kony.ui.TextArea2(extendConfig({
            "autoCapitalize": constants.TEXTAREA_AUTO_CAPITALIZE_NONE,
            "focusSkin": "sknFocus",
            "height": "70dp",
            "id": "txtBannerDescription",
            "isVisible": true,
            "keyBoardStyle": constants.TEXTAREA_KEY_BOARD_STYLE_DEFAULT,
            "left": "0dp",
            "maxTextLength": 50,
            "numberOfVisibleLines": 3,
            "placeholder": "Enter Banner Description",
            "skin": "skntxtAread7d9e0",
            "textInputMode": constants.TEXTAREA_INPUT_MODE_ANY,
            "top": "15dp",
            "width": "100%",
            "zIndex": 1
        }, controller.args[0], "txtBannerDescription"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_TOP_LEFT,
            "padding": [2, 2, 2, 2],
            "paddingInPixel": false
        }, controller.args[1], "txtBannerDescription"), extendConfig({
            "autoCorrect": false,
            "placeholderSkin": "defTextAreaFocus"
        }, controller.args[2], "txtBannerDescription"));
        var BannerDescriptionError = new com.adminConsole.loans.applications.errorMsg(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
            "id": "BannerDescriptionError",
            "isVisible": false,
            "layoutType": kony.flex.FLOW_HORIZONTAL,
            "left": "0dp",
            "masterType": constants.MASTER_TYPE_DEFAULT,
            "isModalContainer": false,
            "skin": "slFbox",
            "top": "10dp",
            "width": "100%",
            "overrides": {
                "errorMsg": {
                    "isVisible": false,
                    "width": "100%"
                },
                "lblErrorText": {
                    "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmAdManagement.ErrorBannerDescriptionEmpty\")"
                }
            }
        }, controller.args[0], "BannerDescriptionError"), extendConfig({
            "overrides": {}
        }, controller.args[1], "BannerDescriptionError"), extendConfig({
            "overrides": {}
        }, controller.args[2], "BannerDescriptionError"));
        var flxBannerImage = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
            "clipBounds": true,
            "id": "flxBannerImage",
            "isVisible": true,
            "layoutType": kony.flex.FLOW_HORIZONTAL,
            "left": "0dp",
            "isModalContainer": false,
            "skin": "slFbox",
            "top": "26dp",
            "width": "100%"
        }, controller.args[0], "flxBannerImage"), extendConfig({}, controller.args[1], "flxBannerImage"), extendConfig({}, controller.args[2], "flxBannerImage"));
        flxBannerImage.setDefaultUnit(kony.flex.DP);
        var lblBannerImage = new kony.ui.Label(extendConfig({
            "id": "lblBannerImage",
            "isVisible": true,
            "left": "0dp",
            "skin": "sknLblLato485c7513px",
            "text": "Banner Image",
            "top": "0dp",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, controller.args[0], "lblBannerImage"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblBannerImage"), extendConfig({}, controller.args[2], "lblBannerImage"));
        var flxBannerImgInfo = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "clipBounds": true,
            "height": "20dp",
            "id": "flxBannerImgInfo",
            "isVisible": true,
            "layoutType": kony.flex.FLOW_HORIZONTAL,
            "left": "20dp",
            "isModalContainer": false,
            "skin": "flxBorder5FB8AE1px",
            "top": "0",
            "width": "130dp"
        }, controller.args[0], "flxBannerImgInfo"), extendConfig({}, controller.args[1], "flxBannerImgInfo"), extendConfig({}, controller.args[2], "flxBannerImgInfo"));
        flxBannerImgInfo.setDefaultUnit(kony.flex.DP);
        var flxImageIndex = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "clipBounds": true,
            "height": "20dp",
            "id": "flxImageIndex",
            "isVisible": false,
            "layoutType": kony.flex.FLOW_HORIZONTAL,
            "left": "0dp",
            "isModalContainer": false,
            "skin": "sknflxBackground5FB8AE",
            "top": "0dp",
            "width": "30dp"
        }, controller.args[0], "flxImageIndex"), extendConfig({}, controller.args[1], "flxImageIndex"), extendConfig({}, controller.args[2], "flxImageIndex"));
        flxImageIndex.setDefaultUnit(kony.flex.DP);
        var lblImageIndex = new kony.ui.Label(extendConfig({
            "centerX": "50%",
            "centerY": "50%",
            "id": "lblImageIndex",
            "isVisible": true,
            "skin": "sknLblFFFFFLatoRegular12Px",
            "text": "1X",
            "top": "0dp",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, controller.args[0], "lblImageIndex"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_CENTER,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblImageIndex"), extendConfig({}, controller.args[2], "lblImageIndex"));
        flxImageIndex.add(lblImageIndex);
        var lblResolution = new kony.ui.Label(extendConfig({
            "centerY": "50%",
            "id": "lblResolution",
            "isVisible": true,
            "left": "5dp",
            "skin": "sknlbl465C77LatoReg12px",
            "text": "Resolution:",
            "top": "0dp",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, controller.args[0], "lblResolution"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblResolution"), extendConfig({}, controller.args[2], "lblResolution"));
        var lblResolutionValue = new kony.ui.Label(extendConfig({
            "centerY": "50%",
            "id": "lblResolutionValue",
            "isVisible": true,
            "left": "5dp",
            "skin": "sknlbl465C77LatoReg12px",
            "text": "560 x 323",
            "top": "0dp",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, controller.args[0], "lblResolutionValue"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblResolutionValue"), extendConfig({}, controller.args[2], "lblResolutionValue"));
        flxBannerImgInfo.add(flxImageIndex, lblResolution, lblResolutionValue);
        flxBannerImage.add(lblBannerImage, flxBannerImgInfo);
        var flxImgSourceValue = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "clipBounds": true,
            "height": "40dp",
            "id": "flxImgSourceValue",
            "isVisible": true,
            "layoutType": kony.flex.FLOW_HORIZONTAL,
            "left": "0dp",
            "isModalContainer": false,
            "skin": "slFbox",
            "top": "15dp",
            "width": "100%",
            "zIndex": 1
        }, controller.args[0], "flxImgSourceValue"), extendConfig({}, controller.args[1], "flxImgSourceValue"), extendConfig({}, controller.args[2], "flxImgSourceValue"));
        flxImgSourceValue.setDefaultUnit(kony.flex.DP);
        var txtBannerImgSource = new kony.ui.TextBox2(extendConfig({
            "autoCapitalize": constants.TEXTBOX_AUTO_CAPITALIZE_NONE,
            "focusSkin": "skntxtbxDetailsFocus14px",
            "height": "100%",
            "id": "txtBannerImgSource",
            "isVisible": true,
            "keyBoardStyle": constants.TEXTBOX_KEY_BOARD_STYLE_PHONE_PAD,
            "left": "0",
            "placeholder": "Select Image ",
            "secureTextEntry": false,
            "skin": "txtD7d9e0",
            "textInputMode": constants.TEXTBOX_INPUT_MODE_NUMERIC,
            "top": "0",
            "width": "80%",
            "zIndex": 10
        }, controller.args[0], "txtBannerImgSource"), extendConfig({
            "containerHeightMode": constants.TEXTBOX_FONT_METRICS_DRIVEN_HEIGHT,
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [2, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "txtBannerImgSource"), extendConfig({
            "autoCorrect": false,
            "placeholderSkin": "defTextBoxPlaceholder"
        }, controller.args[2], "txtBannerImgSource"));
        var btnBannerImgVerify = new kony.ui.Button(extendConfig({
            "height": "100%",
            "id": "btnBannerImgVerify",
            "isVisible": true,
            "left": "0dp",
            "skin": "sknBtnd7d9e0",
            "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmAdManagement.Verify\")",
            "top": "0dp",
            "width": "20%",
            "zIndex": 1
        }, controller.args[0], "btnBannerImgVerify"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_CENTER,
            "displayText": true,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "btnBannerImgVerify"), extendConfig({
            "hoverSkin": "sknBtnd7d9e0"
        }, controller.args[2], "btnBannerImgVerify"));
        flxImgSourceValue.add(txtBannerImgSource, btnBannerImgVerify);
        var BannerImageError = new com.adminConsole.loans.applications.errorMsg(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
            "id": "BannerImageError",
            "isVisible": false,
            "layoutType": kony.flex.FLOW_HORIZONTAL,
            "left": "0dp",
            "masterType": constants.MASTER_TYPE_DEFAULT,
            "isModalContainer": false,
            "skin": "slFbox",
            "top": "10dp",
            "width": "100%",
            "overrides": {
                "errorMsg": {
                    "isVisible": false,
                    "width": "100%"
                },
                "lblErrorText": {
                    "text": "Banner image cannot be empty."
                }
            }
        }, controller.args[0], "BannerImageError"), extendConfig({
            "overrides": {}
        }, controller.args[1], "BannerImageError"), extendConfig({
            "overrides": {}
        }, controller.args[2], "BannerImageError"));
        var lblBannerTargetURL = new kony.ui.Label(extendConfig({
            "id": "lblBannerTargetURL",
            "isVisible": true,
            "left": "0dp",
            "skin": "sknLblLato485c7513px",
            "text": "Banner On-Click Target URL",
            "top": "26dp",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, controller.args[0], "lblBannerTargetURL"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblBannerTargetURL"), extendConfig({}, controller.args[2], "lblBannerTargetURL"));
        var flxBannerTargetURL = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "clipBounds": true,
            "height": "40dp",
            "id": "flxBannerTargetURL",
            "isVisible": true,
            "layoutType": kony.flex.FLOW_HORIZONTAL,
            "left": "0dp",
            "isModalContainer": false,
            "skin": "slFbox",
            "top": "15dp",
            "width": "100%",
            "zIndex": 1
        }, controller.args[0], "flxBannerTargetURL"), extendConfig({}, controller.args[1], "flxBannerTargetURL"), extendConfig({}, controller.args[2], "flxBannerTargetURL"));
        flxBannerTargetURL.setDefaultUnit(kony.flex.DP);
        var txtBannerTargetURLSource = new kony.ui.TextBox2(extendConfig({
            "autoCapitalize": constants.TEXTBOX_AUTO_CAPITALIZE_NONE,
            "focusSkin": "skntxtbxDetailsFocus14px",
            "height": "100%",
            "id": "txtBannerTargetURLSource",
            "isVisible": true,
            "keyBoardStyle": constants.TEXTBOX_KEY_BOARD_STYLE_PHONE_PAD,
            "left": "0",
            "placeholder": "Enter URL",
            "secureTextEntry": false,
            "skin": "txtD7d9e0",
            "textInputMode": constants.TEXTBOX_INPUT_MODE_NUMERIC,
            "top": "0",
            "width": "80%",
            "zIndex": 10
        }, controller.args[0], "txtBannerTargetURLSource"), extendConfig({
            "containerHeightMode": constants.TEXTBOX_FONT_METRICS_DRIVEN_HEIGHT,
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [2, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "txtBannerTargetURLSource"), extendConfig({
            "autoCorrect": false,
            "placeholderSkin": "defTextBoxPlaceholder"
        }, controller.args[2], "txtBannerTargetURLSource"));
        var btnBannerTargetURLVerify = new kony.ui.Button(extendConfig({
            "height": "100%",
            "id": "btnBannerTargetURLVerify",
            "isVisible": true,
            "left": "0dp",
            "skin": "sknBtnd7d9e0",
            "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmAdManagement.Verify\")",
            "top": "0dp",
            "width": "20%",
            "zIndex": 1
        }, controller.args[0], "btnBannerTargetURLVerify"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_CENTER,
            "displayText": true,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "btnBannerTargetURLVerify"), extendConfig({
            "hoverSkin": "sknBtnd7d9e0"
        }, controller.args[2], "btnBannerTargetURLVerify"));
        flxBannerTargetURL.add(txtBannerTargetURLSource, btnBannerTargetURLVerify);
        var BannerTargetURLError = new com.adminConsole.loans.applications.errorMsg(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
            "id": "BannerTargetURLError",
            "isVisible": false,
            "layoutType": kony.flex.FLOW_HORIZONTAL,
            "left": "0dp",
            "masterType": constants.MASTER_TYPE_DEFAULT,
            "isModalContainer": false,
            "skin": "slFbox",
            "top": "10dp",
            "width": "100%",
            "overrides": {
                "errorMsg": {
                    "isVisible": false,
                    "width": "100%"
                },
                "lblErrorText": {
                    "text": "Banner target URL cannot be empty."
                }
            }
        }, controller.args[0], "BannerTargetURLError"), extendConfig({
            "overrides": {}
        }, controller.args[1], "BannerTargetURLError"), extendConfig({
            "overrides": {}
        }, controller.args[2], "BannerTargetURLError"));
        flxPopupDetailsInner.add(flxCopyBannerDetails, lblBannerConfig, flxBannerHeadline, txtBannerHeadline, BannerHeadlineError, flxBannerDescription, txtBannerDescription, BannerDescriptionError, flxBannerImage, flxImgSourceValue, BannerImageError, lblBannerTargetURL, flxBannerTargetURL, BannerTargetURLError);
        flxPopupDetails.add(flxPopupDetailsInner);
        var flxPopupDetails2 = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "clipBounds": true,
            "height": "110dp",
            "id": "flxPopupDetails2",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "left": "0dp",
            "isModalContainer": false,
            "skin": "sknf5f6f8border0px",
            "top": "25dp",
            "width": "100%",
            "zIndex": 2
        }, controller.args[0], "flxPopupDetails2"), extendConfig({}, controller.args[1], "flxPopupDetails2"), extendConfig({}, controller.args[2], "flxPopupDetails2"));
        flxPopupDetails2.setDefaultUnit(kony.flex.DP);
        var flxCTABtnLabel = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "clipBounds": true,
            "height": "100%",
            "id": "flxCTABtnLabel",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "left": "0dp",
            "isModalContainer": false,
            "skin": "slFbox",
            "top": "0dp",
            "width": "50%"
        }, controller.args[0], "flxCTABtnLabel"), extendConfig({}, controller.args[1], "flxCTABtnLabel"), extendConfig({}, controller.args[2], "flxCTABtnLabel"));
        flxCTABtnLabel.setDefaultUnit(kony.flex.DP);
        var flxCTABtnLabelHeader = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
            "clipBounds": true,
            "id": "flxCTABtnLabelHeader",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "left": "15dp",
            "isModalContainer": false,
            "right": "7dp",
            "skin": "slFbox",
            "top": "15dp"
        }, controller.args[0], "flxCTABtnLabelHeader"), extendConfig({}, controller.args[1], "flxCTABtnLabelHeader"), extendConfig({}, controller.args[2], "flxCTABtnLabelHeader"));
        flxCTABtnLabelHeader.setDefaultUnit(kony.flex.DP);
        var lblCTABtnLablel = new kony.ui.Label(extendConfig({
            "id": "lblCTABtnLablel",
            "isVisible": true,
            "left": "0dp",
            "skin": "sknLblLato485c7513px",
            "text": "CTA Button Label (Primary)",
            "top": "0dp",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, controller.args[0], "lblCTABtnLablel"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblCTABtnLablel"), extendConfig({}, controller.args[2], "lblCTABtnLablel"));
        var lblCTABtnLablelSize = new kony.ui.Label(extendConfig({
            "id": "lblCTABtnLablelSize",
            "isVisible": false,
            "right": "0dp",
            "skin": "sknLblLato485c7513px",
            "text": "0/50",
            "top": "0dp",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, controller.args[0], "lblCTABtnLablelSize"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblCTABtnLablelSize"), extendConfig({}, controller.args[2], "lblCTABtnLablelSize"));
        flxCTABtnLabelHeader.add(lblCTABtnLablel, lblCTABtnLablelSize);
        var txtCTABtnLablel = new kony.ui.TextBox2(extendConfig({
            "autoCapitalize": constants.TEXTBOX_AUTO_CAPITALIZE_NONE,
            "focusSkin": "skntxtbxDetailsFocus14px",
            "height": "40dp",
            "id": "txtCTABtnLablel",
            "isVisible": true,
            "keyBoardStyle": constants.TEXTBOX_KEY_BOARD_STYLE_DEFAULT,
            "left": "15dp",
            "maxTextLength": 15,
            "placeholder": "Enter CTA Button Label",
            "right": "7dp",
            "secureTextEntry": false,
            "skin": "txtD7d9e0",
            "textInputMode": constants.TEXTBOX_INPUT_MODE_ANY,
            "top": "40dp",
            "zIndex": 1
        }, controller.args[0], "txtCTABtnLablel"), extendConfig({
            "containerHeightMode": constants.TEXTBOX_FONT_METRICS_DRIVEN_HEIGHT,
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [2, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "txtCTABtnLablel"), extendConfig({
            "autoCorrect": false,
            "placeholderSkin": "sknTextGreyb2bdcd"
        }, controller.args[2], "txtCTABtnLablel"));
        var CTABtnLblError = new com.adminConsole.loans.applications.errorMsg(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
            "bottom": "10dp",
            "id": "CTABtnLblError",
            "isVisible": false,
            "layoutType": kony.flex.FLOW_HORIZONTAL,
            "left": "20dp",
            "masterType": constants.MASTER_TYPE_DEFAULT,
            "isModalContainer": false,
            "right": 20,
            "skin": "slFbox",
            "overrides": {
                "errorMsg": {
                    "bottom": "10dp",
                    "isVisible": false,
                    "left": "20dp",
                    "right": 20,
                    "top": "viz.val_cleared",
                    "width": "viz.val_cleared"
                },
                "lblErrorText": {
                    "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmAdManagement.ErrorCTABtnTextEmpty\")"
                }
            }
        }, controller.args[0], "CTABtnLblError"), extendConfig({
            "overrides": {}
        }, controller.args[1], "CTABtnLblError"), extendConfig({
            "overrides": {}
        }, controller.args[2], "CTABtnLblError"));
        flxCTABtnLabel.add(flxCTABtnLabelHeader, txtCTABtnLablel, CTABtnLblError);
        var flxCTATargetURL = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "clipBounds": true,
            "height": "100%",
            "id": "flxCTATargetURL",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "isModalContainer": false,
            "right": 0,
            "skin": "slFbox",
            "top": "0dp",
            "width": "50%"
        }, controller.args[0], "flxCTATargetURL"), extendConfig({}, controller.args[1], "flxCTATargetURL"), extendConfig({}, controller.args[2], "flxCTATargetURL"));
        flxCTATargetURL.setDefaultUnit(kony.flex.DP);
        var flxCTATargetURLHeader = new kony.ui.Label(extendConfig({
            "id": "flxCTATargetURLHeader",
            "isVisible": true,
            "left": "8dp",
            "skin": "sknLblLato485c7513px",
            "text": "CTA Target URL (Primary)",
            "top": "15dp",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, controller.args[0], "flxCTATargetURLHeader"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "flxCTATargetURLHeader"), extendConfig({}, controller.args[2], "flxCTATargetURLHeader"));
        var flxCTATargetURLText = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "clipBounds": true,
            "height": "40dp",
            "id": "flxCTATargetURLText",
            "isVisible": true,
            "layoutType": kony.flex.FLOW_HORIZONTAL,
            "left": "8dp",
            "isModalContainer": false,
            "right": "15dp",
            "skin": "slFbox",
            "top": "40dp",
            "zIndex": 1
        }, controller.args[0], "flxCTATargetURLText"), extendConfig({}, controller.args[1], "flxCTATargetURLText"), extendConfig({}, controller.args[2], "flxCTATargetURLText"));
        flxCTATargetURLText.setDefaultUnit(kony.flex.DP);
        var txtCTATargetURL = new kony.ui.TextBox2(extendConfig({
            "autoCapitalize": constants.TEXTBOX_AUTO_CAPITALIZE_NONE,
            "focusSkin": "skntxtbxDetailsFocus14px",
            "height": "100%",
            "id": "txtCTATargetURL",
            "isVisible": true,
            "keyBoardStyle": constants.TEXTBOX_KEY_BOARD_STYLE_PHONE_PAD,
            "left": "0",
            "placeholder": "Enter CTA Target URL ",
            "secureTextEntry": false,
            "skin": "txtD7d9e0",
            "textInputMode": constants.TEXTBOX_INPUT_MODE_NUMERIC,
            "top": "0",
            "width": "80%",
            "zIndex": 10
        }, controller.args[0], "txtCTATargetURL"), extendConfig({
            "containerHeightMode": constants.TEXTBOX_FONT_METRICS_DRIVEN_HEIGHT,
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [2, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "txtCTATargetURL"), extendConfig({
            "autoCorrect": false,
            "placeholderSkin": "defTextBoxPlaceholder"
        }, controller.args[2], "txtCTATargetURL"));
        var btnCTATargetURLVerify = new kony.ui.Button(extendConfig({
            "height": "100%",
            "id": "btnCTATargetURLVerify",
            "isVisible": true,
            "left": "0dp",
            "skin": "sknBtnd7d9e0",
            "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmAdManagement.Verify\")",
            "top": "0dp",
            "width": "20%",
            "zIndex": 1
        }, controller.args[0], "btnCTATargetURLVerify"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_CENTER,
            "displayText": true,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "btnCTATargetURLVerify"), extendConfig({
            "hoverSkin": "sknBtnd7d9e0"
        }, controller.args[2], "btnCTATargetURLVerify"));
        flxCTATargetURLText.add(txtCTATargetURL, btnCTATargetURLVerify);
        var CTATargetURLError = new com.adminConsole.loans.applications.errorMsg(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
            "bottom": "10dp",
            "id": "CTATargetURLError",
            "isVisible": false,
            "layoutType": kony.flex.FLOW_HORIZONTAL,
            "left": "10dp",
            "masterType": constants.MASTER_TYPE_DEFAULT,
            "isModalContainer": false,
            "right": 20,
            "skin": "slFbox",
            "overrides": {
                "errorMsg": {
                    "bottom": "10dp",
                    "isVisible": false,
                    "left": "10dp",
                    "right": 20,
                    "top": "viz.val_cleared",
                    "width": "viz.val_cleared"
                },
                "lblErrorText": {
                    "text": "CTA target URL cannot be empty."
                }
            }
        }, controller.args[0], "CTATargetURLError"), extendConfig({
            "overrides": {}
        }, controller.args[1], "CTATargetURLError"), extendConfig({
            "overrides": {}
        }, controller.args[2], "CTATargetURLError"));
        flxCTATargetURL.add(flxCTATargetURLHeader, flxCTATargetURLText, CTATargetURLError);
        flxPopupDetails2.add(flxCTABtnLabel, flxCTATargetURL);
        var flxPopupDetails3 = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
            "bottom": "30dp",
            "clipBounds": true,
            "id": "flxPopupDetails3",
            "isVisible": true,
            "layoutType": kony.flex.FLOW_HORIZONTAL,
            "left": "0dp",
            "isModalContainer": false,
            "skin": "slFbox",
            "top": "26dp",
            "width": "100%"
        }, controller.args[0], "flxPopupDetails3"), extendConfig({}, controller.args[1], "flxPopupDetails3"), extendConfig({}, controller.args[2], "flxPopupDetails3"));
        flxPopupDetails3.setDefaultUnit(kony.flex.DP);
        var flxShowReadLater = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
            "clipBounds": true,
            "id": "flxShowReadLater",
            "isVisible": true,
            "layoutType": kony.flex.FLOW_HORIZONTAL,
            "left": "15dp",
            "isModalContainer": false,
            "skin": "slFbox",
            "top": "0dp",
            "width": "232dp",
            "zIndex": 1
        }, controller.args[0], "flxShowReadLater"), extendConfig({}, controller.args[1], "flxShowReadLater"), extendConfig({}, controller.args[2], "flxShowReadLater"));
        flxShowReadLater.setDefaultUnit(kony.flex.DP);
        var lblShowReadLater = new kony.ui.Label(extendConfig({
            "id": "lblShowReadLater",
            "isVisible": true,
            "left": "10dp",
            "skin": "sknLbl485C75LatoRegular13Px",
            "text": "Show Read Later Button",
            "top": "5dp",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, controller.args[0], "lblShowReadLater"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblShowReadLater"), extendConfig({}, controller.args[2], "lblShowReadLater"));
        var fontIconShowReadLaterInfo = new kony.ui.Label(extendConfig({
            "id": "fontIconShowReadLaterInfo",
            "isVisible": true,
            "left": "10dp",
            "skin": "sknfontIcon33333316px",
            "text": "",
            "top": "5dp",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, controller.args[0], "fontIconShowReadLaterInfo"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "fontIconShowReadLaterInfo"), extendConfig({}, controller.args[2], "fontIconShowReadLaterInfo"));
        var SwithShowReadLater = new kony.ui.Switch(extendConfig({
            "height": "18dp",
            "id": "SwithShowReadLater",
            "isVisible": true,
            "left": "15dp",
            "selectedIndex": 0,
            "skin": "sknSwitchServiceManagement",
            "top": "0",
            "width": "35dp",
            "zIndex": 1
        }, controller.args[0], "SwithShowReadLater"), extendConfig({
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "SwithShowReadLater"), extendConfig({}, controller.args[2], "SwithShowReadLater"));
        flxShowReadLater.add(lblShowReadLater, fontIconShowReadLaterInfo, SwithShowReadLater);
        var flxShowCloseIcon = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
            "clipBounds": true,
            "id": "flxShowCloseIcon",
            "isVisible": true,
            "layoutType": kony.flex.FLOW_HORIZONTAL,
            "left": "40dp",
            "isModalContainer": false,
            "skin": "slFbox",
            "top": "0dp",
            "width": "232dp",
            "zIndex": 1
        }, controller.args[0], "flxShowCloseIcon"), extendConfig({}, controller.args[1], "flxShowCloseIcon"), extendConfig({}, controller.args[2], "flxShowCloseIcon"));
        flxShowCloseIcon.setDefaultUnit(kony.flex.DP);
        var lblShowCloseIcon = new kony.ui.Label(extendConfig({
            "id": "lblShowCloseIcon",
            "isVisible": true,
            "left": "10dp",
            "skin": "sknLbl485C75LatoRegular13Px",
            "text": "Show Close Icon",
            "top": "5dp",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, controller.args[0], "lblShowCloseIcon"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblShowCloseIcon"), extendConfig({}, controller.args[2], "lblShowCloseIcon"));
        var fontIconShowCloseIconInfo = new kony.ui.Label(extendConfig({
            "id": "fontIconShowCloseIconInfo",
            "isVisible": true,
            "left": "10dp",
            "skin": "sknfontIcon33333316px",
            "text": "",
            "top": "5dp",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, controller.args[0], "fontIconShowCloseIconInfo"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "fontIconShowCloseIconInfo"), extendConfig({}, controller.args[2], "fontIconShowCloseIconInfo"));
        var SwitchShowCloseIcon = new kony.ui.Switch(extendConfig({
            "height": "18dp",
            "id": "SwitchShowCloseIcon",
            "isVisible": true,
            "left": "15dp",
            "selectedIndex": 0,
            "skin": "sknSwitchServiceManagement",
            "top": "0",
            "width": "35dp",
            "zIndex": 1
        }, controller.args[0], "SwitchShowCloseIcon"), extendConfig({
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "SwitchShowCloseIcon"), extendConfig({}, controller.args[2], "SwitchShowCloseIcon"));
        flxShowCloseIcon.add(lblShowCloseIcon, fontIconShowCloseIconInfo, SwitchShowCloseIcon);
        flxPopupDetails3.add(flxShowReadLater, flxShowCloseIcon);
        flxPopupDetailsBody.add(flxDispalyHideBanner, flxPopupDetails, flxPopupDetails2, flxPopupDetails3);
        popupDetails.add(flxPopupDetailsHeader, flxPopupDetailsBody);
        return popupDetails;
    }
})