define(function() {
    return function(controller) {
        var attributesContainer = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "clipBounds": true,
            "isMaster": true,
            "height": "100%",
            "id": "attributesContainer",
            "isVisible": true,
            "layoutType": kony.flex.FLOW_VERTICAL,
            "left": "0dp",
            "isModalContainer": false,
            "preShow": function(eventobject) {
                controller.AS_FlexContainer_d410be7967584d79bedd9a7d21c8697f(eventobject);
            },
            "skin": "sknBackgroundFFFFFF",
            "top": "0dp",
            "width": "100%"
        }, controller.args[0], "attributesContainer"), extendConfig({}, controller.args[1], "attributesContainer"), extendConfig({}, controller.args[2], "attributesContainer"));
        attributesContainer.setDefaultUnit(kony.flex.DP);
        var flxAttrSelection = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
            "clipBounds": true,
            "id": "flxAttrSelection",
            "isVisible": true,
            "layoutType": kony.flex.FLOW_VERTICAL,
            "left": "0dp",
            "isModalContainer": false,
            "skin": "slFbox",
            "top": "0dp",
            "width": "100%"
        }, controller.args[0], "flxAttrSelection"), extendConfig({}, controller.args[1], "flxAttrSelection"), extendConfig({}, controller.args[2], "flxAttrSelection"));
        flxAttrSelection.setDefaultUnit(kony.flex.DP);
        var flxattributesSelectionHeader = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "clipBounds": true,
            "height": "70dp",
            "id": "flxattributesSelectionHeader",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "left": "0dp",
            "isModalContainer": false,
            "skin": "slFbox",
            "top": "0dp",
            "width": "100%"
        }, controller.args[0], "flxattributesSelectionHeader"), extendConfig({}, controller.args[1], "flxattributesSelectionHeader"), extendConfig({}, controller.args[2], "flxattributesSelectionHeader"));
        flxattributesSelectionHeader.setDefaultUnit(kony.flex.DP);
        var flxAttrCount = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "clipBounds": true,
            "height": "100%",
            "id": "flxAttrCount",
            "isVisible": true,
            "layoutType": kony.flex.FLOW_HORIZONTAL,
            "left": "0dp",
            "isModalContainer": false,
            "skin": "slFbox",
            "top": "0dp",
            "width": "100%"
        }, controller.args[0], "flxAttrCount"), extendConfig({}, controller.args[1], "flxAttrCount"), extendConfig({}, controller.args[2], "flxAttrCount"));
        flxAttrCount.setDefaultUnit(kony.flex.DP);
        var lblAttributeCount = new kony.ui.Label(extendConfig({
            "id": "lblAttributeCount",
            "isVisible": true,
            "left": "0dp",
            "skin": "sknLbl192B45LatoRegular14px",
            "text": "0",
            "top": "25dp",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, controller.args[0], "lblAttributeCount"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblAttributeCount"), extendConfig({}, controller.args[2], "lblAttributeCount"));
        var lblAttrSelected = new kony.ui.Label(extendConfig({
            "id": "lblAttrSelected",
            "isVisible": true,
            "left": "5dp",
            "skin": "sknLbl192B45LatoRegular14px",
            "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmAlertsManagement.Selected\")",
            "top": "25dp",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, controller.args[0], "lblAttrSelected"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblAttrSelected"), extendConfig({}, controller.args[2], "lblAttrSelected"));
        flxAttrCount.add(lblAttributeCount, lblAttrSelected);
        var flxAttributesSearch = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "bottom": "15dp",
            "clipBounds": true,
            "height": "40px",
            "id": "flxAttributesSearch",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "isModalContainer": false,
            "right": 0,
            "skin": "sknflxd5d9ddop100",
            "top": "15dp",
            "width": "400px",
            "zIndex": 1
        }, controller.args[0], "flxAttributesSearch"), extendConfig({}, controller.args[1], "flxAttributesSearch"), extendConfig({}, controller.args[2], "flxAttributesSearch"));
        flxAttributesSearch.setDefaultUnit(kony.flex.DP);
        var fontIconAttributeSearch = new kony.ui.Label(extendConfig({
            "centerY": "50%",
            "id": "fontIconAttributeSearch",
            "isVisible": true,
            "left": "12px",
            "skin": "sknFontIconSearchImg20px",
            "i18n_text": "kony.i18n.getLocalizedString(\"i18n.userwidgetmodel.lblSearchIcon\")",
            "top": "0dp",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, controller.args[0], "fontIconAttributeSearch"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "fontIconAttributeSearch"), extendConfig({}, controller.args[2], "fontIconAttributeSearch"));
        var txtAttributeSearch = new kony.ui.TextBox2(extendConfig({
            "autoCapitalize": constants.TEXTBOX_AUTO_CAPITALIZE_NONE,
            "centerY": "50%",
            "height": "30px",
            "id": "txtAttributeSearch",
            "isVisible": true,
            "keyBoardStyle": constants.TEXTBOX_KEY_BOARD_STYLE_DEFAULT,
            "left": "35dp",
            "onTouchStart": controller.AS_TextField_cfab06859f0f43eca99495891ee290d5,
            "i18n_placeholder": "kony.i18n.getLocalizedString(\"i18n.frmAdManagement.SearchAttributes\")",
            "right": "35px",
            "secureTextEntry": false,
            "skin": "skntbxffffffNoBorderlato35475f14px",
            "textInputMode": constants.TEXTBOX_INPUT_MODE_ANY,
            "zIndex": 1
        }, controller.args[0], "txtAttributeSearch"), extendConfig({
            "containerHeightMode": constants.TEXTBOX_FONT_METRICS_DRIVEN_HEIGHT,
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [2, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "txtAttributeSearch"), extendConfig({
            "autoCorrect": false
        }, controller.args[2], "txtAttributeSearch"));
        var flxAttributesClearSearch = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "clipBounds": true,
            "height": "100%",
            "id": "flxAttributesClearSearch",
            "isVisible": false,
            "layoutType": kony.flex.FREE_FORM,
            "isModalContainer": false,
            "right": "0px",
            "skin": "sknCursor",
            "top": "0dp",
            "width": "35px",
            "zIndex": 1
        }, controller.args[0], "flxAttributesClearSearch"), extendConfig({}, controller.args[1], "flxAttributesClearSearch"), extendConfig({
            "hoverSkin": "sknCursor"
        }, controller.args[2], "flxAttributesClearSearch"));
        flxAttributesClearSearch.setDefaultUnit(kony.flex.DP);
        var fontIconAttrCross = new kony.ui.Label(extendConfig({
            "centerY": "50%",
            "id": "fontIconAttrCross",
            "isVisible": true,
            "left": "5px",
            "skin": "sknFontIconSearchCross16px",
            "i18n_text": "kony.i18n.getLocalizedString(\"i18n.userwidgetmodel.lblClearSearch\")",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, controller.args[0], "fontIconAttrCross"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "fontIconAttrCross"), extendConfig({}, controller.args[2], "fontIconAttrCross"));
        flxAttributesClearSearch.add(fontIconAttrCross);
        flxAttributesSearch.add(fontIconAttributeSearch, txtAttributeSearch, flxAttributesClearSearch);
        flxattributesSelectionHeader.add(flxAttrCount, flxAttributesSearch);
        var lblAttrHeaderSeperator = new kony.ui.Label(extendConfig({
            "bottom": "0dp",
            "height": "1dp",
            "id": "lblAttrHeaderSeperator",
            "isVisible": true,
            "left": "0dp",
            "skin": "sknLblHeaderLineBg485375",
            "text": ".",
            "width": "100%",
            "zIndex": 1
        }, controller.args[0], "lblAttrHeaderSeperator"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblAttrHeaderSeperator"), extendConfig({}, controller.args[2], "lblAttrHeaderSeperator"));
        flxAttrSelection.add(flxattributesSelectionHeader, lblAttrHeaderSeperator);
        var flxError = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "clipBounds": true,
            "height": "40dp",
            "id": "flxError",
            "isVisible": false,
            "layoutType": kony.flex.FLOW_HORIZONTAL,
            "left": "0dp",
            "isModalContainer": false,
            "skin": "sknRedBorder",
            "top": "15dp",
            "width": "100%"
        }, controller.args[0], "flxError"), extendConfig({}, controller.args[1], "flxError"), extendConfig({}, controller.args[2], "flxError"));
        flxError.setDefaultUnit(kony.flex.DP);
        var flxErrorIcon = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "clipBounds": true,
            "height": "100%",
            "id": "flxErrorIcon",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "isModalContainer": false,
            "skin": "sknRedFill",
            "top": "0dp",
            "width": "40dp",
            "zIndex": 1
        }, controller.args[0], "flxErrorIcon"), extendConfig({}, controller.args[1], "flxErrorIcon"), extendConfig({}, controller.args[2], "flxErrorIcon"));
        flxErrorIcon.setDefaultUnit(kony.flex.DP);
        var lblErrorIcon = new kony.ui.Label(extendConfig({
            "height": "20dp",
            "id": "lblErrorIcon",
            "isVisible": true,
            "left": "10dp",
            "skin": "sknErrorWhiteIcon",
            "text": "",
            "top": "10dp",
            "width": "20dp",
            "zIndex": 1
        }, controller.args[0], "lblErrorIcon"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblErrorIcon"), extendConfig({}, controller.args[2], "lblErrorIcon"));
        flxErrorIcon.add(lblErrorIcon);
        var lblErrorMsg = new kony.ui.Label(extendConfig({
            "id": "lblErrorMsg",
            "isVisible": true,
            "left": "5dp",
            "skin": "sknLbl192B45LatoRegular14px",
            "text": "Add atleast 1 attribute to proceed further",
            "top": "12dp",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, controller.args[0], "lblErrorMsg"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblErrorMsg"), extendConfig({}, controller.args[2], "lblErrorMsg"));
        flxError.add(flxErrorIcon, lblErrorMsg);
        var flxAttrHeader = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "clipBounds": true,
            "height": "50dp",
            "id": "flxAttrHeader",
            "isVisible": true,
            "layoutType": kony.flex.FLOW_HORIZONTAL,
            "left": "0dp",
            "isModalContainer": false,
            "skin": "slFbox",
            "width": "100%"
        }, controller.args[0], "flxAttrHeader"), extendConfig({}, controller.args[1], "flxAttrHeader"), extendConfig({}, controller.args[2], "flxAttrHeader"));
        flxAttrHeader.setDefaultUnit(kony.flex.DP);
        var lblAttributeHeader = new kony.ui.Label(extendConfig({
            "id": "lblAttributeHeader",
            "isVisible": true,
            "skin": "sknLbl73757C11px",
            "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmAdManagement.AttributeCAPS\")",
            "top": "21dp",
            "width": "31%",
            "zIndex": 1
        }, controller.args[0], "lblAttributeHeader"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblAttributeHeader"), extendConfig({}, controller.args[2], "lblAttributeHeader"));
        var lblAttributeCriteriaHeader = new kony.ui.Label(extendConfig({
            "id": "lblAttributeCriteriaHeader",
            "isVisible": true,
            "left": "2%",
            "skin": "sknLbl73757C11px",
            "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmAdManagement.CriteriaCAPS\")",
            "top": "21dp",
            "width": "31%",
            "zIndex": 1
        }, controller.args[0], "lblAttributeCriteriaHeader"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblAttributeCriteriaHeader"), extendConfig({}, controller.args[2], "lblAttributeCriteriaHeader"));
        var lblAttributeValueHeader = new kony.ui.Label(extendConfig({
            "id": "lblAttributeValueHeader",
            "isVisible": true,
            "left": "2%",
            "skin": "sknLbl73757C11px",
            "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmAdManagement.ValueCAPS\")",
            "top": "21dp",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, controller.args[0], "lblAttributeValueHeader"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblAttributeValueHeader"), extendConfig({}, controller.args[2], "lblAttributeValueHeader"));
        flxAttrHeader.add(lblAttributeHeader, lblAttributeCriteriaHeader, lblAttributeValueHeader);
        var lblAttributeHeaderSeparator = new kony.ui.Label(extendConfig({
            "bottom": "0dp",
            "height": "1dp",
            "id": "lblAttributeHeaderSeparator",
            "isVisible": true,
            "left": "0dp",
            "skin": "sknLblHeaderLineBg485375",
            "text": ".",
            "width": "100%",
            "zIndex": 1
        }, controller.args[0], "lblAttributeHeaderSeparator"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblAttributeHeaderSeparator"), extendConfig({}, controller.args[2], "lblAttributeHeaderSeparator"));
        var flxAttributesDetailsBody = new kony.ui.FlexScrollContainer(extendConfig({
            "allowHorizontalBounce": false,
            "allowVerticalBounce": true,
            "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
            "bounces": true,
            "clipBounds": true,
            "enableScrolling": true,
            "horizontalScrollIndicator": true,
            "id": "flxAttributesDetailsBody",
            "isVisible": true,
            "layoutType": kony.flex.FLOW_VERTICAL,
            "left": "0dp",
            "pagingEnabled": false,
            "scrollDirection": kony.flex.SCROLL_VERTICAL,
            "skin": "slFSbox",
            "top": "0dp",
            "verticalScrollIndicator": true,
            "width": "100%",
            "zIndex": 1
        }, controller.args[0], "flxAttributesDetailsBody"), extendConfig({}, controller.args[1], "flxAttributesDetailsBody"), extendConfig({}, controller.args[2], "flxAttributesDetailsBody"));
        flxAttributesDetailsBody.setDefaultUnit(kony.flex.DP);
        var lblNoAttributesFound = new kony.ui.Label(extendConfig({
            "centerX": "50%",
            "centerY": "50%",
            "id": "lblNoAttributesFound",
            "isVisible": false,
            "left": "5dp",
            "skin": "sknLbl192B45LatoRegular14px",
            "text": "No Attributes Found",
            "top": "25dp",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, controller.args[0], "lblNoAttributesFound"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblNoAttributesFound"), extendConfig({}, controller.args[2], "lblNoAttributesFound"));
        flxAttributesDetailsBody.add(lblNoAttributesFound);
        attributesContainer.add(flxAttrSelection, flxError, flxAttrHeader, lblAttributeHeaderSeparator, flxAttributesDetailsBody);
        return attributesContainer;
    }
})