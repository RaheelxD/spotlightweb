define(function() {
    return function(controller) {
        var imageSourceURL = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
            "clipBounds": true,
            "isMaster": true,
            "id": "imageSourceURL",
            "isVisible": true,
            "layoutType": kony.flex.FLOW_VERTICAL,
            "left": "0dp",
            "isModalContainer": false,
            "skin": "slFbox",
            "top": "15dp",
            "width": kony.flex.USE_PREFFERED_SIZE
        }, controller.args[0], "imageSourceURL"), extendConfig({}, controller.args[1], "imageSourceURL"), extendConfig({}, controller.args[2], "imageSourceURL"));
        imageSourceURL.setDefaultUnit(kony.flex.DP);
        var flxInfo = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "clipBounds": true,
            "height": "20dp",
            "id": "flxInfo",
            "isVisible": true,
            "layoutType": kony.flex.FLOW_HORIZONTAL,
            "left": "0dp",
            "isModalContainer": false,
            "skin": "flxBorder5FB8AE1px",
            "top": "0",
            "width": "170dp"
        }, controller.args[0], "flxInfo"), extendConfig({}, controller.args[1], "flxInfo"), extendConfig({}, controller.args[2], "flxInfo"));
        flxInfo.setDefaultUnit(kony.flex.DP);
        var flxImageIndex = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "clipBounds": true,
            "height": "20dp",
            "id": "flxImageIndex",
            "isVisible": true,
            "layoutType": kony.flex.FLOW_HORIZONTAL,
            "left": "0dp",
            "isModalContainer": false,
            "skin": "sknflxBackground5FB8AE",
            "top": "0dp",
            "width": "30dp"
        }, controller.args[0], "flxImageIndex"), extendConfig({}, controller.args[1], "flxImageIndex"), extendConfig({}, controller.args[2], "flxImageIndex"));
        flxImageIndex.setDefaultUnit(kony.flex.DP);
        var lblImageIndex = new kony.ui.Label(extendConfig({
            "centerX": "50%",
            "centerY": "50%",
            "id": "lblImageIndex",
            "isVisible": true,
            "skin": "sknLblFFFFFLatoRegular12Px",
            "text": "1X",
            "top": "0dp",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, controller.args[0], "lblImageIndex"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_CENTER,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblImageIndex"), extendConfig({}, controller.args[2], "lblImageIndex"));
        flxImageIndex.add(lblImageIndex);
        var lblResolution = new kony.ui.Label(extendConfig({
            "centerY": "50%",
            "id": "lblResolution",
            "isVisible": true,
            "left": "5dp",
            "skin": "sknlbl465C77LatoReg12px",
            "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmAdManagement.Resolution\")",
            "top": "0dp",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, controller.args[0], "lblResolution"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblResolution"), extendConfig({}, controller.args[2], "lblResolution"));
        var lblResolutionValue = new kony.ui.Label(extendConfig({
            "centerY": "50%",
            "id": "lblResolutionValue",
            "isVisible": true,
            "left": "5dp",
            "skin": "sknlbl465C77LatoReg12px",
            "text": "1080 x 1080",
            "top": "0dp",
            "width": "80dp",
            "zIndex": 1
        }, controller.args[0], "lblResolutionValue"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblResolutionValue"), extendConfig({}, controller.args[2], "lblResolutionValue"));
        flxInfo.add(flxImageIndex, lblResolution, lblResolutionValue);
        var flxImgSourceValue = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "clipBounds": true,
            "height": "40dp",
            "id": "flxImgSourceValue",
            "isVisible": true,
            "layoutType": kony.flex.FLOW_HORIZONTAL,
            "left": "0dp",
            "isModalContainer": false,
            "skin": "slFbox",
            "top": "15dp",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, controller.args[0], "flxImgSourceValue"), extendConfig({}, controller.args[1], "flxImgSourceValue"), extendConfig({}, controller.args[2], "flxImgSourceValue"));
        flxImgSourceValue.setDefaultUnit(kony.flex.DP);
        var txtImgSource = new kony.ui.TextBox2(extendConfig({
            "autoCapitalize": constants.TEXTBOX_AUTO_CAPITALIZE_NONE,
            "focusSkin": "skntxtbxDetailsFocus14px",
            "height": "100%",
            "id": "txtImgSource",
            "isVisible": true,
            "keyBoardStyle": constants.TEXTBOX_KEY_BOARD_STYLE_PHONE_PAD,
            "left": "0",
            "i18n_placeholder": "kony.i18n.getLocalizedString(\"i18n.frmAdManagement.EnterURL\")",
            "secureTextEntry": false,
            "skin": "txtD7d9e0",
            "textInputMode": constants.TEXTBOX_INPUT_MODE_NUMERIC,
            "top": "0",
            "width": "80%",
            "zIndex": 10
        }, controller.args[0], "txtImgSource"), extendConfig({
            "containerHeightMode": constants.TEXTBOX_FONT_METRICS_DRIVEN_HEIGHT,
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [2, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "txtImgSource"), extendConfig({
            "autoCorrect": false,
            "placeholderSkin": "defTextBoxPlaceholder"
        }, controller.args[2], "txtImgSource"));
        var btnImgVerify = new kony.ui.Button(extendConfig({
            "height": "100%",
            "id": "btnImgVerify",
            "isVisible": true,
            "left": "0dp",
            "skin": "sknBtnd7d9e0",
            "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmAdManagement.Verify\")",
            "top": "0dp",
            "width": "20%",
            "zIndex": 1
        }, controller.args[0], "btnImgVerify"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_CENTER,
            "displayText": true,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "btnImgVerify"), extendConfig({
            "hoverSkin": "sknBtnd7d9e0"
        }, controller.args[2], "btnImgVerify"));
        flxImgSourceValue.add(txtImgSource, btnImgVerify);
        var imageSrcError = new com.adminConsole.loans.applications.errorMsg(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
            "id": "imageSrcError",
            "isVisible": false,
            "layoutType": kony.flex.FLOW_HORIZONTAL,
            "left": "0dp",
            "masterType": constants.MASTER_TYPE_DEFAULT,
            "isModalContainer": false,
            "skin": "slFbox",
            "top": "10dp",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "overrides": {
                "errorMsg": {
                    "isVisible": false
                },
                "lblErrorText": {
                    "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmAdManagement.ErrorImageSourceURL\")"
                }
            }
        }, controller.args[0], "imageSrcError"), extendConfig({
            "overrides": {}
        }, controller.args[1], "imageSrcError"), extendConfig({
            "overrides": {}
        }, controller.args[2], "imageSrcError"));
        imageSourceURL.add(flxInfo, flxImgSourceValue, imageSrcError);
        return imageSourceURL;
    }
})