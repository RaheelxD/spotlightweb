define(function() {
    return function(controller) {
        var selectOptions = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
            "clipBounds": true,
            "isMaster": true,
            "id": "selectOptions",
            "isVisible": true,
            "layoutType": kony.flex.FLOW_VERTICAL,
            "left": "0dp",
            "isModalContainer": false,
            "skin": "slFbox",
            "top": "0dp",
            "width": "100%"
        }, controller.args[0], "selectOptions"), extendConfig({}, controller.args[1], "selectOptions"), extendConfig({}, controller.args[2], "selectOptions"));
        selectOptions.setDefaultUnit(kony.flex.DP);
        var flxArrowImage = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "clipBounds": true,
            "height": "12dp",
            "id": "flxArrowImage",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "left": "0dp",
            "isModalContainer": false,
            "skin": "slFbox",
            "top": "0dp",
            "width": "100%",
            "zIndex": 2
        }, controller.args[0], "flxArrowImage"), extendConfig({}, controller.args[1], "flxArrowImage"), extendConfig({}, controller.args[2], "flxArrowImage"));
        flxArrowImage.setDefaultUnit(kony.flex.DP);
        var imgUpArrow = new kony.ui.Image2(extendConfig({
            "height": "12dp",
            "id": "imgUpArrow",
            "isVisible": true,
            "right": "15dp",
            "skin": "slImage",
            "src": "uparrow_2x.png",
            "top": "0dp",
            "width": "15dp",
            "zIndex": 1
        }, controller.args[0], "imgUpArrow"), extendConfig({
            "imageScaleMode": constants.IMAGE_SCALE_MODE_FIT_TO_DIMENSIONS,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "imgUpArrow"), extendConfig({}, controller.args[2], "imgUpArrow"));
        flxArrowImage.add(imgUpArrow);
        var flxSelectOptionsInner = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
            "clipBounds": true,
            "id": "flxSelectOptionsInner",
            "isVisible": true,
            "layoutType": kony.flex.FLOW_VERTICAL,
            "left": "0dp",
            "isModalContainer": false,
            "skin": "sknflxffffffop100dbdbe6Radius3px",
            "top": "-1dp",
            "width": "100%"
        }, controller.args[0], "flxSelectOptionsInner"), extendConfig({}, controller.args[1], "flxSelectOptionsInner"), extendConfig({}, controller.args[2], "flxSelectOptionsInner"));
        flxSelectOptionsInner.setDefaultUnit(kony.flex.DP);
        var flxOption1 = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "clipBounds": true,
            "height": "35dp",
            "id": "flxOption1",
            "isVisible": true,
            "layoutType": kony.flex.FLOW_HORIZONTAL,
            "left": "0dp",
            "isModalContainer": false,
            "skin": "CopyslFbox0e960df276fa744",
            "top": "0dp",
            "width": "100%",
            "zIndex": 1
        }, controller.args[0], "flxOption1"), extendConfig({}, controller.args[1], "flxOption1"), extendConfig({
            "hoverSkin": "sknContextualMenuEntryHover"
        }, controller.args[2], "flxOption1"));
        flxOption1.setDefaultUnit(kony.flex.DP);
        var fontIconOption1 = new kony.ui.Label(extendConfig({
            "centerY": "50%",
            "id": "fontIconOption1",
            "isVisible": true,
            "left": "20dp",
            "skin": "sknFontIconOptionMenuRow",
            "i18n_text": "kony.i18n.getLocalizedString(\"i18n.userwidgetmodel.lblIconOption1\")",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, controller.args[0], "fontIconOption1"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "fontIconOption1"), extendConfig({}, controller.args[2], "fontIconOption1"));
        var lblOption1 = new kony.ui.Label(extendConfig({
            "centerY": "50%",
            "id": "lblOption1",
            "isVisible": true,
            "left": "15dp",
            "skin": "sknlblLatoBold35475f14px",
            "i18n_text": "kony.i18n.getLocalizedString(\"i18n.permission.Edit\")",
            "top": "4dp",
            "width": "40px",
            "zIndex": 1
        }, controller.args[0], "lblOption1"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblOption1"), extendConfig({}, controller.args[2], "lblOption1"));
        flxOption1.add(fontIconOption1, lblOption1);
        var flxOption2 = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "clipBounds": true,
            "height": "35dp",
            "id": "flxOption2",
            "isVisible": true,
            "layoutType": kony.flex.FLOW_HORIZONTAL,
            "left": "0dp",
            "isModalContainer": false,
            "skin": "CopyslFbox0e55915c24f2a49",
            "width": "100%",
            "zIndex": 1
        }, controller.args[0], "flxOption2"), extendConfig({}, controller.args[1], "flxOption2"), extendConfig({
            "hoverSkin": "sknContextualMenuEntryHover"
        }, controller.args[2], "flxOption2"));
        flxOption2.setDefaultUnit(kony.flex.DP);
        var fontIconOption2 = new kony.ui.Label(extendConfig({
            "centerY": "50%",
            "id": "fontIconOption2",
            "isVisible": true,
            "left": "20px",
            "skin": "sknFontIconOptionMenuRow",
            "i18n_text": "kony.i18n.getLocalizedString(\"i18n.userwidgetmodel.lblIconOption2\")",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, controller.args[0], "fontIconOption2"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "fontIconOption2"), extendConfig({}, controller.args[2], "fontIconOption2"));
        var lblOption2 = new kony.ui.Label(extendConfig({
            "centerY": "50%",
            "id": "lblOption2",
            "isVisible": true,
            "left": "15dp",
            "skin": "sknlblLatoBold35475f14px",
            "i18n_text": "kony.i18n.getLocalizedString(\"i18n.SecurityQuestions.Delete\")",
            "top": "4dp",
            "width": "60px",
            "zIndex": 1
        }, controller.args[0], "lblOption2"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblOption2"), extendConfig({}, controller.args[2], "lblOption2"));
        flxOption2.add(fontIconOption2, lblOption2);
        var flxOption3 = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "clipBounds": true,
            "height": "35dp",
            "id": "flxOption3",
            "isVisible": true,
            "layoutType": kony.flex.FLOW_HORIZONTAL,
            "left": "0dp",
            "isModalContainer": false,
            "skin": "CopyslFbox0e8cab7435c9f44",
            "width": "100%",
            "zIndex": 1
        }, controller.args[0], "flxOption3"), extendConfig({}, controller.args[1], "flxOption3"), extendConfig({
            "hoverSkin": "sknContextualMenuEntryHover"
        }, controller.args[2], "flxOption3"));
        flxOption3.setDefaultUnit(kony.flex.DP);
        var fontIconOption3 = new kony.ui.Label(extendConfig({
            "centerY": "50%",
            "id": "fontIconOption3",
            "isVisible": true,
            "left": "20px",
            "skin": "sknFontIconOptionMenuRow",
            "i18n_text": "kony.i18n.getLocalizedString(\"i18n.userwidgetmodel.lblIconOption4\")",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, controller.args[0], "fontIconOption3"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "fontIconOption3"), extendConfig({}, controller.args[2], "fontIconOption3"));
        var lblOption3 = new kony.ui.Label(extendConfig({
            "centerY": "50%",
            "id": "lblOption3",
            "isVisible": true,
            "left": "15dp",
            "skin": "sknlblLatoBold35475f14px",
            "i18n_text": "kony.i18n.getLocalizedString(\"i18n.permission.Deactivate\")",
            "top": "4dp",
            "width": "70px",
            "zIndex": 1
        }, controller.args[0], "lblOption3"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblOption3"), extendConfig({}, controller.args[2], "lblOption3"));
        flxOption3.add(fontIconOption3, lblOption3);
        var lblSeperator = new kony.ui.Label(extendConfig({
            "bottom": "0dp",
            "height": "1dp",
            "id": "lblSeperator",
            "isVisible": true,
            "left": "0dp",
            "skin": "sknlblSeperatorD7D9E0",
            "text": "-",
            "top": "0dp",
            "width": "100%",
            "zIndex": 2
        }, controller.args[0], "lblSeperator"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblSeperator"), extendConfig({}, controller.args[2], "lblSeperator"));
        var flxOption4 = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "clipBounds": true,
            "height": "35dp",
            "id": "flxOption4",
            "isVisible": true,
            "layoutType": kony.flex.FLOW_HORIZONTAL,
            "left": "0dp",
            "isModalContainer": false,
            "skin": "CopyslFbox0e8cab7435c9f44",
            "top": "0",
            "width": "100%",
            "zIndex": 1
        }, controller.args[0], "flxOption4"), extendConfig({}, controller.args[1], "flxOption4"), extendConfig({
            "hoverSkin": "sknContextualMenuEntryHover"
        }, controller.args[2], "flxOption4"));
        flxOption4.setDefaultUnit(kony.flex.DP);
        var fontIconOption4 = new kony.ui.Label(extendConfig({
            "centerY": "50%",
            "id": "fontIconOption4",
            "isVisible": true,
            "left": "20px",
            "skin": "sknFontIconOptionMenuRow",
            "text": "",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, controller.args[0], "fontIconOption4"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "fontIconOption4"), extendConfig({}, controller.args[2], "fontIconOption4"));
        var lblOption4 = new kony.ui.Label(extendConfig({
            "centerY": "50%",
            "id": "lblOption4",
            "isVisible": true,
            "left": "15dp",
            "skin": "sknlblLatoBold35475f14px",
            "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmAdManagement.CopyCampaign\")",
            "top": "4dp",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, controller.args[0], "lblOption4"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblOption4"), extendConfig({}, controller.args[2], "lblOption4"));
        flxOption4.add(fontIconOption4, lblOption4);
        flxSelectOptionsInner.add(flxOption1, flxOption2, flxOption3, lblSeperator, flxOption4);
        var flxDownArrowImage = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "clipBounds": true,
            "height": "12px",
            "id": "flxDownArrowImage",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "left": "0px",
            "isModalContainer": false,
            "skin": "slFbox",
            "top": "-1px",
            "width": "100%",
            "zIndex": 2
        }, controller.args[0], "flxDownArrowImage"), extendConfig({}, controller.args[1], "flxDownArrowImage"), extendConfig({}, controller.args[2], "flxDownArrowImage"));
        flxDownArrowImage.setDefaultUnit(kony.flex.DP);
        var imgDownArrow = new kony.ui.Image2(extendConfig({
            "height": "12dp",
            "id": "imgDownArrow",
            "isVisible": true,
            "right": "15dp",
            "skin": "slImage",
            "src": "downarrow_2x.png",
            "top": "0dp",
            "width": "15dp",
            "zIndex": 1
        }, controller.args[0], "imgDownArrow"), extendConfig({
            "imageScaleMode": constants.IMAGE_SCALE_MODE_FIT_TO_DIMENSIONS,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "imgDownArrow"), extendConfig({}, controller.args[2], "imgDownArrow"));
        flxDownArrowImage.add(imgDownArrow);
        selectOptions.add(flxArrowImage, flxSelectOptionsInner, flxDownArrowImage);
        return selectOptions;
    }
})