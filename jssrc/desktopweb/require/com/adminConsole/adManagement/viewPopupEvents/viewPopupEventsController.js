define("com/adminConsole/adManagement/viewPopupEvents/userviewPopupEventsController", function() {
    return {
        viewPopupEventsPreShow: function() {
            this.setFlowActions();
        },
        showEvents: function() {
            //setting segment visibility off and changing icon to collapse
            if (this.view.flxEventsBody.isVisible) {
                this.view.flxEventsBody.setVisibility(false);
                this.view.fontIconEventsExpandCollapse.text = "\ue922";
            } else {
                this.view.flxEventsBody.setVisibility(true);
                this.view.fontIconEventsExpandCollapse.text = "\ue915";
            }
        },
        setFlowActions: function() {
            var scopeObj = this;
            this.view.flxEventsExpandCollapse.onClick = function() {
                scopeObj.showEvents();
            };
        }
    };
});
define("com/adminConsole/adManagement/viewPopupEvents/viewPopupEventsControllerActions", {
    /*
        This is an auto generated file and any modifications to it may result in corruption of the action sequence.
    */
    AS_FlexContainer_j06de391c8ed45338732225ebb4947f7: function AS_FlexContainer_j06de391c8ed45338732225ebb4947f7(eventobject) {
        var self = this;
        return self.viewPopupEventsPreShow.call(this);
    }
});
define("com/adminConsole/adManagement/viewPopupEvents/viewPopupEventsController", ["com/adminConsole/adManagement/viewPopupEvents/userviewPopupEventsController", "com/adminConsole/adManagement/viewPopupEvents/viewPopupEventsControllerActions"], function() {
    var controller = require("com/adminConsole/adManagement/viewPopupEvents/userviewPopupEventsController");
    var actions = require("com/adminConsole/adManagement/viewPopupEvents/viewPopupEventsControllerActions");
    for (var key in actions) {
        controller[key] = actions[key];
    }
    return controller;
});
