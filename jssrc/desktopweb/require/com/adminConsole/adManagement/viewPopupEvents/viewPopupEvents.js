define(function() {
    return function(controller) {
        var viewPopupEvents = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
            "clipBounds": true,
            "isMaster": true,
            "id": "viewPopupEvents",
            "isVisible": true,
            "layoutType": kony.flex.FLOW_VERTICAL,
            "left": "0dp",
            "isModalContainer": false,
            "preShow": function(eventobject) {
                controller.AS_FlexContainer_j06de391c8ed45338732225ebb4947f7(eventobject);
            },
            "skin": "sknFlxbgFFFFFFbdrD7D9E0rd4px",
            "top": "0dp",
            "width": "100%"
        }, controller.args[0], "viewPopupEvents"), extendConfig({}, controller.args[1], "viewPopupEvents"), extendConfig({}, controller.args[2], "viewPopupEvents"));
        viewPopupEvents.setDefaultUnit(kony.flex.DP);
        var flxEventsHeader = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "clipBounds": true,
            "height": "45dp",
            "id": "flxEventsHeader",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "left": "0dp",
            "isModalContainer": false,
            "skin": "sknflxf5f6f8Op100",
            "top": "0dp",
            "width": "100%",
            "zIndex": 1
        }, controller.args[0], "flxEventsHeader"), extendConfig({}, controller.args[1], "flxEventsHeader"), extendConfig({}, controller.args[2], "flxEventsHeader"));
        flxEventsHeader.setDefaultUnit(kony.flex.DP);
        var flxEventsExpandCollapse = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "centerY": "50%",
            "clipBounds": true,
            "height": "16dp",
            "id": "flxEventsExpandCollapse",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "left": "10dp",
            "isModalContainer": false,
            "skin": "hoverhandSkin",
            "width": "16dp"
        }, controller.args[0], "flxEventsExpandCollapse"), extendConfig({}, controller.args[1], "flxEventsExpandCollapse"), extendConfig({}, controller.args[2], "flxEventsExpandCollapse"));
        flxEventsExpandCollapse.setDefaultUnit(kony.flex.DP);
        var fontIconEventsExpandCollapse = new kony.ui.Label(extendConfig({
            "centerY": "50%",
            "id": "fontIconEventsExpandCollapse",
            "isVisible": true,
            "left": "0dp",
            "skin": "sknLblIconMoon16px192b45",
            "text": "",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, controller.args[0], "fontIconEventsExpandCollapse"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "fontIconEventsExpandCollapse"), extendConfig({}, controller.args[2], "fontIconEventsExpandCollapse"));
        flxEventsExpandCollapse.add(fontIconEventsExpandCollapse);
        var flxEventsHeaderInner = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
            "centerY": "50%",
            "clipBounds": true,
            "id": "flxEventsHeaderInner",
            "isVisible": true,
            "layoutType": kony.flex.FLOW_HORIZONTAL,
            "left": "36dp",
            "isModalContainer": false,
            "right": "60dp",
            "skin": "slFbox"
        }, controller.args[0], "flxEventsHeaderInner"), extendConfig({}, controller.args[1], "flxEventsHeaderInner"), extendConfig({}, controller.args[2], "flxEventsHeaderInner"));
        flxEventsHeaderInner.setDefaultUnit(kony.flex.DP);
        var lblEventsHeader = new kony.ui.Label(extendConfig({
            "id": "lblEventsHeader",
            "isVisible": true,
            "left": "0dp",
            "skin": "sknLatoRegular192B4516px",
            "text": "Popup Events ",
            "top": 0,
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, controller.args[0], "lblEventsHeader"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblEventsHeader"), extendConfig({}, controller.args[2], "lblEventsHeader"));
        var fontIconEventsInfo = new kony.ui.Label(extendConfig({
            "id": "fontIconEventsInfo",
            "isVisible": true,
            "left": "10dp",
            "skin": "sknfontIcon33333316px",
            "text": "",
            "top": 2,
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, controller.args[0], "fontIconEventsInfo"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "fontIconEventsInfo"), extendConfig({}, controller.args[2], "fontIconEventsInfo"));
        flxEventsHeaderInner.add(lblEventsHeader, fontIconEventsInfo);
        var btnAddEvents = new kony.ui.Button(extendConfig({
            "centerY": "50%",
            "height": "22dp",
            "id": "btnAddEvents",
            "isVisible": true,
            "right": "12dp",
            "skin": "sknbtnf7f7faLatoRegular12Px485c75Border1px485C75Radius20px",
            "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmPermissionsController.Add\")",
            "width": "52dp",
            "zIndex": 1
        }, controller.args[0], "btnAddEvents"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_CENTER,
            "displayText": true,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "btnAddEvents"), extendConfig({
            "hoverSkin": "sknbtnffffffLatoRegular12Px485c75Border1px485C75Radius20px"
        }, controller.args[2], "btnAddEvents"));
        flxEventsHeader.add(flxEventsExpandCollapse, flxEventsHeaderInner, btnAddEvents);
        var flxEventsBody = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
            "clipBounds": true,
            "id": "flxEventsBody",
            "isVisible": true,
            "layoutType": kony.flex.FLOW_VERTICAL,
            "left": "0dp",
            "isModalContainer": false,
            "skin": "slFbox",
            "top": "0dp",
            "width": "100%"
        }, controller.args[0], "flxEventsBody"), extendConfig({}, controller.args[1], "flxEventsBody"), extendConfig({}, controller.args[2], "flxEventsBody"));
        flxEventsBody.setDefaultUnit(kony.flex.DP);
        var flxEventsSegHeader = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "clipBounds": true,
            "height": "50dp",
            "id": "flxEventsSegHeader",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "left": "0dp",
            "isModalContainer": false,
            "skin": "sknBackgroundFFFFFF",
            "top": "0dp",
            "width": "100%"
        }, controller.args[0], "flxEventsSegHeader"), extendConfig({}, controller.args[1], "flxEventsSegHeader"), extendConfig({}, controller.args[2], "flxEventsSegHeader"));
        flxEventsSegHeader.setDefaultUnit(kony.flex.DP);
        var lblAdPlaceholderTypeHeader = new kony.ui.Label(extendConfig({
            "bottom": "15dp",
            "id": "lblAdPlaceholderTypeHeader",
            "isVisible": true,
            "left": "12dp",
            "skin": "sknlblLato696c7313px",
            "text": "AD PLACEHOLDER TYPE",
            "top": "15dp",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, controller.args[0], "lblAdPlaceholderTypeHeader"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblAdPlaceholderTypeHeader"), extendConfig({}, controller.args[2], "lblAdPlaceholderTypeHeader"));
        var lblScreenNameHeader = new kony.ui.Label(extendConfig({
            "id": "lblScreenNameHeader",
            "isVisible": true,
            "left": "38%",
            "skin": "sknlblLato696c7313px",
            "text": "SCREEN NAME",
            "top": "15dp",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, controller.args[0], "lblScreenNameHeader"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblScreenNameHeader"), extendConfig({}, controller.args[2], "lblScreenNameHeader"));
        var lblSeparator = new kony.ui.Label(extendConfig({
            "bottom": "0dp",
            "height": "1px",
            "id": "lblSeparator",
            "isVisible": true,
            "left": "0dp",
            "right": "0dp",
            "skin": "sknlblSeperatorD7D9E0",
            "text": "Label",
            "zIndex": 2
        }, controller.args[0], "lblSeparator"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_TOP_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblSeparator"), extendConfig({}, controller.args[2], "lblSeparator"));
        flxEventsSegHeader.add(lblAdPlaceholderTypeHeader, lblScreenNameHeader, lblSeparator);
        var flxSegData = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
            "clipBounds": true,
            "id": "flxSegData",
            "isVisible": true,
            "layoutType": kony.flex.FLOW_VERTICAL,
            "left": "0dp",
            "isModalContainer": false,
            "skin": "slFbox",
            "top": "0dp",
            "width": "100%"
        }, controller.args[0], "flxSegData"), extendConfig({}, controller.args[1], "flxSegData"), extendConfig({}, controller.args[2], "flxSegData"));
        flxSegData.setDefaultUnit(kony.flex.DP);
        var flxSegWEB = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
            "clipBounds": true,
            "id": "flxSegWEB",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "left": "0dp",
            "isModalContainer": false,
            "skin": "slFbox",
            "top": "0dp",
            "width": "100%"
        }, controller.args[0], "flxSegWEB"), extendConfig({}, controller.args[1], "flxSegWEB"), extendConfig({}, controller.args[2], "flxSegWEB"));
        flxSegWEB.setDefaultUnit(kony.flex.DP);
        var lblAdTypeWEB = new kony.ui.Label(extendConfig({
            "id": "lblAdTypeWEB",
            "isVisible": true,
            "left": "12dp",
            "skin": "sknLbl485C75LatoRegular13Px",
            "text": "Web Popup Ad Banner",
            "top": "15dp",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, controller.args[0], "lblAdTypeWEB"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblAdTypeWEB"), extendConfig({}, controller.args[2], "lblAdTypeWEB"));
        var segEventsWEB = new kony.ui.SegmentedUI2(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
            "data": [{
                "lblDelete": "kony.i18n.getLocalizedString(\"i18n.frmFAQ.fonticonDelete\")",
                "lblEvent": "Events Name",
                "lblEventCode": "Events Code",
                "lblEventSource": "Events Source",
                "lblSeparator": "Label"
            }, {
                "lblDelete": "kony.i18n.getLocalizedString(\"i18n.frmFAQ.fonticonDelete\")",
                "lblEvent": "Events Name",
                "lblEventCode": "Events Code",
                "lblEventSource": "Events Source",
                "lblSeparator": "Label"
            }, {
                "lblDelete": "kony.i18n.getLocalizedString(\"i18n.frmFAQ.fonticonDelete\")",
                "lblEvent": "Events Name",
                "lblEventCode": "Events Code",
                "lblEventSource": "Events Source",
                "lblSeparator": "Label"
            }],
            "groupCells": false,
            "id": "segEventsWEB",
            "isVisible": true,
            "left": "38%",
            "needPageIndicator": true,
            "pageOffDotImage": "pageoffdot.png",
            "pageOnDotImage": "pageondot.png",
            "retainSelection": false,
            "rowFocusSkin": "seg2Focus",
            "rowSkin": "sknseg3pxRadius",
            "rowTemplate": "flxEventsView",
            "sectionHeaderSkin": "sliPhoneSegmentHeader",
            "selectionBehavior": constants.SEGUI_DEFAULT_BEHAVIOR,
            "separatorRequired": false,
            "showScrollbars": false,
            "top": "0dp",
            "viewType": constants.SEGUI_VIEW_TYPE_TABLEVIEW,
            "widgetDataMap": {
                "flxDelete": "flxDelete",
                "flxEventsView": "flxEventsView",
                "lblDelete": "lblDelete",
                "lblEvent": "lblEvent",
                "lblEventCode": "lblEventCode",
                "lblEventSource": "lblEventSource",
                "lblSeparator": "lblSeparator"
            },
            "width": "100%",
            "zIndex": 1
        }, controller.args[0], "segEventsWEB"), extendConfig({
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "segEventsWEB"), extendConfig({}, controller.args[2], "segEventsWEB"));
        flxSegWEB.add(lblAdTypeWEB, segEventsWEB);
        var lblSeparator2 = new kony.ui.Label(extendConfig({
            "height": "1px",
            "id": "lblSeparator2",
            "isVisible": true,
            "left": "0dp",
            "skin": "sknlblSeperatorD7D9E0",
            "text": "Label",
            "top": "0dp",
            "width": "100%",
            "zIndex": 2
        }, controller.args[0], "lblSeparator2"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_TOP_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblSeparator2"), extendConfig({}, controller.args[2], "lblSeparator2"));
        var flxSegMOB = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
            "clipBounds": true,
            "id": "flxSegMOB",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "left": "0dp",
            "isModalContainer": false,
            "skin": "slFbox",
            "top": "0dp",
            "width": "100%"
        }, controller.args[0], "flxSegMOB"), extendConfig({}, controller.args[1], "flxSegMOB"), extendConfig({}, controller.args[2], "flxSegMOB"));
        flxSegMOB.setDefaultUnit(kony.flex.DP);
        var lblAdTypeMOB = new kony.ui.Label(extendConfig({
            "id": "lblAdTypeMOB",
            "isVisible": true,
            "left": "12dp",
            "skin": "sknLbl485C75LatoRegular13Px",
            "text": "Mobile Popup Ad Banner",
            "top": "15dp",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, controller.args[0], "lblAdTypeMOB"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblAdTypeMOB"), extendConfig({}, controller.args[2], "lblAdTypeMOB"));
        var segEventsMOB = new kony.ui.SegmentedUI2(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
            "data": [{
                "lblDelete": "kony.i18n.getLocalizedString(\"i18n.frmFAQ.fonticonDelete\")",
                "lblEvent": "Events Name",
                "lblEventCode": "Events Code",
                "lblEventSource": "Events Source",
                "lblSeparator": "Label"
            }, {
                "lblDelete": "kony.i18n.getLocalizedString(\"i18n.frmFAQ.fonticonDelete\")",
                "lblEvent": "Events Name",
                "lblEventCode": "Events Code",
                "lblEventSource": "Events Source",
                "lblSeparator": "Label"
            }, {
                "lblDelete": "kony.i18n.getLocalizedString(\"i18n.frmFAQ.fonticonDelete\")",
                "lblEvent": "Events Name",
                "lblEventCode": "Events Code",
                "lblEventSource": "Events Source",
                "lblSeparator": "Label"
            }],
            "groupCells": false,
            "id": "segEventsMOB",
            "isVisible": true,
            "left": "38%",
            "needPageIndicator": true,
            "pageOffDotImage": "pageoffdot.png",
            "pageOnDotImage": "pageondot.png",
            "retainSelection": false,
            "rowFocusSkin": "seg2Focus",
            "rowSkin": "sknseg3pxRadius",
            "rowTemplate": "flxEventsView",
            "sectionHeaderSkin": "sliPhoneSegmentHeader",
            "selectionBehavior": constants.SEGUI_DEFAULT_BEHAVIOR,
            "separatorRequired": false,
            "showScrollbars": false,
            "top": "0dp",
            "viewType": constants.SEGUI_VIEW_TYPE_TABLEVIEW,
            "widgetDataMap": {
                "flxDelete": "flxDelete",
                "flxEventsView": "flxEventsView",
                "lblDelete": "lblDelete",
                "lblEvent": "lblEvent",
                "lblEventCode": "lblEventCode",
                "lblEventSource": "lblEventSource",
                "lblSeparator": "lblSeparator"
            },
            "width": "100%",
            "zIndex": 1
        }, controller.args[0], "segEventsMOB"), extendConfig({
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "segEventsMOB"), extendConfig({}, controller.args[2], "segEventsMOB"));
        flxSegMOB.add(lblAdTypeMOB, segEventsMOB);
        flxSegData.add(flxSegWEB, lblSeparator2, flxSegMOB);
        var flxNoResultFound = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "clipBounds": true,
            "height": "150dp",
            "id": "flxNoResultFound",
            "isVisible": false,
            "layoutType": kony.flex.FREE_FORM,
            "left": "0dp",
            "isModalContainer": false,
            "skin": "slFbox",
            "top": "0dp",
            "width": "100%",
            "zIndex": 1
        }, controller.args[0], "flxNoResultFound"), extendConfig({}, controller.args[1], "flxNoResultFound"), extendConfig({}, controller.args[2], "flxNoResultFound"));
        flxNoResultFound.setDefaultUnit(kony.flex.DP);
        var rtxSearchMesg = new kony.ui.RichText(extendConfig({
            "centerX": "50%",
            "centerY": "50%",
            "id": "rtxSearchMesg",
            "isVisible": true,
            "left": "0",
            "skin": "sknRtxLato84939e12Px",
            "text": "No Results found",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, controller.args[0], "rtxSearchMesg"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_CENTER,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "rtxSearchMesg"), extendConfig({}, controller.args[2], "rtxSearchMesg"));
        flxNoResultFound.add(rtxSearchMesg);
        flxEventsBody.add(flxEventsSegHeader, flxSegData, flxNoResultFound);
        viewPopupEvents.add(flxEventsHeader, flxEventsBody);
        return viewPopupEvents;
    }
})