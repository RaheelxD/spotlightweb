define(function() {
    return function(controller) {
        var postLoginSettingspopup = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
            "centerX": "50%",
            "clipBounds": false,
            "isMaster": true,
            "id": "postLoginSettingspopup",
            "isVisible": true,
            "layoutType": kony.flex.FLOW_VERTICAL,
            "isModalContainer": false,
            "skin": "slFbox0j9f841cc563e4e",
            "top": "250px",
            "width": "550px",
            "zIndex": 10
        }, controller.args[0], "postLoginSettingspopup"), extendConfig({}, controller.args[1], "postLoginSettingspopup"), extendConfig({}, controller.args[2], "postLoginSettingspopup"));
        postLoginSettingspopup.setDefaultUnit(kony.flex.DP);
        var flxPopUpTopColor = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "clipBounds": true,
            "height": "10px",
            "id": "flxPopUpTopColor",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "left": "0dp",
            "isModalContainer": false,
            "skin": "sknFlxInfoToastBg357C9ENoRadius",
            "top": "0dp",
            "width": "100%",
            "zIndex": 1
        }, controller.args[0], "flxPopUpTopColor"), extendConfig({}, controller.args[1], "flxPopUpTopColor"), extendConfig({}, controller.args[2], "flxPopUpTopColor"));
        flxPopUpTopColor.setDefaultUnit(kony.flex.DP);
        flxPopUpTopColor.add();
        var flxPopupHeader = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
            "clipBounds": true,
            "id": "flxPopupHeader",
            "isVisible": true,
            "layoutType": kony.flex.FLOW_VERTICAL,
            "left": "0dp",
            "isModalContainer": false,
            "skin": "slFbox0d9c3974835234d",
            "top": "0px",
            "width": "100%",
            "zIndex": 1
        }, controller.args[0], "flxPopupHeader"), extendConfig({}, controller.args[1], "flxPopupHeader"), extendConfig({}, controller.args[2], "flxPopupHeader"));
        flxPopupHeader.setDefaultUnit(kony.flex.DP);
        var flxPopUpClose = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "clipBounds": true,
            "height": "25px",
            "id": "flxPopUpClose",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "isModalContainer": false,
            "right": 15,
            "skin": "CopyslFbox0efc73ba91cad4b",
            "top": "15dp",
            "width": "25px",
            "zIndex": 20
        }, controller.args[0], "flxPopUpClose"), extendConfig({}, controller.args[1], "flxPopUpClose"), extendConfig({}, controller.args[2], "flxPopUpClose"));
        flxPopUpClose.setDefaultUnit(kony.flex.DP);
        var lblPopUpClose = new kony.ui.Label(extendConfig({
            "centerY": "50%",
            "id": "lblPopUpClose",
            "isVisible": true,
            "left": "4dp",
            "skin": "sknIcon18pxGray",
            "i18n_text": "kony.i18n.getLocalizedString(\"i18n.userwidgetmodel.lblClearSearch\")",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, controller.args[0], "lblPopUpClose"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblPopUpClose"), extendConfig({}, controller.args[2], "lblPopUpClose"));
        var fontIconImgCLose = new kony.ui.Label(extendConfig({
            "centerX": "50%",
            "centerY": "50.00%",
            "height": "15dp",
            "id": "fontIconImgCLose",
            "isVisible": true,
            "skin": "sknFontIconSearchCross16px",
            "i18n_text": "kony.i18n.getLocalizedString(\"i18n.userwidgetmodel.lblClearSearch\")",
            "width": "15dp",
            "zIndex": 2
        }, controller.args[0], "fontIconImgCLose"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "fontIconImgCLose"), extendConfig({}, controller.args[2], "fontIconImgCLose"));
        flxPopUpClose.add(lblPopUpClose, fontIconImgCLose);
        var lblPopUpMainMessage = new kony.ui.Label(extendConfig({
            "id": "lblPopUpMainMessage",
            "isVisible": true,
            "left": "20px",
            "skin": "sknlblLatoBold35475f23px",
            "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmAdManagement.ViewHideAds\")",
            "top": "0px",
            "width": "80%",
            "zIndex": 1
        }, controller.args[0], "lblPopUpMainMessage"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblPopUpMainMessage"), extendConfig({}, controller.args[2], "lblPopUpMainMessage"));
        var lblpostLogin = new kony.ui.Label(extendConfig({
            "id": "lblpostLogin",
            "isVisible": true,
            "left": "20px",
            "right": 20,
            "skin": "sknLbl485C75LatoRegular13Px",
            "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmAdManagement.PostLoginFullScreenonMobileApp\")",
            "top": "30px",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, controller.args[0], "lblpostLogin"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblpostLogin"), extendConfig({}, controller.args[2], "lblpostLogin"));
        var flxSwicth = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
            "clipBounds": true,
            "id": "flxSwicth",
            "isVisible": true,
            "layoutType": kony.flex.FLOW_HORIZONTAL,
            "left": "0dp",
            "isModalContainer": false,
            "skin": "slFbox",
            "top": "15dp",
            "width": "100%"
        }, controller.args[0], "flxSwicth"), extendConfig({}, controller.args[1], "flxSwicth"), extendConfig({}, controller.args[2], "flxSwicth"));
        flxSwicth.setDefaultUnit(kony.flex.DP);
        var lblShowAd = new kony.ui.Label(extendConfig({
            "id": "lblShowAd",
            "isVisible": true,
            "left": "20px",
            "right": 20,
            "skin": "sknLbl485C75LatoRegular13Px",
            "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmAdManagement.ShowAdaspopup\")",
            "top": "0",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, controller.args[0], "lblShowAd"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblShowAd"), extendConfig({}, controller.args[2], "lblShowAd"));
        var switchShowAd = new kony.ui.Switch(extendConfig({
            "height": "20dp",
            "id": "switchShowAd",
            "isVisible": true,
            "left": "15dp",
            "selectedIndex": 0,
            "skin": "sknSwitchServiceManagement",
            "top": "0",
            "width": "35dp",
            "zIndex": 1
        }, controller.args[0], "switchShowAd"), extendConfig({
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "switchShowAd"), extendConfig({}, controller.args[2], "switchShowAd"));
        flxSwicth.add(lblShowAd, switchShowAd);
        var lblPostLoginNote = new kony.ui.Label(extendConfig({
            "bottom": "30px",
            "id": "lblPostLoginNote",
            "isVisible": true,
            "left": "20px",
            "right": 20,
            "skin": "skn485C75LatoRegular11Px",
            "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmAdManagement.PostLoginNote\")",
            "top": "15px",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, controller.args[0], "lblPostLoginNote"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblPostLoginNote"), extendConfig({}, controller.args[2], "lblPostLoginNote"));
        flxPopupHeader.add(flxPopUpClose, lblPopUpMainMessage, lblpostLogin, flxSwicth, lblPostLoginNote);
        var flxPopUpButtons = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "clipBounds": true,
            "height": "80px",
            "id": "flxPopUpButtons",
            "isVisible": true,
            "layoutType": kony.flex.FLOW_HORIZONTAL,
            "reverseLayoutDirection": true,
            "left": "0%",
            "isModalContainer": false,
            "skin": "slFbox0dc900c4572aa40",
            "top": "0px",
            "width": "100%",
            "zIndex": 1
        }, controller.args[0], "flxPopUpButtons"), extendConfig({}, controller.args[1], "flxPopUpButtons"), extendConfig({}, controller.args[2], "flxPopUpButtons"));
        flxPopUpButtons.setDefaultUnit(kony.flex.DP);
        var btnPopUpDelete = new kony.ui.Button(extendConfig({
            "centerY": "50%",
            "focusSkin": "sknBtn003E75LatoRegular13pxFFFFFFRad20px",
            "height": "40px",
            "id": "btnPopUpDelete",
            "isVisible": true,
            "minWidth": "100px",
            "right": "20px",
            "skin": "sknBtn003E75LatoRegular13pxFFFFFFRad20px",
            "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmAdManagement.UpdateCAPS\")",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 20
        }, controller.args[0], "btnPopUpDelete"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_CENTER,
            "displayText": true,
            "padding": [3, 0, 3, 0],
            "paddingInPixel": false
        }, controller.args[1], "btnPopUpDelete"), extendConfig({
            "hoverSkin": "sknBtn005198LatoRegular13pxFFFFFFRad20px"
        }, controller.args[2], "btnPopUpDelete"));
        var btnPopUpCancel = new kony.ui.Button(extendConfig({
            "centerY": "50%",
            "focusSkin": "sknBtnLatoRegulara5abc413pxKA",
            "height": "40px",
            "id": "btnPopUpCancel",
            "isVisible": true,
            "right": "20px",
            "skin": "sknBtnLatoRegulara5abc413pxKA",
            "i18n_text": "kony.i18n.getLocalizedString(\"i18n.permission.CANCEL\")",
            "top": "0%",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 20
        }, controller.args[0], "btnPopUpCancel"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_CENTER,
            "displayText": true,
            "padding": [3, 0, 3, 0],
            "paddingInPixel": false
        }, controller.args[1], "btnPopUpCancel"), extendConfig({
            "hoverSkin": "sknbtnffffffLatoBolda5abc413Pxd7d8e01pxBorderRadius20px"
        }, controller.args[2], "btnPopUpCancel"));
        flxPopUpButtons.add(btnPopUpDelete, btnPopUpCancel);
        postLoginSettingspopup.add(flxPopUpTopColor, flxPopupHeader, flxPopUpButtons);
        return postLoginSettingspopup;
    }
})