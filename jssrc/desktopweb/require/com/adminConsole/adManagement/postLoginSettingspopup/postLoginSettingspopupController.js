define("com/adminConsole/adManagement/postLoginSettingspopup/userpostLoginSettingspopupController", function() {
    return {};
});
define("com/adminConsole/adManagement/postLoginSettingspopup/postLoginSettingspopupControllerActions", {
    /*
          This is an auto generated file and any modifications to it may result in corruption of the action sequence.
        */
});
define("com/adminConsole/adManagement/postLoginSettingspopup/postLoginSettingspopupController", ["com/adminConsole/adManagement/postLoginSettingspopup/userpostLoginSettingspopupController", "com/adminConsole/adManagement/postLoginSettingspopup/postLoginSettingspopupControllerActions"], function() {
    var controller = require("com/adminConsole/adManagement/postLoginSettingspopup/userpostLoginSettingspopupController");
    var actions = require("com/adminConsole/adManagement/postLoginSettingspopup/postLoginSettingspopupControllerActions");
    for (var key in actions) {
        controller[key] = actions[key];
    }
    return controller;
});
