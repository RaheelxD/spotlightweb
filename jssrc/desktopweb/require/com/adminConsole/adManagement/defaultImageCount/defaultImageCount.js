define(function() {
    return function(controller) {
        var defaultImageCount = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
            "clipBounds": true,
            "isMaster": true,
            "id": "defaultImageCount",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "left": "0dp",
            "isModalContainer": false,
            "skin": "slFbox",
            "top": "15dp",
            "width": kony.flex.USE_PREFFERED_SIZE
        }, controller.args[0], "defaultImageCount"), extendConfig({}, controller.args[1], "defaultImageCount"), extendConfig({}, controller.args[2], "defaultImageCount"));
        defaultImageCount.setDefaultUnit(kony.flex.DP);
        var flxDefaultImgCountContainer = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
            "clipBounds": true,
            "id": "flxDefaultImgCountContainer",
            "isVisible": true,
            "layoutType": kony.flex.FLOW_VERTICAL,
            "left": "20dp",
            "isModalContainer": false,
            "right": 20,
            "skin": "slFbox",
            "top": "0dp",
            "width": kony.flex.USE_PREFFERED_SIZE
        }, controller.args[0], "flxDefaultImgCountContainer"), extendConfig({}, controller.args[1], "flxDefaultImgCountContainer"), extendConfig({}, controller.args[2], "flxDefaultImgCountContainer"));
        flxDefaultImgCountContainer.setDefaultUnit(kony.flex.DP);
        var lblNoOfImages = new kony.ui.Label(extendConfig({
            "id": "lblNoOfImages",
            "isVisible": true,
            "left": "0dp",
            "skin": "sknLbl485C75LatoRegular13Px",
            "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmAdManagement.NumberofImages\")",
            "top": "0",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, controller.args[0], "lblNoOfImages"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblNoOfImages"), extendConfig({}, controller.args[2], "lblNoOfImages"));
        var flxImageCounter = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "clipBounds": true,
            "height": "70dp",
            "id": "flxImageCounter",
            "isVisible": true,
            "layoutType": kony.flex.FLOW_VERTICAL,
            "left": "0dp",
            "isModalContainer": false,
            "skin": "sknFlxbgFFFFFFbdrD7D9E0rd3px",
            "top": "15dp",
            "width": "435dp",
            "zIndex": 1
        }, controller.args[0], "flxImageCounter"), extendConfig({}, controller.args[1], "flxImageCounter"), extendConfig({}, controller.args[2], "flxImageCounter"));
        flxImageCounter.setDefaultUnit(kony.flex.DP);
        var flxRadioContainer = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "clipBounds": true,
            "height": "40dp",
            "id": "flxRadioContainer",
            "isVisible": true,
            "layoutType": kony.flex.FLOW_HORIZONTAL,
            "left": "0dp",
            "isModalContainer": false,
            "skin": "sknBackgroundFFFFFF",
            "top": "0dp",
            "width": "100%",
            "zIndex": 1
        }, controller.args[0], "flxRadioContainer"), extendConfig({}, controller.args[1], "flxRadioContainer"), extendConfig({}, controller.args[2], "flxRadioContainer"));
        flxRadioContainer.setDefaultUnit(kony.flex.DP);
        var flxRadio1 = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "clipBounds": true,
            "height": "101%",
            "id": "flxRadio1",
            "isVisible": true,
            "layoutType": kony.flex.FLOW_HORIZONTAL,
            "left": -1,
            "isModalContainer": false,
            "skin": "sknborderd7d9e0bgfff0px",
            "top": "-1dp",
            "width": "146dp"
        }, controller.args[0], "flxRadio1"), extendConfig({}, controller.args[1], "flxRadio1"), extendConfig({}, controller.args[2], "flxRadio1"));
        flxRadio1.setDefaultUnit(kony.flex.DP);
        var flxRadioOption1 = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "centerY": "50%",
            "clipBounds": true,
            "height": "20dp",
            "id": "flxRadioOption1",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "left": "55px",
            "isModalContainer": false,
            "skin": "slFbox",
            "width": "20px",
            "zIndex": 1
        }, controller.args[0], "flxRadioOption1"), extendConfig({}, controller.args[1], "flxRadioOption1"), extendConfig({
            "hoverSkin": "sknCursor"
        }, controller.args[2], "flxRadioOption1"));
        flxRadioOption1.setDefaultUnit(kony.flex.DP);
        var imgRadio1 = new kony.ui.Image2(extendConfig({
            "centerX": "50%",
            "centerY": "50%",
            "height": "15dp",
            "id": "imgRadio1",
            "isVisible": true,
            "skin": "slImage",
            "src": "radio_notselected.png",
            "width": "15dp",
            "zIndex": 1
        }, controller.args[0], "imgRadio1"), extendConfig({
            "imageScaleMode": constants.IMAGE_SCALE_MODE_MAINTAIN_ASPECT_RATIO,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "imgRadio1"), extendConfig({}, controller.args[2], "imgRadio1"));
        flxRadioOption1.add(imgRadio1);
        var lblRadioOption1 = new kony.ui.Label(extendConfig({
            "centerY": "50%",
            "id": "lblRadioOption1",
            "isVisible": true,
            "left": "5dp",
            "skin": "slLabel0d20174dce8ea42",
            "text": "1",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, controller.args[0], "lblRadioOption1"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblRadioOption1"), extendConfig({}, controller.args[2], "lblRadioOption1"));
        flxRadio1.add(flxRadioOption1, lblRadioOption1);
        var flxRadio2 = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "clipBounds": true,
            "height": "101%",
            "id": "flxRadio2",
            "isVisible": true,
            "layoutType": kony.flex.FLOW_HORIZONTAL,
            "left": "-1dp",
            "isModalContainer": false,
            "skin": "sknborderd7d9e0bgfff0px",
            "top": "-1dp",
            "width": "146dp"
        }, controller.args[0], "flxRadio2"), extendConfig({}, controller.args[1], "flxRadio2"), extendConfig({}, controller.args[2], "flxRadio2"));
        flxRadio2.setDefaultUnit(kony.flex.DP);
        var flxRadioOption2 = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "centerY": "50%",
            "clipBounds": true,
            "height": "20dp",
            "id": "flxRadioOption2",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "left": "55px",
            "isModalContainer": false,
            "skin": "slFbox",
            "width": "20px",
            "zIndex": 1
        }, controller.args[0], "flxRadioOption2"), extendConfig({}, controller.args[1], "flxRadioOption2"), extendConfig({
            "hoverSkin": "sknCursor"
        }, controller.args[2], "flxRadioOption2"));
        flxRadioOption2.setDefaultUnit(kony.flex.DP);
        var imgRadio2 = new kony.ui.Image2(extendConfig({
            "centerX": "50%",
            "centerY": "50%",
            "height": "15dp",
            "id": "imgRadio2",
            "isVisible": true,
            "skin": "slImage",
            "src": "radio_notselected.png",
            "width": "15dp",
            "zIndex": 1
        }, controller.args[0], "imgRadio2"), extendConfig({
            "imageScaleMode": constants.IMAGE_SCALE_MODE_MAINTAIN_ASPECT_RATIO,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "imgRadio2"), extendConfig({}, controller.args[2], "imgRadio2"));
        flxRadioOption2.add(imgRadio2);
        var lblRadioOption2 = new kony.ui.Label(extendConfig({
            "centerY": "50%",
            "id": "lblRadioOption2",
            "isVisible": true,
            "left": "5dp",
            "skin": "slLabel0d20174dce8ea42",
            "text": "2",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, controller.args[0], "lblRadioOption2"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblRadioOption2"), extendConfig({}, controller.args[2], "lblRadioOption2"));
        flxRadio2.add(flxRadioOption2, lblRadioOption2);
        var flxRadio3 = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "clipBounds": true,
            "height": "101%",
            "id": "flxRadio3",
            "isVisible": true,
            "layoutType": kony.flex.FLOW_HORIZONTAL,
            "left": "-1dp",
            "isModalContainer": false,
            "skin": "sknborderd7d9e0bgfff0px",
            "top": "-1dp",
            "width": "146dp"
        }, controller.args[0], "flxRadio3"), extendConfig({}, controller.args[1], "flxRadio3"), extendConfig({}, controller.args[2], "flxRadio3"));
        flxRadio3.setDefaultUnit(kony.flex.DP);
        var flxRadioOption3 = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "centerY": "50%",
            "clipBounds": true,
            "height": "20dp",
            "id": "flxRadioOption3",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "left": "55px",
            "isModalContainer": false,
            "skin": "slFbox",
            "width": "20px",
            "zIndex": 1
        }, controller.args[0], "flxRadioOption3"), extendConfig({}, controller.args[1], "flxRadioOption3"), extendConfig({
            "hoverSkin": "sknCursor"
        }, controller.args[2], "flxRadioOption3"));
        flxRadioOption3.setDefaultUnit(kony.flex.DP);
        var imgRadio3 = new kony.ui.Image2(extendConfig({
            "centerX": "50%",
            "centerY": "50%",
            "height": "15dp",
            "id": "imgRadio3",
            "isVisible": true,
            "skin": "slImage",
            "src": "radio_notselected.png",
            "width": "15dp",
            "zIndex": 1
        }, controller.args[0], "imgRadio3"), extendConfig({
            "imageScaleMode": constants.IMAGE_SCALE_MODE_MAINTAIN_ASPECT_RATIO,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "imgRadio3"), extendConfig({}, controller.args[2], "imgRadio3"));
        flxRadioOption3.add(imgRadio3);
        var lblRadioOption3 = new kony.ui.Label(extendConfig({
            "centerY": "50%",
            "id": "lblRadioOption3",
            "isVisible": true,
            "left": "5dp",
            "skin": "slLabel0d20174dce8ea42",
            "text": "3",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, controller.args[0], "lblRadioOption3"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblRadioOption3"), extendConfig({}, controller.args[2], "lblRadioOption3"));
        flxRadio3.add(flxRadioOption3, lblRadioOption3);
        flxRadioContainer.add(flxRadio1, flxRadio2, flxRadio3);
        var flxLabels = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "clipBounds": true,
            "height": "30dp",
            "id": "flxLabels",
            "isVisible": true,
            "layoutType": kony.flex.FLOW_HORIZONTAL,
            "left": "0dp",
            "isModalContainer": false,
            "skin": "sknTbxDisabledf3f3f3",
            "top": "0dp",
            "width": "100%",
            "zIndex": 1
        }, controller.args[0], "flxLabels"), extendConfig({}, controller.args[1], "flxLabels"), extendConfig({}, controller.args[2], "flxLabels"));
        flxLabels.setDefaultUnit(kony.flex.DP);
        var lblSingleImage = new kony.ui.Label(extendConfig({
            "centerY": "50%",
            "id": "lblSingleImage",
            "isVisible": true,
            "left": "30dp",
            "skin": "sknlblLato696c7313px",
            "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmAdManagement.SingleImage\")",
            "top": "0",
            "width": "75dp",
            "zIndex": 1
        }, controller.args[0], "lblSingleImage"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblSingleImage"), extendConfig({}, controller.args[2], "lblSingleImage"));
        var lblSeperator = new kony.ui.Label(extendConfig({
            "height": "100%",
            "id": "lblSeperator",
            "isVisible": true,
            "left": "39dp",
            "skin": "sknlblSeperatorD7D9E0",
            "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmAdManagement.SingleImage\")",
            "top": "0",
            "width": "1dp",
            "zIndex": 20
        }, controller.args[0], "lblSeperator"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblSeperator"), extendConfig({}, controller.args[2], "lblSeperator"));
        var lblCarousel = new kony.ui.Label(extendConfig({
            "centerY": "50%",
            "id": "lblCarousel",
            "isVisible": true,
            "left": "120dp",
            "skin": "sknlblLato696c7313px",
            "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmAdManagement.Carousel\")",
            "top": "0",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, controller.args[0], "lblCarousel"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblCarousel"), extendConfig({}, controller.args[2], "lblCarousel"));
        flxLabels.add(lblSingleImage, lblSeperator, lblCarousel);
        flxImageCounter.add(flxRadioContainer, flxLabels);
        flxDefaultImgCountContainer.add(lblNoOfImages, flxImageCounter);
        defaultImageCount.add(flxDefaultImgCountContainer);
        return defaultImageCount;
    }
})