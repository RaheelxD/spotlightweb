define("com/adminConsole/adManagement/defaultImageCount/userdefaultImageCountController", function() {
    return {};
});
define("com/adminConsole/adManagement/defaultImageCount/defaultImageCountControllerActions", {
    /*
          This is an auto generated file and any modifications to it may result in corruption of the action sequence.
        */
});
define("com/adminConsole/adManagement/defaultImageCount/defaultImageCountController", ["com/adminConsole/adManagement/defaultImageCount/userdefaultImageCountController", "com/adminConsole/adManagement/defaultImageCount/defaultImageCountControllerActions"], function() {
    var controller = require("com/adminConsole/adManagement/defaultImageCount/userdefaultImageCountController");
    var actions = require("com/adminConsole/adManagement/defaultImageCount/defaultImageCountControllerActions");
    for (var key in actions) {
        controller[key] = actions[key];
    }
    return controller;
});
