define(function() {
    return function(controller) {
        var channelScreen = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
            "clipBounds": true,
            "isMaster": true,
            "id": "channelScreen",
            "isVisible": true,
            "layoutType": kony.flex.FLOW_VERTICAL,
            "left": "0dp",
            "isModalContainer": false,
            "skin": "sknBackgroundFFFFFF",
            "top": "0dp",
            "width": "100%"
        }, controller.args[0], "channelScreen"), extendConfig({}, controller.args[1], "channelScreen"), extendConfig({}, controller.args[2], "channelScreen"));
        channelScreen.setDefaultUnit(kony.flex.DP);
        var lblScreenName = new kony.ui.Label(extendConfig({
            "height": "40px",
            "id": "lblScreenName",
            "isVisible": true,
            "left": "20px",
            "skin": "sknLbl192B45LatoRegular14px",
            "text": "Pre Login Screen",
            "top": "0px",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, controller.args[0], "lblScreenName"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblScreenName"), extendConfig({}, controller.args[2], "lblScreenName"));
        var flxSeparator1 = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "centerX": "50%",
            "clipBounds": true,
            "height": "1px",
            "id": "flxSeparator1",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "isModalContainer": false,
            "skin": "sknFlxD7D9E0Separator",
            "top": "0",
            "width": "100%",
            "zIndex": 1
        }, controller.args[0], "flxSeparator1"), extendConfig({}, controller.args[1], "flxSeparator1"), extendConfig({}, controller.args[2], "flxSeparator1"));
        flxSeparator1.setDefaultUnit(kony.flex.DP);
        flxSeparator1.add();
        var flxURLHeader = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
            "clipBounds": true,
            "id": "flxURLHeader",
            "isVisible": true,
            "layoutType": kony.flex.FLOW_VERTICAL,
            "left": "0dp",
            "isModalContainer": false,
            "skin": "slFbox",
            "top": "0dp",
            "width": "100%"
        }, controller.args[0], "flxURLHeader"), extendConfig({}, controller.args[1], "flxURLHeader"), extendConfig({}, controller.args[2], "flxURLHeader"));
        flxURLHeader.setDefaultUnit(kony.flex.DP);
        var flxTableHeader = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
            "clipBounds": true,
            "id": "flxTableHeader",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "left": "0dp",
            "isModalContainer": false,
            "skin": "slFbox",
            "top": "0dp",
            "width": "100%"
        }, controller.args[0], "flxTableHeader"), extendConfig({}, controller.args[1], "flxTableHeader"), extendConfig({}, controller.args[2], "flxTableHeader"));
        flxTableHeader.setDefaultUnit(kony.flex.DP);
        var flxULRLabels = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "clipBounds": true,
            "height": "40px",
            "id": "flxULRLabels",
            "isVisible": true,
            "layoutType": kony.flex.FLOW_HORIZONTAL,
            "left": "20dp",
            "isModalContainer": false,
            "right": "20dp",
            "skin": "slFbox",
            "top": "0",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, controller.args[0], "flxULRLabels"), extendConfig({}, controller.args[1], "flxULRLabels"), extendConfig({}, controller.args[2], "flxULRLabels"));
        flxULRLabels.setDefaultUnit(kony.flex.DP);
        var lblResolution = new kony.ui.Label(extendConfig({
            "centerY": "50%",
            "height": "13px",
            "id": "lblResolution",
            "isVisible": true,
            "left": "0%",
            "skin": "sknLbl78818A11px",
            "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmAdManagement.ResolutionCAPS\")",
            "top": "0%",
            "width": "32%",
            "zIndex": 1
        }, controller.args[0], "lblResolution"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblResolution"), extendConfig({}, controller.args[2], "lblResolution"));
        var lblImgSourceURLTag = new kony.ui.Label(extendConfig({
            "centerY": "50%",
            "height": "13px",
            "id": "lblImgSourceURLTag",
            "isVisible": true,
            "left": "2%",
            "skin": "sknLbl78818A11px",
            "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmAdManagement.ImageSourceURLCAPS\")",
            "top": "0%",
            "width": "32%",
            "zIndex": 1
        }, controller.args[0], "lblImgSourceURLTag"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblImgSourceURLTag"), extendConfig({}, controller.args[2], "lblImgSourceURLTag"));
        var lblTargetURLTag = new kony.ui.Label(extendConfig({
            "centerY": "50%",
            "height": "13px",
            "id": "lblTargetURLTag",
            "isVisible": true,
            "left": "2%",
            "skin": "sknLbl78818A11px",
            "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmAdManagement.TargetURLCAPS\")",
            "top": "0px",
            "width": "32%",
            "zIndex": 1
        }, controller.args[0], "lblTargetURLTag"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblTargetURLTag"), extendConfig({}, controller.args[2], "lblTargetURLTag"));
        flxULRLabels.add(lblResolution, lblImgSourceURLTag, lblTargetURLTag);
        flxTableHeader.add(flxULRLabels);
        var flxDefTableHeader = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
            "clipBounds": true,
            "id": "flxDefTableHeader",
            "isVisible": false,
            "layoutType": kony.flex.FREE_FORM,
            "left": "0dp",
            "isModalContainer": false,
            "skin": "slFbox",
            "top": "0dp",
            "width": "100%"
        }, controller.args[0], "flxDefTableHeader"), extendConfig({}, controller.args[1], "flxDefTableHeader"), extendConfig({}, controller.args[2], "flxDefTableHeader"));
        flxDefTableHeader.setDefaultUnit(kony.flex.DP);
        var flxDefURLLabels = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "clipBounds": true,
            "height": "40px",
            "id": "flxDefURLLabels",
            "isVisible": true,
            "layoutType": kony.flex.FLOW_HORIZONTAL,
            "left": "20dp",
            "isModalContainer": false,
            "right": "20dp",
            "skin": "slFbox",
            "top": "0",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, controller.args[0], "flxDefURLLabels"), extendConfig({}, controller.args[1], "flxDefURLLabels"), extendConfig({}, controller.args[2], "flxDefURLLabels"));
        flxDefURLLabels.setDefaultUnit(kony.flex.DP);
        var lblDefResolution = new kony.ui.Label(extendConfig({
            "centerY": "50%",
            "height": "13px",
            "id": "lblDefResolution",
            "isVisible": true,
            "left": "0%",
            "skin": "sknLbl78818A11px",
            "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmAdManagement.ResolutionCAPS\")",
            "top": "0%",
            "width": "18%",
            "zIndex": 1
        }, controller.args[0], "lblDefResolution"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblDefResolution"), extendConfig({}, controller.args[2], "lblDefResolution"));
        var lblImageContainer = new kony.ui.Label(extendConfig({
            "centerY": "50%",
            "id": "lblImageContainer",
            "isVisible": true,
            "left": "2%",
            "skin": "sknLbl78818A11px",
            "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmAdManagement.ImageContainerName_Scale\")",
            "top": "0%",
            "width": "20%",
            "zIndex": 1
        }, controller.args[0], "lblImageContainer"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblImageContainer"), extendConfig({}, controller.args[2], "lblImageContainer"));
        var lblDefImgSourceURL = new kony.ui.Label(extendConfig({
            "centerY": "50%",
            "height": "13px",
            "id": "lblDefImgSourceURL",
            "isVisible": true,
            "left": "2%",
            "skin": "sknLbl78818A11px",
            "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmAdManagement.ImageSourceURLCAPS\")",
            "top": "0%",
            "width": "28%",
            "zIndex": 1
        }, controller.args[0], "lblDefImgSourceURL"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblDefImgSourceURL"), extendConfig({}, controller.args[2], "lblDefImgSourceURL"));
        var lblDefTargetURL = new kony.ui.Label(extendConfig({
            "centerY": "50%",
            "height": "13px",
            "id": "lblDefTargetURL",
            "isVisible": true,
            "left": "2%",
            "skin": "sknLbl78818A11px",
            "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmAdManagement.TargetURLCAPS\")",
            "top": "0px",
            "width": "28%",
            "zIndex": 1
        }, controller.args[0], "lblDefTargetURL"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblDefTargetURL"), extendConfig({}, controller.args[2], "lblDefTargetURL"));
        flxDefURLLabels.add(lblDefResolution, lblImageContainer, lblDefImgSourceURL, lblDefTargetURL);
        flxDefTableHeader.add(flxDefURLLabels);
        var flxSeparator2 = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "centerX": "50%",
            "clipBounds": true,
            "height": "1px",
            "id": "flxSeparator2",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "isModalContainer": false,
            "skin": "sknFlxD7D9E0Separator",
            "top": 0,
            "width": "100%",
            "zIndex": 1
        }, controller.args[0], "flxSeparator2"), extendConfig({}, controller.args[1], "flxSeparator2"), extendConfig({}, controller.args[2], "flxSeparator2"));
        flxSeparator2.setDefaultUnit(kony.flex.DP);
        flxSeparator2.add();
        var segCampaignsData = new kony.ui.SegmentedUI2(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
            "data": [{
                "lblImgSourceURL": "Label",
                "lblResolution": "Label",
                "lblTargetURL": "Label"
            }, {
                "lblImgSourceURL": "Label",
                "lblResolution": "Label",
                "lblTargetURL": "Label"
            }, {
                "lblImgSourceURL": "Label",
                "lblResolution": "Label",
                "lblTargetURL": "Label"
            }, {
                "lblImgSourceURL": "Label",
                "lblResolution": "Label",
                "lblTargetURL": "Label"
            }, {
                "lblImgSourceURL": "Label",
                "lblResolution": "Label",
                "lblTargetURL": "Label"
            }, {
                "lblImgSourceURL": "Label",
                "lblResolution": "Label",
                "lblTargetURL": "Label"
            }, {
                "lblImgSourceURL": "Label",
                "lblResolution": "Label",
                "lblTargetURL": "Label"
            }, {
                "lblImgSourceURL": "Label",
                "lblResolution": "Label",
                "lblTargetURL": "Label"
            }, {
                "lblImgSourceURL": "Label",
                "lblResolution": "Label",
                "lblTargetURL": "Label"
            }, {
                "lblImgSourceURL": "Label",
                "lblResolution": "Label",
                "lblTargetURL": "Label"
            }],
            "groupCells": false,
            "id": "segCampaignsData",
            "isVisible": true,
            "left": "0dp",
            "needPageIndicator": true,
            "pageOffDotImage": "pageoffdot.png",
            "pageOnDotImage": "pageondot.png",
            "retainSelection": false,
            "rowFocusSkin": "seg2Focus",
            "rowSkin": "seg2Normal",
            "rowTemplate": "flxDetailsURLs",
            "sectionHeaderSkin": "sliPhoneSegmentHeader",
            "selectionBehavior": constants.SEGUI_DEFAULT_BEHAVIOR,
            "separatorColor": "d7d9e000",
            "separatorRequired": true,
            "separatorThickness": 1,
            "showScrollbars": false,
            "top": "0dp",
            "viewType": constants.SEGUI_VIEW_TYPE_TABLEVIEW,
            "widgetDataMap": {
                "flxDetailsURLs": "flxDetailsURLs",
                "flxDetailsURLsParent": "flxDetailsURLsParent",
                "flxImgSourceURL": "flxImgSourceURL",
                "flxImgTargetURL": "flxImgTargetURL",
                "flxResolution": "flxResolution",
                "lblImgSourceURL": "lblImgSourceURL",
                "lblResolution": "lblResolution",
                "lblTargetURL": "lblTargetURL"
            },
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, controller.args[0], "segCampaignsData"), extendConfig({
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "segCampaignsData"), extendConfig({}, controller.args[2], "segCampaignsData"));
        var flxSeparator3 = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "clipBounds": true,
            "height": "1px",
            "id": "flxSeparator3",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "isModalContainer": false,
            "skin": "sknFlxD7D9E0Separator",
            "top": 0,
            "width": "100%",
            "zIndex": 1
        }, controller.args[0], "flxSeparator3"), extendConfig({}, controller.args[1], "flxSeparator3"), extendConfig({}, controller.args[2], "flxSeparator3"));
        flxSeparator3.setDefaultUnit(kony.flex.DP);
        flxSeparator3.add();
        flxURLHeader.add(flxTableHeader, flxDefTableHeader, flxSeparator2, segCampaignsData, flxSeparator3);
        channelScreen.add(lblScreenName, flxSeparator1, flxURLHeader);
        return channelScreen;
    }
})