define(function() {
    return function(controller) {
        var dataset = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
            "clipBounds": true,
            "isMaster": true,
            "id": "dataset",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "left": "0dp",
            "isModalContainer": false,
            "skin": "slFbox",
            "top": "0dp",
            "width": "100%"
        }, controller.args[0], "dataset"), extendConfig({}, controller.args[1], "dataset"), extendConfig({}, controller.args[2], "dataset"));
        dataset.setDefaultUnit(kony.flex.DP);
        var flxDatasetContainer = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
            "clipBounds": true,
            "id": "flxDatasetContainer",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "left": "10dp",
            "isModalContainer": false,
            "right": "10dp",
            "skin": "slFbox",
            "top": 0,
            "width": kony.flex.USE_PREFFERED_SIZE
        }, controller.args[0], "flxDatasetContainer"), extendConfig({}, controller.args[1], "flxDatasetContainer"), extendConfig({}, controller.args[2], "flxDatasetContainer"));
        flxDatasetContainer.setDefaultUnit(kony.flex.DP);
        var flxDataset = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
            "clipBounds": true,
            "id": "flxDataset",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "isModalContainer": false,
            "right": "0dp",
            "skin": "sknFlxbgFFFFFFbdrD7D9E0rd3px",
            "top": "10dp",
            "width": "100%",
            "zIndex": 1
        }, controller.args[0], "flxDataset"), extendConfig({}, controller.args[1], "flxDataset"), extendConfig({}, controller.args[2], "flxDataset"));
        flxDataset.setDefaultUnit(kony.flex.DP);
        var fonticonCheckBox = new kony.ui.Label(extendConfig({
            "height": "25dp",
            "id": "fonticonCheckBox",
            "isVisible": true,
            "left": "15dp",
            "skin": "sknFontIconCheckBoxUnselected",
            "text": "",
            "top": "5dp",
            "width": "25dp",
            "zIndex": 2
        }, controller.args[0], "fonticonCheckBox"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "fonticonCheckBox"), extendConfig({}, controller.args[2], "fonticonCheckBox"));
        var lblName = new kony.ui.Label(extendConfig({
            "bottom": "10dp",
            "id": "lblName",
            "isVisible": true,
            "left": "45dp",
            "skin": "sknLbl485C75LatoRegular13Px",
            "text": "KL098234 - Westheimer",
            "top": "10dp",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, controller.args[0], "lblName"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblName"), extendConfig({}, controller.args[2], "lblName"));
        var lblId = new kony.ui.Label(extendConfig({
            "bottom": "0dp",
            "id": "lblId",
            "isVisible": false,
            "left": "55dp",
            "skin": "sknLbl485C75LatoRegular13Px",
            "text": "Id",
            "top": "20dp",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, controller.args[0], "lblId"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblId"), extendConfig({}, controller.args[2], "lblId"));
        var fontIconArrow = new kony.ui.Label(extendConfig({
            "height": "25dp",
            "id": "fontIconArrow",
            "isVisible": false,
            "right": "5dp",
            "skin": "sknFontIcon006cca13px",
            "text": "",
            "top": "5dp",
            "width": "25dp",
            "zIndex": 2
        }, controller.args[0], "fontIconArrow"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "fontIconArrow"), extendConfig({}, controller.args[2], "fontIconArrow"));
        flxDataset.add(fonticonCheckBox, lblName, lblId, fontIconArrow);
        flxDatasetContainer.add(flxDataset);
        dataset.add(flxDatasetContainer);
        return dataset;
    }
})