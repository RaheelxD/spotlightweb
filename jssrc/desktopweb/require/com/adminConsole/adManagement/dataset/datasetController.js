define("com/adminConsole/adManagement/dataset/userdatasetController", function() {
    return {};
});
define("com/adminConsole/adManagement/dataset/datasetControllerActions", {
    /*
          This is an auto generated file and any modifications to it may result in corruption of the action sequence.
        */
});
define("com/adminConsole/adManagement/dataset/datasetController", ["com/adminConsole/adManagement/dataset/userdatasetController", "com/adminConsole/adManagement/dataset/datasetControllerActions"], function() {
    var controller = require("com/adminConsole/adManagement/dataset/userdatasetController");
    var actions = require("com/adminConsole/adManagement/dataset/datasetControllerActions");
    for (var key in actions) {
        controller[key] = actions[key];
    }
    return controller;
});
