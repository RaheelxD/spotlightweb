define(function() {
    return function(controller) {
        var viewOfflineChannelDetails = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
            "clipBounds": true,
            "isMaster": true,
            "id": "viewOfflineChannelDetails",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "left": "0dp",
            "isModalContainer": false,
            "skin": "slFbox",
            "top": "0dp",
            "width": "100%"
        }, controller.args[0], "viewOfflineChannelDetails"), extendConfig({}, controller.args[1], "viewOfflineChannelDetails"), extendConfig({}, controller.args[2], "viewOfflineChannelDetails"));
        viewOfflineChannelDetails.setDefaultUnit(kony.flex.DP);
        var flxOfflineSection = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
            "clipBounds": true,
            "id": "flxOfflineSection",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "left": "20dp",
            "isModalContainer": false,
            "right": "20dp",
            "skin": "sknFlxbgFFFFFFbdrD7D9E0rd3px",
            "top": "0dp",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, controller.args[0], "flxOfflineSection"), extendConfig({}, controller.args[1], "flxOfflineSection"), extendConfig({}, controller.args[2], "flxOfflineSection"));
        flxOfflineSection.setDefaultUnit(kony.flex.DP);
        var flxHeader = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "clipBounds": true,
            "height": "40dp",
            "id": "flxHeader",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "left": "0dp",
            "isModalContainer": false,
            "skin": "sknf5f6f8border0px",
            "top": "0dp",
            "width": "100%"
        }, controller.args[0], "flxHeader"), extendConfig({}, controller.args[1], "flxHeader"), extendConfig({}, controller.args[2], "flxHeader"));
        flxHeader.setDefaultUnit(kony.flex.DP);
        var flxChannelArrow = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "centerY": "50%",
            "clipBounds": true,
            "height": "15dp",
            "id": "flxChannelArrow",
            "isVisible": true,
            "layoutType": kony.flex.FLOW_HORIZONTAL,
            "left": "20dp",
            "isModalContainer": false,
            "onClick": controller.AS_FlexContainer_ab82dc3781f14807b3a7012e7d4aef47,
            "skin": "sknCursor",
            "width": "15dp"
        }, controller.args[0], "flxChannelArrow"), extendConfig({}, controller.args[1], "flxChannelArrow"), extendConfig({}, controller.args[2], "flxChannelArrow"));
        flxChannelArrow.setDefaultUnit(kony.flex.DP);
        var lblChannelArrow = new kony.ui.Label(extendConfig({
            "height": "15dp",
            "id": "lblChannelArrow",
            "isVisible": true,
            "left": "0dp",
            "skin": "sknfontIcon192b4b14px",
            "text": "",
            "top": "0dp",
            "width": "15dp",
            "zIndex": 1
        }, controller.args[0], "lblChannelArrow"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblChannelArrow"), extendConfig({}, controller.args[2], "lblChannelArrow"));
        flxChannelArrow.add(lblChannelArrow);
        var lblHeader = new kony.ui.Label(extendConfig({
            "centerY": "50%",
            "id": "lblHeader",
            "isVisible": true,
            "left": "45dp",
            "skin": "sknLbl16pxLato192B45",
            "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmAlertsManagement.lblEmailCheckBox\")",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, controller.args[0], "lblHeader"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblHeader"), extendConfig({}, controller.args[2], "lblHeader"));
        var btnPreview = new kony.ui.Button(extendConfig({
            "centerY": "50%",
            "focusSkin": "sknbtnffffffLatoRegular4f555dBorder1px485c75",
            "height": "20dp",
            "id": "btnPreview",
            "isVisible": true,
            "right": "20dp",
            "skin": "sknbtnf7f7faLatoRegular4f555dBorder1px8b96a5",
            "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmAlertsManagement.lblEditAlertPreview\")",
            "width": "80dp",
            "zIndex": 10
        }, controller.args[0], "btnPreview"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_CENTER,
            "displayText": true,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "btnPreview"), extendConfig({
            "hoverSkin": "sknbtnffffffLatoRegular4f555dBorder1px485c75"
        }, controller.args[2], "btnPreview"));
        flxHeader.add(flxChannelArrow, lblHeader, btnPreview);
        var flxContent = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
            "clipBounds": true,
            "id": "flxContent",
            "isVisible": true,
            "layoutType": kony.flex.FLOW_VERTICAL,
            "left": "20dp",
            "isModalContainer": false,
            "right": "20dp",
            "skin": "slFbox",
            "top": "40dp",
            "width": kony.flex.USE_PREFFERED_SIZE
        }, controller.args[0], "flxContent"), extendConfig({}, controller.args[1], "flxContent"), extendConfig({}, controller.args[2], "flxContent"));
        flxContent.setDefaultUnit(kony.flex.DP);
        var lblSubjectHeader = new kony.ui.Label(extendConfig({
            "id": "lblSubjectHeader",
            "isVisible": true,
            "left": "0",
            "skin": "sknLbl485c75LatoBold13px",
            "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmAlertsManagement.SubjectColon\")",
            "top": "20px",
            "width": "100%",
            "zIndex": 1
        }, controller.args[0], "lblSubjectHeader"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblSubjectHeader"), extendConfig({}, controller.args[2], "lblSubjectHeader"));
        var lblSubject = new kony.ui.Label(extendConfig({
            "bottom": "20dp",
            "id": "lblSubject",
            "isVisible": true,
            "left": "0",
            "skin": "sknLblLato485c7513px",
            "text": "Platinum Customers",
            "top": "15dp",
            "width": "100%",
            "zIndex": 1
        }, controller.args[0], "lblSubject"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblSubject"), extendConfig({}, controller.args[2], "lblSubject"));
        var lblDescHeader = new kony.ui.Label(extendConfig({
            "id": "lblDescHeader",
            "isVisible": true,
            "left": "0",
            "skin": "sknLbl485c75LatoBold13px",
            "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmAdManagement.emailDescriptionWithColon\")",
            "top": "0px",
            "width": "100%",
            "zIndex": 1
        }, controller.args[0], "lblDescHeader"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblDescHeader"), extendConfig({}, controller.args[2], "lblDescHeader"));
        var rtxDesc = new kony.ui.RichText(extendConfig({
            "bottom": "20dp",
            "id": "rtxDesc",
            "isVisible": true,
            "left": "0",
            "linkSkin": "defRichTextLink",
            "skin": "sknrtxLatoRegular485c7513px",
            "text": "This group includes users of all categories belonging to different age groups who have a minimum balance of $35,000.This group includes users of all categories belonging to different age groups who have a minimum balance of $35,000.This group includes users of all categories belonging to different age groups who have a minimum balance of $35,000 <br><br> # Attachment_image # group include ",
            "top": "15dp",
            "width": "100%",
            "zIndex": 1
        }, controller.args[0], "rtxDesc"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "rtxDesc"), extendConfig({}, controller.args[2], "rtxDesc"));
        flxContent.add(lblSubjectHeader, lblSubject, lblDescHeader, rtxDesc);
        flxOfflineSection.add(flxHeader, flxContent);
        viewOfflineChannelDetails.add(flxOfflineSection);
        return viewOfflineChannelDetails;
    }
})