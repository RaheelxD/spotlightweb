define("com/adminConsole/adManagement/viewOfflineChannelDetails/userviewOfflineChannelDetailsController", function() {
    return {
        showOrHideContent: function(parentObject) {
            let isHide = parentObject.lblChannelArrow.text === "\ue915";
            parentObject.lblChannelArrow.text = isHide ? "\ue922" : "\ue915";
            parentObject.flxContent.isVisible = !isHide;
            parentObject.forceLayout();
        }
    };
});
define("com/adminConsole/adManagement/viewOfflineChannelDetails/viewOfflineChannelDetailsControllerActions", {
    /*
        This is an auto generated file and any modifications to it may result in corruption of the action sequence.
    */
    AS_FlexContainer_ab82dc3781f14807b3a7012e7d4aef47: function AS_FlexContainer_ab82dc3781f14807b3a7012e7d4aef47(eventobject) {
        var self = this;
        return self.showOrHideContent.call(this, null);
    }
});
define("com/adminConsole/adManagement/viewOfflineChannelDetails/viewOfflineChannelDetailsController", ["com/adminConsole/adManagement/viewOfflineChannelDetails/userviewOfflineChannelDetailsController", "com/adminConsole/adManagement/viewOfflineChannelDetails/viewOfflineChannelDetailsControllerActions"], function() {
    var controller = require("com/adminConsole/adManagement/viewOfflineChannelDetails/userviewOfflineChannelDetailsController");
    var actions = require("com/adminConsole/adManagement/viewOfflineChannelDetails/viewOfflineChannelDetailsControllerActions");
    for (var key in actions) {
        controller[key] = actions[key];
    }
    return controller;
});
