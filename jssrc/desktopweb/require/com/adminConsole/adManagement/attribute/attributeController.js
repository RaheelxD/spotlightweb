define("com/adminConsole/adManagement/attribute/userattributeController", function() {
    return {};
});
define("com/adminConsole/adManagement/attribute/attributeControllerActions", {
    /*
          This is an auto generated file and any modifications to it may result in corruption of the action sequence.
        */
});
define("com/adminConsole/adManagement/attribute/attributeController", ["com/adminConsole/adManagement/attribute/userattributeController", "com/adminConsole/adManagement/attribute/attributeControllerActions"], function() {
    var controller = require("com/adminConsole/adManagement/attribute/userattributeController");
    var actions = require("com/adminConsole/adManagement/attribute/attributeControllerActions");
    for (var key in actions) {
        controller[key] = actions[key];
    }
    return controller;
});
