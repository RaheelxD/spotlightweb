define(function() {
    return function(controller) {
        var attribute = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
            "clipBounds": true,
            "isMaster": true,
            "id": "attribute",
            "isVisible": true,
            "layoutType": kony.flex.FLOW_HORIZONTAL,
            "left": "0dp",
            "isModalContainer": false,
            "skin": "sknflxBgF9F9F9",
            "top": "0dp",
            "width": "100%"
        }, controller.args[0], "attribute"), extendConfig({}, controller.args[1], "attribute"), extendConfig({}, controller.args[2], "attribute"));
        attribute.setDefaultUnit(kony.flex.DP);
        var fonticonCheckBox = new kony.ui.Label(extendConfig({
            "height": "25dp",
            "id": "fonticonCheckBox",
            "isVisible": true,
            "left": "20dp",
            "skin": "sknFontIconCheckBoxUnselected",
            "text": "",
            "top": "1dp",
            "width": "25dp",
            "zIndex": 2
        }, controller.args[0], "fonticonCheckBox"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "fonticonCheckBox"), extendConfig({}, controller.args[2], "fonticonCheckBox"));
        var lblAttributeName = new kony.ui.Label(extendConfig({
            "id": "lblAttributeName",
            "isVisible": true,
            "left": "5dp",
            "skin": "sknLbl485C75LatoRegular13Px",
            "text": "KL098234 - Westheimer",
            "top": "6dp",
            "width": "90%",
            "zIndex": 1
        }, controller.args[0], "lblAttributeName"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblAttributeName"), extendConfig({}, controller.args[2], "lblAttributeName"));
        attribute.add(fonticonCheckBox, lblAttributeName);
        return attribute;
    }
})