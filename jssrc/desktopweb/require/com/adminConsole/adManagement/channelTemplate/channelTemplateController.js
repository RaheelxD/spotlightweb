define("com/adminConsole/adManagement/channelTemplate/userchannelTemplateController", function() {
    return {};
});
define("com/adminConsole/adManagement/channelTemplate/channelTemplateControllerActions", {
    /*
      This is an auto generated file and any modifications to it may result in corruption of the action sequence.
    */
    /** preShow defined for channelTemplate **/
    AS_FlexContainer_f35b89a5eac048fba0df59afd1d1657f: function AS_FlexContainer_f35b89a5eac048fba0df59afd1d1657f(eventobject) {
        var self = this;
        return self.preShow.call(this);
    }
});
define("com/adminConsole/adManagement/channelTemplate/channelTemplateController", ["com/adminConsole/adManagement/channelTemplate/userchannelTemplateController", "com/adminConsole/adManagement/channelTemplate/channelTemplateControllerActions"], function() {
    var controller = require("com/adminConsole/adManagement/channelTemplate/userchannelTemplateController");
    var actions = require("com/adminConsole/adManagement/channelTemplate/channelTemplateControllerActions");
    for (var key in actions) {
        controller[key] = actions[key];
    }
    return controller;
});
