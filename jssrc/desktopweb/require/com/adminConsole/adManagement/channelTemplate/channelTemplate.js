define(function() {
    return function(controller) {
        var channelTemplate = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
            "bottom": "20dp",
            "clipBounds": true,
            "isMaster": true,
            "id": "channelTemplate",
            "isVisible": true,
            "layoutType": kony.flex.FLOW_VERTICAL,
            "left": "0dp",
            "isModalContainer": false,
            "skin": "sknFlxbgFFFFFFbdrD7D9E0rd4px",
            "top": "20dp",
            "width": "100%"
        }, controller.args[0], "channelTemplate"), extendConfig({}, controller.args[1], "channelTemplate"), extendConfig({}, controller.args[2], "channelTemplate"));
        channelTemplate.setDefaultUnit(kony.flex.DP);
        var flxChannelHeader = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "clipBounds": true,
            "height": "50dp",
            "id": "flxChannelHeader",
            "isVisible": true,
            "layoutType": kony.flex.FLOW_HORIZONTAL,
            "isModalContainer": false,
            "skin": "sknflxf5f6f8Op100",
            "top": "0dp",
            "width": "100%"
        }, controller.args[0], "flxChannelHeader"), extendConfig({}, controller.args[1], "flxChannelHeader"), extendConfig({}, controller.args[2], "flxChannelHeader"));
        flxChannelHeader.setDefaultUnit(kony.flex.DP);
        var flxChannelArrow = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "centerY": "50%",
            "clipBounds": true,
            "height": "15dp",
            "id": "flxChannelArrow",
            "isVisible": true,
            "layoutType": kony.flex.FLOW_HORIZONTAL,
            "left": "10dp",
            "isModalContainer": false,
            "skin": "sknCursor",
            "top": "13dp",
            "width": "15dp"
        }, controller.args[0], "flxChannelArrow"), extendConfig({}, controller.args[1], "flxChannelArrow"), extendConfig({}, controller.args[2], "flxChannelArrow"));
        flxChannelArrow.setDefaultUnit(kony.flex.DP);
        var lblChannelArrow = new kony.ui.Label(extendConfig({
            "height": "15dp",
            "id": "lblChannelArrow",
            "isVisible": true,
            "left": "0dp",
            "skin": "sknfontIconDescRightArrow14px",
            "text": "",
            "top": "0dp",
            "width": "15dp",
            "zIndex": 1
        }, controller.args[0], "lblChannelArrow"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblChannelArrow"), extendConfig({}, controller.args[2], "lblChannelArrow"));
        flxChannelArrow.add(lblChannelArrow);
        var lblChannelName = new kony.ui.Label(extendConfig({
            "centerY": "50%",
            "id": "lblChannelName",
            "isVisible": true,
            "left": "10dp",
            "skin": "sknLblLato485c7513px",
            "text": "Channel Name",
            "top": "0",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, controller.args[0], "lblChannelName"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblChannelName"), extendConfig({}, controller.args[2], "lblChannelName"));
        var lblModuleName = new kony.ui.Label(extendConfig({
            "centerY": "50%",
            "id": "lblModuleName",
            "isVisible": false,
            "left": "0dp",
            "skin": "sknLblLato485c7513px",
            "text": "Channel Name",
            "top": "0",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, controller.args[0], "lblModuleName"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblModuleName"), extendConfig({}, controller.args[2], "lblModuleName"));
        flxChannelHeader.add(flxChannelArrow, lblChannelName, lblModuleName);
        var flxChannelContainer = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
            "clipBounds": true,
            "id": "flxChannelContainer",
            "isVisible": true,
            "layoutType": kony.flex.FLOW_VERTICAL,
            "left": "0dp",
            "isModalContainer": false,
            "skin": "slFbox",
            "top": "0dp",
            "width": kony.flex.USE_PREFFERED_SIZE
        }, controller.args[0], "flxChannelContainer"), extendConfig({}, controller.args[1], "flxChannelContainer"), extendConfig({}, controller.args[2], "flxChannelContainer"));
        flxChannelContainer.setDefaultUnit(kony.flex.DP);
        var flxImageCounter = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
            "clipBounds": true,
            "id": "flxImageCounter",
            "isVisible": false,
            "layoutType": kony.flex.FLOW_VERTICAL,
            "left": "0dp",
            "isModalContainer": false,
            "skin": "slFbox",
            "top": "0dp",
            "width": kony.flex.USE_PREFFERED_SIZE
        }, controller.args[0], "flxImageCounter"), extendConfig({}, controller.args[1], "flxImageCounter"), extendConfig({}, controller.args[2], "flxImageCounter"));
        flxImageCounter.setDefaultUnit(kony.flex.DP);
        flxImageCounter.add();
        var flxDisplayAds = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
            "clipBounds": true,
            "id": "flxDisplayAds",
            "isVisible": true,
            "layoutType": kony.flex.FLOW_HORIZONTAL,
            "left": "20dp",
            "isModalContainer": false,
            "skin": "slFbox",
            "top": "20dp",
            "width": kony.flex.USE_PREFFERED_SIZE
        }, controller.args[0], "flxDisplayAds"), extendConfig({}, controller.args[1], "flxDisplayAds"), extendConfig({}, controller.args[2], "flxDisplayAds"));
        flxDisplayAds.setDefaultUnit(kony.flex.DP);
        var lblDisplayAds = new kony.ui.Label(extendConfig({
            "id": "lblDisplayAds",
            "isVisible": true,
            "left": "0dp",
            "skin": "sknLbl485C75LatoRegular13Px",
            "text": "Display Ads",
            "top": "3dp",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, controller.args[0], "lblDisplayAds"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblDisplayAds"), extendConfig({}, controller.args[2], "lblDisplayAds"));
        var switchAds = new kony.ui.Switch(extendConfig({
            "height": "20dp",
            "id": "switchAds",
            "isVisible": true,
            "left": "20dp",
            "selectedIndex": 0,
            "skin": "sknSwitchServiceManagement",
            "top": "0",
            "width": "35dp",
            "zIndex": 1
        }, controller.args[0], "switchAds"), extendConfig({
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "switchAds"), extendConfig({}, controller.args[2], "switchAds"));
        flxDisplayAds.add(lblDisplayAds, switchAds);
        var flxChannelDetails = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
            "clipBounds": true,
            "id": "flxChannelDetails",
            "isVisible": true,
            "layoutType": kony.flex.FLOW_VERTICAL,
            "left": "0dp",
            "isModalContainer": false,
            "skin": "slFbox",
            "top": "20dp",
            "width": kony.flex.USE_PREFFERED_SIZE
        }, controller.args[0], "flxChannelDetails"), extendConfig({}, controller.args[1], "flxChannelDetails"), extendConfig({}, controller.args[2], "flxChannelDetails"));
        flxChannelDetails.setDefaultUnit(kony.flex.DP);
        flxChannelDetails.add();
        var flxHideAdContainer = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
            "clipBounds": true,
            "id": "flxHideAdContainer",
            "isVisible": false,
            "layoutType": kony.flex.FREE_FORM,
            "left": "0dp",
            "isModalContainer": false,
            "skin": "slFbox",
            "top": "0dp",
            "width": "100%"
        }, controller.args[0], "flxHideAdContainer"), extendConfig({}, controller.args[1], "flxHideAdContainer"), extendConfig({}, controller.args[2], "flxHideAdContainer"));
        flxHideAdContainer.setDefaultUnit(kony.flex.DP);
        var flxHideAd = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "clipBounds": true,
            "height": "150dp",
            "id": "flxHideAd",
            "isVisible": true,
            "layoutType": kony.flex.FLOW_VERTICAL,
            "left": "20dp",
            "isModalContainer": false,
            "right": "20dp",
            "skin": "sknflxf8f9fabkgdddee0border3pxradius",
            "top": "20dp",
            "width": kony.flex.USE_PREFFERED_SIZE
        }, controller.args[0], "flxHideAd"), extendConfig({}, controller.args[1], "flxHideAd"), extendConfig({}, controller.args[2], "flxHideAd"));
        flxHideAd.setDefaultUnit(kony.flex.DP);
        var lblNoAd = new kony.ui.Label(extendConfig({
            "centerX": "50%",
            "height": "30dp",
            "id": "lblNoAd",
            "isVisible": true,
            "skin": "sknIconmoon9ea9b635px",
            "text": "",
            "top": "50dp",
            "width": "35dp",
            "zIndex": 1
        }, controller.args[0], "lblNoAd"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblNoAd"), extendConfig({}, controller.args[2], "lblNoAd"));
        var lblNoImageConfig = new kony.ui.Label(extendConfig({
            "centerX": "50%",
            "id": "lblNoImageConfig",
            "isVisible": true,
            "left": "0dp",
            "skin": "sknLblLato485c7513px",
            "text": "No image configuration to be shown",
            "top": "10dp",
            "width": "220dp",
            "zIndex": 1
        }, controller.args[0], "lblNoImageConfig"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblNoImageConfig"), extendConfig({}, controller.args[2], "lblNoImageConfig"));
        flxHideAd.add(lblNoAd, lblNoImageConfig);
        flxHideAdContainer.add(flxHideAd);
        flxChannelContainer.add(flxImageCounter, flxDisplayAds, flxChannelDetails, flxHideAdContainer);
        channelTemplate.add(flxChannelHeader, flxChannelContainer);
        return channelTemplate;
    }
})