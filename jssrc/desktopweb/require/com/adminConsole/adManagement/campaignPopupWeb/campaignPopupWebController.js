define("com/adminConsole/adManagement/campaignPopupWeb/usercampaignPopupWebController", function() {
    return {};
});
define("com/adminConsole/adManagement/campaignPopupWeb/campaignPopupWebControllerActions", {
    /*
      This is an auto generated file and any modifications to it may result in corruption of the action sequence.
    */
});
define("com/adminConsole/adManagement/campaignPopupWeb/campaignPopupWebController", ["com/adminConsole/adManagement/campaignPopupWeb/usercampaignPopupWebController", "com/adminConsole/adManagement/campaignPopupWeb/campaignPopupWebControllerActions"], function() {
    var controller = require("com/adminConsole/adManagement/campaignPopupWeb/usercampaignPopupWebController");
    var actions = require("com/adminConsole/adManagement/campaignPopupWeb/campaignPopupWebControllerActions");
    for (var key in actions) {
        controller[key] = actions[key];
    }
    return controller;
});
