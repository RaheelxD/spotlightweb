define(function() {
    return function(controller) {
        var campaignPopupWeb = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
            "clipBounds": true,
            "isMaster": true,
            "id": "campaignPopupWeb",
            "isVisible": true,
            "layoutType": kony.flex.FLOW_VERTICAL,
            "left": "0dp",
            "isModalContainer": false,
            "skin": "slFbox",
            "top": "5dp",
            "width": "560dp",
            "zIndex": 1001
        }, controller.args[0], "campaignPopupWeb"), extendConfig({}, controller.args[1], "campaignPopupWeb"), extendConfig({}, controller.args[2], "campaignPopupWeb"));
        campaignPopupWeb.setDefaultUnit(kony.flex.DP);
        var flxpopup = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
            "clipBounds": true,
            "id": "flxpopup",
            "isVisible": true,
            "layoutType": kony.flex.FLOW_VERTICAL,
            "left": "0dp",
            "isModalContainer": false,
            "skin": "slFbox",
            "top": "0dp",
            "width": "560dp"
        }, controller.args[0], "flxpopup"), extendConfig({}, controller.args[1], "flxpopup"), extendConfig({}, controller.args[2], "flxpopup"));
        flxpopup.setDefaultUnit(kony.flex.DP);
        var flxCampaignHeader = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
            "clipBounds": true,
            "id": "flxCampaignHeader",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "left": "0dp",
            "isModalContainer": false,
            "skin": "slFbox",
            "top": "20dp",
            "width": "100%",
            "zIndex": 1
        }, controller.args[0], "flxCampaignHeader"), extendConfig({}, controller.args[1], "flxCampaignHeader"), extendConfig({}, controller.args[2], "flxCampaignHeader"));
        flxCampaignHeader.setDefaultUnit(kony.flex.DP);
        var lblHeading = new kony.ui.Label(extendConfig({
            "id": "lblHeading",
            "isVisible": true,
            "left": "30dp",
            "right": "60dp",
            "skin": "sknLbl003e7540px",
            "text": "Take control of your financial future",
            "top": "0dp",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, controller.args[0], "lblHeading"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblHeading"), extendConfig({}, controller.args[2], "lblHeading"));
        var lblClose = new kony.ui.Label(extendConfig({
            "height": "20dp",
            "id": "lblClose",
            "isVisible": true,
            "right": "20dp",
            "skin": "sknLblIcon484b5218px",
            "text": "",
            "top": 0,
            "width": "20dp",
            "zIndex": 1
        }, controller.args[0], "lblClose"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblClose"), extendConfig({}, controller.args[2], "lblClose"));
        flxCampaignHeader.add(lblHeading, lblClose);
        var flxCampaignDescription = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
            "clipBounds": true,
            "id": "flxCampaignDescription",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "left": "0",
            "isModalContainer": false,
            "skin": "slFbox",
            "top": "0",
            "width": "100%"
        }, controller.args[0], "flxCampaignDescription"), extendConfig({}, controller.args[1], "flxCampaignDescription"), extendConfig({}, controller.args[2], "flxCampaignDescription"));
        flxCampaignDescription.setDefaultUnit(kony.flex.DP);
        var lblDescription = new kony.ui.Label(extendConfig({
            "id": "lblDescription",
            "isVisible": true,
            "left": "30dp",
            "right": "60dp",
            "skin": "sknLbl20px424242",
            "text": "Get 1.95% APY on your savings with the new Infinity savings account.",
            "top": "0dp",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, controller.args[0], "lblDescription"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblDescription"), extendConfig({}, controller.args[2], "lblDescription"));
        flxCampaignDescription.add(lblDescription);
        var flxCampaignImage = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
            "clipBounds": true,
            "id": "flxCampaignImage",
            "isVisible": true,
            "layoutType": kony.flex.FLOW_VERTICAL,
            "left": "0dp",
            "isModalContainer": false,
            "skin": "slFbox",
            "top": "20dp",
            "width": "100%"
        }, controller.args[0], "flxCampaignImage"), extendConfig({}, controller.args[1], "flxCampaignImage"), extendConfig({}, controller.args[2], "flxCampaignImage"));
        flxCampaignImage.setDefaultUnit(kony.flex.DP);
        var lblCampaignLink = new kony.ui.Label(extendConfig({
            "id": "lblCampaignLink",
            "isVisible": false,
            "left": 0,
            "skin": "sknLbl20px424242",
            "text": "campaign link to be given here",
            "top": 0,
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, controller.args[0], "lblCampaignLink"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblCampaignLink"), extendConfig({}, controller.args[2], "lblCampaignLink"));
        var imgPopup = new kony.ui.Image2(extendConfig({
            "height": "323dp",
            "id": "imgPopup",
            "isVisible": true,
            "left": "0dp",
            "skin": "slImage",
            "src": "imagedrag.png",
            "top": "0dp",
            "width": "100%",
            "zIndex": 1
        }, controller.args[0], "imgPopup"), extendConfig({
            "imageScaleMode": constants.IMAGE_SCALE_MODE_MAINTAIN_ASPECT_RATIO,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "imgPopup"), extendConfig({}, controller.args[2], "imgPopup"));
        flxCampaignImage.add(lblCampaignLink, imgPopup);
        var flxButtons = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
            "bottom": "20dp",
            "clipBounds": true,
            "id": "flxButtons",
            "isVisible": true,
            "layoutType": kony.flex.FLOW_HORIZONTAL,
            "reverseLayoutDirection": true,
            "left": "0dp",
            "isModalContainer": false,
            "skin": "slFbox",
            "top": "20dp",
            "width": "100%"
        }, controller.args[0], "flxButtons"), extendConfig({}, controller.args[1], "flxButtons"), extendConfig({}, controller.args[2], "flxButtons"));
        flxButtons.setDefaultUnit(kony.flex.DP);
        var btnYes = new kony.ui.Button(extendConfig({
            "focusSkin": "sknBtnNormalSSPFFFFFF15PxFocus",
            "height": "40dp",
            "id": "btnYes",
            "isVisible": true,
            "right": "20dp",
            "skin": "sknBtnNormalSSPFFFFFF15Px",
            "width": "30%",
            "zIndex": 1
        }, controller.args[0], "btnYes"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_CENTER,
            "displayText": true,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "btnYes"), extendConfig({
            "hoverSkin": "sknBtnNormalSSPFFFFFFHover15Px",
            "toolTip": "Yes"
        }, controller.args[2], "btnYes"));
        var btnNo = new kony.ui.Button(extendConfig({
            "height": "40dp",
            "id": "btnNo",
            "isVisible": true,
            "right": "20dp",
            "skin": "sknBtnffffffBorder0273e31pxRadius2px",
            "text": "Read Later",
            "width": "30%",
            "zIndex": 1
        }, controller.args[0], "btnNo"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_CENTER,
            "displayText": true,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "btnNo"), extendConfig({
            "hoverSkin": "sknBtnSecondaryFocusSSP3343a815PxHover",
            "toolTip": "No"
        }, controller.args[2], "btnNo"));
        flxButtons.add(btnYes, btnNo);
        flxpopup.add(flxCampaignHeader, flxCampaignDescription, flxCampaignImage, flxButtons);
        campaignPopupWeb.add(flxpopup);
        return campaignPopupWeb;
    }
})