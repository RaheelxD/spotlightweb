define(function() {
    return function(controller) {
        var dashboardCommonTab = new kony.ui.FlexContainer(extendConfig({
            "clipBounds": true,
            "isMaster": true,
            "height": "100%",
            "id": "dashboardCommonTab",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "left": "0dp",
            "isModalContainer": false,
            "preShow": function(eventobject) {
                controller.AS_FlexContainer_ab7d0f743ba94ff98d2abb57bb65cbaa(eventobject);
            },
            "skin": "slFbox",
            "top": "0dp",
            "width": "100%"
        }, controller.args[0], "dashboardCommonTab"), extendConfig({}, controller.args[1], "dashboardCommonTab"), extendConfig({}, controller.args[2], "dashboardCommonTab"));
        dashboardCommonTab.setDefaultUnit(kony.flex.DP);
        var flxProductsTabsWrapper = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "clipBounds": true,
            "height": "40px",
            "id": "flxProductsTabsWrapper",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "left": "0px",
            "isModalContainer": false,
            "right": "0px",
            "skin": "slFbox",
            "top": "0px",
            "zIndex": 2
        }, controller.args[0], "flxProductsTabsWrapper"), extendConfig({}, controller.args[1], "flxProductsTabsWrapper"), extendConfig({}, controller.args[2], "flxProductsTabsWrapper"));
        flxProductsTabsWrapper.setDefaultUnit(kony.flex.DP);
        var btnProfile = new kony.ui.Button(extendConfig({
            "height": "43px",
            "id": "btnProfile",
            "isVisible": true,
            "left": "0px",
            "skin": "sknbtnBgf5f6f8Lato485c75Radius3Px12Px",
            "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmLoansDashboard.ProfileStatic\")",
            "top": "0px",
            "width": "90px",
            "zIndex": 5
        }, controller.args[0], "btnProfile"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_CENTER,
            "displayText": true,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "btnProfile"), extendConfig({}, controller.args[2], "btnProfile"));
        var btnLoans = new kony.ui.Button(extendConfig({
            "height": "43px",
            "id": "btnLoans",
            "isVisible": false,
            "left": "95px",
            "skin": "sknBord7d9e0Rounded3px485c75",
            "text": "LOANS",
            "top": "0px",
            "width": "90px",
            "zIndex": 5
        }, controller.args[0], "btnLoans"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_CENTER,
            "displayText": true,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "btnLoans"), extendConfig({}, controller.args[2], "btnLoans"));
        var btnDeposits = new kony.ui.Button(extendConfig({
            "height": "43px",
            "id": "btnDeposits",
            "isVisible": true,
            "left": "95px",
            "skin": "sknBord7d9e0Rounded3px485c75",
            "text": "DEPOSITS",
            "top": "0px",
            "width": "90px",
            "zIndex": 5
        }, controller.args[0], "btnDeposits"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_CENTER,
            "displayText": true,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "btnDeposits"), extendConfig({}, controller.args[2], "btnDeposits"));
        var btnBanking = new kony.ui.Button(extendConfig({
            "height": "40px",
            "id": "btnBanking",
            "isVisible": false,
            "left": "190px",
            "skin": "sknGeneralInfoTabsUnselected",
            "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmLoansDashboard.BankingStatic\")",
            "top": "0px",
            "width": "90px",
            "zIndex": 5
        }, controller.args[0], "btnBanking"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_CENTER,
            "displayText": true,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "btnBanking"), extendConfig({}, controller.args[2], "btnBanking"));
        var lblBottomSeperatorCommonTabs = new kony.ui.Label(extendConfig({
            "height": "1px",
            "id": "lblBottomSeperatorCommonTabs",
            "isVisible": true,
            "left": "0px",
            "right": "0px",
            "skin": "sknlblSeperatorBgD5D9DD",
            "top": "39px",
            "zIndex": 2
        }, controller.args[0], "lblBottomSeperatorCommonTabs"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblBottomSeperatorCommonTabs"), extendConfig({}, controller.args[2], "lblBottomSeperatorCommonTabs"));
        flxProductsTabsWrapper.add(btnProfile, btnLoans, btnDeposits, btnBanking, lblBottomSeperatorCommonTabs);
        dashboardCommonTab.add(flxProductsTabsWrapper);
        return dashboardCommonTab;
    }
})