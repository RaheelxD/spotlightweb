define(function() {
    return function(controller) {
        var alerts = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
            "clipBounds": true,
            "isMaster": true,
            "id": "alerts",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "left": "0dp",
            "isModalContainer": false,
            "skin": "sknFlxInvisible",
            "top": "0dp",
            "width": "100%"
        }, controller.args[0], "alerts"), extendConfig({}, controller.args[1], "alerts"), extendConfig({}, controller.args[2], "alerts"));
        alerts.setDefaultUnit(kony.flex.DP);
        var flxAlertCategory = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
            "bottom": "20dp",
            "clipBounds": true,
            "id": "flxAlertCategory",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "left": "0px",
            "isModalContainer": false,
            "skin": "sknflxBgF9F9F9",
            "top": "20px",
            "width": "230px",
            "zIndex": 3
        }, controller.args[0], "flxAlertCategory"), extendConfig({}, controller.args[1], "flxAlertCategory"), extendConfig({}, controller.args[2], "flxAlertCategory"));
        flxAlertCategory.setDefaultUnit(kony.flex.DP);
        var flxAlertCategoryRow = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
            "clipBounds": true,
            "id": "flxAlertCategoryRow",
            "isVisible": false,
            "layoutType": kony.flex.FREE_FORM,
            "left": "0dp",
            "isModalContainer": false,
            "skin": "slFbox",
            "top": "0dp",
            "width": "100%"
        }, controller.args[0], "flxAlertCategoryRow"), extendConfig({}, controller.args[1], "flxAlertCategoryRow"), extendConfig({}, controller.args[2], "flxAlertCategoryRow"));
        flxAlertCategoryRow.setDefaultUnit(kony.flex.DP);
        var btnAlertCategory = new kony.ui.Button(extendConfig({
            "bottom": "15dp",
            "id": "btnAlertCategory",
            "isVisible": true,
            "left": "20dp",
            "skin": "sknBtnUtilRest73767812pxReg",
            "text": "SECURITY",
            "top": "20dp",
            "width": "60%",
            "zIndex": 1
        }, controller.args[0], "btnAlertCategory"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_TOP_LEFT,
            "displayText": true,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "btnAlertCategory"), extendConfig({}, controller.args[2], "btnAlertCategory"));
        var lblArrow = new kony.ui.Label(extendConfig({
            "bottom": "15px",
            "height": "15px",
            "id": "lblArrow",
            "isVisible": true,
            "right": "0px",
            "skin": "sknlblcursor",
            "text": "",
            "top": "20px",
            "width": "30px",
            "zIndex": 1
        }, controller.args[0], "lblArrow"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_TOP_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblArrow"), extendConfig({}, controller.args[2], "lblArrow"));
        var lblCategoryLine = new kony.ui.Label(extendConfig({
            "bottom": "0px",
            "height": "1px",
            "id": "lblCategoryLine",
            "isVisible": true,
            "left": "20dp",
            "right": "30dp",
            "skin": "sknLblSeparator",
            "text": "-",
            "zIndex": 1
        }, controller.args[0], "lblCategoryLine"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_TOP_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblCategoryLine"), extendConfig({}, controller.args[2], "lblCategoryLine"));
        flxAlertCategoryRow.add(btnAlertCategory, lblArrow, lblCategoryLine);
        var flxCategoryRowContainer = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
            "clipBounds": true,
            "id": "flxCategoryRowContainer",
            "isVisible": true,
            "layoutType": kony.flex.FLOW_VERTICAL,
            "left": "0dp",
            "isModalContainer": false,
            "skin": "slFbox",
            "top": "0dp",
            "width": "100%"
        }, controller.args[0], "flxCategoryRowContainer"), extendConfig({}, controller.args[1], "flxCategoryRowContainer"), extendConfig({}, controller.args[2], "flxCategoryRowContainer"));
        flxCategoryRowContainer.setDefaultUnit(kony.flex.DP);
        flxCategoryRowContainer.add();
        var flxSubCategory = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
            "clipBounds": true,
            "id": "flxSubCategory",
            "isVisible": false,
            "layoutType": kony.flex.FREE_FORM,
            "left": "0dp",
            "isModalContainer": false,
            "skin": "slFbox",
            "top": "0dp",
            "width": "100%",
            "zIndex": 5
        }, controller.args[0], "flxSubCategory"), extendConfig({}, controller.args[1], "flxSubCategory"), extendConfig({}, controller.args[2], "flxSubCategory"));
        flxSubCategory.setDefaultUnit(kony.flex.DP);
        var segAlertCategory = new kony.ui.SegmentedUI2(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
            "data": [
                [{
                        "btnAccountName": "Savings",
                        "btnAccountsCount": "08",
                        "lblArrow": "",
                        "lblDropArrow": ""
                    },
                    [{
                        "lblAlertCategory": "Label",
                        "lblArrow": ""
                    }, {
                        "lblAlertCategory": "Label",
                        "lblArrow": ""
                    }, {
                        "lblAlertCategory": "Label",
                        "lblArrow": ""
                    }, {
                        "lblAlertCategory": "Label",
                        "lblArrow": ""
                    }]
                ],
                [{
                        "btnAccountName": "Savings",
                        "btnAccountsCount": "08",
                        "lblArrow": "",
                        "lblDropArrow": ""
                    },
                    [{
                        "lblAlertCategory": "Label",
                        "lblArrow": ""
                    }, {
                        "lblAlertCategory": "Label",
                        "lblArrow": ""
                    }, {
                        "lblAlertCategory": "Label",
                        "lblArrow": ""
                    }, {
                        "lblAlertCategory": "Label",
                        "lblArrow": ""
                    }]
                ]
            ],
            "groupCells": false,
            "id": "segAlertCategory",
            "isVisible": true,
            "left": "0px",
            "needPageIndicator": true,
            "pageOffDotImage": "pageoffdot.png",
            "pageOnDotImage": "pageondot.png",
            "retainSelection": false,
            "right": "0px",
            "rowFocusSkin": "seg2Focus",
            "rowTemplate": "flxAlertCategory",
            "sectionHeaderSkin": "sliPhoneSegmentHeader",
            "sectionHeaderTemplate": "flxAccountAlertPreferTab",
            "selectionBehavior": constants.SEGUI_DEFAULT_BEHAVIOR,
            "separatorColor": "aaaaaa00",
            "separatorRequired": false,
            "separatorThickness": 1,
            "showScrollbars": false,
            "top": "0dp",
            "viewType": constants.SEGUI_VIEW_TYPE_TABLEVIEW,
            "widgetDataMap": {
                "btnAccountName": "btnAccountName",
                "btnAccountsCount": "btnAccountsCount",
                "flxAccountAlertPreferTab": "flxAccountAlertPreferTab",
                "flxAlertCategory": "flxAlertCategory",
                "lblAlertCategory": "lblAlertCategory",
                "lblArrow": "lblArrow",
                "lblDropArrow": "lblDropArrow"
            },
            "zIndex": 1
        }, controller.args[0], "segAlertCategory"), extendConfig({
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "segAlertCategory"), extendConfig({}, controller.args[2], "segAlertCategory"));
        flxSubCategory.add(segAlertCategory);
        flxAlertCategory.add(flxAlertCategoryRow, flxCategoryRowContainer, flxSubCategory);
        var flxLeftTabsBg = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "bottom": "0dp",
            "clipBounds": true,
            "id": "flxLeftTabsBg",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "left": "0dp",
            "isModalContainer": false,
            "skin": "sknflxBgF9F9F9",
            "top": "20dp",
            "width": "230px",
            "zIndex": 1
        }, controller.args[0], "flxLeftTabsBg"), extendConfig({}, controller.args[1], "flxLeftTabsBg"), extendConfig({}, controller.args[2], "flxLeftTabsBg"));
        flxLeftTabsBg.setDefaultUnit(kony.flex.DP);
        flxLeftTabsBg.add();
        var lblTitle = new kony.ui.Label(extendConfig({
            "bottom": "0dp",
            "id": "lblTitle",
            "isVisible": false,
            "left": "235px",
            "skin": "sknLbl16pxLatoRegular",
            "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmCustomerManagementController.Details_channel_subscriptions\")",
            "top": "35px",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, controller.args[0], "lblTitle"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblTitle"), extendConfig({}, controller.args[2], "lblTitle"));
        var flxStatus = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "clipBounds": true,
            "height": "40px",
            "id": "flxStatus",
            "isVisible": false,
            "layoutType": kony.flex.FLOW_HORIZONTAL,
            "isModalContainer": false,
            "right": "30px",
            "skin": "slFbox",
            "top": "35px",
            "width": "70px"
        }, controller.args[0], "flxStatus"), extendConfig({}, controller.args[1], "flxStatus"), extendConfig({}, controller.args[2], "flxStatus"));
        flxStatus.setDefaultUnit(kony.flex.DP);
        var lblStatusIcon = new kony.ui.Label(extendConfig({
            "height": "15px",
            "id": "lblStatusIcon",
            "isVisible": true,
            "left": "0px",
            "skin": "sknFontIconActivate",
            "text": "",
            "top": "0px",
            "width": "15px",
            "zIndex": 1
        }, controller.args[0], "lblStatusIcon"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblStatusIcon"), extendConfig({}, controller.args[2], "lblStatusIcon"));
        var lblStatus = new kony.ui.Label(extendConfig({
            "id": "lblStatus",
            "isVisible": true,
            "left": "0px",
            "skin": "sknLatoRegular485c7514px",
            "text": "Enabled",
            "top": "0px",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, controller.args[0], "lblStatus"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblStatus"), extendConfig({}, controller.args[2], "lblStatus"));
        flxStatus.add(lblStatusIcon, lblStatus);
        var flxSubAlerts = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
            "bottom": "20dp",
            "clipBounds": true,
            "id": "flxSubAlerts",
            "isVisible": false,
            "layoutType": kony.flex.FLOW_VERTICAL,
            "left": "250px",
            "isModalContainer": false,
            "right": "20px",
            "skin": "slFbox",
            "top": "70dp",
            "zIndex": 5
        }, controller.args[0], "flxSubAlerts"), extendConfig({}, controller.args[1], "flxSubAlerts"), extendConfig({}, controller.args[2], "flxSubAlerts"));
        flxSubAlerts.setDefaultUnit(kony.flex.DP);
        var listBoxAccounts = new kony.ui.ListBox(extendConfig({
            "height": "35dp",
            "id": "listBoxAccounts",
            "isVisible": false,
            "left": "0dp",
            "masterData": [
                ["lb1", "Placeholder One"],
                ["lb2", "Placeholder Two"],
                ["lb3", "Placeholder Three"]
            ],
            "skin": "sknlstbxBGa9a9a913px",
            "top": "0dp",
            "width": "300px",
            "zIndex": 1
        }, controller.args[0], "listBoxAccounts"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [1, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "listBoxAccounts"), extendConfig({
            "multiSelect": false
        }, controller.args[2], "listBoxAccounts"));
        var flxHeader = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "clipBounds": true,
            "height": "40px",
            "id": "flxHeader",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "left": "0px",
            "isModalContainer": false,
            "top": "0px",
            "width": "100%",
            "zIndex": 1
        }, controller.args[0], "flxHeader"), extendConfig({}, controller.args[1], "flxHeader"), extendConfig({}, controller.args[2], "flxHeader"));
        flxHeader.setDefaultUnit(kony.flex.DP);
        var flxHeaderDescription = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "clipBounds": true,
            "height": "100%",
            "id": "flxHeaderDescription",
            "isVisible": true,
            "layoutType": kony.flex.FLOW_HORIZONTAL,
            "left": "0px",
            "isModalContainer": false,
            "skin": "slFbox",
            "top": "0px",
            "width": "140px",
            "zIndex": 1
        }, controller.args[0], "flxHeaderDescription"), extendConfig({}, controller.args[1], "flxHeaderDescription"), extendConfig({}, controller.args[2], "flxHeaderDescription"));
        flxHeaderDescription.setDefaultUnit(kony.flex.DP);
        var lblHeaderDescription = new kony.ui.Label(extendConfig({
            "centerY": "50%",
            "id": "lblHeaderDescription",
            "isVisible": true,
            "left": "0px",
            "skin": "sknlblLato696c7312px",
            "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmCompanies.ACCOUNTNUMBER\")",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, controller.args[0], "lblHeaderDescription"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblHeaderDescription"), extendConfig({}, controller.args[2], "lblHeaderDescription"));
        var lblHeaderDescriptionIcon = new kony.ui.Label(extendConfig({
            "centerY": "50%",
            "id": "lblHeaderDescriptionIcon",
            "isVisible": false,
            "left": "5px",
            "right": "20px",
            "skin": "sknlblcursor",
            "text": "",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, controller.args[0], "lblHeaderDescriptionIcon"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblHeaderDescriptionIcon"), extendConfig({}, controller.args[2], "lblHeaderDescriptionIcon"));
        flxHeaderDescription.add(lblHeaderDescription, lblHeaderDescriptionIcon);
        var flxHeaderStatus = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "clipBounds": true,
            "height": "100%",
            "id": "flxHeaderStatus",
            "isVisible": true,
            "layoutType": kony.flex.FLOW_HORIZONTAL,
            "left": "40%",
            "isModalContainer": false,
            "skin": "slFbox",
            "top": "0px",
            "width": "100px",
            "zIndex": 1
        }, controller.args[0], "flxHeaderStatus"), extendConfig({}, controller.args[1], "flxHeaderStatus"), extendConfig({}, controller.args[2], "flxHeaderStatus"));
        flxHeaderStatus.setDefaultUnit(kony.flex.DP);
        var lblHeaderStatus = new kony.ui.Label(extendConfig({
            "centerY": "50%",
            "id": "lblHeaderStatus",
            "isVisible": true,
            "left": "0px",
            "skin": "sknlblLato696c7312px",
            "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmAlertsManagement.Alerts_Status_UC\")",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, controller.args[0], "lblHeaderStatus"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblHeaderStatus"), extendConfig({}, controller.args[2], "lblHeaderStatus"));
        var lblHeaderStatusIcon = new kony.ui.Label(extendConfig({
            "centerY": "50%",
            "id": "lblHeaderStatusIcon",
            "isVisible": false,
            "left": "5px",
            "right": "54px",
            "skin": "sknlblcursor",
            "text": "",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, controller.args[0], "lblHeaderStatusIcon"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblHeaderStatusIcon"), extendConfig({}, controller.args[2], "lblHeaderStatusIcon"));
        flxHeaderStatus.add(lblHeaderStatus, lblHeaderStatusIcon);
        var flxHeaderValue = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "clipBounds": true,
            "height": "100%",
            "id": "flxHeaderValue",
            "isVisible": false,
            "layoutType": kony.flex.FLOW_HORIZONTAL,
            "left": "370px",
            "isModalContainer": false,
            "skin": "slFbox",
            "top": "0px",
            "width": "80px",
            "zIndex": 1
        }, controller.args[0], "flxHeaderValue"), extendConfig({}, controller.args[1], "flxHeaderValue"), extendConfig({}, controller.args[2], "flxHeaderValue"));
        flxHeaderValue.setDefaultUnit(kony.flex.DP);
        var lblHeaderValue = new kony.ui.Label(extendConfig({
            "centerY": "50%",
            "id": "lblHeaderValue",
            "isVisible": true,
            "left": "0px",
            "skin": "sknlblLato696c7312px",
            "text": "VALUE",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, controller.args[0], "lblHeaderValue"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblHeaderValue"), extendConfig({}, controller.args[2], "lblHeaderValue"));
        var lblHeaderValueIcon = new kony.ui.Label(extendConfig({
            "centerY": "50%",
            "id": "lblHeaderValueIcon",
            "isVisible": true,
            "left": "5px",
            "right": "54px",
            "skin": "sknlblcursor",
            "text": "",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, controller.args[0], "lblHeaderValueIcon"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblHeaderValueIcon"), extendConfig({}, controller.args[2], "lblHeaderValueIcon"));
        flxHeaderValue.add(lblHeaderValue, lblHeaderValueIcon);
        var flxHeaderPush = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "clipBounds": true,
            "height": "100%",
            "id": "flxHeaderPush",
            "isVisible": false,
            "layoutType": kony.flex.FREE_FORM,
            "isModalContainer": false,
            "right": 205,
            "skin": "slFbox",
            "top": "0px",
            "width": 36,
            "zIndex": 1
        }, controller.args[0], "flxHeaderPush"), extendConfig({}, controller.args[1], "flxHeaderPush"), extendConfig({}, controller.args[2], "flxHeaderPush"));
        flxHeaderPush.setDefaultUnit(kony.flex.DP);
        var lblHeaderPush = new kony.ui.Label(extendConfig({
            "centerY": "50%",
            "id": "lblHeaderPush",
            "isVisible": true,
            "left": "0px",
            "skin": "sknlblLato696c7312px",
            "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmCustomerManagementController.PUSH\")",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, controller.args[0], "lblHeaderPush"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblHeaderPush"), extendConfig({}, controller.args[2], "lblHeaderPush"));
        flxHeaderPush.add(lblHeaderPush);
        var flxHeaderSMS = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "clipBounds": true,
            "height": "100%",
            "id": "flxHeaderSMS",
            "isVisible": false,
            "layoutType": kony.flex.FREE_FORM,
            "isModalContainer": false,
            "right": "124px",
            "skin": "slFbox",
            "top": "0px",
            "width": "28px",
            "zIndex": 1
        }, controller.args[0], "flxHeaderSMS"), extendConfig({}, controller.args[1], "flxHeaderSMS"), extendConfig({}, controller.args[2], "flxHeaderSMS"));
        flxHeaderSMS.setDefaultUnit(kony.flex.DP);
        var lblHeaderSMS = new kony.ui.Label(extendConfig({
            "centerY": "50%",
            "id": "lblHeaderSMS",
            "isVisible": true,
            "left": "0px",
            "skin": "sknlblLato696c7312px",
            "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmCustomerManagementController.SMS\")",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, controller.args[0], "lblHeaderSMS"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblHeaderSMS"), extendConfig({}, controller.args[2], "lblHeaderSMS"));
        flxHeaderSMS.add(lblHeaderSMS);
        var flxHeaderEmail = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "clipBounds": true,
            "height": "100%",
            "id": "flxHeaderEmail",
            "isVisible": false,
            "layoutType": kony.flex.FREE_FORM,
            "isModalContainer": false,
            "right": "30px",
            "skin": "slFbox",
            "top": "0px",
            "width": "41px",
            "zIndex": 1
        }, controller.args[0], "flxHeaderEmail"), extendConfig({}, controller.args[1], "flxHeaderEmail"), extendConfig({}, controller.args[2], "flxHeaderEmail"));
        flxHeaderEmail.setDefaultUnit(kony.flex.DP);
        var lblheaderEmail = new kony.ui.Label(extendConfig({
            "centerY": "50%",
            "id": "lblheaderEmail",
            "isVisible": true,
            "right": "0px",
            "skin": "sknlblLato696c7312px",
            "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmCustomerManagementController.EMAIL\")",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, controller.args[0], "lblheaderEmail"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblheaderEmail"), extendConfig({}, controller.args[2], "lblheaderEmail"));
        flxHeaderEmail.add(lblheaderEmail);
        var flxSeprator = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "bottom": 0,
            "clipBounds": true,
            "height": "1px",
            "id": "flxSeprator",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "left": "0dp",
            "isModalContainer": false,
            "skin": "sknFlxBg696C73Op100",
            "width": "100%",
            "zIndex": 1
        }, controller.args[0], "flxSeprator"), extendConfig({}, controller.args[1], "flxSeprator"), extendConfig({}, controller.args[2], "flxSeprator"));
        flxSeprator.setDefaultUnit(kony.flex.DP);
        flxSeprator.add();
        flxHeader.add(flxHeaderDescription, flxHeaderStatus, flxHeaderValue, flxHeaderPush, flxHeaderSMS, flxHeaderEmail, flxSeprator);
        var segSubAlerts = new kony.ui.SegmentedUI2(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
            "data": [{
                "lblAccountNumValue": "ACC010101084928323",
                "lblEnabledValue": "Enabled",
                "lblIconEnabled": "",
                "lblSeperator": "ACC010101084928323"
            }, {
                "lblAccountNumValue": "ACC010101084928323",
                "lblEnabledValue": "Enabled",
                "lblIconEnabled": "",
                "lblSeperator": "ACC010101084928323"
            }, {
                "lblAccountNumValue": "ACC010101084928323",
                "lblEnabledValue": "Enabled",
                "lblIconEnabled": "",
                "lblSeperator": "ACC010101084928323"
            }],
            "groupCells": false,
            "id": "segSubAlerts",
            "isVisible": true,
            "left": "0dp",
            "needPageIndicator": true,
            "pageOffDotImage": "pageoffdot.png",
            "pageOnDotImage": "pageondot.png",
            "retainSelection": false,
            "rowFocusSkin": "seg2Focus",
            "rowSkin": "seg2Normal",
            "rowTemplate": "flxAlertPreferAccountsList",
            "sectionHeaderSkin": "sliPhoneSegmentHeader",
            "selectionBehavior": constants.SEGUI_DEFAULT_BEHAVIOR,
            "separatorColor": "aaaaaa00",
            "separatorRequired": false,
            "separatorThickness": 1,
            "showScrollbars": false,
            "top": "0dp",
            "viewType": constants.SEGUI_VIEW_TYPE_TABLEVIEW,
            "widgetDataMap": {
                "flxAccountStatus": "flxAccountStatus",
                "flxAlertPreferAccountsList": "flxAlertPreferAccountsList",
                "lblAccountNumValue": "lblAccountNumValue",
                "lblEnabledValue": "lblEnabledValue",
                "lblIconEnabled": "lblIconEnabled",
                "lblSeperator": "lblSeperator"
            },
            "width": "100%",
            "zIndex": 1
        }, controller.args[0], "segSubAlerts"), extendConfig({
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "segSubAlerts"), extendConfig({}, controller.args[2], "segSubAlerts"));
        flxSubAlerts.add(listBoxAccounts, flxHeader, segSubAlerts);
        var flxAlertDetail = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "clipBounds": true,
            "height": "175dp",
            "id": "flxAlertDetail",
            "isVisible": true,
            "layoutType": kony.flex.FLOW_VERTICAL,
            "left": "250dp",
            "isModalContainer": false,
            "right": "20dp",
            "skin": "slFbox",
            "top": "20dp",
            "zIndex": 1
        }, controller.args[0], "flxAlertDetail"), extendConfig({}, controller.args[1], "flxAlertDetail"), extendConfig({}, controller.args[2], "flxAlertDetail"));
        flxAlertDetail.setDefaultUnit(kony.flex.DP);
        var flxAlertDetailHeader = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
            "clipBounds": true,
            "id": "flxAlertDetailHeader",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "left": "0px",
            "isModalContainer": false,
            "right": "0px",
            "top": "0px",
            "width": "100%",
            "zIndex": 1
        }, controller.args[0], "flxAlertDetailHeader"), extendConfig({}, controller.args[1], "flxAlertDetailHeader"), extendConfig({}, controller.args[2], "flxAlertDetailHeader"));
        flxAlertDetailHeader.setDefaultUnit(kony.flex.DP);
        var flxAlertDetailHeaderContainer = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
            "clipBounds": true,
            "id": "flxAlertDetailHeaderContainer",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "left": "0px",
            "isModalContainer": false,
            "right": "30px",
            "skin": "slFbox",
            "top": "0px",
            "width": "100%",
            "zIndex": 1
        }, controller.args[0], "flxAlertDetailHeaderContainer"), extendConfig({}, controller.args[1], "flxAlertDetailHeaderContainer"), extendConfig({}, controller.args[2], "flxAlertDetailHeaderContainer"));
        flxAlertDetailHeaderContainer.setDefaultUnit(kony.flex.DP);
        var lblAlertDetailDisplayName = new kony.ui.Label(extendConfig({
            "id": "lblAlertDetailDisplayName",
            "isVisible": true,
            "left": "0px",
            "skin": "sknLbl192b45LatoReg16px",
            "text": "Security Alert Preferences",
            "top": "20px",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, controller.args[0], "lblAlertDetailDisplayName"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblAlertDetailDisplayName"), extendConfig({}, controller.args[2], "lblAlertDetailDisplayName"));
        var lblAlertDetailAccountValue = new kony.ui.Label(extendConfig({
            "id": "lblAlertDetailAccountValue",
            "isVisible": false,
            "left": "0px",
            "skin": "sknLbl192B45LatoRegular14px",
            "text": "ACC0438279437455",
            "top": "46px",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, controller.args[0], "lblAlertDetailAccountValue"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblAlertDetailAccountValue"), extendConfig({}, controller.args[2], "lblAlertDetailAccountValue"));
        var flxEditAlertsBtn = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "clipBounds": true,
            "height": "22dp",
            "id": "flxEditAlertsBtn",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "isModalContainer": false,
            "right": "0dp",
            "skin": "sknServiceDetailsContent",
            "top": "20dp",
            "width": "50px",
            "zIndex": 1
        }, controller.args[0], "flxEditAlertsBtn"), extendConfig({}, controller.args[1], "flxEditAlertsBtn"), extendConfig({}, controller.args[2], "flxEditAlertsBtn"));
        flxEditAlertsBtn.setDefaultUnit(kony.flex.DP);
        var btnEditAlerts = new kony.ui.Button(extendConfig({
            "height": "22px",
            "id": "btnEditAlerts",
            "isVisible": true,
            "left": "0dp",
            "skin": "sknbtnf7f7faLatoRegular12Px485c75Border1px485C75Radius20px",
            "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmAlertsManagement.Edit\")",
            "top": "0dp",
            "width": "100%",
            "zIndex": 1
        }, controller.args[0], "btnEditAlerts"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_CENTER,
            "displayText": true,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "btnEditAlerts"), extendConfig({
            "hoverSkin": "sknbtnffffffLatoRegular12Px485c75Border1px485C75Radius20px"
        }, controller.args[2], "btnEditAlerts"));
        flxEditAlertsBtn.add(btnEditAlerts);
        var flxBackBtn = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "clipBounds": true,
            "height": "25dp",
            "id": "flxBackBtn",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "isModalContainer": false,
            "right": "0dp",
            "skin": "sknServiceDetailsContent",
            "top": "20dp",
            "width": "65dp",
            "zIndex": 1
        }, controller.args[0], "flxBackBtn"), extendConfig({}, controller.args[1], "flxBackBtn"), extendConfig({}, controller.args[2], "flxBackBtn"));
        flxBackBtn.setDefaultUnit(kony.flex.DP);
        var flxBack = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "clipBounds": true,
            "height": "20dp",
            "id": "flxBack",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "isModalContainer": false,
            "right": "40dp",
            "skin": "sknCursor",
            "top": "2dp",
            "width": "20dp",
            "zIndex": 1
        }, controller.args[0], "flxBack"), extendConfig({}, controller.args[1], "flxBack"), extendConfig({}, controller.args[2], "flxBack"));
        flxBack.setDefaultUnit(kony.flex.DP);
        var fontIconBack = new kony.ui.Label(extendConfig({
            "centerX": "50%",
            "centerY": "50%",
            "height": "14dp",
            "id": "fontIconBack",
            "isVisible": true,
            "left": "0dp",
            "skin": "sknicon15pxBlackBold",
            "text": "",
            "top": "0dp",
            "width": "16dp",
            "zIndex": 1
        }, controller.args[0], "fontIconBack"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "fontIconBack"), extendConfig({}, controller.args[2], "fontIconBack"));
        flxBack.add(fontIconBack);
        var btnBack = new kony.ui.Button(extendConfig({
            "id": "btnBack",
            "isVisible": true,
            "right": "0dp",
            "skin": "sknBtnLato117eb013Px",
            "i18n_text": "kony.i18n.getLocalizedString(\"i18n.userwidgetmodel.btnBack\")",
            "top": "4dp",
            "width": "50dp",
            "zIndex": 1
        }, controller.args[0], "btnBack"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_CENTER,
            "displayText": true,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "btnBack"), extendConfig({}, controller.args[2], "btnBack"));
        flxBackBtn.add(flxBack, btnBack);
        flxAlertDetailHeaderContainer.add(lblAlertDetailDisplayName, lblAlertDetailAccountValue, flxEditAlertsBtn, flxBackBtn);
        flxAlertDetailHeader.add(flxAlertDetailHeaderContainer);
        var flxAlertDetailBody = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "bottom": "20dp",
            "clipBounds": true,
            "height": "70dp",
            "id": "flxAlertDetailBody",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "left": "0px",
            "isModalContainer": false,
            "right": "0px",
            "skin": "slFbox",
            "top": "20px",
            "width": "100%",
            "zIndex": 1
        }, controller.args[0], "flxAlertDetailBody"), extendConfig({}, controller.args[1], "flxAlertDetailBody"), extendConfig({}, controller.args[2], "flxAlertDetailBody"));
        flxAlertDetailBody.setDefaultUnit(kony.flex.DP);
        var flxAlertDetailContainer = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
            "clipBounds": true,
            "id": "flxAlertDetailContainer",
            "isVisible": false,
            "layoutType": kony.flex.FLOW_VERTICAL,
            "left": "0px",
            "isModalContainer": false,
            "right": "30px",
            "skin": "slFbox",
            "top": "0px",
            "zIndex": 1
        }, controller.args[0], "flxAlertDetailContainer"), extendConfig({}, controller.args[1], "flxAlertDetailContainer"), extendConfig({}, controller.args[2], "flxAlertDetailContainer"));
        flxAlertDetailContainer.setDefaultUnit(kony.flex.DP);
        var flxAlertDetailRow0 = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
            "centerX": "50%",
            "clipBounds": true,
            "id": "flxAlertDetailRow0",
            "isVisible": true,
            "layoutType": kony.flex.FLOW_HORIZONTAL,
            "left": "0px",
            "isModalContainer": false,
            "right": "0px",
            "skin": "slFbox",
            "top": "0px",
            "width": "100%",
            "zIndex": 1
        }, controller.args[0], "flxAlertDetailRow0"), extendConfig({}, controller.args[1], "flxAlertDetailRow0"), extendConfig({}, controller.args[2], "flxAlertDetailRow0"));
        flxAlertDetailRow0.setDefaultUnit(kony.flex.DP);
        var flxAlertDetailCol01 = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
            "clipBounds": true,
            "id": "flxAlertDetailCol01",
            "isVisible": true,
            "layoutType": kony.flex.FLOW_VERTICAL,
            "left": "0px",
            "isModalContainer": false,
            "skin": "slFbox",
            "top": "0px",
            "width": "175dp",
            "zIndex": 1
        }, controller.args[0], "flxAlertDetailCol01"), extendConfig({}, controller.args[1], "flxAlertDetailCol01"), extendConfig({}, controller.args[2], "flxAlertDetailCol01"));
        flxAlertDetailCol01.setDefaultUnit(kony.flex.DP);
        var lblAlertDetailNotiStatusHeading = new kony.ui.Label(extendConfig({
            "id": "lblAlertDetailNotiStatusHeading",
            "isVisible": true,
            "left": "0px",
            "skin": "sknlblLato696c7312px",
            "text": "ALERTS NOTIFICATIONS",
            "top": "0px",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, controller.args[0], "lblAlertDetailNotiStatusHeading"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblAlertDetailNotiStatusHeading"), extendConfig({}, controller.args[2], "lblAlertDetailNotiStatusHeading"));
        var flxAlertNotiStatus = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
            "clipBounds": true,
            "id": "flxAlertNotiStatus",
            "isVisible": true,
            "layoutType": kony.flex.FLOW_HORIZONTAL,
            "left": 0,
            "isModalContainer": false,
            "right": "30px",
            "skin": "slFbox",
            "top": "10px",
            "width": kony.flex.USE_PREFFERED_SIZE
        }, controller.args[0], "flxAlertNotiStatus"), extendConfig({}, controller.args[1], "flxAlertNotiStatus"), extendConfig({}, controller.args[2], "flxAlertNotiStatus"));
        flxAlertNotiStatus.setDefaultUnit(kony.flex.DP);
        var lblAlertNotiStatusIcon = new kony.ui.Label(extendConfig({
            "bottom": "30dp",
            "height": "15px",
            "id": "lblAlertNotiStatusIcon",
            "isVisible": true,
            "left": "0px",
            "skin": "sknFontIconActivate",
            "text": "",
            "top": "0px",
            "width": "15px",
            "zIndex": 1
        }, controller.args[0], "lblAlertNotiStatusIcon"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblAlertNotiStatusIcon"), extendConfig({}, controller.args[2], "lblAlertNotiStatusIcon"));
        var lblAlertNotiStatus = new kony.ui.Label(extendConfig({
            "bottom": "30dp",
            "id": "lblAlertNotiStatus",
            "isVisible": true,
            "left": "10px",
            "skin": "sknLatoRegular485c7514px",
            "text": "Enabled",
            "top": "0px",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, controller.args[0], "lblAlertNotiStatus"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblAlertNotiStatus"), extendConfig({}, controller.args[2], "lblAlertNotiStatus"));
        flxAlertNotiStatus.add(lblAlertNotiStatusIcon, lblAlertNotiStatus);
        flxAlertDetailCol01.add(lblAlertDetailNotiStatusHeading, flxAlertNotiStatus);
        var flxAlertDetailCol02 = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
            "clipBounds": true,
            "id": "flxAlertDetailCol02",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "left": "5dp",
            "isModalContainer": false,
            "skin": "slFbox",
            "top": "0px",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, controller.args[0], "flxAlertDetailCol02"), extendConfig({}, controller.args[1], "flxAlertDetailCol02"), extendConfig({}, controller.args[2], "flxAlertDetailCol02"));
        flxAlertDetailCol02.setDefaultUnit(kony.flex.DP);
        var lblAlertDetailChannelsHeading = new kony.ui.Label(extendConfig({
            "id": "lblAlertDetailChannelsHeading",
            "isVisible": true,
            "left": "0dp",
            "skin": "sknlblLato696c7312px",
            "text": "ALERTS CHANNELS ",
            "top": "0dp",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, controller.args[0], "lblAlertDetailChannelsHeading"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblAlertDetailChannelsHeading"), extendConfig({}, controller.args[2], "lblAlertDetailChannelsHeading"));
        var flxAlertDetailChannelsValue = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
            "clipBounds": true,
            "id": "flxAlertDetailChannelsValue",
            "isVisible": true,
            "layoutType": kony.flex.FLOW_HORIZONTAL,
            "left": 0,
            "isModalContainer": false,
            "right": "30px",
            "skin": "slFbox",
            "top": "25px",
            "width": kony.flex.USE_PREFFERED_SIZE
        }, controller.args[0], "flxAlertDetailChannelsValue"), extendConfig({}, controller.args[1], "flxAlertDetailChannelsValue"), extendConfig({}, controller.args[2], "flxAlertDetailChannelsValue"));
        flxAlertDetailChannelsValue.setDefaultUnit(kony.flex.DP);
        var lblAlertDetailChannels = new kony.ui.Label(extendConfig({
            "bottom": "30dp",
            "id": "lblAlertDetailChannels",
            "isVisible": true,
            "left": "0px",
            "skin": "sknLatoRegular485c7514px",
            "text": "Push ,  SMS , Email , Notification Center",
            "top": "0dp",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, controller.args[0], "lblAlertDetailChannels"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblAlertDetailChannels"), extendConfig({}, controller.args[2], "lblAlertDetailChannels"));
        flxAlertDetailChannelsValue.add(lblAlertDetailChannels);
        flxAlertDetailCol02.add(lblAlertDetailChannelsHeading, flxAlertDetailChannelsValue);
        flxAlertDetailRow0.add(flxAlertDetailCol01, flxAlertDetailCol02);
        flxAlertDetailContainer.add(flxAlertDetailRow0);
        var flxAlertEditContainer = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
            "clipBounds": true,
            "id": "flxAlertEditContainer",
            "isVisible": true,
            "layoutType": kony.flex.FLOW_VERTICAL,
            "left": "0px",
            "isModalContainer": false,
            "right": "0px",
            "skin": "slFbox",
            "top": "0px",
            "zIndex": 1
        }, controller.args[0], "flxAlertEditContainer"), extendConfig({}, controller.args[1], "flxAlertEditContainer"), extendConfig({}, controller.args[2], "flxAlertEditContainer"));
        flxAlertEditContainer.setDefaultUnit(kony.flex.DP);
        var flxAlertEditlRow0 = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
            "centerX": "50%",
            "clipBounds": true,
            "id": "flxAlertEditlRow0",
            "isVisible": true,
            "layoutType": kony.flex.FLOW_HORIZONTAL,
            "left": "0px",
            "isModalContainer": false,
            "right": "0px",
            "skin": "slFbox",
            "top": "0px",
            "width": "100%",
            "zIndex": 1
        }, controller.args[0], "flxAlertEditlRow0"), extendConfig({}, controller.args[1], "flxAlertEditlRow0"), extendConfig({}, controller.args[2], "flxAlertEditlRow0"));
        flxAlertEditlRow0.setDefaultUnit(kony.flex.DP);
        var flxAlertEditCol01 = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
            "clipBounds": true,
            "id": "flxAlertEditCol01",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "left": "0px",
            "isModalContainer": false,
            "skin": "slFbox",
            "top": "0px",
            "width": "175dp",
            "zIndex": 1
        }, controller.args[0], "flxAlertEditCol01"), extendConfig({}, controller.args[1], "flxAlertEditCol01"), extendConfig({}, controller.args[2], "flxAlertEditCol01"));
        flxAlertEditCol01.setDefaultUnit(kony.flex.DP);
        var lblAlertEditNotificationsHeading = new kony.ui.Label(extendConfig({
            "id": "lblAlertEditNotificationsHeading",
            "isVisible": true,
            "left": "0px",
            "skin": "sknlblLato696c7312px",
            "text": "ALERTS NOTIFICATIONS",
            "top": "0px",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, controller.args[0], "lblAlertEditNotificationsHeading"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblAlertEditNotificationsHeading"), extendConfig({}, controller.args[2], "lblAlertEditNotificationsHeading"));
        var flxAlertEditNotiStatus = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
            "clipBounds": true,
            "id": "flxAlertEditNotiStatus",
            "isVisible": true,
            "layoutType": kony.flex.FLOW_HORIZONTAL,
            "left": 0,
            "isModalContainer": false,
            "right": "30px",
            "skin": "slFbox",
            "top": "25px",
            "width": kony.flex.USE_PREFFERED_SIZE
        }, controller.args[0], "flxAlertEditNotiStatus"), extendConfig({}, controller.args[1], "flxAlertEditNotiStatus"), extendConfig({}, controller.args[2], "flxAlertEditNotiStatus"));
        flxAlertEditNotiStatus.setDefaultUnit(kony.flex.DP);
        var lblAlertEditNotiStatusIcon = new kony.ui.Label(extendConfig({
            "bottom": "30dp",
            "height": "15px",
            "id": "lblAlertEditNotiStatusIcon",
            "isVisible": true,
            "left": "0px",
            "skin": "sknFontIconActivate",
            "text": "",
            "top": "0px",
            "width": "15px",
            "zIndex": 1
        }, controller.args[0], "lblAlertEditNotiStatusIcon"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblAlertEditNotiStatusIcon"), extendConfig({}, controller.args[2], "lblAlertEditNotiStatusIcon"));
        var lblAlertEditNotiStatus = new kony.ui.Label(extendConfig({
            "id": "lblAlertEditNotiStatus",
            "isVisible": true,
            "left": "10px",
            "skin": "sknLatoRegular485c7514px",
            "text": "Enabled",
            "top": "0dp",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, controller.args[0], "lblAlertEditNotiStatus"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblAlertEditNotiStatus"), extendConfig({}, controller.args[2], "lblAlertEditNotiStatus"));
        var flxEditAlertCategoryIconStatus = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
            "clipBounds": true,
            "id": "flxEditAlertCategoryIconStatus",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "left": "10px",
            "isModalContainer": false,
            "skin": "slFbox",
            "top": "0dp",
            "width": "38px",
            "zIndex": 1
        }, controller.args[0], "flxEditAlertCategoryIconStatus"), extendConfig({}, controller.args[1], "flxEditAlertCategoryIconStatus"), extendConfig({}, controller.args[2], "flxEditAlertCategoryIconStatus"));
        flxEditAlertCategoryIconStatus.setDefaultUnit(kony.flex.DP);
        var editAlertCategoryStatusSwitch = new com.adminConsole.common.customSwitch(extendConfig({
            "height": "25dp",
            "id": "editAlertCategoryStatusSwitch",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "left": "0px",
            "masterType": constants.MASTER_TYPE_DEFAULT,
            "isModalContainer": false,
            "skin": "slFbox",
            "top": "0px",
            "width": "100%",
            "overrides": {
                "customSwitch": {
                    "height": "25dp",
                    "isVisible": true,
                    "left": "0px",
                    "top": "0px",
                    "width": "100%"
                },
                "switchToggle": {
                    "height": "25px",
                    "isVisible": true
                }
            }
        }, controller.args[0], "editAlertCategoryStatusSwitch"), extendConfig({
            "overrides": {}
        }, controller.args[1], "editAlertCategoryStatusSwitch"), extendConfig({
            "overrides": {}
        }, controller.args[2], "editAlertCategoryStatusSwitch"));
        flxEditAlertCategoryIconStatus.add(editAlertCategoryStatusSwitch);
        flxAlertEditNotiStatus.add(lblAlertEditNotiStatusIcon, lblAlertEditNotiStatus, flxEditAlertCategoryIconStatus);
        flxAlertEditCol01.add(lblAlertEditNotificationsHeading, flxAlertEditNotiStatus);
        var flxAlertEditCol02 = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
            "clipBounds": true,
            "id": "flxAlertEditCol02",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "left": "5dp",
            "isModalContainer": false,
            "skin": "slFbox",
            "top": "0px",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, controller.args[0], "flxAlertEditCol02"), extendConfig({}, controller.args[1], "flxAlertEditCol02"), extendConfig({}, controller.args[2], "flxAlertEditCol02"));
        flxAlertEditCol02.setDefaultUnit(kony.flex.DP);
        var lblAlertEditChannelsHeading = new kony.ui.Label(extendConfig({
            "id": "lblAlertEditChannelsHeading",
            "isVisible": true,
            "left": "0dp",
            "skin": "sknlblLato696c7312px",
            "text": "ALERTS CHANNELS ",
            "top": "0dp",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, controller.args[0], "lblAlertEditChannelsHeading"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblAlertEditChannelsHeading"), extendConfig({}, controller.args[2], "lblAlertEditChannelsHeading"));
        var flxAlertEditCheckBoxChannels = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
            "clipBounds": true,
            "id": "flxAlertEditCheckBoxChannels",
            "isVisible": true,
            "layoutType": kony.flex.FLOW_HORIZONTAL,
            "left": "0dp",
            "isModalContainer": false,
            "right": "5px",
            "top": "25dp",
            "zIndex": 1
        }, controller.args[0], "flxAlertEditCheckBoxChannels"), extendConfig({}, controller.args[1], "flxAlertEditCheckBoxChannels"), extendConfig({}, controller.args[2], "flxAlertEditCheckBoxChannels"));
        flxAlertEditCheckBoxChannels.setDefaultUnit(kony.flex.DP);
        var flxChannel1 = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "clipBounds": true,
            "height": "15px",
            "id": "flxChannel1",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "left": "0dp",
            "isModalContainer": false,
            "top": "0dp",
            "width": "15px",
            "zIndex": 1
        }, controller.args[0], "flxChannel1"), extendConfig({}, controller.args[1], "flxChannel1"), extendConfig({}, controller.args[2], "flxChannel1"));
        flxChannel1.setDefaultUnit(kony.flex.DP);
        var imgCheckBoxChannel1 = new kony.ui.Image2(extendConfig({
            "centerY": "50%",
            "height": "15dp",
            "id": "imgCheckBoxChannel1",
            "isVisible": true,
            "left": "0px",
            "skin": "slImage",
            "src": "checkboxselected.png",
            "top": "0px",
            "width": "15dp",
            "zIndex": 1
        }, controller.args[0], "imgCheckBoxChannel1"), extendConfig({
            "imageScaleMode": constants.IMAGE_SCALE_MODE_MAINTAIN_ASPECT_RATIO,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "imgCheckBoxChannel1"), extendConfig({}, controller.args[2], "imgCheckBoxChannel1"));
        flxChannel1.add(imgCheckBoxChannel1);
        var lblChannel1 = new kony.ui.Label(extendConfig({
            "bottom": "30dp",
            "id": "lblChannel1",
            "isVisible": true,
            "left": "5px",
            "skin": "sknlblLato696c7313px",
            "i18n_text": "kony.i18n.getLocalizedString(\"i18n.ProfileManagement.Phone\")",
            "top": "0dp",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, controller.args[0], "lblChannel1"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblChannel1"), extendConfig({}, controller.args[2], "lblChannel1"));
        var flxChannel2 = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "clipBounds": true,
            "height": "15px",
            "id": "flxChannel2",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "left": "20dp",
            "isModalContainer": false,
            "top": "0dp",
            "width": "15px",
            "zIndex": 1
        }, controller.args[0], "flxChannel2"), extendConfig({}, controller.args[1], "flxChannel2"), extendConfig({}, controller.args[2], "flxChannel2"));
        flxChannel2.setDefaultUnit(kony.flex.DP);
        var imgCheckBoxChannel2 = new kony.ui.Image2(extendConfig({
            "centerY": "50%",
            "height": "15dp",
            "id": "imgCheckBoxChannel2",
            "isVisible": true,
            "left": "0px",
            "skin": "slImage",
            "src": "checkboxselected.png",
            "top": "0px",
            "width": "15dp",
            "zIndex": 1
        }, controller.args[0], "imgCheckBoxChannel2"), extendConfig({
            "imageScaleMode": constants.IMAGE_SCALE_MODE_MAINTAIN_ASPECT_RATIO,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "imgCheckBoxChannel2"), extendConfig({}, controller.args[2], "imgCheckBoxChannel2"));
        flxChannel2.add(imgCheckBoxChannel2);
        var lblChannel2 = new kony.ui.Label(extendConfig({
            "bottom": "30dp",
            "id": "lblChannel2",
            "isVisible": true,
            "left": "5px",
            "skin": "sknlblLato696c7313px",
            "i18n_text": "kony.i18n.getLocalizedString(\"i18n.ProfileManagement.EmailId\")",
            "top": "0dp",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, controller.args[0], "lblChannel2"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblChannel2"), extendConfig({}, controller.args[2], "lblChannel2"));
        var flxChannel3 = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "clipBounds": true,
            "height": "15px",
            "id": "flxChannel3",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "left": "20dp",
            "isModalContainer": false,
            "top": "0dp",
            "width": "15px",
            "zIndex": 1
        }, controller.args[0], "flxChannel3"), extendConfig({}, controller.args[1], "flxChannel3"), extendConfig({}, controller.args[2], "flxChannel3"));
        flxChannel3.setDefaultUnit(kony.flex.DP);
        var imgCheckBoxChannel3 = new kony.ui.Image2(extendConfig({
            "centerY": "50%",
            "height": "15dp",
            "id": "imgCheckBoxChannel3",
            "isVisible": true,
            "left": "0px",
            "skin": "slImage",
            "src": "checkboxselected.png",
            "top": "0px",
            "width": "15dp",
            "zIndex": 1
        }, controller.args[0], "imgCheckBoxChannel3"), extendConfig({
            "imageScaleMode": constants.IMAGE_SCALE_MODE_MAINTAIN_ASPECT_RATIO,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "imgCheckBoxChannel3"), extendConfig({}, controller.args[2], "imgCheckBoxChannel3"));
        flxChannel3.add(imgCheckBoxChannel3);
        var lblChannel3 = new kony.ui.Label(extendConfig({
            "bottom": "30dp",
            "id": "lblChannel3",
            "isVisible": true,
            "left": "5px",
            "skin": "sknlblLato696c7313px",
            "i18n_text": "kony.i18n.getLocalizedString(\"i18n.Alerts.Notifications\")",
            "top": "0dp",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, controller.args[0], "lblChannel3"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblChannel3"), extendConfig({}, controller.args[2], "lblChannel3"));
        var flxChannel4 = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "clipBounds": true,
            "height": "15px",
            "id": "flxChannel4",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "left": "20dp",
            "isModalContainer": false,
            "top": "0dp",
            "width": "15px",
            "zIndex": 1
        }, controller.args[0], "flxChannel4"), extendConfig({}, controller.args[1], "flxChannel4"), extendConfig({}, controller.args[2], "flxChannel4"));
        flxChannel4.setDefaultUnit(kony.flex.DP);
        var imgCheckBoxChannel4 = new kony.ui.Image2(extendConfig({
            "centerY": "50%",
            "height": "15dp",
            "id": "imgCheckBoxChannel4",
            "isVisible": true,
            "left": "0px",
            "skin": "slImage",
            "src": "checkboxselected.png",
            "top": "0px",
            "width": "15dp",
            "zIndex": 1
        }, controller.args[0], "imgCheckBoxChannel4"), extendConfig({
            "imageScaleMode": constants.IMAGE_SCALE_MODE_MAINTAIN_ASPECT_RATIO,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "imgCheckBoxChannel4"), extendConfig({}, controller.args[2], "imgCheckBoxChannel4"));
        flxChannel4.add(imgCheckBoxChannel4);
        var lblChannel4 = new kony.ui.Label(extendConfig({
            "bottom": "30dp",
            "id": "lblChannel4",
            "isVisible": true,
            "left": "5px",
            "skin": "sknlblLato696c7313px",
            "i18n_text": "kony.i18n.getLocalizedString(\"i18n.Alerts.Notifications\")",
            "top": "0dp",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, controller.args[0], "lblChannel4"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblChannel4"), extendConfig({}, controller.args[2], "lblChannel4"));
        flxAlertEditCheckBoxChannels.add(flxChannel1, lblChannel1, flxChannel2, lblChannel2, flxChannel3, lblChannel3, flxChannel4, lblChannel4);
        var flxAlertChannelError = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
            "clipBounds": true,
            "id": "flxAlertChannelError",
            "isVisible": false,
            "layoutType": kony.flex.FLOW_HORIZONTAL,
            "left": "0dp",
            "isModalContainer": false,
            "skin": "slFbox",
            "top": "50dp",
            "width": "100%",
            "zIndex": 1
        }, controller.args[0], "flxAlertChannelError"), extendConfig({}, controller.args[1], "flxAlertChannelError"), extendConfig({}, controller.args[2], "flxAlertChannelError"));
        flxAlertChannelError.setDefaultUnit(kony.flex.DP);
        var lblIconErrorCategoryChannel = new kony.ui.Label(extendConfig({
            "id": "lblIconErrorCategoryChannel",
            "isVisible": true,
            "left": "0dp",
            "skin": "sknErrorIcon",
            "text": "",
            "top": "0dp",
            "width": "15dp",
            "zIndex": 1
        }, controller.args[0], "lblIconErrorCategoryChannel"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblIconErrorCategoryChannel"), extendConfig({}, controller.args[2], "lblIconErrorCategoryChannel"));
        var lblErrMsgCategoryChannel = new kony.ui.Label(extendConfig({
            "id": "lblErrMsgCategoryChannel",
            "isVisible": true,
            "left": "0dp",
            "skin": "sknlblError",
            "text": "Please select at least one channel",
            "top": "0dp",
            "width": "200dp",
            "zIndex": 1
        }, controller.args[0], "lblErrMsgCategoryChannel"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblErrMsgCategoryChannel"), extendConfig({}, controller.args[2], "lblErrMsgCategoryChannel"));
        flxAlertChannelError.add(lblIconErrorCategoryChannel, lblErrMsgCategoryChannel);
        flxAlertEditCol02.add(lblAlertEditChannelsHeading, flxAlertEditCheckBoxChannels, flxAlertChannelError);
        flxAlertEditlRow0.add(flxAlertEditCol01, flxAlertEditCol02);
        flxAlertEditContainer.add(flxAlertEditlRow0);
        flxAlertDetailBody.add(flxAlertDetailContainer, flxAlertEditContainer);
        flxAlertDetail.add(flxAlertDetailHeader, flxAlertDetailBody);
        var flxSeperator = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "clipBounds": true,
            "height": "1px",
            "id": "flxSeperator",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "left": "250px",
            "isModalContainer": false,
            "right": "20px",
            "skin": "slFbox",
            "top": "150px",
            "zIndex": 5
        }, controller.args[0], "flxSeperator"), extendConfig({}, controller.args[1], "flxSeperator"), extendConfig({}, controller.args[2], "flxSeperator"));
        flxSeperator.setDefaultUnit(kony.flex.DP);
        var lblSeperator = new kony.ui.Label(extendConfig({
            "height": "1px",
            "id": "lblSeperator",
            "isVisible": true,
            "left": "0px",
            "right": "0px",
            "skin": "sknLblTableHeaderLine",
            "text": ".",
            "top": "0px",
            "zIndex": 1
        }, controller.args[0], "lblSeperator"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblSeperator"), extendConfig({}, controller.args[2], "lblSeperator"));
        flxSeperator.add(lblSeperator);
        var flxAlerts2 = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
            "bottom": "80dp",
            "clipBounds": true,
            "id": "flxAlerts2",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "left": "250dp",
            "isModalContainer": false,
            "right": "20px",
            "skin": "slFbox",
            "top": "152dp",
            "zIndex": 1
        }, controller.args[0], "flxAlerts2"), extendConfig({}, controller.args[1], "flxAlerts2"), extendConfig({}, controller.args[2], "flxAlerts2"));
        flxAlerts2.setDefaultUnit(kony.flex.DP);
        var segAlerts2 = new kony.ui.SegmentedUI2(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
            "data": [{
                "lblAlertDescription": "Label",
                "lblEnabledIcon": "D",
                "lblHorSeparator": "",
                "lblSeperator0a1397462b3464a": "'",
                "lstNotify": {
                    "masterData": [
                        ["lb1", "Placeholder One"],
                        ["lb2", "Placeholder Two"],
                        ["lb3", "Placeholder Three"]
                    ],
                    "selectedKey": null,
                    "selectedKeys": null
                },
                "tbxAmount": "",
                "tbxMaxLimit": "",
                "tbxMinLimit": ""
            }, {
                "lblAlertDescription": "Label",
                "lblEnabledIcon": "D",
                "lblHorSeparator": "",
                "lblSeperator0a1397462b3464a": "'",
                "lstNotify": {
                    "masterData": [
                        ["lb1", "Placeholder One"],
                        ["lb2", "Placeholder Two"],
                        ["lb3", "Placeholder Three"]
                    ],
                    "selectedKey": null,
                    "selectedKeys": null
                },
                "tbxAmount": "",
                "tbxMaxLimit": "",
                "tbxMinLimit": ""
            }, {
                "lblAlertDescription": "Label",
                "lblEnabledIcon": "D",
                "lblHorSeparator": "",
                "lblSeperator0a1397462b3464a": "'",
                "lstNotify": {
                    "masterData": [
                        ["lb1", "Placeholder One"],
                        ["lb2", "Placeholder Two"],
                        ["lb3", "Placeholder Three"]
                    ],
                    "selectedKey": null,
                    "selectedKeys": null
                },
                "tbxAmount": "",
                "tbxMaxLimit": "",
                "tbxMinLimit": ""
            }],
            "groupCells": false,
            "id": "segAlerts2",
            "isVisible": true,
            "left": "0dp",
            "needPageIndicator": true,
            "pageOffDotImage": "pageoffdot.png",
            "pageOnDotImage": "pageondot.png",
            "retainSelection": false,
            "rowFocusSkin": "seg2Focus",
            "rowSkin": "seg2Normal",
            "rowTemplate": "flxAccountAlertsCategoryDetails",
            "sectionHeaderSkin": "sliPhoneSegmentHeader",
            "selectionBehavior": constants.SEGUI_DEFAULT_BEHAVIOR,
            "separatorColor": "aaaaaa00",
            "separatorRequired": false,
            "separatorThickness": 1,
            "showScrollbars": false,
            "top": "0dp",
            "viewType": constants.SEGUI_VIEW_TYPE_TABLEVIEW,
            "widgetDataMap": {
                "flxAccBalanceCreditedDebited": "flxAccBalanceCreditedDebited",
                "flxAccBalanceInBetween": "flxAccBalanceInBetween",
                "flxAccountAlertsCategoryDetails": "flxAccountAlertsCategoryDetails",
                "flxAlertsEnabledStatus": "flxAlertsEnabledStatus",
                "flxEnabledIcon": "flxEnabledIcon",
                "flxHorSeparator": "flxHorSeparator",
                "flxNotifyBalance": "flxNotifyBalance",
                "lblAlertDescription": "lblAlertDescription",
                "lblEnabledIcon": "lblEnabledIcon",
                "lblHorSeparator": "lblHorSeparator",
                "lblSeperator0a1397462b3464a": "lblSeperator0a1397462b3464a",
                "lstNotify": "lstNotify",
                "tbxAmount": "tbxAmount",
                "tbxMaxLimit": "tbxMaxLimit",
                "tbxMinLimit": "tbxMinLimit"
            },
            "width": "100%",
            "zIndex": 1
        }, controller.args[0], "segAlerts2"), extendConfig({
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "segAlerts2"), extendConfig({}, controller.args[2], "segAlerts2"));
        var flxClickBlocker = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
            "bottom": "0dp",
            "clipBounds": false,
            "id": "flxClickBlocker",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "left": "23px",
            "isModalContainer": false,
            "right": "20px",
            "top": "0dp",
            "zIndex": 3
        }, controller.args[0], "flxClickBlocker"), extendConfig({}, controller.args[1], "flxClickBlocker"), extendConfig({}, controller.args[2], "flxClickBlocker"));
        flxClickBlocker.setDefaultUnit(kony.flex.DP);
        flxClickBlocker.add();
        flxAlerts2.add(segAlerts2, flxClickBlocker);
        var flxButtonsSeperator = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "bottom": "81dp",
            "clipBounds": true,
            "height": "1dp",
            "id": "flxButtonsSeperator",
            "isVisible": false,
            "layoutType": kony.flex.FREE_FORM,
            "left": "230dp",
            "isModalContainer": false,
            "right": "0dp",
            "skin": "sknflxd6dbe7",
            "zIndex": 1
        }, controller.args[0], "flxButtonsSeperator"), extendConfig({}, controller.args[1], "flxButtonsSeperator"), extendConfig({}, controller.args[2], "flxButtonsSeperator"));
        flxButtonsSeperator.setDefaultUnit(kony.flex.DP);
        flxButtonsSeperator.add();
        var commonButtons = new com.adminConsole.common.commonButtons(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "bottom": "0dp",
            "height": "80px",
            "id": "commonButtons",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "left": "250dp",
            "masterType": constants.MASTER_TYPE_DEFAULT,
            "isModalContainer": false,
            "right": "20dp",
            "skin": "slFbox",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "overrides": {
                "btnCancel": {
                    "text": "RESET"
                },
                "btnNext": {
                    "isVisible": false
                },
                "commonButtons": {
                    "bottom": "0dp",
                    "left": "250dp",
                    "right": "20dp",
                    "top": "viz.val_cleared",
                    "width": kony.flex.USE_PREFFERED_SIZE
                }
            }
        }, controller.args[0], "commonButtons"), extendConfig({
            "overrides": {}
        }, controller.args[1], "commonButtons"), extendConfig({
            "overrides": {}
        }, controller.args[2], "commonButtons"));
        var flxOverLay = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
            "clipBounds": true,
            "id": "flxOverLay",
            "isVisible": false,
            "layoutType": kony.flex.FREE_FORM,
            "left": "230px",
            "isModalContainer": false,
            "right": "20px",
            "skin": "slFbox",
            "top": "76dp",
            "zIndex": 1
        }, controller.args[0], "flxOverLay"), extendConfig({}, controller.args[1], "flxOverLay"), extendConfig({}, controller.args[2], "flxOverLay"));
        flxOverLay.setDefaultUnit(kony.flex.DP);
        var lblOverLayMessage = new kony.ui.Label(extendConfig({
            "bottom": "100px",
            "centerX": "50%",
            "id": "lblOverLayMessage",
            "isVisible": true,
            "left": "224dp",
            "skin": "sknLbl485C75LatoRegular14px",
            "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmCustomerManagementController.Incidents_for_this_alert_can_not_be_shown_as_it_is_disabled\")",
            "top": "100px",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, controller.args[0], "lblOverLayMessage"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblOverLayMessage"), extendConfig({}, controller.args[2], "lblOverLayMessage"));
        flxOverLay.add(lblOverLayMessage);
        alerts.add(flxAlertCategory, flxLeftTabsBg, lblTitle, flxStatus, flxSubAlerts, flxAlertDetail, flxSeperator, flxAlerts2, flxButtonsSeperator, commonButtons, flxOverLay);
        return alerts;
    }
})