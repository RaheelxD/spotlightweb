define("com/adminConsole/Permissions/viewConfigureCSRAssist/userviewConfigureCSRAssistController", function() {
    return {
        viewConfigureCSRPreshow: function() {
            this.setFlowActions();
        },
        setFlowActions: function() {
            var scopeObj = this;
            this.view.segViewConfigureCSR.onRowClick = function() {
                scopeObj.toggleDescription();
            };
        },
        toggleDescription: function() {
            var self = this;
            var index = self.view.segViewConfigureCSR.selectedRowIndex;
            var rowItem = self.view.segViewConfigureCSR.selectedRowItems;
            var segData = self.view.segViewConfigureCSR.data;
            var rowData = rowItem[0];
            //for remaining rows
            for (var i = 0; i < segData.length; i++) {
                if (i !== index[1] && segData[i].flxViewConfigureDesc.isVisible) {
                    segData[i].flxViewConfigureDesc = {
                        "isVisible": false
                    };
                    segData[i].lblIconArrow = {
                        "skin": "sknfontIconDescRightArrow14px",
                        "text": "\ue922"
                    }; //right-arrow
                    self.view.segViewConfigureCSR.setDataAt(segData[i], i);
                    break;
                }
            }
            //for selected row
            if (rowData.flxViewConfigureDesc.isVisible) {
                rowData.flxViewConfigureDesc = {
                    "isVisible": false
                };
                rowData.lblIconArrow = {
                    "skin": "sknfontIconDescRightArrow14px",
                    "text": "\ue922"
                }; //right-arrow
            } else {
                rowData.flxViewConfigureDesc = {
                    "isVisible": true
                };
                rowData.lblIconArrow = {
                    "skin": "sknfontIconDescDownArrow12px",
                    "text": "\ue915"
                }; //down-arrow
            }
            self.view.segViewConfigureCSR.setDataAt(rowData, index[1]);
            self.view.forceLayout();
        }
    };
});
define("com/adminConsole/Permissions/viewConfigureCSRAssist/viewConfigureCSRAssistControllerActions", {
    /*
        This is an auto generated file and any modifications to it may result in corruption of the action sequence.
    */
    AS_FlexContainer_fd4b6994bf604ee78bff55ba90fcb858: function AS_FlexContainer_fd4b6994bf604ee78bff55ba90fcb858(eventobject) {
        var self = this;
        this.viewConfigureCSRPreshow();
    }
});
define("com/adminConsole/Permissions/viewConfigureCSRAssist/viewConfigureCSRAssistController", ["com/adminConsole/Permissions/viewConfigureCSRAssist/userviewConfigureCSRAssistController", "com/adminConsole/Permissions/viewConfigureCSRAssist/viewConfigureCSRAssistControllerActions"], function() {
    var controller = require("com/adminConsole/Permissions/viewConfigureCSRAssist/userviewConfigureCSRAssistController");
    var actions = require("com/adminConsole/Permissions/viewConfigureCSRAssist/viewConfigureCSRAssistControllerActions");
    for (var key in actions) {
        controller[key] = actions[key];
    }
    return controller;
});
