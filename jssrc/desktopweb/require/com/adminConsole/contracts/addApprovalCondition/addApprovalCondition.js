define(function() {
    return function(controller) {
        var addApprovalCondition = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
            "clipBounds": true,
            "isMaster": true,
            "id": "addApprovalCondition",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "left": "0dp",
            "isModalContainer": false,
            "skin": "sknFlxFFFFFF100O",
            "top": "0dp",
            "width": "100%",
            "zIndex": 1
        }, controller.args[0], "addApprovalCondition"), extendConfig({}, controller.args[1], "addApprovalCondition"), extendConfig({}, controller.args[2], "addApprovalCondition"));
        addApprovalCondition.setDefaultUnit(kony.flex.DP);
        var btnConditionCount = new kony.ui.Button(extendConfig({
            "height": 15,
            "id": "btnConditionCount",
            "isVisible": true,
            "left": "40dp",
            "skin": "sknBtnBgFFFFFFReg485C75sz13noBor",
            "text": "Condition 1",
            "top": "12dp",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 5
        }, controller.args[0], "btnConditionCount"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "displayText": true,
            "padding": [1, 0, 1, 0],
            "paddingInPixel": false
        }, controller.args[1], "btnConditionCount"), extendConfig({
            "hoverSkin": "sknBtnBgFFFFFFReg485C75sz13noBor"
        }, controller.args[2], "btnConditionCount"));
        var flxConditionRemove = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
            "clipBounds": true,
            "id": "flxConditionRemove",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "isModalContainer": false,
            "right": "13dp",
            "skin": "slFbox",
            "top": "15dp",
            "width": "20dp",
            "zIndex": 5
        }, controller.args[0], "flxConditionRemove"), extendConfig({}, controller.args[1], "flxConditionRemove"), extendConfig({}, controller.args[2], "flxConditionRemove"));
        flxConditionRemove.setDefaultUnit(kony.flex.DP);
        var lblIconRemove = new kony.ui.Label(extendConfig({
            "id": "lblIconRemove",
            "isVisible": true,
            "left": "0",
            "skin": "sknIcon9F9E9ESz20px",
            "text": "",
            "top": "0",
            "width": kony.flex.USE_PREFFERED_SIZE
        }, controller.args[0], "lblIconRemove"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblIconRemove"), extendConfig({}, controller.args[2], "lblIconRemove"));
        flxConditionRemove.add(lblIconRemove);
        var flxAddConditionCardBorder = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
            "clipBounds": true,
            "id": "flxAddConditionCardBorder",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "left": "20dp",
            "isModalContainer": false,
            "right": "20dp",
            "skin": "sknFlxBgFFFFFFborD7D9E0rad19",
            "top": "20dp"
        }, controller.args[0], "flxAddConditionCardBorder"), extendConfig({}, controller.args[1], "flxAddConditionCardBorder"), extendConfig({}, controller.args[2], "flxAddConditionCardBorder"));
        flxAddConditionCardBorder.setDefaultUnit(kony.flex.DP);
        var flxAddConditionsCont = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
            "clipBounds": true,
            "id": "flxAddConditionsCont",
            "isVisible": true,
            "layoutType": kony.flex.FLOW_VERTICAL,
            "left": "20dp",
            "isModalContainer": false,
            "right": "20dp",
            "skin": "slFbox",
            "top": "30dp"
        }, controller.args[0], "flxAddConditionsCont"), extendConfig({}, controller.args[1], "flxAddConditionsCont"), extendConfig({}, controller.args[2], "flxAddConditionsCont"));
        flxAddConditionsCont.setDefaultUnit(kony.flex.DP);
        var flxConditionsBox = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
            "clipBounds": true,
            "id": "flxConditionsBox",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "left": "0dp",
            "isModalContainer": false,
            "skin": "sknFlxBgF8F9FABrE1E5EER3px",
            "top": "0dp",
            "width": "100%"
        }, controller.args[0], "flxConditionsBox"), extendConfig({}, controller.args[1], "flxConditionsBox"), extendConfig({}, controller.args[2], "flxConditionsBox"));
        flxConditionsBox.setDefaultUnit(kony.flex.DP);
        var lblCondHeading1 = new kony.ui.Label(extendConfig({
            "id": "lblCondHeading1",
            "isVisible": true,
            "left": "20dp",
            "skin": "sknlblLato696c7312px",
            "text": "NUMBER OF APPROVALS",
            "top": "15dp",
            "width": kony.flex.USE_PREFFERED_SIZE
        }, controller.args[0], "lblCondHeading1"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblCondHeading1"), extendConfig({}, controller.args[2], "lblCondHeading1"));
        var lblCondHeading2 = new kony.ui.Label(extendConfig({
            "id": "lblCondHeading2",
            "isVisible": true,
            "left": "20%",
            "skin": "sknlblLato696c7312px",
            "text": "SIGNATORY GROUP",
            "top": "15dp",
            "width": kony.flex.USE_PREFFERED_SIZE
        }, controller.args[0], "lblCondHeading2"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblCondHeading2"), extendConfig({}, controller.args[2], "lblCondHeading2"));
        var flxApprovalDynamicRows = new kony.ui.FlexContainer(extendConfig({
            "clipBounds": true,
            "height": "220dp",
            "id": "flxApprovalDynamicRows",
            "isVisible": true,
            "layoutType": kony.flex.FLOW_VERTICAL,
            "left": "0",
            "isModalContainer": false,
            "skin": "slFbox",
            "top": "50dp",
            "width": "100%"
        }, controller.args[0], "flxApprovalDynamicRows"), extendConfig({}, controller.args[1], "flxApprovalDynamicRows"), extendConfig({}, controller.args[2], "flxApprovalDynamicRows"));
        flxApprovalDynamicRows.setDefaultUnit(kony.flex.DP);
        flxApprovalDynamicRows.add();
        flxConditionsBox.add(lblCondHeading1, lblCondHeading2, flxApprovalDynamicRows);
        var flxAddNewCondition = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "bottom": "20dp",
            "clipBounds": true,
            "height": "25dp",
            "id": "flxAddNewCondition",
            "isVisible": true,
            "layoutType": kony.flex.FLOW_HORIZONTAL,
            "left": "0",
            "isModalContainer": false,
            "skin": "slFbox",
            "top": "15dp",
            "width": "50%"
        }, controller.args[0], "flxAddNewCondition"), extendConfig({}, controller.args[1], "flxAddNewCondition"), extendConfig({}, controller.args[2], "flxAddNewCondition"));
        flxAddNewCondition.setDefaultUnit(kony.flex.DP);
        var lblAddAndConditionHeading = new kony.ui.Label(extendConfig({
            "centerY": "50%",
            "id": "lblAddAndConditionHeading",
            "isVisible": true,
            "left": "0",
            "skin": "sknLbl485C75LatoRegular13Px",
            "text": "Click to add and condition",
            "width": kony.flex.USE_PREFFERED_SIZE
        }, controller.args[0], "lblAddAndConditionHeading"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_TOP_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblAddAndConditionHeading"), extendConfig({}, controller.args[2], "lblAddAndConditionHeading"));
        var btnAndCondition = new kony.ui.Button(extendConfig({
            "height": "22px",
            "id": "btnAndCondition",
            "isVisible": true,
            "left": "20dp",
            "skin": "sknbtnf7f7faLatoRegular12Px485c75Border1px485C75Radius20px",
            "text": "+ And",
            "top": "0dp",
            "width": "62dp",
            "zIndex": 1
        }, controller.args[0], "btnAndCondition"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_CENTER,
            "displayText": true,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "btnAndCondition"), extendConfig({
            "hoverSkin": "sknbtnffffffLatoRegular12Px485c75Border1px485C75Radius20px"
        }, controller.args[2], "btnAndCondition"));
        flxAddNewCondition.add(lblAddAndConditionHeading, btnAndCondition);
        flxAddConditionsCont.add(flxConditionsBox, flxAddNewCondition);
        flxAddConditionCardBorder.add(flxAddConditionsCont);
        addApprovalCondition.add(btnConditionCount, flxConditionRemove, flxAddConditionCardBorder);
        return addApprovalCondition;
    }
})