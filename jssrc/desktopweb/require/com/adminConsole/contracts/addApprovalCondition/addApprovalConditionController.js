define("com/adminConsole/contracts/addApprovalCondition/useraddApprovalConditionController", function() {
    return {};
});
define("com/adminConsole/contracts/addApprovalCondition/addApprovalConditionControllerActions", {
    /*
          This is an auto generated file and any modifications to it may result in corruption of the action sequence.
        */
});
define("com/adminConsole/contracts/addApprovalCondition/addApprovalConditionController", ["com/adminConsole/contracts/addApprovalCondition/useraddApprovalConditionController", "com/adminConsole/contracts/addApprovalCondition/addApprovalConditionControllerActions"], function() {
    var controller = require("com/adminConsole/contracts/addApprovalCondition/useraddApprovalConditionController");
    var actions = require("com/adminConsole/contracts/addApprovalCondition/addApprovalConditionControllerActions");
    for (var key in actions) {
        controller[key] = actions[key];
    }
    return controller;
});
