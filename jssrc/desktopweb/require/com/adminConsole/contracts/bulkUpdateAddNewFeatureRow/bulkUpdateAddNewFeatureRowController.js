define("com/adminConsole/contracts/bulkUpdateAddNewFeatureRow/userbulkUpdateAddNewFeatureRowController", function() {
    return {};
});
define("com/adminConsole/contracts/bulkUpdateAddNewFeatureRow/bulkUpdateAddNewFeatureRowControllerActions", {
    /*
          This is an auto generated file and any modifications to it may result in corruption of the action sequence.
        */
});
define("com/adminConsole/contracts/bulkUpdateAddNewFeatureRow/bulkUpdateAddNewFeatureRowController", ["com/adminConsole/contracts/bulkUpdateAddNewFeatureRow/userbulkUpdateAddNewFeatureRowController", "com/adminConsole/contracts/bulkUpdateAddNewFeatureRow/bulkUpdateAddNewFeatureRowControllerActions"], function() {
    var controller = require("com/adminConsole/contracts/bulkUpdateAddNewFeatureRow/userbulkUpdateAddNewFeatureRowController");
    var actions = require("com/adminConsole/contracts/bulkUpdateAddNewFeatureRow/bulkUpdateAddNewFeatureRowControllerActions");
    for (var key in actions) {
        controller[key] = actions[key];
    }
    return controller;
});
