define(function() {
    return function(controller) {
        var bulkUpdateAddNewFeatureRow = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
            "clipBounds": false,
            "isMaster": true,
            "id": "bulkUpdateAddNewFeatureRow",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "left": "0dp",
            "isModalContainer": false,
            "skin": "slFbox",
            "top": "0dp",
            "width": "100%"
        }, controller.args[0], "bulkUpdateAddNewFeatureRow"), extendConfig({}, controller.args[1], "bulkUpdateAddNewFeatureRow"), extendConfig({}, controller.args[2], "bulkUpdateAddNewFeatureRow"));
        bulkUpdateAddNewFeatureRow.setDefaultUnit(kony.flex.DP);
        var flxAddNewFeatureContainer = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
            "clipBounds": false,
            "id": "flxAddNewFeatureContainer",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "left": "0",
            "isModalContainer": false,
            "skin": "sknflxf5f6f8Op100BorderE1E5Ed",
            "top": "0",
            "width": "100%"
        }, controller.args[0], "flxAddNewFeatureContainer"), extendConfig({}, controller.args[1], "flxAddNewFeatureContainer"), extendConfig({}, controller.args[2], "flxAddNewFeatureContainer"));
        flxAddNewFeatureContainer.setDefaultUnit(kony.flex.DP);
        var flxRow1 = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "clipBounds": false,
            "height": "85dp",
            "id": "flxRow1",
            "isVisible": true,
            "layoutType": kony.flex.FLOW_HORIZONTAL,
            "left": "15dp",
            "isModalContainer": false,
            "right": "15dp",
            "skin": "slFbox",
            "top": "20dp",
            "zIndex": 2
        }, controller.args[0], "flxRow1"), extendConfig({}, controller.args[1], "flxRow1"), extendConfig({}, controller.args[2], "flxRow1"));
        flxRow1.setDefaultUnit(kony.flex.DP);
        var flxFieldColumn11 = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
            "clipBounds": true,
            "id": "flxFieldColumn11",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "left": "0dp",
            "isModalContainer": false,
            "skin": "slFbox",
            "top": "0dp",
            "width": "33.30%"
        }, controller.args[0], "flxFieldColumn11"), extendConfig({}, controller.args[1], "flxFieldColumn11"), extendConfig({}, controller.args[2], "flxFieldColumn11"));
        flxFieldColumn11.setDefaultUnit(kony.flex.DP);
        var lblFieldName11 = new kony.ui.Label(extendConfig({
            "id": "lblFieldName11",
            "isVisible": true,
            "left": 0,
            "skin": "sknLbl485C75LatoRegular13Px",
            "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmmfascenarios.Feature\")",
            "top": "0",
            "width": kony.flex.USE_PREFFERED_SIZE
        }, controller.args[0], "lblFieldName11"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblFieldName11"), extendConfig({}, controller.args[2], "lblFieldName11"));
        var lstBoxFieldValue11 = new kony.ui.ListBox(extendConfig({
            "focusSkin": "defListBoxFocus",
            "height": "40dp",
            "id": "lstBoxFieldValue11",
            "isVisible": true,
            "left": "0",
            "masterData": [
                ["lb1", "Placeholder One"],
                ["lb2", "Placeholder Two"],
                ["lb3", "Placeholder Three"]
            ],
            "right": "15dp",
            "skin": "sknLbxborderd7d9e03pxradius",
            "top": "27dp"
        }, controller.args[0], "lstBoxFieldValue11"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [3, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lstBoxFieldValue11"), extendConfig({
            "multiSelect": false
        }, controller.args[2], "lstBoxFieldValue11"));
        var flxErrorField11 = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
            "clipBounds": true,
            "id": "flxErrorField11",
            "isVisible": false,
            "layoutType": kony.flex.FREE_FORM,
            "left": "0dp",
            "isModalContainer": false,
            "right": "15dp",
            "skin": "slFbox",
            "top": "68dp",
            "zIndex": 1
        }, controller.args[0], "flxErrorField11"), extendConfig({}, controller.args[1], "flxErrorField11"), extendConfig({}, controller.args[2], "flxErrorField11"));
        flxErrorField11.setDefaultUnit(kony.flex.DP);
        var lblIconError11 = new kony.ui.Label(extendConfig({
            "height": "15dp",
            "id": "lblIconError11",
            "isVisible": true,
            "left": "0dp",
            "skin": "sknErrorIcon",
            "text": "",
            "top": "0dp",
            "width": "15dp",
            "zIndex": 1
        }, controller.args[0], "lblIconError11"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblIconError11"), extendConfig({}, controller.args[2], "lblIconError11"));
        var lblErrorMsg11 = new kony.ui.Label(extendConfig({
            "id": "lblErrorMsg11",
            "isVisible": true,
            "left": "17dp",
            "skin": "sknlblError",
            "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmServiceManagement.lblNoServiceTypeError\")",
            "top": "0dp",
            "width": "80%",
            "zIndex": 1
        }, controller.args[0], "lblErrorMsg11"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblErrorMsg11"), extendConfig({}, controller.args[2], "lblErrorMsg11"));
        flxErrorField11.add(lblIconError11, lblErrorMsg11);
        flxFieldColumn11.add(lblFieldName11, lstBoxFieldValue11, flxErrorField11);
        var flxFieldColumn12 = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
            "clipBounds": false,
            "id": "flxFieldColumn12",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "left": "0dp",
            "isModalContainer": false,
            "skin": "slFbox",
            "top": "0dp",
            "width": "33.30%"
        }, controller.args[0], "flxFieldColumn12"), extendConfig({}, controller.args[1], "flxFieldColumn12"), extendConfig({}, controller.args[2], "flxFieldColumn12"));
        flxFieldColumn12.setDefaultUnit(kony.flex.DP);
        var lblFieldName12 = new kony.ui.Label(extendConfig({
            "id": "lblFieldName12",
            "isVisible": true,
            "left": "0",
            "skin": "sknLbl485C75LatoRegular13Px",
            "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmEnrollCustomer.Action\")",
            "top": "0",
            "width": kony.flex.USE_PREFFERED_SIZE
        }, controller.args[0], "lblFieldName12"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblFieldName12"), extendConfig({}, controller.args[2], "lblFieldName12"));
        var flxFieldValueContainer12 = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "clipBounds": true,
            "height": "40dp",
            "id": "flxFieldValueContainer12",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "left": "0",
            "isModalContainer": false,
            "right": "15dp",
            "skin": "sknFlxbgFFFFFFbdrD7D9E0rd3px",
            "top": "27dp"
        }, controller.args[0], "flxFieldValueContainer12"), extendConfig({}, controller.args[1], "flxFieldValueContainer12"), extendConfig({}, controller.args[2], "flxFieldValueContainer12"));
        flxFieldValueContainer12.setDefaultUnit(kony.flex.DP);
        var lblFieldValue12 = new kony.ui.Label(extendConfig({
            "centerY": "50%",
            "id": "lblFieldValue12",
            "isVisible": true,
            "left": "10dp",
            "skin": "sknLbl485C75LatoRegular13Px",
            "text": "Label",
            "width": kony.flex.USE_PREFFERED_SIZE
        }, controller.args[0], "lblFieldValue12"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblFieldValue12"), extendConfig({}, controller.args[2], "lblFieldValue12"));
        var lblIconArrow12 = new kony.ui.Label(extendConfig({
            "centerY": "50%",
            "id": "lblIconArrow12",
            "isVisible": true,
            "right": "5dp",
            "skin": "sknIcon10px003E75",
            "text": "",
            "width": kony.flex.USE_PREFFERED_SIZE
        }, controller.args[0], "lblIconArrow12"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblIconArrow12"), extendConfig({}, controller.args[2], "lblIconArrow12"));
        flxFieldValueContainer12.add(lblFieldValue12, lblIconArrow12);
        var flxDropdownField12 = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
            "clipBounds": false,
            "id": "flxDropdownField12",
            "isVisible": false,
            "layoutType": kony.flex.FREE_FORM,
            "left": "0dp",
            "isModalContainer": false,
            "skin": "slFbox",
            "top": "70dp",
            "width": "300px",
            "zIndex": 10
        }, controller.args[0], "flxDropdownField12"), extendConfig({}, controller.args[1], "flxDropdownField12"), extendConfig({}, controller.args[2], "flxDropdownField12"));
        flxDropdownField12.setDefaultUnit(kony.flex.DP);
        var productTypeFilterMenu = new com.adminConsole.Products.productTypeFilterMenu(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
            "id": "productTypeFilterMenu",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "left": "0dp",
            "masterType": constants.MASTER_TYPE_DEFAULT,
            "isModalContainer": false,
            "skin": "slFbox",
            "top": "0dp",
            "width": "100%",
            "zIndex": 5,
            "overrides": {
                "flxArrowImage": {
                    "isVisible": false
                },
                "flxCheckbox": {
                    "top": "0px"
                },
                "flxFilterSort": {
                    "isVisible": false
                },
                "productTypeFilterMenu": {
                    "top": "0dp"
                }
            }
        }, controller.args[0], "productTypeFilterMenu"), extendConfig({
            "overrides": {}
        }, controller.args[1], "productTypeFilterMenu"), extendConfig({
            "overrides": {}
        }, controller.args[2], "productTypeFilterMenu"));
        flxDropdownField12.add(productTypeFilterMenu);
        var flxErrorField12 = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
            "clipBounds": true,
            "id": "flxErrorField12",
            "isVisible": false,
            "layoutType": kony.flex.FREE_FORM,
            "left": "0dp",
            "isModalContainer": false,
            "right": "15dp",
            "skin": "slFbox",
            "top": "68dp",
            "zIndex": 1
        }, controller.args[0], "flxErrorField12"), extendConfig({}, controller.args[1], "flxErrorField12"), extendConfig({}, controller.args[2], "flxErrorField12"));
        flxErrorField12.setDefaultUnit(kony.flex.DP);
        var lblIconError12 = new kony.ui.Label(extendConfig({
            "height": "15dp",
            "id": "lblIconError12",
            "isVisible": true,
            "left": "0dp",
            "skin": "sknErrorIcon",
            "text": "",
            "top": "0dp",
            "width": "15dp",
            "zIndex": 1
        }, controller.args[0], "lblIconError12"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblIconError12"), extendConfig({}, controller.args[2], "lblIconError12"));
        var lblErrorMsg12 = new kony.ui.Label(extendConfig({
            "id": "lblErrorMsg12",
            "isVisible": true,
            "left": "17dp",
            "skin": "sknlblError",
            "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmServiceManagement.lblNoServiceTypeError\")",
            "top": "0dp",
            "width": "80%",
            "zIndex": 1
        }, controller.args[0], "lblErrorMsg12"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblErrorMsg12"), extendConfig({}, controller.args[2], "lblErrorMsg12"));
        flxErrorField12.add(lblIconError12, lblErrorMsg12);
        flxFieldColumn12.add(lblFieldName12, flxFieldValueContainer12, flxDropdownField12, flxErrorField12);
        var flxFieldColumn13 = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
            "clipBounds": true,
            "id": "flxFieldColumn13",
            "isVisible": false,
            "layoutType": kony.flex.FREE_FORM,
            "left": "0dp",
            "isModalContainer": false,
            "skin": "slFbox",
            "top": "0dp",
            "width": "33.30%"
        }, controller.args[0], "flxFieldColumn13"), extendConfig({}, controller.args[1], "flxFieldColumn13"), extendConfig({}, controller.args[2], "flxFieldColumn13"));
        flxFieldColumn13.setDefaultUnit(kony.flex.DP);
        var lblFieldName13 = new kony.ui.Label(extendConfig({
            "id": "lblFieldName13",
            "isVisible": true,
            "left": "0",
            "skin": "sknLbl485C75LatoRegular13Px",
            "text": "Label",
            "top": "0",
            "width": kony.flex.USE_PREFFERED_SIZE
        }, controller.args[0], "lblFieldName13"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblFieldName13"), extendConfig({}, controller.args[2], "lblFieldName13"));
        var lstBoxFieldValue13 = new kony.ui.ListBox(extendConfig({
            "focusSkin": "defListBoxFocus",
            "height": "40dp",
            "id": "lstBoxFieldValue13",
            "isVisible": true,
            "left": "0",
            "masterData": [
                ["lb1", "Placeholder One"],
                ["lb2", "Placeholder Two"],
                ["lb3", "Placeholder Three"]
            ],
            "skin": "sknLbxborderd7d9e03pxradius",
            "top": "27dp",
            "width": "100%"
        }, controller.args[0], "lstBoxFieldValue13"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [3, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lstBoxFieldValue13"), extendConfig({
            "multiSelect": false
        }, controller.args[2], "lstBoxFieldValue13"));
        var flxErrorField13 = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
            "clipBounds": true,
            "id": "flxErrorField13",
            "isVisible": false,
            "layoutType": kony.flex.FREE_FORM,
            "left": "0dp",
            "isModalContainer": false,
            "skin": "slFbox",
            "top": "68dp",
            "width": "100%",
            "zIndex": 1
        }, controller.args[0], "flxErrorField13"), extendConfig({}, controller.args[1], "flxErrorField13"), extendConfig({}, controller.args[2], "flxErrorField13"));
        flxErrorField13.setDefaultUnit(kony.flex.DP);
        var lblIconError13 = new kony.ui.Label(extendConfig({
            "height": "15dp",
            "id": "lblIconError13",
            "isVisible": true,
            "left": "0dp",
            "skin": "sknErrorIcon",
            "text": "",
            "top": "0dp",
            "width": "15dp",
            "zIndex": 1
        }, controller.args[0], "lblIconError13"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblIconError13"), extendConfig({}, controller.args[2], "lblIconError13"));
        var lblErrorMsg13 = new kony.ui.Label(extendConfig({
            "id": "lblErrorMsg13",
            "isVisible": true,
            "left": "17dp",
            "skin": "sknlblError",
            "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmServiceManagement.lblNoServiceTypeError\")",
            "top": "0dp",
            "width": "80%",
            "zIndex": 1
        }, controller.args[0], "lblErrorMsg13"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblErrorMsg13"), extendConfig({}, controller.args[2], "lblErrorMsg13"));
        flxErrorField13.add(lblIconError13, lblErrorMsg13);
        flxFieldColumn13.add(lblFieldName13, lstBoxFieldValue13, flxErrorField13);
        var flxFieldColumn21 = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
            "clipBounds": true,
            "id": "flxFieldColumn21",
            "isVisible": false,
            "layoutType": kony.flex.FREE_FORM,
            "left": "0dp",
            "isModalContainer": false,
            "skin": "slFbox",
            "top": "0dp",
            "width": "33.30%"
        }, controller.args[0], "flxFieldColumn21"), extendConfig({}, controller.args[1], "flxFieldColumn21"), extendConfig({}, controller.args[2], "flxFieldColumn21"));
        flxFieldColumn21.setDefaultUnit(kony.flex.DP);
        var lblFieldName21 = new kony.ui.Label(extendConfig({
            "id": "lblFieldName21",
            "isVisible": true,
            "left": "0",
            "skin": "sknLbl485C75LatoRegular13Px",
            "i18n_text": "kony.i18n.getLocalizedString(\"i18n.products.Value\")",
            "top": "0",
            "width": kony.flex.USE_PREFFERED_SIZE
        }, controller.args[0], "lblFieldName21"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblFieldName21"), extendConfig({}, controller.args[2], "lblFieldName21"));
        var flxValue21 = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "clipBounds": true,
            "height": "40dp",
            "id": "flxValue21",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "left": "0dp",
            "isModalContainer": false,
            "right": "15dp",
            "skin": "sknFlxbgFFFFFFbdrD7D9E0rd3px",
            "top": "27dp"
        }, controller.args[0], "flxValue21"), extendConfig({}, controller.args[1], "flxValue21"), extendConfig({}, controller.args[2], "flxValue21"));
        flxValue21.setDefaultUnit(kony.flex.DP);
        var tbxValue21 = new kony.ui.TextBox2(extendConfig({
            "autoCapitalize": constants.TEXTBOX_AUTO_CAPITALIZE_NONE,
            "centerY": "50%",
            "height": "38dp",
            "id": "tbxValue21",
            "isVisible": true,
            "keyBoardStyle": constants.TEXTBOX_KEY_BOARD_STYLE_DEFAULT,
            "left": "30px",
            "right": "0dp",
            "secureTextEntry": false,
            "skin": "sknTbxBgFFFFFFf485c7513pxNoBorder",
            "textInputMode": constants.TEXTBOX_INPUT_MODE_NUMERIC,
            "zIndex": 1
        }, controller.args[0], "tbxValue21"), extendConfig({
            "containerHeightMode": constants.TEXTBOX_FONT_METRICS_DRIVEN_HEIGHT,
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [2, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "tbxValue21"), extendConfig({
            "autoCorrect": false,
            "placeholderSkin": "defTextBoxPlaceholder"
        }, controller.args[2], "tbxValue21"));
        var lblCurrencySymbol21 = new kony.ui.Label(extendConfig({
            "centerY": "50%",
            "id": "lblCurrencySymbol21",
            "isVisible": true,
            "left": "5dp",
            "skin": "sknIcon696C73Sz20px",
            "text": "",
            "width": "20dp",
            "zIndex": 1
        }, controller.args[0], "lblCurrencySymbol21"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_CENTER,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblCurrencySymbol21"), extendConfig({}, controller.args[2], "lblCurrencySymbol21"));
        flxValue21.add(tbxValue21, lblCurrencySymbol21);
        var flxErrorField21 = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
            "clipBounds": true,
            "id": "flxErrorField21",
            "isVisible": false,
            "layoutType": kony.flex.FREE_FORM,
            "left": "0dp",
            "isModalContainer": false,
            "right": "15dp",
            "skin": "slFbox",
            "top": "68dp",
            "zIndex": 1
        }, controller.args[0], "flxErrorField21"), extendConfig({}, controller.args[1], "flxErrorField21"), extendConfig({}, controller.args[2], "flxErrorField21"));
        flxErrorField21.setDefaultUnit(kony.flex.DP);
        var lblIconError21 = new kony.ui.Label(extendConfig({
            "height": "15dp",
            "id": "lblIconError21",
            "isVisible": true,
            "left": "0dp",
            "skin": "sknErrorIcon",
            "text": "",
            "top": "0dp",
            "width": "15dp",
            "zIndex": 1
        }, controller.args[0], "lblIconError21"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblIconError21"), extendConfig({}, controller.args[2], "lblIconError21"));
        var lblErrorMsg21 = new kony.ui.Label(extendConfig({
            "id": "lblErrorMsg21",
            "isVisible": true,
            "left": "17dp",
            "skin": "sknlblError",
            "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmServiceManagement.lblNoServiceTypeError\")",
            "top": "0dp",
            "width": "80%",
            "zIndex": 1
        }, controller.args[0], "lblErrorMsg21"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblErrorMsg21"), extendConfig({}, controller.args[2], "lblErrorMsg21"));
        flxErrorField21.add(lblIconError21, lblErrorMsg21);
        flxFieldColumn21.add(lblFieldName21, flxValue21, flxErrorField21);
        flxRow1.add(flxFieldColumn11, flxFieldColumn12, flxFieldColumn13, flxFieldColumn21);
        var flxRow2 = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "clipBounds": true,
            "height": "85dp",
            "id": "flxRow2",
            "isVisible": false,
            "layoutType": kony.flex.FLOW_HORIZONTAL,
            "left": "15dp",
            "isModalContainer": false,
            "right": "15dp",
            "skin": "slFbox",
            "top": "115dp"
        }, controller.args[0], "flxRow2"), extendConfig({}, controller.args[1], "flxRow2"), extendConfig({}, controller.args[2], "flxRow2"));
        flxRow2.setDefaultUnit(kony.flex.DP);
        var flxFieldColumn22 = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
            "clipBounds": true,
            "id": "flxFieldColumn22",
            "isVisible": false,
            "layoutType": kony.flex.FREE_FORM,
            "left": "0dp",
            "isModalContainer": false,
            "skin": "slFbox",
            "top": "0dp",
            "width": "33.30%"
        }, controller.args[0], "flxFieldColumn22"), extendConfig({}, controller.args[1], "flxFieldColumn22"), extendConfig({}, controller.args[2], "flxFieldColumn22"));
        flxFieldColumn22.setDefaultUnit(kony.flex.DP);
        var lblFieldName22 = new kony.ui.Label(extendConfig({
            "id": "lblFieldName22",
            "isVisible": true,
            "left": "0",
            "skin": "sknLbl485C75LatoRegular13Px",
            "text": "Label",
            "top": "0",
            "width": kony.flex.USE_PREFFERED_SIZE
        }, controller.args[0], "lblFieldName22"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblFieldName22"), extendConfig({}, controller.args[2], "lblFieldName22"));
        var flxValue22 = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "clipBounds": true,
            "height": "40dp",
            "id": "flxValue22",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "left": "0dp",
            "isModalContainer": false,
            "right": "15dp",
            "skin": "sknFlxbgFFFFFFbdrD7D9E0rd3px",
            "top": "27dp"
        }, controller.args[0], "flxValue22"), extendConfig({}, controller.args[1], "flxValue22"), extendConfig({}, controller.args[2], "flxValue22"));
        flxValue22.setDefaultUnit(kony.flex.DP);
        var tbxValue22 = new kony.ui.TextBox2(extendConfig({
            "autoCapitalize": constants.TEXTBOX_AUTO_CAPITALIZE_NONE,
            "centerY": "50%",
            "height": "38dp",
            "id": "tbxValue22",
            "isVisible": true,
            "keyBoardStyle": constants.TEXTBOX_KEY_BOARD_STYLE_DEFAULT,
            "left": "30px",
            "right": "0dp",
            "secureTextEntry": false,
            "skin": "sknTbxBgFFFFFFf485c7513pxNoBorder",
            "textInputMode": constants.TEXTBOX_INPUT_MODE_NUMERIC,
            "zIndex": 1
        }, controller.args[0], "tbxValue22"), extendConfig({
            "containerHeightMode": constants.TEXTBOX_FONT_METRICS_DRIVEN_HEIGHT,
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [2, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "tbxValue22"), extendConfig({
            "autoCorrect": false,
            "placeholderSkin": "defTextBoxPlaceholder"
        }, controller.args[2], "tbxValue22"));
        var lblCurrencySymbol22 = new kony.ui.Label(extendConfig({
            "centerY": "50%",
            "id": "lblCurrencySymbol22",
            "isVisible": true,
            "left": "5dp",
            "skin": "sknIcon696C73Sz20px",
            "text": "",
            "width": "20dp",
            "zIndex": 1
        }, controller.args[0], "lblCurrencySymbol22"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_CENTER,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblCurrencySymbol22"), extendConfig({}, controller.args[2], "lblCurrencySymbol22"));
        flxValue22.add(tbxValue22, lblCurrencySymbol22);
        var flxErrorField22 = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
            "clipBounds": true,
            "id": "flxErrorField22",
            "isVisible": false,
            "layoutType": kony.flex.FREE_FORM,
            "left": "0dp",
            "isModalContainer": false,
            "right": "15dp",
            "skin": "slFbox",
            "top": "68dp",
            "zIndex": 1
        }, controller.args[0], "flxErrorField22"), extendConfig({}, controller.args[1], "flxErrorField22"), extendConfig({}, controller.args[2], "flxErrorField22"));
        flxErrorField22.setDefaultUnit(kony.flex.DP);
        var lblIconError22 = new kony.ui.Label(extendConfig({
            "height": "15dp",
            "id": "lblIconError22",
            "isVisible": true,
            "left": "0dp",
            "skin": "sknErrorIcon",
            "text": "",
            "top": "0dp",
            "width": "15dp",
            "zIndex": 1
        }, controller.args[0], "lblIconError22"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblIconError22"), extendConfig({}, controller.args[2], "lblIconError22"));
        var lblErrorMsg = new kony.ui.Label(extendConfig({
            "id": "lblErrorMsg",
            "isVisible": true,
            "left": "17dp",
            "skin": "sknlblError",
            "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmServiceManagement.lblNoServiceTypeError\")",
            "top": "0dp",
            "width": "80%",
            "zIndex": 1
        }, controller.args[0], "lblErrorMsg"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblErrorMsg"), extendConfig({}, controller.args[2], "lblErrorMsg"));
        flxErrorField22.add(lblIconError22, lblErrorMsg);
        flxFieldColumn22.add(lblFieldName22, flxValue22, flxErrorField22);
        flxRow2.add(flxFieldColumn22);
        var flxDelete = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "clipBounds": true,
            "height": "20dp",
            "id": "flxDelete",
            "isVisible": false,
            "layoutType": kony.flex.FLOW_HORIZONTAL,
            "reverseLayoutDirection": true,
            "isModalContainer": false,
            "right": "20dp",
            "skin": "sknCursor",
            "top": "15dp",
            "width": "75dp",
            "zIndex": 3
        }, controller.args[0], "flxDelete"), extendConfig({}, controller.args[1], "flxDelete"), extendConfig({}, controller.args[2], "flxDelete"));
        flxDelete.setDefaultUnit(kony.flex.DP);
        var lblDelete = new kony.ui.Label(extendConfig({
            "id": "lblDelete",
            "isVisible": true,
            "left": "5dp",
            "skin": "sknLbl485C75LatoRegular13Px",
            "text": "Delete",
            "top": "0",
            "width": kony.flex.USE_PREFFERED_SIZE
        }, controller.args[0], "lblDelete"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblDelete"), extendConfig({}, controller.args[2], "lblDelete"));
        var lblIconDelete = new kony.ui.Label(extendConfig({
            "id": "lblIconDelete",
            "isVisible": true,
            "left": "0",
            "skin": "sknLblIcon485C7515px",
            "text": "",
            "top": "0",
            "width": kony.flex.USE_PREFFERED_SIZE
        }, controller.args[0], "lblIconDelete"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblIconDelete"), extendConfig({}, controller.args[2], "lblIconDelete"));
        flxDelete.add(lblDelete, lblIconDelete);
        flxAddNewFeatureContainer.add(flxRow1, flxRow2, flxDelete);
        bulkUpdateAddNewFeatureRow.add(flxAddNewFeatureContainer);
        return bulkUpdateAddNewFeatureRow;
    }
})