define(function() {
    return function(controller) {
        var contractDetailsPopup = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "clipBounds": true,
            "isMaster": true,
            "height": "100%",
            "id": "contractDetailsPopup",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "left": "0dp",
            "isModalContainer": false,
            "skin": "sknFlxBg222B35Op50PopupBg",
            "top": "0dp",
            "width": "100%"
        }, controller.args[0], "contractDetailsPopup"), extendConfig({}, controller.args[1], "contractDetailsPopup"), extendConfig({}, controller.args[2], "contractDetailsPopup"));
        contractDetailsPopup.setDefaultUnit(kony.flex.DP);
        var flxContractDetailsPopupContainer = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "centerX": "50%",
            "centerY": "50%",
            "clipBounds": true,
            "height": "370dp",
            "id": "flxContractDetailsPopupContainer",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "isModalContainer": false,
            "skin": "sknFlxFFFFFF100O",
            "width": "890dp"
        }, controller.args[0], "flxContractDetailsPopupContainer"), extendConfig({}, controller.args[1], "flxContractDetailsPopupContainer"), extendConfig({}, controller.args[2], "flxContractDetailsPopupContainer"));
        flxContractDetailsPopupContainer.setDefaultUnit(kony.flex.DP);
        var flxPopUpTopColor = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "clipBounds": true,
            "height": "10px",
            "id": "flxPopUpTopColor",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "left": "0dp",
            "isModalContainer": false,
            "skin": "sknFlxInfoToastBg357C9ENoRadius",
            "top": "0dp",
            "width": "100%",
            "zIndex": 1
        }, controller.args[0], "flxPopUpTopColor"), extendConfig({}, controller.args[1], "flxPopUpTopColor"), extendConfig({}, controller.args[2], "flxPopUpTopColor"));
        flxPopUpTopColor.setDefaultUnit(kony.flex.DP);
        flxPopUpTopColor.add();
        var flxPopUpClose = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "clipBounds": true,
            "height": "25px",
            "id": "flxPopUpClose",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "isModalContainer": false,
            "right": 15,
            "skin": "sknCursor",
            "top": "25dp",
            "width": "25px",
            "zIndex": 20
        }, controller.args[0], "flxPopUpClose"), extendConfig({}, controller.args[1], "flxPopUpClose"), extendConfig({}, controller.args[2], "flxPopUpClose"));
        flxPopUpClose.setDefaultUnit(kony.flex.DP);
        var lblPopUpClose = new kony.ui.Label(extendConfig({
            "centerY": "50%",
            "id": "lblPopUpClose",
            "isVisible": true,
            "left": "4dp",
            "skin": "sknIcon18pxGray",
            "i18n_text": "kony.i18n.getLocalizedString(\"i18n.userwidgetmodel.lblClearSearch\")",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, controller.args[0], "lblPopUpClose"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblPopUpClose"), extendConfig({}, controller.args[2], "lblPopUpClose"));
        var fontIconImgCLose = new kony.ui.Label(extendConfig({
            "centerX": "50%",
            "centerY": "50.00%",
            "height": "15dp",
            "id": "fontIconImgCLose",
            "isVisible": true,
            "skin": "sknFontIconSearchCross16px",
            "i18n_text": "kony.i18n.getLocalizedString(\"i18n.userwidgetmodel.lblClearSearch\")",
            "width": "15dp",
            "zIndex": 2
        }, controller.args[0], "fontIconImgCLose"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "fontIconImgCLose"), extendConfig({}, controller.args[2], "fontIconImgCLose"));
        flxPopUpClose.add(lblPopUpClose, fontIconImgCLose);
        var flxBackOption = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
            "clipBounds": true,
            "id": "flxBackOption",
            "isVisible": true,
            "layoutType": kony.flex.FLOW_HORIZONTAL,
            "left": "20dp",
            "isModalContainer": false,
            "skin": "slFbox",
            "top": "35px",
            "width": "65dp",
            "zIndex": 1
        }, controller.args[0], "flxBackOption"), extendConfig({}, controller.args[1], "flxBackOption"), extendConfig({
            "hoverSkin": "sknCursor"
        }, controller.args[2], "flxBackOption"));
        flxBackOption.setDefaultUnit(kony.flex.DP);
        var lblIconBack = new kony.ui.Label(extendConfig({
            "height": "20dp",
            "id": "lblIconBack",
            "isVisible": true,
            "left": "0px",
            "skin": "sknIcon117EB018px",
            "text": "",
            "top": "0px",
            "width": "20dp",
            "zIndex": 1
        }, controller.args[0], "lblIconBack"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblIconBack"), extendConfig({}, controller.args[2], "lblIconBack"));
        var lblBackText = new kony.ui.Label(extendConfig({
            "height": "20dp",
            "id": "lblBackText",
            "isVisible": true,
            "left": "7px",
            "skin": "sknLblLato13px117eb0",
            "text": "Back",
            "top": "0px",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, controller.args[0], "lblBackText"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblBackText"), extendConfig({}, controller.args[2], "lblBackText"));
        flxBackOption.add(lblIconBack, lblBackText);
        var flxDetailsContainer = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
            "clipBounds": true,
            "id": "flxDetailsContainer",
            "isVisible": true,
            "layoutType": kony.flex.FLOW_VERTICAL,
            "left": "20dp",
            "isModalContainer": false,
            "right": "20dp",
            "skin": "slFbox",
            "top": "70dp"
        }, controller.args[0], "flxDetailsContainer"), extendConfig({}, controller.args[1], "flxDetailsContainer"), extendConfig({}, controller.args[2], "flxDetailsContainer"));
        flxDetailsContainer.setDefaultUnit(kony.flex.DP);
        var lblDetailsHeading = new kony.ui.Label(extendConfig({
            "id": "lblDetailsHeading",
            "isVisible": true,
            "left": "0",
            "skin": "sknLbl192b45LatoReg16px",
            "text": "Customer Details",
            "top": "0",
            "width": kony.flex.USE_PREFFERED_SIZE
        }, controller.args[0], "lblDetailsHeading"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblDetailsHeading"), extendConfig({}, controller.args[2], "lblDetailsHeading"));
        var detailsRow1 = new com.adminConsole.view.details1(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
            "id": "detailsRow1",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "left": "0dp",
            "masterType": constants.MASTER_TYPE_DEFAULT,
            "isModalContainer": false,
            "skin": "slFbox",
            "top": "20dp",
            "width": "100%",
            "overrides": {
                "btnLink3": {
                    "centerY": "viz.val_cleared"
                },
                "details1": {
                    "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                    "top": "20dp"
                },
                "flxColumn1": {
                    "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                    "left": "0px",
                    "width": "33%"
                },
                "flxColumn2": {
                    "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                    "left": "35%",
                    "width": "33%"
                },
                "flxColumn3": {
                    "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                    "left": "70%",
                    "right": "0dp"
                },
                "flxDataAndImage1": {
                    "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                    "bottom": "viz.val_cleared",
                    "top": "23dp"
                },
                "flxDataAndImage2": {
                    "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                    "bottom": "viz.val_cleared",
                    "top": "23dp"
                },
                "flxDataAndImage3": {
                    "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                    "bottom": "viz.val_cleared",
                    "top": "23dp"
                },
                "lblData1": {
                    "centerY": "viz.val_cleared",
                    "width": "98%"
                },
                "lblData2": {
                    "centerY": "viz.val_cleared",
                    "width": "98%"
                },
                "lblData3": {
                    "centerY": "viz.val_cleared",
                    "right": "viz.val_cleared",
                    "width": "98%"
                },
                "lblHeading1": {
                    "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmEnrollCustomer.CustomerName_UC\")"
                },
                "lblHeading2": {
                    "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmCustomerManagementController.CUSTOMER_ID\")"
                },
                "lblHeading3": {
                    "text": "INDUSTRY"
                },
                "lblIconData1": {
                    "centerY": "viz.val_cleared"
                },
                "lblIconData2": {
                    "centerY": "viz.val_cleared",
                    "isVisible": false
                },
                "lblIconData3": {
                    "centerY": "viz.val_cleared",
                    "isVisible": false
                }
            }
        }, controller.args[0], "detailsRow1"), extendConfig({
            "overrides": {}
        }, controller.args[1], "detailsRow1"), extendConfig({
            "overrides": {}
        }, controller.args[2], "detailsRow1"));
        var detailsRow2 = new com.adminConsole.view.details1(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
            "id": "detailsRow2",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "left": "0dp",
            "masterType": constants.MASTER_TYPE_DEFAULT,
            "isModalContainer": false,
            "skin": "slFbox",
            "top": "30dp",
            "width": "100%",
            "overrides": {
                "btnLink3": {
                    "centerY": "viz.val_cleared"
                },
                "details1": {
                    "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                    "top": "30dp"
                },
                "flxColumn1": {
                    "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                    "left": "0px",
                    "width": "33%"
                },
                "flxColumn2": {
                    "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                    "left": "35%",
                    "width": "33%"
                },
                "flxColumn3": {
                    "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                    "left": "70%",
                    "right": "0dp"
                },
                "flxDataAndImage1": {
                    "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                    "bottom": "viz.val_cleared",
                    "top": "23dp"
                },
                "flxDataAndImage2": {
                    "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                    "bottom": "viz.val_cleared",
                    "top": "23dp"
                },
                "flxDataAndImage3": {
                    "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                    "bottom": "viz.val_cleared",
                    "top": "23dp"
                },
                "lblData1": {
                    "centerY": "viz.val_cleared",
                    "width": "98%"
                },
                "lblData2": {
                    "centerY": "viz.val_cleared",
                    "width": "98%"
                },
                "lblData3": {
                    "centerY": "viz.val_cleared",
                    "right": "viz.val_cleared",
                    "width": "98%"
                },
                "lblHeading1": {
                    "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmAlertsManagement.EMAILCaps\")"
                },
                "lblHeading2": {
                    "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmCustomerCare.lblPhoneNumber\")"
                },
                "lblIconData1": {
                    "centerY": "viz.val_cleared"
                },
                "lblIconData2": {
                    "centerY": "viz.val_cleared",
                    "isVisible": false
                },
                "lblIconData3": {
                    "centerY": "viz.val_cleared",
                    "isVisible": false
                }
            }
        }, controller.args[0], "detailsRow2"), extendConfig({
            "overrides": {}
        }, controller.args[1], "detailsRow2"), extendConfig({
            "overrides": {}
        }, controller.args[2], "detailsRow2"));
        var detailsRow3 = new com.adminConsole.view.details1(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
            "id": "detailsRow3",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "left": "0dp",
            "masterType": constants.MASTER_TYPE_DEFAULT,
            "isModalContainer": false,
            "skin": "slFbox",
            "top": "30dp",
            "width": "100%",
            "overrides": {
                "btnLink3": {
                    "centerY": "viz.val_cleared"
                },
                "details1": {
                    "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                    "top": "30dp"
                },
                "flxColumn1": {
                    "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                    "left": "0px",
                    "width": "33%"
                },
                "flxColumn2": {
                    "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                    "left": "35%",
                    "width": "33%"
                },
                "flxColumn3": {
                    "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                    "left": "70%",
                    "right": "0dp"
                },
                "flxDataAndImage1": {
                    "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                    "bottom": "viz.val_cleared",
                    "top": "23dp"
                },
                "flxDataAndImage2": {
                    "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                    "bottom": "viz.val_cleared",
                    "top": "23dp"
                },
                "flxDataAndImage3": {
                    "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                    "bottom": "viz.val_cleared",
                    "top": "23dp"
                },
                "lblData1": {
                    "centerY": "viz.val_cleared",
                    "width": "98%"
                },
                "lblData2": {
                    "centerY": "viz.val_cleared",
                    "width": "98%"
                },
                "lblData3": {
                    "centerY": "viz.val_cleared",
                    "right": "viz.val_cleared",
                    "width": "98%"
                },
                "lblHeading1": {
                    "i18n_text": "kony.i18n.getLocalizedString(\"i18n.View.ADDRESS\")"
                },
                "lblIconData1": {
                    "centerY": "viz.val_cleared"
                },
                "lblIconData2": {
                    "centerY": "viz.val_cleared",
                    "isVisible": false
                },
                "lblIconData3": {
                    "centerY": "viz.val_cleared",
                    "isVisible": false
                }
            }
        }, controller.args[0], "detailsRow3"), extendConfig({
            "overrides": {}
        }, controller.args[1], "detailsRow3"), extendConfig({
            "overrides": {}
        }, controller.args[2], "detailsRow3"));
        flxDetailsContainer.add(lblDetailsHeading, detailsRow1, detailsRow2, detailsRow3);
        flxContractDetailsPopupContainer.add(flxPopUpTopColor, flxPopUpClose, flxBackOption, flxDetailsContainer);
        contractDetailsPopup.add(flxContractDetailsPopupContainer);
        return contractDetailsPopup;
    }
})