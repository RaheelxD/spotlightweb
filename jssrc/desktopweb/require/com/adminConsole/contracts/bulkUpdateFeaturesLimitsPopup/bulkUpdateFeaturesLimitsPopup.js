define(function() {
    return function(controller) {
        var bulkUpdateFeaturesLimitsPopup = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "clipBounds": true,
            "isMaster": true,
            "height": "100%",
            "id": "bulkUpdateFeaturesLimitsPopup",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "left": "0dp",
            "isModalContainer": false,
            "skin": "sknFlxBg222B35Op50PopupBg",
            "top": "0dp",
            "width": "100%"
        }, controller.args[0], "bulkUpdateFeaturesLimitsPopup"), extendConfig({}, controller.args[1], "bulkUpdateFeaturesLimitsPopup"), extendConfig({}, controller.args[2], "bulkUpdateFeaturesLimitsPopup"));
        bulkUpdateFeaturesLimitsPopup.setDefaultUnit(kony.flex.DP);
        var flxContractDetailsPopupContainer = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "bottom": "95dp",
            "centerX": "50%",
            "clipBounds": true,
            "id": "flxContractDetailsPopupContainer",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "isModalContainer": false,
            "skin": "sknFlxFFFFFF100O",
            "top": "95dp",
            "width": "986dp"
        }, controller.args[0], "flxContractDetailsPopupContainer"), extendConfig({}, controller.args[1], "flxContractDetailsPopupContainer"), extendConfig({}, controller.args[2], "flxContractDetailsPopupContainer"));
        flxContractDetailsPopupContainer.setDefaultUnit(kony.flex.DP);
        var flxPopUpTopColor = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "clipBounds": true,
            "height": "10px",
            "id": "flxPopUpTopColor",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "left": "0dp",
            "isModalContainer": false,
            "skin": "sknFlxInfoToastBg357C9ENoRadius",
            "top": "0dp",
            "width": "100%",
            "zIndex": 1
        }, controller.args[0], "flxPopUpTopColor"), extendConfig({}, controller.args[1], "flxPopUpTopColor"), extendConfig({}, controller.args[2], "flxPopUpTopColor"));
        flxPopUpTopColor.setDefaultUnit(kony.flex.DP);
        flxPopUpTopColor.add();
        var flxPopUpClose = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "clipBounds": true,
            "height": "25px",
            "id": "flxPopUpClose",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "isModalContainer": false,
            "right": 15,
            "skin": "sknCursor",
            "top": "25dp",
            "width": "25px",
            "zIndex": 20
        }, controller.args[0], "flxPopUpClose"), extendConfig({}, controller.args[1], "flxPopUpClose"), extendConfig({}, controller.args[2], "flxPopUpClose"));
        flxPopUpClose.setDefaultUnit(kony.flex.DP);
        var lblPopUpClose = new kony.ui.Label(extendConfig({
            "centerY": "50%",
            "id": "lblPopUpClose",
            "isVisible": true,
            "left": "4dp",
            "skin": "sknIcon18pxGray",
            "i18n_text": "kony.i18n.getLocalizedString(\"i18n.userwidgetmodel.lblClearSearch\")",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, controller.args[0], "lblPopUpClose"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblPopUpClose"), extendConfig({}, controller.args[2], "lblPopUpClose"));
        var fontIconImgCLose = new kony.ui.Label(extendConfig({
            "centerX": "50%",
            "centerY": "50.00%",
            "height": "15dp",
            "id": "fontIconImgCLose",
            "isVisible": true,
            "skin": "sknFontIconSearchCross16px",
            "i18n_text": "kony.i18n.getLocalizedString(\"i18n.userwidgetmodel.lblClearSearch\")",
            "width": "15dp",
            "zIndex": 2
        }, controller.args[0], "fontIconImgCLose"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "fontIconImgCLose"), extendConfig({}, controller.args[2], "fontIconImgCLose"));
        flxPopUpClose.add(lblPopUpClose, fontIconImgCLose);
        var lblDetailsHeading = new kony.ui.Label(extendConfig({
            "id": "lblDetailsHeading",
            "isVisible": true,
            "left": "20dp",
            "skin": "sknLbl192B4518pxLatoReg",
            "text": "Label",
            "top": "40dp",
            "width": kony.flex.USE_PREFFERED_SIZE
        }, controller.args[0], "lblDetailsHeading"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblDetailsHeading"), extendConfig({}, controller.args[2], "lblDetailsHeading"));
        var flxBulkUpdateScreen1 = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "bottom": "0dp",
            "clipBounds": true,
            "id": "flxBulkUpdateScreen1",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "left": "0dp",
            "isModalContainer": false,
            "skin": "slFbox",
            "top": "60dp",
            "width": "100%"
        }, controller.args[0], "flxBulkUpdateScreen1"), extendConfig({}, controller.args[1], "flxBulkUpdateScreen1"), extendConfig({}, controller.args[2], "flxBulkUpdateScreen1"));
        flxBulkUpdateScreen1.setDefaultUnit(kony.flex.DP);
        var flxSelectionContainer = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "bottom": "80dp",
            "clipBounds": true,
            "id": "flxSelectionContainer",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "left": "20dp",
            "isModalContainer": false,
            "right": "20dp",
            "skin": "slFbox",
            "top": "0dp"
        }, controller.args[0], "flxSelectionContainer"), extendConfig({}, controller.args[1], "flxSelectionContainer"), extendConfig({}, controller.args[2], "flxSelectionContainer"));
        flxSelectionContainer.setDefaultUnit(kony.flex.DP);
        var flxHeaderScreen1 = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "clipBounds": true,
            "height": "106dp",
            "id": "flxHeaderScreen1",
            "isVisible": true,
            "layoutType": kony.flex.FLOW_VERTICAL,
            "left": "0",
            "isModalContainer": false,
            "skin": "slFbox",
            "top": "0",
            "width": "100%"
        }, controller.args[0], "flxHeaderScreen1"), extendConfig({}, controller.args[1], "flxHeaderScreen1"), extendConfig({}, controller.args[2], "flxHeaderScreen1"));
        flxHeaderScreen1.setDefaultUnit(kony.flex.DP);
        var lblHeadingScreen1 = new kony.ui.Label(extendConfig({
            "id": "lblHeadingScreen1",
            "isVisible": true,
            "left": "0dp",
            "skin": "sknLbl192b45LatoReg16px",
            "text": "Label",
            "top": "20dp",
            "width": kony.flex.USE_PREFFERED_SIZE
        }, controller.args[0], "lblHeadingScreen1"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblHeadingScreen1"), extendConfig({}, controller.args[2], "lblHeadingScreen1"));
        var flxSearchContainer = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "clipBounds": true,
            "height": "60dp",
            "id": "flxSearchContainer",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "left": "0",
            "isModalContainer": false,
            "skin": "slFbox",
            "top": "0dp",
            "width": "100%"
        }, controller.args[0], "flxSearchContainer"), extendConfig({}, controller.args[1], "flxSearchContainer"), extendConfig({}, controller.args[2], "flxSearchContainer"));
        flxSearchContainer.setDefaultUnit(kony.flex.DP);
        var customersDropdownBulkUpdate = new com.adminConsole.contracts.customersDropdown(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
            "centerY": "50%",
            "id": "customersDropdownBulkUpdate",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "left": "0dp",
            "masterType": constants.MASTER_TYPE_DEFAULT,
            "isModalContainer": false,
            "skin": "slFbox",
            "width": "240dp",
            "overrides": {
                "customersDropdown": {
                    "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                    "centerY": "50%",
                    "top": "viz.val_cleared",
                    "width": "240dp"
                }
            }
        }, controller.args[0], "customersDropdownBulkUpdate"), extendConfig({
            "overrides": {}
        }, controller.args[1], "customersDropdownBulkUpdate"), extendConfig({
            "overrides": {}
        }, controller.args[2], "customersDropdownBulkUpdate"));
        var searchBoxScreen1 = new com.adminConsole.common.searchBox(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "height": "60dp",
            "id": "searchBoxScreen1",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "masterType": constants.MASTER_TYPE_DEFAULT,
            "isModalContainer": false,
            "right": "0dp",
            "skin": "slFbox",
            "top": "0dp",
            "width": "47%",
            "overrides": {
                "flxSearchContainer": {
                    "centerY": "50%",
                    "height": "40px",
                    "top": "viz.val_cleared",
                    "width": "100%"
                },
                "searchBox": {
                    "bottom": "viz.val_cleared",
                    "centerX": "viz.val_cleared",
                    "centerY": "viz.val_cleared",
                    "height": "60dp",
                    "left": "viz.val_cleared",
                    "maxHeight": "viz.val_cleared",
                    "maxWidth": "viz.val_cleared",
                    "minHeight": "viz.val_cleared",
                    "minWidth": "viz.val_cleared",
                    "right": "0dp",
                    "top": "0dp",
                    "width": "47%"
                },
                "tbxSearchBox": {
                    "centerY": "50%",
                    "i18n_placeholder": "kony.i18n.getLocalizedString(\"i18n.frmEnrollCustomer.SearchByAccountNumberName\")"
                }
            }
        }, controller.args[0], "searchBoxScreen1"), extendConfig({
            "overrides": {}
        }, controller.args[1], "searchBoxScreen1"), extendConfig({
            "overrides": {}
        }, controller.args[2], "searchBoxScreen1"));
        flxSearchContainer.add(customersDropdownBulkUpdate, searchBoxScreen1);
        flxHeaderScreen1.add(lblHeadingScreen1, flxSearchContainer);
        var flxSegmentContainer = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "bottom": "0dp",
            "clipBounds": true,
            "id": "flxSegmentContainer",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "left": "0",
            "isModalContainer": false,
            "skin": "slFbox",
            "top": "40dp",
            "width": "100%"
        }, controller.args[0], "flxSegmentContainer"), extendConfig({}, controller.args[1], "flxSegmentContainer"), extendConfig({}, controller.args[2], "flxSegmentContainer"));
        flxSegmentContainer.setDefaultUnit(kony.flex.DP);
        var segSelectionList = new kony.ui.SegmentedUI2(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "bottom": "10dp",
            "groupCells": false,
            "id": "segSelectionList",
            "isVisible": true,
            "left": "0",
            "needPageIndicator": true,
            "pageOffDotImage": "pageoffdot.png",
            "pageOnDotImage": "pageondot.png",
            "retainSelection": false,
            "rowFocusSkin": "seg2Focus",
            "rowSkin": "seg2Normal",
            "sectionHeaderSkin": "sliPhoneSegmentHeader",
            "selectionBehavior": constants.SEGUI_DEFAULT_BEHAVIOR,
            "separatorColor": "aaaaaa00",
            "separatorRequired": false,
            "separatorThickness": 1,
            "showScrollbars": true,
            "top": "0",
            "viewType": constants.SEGUI_VIEW_TYPE_TABLEVIEW,
            "widgetDataMap": {},
            "width": "100%"
        }, controller.args[0], "segSelectionList"), extendConfig({
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "segSelectionList"), extendConfig({}, controller.args[2], "segSelectionList"));
        var flxFilterMenu = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
            "clipBounds": true,
            "id": "flxFilterMenu",
            "isVisible": false,
            "layoutType": kony.flex.FREE_FORM,
            "isModalContainer": false,
            "right": "30dp",
            "skin": "slFbox",
            "top": "20px",
            "width": "120px",
            "zIndex": 5
        }, controller.args[0], "flxFilterMenu"), extendConfig({}, controller.args[1], "flxFilterMenu"), extendConfig({}, controller.args[2], "flxFilterMenu"));
        flxFilterMenu.setDefaultUnit(kony.flex.DP);
        var filterMenu = new com.adminConsole.common.statusFilterMenu(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
            "id": "filterMenu",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "left": "0dp",
            "masterType": constants.MASTER_TYPE_DEFAULT,
            "isModalContainer": false,
            "skin": "slFbox",
            "top": "5dp",
            "width": "100%",
            "zIndex": 5,
            "overrides": {
                "imgUpArrow": {
                    "src": "uparrow_2x.png"
                },
                "segStatusFilterDropdown": {
                    "data": [{
                        "imgCheckBox": "checkbox.png",
                        "lblDescription": "Active"
                    }, {
                        "imgCheckBox": "checkbox.png",
                        "lblDescription": "Inactive"
                    }, {
                        "imgCheckBox": "checkbox.png",
                        "lblDescription": "Suspended"
                    }],
                    "left": "0dp",
                    "top": "5dp"
                }
            }
        }, controller.args[0], "filterMenu"), extendConfig({
            "overrides": {}
        }, controller.args[1], "filterMenu"), extendConfig({
            "overrides": {}
        }, controller.args[2], "filterMenu"));
        flxFilterMenu.add(filterMenu);
        var lblNoResultsScreen1 = new kony.ui.Label(extendConfig({
            "centerX": "50%",
            "id": "lblNoResultsScreen1",
            "isVisible": false,
            "skin": "sknLblLato84939E12px",
            "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmPoliciesController.No_results_found\")",
            "top": "100dp",
            "width": kony.flex.USE_PREFFERED_SIZE
        }, controller.args[0], "lblNoResultsScreen1"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblNoResultsScreen1"), extendConfig({}, controller.args[2], "lblNoResultsScreen1"));
        flxSegmentContainer.add(segSelectionList, flxFilterMenu, lblNoResultsScreen1);
        flxSelectionContainer.add(flxHeaderScreen1, flxSegmentContainer);
        var flxButtonsScreen1 = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "bottom": "0dp",
            "clipBounds": true,
            "height": "80dp",
            "id": "flxButtonsScreen1",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "left": "0dp",
            "isModalContainer": false,
            "skin": "sknflxBgE4E6EC",
            "width": "100%"
        }, controller.args[0], "flxButtonsScreen1"), extendConfig({}, controller.args[1], "flxButtonsScreen1"), extendConfig({}, controller.args[2], "flxButtonsScreen1"));
        flxButtonsScreen1.setDefaultUnit(kony.flex.DP);
        var commonButtonsScreen1 = new com.adminConsole.common.commonButtons(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "height": "80px",
            "id": "commonButtonsScreen1",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "left": "20dp",
            "masterType": constants.MASTER_TYPE_DEFAULT,
            "isModalContainer": false,
            "right": "20dp",
            "skin": "slFbox",
            "top": "0dp",
            "overrides": {
                "btnNext": {
                    "isVisible": false
                },
                "commonButtons": {
                    "left": "20dp",
                    "right": "20dp",
                    "width": "viz.val_cleared"
                }
            }
        }, controller.args[0], "commonButtonsScreen1"), extendConfig({
            "overrides": {}
        }, controller.args[1], "commonButtonsScreen1"), extendConfig({
            "overrides": {}
        }, controller.args[2], "commonButtonsScreen1"));
        flxButtonsScreen1.add(commonButtonsScreen1);
        flxBulkUpdateScreen1.add(flxSelectionContainer, flxButtonsScreen1);
        var flxBulkUpdateScreen2 = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "bottom": "0dp",
            "clipBounds": true,
            "id": "flxBulkUpdateScreen2",
            "isVisible": false,
            "layoutType": kony.flex.FREE_FORM,
            "left": "0",
            "isModalContainer": false,
            "skin": "slFbox",
            "top": "70dp",
            "width": "100%"
        }, controller.args[0], "flxBulkUpdateScreen2"), extendConfig({}, controller.args[1], "flxBulkUpdateScreen2"), extendConfig({}, controller.args[2], "flxBulkUpdateScreen2"));
        flxBulkUpdateScreen2.setDefaultUnit(kony.flex.DP);
        var flxBulkUpdateBody = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "clipBounds": true,
            "height": "100%",
            "id": "flxBulkUpdateBody",
            "isVisible": true,
            "layoutType": kony.flex.FLOW_VERTICAL,
            "left": "0dp",
            "isModalContainer": false,
            "skin": "slFbox",
            "top": "0dp",
            "width": "100%"
        }, controller.args[0], "flxBulkUpdateBody"), extendConfig({}, controller.args[1], "flxBulkUpdateBody"), extendConfig({}, controller.args[2], "flxBulkUpdateBody"));
        flxBulkUpdateBody.setDefaultUnit(kony.flex.DP);
        var flxBulkUpdateTagsContainer = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
            "clipBounds": true,
            "id": "flxBulkUpdateTagsContainer",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "left": "0",
            "isModalContainer": false,
            "skin": "slFbox",
            "top": "0",
            "width": "100%"
        }, controller.args[0], "flxBulkUpdateTagsContainer"), extendConfig({}, controller.args[1], "flxBulkUpdateTagsContainer"), extendConfig({}, controller.args[2], "flxBulkUpdateTagsContainer"));
        flxBulkUpdateTagsContainer.setDefaultUnit(kony.flex.DP);
        var flxArrow = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
            "clipBounds": true,
            "id": "flxArrow",
            "isVisible": true,
            "layoutType": kony.flex.FLOW_HORIZONTAL,
            "left": "20dp",
            "isModalContainer": false,
            "skin": "slFbox",
            "top": "20dp",
            "width": "35%",
            "zIndex": 1
        }, controller.args[0], "flxArrow"), extendConfig({}, controller.args[1], "flxArrow"), extendConfig({}, controller.args[2], "flxArrow"));
        flxArrow.setDefaultUnit(kony.flex.DP);
        var lblSelectedHeading = new kony.ui.Label(extendConfig({
            "id": "lblSelectedHeading",
            "isVisible": true,
            "left": "0px",
            "skin": "sknLbl485C75LatoRegular13Px",
            "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmGroupsController.Selected_Customers\")",
            "top": "0px",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 200
        }, controller.args[0], "lblSelectedHeading"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblSelectedHeading"), extendConfig({}, controller.args[2], "lblSelectedHeading"));
        var lblIconArrow = new kony.ui.Label(extendConfig({
            "id": "lblIconArrow",
            "isVisible": true,
            "left": "10dp",
            "skin": "sknfontIconDescDownArrow12px",
            "i18n_text": "kony.i18n.getLocalizedString(\"i18n.userwidgetmodel.fontIconBreadcrumbsDown\")",
            "top": "3dp",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 200
        }, controller.args[0], "lblIconArrow"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblIconArrow"), extendConfig({
            "hoverSkin": "sknfontIconDescDownArrow12pxHover"
        }, controller.args[2], "lblIconArrow"));
        var btnModifySearch = new kony.ui.Button(extendConfig({
            "id": "btnModifySearch",
            "isVisible": true,
            "left": "20dp",
            "onClick": controller.AS_Button_e161b42beeaf43beb1f12a5d0c519d0f,
            "skin": "sknBtnLato117eb013Px",
            "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmGroups.btnModifySearch\")",
            "top": "0dp",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, controller.args[0], "btnModifySearch"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_RIGHT,
            "displayText": true,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "btnModifySearch"), extendConfig({
            "hoverSkin": "sknBtnLatoRegular1682b213pxHover"
        }, controller.args[2], "btnModifySearch"));
        flxArrow.add(lblSelectedHeading, lblIconArrow, btnModifySearch);
        var flxTagsContainer = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
            "clipBounds": true,
            "id": "flxTagsContainer",
            "isVisible": false,
            "layoutType": kony.flex.FREE_FORM,
            "left": "0dp",
            "isModalContainer": false,
            "right": "20dp",
            "skin": "slFbox",
            "top": "50dp"
        }, controller.args[0], "flxTagsContainer"), extendConfig({}, controller.args[1], "flxTagsContainer"), extendConfig({}, controller.args[2], "flxTagsContainer"));
        flxTagsContainer.setDefaultUnit(kony.flex.DP);
        flxTagsContainer.add();
        var flxSelectionTag = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "clipBounds": true,
            "height": "20px",
            "id": "flxSelectionTag",
            "isVisible": false,
            "layoutType": kony.flex.FLOW_HORIZONTAL,
            "left": "20dp",
            "isModalContainer": false,
            "skin": "sknFlxBgFFFFFFbor1px003E75",
            "top": "30dp",
            "width": "170px",
            "zIndex": 1
        }, controller.args[0], "flxSelectionTag"), extendConfig({}, controller.args[1], "flxSelectionTag"), extendConfig({}, controller.args[2], "flxSelectionTag"));
        flxSelectionTag.setDefaultUnit(kony.flex.DP);
        var lblTagName = new kony.ui.Label(extendConfig({
            "height": "15px",
            "id": "lblTagName",
            "isVisible": true,
            "left": "10px",
            "skin": "skn003E75Lato12px",
            "text": "KL098234 - Weistheimey",
            "top": "1dp",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, controller.args[0], "lblTagName"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblTagName"), extendConfig({}, controller.args[2], "lblTagName"));
        var flxCross = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "clipBounds": true,
            "height": "100%",
            "id": "flxCross",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "left": "10px",
            "isModalContainer": false,
            "skin": "slFbox",
            "top": "0dp",
            "width": "15px",
            "zIndex": 1
        }, controller.args[0], "flxCross"), extendConfig({}, controller.args[1], "flxCross"), extendConfig({}, controller.args[2], "flxCross"));
        flxCross.setDefaultUnit(kony.flex.DP);
        var lblIconCross = new kony.ui.Label(extendConfig({
            "centerY": "50%",
            "id": "lblIconCross",
            "isVisible": true,
            "right": "7dp",
            "skin": "sknIcoMoon10pxCursor003E75",
            "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmAlertsManagement.lblAlertPreviewClose\")",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, controller.args[0], "lblIconCross"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblIconCross"), extendConfig({}, controller.args[2], "lblIconCross"));
        flxCross.add(lblIconCross);
        flxSelectionTag.add(lblTagName, flxCross);
        flxBulkUpdateTagsContainer.add(flxArrow, flxTagsContainer, flxSelectionTag);
        var lblTitle = new kony.ui.Label(extendConfig({
            "id": "lblTitle",
            "isVisible": true,
            "left": "20dp",
            "skin": "sknLbl192b45LatoReg16px",
            "text": "Label",
            "top": "20dp",
            "width": kony.flex.USE_PREFFERED_SIZE
        }, controller.args[0], "lblTitle"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblTitle"), extendConfig({}, controller.args[2], "lblTitle"));
        var flxRadioGroup = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "clipBounds": true,
            "height": "50px",
            "id": "flxRadioGroup",
            "isVisible": true,
            "layoutType": kony.flex.FLOW_VERTICAL,
            "left": "0dp",
            "isModalContainer": false,
            "skin": "slFbox",
            "top": "15dp",
            "width": "100%"
        }, controller.args[0], "flxRadioGroup"), extendConfig({}, controller.args[1], "flxRadioGroup"), extendConfig({}, controller.args[2], "flxRadioGroup"));
        flxRadioGroup.setDefaultUnit(kony.flex.DP);
        var lblRadioGroupTitle = new kony.ui.Label(extendConfig({
            "id": "lblRadioGroupTitle",
            "isVisible": true,
            "left": "20dp",
            "skin": "sknLbl485C75LatoRegular13Px",
            "text": "Permission Type",
            "top": "0dp",
            "width": kony.flex.USE_PREFFERED_SIZE
        }, controller.args[0], "lblRadioGroupTitle"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblRadioGroupTitle"), extendConfig({}, controller.args[2], "lblRadioGroupTitle"));
        var customRadioButtonGroup = new com.adminConsole.common.customRadioButtonGroup(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
            "id": "customRadioButtonGroup",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "left": "20dp",
            "masterType": constants.MASTER_TYPE_DEFAULT,
            "isModalContainer": false,
            "skin": "slFbox",
            "top": "15dp",
            "width": "100%",
            "overrides": {
                "customRadioButtonGroup": {
                    "left": "20dp",
                    "top": "15dp"
                },
                "flxRadioButton1": {
                    "width": "100px"
                },
                "flxRadioButton3": {
                    "isVisible": false
                },
                "imgRadioButton1": {
                    "src": "radionormal_1x.png"
                },
                "imgRadioButton2": {
                    "src": "radionormal_1x.png"
                },
                "lblRadioButtonValue1": {
                    "text": "Enable",
                    "width": "viz.val_cleared"
                },
                "lblRadioButtonValue2": {
                    "text": "Disable"
                }
            }
        }, controller.args[0], "customRadioButtonGroup"), extendConfig({
            "overrides": {}
        }, controller.args[1], "customRadioButtonGroup"), extendConfig({
            "overrides": {}
        }, controller.args[2], "customRadioButtonGroup"));
        flxRadioGroup.add(lblRadioGroupTitle, customRadioButtonGroup);
        var flxBulkUpdateListContainer = new kony.ui.FlexScrollContainer(extendConfig({
            "allowHorizontalBounce": false,
            "allowVerticalBounce": true,
            "bounces": true,
            "clipBounds": false,
            "enableScrolling": true,
            "height": "350dp",
            "horizontalScrollIndicator": true,
            "id": "flxBulkUpdateListContainer",
            "isVisible": true,
            "layoutType": kony.flex.FLOW_VERTICAL,
            "left": "0dp",
            "pagingEnabled": false,
            "scrollDirection": kony.flex.SCROLL_VERTICAL,
            "skin": "slFSbox",
            "top": "0dp",
            "verticalScrollIndicator": true,
            "width": "100%"
        }, controller.args[0], "flxBulkUpdateListContainer"), extendConfig({}, controller.args[1], "flxBulkUpdateListContainer"), extendConfig({}, controller.args[2], "flxBulkUpdateListContainer"));
        flxBulkUpdateListContainer.setDefaultUnit(kony.flex.DP);
        var flxAddNewRowContainer = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
            "clipBounds": true,
            "id": "flxAddNewRowContainer",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "left": "0",
            "isModalContainer": false,
            "skin": "slFbox",
            "top": "0dp",
            "width": "100%"
        }, controller.args[0], "flxAddNewRowContainer"), extendConfig({}, controller.args[1], "flxAddNewRowContainer"), extendConfig({}, controller.args[2], "flxAddNewRowContainer"));
        flxAddNewRowContainer.setDefaultUnit(kony.flex.DP);
        var bulkUpdateAddNewFeatureRow = new com.adminConsole.contracts.bulkUpdateAddNewFeatureRow(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
            "id": "bulkUpdateAddNewFeatureRow",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "left": "20dp",
            "masterType": constants.MASTER_TYPE_DEFAULT,
            "isModalContainer": false,
            "right": "20dp",
            "skin": "slFbox",
            "top": "0dp",
            "overrides": {
                "bulkUpdateAddNewFeatureRow": {
                    "left": "20dp",
                    "right": "20dp",
                    "width": "viz.val_cleared"
                }
            }
        }, controller.args[0], "bulkUpdateAddNewFeatureRow"), extendConfig({
            "overrides": {}
        }, controller.args[1], "bulkUpdateAddNewFeatureRow"), extendConfig({
            "overrides": {}
        }, controller.args[2], "bulkUpdateAddNewFeatureRow"));
        flxAddNewRowContainer.add(bulkUpdateAddNewFeatureRow);
        var flxAddNewRowListCont = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
            "clipBounds": false,
            "id": "flxAddNewRowListCont",
            "isVisible": false,
            "layoutType": kony.flex.FLOW_VERTICAL,
            "left": "0",
            "isModalContainer": false,
            "skin": "slFbox",
            "top": "0dp",
            "width": "100%"
        }, controller.args[0], "flxAddNewRowListCont"), extendConfig({}, controller.args[1], "flxAddNewRowListCont"), extendConfig({}, controller.args[2], "flxAddNewRowListCont"));
        flxAddNewRowListCont.setDefaultUnit(kony.flex.DP);
        flxAddNewRowListCont.add();
        var flxAddNewRowButton = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "clipBounds": true,
            "height": "40dp",
            "id": "flxAddNewRowButton",
            "isVisible": true,
            "layoutType": kony.flex.FLOW_VERTICAL,
            "left": "0dp",
            "isModalContainer": false,
            "skin": "slFbox",
            "top": "20dp",
            "width": "200dp"
        }, controller.args[0], "flxAddNewRowButton"), extendConfig({}, controller.args[1], "flxAddNewRowButton"), extendConfig({}, controller.args[2], "flxAddNewRowButton"));
        flxAddNewRowButton.setDefaultUnit(kony.flex.DP);
        var btnAddNewRow = new kony.ui.Button(extendConfig({
            "id": "btnAddNewRow",
            "isVisible": true,
            "left": "20dp",
            "onClick": controller.AS_Button_e161b42beeaf43beb1f12a5d0c519d0f,
            "skin": "sknBtnLato117eb013Px",
            "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmGroups.btnModifySearch\")",
            "top": "0dp",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, controller.args[0], "btnAddNewRow"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_RIGHT,
            "displayText": true,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "btnAddNewRow"), extendConfig({
            "hoverSkin": "sknBtnLatoRegular1682b213pxHover"
        }, controller.args[2], "btnAddNewRow"));
        flxAddNewRowButton.add(btnAddNewRow);
        flxBulkUpdateListContainer.add(flxAddNewRowContainer, flxAddNewRowListCont, flxAddNewRowButton);
        flxBulkUpdateBody.add(flxBulkUpdateTagsContainer, lblTitle, flxRadioGroup, flxBulkUpdateListContainer);
        var flxButtonsScreen2 = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "bottom": "0dp",
            "clipBounds": true,
            "height": "80dp",
            "id": "flxButtonsScreen2",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "left": "0dp",
            "isModalContainer": false,
            "skin": "sknflxBgE4E6EC",
            "width": "100%"
        }, controller.args[0], "flxButtonsScreen2"), extendConfig({}, controller.args[1], "flxButtonsScreen2"), extendConfig({}, controller.args[2], "flxButtonsScreen2"));
        flxButtonsScreen2.setDefaultUnit(kony.flex.DP);
        var commonButtonsScreen2 = new com.adminConsole.common.commonButtons(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "height": "80px",
            "id": "commonButtonsScreen2",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "left": "20dp",
            "masterType": constants.MASTER_TYPE_DEFAULT,
            "isModalContainer": false,
            "right": "20dp",
            "skin": "slFbox",
            "top": "0dp",
            "overrides": {
                "btnNext": {
                    "isVisible": false
                },
                "commonButtons": {
                    "left": "20dp",
                    "right": "20dp",
                    "width": "viz.val_cleared"
                }
            }
        }, controller.args[0], "commonButtonsScreen2"), extendConfig({
            "overrides": {}
        }, controller.args[1], "commonButtonsScreen2"), extendConfig({
            "overrides": {}
        }, controller.args[2], "commonButtonsScreen2"));
        flxButtonsScreen2.add(commonButtonsScreen2);
        flxBulkUpdateScreen2.add(flxBulkUpdateBody, flxButtonsScreen2);
        flxContractDetailsPopupContainer.add(flxPopUpTopColor, flxPopUpClose, lblDetailsHeading, flxBulkUpdateScreen1, flxBulkUpdateScreen2);
        bulkUpdateFeaturesLimitsPopup.add(flxContractDetailsPopupContainer);
        return bulkUpdateFeaturesLimitsPopup;
    }
})