define("com/adminConsole/contracts/bulkUpdateFeaturesLimitsPopup/userbulkUpdateFeaturesLimitsPopupController", function() {
    return {};
});
define("com/adminConsole/contracts/bulkUpdateFeaturesLimitsPopup/bulkUpdateFeaturesLimitsPopupControllerActions", {
    /*
        This is an auto generated file and any modifications to it may result in corruption of the action sequence.
    */
});
define("com/adminConsole/contracts/bulkUpdateFeaturesLimitsPopup/bulkUpdateFeaturesLimitsPopupController", ["com/adminConsole/contracts/bulkUpdateFeaturesLimitsPopup/userbulkUpdateFeaturesLimitsPopupController", "com/adminConsole/contracts/bulkUpdateFeaturesLimitsPopup/bulkUpdateFeaturesLimitsPopupControllerActions"], function() {
    var controller = require("com/adminConsole/contracts/bulkUpdateFeaturesLimitsPopup/userbulkUpdateFeaturesLimitsPopupController");
    var actions = require("com/adminConsole/contracts/bulkUpdateFeaturesLimitsPopup/bulkUpdateFeaturesLimitsPopupControllerActions");
    for (var key in actions) {
        controller[key] = actions[key];
    }
    return controller;
});
