define(function() {
    return function(controller) {
        var serviceCard = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "bottom": "20dp",
            "clipBounds": false,
            "isMaster": true,
            "height": "80px",
            "id": "serviceCard",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "left": "0dp",
            "isModalContainer": false,
            "skin": "sknFlxbgFFFFFFbdrD7D9E0rd3px",
            "top": "0dp",
            "width": "100%"
        }, controller.args[0], "serviceCard"), extendConfig({}, controller.args[1], "serviceCard"), extendConfig({}, controller.args[2], "serviceCard"));
        serviceCard.setDefaultUnit(kony.flex.DP);
        var flxCardContainer = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "clipBounds": true,
            "height": "80px",
            "id": "flxCardContainer",
            "isVisible": true,
            "layoutType": kony.flex.FLOW_VERTICAL,
            "left": "0dp",
            "isModalContainer": false,
            "skin": "sknCursor",
            "top": "0dp",
            "width": "100%",
            "zIndex": 1
        }, controller.args[0], "flxCardContainer"), extendConfig({}, controller.args[1], "flxCardContainer"), extendConfig({
            "hoverSkin": "sknCursor"
        }, controller.args[2], "flxCardContainer"));
        flxCardContainer.setDefaultUnit(kony.flex.DP);
        var lblCategoryName = new kony.ui.Label(extendConfig({
            "id": "lblCategoryName",
            "isVisible": true,
            "left": "20dp",
            "skin": "sknLbl192B45LatoRegular14px",
            "text": "Security",
            "top": "15dp",
            "width": "88%",
            "zIndex": 1
        }, controller.args[0], "lblCategoryName"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblCategoryName"), extendConfig({}, controller.args[2], "lblCategoryName"));
        var flxServiceTag = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "bottom": "15dp",
            "clipBounds": true,
            "height": "20px",
            "id": "flxServiceTag",
            "isVisible": true,
            "layoutType": kony.flex.FLOW_HORIZONTAL,
            "left": "20px",
            "isModalContainer": false,
            "skin": "sknflxCustomertagRedRadius4px",
            "top": "15dp",
            "width": "105px"
        }, controller.args[0], "flxServiceTag"), extendConfig({}, controller.args[1], "flxServiceTag"), extendConfig({}, controller.args[2], "flxServiceTag"));
        flxServiceTag.setDefaultUnit(kony.flex.DP);
        var fontIconCircle1 = new kony.ui.Label(extendConfig({
            "centerY": "48%",
            "id": "fontIconCircle1",
            "isVisible": true,
            "left": "5px",
            "skin": "sknCircleCustomerTag",
            "i18n_text": "kony.i18n.getLocalizedString(\"i18n.userwidgetmodel.lblFontIconStatus1\")",
            "top": "0dp",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 2
        }, controller.args[0], "fontIconCircle1"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "fontIconCircle1"), extendConfig({}, controller.args[2], "fontIconCircle1"));
        var lblContent1 = new kony.ui.Label(extendConfig({
            "centerY": "50%",
            "id": "lblContent1",
            "isVisible": true,
            "left": "5px",
            "right": "5dp",
            "skin": "sknCustomerTypeTag",
            "text": "Retail Customer",
            "top": "0dp",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, controller.args[0], "lblContent1"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblContent1"), extendConfig({}, controller.args[2], "lblContent1"));
        flxServiceTag.add(fontIconCircle1, lblContent1);
        flxCardContainer.add(lblCategoryName, flxServiceTag);
        serviceCard.add(flxCardContainer);
        return serviceCard;
    }
})