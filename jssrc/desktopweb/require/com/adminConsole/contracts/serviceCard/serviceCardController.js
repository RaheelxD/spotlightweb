define("com/adminConsole/contracts/serviceCard/userserviceCardController", function() {
    return {};
});
define("com/adminConsole/contracts/serviceCard/serviceCardControllerActions", {
    /*
          This is an auto generated file and any modifications to it may result in corruption of the action sequence.
        */
});
define("com/adminConsole/contracts/serviceCard/serviceCardController", ["com/adminConsole/contracts/serviceCard/userserviceCardController", "com/adminConsole/contracts/serviceCard/serviceCardControllerActions"], function() {
    var controller = require("com/adminConsole/contracts/serviceCard/userserviceCardController");
    var actions = require("com/adminConsole/contracts/serviceCard/serviceCardControllerActions");
    for (var key in actions) {
        controller[key] = actions[key];
    }
    return controller;
});
