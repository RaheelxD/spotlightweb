define(function() {
    return function(controller) {
        var accountsFeaturesCard = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
            "clipBounds": true,
            "isMaster": true,
            "id": "accountsFeaturesCard",
            "isVisible": true,
            "layoutType": kony.flex.FLOW_VERTICAL,
            "left": "0dp",
            "isModalContainer": false,
            "preShow": function(eventobject) {
                controller.AS_FlexContainer_g2a601a510e14c4fa2664ac91d111eeb(eventobject);
            },
            "skin": "sknFlxbgFFFFFFbdrD7D9E0rd3px",
            "top": "0dp",
            "width": "100%"
        }, controller.args[0], "accountsFeaturesCard"), extendConfig({}, controller.args[1], "accountsFeaturesCard"), extendConfig({}, controller.args[2], "accountsFeaturesCard"));
        accountsFeaturesCard.setDefaultUnit(kony.flex.DP);
        var flxCardTopContainer = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
            "clipBounds": true,
            "id": "flxCardTopContainer",
            "isVisible": true,
            "layoutType": kony.flex.FLOW_VERTICAL,
            "left": "0",
            "isModalContainer": false,
            "skin": "slFbox",
            "top": "0",
            "width": "100%"
        }, controller.args[0], "flxCardTopContainer"), extendConfig({}, controller.args[1], "flxCardTopContainer"), extendConfig({}, controller.args[2], "flxCardTopContainer"));
        flxCardTopContainer.setDefaultUnit(kony.flex.DP);
        var flxContractDetails = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
            "clipBounds": true,
            "id": "flxContractDetails",
            "isVisible": true,
            "layoutType": kony.flex.FLOW_VERTICAL,
            "left": "0",
            "isModalContainer": false,
            "skin": "slFbox",
            "top": "5dp",
            "width": "100%"
        }, controller.args[0], "flxContractDetails"), extendConfig({}, controller.args[1], "flxContractDetails"), extendConfig({}, controller.args[2], "flxContractDetails"));
        flxContractDetails.setDefaultUnit(kony.flex.DP);
        var flxDetailHeadingCont = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
            "clipBounds": true,
            "id": "flxDetailHeadingCont",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "left": "0dp",
            "isModalContainer": false,
            "skin": "slFbox",
            "top": "5dp",
            "width": "100%"
        }, controller.args[0], "flxDetailHeadingCont"), extendConfig({}, controller.args[1], "flxDetailHeadingCont"), extendConfig({}, controller.args[2], "flxDetailHeadingCont"));
        flxDetailHeadingCont.setDefaultUnit(kony.flex.DP);
        var flxLeftContainer = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
            "clipBounds": true,
            "id": "flxLeftContainer",
            "isVisible": true,
            "layoutType": kony.flex.FLOW_HORIZONTAL,
            "left": "0dp",
            "isModalContainer": false,
            "skin": "slFbox",
            "top": "0dp",
            "width": "50%"
        }, controller.args[0], "flxLeftContainer"), extendConfig({}, controller.args[1], "flxLeftContainer"), extendConfig({}, controller.args[2], "flxLeftContainer"));
        flxLeftContainer.setDefaultUnit(kony.flex.DP);
        var lblName = new kony.ui.Label(extendConfig({
            "height": "20dp",
            "id": "lblName",
            "isVisible": true,
            "left": "15dp",
            "skin": "sknLbl117EB0LatoReg14px",
            "text": "Label",
            "top": "0dp",
            "width": kony.flex.USE_PREFFERED_SIZE
        }, controller.args[0], "lblName"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblName"), extendConfig({
            "hoverSkin": "sknLbl117EB0LatoReg14pxHov"
        }, controller.args[2], "lblName"));
        var flxContractTag = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "clipBounds": true,
            "height": "20px",
            "id": "flxContractTag",
            "isVisible": false,
            "layoutType": kony.flex.FREE_FORM,
            "left": "10dp",
            "isModalContainer": false,
            "skin": "sknFlxBg3146A4Rd3px",
            "top": "0dp",
            "width": "60px"
        }, controller.args[0], "flxContractTag"), extendConfig({}, controller.args[1], "flxContractTag"), extendConfig({}, controller.args[2], "flxContractTag"));
        flxContractTag.setDefaultUnit(kony.flex.DP);
        var lblContractTag = new kony.ui.Label(extendConfig({
            "centerX": "50%",
            "centerY": "50%",
            "id": "lblContractTag",
            "isVisible": true,
            "skin": "sknCustomerTypeTag",
            "text": "Contract",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, controller.args[0], "lblContractTag"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_BOTTOM_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblContractTag"), extendConfig({}, controller.args[2], "lblContractTag"));
        flxContractTag.add(lblContractTag);
        var flxPrimary = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "clipBounds": true,
            "height": "20dp",
            "id": "flxPrimary",
            "isVisible": false,
            "layoutType": kony.flex.FREE_FORM,
            "left": "10dp",
            "isModalContainer": false,
            "skin": "sknflx1F844DRadius20px",
            "top": "0dp",
            "width": "61dp",
            "zIndex": 1
        }, controller.args[0], "flxPrimary"), extendConfig({}, controller.args[1], "flxPrimary"), extendConfig({}, controller.args[2], "flxPrimary"));
        flxPrimary.setDefaultUnit(kony.flex.DP);
        var lblPrimary = new kony.ui.Label(extendConfig({
            "centerY": "50%",
            "id": "lblPrimary",
            "isVisible": true,
            "left": "10dp",
            "skin": "sknlblffffffLato12px",
            "text": "Primary",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, controller.args[0], "lblPrimary"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_TOP_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblPrimary"), extendConfig({}, controller.args[2], "lblPrimary"));
        flxPrimary.add(lblPrimary);
        flxLeftContainer.add(lblName, flxContractTag, flxPrimary);
        var btnViewEdit = new kony.ui.Button(extendConfig({
            "height": "22px",
            "id": "btnViewEdit",
            "isVisible": false,
            "right": "20dp",
            "skin": "sknbtnf7f7faLatoRegular12Px485c75Border1px485C75Radius20px",
            "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmCustomerProfileFeaturesActions.ViewContract\")",
            "top": "0dp",
            "width": "106dp",
            "zIndex": 1
        }, controller.args[0], "btnViewEdit"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_CENTER,
            "displayText": true,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "btnViewEdit"), extendConfig({
            "hoverSkin": "sknbtnffffffLatoRegular12Px485c75Border1px485C75Radius20px"
        }, controller.args[2], "btnViewEdit"));
        flxDetailHeadingCont.add(flxLeftContainer, btnViewEdit);
        var flxDetails = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
            "clipBounds": true,
            "id": "flxDetails",
            "isVisible": true,
            "layoutType": kony.flex.FLOW_HORIZONTAL,
            "left": "0dp",
            "isModalContainer": false,
            "skin": "slFbox",
            "top": "10dp",
            "width": "100%"
        }, controller.args[0], "flxDetails"), extendConfig({}, controller.args[1], "flxDetails"), extendConfig({}, controller.args[2], "flxDetails"));
        flxDetails.setDefaultUnit(kony.flex.DP);
        var flxColumn1 = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
            "clipBounds": true,
            "id": "flxColumn1",
            "isVisible": true,
            "layoutType": kony.flex.FLOW_VERTICAL,
            "left": "15px",
            "isModalContainer": false,
            "skin": "slFbox",
            "top": "0dp",
            "width": "29%",
            "zIndex": 1
        }, controller.args[0], "flxColumn1"), extendConfig({}, controller.args[1], "flxColumn1"), extendConfig({}, controller.args[2], "flxColumn1"));
        flxColumn1.setDefaultUnit(kony.flex.DP);
        var lblHeading1 = new kony.ui.Label(extendConfig({
            "id": "lblHeading1",
            "isVisible": true,
            "left": "0px",
            "skin": "sknlblLato696c7311px",
            "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmCustomerManagementController.CUSTOMER_ID\")",
            "top": "0px",
            "width": "100%",
            "zIndex": 1
        }, controller.args[0], "lblHeading1"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblHeading1"), extendConfig({}, controller.args[2], "lblHeading1"));
        var lblData1 = new kony.ui.Label(extendConfig({
            "id": "lblData1",
            "isVisible": true,
            "left": "0px",
            "skin": "sknlbl485C75LatoSemiBold13px",
            "i18n_text": "kony.i18n.getLocalizedString(\"i18n.userwidgetmodel.lblData1\")",
            "top": "8px",
            "width": "100%",
            "zIndex": 1
        }, controller.args[0], "lblData1"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblData1"), extendConfig({}, controller.args[2], "lblData1"));
        flxColumn1.add(lblHeading1, lblData1);
        var flxColumn2 = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
            "clipBounds": true,
            "id": "flxColumn2",
            "isVisible": true,
            "layoutType": kony.flex.FLOW_VERTICAL,
            "left": "10dp",
            "isModalContainer": false,
            "skin": "slFbox",
            "top": "0dp",
            "width": "29%",
            "zIndex": 1
        }, controller.args[0], "flxColumn2"), extendConfig({}, controller.args[1], "flxColumn2"), extendConfig({}, controller.args[2], "flxColumn2"));
        flxColumn2.setDefaultUnit(kony.flex.DP);
        var lblHeading2 = new kony.ui.Label(extendConfig({
            "id": "lblHeading2",
            "isVisible": true,
            "left": "0px",
            "skin": "sknlblLato696c7311px",
            "text": "TAX ID",
            "top": "0px",
            "width": "100%",
            "zIndex": 1
        }, controller.args[0], "lblHeading2"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblHeading2"), extendConfig({}, controller.args[2], "lblHeading2"));
        var lblData2 = new kony.ui.Label(extendConfig({
            "id": "lblData2",
            "isVisible": true,
            "left": "0px",
            "skin": "sknlbl485C75LatoSemiBold13px",
            "i18n_text": "kony.i18n.getLocalizedString(\"i18n.userwidgetmodel.lblData1\")",
            "top": "8px",
            "width": "100%",
            "zIndex": 1
        }, controller.args[0], "lblData2"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblData2"), extendConfig({}, controller.args[2], "lblData2"));
        flxColumn2.add(lblHeading2, lblData2);
        var flxColumn3 = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
            "clipBounds": true,
            "id": "flxColumn3",
            "isVisible": true,
            "layoutType": kony.flex.FLOW_VERTICAL,
            "left": "10dp",
            "isModalContainer": false,
            "skin": "slFbox",
            "top": "0dp",
            "width": "29%",
            "zIndex": 1
        }, controller.args[0], "flxColumn3"), extendConfig({}, controller.args[1], "flxColumn3"), extendConfig({}, controller.args[2], "flxColumn3"));
        flxColumn3.setDefaultUnit(kony.flex.DP);
        var lblHeading3 = new kony.ui.Label(extendConfig({
            "id": "lblHeading3",
            "isVisible": true,
            "left": "0px",
            "skin": "sknlblLato696c7311px",
            "i18n_text": "kony.i18n.getLocalizedString(\"i18n.View.ADDRESS\")",
            "top": "0px",
            "width": "100%",
            "zIndex": 1
        }, controller.args[0], "lblHeading3"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblHeading3"), extendConfig({}, controller.args[2], "lblHeading3"));
        var lblData3 = new kony.ui.Label(extendConfig({
            "id": "lblData3",
            "isVisible": true,
            "left": "0px",
            "skin": "sknlbl485C75LatoSemiBold13px",
            "i18n_text": "kony.i18n.getLocalizedString(\"i18n.userwidgetmodel.lblData1\")",
            "top": "8px",
            "width": "97%",
            "zIndex": 1
        }, controller.args[0], "lblData3"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblData3"), extendConfig({}, controller.args[2], "lblData3"));
        flxColumn3.add(lblHeading3, lblData3);
        var flxColumn4 = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
            "clipBounds": true,
            "id": "flxColumn4",
            "isVisible": false,
            "layoutType": kony.flex.FLOW_VERTICAL,
            "left": "10dp",
            "isModalContainer": false,
            "skin": "slFbox",
            "top": "0dp",
            "width": "22.50%",
            "zIndex": 1
        }, controller.args[0], "flxColumn4"), extendConfig({}, controller.args[1], "flxColumn4"), extendConfig({}, controller.args[2], "flxColumn4"));
        flxColumn4.setDefaultUnit(kony.flex.DP);
        var lblHeading4 = new kony.ui.Label(extendConfig({
            "id": "lblHeading4",
            "isVisible": true,
            "left": "0px",
            "skin": "sknlblLato696c7311px",
            "i18n_text": "kony.i18n.getLocalizedString(\"i18n.View.ADDRESS\")",
            "top": "0px",
            "width": "100%",
            "zIndex": 1
        }, controller.args[0], "lblHeading4"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblHeading4"), extendConfig({}, controller.args[2], "lblHeading4"));
        var lblData4 = new kony.ui.Label(extendConfig({
            "id": "lblData4",
            "isVisible": true,
            "left": "0px",
            "skin": "sknlbl485C75LatoSemiBold13px",
            "i18n_text": "kony.i18n.getLocalizedString(\"i18n.userwidgetmodel.lblData1\")",
            "top": "8px",
            "width": "97%",
            "zIndex": 1
        }, controller.args[0], "lblData4"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblData4"), extendConfig({}, controller.args[2], "lblData4"));
        flxColumn4.add(lblHeading4, lblData4);
        flxDetails.add(flxColumn1, flxColumn2, flxColumn3, flxColumn4);
        flxContractDetails.add(flxDetailHeadingCont, flxDetails);
        var flxHeading = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "clipBounds": true,
            "height": "45dp",
            "id": "flxHeading",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "left": "0",
            "isModalContainer": false,
            "skin": "sknflxf5f6f8Op100",
            "top": "15dp",
            "width": "100%"
        }, controller.args[0], "flxHeading"), extendConfig({}, controller.args[1], "flxHeading"), extendConfig({}, controller.args[2], "flxHeading"));
        flxHeading.setDefaultUnit(kony.flex.DP);
        var flxTopSeperatorLine = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "clipBounds": true,
            "height": "1dp",
            "id": "flxTopSeperatorLine",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "left": "0",
            "isModalContainer": false,
            "skin": "sknFlxD7D9E0Separator",
            "top": "0",
            "width": "100%"
        }, controller.args[0], "flxTopSeperatorLine"), extendConfig({}, controller.args[1], "flxTopSeperatorLine"), extendConfig({}, controller.args[2], "flxTopSeperatorLine"));
        flxTopSeperatorLine.setDefaultUnit(kony.flex.DP);
        flxTopSeperatorLine.add();
        var flxHeadingLeftContainer = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "clipBounds": true,
            "height": "100%",
            "id": "flxHeadingLeftContainer",
            "isVisible": true,
            "layoutType": kony.flex.FLOW_HORIZONTAL,
            "left": "10dp",
            "isModalContainer": false,
            "skin": "slFbox",
            "top": "0dp",
            "width": "80%"
        }, controller.args[0], "flxHeadingLeftContainer"), extendConfig({}, controller.args[1], "flxHeadingLeftContainer"), extendConfig({}, controller.args[2], "flxHeadingLeftContainer"));
        flxHeadingLeftContainer.setDefaultUnit(kony.flex.DP);
        var flxArrow = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "centerY": "50%",
            "clipBounds": true,
            "height": "20dp",
            "id": "flxArrow",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "left": "5dp",
            "isModalContainer": false,
            "skin": "sknCursor",
            "width": "20dp"
        }, controller.args[0], "flxArrow"), extendConfig({}, controller.args[1], "flxArrow"), extendConfig({
            "hoverSkin": "sknCursor"
        }, controller.args[2], "flxArrow"));
        flxArrow.setDefaultUnit(kony.flex.DP);
        var lblArrow = new kony.ui.Label(extendConfig({
            "centerY": "50%",
            "id": "lblArrow",
            "isVisible": true,
            "left": "0",
            "skin": "sknIcon00000015px",
            "text": "",
            "width": kony.flex.USE_PREFFERED_SIZE
        }, controller.args[0], "lblArrow"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblArrow"), extendConfig({}, controller.args[2], "lblArrow"));
        flxArrow.add(lblArrow);
        var lblHeading = new kony.ui.Label(extendConfig({
            "centerY": "50%",
            "id": "lblHeading",
            "isVisible": true,
            "left": "5dp",
            "skin": "sknLbl192B45LatoRegular14px",
            "text": "Label",
            "width": kony.flex.USE_PREFFERED_SIZE
        }, controller.args[0], "lblHeading"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblHeading"), extendConfig({}, controller.args[2], "lblHeading"));
        var lblCount = new kony.ui.Label(extendConfig({
            "centerY": "50%",
            "id": "lblCount",
            "isVisible": true,
            "left": "5dp",
            "skin": "sknLblLatoMed00000014px",
            "text": "Label",
            "width": kony.flex.USE_PREFFERED_SIZE
        }, controller.args[0], "lblCount"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblCount"), extendConfig({}, controller.args[2], "lblCount"));
        var lblTotalCount = new kony.ui.Label(extendConfig({
            "centerY": "50%",
            "id": "lblTotalCount",
            "isVisible": false,
            "left": 5,
            "skin": "sknLbl192B45LatoRegular14px",
            "text": "Label",
            "width": kony.flex.USE_PREFFERED_SIZE
        }, controller.args[0], "lblTotalCount"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblTotalCount"), extendConfig({}, controller.args[2], "lblTotalCount"));
        flxHeadingLeftContainer.add(flxArrow, lblHeading, lblCount, lblTotalCount);
        var flxBottomSeperator = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "bottom": "0dp",
            "clipBounds": true,
            "height": "1dp",
            "id": "flxBottomSeperator",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "left": "0dp",
            "isModalContainer": false,
            "skin": "sknFlxD7D9E0Separator",
            "width": "100%"
        }, controller.args[0], "flxBottomSeperator"), extendConfig({}, controller.args[1], "flxBottomSeperator"), extendConfig({}, controller.args[2], "flxBottomSeperator"));
        flxBottomSeperator.setDefaultUnit(kony.flex.DP);
        flxBottomSeperator.add();
        var btnReset = new kony.ui.Button(extendConfig({
            "centerY": "50%",
            "height": "20dp",
            "id": "btnReset",
            "isVisible": false,
            "right": "15dp",
            "skin": "sknBtnLato117eb013Px",
            "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmEnrollCustomer.ResetToDefault\")",
            "width": kony.flex.USE_PREFFERED_SIZE
        }, controller.args[0], "btnReset"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_CENTER,
            "displayText": true,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "btnReset"), extendConfig({}, controller.args[2], "btnReset"));
        var flxHeadingRightContainer = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "clipBounds": true,
            "height": "100%",
            "id": "flxHeadingRightContainer",
            "isVisible": false,
            "layoutType": kony.flex.FLOW_HORIZONTAL,
            "reverseLayoutDirection": true,
            "isModalContainer": false,
            "right": "0dp",
            "skin": "slFbox",
            "top": "0dp",
            "width": "50%",
            "zIndex": 2
        }, controller.args[0], "flxHeadingRightContainer"), extendConfig({}, controller.args[1], "flxHeadingRightContainer"), extendConfig({}, controller.args[2], "flxHeadingRightContainer"));
        flxHeadingRightContainer.setDefaultUnit(kony.flex.DP);
        var btnEdit = new kony.ui.Button(extendConfig({
            "centerY": "50%",
            "height": "22px",
            "id": "btnEdit",
            "isVisible": true,
            "right": "20dp",
            "skin": "sknbtnf7f7faLatoRegular12Px485c75Border1px485C75Radius20px",
            "i18n_text": "kony.i18n.getLocalizedString(\"i18n.common.Edit\")",
            "width": "52dp",
            "zIndex": 1
        }, controller.args[0], "btnEdit"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_CENTER,
            "displayText": true,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "btnEdit"), extendConfig({
            "hoverSkin": "sknbtnffffffLatoRegular12Px485c75Border1px485C75Radius20px"
        }, controller.args[2], "btnEdit"));
        var btnView = new kony.ui.Button(extendConfig({
            "centerY": "50%",
            "height": "22px",
            "id": "btnView",
            "isVisible": true,
            "right": "20dp",
            "skin": "sknbtnf7f7faLatoRegular12Px485c75Border1px485C75Radius20px",
            "text": "View By Accounts",
            "width": "125dp",
            "zIndex": 1
        }, controller.args[0], "btnView"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_CENTER,
            "displayText": true,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "btnView"), extendConfig({
            "hoverSkin": "sknbtnffffffLatoRegular12Px485c75Border1px485C75Radius20px"
        }, controller.args[2], "btnView"));
        flxHeadingRightContainer.add(btnEdit, btnView);
        var flxCheckboxCont = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "centerY": "50%",
            "clipBounds": true,
            "height": "30px",
            "id": "flxCheckboxCont",
            "isVisible": false,
            "layoutType": kony.flex.FLOW_HORIZONTAL,
            "reverseLayoutDirection": true,
            "isModalContainer": false,
            "right": "15dp",
            "skin": "slFbox",
            "width": "50%"
        }, controller.args[0], "flxCheckboxCont"), extendConfig({}, controller.args[1], "flxCheckboxCont"), extendConfig({}, controller.args[2], "flxCheckboxCont"));
        flxCheckboxCont.setDefaultUnit(kony.flex.DP);
        var lblLine = new kony.ui.Label(extendConfig({
            "centerY": "50%",
            "height": "25dp",
            "id": "lblLine",
            "isVisible": false,
            "left": "10px",
            "right": "10dp",
            "skin": "sknlblSeperatorD7D9E0",
            "width": "1dp"
        }, controller.args[0], "lblLine"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_TOP_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblLine"), extendConfig({}, controller.args[2], "lblLine"));
        var lblAccountOptions = new kony.ui.Label(extendConfig({
            "centerY": "50.00%",
            "id": "lblAccountOptions",
            "isVisible": true,
            "left": "5dp",
            "skin": "sknLbl485C75LatoRegular13Px",
            "text": "Implicit Account Access",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, controller.args[0], "lblAccountOptions"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblAccountOptions"), extendConfig({}, controller.args[2], "lblAccountOptions"));
        var fontIconAccountInfo = new kony.ui.Label(extendConfig({
            "centerY": "50.00%",
            "id": "fontIconAccountInfo",
            "isVisible": false,
            "right": "0px",
            "skin": "sknIcon485c75Info16px",
            "text": "",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, controller.args[0], "fontIconAccountInfo"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "fontIconAccountInfo"), extendConfig({}, controller.args[2], "fontIconAccountInfo"));
        var flxCheckboxOptions = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "centerY": "50%",
            "clipBounds": true,
            "height": "15dp",
            "id": "flxCheckboxOptions",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "left": "15dp",
            "isModalContainer": false,
            "right": "5px",
            "skin": "sknCursor",
            "width": "15dp"
        }, controller.args[0], "flxCheckboxOptions"), extendConfig({}, controller.args[1], "flxCheckboxOptions"), extendConfig({}, controller.args[2], "flxCheckboxOptions"));
        flxCheckboxOptions.setDefaultUnit(kony.flex.DP);
        var imgCheckboxOptions = new kony.ui.Image2(extendConfig({
            "height": "100%",
            "id": "imgCheckboxOptions",
            "isVisible": true,
            "left": "0",
            "skin": "slImage",
            "src": "checkboxnormal.png",
            "top": "0",
            "width": "100%"
        }, controller.args[0], "imgCheckboxOptions"), extendConfig({
            "imageScaleMode": constants.IMAGE_SCALE_MODE_MAINTAIN_ASPECT_RATIO,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "imgCheckboxOptions"), extendConfig({}, controller.args[2], "imgCheckboxOptions"));
        flxCheckboxOptions.add(imgCheckboxOptions);
        flxCheckboxCont.add(lblLine, lblAccountOptions, fontIconAccountInfo, flxCheckboxOptions);
        flxHeading.add(flxTopSeperatorLine, flxHeadingLeftContainer, flxBottomSeperator, btnReset, flxHeadingRightContainer, flxCheckboxCont);
        flxCardTopContainer.add(flxContractDetails, flxHeading);
        var flxCardBottomContainer = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
            "clipBounds": true,
            "id": "flxCardBottomContainer",
            "isVisible": false,
            "layoutType": kony.flex.FLOW_VERTICAL,
            "left": "0",
            "isModalContainer": false,
            "skin": "slFbox",
            "top": "0",
            "width": "100%"
        }, controller.args[0], "flxCardBottomContainer"), extendConfig({}, controller.args[1], "flxCardBottomContainer"), extendConfig({}, controller.args[2], "flxCardBottomContainer"));
        flxCardBottomContainer.setDefaultUnit(kony.flex.DP);
        var flxSelectAllOption = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "clipBounds": true,
            "height": "45dp",
            "id": "flxSelectAllOption",
            "isVisible": false,
            "layoutType": kony.flex.FREE_FORM,
            "left": "0",
            "isModalContainer": false,
            "skin": "slFbox",
            "top": "0dp",
            "width": "100%"
        }, controller.args[0], "flxSelectAllOption"), extendConfig({}, controller.args[1], "flxSelectAllOption"), extendConfig({}, controller.args[2], "flxSelectAllOption"));
        flxSelectAllOption.setDefaultUnit(kony.flex.DP);
        var flxSelectAllHeader = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "clipBounds": true,
            "height": "100%",
            "id": "flxSelectAllHeader",
            "isVisible": true,
            "layoutType": kony.flex.FLOW_HORIZONTAL,
            "left": "0dp",
            "isModalContainer": false,
            "skin": "slFbox",
            "top": "0dp",
            "width": "80%"
        }, controller.args[0], "flxSelectAllHeader"), extendConfig({}, controller.args[1], "flxSelectAllHeader"), extendConfig({}, controller.args[2], "flxSelectAllHeader"));
        flxSelectAllHeader.setDefaultUnit(kony.flex.DP);
        var flxCheckbox = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "centerY": "50%",
            "clipBounds": true,
            "height": "15dp",
            "id": "flxCheckbox",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "left": "15dp",
            "isModalContainer": false,
            "skin": "sknCursor",
            "width": "15dp"
        }, controller.args[0], "flxCheckbox"), extendConfig({}, controller.args[1], "flxCheckbox"), extendConfig({}, controller.args[2], "flxCheckbox"));
        flxCheckbox.setDefaultUnit(kony.flex.DP);
        var imgSectionCheckbox = new kony.ui.Image2(extendConfig({
            "height": "100%",
            "id": "imgSectionCheckbox",
            "isVisible": true,
            "left": "0",
            "skin": "slImage",
            "src": "checkboxnormal.png",
            "top": "0",
            "width": "100%"
        }, controller.args[0], "imgSectionCheckbox"), extendConfig({
            "imageScaleMode": constants.IMAGE_SCALE_MODE_MAINTAIN_ASPECT_RATIO,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "imgSectionCheckbox"), extendConfig({}, controller.args[2], "imgSectionCheckbox"));
        flxCheckbox.add(imgSectionCheckbox);
        var lblSelectAllHeading = new kony.ui.Label(extendConfig({
            "centerY": "50%",
            "id": "lblSelectAllHeading",
            "isVisible": true,
            "left": "15dp",
            "skin": "sknLbl485C75LatoRegular13Px",
            "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmEnrollCustomer.SelectAllFeatures_UC\")",
            "width": kony.flex.USE_PREFFERED_SIZE
        }, controller.args[0], "lblSelectAllHeading"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblSelectAllHeading"), extendConfig({}, controller.args[2], "lblSelectAllHeading"));
        flxSelectAllHeader.add(flxCheckbox, lblSelectAllHeading);
        var flxSelectAllSeperator = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "bottom": "0dp",
            "clipBounds": true,
            "height": "1dp",
            "id": "flxSelectAllSeperator",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "left": "0dp",
            "isModalContainer": false,
            "skin": "sknFlxD7D9E0Separator",
            "width": "100%"
        }, controller.args[0], "flxSelectAllSeperator"), extendConfig({}, controller.args[1], "flxSelectAllSeperator"), extendConfig({}, controller.args[2], "flxSelectAllSeperator"));
        flxSelectAllSeperator.setDefaultUnit(kony.flex.DP);
        flxSelectAllSeperator.add();
        flxSelectAllOption.add(flxSelectAllHeader, flxSelectAllSeperator);
        var flxDynamicWidgetsContainer = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
            "clipBounds": true,
            "id": "flxDynamicWidgetsContainer",
            "isVisible": false,
            "layoutType": kony.flex.FLOW_VERTICAL,
            "left": "0",
            "isModalContainer": false,
            "skin": "slFbox",
            "top": "0",
            "width": "100%"
        }, controller.args[0], "flxDynamicWidgetsContainer"), extendConfig({}, controller.args[1], "flxDynamicWidgetsContainer"), extendConfig({}, controller.args[2], "flxDynamicWidgetsContainer"));
        flxDynamicWidgetsContainer.setDefaultUnit(kony.flex.DP);
        flxDynamicWidgetsContainer.add();
        var segAccountFeatures = new kony.ui.SegmentedUI2(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
            "bottom": "0dp",
            "groupCells": false,
            "id": "segAccountFeatures",
            "isVisible": true,
            "left": "0",
            "needPageIndicator": true,
            "pageOffDotImage": "pageoffdot.png",
            "pageOnDotImage": "pageondot.png",
            "retainSelection": false,
            "rowFocusSkin": "seg2Focus",
            "rowSkin": "seg2Normal",
            "sectionHeaderSkin": "sliPhoneSegmentHeader",
            "selectionBehavior": constants.SEGUI_DEFAULT_BEHAVIOR,
            "separatorColor": "aaaaaa00",
            "separatorRequired": false,
            "separatorThickness": 1,
            "showScrollbars": false,
            "top": "0",
            "viewType": constants.SEGUI_VIEW_TYPE_TABLEVIEW,
            "widgetDataMap": {},
            "width": "100%"
        }, controller.args[0], "segAccountFeatures"), extendConfig({
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "segAccountFeatures"), extendConfig({}, controller.args[2], "segAccountFeatures"));
        var reportPagination = new com.adminConsole.reports.reportPagination(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "height": "50dp",
            "id": "reportPagination",
            "isVisible": false,
            "layoutType": kony.flex.FREE_FORM,
            "left": "0dp",
            "masterType": constants.MASTER_TYPE_DEFAULT,
            "isModalContainer": false,
            "skin": "sknBackgroundFFFFFF",
            "top": "0dp",
            "width": "100%",
            "overrides": {
                "reportPagination": {
                    "isVisible": false
                }
            }
        }, controller.args[0], "reportPagination"), extendConfig({
            "overrides": {}
        }, controller.args[1], "reportPagination"), extendConfig({
            "overrides": {}
        }, controller.args[2], "reportPagination"));
        var flxNoFilterResults = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "centerX": "50%",
            "clipBounds": true,
            "height": "75px",
            "id": "flxNoFilterResults",
            "isVisible": false,
            "layoutType": kony.flex.FLOW_HORIZONTAL,
            "isModalContainer": false,
            "skin": "slFbox",
            "top": "0%",
            "width": "100%",
            "zIndex": 1
        }, controller.args[0], "flxNoFilterResults"), extendConfig({}, controller.args[1], "flxNoFilterResults"), extendConfig({}, controller.args[2], "flxNoFilterResults"));
        flxNoFilterResults.setDefaultUnit(kony.flex.DP);
        var lblNoFilterResults = new kony.ui.Label(extendConfig({
            "centerX": "50%",
            "centerY": "50%",
            "id": "lblNoFilterResults",
            "isVisible": true,
            "skin": "sknLblLato84939E12px",
            "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmUsersController.No_results_found\")",
            "width": "80%",
            "zIndex": 1
        }, controller.args[0], "lblNoFilterResults"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_CENTER,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblNoFilterResults"), extendConfig({}, controller.args[2], "lblNoFilterResults"));
        flxNoFilterResults.add(lblNoFilterResults);
        flxCardBottomContainer.add(flxSelectAllOption, flxDynamicWidgetsContainer, segAccountFeatures, reportPagination, flxNoFilterResults);
        accountsFeaturesCard.add(flxCardTopContainer, flxCardBottomContainer);
        return accountsFeaturesCard;
    }
})