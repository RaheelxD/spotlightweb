define("com/adminConsole/contracts/customersDropdown/usercustomersDropdownController", function() {
    return {
        dropdownPreshow: function() {
            var self = this;
            this.view.flxSelectedText.onClick = function() {
                self.showHideCustomersDropdown();
            };
        },
        showHideCustomersDropdown: function() {
            this.view.flxSegmentList.setVisibility(!this.view.flxSegmentList.isVisible);
            this.view.lblIconDropdown.text = this.view.flxSegmentList.isVisible ? "" : "";
            this.view.forceLayout();
        }
    };
});
define("com/adminConsole/contracts/customersDropdown/customersDropdownControllerActions", {
    /*
        This is an auto generated file and any modifications to it may result in corruption of the action sequence.
    */
    AS_FlexContainer_i00ef24af0fe44e9832e03e00f71477c: function AS_FlexContainer_i00ef24af0fe44e9832e03e00f71477c(eventobject) {
        var self = this;
        this.dropdownPreshow();
    },
});
define("com/adminConsole/contracts/customersDropdown/customersDropdownController", ["com/adminConsole/contracts/customersDropdown/usercustomersDropdownController", "com/adminConsole/contracts/customersDropdown/customersDropdownControllerActions"], function() {
    var controller = require("com/adminConsole/contracts/customersDropdown/usercustomersDropdownController");
    var actions = require("com/adminConsole/contracts/customersDropdown/customersDropdownControllerActions");
    for (var key in actions) {
        controller[key] = actions[key];
    }
    return controller;
});
