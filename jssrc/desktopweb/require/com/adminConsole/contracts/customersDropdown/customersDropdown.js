define(function() {
    return function(controller) {
        var customersDropdown = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "clipBounds": false,
            "isMaster": true,
            "height": "100%",
            "id": "customersDropdown",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "left": "0dp",
            "isModalContainer": false,
            "preShow": function(eventobject) {
                controller.AS_FlexContainer_i00ef24af0fe44e9832e03e00f71477c(eventobject);
            },
            "skin": "slFbox",
            "top": "0dp",
            "width": "100%"
        }, controller.args[0], "customersDropdown"), extendConfig({}, controller.args[1], "customersDropdown"), extendConfig({}, controller.args[2], "customersDropdown"));
        customersDropdown.setDefaultUnit(kony.flex.DP);
        var flxSelectedText = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "clipBounds": true,
            "height": "30dp",
            "id": "flxSelectedText",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "left": "0dp",
            "isModalContainer": false,
            "skin": "sknFlxBorder117eb0radius3pxbgfff",
            "top": "0dp",
            "width": "100%",
            "zIndex": 1
        }, controller.args[0], "flxSelectedText"), extendConfig({}, controller.args[1], "flxSelectedText"), extendConfig({
            "hoverSkin": "sknflxffffffBorderE1E5EERadius3pxPointer"
        }, controller.args[2], "flxSelectedText"));
        flxSelectedText.setDefaultUnit(kony.flex.DP);
        var lblSelectedValue = new kony.ui.Label(extendConfig({
            "centerY": "50%",
            "id": "lblSelectedValue",
            "isVisible": true,
            "left": "14dp",
            "skin": "sknlbl485C75LatoSemiBold13px",
            "text": "Label",
            "top": "3dp",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, controller.args[0], "lblSelectedValue"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblSelectedValue"), extendConfig({}, controller.args[2], "lblSelectedValue"));
        var flxDropdown = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "centerY": "50%",
            "clipBounds": true,
            "height": "30dp",
            "id": "flxDropdown",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "isModalContainer": false,
            "right": "0dp",
            "skin": "slFbox",
            "top": "0dp",
            "width": "30dp"
        }, controller.args[0], "flxDropdown"), extendConfig({}, controller.args[1], "flxDropdown"), extendConfig({}, controller.args[2], "flxDropdown"));
        flxDropdown.setDefaultUnit(kony.flex.DP);
        var lblIconDropdown = new kony.ui.Label(extendConfig({
            "centerY": "50%",
            "id": "lblIconDropdown",
            "isVisible": true,
            "right": "10dp",
            "skin": "sknIcoMoon003E7513px",
            "text": "",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, controller.args[0], "lblIconDropdown"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblIconDropdown"), extendConfig({}, controller.args[2], "lblIconDropdown"));
        flxDropdown.add(lblIconDropdown);
        var btnPrimary = new kony.ui.Button(extendConfig({
            "centerY": "50%",
            "height": "20dp",
            "id": "btnPrimary",
            "isVisible": false,
            "right": "35dp",
            "skin": "sknbg1f844dborrad16pxffffff",
            "text": "Primary",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, controller.args[0], "btnPrimary"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_CENTER,
            "displayText": true,
            "padding": [3, 0, 3, 0],
            "paddingInPixel": false
        }, controller.args[1], "btnPrimary"), extendConfig({
            "hoverSkin": "sknbg1f844dborrad16pxffffff"
        }, controller.args[2], "btnPrimary"));
        flxSelectedText.add(lblSelectedValue, flxDropdown, btnPrimary);
        var flxSegmentList = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
            "clipBounds": false,
            "id": "flxSegmentList",
            "isVisible": false,
            "layoutType": kony.flex.FLOW_VERTICAL,
            "left": "0dp",
            "isModalContainer": false,
            "skin": "sknFlxbgFFFFFFbdrD7D9E0rd3px",
            "top": "33dp",
            "width": "100%",
            "zIndex": 1
        }, controller.args[0], "flxSegmentList"), extendConfig({}, controller.args[1], "flxSegmentList"), extendConfig({}, controller.args[2], "flxSegmentList"));
        flxSegmentList.setDefaultUnit(kony.flex.DP);
        var flxSearchContainer = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "bottom": "5dp",
            "clipBounds": true,
            "height": "40px",
            "id": "flxSearchContainer",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "left": "0px",
            "isModalContainer": false,
            "skin": "sknBackgroundFFFFFF",
            "top": "0dp",
            "width": "100%",
            "zIndex": 1
        }, controller.args[0], "flxSearchContainer"), extendConfig({}, controller.args[1], "flxSearchContainer"), extendConfig({}, controller.args[2], "flxSearchContainer"));
        flxSearchContainer.setDefaultUnit(kony.flex.DP);
        var fontIconSearch = new kony.ui.Label(extendConfig({
            "centerY": "50%",
            "id": "fontIconSearch",
            "isVisible": true,
            "left": "10dp",
            "skin": "sknIcoMoonlB2BDCB16px",
            "i18n_text": "kony.i18n.getLocalizedString(\"i18n.userwidgetmodel.lblSearchIcon\")",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, controller.args[0], "fontIconSearch"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "fontIconSearch"), extendConfig({}, controller.args[2], "fontIconSearch"));
        var tbxSearchBox = new kony.ui.TextBox2(extendConfig({
            "autoCapitalize": constants.TEXTBOX_AUTO_CAPITALIZE_NONE,
            "centerY": "50%",
            "height": "30px",
            "id": "tbxSearchBox",
            "isVisible": true,
            "keyBoardStyle": constants.TEXTBOX_KEY_BOARD_STYLE_DEFAULT,
            "left": "35dp",
            "onTouchStart": controller.AS_TextField_cfab06859f0f43eca99495891ee290d5,
            "placeholder": "Search",
            "right": "35px",
            "secureTextEntry": false,
            "skin": "sknTbxBgFFFFFFf485c7513pxNoBorder",
            "textInputMode": constants.TEXTBOX_INPUT_MODE_ANY,
            "zIndex": 1
        }, controller.args[0], "tbxSearchBox"), extendConfig({
            "containerHeightMode": constants.TEXTBOX_FONT_METRICS_DRIVEN_HEIGHT,
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "tbxSearchBox"), extendConfig({
            "autoCorrect": false
        }, controller.args[2], "tbxSearchBox"));
        var flxClearSearchImage = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "clipBounds": true,
            "height": "100%",
            "id": "flxClearSearchImage",
            "isVisible": false,
            "layoutType": kony.flex.FREE_FORM,
            "isModalContainer": false,
            "right": "0px",
            "skin": "slFbox",
            "top": "0dp",
            "width": "35px",
            "zIndex": 1
        }, controller.args[0], "flxClearSearchImage"), extendConfig({}, controller.args[1], "flxClearSearchImage"), extendConfig({
            "hoverSkin": "sknCursor"
        }, controller.args[2], "flxClearSearchImage"));
        flxClearSearchImage.setDefaultUnit(kony.flex.DP);
        var fontIconClearSearch = new kony.ui.Label(extendConfig({
            "centerY": "50%",
            "id": "fontIconClearSearch",
            "isVisible": true,
            "right": "15dp",
            "skin": "sknFontIconSearchCross20px",
            "i18n_text": "kony.i18n.getLocalizedString(\"i18n.userwidgetmodel.lblClearSearch\")",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, controller.args[0], "fontIconClearSearch"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "fontIconClearSearch"), extendConfig({}, controller.args[2], "fontIconClearSearch"));
        flxClearSearchImage.add(fontIconClearSearch);
        var lblSearchSeparator = new kony.ui.Label(extendConfig({
            "bottom": "0dp",
            "height": "1px",
            "id": "lblSearchSeparator",
            "isVisible": true,
            "left": "0dp",
            "skin": "sknlblSeperatorD7D9E0",
            "text": "-",
            "width": "100%",
            "zIndex": 1
        }, controller.args[0], "lblSearchSeparator"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblSearchSeparator"), extendConfig({}, controller.args[2], "lblSearchSeparator"));
        flxSearchContainer.add(fontIconSearch, tbxSearchBox, flxClearSearchImage, lblSearchSeparator);
        var segList = new kony.ui.SegmentedUI2(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
            "bottom": "10px",
            "data": [{
                "btnPrimary": "Primary",
                "lblCustomerName": "Label"
            }, {
                "btnPrimary": "Primary",
                "lblCustomerName": "Label"
            }, {
                "btnPrimary": "Primary",
                "lblCustomerName": "Label"
            }, {
                "btnPrimary": "Primary",
                "lblCustomerName": "Label"
            }],
            "groupCells": false,
            "id": "segList",
            "isVisible": true,
            "left": "0dp",
            "needPageIndicator": true,
            "pageOffDotImage": "pageoffdot.png",
            "pageOnDotImage": "pageondot.png",
            "retainSelection": false,
            "rowFocusSkin": "seg2Focus",
            "rowSkin": "seg2Normal",
            "rowTemplate": "flxCustomerName",
            "sectionHeaderSkin": "sliPhoneSegmentHeader",
            "selectionBehavior": constants.SEGUI_MULTI_SELECT_BEHAVIOR,
            "selectionBehaviorConfig": {
                "imageIdentifier": "imgCheckBox",
                "selectedStateImage": "checkboxselected.png",
                "unselectedStateImage": "checkboxnormal.png"
            },
            "separatorColor": "aaaaaa00",
            "separatorRequired": false,
            "separatorThickness": 1,
            "showScrollbars": false,
            "top": "0dp",
            "viewType": constants.SEGUI_VIEW_TYPE_TABLEVIEW,
            "widgetDataMap": {
                "btnPrimary": "btnPrimary",
                "flxCustomerName": "flxCustomerName",
                "flxCustomerNameCont": "flxCustomerNameCont",
                "lblCustomerName": "lblCustomerName"
            },
            "width": "100%",
            "zIndex": 1
        }, controller.args[0], "segList"), extendConfig({
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "segList"), extendConfig({}, controller.args[2], "segList"));
        var flxNoResult = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "clipBounds": false,
            "height": "100dp",
            "id": "flxNoResult",
            "isVisible": false,
            "layoutType": kony.flex.FREE_FORM,
            "left": "15px",
            "isModalContainer": false,
            "skin": "slFbox",
            "top": "0px",
            "width": "97%",
            "zIndex": 1
        }, controller.args[0], "flxNoResult"), extendConfig({}, controller.args[1], "flxNoResult"), extendConfig({}, controller.args[2], "flxNoResult"));
        flxNoResult.setDefaultUnit(kony.flex.DP);
        var lblNoResult = new kony.ui.Label(extendConfig({
            "centerX": "50%",
            "centerY": "49%",
            "id": "lblNoResult",
            "isVisible": true,
            "skin": "sknLblLatoBold485c7513px",
            "i18n_text": "kony.i18n.getLocalizedString(\"i18n.FastTransfers.NoResultsFound\")",
            "width": "90%",
            "zIndex": 2
        }, controller.args[0], "lblNoResult"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblNoResult"), extendConfig({}, controller.args[2], "lblNoResult"));
        flxNoResult.add(lblNoResult);
        flxSegmentList.add(flxSearchContainer, segList, flxNoResult);
        customersDropdown.add(flxSelectedText, flxSegmentList);
        return customersDropdown;
    }
})