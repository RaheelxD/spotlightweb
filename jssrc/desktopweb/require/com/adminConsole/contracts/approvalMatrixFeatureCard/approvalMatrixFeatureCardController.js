define("com/adminConsole/contracts/approvalMatrixFeatureCard/userapprovalMatrixFeatureCardController", function() {
    return {
        callPreShow: function() {
            this.setActions();
        },
        setActions: function() {},
        /*
         * toggle listing segment on clock of arrow
         */
        toggleCollapseArrow: function(opt) {
            if (opt === true) {
                this.view.lblToggle.text = "\ue915"; //down-arrow
                this.view.lblToggle.skin = "sknIcon00000014px";
                this.view.flxCardBottomContainer.setVisibility(true);
                this.view.flxBottomSeperator.setVisibility(true);
                this.view.flxTopSeperatorLine.setVisibility(false);
            } else if (opt === false) {
                this.view.lblToggle.text = "\ue922"; //right-arrow
                this.view.lblToggle.skin = "sknIcon00000015px";
                this.view.flxCardBottomContainer.setVisibility(false);
                this.view.flxBottomSeperator.setVisibility(false);
                this.view.flxTopSeperatorLine.setVisibility(true);
            }
        }
    };
});
define("com/adminConsole/contracts/approvalMatrixFeatureCard/approvalMatrixFeatureCardControllerActions", {
    /*
        This is an auto generated file and any modifications to it may result in corruption of the action sequence.
    */
    AS_FlexContainer_b8b1c898df68458a8aaec294f8726a9f: function AS_FlexContainer_b8b1c898df68458a8aaec294f8726a9f(eventobject) {
        var self = this;
        this.callPreShow();
    }
});
define("com/adminConsole/contracts/approvalMatrixFeatureCard/approvalMatrixFeatureCardController", ["com/adminConsole/contracts/approvalMatrixFeatureCard/userapprovalMatrixFeatureCardController", "com/adminConsole/contracts/approvalMatrixFeatureCard/approvalMatrixFeatureCardControllerActions"], function() {
    var controller = require("com/adminConsole/contracts/approvalMatrixFeatureCard/userapprovalMatrixFeatureCardController");
    var actions = require("com/adminConsole/contracts/approvalMatrixFeatureCard/approvalMatrixFeatureCardControllerActions");
    for (var key in actions) {
        controller[key] = actions[key];
    }
    return controller;
});
