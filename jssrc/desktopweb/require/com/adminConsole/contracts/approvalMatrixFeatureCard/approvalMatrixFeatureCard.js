define(function() {
    return function(controller) {
        var approvalMatrixFeatureCard = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
            "clipBounds": true,
            "isMaster": true,
            "id": "approvalMatrixFeatureCard",
            "isVisible": true,
            "layoutType": kony.flex.FLOW_VERTICAL,
            "left": "0dp",
            "isModalContainer": false,
            "preShow": function(eventobject) {
                controller.AS_FlexContainer_b8b1c898df68458a8aaec294f8726a9f(eventobject);
            },
            "skin": "sknFlxFFFFFF100O",
            "top": "0dp",
            "width": "100%"
        }, controller.args[0], "approvalMatrixFeatureCard"), extendConfig({}, controller.args[1], "approvalMatrixFeatureCard"), extendConfig({}, controller.args[2], "approvalMatrixFeatureCard"));
        approvalMatrixFeatureCard.setDefaultUnit(kony.flex.DP);
        var flxCardTopContainer = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
            "clipBounds": true,
            "id": "flxCardTopContainer",
            "isVisible": true,
            "layoutType": kony.flex.FLOW_VERTICAL,
            "left": "0",
            "isModalContainer": false,
            "skin": "slFbox",
            "top": "0",
            "width": "100%"
        }, controller.args[0], "flxCardTopContainer"), extendConfig({}, controller.args[1], "flxCardTopContainer"), extendConfig({}, controller.args[2], "flxCardTopContainer"));
        flxCardTopContainer.setDefaultUnit(kony.flex.DP);
        var flxRow1 = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
            "clipBounds": true,
            "id": "flxRow1",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "left": "0dp",
            "isModalContainer": false,
            "skin": "slFbox",
            "top": "12px",
            "width": "100%"
        }, controller.args[0], "flxRow1"), extendConfig({}, controller.args[1], "flxRow1"), extendConfig({}, controller.args[2], "flxRow1"));
        flxRow1.setDefaultUnit(kony.flex.DP);
        var flxToggle = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "clipBounds": true,
            "height": "13dp",
            "id": "flxToggle",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "left": "15dp",
            "isModalContainer": false,
            "right": "0px",
            "skin": "sknCursor",
            "top": "3dp",
            "width": "13dp",
            "zIndex": 1
        }, controller.args[0], "flxToggle"), extendConfig({}, controller.args[1], "flxToggle"), extendConfig({
            "hoverSkin": "sknCursor"
        }, controller.args[2], "flxToggle"));
        flxToggle.setDefaultUnit(kony.flex.DP);
        var lblToggle = new kony.ui.Label(extendConfig({
            "centerX": "50%",
            "centerY": "50%",
            "height": "100%",
            "id": "lblToggle",
            "isVisible": true,
            "skin": "sknLblIIcoMoon485c7511px",
            "i18n_text": "kony.i18n.getLocalizedString(\"i18n.userwidgetmodel.fontIconBreadcrumbsDown\")",
            "width": "100%",
            "zIndex": 1
        }, controller.args[0], "lblToggle"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblToggle"), extendConfig({}, controller.args[2], "lblToggle"));
        flxToggle.add(lblToggle);
        var lblFeatureName = new kony.ui.Label(extendConfig({
            "id": "lblFeatureName",
            "isVisible": true,
            "left": "35px",
            "skin": "sknLbl192b45LatoReg16px",
            "text": "International Transfers",
            "top": "0",
            "width": "80%",
            "zIndex": 1
        }, controller.args[0], "lblFeatureName"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblFeatureName"), extendConfig({}, controller.args[2], "lblFeatureName"));
        var flxStatus = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
            "clipBounds": true,
            "id": "flxStatus",
            "isVisible": true,
            "layoutType": kony.flex.FLOW_HORIZONTAL,
            "reverseLayoutDirection": true,
            "isModalContainer": false,
            "right": "20dp",
            "skin": "slFbox",
            "top": "2dp",
            "width": "10%"
        }, controller.args[0], "flxStatus"), extendConfig({}, controller.args[1], "flxStatus"), extendConfig({}, controller.args[2], "flxStatus"));
        flxStatus.setDefaultUnit(kony.flex.DP);
        var lblStatusValue = new kony.ui.Label(extendConfig({
            "id": "lblStatusValue",
            "isVisible": true,
            "left": "5dp",
            "skin": "sknlblLato5bc06cBold14px",
            "i18n_text": "kony.i18n.getLocalizedString(\"i18n.secureimage.Active\")",
            "top": "0px",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, controller.args[0], "lblStatusValue"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblStatusValue"), extendConfig({}, controller.args[2], "lblStatusValue"));
        var lblIconStatus = new kony.ui.Label(extendConfig({
            "id": "lblIconStatus",
            "isVisible": true,
            "left": "0dp",
            "skin": "sknFontIconActivate",
            "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmAlertsManagement.lblViewAlertStatusKey\")",
            "top": 2,
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, controller.args[0], "lblIconStatus"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblIconStatus"), extendConfig({}, controller.args[2], "lblIconStatus"));
        flxStatus.add(lblStatusValue, lblIconStatus);
        flxRow1.add(flxToggle, lblFeatureName, flxStatus);
        var flxRow2 = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
            "clipBounds": true,
            "id": "flxRow2",
            "isVisible": true,
            "layoutType": kony.flex.FLOW_HORIZONTAL,
            "left": "0dp",
            "isModalContainer": false,
            "skin": "slFbox",
            "top": "10dp",
            "width": "100%"
        }, controller.args[0], "flxRow2"), extendConfig({}, controller.args[1], "flxRow2"), extendConfig({}, controller.args[2], "flxRow2"));
        flxRow2.setDefaultUnit(kony.flex.DP);
        var flxMonetaryCount = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
            "bottom": "13dp",
            "clipBounds": true,
            "id": "flxMonetaryCount",
            "isVisible": true,
            "layoutType": kony.flex.FLOW_HORIZONTAL,
            "left": "35dp",
            "isModalContainer": false,
            "skin": "slFbox",
            "top": "0dp",
            "width": "175dp"
        }, controller.args[0], "flxMonetaryCount"), extendConfig({}, controller.args[1], "flxMonetaryCount"), extendConfig({}, controller.args[2], "flxMonetaryCount"));
        flxMonetaryCount.setDefaultUnit(kony.flex.DP);
        var lblIconMonetary = new kony.ui.Label(extendConfig({
            "height": "15dp",
            "id": "lblIconMonetary",
            "isVisible": true,
            "left": "0dp",
            "skin": "sknFontIcon485C7518px",
            "text": "",
            "top": "0dp",
            "width": "18dp"
        }, controller.args[0], "lblIconMonetary"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblIconMonetary"), extendConfig({}, controller.args[2], "lblIconMonetary"));
        var lblMonetaryActions = new kony.ui.Label(extendConfig({
            "id": "lblMonetaryActions",
            "isVisible": true,
            "left": "5dp",
            "skin": "sknLbl485C75LatoRegular13Px",
            "text": "Monetary Actions:",
            "top": "0dp",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, controller.args[0], "lblMonetaryActions"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblMonetaryActions"), extendConfig({}, controller.args[2], "lblMonetaryActions"));
        var lblCountActions1 = new kony.ui.Label(extendConfig({
            "id": "lblCountActions1",
            "isVisible": true,
            "left": "3dp",
            "skin": "sknLbl192b45LatoBold13px",
            "text": "02",
            "top": "0dp",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, controller.args[0], "lblCountActions1"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblCountActions1"), extendConfig({}, controller.args[2], "lblCountActions1"));
        flxMonetaryCount.add(lblIconMonetary, lblMonetaryActions, lblCountActions1);
        var flxNonMonetaryCount = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
            "bottom": "13dp",
            "clipBounds": true,
            "id": "flxNonMonetaryCount",
            "isVisible": true,
            "layoutType": kony.flex.FLOW_HORIZONTAL,
            "left": "0dp",
            "isModalContainer": false,
            "skin": "slFbox",
            "top": "0dp",
            "width": "200dp"
        }, controller.args[0], "flxNonMonetaryCount"), extendConfig({}, controller.args[1], "flxNonMonetaryCount"), extendConfig({}, controller.args[2], "flxNonMonetaryCount"));
        flxNonMonetaryCount.setDefaultUnit(kony.flex.DP);
        var lblIconNonMonetary = new kony.ui.Label(extendConfig({
            "height": "15dp",
            "id": "lblIconNonMonetary",
            "isVisible": true,
            "left": "0dp",
            "skin": "sknFontIcon485C7518px",
            "text": "",
            "top": "0dp",
            "width": "18dp"
        }, controller.args[0], "lblIconNonMonetary"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblIconNonMonetary"), extendConfig({}, controller.args[2], "lblIconNonMonetary"));
        var lblNonMonetaryActions = new kony.ui.Label(extendConfig({
            "id": "lblNonMonetaryActions",
            "isVisible": true,
            "left": "5dp",
            "skin": "sknLbl485C75LatoRegular13Px",
            "text": "Non Monetary Actions:",
            "top": "0dp",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, controller.args[0], "lblNonMonetaryActions"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblNonMonetaryActions"), extendConfig({}, controller.args[2], "lblNonMonetaryActions"));
        var lblCountActions2 = new kony.ui.Label(extendConfig({
            "id": "lblCountActions2",
            "isVisible": true,
            "left": "10dp",
            "skin": "sknLbl192b45LatoBold13px",
            "text": "2",
            "top": "0dp",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, controller.args[0], "lblCountActions2"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblCountActions2"), extendConfig({}, controller.args[2], "lblCountActions2"));
        flxNonMonetaryCount.add(lblIconNonMonetary, lblNonMonetaryActions, lblCountActions2);
        flxRow2.add(flxMonetaryCount, flxNonMonetaryCount);
        var flxTopSeperatorLine = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "clipBounds": true,
            "height": "1dp",
            "id": "flxTopSeperatorLine",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "left": "0",
            "isModalContainer": false,
            "skin": "sknFlxD7D9E0Separator",
            "top": "0",
            "width": "100%"
        }, controller.args[0], "flxTopSeperatorLine"), extendConfig({}, controller.args[1], "flxTopSeperatorLine"), extendConfig({}, controller.args[2], "flxTopSeperatorLine"));
        flxTopSeperatorLine.setDefaultUnit(kony.flex.DP);
        flxTopSeperatorLine.add();
        flxCardTopContainer.add(flxRow1, flxRow2, flxTopSeperatorLine);
        var flxCardBottomContainer = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
            "clipBounds": true,
            "id": "flxCardBottomContainer",
            "isVisible": false,
            "layoutType": kony.flex.FREE_FORM,
            "left": "0",
            "isModalContainer": false,
            "skin": "slFbox",
            "top": "0",
            "width": "100%"
        }, controller.args[0], "flxCardBottomContainer"), extendConfig({}, controller.args[1], "flxCardBottomContainer"), extendConfig({}, controller.args[2], "flxCardBottomContainer"));
        flxCardBottomContainer.setDefaultUnit(kony.flex.DP);
        var segFeatureActions = new kony.ui.SegmentedUI2(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
            "bottom": "0dp",
            "data": [
                [{
                        "lblActionName": "Monetary Actions",
                        "lblApprovalRequiredHeader": "kony.i18n.getLocalizedString(\"i18n.frmComapnies.APPROVAL_REQUIRED\")",
                        "lblApproversHeader": "kony.i18n.getLocalizedString(\"i18n.frmComapnies.APPROVERS\")",
                        "lblFASeperator1": "kony.i18n.getLocalizedString(\"i18n.frmCSR.lblSeperator\")",
                        "lblFASeperator2": "kony.i18n.getLocalizedString(\"i18n.frmCSR.lblSeperator\")",
                        "lblFASeperatorTop": "kony.i18n.getLocalizedString(\"i18n.frmCSR.lblSeperator\")",
                        "lblIconAction": "",
                        "lblRangeHeader": "kony.i18n.getLocalizedString(\"i18n.frmComapnies.RANGE\")",
                        "lbxTransactionType": {
                            "masterData": [
                                ["lb1", "Placeholder One"],
                                ["lb2", "Placeholder Two"],
                                ["lb3", "Placeholder Three"]
                            ],
                            "selectedKey": null,
                            "selectedKeys": null
                        }
                    },
                    [{
                        "lblApprovalNA": "N/A",
                        "lblApprovalValue1": "Upto $100",
                        "lblApprovalValue2": "Yes",
                        "lblApprovalView": "kony.i18n.getLocalizedString(\"i18n.permission.View\")",
                        "lblIconAction": "",
                        "lblLine": "kony.i18n.getLocalizedString(\"i18n.frmCSR.lblSeperator\")"
                    }, {
                        "lblApprovalNA": "N/A",
                        "lblApprovalValue1": "Upto $100",
                        "lblApprovalValue2": "Yes",
                        "lblApprovalView": "kony.i18n.getLocalizedString(\"i18n.permission.View\")",
                        "lblIconAction": "",
                        "lblLine": "kony.i18n.getLocalizedString(\"i18n.frmCSR.lblSeperator\")"
                    }, {
                        "lblApprovalNA": "N/A",
                        "lblApprovalValue1": "Upto $100",
                        "lblApprovalValue2": "Yes",
                        "lblApprovalView": "kony.i18n.getLocalizedString(\"i18n.permission.View\")",
                        "lblIconAction": "",
                        "lblLine": "kony.i18n.getLocalizedString(\"i18n.frmCSR.lblSeperator\")"
                    }]
                ],
                [{
                        "lblActionName": "Monetary Actions",
                        "lblApprovalRequiredHeader": "kony.i18n.getLocalizedString(\"i18n.frmComapnies.APPROVAL_REQUIRED\")",
                        "lblApproversHeader": "kony.i18n.getLocalizedString(\"i18n.frmComapnies.APPROVERS\")",
                        "lblFASeperator1": "kony.i18n.getLocalizedString(\"i18n.frmCSR.lblSeperator\")",
                        "lblFASeperator2": "kony.i18n.getLocalizedString(\"i18n.frmCSR.lblSeperator\")",
                        "lblFASeperatorTop": "kony.i18n.getLocalizedString(\"i18n.frmCSR.lblSeperator\")",
                        "lblIconAction": "",
                        "lblRangeHeader": "kony.i18n.getLocalizedString(\"i18n.frmComapnies.RANGE\")",
                        "lbxTransactionType": {
                            "masterData": [
                                ["lb1", "Placeholder One"],
                                ["lb2", "Placeholder Two"],
                                ["lb3", "Placeholder Three"]
                            ],
                            "selectedKey": null,
                            "selectedKeys": null
                        }
                    },
                    [{
                        "lblApprovalNA": "N/A",
                        "lblApprovalValue1": "Upto $100",
                        "lblApprovalValue2": "Yes",
                        "lblApprovalView": "kony.i18n.getLocalizedString(\"i18n.permission.View\")",
                        "lblIconAction": "",
                        "lblLine": "kony.i18n.getLocalizedString(\"i18n.frmCSR.lblSeperator\")"
                    }, {
                        "lblApprovalNA": "N/A",
                        "lblApprovalValue1": "Upto $100",
                        "lblApprovalValue2": "Yes",
                        "lblApprovalView": "kony.i18n.getLocalizedString(\"i18n.permission.View\")",
                        "lblIconAction": "",
                        "lblLine": "kony.i18n.getLocalizedString(\"i18n.frmCSR.lblSeperator\")"
                    }]
                ]
            ],
            "groupCells": false,
            "id": "segFeatureActions",
            "isVisible": true,
            "left": "0",
            "needPageIndicator": true,
            "pageOffDotImage": "pageoffdot.png",
            "pageOnDotImage": "pageondot.png",
            "retainSelection": false,
            "rowFocusSkin": "seg2Focus",
            "rowSkin": "seg2Normal",
            "rowTemplate": "flxApprovalMatrixRangeRow",
            "sectionHeaderSkin": "sliPhoneSegmentHeader",
            "sectionHeaderTemplate": "flxApprovalMatrixActionHeader",
            "selectionBehavior": constants.SEGUI_DEFAULT_BEHAVIOR,
            "separatorColor": "aaaaaa00",
            "separatorRequired": false,
            "separatorThickness": 1,
            "showScrollbars": false,
            "top": "0dp",
            "viewType": constants.SEGUI_VIEW_TYPE_TABLEVIEW,
            "widgetDataMap": {
                "flxActionName": "flxActionName",
                "flxApprovalEdit": "flxApprovalEdit",
                "flxApprovalMatrixActionHeader": "flxApprovalMatrixActionHeader",
                "flxApprovalMatrixRangeRow": "flxApprovalMatrixRangeRow",
                "flxApprovalRangeCont": "flxApprovalRangeCont",
                "flxViewApprovalsHeader": "flxViewApprovalsHeader",
                "lblActionName": "lblActionName",
                "lblApprovalNA": "lblApprovalNA",
                "lblApprovalRequiredHeader": "lblApprovalRequiredHeader",
                "lblApprovalValue1": "lblApprovalValue1",
                "lblApprovalValue2": "lblApprovalValue2",
                "lblApprovalView": "lblApprovalView",
                "lblApproversHeader": "lblApproversHeader",
                "lblFASeperator1": "lblFASeperator1",
                "lblFASeperator2": "lblFASeperator2",
                "lblFASeperatorTop": "lblFASeperatorTop",
                "lblIconAction": "lblIconAction",
                "lblLine": "lblLine",
                "lblRangeHeader": "lblRangeHeader",
                "lbxTransactionType": "lbxTransactionType"
            },
            "width": "100%"
        }, controller.args[0], "segFeatureActions"), extendConfig({
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "segFeatureActions"), extendConfig({}, controller.args[2], "segFeatureActions"));
        var flxNoFilterResults = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "clipBounds": true,
            "height": "75px",
            "id": "flxNoFilterResults",
            "isVisible": false,
            "layoutType": kony.flex.FLOW_HORIZONTAL,
            "left": "0dp",
            "isModalContainer": false,
            "skin": "slFbox",
            "top": "0dp",
            "width": "100%",
            "zIndex": 1
        }, controller.args[0], "flxNoFilterResults"), extendConfig({}, controller.args[1], "flxNoFilterResults"), extendConfig({}, controller.args[2], "flxNoFilterResults"));
        flxNoFilterResults.setDefaultUnit(kony.flex.DP);
        var lblNoFilterResults = new kony.ui.Label(extendConfig({
            "centerX": "50%",
            "centerY": "50%",
            "id": "lblNoFilterResults",
            "isVisible": true,
            "skin": "sknLblLato84939E12px",
            "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmUsersController.No_results_found\")",
            "width": "80%",
            "zIndex": 1
        }, controller.args[0], "lblNoFilterResults"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_CENTER,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblNoFilterResults"), extendConfig({}, controller.args[2], "lblNoFilterResults"));
        flxNoFilterResults.add(lblNoFilterResults);
        var flxBottomSeperator = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "bottom": "0dp",
            "clipBounds": true,
            "height": "1dp",
            "id": "flxBottomSeperator",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "left": "0dp",
            "isModalContainer": false,
            "skin": "sknFlxD7D9E0Separator",
            "width": "100%",
            "zIndex": 5
        }, controller.args[0], "flxBottomSeperator"), extendConfig({}, controller.args[1], "flxBottomSeperator"), extendConfig({}, controller.args[2], "flxBottomSeperator"));
        flxBottomSeperator.setDefaultUnit(kony.flex.DP);
        flxBottomSeperator.add();
        flxCardBottomContainer.add(segFeatureActions, flxNoFilterResults, flxBottomSeperator);
        approvalMatrixFeatureCard.add(flxCardTopContainer, flxCardBottomContainer);
        return approvalMatrixFeatureCard;
    }
})