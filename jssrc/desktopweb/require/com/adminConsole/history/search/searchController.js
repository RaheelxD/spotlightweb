define("com/adminConsole/history/search/usersearchController", function() {
    return {};
});
define("com/adminConsole/history/search/searchControllerActions", {
    /*
        This is an auto generated file and any modifications to it may result in corruption of the action sequence.
    */
    AS_TextField_8869ed77fd9d4cc3bb4d7faea4e0afdb: function AS_TextField_8869ed77fd9d4cc3bb4d7faea4e0afdb(eventobject, x, y) {
        var self = this;
    }
});
define("com/adminConsole/history/search/searchController", ["com/adminConsole/history/search/usersearchController", "com/adminConsole/history/search/searchControllerActions"], function() {
    var controller = require("com/adminConsole/history/search/usersearchController");
    var actions = require("com/adminConsole/history/search/searchControllerActions");
    for (var key in actions) {
        controller[key] = actions[key];
    }
    return controller;
});
