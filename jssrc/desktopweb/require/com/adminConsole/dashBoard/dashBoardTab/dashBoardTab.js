define(function() {
    return function(controller) {
        var dashBoardTab = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "clipBounds": true,
            "isMaster": true,
            "height": "100px",
            "id": "dashBoardTab",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "left": "8.30%",
            "isModalContainer": false,
            "skin": "sknLblTabShadow",
            "top": "57px",
            "width": "37.60%",
            "zIndex": 1
        }, controller.args[0], "dashBoardTab"), extendConfig({}, controller.args[1], "dashBoardTab"), extendConfig({}, controller.args[2], "dashBoardTab"));
        dashBoardTab.setDefaultUnit(kony.flex.DP);
        var flxContainerDashboardTab = new kony.ui.FlexScrollContainer(extendConfig({
            "allowHorizontalBounce": false,
            "allowVerticalBounce": true,
            "bounces": true,
            "clipBounds": true,
            "enableScrolling": true,
            "height": "100%",
            "horizontalScrollIndicator": true,
            "id": "flxContainerDashboardTab",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "left": "0%",
            "pagingEnabled": false,
            "scrollDirection": kony.flex.SCROLL_VERTICAL,
            "skin": "sknFlxHandCursor",
            "top": "0%",
            "verticalScrollIndicator": true,
            "width": "100%",
            "zIndex": 1
        }, controller.args[0], "flxContainerDashboardTab"), extendConfig({}, controller.args[1], "flxContainerDashboardTab"), extendConfig({}, controller.args[2], "flxContainerDashboardTab"));
        flxContainerDashboardTab.setDefaultUnit(kony.flex.DP);
        var lblImage = new kony.ui.Label(extendConfig({
            "id": "lblImage",
            "isVisible": true,
            "right": "8%",
            "skin": "sknLblIcomoon28px485c75",
            "i18n_text": "kony.i18n.getLocalizedString(\"i18n.userwidgetmodel.lblImage\")",
            "top": "12%",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, controller.args[0], "lblImage"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_CENTER,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblImage"), extendConfig({}, controller.args[2], "lblImage"));
        var lblCategory = new kony.ui.Label(extendConfig({
            "bottom": "20%",
            "id": "lblCategory",
            "isVisible": true,
            "left": "11%",
            "skin": "sknLblLatoMed12px485c75",
            "i18n_text": "kony.i18n.getLocalizedString(\"i18n.userwidgetmodel.lblCategory\")",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, controller.args[0], "lblCategory"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_CENTER,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblCategory"), extendConfig({}, controller.args[2], "lblCategory"));
        var lblCount = new kony.ui.Label(extendConfig({
            "bottom": "38%",
            "id": "lblCount",
            "isVisible": true,
            "left": "11%",
            "skin": "sknLblLatoMed26px192b45",
            "i18n_text": "kony.i18n.getLocalizedString(\"i18n.userwidgetmodel.lblCount\")",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, controller.args[0], "lblCount"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_CENTER,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblCount"), extendConfig({}, controller.args[2], "lblCount"));
        flxContainerDashboardTab.add(lblImage, lblCategory, lblCount);
        var flxClick = new kony.ui.FlexContainer(extendConfig({
            "clipBounds": true,
            "height": "100%",
            "id": "flxClick",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "left": "0%",
            "isModalContainer": false,
            "skin": "sknFlxHandCursor",
            "top": "0%",
            "width": "100%",
            "zIndex": 1
        }, controller.args[0], "flxClick"), extendConfig({}, controller.args[1], "flxClick"), extendConfig({}, controller.args[2], "flxClick"));
        flxClick.setDefaultUnit(kony.flex.DP);
        flxClick.add();
        dashBoardTab.add(flxContainerDashboardTab, flxClick);
        return dashBoardTab;
    }
})