define("com/adminConsole/reports/dynamicParameters/userdynamicParametersController", function() {
    return {};
});
define("com/adminConsole/reports/dynamicParameters/dynamicParametersControllerActions", {
    /*
          This is an auto generated file and any modifications to it may result in corruption of the action sequence.
        */
});
define("com/adminConsole/reports/dynamicParameters/dynamicParametersController", ["com/adminConsole/reports/dynamicParameters/userdynamicParametersController", "com/adminConsole/reports/dynamicParameters/dynamicParametersControllerActions"], function() {
    var controller = require("com/adminConsole/reports/dynamicParameters/userdynamicParametersController");
    var actions = require("com/adminConsole/reports/dynamicParameters/dynamicParametersControllerActions");
    for (var key in actions) {
        controller[key] = actions[key];
    }
    return controller;
});
