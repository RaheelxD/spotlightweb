define(function() {
    return function(controller) {
        var dynamicParameters = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "clipBounds": false,
            "isMaster": true,
            "height": "100%",
            "id": "dynamicParameters",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "left": "0dp",
            "isModalContainer": false,
            "skin": "sknBackgroundFFFFFF",
            "top": "0dp",
            "width": "100%"
        }, controller.args[0], "dynamicParameters"), extendConfig({}, controller.args[1], "dynamicParameters"), extendConfig({}, controller.args[2], "dynamicParameters"));
        dynamicParameters.setDefaultUnit(kony.flex.DP);
        var flxHeading = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
            "clipBounds": false,
            "id": "flxHeading",
            "isVisible": true,
            "layoutType": kony.flex.FLOW_HORIZONTAL,
            "left": "0dp",
            "isModalContainer": false,
            "right": "15dp",
            "skin": "slFbox",
            "top": "0dp"
        }, controller.args[0], "flxHeading"), extendConfig({}, controller.args[1], "flxHeading"), extendConfig({}, controller.args[2], "flxHeading"));
        flxHeading.setDefaultUnit(kony.flex.DP);
        var lblHeadingText = new kony.ui.Label(extendConfig({
            "id": "lblHeadingText",
            "isVisible": true,
            "left": "0dp",
            "skin": "sknlblLato485c7514px",
            "text": "Label",
            "top": "0dp",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, controller.args[0], "lblHeadingText"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblHeadingText"), extendConfig({}, controller.args[2], "lblHeadingText"));
        var lblOptional = new kony.ui.Label(extendConfig({
            "id": "lblOptional",
            "isVisible": false,
            "left": "5dp",
            "skin": "sknLblLato84939E12px",
            "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmPermissions.lblOptional\")",
            "top": "0dp",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, controller.args[0], "lblOptional"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblOptional"), extendConfig({}, controller.args[2], "lblOptional"));
        flxHeading.add(lblHeadingText, lblOptional);
        var flxInputComponents = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "clipBounds": false,
            "height": "100%",
            "id": "flxInputComponents",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "left": "0dp",
            "isModalContainer": false,
            "skin": "slFbox",
            "top": "0dp",
            "width": "100%"
        }, controller.args[0], "flxInputComponents"), extendConfig({}, controller.args[1], "flxInputComponents"), extendConfig({}, controller.args[2], "flxInputComponents"));
        flxInputComponents.setDefaultUnit(kony.flex.DP);
        var flxDateTimePicker = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "clipBounds": false,
            "height": "40dp",
            "id": "flxDateTimePicker",
            "isVisible": false,
            "layoutType": kony.flex.FLOW_HORIZONTAL,
            "left": "0dp",
            "isModalContainer": false,
            "skin": "slFbox",
            "top": "25dp",
            "width": "100%"
        }, controller.args[0], "flxDateTimePicker"), extendConfig({}, controller.args[1], "flxDateTimePicker"), extendConfig({}, controller.args[2], "flxDateTimePicker"));
        flxDateTimePicker.setDefaultUnit(kony.flex.DP);
        var flxCalendar = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "clipBounds": true,
            "height": "100%",
            "id": "flxCalendar",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "left": "0dp",
            "isModalContainer": false,
            "skin": "slFbox",
            "top": "0dp",
            "width": "50%"
        }, controller.args[0], "flxCalendar"), extendConfig({}, controller.args[1], "flxCalendar"), extendConfig({}, controller.args[2], "flxCalendar"));
        flxCalendar.setDefaultUnit(kony.flex.DP);
        var CalDate = new kony.ui.Calendar(extendConfig({
            "calendarIcon": "calicon.png",
            "dateComponents": [null, null, null],
            "dateFormat": "yyyy/MM/dd",
            "height": "40dp",
            "hour": 0,
            "id": "CalDate",
            "isVisible": true,
            "left": "0",
            "minutes": 0,
            "right": "15dp",
            "seconds": 0,
            "skin": "sknCald7d9e0font485C75bor3px",
            "top": "0dp",
            "viewType": constants.CALENDAR_VIEW_TYPE_DEFAULT
        }, controller.args[0], "CalDate"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [2, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "CalDate"), extendConfig({
            "noOfMonths": 1
        }, controller.args[2], "CalDate"));
        flxCalendar.add(CalDate);
        var flxTime = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "clipBounds": true,
            "height": "100%",
            "id": "flxTime",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "left": "0dp",
            "isModalContainer": false,
            "skin": "slFbox",
            "top": "0dp",
            "width": "50%"
        }, controller.args[0], "flxTime"), extendConfig({}, controller.args[1], "flxTime"), extendConfig({}, controller.args[2], "flxTime"));
        flxTime.setDefaultUnit(kony.flex.DP);
        var flxOuterBorder = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "clipBounds": true,
            "height": "40px",
            "id": "flxOuterBorder",
            "isVisible": true,
            "layoutType": kony.flex.FLOW_HORIZONTAL,
            "left": "0dp",
            "isModalContainer": false,
            "right": "20dp",
            "skin": "sknFlxbgFFFFFFbdrD7D9E0rd3px",
            "top": "0dp",
            "zIndex": 1
        }, controller.args[0], "flxOuterBorder"), extendConfig({}, controller.args[1], "flxOuterBorder"), extendConfig({}, controller.args[2], "flxOuterBorder"));
        flxOuterBorder.setDefaultUnit(kony.flex.DP);
        var lstbxHours = new kony.ui.ListBox(extendConfig({
            "height": "40px",
            "id": "lstbxHours",
            "isVisible": true,
            "left": "1%",
            "masterData": [
                ["HH", "HH"],
                ["00", "00"],
                ["01", "01"],
                ["02", "02"],
                ["03", "03"],
                ["04", "04"],
                ["05", "05"],
                ["06", "06"],
                ["07", "07"],
                ["08", "08"],
                ["09", "09"],
                ["10", "10"],
                ["11", "11"],
                ["12", "12"],
                ["13", "13"],
                ["14", "14"],
                ["15", "15"],
                ["16", "16"],
                ["17", "17"],
                ["18", "18"],
                ["19", "19"],
                ["20", "20"],
                ["21", "21"],
                ["22", "22"],
                ["23", "23"]
            ],
            "selectedKey": "HH",
            "skin": "CopysknlbxBorderc0f235394b4a5547",
            "top": "0dp",
            "width": "27%",
            "zIndex": 2
        }, controller.args[0], "lstbxHours"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [3, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lstbxHours"), extendConfig({
            "multiSelect": false
        }, controller.args[2], "lstbxHours"));
        var lblColen = new kony.ui.Label(extendConfig({
            "id": "lblColen",
            "isVisible": true,
            "left": "4%",
            "skin": "CopyslFbox0b6ab2ac21ea948",
            "i18n_text": "kony.i18n.getLocalizedString(\"i18n.userwidgetmodel.lblColen\")",
            "top": "10px",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, controller.args[0], "lblColen"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblColen"), extendConfig({}, controller.args[2], "lblColen"));
        var lstbxMinutes = new kony.ui.ListBox(extendConfig({
            "height": "40px",
            "id": "lstbxMinutes",
            "isVisible": true,
            "left": "2%",
            "masterData": [
                ["MM", "MM"],
                ["0", "00"],
                ["1", "01"],
                ["2", "02"],
                ["3", "03"],
                ["4", "04"],
                ["5", "05"],
                ["6", "06"],
                ["7", "07"],
                ["8", "08"],
                ["9", "09"],
                ["10", "10"],
                ["11", "11"],
                ["12", "12"],
                ["13", "13"],
                ["14", "14"],
                ["15", "15"],
                ["16", "16"],
                ["17", "17"],
                ["18", "18"],
                ["19", "19"],
                ["20", "20"],
                ["21", "21"],
                ["22", "22"],
                ["23", "23"],
                ["24", "24"],
                ["25", "25"],
                ["26", "26"],
                ["27", "27"],
                ["28", "28"],
                ["29", "29"],
                ["30", "30"],
                ["31", "31"],
                ["32", "32"],
                ["33", "33"],
                ["34", "34"],
                ["35", "35"],
                ["36", "36"],
                ["37", "37"],
                ["38", "38"],
                ["39", "39"],
                ["40", "40"],
                ["41", "41"],
                ["42", "42"],
                ["43", "43"],
                ["44", "44"],
                ["45", "45"],
                ["46", "46"],
                ["47", "47"],
                ["48", "48"],
                ["49", "49"],
                ["50", "50"],
                ["51", "51"],
                ["52", "52"],
                ["53", "53"],
                ["54", "54"],
                ["55", "55"],
                ["56", "56"],
                ["57", "57"],
                ["58", "58"],
                ["59", "59"]
            ],
            "right": "0dp",
            "selectedKey": "MM",
            "skin": "CopysknlbxBorderc0f235394b4a5547",
            "top": "0dp",
            "width": "28%",
            "zIndex": 2
        }, controller.args[0], "lstbxMinutes"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [3, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lstbxMinutes"), extendConfig({
            "multiSelect": false
        }, controller.args[2], "lstbxMinutes"));
        var lblColen2 = new kony.ui.Label(extendConfig({
            "id": "lblColen2",
            "isVisible": true,
            "left": "4%",
            "skin": "CopyslFbox0b6ab2ac21ea948",
            "i18n_text": "kony.i18n.getLocalizedString(\"i18n.userwidgetmodel.lblColen\")",
            "top": "10px",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, controller.args[0], "lblColen2"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblColen2"), extendConfig({}, controller.args[2], "lblColen2"));
        var lstbxSeconds = new kony.ui.ListBox(extendConfig({
            "height": "40px",
            "id": "lstbxSeconds",
            "isVisible": true,
            "left": "2%",
            "masterData": [
                ["SS", "SS"],
                ["0", "00"],
                ["1", "01"],
                ["2", "02"],
                ["3", "03"],
                ["4", "04"],
                ["5", "05"],
                ["6", "06"],
                ["7", "07"],
                ["8", "08"],
                ["9", "09"],
                ["10", "10"],
                ["11", "11"],
                ["12", "12"],
                ["13", "13"],
                ["14", "14"],
                ["15", "15"],
                ["16", "16"],
                ["17", "17"],
                ["18", "18"],
                ["19", "19"],
                ["20", "20"],
                ["21", "21"],
                ["22", "22"],
                ["23", "23"],
                ["24", "24"],
                ["25", "25"],
                ["26", "26"],
                ["27", "27"],
                ["28", "28"],
                ["29", "29"],
                ["30", "30"],
                ["31", "31"],
                ["32", "32"],
                ["33", "33"],
                ["34", "34"],
                ["35", "35"],
                ["36", "36"],
                ["37", "37"],
                ["38", "38"],
                ["39", "39"],
                ["40", "40"],
                ["41", "41"],
                ["42", "42"],
                ["43", "43"],
                ["44", "44"],
                ["45", "45"],
                ["46", "46"],
                ["47", "47"],
                ["48", "48"],
                ["49", "49"],
                ["50", "50"],
                ["51", "51"],
                ["52", "52"],
                ["53", "53"],
                ["54", "54"],
                ["55", "55"],
                ["56", "56"],
                ["57", "57"],
                ["58", "58"],
                ["59", "59"]
            ],
            "selectedKey": "SS",
            "skin": "CopysknlbxBorderc0f235394b4a5547",
            "top": "0dp",
            "width": "26%",
            "zIndex": 2
        }, controller.args[0], "lstbxSeconds"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [3, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lstbxSeconds"), extendConfig({
            "multiSelect": false
        }, controller.args[2], "lstbxSeconds"));
        flxOuterBorder.add(lstbxHours, lblColen, lstbxMinutes, lblColen2, lstbxSeconds);
        flxTime.add(flxOuterBorder);
        flxDateTimePicker.add(flxCalendar, flxTime);
        var flxDatePicker = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "clipBounds": false,
            "height": "40dp",
            "id": "flxDatePicker",
            "isVisible": false,
            "layoutType": kony.flex.FREE_FORM,
            "left": "0dp",
            "isModalContainer": false,
            "skin": "slFbox",
            "top": "25dp",
            "width": "100%"
        }, controller.args[0], "flxDatePicker"), extendConfig({}, controller.args[1], "flxDatePicker"), extendConfig({}, controller.args[2], "flxDatePicker"));
        flxDatePicker.setDefaultUnit(kony.flex.DP);
        var flxSingleDate = new kony.ui.Calendar(extendConfig({
            "calendarIcon": "calicon.png",
            "dateComponents": [null, null, null],
            "dateFormat": "yyyy/MM/dd",
            "height": "40dp",
            "hour": 0,
            "id": "flxSingleDate",
            "isVisible": true,
            "left": "0",
            "minutes": 0,
            "right": "20dp",
            "seconds": 0,
            "skin": "sknCald7d9e0font485C75bor3px",
            "top": "0dp",
            "viewType": constants.CALENDAR_VIEW_TYPE_DEFAULT
        }, controller.args[0], "flxSingleDate"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [3, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "flxSingleDate"), extendConfig({
            "noOfMonths": 1
        }, controller.args[2], "flxSingleDate"));
        flxDatePicker.add(flxSingleDate);
        var flxSingleList = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "clipBounds": false,
            "height": "40dp",
            "id": "flxSingleList",
            "isVisible": false,
            "layoutType": kony.flex.FREE_FORM,
            "left": "0dp",
            "isModalContainer": false,
            "skin": "slFbox",
            "top": "20dp",
            "width": "100%"
        }, controller.args[0], "flxSingleList"), extendConfig({}, controller.args[1], "flxSingleList"), extendConfig({}, controller.args[2], "flxSingleList"));
        flxSingleList.setDefaultUnit(kony.flex.DP);
        var lbxSngleListBox = new kony.ui.ListBox(extendConfig({
            "focusSkin": "defListBoxFocus",
            "height": "40dp",
            "id": "lbxSngleListBox",
            "isVisible": true,
            "left": "0",
            "masterData": [
                ["lb1", "Placeholder One"],
                ["lb2", "Placeholder Two"],
                ["lb3", "Placeholder Three"]
            ],
            "right": "20dp",
            "skin": "sknlbxConfigurationBundles",
            "top": "0"
        }, controller.args[0], "lbxSngleListBox"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [3, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lbxSngleListBox"), extendConfig({
            "multiSelect": false
        }, controller.args[2], "lbxSngleListBox"));
        flxSingleList.add(lbxSngleListBox);
        var flxMultiListBox = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "clipBounds": false,
            "height": "40dp",
            "id": "flxMultiListBox",
            "isVisible": false,
            "layoutType": kony.flex.FREE_FORM,
            "left": "0dp",
            "isModalContainer": false,
            "skin": "slFbox",
            "top": "25dp",
            "width": "100%"
        }, controller.args[0], "flxMultiListBox"), extendConfig({}, controller.args[1], "flxMultiListBox"), extendConfig({}, controller.args[2], "flxMultiListBox"));
        flxMultiListBox.setDefaultUnit(kony.flex.DP);
        var customListbox = new com.adminConsole.alerts.customListbox(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
            "id": "customListbox",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "left": "0dp",
            "masterType": constants.MASTER_TYPE_DEFAULT,
            "isModalContainer": false,
            "right": "20dp",
            "skin": "slFbox",
            "top": "0dp",
            "overrides": {
                "customListbox": {
                    "right": "20dp",
                    "width": "viz.val_cleared"
                },
                "flxSegmentList": {
                    "isVisible": false
                },
                "flxSelectedText": {
                    "clipBounds": false
                },
                "imgCheckBox": {
                    "src": "checkboxnormal.png"
                }
            }
        }, controller.args[0], "customListbox"), extendConfig({
            "overrides": {}
        }, controller.args[1], "customListbox"), extendConfig({
            "overrides": {}
        }, controller.args[2], "customListbox"));
        flxMultiListBox.add(customListbox);
        var flxSingleText = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "clipBounds": false,
            "height": "40dp",
            "id": "flxSingleText",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "left": "0dp",
            "isModalContainer": false,
            "skin": "slFbox",
            "top": "25dp",
            "width": "100%"
        }, controller.args[0], "flxSingleText"), extendConfig({}, controller.args[1], "flxSingleText"), extendConfig({}, controller.args[2], "flxSingleText"));
        flxSingleText.setDefaultUnit(kony.flex.DP);
        var flxEnterValue = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "clipBounds": false,
            "height": "40dp",
            "id": "flxEnterValue",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "left": "0dp",
            "isModalContainer": false,
            "right": "20dp",
            "skin": "sknflxEnterValueNormal",
            "top": "0dp",
            "zIndex": 2
        }, controller.args[0], "flxEnterValue"), extendConfig({}, controller.args[1], "flxEnterValue"), extendConfig({}, controller.args[2], "flxEnterValue"));
        flxEnterValue.setDefaultUnit(kony.flex.DP);
        var tbxEnterValue = new kony.ui.TextBox2(extendConfig({
            "autoCapitalize": constants.TEXTBOX_AUTO_CAPITALIZE_NONE,
            "height": "36dp",
            "id": "tbxEnterValue",
            "isVisible": true,
            "keyBoardStyle": constants.TEXTBOX_KEY_BOARD_STYLE_DEFAULT,
            "left": "2dp",
            "placeholder": "Placeholder",
            "right": "2dp",
            "secureTextEntry": false,
            "skin": "sknTbx485c75LatoReg13pxNoBor",
            "textInputMode": constants.TEXTBOX_INPUT_MODE_ANY,
            "top": "1dp",
            "zIndex": 1
        }, controller.args[0], "tbxEnterValue"), extendConfig({
            "containerHeightMode": constants.TEXTBOX_FONT_METRICS_DRIVEN_HEIGHT,
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [3, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "tbxEnterValue"), extendConfig({
            "autoCorrect": false,
            "placeholderSkin": "defTextBoxPlaceholder"
        }, controller.args[2], "tbxEnterValue"));
        var flxBtnCheck = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "clipBounds": false,
            "height": "40dp",
            "id": "flxBtnCheck",
            "isVisible": false,
            "layoutType": kony.flex.FREE_FORM,
            "isModalContainer": false,
            "right": "0dp",
            "skin": "slFbox",
            "top": "0dp",
            "width": "75dp"
        }, controller.args[0], "flxBtnCheck"), extendConfig({}, controller.args[1], "flxBtnCheck"), extendConfig({}, controller.args[2], "flxBtnCheck"));
        flxBtnCheck.setDefaultUnit(kony.flex.DP);
        var btnCheck = new kony.ui.Button(extendConfig({
            "height": "40dp",
            "id": "btnCheck",
            "isVisible": true,
            "left": "-5dp",
            "skin": "sknbtnEnterValueCheck",
            "text": "Check",
            "top": "0dp",
            "width": "80dp",
            "zIndex": 1
        }, controller.args[0], "btnCheck"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_CENTER,
            "displayText": true,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "btnCheck"), extendConfig({}, controller.args[2], "btnCheck"));
        var flxSepartor = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "clipBounds": true,
            "height": "40dp",
            "id": "flxSepartor",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "left": "0dp",
            "isModalContainer": false,
            "skin": "sknSeparatorCsr",
            "top": "0dp",
            "width": "1dp",
            "zIndex": 2
        }, controller.args[0], "flxSepartor"), extendConfig({}, controller.args[1], "flxSepartor"), extendConfig({}, controller.args[2], "flxSepartor"));
        flxSepartor.setDefaultUnit(kony.flex.DP);
        flxSepartor.add();
        flxBtnCheck.add(btnCheck, flxSepartor);
        flxEnterValue.add(tbxEnterValue, flxBtnCheck);
        flxSingleText.add(flxEnterValue);
        flxInputComponents.add(flxDateTimePicker, flxDatePicker, flxSingleList, flxMultiListBox, flxSingleText);
        var flxInlineError = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
            "clipBounds": false,
            "id": "flxInlineError",
            "isVisible": false,
            "layoutType": kony.flex.FLOW_HORIZONTAL,
            "left": "0dp",
            "isModalContainer": false,
            "right": "15dp",
            "skin": "slFbox",
            "top": "70dp"
        }, controller.args[0], "flxInlineError"), extendConfig({}, controller.args[1], "flxInlineError"), extendConfig({}, controller.args[2], "flxInlineError"));
        flxInlineError.setDefaultUnit(kony.flex.DP);
        var lblErrorIcon = new kony.ui.Label(extendConfig({
            "id": "lblErrorIcon",
            "isVisible": true,
            "left": "0dp",
            "skin": "sknFontIconError",
            "text": "",
            "top": "1dp",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, controller.args[0], "lblErrorIcon"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblErrorIcon"), extendConfig({}, controller.args[2], "lblErrorIcon"));
        var lblErrorText = new kony.ui.Label(extendConfig({
            "id": "lblErrorText",
            "isVisible": true,
            "left": "5dp",
            "skin": "sknlblError",
            "text": "Error",
            "top": "0dp",
            "width": "85%",
            "zIndex": 1
        }, controller.args[0], "lblErrorText"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblErrorText"), extendConfig({}, controller.args[2], "lblErrorText"));
        flxInlineError.add(lblErrorIcon, lblErrorText);
        dynamicParameters.add(flxHeading, flxInputComponents, flxInlineError);
        return dynamicParameters;
    }
})