define(function() {
    return function(controller) {
        var reportPagination = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "clipBounds": true,
            "isMaster": true,
            "height": "50dp",
            "id": "reportPagination",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "left": "0dp",
            "isModalContainer": false,
            "skin": "sknBackgroundFFFFFF",
            "top": "0dp",
            "width": "100%"
        }, controller.args[0], "reportPagination"), extendConfig({}, controller.args[1], "reportPagination"), extendConfig({}, controller.args[2], "reportPagination"));
        reportPagination.setDefaultUnit(kony.flex.DP);
        var lblShowing = new kony.ui.Label(extendConfig({
            "bottom": "17px",
            "height": "15dp",
            "id": "lblShowing",
            "isVisible": true,
            "left": "20dp",
            "skin": "skn73767812pxLato",
            "text": "Showing 1-10 of 95 ",
            "top": "17dp",
            "width": "60%",
            "zIndex": 1
        }, controller.args[0], "lblShowing"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblShowing"), extendConfig({}, controller.args[2], "lblShowing"));
        var flxPagingNumber = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "clipBounds": true,
            "height": "100%",
            "id": "flxPagingNumber",
            "isVisible": true,
            "layoutType": kony.flex.FLOW_HORIZONTAL,
            "reverseLayoutDirection": true,
            "isModalContainer": false,
            "right": "20dp",
            "skin": "slFbox",
            "top": "0",
            "width": "500dp",
            "zIndex": 1
        }, controller.args[0], "flxPagingNumber"), extendConfig({}, controller.args[1], "flxPagingNumber"), extendConfig({}, controller.args[2], "flxPagingNumber"));
        flxPagingNumber.setDefaultUnit(kony.flex.DP);
        var flxGo = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "centerY": "50%",
            "clipBounds": true,
            "height": "21dp",
            "id": "flxGo",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "left": "10dp",
            "isModalContainer": false,
            "skin": "sknFlxBg003e753pxrad",
            "width": "40dp",
            "zIndex": 1
        }, controller.args[0], "flxGo"), extendConfig({}, controller.args[1], "flxGo"), extendConfig({}, controller.args[2], "flxGo"));
        flxGo.setDefaultUnit(kony.flex.DP);
        var lblGo = new kony.ui.Label(extendConfig({
            "centerX": "50%",
            "centerY": "50%",
            "height": "16dp",
            "id": "lblGo",
            "isVisible": true,
            "left": "0dp",
            "skin": "sknLbl0OFontFFFFFF100O",
            "text": "Go",
            "width": "33dp",
            "zIndex": 1
        }, controller.args[0], "lblGo"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_CENTER,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblGo"), extendConfig({}, controller.args[2], "lblGo"));
        flxGo.add(lblGo);
        var flxPageNumberTextBox = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "centerY": "50%",
            "clipBounds": true,
            "height": "21dp",
            "id": "flxPageNumberTextBox",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "left": "12dp",
            "isModalContainer": false,
            "skin": "sknflxborder979797",
            "width": "40dp",
            "zIndex": 1
        }, controller.args[0], "flxPageNumberTextBox"), extendConfig({}, controller.args[1], "flxPageNumberTextBox"), extendConfig({}, controller.args[2], "flxPageNumberTextBox"));
        flxPageNumberTextBox.setDefaultUnit(kony.flex.DP);
        var tbxPageNumber = new kony.ui.TextBox2(extendConfig({
            "autoCapitalize": constants.TEXTBOX_AUTO_CAPITALIZE_NONE,
            "focusSkin": "sknTbxlatreg13px000000Noborder",
            "height": "20dp",
            "id": "tbxPageNumber",
            "isVisible": true,
            "keyBoardStyle": constants.TEXTBOX_KEY_BOARD_STYLE_DEFAULT,
            "left": "0dp",
            "secureTextEntry": false,
            "skin": "sknTbxlatreg13px000000Noborder",
            "text": "2",
            "textInputMode": constants.TEXTBOX_INPUT_MODE_ANY,
            "top": "0dp",
            "width": "38dp",
            "zIndex": 1
        }, controller.args[0], "tbxPageNumber"), extendConfig({
            "containerHeightMode": constants.TEXTBOX_FONT_METRICS_DRIVEN_HEIGHT,
            "contentAlignment": constants.CONTENT_ALIGN_CENTER,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "tbxPageNumber"), extendConfig({
            "autoCorrect": false
        }, controller.args[2], "tbxPageNumber"));
        flxPageNumberTextBox.add(tbxPageNumber);
        var flxPaginationSeperator = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "bottom": "10dp",
            "clipBounds": true,
            "height": "30dp",
            "id": "flxPaginationSeperator",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "left": "10dp",
            "isModalContainer": false,
            "skin": "sknflxd6dbe7",
            "top": "10dp",
            "width": "1px",
            "zIndex": 1
        }, controller.args[0], "flxPaginationSeperator"), extendConfig({}, controller.args[1], "flxPaginationSeperator"), extendConfig({}, controller.args[2], "flxPaginationSeperator"));
        flxPaginationSeperator.setDefaultUnit(kony.flex.DP);
        flxPaginationSeperator.add();
        var flxnext = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "centerY": "50%",
            "clipBounds": true,
            "height": "16dp",
            "id": "flxnext",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "left": "10dp",
            "isModalContainer": false,
            "skin": "CopyslFbox",
            "top": "0",
            "width": "16dp",
            "zIndex": 1
        }, controller.args[0], "flxnext"), extendConfig({}, controller.args[1], "flxnext"), extendConfig({
            "hoverSkin": "sknFlxPointer"
        }, controller.args[2], "flxnext"));
        flxnext.setDefaultUnit(kony.flex.DP);
        var lblarrowNext = new kony.ui.Label(extendConfig({
            "centerX": "50%",
            "centerY": "50%",
            "height": "12dp",
            "id": "lblarrowNext",
            "isVisible": true,
            "left": "60dp",
            "skin": "sknlblfonticon10px",
            "text": "",
            "top": "56dp",
            "width": "16dp",
            "zIndex": 1
        }, controller.args[0], "lblarrowNext"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblarrowNext"), extendConfig({}, controller.args[2], "lblarrowNext"));
        flxnext.add(lblarrowNext);
        var flxNumber = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "centerY": "50%",
            "clipBounds": true,
            "height": "17dp",
            "id": "flxNumber",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "left": "10dp",
            "isModalContainer": false,
            "skin": "slFbox",
            "width": "33dp",
            "zIndex": 1
        }, controller.args[0], "flxNumber"), extendConfig({}, controller.args[1], "flxNumber"), extendConfig({}, controller.args[2], "flxNumber"));
        flxNumber.setDefaultUnit(kony.flex.DP);
        var lblNumber = new kony.ui.Label(extendConfig({
            "centerX": "50%",
            "centerY": "50%",
            "height": "16dp",
            "id": "lblNumber",
            "isVisible": true,
            "left": "0dp",
            "skin": "sknLbl192b45LatoBold12px",
            "text": "1",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, controller.args[0], "lblNumber"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_CENTER,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblNumber"), extendConfig({}, controller.args[2], "lblNumber"));
        flxNumber.add(lblNumber);
        var flxPrevious = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "centerY": "50%",
            "clipBounds": true,
            "height": "16dp",
            "id": "flxPrevious",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "left": "10dp",
            "isModalContainer": false,
            "skin": "CopyslFbox",
            "top": "0",
            "width": "20dp",
            "zIndex": 1
        }, controller.args[0], "flxPrevious"), extendConfig({}, controller.args[1], "flxPrevious"), extendConfig({
            "hoverSkin": "sknFlxPointer"
        }, controller.args[2], "flxPrevious"));
        flxPrevious.setDefaultUnit(kony.flex.DP);
        var lblarrowprevious = new kony.ui.Label(extendConfig({
            "centerX": "50%",
            "centerY": "50%",
            "height": "12dp",
            "id": "lblarrowprevious",
            "isVisible": true,
            "left": "60dp",
            "skin": "sknlblfonticon10px",
            "text": "",
            "top": "56dp",
            "width": "16dp",
            "zIndex": 1
        }, controller.args[0], "lblarrowprevious"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblarrowprevious"), extendConfig({}, controller.args[2], "lblarrowprevious"));
        flxPrevious.add(lblarrowprevious);
        flxPagingNumber.add(flxGo, flxPageNumberTextBox, flxPaginationSeperator, flxnext, flxNumber, flxPrevious);
        reportPagination.add(lblShowing, flxPagingNumber);
        return reportPagination;
    }
})