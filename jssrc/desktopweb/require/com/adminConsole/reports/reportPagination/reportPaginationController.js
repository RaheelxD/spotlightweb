define("com/adminConsole/reports/reportPagination/userreportPaginationController", function() {
    return {};
});
define("com/adminConsole/reports/reportPagination/reportPaginationControllerActions", {
    /*
          This is an auto generated file and any modifications to it may result in corruption of the action sequence.
        */
});
define("com/adminConsole/reports/reportPagination/reportPaginationController", ["com/adminConsole/reports/reportPagination/userreportPaginationController", "com/adminConsole/reports/reportPagination/reportPaginationControllerActions"], function() {
    var controller = require("com/adminConsole/reports/reportPagination/userreportPaginationController");
    var actions = require("com/adminConsole/reports/reportPagination/reportPaginationControllerActions");
    for (var key in actions) {
        controller[key] = actions[key];
    }
    return controller;
});
