define("com/adminConsole/businessTypes/addBusinessTypes/useraddBusinessTypesController", function() {
    return {};
});
define("com/adminConsole/businessTypes/addBusinessTypes/addBusinessTypesControllerActions", {
    /*
          This is an auto generated file and any modifications to it may result in corruption of the action sequence.
        */
});
define("com/adminConsole/businessTypes/addBusinessTypes/addBusinessTypesController", ["com/adminConsole/businessTypes/addBusinessTypes/useraddBusinessTypesController", "com/adminConsole/businessTypes/addBusinessTypes/addBusinessTypesControllerActions"], function() {
    var controller = require("com/adminConsole/businessTypes/addBusinessTypes/useraddBusinessTypesController");
    var actions = require("com/adminConsole/businessTypes/addBusinessTypes/addBusinessTypesControllerActions");
    for (var key in actions) {
        controller[key] = actions[key];
    }
    return controller;
});
