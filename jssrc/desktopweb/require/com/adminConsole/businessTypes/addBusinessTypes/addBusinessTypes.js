define(function() {
    return function(controller) {
        var addBusinessTypes = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "clipBounds": true,
            "isMaster": true,
            "height": "80dp",
            "id": "addBusinessTypes",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "left": "0dp",
            "isModalContainer": false,
            "skin": "slFbox",
            "top": "0dp",
            "width": "100%"
        }, controller.args[0], "addBusinessTypes"), extendConfig({}, controller.args[1], "addBusinessTypes"), extendConfig({}, controller.args[2], "addBusinessTypes"));
        addBusinessTypes.setDefaultUnit(kony.flex.DP);
        var lblCount = new kony.ui.Label(extendConfig({
            "id": "lblCount",
            "isVisible": false,
            "right": "40dp",
            "skin": "slLabel0d20174dce8ea42",
            "text": "0/10",
            "top": "2dp",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, controller.args[0], "lblCount"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblCount"), extendConfig({}, controller.args[2], "lblCount"));
        var flxTextAndDelete = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "bottom": "10dp",
            "clipBounds": true,
            "height": "40dp",
            "id": "flxTextAndDelete",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "left": "0dp",
            "isModalContainer": false,
            "skin": "slFbox",
            "top": "20dp",
            "width": "100%"
        }, controller.args[0], "flxTextAndDelete"), extendConfig({}, controller.args[1], "flxTextAndDelete"), extendConfig({}, controller.args[2], "flxTextAndDelete"));
        flxTextAndDelete.setDefaultUnit(kony.flex.DP);
        var tbxEnterValue = new kony.ui.TextBox2(extendConfig({
            "autoCapitalize": constants.TEXTBOX_AUTO_CAPITALIZE_NONE,
            "focusSkin": "skntbxLato35475f14px",
            "height": "100%",
            "id": "tbxEnterValue",
            "isVisible": true,
            "keyBoardStyle": constants.TEXTBOX_KEY_BOARD_STYLE_DEFAULT,
            "left": "10dp",
            "maxTextLength": 50,
            "i18n_placeholder": "kony.i18n.getLocalizedString(\"i18n.frmBusinessTypes.authorized_signatories\")",
            "right": "40dp",
            "secureTextEntry": false,
            "skin": "skntbxLato35475f14px",
            "textInputMode": constants.TEXTBOX_INPUT_MODE_ANY,
            "top": "0dp",
            "zIndex": 1
        }, controller.args[0], "tbxEnterValue"), extendConfig({
            "containerHeightMode": constants.TEXTBOX_FONT_METRICS_DRIVEN_HEIGHT,
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [2, 2, 2, 2],
            "paddingInPixel": false
        }, controller.args[1], "tbxEnterValue"), extendConfig({
            "autoCorrect": false
        }, controller.args[2], "tbxEnterValue"));
        var flxDelete = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "clipBounds": true,
            "height": "100%",
            "id": "flxDelete",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "isModalContainer": false,
            "right": "10px",
            "skin": "CopyslFbox0efc73ba91cad4b",
            "top": "0px",
            "width": "22px"
        }, controller.args[0], "flxDelete"), extendConfig({}, controller.args[1], "flxDelete"), extendConfig({
            "hoverSkin": "tabsHoverSkin"
        }, controller.args[2], "flxDelete"));
        flxDelete.setDefaultUnit(kony.flex.DP);
        var lblDelete = new kony.ui.Label(extendConfig({
            "centerX": "50%",
            "centerY": "50%",
            "height": "22px",
            "id": "lblDelete",
            "isVisible": true,
            "skin": "sknIcon22px192b45",
            "i18n_text": "kony.i18n.getLocalizedString(\"i18n.userwidgetmodel.lblIconOption2\")",
            "width": "22px",
            "zIndex": 1
        }, controller.args[0], "lblDelete"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblDelete"), extendConfig({}, controller.args[2], "lblDelete"));
        flxDelete.add(lblDelete);
        flxTextAndDelete.add(tbxEnterValue, flxDelete);
        var flxInlineError = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
            "clipBounds": true,
            "id": "flxInlineError",
            "isVisible": false,
            "layoutType": kony.flex.FLOW_HORIZONTAL,
            "left": "10dp",
            "isModalContainer": false,
            "skin": "slFbox",
            "top": "62dp",
            "width": "100%",
            "zIndex": 1
        }, controller.args[0], "flxInlineError"), extendConfig({}, controller.args[1], "flxInlineError"), extendConfig({}, controller.args[2], "flxInlineError"));
        flxInlineError.setDefaultUnit(kony.flex.DP);
        var lblIconError = new kony.ui.Label(extendConfig({
            "id": "lblIconError",
            "isVisible": true,
            "left": "0dp",
            "skin": "sknErrorIcon",
            "text": "",
            "top": "0dp",
            "width": "15dp",
            "zIndex": 1
        }, controller.args[0], "lblIconError"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblIconError"), extendConfig({}, controller.args[2], "lblIconError"));
        var lblError = new kony.ui.Label(extendConfig({
            "id": "lblError",
            "isVisible": true,
            "left": "0dp",
            "right": "0dp",
            "skin": "sknlblError",
            "text": "Authorized SIgnatory with same name already exists.",
            "top": "0dp",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, controller.args[0], "lblError"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblError"), extendConfig({}, controller.args[2], "lblError"));
        flxInlineError.add(lblIconError, lblError);
        addBusinessTypes.add(lblCount, flxTextAndDelete, flxInlineError);
        return addBusinessTypes;
    }
})