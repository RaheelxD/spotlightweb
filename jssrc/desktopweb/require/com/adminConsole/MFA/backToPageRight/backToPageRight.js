define(function() {
    return function(controller) {
        var backToPageRight = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "clipBounds": true,
            "isMaster": true,
            "height": "20dp",
            "id": "backToPageRight",
            "isVisible": true,
            "layoutType": kony.flex.FLOW_HORIZONTAL,
            "reverseLayoutDirection": true,
            "left": "0dp",
            "isModalContainer": false,
            "skin": "slFbox",
            "top": "0dp",
            "width": "100%"
        }, controller.args[0], "backToPageRight"), extendConfig({}, controller.args[1], "backToPageRight"), extendConfig({}, controller.args[2], "backToPageRight"));
        backToPageRight.setDefaultUnit(kony.flex.DP);
        var btnBack = new kony.ui.Button(extendConfig({
            "centerY": "50%",
            "height": "20dp",
            "id": "btnBack",
            "isVisible": true,
            "right": "0dp",
            "skin": "sknBtnLato117eb013Px",
            "i18n_text": "kony.i18n.getLocalizedString(\"i18n.userwidgetmodel.btnBack\")",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, controller.args[0], "btnBack"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_CENTER,
            "displayText": true,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "btnBack"), extendConfig({}, controller.args[2], "btnBack"));
        var flxBack = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "centerY": "50%",
            "clipBounds": true,
            "height": "20dp",
            "id": "flxBack",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "isModalContainer": false,
            "right": "7dp",
            "skin": "sknCursor",
            "width": "20px",
            "zIndex": 1
        }, controller.args[0], "flxBack"), extendConfig({}, controller.args[1], "flxBack"), extendConfig({}, controller.args[2], "flxBack"));
        flxBack.setDefaultUnit(kony.flex.DP);
        var fontIconBack = new kony.ui.Label(extendConfig({
            "centerX": "50%",
            "centerY": "50%",
            "height": "14dp",
            "id": "fontIconBack",
            "isVisible": true,
            "left": "0dp",
            "skin": "sknIcon117eb015px",
            "text": "",
            "top": "0dp",
            "width": "16dp",
            "zIndex": 1
        }, controller.args[0], "fontIconBack"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "fontIconBack"), extendConfig({}, controller.args[2], "fontIconBack"));
        flxBack.add(fontIconBack);
        backToPageRight.add(btnBack, flxBack);
        return backToPageRight;
    }
})