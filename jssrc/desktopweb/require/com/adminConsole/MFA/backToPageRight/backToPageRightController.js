define("com/adminConsole/MFA/backToPageRight/userbackToPageRightController", function() {
    return {};
});
define("com/adminConsole/MFA/backToPageRight/backToPageRightControllerActions", {
    /*
          This is an auto generated file and any modifications to it may result in corruption of the action sequence.
        */
});
define("com/adminConsole/MFA/backToPageRight/backToPageRightController", ["com/adminConsole/MFA/backToPageRight/userbackToPageRightController", "com/adminConsole/MFA/backToPageRight/backToPageRightControllerActions"], function() {
    var controller = require("com/adminConsole/MFA/backToPageRight/userbackToPageRightController");
    var actions = require("com/adminConsole/MFA/backToPageRight/backToPageRightControllerActions");
    for (var key in actions) {
        controller[key] = actions[key];
    }
    return controller;
});
