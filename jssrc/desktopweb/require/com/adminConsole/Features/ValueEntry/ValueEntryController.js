define("com/adminConsole/Features/ValueEntry/userValueEntryController", function() {
    return {};
});
define("com/adminConsole/Features/ValueEntry/ValueEntryControllerActions", {
    /*
          This is an auto generated file and any modifications to it may result in corruption of the action sequence.
        */
});
define("com/adminConsole/Features/ValueEntry/ValueEntryController", ["com/adminConsole/Features/ValueEntry/userValueEntryController", "com/adminConsole/Features/ValueEntry/ValueEntryControllerActions"], function() {
    var controller = require("com/adminConsole/Features/ValueEntry/userValueEntryController");
    var actions = require("com/adminConsole/Features/ValueEntry/ValueEntryControllerActions");
    for (var key in actions) {
        controller[key] = actions[key];
    }
    return controller;
});
