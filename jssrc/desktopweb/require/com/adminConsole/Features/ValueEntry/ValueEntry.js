define(function() {
    return function(controller) {
        var ValueEntry = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
            "clipBounds": false,
            "isMaster": true,
            "id": "ValueEntry",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "left": "0dp",
            "isModalContainer": false,
            "skin": "slFbox",
            "top": "0dp",
            "width": "100%"
        }, controller.args[0], "ValueEntry"), extendConfig({}, controller.args[1], "ValueEntry"), extendConfig({}, controller.args[2], "ValueEntry"));
        ValueEntry.setDefaultUnit(kony.flex.DP);
        var flxValueTextBox = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "clipBounds": true,
            "height": "40dp",
            "id": "flxValueTextBox",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "left": "0dp",
            "isModalContainer": false,
            "right": "0dp",
            "skin": "sknFlxbgFFFFFFbdrD7D9E0rd3px",
            "top": "0dp"
        }, controller.args[0], "flxValueTextBox"), extendConfig({}, controller.args[1], "flxValueTextBox"), extendConfig({}, controller.args[2], "flxValueTextBox"));
        flxValueTextBox.setDefaultUnit(kony.flex.DP);
        var tbxEnterValue = new kony.ui.TextBox2(extendConfig({
            "autoCapitalize": constants.TEXTBOX_AUTO_CAPITALIZE_NONE,
            "centerY": "50%",
            "height": "38dp",
            "id": "tbxEnterValue",
            "isVisible": true,
            "keyBoardStyle": constants.TEXTBOX_KEY_BOARD_STYLE_DEFAULT,
            "left": "30px",
            "right": "0dp",
            "secureTextEntry": false,
            "skin": "sknTbxBgFFFFFFf485c7513pxNoBorder",
            "textInputMode": constants.TEXTBOX_INPUT_MODE_NUMERIC,
            "zIndex": 1
        }, controller.args[0], "tbxEnterValue"), extendConfig({
            "containerHeightMode": constants.TEXTBOX_FONT_METRICS_DRIVEN_HEIGHT,
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [2, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "tbxEnterValue"), extendConfig({
            "autoCorrect": false,
            "placeholderSkin": "defTextBoxPlaceholder"
        }, controller.args[2], "tbxEnterValue"));
        var lblCurrencySymbol = new kony.ui.Label(extendConfig({
            "centerY": "50%",
            "id": "lblCurrencySymbol",
            "isVisible": true,
            "left": "5dp",
            "skin": "sknFontIcon485c7520px",
            "text": "",
            "width": "20dp",
            "zIndex": 1
        }, controller.args[0], "lblCurrencySymbol"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblCurrencySymbol"), extendConfig({}, controller.args[2], "lblCurrencySymbol"));
        flxValueTextBox.add(tbxEnterValue, lblCurrencySymbol);
        ValueEntry.add(flxValueTextBox);
        return ValueEntry;
    }
})