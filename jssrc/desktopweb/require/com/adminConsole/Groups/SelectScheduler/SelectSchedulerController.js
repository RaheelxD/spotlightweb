define("com/adminConsole/Groups/SelectScheduler/userSelectSchedulerController", function() {
    return {
        showSchedulerDetails: function() {
            var ScopeObj = this;
            var index = kony.application.getCurrentForm().SelectScheduler.segMasterList.selectedIndex;
            var rowIndex = index[1];
            var data = kony.application.getCurrentForm().SelectScheduler.segMasterList.data;
            if (data[rowIndex].imgCheckbox.src === "radio_notselected.png") {
                kony.application.getCurrentForm().SelectScheduler.flxNoDetails.setVisibility(true);
                kony.application.getCurrentForm().SelectScheduler.flxDetails.setVisibility(false);
            } else {
                kony.application.getCurrentForm().SelectScheduler.flxNoDetails.setVisibility(false);
                kony.application.getCurrentForm().SelectScheduler.flxDetails.setVisibility(true);
            }
        }
    };
});
define("com/adminConsole/Groups/SelectScheduler/SelectSchedulerControllerActions", {
    /*
        This is an auto generated file and any modifications to it may result in corruption of the action sequence.
    */
    AS_Segment_b767df7c8e0647a087391c42e1b9d453: function AS_Segment_b767df7c8e0647a087391c42e1b9d453(eventobject, sectionNumber, rowNumber) {
        var self = this;
        this.showSchedulerDetails();
    }
});
define("com/adminConsole/Groups/SelectScheduler/SelectSchedulerController", ["com/adminConsole/Groups/SelectScheduler/userSelectSchedulerController", "com/adminConsole/Groups/SelectScheduler/SelectSchedulerControllerActions"], function() {
    var controller = require("com/adminConsole/Groups/SelectScheduler/userSelectSchedulerController");
    var actions = require("com/adminConsole/Groups/SelectScheduler/SelectSchedulerControllerActions");
    for (var key in actions) {
        controller[key] = actions[key];
    }
    return controller;
});
