define(function() {
    return function(controller) {
        var AdvancedSearch = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "clipBounds": true,
            "isMaster": true,
            "height": "90%",
            "id": "AdvancedSearch",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "left": "0dp",
            "isModalContainer": false,
            "preShow": function(eventobject) {
                controller.AS_FlexContainer_fdd27c0633c14c7e8cc9f106c0e50480(eventobject);
            },
            "skin": "slFbox0dc900c4572aa40",
            "top": "0dp",
            "width": "70%",
            "zIndex": 1
        }, controller.args[0], "AdvancedSearch"), extendConfig({}, controller.args[1], "AdvancedSearch"), extendConfig({}, controller.args[2], "AdvancedSearch"));
        AdvancedSearch.setDefaultUnit(kony.flex.DP);
        var flxPopUp = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "bottom": "9%",
            "clipBounds": false,
            "height": "100%",
            "id": "flxPopUp",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "left": "0px",
            "isModalContainer": false,
            "skin": "sknflxffffffop100",
            "top": "0%",
            "width": "100%",
            "zIndex": 10
        }, controller.args[0], "flxPopUp"), extendConfig({}, controller.args[1], "flxPopUp"), extendConfig({}, controller.args[2], "flxPopUp"));
        flxPopUp.setDefaultUnit(kony.flex.DP);
        var flxPopUpTopColor = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "clipBounds": true,
            "height": "10px",
            "id": "flxPopUpTopColor",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "left": "0dp",
            "isModalContainer": false,
            "skin": "sknflx11abeb",
            "top": "0dp",
            "width": "100%",
            "zIndex": 1
        }, controller.args[0], "flxPopUpTopColor"), extendConfig({}, controller.args[1], "flxPopUpTopColor"), extendConfig({}, controller.args[2], "flxPopUpTopColor"));
        flxPopUpTopColor.setDefaultUnit(kony.flex.DP);
        flxPopUpTopColor.add();
        var flxPopupHeader = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "centerX": "50%",
            "clipBounds": true,
            "height": "48px",
            "id": "flxPopupHeader",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "left": "20px",
            "isModalContainer": false,
            "right": "20px",
            "skin": "slFbox0d9c3974835234d",
            "top": "10px",
            "zIndex": 1
        }, controller.args[0], "flxPopupHeader"), extendConfig({}, controller.args[1], "flxPopupHeader"), extendConfig({}, controller.args[2], "flxPopupHeader"));
        flxPopupHeader.setDefaultUnit(kony.flex.DP);
        var lblPopUpMainMessage = new kony.ui.Label(extendConfig({
            "id": "lblPopUpMainMessage",
            "isVisible": true,
            "left": "0px",
            "skin": "sknlblLatoRegular484b5213px",
            "i18n_text": "kony.i18n.getLocalizedString(\"i18n.userwidgetmodel.search_for_customers\")",
            "top": "20px",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, controller.args[0], "lblPopUpMainMessage"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblPopUpMainMessage"), extendConfig({}, controller.args[2], "lblPopUpMainMessage"));
        var flxRightImage = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "centerY": "50%",
            "clipBounds": true,
            "height": "20px",
            "id": "flxRightImage",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "isModalContainer": false,
            "right": 0,
            "skin": "slFbox",
            "width": "20px",
            "zIndex": 1
        }, controller.args[0], "flxRightImage"), extendConfig({}, controller.args[1], "flxRightImage"), extendConfig({
            "hoverSkin": "sknCursor"
        }, controller.args[2], "flxRightImage"));
        flxRightImage.setDefaultUnit(kony.flex.DP);
        var fontIconRight = new kony.ui.Label(extendConfig({
            "centerY": "50%",
            "id": "fontIconRight",
            "isVisible": true,
            "left": "0dp",
            "skin": "sknFontIconSearchCross20px",
            "i18n_text": "kony.i18n.getLocalizedString(\"i18n.userwidgetmodel.lblClearSearch\")",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, controller.args[0], "fontIconRight"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "fontIconRight"), extendConfig({}, controller.args[2], "fontIconRight"));
        flxRightImage.add(fontIconRight);
        flxPopupHeader.add(lblPopUpMainMessage, flxRightImage);
        var flxPopupBody = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "bottom": "60px",
            "clipBounds": true,
            "id": "flxPopupBody",
            "isVisible": true,
            "layoutType": kony.flex.FLOW_VERTICAL,
            "left": "20px",
            "isModalContainer": false,
            "right": "20px",
            "skin": "slFbox",
            "top": "58px",
            "zIndex": 5
        }, controller.args[0], "flxPopupBody"), extendConfig({}, controller.args[1], "flxPopupBody"), extendConfig({}, controller.args[2], "flxPopupBody"));
        flxPopupBody.setDefaultUnit(kony.flex.DP);
        var flxRow0 = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
            "clipBounds": true,
            "id": "flxRow0",
            "isVisible": true,
            "layoutType": kony.flex.FLOW_VERTICAL,
            "left": "0dp",
            "isModalContainer": false,
            "skin": "slFbox",
            "top": "5dp",
            "width": "100%",
            "zIndex": 1
        }, controller.args[0], "flxRow0"), extendConfig({}, controller.args[1], "flxRow0"), extendConfig({}, controller.args[2], "flxRow0"));
        flxRow0.setDefaultUnit(kony.flex.DP);
        var lblRow0 = new kony.ui.Label(extendConfig({
            "id": "lblRow0",
            "isVisible": true,
            "left": "0px",
            "skin": "sknlblLatoRegular485c75Font12px",
            "i18n_text": "kony.i18n.getLocalizedString(\"i18n.userwidgetmodel.lblRow0\")",
            "top": "0px",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, controller.args[0], "lblRow0"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblRow0"), extendConfig({}, controller.args[2], "lblRow0"));
        var flxSearchContainer = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "clipBounds": true,
            "focusSkin": "sknflxBgffffffBorder1293ccRadius30Px",
            "height": "40px",
            "id": "flxSearchContainer",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "left": "0px",
            "isModalContainer": false,
            "skin": "sknflxBgffffffBorderc1c9ceRadius30px",
            "top": "6dp",
            "width": "100%",
            "zIndex": 1
        }, controller.args[0], "flxSearchContainer"), extendConfig({}, controller.args[1], "flxSearchContainer"), extendConfig({}, controller.args[2], "flxSearchContainer"));
        flxSearchContainer.setDefaultUnit(kony.flex.DP);
        var fontIconSearch = new kony.ui.Label(extendConfig({
            "centerY": "50%",
            "id": "fontIconSearch",
            "isVisible": true,
            "left": "10dp",
            "skin": "sknFontIconSearchCross20px",
            "i18n_text": "kony.i18n.getLocalizedString(\"i18n.userwidgetmodel.lblSearchIcon\")",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, controller.args[0], "fontIconSearch"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "fontIconSearch"), extendConfig({}, controller.args[2], "fontIconSearch"));
        var tbxSearchBox = new kony.ui.TextBox2(extendConfig({
            "autoCapitalize": constants.TEXTBOX_AUTO_CAPITALIZE_NONE,
            "centerY": "50%",
            "height": "30px",
            "id": "tbxSearchBox",
            "isVisible": true,
            "keyBoardStyle": constants.TEXTBOX_KEY_BOARD_STYLE_DEFAULT,
            "left": "35dp",
            "onTouchStart": controller.AS_TextField_cfab06859f0f43eca99495891ee290d5,
            "placeholder": "Enter Customer Last Name, ID or Username",
            "right": "35px",
            "secureTextEntry": false,
            "skin": "skntbxffffffNoBorderlato35475f14px",
            "textInputMode": constants.TEXTBOX_INPUT_MODE_ANY,
            "zIndex": 1
        }, controller.args[0], "tbxSearchBox"), extendConfig({
            "containerHeightMode": constants.TEXTBOX_FONT_METRICS_DRIVEN_HEIGHT,
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "tbxSearchBox"), extendConfig({
            "autoCorrect": false
        }, controller.args[2], "tbxSearchBox"));
        var flxClearSearchImage = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "clipBounds": true,
            "height": "100%",
            "id": "flxClearSearchImage",
            "isVisible": false,
            "layoutType": kony.flex.FREE_FORM,
            "isModalContainer": false,
            "right": "0px",
            "skin": "slFbox",
            "top": "0dp",
            "width": "35px",
            "zIndex": 1
        }, controller.args[0], "flxClearSearchImage"), extendConfig({}, controller.args[1], "flxClearSearchImage"), extendConfig({
            "hoverSkin": "sknCursor"
        }, controller.args[2], "flxClearSearchImage"));
        flxClearSearchImage.setDefaultUnit(kony.flex.DP);
        var fontIconClearSearch = new kony.ui.Label(extendConfig({
            "centerY": "50%",
            "id": "fontIconClearSearch",
            "isVisible": true,
            "right": "15dp",
            "skin": "sknFontIconSearchCross20px",
            "i18n_text": "kony.i18n.getLocalizedString(\"i18n.userwidgetmodel.lblClearSearch\")",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, controller.args[0], "fontIconClearSearch"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "fontIconClearSearch"), extendConfig({}, controller.args[2], "fontIconClearSearch"));
        flxClearSearchImage.add(fontIconClearSearch);
        flxSearchContainer.add(fontIconSearch, tbxSearchBox, flxClearSearchImage);
        flxRow0.add(lblRow0, flxSearchContainer);
        var flxRowHeader1 = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
            "clipBounds": true,
            "id": "flxRowHeader1",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "left": "0dp",
            "isModalContainer": false,
            "skin": "slFbox0d9c3974835234d",
            "top": "38px",
            "width": "100%",
            "zIndex": 1
        }, controller.args[0], "flxRowHeader1"), extendConfig({}, controller.args[1], "flxRowHeader1"), extendConfig({}, controller.args[2], "flxRowHeader1"));
        flxRowHeader1.setDefaultUnit(kony.flex.DP);
        var lblRowHeader1 = new kony.ui.Label(extendConfig({
            "id": "lblRowHeader1",
            "isVisible": true,
            "left": "0px",
            "skin": "sknlblLatoRegular484b5213px",
            "i18n_text": "kony.i18n.getLocalizedString(\"i18n.userwidgetmodel.lblRowHeader1\")",
            "top": "0px",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 2
        }, controller.args[0], "lblRowHeader1"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblRowHeader1"), extendConfig({}, controller.args[2], "lblRowHeader1"));
        flxRowHeader1.add(lblRowHeader1);
        var flxRow1 = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "clipBounds": false,
            "height": "75px",
            "id": "flxRow1",
            "isVisible": true,
            "layoutType": kony.flex.FLOW_HORIZONTAL,
            "left": "0dp",
            "isModalContainer": false,
            "skin": "slFbox",
            "top": "20px",
            "width": "100%",
            "zIndex": 10
        }, controller.args[0], "flxRow1"), extendConfig({}, controller.args[1], "flxRow1"), extendConfig({}, controller.args[2], "flxRow1"));
        flxRow1.setDefaultUnit(kony.flex.DP);
        var flxColumn01 = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
            "clipBounds": false,
            "id": "flxColumn01",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "left": "0dp",
            "isModalContainer": false,
            "skin": "slFbox",
            "top": "1dp",
            "width": "31%",
            "zIndex": 1
        }, controller.args[0], "flxColumn01"), extendConfig({}, controller.args[1], "flxColumn01"), extendConfig({}, controller.args[2], "flxColumn01"));
        flxColumn01.setDefaultUnit(kony.flex.DP);
        var lblCol01 = new kony.ui.Label(extendConfig({
            "id": "lblCol01",
            "isVisible": true,
            "left": "0dp",
            "skin": "sknlblLatoRegular485c7512px",
            "i18n_text": "kony.i18n.getLocalizedString(\"i18n.userwidgetmodel.lblCol01\")",
            "top": "0px",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, controller.args[0], "lblCol01"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblCol01"), extendConfig({}, controller.args[2], "lblCol01"));
        var flxDropDown01 = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "clipBounds": false,
            "height": "41px",
            "id": "flxDropDown01",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "left": "0dp",
            "isModalContainer": false,
            "skin": "sknflxffffffoptemplateop3px",
            "top": "28dp",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, controller.args[0], "flxDropDown01"), extendConfig({}, controller.args[1], "flxDropDown01"), extendConfig({
            "hoverSkin": "sknFlxSegRowHover11abeb"
        }, controller.args[2], "flxDropDown01"));
        flxDropDown01.setDefaultUnit(kony.flex.DP);
        var lblSelectedRows = new kony.ui.Label(extendConfig({
            "centerY": "49%",
            "id": "lblSelectedRows",
            "isVisible": true,
            "left": "12px",
            "right": "45dp",
            "skin": "sknlbl0h2ff0d6b13f947AD",
            "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmGroupsController.Select_the_Branches\")",
            "zIndex": 1
        }, controller.args[0], "lblSelectedRows"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblSelectedRows"), extendConfig({}, controller.args[2], "lblSelectedRows"));
        var flxSearchDownImg = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "centerY": "50%",
            "clipBounds": true,
            "height": "12px",
            "id": "flxSearchDownImg",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "isModalContainer": false,
            "right": "15px",
            "skin": "slFbox",
            "width": "15px",
            "zIndex": 1
        }, controller.args[0], "flxSearchDownImg"), extendConfig({}, controller.args[1], "flxSearchDownImg"), extendConfig({}, controller.args[2], "flxSearchDownImg"));
        flxSearchDownImg.setDefaultUnit(kony.flex.DP);
        var fontIconSearchDown01 = new kony.ui.Label(extendConfig({
            "centerY": "50%",
            "id": "fontIconSearchDown01",
            "isVisible": true,
            "left": "0dp",
            "skin": "sknfontIconListbox15px",
            "i18n_text": "kony.i18n.getLocalizedString(\"i18n.userwidgetmodel.fontIconSearchDown01\")",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 2
        }, controller.args[0], "fontIconSearchDown01"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "fontIconSearchDown01"), extendConfig({}, controller.args[2], "fontIconSearchDown01"));
        flxSearchDownImg.add(fontIconSearchDown01);
        flxDropDown01.add(lblSelectedRows, flxSearchDownImg);
        var flxDropDownDetail01 = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "clipBounds": false,
            "height": "167px",
            "id": "flxDropDownDetail01",
            "isVisible": false,
            "layoutType": kony.flex.FREE_FORM,
            "left": "0dp",
            "isModalContainer": false,
            "skin": "skntxtbxNormald7d9e0",
            "top": "72px",
            "width": "100%",
            "zIndex": 40
        }, controller.args[0], "flxDropDownDetail01"), extendConfig({}, controller.args[1], "flxDropDownDetail01"), extendConfig({
            "hoverSkin": "skntxtbxhover11abeb"
        }, controller.args[2], "flxDropDownDetail01"));
        flxDropDownDetail01.setDefaultUnit(kony.flex.DP);
        var AdvancedSearchDropDown01 = new com.adminConsole.Groups.AdvancedSearchDropDown(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "height": "100%",
            "id": "AdvancedSearchDropDown01",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "left": "0dp",
            "masterType": constants.MASTER_TYPE_DEFAULT,
            "isModalContainer": false,
            "skin": "sknNormalDefault",
            "top": "0dp",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 100,
            "overrides": {
                "AdvancedSearchDropDown": {
                    "height": "100%"
                }
            }
        }, controller.args[0], "AdvancedSearchDropDown01"), extendConfig({
            "overrides": {}
        }, controller.args[1], "AdvancedSearchDropDown01"), extendConfig({
            "overrides": {}
        }, controller.args[2], "AdvancedSearchDropDown01"));
        flxDropDownDetail01.add(AdvancedSearchDropDown01);
        flxColumn01.add(lblCol01, flxDropDown01, flxDropDownDetail01);
        var flxColumn11 = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
            "clipBounds": false,
            "id": "flxColumn11",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "left": "3.50%",
            "isModalContainer": false,
            "skin": "slFbox",
            "top": "0dp",
            "width": "31%",
            "zIndex": 1
        }, controller.args[0], "flxColumn11"), extendConfig({}, controller.args[1], "flxColumn11"), extendConfig({}, controller.args[2], "flxColumn11"));
        flxColumn11.setDefaultUnit(kony.flex.DP);
        var lblCol11 = new kony.ui.Label(extendConfig({
            "id": "lblCol11",
            "isVisible": true,
            "left": "0dp",
            "skin": "sknlblLatoRegular485c7512px",
            "i18n_text": "kony.i18n.getLocalizedString(\"i18n.userwidgetmodel.lblCol11\")",
            "top": "0px",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, controller.args[0], "lblCol11"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblCol11"), extendConfig({}, controller.args[2], "lblCol11"));
        var flxDropDown11 = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "clipBounds": true,
            "height": "41px",
            "id": "flxDropDown11",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "left": "0dp",
            "isModalContainer": false,
            "skin": "sknflxffffffoptemplateop3px",
            "top": "28dp",
            "width": "99%",
            "zIndex": 1
        }, controller.args[0], "flxDropDown11"), extendConfig({}, controller.args[1], "flxDropDown11"), extendConfig({
            "hoverSkin": "sknFlxSegRowHover11abeb"
        }, controller.args[2], "flxDropDown11"));
        flxDropDown11.setDefaultUnit(kony.flex.DP);
        var lblSelectedRows11 = new kony.ui.Label(extendConfig({
            "centerY": "50%",
            "id": "lblSelectedRows11",
            "isVisible": true,
            "left": "12px",
            "right": "45dp",
            "skin": "sknlbl0h2ff0d6b13f947AD",
            "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmGroupsController.Select_the_Products\")",
            "zIndex": 1
        }, controller.args[0], "lblSelectedRows11"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblSelectedRows11"), extendConfig({}, controller.args[2], "lblSelectedRows11"));
        var flxSearchDownImg11 = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "centerY": "50%",
            "clipBounds": true,
            "height": "12px",
            "id": "flxSearchDownImg11",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "isModalContainer": false,
            "right": "15px",
            "skin": "slFbox",
            "width": "15px",
            "zIndex": 1
        }, controller.args[0], "flxSearchDownImg11"), extendConfig({}, controller.args[1], "flxSearchDownImg11"), extendConfig({}, controller.args[2], "flxSearchDownImg11"));
        flxSearchDownImg11.setDefaultUnit(kony.flex.DP);
        var fontIconSearchDown11 = new kony.ui.Label(extendConfig({
            "centerY": "50%",
            "id": "fontIconSearchDown11",
            "isVisible": true,
            "left": "0dp",
            "skin": "sknfontIconListbox15px",
            "i18n_text": "kony.i18n.getLocalizedString(\"i18n.userwidgetmodel.fontIconSearchDown01\")",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 2
        }, controller.args[0], "fontIconSearchDown11"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "fontIconSearchDown11"), extendConfig({}, controller.args[2], "fontIconSearchDown11"));
        flxSearchDownImg11.add(fontIconSearchDown11);
        flxDropDown11.add(lblSelectedRows11, flxSearchDownImg11);
        var flxDropDownDetail11 = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "clipBounds": true,
            "height": "167px",
            "id": "flxDropDownDetail11",
            "isVisible": false,
            "layoutType": kony.flex.FREE_FORM,
            "left": "0dp",
            "isModalContainer": false,
            "skin": "skntxtbxNormald7d9e0",
            "top": "72px",
            "width": "100%",
            "zIndex": 1
        }, controller.args[0], "flxDropDownDetail11"), extendConfig({}, controller.args[1], "flxDropDownDetail11"), extendConfig({
            "hoverSkin": "skntxtbxhover11abeb"
        }, controller.args[2], "flxDropDownDetail11"));
        flxDropDownDetail11.setDefaultUnit(kony.flex.DP);
        var AdvSearchDropDown11 = new com.adminConsole.Groups.AdvancedSearchDropDown(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "height": "100%",
            "id": "AdvSearchDropDown11",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "left": "0dp",
            "masterType": constants.MASTER_TYPE_DEFAULT,
            "isModalContainer": false,
            "skin": "sknNormalDefault",
            "top": "0dp",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 100,
            "overrides": {}
        }, controller.args[0], "AdvSearchDropDown11"), extendConfig({
            "overrides": {}
        }, controller.args[1], "AdvSearchDropDown11"), extendConfig({
            "overrides": {}
        }, controller.args[2], "AdvSearchDropDown11"));
        flxDropDownDetail11.add(AdvSearchDropDown11);
        flxColumn11.add(lblCol11, flxDropDown11, flxDropDownDetail11);
        var flxColumn02 = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
            "clipBounds": true,
            "id": "flxColumn02",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "left": "3.50%",
            "isModalContainer": false,
            "skin": "slFbox",
            "top": "1dp",
            "width": "31%",
            "zIndex": 5
        }, controller.args[0], "flxColumn02"), extendConfig({}, controller.args[1], "flxColumn02"), extendConfig({}, controller.args[2], "flxColumn02"));
        flxColumn02.setDefaultUnit(kony.flex.DP);
        var lblCol02 = new kony.ui.Label(extendConfig({
            "id": "lblCol02",
            "isVisible": true,
            "left": "0dp",
            "skin": "sknlblLatoRegular485c7512px",
            "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmCreateNewUser.lblCityUser\")",
            "top": "0px",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, controller.args[0], "lblCol02"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblCol02"), extendConfig({}, controller.args[2], "lblCol02"));
        var tbxSearchKey = new kony.ui.TextBox2(extendConfig({
            "autoCapitalize": constants.TEXTBOX_AUTO_CAPITALIZE_NONE,
            "focusSkin": "defTextBoxFocus",
            "height": "40dp",
            "id": "tbxSearchKey",
            "isVisible": true,
            "keyBoardStyle": constants.TEXTBOX_KEY_BOARD_STYLE_DEFAULT,
            "left": "0dp",
            "i18n_placeholder": "kony.i18n.getLocalizedString(\"i18n.common.city\")",
            "secureTextEntry": false,
            "skin": "skntbxLato35475f14px",
            "textInputMode": constants.TEXTBOX_INPUT_MODE_ANY,
            "top": "28dp",
            "width": "100%",
            "zIndex": 1
        }, controller.args[0], "tbxSearchKey"), extendConfig({
            "containerHeightMode": constants.TEXTBOX_FONT_METRICS_DRIVEN_HEIGHT,
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [3, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "tbxSearchKey"), extendConfig({
            "autoCorrect": false,
            "placeholderSkin": "defTextBoxPlaceholder"
        }, controller.args[2], "tbxSearchKey"));
        var flxTypeHeadCity = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
            "clipBounds": true,
            "id": "flxTypeHeadCity",
            "isVisible": false,
            "layoutType": kony.flex.FREE_FORM,
            "left": "0dp",
            "isModalContainer": false,
            "skin": "slFbox",
            "top": "28dp",
            "width": "100%"
        }, controller.args[0], "flxTypeHeadCity"), extendConfig({}, controller.args[1], "flxTypeHeadCity"), extendConfig({}, controller.args[2], "flxTypeHeadCity"));
        flxTypeHeadCity.setDefaultUnit(kony.flex.DP);
        var typeHeadCity = new com.adminConsole.common.typeHead(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "height": "180dp",
            "id": "typeHeadCity",
            "isVisible": false,
            "layoutType": kony.flex.FREE_FORM,
            "left": "0dp",
            "masterType": constants.MASTER_TYPE_DEFAULT,
            "isModalContainer": false,
            "skin": "slFbox",
            "top": "0dp",
            "width": "100%",
            "overrides": {
                "segSearchResult": {
                    "data": [{
                        "lblAddress": "country",
                        "lblPinIcon": ""
                    }, {
                        "lblAddress": "country",
                        "lblPinIcon": ""
                    }, {
                        "lblAddress": "country",
                        "lblPinIcon": ""
                    }, {
                        "lblAddress": "country",
                        "lblPinIcon": ""
                    }, {
                        "lblAddress": "country",
                        "lblPinIcon": ""
                    }],
                    "isVisible": false
                },
                "tbxSearchKey": {
                    "i18n_placeholder": "kony.i18n.getLocalizedString(\"i18n.frmGroupsController.city\")"
                },
                "typeHead": {
                    "height": "180dp",
                    "isVisible": false,
                    "top": "0dp"
                }
            }
        }, controller.args[0], "typeHeadCity"), extendConfig({
            "overrides": {}
        }, controller.args[1], "typeHeadCity"), extendConfig({
            "overrides": {}
        }, controller.args[2], "typeHeadCity"));
        flxTypeHeadCity.add(typeHeadCity);
        flxColumn02.add(lblCol02, tbxSearchKey, flxTypeHeadCity);
        flxRow1.add(flxColumn01, flxColumn11, flxColumn02);
        var flxRow2 = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "clipBounds": false,
            "height": "75px",
            "id": "flxRow2",
            "isVisible": true,
            "layoutType": kony.flex.FLOW_HORIZONTAL,
            "left": "0dp",
            "isModalContainer": false,
            "skin": "slFbox",
            "top": "20px",
            "width": "100%",
            "zIndex": 5
        }, controller.args[0], "flxRow2"), extendConfig({}, controller.args[1], "flxRow2"), extendConfig({}, controller.args[2], "flxRow2"));
        flxRow2.setDefaultUnit(kony.flex.DP);
        var flxColumn03 = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
            "clipBounds": false,
            "id": "flxColumn03",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "left": "0dp",
            "isModalContainer": false,
            "skin": "slFbox",
            "top": "1dp",
            "width": "31%",
            "zIndex": 1
        }, controller.args[0], "flxColumn03"), extendConfig({}, controller.args[1], "flxColumn03"), extendConfig({}, controller.args[2], "flxColumn03"));
        flxColumn03.setDefaultUnit(kony.flex.DP);
        var lblCol03 = new kony.ui.Label(extendConfig({
            "id": "lblCol03",
            "isVisible": true,
            "left": "0dp",
            "skin": "sknlblLatoRegular485c7512px",
            "i18n_text": "kony.i18n.getLocalizedString(\"i18n.userwidgetmodel.lblCol03\")",
            "top": "0px",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, controller.args[0], "lblCol03"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblCol03"), extendConfig({}, controller.args[2], "lblCol03"));
        var flxDropDown03 = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "clipBounds": false,
            "height": "41px",
            "id": "flxDropDown03",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "left": "0dp",
            "isModalContainer": false,
            "skin": "sknflxffffffoptemplateop3px",
            "top": "28dp",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, controller.args[0], "flxDropDown03"), extendConfig({}, controller.args[1], "flxDropDown03"), extendConfig({
            "hoverSkin": "sknFlxSegRowHover11abeb"
        }, controller.args[2], "flxDropDown03"));
        flxDropDown03.setDefaultUnit(kony.flex.DP);
        var lblSelectedRows03 = new kony.ui.Label(extendConfig({
            "centerY": "49%",
            "id": "lblSelectedRows03",
            "isVisible": true,
            "left": "12px",
            "right": "45dp",
            "skin": "sknlbl0h2ff0d6b13f947AD",
            "i18n_text": "kony.i18n.getLocalizedString(\"i18n.userwidgetmodel.lblSelectedRows03\")",
            "zIndex": 1
        }, controller.args[0], "lblSelectedRows03"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblSelectedRows03"), extendConfig({}, controller.args[2], "lblSelectedRows03"));
        var flxSearchDownImg03 = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "centerY": "50%",
            "clipBounds": false,
            "height": "12px",
            "id": "flxSearchDownImg03",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "isModalContainer": false,
            "right": "15px",
            "skin": "slFbox",
            "width": "15px",
            "zIndex": 1
        }, controller.args[0], "flxSearchDownImg03"), extendConfig({}, controller.args[1], "flxSearchDownImg03"), extendConfig({}, controller.args[2], "flxSearchDownImg03"));
        flxSearchDownImg03.setDefaultUnit(kony.flex.DP);
        var fontIconSearchDown03 = new kony.ui.Label(extendConfig({
            "centerY": "50%",
            "id": "fontIconSearchDown03",
            "isVisible": true,
            "left": "0dp",
            "skin": "sknfontIconListbox15px",
            "i18n_text": "kony.i18n.getLocalizedString(\"i18n.userwidgetmodel.fontIconSearchDown01\")",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 2
        }, controller.args[0], "fontIconSearchDown03"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "fontIconSearchDown03"), extendConfig({}, controller.args[2], "fontIconSearchDown03"));
        flxSearchDownImg03.add(fontIconSearchDown03);
        flxDropDown03.add(lblSelectedRows03, flxSearchDownImg03);
        var flxDropDownDetail03 = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "clipBounds": false,
            "height": "155px",
            "id": "flxDropDownDetail03",
            "isVisible": false,
            "layoutType": kony.flex.FREE_FORM,
            "left": "0dp",
            "isModalContainer": false,
            "skin": "skntxtbxNormald7d9e0",
            "top": "72px",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, controller.args[0], "flxDropDownDetail03"), extendConfig({}, controller.args[1], "flxDropDownDetail03"), extendConfig({
            "hoverSkin": "skntxtbxhover11abeb"
        }, controller.args[2], "flxDropDownDetail03"));
        flxDropDownDetail03.setDefaultUnit(kony.flex.DP);
        var AdvancedSearchDropDown03 = new com.adminConsole.Groups.AdvancedSearchDropDown(extendConfig({
            "height": "100%",
            "id": "AdvancedSearchDropDown03",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "left": "0dp",
            "masterType": constants.MASTER_TYPE_DEFAULT,
            "isModalContainer": false,
            "skin": "sknNormalDefault",
            "top": "0dp",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 100,
            "overrides": {
                "flxDropDown": {
                    "height": "150px"
                },
                "flxPopupBody": {
                    "height": "100px"
                },
                "richTexNoResult": {
                    "centerY": "50%",
                    "height": "30dp",
                    "top": "viz.val_cleared"
                },
                "sgmentData": {
                    "height": "100dp"
                }
            }
        }, controller.args[0], "AdvancedSearchDropDown03"), extendConfig({
            "overrides": {}
        }, controller.args[1], "AdvancedSearchDropDown03"), extendConfig({
            "overrides": {}
        }, controller.args[2], "AdvancedSearchDropDown03"));
        flxDropDownDetail03.add(AdvancedSearchDropDown03);
        flxColumn03.add(lblCol03, flxDropDown03, flxDropDownDetail03);
        var flxColumn12 = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
            "clipBounds": true,
            "id": "flxColumn12",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "left": "3.50%",
            "isModalContainer": false,
            "skin": "slFbox",
            "top": "0dp",
            "width": "31%",
            "zIndex": 1
        }, controller.args[0], "flxColumn12"), extendConfig({}, controller.args[1], "flxColumn12"), extendConfig({}, controller.args[2], "flxColumn12"));
        flxColumn12.setDefaultUnit(kony.flex.DP);
        var lblCol12 = new kony.ui.Label(extendConfig({
            "id": "lblCol12",
            "isVisible": true,
            "left": "0dp",
            "skin": "sknlblLatoRegular485c7512px",
            "i18n_text": "kony.i18n.getLocalizedString(\"i18n.Groups.Heading\")",
            "top": "0px",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, controller.args[0], "lblCol12"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblCol12"), extendConfig({}, controller.args[2], "lblCol12"));
        var flxDropDown12 = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "clipBounds": true,
            "height": "41px",
            "id": "flxDropDown12",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "left": "0dp",
            "isModalContainer": false,
            "skin": "sknflxffffffoptemplateop3px",
            "top": "28dp",
            "width": "99%",
            "zIndex": 1
        }, controller.args[0], "flxDropDown12"), extendConfig({}, controller.args[1], "flxDropDown12"), extendConfig({
            "hoverSkin": "sknFlxSegRowHover11abeb"
        }, controller.args[2], "flxDropDown12"));
        flxDropDown12.setDefaultUnit(kony.flex.DP);
        var lblSelectedRows12 = new kony.ui.Label(extendConfig({
            "centerY": "50%",
            "id": "lblSelectedRows12",
            "isVisible": true,
            "left": "12px",
            "right": "45dp",
            "skin": "sknlbl0h2ff0d6b13f947AD",
            "i18n_text": "kony.i18n.getLocalizedString(\"i18n.userwidgetmodel.lblSelectedRows12\")",
            "zIndex": 1
        }, controller.args[0], "lblSelectedRows12"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblSelectedRows12"), extendConfig({}, controller.args[2], "lblSelectedRows12"));
        var flxSearchDownImg12 = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "centerY": "50%",
            "clipBounds": true,
            "height": "12px",
            "id": "flxSearchDownImg12",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "isModalContainer": false,
            "right": "15px",
            "skin": "slFbox",
            "width": "15px",
            "zIndex": 1
        }, controller.args[0], "flxSearchDownImg12"), extendConfig({}, controller.args[1], "flxSearchDownImg12"), extendConfig({}, controller.args[2], "flxSearchDownImg12"));
        flxSearchDownImg12.setDefaultUnit(kony.flex.DP);
        var fontIconSearchDown12 = new kony.ui.Label(extendConfig({
            "centerY": "50%",
            "id": "fontIconSearchDown12",
            "isVisible": true,
            "left": "0dp",
            "skin": "sknfontIconListbox15px",
            "i18n_text": "kony.i18n.getLocalizedString(\"i18n.userwidgetmodel.fontIconSearchDown01\")",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 2
        }, controller.args[0], "fontIconSearchDown12"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "fontIconSearchDown12"), extendConfig({}, controller.args[2], "fontIconSearchDown12"));
        flxSearchDownImg12.add(fontIconSearchDown12);
        flxDropDown12.add(lblSelectedRows12, flxSearchDownImg12);
        var flxDropDownDetail12 = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "clipBounds": true,
            "height": "155px",
            "id": "flxDropDownDetail12",
            "isVisible": false,
            "layoutType": kony.flex.FREE_FORM,
            "left": "0dp",
            "isModalContainer": false,
            "skin": "skntxtbxNormald7d9e0",
            "top": "72px",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, controller.args[0], "flxDropDownDetail12"), extendConfig({}, controller.args[1], "flxDropDownDetail12"), extendConfig({
            "hoverSkin": "skntxtbxhover11abeb"
        }, controller.args[2], "flxDropDownDetail12"));
        flxDropDownDetail12.setDefaultUnit(kony.flex.DP);
        var AdvancedSearchDropDown12 = new com.adminConsole.Groups.AdvancedSearchDropDown(extendConfig({
            "height": "100%",
            "id": "AdvancedSearchDropDown12",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "left": "0dp",
            "masterType": constants.MASTER_TYPE_DEFAULT,
            "isModalContainer": false,
            "skin": "sknNormalDefault",
            "top": "0dp",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 100,
            "overrides": {
                "flxDropDown": {
                    "height": "150px"
                },
                "flxPopupBody": {
                    "height": "100px"
                },
                "richTexNoResult": {
                    "centerY": "50%",
                    "height": "30dp",
                    "top": "viz.val_cleared"
                },
                "sgmentData": {
                    "height": "100dp"
                }
            }
        }, controller.args[0], "AdvancedSearchDropDown12"), extendConfig({
            "overrides": {}
        }, controller.args[1], "AdvancedSearchDropDown12"), extendConfig({
            "overrides": {}
        }, controller.args[2], "AdvancedSearchDropDown12"));
        flxDropDownDetail12.add(AdvancedSearchDropDown12);
        flxColumn12.add(lblCol12, flxDropDown12, flxDropDownDetail12);
        var flxColumn13 = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
            "clipBounds": true,
            "id": "flxColumn13",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "left": "3.50%",
            "isModalContainer": false,
            "skin": "slFbox",
            "top": "0dp",
            "width": "31%",
            "zIndex": 1
        }, controller.args[0], "flxColumn13"), extendConfig({}, controller.args[1], "flxColumn13"), extendConfig({}, controller.args[2], "flxColumn13"));
        flxColumn13.setDefaultUnit(kony.flex.DP);
        var lblCol13 = new kony.ui.Label(extendConfig({
            "id": "lblCol13",
            "isVisible": true,
            "left": "0dp",
            "skin": "sknlblLatoRegular485c7512px",
            "i18n_text": "kony.i18n.getLocalizedString(\"i18n.userwidgetmodel.lblCol13\")",
            "top": "0px",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, controller.args[0], "lblCol13"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblCol13"), extendConfig({}, controller.args[2], "lblCol13"));
        var flxSelectFlags = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "clipBounds": false,
            "height": "41px",
            "id": "flxSelectFlags",
            "isVisible": true,
            "layoutType": kony.flex.FLOW_HORIZONTAL,
            "left": "0dp",
            "isModalContainer": false,
            "right": "0dp",
            "skin": "slFbox",
            "top": "28px",
            "width": "319dp",
            "zIndex": 111
        }, controller.args[0], "flxSelectFlags"), extendConfig({}, controller.args[1], "flxSelectFlags"), extendConfig({}, controller.args[2], "flxSelectFlags"));
        flxSelectFlags.setDefaultUnit(kony.flex.DP);
        var flxFlag1 = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "centerY": "50%",
            "clipBounds": true,
            "height": "13dp",
            "id": "flxFlag1",
            "isVisible": true,
            "layoutType": kony.flex.FLOW_HORIZONTAL,
            "left": "0px",
            "isModalContainer": false,
            "skin": "slFbox",
            "top": 15,
            "width": "13px",
            "zIndex": 2
        }, controller.args[0], "flxFlag1"), extendConfig({}, controller.args[1], "flxFlag1"), extendConfig({
            "hoverSkin": "sknCursor"
        }, controller.args[2], "flxFlag1"));
        flxFlag1.setDefaultUnit(kony.flex.DP);
        var imgFlag1 = new kony.ui.Image2(extendConfig({
            "centerX": "50%",
            "centerY": "50%",
            "height": "13dp",
            "id": "imgFlag1",
            "isVisible": true,
            "skin": "slImage",
            "src": "radionormal_2x.png",
            "width": "13dp",
            "zIndex": 1
        }, controller.args[0], "imgFlag1"), extendConfig({
            "imageScaleMode": constants.IMAGE_SCALE_MODE_FIT_TO_DIMENSIONS,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "imgFlag1"), extendConfig({
            "toolTip": "Active"
        }, controller.args[2], "imgFlag1"));
        flxFlag1.add(imgFlag1);
        var lblFlag1 = new kony.ui.Label(extendConfig({
            "centerY": "50%",
            "id": "lblFlag1",
            "isVisible": true,
            "left": "6px",
            "skin": "sknlblLatoBold35475f14px",
            "i18n_text": "kony.i18n.getLocalizedString(\"i18n.secureimage.Active\")",
            "top": "15px",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, controller.args[0], "lblFlag1"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblFlag1"), extendConfig({}, controller.args[2], "lblFlag1"));
        var flxFlag2 = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "centerY": "50%",
            "clipBounds": true,
            "height": "13dp",
            "id": "flxFlag2",
            "isVisible": true,
            "layoutType": kony.flex.FLOW_HORIZONTAL,
            "left": "20px",
            "isModalContainer": false,
            "skin": "slFbox",
            "top": "0dp",
            "width": "13px",
            "zIndex": 1
        }, controller.args[0], "flxFlag2"), extendConfig({}, controller.args[1], "flxFlag2"), extendConfig({
            "hoverSkin": "sknCursor"
        }, controller.args[2], "flxFlag2"));
        flxFlag2.setDefaultUnit(kony.flex.DP);
        var imgFlag2 = new kony.ui.Image2(extendConfig({
            "centerX": "50%",
            "centerY": "50%",
            "height": "13dp",
            "id": "imgFlag2",
            "isVisible": true,
            "skin": "slImage",
            "src": "radionormal_2x.png",
            "width": "13dp",
            "zIndex": 1
        }, controller.args[0], "imgFlag2"), extendConfig({
            "imageScaleMode": constants.IMAGE_SCALE_MODE_FIT_TO_DIMENSIONS,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "imgFlag2"), extendConfig({
            "toolTip": "Deactive"
        }, controller.args[2], "imgFlag2"));
        flxFlag2.add(imgFlag2);
        var lblFlag2 = new kony.ui.Label(extendConfig({
            "centerY": "50%",
            "id": "lblFlag2",
            "isVisible": true,
            "left": "6px",
            "skin": "sknlblLatoBold35475f14px",
            "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmCustomers.Suspended\")",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, controller.args[0], "lblFlag2"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblFlag2"), extendConfig({}, controller.args[2], "lblFlag2"));
        var flxFlag3 = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "centerY": "50%",
            "clipBounds": true,
            "height": "13dp",
            "id": "flxFlag3",
            "isVisible": true,
            "layoutType": kony.flex.FLOW_HORIZONTAL,
            "left": "20px",
            "isModalContainer": false,
            "skin": "slFbox",
            "top": "0dp",
            "width": "13px",
            "zIndex": 1
        }, controller.args[0], "flxFlag3"), extendConfig({}, controller.args[1], "flxFlag3"), extendConfig({
            "hoverSkin": "sknCursor"
        }, controller.args[2], "flxFlag3"));
        flxFlag3.setDefaultUnit(kony.flex.DP);
        var imgFlag3 = new kony.ui.Image2(extendConfig({
            "centerX": "50%",
            "centerY": "50%",
            "height": "13dp",
            "id": "imgFlag3",
            "isVisible": true,
            "left": "15dp",
            "skin": "slImage",
            "src": "radionormal_2x.png",
            "width": "13dp",
            "zIndex": 1
        }, controller.args[0], "imgFlag3"), extendConfig({
            "imageScaleMode": constants.IMAGE_SCALE_MODE_FIT_TO_DIMENSIONS,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "imgFlag3"), extendConfig({
            "toolTip": "Delete"
        }, controller.args[2], "imgFlag3"));
        flxFlag3.add(imgFlag3);
        var lblFlag3 = new kony.ui.Label(extendConfig({
            "centerY": "50%",
            "id": "lblFlag3",
            "isVisible": true,
            "left": "6px",
            "right": "20dp",
            "skin": "sknlblLatoBold35475f14px",
            "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmCustomerManagementController.Locked\")",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, controller.args[0], "lblFlag3"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblFlag3"), extendConfig({}, controller.args[2], "lblFlag3"));
        flxSelectFlags.add(flxFlag1, lblFlag1, flxFlag2, lblFlag2, flxFlag3, lblFlag3);
        flxColumn13.add(lblCol13, flxSelectFlags);
        flxRow2.add(flxColumn03, flxColumn12, flxColumn13);
        var flxRowHeader2 = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
            "clipBounds": false,
            "id": "flxRowHeader2",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "left": "0dp",
            "isModalContainer": false,
            "skin": "slFbox0d9c3974835234d",
            "top": "25px",
            "width": "100%",
            "zIndex": 1
        }, controller.args[0], "flxRowHeader2"), extendConfig({}, controller.args[1], "flxRowHeader2"), extendConfig({}, controller.args[2], "flxRowHeader2"));
        flxRowHeader2.setDefaultUnit(kony.flex.DP);
        var lblRowHeader2 = new kony.ui.Label(extendConfig({
            "id": "lblRowHeader2",
            "isVisible": true,
            "left": "0px",
            "skin": "sknlblLatoRegular484b5213px",
            "i18n_text": "kony.i18n.getLocalizedString(\"i18n.userwidgetmodel.lblRowHeader2\")",
            "top": "0px",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 2
        }, controller.args[0], "lblRowHeader2"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblRowHeader2"), extendConfig({}, controller.args[2], "lblRowHeader2"));
        flxRowHeader2.add(lblRowHeader2);
        var flxRow3 = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
            "clipBounds": false,
            "id": "flxRow3",
            "isVisible": true,
            "layoutType": kony.flex.FLOW_HORIZONTAL,
            "left": "0dp",
            "isModalContainer": false,
            "skin": "slFbox",
            "top": "20px",
            "width": "100%",
            "zIndex": 3
        }, controller.args[0], "flxRow3"), extendConfig({}, controller.args[1], "flxRow3"), extendConfig({}, controller.args[2], "flxRow3"));
        flxRow3.setDefaultUnit(kony.flex.DP);
        var flxColumn31 = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "clipBounds": true,
            "height": "75px",
            "id": "flxColumn31",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "left": "0px",
            "isModalContainer": false,
            "right": 0,
            "skin": "slFbox",
            "top": "0dp",
            "width": "15%",
            "zIndex": 1
        }, controller.args[0], "flxColumn31"), extendConfig({}, controller.args[1], "flxColumn31"), extendConfig({}, controller.args[2], "flxColumn31"));
        flxColumn31.setDefaultUnit(kony.flex.DP);
        var flxBefore = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "clipBounds": false,
            "height": "15dp",
            "id": "flxBefore",
            "isVisible": true,
            "layoutType": kony.flex.FLOW_HORIZONTAL,
            "left": "0px",
            "isModalContainer": false,
            "skin": "slFbox",
            "top": 0,
            "width": "15px",
            "zIndex": 2
        }, controller.args[0], "flxBefore"), extendConfig({}, controller.args[1], "flxBefore"), extendConfig({
            "hoverSkin": "sknCursor"
        }, controller.args[2], "flxBefore"));
        flxBefore.setDefaultUnit(kony.flex.DP);
        var imgBefore = new kony.ui.Image2(extendConfig({
            "centerX": "50%",
            "centerY": "50%",
            "height": "13dp",
            "id": "imgBefore",
            "isVisible": true,
            "skin": "slImage",
            "src": "radionormal_2x.png",
            "width": "13dp",
            "zIndex": 1
        }, controller.args[0], "imgBefore"), extendConfig({
            "imageScaleMode": constants.IMAGE_SCALE_MODE_FIT_TO_DIMENSIONS,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "imgBefore"), extendConfig({
            "toolTip": "Active"
        }, controller.args[2], "imgBefore"));
        flxBefore.add(imgBefore);
        var lblbefore = new kony.ui.Label(extendConfig({
            "id": "lblbefore",
            "isVisible": true,
            "left": "30px",
            "skin": "sknlblLatoBold35475f14px",
            "i18n_text": "kony.i18n.getLocalizedString(\"i18n.userwidgetmodel.lblbefore\")",
            "top": "0px",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, controller.args[0], "lblbefore"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblbefore"), extendConfig({}, controller.args[2], "lblbefore"));
        flxColumn31.add(flxBefore, lblbefore);
        var flxColumn32 = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "clipBounds": true,
            "height": "75px",
            "id": "flxColumn32",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "left": "1%",
            "isModalContainer": false,
            "right": 0,
            "skin": "slFbox",
            "top": "0dp",
            "width": "15%",
            "zIndex": 1
        }, controller.args[0], "flxColumn32"), extendConfig({}, controller.args[1], "flxColumn32"), extendConfig({}, controller.args[2], "flxColumn32"));
        flxColumn32.setDefaultUnit(kony.flex.DP);
        var flxAfter = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "clipBounds": false,
            "height": "15dp",
            "id": "flxAfter",
            "isVisible": true,
            "layoutType": kony.flex.FLOW_HORIZONTAL,
            "left": "0%",
            "isModalContainer": false,
            "skin": "slFbox",
            "top": "0dp",
            "width": "15px",
            "zIndex": 1
        }, controller.args[0], "flxAfter"), extendConfig({}, controller.args[1], "flxAfter"), extendConfig({
            "hoverSkin": "sknCursor"
        }, controller.args[2], "flxAfter"));
        flxAfter.setDefaultUnit(kony.flex.DP);
        var imgAfter = new kony.ui.Image2(extendConfig({
            "centerX": "50%",
            "centerY": "50%",
            "height": "13dp",
            "id": "imgAfter",
            "isVisible": true,
            "skin": "slImage",
            "src": "radionormal_2x.png",
            "width": "13dp",
            "zIndex": 1
        }, controller.args[0], "imgAfter"), extendConfig({
            "imageScaleMode": constants.IMAGE_SCALE_MODE_FIT_TO_DIMENSIONS,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "imgAfter"), extendConfig({
            "toolTip": "Deactive"
        }, controller.args[2], "imgAfter"));
        flxAfter.add(imgAfter);
        var lblAfter = new kony.ui.Label(extendConfig({
            "id": "lblAfter",
            "isVisible": true,
            "left": "30px",
            "skin": "sknlblLatoBold35475f14px",
            "i18n_text": "kony.i18n.getLocalizedString(\"i18n.userwidgetmodel.lblAfter\")",
            "top": "0px",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, controller.args[0], "lblAfter"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblAfter"), extendConfig({}, controller.args[2], "lblAfter"));
        flxColumn32.add(flxAfter, lblAfter);
        var flxColumn33 = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "clipBounds": true,
            "height": "75px",
            "id": "flxColumn33",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "left": "3.50%",
            "isModalContainer": false,
            "right": 0,
            "skin": "slFbox",
            "top": "0dp",
            "width": "31%",
            "zIndex": 1
        }, controller.args[0], "flxColumn33"), extendConfig({}, controller.args[1], "flxColumn33"), extendConfig({}, controller.args[2], "flxColumn33"));
        flxColumn33.setDefaultUnit(kony.flex.DP);
        var flxBetween = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "clipBounds": false,
            "height": "15dp",
            "id": "flxBetween",
            "isVisible": true,
            "layoutType": kony.flex.FLOW_HORIZONTAL,
            "left": "5px",
            "isModalContainer": false,
            "skin": "slFbox",
            "top": "0dp",
            "width": "15px",
            "zIndex": 1
        }, controller.args[0], "flxBetween"), extendConfig({}, controller.args[1], "flxBetween"), extendConfig({
            "hoverSkin": "sknCursor"
        }, controller.args[2], "flxBetween"));
        flxBetween.setDefaultUnit(kony.flex.DP);
        var imgBetween = new kony.ui.Image2(extendConfig({
            "centerX": "50%",
            "centerY": "50%",
            "height": "13dp",
            "id": "imgBetween",
            "isVisible": true,
            "left": "15dp",
            "skin": "slImage",
            "src": "radionormal_2x.png",
            "width": "13dp",
            "zIndex": 1
        }, controller.args[0], "imgBetween"), extendConfig({
            "imageScaleMode": constants.IMAGE_SCALE_MODE_FIT_TO_DIMENSIONS,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "imgBetween"), extendConfig({
            "toolTip": "Delete"
        }, controller.args[2], "imgBetween"));
        flxBetween.add(imgBetween);
        var lblBetween = new kony.ui.Label(extendConfig({
            "id": "lblBetween",
            "isVisible": true,
            "left": "30px",
            "right": "20dp",
            "skin": "sknlblLatoBold35475f14px",
            "i18n_text": "kony.i18n.getLocalizedString(\"i18n.userwidgetmodel.lblBetween\")",
            "top": "0px",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, controller.args[0], "lblBetween"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblBetween"), extendConfig({}, controller.args[2], "lblBetween"));
        flxColumn33.add(flxBetween, lblBetween);
        var flxBankStaff = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
            "clipBounds": true,
            "id": "flxBankStaff",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "left": "3.50%",
            "isModalContainer": false,
            "skin": "slFbox",
            "top": "0dp",
            "width": "31%",
            "zIndex": 1
        }, controller.args[0], "flxBankStaff"), extendConfig({}, controller.args[1], "flxBankStaff"), extendConfig({}, controller.args[2], "flxBankStaff"));
        flxBankStaff.setDefaultUnit(kony.flex.DP);
        var lblisBankStaff = new kony.ui.Label(extendConfig({
            "id": "lblisBankStaff",
            "isVisible": true,
            "left": "0dp",
            "skin": "sknlblLatoRegular485c7512px",
            "i18n_text": "kony.i18n.getLocalizedString(\"i18n.Groups.IsBankStaff\")",
            "top": "0px",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, controller.args[0], "lblisBankStaff"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblisBankStaff"), extendConfig({}, controller.args[2], "lblisBankStaff"));
        var flxBankStaffSwitch = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "clipBounds": false,
            "height": "41px",
            "id": "flxBankStaffSwitch",
            "isVisible": true,
            "layoutType": kony.flex.FLOW_HORIZONTAL,
            "left": "0dp",
            "isModalContainer": false,
            "right": "0dp",
            "skin": "slFbox",
            "top": "28px",
            "width": "319dp",
            "zIndex": 111
        }, controller.args[0], "flxBankStaffSwitch"), extendConfig({}, controller.args[1], "flxBankStaffSwitch"), extendConfig({}, controller.args[2], "flxBankStaffSwitch"));
        flxBankStaffSwitch.setDefaultUnit(kony.flex.DP);
        var switchToggle = new kony.ui.Switch(extendConfig({
            "height": "25dp",
            "id": "switchToggle",
            "isVisible": true,
            "left": "0dp",
            "leftSideText": "ON",
            "rightSideText": "OFF",
            "selectedIndex": 0,
            "skin": "sknSwitchServiceManagement",
            "top": "0px",
            "width": "38dp",
            "zIndex": 1
        }, controller.args[0], "switchToggle"), extendConfig({
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "switchToggle"), extendConfig({}, controller.args[2], "switchToggle"));
        flxBankStaffSwitch.add(switchToggle);
        flxBankStaff.add(lblisBankStaff, flxBankStaffSwitch);
        flxRow3.add(flxColumn31, flxColumn32, flxColumn33, flxBankStaff);
        flxPopupBody.add(flxRow0, flxRowHeader1, flxRow1, flxRow2, flxRowHeader2, flxRow3);
        var flxPopUpButtons = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "bottom": "0px",
            "clipBounds": false,
            "height": "80px",
            "id": "flxPopUpButtons",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "left": "0px",
            "isModalContainer": false,
            "skin": "lFbox0he6e13dfc3b44eAD",
            "width": "100%",
            "zIndex": 1
        }, controller.args[0], "flxPopUpButtons"), extendConfig({}, controller.args[1], "flxPopUpButtons"), extendConfig({}, controller.args[2], "flxPopUpButtons"));
        flxPopUpButtons.setDefaultUnit(kony.flex.DP);
        var flxCommonButton = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "bottom": "0px",
            "clipBounds": false,
            "height": "80px",
            "id": "flxCommonButton",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "left": "20px",
            "isModalContainer": false,
            "right": "20px",
            "skin": "lFbox0he6e13dfc3b44eAD",
            "zIndex": 1
        }, controller.args[0], "flxCommonButton"), extendConfig({}, controller.args[1], "flxCommonButton"), extendConfig({}, controller.args[2], "flxCommonButton"));
        flxCommonButton.setDefaultUnit(kony.flex.DP);
        var commonButtons = new com.adminConsole.common.commonButtons(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "height": "80px",
            "id": "commonButtons",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "left": "0px",
            "masterType": constants.MASTER_TYPE_DEFAULT,
            "isModalContainer": false,
            "skin": "slFbox",
            "top": "0dp",
            "width": "100%",
            "zIndex": 20,
            "overrides": {
                "btnCancel": {
                    "i18n_text": "kony.i18n.getLocalizedString(\"i18n.secureimage.CANCEL\")"
                },
                "btnNext": {
                    "text": "RESET"
                },
                "btnSave": {
                    "text": "SEARCH"
                },
                "commonButtons": {
                    "left": "0px",
                    "right": "viz.val_cleared",
                    "top": "0dp",
                    "width": "100%",
                    "zIndex": 20
                },
                "flxRightButtons": {
                    "right": "10px"
                }
            }
        }, controller.args[0], "commonButtons"), extendConfig({
            "overrides": {}
        }, controller.args[1], "commonButtons"), extendConfig({
            "overrides": {}
        }, controller.args[2], "commonButtons"));
        flxCommonButton.add(commonButtons);
        flxPopUpButtons.add(flxCommonButton);
        flxPopUp.add(flxPopUpTopColor, flxPopupHeader, flxPopupBody, flxPopUpButtons);
        AdvancedSearch.add(flxPopUp);
        return AdvancedSearch;
    }
})