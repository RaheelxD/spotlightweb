define("com/adminConsole/Groups/AdvancedSearchDropDown/userAdvancedSearchDropDownController", function() {
    return {
        setCompFlowActions: function() {
            var scopeObj = this;
            this.view.tbxSearchBox.onTouchStart = function() {
                scopeObj.view.flxSearchContainer.skin = "sknFlxSegRowHover11abeb";
            };
            this.view.tbxSearchBox.onEndEditing = function() {
                scopeObj.view.flxSearchContainer.skin = "sknflxffffffBorderd6dbe7Radius4px";
            };
        },
        showSchedulerDetails: function() {
            var ScopeObj = this;
        }
    };
});
define("com/adminConsole/Groups/AdvancedSearchDropDown/AdvancedSearchDropDownControllerActions", {
    /*
        This is an auto generated file and any modifications to it may result in corruption of the action sequence.
    */
    AS_FlexContainer_ab2e67b6a33d41fb9a7bfe3ada231b88: function AS_FlexContainer_ab2e67b6a33d41fb9a7bfe3ada231b88(eventobject) {
        var self = this;
        this.setCompFlowActions();
    }
});
define("com/adminConsole/Groups/AdvancedSearchDropDown/AdvancedSearchDropDownController", ["com/adminConsole/Groups/AdvancedSearchDropDown/userAdvancedSearchDropDownController", "com/adminConsole/Groups/AdvancedSearchDropDown/AdvancedSearchDropDownControllerActions"], function() {
    var controller = require("com/adminConsole/Groups/AdvancedSearchDropDown/userAdvancedSearchDropDownController");
    var actions = require("com/adminConsole/Groups/AdvancedSearchDropDown/AdvancedSearchDropDownControllerActions");
    for (var key in actions) {
        controller[key] = actions[key];
    }
    return controller;
});
