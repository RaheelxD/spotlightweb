define(function() {
    return function(controller) {
        var tableView = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "clipBounds": true,
            "isMaster": true,
            "height": "300dp",
            "id": "tableView",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "isModalContainer": false,
            "skin": "slFbox",
            "width": "100%"
        }, controller.args[0], "tableView"), extendConfig({}, controller.args[1], "tableView"), extendConfig({}, controller.args[2], "tableView"));
        tableView.setDefaultUnit(kony.flex.DP);
        var flxHeaders = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "clipBounds": true,
            "height": "60dp",
            "id": "flxHeaders",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "left": "0dp",
            "isModalContainer": false,
            "skin": "sknflxffffffop100",
            "top": "0dp",
            "width": "100%",
            "zIndex": 1
        }, controller.args[0], "flxHeaders"), extendConfig({}, controller.args[1], "flxHeaders"), extendConfig({}, controller.args[2], "flxHeaders"));
        flxHeaders.setDefaultUnit(kony.flex.DP);
        var flxHeaderLeft = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "clipBounds": true,
            "height": "60dp",
            "id": "flxHeaderLeft",
            "isVisible": true,
            "layoutType": kony.flex.FLOW_HORIZONTAL,
            "left": "0dp",
            "isModalContainer": false,
            "skin": "slFbox",
            "top": "0dp",
            "width": "30%",
            "zIndex": 2
        }, controller.args[0], "flxHeaderLeft"), extendConfig({}, controller.args[1], "flxHeaderLeft"), extendConfig({}, controller.args[2], "flxHeaderLeft"));
        flxHeaderLeft.setDefaultUnit(kony.flex.DP);
        var flxHeaderCheckbox = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "centerY": "50%",
            "clipBounds": true,
            "height": "30px",
            "id": "flxHeaderCheckbox",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "left": "35px",
            "isModalContainer": false,
            "skin": "sknCursor",
            "width": "30px",
            "zIndex": 2
        }, controller.args[0], "flxHeaderCheckbox"), extendConfig({}, controller.args[1], "flxHeaderCheckbox"), extendConfig({
            "hoverSkin": "sknCursor"
        }, controller.args[2], "flxHeaderCheckbox"));
        flxHeaderCheckbox.setDefaultUnit(kony.flex.DP);
        var imgHeaderCheckBox = new kony.ui.Image2(extendConfig({
            "centerX": "50%",
            "centerY": "50%",
            "height": "12px",
            "id": "imgHeaderCheckBox",
            "isVisible": true,
            "skin": "slImage",
            "src": "checkbox.png",
            "width": "12px",
            "zIndex": 2
        }, controller.args[0], "imgHeaderCheckBox"), extendConfig({
            "imageScaleMode": constants.IMAGE_SCALE_MODE_MAINTAIN_ASPECT_RATIO,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "imgHeaderCheckBox"), extendConfig({}, controller.args[2], "imgHeaderCheckBox"));
        flxHeaderCheckbox.add(imgHeaderCheckBox);
        var lblHeaderServiceName = new kony.ui.Label(extendConfig({
            "centerY": "50%",
            "id": "lblHeaderServiceName",
            "isVisible": true,
            "left": "55px",
            "skin": "sknlblLato696c7312px",
            "i18n_text": "kony.i18n.getLocalizedString(\"i18n.permission.NAME\")",
            "width": "50%",
            "zIndex": 1
        }, controller.args[0], "lblHeaderServiceName"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblHeaderServiceName"), extendConfig({}, controller.args[2], "lblHeaderServiceName"));
        flxHeaderLeft.add(flxHeaderCheckbox, lblHeaderServiceName);
        var lblHeaderDescription = new kony.ui.Label(extendConfig({
            "centerY": "50%",
            "id": "lblHeaderDescription",
            "isVisible": true,
            "left": "29%",
            "skin": "sknlblLato696c7312px",
            "i18n_text": "kony.i18n.getLocalizedString(\"i18n.Services.Supportedchannels\")",
            "width": "34%",
            "zIndex": 1
        }, controller.args[0], "lblHeaderDescription"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblHeaderDescription"), extendConfig({}, controller.args[2], "lblHeaderDescription"));
        var flxHeaderCategory = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "centerY": "50%",
            "clipBounds": true,
            "height": "60px",
            "id": "flxHeaderCategory",
            "isVisible": true,
            "layoutType": kony.flex.FLOW_HORIZONTAL,
            "left": "63.50%",
            "isModalContainer": false,
            "right": "60px",
            "skin": "slFbox",
            "width": "13%",
            "zIndex": 1
        }, controller.args[0], "flxHeaderCategory"), extendConfig({}, controller.args[1], "flxHeaderCategory"), extendConfig({}, controller.args[2], "flxHeaderCategory"));
        flxHeaderCategory.setDefaultUnit(kony.flex.DP);
        var lblHeaderCategory = new kony.ui.Label(extendConfig({
            "centerY": "50%",
            "id": "lblHeaderCategory",
            "isVisible": true,
            "left": "0px",
            "skin": "sknlblLato696c7312px",
            "i18n_text": "kony.i18n.getLocalizedString(\"i18n.Group.CATEGORY\")",
            "width": "70dp",
            "zIndex": 1
        }, controller.args[0], "lblHeaderCategory"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblHeaderCategory"), extendConfig({
            "hoverSkin": "sknlblLatoBold00000012px"
        }, controller.args[2], "lblHeaderCategory"));
        var flxheaderCategoryStatus = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "centerY": "50%",
            "clipBounds": true,
            "height": "12px",
            "id": "flxheaderCategoryStatus",
            "isVisible": true,
            "layoutType": kony.flex.FLOW_HORIZONTAL,
            "isModalContainer": false,
            "right": "10px",
            "skin": "sknCursor",
            "width": "12px"
        }, controller.args[0], "flxheaderCategoryStatus"), extendConfig({}, controller.args[1], "flxheaderCategoryStatus"), extendConfig({}, controller.args[2], "flxheaderCategoryStatus"));
        flxheaderCategoryStatus.setDefaultUnit(kony.flex.DP);
        var imgHeaderCategoryStatus = new kony.ui.Image2(extendConfig({
            "centerY": "50%",
            "height": "12dp",
            "id": "imgHeaderCategoryStatus",
            "isVisible": false,
            "right": "10px",
            "skin": "slImage",
            "src": "u195.png",
            "width": "12dp",
            "zIndex": 1
        }, controller.args[0], "imgHeaderCategoryStatus"), extendConfig({
            "imageScaleMode": constants.IMAGE_SCALE_MODE_MAINTAIN_ASPECT_RATIO,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "imgHeaderCategoryStatus"), extendConfig({}, controller.args[2], "imgHeaderCategoryStatus"));
        var fonticonHeaderCategory = new kony.ui.Label(extendConfig({
            "centerY": "50%",
            "id": "fonticonHeaderCategory",
            "isVisible": true,
            "right": "10px",
            "skin": "sknIcon15px",
            "i18n_text": "kony.i18n.getLocalizedString(\"i18n.userwidgetmodel.lblIconAssignedTo\")",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, controller.args[0], "fonticonHeaderCategory"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "fonticonHeaderCategory"), extendConfig({}, controller.args[2], "fonticonHeaderCategory"));
        flxheaderCategoryStatus.add(imgHeaderCategoryStatus, fonticonHeaderCategory);
        flxHeaderCategory.add(lblHeaderCategory, flxheaderCategoryStatus);
        var flxHeaderStatus = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "centerY": "50%",
            "clipBounds": true,
            "height": "60px",
            "id": "flxHeaderStatus",
            "isVisible": true,
            "layoutType": kony.flex.FLOW_HORIZONTAL,
            "isModalContainer": false,
            "right": "70px",
            "skin": "slFbox",
            "width": "130px",
            "zIndex": 1
        }, controller.args[0], "flxHeaderStatus"), extendConfig({}, controller.args[1], "flxHeaderStatus"), extendConfig({}, controller.args[2], "flxHeaderStatus"));
        flxHeaderStatus.setDefaultUnit(kony.flex.DP);
        var lblHeaderServiceStatus = new kony.ui.Label(extendConfig({
            "centerY": "50%",
            "id": "lblHeaderServiceStatus",
            "isVisible": true,
            "left": "10px",
            "skin": "sknlblLato696c7312px",
            "i18n_text": "kony.i18n.getLocalizedString(\"i18n.roles.STATUS\")",
            "width": "60dp",
            "zIndex": 1
        }, controller.args[0], "lblHeaderServiceStatus"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblHeaderServiceStatus"), extendConfig({
            "hoverSkin": "sknlblLatoBold00000012px"
        }, controller.args[2], "lblHeaderServiceStatus"));
        var flxHeaderStatus1 = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "centerY": "50%",
            "clipBounds": true,
            "height": "12px",
            "id": "flxHeaderStatus1",
            "isVisible": true,
            "layoutType": kony.flex.FLOW_HORIZONTAL,
            "isModalContainer": false,
            "right": "10px",
            "skin": "sknCursor",
            "width": "12px"
        }, controller.args[0], "flxHeaderStatus1"), extendConfig({}, controller.args[1], "flxHeaderStatus1"), extendConfig({}, controller.args[2], "flxHeaderStatus1"));
        flxHeaderStatus1.setDefaultUnit(kony.flex.DP);
        var imgHeaderServiceStatus = new kony.ui.Image2(extendConfig({
            "centerY": "50%",
            "height": "12dp",
            "id": "imgHeaderServiceStatus",
            "isVisible": false,
            "right": "10px",
            "skin": "slImage",
            "src": "u195.png",
            "width": "12dp",
            "zIndex": 1
        }, controller.args[0], "imgHeaderServiceStatus"), extendConfig({
            "imageScaleMode": constants.IMAGE_SCALE_MODE_MAINTAIN_ASPECT_RATIO,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "imgHeaderServiceStatus"), extendConfig({}, controller.args[2], "imgHeaderServiceStatus"));
        var fonticonHeaderServiceStatus = new kony.ui.Label(extendConfig({
            "centerY": "50%",
            "id": "fonticonHeaderServiceStatus",
            "isVisible": true,
            "right": "15px",
            "skin": "sknIcon15px",
            "i18n_text": "kony.i18n.getLocalizedString(\"i18n.userwidgetmodel.lblIconAssignedTo\")",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, controller.args[0], "fonticonHeaderServiceStatus"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "fonticonHeaderServiceStatus"), extendConfig({}, controller.args[2], "fonticonHeaderServiceStatus"));
        flxHeaderStatus1.add(imgHeaderServiceStatus, fonticonHeaderServiceStatus);
        flxHeaderStatus.add(lblHeaderServiceStatus, flxHeaderStatus1);
        var lblHeaderSeparator = new kony.ui.Label(extendConfig({
            "bottom": "0px",
            "centerX": "50%",
            "height": "1dp",
            "id": "lblHeaderSeparator",
            "isVisible": true,
            "left": "0dp",
            "skin": "sknLblTableHeaderLine",
            "i18n_text": "kony.i18n.getLocalizedString(\"i18n.userwidgetmodel.lblHeaderSeparator\")",
            "width": "96%",
            "zIndex": 2
        }, controller.args[0], "lblHeaderSeparator"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblHeaderSeparator"), extendConfig({}, controller.args[2], "lblHeaderSeparator"));
        flxHeaders.add(flxHeaderLeft, lblHeaderDescription, flxHeaderCategory, flxHeaderStatus, lblHeaderSeparator);
        var flxTableView = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
            "clipBounds": true,
            "id": "flxTableView",
            "isVisible": true,
            "layoutType": kony.flex.FLOW_VERTICAL,
            "left": "0dp",
            "isModalContainer": false,
            "right": "0dp",
            "skin": "slFbox",
            "top": "60dp",
            "zIndex": 1
        }, controller.args[0], "flxTableView"), extendConfig({}, controller.args[1], "flxTableView"), extendConfig({}, controller.args[2], "flxTableView"));
        flxTableView.setDefaultUnit(kony.flex.DP);
        var segServicesAndFaq = new kony.ui.SegmentedUI2(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "data": [{
                "ImgArrow": "imagedrag.png",
                "ImgCheckBox": "checkboxnormal.png",
                "imgOptions": "dots3x.png",
                "imgServiceStatus": "active_circle2x.png",
                "lblCategory": "Category",
                "lblDescription": "Mobile App",
                "lblIconOptions": "",
                "lblServiceName": "Jompay",
                "lblServiceStatus": "Active"
            }, {
                "ImgArrow": "imagedrag.png",
                "ImgCheckBox": "checkboxnormal.png",
                "imgOptions": "dots3x.png",
                "imgServiceStatus": "active_circle2x.png",
                "lblCategory": "Category",
                "lblDescription": "Mobile App",
                "lblIconOptions": "",
                "lblServiceName": "Jompay",
                "lblServiceStatus": "Active"
            }, {
                "ImgArrow": "imagedrag.png",
                "ImgCheckBox": "checkboxnormal.png",
                "imgOptions": "dots3x.png",
                "imgServiceStatus": "active_circle2x.png",
                "lblCategory": "Category",
                "lblDescription": "Mobile App",
                "lblIconOptions": "",
                "lblServiceName": "Jompay",
                "lblServiceStatus": "Active"
            }],
            "groupCells": false,
            "height": "200dp",
            "id": "segServicesAndFaq",
            "isVisible": true,
            "left": "0dp",
            "needPageIndicator": true,
            "pageOffDotImage": "pageoffdot.png",
            "pageOnDotImage": "pageondot.png",
            "retainSelection": false,
            "rowFocusSkin": "seg2Focus",
            "rowSkin": "seg2Normal",
            "rowTemplate": "flxFAQ",
            "sectionHeaderSkin": "sliPhoneSegmentHeader",
            "selectionBehavior": constants.SEGUI_MULTI_SELECT_BEHAVIOR,
            "selectionBehaviorConfig": {
                "imageIdentifier": "imgCheckBox",
                "selectedStateImage": "checkboxselected.png",
                "unselectedStateImage": "checkbox.png"
            },
            "separatorRequired": false,
            "showScrollbars": true,
            "top": "0px",
            "viewType": constants.SEGUI_VIEW_TYPE_TABLEVIEW,
            "widgetDataMap": {
                "ImgArrow": "ImgArrow",
                "ImgCheckBox": "ImgCheckBox",
                "flxArrow": "flxArrow",
                "flxCheckBox": "flxCheckBox",
                "flxFAQ": "flxFAQ",
                "flxOptions": "flxOptions",
                "flxStatus": "flxStatus",
                "imgOptions": "imgOptions",
                "imgServiceStatus": "imgServiceStatus",
                "lblCategory": "lblCategory",
                "lblDescription": "lblDescription",
                "lblIconOptions": "lblIconOptions",
                "lblServiceName": "lblServiceName",
                "lblServiceStatus": "lblServiceStatus"
            },
            "width": "100%",
            "zIndex": 1
        }, controller.args[0], "segServicesAndFaq"), extendConfig({
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "segServicesAndFaq"), extendConfig({}, controller.args[2], "segServicesAndFaq"));
        var flxPagination = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "bottom": 20,
            "clipBounds": false,
            "height": "30px",
            "id": "flxPagination",
            "isVisible": true,
            "layoutType": kony.flex.FLOW_HORIZONTAL,
            "isModalContainer": false,
            "right": "35dp",
            "skin": "slFbox0d9c3974835234d",
            "top": "15dp",
            "width": "222px",
            "zIndex": 1
        }, controller.args[0], "flxPagination"), extendConfig({}, controller.args[1], "flxPagination"), extendConfig({}, controller.args[2], "flxPagination"));
        flxPagination.setDefaultUnit(kony.flex.DP);
        var lbxPagination = new kony.ui.ListBox(extendConfig({
            "height": "30dp",
            "id": "lbxPagination",
            "isVisible": true,
            "left": "0dp",
            "masterData": [
                ["lb1", "Page 1 of 20"],
                ["lb2", "Page 2 of 20"],
                ["lb3", "Page 3 of 20"],
                ["lb4", "Page 4 of 20"]
            ],
            "onSelection": controller.AS_ListBox_c533dde7ca474e5ea4806827d1c7c737,
            "selectedKey": "lb1",
            "skin": "sknlbxNobgNoBorderPagination",
            "top": "0dp",
            "width": "120dp",
            "zIndex": 1
        }, controller.args[0], "lbxPagination"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lbxPagination"), extendConfig({
            "multiSelect": false
        }, controller.args[2], "lbxPagination"));
        var flxPaginationSeperator = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "clipBounds": true,
            "height": "100%",
            "id": "flxPaginationSeperator",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "left": "15dp",
            "isModalContainer": false,
            "skin": "sknflxd6dbe7",
            "top": "2dp",
            "width": "1px",
            "zIndex": 1
        }, controller.args[0], "flxPaginationSeperator"), extendConfig({}, controller.args[1], "flxPaginationSeperator"), extendConfig({}, controller.args[2], "flxPaginationSeperator"));
        flxPaginationSeperator.setDefaultUnit(kony.flex.DP);
        flxPaginationSeperator.add();
        var flxPrevious = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "centerY": "50%",
            "clipBounds": true,
            "height": "35dp",
            "id": "flxPrevious",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "left": "15dp",
            "isModalContainer": false,
            "onClick": controller.AS_FlexContainer_e988daccd7344cf5899e7b7cd377d80d,
            "skin": "sknflxRadius40px",
            "width": "35px",
            "zIndex": 1
        }, controller.args[0], "flxPrevious"), extendConfig({}, controller.args[1], "flxPrevious"), extendConfig({}, controller.args[2], "flxPrevious"));
        flxPrevious.setDefaultUnit(kony.flex.DP);
        var fontIconPrevious = new kony.ui.Label(extendConfig({
            "centerY": "50%",
            "id": "fontIconPrevious",
            "isVisible": true,
            "left": "0px",
            "skin": "sknFontIconPrevNextPage",
            "i18n_text": "kony.i18n.getLocalizedString(\"i18n.userwidgetmodel.fontIconPrevious\")",
            "top": "0dp",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, controller.args[0], "fontIconPrevious"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "fontIconPrevious"), extendConfig({}, controller.args[2], "fontIconPrevious"));
        flxPrevious.add(fontIconPrevious);
        var flxNext = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "centerY": "50%",
            "clipBounds": true,
            "height": "35dp",
            "id": "flxNext",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "left": "3dp",
            "isModalContainer": false,
            "onClick": controller.AS_FlexContainer_fb4a9fa4fe9d4519b4f48a7cd95bee8f,
            "skin": "sknflxRadius40px",
            "width": "35px",
            "zIndex": 1
        }, controller.args[0], "flxNext"), extendConfig({}, controller.args[1], "flxNext"), extendConfig({}, controller.args[2], "flxNext"));
        flxNext.setDefaultUnit(kony.flex.DP);
        var fontIconNext = new kony.ui.Label(extendConfig({
            "centerY": "50%",
            "id": "fontIconNext",
            "isVisible": true,
            "left": "0dp",
            "skin": "sknFontIconPrevNextPage",
            "i18n_text": "kony.i18n.getLocalizedString(\"i18n.userwidgetmodel.fontIconNext\")",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, controller.args[0], "fontIconNext"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "fontIconNext"), extendConfig({}, controller.args[2], "fontIconNext"));
        flxNext.add(fontIconNext);
        var imgPreviousArrow = new kony.ui.Image2(extendConfig({
            "height": "30px",
            "id": "imgPreviousArrow",
            "isVisible": false,
            "left": "15px",
            "onTouchStart": controller.AS_Image_j56563ebfafd4b3d80502fec0c98cba7,
            "skin": "slImage",
            "src": "rightarrow_circel2x.png",
            "top": "0px",
            "width": "30dp",
            "zIndex": 1
        }, controller.args[0], "imgPreviousArrow"), extendConfig({
            "imageScaleMode": constants.IMAGE_SCALE_MODE_MAINTAIN_ASPECT_RATIO,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "imgPreviousArrow"), extendConfig({}, controller.args[2], "imgPreviousArrow"));
        var imgNextArrow = new kony.ui.Image2(extendConfig({
            "height": "30dp",
            "id": "imgNextArrow",
            "isVisible": false,
            "left": "10dp",
            "onTouchStart": controller.AS_Image_ed3c712efdb8401b8975d8b184830e46,
            "skin": "slImage",
            "src": "left_cricle.png",
            "top": "0dp",
            "width": "30dp",
            "zIndex": 1
        }, controller.args[0], "imgNextArrow"), extendConfig({
            "imageScaleMode": constants.IMAGE_SCALE_MODE_MAINTAIN_ASPECT_RATIO,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "imgNextArrow"), extendConfig({}, controller.args[2], "imgNextArrow"));
        flxPagination.add(lbxPagination, flxPaginationSeperator, flxPrevious, flxNext, imgPreviousArrow, imgNextArrow);
        flxTableView.add(segServicesAndFaq, flxPagination);
        var flxNoRecordsFound = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "clipBounds": true,
            "height": "200dp",
            "id": "flxNoRecordsFound",
            "isVisible": false,
            "layoutType": kony.flex.FREE_FORM,
            "left": "0dp",
            "isModalContainer": false,
            "skin": "slFbox",
            "top": "0dp",
            "width": "100%",
            "zIndex": 1
        }, controller.args[0], "flxNoRecordsFound"), extendConfig({}, controller.args[1], "flxNoRecordsFound"), extendConfig({}, controller.args[2], "flxNoRecordsFound"));
        flxNoRecordsFound.setDefaultUnit(kony.flex.DP);
        var rtxNoRecords = new kony.ui.RichText(extendConfig({
            "centerX": "50%",
            "centerY": "50%",
            "id": "rtxNoRecords",
            "isVisible": true,
            "skin": "sknRtxLato84939e12Px",
            "i18n_text": "kony.i18n.getLocalizedString(\"i18n.userwidgetmodel.rtxNoRecords\")",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, controller.args[0], "rtxNoRecords"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "rtxNoRecords"), extendConfig({}, controller.args[2], "rtxNoRecords"));
        flxNoRecordsFound.add(rtxNoRecords);
        tableView.add(flxHeaders, flxTableView, flxNoRecordsFound);
        return tableView;
    }
})