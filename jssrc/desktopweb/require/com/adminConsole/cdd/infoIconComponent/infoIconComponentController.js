define("com/adminConsole/cdd/infoIconComponent/userinfoIconComponentController", function() {
    return {};
});
define("com/adminConsole/cdd/infoIconComponent/infoIconComponentControllerActions", {
    /*
          This is an auto generated file and any modifications to it may result in corruption of the action sequence.
        */
});
define("com/adminConsole/cdd/infoIconComponent/infoIconComponentController", ["com/adminConsole/cdd/infoIconComponent/userinfoIconComponentController", "com/adminConsole/cdd/infoIconComponent/infoIconComponentControllerActions"], function() {
    var controller = require("com/adminConsole/cdd/infoIconComponent/userinfoIconComponentController");
    var actions = require("com/adminConsole/cdd/infoIconComponent/infoIconComponentControllerActions");
    for (var key in actions) {
        controller[key] = actions[key];
    }
    return controller;
});
