define(function() {
    return function(controller) {
        var editMessages = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
            "clipBounds": true,
            "isMaster": true,
            "id": "editMessages",
            "isVisible": true,
            "layoutType": kony.flex.FLOW_VERTICAL,
            "left": "35dp",
            "isModalContainer": false,
            "right": "35dp",
            "skin": "sknflxffffffop0a",
            "top": "50dp"
        }, controller.args[0], "editMessages"), extendConfig({}, controller.args[1], "editMessages"), extendConfig({}, controller.args[2], "editMessages"));
        editMessages.setDefaultUnit(kony.flex.DP);
        var flxBack = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "clipBounds": true,
            "height": "50dp",
            "id": "flxBack",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "left": "0dp",
            "isModalContainer": false,
            "right": "0dp",
            "skin": "sknfbfcfc",
            "top": "0dp",
            "zIndex": 1
        }, controller.args[0], "flxBack"), extendConfig({}, controller.args[1], "flxBack"), extendConfig({}, controller.args[2], "flxBack"));
        flxBack.setDefaultUnit(kony.flex.DP);
        var backToPageHeader = new com.adminConsole.customerMang.backToPageHeader(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "height": "20px",
            "id": "backToPageHeader",
            "isVisible": true,
            "layoutType": kony.flex.FLOW_HORIZONTAL,
            "left": "0dp",
            "masterType": constants.MASTER_TYPE_DEFAULT,
            "isModalContainer": false,
            "skin": "slFbox",
            "top": "0dp",
            "width": "20%",
            "overrides": {
                "backToPageHeader": {
                    "centerX": "viz.val_cleared",
                    "centerY": "50%",
                    "width": "20%"
                }
            }
        }, controller.args[0], "backToPageHeader"), extendConfig({
            "overrides": {}
        }, controller.args[1], "backToPageHeader"), extendConfig({
            "overrides": {}
        }, controller.args[2], "backToPageHeader"));
        var btnReply = new kony.ui.Button(extendConfig({
            "centerY": "50%",
            "focusSkin": "sknbtnf7f7faLatoRegular12Px485c75Border1px485C75Radius20px",
            "height": "28dp",
            "id": "btnReply",
            "isVisible": true,
            "right": 35,
            "skin": "sknbtnf7f7faLatoRegular12Px485c75Border1px485C75Radius20px",
            "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmCSRController.Reply\")",
            "width": "70dp",
            "zIndex": 1
        }, controller.args[0], "btnReply"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_CENTER,
            "displayText": true,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "btnReply"), extendConfig({
            "hoverSkin": "sknbtnffffffLatoRegular12Px485c75Border1px485C75Radius20px"
        }, controller.args[2], "btnReply"));
        var btnAssign = new kony.ui.Button(extendConfig({
            "centerY": "50%",
            "focusSkin": "sknbtnf7f7faLatoRegular12Px485c75Border1px485C75Radius20px",
            "height": "28dp",
            "id": "btnAssign",
            "isVisible": true,
            "right": "120px",
            "skin": "sknbtnf7f7faLatoRegular12Px485c75Border1px485C75Radius20px",
            "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmCSRController.Assign\")",
            "width": "70dp",
            "zIndex": 1
        }, controller.args[0], "btnAssign"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_CENTER,
            "displayText": true,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "btnAssign"), extendConfig({
            "hoverSkin": "sknbtnffffffLatoRegular12Px485c75Border1px485C75Radius20px"
        }, controller.args[2], "btnAssign"));
        var lblAssign = new kony.ui.Label(extendConfig({
            "centerY": "50%",
            "id": "lblAssign",
            "isVisible": true,
            "right": "225px",
            "skin": "sknlblLatoBold35475f14px",
            "i18n_text": "kony.i18n.getLocalizedString(\"i18n.userwidgetmodel.lblAssign\")",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, controller.args[0], "lblAssign"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblAssign"), extendConfig({}, controller.args[2], "lblAssign"));
        var lblSeperator = new kony.ui.Label(extendConfig({
            "bottom": 0,
            "height": "1px",
            "id": "lblSeperator",
            "isVisible": true,
            "left": "0px",
            "right": "0px",
            "skin": "sknSeparator",
            "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmAlertsManagement.lblAlertTypeDescriptionCount\")",
            "zIndex": 1
        }, controller.args[0], "lblSeperator"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblSeperator"), extendConfig({}, controller.args[2], "lblSeperator"));
        flxBack.add(backToPageHeader, btnReply, btnAssign, lblAssign, lblSeperator);
        var flxHeading = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "clipBounds": true,
            "height": "50px",
            "id": "flxHeading",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "left": "0dp",
            "isModalContainer": false,
            "right": "0px",
            "skin": "sknflxffffffop0a",
            "top": "0dp",
            "zIndex": 1
        }, controller.args[0], "flxHeading"), extendConfig({}, controller.args[1], "flxHeading"), extendConfig({}, controller.args[2], "flxHeading"));
        flxHeading.setDefaultUnit(kony.flex.DP);
        var imgPermissionStatus = new kony.ui.Image2(extendConfig({
            "centerY": "50%",
            "height": "10dp",
            "id": "imgPermissionStatus",
            "isVisible": false,
            "left": "35dp",
            "skin": "slImage",
            "src": "active_circle2x.png",
            "width": "10dp",
            "zIndex": 1
        }, controller.args[0], "imgPermissionStatus"), extendConfig({
            "imageScaleMode": constants.IMAGE_SCALE_MODE_MAINTAIN_ASPECT_RATIO,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "imgPermissionStatus"), extendConfig({}, controller.args[2], "imgPermissionStatus"));
        var lblIconPermissionStatus = new kony.ui.Label(extendConfig({
            "centerY": "50%",
            "height": "15px",
            "id": "lblIconPermissionStatus",
            "isVisible": true,
            "left": "35px",
            "skin": "sknIcon13pxGray",
            "i18n_text": "kony.i18n.getLocalizedString(\"i18n.userwidgetmodel.lblFontIconStatus1\")",
            "width": "15px",
            "zIndex": 1
        }, controller.args[0], "lblIconPermissionStatus"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblIconPermissionStatus"), extendConfig({}, controller.args[2], "lblIconPermissionStatus"));
        var lblHeading = new kony.ui.Label(extendConfig({
            "centerY": "50%",
            "id": "lblHeading",
            "isVisible": true,
            "left": "50dp",
            "skin": "blLatoRegular18px",
            "i18n_text": "kony.i18n.getLocalizedString(\"i18n.userwidgetmodel.lblHeading\")",
            "top": "15px",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, controller.args[0], "lblHeading"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblHeading"), extendConfig({}, controller.args[2], "lblHeading"));
        var btnStatus = new kony.ui.Button(extendConfig({
            "centerY": "50%",
            "id": "btnStatus",
            "isVisible": true,
            "right": "35px",
            "skin": "sknBtnLato0b6689b17f1bf4f12px",
            "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmCSRController.Mark_as_IN_PROGRESS\")",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, controller.args[0], "btnStatus"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_CENTER,
            "displayText": true,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "btnStatus"), extendConfig({}, controller.args[2], "btnStatus"));
        var lblSeperator1 = new kony.ui.Label(extendConfig({
            "bottom": 0,
            "height": "1px",
            "id": "lblSeperator1",
            "isVisible": true,
            "left": "0px",
            "right": "0px",
            "skin": "sknSeparator",
            "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmAlertsManagement.lblAlertTypeDescriptionCount\")",
            "zIndex": 1
        }, controller.args[0], "lblSeperator1"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblSeperator1"), extendConfig({}, controller.args[2], "lblSeperator1"));
        flxHeading.add(imgPermissionStatus, lblIconPermissionStatus, lblHeading, btnStatus, lblSeperator1);
        var flxDraft = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
            "bottom": "40dp",
            "clipBounds": true,
            "id": "flxDraft",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "left": "0dp",
            "isModalContainer": false,
            "skin": "slFbox",
            "top": "0dp",
            "width": "100%",
            "zIndex": 1
        }, controller.args[0], "flxDraft"), extendConfig({}, controller.args[1], "flxDraft"), extendConfig({}, controller.args[2], "flxDraft"));
        flxDraft.setDefaultUnit(kony.flex.DP);
        var segMesgs = new kony.ui.SegmentedUI2(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
            "data": [{
                "btnDraft": "Draft",
                "imgAttatch": "attachment_2x.png",
                "lblAttach": "",
                "lblDate": "12 December 2017 09:30 a.m.",
                "lblName": "Nicky Paretla",
                "lblSeperator": "'",
                "rtxDescription": "\nLorem ipsum dolor sit amet, <br>\nconsectetur adipiscing elit......"
            }],
            "groupCells": false,
            "id": "segMesgs",
            "isVisible": true,
            "left": "35px",
            "needPageIndicator": true,
            "pageOffDotImage": "pageoffdot.png",
            "pageOnDotImage": "pageondot.png",
            "retainSelection": false,
            "right": "35px",
            "rowFocusSkin": "seg2Focus",
            "rowSkin": "seg2Normal",
            "rowTemplate": "flxMain",
            "sectionHeaderSkin": "sliPhoneSegmentHeader",
            "selectionBehavior": constants.SEGUI_DEFAULT_BEHAVIOR,
            "separatorRequired": false,
            "showScrollbars": false,
            "top": "1dp",
            "viewType": constants.SEGUI_VIEW_TYPE_TABLEVIEW,
            "widgetDataMap": {
                "btnDraft": "btnDraft",
                "flxAttatchment": "flxAttatchment",
                "flxHeader": "flxHeader",
                "flxHeading": "flxHeading",
                "flxMain": "flxMain",
                "imgAttatch": "imgAttatch",
                "lblAttach": "lblAttach",
                "lblDate": "lblDate",
                "lblName": "lblName",
                "lblSeperator": "lblSeperator",
                "rtxDescription": "rtxDescription"
            },
            "zIndex": 1
        }, controller.args[0], "segMesgs"), extendConfig({
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "segMesgs"), extendConfig({}, controller.args[2], "segMesgs"));
        flxDraft.add(segMesgs);
        var flxNomesg = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "clipBounds": true,
            "height": "150px",
            "id": "flxNomesg",
            "isVisible": false,
            "layoutType": kony.flex.FREE_FORM,
            "left": "20px",
            "isModalContainer": false,
            "skin": "slFbox",
            "top": "0dp",
            "width": "96%",
            "zIndex": 1
        }, controller.args[0], "flxNomesg"), extendConfig({}, controller.args[1], "flxNomesg"), extendConfig({}, controller.args[2], "flxNomesg"));
        flxNomesg.setDefaultUnit(kony.flex.DP);
        var rtxnomesg = new kony.ui.RichText(extendConfig({
            "bottom": "50px",
            "centerX": "50%",
            "centerY": "50%",
            "id": "rtxnomesg",
            "isVisible": true,
            "skin": "sknRtxLato84939e12Px",
            "i18n_text": "kony.i18n.getLocalizedString(\"i18n.userwidgetmodel.rtxnomesg\")",
            "width": "80%",
            "zIndex": 1
        }, controller.args[0], "rtxnomesg"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_CENTER,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "rtxnomesg"), extendConfig({}, controller.args[2], "rtxnomesg"));
        flxNomesg.add(rtxnomesg);
        editMessages.add(flxBack, flxHeading, flxDraft, flxNomesg);
        return editMessages;
    }
})