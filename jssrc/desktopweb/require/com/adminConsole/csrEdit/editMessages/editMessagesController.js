define("com/adminConsole/csrEdit/editMessages/usereditMessagesController", function() {
    return {};
});
define("com/adminConsole/csrEdit/editMessages/editMessagesControllerActions", {
    /*
          This is an auto generated file and any modifications to it may result in corruption of the action sequence.
        */
});
define("com/adminConsole/csrEdit/editMessages/editMessagesController", ["com/adminConsole/csrEdit/editMessages/usereditMessagesController", "com/adminConsole/csrEdit/editMessages/editMessagesControllerActions"], function() {
    var controller = require("com/adminConsole/csrEdit/editMessages/usereditMessagesController");
    var actions = require("com/adminConsole/csrEdit/editMessages/editMessagesControllerActions");
    for (var key in actions) {
        controller[key] = actions[key];
    }
    return controller;
});
