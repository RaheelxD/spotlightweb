define(function() {
    return function(controller) {
        var dropdownMainHeader = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
            "clipBounds": true,
            "isMaster": true,
            "id": "dropdownMainHeader",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "left": "0dp",
            "isModalContainer": false,
            "preShow": function(eventobject) {
                controller.AS_FlexContainer_cdc847d47b7a4303824c23e2dde9002a(eventobject);
            },
            "skin": "slFbox",
            "top": "0dp",
            "width": "100%"
        }, controller.args[0], "dropdownMainHeader"), extendConfig({}, controller.args[1], "dropdownMainHeader"), extendConfig({}, controller.args[2], "dropdownMainHeader"));
        dropdownMainHeader.setDefaultUnit(kony.flex.DP);
        var flxHeaderUserOptions = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "clipBounds": true,
            "height": "25dp",
            "id": "flxHeaderUserOptions",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "isModalContainer": false,
            "right": "0px",
            "skin": "sknflxf5f6f8Op100",
            "top": "0dp",
            "width": "100%",
            "zIndex": 2
        }, controller.args[0], "flxHeaderUserOptions"), extendConfig({}, controller.args[1], "flxHeaderUserOptions"), extendConfig({}, controller.args[2], "flxHeaderUserOptions"));
        flxHeaderUserOptions.setDefaultUnit(kony.flex.DP);
        var lblUserName = new kony.ui.Label(extendConfig({
            "centerY": "50%",
            "id": "lblUserName",
            "isVisible": true,
            "right": 40,
            "skin": "slLabel0d21bb75081954f",
            "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmCreateNewUser.lblUser\")",
            "top": "3dp",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, controller.args[0], "lblUserName"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblUserName"), extendConfig({
            "hoverSkin": "sknLblLatoRegular42424213pxHoverCursor"
        }, controller.args[2], "lblUserName"));
        var lblIconDownArrow = new kony.ui.Label(extendConfig({
            "centerY": "50%",
            "id": "lblIconDownArrow",
            "isVisible": true,
            "right": 17,
            "skin": "sknFontIconBreadcrumb",
            "text": "",
            "top": "3dp",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, controller.args[0], "lblIconDownArrow"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblIconDownArrow"), extendConfig({
            "hoverSkin": "sknFontIconBreadcrumbHoverCursor"
        }, controller.args[2], "lblIconDownArrow"));
        flxHeaderUserOptions.add(lblUserName, lblIconDownArrow);
        var flxDropdown = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "clipBounds": true,
            "height": "133dp",
            "id": "flxDropdown",
            "isVisible": false,
            "layoutType": kony.flex.FREE_FORM,
            "isModalContainer": false,
            "right": "0dp",
            "skin": "slFbox",
            "top": "0dp",
            "width": "205dp",
            "zIndex": 1
        }, controller.args[0], "flxDropdown"), extendConfig({}, controller.args[1], "flxDropdown"), extendConfig({}, controller.args[2], "flxDropdown"));
        flxDropdown.setDefaultUnit(kony.flex.DP);
        var flxArrowImage = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "clipBounds": true,
            "height": "12dp",
            "id": "flxArrowImage",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "left": "0dp",
            "isModalContainer": false,
            "skin": "slFbox",
            "top": "0dp",
            "width": "100%",
            "zIndex": 2
        }, controller.args[0], "flxArrowImage"), extendConfig({}, controller.args[1], "flxArrowImage"), extendConfig({}, controller.args[2], "flxArrowImage"));
        flxArrowImage.setDefaultUnit(kony.flex.DP);
        var imgUpArrow = new kony.ui.Image2(extendConfig({
            "height": "12dp",
            "id": "imgUpArrow",
            "isVisible": true,
            "right": "15dp",
            "skin": "slImage",
            "src": "uparrow_2x.png",
            "top": "0dp",
            "width": "15dp",
            "zIndex": 1
        }, controller.args[0], "imgUpArrow"), extendConfig({
            "imageScaleMode": constants.IMAGE_SCALE_MODE_FIT_TO_DIMENSIONS,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "imgUpArrow"), extendConfig({}, controller.args[2], "imgUpArrow"));
        flxArrowImage.add(imgUpArrow);
        var flxOuterActions = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
            "clipBounds": true,
            "id": "flxOuterActions",
            "isVisible": true,
            "layoutType": kony.flex.FLOW_VERTICAL,
            "left": "0dp",
            "isModalContainer": false,
            "skin": "sknflxffffffop100dbdbe6Radius3px",
            "top": "11dp",
            "width": "100%",
            "zIndex": 1
        }, controller.args[0], "flxOuterActions"), extendConfig({}, controller.args[1], "flxOuterActions"), extendConfig({}, controller.args[2], "flxOuterActions"));
        flxOuterActions.setDefaultUnit(kony.flex.DP);
        var flxChangePassword = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "clipBounds": true,
            "focusSkin": "sknflxffffffop100",
            "height": "35dp",
            "id": "flxChangePassword",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "left": "0",
            "isModalContainer": false,
            "skin": "sknflxffffffop100",
            "top": "15dp",
            "width": "100%"
        }, controller.args[0], "flxChangePassword"), extendConfig({}, controller.args[1], "flxChangePassword"), extendConfig({
            "hoverSkin": "sknContextualMenuEntryHover2"
        }, controller.args[2], "flxChangePassword"));
        flxChangePassword.setDefaultUnit(kony.flex.DP);
        var flxIconChangePswd = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "centerY": "50%",
            "clipBounds": true,
            "height": "20px",
            "id": "flxIconChangePswd",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "left": "20px",
            "isModalContainer": false,
            "skin": "slFbox",
            "width": "20px",
            "zIndex": 2
        }, controller.args[0], "flxIconChangePswd"), extendConfig({}, controller.args[1], "flxIconChangePswd"), extendConfig({}, controller.args[2], "flxIconChangePswd"));
        flxIconChangePswd.setDefaultUnit(kony.flex.DP);
        var lblIconChangePswrd = new kony.ui.Label(extendConfig({
            "centerY": "50%",
            "id": "lblIconChangePswrd",
            "isVisible": true,
            "left": "0px",
            "skin": "sknIcon696C73Sz20px",
            "text": "",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, controller.args[0], "lblIconChangePswrd"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblIconChangePswrd"), extendConfig({}, controller.args[2], "lblIconChangePswrd"));
        flxIconChangePswd.add(lblIconChangePswrd);
        var lblChangePswd = new kony.ui.Label(extendConfig({
            "centerY": "50%",
            "id": "lblChangePswd",
            "isVisible": true,
            "left": "55px",
            "skin": "sknlblLatoBold35475f14px",
            "i18n_text": "kony.i18n.getLocalizedString(\"i18n.userwidgetmodel.dropdownMainHeader.lblChangePswd\")",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, controller.args[0], "lblChangePswd"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblChangePswd"), extendConfig({}, controller.args[2], "lblChangePswd"));
        flxChangePassword.add(flxIconChangePswd, lblChangePswd);
        var flxSignOut = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "bottom": "10dp",
            "clipBounds": true,
            "focusSkin": "sknflxffffffop100",
            "height": "35dp",
            "id": "flxSignOut",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "left": "0",
            "isModalContainer": false,
            "skin": "sknflxffffffop100",
            "top": "0dp",
            "width": "100%"
        }, controller.args[0], "flxSignOut"), extendConfig({}, controller.args[1], "flxSignOut"), extendConfig({
            "hoverSkin": "sknContextualMenuEntryHover2"
        }, controller.args[2], "flxSignOut"));
        flxSignOut.setDefaultUnit(kony.flex.DP);
        var flxIconSignOut = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "centerY": "50%",
            "clipBounds": true,
            "height": "20px",
            "id": "flxIconSignOut",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "left": "20px",
            "isModalContainer": false,
            "skin": "slFbox",
            "width": "20px",
            "zIndex": 2
        }, controller.args[0], "flxIconSignOut"), extendConfig({}, controller.args[1], "flxIconSignOut"), extendConfig({}, controller.args[2], "flxIconSignOut"));
        flxIconSignOut.setDefaultUnit(kony.flex.DP);
        var lblIconSignOut = new kony.ui.Label(extendConfig({
            "centerY": "50%",
            "id": "lblIconSignOut",
            "isVisible": true,
            "left": "0px",
            "skin": "sknIcon696C73Sz20px",
            "text": "",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, controller.args[0], "lblIconSignOut"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblIconSignOut"), extendConfig({}, controller.args[2], "lblIconSignOut"));
        flxIconSignOut.add(lblIconSignOut);
        var lblSignOut = new kony.ui.Label(extendConfig({
            "centerY": "50%",
            "id": "lblSignOut",
            "isVisible": true,
            "left": "55px",
            "skin": "sknlblLatoBold35475f14px",
            "i18n_text": "kony.i18n.getLocalizedString(\"i18n.userwidgetmodel.dropdownMainHeader.lblSignOut\")",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, controller.args[0], "lblSignOut"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblSignOut"), extendConfig({}, controller.args[2], "lblSignOut"));
        flxSignOut.add(flxIconSignOut, lblSignOut);
        flxOuterActions.add(flxChangePassword, flxSignOut);
        flxDropdown.add(flxArrowImage, flxOuterActions);
        dropdownMainHeader.add(flxHeaderUserOptions, flxDropdown);
        return dropdownMainHeader;
    }
})