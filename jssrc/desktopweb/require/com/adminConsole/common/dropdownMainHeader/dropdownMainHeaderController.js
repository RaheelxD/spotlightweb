define("com/adminConsole/common/dropdownMainHeader/userdropdownMainHeaderController", function() {
    return {
        setActions: function() {
            var scopeObj = this;
            this.view.flxOuterActions.onHover = scopeObj.onHoverEventCallback;
            this.view.flxChangePassword.onClick = function() {
                var authModule = kony.mvc.MDAApplication.getSharedInstance().getModuleManager().getModule("AuthModule");
                authModule.presentationController.showChangePasswordScreen();
            };
            this.view.flxSignOut.onClick = function() {
                var authModule = kony.mvc.MDAApplication.getSharedInstance().getModuleManager().getModule("AuthModule");
                authModule.presentationController.doLogout();
            };
            this.view.lblIconDownArrow.onClick = function() {
                scopeObj.showDropDown();
            };
            this.view.lblUserName.onClick = function() {
                scopeObj.showDropDown();
            };
        },
        preShow: function() {
            this.setActions();
            this.view.flxDropdown.setVisibility(false);
            this.view.lblUserName.text = "Hello, " + kony.mvc.MDAApplication.getSharedInstance().appContext.userName;
            var authModule = kony.mvc.MDAApplication.getSharedInstance().getModuleManager().getModule("AuthModule");
            if (authModule) {
                authModule.presentationController.getLoginTypeConfiguration(this.onFetchLoginTypeSuccess, function onError() {});
            }
        },
        onFetchLoginTypeSuccess: function(response) {
            var iskeyCloakEnabled = response.isKeyCloakEnabled;
            this.view.flxChangePassword.setVisibility(!iskeyCloakEnabled);
        },
        showDropDown: function() {
            var self = this;
            var currForm = kony.application.getCurrentForm();
            if (currForm.dropdownMainHeader.flxDropdown.isVisible === true) {
                self.view.flxDropdown.setVisibility(false);
            } else {
                self.view.flxDropdown.setVisibility(true);
                self.view.flxDropdown.top = "28px";
            }
            currForm.forceLayout();
        },
        onHoverEventCallback: function(widget, context) {
            if (context.eventType === constants.ONHOVER_MOUSE_ENTER || context.eventType === constants.ONHOVER_MOUSE_MOVE) {
                widget.parent.setVisibility(true);
            } else if (context.eventType === constants.ONHOVER_MOUSE_LEAVE) {
                widget.parent.setVisibility(false);
                var currForm = kony.application.getCurrentForm();
                currForm.forceLayout();
            }
        },
    };
});
define("com/adminConsole/common/dropdownMainHeader/dropdownMainHeaderControllerActions", {
    /*
        This is an auto generated file and any modifications to it may result in corruption of the action sequence.
    */
    AS_FlexContainer_cdc847d47b7a4303824c23e2dde9002a: function AS_FlexContainer_cdc847d47b7a4303824c23e2dde9002a(eventobject) {
        var self = this;
        this.preShow();
    }
});
define("com/adminConsole/common/dropdownMainHeader/dropdownMainHeaderController", ["com/adminConsole/common/dropdownMainHeader/userdropdownMainHeaderController", "com/adminConsole/common/dropdownMainHeader/dropdownMainHeaderControllerActions"], function() {
    var controller = require("com/adminConsole/common/dropdownMainHeader/userdropdownMainHeaderController");
    var actions = require("com/adminConsole/common/dropdownMainHeader/dropdownMainHeaderControllerActions");
    for (var key in actions) {
        controller[key] = actions[key];
    }
    return controller;
});
