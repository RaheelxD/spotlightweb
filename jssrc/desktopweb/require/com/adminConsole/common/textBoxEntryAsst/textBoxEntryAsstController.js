define("com/adminConsole/common/textBoxEntryAsst/usertextBoxEntryAsstController", function() {
    return {};
});
define("com/adminConsole/common/textBoxEntryAsst/textBoxEntryAsstControllerActions", {
    /*
          This is an auto generated file and any modifications to it may result in corruption of the action sequence.
        */
});
define("com/adminConsole/common/textBoxEntryAsst/textBoxEntryAsstController", ["com/adminConsole/common/textBoxEntryAsst/usertextBoxEntryAsstController", "com/adminConsole/common/textBoxEntryAsst/textBoxEntryAsstControllerActions"], function() {
    var controller = require("com/adminConsole/common/textBoxEntryAsst/usertextBoxEntryAsstController");
    var actions = require("com/adminConsole/common/textBoxEntryAsst/textBoxEntryAsstControllerActions");
    for (var key in actions) {
        controller[key] = actions[key];
    }
    return controller;
});
