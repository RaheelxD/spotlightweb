define(function() {
    return function(controller) {
        var commonButtons = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "clipBounds": true,
            "isMaster": true,
            "height": "80px",
            "id": "commonButtons",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "left": "0dp",
            "isModalContainer": false,
            "skin": "slFbox",
            "top": "0dp",
            "width": "100%"
        }, controller.args[0], "commonButtons"), extendConfig({}, controller.args[1], "commonButtons"), extendConfig({}, controller.args[2], "commonButtons"));
        commonButtons.setDefaultUnit(kony.flex.DP);
        var btnCancel = new kony.ui.Button(extendConfig({
            "centerY": "50%",
            "focusSkin": "sknbtnf7f7faLatoRegular4f555dBorder1px8b96a5",
            "height": "40dp",
            "id": "btnCancel",
            "isVisible": true,
            "left": "0px",
            "skin": "sknbtnf7f7faLatoRegular4f555dBorder1px8b96a5",
            "i18n_text": "kony.i18n.getLocalizedString(\"i18n.permission.CANCEL\")",
            "top": "0%",
            "width": "100px",
            "zIndex": 2
        }, controller.args[0], "btnCancel"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_CENTER,
            "displayText": true,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "btnCancel"), extendConfig({
            "hoverSkin": "sknbtnffffffLatoRegular4f555dBorder1px485c75"
        }, controller.args[2], "btnCancel"));
        var flxRightButtons = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "centerY": "50%",
            "clipBounds": true,
            "height": "100%",
            "id": "flxRightButtons",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "isModalContainer": false,
            "right": "0px",
            "skin": "slFbox",
            "width": "220px",
            "zIndex": 1
        }, controller.args[0], "flxRightButtons"), extendConfig({}, controller.args[1], "flxRightButtons"), extendConfig({}, controller.args[2], "flxRightButtons"));
        flxRightButtons.setDefaultUnit(kony.flex.DP);
        var btnNext = new kony.ui.Button(extendConfig({
            "centerY": "50%",
            "focusSkin": "sknBtn4A77A0LatoRegular13pxFFFFFFRad20px",
            "height": "40dp",
            "id": "btnNext",
            "isVisible": true,
            "left": "0px",
            "minWidth": "100px",
            "skin": "sknBtn4A77A0LatoRegular13pxFFFFFFRad20px",
            "i18n_text": "kony.i18n.getLocalizedString(\"i18n.permission.NEXT\")",
            "top": "0%",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, controller.args[0], "btnNext"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_CENTER,
            "displayText": true,
            "padding": [8, 0, 8, 0],
            "paddingInPixel": false
        }, controller.args[1], "btnNext"), extendConfig({
            "hoverSkin": "sknBtn2D5982LatoRegular13pxFFFFFFRad20px"
        }, controller.args[2], "btnNext"));
        var btnSave = new kony.ui.Button(extendConfig({
            "centerY": "50%",
            "focusSkin": "sknBtn003E75LatoRegular13pxFFFFFFRad20px",
            "height": "40dp",
            "id": "btnSave",
            "isVisible": true,
            "right": "0px",
            "skin": "sknBtn003E75LatoRegular13pxFFFFFFRad20px",
            "i18n_text": "kony.i18n.getLocalizedString(\"i18n.permission.SAVE\")",
            "top": "0%",
            "width": "100px",
            "zIndex": 1
        }, controller.args[0], "btnSave"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_CENTER,
            "displayText": true,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "btnSave"), extendConfig({
            "hoverSkin": "sknBtn005198LatoRegular13pxFFFFFFRad20px"
        }, controller.args[2], "btnSave"));
        flxRightButtons.add(btnNext, btnSave);
        commonButtons.add(btnCancel, flxRightButtons);
        return commonButtons;
    }
})