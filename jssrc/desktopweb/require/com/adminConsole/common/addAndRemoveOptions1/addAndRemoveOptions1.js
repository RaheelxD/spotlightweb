define(function() {
    return function(controller) {
        var addAndRemoveOptions1 = new kony.ui.FlexContainer(extendConfig({
            "clipBounds": true,
            "isMaster": true,
            "height": "100%",
            "id": "addAndRemoveOptions1",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "left": "0dp",
            "isModalContainer": false,
            "skin": "slFbox",
            "top": "0dp",
            "width": "100%"
        }, controller.args[0], "addAndRemoveOptions1"), extendConfig({}, controller.args[1], "addAndRemoveOptions1"), extendConfig({}, controller.args[2], "addAndRemoveOptions1"));
        addAndRemoveOptions1.setDefaultUnit(kony.flex.DP);
        var flxAvailableOptionsWrapper = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "bottom": "0px",
            "clipBounds": true,
            "id": "flxAvailableOptionsWrapper",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "left": "0px",
            "isModalContainer": false,
            "skin": "slFbox",
            "top": "20px",
            "width": "57.14%",
            "zIndex": 1
        }, controller.args[0], "flxAvailableOptionsWrapper"), extendConfig({}, controller.args[1], "flxAvailableOptionsWrapper"), extendConfig({}, controller.args[2], "flxAvailableOptionsWrapper"));
        flxAvailableOptionsWrapper.setDefaultUnit(kony.flex.DP);
        var flxAvailableOptions = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "clipBounds": true,
            "height": "100%",
            "id": "flxAvailableOptions",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "left": "35px",
            "isModalContainer": false,
            "right": "18px",
            "skin": "slFbox",
            "top": "0px",
            "zIndex": 1
        }, controller.args[0], "flxAvailableOptions"), extendConfig({}, controller.args[1], "flxAvailableOptions"), extendConfig({}, controller.args[2], "flxAvailableOptions"));
        flxAvailableOptions.setDefaultUnit(kony.flex.DP);
        var lblAvailableOptionsHeading = new kony.ui.Label(extendConfig({
            "id": "lblAvailableOptionsHeading",
            "isVisible": true,
            "left": "0px",
            "skin": "sknlblLatoRegular00000013px",
            "i18n_text": "kony.i18n.getLocalizedString(\"i18n.userwidgetmodel.lblAvailableOptionsHeading\")",
            "top": 0,
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, controller.args[0], "lblAvailableOptionsHeading"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblAvailableOptionsHeading"), extendConfig({}, controller.args[2], "lblAvailableOptionsHeading"));
        var flxError = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "clipBounds": true,
            "height": "15dp",
            "id": "flxError",
            "isVisible": false,
            "layoutType": kony.flex.FREE_FORM,
            "left": "17%",
            "isModalContainer": false,
            "skin": "slFbox",
            "top": "0dp",
            "width": "40%",
            "zIndex": 1
        }, controller.args[0], "flxError"), extendConfig({}, controller.args[1], "flxError"), extendConfig({}, controller.args[2], "flxError"));
        flxError.setDefaultUnit(kony.flex.DP);
        var lblErrorIcon = new kony.ui.Label(extendConfig({
            "height": "15dp",
            "id": "lblErrorIcon",
            "isVisible": true,
            "left": "0dp",
            "skin": "sknErrorIcon",
            "text": "",
            "top": "0dp",
            "width": "15dp",
            "zIndex": 1
        }, controller.args[0], "lblErrorIcon"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblErrorIcon"), extendConfig({}, controller.args[2], "lblErrorIcon"));
        var lblErrorText = new kony.ui.Label(extendConfig({
            "height": "15dp",
            "id": "lblErrorText",
            "isVisible": true,
            "left": "15dp",
            "skin": "sknlblError",
            "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmCompanies.Select_atleast_one_account_to_proceed\")",
            "top": "0dp",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, controller.args[0], "lblErrorText"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblErrorText"), extendConfig({}, controller.args[2], "lblErrorText"));
        flxError.add(lblErrorIcon, lblErrorText);
        var btnSelectAll = new kony.ui.Button(extendConfig({
            "id": "btnSelectAll",
            "isVisible": true,
            "right": "0px",
            "skin": "contextuallinks",
            "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmGroupsController.Add_All\")",
            "top": 0,
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, controller.args[0], "btnSelectAll"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "displayText": true,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "btnSelectAll"), extendConfig({}, controller.args[2], "btnSelectAll"));
        var flxAvailableOptSearchSegment = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "bottom": "20dp",
            "clipBounds": true,
            "id": "flxAvailableOptSearchSegment",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "left": "0dp",
            "isModalContainer": false,
            "skin": "sknflxffffffBordere1e5ed4Px",
            "top": "30dp",
            "width": "100%"
        }, controller.args[0], "flxAvailableOptSearchSegment"), extendConfig({}, controller.args[1], "flxAvailableOptSearchSegment"), extendConfig({}, controller.args[2], "flxAvailableOptSearchSegment"));
        flxAvailableOptSearchSegment.setDefaultUnit(kony.flex.DP);
        var flxSearch = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "clipBounds": true,
            "height": "55px",
            "id": "flxSearch",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "left": "0px",
            "isModalContainer": false,
            "right": "0px",
            "skin": "sknflxf5f6f8Op100",
            "top": "0px",
            "width": "100%",
            "zIndex": 1
        }, controller.args[0], "flxSearch"), extendConfig({}, controller.args[1], "flxSearch"), extendConfig({}, controller.args[2], "flxSearch"));
        flxSearch.setDefaultUnit(kony.flex.DP);
        var flxSearchContainer = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "centerY": "50%",
            "clipBounds": true,
            "focusSkin": "sknflxBgffffffBorder1293ccRadius30Px",
            "height": "35px",
            "id": "flxSearchContainer",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "left": "10dp",
            "isModalContainer": false,
            "right": 10,
            "skin": "sknflxd5d9ddop100",
            "zIndex": 1
        }, controller.args[0], "flxSearchContainer"), extendConfig({}, controller.args[1], "flxSearchContainer"), extendConfig({}, controller.args[2], "flxSearchContainer"));
        flxSearchContainer.setDefaultUnit(kony.flex.DP);
        var flxSearchImage = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "clipBounds": true,
            "height": "100%",
            "id": "flxSearchImage",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "left": "0dp",
            "isModalContainer": false,
            "skin": "slFbox",
            "top": "0dp",
            "width": "35px",
            "zIndex": 1
        }, controller.args[0], "flxSearchImage"), extendConfig({}, controller.args[1], "flxSearchImage"), extendConfig({}, controller.args[2], "flxSearchImage"));
        flxSearchImage.setDefaultUnit(kony.flex.DP);
        var lblSearchIcon = new kony.ui.Label(extendConfig({
            "centerY": "50%",
            "height": "20dp",
            "id": "lblSearchIcon",
            "isVisible": true,
            "left": "14dp",
            "skin": "sknFontIconSearchImg20px",
            "i18n_text": "kony.i18n.getLocalizedString(\"i18n.userwidgetmodel.lblSearchIcon\")",
            "width": "20dp",
            "zIndex": 1
        }, controller.args[0], "lblSearchIcon"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblSearchIcon"), extendConfig({}, controller.args[2], "lblSearchIcon"));
        flxSearchImage.add(lblSearchIcon);
        var tbxSearchBox = new kony.ui.TextBox2(extendConfig({
            "autoCapitalize": constants.TEXTBOX_AUTO_CAPITALIZE_NONE,
            "centerY": "50%",
            "height": "100%",
            "id": "tbxSearchBox",
            "isVisible": true,
            "keyBoardStyle": constants.TEXTBOX_KEY_BOARD_STYLE_DEFAULT,
            "left": "32dp",
            "i18n_placeholder": "kony.i18n.getLocalizedString(\"i18n.permission.search\")",
            "right": "35px",
            "secureTextEntry": false,
            "skin": "skntbxffffffNoBorderlato35475f14px",
            "textInputMode": constants.TEXTBOX_INPUT_MODE_ANY,
            "zIndex": 1
        }, controller.args[0], "tbxSearchBox"), extendConfig({
            "containerHeightMode": constants.TEXTBOX_FONT_METRICS_DRIVEN_HEIGHT,
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [3, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "tbxSearchBox"), extendConfig({
            "autoCorrect": false
        }, controller.args[2], "tbxSearchBox"));
        var flxClearSearchImage = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "clipBounds": true,
            "height": "100%",
            "id": "flxClearSearchImage",
            "isVisible": false,
            "layoutType": kony.flex.FREE_FORM,
            "isModalContainer": false,
            "right": "0px",
            "skin": "slFbox",
            "top": "0dp",
            "width": "35px",
            "zIndex": 1
        }, controller.args[0], "flxClearSearchImage"), extendConfig({}, controller.args[1], "flxClearSearchImage"), extendConfig({
            "hoverSkin": "sknCursor"
        }, controller.args[2], "flxClearSearchImage"));
        flxClearSearchImage.setDefaultUnit(kony.flex.DP);
        var lblIconClose = new kony.ui.Label(extendConfig({
            "centerY": "49%",
            "id": "lblIconClose",
            "isVisible": true,
            "left": "4px",
            "skin": "sknFontIconSearchCross20px",
            "i18n_text": "kony.i18n.getLocalizedString(\"i18n.userwidgetmodel.lblClearSearch\")",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, controller.args[0], "lblIconClose"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblIconClose"), extendConfig({}, controller.args[2], "lblIconClose"));
        flxClearSearchImage.add(lblIconClose);
        flxSearchContainer.add(flxSearchImage, tbxSearchBox, flxClearSearchImage);
        var flxFilteredSearch = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "centerY": "50%",
            "clipBounds": true,
            "height": "35dp",
            "id": "flxFilteredSearch",
            "isVisible": false,
            "layoutType": kony.flex.FLOW_VERTICAL,
            "left": "10dp",
            "isModalContainer": false,
            "right": "10dp",
            "skin": "slFbox",
            "zIndex": 1
        }, controller.args[0], "flxFilteredSearch"), extendConfig({}, controller.args[1], "flxFilteredSearch"), extendConfig({}, controller.args[2], "flxFilteredSearch"));
        flxFilteredSearch.setDefaultUnit(kony.flex.DP);
        var flxSearchFilterTextBox = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "clipBounds": true,
            "height": "35dp",
            "id": "flxSearchFilterTextBox",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "left": "0dp",
            "isModalContainer": false,
            "skin": "slFbox",
            "top": "0dp",
            "width": "100%"
        }, controller.args[0], "flxSearchFilterTextBox"), extendConfig({}, controller.args[1], "flxSearchFilterTextBox"), extendConfig({}, controller.args[2], "flxSearchFilterTextBox"));
        flxSearchFilterTextBox.setDefaultUnit(kony.flex.DP);
        var flxSelectedType = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "clipBounds": true,
            "height": "35dp",
            "id": "flxSelectedType",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "left": "0dp",
            "isModalContainer": false,
            "skin": "sknFlxF5F6F8brD7D9E0rad30px",
            "top": "0dp",
            "width": "130dp",
            "zIndex": 1
        }, controller.args[0], "flxSelectedType"), extendConfig({}, controller.args[1], "flxSelectedType"), extendConfig({}, controller.args[2], "flxSelectedType"));
        flxSelectedType.setDefaultUnit(kony.flex.DP);
        var lblSelSearchType = new kony.ui.Label(extendConfig({
            "centerY": "50%",
            "id": "lblSelSearchType",
            "isVisible": true,
            "left": "15dp",
            "right": "40dp",
            "skin": "sknLbl485C75Font12pxKA",
            "text": "Customer ID",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, controller.args[0], "lblSelSearchType"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblSelSearchType"), extendConfig({}, controller.args[2], "lblSelSearchType"));
        var lblIconDropDown = new kony.ui.Label(extendConfig({
            "id": "lblIconDropDown",
            "isVisible": false,
            "right": "30dp",
            "skin": "sknIcon12pxBlack",
            "text": "",
            "top": "10dp",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, controller.args[0], "lblIconDropDown"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblIconDropDown"), extendConfig({}, controller.args[2], "lblIconDropDown"));
        flxSelectedType.add(lblSelSearchType, lblIconDropDown);
        var tbxFilterSearch = new kony.ui.TextBox2(extendConfig({
            "autoCapitalize": constants.TEXTBOX_AUTO_CAPITALIZE_NONE,
            "focusSkin": "defTextBoxFocus",
            "height": "35dp",
            "id": "tbxFilterSearch",
            "isVisible": true,
            "keyBoardStyle": constants.TEXTBOX_KEY_BOARD_STYLE_DEFAULT,
            "left": "95dp",
            "i18n_placeholder": "kony.i18n.getLocalizedString(\"i18n.frmCompanies.Search_accounts_by\")",
            "right": "0dp",
            "secureTextEntry": false,
            "skin": "sknTbxBgFFFFFFBrD7d9E01pxR30pxRight",
            "textInputMode": constants.TEXTBOX_INPUT_MODE_ANY,
            "top": "0dp",
            "zIndex": 3
        }, controller.args[0], "tbxFilterSearch"), extendConfig({
            "containerHeightMode": constants.TEXTBOX_FONT_METRICS_DRIVEN_HEIGHT,
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [3, 0, 1, 0],
            "paddingInPixel": false
        }, controller.args[1], "tbxFilterSearch"), extendConfig({
            "autoCorrect": false,
            "placeholderSkin": "defTextBoxPlaceholder"
        }, controller.args[2], "tbxFilterSearch"));
        var flxSearchClick = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "clipBounds": true,
            "height": "35dp",
            "id": "flxSearchClick",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "isModalContainer": false,
            "right": "0dp",
            "skin": "slFbox",
            "top": "0dp",
            "width": "40dp",
            "zIndex": 5
        }, controller.args[0], "flxSearchClick"), extendConfig({}, controller.args[1], "flxSearchClick"), extendConfig({
            "hoverSkin": "sknCursor"
        }, controller.args[2], "flxSearchClick"));
        flxSearchClick.setDefaultUnit(kony.flex.DP);
        var lblIconSearchBtn = new kony.ui.Label(extendConfig({
            "centerY": "50%",
            "id": "lblIconSearchBtn",
            "isVisible": true,
            "right": "12dp",
            "skin": "sknFontIconSearchImg20px",
            "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmGroups.fontIconSearchimg\")",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, controller.args[0], "lblIconSearchBtn"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblIconSearchBtn"), extendConfig({}, controller.args[2], "lblIconSearchBtn"));
        flxSearchClick.add(lblIconSearchBtn);
        var flxFilterSearchClose = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "clipBounds": true,
            "height": "35dp",
            "id": "flxFilterSearchClose",
            "isVisible": false,
            "layoutType": kony.flex.FREE_FORM,
            "isModalContainer": false,
            "right": "0dp",
            "skin": "slFbox",
            "top": "0dp",
            "width": "40dp",
            "zIndex": 5
        }, controller.args[0], "flxFilterSearchClose"), extendConfig({}, controller.args[1], "flxFilterSearchClose"), extendConfig({
            "hoverSkin": "sknCursor"
        }, controller.args[2], "flxFilterSearchClose"));
        flxFilterSearchClose.setDefaultUnit(kony.flex.DP);
        var lblFilterSearchClose = new kony.ui.Label(extendConfig({
            "centerY": "50%",
            "id": "lblFilterSearchClose",
            "isVisible": true,
            "right": "13dp",
            "skin": "sknIcon696C73sz16px",
            "text": "",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, controller.args[0], "lblFilterSearchClose"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblFilterSearchClose"), extendConfig({}, controller.args[2], "lblFilterSearchClose"));
        flxFilterSearchClose.add(lblFilterSearchClose);
        flxSearchFilterTextBox.add(flxSelectedType, tbxFilterSearch, flxSearchClick, flxFilterSearchClose);
        flxFilteredSearch.add(flxSearchFilterTextBox);
        flxSearch.add(flxSearchContainer, flxFilteredSearch);
        var flxSegSearchType = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "bottom": "5dp",
            "clipBounds": true,
            "height": "80dp",
            "id": "flxSegSearchType",
            "isVisible": false,
            "layoutType": kony.flex.FREE_FORM,
            "left": "20dp",
            "isModalContainer": false,
            "skin": "sknflxffffffop100Bordercbcdd1Radius3px",
            "top": "48dp",
            "width": "125dp",
            "zIndex": 2
        }, controller.args[0], "flxSegSearchType"), extendConfig({}, controller.args[1], "flxSegSearchType"), extendConfig({}, controller.args[2], "flxSegSearchType"));
        flxSegSearchType.setDefaultUnit(kony.flex.DP);
        var segSearchType = new kony.ui.SegmentedUI2(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
            "data": [{
                "lblValue": "Account Id"
            }, {
                "lblValue": "Tax Number"
            }],
            "groupCells": false,
            "id": "segSearchType",
            "isVisible": true,
            "left": "10dp",
            "needPageIndicator": true,
            "pageOffDotImage": "pageoffdot.png",
            "pageOnDotImage": "pageondot.png",
            "retainSelection": false,
            "right": "10dp",
            "rowFocusSkin": "seg2Focus",
            "rowSkin": "seg2Normal",
            "rowTemplate": "flxDropDown",
            "sectionHeaderSkin": "sliPhoneSegmentHeader",
            "selectionBehavior": constants.SEGUI_DEFAULT_BEHAVIOR,
            "separatorColor": "aaaaaa47",
            "separatorRequired": true,
            "separatorThickness": 1,
            "showScrollbars": false,
            "top": "0dp",
            "viewType": constants.SEGUI_VIEW_TYPE_TABLEVIEW,
            "widgetDataMap": {
                "flxDropDown": "flxDropDown",
                "flxDropDownContainer": "flxDropDownContainer",
                "lblValue": "lblValue"
            },
            "zIndex": 1
        }, controller.args[0], "segSearchType"), extendConfig({
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "segSearchType"), extendConfig({}, controller.args[2], "segSearchType"));
        flxSegSearchType.add(segSearchType);
        var flxAddOptionsSegment = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "bottom": "0px",
            "clipBounds": true,
            "id": "flxAddOptionsSegment",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "left": "0px",
            "isModalContainer": false,
            "skin": "sknFlxFFFFFF100O",
            "top": "55px",
            "width": "100%",
            "zIndex": 1
        }, controller.args[0], "flxAddOptionsSegment"), extendConfig({}, controller.args[1], "flxAddOptionsSegment"), extendConfig({}, controller.args[2], "flxAddOptionsSegment"));
        flxAddOptionsSegment.setDefaultUnit(kony.flex.DP);
        var segAddOptions = new kony.ui.SegmentedUI2(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "groupCells": false,
            "height": "100%",
            "id": "segAddOptions",
            "isVisible": true,
            "left": "0dp",
            "needPageIndicator": true,
            "pageOffDotImage": "pageoffdot.png",
            "pageOnDotImage": "pageondot.png",
            "retainSelection": false,
            "rowFocusSkin": "seg2Focus",
            "rowSkin": "seg2Normal",
            "sectionHeaderSkin": "sliPhoneSegmentHeader",
            "selectionBehavior": constants.SEGUI_DEFAULT_BEHAVIOR,
            "separatorRequired": false,
            "showScrollbars": true,
            "top": "0dp",
            "viewType": constants.SEGUI_VIEW_TYPE_TABLEVIEW,
            "widgetDataMap": {},
            "width": "100%",
            "zIndex": 1
        }, controller.args[0], "segAddOptions"), extendConfig({
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "segAddOptions"), extendConfig({}, controller.args[2], "segAddOptions"));
        var rtxAvailableOptionsMessage = new kony.ui.RichText(extendConfig({
            "centerX": "50%",
            "centerY": "50%",
            "id": "rtxAvailableOptionsMessage",
            "isVisible": true,
            "skin": "sknRtxLato84939e12Px",
            "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmCSR.richtextNoResult\")",
            "width": "80%",
            "zIndex": 1
        }, controller.args[0], "rtxAvailableOptionsMessage"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_CENTER,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "rtxAvailableOptionsMessage"), extendConfig({}, controller.args[2], "rtxAvailableOptionsMessage"));
        var flxAccountSearchLoading = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "clipBounds": true,
            "height": "100%",
            "id": "flxAccountSearchLoading",
            "isVisible": false,
            "layoutType": kony.flex.FREE_FORM,
            "left": "0px",
            "isModalContainer": false,
            "right": "0px",
            "skin": "sknLoadingBlur",
            "top": "0px",
            "width": "100%",
            "zIndex": 10
        }, controller.args[0], "flxAccountSearchLoading"), extendConfig({}, controller.args[1], "flxAccountSearchLoading"), extendConfig({}, controller.args[2], "flxAccountSearchLoading"));
        flxAccountSearchLoading.setDefaultUnit(kony.flex.DP);
        var flxImageContainerAccountSearch = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "centerX": "50%",
            "centerY": "50%",
            "clipBounds": true,
            "height": "75px",
            "id": "flxImageContainerAccountSearch",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "isModalContainer": false,
            "skin": "slFbox",
            "top": "250px",
            "width": "75px",
            "zIndex": 1
        }, controller.args[0], "flxImageContainerAccountSearch"), extendConfig({}, controller.args[1], "flxImageContainerAccountSearch"), extendConfig({}, controller.args[2], "flxImageContainerAccountSearch"));
        flxImageContainerAccountSearch.setDefaultUnit(kony.flex.DP);
        var imgLoadingAccountSearch = new kony.ui.Image2(extendConfig({
            "centerX": "50%",
            "centerY": "50%",
            "height": "50px",
            "id": "imgLoadingAccountSearch",
            "isVisible": true,
            "skin": "slImage",
            "src": "loadingscreenimage.gif",
            "width": "50px",
            "zIndex": 1
        }, controller.args[0], "imgLoadingAccountSearch"), extendConfig({
            "imageScaleMode": constants.IMAGE_SCALE_MODE_MAINTAIN_ASPECT_RATIO,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "imgLoadingAccountSearch"), extendConfig({}, controller.args[2], "imgLoadingAccountSearch"));
        flxImageContainerAccountSearch.add(imgLoadingAccountSearch);
        flxAccountSearchLoading.add(flxImageContainerAccountSearch);
        flxAddOptionsSegment.add(segAddOptions, rtxAvailableOptionsMessage, flxAccountSearchLoading);
        flxAvailableOptSearchSegment.add(flxSearch, flxSegSearchType, flxAddOptionsSegment);
        flxAvailableOptions.add(lblAvailableOptionsHeading, flxError, btnSelectAll, flxAvailableOptSearchSegment);
        flxAvailableOptionsWrapper.add(flxAvailableOptions);
        var flxSelectedOptionsWrapper = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "bottom": "0px",
            "clipBounds": true,
            "id": "flxSelectedOptionsWrapper",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "isModalContainer": false,
            "right": "0px",
            "skin": "slFbox",
            "top": "20px",
            "width": "42.85%",
            "zIndex": 1
        }, controller.args[0], "flxSelectedOptionsWrapper"), extendConfig({}, controller.args[1], "flxSelectedOptionsWrapper"), extendConfig({}, controller.args[2], "flxSelectedOptionsWrapper"));
        flxSelectedOptionsWrapper.setDefaultUnit(kony.flex.DP);
        var flxSelectedOptions = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "clipBounds": true,
            "height": "100%",
            "id": "flxSelectedOptions",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "left": "17px",
            "isModalContainer": false,
            "right": "35px",
            "skin": "slFbox",
            "top": "0px",
            "zIndex": 1
        }, controller.args[0], "flxSelectedOptions"), extendConfig({}, controller.args[1], "flxSelectedOptions"), extendConfig({}, controller.args[2], "flxSelectedOptions"));
        flxSelectedOptions.setDefaultUnit(kony.flex.DP);
        var lblSelectedOption = new kony.ui.Label(extendConfig({
            "id": "lblSelectedOption",
            "isVisible": true,
            "left": "0px",
            "skin": "sknlblLatoRegular00000013px",
            "i18n_text": "kony.i18n.getLocalizedString(\"i18n.permission.SelectedUsers\")",
            "top": "0px",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, controller.args[0], "lblSelectedOption"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblSelectedOption"), extendConfig({}, controller.args[2], "lblSelectedOption"));
        var btnRemoveAll = new kony.ui.Button(extendConfig({
            "id": "btnRemoveAll",
            "isVisible": true,
            "right": "0px",
            "skin": "contextuallinks",
            "i18n_text": "kony.i18n.getLocalizedString(\"i18n.SelectedOptions.RemoveAll\")",
            "top": "0px",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, controller.args[0], "btnRemoveAll"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "displayText": true,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "btnRemoveAll"), extendConfig({}, controller.args[2], "btnRemoveAll"));
        var flxSegSelectedOptions = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "bottom": "20px",
            "clipBounds": true,
            "id": "flxSegSelectedOptions",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "left": "0dp",
            "isModalContainer": false,
            "skin": "sknflxf9f9f9Border1pxRad4px",
            "top": "30dp",
            "width": "100%",
            "zIndex": 1
        }, controller.args[0], "flxSegSelectedOptions"), extendConfig({}, controller.args[1], "flxSegSelectedOptions"), extendConfig({}, controller.args[2], "flxSegSelectedOptions"));
        flxSegSelectedOptions.setDefaultUnit(kony.flex.DP);
        var segSelectedOptions = new kony.ui.SegmentedUI2(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "data": [{
                "imgClose": "",
                "lblOption": ""
            }],
            "groupCells": false,
            "height": "100%",
            "id": "segSelectedOptions",
            "isVisible": true,
            "left": "0px",
            "needPageIndicator": true,
            "pageOffDotImage": "pageoffdot.png",
            "pageOnDotImage": "pageondot.png",
            "retainSelection": false,
            "rowSkin": "sknsegf9f9f9Op100",
            "rowTemplate": "flxOptionAdded",
            "sectionHeaderSkin": "sliPhoneSegmentHeader0h3448bdd9d8941",
            "selectionBehavior": constants.SEGUI_DEFAULT_BEHAVIOR,
            "separatorRequired": false,
            "separatorThickness": 0,
            "showScrollbars": true,
            "top": "0px",
            "viewType": constants.SEGUI_VIEW_TYPE_TABLEVIEW,
            "widgetDataMap": {
                "flxAddOptionWrapper": "flxAddOptionWrapper",
                "flxClose": "flxClose",
                "flxOptionAdded": "flxOptionAdded",
                "imgClose": "imgClose",
                "lblOption": "lblOption"
            },
            "width": "100%",
            "zIndex": 1
        }, controller.args[0], "segSelectedOptions"), extendConfig({
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "segSelectedOptions"), extendConfig({}, controller.args[2], "segSelectedOptions"));
        var rtxSelectedOptionsMessage = new kony.ui.RichText(extendConfig({
            "centerX": "50%",
            "centerY": "50%",
            "id": "rtxSelectedOptionsMessage",
            "isVisible": true,
            "skin": "sknRtxLato84939e12Px",
            "i18n_text": "kony.i18n.getLocalizedString(\"i18n.userwidgetmodel.rtxSelectedOptionsMessage\")",
            "width": "80%",
            "zIndex": 1
        }, controller.args[0], "rtxSelectedOptionsMessage"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_CENTER,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "rtxSelectedOptionsMessage"), extendConfig({}, controller.args[2], "rtxSelectedOptionsMessage"));
        flxSegSelectedOptions.add(segSelectedOptions, rtxSelectedOptionsMessage);
        flxSelectedOptions.add(lblSelectedOption, btnRemoveAll, flxSegSelectedOptions);
        flxSelectedOptionsWrapper.add(flxSelectedOptions);
        addAndRemoveOptions1.add(flxAvailableOptionsWrapper, flxSelectedOptionsWrapper);
        return addAndRemoveOptions1;
    }
})