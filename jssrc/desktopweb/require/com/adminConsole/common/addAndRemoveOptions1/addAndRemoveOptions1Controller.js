define("com/adminConsole/common/addAndRemoveOptions1/useraddAndRemoveOptions1Controller", function() {
    return {};
});
define("com/adminConsole/common/addAndRemoveOptions1/addAndRemoveOptions1ControllerActions", {
    /*
      This is an auto generated file and any modifications to it may result in corruption of the action sequence.
    */
});
define("com/adminConsole/common/addAndRemoveOptions1/addAndRemoveOptions1Controller", ["com/adminConsole/common/addAndRemoveOptions1/useraddAndRemoveOptions1Controller", "com/adminConsole/common/addAndRemoveOptions1/addAndRemoveOptions1ControllerActions"], function() {
    var controller = require("com/adminConsole/common/addAndRemoveOptions1/useraddAndRemoveOptions1Controller");
    var actions = require("com/adminConsole/common/addAndRemoveOptions1/addAndRemoveOptions1ControllerActions");
    for (var key in actions) {
        controller[key] = actions[key];
    }
    return controller;
});
