define("com/adminConsole/common/breadcrumbs1/userbreadcrumbs1Controller", function() {
    return {};
});
define("com/adminConsole/common/breadcrumbs1/breadcrumbs1ControllerActions", {
    /*
          This is an auto generated file and any modifications to it may result in corruption of the action sequence.
        */
});
define("com/adminConsole/common/breadcrumbs1/breadcrumbs1Controller", ["com/adminConsole/common/breadcrumbs1/userbreadcrumbs1Controller", "com/adminConsole/common/breadcrumbs1/breadcrumbs1ControllerActions"], function() {
    var controller = require("com/adminConsole/common/breadcrumbs1/userbreadcrumbs1Controller");
    var actions = require("com/adminConsole/common/breadcrumbs1/breadcrumbs1ControllerActions");
    for (var key in actions) {
        controller[key] = actions[key];
    }
    return controller;
});
