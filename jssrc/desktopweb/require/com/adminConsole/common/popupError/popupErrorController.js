define("com/adminConsole/common/popupError/userpopupErrorController", function() {
    return {};
});
define("com/adminConsole/common/popupError/popupErrorControllerActions", {
    /*
          This is an auto generated file and any modifications to it may result in corruption of the action sequence.
        */
});
define("com/adminConsole/common/popupError/popupErrorController", ["com/adminConsole/common/popupError/userpopupErrorController", "com/adminConsole/common/popupError/popupErrorControllerActions"], function() {
    var controller = require("com/adminConsole/common/popupError/userpopupErrorController");
    var actions = require("com/adminConsole/common/popupError/popupErrorControllerActions");
    for (var key in actions) {
        controller[key] = actions[key];
    }
    return controller;
});
