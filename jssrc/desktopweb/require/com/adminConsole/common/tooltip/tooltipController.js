define("com/adminConsole/common/tooltip/usertooltipController", function() {
    return {};
});
define("com/adminConsole/common/tooltip/tooltipControllerActions", {
    /*
          This is an auto generated file and any modifications to it may result in corruption of the action sequence.
        */
});
define("com/adminConsole/common/tooltip/tooltipController", ["com/adminConsole/common/tooltip/usertooltipController", "com/adminConsole/common/tooltip/tooltipControllerActions"], function() {
    var controller = require("com/adminConsole/common/tooltip/usertooltipController");
    var actions = require("com/adminConsole/common/tooltip/tooltipControllerActions");
    for (var key in actions) {
        controller[key] = actions[key];
    }
    return controller;
});
