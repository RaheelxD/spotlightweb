define(function() {
    return function(controller) {
        var customRadioButtonGroup = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
            "clipBounds": true,
            "isMaster": true,
            "id": "customRadioButtonGroup",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "left": "0dp",
            "isModalContainer": false,
            "preShow": function(eventobject) {
                controller.AS_FlexContainer_be0cdb6290fe411fa70d45aa45b4497d(eventobject);
            },
            "skin": "slFbox",
            "top": "0dp",
            "width": "100%"
        }, controller.args[0], "customRadioButtonGroup"), extendConfig({}, controller.args[1], "customRadioButtonGroup"), extendConfig({}, controller.args[2], "customRadioButtonGroup"));
        customRadioButtonGroup.setDefaultUnit(kony.flex.DP);
        var flxMainContainer = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
            "clipBounds": true,
            "id": "flxMainContainer",
            "isVisible": true,
            "layoutType": kony.flex.FLOW_HORIZONTAL,
            "left": "0dp",
            "isModalContainer": false,
            "skin": "slFbox",
            "top": "0dp",
            "width": "100%",
            "zIndex": 1
        }, controller.args[0], "flxMainContainer"), extendConfig({}, controller.args[1], "flxMainContainer"), extendConfig({}, controller.args[2], "flxMainContainer"));
        flxMainContainer.setDefaultUnit(kony.flex.DP);
        var flxRadioButton1 = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
            "clipBounds": true,
            "id": "flxRadioButton1",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "left": "0dp",
            "isModalContainer": false,
            "skin": "slFbox",
            "top": "0dp",
            "width": "110dp",
            "zIndex": 1
        }, controller.args[0], "flxRadioButton1"), extendConfig({}, controller.args[1], "flxRadioButton1"), extendConfig({}, controller.args[2], "flxRadioButton1"));
        flxRadioButton1.setDefaultUnit(kony.flex.DP);
        var imgRadioButton1 = new kony.ui.Image2(extendConfig({
            "height": "15dp",
            "id": "imgRadioButton1",
            "isVisible": true,
            "left": "0dp",
            "skin": "slImage",
            "src": "radionormal_1x.png",
            "top": "0dp",
            "width": "15dp",
            "zIndex": 1
        }, controller.args[0], "imgRadioButton1"), extendConfig({
            "imageScaleMode": constants.IMAGE_SCALE_MODE_MAINTAIN_ASPECT_RATIO,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "imgRadioButton1"), extendConfig({}, controller.args[2], "imgRadioButton1"));
        var lblRadioButtonValue1 = new kony.ui.Label(extendConfig({
            "id": "lblRadioButtonValue1",
            "isVisible": true,
            "left": "20dp",
            "right": "10dp",
            "skin": "sknLbl485C75LatoRegular13Px",
            "text": "Sample Value",
            "top": "0dp",
            "zIndex": 1
        }, controller.args[0], "lblRadioButtonValue1"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_TOP_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblRadioButtonValue1"), extendConfig({}, controller.args[2], "lblRadioButtonValue1"));
        flxRadioButton1.add(imgRadioButton1, lblRadioButtonValue1);
        var flxRadioButton2 = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
            "clipBounds": true,
            "id": "flxRadioButton2",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "left": "10dp",
            "isModalContainer": false,
            "skin": "slFbox",
            "top": "0dp",
            "width": "110dp",
            "zIndex": 1
        }, controller.args[0], "flxRadioButton2"), extendConfig({}, controller.args[1], "flxRadioButton2"), extendConfig({}, controller.args[2], "flxRadioButton2"));
        flxRadioButton2.setDefaultUnit(kony.flex.DP);
        var imgRadioButton2 = new kony.ui.Image2(extendConfig({
            "height": "15dp",
            "id": "imgRadioButton2",
            "isVisible": true,
            "left": "0dp",
            "skin": "slImage",
            "src": "radionormal_1x.png",
            "top": "0dp",
            "width": "15dp",
            "zIndex": 1
        }, controller.args[0], "imgRadioButton2"), extendConfig({
            "imageScaleMode": constants.IMAGE_SCALE_MODE_MAINTAIN_ASPECT_RATIO,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "imgRadioButton2"), extendConfig({}, controller.args[2], "imgRadioButton2"));
        var lblRadioButtonValue2 = new kony.ui.Label(extendConfig({
            "id": "lblRadioButtonValue2",
            "isVisible": true,
            "left": "20dp",
            "right": "10dp",
            "skin": "sknLbl485C75LatoRegular13Px",
            "text": "Sample Value",
            "top": "0dp",
            "zIndex": 1
        }, controller.args[0], "lblRadioButtonValue2"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_TOP_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblRadioButtonValue2"), extendConfig({}, controller.args[2], "lblRadioButtonValue2"));
        flxRadioButton2.add(imgRadioButton2, lblRadioButtonValue2);
        var flxRadioButton3 = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
            "clipBounds": true,
            "id": "flxRadioButton3",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "left": "10dp",
            "isModalContainer": false,
            "skin": "slFbox",
            "top": "0dp",
            "width": "110dp",
            "zIndex": 1
        }, controller.args[0], "flxRadioButton3"), extendConfig({}, controller.args[1], "flxRadioButton3"), extendConfig({}, controller.args[2], "flxRadioButton3"));
        flxRadioButton3.setDefaultUnit(kony.flex.DP);
        var imgRadioButton3 = new kony.ui.Image2(extendConfig({
            "height": "15dp",
            "id": "imgRadioButton3",
            "isVisible": true,
            "left": "0dp",
            "skin": "slImage",
            "src": "radionormal_1x.png",
            "top": "0dp",
            "width": "15dp",
            "zIndex": 1
        }, controller.args[0], "imgRadioButton3"), extendConfig({
            "imageScaleMode": constants.IMAGE_SCALE_MODE_MAINTAIN_ASPECT_RATIO,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "imgRadioButton3"), extendConfig({}, controller.args[2], "imgRadioButton3"));
        var lblRadioButtonValue3 = new kony.ui.Label(extendConfig({
            "id": "lblRadioButtonValue3",
            "isVisible": true,
            "left": "20dp",
            "right": "10dp",
            "skin": "sknLbl485C75LatoRegular13Px",
            "text": "Sample Value",
            "top": "0dp",
            "zIndex": 1
        }, controller.args[0], "lblRadioButtonValue3"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_TOP_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblRadioButtonValue3"), extendConfig({}, controller.args[2], "lblRadioButtonValue3"));
        flxRadioButton3.add(imgRadioButton3, lblRadioButtonValue3);
        var flxRadioButton4 = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
            "clipBounds": true,
            "id": "flxRadioButton4",
            "isVisible": false,
            "layoutType": kony.flex.FREE_FORM,
            "left": "10dp",
            "isModalContainer": false,
            "skin": "slFbox",
            "top": "0dp",
            "width": "110dp",
            "zIndex": 1
        }, controller.args[0], "flxRadioButton4"), extendConfig({}, controller.args[1], "flxRadioButton4"), extendConfig({}, controller.args[2], "flxRadioButton4"));
        flxRadioButton4.setDefaultUnit(kony.flex.DP);
        var imgRadioButton4 = new kony.ui.Image2(extendConfig({
            "height": "15dp",
            "id": "imgRadioButton4",
            "isVisible": true,
            "left": "0dp",
            "skin": "slImage",
            "src": "radionormal_1x.png",
            "top": "0dp",
            "width": "15dp",
            "zIndex": 1
        }, controller.args[0], "imgRadioButton4"), extendConfig({
            "imageScaleMode": constants.IMAGE_SCALE_MODE_MAINTAIN_ASPECT_RATIO,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "imgRadioButton4"), extendConfig({}, controller.args[2], "imgRadioButton4"));
        var lblRadioButtonValue4 = new kony.ui.Label(extendConfig({
            "id": "lblRadioButtonValue4",
            "isVisible": true,
            "left": "20dp",
            "right": "10dp",
            "skin": "sknLbl485C75LatoRegular13Px",
            "text": "Sample Value",
            "top": "0dp",
            "zIndex": 1
        }, controller.args[0], "lblRadioButtonValue4"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_TOP_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblRadioButtonValue4"), extendConfig({}, controller.args[2], "lblRadioButtonValue4"));
        flxRadioButton4.add(imgRadioButton4, lblRadioButtonValue4);
        var flxRadioButton5 = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
            "clipBounds": true,
            "id": "flxRadioButton5",
            "isVisible": false,
            "layoutType": kony.flex.FREE_FORM,
            "left": "10dp",
            "isModalContainer": false,
            "skin": "slFbox",
            "top": "0dp",
            "width": "110dp",
            "zIndex": 1
        }, controller.args[0], "flxRadioButton5"), extendConfig({}, controller.args[1], "flxRadioButton5"), extendConfig({}, controller.args[2], "flxRadioButton5"));
        flxRadioButton5.setDefaultUnit(kony.flex.DP);
        var imgRadioButton5 = new kony.ui.Image2(extendConfig({
            "height": "15dp",
            "id": "imgRadioButton5",
            "isVisible": true,
            "left": "0dp",
            "skin": "slImage",
            "src": "radionormal_1x.png",
            "top": "0dp",
            "width": "15dp",
            "zIndex": 1
        }, controller.args[0], "imgRadioButton5"), extendConfig({
            "imageScaleMode": constants.IMAGE_SCALE_MODE_MAINTAIN_ASPECT_RATIO,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "imgRadioButton5"), extendConfig({}, controller.args[2], "imgRadioButton5"));
        var lblRadioButtonValue5 = new kony.ui.Label(extendConfig({
            "id": "lblRadioButtonValue5",
            "isVisible": true,
            "left": "20dp",
            "right": "10dp",
            "skin": "sknLbl485C75LatoRegular13Px",
            "text": "Sample Value",
            "top": "0dp",
            "zIndex": 1
        }, controller.args[0], "lblRadioButtonValue5"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_TOP_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblRadioButtonValue5"), extendConfig({}, controller.args[2], "lblRadioButtonValue5"));
        flxRadioButton5.add(imgRadioButton5, lblRadioButtonValue5);
        flxMainContainer.add(flxRadioButton1, flxRadioButton2, flxRadioButton3, flxRadioButton4, flxRadioButton5);
        customRadioButtonGroup.add(flxMainContainer);
        return customRadioButtonGroup;
    }
})