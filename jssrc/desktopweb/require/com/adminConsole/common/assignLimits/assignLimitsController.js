define("com/adminConsole/common/assignLimits/userassignLimitsController", function() {
    return {};
});
define("com/adminConsole/common/assignLimits/assignLimitsControllerActions", {
    /*
        This is an auto generated file and any modifications to it may result in corruption of the action sequence.
    */
});
define("com/adminConsole/common/assignLimits/assignLimitsController", ["com/adminConsole/common/assignLimits/userassignLimitsController", "com/adminConsole/common/assignLimits/assignLimitsControllerActions"], function() {
    var controller = require("com/adminConsole/common/assignLimits/userassignLimitsController");
    var actions = require("com/adminConsole/common/assignLimits/assignLimitsControllerActions");
    for (var key in actions) {
        controller[key] = actions[key];
    }
    return controller;
});
