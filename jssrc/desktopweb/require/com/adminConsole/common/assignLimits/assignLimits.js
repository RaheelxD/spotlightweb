define(function() {
    return function(controller) {
        var assignLimits = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
            "clipBounds": true,
            "isMaster": true,
            "id": "assignLimits",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "left": "0dp",
            "isModalContainer": false,
            "skin": "slFbox",
            "top": "0dp",
            "width": kony.flex.USE_PREFFERED_SIZE
        }, controller.args[0], "assignLimits"), extendConfig({}, controller.args[1], "assignLimits"), extendConfig({}, controller.args[2], "assignLimits"));
        assignLimits.setDefaultUnit(kony.flex.DP);
        var flxAssignLimits = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
            "bottom": "10dp",
            "clipBounds": true,
            "id": "flxAssignLimits",
            "isVisible": true,
            "layoutType": kony.flex.FLOW_VERTICAL,
            "left": "2%",
            "isModalContainer": false,
            "right": "1%",
            "skin": "sknborderd7d9e0bgfff0px",
            "top": "10dp",
            "width": "97%"
        }, controller.args[0], "flxAssignLimits"), extendConfig({}, controller.args[1], "flxAssignLimits"), extendConfig({}, controller.args[2], "flxAssignLimits"));
        flxAssignLimits.setDefaultUnit(kony.flex.DP);
        var flxHeader = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "clipBounds": true,
            "height": "42dp",
            "id": "flxHeader",
            "isVisible": true,
            "layoutType": kony.flex.FLOW_VERTICAL,
            "left": "0dp",
            "isModalContainer": false,
            "skin": "sknf5f6f8border0px",
            "top": "0dp",
            "width": "100%"
        }, controller.args[0], "flxHeader"), extendConfig({}, controller.args[1], "flxHeader"), extendConfig({}, controller.args[2], "flxHeader"));
        flxHeader.setDefaultUnit(kony.flex.DP);
        var flxTopRow = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
            "clipBounds": true,
            "id": "flxTopRow",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "left": "0dp",
            "isModalContainer": false,
            "skin": "slFbox",
            "top": "12dp",
            "width": "100%"
        }, controller.args[0], "flxTopRow"), extendConfig({}, controller.args[1], "flxTopRow"), extendConfig({}, controller.args[2], "flxTopRow"));
        flxTopRow.setDefaultUnit(kony.flex.DP);
        var lblFeatureName = new kony.ui.Label(extendConfig({
            "id": "lblFeatureName",
            "isVisible": true,
            "left": "1%",
            "skin": "sknLbl192B45LatoRegular14px",
            "text": "Bill Payment",
            "textStyle": {},
            "top": "0dp",
            "width": kony.flex.USE_PREFFERED_SIZE
        }, controller.args[0], "lblFeatureName"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblFeatureName"), extendConfig({}, controller.args[2], "lblFeatureName"));
        var flxFeatureStatus = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
            "clipBounds": true,
            "id": "flxFeatureStatus",
            "isVisible": true,
            "layoutType": kony.flex.FLOW_HORIZONTAL,
            "reverseLayoutDirection": true,
            "isModalContainer": false,
            "right": "0dp",
            "skin": "slFbox",
            "top": "0dp",
            "width": "50%"
        }, controller.args[0], "flxFeatureStatus"), extendConfig({}, controller.args[1], "flxFeatureStatus"), extendConfig({}, controller.args[2], "flxFeatureStatus"));
        flxFeatureStatus.setDefaultUnit(kony.flex.DP);
        var featureStatus = new kony.ui.Label(extendConfig({
            "id": "featureStatus",
            "isVisible": true,
            "right": "20dp",
            "skin": "sknlblLato5bc06cBold14px",
            "i18n_text": "kony.i18n.getLocalizedString(\"i18n.secureimage.Active\")",
            "textStyle": {},
            "top": "0dp",
            "width": kony.flex.USE_PREFFERED_SIZE
        }, controller.args[0], "featureStatus"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "featureStatus"), extendConfig({}, controller.args[2], "featureStatus"));
        var featureStatusIcon = new kony.ui.Label(extendConfig({
            "id": "featureStatusIcon",
            "isVisible": true,
            "right": "5dp",
            "skin": "sknFontIconActivate",
            "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmAlertsManagement.lblViewAlertStatusKey\")",
            "textStyle": {},
            "top": "2dp",
            "width": kony.flex.USE_PREFFERED_SIZE
        }, controller.args[0], "featureStatusIcon"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "featureStatusIcon"), extendConfig({}, controller.args[2], "featureStatusIcon"));
        flxFeatureStatus.add(featureStatus, featureStatusIcon);
        flxTopRow.add(lblFeatureName, flxFeatureStatus);
        flxHeader.add(flxTopRow);
        var flxActionsAndLimits = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
            "bottom": "5dp",
            "clipBounds": true,
            "id": "flxActionsAndLimits",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "left": "0dp",
            "isModalContainer": false,
            "skin": "slFbox",
            "top": "0dp",
            "width": "100%"
        }, controller.args[0], "flxActionsAndLimits"), extendConfig({}, controller.args[1], "flxActionsAndLimits"), extendConfig({}, controller.args[2], "flxActionsAndLimits"));
        flxActionsAndLimits.setDefaultUnit(kony.flex.DP);
        var flxHeaderActionsLimits = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "clipBounds": true,
            "height": "40dp",
            "id": "flxHeaderActionsLimits",
            "isVisible": true,
            "layoutType": kony.flex.FLOW_HORIZONTAL,
            "left": "0dp",
            "isModalContainer": false,
            "skin": "sknflxffffffop100",
            "top": "0dp",
            "width": "100%"
        }, controller.args[0], "flxHeaderActionsLimits"), extendConfig({}, controller.args[1], "flxHeaderActionsLimits"), extendConfig({}, controller.args[2], "flxHeaderActionsLimits"));
        flxHeaderActionsLimits.setDefaultUnit(kony.flex.DP);
        var lblAction = new kony.ui.Label(extendConfig({
            "centerY": "50%",
            "id": "lblAction",
            "isVisible": true,
            "left": "1%",
            "skin": "sknlblLato696c7311px",
            "text": "ACTION",
            "textStyle": {},
            "top": "10dp",
            "width": "30%"
        }, controller.args[0], "lblAction"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblAction"), extendConfig({}, controller.args[2], "lblAction"));
        var flxPerTransaction = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
            "centerY": "50%",
            "clipBounds": true,
            "id": "flxPerTransaction",
            "isVisible": true,
            "layoutType": kony.flex.FLOW_HORIZONTAL,
            "left": "1%",
            "isModalContainer": false,
            "skin": "slFbox",
            "width": "20%"
        }, controller.args[0], "flxPerTransaction"), extendConfig({}, controller.args[1], "flxPerTransaction"), extendConfig({}, controller.args[2], "flxPerTransaction"));
        flxPerTransaction.setDefaultUnit(kony.flex.DP);
        var lblPerTransaction = new kony.ui.Label(extendConfig({
            "id": "lblPerTransaction",
            "isVisible": true,
            "left": "0dp",
            "skin": "sknlblLato696c7311px",
            "text": "PER TRANSACTION",
            "textStyle": {},
            "top": "0dp",
            "width": kony.flex.USE_PREFFERED_SIZE
        }, controller.args[0], "lblPerTransaction"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblPerTransaction"), extendConfig({}, controller.args[2], "lblPerTransaction"));
        var flxDefaultInfo1 = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
            "clipBounds": true,
            "id": "flxDefaultInfo1",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "left": "2dp",
            "isModalContainer": false,
            "onClick": controller.AS_FlexContainer_aeb8168ea3624740bf06d583316c5596,
            "skin": "sknCursor",
            "top": "0dp",
            "width": "15dp",
            "zIndex": 2
        }, controller.args[0], "flxDefaultInfo1"), extendConfig({}, controller.args[1], "flxDefaultInfo1"), extendConfig({}, controller.args[2], "flxDefaultInfo1"));
        flxDefaultInfo1.setDefaultUnit(kony.flex.DP);
        var fontIconDefaultInfo1 = new kony.ui.Label(extendConfig({
            "id": "fontIconDefaultInfo1",
            "isVisible": true,
            "left": "0dp",
            "skin": "sknfontIcon192b4b14px",
            "text": "",
            "textStyle": {},
            "top": "0dp",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, controller.args[0], "fontIconDefaultInfo1"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_TOP_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "fontIconDefaultInfo1"), extendConfig({}, controller.args[2], "fontIconDefaultInfo1"));
        flxDefaultInfo1.add(fontIconDefaultInfo1);
        flxPerTransaction.add(lblPerTransaction, flxDefaultInfo1);
        var flxDailyTransaction = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
            "centerY": "50%",
            "clipBounds": true,
            "id": "flxDailyTransaction",
            "isVisible": true,
            "layoutType": kony.flex.FLOW_HORIZONTAL,
            "left": "2%",
            "isModalContainer": false,
            "skin": "slFbox",
            "width": "20%"
        }, controller.args[0], "flxDailyTransaction"), extendConfig({}, controller.args[1], "flxDailyTransaction"), extendConfig({}, controller.args[2], "flxDailyTransaction"));
        flxDailyTransaction.setDefaultUnit(kony.flex.DP);
        var lblDailyTransaction = new kony.ui.Label(extendConfig({
            "id": "lblDailyTransaction",
            "isVisible": true,
            "left": "0dp",
            "skin": "sknlblLato696c7311px",
            "text": "DAILY TRANSACTION",
            "textStyle": {},
            "top": "0dp",
            "width": kony.flex.USE_PREFFERED_SIZE
        }, controller.args[0], "lblDailyTransaction"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblDailyTransaction"), extendConfig({}, controller.args[2], "lblDailyTransaction"));
        var flxDefaultInfo2 = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
            "clipBounds": true,
            "id": "flxDefaultInfo2",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "left": "2dp",
            "isModalContainer": false,
            "onClick": controller.AS_FlexContainer_aeb8168ea3624740bf06d583316c5596,
            "skin": "sknCursor",
            "top": "0dp",
            "width": "15dp",
            "zIndex": 2
        }, controller.args[0], "flxDefaultInfo2"), extendConfig({}, controller.args[1], "flxDefaultInfo2"), extendConfig({}, controller.args[2], "flxDefaultInfo2"));
        flxDefaultInfo2.setDefaultUnit(kony.flex.DP);
        var fontIconDefaultInfo2 = new kony.ui.Label(extendConfig({
            "id": "fontIconDefaultInfo2",
            "isVisible": true,
            "left": "0dp",
            "skin": "sknfontIcon192b4b14px",
            "text": "",
            "textStyle": {},
            "top": "0dp",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, controller.args[0], "fontIconDefaultInfo2"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_TOP_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "fontIconDefaultInfo2"), extendConfig({}, controller.args[2], "fontIconDefaultInfo2"));
        flxDefaultInfo2.add(fontIconDefaultInfo2);
        flxDailyTransaction.add(lblDailyTransaction, flxDefaultInfo2);
        var flxWeeklyTransaction = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
            "centerY": "50%",
            "clipBounds": true,
            "id": "flxWeeklyTransaction",
            "isVisible": true,
            "layoutType": kony.flex.FLOW_HORIZONTAL,
            "left": "2%",
            "isModalContainer": false,
            "skin": "slFbox",
            "width": "22%"
        }, controller.args[0], "flxWeeklyTransaction"), extendConfig({}, controller.args[1], "flxWeeklyTransaction"), extendConfig({}, controller.args[2], "flxWeeklyTransaction"));
        flxWeeklyTransaction.setDefaultUnit(kony.flex.DP);
        var lblWeeklyTransaction = new kony.ui.Label(extendConfig({
            "id": "lblWeeklyTransaction",
            "isVisible": true,
            "left": "0dp",
            "skin": "sknlblLato696c7311px",
            "text": "WEEKLY TRANSACTION",
            "textStyle": {},
            "top": "0dp",
            "width": kony.flex.USE_PREFFERED_SIZE
        }, controller.args[0], "lblWeeklyTransaction"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblWeeklyTransaction"), extendConfig({}, controller.args[2], "lblWeeklyTransaction"));
        var flxDefaultInfo3 = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
            "clipBounds": true,
            "id": "flxDefaultInfo3",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "left": "2dp",
            "isModalContainer": false,
            "onClick": controller.AS_FlexContainer_aeb8168ea3624740bf06d583316c5596,
            "skin": "sknCursor",
            "top": "0dp",
            "width": "15dp",
            "zIndex": 2
        }, controller.args[0], "flxDefaultInfo3"), extendConfig({}, controller.args[1], "flxDefaultInfo3"), extendConfig({}, controller.args[2], "flxDefaultInfo3"));
        flxDefaultInfo3.setDefaultUnit(kony.flex.DP);
        var fontIconDefaultInfo3 = new kony.ui.Label(extendConfig({
            "id": "fontIconDefaultInfo3",
            "isVisible": true,
            "left": "0dp",
            "skin": "sknfontIcon192b4b14px",
            "text": "",
            "textStyle": {},
            "top": "0dp",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, controller.args[0], "fontIconDefaultInfo3"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_TOP_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "fontIconDefaultInfo3"), extendConfig({}, controller.args[2], "fontIconDefaultInfo3"));
        flxDefaultInfo3.add(fontIconDefaultInfo3);
        flxWeeklyTransaction.add(lblWeeklyTransaction, flxDefaultInfo3);
        flxHeaderActionsLimits.add(lblAction, flxPerTransaction, flxDailyTransaction, flxWeeklyTransaction);
        var lblSeparator = new kony.ui.Label(extendConfig({
            "height": "1px",
            "id": "lblSeparator",
            "isVisible": true,
            "left": "1%",
            "skin": "sknLblSeparator696C73",
            "text": ".",
            "textStyle": {},
            "top": "40dp",
            "width": "98%",
            "zIndex": 1
        }, controller.args[0], "lblSeparator"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblSeparator"), extendConfig({}, controller.args[2], "lblSeparator"));
        var flxActionsLimitsSegment = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
            "clipBounds": true,
            "id": "flxActionsLimitsSegment",
            "isVisible": true,
            "layoutType": kony.flex.FLOW_VERTICAL,
            "left": "0",
            "isModalContainer": false,
            "skin": "slFbox",
            "top": "41dp",
            "width": "100%"
        }, controller.args[0], "flxActionsLimitsSegment"), extendConfig({}, controller.args[1], "flxActionsLimitsSegment"), extendConfig({}, controller.args[2], "flxActionsLimitsSegment"));
        flxActionsLimitsSegment.setDefaultUnit(kony.flex.DP);
        var SegActionsLimits = new kony.ui.SegmentedUI2(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
            "data": [{
                "lblActionName": "kony.i18n.getLocalizedString(\"i18n.frmGroupsController.Min_Transaction_Value\")",
                "lblCurrencySymbol1": "",
                "lblCurrencySymbol2": "",
                "lblCurrencySymbol3": "",
                "lblCurrencySymbol4": "",
                "lblCurrencySymbol5": "",
                "lblLowerLimit": "2000",
                "lblRange": "kony.i18n.getLocalizedString(\"i18n.actionLimits.range\")",
                "lblRangeSymbol": "-",
                "lblSeparator": ".",
                "lblUpperLimit": "4000",
                "tbxLimitValue1": "",
                "tbxLimitValue2": "",
                "tbxLimitValue3": ""
            }, {
                "lblActionName": "kony.i18n.getLocalizedString(\"i18n.frmGroupsController.Min_Transaction_Value\")",
                "lblCurrencySymbol1": "",
                "lblCurrencySymbol2": "",
                "lblCurrencySymbol3": "",
                "lblCurrencySymbol4": "",
                "lblCurrencySymbol5": "",
                "lblLowerLimit": "2000",
                "lblRange": "kony.i18n.getLocalizedString(\"i18n.actionLimits.range\")",
                "lblRangeSymbol": "-",
                "lblSeparator": ".",
                "lblUpperLimit": "4000",
                "tbxLimitValue1": "",
                "tbxLimitValue2": "",
                "tbxLimitValue3": ""
            }, {
                "lblActionName": "kony.i18n.getLocalizedString(\"i18n.frmGroupsController.Min_Transaction_Value\")",
                "lblCurrencySymbol1": "",
                "lblCurrencySymbol2": "",
                "lblCurrencySymbol3": "",
                "lblCurrencySymbol4": "",
                "lblCurrencySymbol5": "",
                "lblLowerLimit": "2000",
                "lblRange": "kony.i18n.getLocalizedString(\"i18n.actionLimits.range\")",
                "lblRangeSymbol": "-",
                "lblSeparator": ".",
                "lblUpperLimit": "4000",
                "tbxLimitValue1": "",
                "tbxLimitValue2": "",
                "tbxLimitValue3": ""
            }, {
                "lblActionName": "kony.i18n.getLocalizedString(\"i18n.frmGroupsController.Min_Transaction_Value\")",
                "lblCurrencySymbol1": "",
                "lblCurrencySymbol2": "",
                "lblCurrencySymbol3": "",
                "lblCurrencySymbol4": "",
                "lblCurrencySymbol5": "",
                "lblLowerLimit": "2000",
                "lblRange": "kony.i18n.getLocalizedString(\"i18n.actionLimits.range\")",
                "lblRangeSymbol": "-",
                "lblSeparator": ".",
                "lblUpperLimit": "4000",
                "tbxLimitValue1": "",
                "tbxLimitValue2": "",
                "tbxLimitValue3": ""
            }, {
                "lblActionName": "kony.i18n.getLocalizedString(\"i18n.frmGroupsController.Min_Transaction_Value\")",
                "lblCurrencySymbol1": "",
                "lblCurrencySymbol2": "",
                "lblCurrencySymbol3": "",
                "lblCurrencySymbol4": "",
                "lblCurrencySymbol5": "",
                "lblLowerLimit": "2000",
                "lblRange": "kony.i18n.getLocalizedString(\"i18n.actionLimits.range\")",
                "lblRangeSymbol": "-",
                "lblSeparator": ".",
                "lblUpperLimit": "4000",
                "tbxLimitValue1": "",
                "tbxLimitValue2": "",
                "tbxLimitValue3": ""
            }, {
                "lblActionName": "kony.i18n.getLocalizedString(\"i18n.frmGroupsController.Min_Transaction_Value\")",
                "lblCurrencySymbol1": "",
                "lblCurrencySymbol2": "",
                "lblCurrencySymbol3": "",
                "lblCurrencySymbol4": "",
                "lblCurrencySymbol5": "",
                "lblLowerLimit": "2000",
                "lblRange": "kony.i18n.getLocalizedString(\"i18n.actionLimits.range\")",
                "lblRangeSymbol": "-",
                "lblSeparator": ".",
                "lblUpperLimit": "4000",
                "tbxLimitValue1": "",
                "tbxLimitValue2": "",
                "tbxLimitValue3": ""
            }],
            "groupCells": false,
            "id": "SegActionsLimits",
            "isVisible": false,
            "left": "0",
            "needPageIndicator": true,
            "pageOffDotImage": "pageoffdot.png",
            "pageOnDotImage": "pageondot.png",
            "retainSelection": false,
            "rowFocusSkin": "seg2Focus",
            "rowSkin": "seg2Normal",
            "rowTemplate": "flxActionsLimits",
            "sectionHeaderSkin": "sliPhoneSegmentHeader",
            "selectionBehavior": constants.SEGUI_SINGLE_SELECT_BEHAVIOR,
            "separatorColor": "aaaaaa00",
            "separatorRequired": false,
            "separatorThickness": 1,
            "showScrollbars": false,
            "top": "0",
            "viewType": constants.SEGUI_VIEW_TYPE_TABLEVIEW,
            "widgetDataMap": {
                "flxActionsLimits": "flxActionsLimits",
                "flxAmountRange": "flxAmountRange",
                "flxLimit1Range": "flxLimit1Range",
                "flxLimitValue1": "flxLimitValue1",
                "flxLimitValue2": "flxLimitValue2",
                "flxLimitValue3": "flxLimitValue3",
                "flxLimitsRow1": "flxLimitsRow1",
                "lblActionName": "lblActionName",
                "lblCurrencySymbol1": "lblCurrencySymbol1",
                "lblCurrencySymbol2": "lblCurrencySymbol2",
                "lblCurrencySymbol3": "lblCurrencySymbol3",
                "lblCurrencySymbol4": "lblCurrencySymbol4",
                "lblCurrencySymbol5": "lblCurrencySymbol5",
                "lblLowerLimit": "lblLowerLimit",
                "lblRange": "lblRange",
                "lblRangeSymbol": "lblRangeSymbol",
                "lblSeparator": "lblSeparator",
                "lblUpperLimit": "lblUpperLimit",
                "tbxLimitValue1": "tbxLimitValue1",
                "tbxLimitValue2": "tbxLimitValue2",
                "tbxLimitValue3": "tbxLimitValue3"
            },
            "width": "100%"
        }, controller.args[0], "SegActionsLimits"), extendConfig({
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "SegActionsLimits"), extendConfig({}, controller.args[2], "SegActionsLimits"));
        var flxActionsLimits = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
            "clipBounds": true,
            "id": "flxActionsLimits",
            "isVisible": false,
            "layoutType": kony.flex.FREE_FORM,
            "left": "0dp",
            "isModalContainer": false,
            "skin": "sknflxffffffop100",
            "top": "0dp",
            "width": "100%"
        }, controller.args[0], "flxActionsLimits"), extendConfig({}, controller.args[1], "flxActionsLimits"), extendConfig({}, controller.args[2], "flxActionsLimits"));
        flxActionsLimits.setDefaultUnit(kony.flex.DP);
        var flxLimitsRow = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
            "bottom": 0,
            "clipBounds": true,
            "id": "flxLimitsRow",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "left": "0dp",
            "isModalContainer": false,
            "skin": "slFbox",
            "top": "0dp",
            "width": "100%",
            "zIndex": 1
        }, controller.args[0], "flxLimitsRow"), extendConfig({}, controller.args[1], "flxLimitsRow"), extendConfig({}, controller.args[2], "flxLimitsRow"));
        flxLimitsRow.setDefaultUnit(kony.flex.DP);
        var lblActionName = new kony.ui.Label(extendConfig({
            "id": "lblActionName",
            "isVisible": true,
            "left": "1%",
            "skin": "sknLbl485C75LatoRegular13Px",
            "text": "Create Bill Payment",
            "top": "22dp",
            "width": "30%",
            "zIndex": 1
        }, controller.args[0], "lblActionName"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblActionName"), extendConfig({}, controller.args[2], "lblActionName"));
        var flxLimitValueA = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "bottom": "15dp",
            "clipBounds": true,
            "height": "40dp",
            "id": "flxLimitValueA",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "left": "32.50%",
            "isModalContainer": false,
            "skin": "sknFlxbgFFFFFFbdrD7D9E0rd3px",
            "top": "15dp",
            "width": "20%"
        }, controller.args[0], "flxLimitValueA"), extendConfig({}, controller.args[1], "flxLimitValueA"), extendConfig({}, controller.args[2], "flxLimitValueA"));
        flxLimitValueA.setDefaultUnit(kony.flex.DP);
        var lblCurrencySymbolA = new kony.ui.Label(extendConfig({
            "centerY": "52%",
            "id": "lblCurrencySymbolA",
            "isVisible": true,
            "left": "5dp",
            "skin": "sknFontIcon485C7518px",
            "text": "",
            "width": "20dp",
            "zIndex": 1
        }, controller.args[0], "lblCurrencySymbolA"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblCurrencySymbolA"), extendConfig({}, controller.args[2], "lblCurrencySymbolA"));
        var tbxLimitValueA = new kony.ui.TextBox2(extendConfig({
            "autoCapitalize": constants.TEXTBOX_AUTO_CAPITALIZE_NONE,
            "focusSkin": "sknTbx485c75LatoReg13pxNoBor",
            "height": "40dp",
            "id": "tbxLimitValueA",
            "isVisible": true,
            "keyBoardStyle": constants.TEXTBOX_KEY_BOARD_STYLE_DEFAULT,
            "left": "25dp",
            "right": "5dp",
            "secureTextEntry": false,
            "skin": "sknTbxBgFFFFFFf485c7513pxNoBorder",
            "textInputMode": constants.TEXTBOX_INPUT_MODE_NUMERIC,
            "top": "0dp",
            "zIndex": 1
        }, controller.args[0], "tbxLimitValueA"), extendConfig({
            "containerHeightMode": constants.TEXTBOX_FONT_METRICS_DRIVEN_HEIGHT,
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [1, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "tbxLimitValueA"), extendConfig({
            "autoCorrect": false,
            "placeholderSkin": "defTextBoxPlaceholder"
        }, controller.args[2], "tbxLimitValueA"));
        flxLimitValueA.add(lblCurrencySymbolA, tbxLimitValueA);
        var flxLimitValueB = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "bottom": "15dp",
            "clipBounds": true,
            "height": "40dp",
            "id": "flxLimitValueB",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "left": "55%",
            "isModalContainer": false,
            "skin": "sknFlxbgFFFFFFbdrD7D9E0rd3px",
            "top": "15dp",
            "width": "20%",
            "zIndex": 1
        }, controller.args[0], "flxLimitValueB"), extendConfig({}, controller.args[1], "flxLimitValueB"), extendConfig({}, controller.args[2], "flxLimitValueB"));
        flxLimitValueB.setDefaultUnit(kony.flex.DP);
        var lblCurrencySymbolB = new kony.ui.Label(extendConfig({
            "centerY": "52%",
            "id": "lblCurrencySymbolB",
            "isVisible": true,
            "left": "5dp",
            "skin": "sknFontIcon485C7518px",
            "text": "",
            "width": "20dp",
            "zIndex": 1
        }, controller.args[0], "lblCurrencySymbolB"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblCurrencySymbolB"), extendConfig({}, controller.args[2], "lblCurrencySymbolB"));
        var tbxLimitValueB = new kony.ui.TextBox2(extendConfig({
            "autoCapitalize": constants.TEXTBOX_AUTO_CAPITALIZE_NONE,
            "focusSkin": "sknTbx485c75LatoReg13pxNoBor",
            "height": "40dp",
            "id": "tbxLimitValueB",
            "isVisible": true,
            "keyBoardStyle": constants.TEXTBOX_KEY_BOARD_STYLE_DEFAULT,
            "left": "25dp",
            "right": "5dp",
            "secureTextEntry": false,
            "skin": "sknTbxBgFFFFFFf485c7513pxNoBorder",
            "textInputMode": constants.TEXTBOX_INPUT_MODE_NUMERIC,
            "top": "0dp",
            "zIndex": 1
        }, controller.args[0], "tbxLimitValueB"), extendConfig({
            "containerHeightMode": constants.TEXTBOX_FONT_METRICS_DRIVEN_HEIGHT,
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [1, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "tbxLimitValueB"), extendConfig({
            "autoCorrect": false,
            "placeholderSkin": "defTextBoxPlaceholder"
        }, controller.args[2], "tbxLimitValueB"));
        flxLimitValueB.add(lblCurrencySymbolB, tbxLimitValueB);
        var flxLimitValueC = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "bottom": "15dp",
            "clipBounds": true,
            "height": "40dp",
            "id": "flxLimitValueC",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "left": "77.50%",
            "isModalContainer": false,
            "skin": "sknFlxbgFFFFFFbdrD7D9E0rd3px",
            "top": "15dp",
            "width": "20%"
        }, controller.args[0], "flxLimitValueC"), extendConfig({}, controller.args[1], "flxLimitValueC"), extendConfig({}, controller.args[2], "flxLimitValueC"));
        flxLimitValueC.setDefaultUnit(kony.flex.DP);
        var lblCurrencySymbolC = new kony.ui.Label(extendConfig({
            "centerY": "52%",
            "id": "lblCurrencySymbolC",
            "isVisible": true,
            "left": "5dp",
            "skin": "sknFontIcon485C7518px",
            "text": "",
            "width": "20dp",
            "zIndex": 1
        }, controller.args[0], "lblCurrencySymbolC"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblCurrencySymbolC"), extendConfig({}, controller.args[2], "lblCurrencySymbolC"));
        var tbxLimitValueC = new kony.ui.TextBox2(extendConfig({
            "autoCapitalize": constants.TEXTBOX_AUTO_CAPITALIZE_NONE,
            "focusSkin": "sknTbx485c75LatoReg13pxNoBor",
            "height": "40dp",
            "id": "tbxLimitValueC",
            "isVisible": true,
            "keyBoardStyle": constants.TEXTBOX_KEY_BOARD_STYLE_DEFAULT,
            "left": "25dp",
            "right": "5dp",
            "secureTextEntry": false,
            "skin": "sknTbxBgFFFFFFf485c7513pxNoBorder",
            "textInputMode": constants.TEXTBOX_INPUT_MODE_NUMERIC,
            "top": "0dp",
            "zIndex": 1
        }, controller.args[0], "tbxLimitValueC"), extendConfig({
            "containerHeightMode": constants.TEXTBOX_FONT_METRICS_DRIVEN_HEIGHT,
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [1, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "tbxLimitValueC"), extendConfig({
            "autoCorrect": false,
            "placeholderSkin": "defTextBoxPlaceholder"
        }, controller.args[2], "tbxLimitValueC"));
        flxLimitValueC.add(lblCurrencySymbolC, tbxLimitValueC);
        var flxLimitRangeA = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
            "bottom": "5dp",
            "clipBounds": true,
            "id": "flxLimitRangeA",
            "isVisible": false,
            "layoutType": kony.flex.FLOW_HORIZONTAL,
            "left": "32.50%",
            "isModalContainer": false,
            "skin": "slFbox",
            "top": "60dp",
            "width": "20%",
            "zIndex": 1
        }, controller.args[0], "flxLimitRangeA"), extendConfig({}, controller.args[1], "flxLimitRangeA"), extendConfig({}, controller.args[2], "flxLimitRangeA"));
        flxLimitRangeA.setDefaultUnit(kony.flex.DP);
        var lblErrorA = new kony.ui.Label(extendConfig({
            "id": "lblErrorA",
            "isVisible": false,
            "left": "0dp",
            "right": "0dp",
            "skin": "sknlblError",
            "text": "Value cannot be more than $4000",
            "top": "0dp",
            "width": "100%",
            "zIndex": 1
        }, controller.args[0], "lblErrorA"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblErrorA"), extendConfig({}, controller.args[2], "lblErrorA"));
        var lblRangeA = new kony.ui.Label(extendConfig({
            "id": "lblRangeA",
            "isVisible": true,
            "left": "2dp",
            "skin": "sknlblLatoReg485c7512px",
            "text": "Range: $ 2000 - $ 4000",
            "top": "0",
            "width": "100%"
        }, controller.args[0], "lblRangeA"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblRangeA"), extendConfig({}, controller.args[2], "lblRangeA"));
        flxLimitRangeA.add(lblErrorA, lblRangeA);
        var flxLimitRangeB = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
            "bottom": "5dp",
            "clipBounds": true,
            "id": "flxLimitRangeB",
            "isVisible": false,
            "layoutType": kony.flex.FLOW_HORIZONTAL,
            "left": "54.50%",
            "isModalContainer": false,
            "skin": "slFbox",
            "top": "60dp",
            "width": "20%",
            "zIndex": 1
        }, controller.args[0], "flxLimitRangeB"), extendConfig({}, controller.args[1], "flxLimitRangeB"), extendConfig({}, controller.args[2], "flxLimitRangeB"));
        flxLimitRangeB.setDefaultUnit(kony.flex.DP);
        var lblRangeB = new kony.ui.Label(extendConfig({
            "id": "lblRangeB",
            "isVisible": true,
            "left": "2dp",
            "skin": "sknlblLatoReg485c7512px",
            "text": "Range: $ 2000 - $ 4000",
            "top": "0",
            "width": "100%"
        }, controller.args[0], "lblRangeB"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblRangeB"), extendConfig({}, controller.args[2], "lblRangeB"));
        var lblErrorB = new kony.ui.Label(extendConfig({
            "id": "lblErrorB",
            "isVisible": false,
            "left": "0dp",
            "right": "0dp",
            "skin": "sknlblError",
            "text": "Value cannot be more than $4000",
            "top": "0dp",
            "width": "100%",
            "zIndex": 1
        }, controller.args[0], "lblErrorB"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblErrorB"), extendConfig({}, controller.args[2], "lblErrorB"));
        flxLimitRangeB.add(lblRangeB, lblErrorB);
        var flxLimitRangeC = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
            "bottom": "2dp",
            "clipBounds": true,
            "id": "flxLimitRangeC",
            "isVisible": false,
            "layoutType": kony.flex.FLOW_HORIZONTAL,
            "left": "77.50%",
            "isModalContainer": false,
            "skin": "slFbox",
            "top": "60dp",
            "width": "20%",
            "zIndex": 1
        }, controller.args[0], "flxLimitRangeC"), extendConfig({}, controller.args[1], "flxLimitRangeC"), extendConfig({}, controller.args[2], "flxLimitRangeC"));
        flxLimitRangeC.setDefaultUnit(kony.flex.DP);
        var lblRangeC = new kony.ui.Label(extendConfig({
            "id": "lblRangeC",
            "isVisible": true,
            "left": "2dp",
            "skin": "sknlblLatoReg485c7512px",
            "text": "Range: $ 2000 - $ 4000",
            "top": "0",
            "width": "100%"
        }, controller.args[0], "lblRangeC"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblRangeC"), extendConfig({}, controller.args[2], "lblRangeC"));
        var lblErrorC = new kony.ui.Label(extendConfig({
            "id": "lblErrorC",
            "isVisible": false,
            "left": "0dp",
            "right": "0dp",
            "skin": "sknlblError",
            "text": "Value cannot be more than $4000",
            "top": "0dp",
            "width": "100%",
            "zIndex": 1
        }, controller.args[0], "lblErrorC"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblErrorC"), extendConfig({}, controller.args[2], "lblErrorC"));
        flxLimitRangeC.add(lblRangeC, lblErrorC);
        var lblSeparatorA = new kony.ui.Label(extendConfig({
            "height": "1px",
            "id": "lblSeparatorA",
            "isVisible": true,
            "left": "1%",
            "right": "1%",
            "skin": "sknLblSeparatore7e7e7",
            "text": ".",
            "textStyle": {},
            "top": "0dp",
            "zIndex": 1
        }, controller.args[0], "lblSeparatorA"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblSeparatorA"), extendConfig({}, controller.args[2], "lblSeparatorA"));
        flxLimitsRow.add(lblActionName, flxLimitValueA, flxLimitValueB, flxLimitValueC, flxLimitRangeA, flxLimitRangeB, flxLimitRangeC, lblSeparatorA);
        flxActionsLimits.add(flxLimitsRow);
        flxActionsLimitsSegment.add(SegActionsLimits, flxActionsLimits);
        flxActionsAndLimits.add(flxHeaderActionsLimits, lblSeparator, flxActionsLimitsSegment);
        flxAssignLimits.add(flxHeader, flxActionsAndLimits);
        var ToolTip = new com.adminConsole.Customers.ToolTip(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
            "id": "ToolTip",
            "isVisible": false,
            "layoutType": kony.flex.FREE_FORM,
            "left": "298dp",
            "masterType": constants.MASTER_TYPE_DEFAULT,
            "isModalContainer": false,
            "skin": "slFbox",
            "top": "77dp",
            "width": "230px",
            "overrides": {
                "ToolTip": {
                    "isVisible": false,
                    "left": "298dp",
                    "top": "77dp"
                },
                "flxToolTipMessage": {
                    "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                    "centerX": "50%",
                    "left": "0dp",
                    "minHeight": "70dp",
                    "top": "9dp",
                    "width": "210dp"
                },
                "lblNoConcentToolTip": {
                    "bottom": "10dp",
                    "height": kony.flex.USE_PREFFERED_SIZE,
                    "left": "5dp",
                    "top": "10dp",
                    "width": "195dp"
                },
                "lblarrow": {
                    "centerX": "viz.val_cleared",
                    "height": "10dp",
                    "left": "viz.val_cleared",
                    "right": "20dp",
                    "width": "17dp"
                }
            }
        }, controller.args[0], "ToolTip"), extendConfig({
            "overrides": {}
        }, controller.args[1], "ToolTip"), extendConfig({
            "overrides": {}
        }, controller.args[2], "ToolTip"));
        assignLimits.add(flxAssignLimits, ToolTip);
        return assignLimits;
    }
})