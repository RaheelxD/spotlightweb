define("com/adminConsole/common/NotificationFlagMessage/userNotificationFlagMessageController", function() {
    return {};
});
define("com/adminConsole/common/NotificationFlagMessage/NotificationFlagMessageControllerActions", {
    /*
          This is an auto generated file and any modifications to it may result in corruption of the action sequence.
        */
});
define("com/adminConsole/common/NotificationFlagMessage/NotificationFlagMessageController", ["com/adminConsole/common/NotificationFlagMessage/userNotificationFlagMessageController", "com/adminConsole/common/NotificationFlagMessage/NotificationFlagMessageControllerActions"], function() {
    var controller = require("com/adminConsole/common/NotificationFlagMessage/userNotificationFlagMessageController");
    var actions = require("com/adminConsole/common/NotificationFlagMessage/NotificationFlagMessageControllerActions");
    for (var key in actions) {
        controller[key] = actions[key];
    }
    return controller;
});
