define(function() {
    return function(controller) {
        var NotificationFlagMessage = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
            "clipBounds": true,
            "isMaster": true,
            "id": "NotificationFlagMessage",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "left": "0dp",
            "isModalContainer": false,
            "skin": "slFbox",
            "top": "0dp",
            "width": "100%"
        }, controller.args[0], "NotificationFlagMessage"), extendConfig({}, controller.args[1], "NotificationFlagMessage"), extendConfig({}, controller.args[2], "NotificationFlagMessage"));
        NotificationFlagMessage.setDefaultUnit(kony.flex.DP);
        var flxFlagContainer = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "clipBounds": true,
            "height": "40dp",
            "id": "flxFlagContainer",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "left": "0dp",
            "isModalContainer": false,
            "right": "0dp",
            "skin": "sknFlxBgFFFFFFBor4A77A0S1pxR1px",
            "top": "0dp",
            "zIndex": 1
        }, controller.args[0], "flxFlagContainer"), extendConfig({}, controller.args[1], "flxFlagContainer"), extendConfig({}, controller.args[2], "flxFlagContainer"));
        flxFlagContainer.setDefaultUnit(kony.flex.DP);
        var flxIconContainer = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "clipBounds": true,
            "height": "40dp",
            "id": "flxIconContainer",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "left": "0dp",
            "isModalContainer": false,
            "skin": "sknFlxBg4A77A0",
            "top": "0dp",
            "width": "40dp",
            "zIndex": 1
        }, controller.args[0], "flxIconContainer"), extendConfig({}, controller.args[1], "flxIconContainer"), extendConfig({}, controller.args[2], "flxIconContainer"));
        flxIconContainer.setDefaultUnit(kony.flex.DP);
        var lblNotificationIcon = new kony.ui.Label(extendConfig({
            "centerX": "48%",
            "centerY": "48%",
            "height": "19px",
            "id": "lblNotificationIcon",
            "isVisible": true,
            "skin": "sknErrorWhiteIcon19px",
            "text": "",
            "width": "19px",
            "zIndex": 1
        }, controller.args[0], "lblNotificationIcon"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_CENTER,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblNotificationIcon"), extendConfig({}, controller.args[2], "lblNotificationIcon"));
        flxIconContainer.add(lblNotificationIcon);
        var lblMessage = new kony.ui.Label(extendConfig({
            "centerY": "50%",
            "id": "lblMessage",
            "isVisible": true,
            "left": "50dp",
            "skin": "sknLbl485C75LatoRegular13Px",
            "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmCompanies.Select_atleast_one_account_to_proceed\")",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, controller.args[0], "lblMessage"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblMessage"), extendConfig({}, controller.args[2], "lblMessage"));
        flxFlagContainer.add(flxIconContainer, lblMessage);
        NotificationFlagMessage.add(flxFlagContainer);
        return NotificationFlagMessage;
    }
})