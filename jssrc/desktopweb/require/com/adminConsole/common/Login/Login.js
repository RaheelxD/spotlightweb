define(function() {
    return function(controller) {
        var Login = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "clipBounds": true,
            "isMaster": true,
            "height": "100%",
            "id": "Login",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "left": "0dp",
            "isModalContainer": false,
            "preShow": function(eventobject) {
                controller.AS_FlexContainer_af5b4bc19d844c91b6848633faf495f1(eventobject);
            },
            "skin": "slFbox",
            "top": "0dp",
            "width": "100%"
        }, controller.args[0], "Login"), extendConfig({}, controller.args[1], "Login"), extendConfig({}, controller.args[2], "Login"));
        Login.setDefaultUnit(kony.flex.DP);
        var flxLogin = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "clipBounds": true,
            "height": "100%",
            "id": "flxLogin",
            "isVisible": false,
            "layoutType": kony.flex.FREE_FORM,
            "left": "0dp",
            "masterType": constants.MASTER_TYPE_USERWIDGET,
            "isModalContainer": false,
            "skin": "slFbox0i27a4037c45c43",
            "top": "0dp",
            "width": "385dp",
            "zIndex": 1
        }, controller.args[0], "flxLogin"), extendConfig({}, controller.args[1], "flxLogin"), extendConfig({}, controller.args[2], "flxLogin"));
        flxLogin.setDefaultUnit(kony.flex.DP);
        var lblHeading = new kony.ui.Label(extendConfig({
            "height": "30px",
            "id": "lblHeading",
            "isVisible": true,
            "left": "40dp",
            "skin": "sknlbl484b52SourceSans16px",
            "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmLogin.welcomeHeader\")",
            "top": "40dp",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, controller.args[0], "lblHeading"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_CENTER,
            "padding": [1, 0, 1, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblHeading"), extendConfig({}, controller.args[2], "lblHeading"));
        var lblUserid = new kony.ui.Label(extendConfig({
            "id": "lblUserid",
            "isVisible": true,
            "left": "40dp",
            "skin": "slLabel0d20174dce8ea42",
            "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmCustomerManagementController.User_ID\")",
            "top": "100dp",
            "width": "200dp",
            "zIndex": 1
        }, controller.args[0], "lblUserid"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblUserid"), extendConfig({}, controller.args[2], "lblUserid"));
        var txtUserName = new kony.ui.TextBox2(extendConfig({
            "autoCapitalize": constants.TEXTBOX_AUTO_CAPITALIZE_NONE,
            "focusSkin": "skntbxLatof13pxFocus",
            "height": "40dp",
            "id": "txtUserName",
            "isVisible": true,
            "keyBoardStyle": constants.TEXTBOX_KEY_BOARD_STYLE_DEFAULT,
            "left": "40dp",
            "i18n_placeholder": "kony.i18n.getLocalizedString(\"i18n.frmLogin.Enter_Username\")",
            "right": 40,
            "secureTextEntry": false,
            "skin": "Skinuseridfield0bc733c4148544e",
            "textInputMode": constants.TEXTBOX_INPUT_MODE_ANY,
            "top": "125dp",
            "zIndex": 1
        }, controller.args[0], "txtUserName"), extendConfig({
            "containerHeightMode": constants.TEXTBOX_FONT_METRICS_DRIVEN_HEIGHT,
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [3, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "txtUserName"), extendConfig({
            "autoCorrect": false,
            "onBeginEditing": controller.AS_TextField_b9c2411768f14bf19f3784bfbd790d85
        }, controller.args[2], "txtUserName"));
        var lblPassword = new kony.ui.Label(extendConfig({
            "id": "lblPassword",
            "isVisible": true,
            "left": "40dp",
            "skin": "slLabel0d20174dce8ea42",
            "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmCreateNewUser.lblPasswordUser\")",
            "top": "200dp",
            "width": "200dp",
            "zIndex": 1
        }, controller.args[0], "lblPassword"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblPassword"), extendConfig({}, controller.args[2], "lblPassword"));
        var txtPassword = new kony.ui.TextBox2(extendConfig({
            "autoCapitalize": constants.TEXTBOX_AUTO_CAPITALIZE_NONE,
            "focusSkin": "skntbxLatof13pxFocus",
            "height": "40dp",
            "id": "txtPassword",
            "isVisible": true,
            "keyBoardStyle": constants.TEXTBOX_KEY_BOARD_STYLE_DEFAULT,
            "left": "40dp",
            "i18n_placeholder": "kony.i18n.getLocalizedString(\"i18n.frmLogin.Enter_Password\")",
            "right": 40,
            "secureTextEntry": true,
            "skin": "skinPasswordLogin",
            "textInputMode": constants.TEXTBOX_INPUT_MODE_ANY,
            "top": "225dp",
            "zIndex": 1
        }, controller.args[0], "txtPassword"), extendConfig({
            "containerHeightMode": constants.TEXTBOX_FONT_METRICS_DRIVEN_HEIGHT,
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [3, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "txtPassword"), extendConfig({
            "autoCorrect": false,
            "onBeginEditing": controller.AS_TextField_b8175308d8424df495ecc3ca9d37b599
        }, controller.args[2], "txtPassword"));
        var chkbxRemember = new kony.ui.CheckBoxGroup(extendConfig({
            "height": "23px",
            "id": "chkbxRemember",
            "isVisible": false,
            "left": "42dp",
            "masterData": [
                ["true", "Remember me"]
            ],
            "selectedKeys": ["true"],
            "skin": "slCheckBoxGroup0h1860dff06d344",
            "top": "242dp",
            "width": "50%",
            "zIndex": 1
        }, controller.args[0], "chkbxRemember"), extendConfig({
            "itemOrientation": constants.CHECKBOX_ITEM_ORIENTATION_VERTICAL,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "chkbxRemember"), extendConfig({}, controller.args[2], "chkbxRemember"));
        var flxLoginRememberMe = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "clipBounds": true,
            "height": "15px",
            "id": "flxLoginRememberMe",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "left": "42dp",
            "isModalContainer": false,
            "top": "300dp",
            "width": "130px",
            "zIndex": 1
        }, controller.args[0], "flxLoginRememberMe"), extendConfig({}, controller.args[1], "flxLoginRememberMe"), extendConfig({}, controller.args[2], "flxLoginRememberMe"));
        flxLoginRememberMe.setDefaultUnit(kony.flex.DP);
        var flxRememberMe = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "clipBounds": true,
            "height": "100%",
            "id": "flxRememberMe",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "left": "0dp",
            "isModalContainer": false,
            "skin": "slFbox",
            "top": "0dp",
            "width": "15px",
            "zIndex": 1
        }, controller.args[0], "flxRememberMe"), extendConfig({}, controller.args[1], "flxRememberMe"), extendConfig({
            "hoverSkin": "sknCursor"
        }, controller.args[2], "flxRememberMe"));
        flxRememberMe.setDefaultUnit(kony.flex.DP);
        var imgLoginRememberMe = new kony.ui.Image2(extendConfig({
            "height": "100%",
            "id": "imgLoginRememberMe",
            "isVisible": true,
            "left": "0px",
            "skin": "slImage",
            "src": "checkboxnormal.png",
            "top": "0px",
            "width": "15px",
            "zIndex": 1
        }, controller.args[0], "imgLoginRememberMe"), extendConfig({
            "imageScaleMode": constants.IMAGE_SCALE_MODE_MAINTAIN_ASPECT_RATIO,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "imgLoginRememberMe"), extendConfig({}, controller.args[2], "imgLoginRememberMe"));
        flxRememberMe.add(imgLoginRememberMe);
        var lblLoginRememberMe = new kony.ui.Label(extendConfig({
            "id": "lblLoginRememberMe",
            "isVisible": true,
            "left": "25dp",
            "skin": "slLabel0d20174dce8ea42",
            "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmLogin.lblLoginRememberMe\")",
            "top": "0dp",
            "width": "100px",
            "zIndex": 1
        }, controller.args[0], "lblLoginRememberMe"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblLoginRememberMe"), extendConfig({}, controller.args[2], "lblLoginRememberMe"));
        flxLoginRememberMe.add(flxRememberMe, lblLoginRememberMe);
        var btnImage = new kony.ui.Button(extendConfig({
            "focusSkin": "slButtonGlossRed0ac4572db3b9342",
            "height": "25dp",
            "id": "btnImage",
            "isVisible": false,
            "left": "250dp",
            "skin": "slButtonGlossBlue0a0c10b45f20d47",
            "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmAlertsManagement.lblAlertTypeDescriptionCount\")",
            "top": "182dp",
            "width": "40dp",
            "zIndex": 1
        }, controller.args[0], "btnImage"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_CENTER,
            "displayText": true,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "btnImage"), extendConfig({}, controller.args[2], "btnImage"));
        var flxUsernameErrorMsg = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
            "clipBounds": true,
            "id": "flxUsernameErrorMsg",
            "isVisible": false,
            "layoutType": kony.flex.FREE_FORM,
            "left": "11%",
            "minHeight": "10px",
            "isModalContainer": false,
            "skin": "slFbox",
            "top": "170px",
            "width": "273px"
        }, controller.args[0], "flxUsernameErrorMsg"), extendConfig({}, controller.args[1], "flxUsernameErrorMsg"), extendConfig({}, controller.args[2], "flxUsernameErrorMsg"));
        flxUsernameErrorMsg.setDefaultUnit(kony.flex.DP);
        var lblUsernameErrorMsg = new kony.ui.Label(extendConfig({
            "id": "lblUsernameErrorMsg",
            "isVisible": true,
            "left": "15px",
            "skin": "sknlblError",
            "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmLogin.lblUsernameErrorMsg\")",
            "top": "0px",
            "width": "256px",
            "zIndex": 1
        }, controller.args[0], "lblUsernameErrorMsg"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblUsernameErrorMsg"), extendConfig({}, controller.args[2], "lblUsernameErrorMsg"));
        var lblUsernameErrorIcon = new kony.ui.Label(extendConfig({
            "height": "15dp",
            "id": "lblUsernameErrorIcon",
            "isVisible": true,
            "left": "0dp",
            "skin": "sknFontIconError",
            "text": "",
            "top": "0dp",
            "width": "15dp",
            "zIndex": 1
        }, controller.args[0], "lblUsernameErrorIcon"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblUsernameErrorIcon"), extendConfig({}, controller.args[2], "lblUsernameErrorIcon"));
        flxUsernameErrorMsg.add(lblUsernameErrorMsg, lblUsernameErrorIcon);
        var flxPasswordErrorMsg = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
            "clipBounds": true,
            "id": "flxPasswordErrorMsg",
            "isVisible": false,
            "layoutType": kony.flex.FREE_FORM,
            "left": "11%",
            "minHeight": "10px",
            "isModalContainer": false,
            "skin": "slFbox",
            "top": "270px",
            "width": "273px"
        }, controller.args[0], "flxPasswordErrorMsg"), extendConfig({}, controller.args[1], "flxPasswordErrorMsg"), extendConfig({}, controller.args[2], "flxPasswordErrorMsg"));
        flxPasswordErrorMsg.setDefaultUnit(kony.flex.DP);
        var lblPasswordErrorMsg = new kony.ui.Label(extendConfig({
            "id": "lblPasswordErrorMsg",
            "isVisible": true,
            "left": "15px",
            "skin": "sknlblError",
            "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmLogin.lblPasswordErrorMsg\")",
            "top": "0px",
            "width": "256px",
            "zIndex": 1
        }, controller.args[0], "lblPasswordErrorMsg"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblPasswordErrorMsg"), extendConfig({}, controller.args[2], "lblPasswordErrorMsg"));
        var lblPasswordErrorIcon = new kony.ui.Label(extendConfig({
            "height": "15dp",
            "id": "lblPasswordErrorIcon",
            "isVisible": true,
            "left": "0dp",
            "skin": "sknFontIconError",
            "text": "",
            "top": "0dp",
            "width": "15dp",
            "zIndex": 1
        }, controller.args[0], "lblPasswordErrorIcon"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblPasswordErrorIcon"), extendConfig({}, controller.args[2], "lblPasswordErrorIcon"));
        flxPasswordErrorMsg.add(lblPasswordErrorMsg, lblPasswordErrorIcon);
        var flxEyeIconContainer = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "clipBounds": true,
            "height": "36px",
            "id": "flxEyeIconContainer",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "isModalContainer": false,
            "right": "42px",
            "skin": "sknFlxTrans",
            "top": "227px",
            "width": "36px"
        }, controller.args[0], "flxEyeIconContainer"), extendConfig({}, controller.args[1], "flxEyeIconContainer"), extendConfig({}, controller.args[2], "flxEyeIconContainer"));
        flxEyeIconContainer.setDefaultUnit(kony.flex.DP);
        var flxEyecross = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "centerX": "50%",
            "centerY": "50%",
            "clipBounds": true,
            "height": "25dp",
            "id": "flxEyecross",
            "isVisible": false,
            "layoutType": kony.flex.FREE_FORM,
            "isModalContainer": false,
            "skin": "slFbox",
            "width": "25dp",
            "zIndex": 2
        }, controller.args[0], "flxEyecross"), extendConfig({}, controller.args[1], "flxEyecross"), extendConfig({}, controller.args[2], "flxEyecross"));
        flxEyecross.setDefaultUnit(kony.flex.DP);
        var lblEyecross = new kony.ui.Label(extendConfig({
            "centerX": "50%",
            "centerY": "50%",
            "height": "25dp",
            "id": "lblEyecross",
            "isVisible": true,
            "skin": "sknEyeIcon",
            "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmLogin.lblEyecross\")",
            "width": "25dp",
            "zIndex": 1
        }, controller.args[0], "lblEyecross"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblEyecross"), extendConfig({
            "hoverSkin": "sknEyeIconHover"
        }, controller.args[2], "lblEyecross"));
        flxEyecross.add(lblEyecross);
        var flxEyeicon = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "centerX": "50%",
            "centerY": "50%",
            "clipBounds": true,
            "height": "25dp",
            "id": "flxEyeicon",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "isModalContainer": false,
            "skin": "slFbox",
            "width": "25dp",
            "zIndex": 2
        }, controller.args[0], "flxEyeicon"), extendConfig({}, controller.args[1], "flxEyeicon"), extendConfig({}, controller.args[2], "flxEyeicon"));
        flxEyeicon.setDefaultUnit(kony.flex.DP);
        var lblEyeicon = new kony.ui.Label(extendConfig({
            "centerX": "50%",
            "centerY": "50%",
            "height": 25,
            "id": "lblEyeicon",
            "isVisible": true,
            "skin": "sknEyeIcon",
            "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmLogin.lblEyeicon\")",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 2
        }, controller.args[0], "lblEyeicon"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblEyeicon"), extendConfig({}, controller.args[2], "lblEyeicon"));
        flxEyeicon.add(lblEyeicon);
        flxEyeIconContainer.add(flxEyecross, flxEyeicon);
        var flxLoginButtons = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "clipBounds": true,
            "height": "40px",
            "id": "flxLoginButtons",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "left": "40dp",
            "isModalContainer": false,
            "right": "40dp",
            "skin": "slFbox",
            "top": "350px"
        }, controller.args[0], "flxLoginButtons"), extendConfig({}, controller.args[1], "flxLoginButtons"), extendConfig({}, controller.args[2], "flxLoginButtons"));
        flxLoginButtons.setDefaultUnit(kony.flex.DP);
        var flxForgotCredentials = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "centerY": "50%",
            "clipBounds": true,
            "height": "20px",
            "id": "flxForgotCredentials",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "left": "0px",
            "isModalContainer": false,
            "skin": "sknCursor",
            "top": "0px",
            "width": "190px",
            "zIndex": 10
        }, controller.args[0], "flxForgotCredentials"), extendConfig({}, controller.args[1], "flxForgotCredentials"), extendConfig({
            "hoverSkin": "hoverhandSkin2"
        }, controller.args[2], "flxForgotCredentials"));
        flxForgotCredentials.setDefaultUnit(kony.flex.DP);
        var lblForgotCredentials = new kony.ui.Label(extendConfig({
            "centerY": "50%",
            "id": "lblForgotCredentials",
            "isVisible": true,
            "left": "0dp",
            "skin": "sknLblLato13px117eb0",
            "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmLogin.flxForgotCredentials\")",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, controller.args[0], "lblForgotCredentials"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_CENTER,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblForgotCredentials"), extendConfig({}, controller.args[2], "lblForgotCredentials"));
        flxForgotCredentials.add(lblForgotCredentials);
        var btnLogin = new kony.ui.Button(extendConfig({
            "focusSkin": "sknBtn003E75LatoRegular13pxFFFFFFRad20px",
            "height": "40dp",
            "id": "btnLogin",
            "isVisible": true,
            "right": "0px",
            "skin": "sknBtn003E75LatoRegular13pxFFFFFFRad20px",
            "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmLogin.btnLogin\")",
            "top": "0dp",
            "width": "107dp",
            "zIndex": 1
        }, controller.args[0], "btnLogin"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_CENTER,
            "displayText": true,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "btnLogin"), extendConfig({
            "hoverSkin": "sknBtn005198LatoRegular13pxFFFFFFRad20px"
        }, controller.args[2], "btnLogin"));
        flxLoginButtons.add(flxForgotCredentials, btnLogin);
        flxLogin.add(lblHeading, lblUserid, txtUserName, lblPassword, txtPassword, chkbxRemember, flxLoginRememberMe, btnImage, flxUsernameErrorMsg, flxPasswordErrorMsg, flxEyeIconContainer, flxLoginButtons);
        var flxRedirect = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "clipBounds": true,
            "height": "100%",
            "id": "flxRedirect",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "left": "0dp",
            "masterType": constants.MASTER_TYPE_USERWIDGET,
            "isModalContainer": false,
            "skin": "slFbox0i27a4037c45c43",
            "top": "0dp",
            "width": "385dp",
            "zIndex": 1
        }, controller.args[0], "flxRedirect"), extendConfig({}, controller.args[1], "flxRedirect"), extendConfig({}, controller.args[2], "flxRedirect"));
        flxRedirect.setDefaultUnit(kony.flex.DP);
        var CopylblHeading0bbc842957fbb43 = new kony.ui.Label(extendConfig({
            "centerX": "50%",
            "centerY": "50%",
            "height": "30px",
            "id": "CopylblHeading0bbc842957fbb43",
            "isVisible": true,
            "skin": "sknlbl484b52SourceSans16px",
            "text": "Redirecting to Keycloak for Login...",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, controller.args[0], "CopylblHeading0bbc842957fbb43"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_CENTER,
            "padding": [1, 0, 1, 0],
            "paddingInPixel": false
        }, controller.args[1], "CopylblHeading0bbc842957fbb43"), extendConfig({}, controller.args[2], "CopylblHeading0bbc842957fbb43"));
        flxRedirect.add(CopylblHeading0bbc842957fbb43);
        Login.add(flxLogin, flxRedirect);
        return Login;
    }
})