define("com/adminConsole/common/Login/userLoginController", ['./LoginBusinessController', 'Navigation_Presentation_Extn'], function(LoginBusinessController, navigation) {
    return {
        constructor: function(baseConfig, layoutConfig, pspConfig) {
            this.LoginBusinessController = new LoginBusinessController();
        },
        //Logic for getters/setters of custom properties
        initGettersSetters: function() {},
        preShow: function() {
            var scopeObj = this;
            scopeObj.login();
        },
        login: function() {
            //       var usernamePasswordObj={
            //         "inputUsername":this.view.txtUserName.text,
            //         "inputPassword":this.view.txtPassword.text,
            //         "loginOptions": {
            //           "isSSOEnabled": true,
            //           "isOfflineEnabled": false
            //         }
            //       };
            //       kony.print("usernamePasswordObj: "+JSON.stringify(usernamePasswordObj));
            kony.adminConsole.utils.showProgressBar(this.view);
            var options = {};
            options.include_profile = true;
            //browserWidget is mandatory if using the MVC Architecture
            var loginOptions = {};
            loginOptions.isSSOEnabled = false;
            loginOptions.continueOnRefreshError = false;
            loginOptions.persistLoginResponse = true;
            options.loginOptions = loginOptions;
            this.LoginBusinessController.login(options, this.onLoginSuccess, this.onLoginFailure);
        },
        onLoginSuccess: function() {
            kony.application.dismissLoadingScreen();
            navigation.navigateTo('DashboardModule', 'fetchDashBoardData');
            //loginSuccessCallBack(response, usernamePasswordObj);
        },
        onLoginFailure: function() {
            kony.application.dismissLoadingScreen();
            //loginFailureCallBack(response);
        }
    };
});
define("com/adminConsole/common/Login/LoginControllerActions", {
    /*
        This is an auto generated file and any modifications to it may result in corruption of the action sequence.
    */
});
define("com/adminConsole/common/Login/LoginController", ["com/adminConsole/common/Login/userLoginController", "com/adminConsole/common/Login/LoginControllerActions"], function() {
    var controller = require("com/adminConsole/common/Login/userLoginController");
    var actions = require("com/adminConsole/common/Login/LoginControllerActions");
    for (var key in actions) {
        controller[key] = actions[key];
    }
    controller.initializeProperties = function() {
        if (this.initGettersSetters) {
            this.initGettersSetters.apply(this, arguments);
        }
    };
    return controller;
});
