define(function() {
    return function(controller) {
        var noResultsWithButton = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
            "clipBounds": true,
            "isMaster": true,
            "id": "noResultsWithButton",
            "isVisible": true,
            "layoutType": kony.flex.FLOW_VERTICAL,
            "left": "0dp",
            "isModalContainer": false,
            "skin": "sknFlxFFFFFF100O",
            "top": "0dp",
            "width": "100%"
        }, controller.args[0], "noResultsWithButton"), extendConfig({}, controller.args[1], "noResultsWithButton"), extendConfig({}, controller.args[2], "noResultsWithButton"));
        noResultsWithButton.setDefaultUnit(kony.flex.DP);
        var lblNoRecordsAvailable = new kony.ui.Label(extendConfig({
            "centerX": "50%",
            "id": "lblNoRecordsAvailable",
            "isVisible": true,
            "skin": "sknLbl485C75LatoRegular13Px",
            "text": "No records available",
            "top": "20dp",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, controller.args[0], "lblNoRecordsAvailable"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblNoRecordsAvailable"), extendConfig({}, controller.args[2], "lblNoRecordsAvailable"));
        var btnAddRecord = new kony.ui.Button(extendConfig({
            "bottom": "20dp",
            "centerX": "50%",
            "height": "22px",
            "id": "btnAddRecord",
            "isVisible": true,
            "skin": "sknbtnf7f7faLatoRegular12Px485c75Border1px485C75Radius20px",
            "text": "Add Button",
            "top": "15dp",
            "width": "90dp",
            "zIndex": 1
        }, controller.args[0], "btnAddRecord"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_CENTER,
            "displayText": true,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "btnAddRecord"), extendConfig({
            "hoverSkin": "sknbtnffffffLatoRegular12Px485c75Border1px485C75Radius20px"
        }, controller.args[2], "btnAddRecord"));
        noResultsWithButton.add(lblNoRecordsAvailable, btnAddRecord);
        return noResultsWithButton;
    }
})