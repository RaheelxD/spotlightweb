define("com/adminConsole/common/noResultsWithButton/usernoResultsWithButtonController", function() {
    return {};
});
define("com/adminConsole/common/noResultsWithButton/noResultsWithButtonControllerActions", {
    /*
          This is an auto generated file and any modifications to it may result in corruption of the action sequence.
        */
});
define("com/adminConsole/common/noResultsWithButton/noResultsWithButtonController", ["com/adminConsole/common/noResultsWithButton/usernoResultsWithButtonController", "com/adminConsole/common/noResultsWithButton/noResultsWithButtonControllerActions"], function() {
    var controller = require("com/adminConsole/common/noResultsWithButton/usernoResultsWithButtonController");
    var actions = require("com/adminConsole/common/noResultsWithButton/noResultsWithButtonControllerActions");
    for (var key in actions) {
        controller[key] = actions[key];
    }
    return controller;
});
