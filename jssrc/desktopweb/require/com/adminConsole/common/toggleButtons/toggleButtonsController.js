define("com/adminConsole/common/toggleButtons/usertoggleButtonsController", function() {
    return {};
});
define("com/adminConsole/common/toggleButtons/toggleButtonsControllerActions", {
    /*
          This is an auto generated file and any modifications to it may result in corruption of the action sequence.
        */
});
define("com/adminConsole/common/toggleButtons/toggleButtonsController", ["com/adminConsole/common/toggleButtons/usertoggleButtonsController", "com/adminConsole/common/toggleButtons/toggleButtonsControllerActions"], function() {
    var controller = require("com/adminConsole/common/toggleButtons/usertoggleButtonsController");
    var actions = require("com/adminConsole/common/toggleButtons/toggleButtonsControllerActions");
    for (var key in actions) {
        controller[key] = actions[key];
    }
    return controller;
});
