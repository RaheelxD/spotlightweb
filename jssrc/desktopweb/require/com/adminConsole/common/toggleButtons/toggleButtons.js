define(function() {
    return function(controller) {
        var toggleButtons = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "clipBounds": true,
            "isMaster": true,
            "height": "100%",
            "id": "toggleButtons",
            "isVisible": true,
            "layoutType": kony.flex.FLOW_HORIZONTAL,
            "left": "0dp",
            "isModalContainer": false,
            "skin": "slFbox",
            "top": "0dp",
            "width": "100%"
        }, controller.args[0], "toggleButtons"), extendConfig({}, controller.args[1], "toggleButtons"), extendConfig({}, controller.args[2], "toggleButtons"));
        toggleButtons.setDefaultUnit(kony.flex.DP);
        var btnToggleLeft = new kony.ui.Button(extendConfig({
            "height": "30dp",
            "id": "btnToggleLeft",
            "isVisible": true,
            "left": "0dp",
            "skin": "sknBtnBgE5F0F9Br006CCAFn0069cdSemiBold12pxLeftRnd",
            "text": "Option 1",
            "top": "0dp",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, controller.args[0], "btnToggleLeft"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_CENTER,
            "displayText": true,
            "padding": [2, 0, 2, 0],
            "paddingInPixel": false
        }, controller.args[1], "btnToggleLeft"), extendConfig({}, controller.args[2], "btnToggleLeft"));
        var btnToggleRight = new kony.ui.Button(extendConfig({
            "height": "30dp",
            "id": "btnToggleRight",
            "isVisible": true,
            "left": "0dp",
            "skin": "sknBtnBgFFFFFFBrD7D9E0Fn485C75Reg12pxRightRnd",
            "text": "Option 2",
            "top": "0dp",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, controller.args[0], "btnToggleRight"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_CENTER,
            "displayText": true,
            "padding": [2, 0, 2, 0],
            "paddingInPixel": false
        }, controller.args[1], "btnToggleRight"), extendConfig({}, controller.args[2], "btnToggleRight"));
        toggleButtons.add(btnToggleLeft, btnToggleRight);
        return toggleButtons;
    }
})