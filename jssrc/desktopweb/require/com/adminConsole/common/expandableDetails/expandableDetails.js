define(function() {
    return function(controller) {
        var expandableDetails = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
            "clipBounds": true,
            "isMaster": true,
            "id": "expandableDetails",
            "isVisible": true,
            "layoutType": kony.flex.FLOW_VERTICAL,
            "left": "0dp",
            "isModalContainer": false,
            "skin": "sknflxffffffop100Bordercbcdd1Radius3px",
            "top": "0dp",
            "width": "100%"
        }, controller.args[0], "expandableDetails"), extendConfig({}, controller.args[1], "expandableDetails"), extendConfig({}, controller.args[2], "expandableDetails"));
        expandableDetails.setDefaultUnit(kony.flex.DP);
        var flxDetailsHeader = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "clipBounds": true,
            "height": "60dp",
            "id": "flxDetailsHeader",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "left": "0px",
            "isModalContainer": false,
            "skin": "slFbox",
            "top": "0dp",
            "width": "100%",
            "zIndex": 1
        }, controller.args[0], "flxDetailsHeader"), extendConfig({}, controller.args[1], "flxDetailsHeader"), extendConfig({}, controller.args[2], "flxDetailsHeader"));
        flxDetailsHeader.setDefaultUnit(kony.flex.DP);
        var flxLeftOptions = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "clipBounds": true,
            "height": "100%",
            "id": "flxLeftOptions",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "left": "0dp",
            "isModalContainer": false,
            "skin": "slFbox",
            "top": "0dp",
            "width": "80%"
        }, controller.args[0], "flxLeftOptions"), extendConfig({}, controller.args[1], "flxLeftOptions"), extendConfig({}, controller.args[2], "flxLeftOptions"));
        flxLeftOptions.setDefaultUnit(kony.flex.DP);
        var lblExpandCollapseIcon = new kony.ui.Label(extendConfig({
            "centerY": "50%",
            "height": "15px",
            "id": "lblExpandCollapseIcon",
            "isVisible": true,
            "left": "20dp",
            "skin": "sknFontIconOptionMenu",
            "text": "",
            "width": "15px",
            "zIndex": 1
        }, controller.args[0], "lblExpandCollapseIcon"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblExpandCollapseIcon"), extendConfig({}, controller.args[2], "lblExpandCollapseIcon"));
        var lblMFAViewHeading = new kony.ui.Label(extendConfig({
            "bottom": "20px",
            "id": "lblMFAViewHeading",
            "isVisible": true,
            "left": "50px",
            "skin": "sknLbl192b45LatoReg16px",
            "text": "Retail Banking | Transactional - P2P Transfer",
            "top": "20px",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, controller.args[0], "lblMFAViewHeading"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblMFAViewHeading"), extendConfig({}, controller.args[2], "lblMFAViewHeading"));
        flxLeftOptions.add(lblExpandCollapseIcon, lblMFAViewHeading);
        var flxRightOptions = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "clipBounds": true,
            "height": "100%",
            "id": "flxRightOptions",
            "isVisible": true,
            "layoutType": kony.flex.FLOW_HORIZONTAL,
            "reverseLayoutDirection": true,
            "isModalContainer": false,
            "right": "0px",
            "skin": "slFbox",
            "top": "0dp",
            "width": "20%"
        }, controller.args[0], "flxRightOptions"), extendConfig({}, controller.args[1], "flxRightOptions"), extendConfig({}, controller.args[2], "flxRightOptions"));
        flxRightOptions.setDefaultUnit(kony.flex.DP);
        var flxOptions = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "centerY": "50%",
            "clipBounds": true,
            "height": "25dp",
            "id": "flxOptions",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "isModalContainer": false,
            "onClick": controller.AS_FlexContainer_a4df6b3c553a41f9b22caa3c8542e575,
            "right": "20px",
            "skin": "slFbox",
            "top": "10dp",
            "width": "25px",
            "zIndex": 2
        }, controller.args[0], "flxOptions"), extendConfig({}, controller.args[1], "flxOptions"), extendConfig({
            "hoverSkin": "sknflxffffffop100Border424242Radius100px"
        }, controller.args[2], "flxOptions"));
        flxOptions.setDefaultUnit(kony.flex.DP);
        var lblIconOptions = new kony.ui.Label(extendConfig({
            "centerX": "50%",
            "centerY": "50%",
            "height": "15px",
            "id": "lblIconOptions",
            "isVisible": true,
            "left": "14dp",
            "skin": "sknFontIconOptionMenu",
            "text": "",
            "width": "15px",
            "zIndex": 1
        }, controller.args[0], "lblIconOptions"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblIconOptions"), extendConfig({}, controller.args[2], "lblIconOptions"));
        flxOptions.add(lblIconOptions);
        var flxStatus = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "centerY": "50%",
            "clipBounds": true,
            "height": "20px",
            "id": "flxStatus",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "isModalContainer": false,
            "right": "0px",
            "skin": "slFbox",
            "width": "70px",
            "zIndex": 1
        }, controller.args[0], "flxStatus"), extendConfig({}, controller.args[1], "flxStatus"), extendConfig({}, controller.args[2], "flxStatus"));
        flxStatus.setDefaultUnit(kony.flex.DP);
        var imgServiceStatus = new kony.ui.Label(extendConfig({
            "centerY": "50%",
            "id": "imgServiceStatus",
            "isVisible": true,
            "left": "0dp",
            "skin": "sknFontIconActivate",
            "i18n_text": "kony.i18n.getLocalizedString(\"i18n.userwidgetmodel.lblFontIconStatus1\")",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, controller.args[0], "imgServiceStatus"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "imgServiceStatus"), extendConfig({}, controller.args[2], "imgServiceStatus"));
        var lblServiceStatus = new kony.ui.Label(extendConfig({
            "centerY": "50%",
            "id": "lblServiceStatus",
            "isVisible": true,
            "left": 20,
            "right": "20dp",
            "skin": "sknlblLato5bc06cBold14px",
            "text": "Active",
            "width": "50px",
            "zIndex": 1
        }, controller.args[0], "lblServiceStatus"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblServiceStatus"), extendConfig({}, controller.args[2], "lblServiceStatus"));
        flxStatus.add(imgServiceStatus, lblServiceStatus);
        flxRightOptions.add(flxOptions, flxStatus);
        flxDetailsHeader.add(flxLeftOptions, flxRightOptions);
        var flxDetailsBody = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "clipBounds": true,
            "height": "245dp",
            "id": "flxDetailsBody",
            "isVisible": true,
            "layoutType": kony.flex.FLOW_VERTICAL,
            "left": "0dp",
            "isModalContainer": false,
            "skin": "slFbox",
            "top": "0dp",
            "width": "100%",
            "zIndex": 1
        }, controller.args[0], "flxDetailsBody"), extendConfig({}, controller.args[1], "flxDetailsBody"), extendConfig({}, controller.args[2], "flxDetailsBody"));
        flxDetailsBody.setDefaultUnit(kony.flex.DP);
        var detailsBodyRow1 = new com.adminConsole.customerMang.productDetails(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "height": "45dp",
            "id": "detailsBodyRow1",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "left": "0dp",
            "masterType": constants.MASTER_TYPE_DEFAULT,
            "isModalContainer": false,
            "skin": "slFbox",
            "top": "0dp",
            "width": "100%",
            "overrides": {
                "btnData11": {
                    "isVisible": false
                },
                "flxColumn1": {
                    "height": "45px"
                },
                "flxColumn2": {
                    "height": "45px",
                    "layoutType": kony.flex.FLOW_VERTICAL
                },
                "flxColumn3": {
                    "height": "45px",
                    "layoutType": kony.flex.FLOW_VERTICAL
                },
                "flxDataAndImage2": {
                    "top": 10
                },
                "flxDataAndImage3": {
                    "top": 10
                },
                "flxDataRow1": {
                    "top": "10px"
                },
                "flxDataRow2": {
                    "isVisible": false
                },
                "imgData2": {
                    "src": "active_circle2x.png"
                },
                "imgData3": {
                    "src": "active_circle2x.png"
                },
                "lblData11": {
                    "text": "Data for details"
                },
                "productDetails": {
                    "height": "45dp"
                }
            }
        }, controller.args[0], "detailsBodyRow1"), extendConfig({
            "overrides": {}
        }, controller.args[1], "detailsBodyRow1"), extendConfig({
            "overrides": {}
        }, controller.args[2], "detailsBodyRow1"));
        var detailsBodyRow2 = new com.adminConsole.customerMang.productDetails(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "height": "45dp",
            "id": "detailsBodyRow2",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "left": "0dp",
            "masterType": constants.MASTER_TYPE_DEFAULT,
            "isModalContainer": false,
            "skin": "slFbox",
            "top": "15dp",
            "width": "100%",
            "overrides": {
                "btnData11": {
                    "isVisible": false
                },
                "flxColumn1": {
                    "height": "45px"
                },
                "flxColumn2": {
                    "height": "45px",
                    "layoutType": kony.flex.FLOW_VERTICAL
                },
                "flxColumn3": {
                    "height": "45px",
                    "layoutType": kony.flex.FLOW_VERTICAL
                },
                "flxDataAndImage2": {
                    "top": 10
                },
                "flxDataAndImage3": {
                    "top": 10
                },
                "flxDataRow1": {
                    "top": "10px"
                },
                "flxDataRow2": {
                    "isVisible": false
                },
                "imgData2": {
                    "src": "active_circle2x.png"
                },
                "imgData3": {
                    "src": "active_circle2x.png"
                },
                "lblData11": {
                    "text": "Data for details"
                },
                "productDetails": {
                    "height": "45dp",
                    "left": "0dp",
                    "top": "15dp"
                }
            }
        }, controller.args[0], "detailsBodyRow2"), extendConfig({
            "overrides": {}
        }, controller.args[1], "detailsBodyRow2"), extendConfig({
            "overrides": {}
        }, controller.args[2], "detailsBodyRow2"));
        var detailsBodyRow3 = new com.adminConsole.customerMang.productDetails(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "height": "45dp",
            "id": "detailsBodyRow3",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "left": "0dp",
            "masterType": constants.MASTER_TYPE_DEFAULT,
            "isModalContainer": false,
            "skin": "slFbox",
            "top": "15dp",
            "width": "100%",
            "overrides": {
                "btnData11": {
                    "isVisible": false
                },
                "flxColumn1": {
                    "height": "45px"
                },
                "flxColumn2": {
                    "height": "45px",
                    "layoutType": kony.flex.FLOW_VERTICAL
                },
                "flxColumn3": {
                    "height": "45px",
                    "layoutType": kony.flex.FLOW_VERTICAL
                },
                "flxDataAndImage2": {
                    "top": 10
                },
                "flxDataAndImage3": {
                    "top": 10
                },
                "flxDataRow1": {
                    "top": "10px"
                },
                "flxDataRow2": {
                    "isVisible": false
                },
                "imgData2": {
                    "src": "active_circle2x.png"
                },
                "imgData3": {
                    "src": "active_circle2x.png"
                },
                "lblData11": {
                    "text": "Data for details"
                },
                "productDetails": {
                    "height": "45dp",
                    "left": "0dp",
                    "top": "15dp"
                }
            }
        }, controller.args[0], "detailsBodyRow3"), extendConfig({
            "overrides": {}
        }, controller.args[1], "detailsBodyRow3"), extendConfig({
            "overrides": {}
        }, controller.args[2], "detailsBodyRow3"));
        var detailsBodyRow4 = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "clipBounds": true,
            "height": "45dp",
            "id": "detailsBodyRow4",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "left": "0dp",
            "isModalContainer": false,
            "skin": "slFbox",
            "top": "15dp",
            "width": "100%",
            "zIndex": 1
        }, controller.args[0], "detailsBodyRow4"), extendConfig({}, controller.args[1], "detailsBodyRow4"), extendConfig({}, controller.args[2], "detailsBodyRow4"));
        detailsBodyRow4.setDefaultUnit(kony.flex.DP);
        var flxColumn4 = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "clipBounds": true,
            "height": "45px",
            "id": "flxColumn4",
            "isVisible": true,
            "layoutType": kony.flex.FLOW_VERTICAL,
            "left": "20px",
            "isModalContainer": false,
            "right": "20dp",
            "skin": "slFbox",
            "top": "0dp",
            "zIndex": 1
        }, controller.args[0], "flxColumn4"), extendConfig({}, controller.args[1], "flxColumn4"), extendConfig({}, controller.args[2], "flxColumn4"));
        flxColumn4.setDefaultUnit(kony.flex.DP);
        var lblHeading4 = new kony.ui.Label(extendConfig({
            "id": "lblHeading4",
            "isVisible": true,
            "left": "0px",
            "skin": "sknlblLato5d6c7f12px",
            "i18n_text": "kony.i18n.getLocalizedString(\"i18n.userwidgetmodel.lblHeading1\")",
            "top": "0px",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, controller.args[0], "lblHeading4"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblHeading4"), extendConfig({}, controller.args[2], "lblHeading4"));
        var lblData4 = new kony.ui.Label(extendConfig({
            "centerY": "50%",
            "id": "lblData4",
            "isVisible": true,
            "left": "0px",
            "skin": "sknlblLatoBold35475f14px",
            "i18n_text": "kony.i18n.getLocalizedString(\"i18n.userwidgetmodel.lblData1\")",
            "top": "0px",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, controller.args[0], "lblData4"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblData4"), extendConfig({}, controller.args[2], "lblData4"));
        flxColumn4.add(lblHeading4, lblData4);
        detailsBodyRow4.add(flxColumn4);
        flxDetailsBody.add(detailsBodyRow1, detailsBodyRow2, detailsBodyRow3, detailsBodyRow4);
        expandableDetails.add(flxDetailsHeader, flxDetailsBody);
        return expandableDetails;
    }
})