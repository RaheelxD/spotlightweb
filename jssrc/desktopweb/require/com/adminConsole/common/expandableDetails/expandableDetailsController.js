define("com/adminConsole/common/expandableDetails/userexpandableDetailsController", function() {
    return {};
});
define("com/adminConsole/common/expandableDetails/expandableDetailsControllerActions", {
    /*
        This is an auto generated file and any modifications to it may result in corruption of the action sequence.
    */
});
define("com/adminConsole/common/expandableDetails/expandableDetailsController", ["com/adminConsole/common/expandableDetails/userexpandableDetailsController", "com/adminConsole/common/expandableDetails/expandableDetailsControllerActions"], function() {
    var controller = require("com/adminConsole/common/expandableDetails/userexpandableDetailsController");
    var actions = require("com/adminConsole/common/expandableDetails/expandableDetailsControllerActions");
    for (var key in actions) {
        controller[key] = actions[key];
    }
    return controller;
});
