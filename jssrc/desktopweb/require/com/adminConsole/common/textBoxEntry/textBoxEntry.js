define(function() {
    return function(controller) {
        var textBoxEntry = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
            "clipBounds": true,
            "isMaster": true,
            "id": "textBoxEntry",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "left": "0dp",
            "isModalContainer": false,
            "preShow": function(eventobject) {
                controller.AS_FlexContainer_de9be5c29c094de1902dba128b9822ca(eventobject);
            },
            "skin": "slFbox",
            "top": "0dp",
            "width": "100%",
            "zIndex": 1
        }, controller.args[0], "textBoxEntry"), extendConfig({}, controller.args[1], "textBoxEntry"), extendConfig({}, controller.args[2], "textBoxEntry"));
        textBoxEntry.setDefaultUnit(kony.flex.DP);
        var flxHeading = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
            "clipBounds": true,
            "id": "flxHeading",
            "isVisible": true,
            "layoutType": kony.flex.FLOW_HORIZONTAL,
            "left": "10dp",
            "isModalContainer": false,
            "skin": "slFbox",
            "top": "0dp",
            "width": "70%"
        }, controller.args[0], "flxHeading"), extendConfig({}, controller.args[1], "flxHeading"), extendConfig({}, controller.args[2], "flxHeading"));
        flxHeading.setDefaultUnit(kony.flex.DP);
        var lblHeadingText = new kony.ui.Label(extendConfig({
            "id": "lblHeadingText",
            "isVisible": true,
            "left": "0dp",
            "skin": "sknlblLato485c7514px",
            "text": "Label",
            "top": "0dp",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, controller.args[0], "lblHeadingText"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblHeadingText"), extendConfig({}, controller.args[2], "lblHeadingText"));
        var lblOptional = new kony.ui.Label(extendConfig({
            "id": "lblOptional",
            "isVisible": false,
            "left": "5dp",
            "skin": "sknLblLato84939E12px",
            "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmPermissions.lblOptional\")",
            "top": "0dp",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, controller.args[0], "lblOptional"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblOptional"), extendConfig({}, controller.args[2], "lblOptional"));
        flxHeading.add(lblHeadingText, lblOptional);
        var lblCount = new kony.ui.Label(extendConfig({
            "id": "lblCount",
            "isVisible": false,
            "right": "0dp",
            "skin": "slLabel0d20174dce8ea42",
            "text": "0/10",
            "top": "0dp",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, controller.args[0], "lblCount"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblCount"), extendConfig({}, controller.args[2], "lblCount"));
        var flxEnterValue = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "clipBounds": true,
            "height": "40dp",
            "id": "flxEnterValue",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "left": "10dp",
            "isModalContainer": false,
            "right": "0dp",
            "skin": "sknflxEnterValueNormal",
            "top": "25dp",
            "zIndex": 1
        }, controller.args[0], "flxEnterValue"), extendConfig({}, controller.args[1], "flxEnterValue"), extendConfig({}, controller.args[2], "flxEnterValue"));
        flxEnterValue.setDefaultUnit(kony.flex.DP);
        var tbxEnterValue = new kony.ui.TextBox2(extendConfig({
            "autoCapitalize": constants.TEXTBOX_AUTO_CAPITALIZE_NONE,
            "height": "40dp",
            "id": "tbxEnterValue",
            "isVisible": true,
            "keyBoardStyle": constants.TEXTBOX_KEY_BOARD_STYLE_DEFAULT,
            "left": "0dp",
            "placeholder": "Placeholder",
            "right": "0dp",
            "secureTextEntry": false,
            "skin": "sknTbx485c75LatoReg13pxNoBor",
            "textInputMode": constants.TEXTBOX_INPUT_MODE_ANY,
            "top": "0dp",
            "zIndex": 1
        }, controller.args[0], "tbxEnterValue"), extendConfig({
            "containerHeightMode": constants.TEXTBOX_FONT_METRICS_DRIVEN_HEIGHT,
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [3, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "tbxEnterValue"), extendConfig({
            "autoCorrect": false,
            "placeholderSkin": "defTextBoxPlaceholder"
        }, controller.args[2], "tbxEnterValue"));
        var flxBtnCheck = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "clipBounds": true,
            "height": "40dp",
            "id": "flxBtnCheck",
            "isVisible": false,
            "layoutType": kony.flex.FREE_FORM,
            "isModalContainer": false,
            "right": "0dp",
            "skin": "slFbox",
            "top": "0dp",
            "width": "75dp"
        }, controller.args[0], "flxBtnCheck"), extendConfig({}, controller.args[1], "flxBtnCheck"), extendConfig({}, controller.args[2], "flxBtnCheck"));
        flxBtnCheck.setDefaultUnit(kony.flex.DP);
        var btnCheck = new kony.ui.Button(extendConfig({
            "height": "40dp",
            "id": "btnCheck",
            "isVisible": true,
            "left": "-5dp",
            "skin": "sknbtnEnterValueCheck",
            "text": "Check",
            "top": "0dp",
            "width": "80dp",
            "zIndex": 1
        }, controller.args[0], "btnCheck"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_CENTER,
            "displayText": true,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "btnCheck"), extendConfig({}, controller.args[2], "btnCheck"));
        var flxSepartor = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "clipBounds": true,
            "height": "40dp",
            "id": "flxSepartor",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "left": "0dp",
            "isModalContainer": false,
            "skin": "sknSeparatorCsr",
            "top": "0dp",
            "width": "1dp",
            "zIndex": 2
        }, controller.args[0], "flxSepartor"), extendConfig({}, controller.args[1], "flxSepartor"), extendConfig({}, controller.args[2], "flxSepartor"));
        flxSepartor.setDefaultUnit(kony.flex.DP);
        flxSepartor.add();
        flxBtnCheck.add(btnCheck, flxSepartor);
        flxEnterValue.add(tbxEnterValue, flxBtnCheck);
        var flxInlineError = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
            "clipBounds": true,
            "id": "flxInlineError",
            "isVisible": false,
            "layoutType": kony.flex.FLOW_HORIZONTAL,
            "left": "10dp",
            "isModalContainer": false,
            "right": "0dp",
            "skin": "slFbox",
            "top": "70dp",
            "width": "95%"
        }, controller.args[0], "flxInlineError"), extendConfig({}, controller.args[1], "flxInlineError"), extendConfig({}, controller.args[2], "flxInlineError"));
        flxInlineError.setDefaultUnit(kony.flex.DP);
        var lblErrorIcon = new kony.ui.Label(extendConfig({
            "id": "lblErrorIcon",
            "isVisible": true,
            "left": "0dp",
            "skin": "sknFontIconError",
            "text": "",
            "top": "1dp",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, controller.args[0], "lblErrorIcon"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblErrorIcon"), extendConfig({}, controller.args[2], "lblErrorIcon"));
        var lblErrorText = new kony.ui.Label(extendConfig({
            "id": "lblErrorText",
            "isVisible": true,
            "left": "5dp",
            "skin": "sknlblError",
            "text": "Error",
            "top": "0dp",
            "width": "85%",
            "zIndex": 1
        }, controller.args[0], "lblErrorText"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblErrorText"), extendConfig({}, controller.args[2], "lblErrorText"));
        flxInlineError.add(lblErrorIcon, lblErrorText);
        var flxUserNameError = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "clipBounds": true,
            "height": "25px",
            "id": "flxUserNameError",
            "isVisible": false,
            "layoutType": kony.flex.FLOW_HORIZONTAL,
            "left": "10dp",
            "isModalContainer": false,
            "skin": "slFbox",
            "top": "70dp",
            "width": "100%",
            "zIndex": 1
        }, controller.args[0], "flxUserNameError"), extendConfig({}, controller.args[1], "flxUserNameError"), extendConfig({}, controller.args[2], "flxUserNameError"));
        flxUserNameError.setDefaultUnit(kony.flex.DP);
        var lblUsernameCheckIcon = new kony.ui.Label(extendConfig({
            "height": "100%",
            "id": "lblUsernameCheckIcon",
            "isVisible": true,
            "left": "0dp",
            "skin": "sknlblError",
            "i18n_text": "kony.i18n.getLocalizedString(\"i18n.common.Username_cannot_be_empty\")",
            "top": "0dp",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, controller.args[0], "lblUsernameCheckIcon"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblUsernameCheckIcon"), extendConfig({}, controller.args[2], "lblUsernameCheckIcon"));
        var lblUsernameCheck = new kony.ui.Label(extendConfig({
            "height": "15dp",
            "id": "lblUsernameCheck",
            "isVisible": true,
            "left": "0dp",
            "skin": "sknErrorIcon",
            "text": "",
            "top": "5dp",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, controller.args[0], "lblUsernameCheck"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblUsernameCheck"), extendConfig({}, controller.args[2], "lblUsernameCheck"));
        flxUserNameError.add(lblUsernameCheckIcon, lblUsernameCheck);
        textBoxEntry.add(flxHeading, lblCount, flxEnterValue, flxInlineError, flxUserNameError);
        return textBoxEntry;
    }
})