define("com/adminConsole/common/textBoxEntry/usertextBoxEntryController", function() {
    return {
        callPreshow: function() {
            this.setActions();
            this.view.flxEnterValue.skin = "sknflxEnterValueNormal";
        },
        setActions: function() {
            var scopeObj = this;
            this.view.tbxEnterValue.onBeginEditing = function() {
                if (scopeObj.view.flxEnterValue.skin !== "sknFlxBgFFFFFFBr1293cc1pxRound3px" && (scopeObj.view.flxBtnCheck.isVisible === false)) scopeObj.view.flxEnterValue.skin = "sknFlxBgFFFFFFBr1293cc1pxRound3px";
            };
            this.view.tbxEnterValue.onEndEditing = function() {
                if (scopeObj.view.flxEnterValue.skin !== "sknflxEnterValueNormal") scopeObj.view.flxEnterValue.skin = "sknflxEnterValueNormal";
            };
        }
    };
});
define("com/adminConsole/common/textBoxEntry/textBoxEntryControllerActions", {
    /*
        This is an auto generated file and any modifications to it may result in corruption of the action sequence.
    */
    AS_FlexContainer_de9be5c29c094de1902dba128b9822ca: function AS_FlexContainer_de9be5c29c094de1902dba128b9822ca(eventobject) {
        var self = this;
        this.callPreshow();
    }
});
define("com/adminConsole/common/textBoxEntry/textBoxEntryController", ["com/adminConsole/common/textBoxEntry/usertextBoxEntryController", "com/adminConsole/common/textBoxEntry/textBoxEntryControllerActions"], function() {
    var controller = require("com/adminConsole/common/textBoxEntry/usertextBoxEntryController");
    var actions = require("com/adminConsole/common/textBoxEntry/textBoxEntryControllerActions");
    for (var key in actions) {
        controller[key] = actions[key];
    }
    return controller;
});
