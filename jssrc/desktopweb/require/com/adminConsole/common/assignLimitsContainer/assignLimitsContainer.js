define(function() {
    return function(controller) {
        var assignLimitsContainer = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "clipBounds": true,
            "isMaster": true,
            "height": "100%",
            "id": "assignLimitsContainer",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "left": "0dp",
            "isModalContainer": false,
            "skin": "slFbox",
            "top": "0dp",
            "width": "100%"
        }, controller.args[0], "assignLimitsContainer"), extendConfig({}, controller.args[1], "assignLimitsContainer"), extendConfig({}, controller.args[2], "assignLimitsContainer"));
        assignLimitsContainer.setDefaultUnit(kony.flex.DP);
        var flxReset = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "bottom": "610dp",
            "clipBounds": true,
            "height": "6%",
            "id": "flxReset",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "left": "0",
            "isModalContainer": false,
            "skin": "slFbox",
            "top": "0dp",
            "width": "100%"
        }, controller.args[0], "flxReset"), extendConfig({}, controller.args[1], "flxReset"), extendConfig({}, controller.args[2], "flxReset"));
        flxReset.setDefaultUnit(kony.flex.DP);
        var flxBtnContainer = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
            "centerY": "50%",
            "clipBounds": true,
            "id": "flxBtnContainer",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "isModalContainer": false,
            "right": "0%",
            "skin": "slFbox",
            "width": "5%"
        }, controller.args[0], "flxBtnContainer"), extendConfig({}, controller.args[1], "flxBtnContainer"), extendConfig({}, controller.args[2], "flxBtnContainer"));
        flxBtnContainer.setDefaultUnit(kony.flex.DP);
        var btnOption1 = new kony.ui.Button(extendConfig({
            "bottom": "0dp",
            "id": "btnOption1",
            "isVisible": true,
            "right": "20%",
            "skin": "sknBtnLato117eb013Px",
            "i18n_text": "kony.i18n.getLocalizedString(\"i18n.Common.button.Reset\")",
            "top": "0dp",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, controller.args[0], "btnOption1"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_TOP_LEFT,
            "displayText": true,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "btnOption1"), extendConfig({
            "hoverSkin": "Btn000000font14px"
        }, controller.args[2], "btnOption1"));
        flxBtnContainer.add(btnOption1);
        flxReset.add(flxBtnContainer);
        var flxScrollAssignLimits = new kony.ui.FlexScrollContainer(extendConfig({
            "allowHorizontalBounce": false,
            "allowVerticalBounce": true,
            "bottom": "81dp",
            "bounces": true,
            "clipBounds": true,
            "enableScrolling": true,
            "horizontalScrollIndicator": true,
            "id": "flxScrollAssignLimits",
            "isVisible": true,
            "layoutType": kony.flex.FLOW_VERTICAL,
            "left": "0dp",
            "pagingEnabled": false,
            "scrollDirection": kony.flex.SCROLL_VERTICAL,
            "skin": "slFSbox",
            "top": "72dp",
            "verticalScrollIndicator": true,
            "width": kony.flex.USE_PREFFERED_SIZE
        }, controller.args[0], "flxScrollAssignLimits"), extendConfig({}, controller.args[1], "flxScrollAssignLimits"), extendConfig({}, controller.args[2], "flxScrollAssignLimits"));
        flxScrollAssignLimits.setDefaultUnit(kony.flex.DP);
        flxScrollAssignLimits.add();
        var flxSeparator = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "bottom": "80dp",
            "clipBounds": true,
            "height": "1dp",
            "id": "flxSeparator",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "left": 0,
            "isModalContainer": false,
            "skin": "sknflxd6dbe7",
            "width": "100%",
            "zIndex": 1
        }, controller.args[0], "flxSeparator"), extendConfig({}, controller.args[1], "flxSeparator"), extendConfig({}, controller.args[2], "flxSeparator"));
        flxSeparator.setDefaultUnit(kony.flex.DP);
        flxSeparator.add();
        var flxGroupDetailsButtons = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "bottom": "0dp",
            "clipBounds": true,
            "height": "80dp",
            "id": "flxGroupDetailsButtons",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "left": "0.00%",
            "isModalContainer": false,
            "skin": "slFbox",
            "width": "100%",
            "zIndex": 1
        }, controller.args[0], "flxGroupDetailsButtons"), extendConfig({}, controller.args[1], "flxGroupDetailsButtons"), extendConfig({}, controller.args[2], "flxGroupDetailsButtons"));
        flxGroupDetailsButtons.setDefaultUnit(kony.flex.DP);
        var btnCancel = new kony.ui.Button(extendConfig({
            "centerY": "50%",
            "focusSkin": "sknBtnLatoRegulara5abc413pxKA",
            "height": "40dp",
            "id": "btnCancel",
            "isVisible": true,
            "left": "20px",
            "right": "29.50%",
            "skin": "sknbtnf7f7faLatoRegular4f555dBorder1px8b96a5",
            "i18n_text": "kony.i18n.getLocalizedString(\"i18n.permission.CANCEL\")",
            "top": "0%",
            "width": "100px",
            "zIndex": 1
        }, controller.args[0], "btnCancel"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_CENTER,
            "displayText": true,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "btnCancel"), extendConfig({
            "hoverSkin": "sknbtnffffffLatoRegular4f555dBorder1px485c75"
        }, controller.args[2], "btnCancel"));
        var flxRightButton = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "centerY": "50%",
            "clipBounds": true,
            "height": "100%",
            "id": "flxRightButton",
            "isVisible": true,
            "layoutType": kony.flex.FLOW_HORIZONTAL,
            "reverseLayoutDirection": true,
            "isModalContainer": false,
            "right": "0px",
            "skin": "slFbox",
            "width": "280px",
            "zIndex": 1
        }, controller.args[0], "flxRightButton"), extendConfig({}, controller.args[1], "flxRightButton"), extendConfig({}, controller.args[2], "flxRightButton"));
        flxRightButton.setDefaultUnit(kony.flex.DP);
        var btnNext = new kony.ui.Button(extendConfig({
            "centerY": "50%",
            "focusSkin": "sknBtn003E75LatoRegular13pxFFFFFFRad20px",
            "height": "40dp",
            "id": "btnNext",
            "isVisible": true,
            "right": "20dp",
            "skin": "sknBtn003E75LatoRegular13pxFFFFFFRad20px",
            "i18n_text": "kony.i18n.getLocalizedString(\"i18n.permission.NEXT\")",
            "top": "0%",
            "width": "93dp",
            "zIndex": 1
        }, controller.args[0], "btnNext"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_CENTER,
            "displayText": true,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "btnNext"), extendConfig({
            "hoverSkin": "sknBtn005198LatoRegular13pxFFFFFFRad20px"
        }, controller.args[2], "btnNext"));
        flxRightButton.add(btnNext);
        flxGroupDetailsButtons.add(btnCancel, flxRightButton);
        assignLimitsContainer.add(flxReset, flxScrollAssignLimits, flxSeparator, flxGroupDetailsButtons);
        return assignLimitsContainer;
    }
})