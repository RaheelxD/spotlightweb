define("com/adminConsole/common/assignLimitsContainer/userassignLimitsContainerController", function() {
    return {};
});
define("com/adminConsole/common/assignLimitsContainer/assignLimitsContainerControllerActions", {
    /*
          This is an auto generated file and any modifications to it may result in corruption of the action sequence.
        */
});
define("com/adminConsole/common/assignLimitsContainer/assignLimitsContainerController", ["com/adminConsole/common/assignLimitsContainer/userassignLimitsContainerController", "com/adminConsole/common/assignLimitsContainer/assignLimitsContainerControllerActions"], function() {
    var controller = require("com/adminConsole/common/assignLimitsContainer/userassignLimitsContainerController");
    var actions = require("com/adminConsole/common/assignLimitsContainer/assignLimitsContainerControllerActions");
    for (var key in actions) {
        controller[key] = actions[key];
    }
    return controller;
});
