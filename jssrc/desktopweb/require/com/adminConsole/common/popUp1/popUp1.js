define(function() {
    return function(controller) {
        var popUp1 = new kony.ui.FlexContainer(extendConfig({
            "clipBounds": true,
            "isMaster": true,
            "height": "100%",
            "id": "popUp1",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "left": "0dp",
            "isModalContainer": false,
            "skin": "sknflxBg000000Op50",
            "top": "0dp",
            "width": "100%",
            "zIndex": 1
        }, controller.args[0], "popUp1"), extendConfig({}, controller.args[1], "popUp1"), extendConfig({}, controller.args[2], "popUp1"));
        popUp1.setDefaultUnit(kony.flex.DP);
        var flxPopUp = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
            "centerX": "50%",
            "clipBounds": false,
            "id": "flxPopUp",
            "isVisible": true,
            "layoutType": kony.flex.FLOW_VERTICAL,
            "isModalContainer": false,
            "skin": "sknFlxFFFFFF100O",
            "top": "250px",
            "width": "600px",
            "zIndex": 10
        }, controller.args[0], "flxPopUp"), extendConfig({}, controller.args[1], "flxPopUp"), extendConfig({}, controller.args[2], "flxPopUp"));
        flxPopUp.setDefaultUnit(kony.flex.DP);
        var flxPopUpTopColor = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "clipBounds": true,
            "height": "10px",
            "id": "flxPopUpTopColor",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "left": "0dp",
            "isModalContainer": false,
            "skin": "sknflxebb54cOp100",
            "top": "0dp",
            "width": "100%",
            "zIndex": 1
        }, controller.args[0], "flxPopUpTopColor"), extendConfig({}, controller.args[1], "flxPopUpTopColor"), extendConfig({}, controller.args[2], "flxPopUpTopColor"));
        flxPopUpTopColor.setDefaultUnit(kony.flex.DP);
        flxPopUpTopColor.add();
        var flxPopupHeader = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
            "clipBounds": true,
            "id": "flxPopupHeader",
            "isVisible": true,
            "layoutType": kony.flex.FLOW_VERTICAL,
            "left": "0dp",
            "isModalContainer": false,
            "skin": "sknFlxFFFFFF100O",
            "top": "0px",
            "width": "100%",
            "zIndex": 1
        }, controller.args[0], "flxPopupHeader"), extendConfig({}, controller.args[1], "flxPopupHeader"), extendConfig({}, controller.args[2], "flxPopupHeader"));
        flxPopupHeader.setDefaultUnit(kony.flex.DP);
        var flxPopUpClose = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "clipBounds": true,
            "height": "25px",
            "id": "flxPopUpClose",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "isModalContainer": false,
            "right": 15,
            "skin": "slFbox",
            "top": "15dp",
            "width": "25px",
            "zIndex": 20
        }, controller.args[0], "flxPopUpClose"), extendConfig({}, controller.args[1], "flxPopUpClose"), extendConfig({
            "hoverSkin": "sknCursor"
        }, controller.args[2], "flxPopUpClose"));
        flxPopUpClose.setDefaultUnit(kony.flex.DP);
        var lblPopupClose = new kony.ui.Label(extendConfig({
            "centerX": "50%",
            "centerY": "50%",
            "height": "20dp",
            "id": "lblPopupClose",
            "isVisible": true,
            "left": "0dp",
            "skin": "sknIcon18pxGray",
            "i18n_text": "kony.i18n.getLocalizedString(\"i18n.userwidgetmodel.lblClearSearch\")",
            "width": "20dp",
            "zIndex": 1
        }, controller.args[0], "lblPopupClose"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblPopupClose"), extendConfig({}, controller.args[2], "lblPopupClose"));
        flxPopUpClose.add(lblPopupClose);
        var lblPopUpMainMessage = new kony.ui.Label(extendConfig({
            "id": "lblPopUpMainMessage",
            "isVisible": true,
            "left": "20px",
            "skin": "sknlblLatoBold35475f23px",
            "i18n_text": "kony.i18n.getLocalizedString(\"i18n.SecurityQuestions.DeleteQuestion\")",
            "top": "0px",
            "width": "80%",
            "zIndex": 1
        }, controller.args[0], "lblPopUpMainMessage"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblPopUpMainMessage"), extendConfig({}, controller.args[2], "lblPopUpMainMessage"));
        var rtxPopUpDisclaimer = new kony.ui.RichText(extendConfig({
            "bottom": 30,
            "id": "rtxPopUpDisclaimer",
            "isVisible": true,
            "left": "20px",
            "right": 20,
            "skin": "sknrtxLato0df8337c414274d",
            "i18n_text": "kony.i18n.getLocalizedString(\"i18n.userwidgetmodel.rtxPopUpDisclaimer\")",
            "top": "30px",
            "zIndex": 1
        }, controller.args[0], "rtxPopUpDisclaimer"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "rtxPopUpDisclaimer"), extendConfig({}, controller.args[2], "rtxPopUpDisclaimer"));
        flxPopupHeader.add(flxPopUpClose, lblPopUpMainMessage, rtxPopUpDisclaimer);
        var flxDependencyViewContainer = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
            "clipBounds": true,
            "id": "flxDependencyViewContainer",
            "isVisible": false,
            "layoutType": kony.flex.FLOW_VERTICAL,
            "left": "0dp",
            "isModalContainer": false,
            "skin": "slFbox",
            "top": "0px",
            "width": "100%",
            "zIndex": 1
        }, controller.args[0], "flxDependencyViewContainer"), extendConfig({}, controller.args[1], "flxDependencyViewContainer"), extendConfig({}, controller.args[2], "flxDependencyViewContainer"));
        flxDependencyViewContainer.setDefaultUnit(kony.flex.DP);
        var lblSeparatorLine = new kony.ui.Label(extendConfig({
            "centerX": "50%",
            "height": "2dp",
            "id": "lblSeparatorLine",
            "isVisible": true,
            "left": "0dp",
            "skin": "sknlblSeperatorBgD5D9DD",
            "text": "Label",
            "top": "0dp",
            "width": "94%",
            "zIndex": 1
        }, controller.args[0], "lblSeparatorLine"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblSeparatorLine"), extendConfig({}, controller.args[2], "lblSeparatorLine"));
        var flxDependencyHeading = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "clipBounds": true,
            "height": "20dp",
            "id": "flxDependencyHeading",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "left": "0dp",
            "isModalContainer": false,
            "skin": "slFbox",
            "top": "10dp",
            "width": "100%",
            "zIndex": 20
        }, controller.args[0], "flxDependencyHeading"), extendConfig({}, controller.args[1], "flxDependencyHeading"), extendConfig({}, controller.args[2], "flxDependencyHeading"));
        flxDependencyHeading.setDefaultUnit(kony.flex.DP);
        var lblDependencyHeading = new kony.ui.Label(extendConfig({
            "centerY": "48%",
            "id": "lblDependencyHeading",
            "isVisible": true,
            "left": "20dp",
            "skin": "sknLbl192b45LatoReg16px",
            "text": "Dependencies",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, controller.args[0], "lblDependencyHeading"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblDependencyHeading"), extendConfig({}, controller.args[2], "lblDependencyHeading"));
        flxDependencyHeading.add(lblDependencyHeading);
        var flxDependencyList = new kony.ui.FlexScrollContainer(extendConfig({
            "allowHorizontalBounce": false,
            "allowVerticalBounce": true,
            "bottom": "20dp",
            "bounces": true,
            "clipBounds": true,
            "enableScrolling": true,
            "height": "250dp",
            "horizontalScrollIndicator": true,
            "id": "flxDependencyList",
            "isVisible": true,
            "layoutType": kony.flex.FLOW_VERTICAL,
            "left": "20dp",
            "pagingEnabled": false,
            "right": "20dp",
            "scrollDirection": kony.flex.SCROLL_VERTICAL,
            "skin": "sknFlxBgFFFFFFBore4e6ec3Px",
            "top": "20dp",
            "verticalScrollIndicator": true,
            "width": "94%"
        }, controller.args[0], "flxDependencyList"), extendConfig({}, controller.args[1], "flxDependencyList"), extendConfig({}, controller.args[2], "flxDependencyList"));
        flxDependencyList.setDefaultUnit(kony.flex.DP);
        var flxDependencyHeader = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "clipBounds": true,
            "height": "40dp",
            "id": "flxDependencyHeader",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "left": "0dp",
            "isModalContainer": false,
            "skin": "sknf5f6f8border0px",
            "top": "0dp",
            "width": "100%",
            "zIndex": 1
        }, controller.args[0], "flxDependencyHeader"), extendConfig({}, controller.args[1], "flxDependencyHeader"), extendConfig({}, controller.args[2], "flxDependencyHeader"));
        flxDependencyHeader.setDefaultUnit(kony.flex.DP);
        var lblDependencyHeader = new kony.ui.Label(extendConfig({
            "centerY": "50%",
            "id": "lblDependencyHeader",
            "isVisible": true,
            "left": "20dp",
            "skin": "sknLbl192b45LatoReg16px",
            "text": "Actions(4)",
            "top": "0dp",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, controller.args[0], "lblDependencyHeader"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblDependencyHeader"), extendConfig({}, controller.args[2], "lblDependencyHeader"));
        var lblDependencyHeaderSeparator = new kony.ui.Label(extendConfig({
            "height": "1px",
            "id": "lblDependencyHeaderSeparator",
            "isVisible": true,
            "left": "0dp",
            "skin": "sknlblSeperator",
            "text": ".",
            "top": "39dp",
            "width": "100%",
            "zIndex": 1
        }, controller.args[0], "lblDependencyHeaderSeparator"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_BOTTOM_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblDependencyHeaderSeparator"), extendConfig({}, controller.args[2], "lblDependencyHeaderSeparator"));
        flxDependencyHeader.add(lblDependencyHeader, lblDependencyHeaderSeparator);
        var flxDependencySegmentHeader = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "clipBounds": true,
            "height": "50dp",
            "id": "flxDependencySegmentHeader",
            "isVisible": true,
            "layoutType": kony.flex.FLOW_HORIZONTAL,
            "left": "0dp",
            "isModalContainer": false,
            "skin": "slFbox",
            "top": "0dp",
            "width": "100%"
        }, controller.args[0], "flxDependencySegmentHeader"), extendConfig({}, controller.args[1], "flxDependencySegmentHeader"), extendConfig({}, controller.args[2], "flxDependencySegmentHeader"));
        flxDependencySegmentHeader.setDefaultUnit(kony.flex.DP);
        var flxDependencyFeatureNameHeader = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "clipBounds": true,
            "height": "100%",
            "id": "flxDependencyFeatureNameHeader",
            "isVisible": true,
            "layoutType": kony.flex.FLOW_HORIZONTAL,
            "left": "5%",
            "isModalContainer": false,
            "onClick": controller.AS_FlexContainer_af76450867de4196a1e2508dc8c1eae6,
            "skin": "slFbox",
            "top": 0,
            "width": "28%",
            "zIndex": 1
        }, controller.args[0], "flxDependencyFeatureNameHeader"), extendConfig({}, controller.args[1], "flxDependencyFeatureNameHeader"), extendConfig({}, controller.args[2], "flxDependencyFeatureNameHeader"));
        flxDependencyFeatureNameHeader.setDefaultUnit(kony.flex.DP);
        var lblDependencyNameHeader = new kony.ui.Label(extendConfig({
            "centerY": "50%",
            "id": "lblDependencyNameHeader",
            "isVisible": true,
            "left": "0px",
            "skin": "sknlblLato696c7312px",
            "i18n_text": "kony.i18n.getLocalizedString(\"i18n.products.Feature_Name_CAPS\")",
            "top": 0,
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, controller.args[0], "lblDependencyNameHeader"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblDependencyNameHeader"), extendConfig({
            "hoverSkin": "sknlblLatoBold00000012px"
        }, controller.args[2], "lblDependencyNameHeader"));
        var fontIconSortDependencyFeatureName = new kony.ui.Label(extendConfig({
            "centerY": "50%",
            "id": "fontIconSortDependencyFeatureName",
            "isVisible": true,
            "left": "5dp",
            "skin": "sknIcon12pxBlack",
            "text": "",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, controller.args[0], "fontIconSortDependencyFeatureName"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "fontIconSortDependencyFeatureName"), extendConfig({
            "hoverSkin": "sknIcon12pxBlackHover"
        }, controller.args[2], "fontIconSortDependencyFeatureName"));
        flxDependencyFeatureNameHeader.add(lblDependencyNameHeader, fontIconSortDependencyFeatureName);
        var flxDependencyActionNameHeader = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "clipBounds": true,
            "height": "100%",
            "id": "flxDependencyActionNameHeader",
            "isVisible": true,
            "layoutType": kony.flex.FLOW_HORIZONTAL,
            "left": "5%",
            "isModalContainer": false,
            "onClick": controller.AS_FlexContainer_af76450867de4196a1e2508dc8c1eae6,
            "skin": "slFbox",
            "top": 0,
            "width": "28%",
            "zIndex": 1
        }, controller.args[0], "flxDependencyActionNameHeader"), extendConfig({}, controller.args[1], "flxDependencyActionNameHeader"), extendConfig({}, controller.args[2], "flxDependencyActionNameHeader"));
        flxDependencyActionNameHeader.setDefaultUnit(kony.flex.DP);
        var lblDependencyActionNameHeader = new kony.ui.Label(extendConfig({
            "centerY": "50%",
            "id": "lblDependencyActionNameHeader",
            "isVisible": true,
            "left": "0px",
            "skin": "sknlblLato696c7312px",
            "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmServiceManagement.ActionNameUC\")",
            "top": 0,
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, controller.args[0], "lblDependencyActionNameHeader"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblDependencyActionNameHeader"), extendConfig({
            "hoverSkin": "sknlblLatoBold00000012px"
        }, controller.args[2], "lblDependencyActionNameHeader"));
        var fontIconSortDependencyActionName = new kony.ui.Label(extendConfig({
            "centerY": "50%",
            "id": "fontIconSortDependencyActionName",
            "isVisible": true,
            "left": "5dp",
            "skin": "sknIcon12pxBlack",
            "text": "",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, controller.args[0], "fontIconSortDependencyActionName"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "fontIconSortDependencyActionName"), extendConfig({
            "hoverSkin": "sknIcon12pxBlackHover"
        }, controller.args[2], "fontIconSortDependencyActionName"));
        flxDependencyActionNameHeader.add(lblDependencyActionNameHeader, fontIconSortDependencyActionName);
        var flxDependencyActionCodeHeader = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "clipBounds": true,
            "height": "100%",
            "id": "flxDependencyActionCodeHeader",
            "isVisible": true,
            "layoutType": kony.flex.FLOW_HORIZONTAL,
            "left": "5%",
            "isModalContainer": false,
            "onClick": controller.AS_FlexContainer_af76450867de4196a1e2508dc8c1eae6,
            "skin": "slFbox",
            "top": 0,
            "width": "28%",
            "zIndex": 1
        }, controller.args[0], "flxDependencyActionCodeHeader"), extendConfig({}, controller.args[1], "flxDependencyActionCodeHeader"), extendConfig({}, controller.args[2], "flxDependencyActionCodeHeader"));
        flxDependencyActionCodeHeader.setDefaultUnit(kony.flex.DP);
        var lblDependencyActionCodeHeader = new kony.ui.Label(extendConfig({
            "centerY": "50%",
            "id": "lblDependencyActionCodeHeader",
            "isVisible": true,
            "left": "0px",
            "skin": "sknlblLato696c7312px",
            "text": "ACTION CODE",
            "top": 0,
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, controller.args[0], "lblDependencyActionCodeHeader"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblDependencyActionCodeHeader"), extendConfig({
            "hoverSkin": "sknlblLatoBold00000012px"
        }, controller.args[2], "lblDependencyActionCodeHeader"));
        var fontIconSortDependencyActionCode = new kony.ui.Label(extendConfig({
            "centerY": "50%",
            "id": "fontIconSortDependencyActionCode",
            "isVisible": true,
            "left": "5dp",
            "skin": "sknIcon12pxBlack",
            "text": "",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, controller.args[0], "fontIconSortDependencyActionCode"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "fontIconSortDependencyActionCode"), extendConfig({
            "hoverSkin": "sknIcon12pxBlackHover"
        }, controller.args[2], "fontIconSortDependencyActionCode"));
        flxDependencyActionCodeHeader.add(lblDependencyActionCodeHeader, fontIconSortDependencyActionCode);
        flxDependencySegmentHeader.add(flxDependencyFeatureNameHeader, flxDependencyActionNameHeader, flxDependencyActionCodeHeader);
        var lblSeperatorLine = new kony.ui.Label(extendConfig({
            "centerX": "50%",
            "height": "1dp",
            "id": "lblSeperatorLine",
            "isVisible": true,
            "left": "0dp",
            "skin": "sknlblSeperatorBgD5D9DD",
            "text": "Label",
            "top": "0dp",
            "width": "94%",
            "zIndex": 1
        }, controller.args[0], "lblSeperatorLine"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblSeperatorLine"), extendConfig({}, controller.args[2], "lblSeperatorLine"));
        var flxDependencySegment = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
            "clipBounds": true,
            "id": "flxDependencySegment",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "left": "0dp",
            "isModalContainer": false,
            "skin": "slFbox",
            "top": "0dp",
            "width": "100%",
            "zIndex": 1
        }, controller.args[0], "flxDependencySegment"), extendConfig({}, controller.args[1], "flxDependencySegment"), extendConfig({}, controller.args[2], "flxDependencySegment"));
        flxDependencySegment.setDefaultUnit(kony.flex.DP);
        var segDependencyList = new kony.ui.SegmentedUI2(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
            "data": [{
                "lblActionCode": "Label",
                "lblActionName": "Label",
                "lblFeatureName": "Label",
                "lblSeperatorLine": "Label"
            }, {
                "lblActionCode": "Label",
                "lblActionName": "Label",
                "lblFeatureName": "Label",
                "lblSeperatorLine": "Label"
            }],
            "groupCells": false,
            "id": "segDependencyList",
            "isVisible": true,
            "left": "0dp",
            "needPageIndicator": true,
            "pageOffDotImage": "pageoffdot.png",
            "pageOnDotImage": "pageondot.png",
            "retainSelection": false,
            "rowFocusSkin": "seg2Focus",
            "rowSkin": "seg2Normal",
            "rowTemplate": "flxDependency",
            "sectionHeaderSkin": "sliPhoneSegmentHeader",
            "selectionBehavior": constants.SEGUI_DEFAULT_BEHAVIOR,
            "separatorRequired": false,
            "showScrollbars": false,
            "top": "0dp",
            "viewType": constants.SEGUI_VIEW_TYPE_TABLEVIEW,
            "widgetDataMap": {
                "flxDependency": "flxDependency",
                "flxDependencyDetails": "flxDependencyDetails",
                "lblActionCode": "lblActionCode",
                "lblActionName": "lblActionName",
                "lblFeatureName": "lblFeatureName",
                "lblSeperatorLine": "lblSeperatorLine"
            },
            "width": "100%",
            "zIndex": 1
        }, controller.args[0], "segDependencyList"), extendConfig({
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "segDependencyList"), extendConfig({}, controller.args[2], "segDependencyList"));
        var lblDependencyListNoResultsFound = new kony.ui.Label(extendConfig({
            "centerX": "50%",
            "height": "150dp",
            "id": "lblDependencyListNoResultsFound",
            "isVisible": false,
            "skin": "sknLbl485c75FontSize12px",
            "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmUsersController.No_results_found\")",
            "top": "0dp",
            "width": "500dp",
            "zIndex": 5
        }, controller.args[0], "lblDependencyListNoResultsFound"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_CENTER,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblDependencyListNoResultsFound"), extendConfig({}, controller.args[2], "lblDependencyListNoResultsFound"));
        flxDependencySegment.add(segDependencyList, lblDependencyListNoResultsFound);
        flxDependencyList.add(flxDependencyHeader, flxDependencySegmentHeader, lblSeperatorLine, flxDependencySegment);
        flxDependencyViewContainer.add(lblSeparatorLine, flxDependencyHeading, flxDependencyList);
        var flxPopUpButtons = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "clipBounds": true,
            "height": "80px",
            "id": "flxPopUpButtons",
            "isVisible": true,
            "layoutType": kony.flex.FLOW_HORIZONTAL,
            "reverseLayoutDirection": true,
            "left": "0%",
            "isModalContainer": false,
            "skin": "slFbox0dc900c4572aa40",
            "top": "0px",
            "width": "100%",
            "zIndex": 1
        }, controller.args[0], "flxPopUpButtons"), extendConfig({}, controller.args[1], "flxPopUpButtons"), extendConfig({}, controller.args[2], "flxPopUpButtons"));
        flxPopUpButtons.setDefaultUnit(kony.flex.DP);
        var btnPopUpDelete = new kony.ui.Button(extendConfig({
            "centerY": "50%",
            "focusSkin": "sknBtn003E75LatoRegular13pxFFFFFFRad20px",
            "height": "40px",
            "id": "btnPopUpDelete",
            "isVisible": true,
            "minWidth": "80px",
            "right": "20px",
            "skin": "sknBtn003E75LatoRegular13pxFFFFFFRad20px",
            "i18n_text": "kony.i18n.getLocalizedString(\"i18n.SecurityQuestions.OK\")",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 20
        }, controller.args[0], "btnPopUpDelete"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_CENTER,
            "displayText": true,
            "padding": [5, 0, 5, 0],
            "paddingInPixel": false
        }, controller.args[1], "btnPopUpDelete"), extendConfig({
            "hoverSkin": "sknBtn005198LatoRegular13pxFFFFFFRad20px"
        }, controller.args[2], "btnPopUpDelete"));
        var btnPopUpCancel = new kony.ui.Button(extendConfig({
            "centerY": "50%",
            "focusSkin": "sknbtnf7f7faLatoRegular4f555dBorder1px8b96a5",
            "height": "40px",
            "id": "btnPopUpCancel",
            "isVisible": true,
            "right": "20px",
            "skin": "sknbtnf7f7faLatoRegular4f555dBorder1px8b96a5",
            "i18n_text": "kony.i18n.getLocalizedString(\"i18n.permission.CANCEL\")",
            "top": "0%",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 20
        }, controller.args[0], "btnPopUpCancel"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_CENTER,
            "displayText": true,
            "padding": [5, 0, 5, 0],
            "paddingInPixel": false
        }, controller.args[1], "btnPopUpCancel"), extendConfig({
            "hoverSkin": "sknbtnffffffLatoRegular4f555dBorder1px485c75"
        }, controller.args[2], "btnPopUpCancel"));
        flxPopUpButtons.add(btnPopUpDelete, btnPopUpCancel);
        flxPopUp.add(flxPopUpTopColor, flxPopupHeader, flxDependencyViewContainer, flxPopUpButtons);
        popUp1.add(flxPopUp);
        return popUp1;
    }
})