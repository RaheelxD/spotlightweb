define("com/adminConsole/common/productHeader/userproductHeaderController", function() {
    return {};
});
define("com/adminConsole/common/productHeader/productHeaderControllerActions", {
    /*
          This is an auto generated file and any modifications to it may result in corruption of the action sequence.
        */
});
define("com/adminConsole/common/productHeader/productHeaderController", ["com/adminConsole/common/productHeader/userproductHeaderController", "com/adminConsole/common/productHeader/productHeaderControllerActions"], function() {
    var controller = require("com/adminConsole/common/productHeader/userproductHeaderController");
    var actions = require("com/adminConsole/common/productHeader/productHeaderControllerActions");
    for (var key in actions) {
        controller[key] = actions[key];
    }
    return controller;
});
