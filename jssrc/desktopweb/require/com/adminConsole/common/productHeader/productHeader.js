define(function() {
    return function(controller) {
        var productHeader = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "clipBounds": true,
            "isMaster": true,
            "height": "50px",
            "id": "productHeader",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "isModalContainer": false,
            "skin": "slFbox",
            "width": "100%"
        }, controller.args[0], "productHeader"), extendConfig({}, controller.args[1], "productHeader"), extendConfig({}, controller.args[2], "productHeader"));
        productHeader.setDefaultUnit(kony.flex.DP);
        var flxAccountDetailsColumn1 = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "clipBounds": true,
            "height": "100%",
            "id": "flxAccountDetailsColumn1",
            "isVisible": true,
            "layoutType": kony.flex.FLOW_HORIZONTAL,
            "left": "0dp",
            "isModalContainer": false,
            "skin": "slFbox",
            "top": "0dp",
            "width": "50%"
        }, controller.args[0], "flxAccountDetailsColumn1"), extendConfig({}, controller.args[1], "flxAccountDetailsColumn1"), extendConfig({}, controller.args[2], "flxAccountDetailsColumn1"));
        flxAccountDetailsColumn1.setDefaultUnit(kony.flex.DP);
        var lblProductCardName = new kony.ui.Label(extendConfig({
            "centerY": "50%",
            "id": "lblProductCardName",
            "isVisible": true,
            "left": "20dp",
            "skin": "sknlblLatoRegular485c7518Px",
            "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmCustomerManagement.lblProductName\")",
            "top": "0dp",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, controller.args[0], "lblProductCardName"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblProductCardName"), extendConfig({}, controller.args[2], "lblProductCardName"));
        var fontIconCardStatus = new kony.ui.Label(extendConfig({
            "centerY": "50%",
            "id": "fontIconCardStatus",
            "isVisible": true,
            "left": "10px",
            "skin": "sknFontIconActivate",
            "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmAlertsManagement.lblViewAlertStatusKey\")",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, controller.args[0], "fontIconCardStatus"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "fontIconCardStatus"), extendConfig({}, controller.args[2], "fontIconCardStatus"));
        var lblCardStatus = new kony.ui.Label(extendConfig({
            "centerY": "50%",
            "id": "lblCardStatus",
            "isVisible": true,
            "left": "5px",
            "skin": "sknlblLato5bc06cBold14px",
            "i18n_text": "kony.i18n.getLocalizedString(\"i18n.secureimage.Active\")",
            "top": "0dp",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, controller.args[0], "lblCardStatus"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblCardStatus"), extendConfig({}, controller.args[2], "lblCardStatus"));
        flxAccountDetailsColumn1.add(lblProductCardName, fontIconCardStatus, lblCardStatus);
        var flxAccountDetailsColumn2 = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "clipBounds": true,
            "height": "100%",
            "id": "flxAccountDetailsColumn2",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "reverseLayoutDirection": false,
            "isModalContainer": false,
            "right": "20px",
            "skin": "slFbox",
            "top": "0dp",
            "width": "60%"
        }, controller.args[0], "flxAccountDetailsColumn2"), extendConfig({}, controller.args[1], "flxAccountDetailsColumn2"), extendConfig({}, controller.args[2], "flxAccountDetailsColumn2"));
        flxAccountDetailsColumn2.setDefaultUnit(kony.flex.DP);
        var lblIBAN = new kony.ui.Label(extendConfig({
            "centerY": "50%",
            "id": "lblIBAN",
            "isVisible": true,
            "right": "0px",
            "skin": "sknlblLatoBold35475f14px",
            "text": "9897765676545678",
            "width": "140px",
            "zIndex": 1
        }, controller.args[0], "lblIBAN"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_BOTTOM_RIGHT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblIBAN"), extendConfig({}, controller.args[2], "lblIBAN"));
        var lblIBANLabel = new kony.ui.Label(extendConfig({
            "centerY": "50%",
            "id": "lblIBANLabel",
            "isVisible": true,
            "right": "170px",
            "skin": "sknlblLato5d6c7f12px",
            "text": "IBAN:",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, controller.args[0], "lblIBANLabel"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_RIGHT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblIBANLabel"), extendConfig({}, controller.args[2], "lblIBANLabel"));
        var blAccountNumber = new kony.ui.Label(extendConfig({
            "centerY": "50%",
            "id": "blAccountNumber",
            "isVisible": true,
            "right": "0px",
            "skin": "sknlblLatoBold35475f14px",
            "text": "9897765676545678",
            "width": "140px",
            "zIndex": 1
        }, controller.args[0], "blAccountNumber"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_BOTTOM_RIGHT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "blAccountNumber"), extendConfig({}, controller.args[2], "blAccountNumber"));
        var lblAccountNumberLabel = new kony.ui.Label(extendConfig({
            "centerY": "50%",
            "id": "lblAccountNumberLabel",
            "isVisible": true,
            "right": "170px",
            "skin": "sknlblLato5d6c7f12px",
            "text": "Account Number:",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, controller.args[0], "lblAccountNumberLabel"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_RIGHT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblAccountNumberLabel"), extendConfig({}, controller.args[2], "lblAccountNumberLabel"));
        flxAccountDetailsColumn2.add(lblIBAN, lblIBANLabel, blAccountNumber, lblAccountNumberLabel);
        var lblSeperator = new kony.ui.Label(extendConfig({
            "bottom": "0px",
            "height": "1px",
            "id": "lblSeperator",
            "isVisible": true,
            "left": "0dp",
            "skin": "sknlblSeperator",
            "width": "100%",
            "zIndex": 1
        }, controller.args[0], "lblSeperator"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblSeperator"), extendConfig({}, controller.args[2], "lblSeperator"));
        productHeader.add(flxAccountDetailsColumn1, flxAccountDetailsColumn2, lblSeperator);
        return productHeader;
    }
})