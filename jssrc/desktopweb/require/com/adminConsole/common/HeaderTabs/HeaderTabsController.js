define("com/adminConsole/common/HeaderTabs/userHeaderTabsController", function() {
    return {};
});
define("com/adminConsole/common/HeaderTabs/HeaderTabsControllerActions", {
    /*
          This is an auto generated file and any modifications to it may result in corruption of the action sequence.
        */
});
define("com/adminConsole/common/HeaderTabs/HeaderTabsController", ["com/adminConsole/common/HeaderTabs/userHeaderTabsController", "com/adminConsole/common/HeaderTabs/HeaderTabsControllerActions"], function() {
    var controller = require("com/adminConsole/common/HeaderTabs/userHeaderTabsController");
    var actions = require("com/adminConsole/common/HeaderTabs/HeaderTabsControllerActions");
    for (var key in actions) {
        controller[key] = actions[key];
    }
    return controller;
});
