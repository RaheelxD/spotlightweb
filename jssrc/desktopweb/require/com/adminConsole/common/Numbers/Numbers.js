define(function() {
    return function(controller) {
        var Numbers = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "clipBounds": true,
            "isMaster": true,
            "height": "40px",
            "id": "Numbers",
            "isVisible": true,
            "layoutType": kony.flex.FLOW_HORIZONTAL,
            "left": "0dp",
            "isModalContainer": false,
            "skin": "sknflxf3f6f8Radius3PX",
            "top": "0dp",
            "width": "224px"
        }, controller.args[0], "Numbers"), extendConfig({}, controller.args[1], "Numbers"), extendConfig({}, controller.args[2], "Numbers"));
        Numbers.setDefaultUnit(kony.flex.DP);
        var flxLbl0 = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "centerY": "50%",
            "clipBounds": true,
            "height": "19px",
            "id": "flxLbl0",
            "isVisible": true,
            "layoutType": kony.flex.FLOW_HORIZONTAL,
            "left": "20dp",
            "isModalContainer": false,
            "onClick": controller.AS_FlexContainer_d1bb929b869f4a648d93176ae2210072,
            "skin": "slFbox",
            "top": "0dp",
            "width": "18px"
        }, controller.args[0], "flxLbl0"), extendConfig({}, controller.args[1], "flxLbl0"), extendConfig({
            "hoverSkin": "sknCursor"
        }, controller.args[2], "flxLbl0"));
        flxLbl0.setDefaultUnit(kony.flex.DP);
        var lbl0 = new kony.ui.Label(extendConfig({
            "centerX": "45%",
            "centerY": "50%",
            "id": "lbl0",
            "isVisible": true,
            "skin": "sknLatoRegular485c7514px",
            "text": "0",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, controller.args[0], "lbl0"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lbl0"), extendConfig({}, controller.args[2], "lbl0"));
        flxLbl0.add(lbl0);
        var flxLbl1 = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "centerY": "50%",
            "clipBounds": true,
            "height": "19px",
            "id": "flxLbl1",
            "isVisible": true,
            "layoutType": kony.flex.FLOW_HORIZONTAL,
            "left": "15dp",
            "isModalContainer": false,
            "onClick": controller.AS_FlexContainer_ffa4cfda051047e5a9dd13707f73f958,
            "skin": "sknFlxBg003E75Op100Rad3PX",
            "top": "0dp",
            "width": "18px"
        }, controller.args[0], "flxLbl1"), extendConfig({}, controller.args[1], "flxLbl1"), extendConfig({
            "hoverSkin": "sknCursor"
        }, controller.args[2], "flxLbl1"));
        flxLbl1.setDefaultUnit(kony.flex.DP);
        var lbl1 = new kony.ui.Label(extendConfig({
            "centerX": "45%",
            "centerY": "50%",
            "id": "lbl1",
            "isVisible": true,
            "skin": "sknLblffffff13px",
            "text": "1",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, controller.args[0], "lbl1"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lbl1"), extendConfig({}, controller.args[2], "lbl1"));
        flxLbl1.add(lbl1);
        var flxLbl2 = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "centerY": "50%",
            "clipBounds": true,
            "height": "19px",
            "id": "flxLbl2",
            "isVisible": true,
            "layoutType": kony.flex.FLOW_HORIZONTAL,
            "left": "15dp",
            "isModalContainer": false,
            "onClick": controller.AS_FlexContainer_ga3e88e710cb4e3ebaf98186349569bc,
            "skin": "slFbox",
            "top": "0dp",
            "width": "18px"
        }, controller.args[0], "flxLbl2"), extendConfig({}, controller.args[1], "flxLbl2"), extendConfig({
            "hoverSkin": "sknCursor"
        }, controller.args[2], "flxLbl2"));
        flxLbl2.setDefaultUnit(kony.flex.DP);
        var lbl2 = new kony.ui.Label(extendConfig({
            "centerX": "45%",
            "centerY": "50%",
            "id": "lbl2",
            "isVisible": true,
            "skin": "sknLatoRegular485c7514px",
            "text": "2",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, controller.args[0], "lbl2"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lbl2"), extendConfig({}, controller.args[2], "lbl2"));
        flxLbl2.add(lbl2);
        var flxLbl3 = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "centerY": "50%",
            "clipBounds": true,
            "height": "19px",
            "id": "flxLbl3",
            "isVisible": true,
            "layoutType": kony.flex.FLOW_HORIZONTAL,
            "left": "15dp",
            "isModalContainer": false,
            "onClick": controller.AS_FlexContainer_c48c03ee574b4e5a8e933e010a9fdafa,
            "skin": "slFbox",
            "top": "0dp",
            "width": "18px"
        }, controller.args[0], "flxLbl3"), extendConfig({}, controller.args[1], "flxLbl3"), extendConfig({
            "hoverSkin": "sknCursor"
        }, controller.args[2], "flxLbl3"));
        flxLbl3.setDefaultUnit(kony.flex.DP);
        var lbl3 = new kony.ui.Label(extendConfig({
            "centerX": "45%",
            "centerY": "50%",
            "id": "lbl3",
            "isVisible": true,
            "skin": "sknLatoRegular485c7514px",
            "text": "3",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, controller.args[0], "lbl3"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lbl3"), extendConfig({}, controller.args[2], "lbl3"));
        flxLbl3.add(lbl3);
        var flxLbl4 = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "centerY": "50%",
            "clipBounds": true,
            "height": "19px",
            "id": "flxLbl4",
            "isVisible": true,
            "layoutType": kony.flex.FLOW_HORIZONTAL,
            "left": "15dp",
            "isModalContainer": false,
            "onClick": controller.AS_FlexContainer_d8e34dc14ed74c8a901427411dd46daf,
            "skin": "slFbox",
            "top": "0dp",
            "width": "18px"
        }, controller.args[0], "flxLbl4"), extendConfig({}, controller.args[1], "flxLbl4"), extendConfig({
            "hoverSkin": "sknCursor"
        }, controller.args[2], "flxLbl4"));
        flxLbl4.setDefaultUnit(kony.flex.DP);
        var lbl4 = new kony.ui.Label(extendConfig({
            "centerX": "45%",
            "centerY": "50%",
            "id": "lbl4",
            "isVisible": true,
            "skin": "sknLatoRegular485c7514px",
            "text": "4",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, controller.args[0], "lbl4"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lbl4"), extendConfig({}, controller.args[2], "lbl4"));
        flxLbl4.add(lbl4);
        var flxLbl5 = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "centerY": "50%",
            "clipBounds": true,
            "height": "19px",
            "id": "flxLbl5",
            "isVisible": true,
            "layoutType": kony.flex.FLOW_HORIZONTAL,
            "left": "15dp",
            "isModalContainer": false,
            "onClick": controller.AS_FlexContainer_i374785a23c140e3b491464e58392fca,
            "skin": "slFbox",
            "top": "0dp",
            "width": "18px"
        }, controller.args[0], "flxLbl5"), extendConfig({}, controller.args[1], "flxLbl5"), extendConfig({
            "hoverSkin": "sknCursor"
        }, controller.args[2], "flxLbl5"));
        flxLbl5.setDefaultUnit(kony.flex.DP);
        var lbl5 = new kony.ui.Label(extendConfig({
            "centerX": "45%",
            "centerY": "50%",
            "id": "lbl5",
            "isVisible": true,
            "skin": "sknLatoRegular485c7514px",
            "text": "5",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, controller.args[0], "lbl5"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lbl5"), extendConfig({}, controller.args[2], "lbl5"));
        flxLbl5.add(lbl5);
        Numbers.add(flxLbl0, flxLbl1, flxLbl2, flxLbl3, flxLbl4, flxLbl5);
        return Numbers;
    }
})