define(function() {
    return function(controller) {
        var loadingIndicator = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "clipBounds": true,
            "isMaster": true,
            "height": "100%",
            "id": "loadingIndicator",
            "isVisible": true,
            "layoutType": kony.flex.FLOW_HORIZONTAL,
            "left": "0dp",
            "isModalContainer": false,
            "right": "0dp",
            "skin": "sknLoadingBlur",
            "top": "0dp",
            "width": "100%"
        }, controller.args[0], "loadingIndicator"), extendConfig({}, controller.args[1], "loadingIndicator"), extendConfig({}, controller.args[2], "loadingIndicator"));
        loadingIndicator.setDefaultUnit(kony.flex.DP);
        var flxImageContainer = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "centerX": "50%",
            "centerY": "50%",
            "clipBounds": true,
            "height": "75px",
            "id": "flxImageContainer",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "isModalContainer": false,
            "skin": "slFbox",
            "width": "75px",
            "zIndex": 1
        }, controller.args[0], "flxImageContainer"), extendConfig({}, controller.args[1], "flxImageContainer"), extendConfig({}, controller.args[2], "flxImageContainer"));
        flxImageContainer.setDefaultUnit(kony.flex.DP);
        var imgLoading = new kony.ui.Image2(extendConfig({
            "centerX": "50%",
            "centerY": "50%",
            "height": "50px",
            "id": "imgLoading",
            "isVisible": true,
            "skin": "slImage",
            "src": "loadingscreenimage.gif",
            "width": "50px",
            "zIndex": 1
        }, controller.args[0], "imgLoading"), extendConfig({
            "imageScaleMode": constants.IMAGE_SCALE_MODE_MAINTAIN_ASPECT_RATIO,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "imgLoading"), extendConfig({}, controller.args[2], "imgLoading"));
        flxImageContainer.add(imgLoading);
        loadingIndicator.add(flxImageContainer);
        return loadingIndicator;
    }
})