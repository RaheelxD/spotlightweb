define("com/adminConsole/common/loadingIndicator/userloadingIndicatorController", function() {
    return {};
});
define("com/adminConsole/common/loadingIndicator/loadingIndicatorControllerActions", {
    /*
          This is an auto generated file and any modifications to it may result in corruption of the action sequence.
        */
});
define("com/adminConsole/common/loadingIndicator/loadingIndicatorController", ["com/adminConsole/common/loadingIndicator/userloadingIndicatorController", "com/adminConsole/common/loadingIndicator/loadingIndicatorControllerActions"], function() {
    var controller = require("com/adminConsole/common/loadingIndicator/userloadingIndicatorController");
    var actions = require("com/adminConsole/common/loadingIndicator/loadingIndicatorControllerActions");
    for (var key in actions) {
        controller[key] = actions[key];
    }
    return controller;
});
