define("com/adminConsole/common/searchBox/usersearchBoxController", function() {
    return {
        /*
         * focus search box
         * @param: opt - true/false
         */
        setSearchBoxFocus: function(opt) {
            if (opt === true) {
                this.view.flxSearchContainer.skin = "sknflxBgffffffBorder1293ccRadius30Px";
            } else if (opt === false) {
                this.view.flxSearchContainer.skin = "sknflxd5d9ddop100";
            }
        },
        /*
         * clear search text
         */
        clearSearchBox: function() {
            this.setSearchBoxFocus(false);
            this.view.flxSearchCancel.setVisibility(false);
            this.view.tbxSearchBox.text = "";
        },
    };
});
define("com/adminConsole/common/searchBox/searchBoxControllerActions", {
    /*
          This is an auto generated file and any modifications to it may result in corruption of the action sequence.
        */
});
define("com/adminConsole/common/searchBox/searchBoxController", ["com/adminConsole/common/searchBox/usersearchBoxController", "com/adminConsole/common/searchBox/searchBoxControllerActions"], function() {
    var controller = require("com/adminConsole/common/searchBox/usersearchBoxController");
    var actions = require("com/adminConsole/common/searchBox/searchBoxControllerActions");
    for (var key in actions) {
        controller[key] = actions[key];
    }
    return controller;
});
