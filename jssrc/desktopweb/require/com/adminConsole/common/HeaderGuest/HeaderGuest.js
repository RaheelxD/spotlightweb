define(function() {
    return function(controller) {
        var HeaderGuest = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "clipBounds": true,
            "isMaster": true,
            "height": "70dp",
            "id": "HeaderGuest",
            "isVisible": true,
            "layoutType": kony.flex.FLOW_HORIZONTAL,
            "left": "0dp",
            "isModalContainer": false,
            "skin": "slFbox",
            "top": "0dp",
            "width": "100%"
        }, controller.args[0], "HeaderGuest"), extendConfig({}, controller.args[1], "HeaderGuest"), extendConfig({}, controller.args[2], "HeaderGuest"));
        HeaderGuest.setDefaultUnit(kony.flex.DP);
        var lblUserName = new kony.ui.Label(extendConfig({
            "centerY": "50%",
            "id": "lblUserName",
            "isVisible": true,
            "left": "0dp",
            "skin": "sknlblLatoRegular485c7518Px",
            "text": "Customer Name",
            "top": "15dp",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, controller.args[0], "lblUserName"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_CENTER,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblUserName"), extendConfig({}, controller.args[2], "lblUserName"));
        var flxUserType = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "centerY": "50%",
            "clipBounds": true,
            "height": "25dp",
            "id": "flxUserType",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "left": "10dp",
            "isModalContainer": false,
            "skin": "sknFlxf5f8f8Bore4e6ec3px",
            "width": "120dp",
            "zIndex": 1
        }, controller.args[0], "flxUserType"), extendConfig({}, controller.args[1], "flxUserType"), extendConfig({}, controller.args[2], "flxUserType"));
        flxUserType.setDefaultUnit(kony.flex.DP);
        var lblUserType = new kony.ui.Label(extendConfig({
            "centerX": "50%",
            "centerY": "50%",
            "id": "lblUserType",
            "isVisible": true,
            "skin": "sknlblLatoReg485c7514px",
            "text": "New Customer",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, controller.args[0], "lblUserType"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_CENTER,
            "padding": [0, 0, 0, 2],
            "paddingInPixel": false
        }, controller.args[1], "lblUserType"), extendConfig({}, controller.args[2], "lblUserType"));
        flxUserType.add(lblUserType);
        HeaderGuest.add(lblUserName, flxUserType);
        return HeaderGuest;
    }
})