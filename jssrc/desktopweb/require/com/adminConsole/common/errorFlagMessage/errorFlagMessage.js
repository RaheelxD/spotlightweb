define(function() {
    return function(controller) {
        var errorFlagMessage = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
            "clipBounds": true,
            "isMaster": true,
            "id": "errorFlagMessage",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "left": "0dp",
            "isModalContainer": false,
            "skin": "slFbox",
            "top": "0dp",
            "width": "100%"
        }, controller.args[0], "errorFlagMessage"), extendConfig({}, controller.args[1], "errorFlagMessage"), extendConfig({}, controller.args[2], "errorFlagMessage"));
        errorFlagMessage.setDefaultUnit(kony.flex.DP);
        var flxErrorContainer = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "clipBounds": true,
            "height": "40dp",
            "id": "flxErrorContainer",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "left": "0dp",
            "isModalContainer": false,
            "right": "0dp",
            "skin": "sknFlxBorder1pxe61919",
            "top": "0dp",
            "zIndex": 1
        }, controller.args[0], "flxErrorContainer"), extendConfig({}, controller.args[1], "flxErrorContainer"), extendConfig({}, controller.args[2], "flxErrorContainer"));
        flxErrorContainer.setDefaultUnit(kony.flex.DP);
        var flxErrorIconContainer = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "clipBounds": true,
            "height": "41dp",
            "id": "flxErrorIconContainer",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "left": "0dp",
            "isModalContainer": false,
            "skin": "sknFlxBge61919",
            "top": "-1dp",
            "width": "40dp",
            "zIndex": 1
        }, controller.args[0], "flxErrorIconContainer"), extendConfig({}, controller.args[1], "flxErrorIconContainer"), extendConfig({}, controller.args[2], "flxErrorIconContainer"));
        flxErrorIconContainer.setDefaultUnit(kony.flex.DP);
        var lblAccountErrorIcon = new kony.ui.Label(extendConfig({
            "centerX": "50%",
            "centerY": "50%",
            "height": "11px",
            "id": "lblAccountErrorIcon",
            "isVisible": true,
            "skin": "sknErrorWhiteIcon",
            "text": "",
            "width": "15px",
            "zIndex": 1
        }, controller.args[0], "lblAccountErrorIcon"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblAccountErrorIcon"), extendConfig({}, controller.args[2], "lblAccountErrorIcon"));
        flxErrorIconContainer.add(lblAccountErrorIcon);
        var lblErrorValue = new kony.ui.Label(extendConfig({
            "centerY": "50%",
            "id": "lblErrorValue",
            "isVisible": true,
            "left": "50dp",
            "skin": "sknLbl485C75LatoRegular13Px",
            "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmCompanies.Select_atleast_one_account_to_proceed\")",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, controller.args[0], "lblErrorValue"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblErrorValue"), extendConfig({}, controller.args[2], "lblErrorValue"));
        flxErrorContainer.add(flxErrorIconContainer, lblErrorValue);
        errorFlagMessage.add(flxErrorContainer);
        return errorFlagMessage;
    }
})