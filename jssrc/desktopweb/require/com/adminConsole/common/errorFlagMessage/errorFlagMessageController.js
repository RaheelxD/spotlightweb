define("com/adminConsole/common/errorFlagMessage/usererrorFlagMessageController", function() {
    return {
        /*
         * set error message skin
         */
        setErrorSkin: function() {
            this.view.flxErrorContainer.skin = "sknFlxBorder1pxe61919";
            this.view.flxErrorIconContainer.skin = "sknFlxBge61919";
            this.view.lblAccountErrorIcon.text = "";
        },
        /*
         * set info message skin
         */
        setInfoSkin: function() {
            this.view.flxErrorContainer.skin = "sknFlxBor4A77A0Rd2px";
            this.view.flxErrorIconContainer.skin = "sknFlxBg4A77A0";
            this.view.lblAccountErrorIcon.text = "\ue94d";
        }
    };
});
define("com/adminConsole/common/errorFlagMessage/errorFlagMessageControllerActions", {
    /*
          This is an auto generated file and any modifications to it may result in corruption of the action sequence.
        */
});
define("com/adminConsole/common/errorFlagMessage/errorFlagMessageController", ["com/adminConsole/common/errorFlagMessage/usererrorFlagMessageController", "com/adminConsole/common/errorFlagMessage/errorFlagMessageControllerActions"], function() {
    var controller = require("com/adminConsole/common/errorFlagMessage/usererrorFlagMessageController");
    var actions = require("com/adminConsole/common/errorFlagMessage/errorFlagMessageControllerActions");
    for (var key in actions) {
        controller[key] = actions[key];
    }
    return controller;
});
