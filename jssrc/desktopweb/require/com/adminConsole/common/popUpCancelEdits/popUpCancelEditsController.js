define("com/adminConsole/common/popUpCancelEdits/userpopUpCancelEditsController", function() {
    return {
        setPopUpActions: function(navigate, cancel) {
            this.view.btnPopUpCancel.onClick = cancel;
            this.view.flxPopUpClose.onClick = cancel;
            this.view.btnPopUpDelete.onClick = navigate;
        },
        clearPopUpActions: function() {
            var noAction = function() {
                kony.print('No Action Assigned this Event.');
            };
            this.view.btnPopUpCancel.onClick = noAction;
            this.view.flxPopUpClose.onClick = noAction;
            this.view.btnPopUpDelete.onClick = noAction;
        }
    };
});
define("com/adminConsole/common/popUpCancelEdits/popUpCancelEditsControllerActions", {
    /*
          This is an auto generated file and any modifications to it may result in corruption of the action sequence.
        */
});
define("com/adminConsole/common/popUpCancelEdits/popUpCancelEditsController", ["com/adminConsole/common/popUpCancelEdits/userpopUpCancelEditsController", "com/adminConsole/common/popUpCancelEdits/popUpCancelEditsControllerActions"], function() {
    var controller = require("com/adminConsole/common/popUpCancelEdits/userpopUpCancelEditsController");
    var actions = require("com/adminConsole/common/popUpCancelEdits/popUpCancelEditsControllerActions");
    for (var key in actions) {
        controller[key] = actions[key];
    }
    return controller;
});
