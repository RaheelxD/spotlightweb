define("com/adminConsole/common/leadContextualMenu/userleadContextualMenuController", function() {
    return {};
});
define("com/adminConsole/common/leadContextualMenu/leadContextualMenuControllerActions", {
    /*
          This is an auto generated file and any modifications to it may result in corruption of the action sequence.
        */
});
define("com/adminConsole/common/leadContextualMenu/leadContextualMenuController", ["com/adminConsole/common/leadContextualMenu/userleadContextualMenuController", "com/adminConsole/common/leadContextualMenu/leadContextualMenuControllerActions"], function() {
    var controller = require("com/adminConsole/common/leadContextualMenu/userleadContextualMenuController");
    var actions = require("com/adminConsole/common/leadContextualMenu/leadContextualMenuControllerActions");
    for (var key in actions) {
        controller[key] = actions[key];
    }
    return controller;
});
