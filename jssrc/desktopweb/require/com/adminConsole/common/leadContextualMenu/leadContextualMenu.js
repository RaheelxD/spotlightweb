define(function() {
    return function(controller) {
        var leadContextualMenu = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
            "clipBounds": true,
            "isMaster": true,
            "id": "leadContextualMenu",
            "isVisible": true,
            "layoutType": kony.flex.FLOW_VERTICAL,
            "left": "0px",
            "isModalContainer": false,
            "skin": "sknflxffffffop100dbdbe6Radius3px",
            "top": "0px",
            "width": "100%"
        }, controller.args[0], "leadContextualMenu"), extendConfig({}, controller.args[1], "leadContextualMenu"), extendConfig({}, controller.args[2], "leadContextualMenu"));
        leadContextualMenu.setDefaultUnit(kony.flex.DP);
        var flxEdit = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "clipBounds": true,
            "height": "35px",
            "id": "flxEdit",
            "isVisible": true,
            "layoutType": kony.flex.FLOW_HORIZONTAL,
            "left": "0px",
            "isModalContainer": false,
            "skin": "CopyslFbox0i3b050eeb8134e",
            "top": "0px",
            "width": "100%",
            "zIndex": 1
        }, controller.args[0], "flxEdit"), extendConfig({}, controller.args[1], "flxEdit"), extendConfig({}, controller.args[2], "flxEdit"));
        flxEdit.setDefaultUnit(kony.flex.DP);
        var lblIconEdit = new kony.ui.Label(extendConfig({
            "centerY": "50%",
            "height": "20px",
            "id": "lblIconEdit",
            "isVisible": true,
            "left": "14px",
            "skin": "sknIcon20px",
            "i18n_text": "kony.i18n.getLocalizedString(\"i18n.userwidgetmodel.lblIconOption1\")",
            "width": "20px",
            "zIndex": 1
        }, controller.args[0], "lblIconEdit"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblIconEdit"), extendConfig({}, controller.args[2], "lblIconEdit"));
        var lblEdit = new kony.ui.Label(extendConfig({
            "centerY": "50%",
            "id": "lblEdit",
            "isVisible": true,
            "left": "10dp",
            "skin": "sknlblLatoBold35475f14px",
            "i18n_text": "kony.i18n.getLocalizedString(\"i18n.LeadManagement.contextualMenuEdit\")",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, controller.args[0], "lblEdit"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblEdit"), extendConfig({
            "hoverSkin": "sknlblLato11ABEB13px"
        }, controller.args[2], "lblEdit"));
        flxEdit.add(lblIconEdit, lblEdit);
        var flxAssign = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "clipBounds": true,
            "height": "35px",
            "id": "flxAssign",
            "isVisible": false,
            "layoutType": kony.flex.FLOW_HORIZONTAL,
            "left": "0px",
            "isModalContainer": false,
            "skin": "CopyslFbox0i3b050eeb8134e",
            "top": "0px",
            "width": "100%",
            "zIndex": 1
        }, controller.args[0], "flxAssign"), extendConfig({}, controller.args[1], "flxAssign"), extendConfig({}, controller.args[2], "flxAssign"));
        flxAssign.setDefaultUnit(kony.flex.DP);
        var lblIconAssign = new kony.ui.Label(extendConfig({
            "centerY": "50%",
            "height": "20px",
            "id": "lblIconAssign",
            "isVisible": true,
            "left": "14px",
            "skin": "sknIcon20px",
            "i18n_text": "kony.i18n.getLocalizedString(\"i18n.userwidgetmodel.lblFontIconStatus1\")",
            "width": "20px",
            "zIndex": 1
        }, controller.args[0], "lblIconAssign"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblIconAssign"), extendConfig({}, controller.args[2], "lblIconAssign"));
        var lblAssign = new kony.ui.Label(extendConfig({
            "centerY": "50%",
            "id": "lblAssign",
            "isVisible": true,
            "left": "10dp",
            "skin": "sknlblLatoBold35475f14px",
            "i18n_text": "kony.i18n.getLocalizedString(\"i18n.LeadManagement.contextualMenuAssign\")",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, controller.args[0], "lblAssign"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblAssign"), extendConfig({
            "hoverSkin": "sknlblLato11ABEB13px"
        }, controller.args[2], "lblAssign"));
        flxAssign.add(lblIconAssign, lblAssign);
        var flxMoveToNew = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "clipBounds": true,
            "height": "35px",
            "id": "flxMoveToNew",
            "isVisible": true,
            "layoutType": kony.flex.FLOW_HORIZONTAL,
            "left": "0px",
            "isModalContainer": false,
            "skin": "CopyslFbox0i3b050eeb8134e",
            "top": "0px",
            "width": "100%",
            "zIndex": 1
        }, controller.args[0], "flxMoveToNew"), extendConfig({}, controller.args[1], "flxMoveToNew"), extendConfig({}, controller.args[2], "flxMoveToNew"));
        flxMoveToNew.setDefaultUnit(kony.flex.DP);
        var lblIconMoveToNew = new kony.ui.Label(extendConfig({
            "centerY": "50%",
            "height": "20px",
            "id": "lblIconMoveToNew",
            "isVisible": true,
            "left": "14px",
            "skin": "sknIcon20px",
            "i18n_text": "kony.i18n.getLocalizedString(\"i18n.userwidgetmodel.fonticonProgress\")",
            "width": "20px",
            "zIndex": 1
        }, controller.args[0], "lblIconMoveToNew"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblIconMoveToNew"), extendConfig({}, controller.args[2], "lblIconMoveToNew"));
        var lblMoveToNew = new kony.ui.Label(extendConfig({
            "centerY": "50%",
            "id": "lblMoveToNew",
            "isVisible": true,
            "left": "10dp",
            "skin": "sknlblLatoBold35475f14px",
            "i18n_text": "kony.i18n.getLocalizedString(\"i18n.LeadManagement.contextualMenuMoveToNew\")",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, controller.args[0], "lblMoveToNew"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblMoveToNew"), extendConfig({
            "hoverSkin": "sknlblLato11ABEB13px"
        }, controller.args[2], "lblMoveToNew"));
        flxMoveToNew.add(lblIconMoveToNew, lblMoveToNew);
        var flxMoveToInProgress = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "clipBounds": true,
            "height": "35px",
            "id": "flxMoveToInProgress",
            "isVisible": true,
            "layoutType": kony.flex.FLOW_HORIZONTAL,
            "left": "0px",
            "isModalContainer": false,
            "skin": "CopyslFbox0i3b050eeb8134e",
            "top": "0px",
            "width": "100%",
            "zIndex": 1
        }, controller.args[0], "flxMoveToInProgress"), extendConfig({}, controller.args[1], "flxMoveToInProgress"), extendConfig({}, controller.args[2], "flxMoveToInProgress"));
        flxMoveToInProgress.setDefaultUnit(kony.flex.DP);
        var lblIconMoveToInProgress = new kony.ui.Label(extendConfig({
            "centerY": "50%",
            "height": "20px",
            "id": "lblIconMoveToInProgress",
            "isVisible": true,
            "left": "14px",
            "skin": "sknIcon20px",
            "i18n_text": "kony.i18n.getLocalizedString(\"i18n.userwidgetmodel.fonticonProgress\")",
            "width": "20px",
            "zIndex": 1
        }, controller.args[0], "lblIconMoveToInProgress"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblIconMoveToInProgress"), extendConfig({}, controller.args[2], "lblIconMoveToInProgress"));
        var lblMoveToInProgress = new kony.ui.Label(extendConfig({
            "centerY": "50%",
            "id": "lblMoveToInProgress",
            "isVisible": true,
            "left": "10dp",
            "skin": "sknlblLatoBold35475f14px",
            "i18n_text": "kony.i18n.getLocalizedString(\"i18n.LeadManagement.contextualMenuMoveToInProgress\")",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, controller.args[0], "lblMoveToInProgress"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblMoveToInProgress"), extendConfig({
            "hoverSkin": "sknlblLato11ABEB13px"
        }, controller.args[2], "lblMoveToInProgress"));
        flxMoveToInProgress.add(lblIconMoveToInProgress, lblMoveToInProgress);
        var flxStartAnApplication = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "clipBounds": true,
            "height": "35px",
            "id": "flxStartAnApplication",
            "isVisible": false,
            "layoutType": kony.flex.FLOW_HORIZONTAL,
            "left": "0px",
            "isModalContainer": false,
            "skin": "CopyslFbox0i3b050eeb8134e",
            "top": "0px",
            "width": "100%",
            "zIndex": 1
        }, controller.args[0], "flxStartAnApplication"), extendConfig({}, controller.args[1], "flxStartAnApplication"), extendConfig({}, controller.args[2], "flxStartAnApplication"));
        flxStartAnApplication.setDefaultUnit(kony.flex.DP);
        var lblIconStartAnApplication = new kony.ui.Label(extendConfig({
            "centerY": "50%",
            "height": "20px",
            "id": "lblIconStartAnApplication",
            "isVisible": true,
            "left": "14px",
            "skin": "sknIcon20px",
            "i18n_text": "kony.i18n.getLocalizedString(\"i18n.userwidgetmodel.lblIconOption1\")",
            "width": "20px",
            "zIndex": 1
        }, controller.args[0], "lblIconStartAnApplication"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblIconStartAnApplication"), extendConfig({}, controller.args[2], "lblIconStartAnApplication"));
        var lblStartAnApplication = new kony.ui.Label(extendConfig({
            "centerY": "50%",
            "id": "lblStartAnApplication",
            "isVisible": true,
            "left": "10dp",
            "skin": "sknlblLatoBold35475f14px",
            "i18n_text": "kony.i18n.getLocalizedString(\"i18n.LeadManagement.contextualMenuStartAnApplication\")",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, controller.args[0], "lblStartAnApplication"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblStartAnApplication"), extendConfig({
            "hoverSkin": "sknlblLato11ABEB13px"
        }, controller.args[2], "lblStartAnApplication"));
        flxStartAnApplication.add(lblIconStartAnApplication, lblStartAnApplication);
        var flxClose = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "clipBounds": true,
            "height": "35px",
            "id": "flxClose",
            "isVisible": true,
            "layoutType": kony.flex.FLOW_HORIZONTAL,
            "left": "0px",
            "isModalContainer": false,
            "skin": "CopyslFbox0i3b050eeb8134e",
            "top": "0px",
            "width": "100%",
            "zIndex": 1
        }, controller.args[0], "flxClose"), extendConfig({}, controller.args[1], "flxClose"), extendConfig({}, controller.args[2], "flxClose"));
        flxClose.setDefaultUnit(kony.flex.DP);
        var lblIconClose = new kony.ui.Label(extendConfig({
            "centerY": "50%",
            "height": "20px",
            "id": "lblIconClose",
            "isVisible": true,
            "left": "14px",
            "skin": "sknIcon20px",
            "i18n_text": "kony.i18n.getLocalizedString(\"i18n.userwidgetmodel.fonticonCross\")",
            "width": "20px",
            "zIndex": 1
        }, controller.args[0], "lblIconClose"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblIconClose"), extendConfig({}, controller.args[2], "lblIconClose"));
        var lblClose = new kony.ui.Label(extendConfig({
            "centerY": "50%",
            "id": "lblClose",
            "isVisible": true,
            "left": "10dp",
            "skin": "sknlblLatoBold35475f14px",
            "i18n_text": "kony.i18n.getLocalizedString(\"i18n.LeadManagement.contextualMenuClose\")",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, controller.args[0], "lblClose"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblClose"), extendConfig({
            "hoverSkin": "sknlblLato11ABEB13px"
        }, controller.args[2], "lblClose"));
        flxClose.add(lblIconClose, lblClose);
        leadContextualMenu.add(flxEdit, flxAssign, flxMoveToNew, flxMoveToInProgress, flxStartAnApplication, flxClose);
        return leadContextualMenu;
    }
})