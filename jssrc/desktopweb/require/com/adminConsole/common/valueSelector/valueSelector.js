define(function() {
    return function(controller) {
        var valueSelector = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "clipBounds": true,
            "isMaster": true,
            "height": "100%",
            "id": "valueSelector",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "left": "0dp",
            "isModalContainer": false,
            "preShow": function(eventobject) {
                controller.AS_FlexContainer_j5d31046235542f0bcdc5d5f2452d3a2(eventobject);
            },
            "skin": "slFbox",
            "top": "0dp",
            "width": "100%"
        }, controller.args[0], "valueSelector"), extendConfig({}, controller.args[1], "valueSelector"), extendConfig({}, controller.args[2], "valueSelector"));
        valueSelector.setDefaultUnit(kony.flex.DP);
        var flxOuterContainerBox = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "clipBounds": true,
            "height": "40dp",
            "id": "flxOuterContainerBox",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "left": "0dp",
            "isModalContainer": false,
            "skin": "sknFlxbgFFFFFFbdrD7D9E0rd3px",
            "top": "0dp",
            "width": "259dp",
            "zIndex": 1
        }, controller.args[0], "flxOuterContainerBox"), extendConfig({}, controller.args[1], "flxOuterContainerBox"), extendConfig({}, controller.args[2], "flxOuterContainerBox"));
        flxOuterContainerBox.setDefaultUnit(kony.flex.DP);
        var flxValueBox1 = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "clipBounds": true,
            "height": "100%",
            "id": "flxValueBox1",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "left": "0dp",
            "isModalContainer": false,
            "skin": "sknCursor",
            "top": "0dp",
            "width": "37dp",
            "zIndex": 1
        }, controller.args[0], "flxValueBox1"), extendConfig({}, controller.args[1], "flxValueBox1"), extendConfig({}, controller.args[2], "flxValueBox1"));
        flxValueBox1.setDefaultUnit(kony.flex.DP);
        var flxValueSelectedBox1 = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "centerX": "50%",
            "centerY": "48%",
            "clipBounds": true,
            "height": "30dp",
            "id": "flxValueSelectedBox1",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "isModalContainer": false,
            "skin": "slFbox",
            "width": "28dp",
            "zIndex": 1
        }, controller.args[0], "flxValueSelectedBox1"), extendConfig({}, controller.args[1], "flxValueSelectedBox1"), extendConfig({}, controller.args[2], "flxValueSelectedBox1"));
        flxValueSelectedBox1.setDefaultUnit(kony.flex.DP);
        var lblValue1 = new kony.ui.Label(extendConfig({
            "centerX": "50%",
            "centerY": "50%",
            "id": "lblValue1",
            "isVisible": true,
            "skin": "sknLbl485C75LatoRegular13Px",
            "text": "1",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, controller.args[0], "lblValue1"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblValue1"), extendConfig({}, controller.args[2], "lblValue1"));
        flxValueSelectedBox1.add(lblValue1);
        var lblVerticalLine1 = new kony.ui.Label(extendConfig({
            "bottom": "10dp",
            "id": "lblVerticalLine1",
            "isVisible": true,
            "right": "0dp",
            "skin": "sknlblSeperatorD7D9E0",
            "text": "-",
            "top": "10dp",
            "width": "1dp",
            "zIndex": 1
        }, controller.args[0], "lblVerticalLine1"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblVerticalLine1"), extendConfig({}, controller.args[2], "lblVerticalLine1"));
        flxValueBox1.add(flxValueSelectedBox1, lblVerticalLine1);
        var flxValueBox2 = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "clipBounds": true,
            "height": "100%",
            "id": "flxValueBox2",
            "isVisible": false,
            "layoutType": kony.flex.FREE_FORM,
            "left": "37dp",
            "isModalContainer": false,
            "skin": "sknCursor",
            "top": "0dp",
            "width": "37dp",
            "zIndex": 1
        }, controller.args[0], "flxValueBox2"), extendConfig({}, controller.args[1], "flxValueBox2"), extendConfig({}, controller.args[2], "flxValueBox2"));
        flxValueBox2.setDefaultUnit(kony.flex.DP);
        var flxValueSelectedBox2 = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "centerX": "50%",
            "centerY": "48%",
            "clipBounds": true,
            "height": "30dp",
            "id": "flxValueSelectedBox2",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "isModalContainer": false,
            "skin": "slFbox",
            "width": "28dp",
            "zIndex": 1
        }, controller.args[0], "flxValueSelectedBox2"), extendConfig({}, controller.args[1], "flxValueSelectedBox2"), extendConfig({}, controller.args[2], "flxValueSelectedBox2"));
        flxValueSelectedBox2.setDefaultUnit(kony.flex.DP);
        var lblValue2 = new kony.ui.Label(extendConfig({
            "centerX": "50%",
            "centerY": "50%",
            "id": "lblValue2",
            "isVisible": true,
            "skin": "sknLbl485C75LatoRegular13Px",
            "text": "1",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, controller.args[0], "lblValue2"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblValue2"), extendConfig({}, controller.args[2], "lblValue2"));
        flxValueSelectedBox2.add(lblValue2);
        var lblVerticalLine2 = new kony.ui.Label(extendConfig({
            "bottom": "10dp",
            "id": "lblVerticalLine2",
            "isVisible": true,
            "right": "0dp",
            "skin": "sknlblSeperatorD7D9E0",
            "text": "-",
            "top": "10dp",
            "width": "1dp",
            "zIndex": 1
        }, controller.args[0], "lblVerticalLine2"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblVerticalLine2"), extendConfig({}, controller.args[2], "lblVerticalLine2"));
        flxValueBox2.add(flxValueSelectedBox2, lblVerticalLine2);
        var flxValueBox3 = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "clipBounds": true,
            "height": "100%",
            "id": "flxValueBox3",
            "isVisible": false,
            "layoutType": kony.flex.FREE_FORM,
            "left": "74dp",
            "isModalContainer": false,
            "skin": "sknCursor",
            "top": "0dp",
            "width": "37dp",
            "zIndex": 1
        }, controller.args[0], "flxValueBox3"), extendConfig({}, controller.args[1], "flxValueBox3"), extendConfig({}, controller.args[2], "flxValueBox3"));
        flxValueBox3.setDefaultUnit(kony.flex.DP);
        var flxValueSelectedBox3 = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "centerX": "50%",
            "centerY": "48%",
            "clipBounds": true,
            "height": "30dp",
            "id": "flxValueSelectedBox3",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "isModalContainer": false,
            "skin": "slFbox",
            "width": "28dp",
            "zIndex": 1
        }, controller.args[0], "flxValueSelectedBox3"), extendConfig({}, controller.args[1], "flxValueSelectedBox3"), extendConfig({}, controller.args[2], "flxValueSelectedBox3"));
        flxValueSelectedBox3.setDefaultUnit(kony.flex.DP);
        var lblValue3 = new kony.ui.Label(extendConfig({
            "centerX": "50%",
            "centerY": "50%",
            "id": "lblValue3",
            "isVisible": true,
            "skin": "sknLbl485C75LatoRegular13Px",
            "text": "1",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, controller.args[0], "lblValue3"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblValue3"), extendConfig({}, controller.args[2], "lblValue3"));
        flxValueSelectedBox3.add(lblValue3);
        var lblVerticalLine3 = new kony.ui.Label(extendConfig({
            "bottom": "10dp",
            "id": "lblVerticalLine3",
            "isVisible": true,
            "right": "0dp",
            "skin": "sknlblSeperatorD7D9E0",
            "text": "-",
            "top": "10dp",
            "width": "1dp",
            "zIndex": 1
        }, controller.args[0], "lblVerticalLine3"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblVerticalLine3"), extendConfig({}, controller.args[2], "lblVerticalLine3"));
        flxValueBox3.add(flxValueSelectedBox3, lblVerticalLine3);
        var flxValueBox4 = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "clipBounds": true,
            "height": "100%",
            "id": "flxValueBox4",
            "isVisible": false,
            "layoutType": kony.flex.FREE_FORM,
            "left": "111dp",
            "isModalContainer": false,
            "skin": "sknCursor",
            "top": "0dp",
            "width": "37dp",
            "zIndex": 1
        }, controller.args[0], "flxValueBox4"), extendConfig({}, controller.args[1], "flxValueBox4"), extendConfig({}, controller.args[2], "flxValueBox4"));
        flxValueBox4.setDefaultUnit(kony.flex.DP);
        var flxValueSelectedBox4 = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "centerX": "50%",
            "centerY": "48%",
            "clipBounds": true,
            "height": "30dp",
            "id": "flxValueSelectedBox4",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "isModalContainer": false,
            "skin": "slFbox",
            "width": "28dp",
            "zIndex": 1
        }, controller.args[0], "flxValueSelectedBox4"), extendConfig({}, controller.args[1], "flxValueSelectedBox4"), extendConfig({}, controller.args[2], "flxValueSelectedBox4"));
        flxValueSelectedBox4.setDefaultUnit(kony.flex.DP);
        var lblValue4 = new kony.ui.Label(extendConfig({
            "centerX": "50%",
            "centerY": "50%",
            "id": "lblValue4",
            "isVisible": true,
            "skin": "sknLbl485C75LatoRegular13Px",
            "text": "1",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, controller.args[0], "lblValue4"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblValue4"), extendConfig({}, controller.args[2], "lblValue4"));
        flxValueSelectedBox4.add(lblValue4);
        var lblVerticalLine4 = new kony.ui.Label(extendConfig({
            "bottom": "10dp",
            "id": "lblVerticalLine4",
            "isVisible": true,
            "right": "0dp",
            "skin": "sknlblSeperatorD7D9E0",
            "text": "-",
            "top": "10dp",
            "width": "1dp",
            "zIndex": 1
        }, controller.args[0], "lblVerticalLine4"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblVerticalLine4"), extendConfig({}, controller.args[2], "lblVerticalLine4"));
        flxValueBox4.add(flxValueSelectedBox4, lblVerticalLine4);
        var flxValueBox5 = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "clipBounds": true,
            "height": "100%",
            "id": "flxValueBox5",
            "isVisible": false,
            "layoutType": kony.flex.FREE_FORM,
            "left": "148dp",
            "isModalContainer": false,
            "skin": "sknCursor",
            "top": "0dp",
            "width": "37dp",
            "zIndex": 1
        }, controller.args[0], "flxValueBox5"), extendConfig({}, controller.args[1], "flxValueBox5"), extendConfig({}, controller.args[2], "flxValueBox5"));
        flxValueBox5.setDefaultUnit(kony.flex.DP);
        var flxValueSelectedBox5 = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "centerX": "50%",
            "centerY": "48%",
            "clipBounds": true,
            "height": "30dp",
            "id": "flxValueSelectedBox5",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "isModalContainer": false,
            "skin": "slFbox",
            "width": "28dp",
            "zIndex": 1
        }, controller.args[0], "flxValueSelectedBox5"), extendConfig({}, controller.args[1], "flxValueSelectedBox5"), extendConfig({}, controller.args[2], "flxValueSelectedBox5"));
        flxValueSelectedBox5.setDefaultUnit(kony.flex.DP);
        var lblValue5 = new kony.ui.Label(extendConfig({
            "centerX": "50%",
            "centerY": "50%",
            "id": "lblValue5",
            "isVisible": true,
            "skin": "sknLbl485C75LatoRegular13Px",
            "text": "1",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, controller.args[0], "lblValue5"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblValue5"), extendConfig({}, controller.args[2], "lblValue5"));
        flxValueSelectedBox5.add(lblValue5);
        var lblVerticalLine5 = new kony.ui.Label(extendConfig({
            "bottom": "10dp",
            "id": "lblVerticalLine5",
            "isVisible": true,
            "right": "0dp",
            "skin": "sknlblSeperatorD7D9E0",
            "text": "-",
            "top": "10dp",
            "width": "1dp",
            "zIndex": 1
        }, controller.args[0], "lblVerticalLine5"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblVerticalLine5"), extendConfig({}, controller.args[2], "lblVerticalLine5"));
        flxValueBox5.add(flxValueSelectedBox5, lblVerticalLine5);
        var flxValueBox6 = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "clipBounds": true,
            "height": "100%",
            "id": "flxValueBox6",
            "isVisible": false,
            "layoutType": kony.flex.FREE_FORM,
            "left": "185dp",
            "isModalContainer": false,
            "skin": "sknCursor",
            "top": "0dp",
            "width": "37dp",
            "zIndex": 1
        }, controller.args[0], "flxValueBox6"), extendConfig({}, controller.args[1], "flxValueBox6"), extendConfig({}, controller.args[2], "flxValueBox6"));
        flxValueBox6.setDefaultUnit(kony.flex.DP);
        var flxValueSelectedBox6 = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "centerX": "50%",
            "centerY": "48%",
            "clipBounds": true,
            "height": "30dp",
            "id": "flxValueSelectedBox6",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "isModalContainer": false,
            "skin": "slFbox",
            "width": "28dp",
            "zIndex": 1
        }, controller.args[0], "flxValueSelectedBox6"), extendConfig({}, controller.args[1], "flxValueSelectedBox6"), extendConfig({}, controller.args[2], "flxValueSelectedBox6"));
        flxValueSelectedBox6.setDefaultUnit(kony.flex.DP);
        var lblValue6 = new kony.ui.Label(extendConfig({
            "centerX": "50%",
            "centerY": "50%",
            "id": "lblValue6",
            "isVisible": true,
            "skin": "sknLbl485C75LatoRegular13Px",
            "text": "1",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, controller.args[0], "lblValue6"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblValue6"), extendConfig({}, controller.args[2], "lblValue6"));
        flxValueSelectedBox6.add(lblValue6);
        var lblVerticalLine6 = new kony.ui.Label(extendConfig({
            "bottom": "10dp",
            "id": "lblVerticalLine6",
            "isVisible": true,
            "right": "0dp",
            "skin": "sknlblSeperatorD7D9E0",
            "text": "-",
            "top": "10dp",
            "width": "1dp",
            "zIndex": 1
        }, controller.args[0], "lblVerticalLine6"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblVerticalLine6"), extendConfig({}, controller.args[2], "lblVerticalLine6"));
        flxValueBox6.add(flxValueSelectedBox6, lblVerticalLine6);
        var flxValueBox7 = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "clipBounds": true,
            "height": "100%",
            "id": "flxValueBox7",
            "isVisible": false,
            "layoutType": kony.flex.FREE_FORM,
            "left": "222dp",
            "isModalContainer": false,
            "skin": "sknCursor",
            "top": "0dp",
            "width": "37dp",
            "zIndex": 1
        }, controller.args[0], "flxValueBox7"), extendConfig({}, controller.args[1], "flxValueBox7"), extendConfig({}, controller.args[2], "flxValueBox7"));
        flxValueBox7.setDefaultUnit(kony.flex.DP);
        var flxValueSelectedBox7 = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "centerX": "50%",
            "centerY": "48%",
            "clipBounds": true,
            "height": "30dp",
            "id": "flxValueSelectedBox7",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "isModalContainer": false,
            "skin": "slFbox",
            "width": "28dp",
            "zIndex": 1
        }, controller.args[0], "flxValueSelectedBox7"), extendConfig({}, controller.args[1], "flxValueSelectedBox7"), extendConfig({}, controller.args[2], "flxValueSelectedBox7"));
        flxValueSelectedBox7.setDefaultUnit(kony.flex.DP);
        var lblValue7 = new kony.ui.Label(extendConfig({
            "centerX": "50%",
            "centerY": "50%",
            "id": "lblValue7",
            "isVisible": true,
            "skin": "sknLbl485C75LatoRegular13Px",
            "text": "1",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, controller.args[0], "lblValue7"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblValue7"), extendConfig({}, controller.args[2], "lblValue7"));
        flxValueSelectedBox7.add(lblValue7);
        var lblVerticalLine7 = new kony.ui.Label(extendConfig({
            "bottom": "10dp",
            "id": "lblVerticalLine7",
            "isVisible": true,
            "right": "0dp",
            "skin": "sknlblSeperatorD7D9E0",
            "text": "-",
            "top": "10dp",
            "width": "1dp",
            "zIndex": 1
        }, controller.args[0], "lblVerticalLine7"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblVerticalLine7"), extendConfig({}, controller.args[2], "lblVerticalLine7"));
        flxValueBox7.add(flxValueSelectedBox7, lblVerticalLine7);
        flxOuterContainerBox.add(flxValueBox1, flxValueBox2, flxValueBox3, flxValueBox4, flxValueBox5, flxValueBox6, flxValueBox7);
        valueSelector.add(flxOuterContainerBox);
        return valueSelector;
    }
})