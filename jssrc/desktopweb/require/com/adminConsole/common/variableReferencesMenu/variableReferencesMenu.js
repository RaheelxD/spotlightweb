define(function() {
    return function(controller) {
        var variableReferencesMenu = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
            "clipBounds": true,
            "isMaster": true,
            "id": "variableReferencesMenu",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "left": "0dp",
            "isModalContainer": false,
            "skin": "slFbox",
            "top": "5dp",
            "width": "300dp",
            "zIndex": 5
        }, controller.args[0], "variableReferencesMenu"), extendConfig({}, controller.args[1], "variableReferencesMenu"), extendConfig({}, controller.args[2], "variableReferencesMenu"));
        variableReferencesMenu.setDefaultUnit(kony.flex.DP);
        var flxArrowImage = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "clipBounds": true,
            "height": "18dp",
            "id": "flxArrowImage",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "left": "0dp",
            "isModalContainer": false,
            "skin": "slFbox",
            "top": "8dp",
            "width": "18dp",
            "zIndex": 2
        }, controller.args[0], "flxArrowImage"), extendConfig({}, controller.args[1], "flxArrowImage"), extendConfig({}, controller.args[2], "flxArrowImage"));
        flxArrowImage.setDefaultUnit(kony.flex.DP);
        var imgUpArrow = new kony.ui.Image2(extendConfig({
            "height": "18dp",
            "id": "imgUpArrow",
            "isVisible": true,
            "right": "0dp",
            "skin": "slImage",
            "src": "leftarrow_2x.png",
            "top": "0dp",
            "width": "12dp",
            "zIndex": 1
        }, controller.args[0], "imgUpArrow"), extendConfig({
            "imageScaleMode": constants.IMAGE_SCALE_MODE_FIT_TO_DIMENSIONS,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "imgUpArrow"), extendConfig({}, controller.args[2], "imgUpArrow"));
        flxArrowImage.add(imgUpArrow);
        var flxChechboxOuter = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
            "clipBounds": true,
            "id": "flxChechboxOuter",
            "isVisible": true,
            "layoutType": kony.flex.FLOW_VERTICAL,
            "left": "17dp",
            "isModalContainer": false,
            "right": "0dp",
            "skin": "sknflxffffffOp100BorderE1E5Ed",
            "top": "0dp",
            "zIndex": 1
        }, controller.args[0], "flxChechboxOuter"), extendConfig({}, controller.args[1], "flxChechboxOuter"), extendConfig({}, controller.args[2], "flxChechboxOuter"));
        flxChechboxOuter.setDefaultUnit(kony.flex.DP);
        var lblHeading = new kony.ui.Label(extendConfig({
            "centerX": "50%",
            "id": "lblHeading",
            "isVisible": true,
            "left": "10dp",
            "right": "10dp",
            "skin": "sknlbl485c7511px",
            "text": "Following variables should be enclosed in '<>'",
            "top": "10dp",
            "width": "95%",
            "zIndex": 1
        }, controller.args[0], "lblHeading"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblHeading"), extendConfig({}, controller.args[2], "lblHeading"));
        var lblSeperator = new kony.ui.Label(extendConfig({
            "bottom": 0,
            "centerX": "50%",
            "height": "1px",
            "id": "lblSeperator",
            "isVisible": true,
            "skin": "sknlblSeperator",
            "text": ".",
            "top": "5dp",
            "width": "95%",
            "zIndex": 1
        }, controller.args[0], "lblSeperator"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblSeperator"), extendConfig({}, controller.args[2], "lblSeperator"));
        var segOptionsDropdown = new kony.ui.SegmentedUI2(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
            "centerX": "50%",
            "data": [{
                "lblOptionValue": "Label"
            }, {
                "lblOptionValue": "Label"
            }, {
                "lblOptionValue": "Label"
            }],
            "groupCells": false,
            "id": "segOptionsDropdown",
            "isVisible": true,
            "needPageIndicator": true,
            "pageOffDotImage": "pageoffdot.png",
            "pageOnDotImage": "pageondot.png",
            "retainSelection": false,
            "rowFocusSkin": "seg2Focus",
            "rowSkin": "seg2Normal",
            "rowTemplate": "flxOptionList",
            "sectionHeaderSkin": "sliPhoneSegmentHeader",
            "selectionBehavior": constants.SEGUI_MULTI_SELECT_BEHAVIOR,
            "selectionBehaviorConfig": {
                "imageIdentifier": "imgCheckBox",
                "selectedStateImage": "checkboxselected.png",
                "unselectedStateImage": "checkbox.png"
            },
            "separatorColor": "aaaaaa00",
            "separatorRequired": false,
            "separatorThickness": 0,
            "showScrollbars": false,
            "top": "0dp",
            "viewType": constants.SEGUI_VIEW_TYPE_TABLEVIEW,
            "widgetDataMap": {
                "flxOptionList": "flxOptionList",
                "lblOptionValue": "lblOptionValue"
            },
            "width": "95%",
            "zIndex": 1
        }, controller.args[0], "segOptionsDropdown"), extendConfig({
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "segOptionsDropdown"), extendConfig({}, controller.args[2], "segOptionsDropdown"));
        flxChechboxOuter.add(lblHeading, lblSeperator, segOptionsDropdown);
        variableReferencesMenu.add(flxArrowImage, flxChechboxOuter);
        return variableReferencesMenu;
    }
})