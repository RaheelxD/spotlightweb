define(function() {
    return function(controller) {
        var contactNumber = new kony.ui.FlexContainer(extendConfig({
            "clipBounds": true,
            "isMaster": true,
            "height": "100%",
            "id": "contactNumber",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "left": "0dp",
            "isModalContainer": false,
            "skin": "slFbox",
            "top": "0dp",
            "width": "100%"
        }, controller.args[0], "contactNumber"), extendConfig({}, controller.args[1], "contactNumber"), extendConfig({}, controller.args[2], "contactNumber"));
        contactNumber.setDefaultUnit(kony.flex.DP);
        var flxContactNumberWrapper = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "clipBounds": true,
            "height": "80dp",
            "id": "flxContactNumberWrapper",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "left": "0dp",
            "isModalContainer": false,
            "skin": "slFbox",
            "top": "0dp",
            "width": "300dp"
        }, controller.args[0], "flxContactNumberWrapper"), extendConfig({}, controller.args[1], "flxContactNumberWrapper"), extendConfig({}, controller.args[2], "flxContactNumberWrapper"));
        flxContactNumberWrapper.setDefaultUnit(kony.flex.DP);
        var txtISDCode = new kony.ui.TextBox2(extendConfig({
            "autoCapitalize": constants.TEXTBOX_AUTO_CAPITALIZE_NONE,
            "focusSkin": "defTextBoxFocus",
            "height": "40dp",
            "id": "txtISDCode",
            "isVisible": true,
            "keyBoardStyle": constants.TEXTBOX_KEY_BOARD_STYLE_DEFAULT,
            "left": "0dp",
            "placeholder": "ISD Code",
            "secureTextEntry": false,
            "skin": "skntxtbxDetails0bbf1235271384a",
            "textInputMode": constants.TEXTBOX_INPUT_MODE_ANY,
            "top": "0dp",
            "width": "70dp",
            "zIndex": 1
        }, controller.args[0], "txtISDCode"), extendConfig({
            "containerHeightMode": constants.TEXTBOX_FONT_METRICS_DRIVEN_HEIGHT,
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [3, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "txtISDCode"), extendConfig({
            "autoCorrect": false,
            "placeholderSkin": "defTextBoxPlaceholder"
        }, controller.args[2], "txtISDCode"));
        var txtContactNumber = new kony.ui.TextBox2(extendConfig({
            "autoCapitalize": constants.TEXTBOX_AUTO_CAPITALIZE_NONE,
            "focusSkin": "defTextBoxFocus",
            "height": "40dp",
            "id": "txtContactNumber",
            "isVisible": true,
            "keyBoardStyle": constants.TEXTBOX_KEY_BOARD_STYLE_DEFAULT,
            "left": "100dp",
            "placeholder": "Contact Number",
            "secureTextEntry": false,
            "skin": "skntxtbxDetails0bbf1235271384a",
            "textInputMode": constants.TEXTBOX_INPUT_MODE_ANY,
            "top": "0dp",
            "width": "200dp",
            "zIndex": 1
        }, controller.args[0], "txtContactNumber"), extendConfig({
            "containerHeightMode": constants.TEXTBOX_FONT_METRICS_DRIVEN_HEIGHT,
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [3, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "txtContactNumber"), extendConfig({
            "autoCorrect": false,
            "placeholderSkin": "defTextBoxPlaceholder"
        }, controller.args[2], "txtContactNumber"));
        var lblDash = new kony.ui.Label(extendConfig({
            "height": "40dp",
            "id": "lblDash",
            "isVisible": true,
            "left": "80dp",
            "skin": "sknLblDash",
            "text": "—",
            "top": "0dp",
            "width": "6px",
            "zIndex": 1
        }, controller.args[0], "lblDash"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblDash"), extendConfig({}, controller.args[2], "lblDash"));
        var flxError = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "clipBounds": true,
            "height": "15dp",
            "id": "flxError",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "left": "0dp",
            "isModalContainer": false,
            "skin": "slFbox",
            "top": "45dp",
            "width": "100%",
            "zIndex": 1
        }, controller.args[0], "flxError"), extendConfig({}, controller.args[1], "flxError"), extendConfig({}, controller.args[2], "flxError"));
        flxError.setDefaultUnit(kony.flex.DP);
        var lblErrorIcon = new kony.ui.Label(extendConfig({
            "height": "15dp",
            "id": "lblErrorIcon",
            "isVisible": true,
            "left": "0dp",
            "skin": "sknErrorIcon",
            "text": "",
            "top": "0dp",
            "width": "15dp",
            "zIndex": 1
        }, controller.args[0], "lblErrorIcon"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblErrorIcon"), extendConfig({}, controller.args[2], "lblErrorIcon"));
        var lblErrorText = new kony.ui.Label(extendConfig({
            "height": "15dp",
            "id": "lblErrorText",
            "isVisible": true,
            "left": "15dp",
            "skin": "sknlblError",
            "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmLocations.lblNoPhnNumberError\")",
            "top": "0dp",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, controller.args[0], "lblErrorText"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblErrorText"), extendConfig({}, controller.args[2], "lblErrorText"));
        flxError.add(lblErrorIcon, lblErrorText);
        flxContactNumberWrapper.add(txtISDCode, txtContactNumber, lblDash, flxError);
        contactNumber.add(flxContactNumberWrapper);
        return contactNumber;
    }
})