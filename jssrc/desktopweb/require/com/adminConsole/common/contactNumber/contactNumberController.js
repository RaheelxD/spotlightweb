define("com/adminConsole/common/contactNumber/usercontactNumberController", function() {
    var addingPlus = function(text) {
        var finalText;
        if (text && text !== "" && text.substr(0, 1) !== "+") finalText = "+" + text;
        else finalText = text;
        return finalText;
    };
    var ISDErrorValidations = function(text) {};
    var phoneNumberErrorValidations = function(text, ref) {};
    var showErrorMsg = function(text) {
        this.view.flxError.setVisibility(true);
        this.view.lblErrorText.text = text;
        this.view.txtContactNumber.skin = "skinredbg";
    };
    var hideErrorMsg = function(box) {
        this.view.flxError.setVisibility(false);
        if (box === 1) this.view.txtISDCode.skin = "skntxtbxDetails0bbf1235271384a";
        else this.view.txtContactNumber.skin = "skntxtbxDetails0bbf1235271384a";
    };
    return {
        addingPlus: addingPlus,
        showErrorMsg: showErrorMsg,
        hideErrorMsg: hideErrorMsg
    };
});
define("com/adminConsole/common/contactNumber/contactNumberControllerActions", {
    /*
      This is an auto generated file and any modifications to it may result in corruption of the action sequence.
    */
});
define("com/adminConsole/common/contactNumber/contactNumberController", ["com/adminConsole/common/contactNumber/usercontactNumberController", "com/adminConsole/common/contactNumber/contactNumberControllerActions"], function() {
    var controller = require("com/adminConsole/common/contactNumber/usercontactNumberController");
    var actions = require("com/adminConsole/common/contactNumber/contactNumberControllerActions");
    for (var key in actions) {
        controller[key] = actions[key];
    }
    return controller;
});
