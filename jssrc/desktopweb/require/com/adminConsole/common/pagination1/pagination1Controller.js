define("com/adminConsole/common/pagination1/userpagination1Controller", function() {
    return {};
});
define("com/adminConsole/common/pagination1/pagination1ControllerActions", {
    /*
          This is an auto generated file and any modifications to it may result in corruption of the action sequence.
        */
});
define("com/adminConsole/common/pagination1/pagination1Controller", ["com/adminConsole/common/pagination1/userpagination1Controller", "com/adminConsole/common/pagination1/pagination1ControllerActions"], function() {
    var controller = require("com/adminConsole/common/pagination1/userpagination1Controller");
    var actions = require("com/adminConsole/common/pagination1/pagination1ControllerActions");
    for (var key in actions) {
        controller[key] = actions[key];
    }
    return controller;
});
