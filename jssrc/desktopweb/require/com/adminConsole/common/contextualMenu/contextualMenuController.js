define("com/adminConsole/common/contextualMenu/usercontextualMenuController", function() {
    return {};
});
define("com/adminConsole/common/contextualMenu/contextualMenuControllerActions", {
    /*
          This is an auto generated file and any modifications to it may result in corruption of the action sequence.
        */
});
define("com/adminConsole/common/contextualMenu/contextualMenuController", ["com/adminConsole/common/contextualMenu/usercontextualMenuController", "com/adminConsole/common/contextualMenu/contextualMenuControllerActions"], function() {
    var controller = require("com/adminConsole/common/contextualMenu/usercontextualMenuController");
    var actions = require("com/adminConsole/common/contextualMenu/contextualMenuControllerActions");
    for (var key in actions) {
        controller[key] = actions[key];
    }
    return controller;
});
