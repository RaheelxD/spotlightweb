define(function() {
    return function(controller) {
        var verticalTabs1 = new kony.ui.FlexContainer(extendConfig({
            "clipBounds": true,
            "isMaster": true,
            "height": "100%",
            "id": "verticalTabs1",
            "isVisible": true,
            "layoutType": kony.flex.FLOW_VERTICAL,
            "left": "0dp",
            "isModalContainer": false,
            "skin": "sknflxf9f9f9NoBorder",
            "top": "0dp",
            "width": "100%"
        }, controller.args[0], "verticalTabs1"), extendConfig({}, controller.args[1], "verticalTabs1"), extendConfig({}, controller.args[2], "verticalTabs1"));
        verticalTabs1.setDefaultUnit(kony.flex.DP);
        var flxOption0 = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
            "clipBounds": true,
            "id": "flxOption0",
            "isVisible": false,
            "layoutType": kony.flex.FLOW_VERTICAL,
            "left": "0dp",
            "isModalContainer": false,
            "skin": "slFbox",
            "top": "0dp",
            "width": "100%",
            "zIndex": 1
        }, controller.args[0], "flxOption0"), extendConfig({}, controller.args[1], "flxOption0"), extendConfig({}, controller.args[2], "flxOption0"));
        flxOption0.setDefaultUnit(kony.flex.DP);
        var flxContentContainer0 = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
            "clipBounds": true,
            "id": "flxContentContainer0",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "left": "0dp",
            "isModalContainer": false,
            "skin": "slFbox",
            "top": "20px",
            "width": "100%"
        }, controller.args[0], "flxContentContainer0"), extendConfig({}, controller.args[1], "flxContentContainer0"), extendConfig({}, controller.args[2], "flxContentContainer0"));
        flxContentContainer0.setDefaultUnit(kony.flex.DP);
        var btnOption0 = new kony.ui.Button(extendConfig({
            "bottom": "0dp",
            "id": "btnOption0",
            "isVisible": true,
            "left": "20dp",
            "skin": "Btna1acbafont12pxKA",
            "i18n_text": "kony.i18n.getLocalizedString(\"i18n.userwidgetmodel.btnOption1\")",
            "top": "0dp",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, controller.args[0], "btnOption0"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_TOP_LEFT,
            "displayText": true,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "btnOption0"), extendConfig({
            "hoverSkin": "Btn000000font14px"
        }, controller.args[2], "btnOption0"));
        var flxImgArrow0 = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "bottom": "0dp",
            "clipBounds": true,
            "height": "15px",
            "id": "flxImgArrow0",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "isModalContainer": false,
            "right": "20px",
            "skin": "slFbox",
            "top": "0dp",
            "width": "15px",
            "zIndex": 1
        }, controller.args[0], "flxImgArrow0"), extendConfig({}, controller.args[1], "flxImgArrow0"), extendConfig({}, controller.args[2], "flxImgArrow0"));
        flxImgArrow0.setDefaultUnit(kony.flex.DP);
        var imgSelected0 = new kony.ui.Image2(extendConfig({
            "centerY": "50%",
            "height": "100%",
            "id": "imgSelected0",
            "isVisible": false,
            "right": 0,
            "skin": "slImage",
            "src": "right_arrow2x.png",
            "width": "100%",
            "zIndex": 1
        }, controller.args[0], "imgSelected0"), extendConfig({
            "imageScaleMode": constants.IMAGE_SCALE_MODE_MAINTAIN_ASPECT_RATIO,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "imgSelected0"), extendConfig({}, controller.args[2], "imgSelected0"));
        var lblSelected0 = new kony.ui.Label(extendConfig({
            "centerX": "50%",
            "centerY": "50%",
            "height": "100%",
            "id": "lblSelected0",
            "isVisible": true,
            "skin": "sknicon15pxBlack",
            "i18n_text": "kony.i18n.getLocalizedString(\"i18n.userwidgetmodel.fontIconSelected1\")",
            "width": "100%",
            "zIndex": 1
        }, controller.args[0], "lblSelected0"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblSelected0"), extendConfig({}, controller.args[2], "lblSelected0"));
        flxImgArrow0.add(imgSelected0, lblSelected0);
        flxContentContainer0.add(btnOption0, flxImgArrow0);
        var lblOptional0 = new kony.ui.Label(extendConfig({
            "id": "lblOptional0",
            "isVisible": false,
            "left": "20px",
            "skin": "slLabel0d4f692dab05249",
            "i18n_text": "kony.i18n.getLocalizedString(\"i18n.permission.Optional\")",
            "top": "10px",
            "width": "93px",
            "zIndex": 1
        }, controller.args[0], "lblOptional0"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblOptional0"), extendConfig({}, controller.args[2], "lblOptional0"));
        var flxSepratorcontainer0 = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "clipBounds": true,
            "height": "1px",
            "id": "flxSepratorcontainer0",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "left": "0dp",
            "isModalContainer": false,
            "skin": "slFbox",
            "top": "15px",
            "width": "100%"
        }, controller.args[0], "flxSepratorcontainer0"), extendConfig({}, controller.args[1], "flxSepratorcontainer0"), extendConfig({}, controller.args[2], "flxSepratorcontainer0"));
        flxSepratorcontainer0.setDefaultUnit(kony.flex.DP);
        var flxoptionSeperator0 = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "bottom": 0,
            "clipBounds": true,
            "height": "1px",
            "id": "flxoptionSeperator0",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "left": "20px",
            "isModalContainer": false,
            "right": "20px",
            "skin": "sknflxe1e5edop100",
            "top": 0,
            "zIndex": 1
        }, controller.args[0], "flxoptionSeperator0"), extendConfig({}, controller.args[1], "flxoptionSeperator0"), extendConfig({}, controller.args[2], "flxoptionSeperator0"));
        flxoptionSeperator0.setDefaultUnit(kony.flex.DP);
        flxoptionSeperator0.add();
        flxSepratorcontainer0.add(flxoptionSeperator0);
        flxOption0.add(flxContentContainer0, lblOptional0, flxSepratorcontainer0);
        var flxOption1 = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
            "clipBounds": true,
            "id": "flxOption1",
            "isVisible": true,
            "layoutType": kony.flex.FLOW_VERTICAL,
            "left": "0dp",
            "isModalContainer": false,
            "skin": "slFbox",
            "top": "0dp",
            "width": "100%",
            "zIndex": 1
        }, controller.args[0], "flxOption1"), extendConfig({}, controller.args[1], "flxOption1"), extendConfig({}, controller.args[2], "flxOption1"));
        flxOption1.setDefaultUnit(kony.flex.DP);
        var flxContentContainer1 = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
            "clipBounds": true,
            "id": "flxContentContainer1",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "left": "0dp",
            "isModalContainer": false,
            "skin": "slFbox",
            "top": "20px",
            "width": "100%"
        }, controller.args[0], "flxContentContainer1"), extendConfig({}, controller.args[1], "flxContentContainer1"), extendConfig({}, controller.args[2], "flxContentContainer1"));
        flxContentContainer1.setDefaultUnit(kony.flex.DP);
        var btnOption1 = new kony.ui.Button(extendConfig({
            "bottom": "0dp",
            "id": "btnOption1",
            "isVisible": true,
            "left": "20dp",
            "skin": "Btn000000font14px",
            "i18n_text": "kony.i18n.getLocalizedString(\"i18n.userwidgetmodel.btnOption1\")",
            "top": "0dp",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, controller.args[0], "btnOption1"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_TOP_LEFT,
            "displayText": true,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "btnOption1"), extendConfig({
            "hoverSkin": "Btn000000font14px"
        }, controller.args[2], "btnOption1"));
        var flxImgArrow1 = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "bottom": "0dp",
            "clipBounds": true,
            "height": "15px",
            "id": "flxImgArrow1",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "isModalContainer": false,
            "right": "20px",
            "skin": "slFbox",
            "top": "0dp",
            "width": "15px",
            "zIndex": 1
        }, controller.args[0], "flxImgArrow1"), extendConfig({}, controller.args[1], "flxImgArrow1"), extendConfig({}, controller.args[2], "flxImgArrow1"));
        flxImgArrow1.setDefaultUnit(kony.flex.DP);
        var imgSelected1 = new kony.ui.Image2(extendConfig({
            "centerY": "50%",
            "height": "100%",
            "id": "imgSelected1",
            "isVisible": false,
            "right": 0,
            "skin": "slImage",
            "src": "right_arrow2x.png",
            "width": "100%",
            "zIndex": 1
        }, controller.args[0], "imgSelected1"), extendConfig({
            "imageScaleMode": constants.IMAGE_SCALE_MODE_MAINTAIN_ASPECT_RATIO,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "imgSelected1"), extendConfig({}, controller.args[2], "imgSelected1"));
        var lblSelected1 = new kony.ui.Label(extendConfig({
            "centerX": "50%",
            "centerY": "50%",
            "height": "100%",
            "id": "lblSelected1",
            "isVisible": true,
            "skin": "sknicon15pxBlack",
            "i18n_text": "kony.i18n.getLocalizedString(\"i18n.userwidgetmodel.fontIconSelected1\")",
            "width": "100%",
            "zIndex": 1
        }, controller.args[0], "lblSelected1"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblSelected1"), extendConfig({}, controller.args[2], "lblSelected1"));
        flxImgArrow1.add(imgSelected1, lblSelected1);
        flxContentContainer1.add(btnOption1, flxImgArrow1);
        var lblOptional1 = new kony.ui.Label(extendConfig({
            "id": "lblOptional1",
            "isVisible": false,
            "left": "20px",
            "skin": "slLabel0d4f692dab05249",
            "i18n_text": "kony.i18n.getLocalizedString(\"i18n.permission.Optional\")",
            "top": "10px",
            "width": "93px",
            "zIndex": 1
        }, controller.args[0], "lblOptional1"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblOptional1"), extendConfig({}, controller.args[2], "lblOptional1"));
        var flxSepratorcontainer1 = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "clipBounds": true,
            "height": "1px",
            "id": "flxSepratorcontainer1",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "left": "0dp",
            "isModalContainer": false,
            "skin": "slFbox",
            "top": "15px",
            "width": "100%"
        }, controller.args[0], "flxSepratorcontainer1"), extendConfig({}, controller.args[1], "flxSepratorcontainer1"), extendConfig({}, controller.args[2], "flxSepratorcontainer1"));
        flxSepratorcontainer1.setDefaultUnit(kony.flex.DP);
        var flxoptionSeperator1 = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "bottom": 0,
            "clipBounds": true,
            "height": "1px",
            "id": "flxoptionSeperator1",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "left": "20px",
            "isModalContainer": false,
            "right": "20px",
            "skin": "sknflxe1e5edop100",
            "top": 0,
            "zIndex": 1
        }, controller.args[0], "flxoptionSeperator1"), extendConfig({}, controller.args[1], "flxoptionSeperator1"), extendConfig({}, controller.args[2], "flxoptionSeperator1"));
        flxoptionSeperator1.setDefaultUnit(kony.flex.DP);
        flxoptionSeperator1.add();
        flxSepratorcontainer1.add(flxoptionSeperator1);
        flxOption1.add(flxContentContainer1, lblOptional1, flxSepratorcontainer1);
        var flxOption2 = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
            "clipBounds": true,
            "id": "flxOption2",
            "isVisible": true,
            "layoutType": kony.flex.FLOW_VERTICAL,
            "left": "0dp",
            "isModalContainer": false,
            "skin": "slFbox",
            "top": "0dp",
            "width": "100%",
            "zIndex": 1
        }, controller.args[0], "flxOption2"), extendConfig({}, controller.args[1], "flxOption2"), extendConfig({}, controller.args[2], "flxOption2"));
        flxOption2.setDefaultUnit(kony.flex.DP);
        var flxContentConatainer2 = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
            "clipBounds": true,
            "id": "flxContentConatainer2",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "left": "0dp",
            "isModalContainer": false,
            "skin": "slFbox",
            "top": "15px",
            "width": "100%"
        }, controller.args[0], "flxContentConatainer2"), extendConfig({}, controller.args[1], "flxContentConatainer2"), extendConfig({}, controller.args[2], "flxContentConatainer2"));
        flxContentConatainer2.setDefaultUnit(kony.flex.DP);
        var btnOption2 = new kony.ui.Button(extendConfig({
            "bottom": "0dp",
            "id": "btnOption2",
            "isVisible": true,
            "left": "20dp",
            "skin": "Btna1acbafont12pxKA",
            "i18n_text": "kony.i18n.getLocalizedString(\"i18n.userwidgetmodel.btnOption2\")",
            "top": "0dp",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, controller.args[0], "btnOption2"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_TOP_LEFT,
            "displayText": true,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "btnOption2"), extendConfig({
            "hoverSkin": "Btn000000font14px"
        }, controller.args[2], "btnOption2"));
        var flxImgArrow2 = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "bottom": "0dp",
            "clipBounds": true,
            "height": "15px",
            "id": "flxImgArrow2",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "isModalContainer": false,
            "right": "20px",
            "skin": "slFbox",
            "top": "0px",
            "width": "15px",
            "zIndex": 1
        }, controller.args[0], "flxImgArrow2"), extendConfig({}, controller.args[1], "flxImgArrow2"), extendConfig({}, controller.args[2], "flxImgArrow2"));
        flxImgArrow2.setDefaultUnit(kony.flex.DP);
        var imgSelected2 = new kony.ui.Image2(extendConfig({
            "centerY": "50%",
            "height": "100%",
            "id": "imgSelected2",
            "isVisible": false,
            "right": 0,
            "skin": "slImage",
            "src": "right_arrow2x.png",
            "width": "100%",
            "zIndex": 1
        }, controller.args[0], "imgSelected2"), extendConfig({
            "imageScaleMode": constants.IMAGE_SCALE_MODE_MAINTAIN_ASPECT_RATIO,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "imgSelected2"), extendConfig({}, controller.args[2], "imgSelected2"));
        var lblSelected2 = new kony.ui.Label(extendConfig({
            "centerX": "50%",
            "centerY": "50%",
            "height": "100%",
            "id": "lblSelected2",
            "isVisible": true,
            "skin": "sknicon15pxBlack",
            "i18n_text": "kony.i18n.getLocalizedString(\"i18n.userwidgetmodel.fontIconSelected1\")",
            "width": "100%",
            "zIndex": 1
        }, controller.args[0], "lblSelected2"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblSelected2"), extendConfig({}, controller.args[2], "lblSelected2"));
        flxImgArrow2.add(imgSelected2, lblSelected2);
        flxContentConatainer2.add(btnOption2, flxImgArrow2);
        var lblOptional2 = new kony.ui.Label(extendConfig({
            "id": "lblOptional2",
            "isVisible": true,
            "left": "20px",
            "skin": "slLabel0d4f692dab05249",
            "i18n_text": "kony.i18n.getLocalizedString(\"i18n.permission.Optional\")",
            "top": "10px",
            "width": "93px",
            "zIndex": 1
        }, controller.args[0], "lblOptional2"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblOptional2"), extendConfig({}, controller.args[2], "lblOptional2"));
        var flxSepratorcontainer2 = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "clipBounds": true,
            "height": "1px",
            "id": "flxSepratorcontainer2",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "left": "0dp",
            "isModalContainer": false,
            "skin": "slFbox",
            "top": "15px",
            "width": "100%"
        }, controller.args[0], "flxSepratorcontainer2"), extendConfig({}, controller.args[1], "flxSepratorcontainer2"), extendConfig({}, controller.args[2], "flxSepratorcontainer2"));
        flxSepratorcontainer2.setDefaultUnit(kony.flex.DP);
        var flxAddSeperator2 = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "bottom": "0dp",
            "clipBounds": true,
            "height": "1px",
            "id": "flxAddSeperator2",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "left": "20px",
            "isModalContainer": false,
            "right": "20px",
            "skin": "sknflxe1e5edop100",
            "top": "0dp",
            "zIndex": 1
        }, controller.args[0], "flxAddSeperator2"), extendConfig({}, controller.args[1], "flxAddSeperator2"), extendConfig({}, controller.args[2], "flxAddSeperator2"));
        flxAddSeperator2.setDefaultUnit(kony.flex.DP);
        flxAddSeperator2.add();
        flxSepratorcontainer2.add(flxAddSeperator2);
        flxOption2.add(flxContentConatainer2, lblOptional2, flxSepratorcontainer2);
        var flxOption3 = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
            "clipBounds": true,
            "id": "flxOption3",
            "isVisible": true,
            "layoutType": kony.flex.FLOW_VERTICAL,
            "left": "0dp",
            "isModalContainer": false,
            "skin": "slFbox",
            "top": "0dp",
            "width": "100%",
            "zIndex": 1
        }, controller.args[0], "flxOption3"), extendConfig({}, controller.args[1], "flxOption3"), extendConfig({}, controller.args[2], "flxOption3"));
        flxOption3.setDefaultUnit(kony.flex.DP);
        var flxContentContainer3 = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
            "clipBounds": true,
            "id": "flxContentContainer3",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "left": "0dp",
            "isModalContainer": false,
            "skin": "slFbox",
            "top": "15dp",
            "width": "100%"
        }, controller.args[0], "flxContentContainer3"), extendConfig({}, controller.args[1], "flxContentContainer3"), extendConfig({}, controller.args[2], "flxContentContainer3"));
        flxContentContainer3.setDefaultUnit(kony.flex.DP);
        var btnOption3 = new kony.ui.Button(extendConfig({
            "bottom": "0dp",
            "id": "btnOption3",
            "isVisible": true,
            "left": "20dp",
            "skin": "Btna1acbafont12pxKA",
            "i18n_text": "kony.i18n.getLocalizedString(\"i18n.userwidgetmodel.btnOption3\")",
            "top": "0dp",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, controller.args[0], "btnOption3"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_TOP_LEFT,
            "displayText": true,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "btnOption3"), extendConfig({
            "hoverSkin": "Btn000000font14px"
        }, controller.args[2], "btnOption3"));
        var flxImgArrow3 = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "bottom": "0dp",
            "clipBounds": true,
            "height": "15px",
            "id": "flxImgArrow3",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "isModalContainer": false,
            "right": "20px",
            "skin": "slFbox",
            "top": "0dp",
            "width": "15px",
            "zIndex": 1
        }, controller.args[0], "flxImgArrow3"), extendConfig({}, controller.args[1], "flxImgArrow3"), extendConfig({}, controller.args[2], "flxImgArrow3"));
        flxImgArrow3.setDefaultUnit(kony.flex.DP);
        var imgSelected3 = new kony.ui.Image2(extendConfig({
            "height": "100%",
            "id": "imgSelected3",
            "isVisible": false,
            "right": 0,
            "skin": "slImage",
            "src": "right_arrow2x.png",
            "top": "0dp",
            "width": "100%",
            "zIndex": 1
        }, controller.args[0], "imgSelected3"), extendConfig({
            "imageScaleMode": constants.IMAGE_SCALE_MODE_MAINTAIN_ASPECT_RATIO,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "imgSelected3"), extendConfig({}, controller.args[2], "imgSelected3"));
        var lblSelected3 = new kony.ui.Label(extendConfig({
            "centerX": "50%",
            "centerY": "50%",
            "height": "100%",
            "id": "lblSelected3",
            "isVisible": true,
            "left": "5dp",
            "skin": "sknicon15pxBlack",
            "i18n_text": "kony.i18n.getLocalizedString(\"i18n.userwidgetmodel.fontIconSelected1\")",
            "top": "6dp",
            "width": "100%",
            "zIndex": 1
        }, controller.args[0], "lblSelected3"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblSelected3"), extendConfig({}, controller.args[2], "lblSelected3"));
        flxImgArrow3.add(imgSelected3, lblSelected3);
        flxContentContainer3.add(btnOption3, flxImgArrow3);
        var lblOptional3 = new kony.ui.Label(extendConfig({
            "id": "lblOptional3",
            "isVisible": true,
            "left": "20px",
            "skin": "slLabel0d4f692dab05249",
            "i18n_text": "kony.i18n.getLocalizedString(\"i18n.permission.Optional\")",
            "top": "10px",
            "width": "93px",
            "zIndex": 1
        }, controller.args[0], "lblOptional3"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblOptional3"), extendConfig({}, controller.args[2], "lblOptional3"));
        var flxSepratorConatainer3 = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "clipBounds": true,
            "height": "1dp",
            "id": "flxSepratorConatainer3",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "left": "0dp",
            "isModalContainer": false,
            "skin": "slFbox",
            "top": "15dp",
            "width": "100%"
        }, controller.args[0], "flxSepratorConatainer3"), extendConfig({}, controller.args[1], "flxSepratorConatainer3"), extendConfig({}, controller.args[2], "flxSepratorConatainer3"));
        flxSepratorConatainer3.setDefaultUnit(kony.flex.DP);
        var flxAddSeperator3 = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "bottom": 0,
            "clipBounds": true,
            "height": "1px",
            "id": "flxAddSeperator3",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "left": "20px",
            "isModalContainer": false,
            "right": "20px",
            "skin": "sknflxe1e5edop100",
            "zIndex": 1
        }, controller.args[0], "flxAddSeperator3"), extendConfig({}, controller.args[1], "flxAddSeperator3"), extendConfig({}, controller.args[2], "flxAddSeperator3"));
        flxAddSeperator3.setDefaultUnit(kony.flex.DP);
        flxAddSeperator3.add();
        flxSepratorConatainer3.add(flxAddSeperator3);
        flxOption3.add(flxContentContainer3, lblOptional3, flxSepratorConatainer3);
        var flxOption4 = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
            "clipBounds": true,
            "id": "flxOption4",
            "isVisible": true,
            "layoutType": kony.flex.FLOW_VERTICAL,
            "left": "0dp",
            "isModalContainer": false,
            "skin": "slFbox",
            "top": "0dp",
            "width": "100%",
            "zIndex": 1
        }, controller.args[0], "flxOption4"), extendConfig({}, controller.args[1], "flxOption4"), extendConfig({}, controller.args[2], "flxOption4"));
        flxOption4.setDefaultUnit(kony.flex.DP);
        var flxContentContainer4 = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
            "clipBounds": true,
            "id": "flxContentContainer4",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "left": "0dp",
            "isModalContainer": false,
            "skin": "slFbox",
            "top": "15px",
            "width": "100%"
        }, controller.args[0], "flxContentContainer4"), extendConfig({}, controller.args[1], "flxContentContainer4"), extendConfig({}, controller.args[2], "flxContentContainer4"));
        flxContentContainer4.setDefaultUnit(kony.flex.DP);
        var lblIconDropArrow4 = new kony.ui.Label(extendConfig({
            "id": "lblIconDropArrow4",
            "isVisible": false,
            "left": "20dp",
            "skin": "sknIcon485C7513px",
            "text": "",
            "top": "0px",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, controller.args[0], "lblIconDropArrow4"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_TOP_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblIconDropArrow4"), extendConfig({}, controller.args[2], "lblIconDropArrow4"));
        var btnOption4 = new kony.ui.Button(extendConfig({
            "bottom": "0dp",
            "id": "btnOption4",
            "isVisible": true,
            "left": "20dp",
            "skin": "Btna1acbafont12pxKA",
            "i18n_text": "kony.i18n.getLocalizedString(\"i18n.userwidgetmodel.btnOption4\")",
            "top": "0dp",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, controller.args[0], "btnOption4"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_TOP_LEFT,
            "displayText": true,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "btnOption4"), extendConfig({
            "hoverSkin": "Btn000000font14px"
        }, controller.args[2], "btnOption4"));
        var flxImgArrow4 = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "bottom": "0dp",
            "clipBounds": true,
            "height": "15px",
            "id": "flxImgArrow4",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "isModalContainer": false,
            "right": "20px",
            "skin": "slFbox",
            "top": "0px",
            "width": "15px",
            "zIndex": 1
        }, controller.args[0], "flxImgArrow4"), extendConfig({}, controller.args[1], "flxImgArrow4"), extendConfig({}, controller.args[2], "flxImgArrow4"));
        flxImgArrow4.setDefaultUnit(kony.flex.DP);
        var imgSelected4 = new kony.ui.Image2(extendConfig({
            "height": "100%",
            "id": "imgSelected4",
            "isVisible": false,
            "right": 0,
            "skin": "slImage",
            "src": "right_arrow2x.png",
            "top": "0dp",
            "width": "100%",
            "zIndex": 1
        }, controller.args[0], "imgSelected4"), extendConfig({
            "imageScaleMode": constants.IMAGE_SCALE_MODE_MAINTAIN_ASPECT_RATIO,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "imgSelected4"), extendConfig({}, controller.args[2], "imgSelected4"));
        var lblSelected4 = new kony.ui.Label(extendConfig({
            "centerX": "50%",
            "centerY": "50%",
            "height": "100%",
            "id": "lblSelected4",
            "isVisible": true,
            "skin": "sknicon15pxBlack",
            "i18n_text": "kony.i18n.getLocalizedString(\"i18n.userwidgetmodel.fontIconSelected1\")",
            "width": "100%",
            "zIndex": 1
        }, controller.args[0], "lblSelected4"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblSelected4"), extendConfig({}, controller.args[2], "lblSelected4"));
        flxImgArrow4.add(imgSelected4, lblSelected4);
        flxContentContainer4.add(lblIconDropArrow4, btnOption4, flxImgArrow4);
        var lblOptional4 = new kony.ui.Label(extendConfig({
            "id": "lblOptional4",
            "isVisible": true,
            "left": "20px",
            "skin": "slLabel0d4f692dab05249",
            "i18n_text": "kony.i18n.getLocalizedString(\"i18n.permission.Optional\")",
            "top": "10px",
            "width": "93px",
            "zIndex": 1
        }, controller.args[0], "lblOptional4"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblOptional4"), extendConfig({}, controller.args[2], "lblOptional4"));
        var flxAccountsLeftMenu = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
            "clipBounds": true,
            "id": "flxAccountsLeftMenu",
            "isVisible": false,
            "layoutType": kony.flex.FREE_FORM,
            "left": "0dp",
            "isModalContainer": false,
            "skin": "slFbox",
            "top": "0dp",
            "width": "100%"
        }, controller.args[0], "flxAccountsLeftMenu"), extendConfig({}, controller.args[1], "flxAccountsLeftMenu"), extendConfig({}, controller.args[2], "flxAccountsLeftMenu"));
        flxAccountsLeftMenu.setDefaultUnit(kony.flex.DP);
        var segCompanyAccounts = new kony.ui.SegmentedUI2(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
            "data": [{
                "lblAccountNumber": "0989865887",
                "lblAccountType": "Salary Account",
                "lblArrow": "kony.i18n.getLocalizedString(\"i18n.frmPermissions.fontIconRightArrow1\")",
                "lblEntitlementsSeperator": "."
            }, {
                "lblAccountNumber": "0989865887",
                "lblAccountType": "Salary Account",
                "lblArrow": "kony.i18n.getLocalizedString(\"i18n.frmPermissions.fontIconRightArrow1\")",
                "lblEntitlementsSeperator": "."
            }, {
                "lblAccountNumber": "0989865887",
                "lblAccountType": "Salary Account",
                "lblArrow": "kony.i18n.getLocalizedString(\"i18n.frmPermissions.fontIconRightArrow1\")",
                "lblEntitlementsSeperator": "."
            }, {
                "lblAccountNumber": "0989865887",
                "lblAccountType": "Salary Account",
                "lblArrow": "kony.i18n.getLocalizedString(\"i18n.frmPermissions.fontIconRightArrow1\")",
                "lblEntitlementsSeperator": "."
            }, {
                "lblAccountNumber": "0989865887",
                "lblAccountType": "Salary Account",
                "lblArrow": "kony.i18n.getLocalizedString(\"i18n.frmPermissions.fontIconRightArrow1\")",
                "lblEntitlementsSeperator": "."
            }, {
                "lblAccountNumber": "0989865887",
                "lblAccountType": "Salary Account",
                "lblArrow": "kony.i18n.getLocalizedString(\"i18n.frmPermissions.fontIconRightArrow1\")",
                "lblEntitlementsSeperator": "."
            }, {
                "lblAccountNumber": "0989865887",
                "lblAccountType": "Salary Account",
                "lblArrow": "kony.i18n.getLocalizedString(\"i18n.frmPermissions.fontIconRightArrow1\")",
                "lblEntitlementsSeperator": "."
            }, {
                "lblAccountNumber": "0989865887",
                "lblAccountType": "Salary Account",
                "lblArrow": "kony.i18n.getLocalizedString(\"i18n.frmPermissions.fontIconRightArrow1\")",
                "lblEntitlementsSeperator": "."
            }, {
                "lblAccountNumber": "0989865887",
                "lblAccountType": "Salary Account",
                "lblArrow": "kony.i18n.getLocalizedString(\"i18n.frmPermissions.fontIconRightArrow1\")",
                "lblEntitlementsSeperator": "."
            }, {
                "lblAccountNumber": "0989865887",
                "lblAccountType": "Salary Account",
                "lblArrow": "kony.i18n.getLocalizedString(\"i18n.frmPermissions.fontIconRightArrow1\")",
                "lblEntitlementsSeperator": "."
            }],
            "groupCells": false,
            "id": "segCompanyAccounts",
            "isVisible": true,
            "left": "15dp",
            "needPageIndicator": true,
            "pageOffDotImage": "pageoffdot.png",
            "pageOnDotImage": "pageondot.png",
            "retainSelection": false,
            "right": "15dp",
            "rowFocusSkin": "seg2Focus",
            "rowSkin": "seg2Normal",
            "rowTemplate": "flxCompanyAccounts",
            "sectionHeaderSkin": "sliPhoneSegmentHeader",
            "selectionBehavior": constants.SEGUI_DEFAULT_BEHAVIOR,
            "separatorRequired": false,
            "showScrollbars": false,
            "top": "0",
            "viewType": constants.SEGUI_VIEW_TYPE_TABLEVIEW,
            "widgetDataMap": {
                "flxAccountDetail": "flxAccountDetail",
                "flxArrow": "flxArrow",
                "flxCompanyAccounts": "flxCompanyAccounts",
                "flxContent": "flxContent",
                "lblAccountNumber": "lblAccountNumber",
                "lblAccountType": "lblAccountType",
                "lblArrow": "lblArrow",
                "lblEntitlementsSeperator": "lblEntitlementsSeperator"
            },
            "width": kony.flex.USE_PREFFERED_SIZE
        }, controller.args[0], "segCompanyAccounts"), extendConfig({
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "segCompanyAccounts"), extendConfig({}, controller.args[2], "segCompanyAccounts"));
        flxAccountsLeftMenu.add(segCompanyAccounts);
        var flxSepratorContainer = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "clipBounds": true,
            "height": "1dp",
            "id": "flxSepratorContainer",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "left": "0dp",
            "isModalContainer": false,
            "skin": "slFbox",
            "top": "15px",
            "width": "100%"
        }, controller.args[0], "flxSepratorContainer"), extendConfig({}, controller.args[1], "flxSepratorContainer"), extendConfig({}, controller.args[2], "flxSepratorContainer"));
        flxSepratorContainer.setDefaultUnit(kony.flex.DP);
        var flxAddSeperator4 = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "bottom": 0,
            "clipBounds": true,
            "height": "1px",
            "id": "flxAddSeperator4",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "left": "20px",
            "isModalContainer": false,
            "right": "20px",
            "skin": "sknflxe1e5edop100",
            "top": "0dp",
            "zIndex": 1
        }, controller.args[0], "flxAddSeperator4"), extendConfig({}, controller.args[1], "flxAddSeperator4"), extendConfig({}, controller.args[2], "flxAddSeperator4"));
        flxAddSeperator4.setDefaultUnit(kony.flex.DP);
        flxAddSeperator4.add();
        flxSepratorContainer.add(flxAddSeperator4);
        flxOption4.add(flxContentContainer4, lblOptional4, flxAccountsLeftMenu, flxSepratorContainer);
        var flxOption5 = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
            "clipBounds": true,
            "id": "flxOption5",
            "isVisible": false,
            "layoutType": kony.flex.FLOW_VERTICAL,
            "left": "0dp",
            "isModalContainer": false,
            "skin": "slFbox",
            "top": "0dp",
            "width": "100%",
            "zIndex": 1
        }, controller.args[0], "flxOption5"), extendConfig({}, controller.args[1], "flxOption5"), extendConfig({}, controller.args[2], "flxOption5"));
        flxOption5.setDefaultUnit(kony.flex.DP);
        var flxContentContainer5 = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
            "clipBounds": true,
            "id": "flxContentContainer5",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "left": "0dp",
            "isModalContainer": false,
            "skin": "slFbox",
            "top": "15px",
            "width": "100%"
        }, controller.args[0], "flxContentContainer5"), extendConfig({}, controller.args[1], "flxContentContainer5"), extendConfig({}, controller.args[2], "flxContentContainer5"));
        flxContentContainer5.setDefaultUnit(kony.flex.DP);
        var btnOption5 = new kony.ui.Button(extendConfig({
            "bottom": "0dp",
            "id": "btnOption5",
            "isVisible": true,
            "left": "20dp",
            "skin": "Btna1acbafont12pxKA",
            "text": "OPTION5",
            "top": "0dp",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, controller.args[0], "btnOption5"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_TOP_LEFT,
            "displayText": true,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "btnOption5"), extendConfig({
            "hoverSkin": "Btn000000font14px"
        }, controller.args[2], "btnOption5"));
        var flxImgArrow5 = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "bottom": "0dp",
            "clipBounds": true,
            "height": "15px",
            "id": "flxImgArrow5",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "isModalContainer": false,
            "right": "20px",
            "skin": "slFbox",
            "top": "0px",
            "width": "15px",
            "zIndex": 1
        }, controller.args[0], "flxImgArrow5"), extendConfig({}, controller.args[1], "flxImgArrow5"), extendConfig({}, controller.args[2], "flxImgArrow5"));
        flxImgArrow5.setDefaultUnit(kony.flex.DP);
        var imgSelected5 = new kony.ui.Image2(extendConfig({
            "height": "100%",
            "id": "imgSelected5",
            "isVisible": false,
            "right": 0,
            "skin": "slImage",
            "src": "right_arrow2x.png",
            "top": "0dp",
            "width": "100%",
            "zIndex": 1
        }, controller.args[0], "imgSelected5"), extendConfig({
            "imageScaleMode": constants.IMAGE_SCALE_MODE_MAINTAIN_ASPECT_RATIO,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "imgSelected5"), extendConfig({}, controller.args[2], "imgSelected5"));
        var lblSelected5 = new kony.ui.Label(extendConfig({
            "centerX": "50%",
            "centerY": "50%",
            "height": "100%",
            "id": "lblSelected5",
            "isVisible": true,
            "skin": "sknicon15pxBlack",
            "i18n_text": "kony.i18n.getLocalizedString(\"i18n.userwidgetmodel.fontIconSelected1\")",
            "width": "100%",
            "zIndex": 1
        }, controller.args[0], "lblSelected5"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblSelected5"), extendConfig({}, controller.args[2], "lblSelected5"));
        flxImgArrow5.add(imgSelected5, lblSelected5);
        flxContentContainer5.add(btnOption5, flxImgArrow5);
        var lblOptional5 = new kony.ui.Label(extendConfig({
            "id": "lblOptional5",
            "isVisible": true,
            "left": "20px",
            "skin": "slLabel0d4f692dab05249",
            "i18n_text": "kony.i18n.getLocalizedString(\"i18n.permission.Optional\")",
            "top": "10px",
            "width": "93px",
            "zIndex": 1
        }, controller.args[0], "lblOptional5"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblOptional5"), extendConfig({}, controller.args[2], "lblOptional5"));
        var flxSepratorContainer5 = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "clipBounds": true,
            "height": "1dp",
            "id": "flxSepratorContainer5",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "left": "0dp",
            "isModalContainer": false,
            "skin": "slFbox",
            "top": "15px",
            "width": "100%"
        }, controller.args[0], "flxSepratorContainer5"), extendConfig({}, controller.args[1], "flxSepratorContainer5"), extendConfig({}, controller.args[2], "flxSepratorContainer5"));
        flxSepratorContainer5.setDefaultUnit(kony.flex.DP);
        var flxAddSeperator5 = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "bottom": 0,
            "clipBounds": true,
            "height": "1px",
            "id": "flxAddSeperator5",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "left": "20px",
            "isModalContainer": false,
            "right": "20px",
            "skin": "sknflxe1e5edop100",
            "top": "0dp",
            "zIndex": 1
        }, controller.args[0], "flxAddSeperator5"), extendConfig({}, controller.args[1], "flxAddSeperator5"), extendConfig({}, controller.args[2], "flxAddSeperator5"));
        flxAddSeperator5.setDefaultUnit(kony.flex.DP);
        flxAddSeperator5.add();
        flxSepratorContainer5.add(flxAddSeperator5);
        flxOption5.add(flxContentContainer5, lblOptional5, flxSepratorContainer5);
        var flxOption6 = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
            "clipBounds": true,
            "id": "flxOption6",
            "isVisible": false,
            "layoutType": kony.flex.FLOW_VERTICAL,
            "left": "0dp",
            "isModalContainer": false,
            "skin": "slFbox",
            "top": "0dp",
            "width": "100%",
            "zIndex": 1
        }, controller.args[0], "flxOption6"), extendConfig({}, controller.args[1], "flxOption6"), extendConfig({}, controller.args[2], "flxOption6"));
        flxOption6.setDefaultUnit(kony.flex.DP);
        var flxContentContainer6 = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
            "clipBounds": true,
            "id": "flxContentContainer6",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "left": "0dp",
            "isModalContainer": false,
            "skin": "slFbox",
            "top": "15px",
            "width": "100%"
        }, controller.args[0], "flxContentContainer6"), extendConfig({}, controller.args[1], "flxContentContainer6"), extendConfig({}, controller.args[2], "flxContentContainer6"));
        flxContentContainer6.setDefaultUnit(kony.flex.DP);
        var btnOption6 = new kony.ui.Button(extendConfig({
            "bottom": "0dp",
            "id": "btnOption6",
            "isVisible": true,
            "left": "20dp",
            "skin": "Btna1acbafont12pxKA",
            "text": "OPTION6",
            "top": "0dp",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, controller.args[0], "btnOption6"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_TOP_LEFT,
            "displayText": true,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "btnOption6"), extendConfig({
            "hoverSkin": "Btn000000font14px"
        }, controller.args[2], "btnOption6"));
        var flxImgArrow6 = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "bottom": "0dp",
            "clipBounds": true,
            "height": "15px",
            "id": "flxImgArrow6",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "isModalContainer": false,
            "right": "20px",
            "skin": "slFbox",
            "top": "0px",
            "width": "15px",
            "zIndex": 1
        }, controller.args[0], "flxImgArrow6"), extendConfig({}, controller.args[1], "flxImgArrow6"), extendConfig({}, controller.args[2], "flxImgArrow6"));
        flxImgArrow6.setDefaultUnit(kony.flex.DP);
        var imgSelected6 = new kony.ui.Image2(extendConfig({
            "height": "100%",
            "id": "imgSelected6",
            "isVisible": false,
            "right": 0,
            "skin": "slImage",
            "src": "right_arrow2x.png",
            "top": "0dp",
            "width": "100%",
            "zIndex": 1
        }, controller.args[0], "imgSelected6"), extendConfig({
            "imageScaleMode": constants.IMAGE_SCALE_MODE_MAINTAIN_ASPECT_RATIO,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "imgSelected6"), extendConfig({}, controller.args[2], "imgSelected6"));
        var lblSelected6 = new kony.ui.Label(extendConfig({
            "centerX": "50%",
            "centerY": "50%",
            "height": "100%",
            "id": "lblSelected6",
            "isVisible": true,
            "skin": "sknicon15pxBlack",
            "i18n_text": "kony.i18n.getLocalizedString(\"i18n.userwidgetmodel.fontIconSelected1\")",
            "width": "100%",
            "zIndex": 1
        }, controller.args[0], "lblSelected6"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblSelected6"), extendConfig({}, controller.args[2], "lblSelected6"));
        flxImgArrow6.add(imgSelected6, lblSelected6);
        var lblIconDropArrow6 = new kony.ui.Label(extendConfig({
            "id": "lblIconDropArrow6",
            "isVisible": false,
            "left": "20dp",
            "skin": "sknIcon485C7513px",
            "text": "",
            "top": "0px",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, controller.args[0], "lblIconDropArrow6"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_TOP_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblIconDropArrow6"), extendConfig({}, controller.args[2], "lblIconDropArrow6"));
        flxContentContainer6.add(btnOption6, flxImgArrow6, lblIconDropArrow6);
        var lblOptional6 = new kony.ui.Label(extendConfig({
            "id": "lblOptional6",
            "isVisible": true,
            "left": "20px",
            "skin": "slLabel0d4f692dab05249",
            "i18n_text": "kony.i18n.getLocalizedString(\"i18n.permission.Optional\")",
            "top": "10px",
            "width": "93px",
            "zIndex": 1
        }, controller.args[0], "lblOptional6"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblOptional6"), extendConfig({}, controller.args[2], "lblOptional6"));
        var flxAccountsLeftMenu6 = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
            "clipBounds": true,
            "id": "flxAccountsLeftMenu6",
            "isVisible": false,
            "layoutType": kony.flex.FREE_FORM,
            "left": "0dp",
            "isModalContainer": false,
            "skin": "slFbox",
            "top": "0dp",
            "width": "100%"
        }, controller.args[0], "flxAccountsLeftMenu6"), extendConfig({}, controller.args[1], "flxAccountsLeftMenu6"), extendConfig({}, controller.args[2], "flxAccountsLeftMenu6"));
        flxAccountsLeftMenu6.setDefaultUnit(kony.flex.DP);
        var segCompanyAccount6 = new kony.ui.SegmentedUI2(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
            "data": [{
                "lblAccountNumber": "0989865887",
                "lblAccountType": "Salary Account",
                "lblArrow": "kony.i18n.getLocalizedString(\"i18n.frmPermissions.fontIconRightArrow1\")",
                "lblEntitlementsSeperator": "."
            }, {
                "lblAccountNumber": "0989865887",
                "lblAccountType": "Salary Account",
                "lblArrow": "kony.i18n.getLocalizedString(\"i18n.frmPermissions.fontIconRightArrow1\")",
                "lblEntitlementsSeperator": "."
            }, {
                "lblAccountNumber": "0989865887",
                "lblAccountType": "Salary Account",
                "lblArrow": "kony.i18n.getLocalizedString(\"i18n.frmPermissions.fontIconRightArrow1\")",
                "lblEntitlementsSeperator": "."
            }, {
                "lblAccountNumber": "0989865887",
                "lblAccountType": "Salary Account",
                "lblArrow": "kony.i18n.getLocalizedString(\"i18n.frmPermissions.fontIconRightArrow1\")",
                "lblEntitlementsSeperator": "."
            }, {
                "lblAccountNumber": "0989865887",
                "lblAccountType": "Salary Account",
                "lblArrow": "kony.i18n.getLocalizedString(\"i18n.frmPermissions.fontIconRightArrow1\")",
                "lblEntitlementsSeperator": "."
            }, {
                "lblAccountNumber": "0989865887",
                "lblAccountType": "Salary Account",
                "lblArrow": "kony.i18n.getLocalizedString(\"i18n.frmPermissions.fontIconRightArrow1\")",
                "lblEntitlementsSeperator": "."
            }, {
                "lblAccountNumber": "0989865887",
                "lblAccountType": "Salary Account",
                "lblArrow": "kony.i18n.getLocalizedString(\"i18n.frmPermissions.fontIconRightArrow1\")",
                "lblEntitlementsSeperator": "."
            }, {
                "lblAccountNumber": "0989865887",
                "lblAccountType": "Salary Account",
                "lblArrow": "kony.i18n.getLocalizedString(\"i18n.frmPermissions.fontIconRightArrow1\")",
                "lblEntitlementsSeperator": "."
            }, {
                "lblAccountNumber": "0989865887",
                "lblAccountType": "Salary Account",
                "lblArrow": "kony.i18n.getLocalizedString(\"i18n.frmPermissions.fontIconRightArrow1\")",
                "lblEntitlementsSeperator": "."
            }, {
                "lblAccountNumber": "0989865887",
                "lblAccountType": "Salary Account",
                "lblArrow": "kony.i18n.getLocalizedString(\"i18n.frmPermissions.fontIconRightArrow1\")",
                "lblEntitlementsSeperator": "."
            }],
            "groupCells": false,
            "id": "segCompanyAccount6",
            "isVisible": true,
            "left": "15dp",
            "needPageIndicator": true,
            "pageOffDotImage": "pageoffdot.png",
            "pageOnDotImage": "pageondot.png",
            "retainSelection": false,
            "right": "15dp",
            "rowFocusSkin": "seg2Focus",
            "rowSkin": "seg2Normal",
            "rowTemplate": "flxCompanyAccounts",
            "sectionHeaderSkin": "sliPhoneSegmentHeader",
            "selectionBehavior": constants.SEGUI_DEFAULT_BEHAVIOR,
            "separatorRequired": false,
            "showScrollbars": false,
            "top": "0",
            "viewType": constants.SEGUI_VIEW_TYPE_TABLEVIEW,
            "widgetDataMap": {
                "flxAccountDetail": "flxAccountDetail",
                "flxArrow": "flxArrow",
                "flxCompanyAccounts": "flxCompanyAccounts",
                "flxContent": "flxContent",
                "lblAccountNumber": "lblAccountNumber",
                "lblAccountType": "lblAccountType",
                "lblArrow": "lblArrow",
                "lblEntitlementsSeperator": "lblEntitlementsSeperator"
            },
            "width": kony.flex.USE_PREFFERED_SIZE
        }, controller.args[0], "segCompanyAccount6"), extendConfig({
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "segCompanyAccount6"), extendConfig({}, controller.args[2], "segCompanyAccount6"));
        flxAccountsLeftMenu6.add(segCompanyAccount6);
        var flxSepratorContainer6 = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "clipBounds": true,
            "height": "1dp",
            "id": "flxSepratorContainer6",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "left": "0dp",
            "isModalContainer": false,
            "skin": "slFbox",
            "top": "15px",
            "width": "100%"
        }, controller.args[0], "flxSepratorContainer6"), extendConfig({}, controller.args[1], "flxSepratorContainer6"), extendConfig({}, controller.args[2], "flxSepratorContainer6"));
        flxSepratorContainer6.setDefaultUnit(kony.flex.DP);
        var flxAddSeperator6 = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "bottom": 0,
            "clipBounds": true,
            "height": "1px",
            "id": "flxAddSeperator6",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "left": "20px",
            "isModalContainer": false,
            "right": "20px",
            "skin": "sknflxe1e5edop100",
            "top": "0dp",
            "zIndex": 1
        }, controller.args[0], "flxAddSeperator6"), extendConfig({}, controller.args[1], "flxAddSeperator6"), extendConfig({}, controller.args[2], "flxAddSeperator6"));
        flxAddSeperator6.setDefaultUnit(kony.flex.DP);
        flxAddSeperator6.add();
        flxSepratorContainer6.add(flxAddSeperator6);
        flxOption6.add(flxContentContainer6, lblOptional6, flxAccountsLeftMenu6, flxSepratorContainer6);
        var flxBottomPadding = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "bottom": "0dp",
            "clipBounds": true,
            "height": "20px",
            "id": "flxBottomPadding",
            "isVisible": true,
            "layoutType": kony.flex.FLOW_VERTICAL,
            "left": "0dp",
            "isModalContainer": false,
            "skin": "slFbox",
            "width": "100%",
            "zIndex": 1
        }, controller.args[0], "flxBottomPadding"), extendConfig({}, controller.args[1], "flxBottomPadding"), extendConfig({}, controller.args[2], "flxBottomPadding"));
        flxBottomPadding.setDefaultUnit(kony.flex.DP);
        flxBottomPadding.add();
        verticalTabs1.add(flxOption0, flxOption1, flxOption2, flxOption3, flxOption4, flxOption5, flxOption6, flxBottomPadding);
        return verticalTabs1;
    }
})