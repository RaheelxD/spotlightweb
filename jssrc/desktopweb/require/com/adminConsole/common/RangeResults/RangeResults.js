define(function() {
    return function(controller) {
        var RangeResults = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
            "clipBounds": true,
            "isMaster": true,
            "id": "RangeResults",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "left": "0dp",
            "isModalContainer": false,
            "skin": "sknflxffffffop100Bordercbcdd1Radius3px",
            "top": "0dp",
            "width": "100%"
        }, controller.args[0], "RangeResults"), extendConfig({}, controller.args[1], "RangeResults"), extendConfig({}, controller.args[2], "RangeResults"));
        RangeResults.setDefaultUnit(kony.flex.DP);
        var flxDetailsHeader = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "clipBounds": true,
            "height": "60dp",
            "id": "flxDetailsHeader",
            "isVisible": true,
            "layoutType": kony.flex.FLOW_HORIZONTAL,
            "left": "0px",
            "isModalContainer": false,
            "skin": "slFbox",
            "top": "0dp",
            "width": "100%",
            "zIndex": 1
        }, controller.args[0], "flxDetailsHeader"), extendConfig({}, controller.args[1], "flxDetailsHeader"), extendConfig({}, controller.args[2], "flxDetailsHeader"));
        flxDetailsHeader.setDefaultUnit(kony.flex.DP);
        var lblJobHistory = new kony.ui.Label(extendConfig({
            "centerY": "50%",
            "id": "lblJobHistory",
            "isVisible": true,
            "left": "24dp",
            "skin": "sknlblLato5d6c7f12px",
            "text": "Job History",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, controller.args[0], "lblJobHistory"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblJobHistory"), extendConfig({}, controller.args[2], "lblJobHistory"));
        var flxCalender = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "centerY": "50%",
            "clipBounds": true,
            "height": "40dp",
            "id": "flxCalender",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "left": "15dp",
            "isModalContainer": false,
            "skin": "sknflxffffffop100Bordercbcdd1Radius3px",
            "width": "200dp",
            "zIndex": 1
        }, controller.args[0], "flxCalender"), extendConfig({}, controller.args[1], "flxCalender"), extendConfig({}, controller.args[2], "flxCalender"));
        flxCalender.setDefaultUnit(kony.flex.DP);
        var flxCalenderIcon = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "clipBounds": true,
            "height": "40dp",
            "id": "flxCalenderIcon",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "isModalContainer": false,
            "right": "0dp",
            "skin": "slFbox",
            "top": "0dp",
            "width": "40dp",
            "zIndex": 1
        }, controller.args[0], "flxCalenderIcon"), extendConfig({}, controller.args[1], "flxCalenderIcon"), extendConfig({}, controller.args[2], "flxCalenderIcon"));
        flxCalenderIcon.setDefaultUnit(kony.flex.DP);
        flxCalenderIcon.add();
        flxCalender.add(flxCalenderIcon);
        flxDetailsHeader.add(lblJobHistory, flxCalender);
        var flxSeprator = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "clipBounds": true,
            "height": "1dp",
            "id": "flxSeprator",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "left": "20dp",
            "isModalContainer": false,
            "right": "20dp",
            "skin": "sknflxffffffop100Bordercbcdd1Radius3px",
            "top": "60dp",
            "zIndex": 2
        }, controller.args[0], "flxSeprator"), extendConfig({}, controller.args[1], "flxSeprator"), extendConfig({}, controller.args[2], "flxSeprator"));
        flxSeprator.setDefaultUnit(kony.flex.DP);
        flxSeprator.add();
        var flxDetailSegmentHeader = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "clipBounds": true,
            "height": "60dp",
            "id": "flxDetailSegmentHeader",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "left": "0px",
            "isModalContainer": false,
            "skin": "slFbox",
            "top": "60dp",
            "width": "100%",
            "zIndex": 1
        }, controller.args[0], "flxDetailSegmentHeader"), extendConfig({}, controller.args[1], "flxDetailSegmentHeader"), extendConfig({}, controller.args[2], "flxDetailSegmentHeader"));
        flxDetailSegmentHeader.setDefaultUnit(kony.flex.DP);
        var flxDetailSegmentHeaderInner = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "clipBounds": true,
            "height": "100%",
            "id": "flxDetailSegmentHeaderInner",
            "isVisible": true,
            "layoutType": kony.flex.FLOW_HORIZONTAL,
            "left": "20dp",
            "isModalContainer": false,
            "right": "20dp",
            "skin": "slFbox",
            "top": "0dp"
        }, controller.args[0], "flxDetailSegmentHeaderInner"), extendConfig({}, controller.args[1], "flxDetailSegmentHeaderInner"), extendConfig({}, controller.args[2], "flxDetailSegmentHeaderInner"));
        flxDetailSegmentHeaderInner.setDefaultUnit(kony.flex.DP);
        var flxHeaderTitle1 = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "centerY": "50%",
            "clipBounds": true,
            "height": "100%",
            "id": "flxHeaderTitle1",
            "isVisible": true,
            "layoutType": kony.flex.FLOW_HORIZONTAL,
            "left": "0px",
            "isModalContainer": false,
            "onClick": controller.AS_FlexContainer_fbe01558a0a64e0ca4625eeff7f71077,
            "skin": "slFbox",
            "top": 0,
            "width": "25%",
            "zIndex": 1
        }, controller.args[0], "flxHeaderTitle1"), extendConfig({}, controller.args[1], "flxHeaderTitle1"), extendConfig({}, controller.args[2], "flxHeaderTitle1"));
        flxHeaderTitle1.setDefaultUnit(kony.flex.DP);
        var lblNameHeaderTitle1 = new kony.ui.Label(extendConfig({
            "centerY": "50%",
            "id": "lblNameHeaderTitle1",
            "isVisible": true,
            "left": "0px",
            "skin": "sknlblLato696c7312px",
            "text": "HEADER1",
            "top": 0,
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, controller.args[0], "lblNameHeaderTitle1"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblNameHeaderTitle1"), extendConfig({
            "hoverSkin": "sknlblLatoBold00000012px"
        }, controller.args[2], "lblNameHeaderTitle1"));
        var lblIconHedaerTitle1 = new kony.ui.Label(extendConfig({
            "centerY": "50%",
            "id": "lblIconHedaerTitle1",
            "isVisible": true,
            "left": "10dp",
            "skin": "sknIcon15px",
            "text": "",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, controller.args[0], "lblIconHedaerTitle1"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblIconHedaerTitle1"), extendConfig({
            "hoverSkin": "sknlblCursorFont"
        }, controller.args[2], "lblIconHedaerTitle1"));
        flxHeaderTitle1.add(lblNameHeaderTitle1, lblIconHedaerTitle1);
        var flxHeaderTitle2 = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "centerY": "50%",
            "clipBounds": true,
            "height": "100%",
            "id": "flxHeaderTitle2",
            "isVisible": true,
            "layoutType": kony.flex.FLOW_HORIZONTAL,
            "left": "0%",
            "isModalContainer": false,
            "skin": "slFbox",
            "top": "0dp",
            "width": "15%",
            "zIndex": 1
        }, controller.args[0], "flxHeaderTitle2"), extendConfig({}, controller.args[1], "flxHeaderTitle2"), extendConfig({}, controller.args[2], "flxHeaderTitle2"));
        flxHeaderTitle2.setDefaultUnit(kony.flex.DP);
        var lblNameHeaderTitle2 = new kony.ui.Label(extendConfig({
            "centerY": "50%",
            "id": "lblNameHeaderTitle2",
            "isVisible": true,
            "left": "0dp",
            "skin": "sknlblLato696c7312px",
            "text": "HEADER2",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, controller.args[0], "lblNameHeaderTitle2"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblNameHeaderTitle2"), extendConfig({}, controller.args[2], "lblNameHeaderTitle2"));
        var lblIconHedaerTitle2 = new kony.ui.Label(extendConfig({
            "centerY": "50%",
            "id": "lblIconHedaerTitle2",
            "isVisible": true,
            "left": "10dp",
            "skin": "sknIcon15px",
            "text": "",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, controller.args[0], "lblIconHedaerTitle2"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblIconHedaerTitle2"), extendConfig({
            "hoverSkin": "sknlblCursorFont"
        }, controller.args[2], "lblIconHedaerTitle2"));
        flxHeaderTitle2.add(lblNameHeaderTitle2, lblIconHedaerTitle2);
        var flxHeaderTitle3 = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "centerY": "50%",
            "clipBounds": true,
            "height": "100%",
            "id": "flxHeaderTitle3",
            "isVisible": true,
            "layoutType": kony.flex.FLOW_HORIZONTAL,
            "left": "0%",
            "isModalContainer": false,
            "onClick": controller.AS_FlexContainer_bed9873cfdeb4311ba234401c30421f2,
            "skin": "slFbox",
            "top": "0dp",
            "width": "15%",
            "zIndex": 1
        }, controller.args[0], "flxHeaderTitle3"), extendConfig({}, controller.args[1], "flxHeaderTitle3"), extendConfig({}, controller.args[2], "flxHeaderTitle3"));
        flxHeaderTitle3.setDefaultUnit(kony.flex.DP);
        var lblNameHeaderTitle3 = new kony.ui.Label(extendConfig({
            "centerY": "50%",
            "id": "lblNameHeaderTitle3",
            "isVisible": true,
            "left": "0px",
            "skin": "sknlblLato696c7312px",
            "text": "HEADER3",
            "top": 0,
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, controller.args[0], "lblNameHeaderTitle3"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblNameHeaderTitle3"), extendConfig({
            "hoverSkin": "sknlblLatoBold00000012px"
        }, controller.args[2], "lblNameHeaderTitle3"));
        var lblIconHedaerTitle3 = new kony.ui.Label(extendConfig({
            "centerY": "50%",
            "id": "lblIconHedaerTitle3",
            "isVisible": true,
            "left": "10dp",
            "skin": "sknIcon15px",
            "text": "",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, controller.args[0], "lblIconHedaerTitle3"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblIconHedaerTitle3"), extendConfig({
            "hoverSkin": "sknlblCursorFont"
        }, controller.args[2], "lblIconHedaerTitle3"));
        flxHeaderTitle3.add(lblNameHeaderTitle3, lblIconHedaerTitle3);
        var flxHeaderTitle4 = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "centerY": "50%",
            "clipBounds": true,
            "height": "100%",
            "id": "flxHeaderTitle4",
            "isVisible": true,
            "layoutType": kony.flex.FLOW_HORIZONTAL,
            "left": "0%",
            "isModalContainer": false,
            "onClick": controller.AS_FlexContainer_d0c82a1545084c569df06fa1ca405fa6,
            "skin": "slFbox",
            "top": "0dp",
            "width": "15%",
            "zIndex": 1
        }, controller.args[0], "flxHeaderTitle4"), extendConfig({}, controller.args[1], "flxHeaderTitle4"), extendConfig({}, controller.args[2], "flxHeaderTitle4"));
        flxHeaderTitle4.setDefaultUnit(kony.flex.DP);
        var lblNameHeaderTitle4 = new kony.ui.Label(extendConfig({
            "centerY": "50%",
            "id": "lblNameHeaderTitle4",
            "isVisible": true,
            "left": "0px",
            "skin": "sknlblLato696c7312px",
            "text": "HEADER4",
            "top": 0,
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, controller.args[0], "lblNameHeaderTitle4"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblNameHeaderTitle4"), extendConfig({
            "hoverSkin": "sknlblLatoBold00000012px"
        }, controller.args[2], "lblNameHeaderTitle4"));
        var lblIconHedaerTitle4 = new kony.ui.Label(extendConfig({
            "centerY": "50%",
            "id": "lblIconHedaerTitle4",
            "isVisible": true,
            "left": "10dp",
            "skin": "sknIcon15px",
            "text": "",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, controller.args[0], "lblIconHedaerTitle4"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblIconHedaerTitle4"), extendConfig({
            "hoverSkin": "sknlblCursorFont"
        }, controller.args[2], "lblIconHedaerTitle4"));
        flxHeaderTitle4.add(lblNameHeaderTitle4, lblIconHedaerTitle4);
        var flxHeaderTitle5 = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "centerY": "50%",
            "clipBounds": true,
            "height": "100%",
            "id": "flxHeaderTitle5",
            "isVisible": true,
            "layoutType": kony.flex.FLOW_HORIZONTAL,
            "left": "0dp",
            "isModalContainer": false,
            "onClick": controller.AS_FlexContainer_c62ddb6717304b7199d5a79964e58736,
            "skin": "slFbox",
            "top": "0dp",
            "width": "15%",
            "zIndex": 1
        }, controller.args[0], "flxHeaderTitle5"), extendConfig({}, controller.args[1], "flxHeaderTitle5"), extendConfig({}, controller.args[2], "flxHeaderTitle5"));
        flxHeaderTitle5.setDefaultUnit(kony.flex.DP);
        var lblNameHeaderTitle5 = new kony.ui.Label(extendConfig({
            "centerY": "50%",
            "id": "lblNameHeaderTitle5",
            "isVisible": true,
            "left": "0px",
            "skin": "sknlblLato696c7312px",
            "text": "HEADER5",
            "top": 0,
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, controller.args[0], "lblNameHeaderTitle5"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblNameHeaderTitle5"), extendConfig({
            "hoverSkin": "sknlblLatoBold00000012px"
        }, controller.args[2], "lblNameHeaderTitle5"));
        var lblIconHedaerTitle5 = new kony.ui.Label(extendConfig({
            "centerY": "50%",
            "id": "lblIconHedaerTitle5",
            "isVisible": true,
            "left": "10dp",
            "skin": "sknIcon15px",
            "text": "",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, controller.args[0], "lblIconHedaerTitle5"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblIconHedaerTitle5"), extendConfig({
            "hoverSkin": "sknlblCursorFont"
        }, controller.args[2], "lblIconHedaerTitle5"));
        flxHeaderTitle5.add(lblNameHeaderTitle5, lblIconHedaerTitle5);
        flxDetailSegmentHeaderInner.add(flxHeaderTitle1, flxHeaderTitle2, flxHeaderTitle3, flxHeaderTitle4, flxHeaderTitle5);
        var lblHeaderSeperator = new kony.ui.Label(extendConfig({
            "height": "1px",
            "id": "lblHeaderSeperator",
            "isVisible": true,
            "left": "20px",
            "right": "20px",
            "skin": "sknLblTableHeaderLine",
            "text": "-",
            "top": "59dp",
            "zIndex": 2
        }, controller.args[0], "lblHeaderSeperator"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblHeaderSeperator"), extendConfig({}, controller.args[2], "lblHeaderSeperator"));
        flxDetailSegmentHeader.add(flxDetailSegmentHeaderInner, lblHeaderSeperator);
        var segDetailResults = new kony.ui.SegmentedUI2(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
            "bottom": "15dp",
            "groupCells": false,
            "id": "segDetailResults",
            "isVisible": true,
            "left": "0dp",
            "needPageIndicator": true,
            "pageOffDotImage": "pageoffdot.png",
            "pageOnDotImage": "pageondot.png",
            "retainSelection": false,
            "rowFocusSkin": "seg2Focus",
            "rowSkin": "seg2Normal",
            "sectionHeaderSkin": "sliPhoneSegmentHeader",
            "selectionBehavior": constants.SEGUI_DEFAULT_BEHAVIOR,
            "separatorColor": "aaaaaa00",
            "separatorRequired": true,
            "separatorThickness": 1,
            "showScrollbars": false,
            "top": "120dp",
            "viewType": constants.SEGUI_VIEW_TYPE_TABLEVIEW,
            "widgetDataMap": {},
            "width": "100%",
            "zIndex": 1
        }, controller.args[0], "segDetailResults"), extendConfig({
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "segDetailResults"), extendConfig({}, controller.args[2], "segDetailResults"));
        RangeResults.add(flxDetailsHeader, flxSeprator, flxDetailSegmentHeader, segDetailResults);
        return RangeResults;
    }
})