define("com/adminConsole/common/RangeResults/userRangeResultsController", function() {
    return {};
});
define("com/adminConsole/common/RangeResults/RangeResultsControllerActions", {
    /*
        This is an auto generated file and any modifications to it may result in corruption of the action sequence.
    */
});
define("com/adminConsole/common/RangeResults/RangeResultsController", ["com/adminConsole/common/RangeResults/userRangeResultsController", "com/adminConsole/common/RangeResults/RangeResultsControllerActions"], function() {
    var controller = require("com/adminConsole/common/RangeResults/userRangeResultsController");
    var actions = require("com/adminConsole/common/RangeResults/RangeResultsControllerActions");
    for (var key in actions) {
        controller[key] = actions[key];
    }
    return controller;
});
