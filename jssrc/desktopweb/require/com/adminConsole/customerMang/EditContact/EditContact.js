define(function() {
    return function(controller) {
        var EditContact = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
            "clipBounds": true,
            "isMaster": true,
            "id": "EditContact",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "isModalContainer": false,
            "preShow": function(eventobject) {
                controller.AS_FlexContainer_he82ddc60f0a44c5b91d4e0e1d5482e9(eventobject);
            },
            "skin": "slFbox0hfd18814fd664dCM",
            "width": "100%"
        }, controller.args[0], "EditContact"), extendConfig({}, controller.args[1], "EditContact"), extendConfig({}, controller.args[2], "EditContact"));
        EditContact.setDefaultUnit(kony.flex.DP);
        var flxViewContact = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
            "clipBounds": true,
            "id": "flxViewContact",
            "isVisible": true,
            "layoutType": kony.flex.FLOW_VERTICAL,
            "isModalContainer": false,
            "skin": "slFbox0f9c8d5f6aeb842CM",
            "width": "100%",
            "zIndex": 1
        }, controller.args[0], "flxViewContact"), extendConfig({}, controller.args[1], "flxViewContact"), extendConfig({}, controller.args[2], "flxViewContact"));
        flxViewContact.setDefaultUnit(kony.flex.DP);
        var flxContactHeader = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "clipBounds": true,
            "height": "49px",
            "id": "flxContactHeader",
            "isVisible": true,
            "layoutType": kony.flex.FLOW_HORIZONTAL,
            "left": "0px",
            "isModalContainer": false,
            "skin": "slFbox0b7c2de721b0a4fCM",
            "top": "0dp",
            "width": "100%",
            "zIndex": 1
        }, controller.args[0], "flxContactHeader"), extendConfig({}, controller.args[1], "flxContactHeader"), extendConfig({}, controller.args[2], "flxContactHeader"));
        flxContactHeader.setDefaultUnit(kony.flex.DP);
        var lblContactHeader = new kony.ui.Label(extendConfig({
            "bottom": "0px",
            "centerY": "50%",
            "id": "lblContactHeader",
            "isVisible": true,
            "left": "20dp",
            "skin": "sknlblLato0a2ef656a8dde46CN",
            "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmCustomerManagementController.Contact_numbers\")",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, controller.args[0], "lblContactHeader"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblContactHeader"), extendConfig({}, controller.args[2], "lblContactHeader"));
        flxContactHeader.add(lblContactHeader);
        var flxContactRow1 = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "clipBounds": true,
            "height": "110px",
            "id": "flxContactRow1",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "left": "0px",
            "isModalContainer": false,
            "skin": "sknNormalDefault",
            "top": "0dp",
            "width": "100%",
            "zIndex": 1
        }, controller.args[0], "flxContactRow1"), extendConfig({}, controller.args[1], "flxContactRow1"), extendConfig({}, controller.args[2], "flxContactRow1"));
        flxContactRow1.setDefaultUnit(kony.flex.DP);
        var flxContactDetails1 = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "centerY": "50%",
            "clipBounds": true,
            "height": "100%",
            "id": "flxContactDetails1",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "left": "20px",
            "isModalContainer": false,
            "right": "51%",
            "skin": "slFbox",
            "zIndex": 2
        }, controller.args[0], "flxContactDetails1"), extendConfig({}, controller.args[1], "flxContactDetails1"), extendConfig({}, controller.args[2], "flxContactDetails1"));
        flxContactDetails1.setDefaultUnit(kony.flex.DP);
        var lblContactType1 = new kony.ui.ListBox(extendConfig({
            "focusSkin": "sknlbxBgffffffBorder11abebRadius3Px14px",
            "height": "40dp",
            "id": "lblContactType1",
            "isVisible": true,
            "left": "0px",
            "masterData": [
                ["type0", "Home"],
                ["type1", "Work"],
                ["type2", "Other"]
            ],
            "selectedKey": "type0",
            "skin": "sknlstbxNormal0f9abd8e88aa64a",
            "top": "0px",
            "width": "150px",
            "zIndex": 2
        }, controller.args[0], "lblContactType1"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [1, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblContactType1"), extendConfig({
            "multiSelect": false
        }, controller.args[2], "lblContactType1"));
        var txtfldContactNum1 = new kony.ui.TextBox2(extendConfig({
            "autoCapitalize": constants.TEXTBOX_AUTO_CAPITALIZE_NONE,
            "bottom": "18px",
            "focusSkin": "skntxtbxDetailsFocus14px",
            "height": "40dp",
            "id": "txtfldContactNum1",
            "isVisible": true,
            "keyBoardStyle": constants.TEXTBOX_KEY_BOARD_STYLE_DEFAULT,
            "left": "0dp",
            "placeholder": "Phone Number",
            "secureTextEntry": false,
            "skin": "skntxtbxDetails0bbf1235271384a",
            "text": "TextBox2",
            "textInputMode": constants.TEXTBOX_INPUT_MODE_ANY,
            "width": "70%",
            "zIndex": 1
        }, controller.args[0], "txtfldContactNum1"), extendConfig({
            "containerHeightMode": constants.TEXTBOX_FONT_METRICS_DRIVEN_HEIGHT,
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [2, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "txtfldContactNum1"), extendConfig({
            "autoCorrect": false
        }, controller.args[2], "txtfldContactNum1"));
        var flxRadioButtons1 = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "bottom": "18px",
            "clipBounds": true,
            "height": "40px",
            "id": "flxRadioButtons1",
            "isVisible": true,
            "layoutType": kony.flex.FLOW_HORIZONTAL,
            "left": "73%",
            "isModalContainer": false,
            "skin": "slFbox",
            "width": "120px",
            "zIndex": 2
        }, controller.args[0], "flxRadioButtons1"), extendConfig({}, controller.args[1], "flxRadioButtons1"), extendConfig({}, controller.args[2], "flxRadioButtons1"));
        flxRadioButtons1.setDefaultUnit(kony.flex.DP);
        var flxRadio1 = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "centerY": "50%",
            "clipBounds": true,
            "height": "20dp",
            "id": "flxRadio1",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "left": "0px",
            "isModalContainer": false,
            "skin": "sknCursor",
            "width": "20px",
            "zIndex": 1
        }, controller.args[0], "flxRadio1"), extendConfig({}, controller.args[1], "flxRadio1"), extendConfig({}, controller.args[2], "flxRadio1"));
        flxRadio1.setDefaultUnit(kony.flex.DP);
        var imgRadio1 = new kony.ui.Image2(extendConfig({
            "centerX": "50%",
            "centerY": "50%",
            "height": "15dp",
            "id": "imgRadio1",
            "isVisible": true,
            "skin": "slImage",
            "src": "radio_selected.png",
            "width": "15px",
            "zIndex": 1
        }, controller.args[0], "imgRadio1"), extendConfig({
            "imageScaleMode": constants.IMAGE_SCALE_MODE_MAINTAIN_ASPECT_RATIO,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "imgRadio1"), extendConfig({}, controller.args[2], "imgRadio1"));
        flxRadio1.add(imgRadio1);
        var lblprimary1 = new kony.ui.Label(extendConfig({
            "bottom": "25px",
            "centerY": "50%",
            "id": "lblprimary1",
            "isVisible": true,
            "left": "5px",
            "skin": "sknlblLatoRegular484b5213px",
            "i18n_text": "kony.i18n.getLocalizedString(\"i18n.userwidgetmodel.lblprimary1\")",
            "width": "90px",
            "zIndex": 1
        }, controller.args[0], "lblprimary1"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblprimary1"), extendConfig({}, controller.args[2], "lblprimary1"));
        flxRadioButtons1.add(flxRadio1, lblprimary1);
        var contactNumber1 = new com.adminConsole.common.contactNumber(extendConfig({
            "bottom": "0px",
            "height": "100%",
            "id": "contactNumber1",
            "isVisible": false,
            "layoutType": kony.flex.FREE_FORM,
            "left": "0dp",
            "masterType": constants.MASTER_TYPE_DEFAULT,
            "isModalContainer": false,
            "skin": "slFbox",
            "width": "80%",
            "zIndex": 1,
            "overrides": {
                "contactNumber": {
                    "bottom": "0px",
                    "height": "100%",
                    "isVisible": false,
                    "top": "viz.val_cleared",
                    "width": "80%",
                    "zIndex": 1
                },
                "flxContactNumberWrapper": {
                    "bottom": "0px",
                    "top": 50
                },
                "flxError": {
                    "isVisible": false
                }
            }
        }, controller.args[0], "contactNumber1"), extendConfig({
            "overrides": {}
        }, controller.args[1], "contactNumber1"), extendConfig({
            "overrides": {}
        }, controller.args[2], "contactNumber1"));
        var flxContactError1 = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "bottom": "0dp",
            "clipBounds": true,
            "height": "17dp",
            "id": "flxContactError1",
            "isVisible": false,
            "layoutType": kony.flex.FREE_FORM,
            "left": "0dp",
            "isModalContainer": false,
            "skin": "slFbox",
            "width": kony.flex.USE_PREFFERED_SIZE
        }, controller.args[0], "flxContactError1"), extendConfig({}, controller.args[1], "flxContactError1"), extendConfig({}, controller.args[2], "flxContactError1"));
        flxContactError1.setDefaultUnit(kony.flex.DP);
        var lblContactErrorIcon1 = new kony.ui.Label(extendConfig({
            "centerY": "52%",
            "id": "lblContactErrorIcon1",
            "isVisible": true,
            "left": "0dp",
            "skin": "sknErrorIcon",
            "text": "",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, controller.args[0], "lblContactErrorIcon1"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblContactErrorIcon1"), extendConfig({}, controller.args[2], "lblContactErrorIcon1"));
        var lblErrorContact1 = new kony.ui.Label(extendConfig({
            "centerY": "50%",
            "height": "17dp",
            "id": "lblErrorContact1",
            "isVisible": true,
            "left": "17dp",
            "skin": "sknlblError",
            "i18n_text": "kony.i18n.getLocalizedString(\"i18n.userwidgetmodel.lblErrorContact1\")",
            "width": "80%",
            "zIndex": 1
        }, controller.args[0], "lblErrorContact1"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblErrorContact1"), extendConfig({}, controller.args[2], "lblErrorContact1"));
        flxContactError1.add(lblContactErrorIcon1, lblErrorContact1);
        flxContactDetails1.add(lblContactType1, txtfldContactNum1, flxRadioButtons1, contactNumber1, flxContactError1);
        var flxContactDetails2 = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "centerY": "50%",
            "clipBounds": true,
            "height": "100%",
            "id": "flxContactDetails2",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "left": "50%",
            "isModalContainer": false,
            "right": "20dp",
            "skin": "slFbox0f14e6ffcf98744CM",
            "zIndex": 1
        }, controller.args[0], "flxContactDetails2"), extendConfig({}, controller.args[1], "flxContactDetails2"), extendConfig({}, controller.args[2], "flxContactDetails2"));
        flxContactDetails2.setDefaultUnit(kony.flex.DP);
        var lblContactType2 = new kony.ui.ListBox(extendConfig({
            "focusSkin": "sknlbxBgffffffBorder11abebRadius3Px14px",
            "height": "40dp",
            "id": "lblContactType2",
            "isVisible": true,
            "left": "0px",
            "masterData": [
                ["type0", "Home"],
                ["type1", "Work"],
                ["type2", "Other"]
            ],
            "selectedKey": "type0",
            "skin": "sknlstbxNormal0f9abd8e88aa64a",
            "top": "0px",
            "width": "150px",
            "zIndex": 2
        }, controller.args[0], "lblContactType2"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [1, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblContactType2"), extendConfig({
            "multiSelect": false
        }, controller.args[2], "lblContactType2"));
        var txtfldContactNum2 = new kony.ui.TextBox2(extendConfig({
            "autoCapitalize": constants.TEXTBOX_AUTO_CAPITALIZE_NONE,
            "bottom": "18px",
            "focusSkin": "skntxtbxDetailsFocus14px",
            "height": "36dp",
            "id": "txtfldContactNum2",
            "isVisible": true,
            "keyBoardStyle": constants.TEXTBOX_KEY_BOARD_STYLE_DEFAULT,
            "left": "0dp",
            "placeholder": "Phone Number",
            "secureTextEntry": false,
            "skin": "skntxtbxDetails0bbf1235271384a",
            "text": "TextBox2",
            "textInputMode": constants.TEXTBOX_INPUT_MODE_ANY,
            "width": "70%",
            "zIndex": 2
        }, controller.args[0], "txtfldContactNum2"), extendConfig({
            "containerHeightMode": constants.TEXTBOX_FONT_METRICS_DRIVEN_HEIGHT,
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [2, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "txtfldContactNum2"), extendConfig({
            "autoCorrect": false
        }, controller.args[2], "txtfldContactNum2"));
        var flxRadioButtons2 = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "bottom": "18px",
            "clipBounds": true,
            "height": "40px",
            "id": "flxRadioButtons2",
            "isVisible": true,
            "layoutType": kony.flex.FLOW_HORIZONTAL,
            "left": "73%",
            "isModalContainer": false,
            "skin": "slFbox",
            "width": "120px",
            "zIndex": 2
        }, controller.args[0], "flxRadioButtons2"), extendConfig({}, controller.args[1], "flxRadioButtons2"), extendConfig({}, controller.args[2], "flxRadioButtons2"));
        flxRadioButtons2.setDefaultUnit(kony.flex.DP);
        var flxRadio2 = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "centerY": "50%",
            "clipBounds": true,
            "height": "20dp",
            "id": "flxRadio2",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "left": "0px",
            "isModalContainer": false,
            "skin": "sknCursor",
            "width": "20px",
            "zIndex": 1
        }, controller.args[0], "flxRadio2"), extendConfig({}, controller.args[1], "flxRadio2"), extendConfig({}, controller.args[2], "flxRadio2"));
        flxRadio2.setDefaultUnit(kony.flex.DP);
        var imgRadio2 = new kony.ui.Image2(extendConfig({
            "centerX": "50%",
            "centerY": "50%",
            "height": "15dp",
            "id": "imgRadio2",
            "isVisible": true,
            "skin": "slImage",
            "src": "radio_notselected.png",
            "width": "15px",
            "zIndex": 1
        }, controller.args[0], "imgRadio2"), extendConfig({
            "imageScaleMode": constants.IMAGE_SCALE_MODE_MAINTAIN_ASPECT_RATIO,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "imgRadio2"), extendConfig({}, controller.args[2], "imgRadio2"));
        flxRadio2.add(imgRadio2);
        var lblprimary2 = new kony.ui.Label(extendConfig({
            "bottom": "25px",
            "centerY": "50%",
            "id": "lblprimary2",
            "isVisible": true,
            "left": "5px",
            "skin": "sknlblLatoRegular484b5213px",
            "i18n_text": "kony.i18n.getLocalizedString(\"i18n.userwidgetmodel.lblprimary1\")",
            "width": "90px",
            "zIndex": 1
        }, controller.args[0], "lblprimary2"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblprimary2"), extendConfig({}, controller.args[2], "lblprimary2"));
        flxRadioButtons2.add(flxRadio2, lblprimary2);
        var contactNumber2 = new com.adminConsole.common.contactNumber(extendConfig({
            "bottom": "0px",
            "height": "100%",
            "id": "contactNumber2",
            "isVisible": false,
            "layoutType": kony.flex.FREE_FORM,
            "left": "0dp",
            "masterType": constants.MASTER_TYPE_DEFAULT,
            "isModalContainer": false,
            "skin": "slFbox",
            "width": "80%",
            "overrides": {
                "contactNumber": {
                    "bottom": "0px",
                    "height": "100%",
                    "isVisible": false,
                    "top": "viz.val_cleared",
                    "width": "80%"
                },
                "flxContactNumberWrapper": {
                    "bottom": "0px",
                    "top": 50
                },
                "flxError": {
                    "isVisible": false
                }
            }
        }, controller.args[0], "contactNumber2"), extendConfig({
            "overrides": {}
        }, controller.args[1], "contactNumber2"), extendConfig({
            "overrides": {}
        }, controller.args[2], "contactNumber2"));
        var flxContactError2 = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "bottom": "0dp",
            "clipBounds": true,
            "height": "17dp",
            "id": "flxContactError2",
            "isVisible": false,
            "layoutType": kony.flex.FREE_FORM,
            "left": "0dp",
            "isModalContainer": false,
            "skin": "slFbox",
            "width": kony.flex.USE_PREFFERED_SIZE
        }, controller.args[0], "flxContactError2"), extendConfig({}, controller.args[1], "flxContactError2"), extendConfig({}, controller.args[2], "flxContactError2"));
        flxContactError2.setDefaultUnit(kony.flex.DP);
        var lblContactErrorIcon2 = new kony.ui.Label(extendConfig({
            "centerY": "52%",
            "id": "lblContactErrorIcon2",
            "isVisible": true,
            "left": "0dp",
            "skin": "sknErrorIcon",
            "text": "",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, controller.args[0], "lblContactErrorIcon2"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblContactErrorIcon2"), extendConfig({}, controller.args[2], "lblContactErrorIcon2"));
        var lblErrorContact2 = new kony.ui.Label(extendConfig({
            "centerY": "50%",
            "id": "lblErrorContact2",
            "isVisible": true,
            "left": "17dp",
            "skin": "sknlblError",
            "i18n_text": "kony.i18n.getLocalizedString(\"i18n.userwidgetmodel.lblErrorContact1\")",
            "width": "80%",
            "zIndex": 1
        }, controller.args[0], "lblErrorContact2"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblErrorContact2"), extendConfig({}, controller.args[2], "lblErrorContact2"));
        flxContactError2.add(lblContactErrorIcon2, lblErrorContact2);
        flxContactDetails2.add(lblContactType2, txtfldContactNum2, flxRadioButtons2, contactNumber2, flxContactError2);
        flxContactRow1.add(flxContactDetails1, flxContactDetails2);
        var flxContactRow2 = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "clipBounds": true,
            "height": "110px",
            "id": "flxContactRow2",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "left": "0px",
            "isModalContainer": false,
            "right": "40px",
            "skin": "sknNormalDefault",
            "top": "10dp",
            "width": "100%",
            "zIndex": 1
        }, controller.args[0], "flxContactRow2"), extendConfig({}, controller.args[1], "flxContactRow2"), extendConfig({}, controller.args[2], "flxContactRow2"));
        flxContactRow2.setDefaultUnit(kony.flex.DP);
        var flxContactDetails3 = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "centerY": "50%",
            "clipBounds": true,
            "height": "100%",
            "id": "flxContactDetails3",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "left": "20px",
            "isModalContainer": false,
            "right": "51%",
            "skin": "slFbox",
            "zIndex": 1
        }, controller.args[0], "flxContactDetails3"), extendConfig({}, controller.args[1], "flxContactDetails3"), extendConfig({}, controller.args[2], "flxContactDetails3"));
        flxContactDetails3.setDefaultUnit(kony.flex.DP);
        var txtfldContactNum3 = new kony.ui.TextBox2(extendConfig({
            "autoCapitalize": constants.TEXTBOX_AUTO_CAPITALIZE_NONE,
            "bottom": "18px",
            "focusSkin": "skntxtbxDetailsFocus14px",
            "height": "40dp",
            "id": "txtfldContactNum3",
            "isVisible": true,
            "keyBoardStyle": constants.TEXTBOX_KEY_BOARD_STYLE_DEFAULT,
            "left": "0dp",
            "placeholder": "Phone Number",
            "secureTextEntry": false,
            "skin": "skntxtbxDetails0bbf1235271384a",
            "text": "TextBox2",
            "textInputMode": constants.TEXTBOX_INPUT_MODE_ANY,
            "width": "70%",
            "zIndex": 2
        }, controller.args[0], "txtfldContactNum3"), extendConfig({
            "containerHeightMode": constants.TEXTBOX_FONT_METRICS_DRIVEN_HEIGHT,
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [2, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "txtfldContactNum3"), extendConfig({
            "autoCorrect": false
        }, controller.args[2], "txtfldContactNum3"));
        var flxRadioButtons3 = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "bottom": "18px",
            "clipBounds": true,
            "height": "40px",
            "id": "flxRadioButtons3",
            "isVisible": true,
            "layoutType": kony.flex.FLOW_HORIZONTAL,
            "left": "73%",
            "isModalContainer": false,
            "skin": "slFbox",
            "width": "120px",
            "zIndex": 2
        }, controller.args[0], "flxRadioButtons3"), extendConfig({}, controller.args[1], "flxRadioButtons3"), extendConfig({}, controller.args[2], "flxRadioButtons3"));
        flxRadioButtons3.setDefaultUnit(kony.flex.DP);
        var flxRadio3 = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "centerY": "50%",
            "clipBounds": true,
            "height": "20dp",
            "id": "flxRadio3",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "left": "0px",
            "isModalContainer": false,
            "skin": "slFbox",
            "width": "20px",
            "zIndex": 1
        }, controller.args[0], "flxRadio3"), extendConfig({}, controller.args[1], "flxRadio3"), extendConfig({}, controller.args[2], "flxRadio3"));
        flxRadio3.setDefaultUnit(kony.flex.DP);
        var imgRadio3 = new kony.ui.Image2(extendConfig({
            "centerX": "50%",
            "centerY": "50%",
            "height": "15dp",
            "id": "imgRadio3",
            "isVisible": true,
            "skin": "slImage",
            "src": "radio_notselected.png",
            "width": "15px",
            "zIndex": 1
        }, controller.args[0], "imgRadio3"), extendConfig({
            "imageScaleMode": constants.IMAGE_SCALE_MODE_MAINTAIN_ASPECT_RATIO,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "imgRadio3"), extendConfig({}, controller.args[2], "imgRadio3"));
        flxRadio3.add(imgRadio3);
        var lblprimary3 = new kony.ui.Label(extendConfig({
            "bottom": "25px",
            "centerY": "50%",
            "id": "lblprimary3",
            "isVisible": true,
            "left": "5px",
            "skin": "sknlblLatoRegular484b5213px",
            "i18n_text": "kony.i18n.getLocalizedString(\"i18n.userwidgetmodel.lblprimary1\")",
            "width": "90px",
            "zIndex": 1
        }, controller.args[0], "lblprimary3"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblprimary3"), extendConfig({}, controller.args[2], "lblprimary3"));
        flxRadioButtons3.add(flxRadio3, lblprimary3);
        var lblContactType3 = new kony.ui.ListBox(extendConfig({
            "focusSkin": "sknlbxBgffffffBorder11abebRadius3Px14px",
            "height": "40dp",
            "id": "lblContactType3",
            "isVisible": true,
            "left": "0px",
            "masterData": [
                ["type0", "Home"],
                ["type1", "Work"],
                ["type2", "Other"]
            ],
            "selectedKey": "type0",
            "skin": "sknlstbxNormal0f9abd8e88aa64a",
            "top": "0px",
            "width": "150px",
            "zIndex": 2
        }, controller.args[0], "lblContactType3"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [1, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblContactType3"), extendConfig({
            "multiSelect": false
        }, controller.args[2], "lblContactType3"));
        var contactNumber3 = new com.adminConsole.common.contactNumber(extendConfig({
            "bottom": "0px",
            "height": "100%",
            "id": "contactNumber3",
            "isVisible": false,
            "layoutType": kony.flex.FREE_FORM,
            "left": "0dp",
            "masterType": constants.MASTER_TYPE_DEFAULT,
            "isModalContainer": false,
            "skin": "slFbox",
            "width": "80%",
            "overrides": {
                "contactNumber": {
                    "bottom": "0px",
                    "height": "100%",
                    "isVisible": false,
                    "top": "viz.val_cleared",
                    "width": "80%"
                },
                "flxContactNumberWrapper": {
                    "bottom": "0px",
                    "top": 50
                },
                "flxError": {
                    "isVisible": false
                }
            }
        }, controller.args[0], "contactNumber3"), extendConfig({
            "overrides": {}
        }, controller.args[1], "contactNumber3"), extendConfig({
            "overrides": {}
        }, controller.args[2], "contactNumber3"));
        var flxContactError3 = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "bottom": "0dp",
            "clipBounds": true,
            "height": "17dp",
            "id": "flxContactError3",
            "isVisible": false,
            "layoutType": kony.flex.FREE_FORM,
            "left": "0dp",
            "isModalContainer": false,
            "skin": "slFbox",
            "width": kony.flex.USE_PREFFERED_SIZE
        }, controller.args[0], "flxContactError3"), extendConfig({}, controller.args[1], "flxContactError3"), extendConfig({}, controller.args[2], "flxContactError3"));
        flxContactError3.setDefaultUnit(kony.flex.DP);
        var lblContactErrorIcon3 = new kony.ui.Label(extendConfig({
            "centerY": "52%",
            "id": "lblContactErrorIcon3",
            "isVisible": true,
            "left": "0dp",
            "skin": "sknErrorIcon",
            "text": "",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, controller.args[0], "lblContactErrorIcon3"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblContactErrorIcon3"), extendConfig({}, controller.args[2], "lblContactErrorIcon3"));
        var lblErrorContact3 = new kony.ui.Label(extendConfig({
            "centerY": "50%",
            "id": "lblErrorContact3",
            "isVisible": true,
            "left": "17dp",
            "skin": "sknlblError",
            "i18n_text": "kony.i18n.getLocalizedString(\"i18n.userwidgetmodel.lblErrorContact1\")",
            "width": "80%",
            "zIndex": 1
        }, controller.args[0], "lblErrorContact3"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblErrorContact3"), extendConfig({}, controller.args[2], "lblErrorContact3"));
        flxContactError3.add(lblContactErrorIcon3, lblErrorContact3);
        flxContactDetails3.add(txtfldContactNum3, flxRadioButtons3, lblContactType3, contactNumber3, flxContactError3);
        flxContactRow2.add(flxContactDetails3);
        flxViewContact.add(flxContactHeader, flxContactRow1, flxContactRow2);
        EditContact.add(flxViewContact);
        return EditContact;
    }
})