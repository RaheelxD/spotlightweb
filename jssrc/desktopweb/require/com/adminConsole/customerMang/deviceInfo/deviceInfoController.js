define("com/adminConsole/customerMang/deviceInfo/userdeviceInfoController", function() {
    return {};
});
define("com/adminConsole/customerMang/deviceInfo/deviceInfoControllerActions", {
    /*
          This is an auto generated file and any modifications to it may result in corruption of the action sequence.
        */
});
define("com/adminConsole/customerMang/deviceInfo/deviceInfoController", ["com/adminConsole/customerMang/deviceInfo/userdeviceInfoController", "com/adminConsole/customerMang/deviceInfo/deviceInfoControllerActions"], function() {
    var controller = require("com/adminConsole/customerMang/deviceInfo/userdeviceInfoController");
    var actions = require("com/adminConsole/customerMang/deviceInfo/deviceInfoControllerActions");
    for (var key in actions) {
        controller[key] = actions[key];
    }
    return controller;
});
