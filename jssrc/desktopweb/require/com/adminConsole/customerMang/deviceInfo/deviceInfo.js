define(function() {
    return function(controller) {
        var deviceInfo = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "clipBounds": true,
            "isMaster": true,
            "height": "85dp",
            "id": "deviceInfo",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "left": "0dp",
            "isModalContainer": false,
            "skin": "CopyslFbox0a8186fa22dd549",
            "top": "0dp",
            "width": "250px"
        }, controller.args[0], "deviceInfo"), extendConfig({}, controller.args[1], "deviceInfo"), extendConfig({}, controller.args[2], "deviceInfo"));
        deviceInfo.setDefaultUnit(kony.flex.DP);
        var flxArrowImage = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "clipBounds": true,
            "height": "100%",
            "id": "flxArrowImage",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "left": "0dp",
            "isModalContainer": false,
            "skin": "slFbox",
            "top": "0dp",
            "width": "12dp",
            "zIndex": 5
        }, controller.args[0], "flxArrowImage"), extendConfig({}, controller.args[1], "flxArrowImage"), extendConfig({}, controller.args[2], "flxArrowImage"));
        flxArrowImage.setDefaultUnit(kony.flex.DP);
        var imgLeftArrow = new kony.ui.Image2(extendConfig({
            "height": "15dp",
            "id": "imgLeftArrow",
            "isVisible": true,
            "right": "0dp",
            "skin": "slImage",
            "src": "arrowvariableref1x.png",
            "top": "20dp",
            "width": "11dp",
            "zIndex": 1
        }, controller.args[0], "imgLeftArrow"), extendConfig({
            "imageScaleMode": constants.IMAGE_SCALE_MODE_FIT_TO_DIMENSIONS,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "imgLeftArrow"), extendConfig({}, controller.args[2], "imgLeftArrow"));
        flxArrowImage.add(imgLeftArrow);
        var flxDeviceInfo = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "clipBounds": true,
            "height": "100%",
            "id": "flxDeviceInfo",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "left": "11dp",
            "isModalContainer": false,
            "right": "0dp",
            "skin": "sknflxffffffop100dbdbe6Radius3px",
            "top": "0dp",
            "zIndex": 2
        }, controller.args[0], "flxDeviceInfo"), extendConfig({}, controller.args[1], "flxDeviceInfo"), extendConfig({}, controller.args[2], "flxDeviceInfo"));
        flxDeviceInfo.setDefaultUnit(kony.flex.DP);
        var flxRegisteredOn = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "clipBounds": true,
            "height": "13px",
            "id": "flxRegisteredOn",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "left": "0dp",
            "isModalContainer": false,
            "skin": "slFbox",
            "top": "13dp",
            "width": "100%"
        }, controller.args[0], "flxRegisteredOn"), extendConfig({}, controller.args[1], "flxRegisteredOn"), extendConfig({}, controller.args[2], "flxRegisteredOn"));
        flxRegisteredOn.setDefaultUnit(kony.flex.DP);
        var lblRegisterOn = new kony.ui.Label(extendConfig({
            "height": "13px",
            "id": "lblRegisterOn",
            "isVisible": true,
            "left": "13dp",
            "skin": "sknDeviceInfo",
            "text": "REGISTERED ON",
            "top": "0dp",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, controller.args[0], "lblRegisterOn"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblRegisterOn"), extendConfig({}, controller.args[2], "lblRegisterOn"));
        var lblRegisterdOnValue = new kony.ui.Label(extendConfig({
            "height": "13px",
            "id": "lblRegisterdOnValue",
            "isVisible": true,
            "left": "110dp",
            "skin": "sknDeviceInfoValue",
            "text": "11-11-2017 10:10 AM",
            "top": "0dp",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, controller.args[0], "lblRegisterdOnValue"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblRegisterdOnValue"), extendConfig({}, controller.args[2], "lblRegisterdOnValue"));
        flxRegisteredOn.add(lblRegisterOn, lblRegisterdOnValue);
        var flxDevice = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "clipBounds": true,
            "height": "15px",
            "id": "flxDevice",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "left": "0dp",
            "isModalContainer": false,
            "skin": "slFbox",
            "top": "36dp",
            "width": "100%"
        }, controller.args[0], "flxDevice"), extendConfig({}, controller.args[1], "flxDevice"), extendConfig({}, controller.args[2], "flxDevice"));
        flxDevice.setDefaultUnit(kony.flex.DP);
        var lblDeviceId = new kony.ui.Label(extendConfig({
            "height": "13px",
            "id": "lblDeviceId",
            "isVisible": true,
            "left": "13dp",
            "skin": "sknDeviceInfo",
            "text": "DEVICE ID",
            "top": "0dp",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, controller.args[0], "lblDeviceId"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblDeviceId"), extendConfig({}, controller.args[2], "lblDeviceId"));
        var lblDeviceIdValue = new kony.ui.Label(extendConfig({
            "height": "13px",
            "id": "lblDeviceIdValue",
            "isVisible": true,
            "left": "110px",
            "skin": "sknDeviceInfoValue",
            "text": "ASDSFAGFGFN",
            "top": "0dp",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, controller.args[0], "lblDeviceIdValue"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblDeviceIdValue"), extendConfig({}, controller.args[2], "lblDeviceIdValue"));
        var fonticonCopy = new kony.ui.Label(extendConfig({
            "height": "16px",
            "id": "fonticonCopy",
            "isVisible": true,
            "left": "205dp",
            "skin": "sknFontIconCurrency",
            "text": "",
            "top": "0dp",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, controller.args[0], "fonticonCopy"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "fonticonCopy"), extendConfig({
            "hoverSkin": "sknFontIconCopytoClipBoard"
        }, controller.args[2], "fonticonCopy"));
        flxDevice.add(lblDeviceId, lblDeviceIdValue, fonticonCopy);
        var flxOS = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "clipBounds": true,
            "height": "13px",
            "id": "flxOS",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "left": "0dp",
            "isModalContainer": false,
            "skin": "slFbox",
            "top": "59dp",
            "width": "100%"
        }, controller.args[0], "flxOS"), extendConfig({}, controller.args[1], "flxOS"), extendConfig({}, controller.args[2], "flxOS"));
        flxOS.setDefaultUnit(kony.flex.DP);
        var lblOS = new kony.ui.Label(extendConfig({
            "height": "13px",
            "id": "lblOS",
            "isVisible": true,
            "left": "13dp",
            "right": "13px",
            "skin": "sknDeviceInfo",
            "text": "OS",
            "top": 0,
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, controller.args[0], "lblOS"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblOS"), extendConfig({}, controller.args[2], "lblOS"));
        var lblOSValue = new kony.ui.Label(extendConfig({
            "height": "13px",
            "id": "lblOSValue",
            "isVisible": true,
            "left": "110dp",
            "skin": "sknDeviceInfoValue",
            "text": "iOS 11",
            "top": "0dp",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, controller.args[0], "lblOSValue"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblOSValue"), extendConfig({}, controller.args[2], "lblOSValue"));
        flxOS.add(lblOS, lblOSValue);
        var flxCopyTooltip = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "clipBounds": true,
            "height": "30px",
            "id": "flxCopyTooltip",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "left": "175dp",
            "isModalContainer": false,
            "skin": "slFbox",
            "top": "48dp",
            "width": "48px"
        }, controller.args[0], "flxCopyTooltip"), extendConfig({}, controller.args[1], "flxCopyTooltip"), extendConfig({}, controller.args[2], "flxCopyTooltip"));
        flxCopyTooltip.setDefaultUnit(kony.flex.DP);
        var lblarrow = new kony.ui.Label(extendConfig({
            "centerX": "80%",
            "height": "10dp",
            "id": "lblarrow",
            "isVisible": true,
            "left": "0px",
            "skin": "sknfonticonCustomersClipboardTooltip",
            "i18n_text": "kony.i18n.getLocalizedString(\"i18n.userwidgetmodel.fontIconUp\")",
            "top": "0px",
            "width": "10px",
            "zIndex": 20
        }, controller.args[0], "lblarrow"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblarrow"), extendConfig({}, controller.args[2], "lblarrow"));
        var flxToolTipMessage = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "centerX": "50%",
            "clipBounds": true,
            "height": "15px",
            "id": "flxToolTipMessage",
            "isVisible": true,
            "layoutType": kony.flex.FLOW_HORIZONTAL,
            "left": "0px",
            "isModalContainer": false,
            "skin": "sknflxTooltip",
            "top": "6px",
            "width": "100%",
            "zIndex": 20
        }, controller.args[0], "flxToolTipMessage"), extendConfig({}, controller.args[1], "flxToolTipMessage"), extendConfig({}, controller.args[2], "flxToolTipMessage"));
        flxToolTipMessage.setDefaultUnit(kony.flex.DP);
        var lblToolTip = new kony.ui.Label(extendConfig({
            "centerX": "50%",
            "centerY": "45%",
            "id": "lblToolTip",
            "isVisible": true,
            "skin": "sknCopytoClipboard",
            "i18n_text": "kony.i18n.getLocalizedString(\"i18n.customermanagement.COPIED\")",
            "width": "93%",
            "zIndex": 1
        }, controller.args[0], "lblToolTip"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_CENTER,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblToolTip"), extendConfig({}, controller.args[2], "lblToolTip"));
        flxToolTipMessage.add(lblToolTip);
        flxCopyTooltip.add(lblarrow, flxToolTipMessage);
        flxDeviceInfo.add(flxRegisteredOn, flxDevice, flxOS, flxCopyTooltip);
        deviceInfo.add(flxArrowImage, flxDeviceInfo);
        return deviceInfo;
    }
})