define(function() {
    return function(controller) {
        var generalInfoHeader = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
            "clipBounds": true,
            "isMaster": true,
            "id": "generalInfoHeader",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "isModalContainer": false,
            "skin": "slFbox",
            "width": "100%"
        }, controller.args[0], "generalInfoHeader"), extendConfig({}, controller.args[1], "generalInfoHeader"), extendConfig({}, controller.args[2], "generalInfoHeader"));
        generalInfoHeader.setDefaultUnit(kony.flex.DP);
        var flxHeader = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
            "clipBounds": true,
            "id": "flxHeader",
            "isVisible": true,
            "layoutType": kony.flex.FLOW_VERTICAL,
            "left": "0dp",
            "isModalContainer": false,
            "skin": "slFbox",
            "top": "0dp",
            "width": "100%"
        }, controller.args[0], "flxHeader"), extendConfig({}, controller.args[1], "flxHeader"), extendConfig({}, controller.args[2], "flxHeader"));
        flxHeader.setDefaultUnit(kony.flex.DP);
        var flxNotification = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "clipBounds": true,
            "height": "40dp",
            "id": "flxNotification",
            "isVisible": false,
            "layoutType": kony.flex.FREE_FORM,
            "left": "0dp",
            "isModalContainer": false,
            "right": "0dp",
            "skin": "sknFlxBgFFFFFFBor4A77A0S1pxR1px",
            "top": "10dp",
            "width": "99%",
            "zIndex": 1
        }, controller.args[0], "flxNotification"), extendConfig({}, controller.args[1], "flxNotification"), extendConfig({}, controller.args[2], "flxNotification"));
        flxNotification.setDefaultUnit(kony.flex.DP);
        var flxIconContainer = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "clipBounds": true,
            "height": "40dp",
            "id": "flxIconContainer",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "left": "0dp",
            "isModalContainer": false,
            "skin": "sknFlxBg4A77A0",
            "top": "0dp",
            "width": "40dp",
            "zIndex": 1
        }, controller.args[0], "flxIconContainer"), extendConfig({}, controller.args[1], "flxIconContainer"), extendConfig({}, controller.args[2], "flxIconContainer"));
        flxIconContainer.setDefaultUnit(kony.flex.DP);
        var lblNotificationIcon = new kony.ui.Label(extendConfig({
            "centerX": "48%",
            "centerY": "48%",
            "height": "19px",
            "id": "lblNotificationIcon",
            "isVisible": true,
            "skin": "sknErrorWhiteIcon19px",
            "text": "",
            "width": "19px",
            "zIndex": 1
        }, controller.args[0], "lblNotificationIcon"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_CENTER,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblNotificationIcon"), extendConfig({}, controller.args[2], "lblNotificationIcon"));
        flxIconContainer.add(lblNotificationIcon);
        var lblMessage = new kony.ui.Label(extendConfig({
            "centerY": "50%",
            "id": "lblMessage",
            "isVisible": true,
            "left": "50dp",
            "skin": "sknLbl485C75LatoRegular13Px",
            "text": "Profile is not yet activated.",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, controller.args[0], "lblMessage"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblMessage"), extendConfig({}, controller.args[2], "lblMessage"));
        var flxResendActivationCode = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "centerY": "50%",
            "clipBounds": true,
            "height": "40dp",
            "id": "flxResendActivationCode",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "left": "300dp",
            "isModalContainer": false,
            "skin": "slFbox",
            "width": "150dp",
            "zIndex": 1
        }, controller.args[0], "flxResendActivationCode"), extendConfig({}, controller.args[1], "flxResendActivationCode"), extendConfig({
            "hoverSkin": "hoverhandSkin"
        }, controller.args[2], "flxResendActivationCode"));
        flxResendActivationCode.setDefaultUnit(kony.flex.DP);
        var lblResendActivationCode = new kony.ui.Label(extendConfig({
            "centerY": "50%",
            "id": "lblResendActivationCode",
            "isVisible": true,
            "left": "10dp",
            "skin": "sknLblLato13px117eb0",
            "text": "Resend Activation Code",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, controller.args[0], "lblResendActivationCode"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblResendActivationCode"), extendConfig({}, controller.args[2], "lblResendActivationCode"));
        var flxUnderline = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "clipBounds": true,
            "height": "1px",
            "id": "flxUnderline",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "left": "10dp",
            "isModalContainer": false,
            "skin": "sknFlxBorder117eb0CustomRadius5px",
            "top": "27dp",
            "width": "138dp",
            "zIndex": 1
        }, controller.args[0], "flxUnderline"), extendConfig({}, controller.args[1], "flxUnderline"), extendConfig({}, controller.args[2], "flxUnderline"));
        flxUnderline.setDefaultUnit(kony.flex.DP);
        flxUnderline.add();
        flxResendActivationCode.add(lblResendActivationCode, flxUnderline);
        flxNotification.add(flxIconContainer, lblMessage, flxResendActivationCode);
        var flxTagsandbuttons = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "clipBounds": true,
            "height": "40px",
            "id": "flxTagsandbuttons",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "left": "0dp",
            "isModalContainer": false,
            "skin": "slFbox",
            "top": "10dp",
            "width": "100%"
        }, controller.args[0], "flxTagsandbuttons"), extendConfig({}, controller.args[1], "flxTagsandbuttons"), extendConfig({}, controller.args[2], "flxTagsandbuttons"));
        flxTagsandbuttons.setDefaultUnit(kony.flex.DP);
        var flxCustomerTags = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "clipBounds": true,
            "height": "40px",
            "id": "flxCustomerTags",
            "isVisible": true,
            "layoutType": kony.flex.FLOW_HORIZONTAL,
            "left": "0dp",
            "isModalContainer": false,
            "skin": "slFbox",
            "top": "0dp",
            "width": "30%",
            "zIndex": 1
        }, controller.args[0], "flxCustomerTags"), extendConfig({}, controller.args[1], "flxCustomerTags"), extendConfig({}, controller.args[2], "flxCustomerTags"));
        flxCustomerTags.setDefaultUnit(kony.flex.DP);
        var flxRetailTag = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "centerY": "50%",
            "clipBounds": true,
            "height": "20px",
            "id": "flxRetailTag",
            "isVisible": true,
            "layoutType": kony.flex.FLOW_HORIZONTAL,
            "left": "0px",
            "isModalContainer": false,
            "skin": "sknflxCustomertagRed",
            "top": "0dp",
            "width": "77px"
        }, controller.args[0], "flxRetailTag"), extendConfig({}, controller.args[1], "flxRetailTag"), extendConfig({}, controller.args[2], "flxRetailTag"));
        flxRetailTag.setDefaultUnit(kony.flex.DP);
        var fontIconCircle1 = new kony.ui.Label(extendConfig({
            "centerY": "48%",
            "id": "fontIconCircle1",
            "isVisible": true,
            "left": "5px",
            "skin": "sknCircleCustomerTag",
            "i18n_text": "kony.i18n.getLocalizedString(\"i18n.userwidgetmodel.lblFontIconStatus1\")",
            "top": "0dp",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 2
        }, controller.args[0], "fontIconCircle1"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "fontIconCircle1"), extendConfig({}, controller.args[2], "fontIconCircle1"));
        var lblContent1 = new kony.ui.Label(extendConfig({
            "centerY": "50%",
            "id": "lblContent1",
            "isVisible": true,
            "left": "5px",
            "right": "5dp",
            "skin": "sknCustomerTypeTag",
            "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmCustomerManagementController.RetailUser\")",
            "top": "0dp",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, controller.args[0], "lblContent1"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblContent1"), extendConfig({}, controller.args[2], "lblContent1"));
        flxRetailTag.add(fontIconCircle1, lblContent1);
        var flxSmallBusinessTag = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "centerY": "50%",
            "clipBounds": true,
            "height": "20px",
            "id": "flxSmallBusinessTag",
            "isVisible": false,
            "layoutType": kony.flex.FLOW_HORIZONTAL,
            "left": "0px",
            "isModalContainer": false,
            "skin": "sknflxCustomertagLiteGreen",
            "top": "0dp",
            "width": "125px"
        }, controller.args[0], "flxSmallBusinessTag"), extendConfig({}, controller.args[1], "flxSmallBusinessTag"), extendConfig({}, controller.args[2], "flxSmallBusinessTag"));
        flxSmallBusinessTag.setDefaultUnit(kony.flex.DP);
        var fontIconCircle3 = new kony.ui.Label(extendConfig({
            "centerY": "48%",
            "id": "fontIconCircle3",
            "isVisible": true,
            "left": "5px",
            "skin": "sknCircleCustomerTag",
            "i18n_text": "kony.i18n.getLocalizedString(\"i18n.userwidgetmodel.lblFontIconStatus1\")",
            "top": "0dp",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 2
        }, controller.args[0], "fontIconCircle3"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "fontIconCircle3"), extendConfig({}, controller.args[2], "fontIconCircle3"));
        var lblContent3 = new kony.ui.Label(extendConfig({
            "centerY": "50%",
            "id": "lblContent3",
            "isVisible": true,
            "left": "5px",
            "skin": "sknCustomerTypeTag",
            "text": "Small Business User",
            "top": "0dp",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, controller.args[0], "lblContent3"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblContent3"), extendConfig({}, controller.args[2], "lblContent3"));
        flxSmallBusinessTag.add(fontIconCircle3, lblContent3);
        var flxMicroBusinessTag = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "centerY": "50%",
            "clipBounds": true,
            "height": "20px",
            "id": "flxMicroBusinessTag",
            "isVisible": true,
            "layoutType": kony.flex.FLOW_HORIZONTAL,
            "left": "0px",
            "isModalContainer": false,
            "skin": "sknflxCustomertagPurple",
            "top": "0dp",
            "width": "97px"
        }, controller.args[0], "flxMicroBusinessTag"), extendConfig({}, controller.args[1], "flxMicroBusinessTag"), extendConfig({}, controller.args[2], "flxMicroBusinessTag"));
        flxMicroBusinessTag.setDefaultUnit(kony.flex.DP);
        var fontIconCircle4 = new kony.ui.Label(extendConfig({
            "centerY": "48%",
            "id": "fontIconCircle4",
            "isVisible": true,
            "left": "5px",
            "skin": "sknCircleCustomerTag",
            "i18n_text": "kony.i18n.getLocalizedString(\"i18n.userwidgetmodel.lblFontIconStatus1\")",
            "top": "0dp",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 2
        }, controller.args[0], "fontIconCircle4"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "fontIconCircle4"), extendConfig({}, controller.args[2], "fontIconCircle4"));
        var lblContent4 = new kony.ui.Label(extendConfig({
            "centerY": "50%",
            "id": "lblContent4",
            "isVisible": true,
            "left": "5px",
            "skin": "sknCustomerTypeTag",
            "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmCustomerProfile.BusinessUser\")",
            "top": "0dp",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, controller.args[0], "lblContent4"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblContent4"), extendConfig({}, controller.args[2], "lblContent4"));
        flxMicroBusinessTag.add(fontIconCircle4, lblContent4);
        var flxWealthTag = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "centerY": "50%",
            "clipBounds": true,
            "height": "20px",
            "id": "flxWealthTag",
            "isVisible": false,
            "layoutType": kony.flex.FLOW_HORIZONTAL,
            "left": "0px",
            "isModalContainer": false,
            "skin": "sknflxCustomertag0D4CFF",
            "top": "0dp",
            "width": "90px"
        }, controller.args[0], "flxWealthTag"), extendConfig({}, controller.args[1], "flxWealthTag"), extendConfig({}, controller.args[2], "flxWealthTag"));
        flxWealthTag.setDefaultUnit(kony.flex.DP);
        var fontIconCircle5 = new kony.ui.Label(extendConfig({
            "centerY": "48%",
            "id": "fontIconCircle5",
            "isVisible": true,
            "left": "5px",
            "skin": "sknCircleCustomerTag",
            "i18n_text": "kony.i18n.getLocalizedString(\"i18n.userwidgetmodel.lblFontIconStatus1\")",
            "top": "0dp",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 2
        }, controller.args[0], "fontIconCircle5"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "fontIconCircle5"), extendConfig({}, controller.args[2], "fontIconCircle5"));
        var lblContent5 = new kony.ui.Label(extendConfig({
            "centerY": "50%",
            "id": "lblContent5",
            "isVisible": true,
            "left": "5px",
            "skin": "sknCustomerTypeTag",
            "text": "Wealth User",
            "top": "0dp",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, controller.args[0], "lblContent5"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblContent5"), extendConfig({}, controller.args[2], "lblContent5"));
        flxWealthTag.add(fontIconCircle5, lblContent5);
        var flxApplicantTag = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "centerY": "50%",
            "clipBounds": true,
            "height": "20px",
            "id": "flxApplicantTag",
            "isVisible": true,
            "layoutType": kony.flex.FLOW_HORIZONTAL,
            "left": "0px",
            "isModalContainer": false,
            "skin": "sknflxCustomertagBlue",
            "top": "0dp",
            "width": "70px"
        }, controller.args[0], "flxApplicantTag"), extendConfig({}, controller.args[1], "flxApplicantTag"), extendConfig({}, controller.args[2], "flxApplicantTag"));
        flxApplicantTag.setDefaultUnit(kony.flex.DP);
        var fontIconCircle2 = new kony.ui.Label(extendConfig({
            "centerY": "48%",
            "id": "fontIconCircle2",
            "isVisible": true,
            "left": "5px",
            "skin": "sknCircleCustomerTag",
            "i18n_text": "kony.i18n.getLocalizedString(\"i18n.userwidgetmodel.lblFontIconStatus1\")",
            "top": "0dp",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 2
        }, controller.args[0], "fontIconCircle2"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "fontIconCircle2"), extendConfig({}, controller.args[2], "fontIconCircle2"));
        var lblContent2 = new kony.ui.Label(extendConfig({
            "centerY": "50%",
            "id": "lblContent2",
            "isVisible": true,
            "left": "5px",
            "skin": "sknCustomerTypeTag",
            "text": "Applicant",
            "top": "0dp",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, controller.args[0], "lblContent2"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblContent2"), extendConfig({}, controller.args[2], "lblContent2"));
        flxApplicantTag.add(fontIconCircle2, lblContent2);
        flxCustomerTags.add(flxRetailTag, flxSmallBusinessTag, flxMicroBusinessTag, flxWealthTag, flxApplicantTag);
        var flxActionButtons = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "clipBounds": true,
            "height": "40px",
            "id": "flxActionButtons",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "reverseLayoutDirection": false,
            "left": "25%",
            "isModalContainer": false,
            "skin": "slFbox",
            "top": "0px",
            "width": "75%",
            "zIndex": 5
        }, controller.args[0], "flxActionButtons"), extendConfig({}, controller.args[1], "flxActionButtons"), extendConfig({}, controller.args[2], "flxActionButtons"));
        flxActionButtons.setDefaultUnit(kony.flex.DP);
        var flxActionsbtnGroup = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "clipBounds": true,
            "height": "100%",
            "id": "flxActionsbtnGroup",
            "isVisible": true,
            "layoutType": kony.flex.FLOW_HORIZONTAL,
            "reverseLayoutDirection": true,
            "left": "0dp",
            "isModalContainer": false,
            "skin": "slFbox",
            "top": "0dp",
            "width": "100%"
        }, controller.args[0], "flxActionsbtnGroup"), extendConfig({}, controller.args[1], "flxActionsbtnGroup"), extendConfig({}, controller.args[2], "flxActionsbtnGroup"));
        flxActionsbtnGroup.setDefaultUnit(kony.flex.DP);
        var flxOptions = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "centerY": "50%",
            "clipBounds": true,
            "height": "30px",
            "id": "flxOptions",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "isModalContainer": false,
            "right": "12px",
            "skin": "sknFlxBgFFFFFFRounded",
            "width": "30px",
            "zIndex": 3
        }, controller.args[0], "flxOptions"), extendConfig({}, controller.args[1], "flxOptions"), extendConfig({
            "hoverSkin": "sknflxffffffop100Border424242Radius100px"
        }, controller.args[2], "flxOptions"));
        flxOptions.setDefaultUnit(kony.flex.DP);
        var lblIconAlertTypeOptions = new kony.ui.Label(extendConfig({
            "centerX": "50%",
            "centerY": "50%",
            "id": "lblIconAlertTypeOptions",
            "isVisible": true,
            "skin": "sknFontIconOptionMenu",
            "i18n_text": "kony.i18n.getLocalizedString(\"i18n.userwidgetmodel.fontIconImgOptions\")",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, controller.args[0], "lblIconAlertTypeOptions"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblIconAlertTypeOptions"), extendConfig({}, controller.args[2], "lblIconAlertTypeOptions"));
        flxOptions.add(lblIconAlertTypeOptions);
        var flxCSRAssist = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "centerY": "50%",
            "clipBounds": true,
            "height": "22px",
            "id": "flxCSRAssist",
            "isVisible": true,
            "layoutType": kony.flex.FLOW_HORIZONTAL,
            "isModalContainer": false,
            "right": "20px",
            "skin": "sknFlxBg003E75Br1pxRd20px",
            "width": "140px",
            "zIndex": 1
        }, controller.args[0], "flxCSRAssist"), extendConfig({}, controller.args[1], "flxCSRAssist"), extendConfig({
            "hoverSkin": "sknFlxBg005198Br1pxRd20pxHov"
        }, controller.args[2], "flxCSRAssist"));
        flxCSRAssist.setDefaultUnit(kony.flex.DP);
        var fonticonCSRAssist = new kony.ui.Label(extendConfig({
            "centerY": "45%",
            "height": "15px",
            "id": "fonticonCSRAssist",
            "isVisible": true,
            "left": "10px",
            "skin": "sknIcon13pxWhite",
            "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmPermissions.fontIconPrevious\")",
            "width": "15px",
            "zIndex": 1
        }, controller.args[0], "fonticonCSRAssist"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "fonticonCSRAssist"), extendConfig({}, controller.args[2], "fonticonCSRAssist"));
        var lblCSRAssist = new kony.ui.Label(extendConfig({
            "centerY": "45%",
            "id": "lblCSRAssist",
            "isVisible": true,
            "skin": "sknlblCSRAssist",
            "i18n_text": "kony.i18n.getLocalizedString(\"i18n.userwidgetmodel.lblCSRAssist\")",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, controller.args[0], "lblCSRAssist"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblCSRAssist"), extendConfig({}, controller.args[2], "lblCSRAssist"));
        flxCSRAssist.add(fonticonCSRAssist, lblCSRAssist);
        var flxUnlock = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "centerY": "50%",
            "clipBounds": true,
            "height": "22px",
            "id": "flxUnlock",
            "isVisible": true,
            "layoutType": kony.flex.FLOW_HORIZONTAL,
            "isModalContainer": false,
            "right": "20dp",
            "skin": "sknBackgroundColorUnlockCustomer",
            "width": "180px",
            "zIndex": 1
        }, controller.args[0], "flxUnlock"), extendConfig({}, controller.args[1], "flxUnlock"), extendConfig({
            "hoverSkin": "sknHoverUnlock"
        }, controller.args[2], "flxUnlock"));
        flxUnlock.setDefaultUnit(kony.flex.DP);
        var fonticonUnlock = new kony.ui.Label(extendConfig({
            "centerY": "45%",
            "id": "fonticonUnlock",
            "isVisible": true,
            "left": "9px",
            "skin": "sknfontIconLock",
            "i18n_text": "kony.i18n.getLocalizedString(\"i18n.userwidgetmodel.fonticonCSRAssist\")",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, controller.args[0], "fonticonUnlock"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_CENTER,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "fonticonUnlock"), extendConfig({}, controller.args[2], "fonticonUnlock"));
        var lblUnlock = new kony.ui.Label(extendConfig({
            "centerY": "45%",
            "id": "lblUnlock",
            "isVisible": true,
            "left": "5px",
            "skin": "sknlblLatoBold35475f14px",
            "i18n_text": "kony.i18n.getLocalizedString(\"i18n.CustomerManagement.SendLinkToUnlock\")",
            "width": "100%",
            "zIndex": 1
        }, controller.args[0], "lblUnlock"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblUnlock"), extendConfig({}, controller.args[2], "lblUnlock"));
        flxUnlock.add(fonticonUnlock, lblUnlock);
        var flxLinkProfileButton = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "centerY": "50%",
            "clipBounds": true,
            "height": "22px",
            "id": "flxLinkProfileButton",
            "isVisible": false,
            "layoutType": kony.flex.FLOW_HORIZONTAL,
            "isModalContainer": false,
            "right": "20dp",
            "skin": "sknBackgroundColorUnlockCustomer",
            "width": "172dp",
            "zIndex": 1
        }, controller.args[0], "flxLinkProfileButton"), extendConfig({}, controller.args[1], "flxLinkProfileButton"), extendConfig({
            "hoverSkin": "sknHoverUnlock"
        }, controller.args[2], "flxLinkProfileButton"));
        flxLinkProfileButton.setDefaultUnit(kony.flex.DP);
        var lblIconLinkProfile = new kony.ui.Label(extendConfig({
            "centerY": "50%",
            "id": "lblIconLinkProfile",
            "isVisible": true,
            "left": "15px",
            "skin": "sknIcon485C7513px",
            "text": "",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, controller.args[0], "lblIconLinkProfile"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_CENTER,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblIconLinkProfile"), extendConfig({}, controller.args[2], "lblIconLinkProfile"));
        var lblLinkProfile = new kony.ui.Label(extendConfig({
            "centerY": "45%",
            "id": "lblLinkProfile",
            "isVisible": true,
            "left": "10px",
            "skin": "sknlblLatoBold35475f14px",
            "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmCustomerManagement.LinkBusinessProfile\")",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, controller.args[0], "lblLinkProfile"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblLinkProfile"), extendConfig({}, controller.args[2], "lblLinkProfile"));
        flxLinkProfileButton.add(lblIconLinkProfile, lblLinkProfile);
        var flxDelinkProfileButton = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "centerY": "50%",
            "clipBounds": true,
            "height": "22px",
            "id": "flxDelinkProfileButton",
            "isVisible": false,
            "layoutType": kony.flex.FLOW_HORIZONTAL,
            "isModalContainer": false,
            "right": "20dp",
            "skin": "sknBackgroundColorUnlockCustomer",
            "width": "148dp",
            "zIndex": 1
        }, controller.args[0], "flxDelinkProfileButton"), extendConfig({}, controller.args[1], "flxDelinkProfileButton"), extendConfig({
            "hoverSkin": "sknHoverUnlock"
        }, controller.args[2], "flxDelinkProfileButton"));
        flxDelinkProfileButton.setDefaultUnit(kony.flex.DP);
        var lblIconDelink = new kony.ui.Label(extendConfig({
            "centerY": "50%",
            "id": "lblIconDelink",
            "isVisible": true,
            "left": "14px",
            "skin": "sknFontIcon485C7518px",
            "text": "",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, controller.args[0], "lblIconDelink"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_CENTER,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblIconDelink"), extendConfig({}, controller.args[2], "lblIconDelink"));
        var lblDelinkProfile = new kony.ui.Label(extendConfig({
            "centerY": "45%",
            "id": "lblDelinkProfile",
            "isVisible": true,
            "left": "7px",
            "skin": "sknlblLatoBold35475f14px",
            "text": "De-Link Profiles",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, controller.args[0], "lblDelinkProfile"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblDelinkProfile"), extendConfig({}, controller.args[2], "lblDelinkProfile"));
        flxDelinkProfileButton.add(lblIconDelink, lblDelinkProfile);
        var flxCSRAssistNewApp = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "centerY": "50%",
            "clipBounds": true,
            "height": "22px",
            "id": "flxCSRAssistNewApp",
            "isVisible": false,
            "layoutType": kony.flex.FLOW_HORIZONTAL,
            "isModalContainer": false,
            "right": "20px",
            "skin": "sknFlxBg003E75Br1pxRd20px",
            "width": "140px",
            "zIndex": 1
        }, controller.args[0], "flxCSRAssistNewApp"), extendConfig({}, controller.args[1], "flxCSRAssistNewApp"), extendConfig({
            "hoverSkin": "sknFlxBg005198Br1pxRd20pxHov"
        }, controller.args[2], "flxCSRAssistNewApp"));
        flxCSRAssistNewApp.setDefaultUnit(kony.flex.DP);
        var fontIconCSRAssistNewApp = new kony.ui.Label(extendConfig({
            "centerY": "45%",
            "height": "15px",
            "id": "fontIconCSRAssistNewApp",
            "isVisible": true,
            "left": "10px",
            "skin": "sknIcon13pxWhite",
            "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmPermissions.fontIconPrevious\")",
            "width": "15px",
            "zIndex": 1
        }, controller.args[0], "fontIconCSRAssistNewApp"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "fontIconCSRAssistNewApp"), extendConfig({}, controller.args[2], "fontIconCSRAssistNewApp"));
        var lblflxCSRAssistNewApp = new kony.ui.Label(extendConfig({
            "centerY": "45%",
            "id": "lblflxCSRAssistNewApp",
            "isVisible": true,
            "left": "0",
            "skin": "sknlblCSRAssist",
            "i18n_text": "kony.i18n.getLocalizedString(\"i18n.userwidgetmodel.lblCSRAssistNewApp\")",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, controller.args[0], "lblflxCSRAssistNewApp"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblflxCSRAssistNewApp"), extendConfig({}, controller.args[2], "lblflxCSRAssistNewApp"));
        flxCSRAssistNewApp.add(fontIconCSRAssistNewApp, lblflxCSRAssistNewApp);
        flxActionsbtnGroup.add(flxOptions, flxCSRAssist, flxUnlock, flxLinkProfileButton, flxDelinkProfileButton, flxCSRAssistNewApp);
        flxActionButtons.add(flxActionsbtnGroup);
        flxTagsandbuttons.add(flxCustomerTags, flxActionButtons);
        var flxDefaultSearchHeader = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "clipBounds": true,
            "height": "40dp",
            "id": "flxDefaultSearchHeader",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "left": "0dp",
            "isModalContainer": false,
            "skin": "slFbox",
            "top": "0dp",
            "width": "100%",
            "zIndex": 1
        }, controller.args[0], "flxDefaultSearchHeader"), extendConfig({}, controller.args[1], "flxDefaultSearchHeader"), extendConfig({}, controller.args[2], "flxDefaultSearchHeader"));
        flxDefaultSearchHeader.setDefaultUnit(kony.flex.DP);
        var flxLeftDetailsCont = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "clipBounds": true,
            "height": "100%",
            "id": "flxLeftDetailsCont",
            "isVisible": true,
            "layoutType": kony.flex.FLOW_HORIZONTAL,
            "left": "0dp",
            "isModalContainer": false,
            "skin": "slFbox",
            "top": "0dp",
            "width": "70%"
        }, controller.args[0], "flxLeftDetailsCont"), extendConfig({}, controller.args[1], "flxLeftDetailsCont"), extendConfig({}, controller.args[2], "flxLeftDetailsCont"));
        flxLeftDetailsCont.setDefaultUnit(kony.flex.DP);
        var lblCustomerName = new kony.ui.Label(extendConfig({
            "centerY": "50%",
            "id": "lblCustomerName",
            "isVisible": true,
            "left": "0dp",
            "skin": "sknLatoRegular192B4518px",
            "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmLogs.lblCustomerName3\")",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, controller.args[0], "lblCustomerName"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblCustomerName"), extendConfig({}, controller.args[2], "lblCustomerName"));
        var fonticonActive = new kony.ui.Label(extendConfig({
            "centerY": "50%",
            "id": "fonticonActive",
            "isVisible": true,
            "left": "15dp",
            "skin": "sknFontIconActivate",
            "i18n_text": "kony.i18n.getLocalizedString(\"i18n.userwidgetmodel.lblFontIconStatus1\")",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 2
        }, controller.args[0], "fonticonActive"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "fonticonActive"), extendConfig({}, controller.args[2], "fonticonActive"));
        var lblStatus = new kony.ui.Label(extendConfig({
            "centerY": "50%",
            "id": "lblStatus",
            "isVisible": true,
            "left": "5px",
            "skin": "sknlblLato5bc06cBold14px",
            "i18n_text": "kony.i18n.getLocalizedString(\"i18n.secureimage.Active\")",
            "top": "0dp",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, controller.args[0], "lblStatus"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblStatus"), extendConfig({}, controller.args[2], "lblStatus"));
        flxLeftDetailsCont.add(lblCustomerName, fonticonActive, lblStatus);
        var flxRightDetailsCont = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "clipBounds": true,
            "height": "100%",
            "id": "flxRightDetailsCont",
            "isVisible": false,
            "layoutType": kony.flex.FREE_FORM,
            "isModalContainer": false,
            "right": "0dp",
            "skin": "slFbox",
            "top": "0",
            "width": "30%"
        }, controller.args[0], "flxRightDetailsCont"), extendConfig({}, controller.args[1], "flxRightDetailsCont"), extendConfig({}, controller.args[2], "flxRightDetailsCont"));
        flxRightDetailsCont.setDefaultUnit(kony.flex.DP);
        var btnEnrollNow = new kony.ui.Button(extendConfig({
            "focusSkin": "sknBtn003E75LatoRegular13pxFFFFFFRad20px",
            "height": "30dp",
            "id": "btnEnrollNow",
            "isVisible": true,
            "right": "1dp",
            "skin": "sknBtn003E75LatoRegular13pxFFFFFFRad20px",
            "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmEnrollCustomer.EnrollNow_UC\")",
            "top": "10dp",
            "width": "145px",
            "zIndex": 1
        }, controller.args[0], "btnEnrollNow"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_CENTER,
            "displayText": true,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "btnEnrollNow"), extendConfig({
            "hoverSkin": "sknBtn005198LatoRegular13pxFFFFFFRad20px"
        }, controller.args[2], "btnEnrollNow"));
        flxRightDetailsCont.add(btnEnrollNow);
        flxDefaultSearchHeader.add(flxLeftDetailsCont, flxRightDetailsCont);
        var flxRiskStatus = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "clipBounds": true,
            "height": "40px",
            "id": "flxRiskStatus",
            "isVisible": true,
            "layoutType": kony.flex.FLOW_HORIZONTAL,
            "left": "0dp",
            "isModalContainer": false,
            "skin": "slFbox",
            "top": "0dp",
            "width": "70%",
            "zIndex": 10
        }, controller.args[0], "flxRiskStatus"), extendConfig({}, controller.args[1], "flxRiskStatus"), extendConfig({}, controller.args[2], "flxRiskStatus"));
        flxRiskStatus.setDefaultUnit(kony.flex.DP);
        var fonticonRisk = new kony.ui.Label(extendConfig({
            "centerY": "50%",
            "id": "fonticonRisk",
            "isVisible": true,
            "left": "0px",
            "skin": "fonticonhighrisk",
            "i18n_text": "kony.i18n.getLocalizedString(\"i18n.userwidgetmodel.fonticonRisk\")",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, controller.args[0], "fonticonRisk"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "fonticonRisk"), extendConfig({}, controller.args[2], "fonticonRisk"));
        var lblRisk = new kony.ui.Label(extendConfig({
            "centerY": "50%",
            "id": "lblRisk",
            "isVisible": true,
            "left": "5dp",
            "skin": "slLabelf6b3133bfe92394b",
            "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmCustomerManagementController.High_Risk\")",
            "top": "0dp",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, controller.args[0], "lblRisk"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblRisk"), extendConfig({}, controller.args[2], "lblRisk"));
        var fonticonFraud = new kony.ui.Label(extendConfig({
            "centerY": "50%",
            "id": "fonticonFraud",
            "isVisible": true,
            "left": "25px",
            "skin": "fonticonfraud",
            "i18n_text": "kony.i18n.getLocalizedString(\"i18n.userwidgetmodel.fonticonFraud\")",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, controller.args[0], "fonticonFraud"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "fonticonFraud"), extendConfig({}, controller.args[2], "fonticonFraud"));
        var lblFraud = new kony.ui.Label(extendConfig({
            "centerY": "50%",
            "id": "lblFraud",
            "isVisible": true,
            "left": "5px",
            "skin": "sknLabelEE6565a2f3bb4d",
            "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmCustomerManagementController.Fraud_Detected\")",
            "top": "0dp",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, controller.args[0], "lblFraud"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblFraud"), extendConfig({}, controller.args[2], "lblFraud"));
        var fonticonDefaulter = new kony.ui.Label(extendConfig({
            "centerY": "50%",
            "id": "fonticonDefaulter",
            "isVisible": true,
            "left": "25px",
            "skin": "sknfontIcoDefaulter",
            "i18n_text": "kony.i18n.getLocalizedString(\"i18n.userwidgetmodel.fonticonDefaulter\")",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, controller.args[0], "fonticonDefaulter"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "fonticonDefaulter"), extendConfig({}, controller.args[2], "fonticonDefaulter"));
        var lblDefaulter = new kony.ui.Label(extendConfig({
            "centerY": "50%",
            "id": "lblDefaulter",
            "isVisible": true,
            "left": "5px",
            "skin": "sknlLabelfc6c21abe24974f",
            "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmCustomerManagementController.Defaulter\")",
            "top": "0dp",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, controller.args[0], "lblDefaulter"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblDefaulter"), extendConfig({}, controller.args[2], "lblDefaulter"));
        flxRiskStatus.add(fonticonRisk, lblRisk, fonticonFraud, lblFraud, fonticonDefaulter, lblDefaulter);
        var lblSeparator = new kony.ui.Label(extendConfig({
            "bottom": "0px",
            "height": "1px",
            "id": "lblSeparator",
            "isVisible": true,
            "left": 0,
            "right": 0,
            "skin": "sknConfigSeparator",
            "i18n_text": "kony.i18n.getLocalizedString(\"i18n.userwidgetmodel.lblHeaderSeparator\")",
            "width": "100%",
            "zIndex": 1
        }, controller.args[0], "lblSeparator"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblSeparator"), extendConfig({}, controller.args[2], "lblSeparator"));
        flxHeader.add(flxNotification, flxTagsandbuttons, flxDefaultSearchHeader, flxRiskStatus, lblSeparator);
        generalInfoHeader.add(flxHeader);
        return generalInfoHeader;
    }
})