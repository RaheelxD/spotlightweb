define(function() {
    return function(controller) {
        var EditAddress = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "clipBounds": true,
            "isMaster": true,
            "height": "100%",
            "id": "EditAddress",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "left": "0dp",
            "isModalContainer": false,
            "skin": "slFbox0hfd18814fd664dCM",
            "top": "0dp",
            "width": "100%"
        }, controller.args[0], "EditAddress"), extendConfig({}, controller.args[1], "EditAddress"), extendConfig({}, controller.args[2], "EditAddress"));
        EditAddress.setDefaultUnit(kony.flex.DP);
        var flxViewContact = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
            "clipBounds": true,
            "id": "flxViewContact",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "isModalContainer": false,
            "skin": "slFbox0f47e65b3392947CM",
            "width": "100%",
            "zIndex": 1
        }, controller.args[0], "flxViewContact"), extendConfig({}, controller.args[1], "flxViewContact"), extendConfig({}, controller.args[2], "flxViewContact"));
        flxViewContact.setDefaultUnit(kony.flex.DP);
        var flxContactHeader = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "clipBounds": true,
            "height": "50px",
            "id": "flxContactHeader",
            "isVisible": true,
            "layoutType": kony.flex.FLOW_HORIZONTAL,
            "left": "0px",
            "isModalContainer": false,
            "skin": "slFbox0g34dd780040e45CM",
            "top": "0dp",
            "width": "100%",
            "zIndex": 1
        }, controller.args[0], "flxContactHeader"), extendConfig({}, controller.args[1], "flxContactHeader"), extendConfig({}, controller.args[2], "flxContactHeader"));
        flxContactHeader.setDefaultUnit(kony.flex.DP);
        var lblContactHeader = new kony.ui.Label(extendConfig({
            "bottom": 0,
            "centerY": "50%",
            "id": "lblContactHeader",
            "isVisible": true,
            "left": "20dp",
            "skin": "sknlblLato0a2ef656a8dde46CN",
            "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmCustomerManagementController.Address\")",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, controller.args[0], "lblContactHeader"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblContactHeader"), extendConfig({}, controller.args[2], "lblContactHeader"));
        flxContactHeader.add(lblContactHeader);
        var flxAddressDetails = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "clipBounds": true,
            "height": "470px",
            "id": "flxAddressDetails",
            "isVisible": true,
            "layoutType": kony.flex.FLOW_VERTICAL,
            "left": "20px",
            "isModalContainer": false,
            "right": "20px",
            "skin": "slFbox",
            "top": "30px",
            "zIndex": 2
        }, controller.args[0], "flxAddressDetails"), extendConfig({}, controller.args[1], "flxAddressDetails"), extendConfig({}, controller.args[2], "flxAddressDetails"));
        flxAddressDetails.setDefaultUnit(kony.flex.DP);
        var flxRow1 = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "clipBounds": true,
            "height": "40px",
            "id": "flxRow1",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "left": "0dp",
            "isModalContainer": false,
            "skin": "slFbox",
            "top": "15px",
            "width": "100%",
            "zIndex": 2
        }, controller.args[0], "flxRow1"), extendConfig({}, controller.args[1], "flxRow1"), extendConfig({}, controller.args[2], "flxRow1"));
        flxRow1.setDefaultUnit(kony.flex.DP);
        var lblprimary1 = new kony.ui.Label(extendConfig({
            "centerY": "50%",
            "id": "lblprimary1",
            "isVisible": true,
            "left": "195px",
            "skin": "sknlblLatoRegular484b5213px",
            "i18n_text": "kony.i18n.getLocalizedString(\"i18n.userwidgetmodel.lblprimary1\")",
            "top": "2px",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, controller.args[0], "lblprimary1"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblprimary1"), extendConfig({}, controller.args[2], "lblprimary1"));
        var flxRadio1 = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "centerY": "50%",
            "clipBounds": true,
            "height": "20dp",
            "id": "flxRadio1",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "left": "170px",
            "isModalContainer": false,
            "skin": "sknCursor",
            "top": 0,
            "width": "20px",
            "zIndex": 1
        }, controller.args[0], "flxRadio1"), extendConfig({}, controller.args[1], "flxRadio1"), extendConfig({}, controller.args[2], "flxRadio1"));
        flxRadio1.setDefaultUnit(kony.flex.DP);
        var imgRadio1 = new kony.ui.Image2(extendConfig({
            "centerX": "50%",
            "centerY": "50%",
            "height": "15dp",
            "id": "imgRadio1",
            "isVisible": true,
            "skin": "slImage",
            "src": "radio_notselected.png",
            "width": "15px",
            "zIndex": 1
        }, controller.args[0], "imgRadio1"), extendConfig({
            "imageScaleMode": constants.IMAGE_SCALE_MODE_MAINTAIN_ASPECT_RATIO,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "imgRadio1"), extendConfig({}, controller.args[2], "imgRadio1"));
        flxRadio1.add(imgRadio1);
        var lblAddressType = new kony.ui.ListBox(extendConfig({
            "centerY": "50%",
            "focusSkin": "sknlbxBgffffffBorder11abebRadius3Px14px",
            "height": "40dp",
            "id": "lblAddressType",
            "isVisible": true,
            "left": "0px",
            "masterData": [
                ["type0", "Home"],
                ["type1", "Work"],
                ["type2", "Other"]
            ],
            "selectedKey": "type0",
            "skin": "sknlstbxNormal0f9abd8e88aa64a",
            "width": "150px",
            "zIndex": 2
        }, controller.args[0], "lblAddressType"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [1, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblAddressType"), extendConfig({
            "hoverSkin": "sknlstbxcursor",
            "multiSelect": false
        }, controller.args[2], "lblAddressType"));
        flxRow1.add(lblprimary1, flxRadio1, lblAddressType);
        var flxRow2 = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "clipBounds": true,
            "height": "170dp",
            "id": "flxRow2",
            "isVisible": true,
            "layoutType": kony.flex.FLOW_VERTICAL,
            "left": "0dp",
            "isModalContainer": false,
            "right": "35px",
            "skin": "slFbox",
            "top": "15px",
            "width": "100%",
            "zIndex": 2
        }, controller.args[0], "flxRow2"), extendConfig({}, controller.args[1], "flxRow2"), extendConfig({}, controller.args[2], "flxRow2"));
        flxRow2.setDefaultUnit(kony.flex.DP);
        var flxAddressLine1 = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "clipBounds": true,
            "height": "20px",
            "id": "flxAddressLine1",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "left": "0dp",
            "isModalContainer": false,
            "skin": "slFbox",
            "top": "0dp",
            "width": "100%"
        }, controller.args[0], "flxAddressLine1"), extendConfig({}, controller.args[1], "flxAddressLine1"), extendConfig({}, controller.args[2], "flxAddressLine1"));
        flxAddressLine1.setDefaultUnit(kony.flex.DP);
        var lblAddressLine1 = new kony.ui.Label(extendConfig({
            "id": "lblAddressLine1",
            "isVisible": true,
            "left": "0px",
            "skin": "sknlblLatoRegular00000013px",
            "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmCreateNewUser.lblAddressLine1User\")",
            "top": "0px",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 3
        }, controller.args[0], "lblAddressLine1"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblAddressLine1"), extendConfig({}, controller.args[2], "lblAddressLine1"));
        var lblAddressLine1Size = new kony.ui.Label(extendConfig({
            "id": "lblAddressLine1Size",
            "isVisible": true,
            "right": "0px",
            "skin": "sknllbl485c75Lato13px",
            "text": "0/100",
            "top": 0,
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 5
        }, controller.args[0], "lblAddressLine1Size"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblAddressLine1Size"), extendConfig({}, controller.args[2], "lblAddressLine1Size"));
        flxAddressLine1.add(lblAddressLine1, lblAddressLine1Size);
        var txtfldAddressLine1 = new kony.ui.TextBox2(extendConfig({
            "autoCapitalize": constants.TEXTBOX_AUTO_CAPITALIZE_NONE,
            "focusSkin": "skntxtbxDetailsFocus14px",
            "height": "40dp",
            "id": "txtfldAddressLine1",
            "isVisible": true,
            "keyBoardStyle": constants.TEXTBOX_KEY_BOARD_STYLE_DEFAULT,
            "left": "0dp",
            "maxTextLength": 100,
            "placeholder": "Address Line 1",
            "secureTextEntry": false,
            "skin": "skntxtbxDetails0bbf1235271384a",
            "textInputMode": constants.TEXTBOX_INPUT_MODE_ANY,
            "top": "5px",
            "width": "100%",
            "zIndex": 2
        }, controller.args[0], "txtfldAddressLine1"), extendConfig({
            "containerHeightMode": constants.TEXTBOX_FONT_METRICS_DRIVEN_HEIGHT,
            "contentAlignment": constants.CONTENT_ALIGN_TOP_LEFT,
            "padding": [1, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "txtfldAddressLine1"), extendConfig({
            "autoCorrect": false
        }, controller.args[2], "txtfldAddressLine1"));
        var lblErrorAddressLine1 = new kony.ui.Label(extendConfig({
            "bottom": "0px",
            "id": "lblErrorAddressLine1",
            "isVisible": false,
            "left": "0dp",
            "skin": "sknlblError",
            "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmCustomers.ErrorAddressLine1\")",
            "top": "5px",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, controller.args[0], "lblErrorAddressLine1"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblErrorAddressLine1"), extendConfig({}, controller.args[2], "lblErrorAddressLine1"));
        var flxAddrsssLine2 = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "clipBounds": true,
            "height": "20px",
            "id": "flxAddrsssLine2",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "left": "0dp",
            "isModalContainer": false,
            "skin": "slFbox",
            "top": "10dp",
            "width": "100%"
        }, controller.args[0], "flxAddrsssLine2"), extendConfig({}, controller.args[1], "flxAddrsssLine2"), extendConfig({}, controller.args[2], "flxAddrsssLine2"));
        flxAddrsssLine2.setDefaultUnit(kony.flex.DP);
        var lblAddressLine2 = new kony.ui.Label(extendConfig({
            "id": "lblAddressLine2",
            "isVisible": true,
            "left": "0px",
            "skin": "sknlblLatoRegular00000013px",
            "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmCreateNewUser.lblAddressLine2User\")",
            "top": "0px",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 3
        }, controller.args[0], "lblAddressLine2"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblAddressLine2"), extendConfig({}, controller.args[2], "lblAddressLine2"));
        var lblAddressLine2Size = new kony.ui.Label(extendConfig({
            "id": "lblAddressLine2Size",
            "isVisible": true,
            "right": "0px",
            "skin": "sknllbl485c75Lato13px",
            "text": "0/100",
            "top": "0px",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 5
        }, controller.args[0], "lblAddressLine2Size"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblAddressLine2Size"), extendConfig({}, controller.args[2], "lblAddressLine2Size"));
        flxAddrsssLine2.add(lblAddressLine2, lblAddressLine2Size);
        var txtfldAddressLine2 = new kony.ui.TextBox2(extendConfig({
            "autoCapitalize": constants.TEXTBOX_AUTO_CAPITALIZE_NONE,
            "focusSkin": "skntxtbxDetailsFocus14px",
            "height": "40dp",
            "id": "txtfldAddressLine2",
            "isVisible": true,
            "keyBoardStyle": constants.TEXTBOX_KEY_BOARD_STYLE_DEFAULT,
            "left": "0dp",
            "maxTextLength": 100,
            "placeholder": "Address Line 2",
            "secureTextEntry": false,
            "skin": "skntxtbxDetails0bbf1235271384a",
            "textInputMode": constants.TEXTBOX_INPUT_MODE_ANY,
            "top": "5px",
            "width": "100%",
            "zIndex": 2
        }, controller.args[0], "txtfldAddressLine2"), extendConfig({
            "containerHeightMode": constants.TEXTBOX_FONT_METRICS_DRIVEN_HEIGHT,
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [1, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "txtfldAddressLine2"), extendConfig({
            "autoCorrect": false
        }, controller.args[2], "txtfldAddressLine2"));
        flxRow2.add(flxAddressLine1, txtfldAddressLine1, lblErrorAddressLine1, flxAddrsssLine2, txtfldAddressLine2);
        var flxRow3 = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "clipBounds": true,
            "height": "88px",
            "id": "flxRow3",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "left": "0px",
            "isModalContainer": false,
            "skin": "sknNormalDefault",
            "top": "10dp",
            "width": "100%",
            "zIndex": 1
        }, controller.args[0], "flxRow3"), extendConfig({}, controller.args[1], "flxRow3"), extendConfig({}, controller.args[2], "flxRow3"));
        flxRow3.setDefaultUnit(kony.flex.DP);
        var flxAddressState = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "clipBounds": true,
            "height": "100%",
            "id": "flxAddressState",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "left": "34%",
            "isModalContainer": false,
            "skin": "slFbox",
            "top": "0dp",
            "width": "32%",
            "zIndex": 2
        }, controller.args[0], "flxAddressState"), extendConfig({}, controller.args[1], "flxAddressState"), extendConfig({}, controller.args[2], "flxAddressState"));
        flxAddressState.setDefaultUnit(kony.flex.DP);
        var lblState = new kony.ui.Label(extendConfig({
            "id": "lblState",
            "isVisible": true,
            "left": "0px",
            "skin": "sknlblLatoRegular00000013px",
            "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmCreateNewUser.lblStateUser\")",
            "top": "0px",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, controller.args[0], "lblState"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblState"), extendConfig({}, controller.args[2], "lblState"));
        var lstboxstate = new kony.ui.ListBox(extendConfig({
            "focusSkin": "sknlbxBgffffffBorder11abebRadius3Px14px",
            "height": "40dp",
            "id": "lstboxstate",
            "isVisible": true,
            "left": "0px",
            "masterData": [
                ["st0", "Select"],
                ["st1", "California"],
                ["st2", "Texas"],
                ["st3", "Andhra Pradesh"],
                ["st4", "Karnataka"],
                ["st5", "Goa"],
                ["st6", "Himachal Pradesh"]
            ],
            "selectedKey": "st0",
            "skin": "sknlstbxNormal0f9abd8e88aa64a",
            "top": "25dp",
            "width": "100%",
            "zIndex": 2
        }, controller.args[0], "lstboxstate"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [3, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lstboxstate"), extendConfig({
            "hoverSkin": "sknlstbxcursor",
            "multiSelect": false
        }, controller.args[2], "lstboxstate"));
        var lblErrorState = new kony.ui.Label(extendConfig({
            "bottom": "0px",
            "id": "lblErrorState",
            "isVisible": false,
            "left": "0dp",
            "skin": "sknlblError",
            "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmCustomers.ErrorState\")",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, controller.args[0], "lblErrorState"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblErrorState"), extendConfig({}, controller.args[2], "lblErrorState"));
        flxAddressState.add(lblState, lstboxstate, lblErrorState);
        var flxAddressZipCode = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "clipBounds": true,
            "height": "100%",
            "id": "flxAddressZipCode",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "left": "68%",
            "isModalContainer": false,
            "skin": "slFbox",
            "top": "0dp",
            "width": "32%",
            "zIndex": 2
        }, controller.args[0], "flxAddressZipCode"), extendConfig({}, controller.args[1], "flxAddressZipCode"), extendConfig({}, controller.args[2], "flxAddressZipCode"));
        flxAddressZipCode.setDefaultUnit(kony.flex.DP);
        var lblzipCode = new kony.ui.Label(extendConfig({
            "id": "lblzipCode",
            "isVisible": true,
            "left": "0px",
            "skin": "sknlblLatoRegular00000013px",
            "i18n_text": "kony.i18n.getLocalizedString(\"i18n.userwidgetmodel.lblzipCode\")",
            "top": "0px",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, controller.args[0], "lblzipCode"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblzipCode"), extendConfig({}, controller.args[2], "lblzipCode"));
        var txtfldZipCode = new kony.ui.TextBox2(extendConfig({
            "autoCapitalize": constants.TEXTBOX_AUTO_CAPITALIZE_NONE,
            "focusSkin": "skntxtbxDetailsFocus14px",
            "height": "40px",
            "id": "txtfldZipCode",
            "isVisible": true,
            "keyBoardStyle": constants.TEXTBOX_KEY_BOARD_STYLE_DEFAULT,
            "left": "0%",
            "i18n_placeholder": "kony.i18n.getLocalizedString(\"i18n.userwidgetmodel.lblzipCode\")",
            "secureTextEntry": false,
            "skin": "skntxtbxDetails0bbf1235271384a",
            "textInputMode": constants.TEXTBOX_INPUT_MODE_ANY,
            "top": "25px",
            "width": "100%",
            "zIndex": 2
        }, controller.args[0], "txtfldZipCode"), extendConfig({
            "containerHeightMode": constants.TEXTBOX_FONT_METRICS_DRIVEN_HEIGHT,
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [3, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "txtfldZipCode"), extendConfig({
            "autoCorrect": false
        }, controller.args[2], "txtfldZipCode"));
        var lblErrorZipcode = new kony.ui.Label(extendConfig({
            "bottom": "0px",
            "id": "lblErrorZipcode",
            "isVisible": false,
            "left": "0dp",
            "skin": "sknlblError",
            "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmCustomers.ErrorZipcode\")",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, controller.args[0], "lblErrorZipcode"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblErrorZipcode"), extendConfig({}, controller.args[2], "lblErrorZipcode"));
        var lblZipcodeSize = new kony.ui.Label(extendConfig({
            "id": "lblZipcodeSize",
            "isVisible": true,
            "right": "0px",
            "skin": "sknllbl485c75Lato13px",
            "text": "0/20",
            "top": "0px",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 5
        }, controller.args[0], "lblZipcodeSize"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblZipcodeSize"), extendConfig({}, controller.args[2], "lblZipcodeSize"));
        flxAddressZipCode.add(lblzipCode, txtfldZipCode, lblErrorZipcode, lblZipcodeSize);
        var flxAddressCountry = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "clipBounds": true,
            "height": "100%",
            "id": "flxAddressCountry",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "left": "0dp",
            "isModalContainer": false,
            "skin": "slFbox",
            "top": "0dp",
            "width": "32%",
            "zIndex": 2
        }, controller.args[0], "flxAddressCountry"), extendConfig({}, controller.args[1], "flxAddressCountry"), extendConfig({}, controller.args[2], "flxAddressCountry"));
        flxAddressCountry.setDefaultUnit(kony.flex.DP);
        var lblCoountry = new kony.ui.Label(extendConfig({
            "id": "lblCoountry",
            "isVisible": true,
            "left": "0px",
            "skin": "sknlblLatoRegular00000013px",
            "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmCreateNewUser.lblCountryUser\")",
            "top": "0px",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, controller.args[0], "lblCoountry"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblCoountry"), extendConfig({}, controller.args[2], "lblCoountry"));
        var lstboxCountry = new kony.ui.ListBox(extendConfig({
            "focusSkin": "sknlbxBgffffffBorder11abebRadius3Px14px",
            "height": "40dp",
            "id": "lstboxCountry",
            "isVisible": true,
            "left": "0px",
            "masterData": [
                ["ct0", "Select"],
                ["ct1", "United States"],
                ["ct2", "Canada"],
                ["ct3", "India"],
                ["ct4", "Germany"]
            ],
            "selectedKey": "ct0",
            "skin": "sknlstbxNormal0f9abd8e88aa64a",
            "top": "25dp",
            "width": "100%",
            "zIndex": 2
        }, controller.args[0], "lstboxCountry"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [3, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lstboxCountry"), extendConfig({
            "hoverSkin": "sknlstbxcursor",
            "multiSelect": false
        }, controller.args[2], "lstboxCountry"));
        var lblErrorCountry = new kony.ui.Label(extendConfig({
            "bottom": "0px",
            "id": "lblErrorCountry",
            "isVisible": false,
            "left": "0dp",
            "skin": "sknlblError",
            "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmCustomers.ErrorCountry\")",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, controller.args[0], "lblErrorCountry"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblErrorCountry"), extendConfig({}, controller.args[2], "lblErrorCountry"));
        flxAddressCountry.add(lblCoountry, lstboxCountry, lblErrorCountry);
        flxRow3.add(flxAddressState, flxAddressZipCode, flxAddressCountry);
        var flxRow4 = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "clipBounds": true,
            "height": "88px",
            "id": "flxRow4",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "left": "0px",
            "isModalContainer": false,
            "skin": "sknNormalDefault",
            "top": "15dp",
            "width": "100%",
            "zIndex": 1
        }, controller.args[0], "flxRow4"), extendConfig({}, controller.args[1], "flxRow4"), extendConfig({}, controller.args[2], "flxRow4"));
        flxRow4.setDefaultUnit(kony.flex.DP);
        var flxAddressCity = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "clipBounds": true,
            "height": "100%",
            "id": "flxAddressCity",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "left": "0dp",
            "isModalContainer": false,
            "skin": "slFbox",
            "top": "0dp",
            "width": "32%",
            "zIndex": 2
        }, controller.args[0], "flxAddressCity"), extendConfig({}, controller.args[1], "flxAddressCity"), extendConfig({}, controller.args[2], "flxAddressCity"));
        flxAddressCity.setDefaultUnit(kony.flex.DP);
        var lblContactParam3 = new kony.ui.Label(extendConfig({
            "id": "lblContactParam3",
            "isVisible": true,
            "left": "0px",
            "skin": "sknlblLatoRegular00000013px",
            "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmCreateNewUser.lblCityUser\")",
            "top": "0px",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, controller.args[0], "lblContactParam3"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblContactParam3"), extendConfig({}, controller.args[2], "lblContactParam3"));
        var lstboxCity = new kony.ui.ListBox(extendConfig({
            "focusSkin": "sknlbxBgffffffBorder11abebRadius3Px14px",
            "height": "40dp",
            "id": "lstboxCity",
            "isVisible": false,
            "left": "0px",
            "masterData": [
                ["cty0", "Select"],
                ["cty1", "Mumbai"],
                ["cty2", "Hderabad"],
                ["cty3", "Delhi"],
                ["cty4", "Austin"]
            ],
            "selectedKey": "cty0",
            "skin": "sknlstbxNormal0f9abd8e88aa64a",
            "top": "25dp",
            "width": "100%",
            "zIndex": 2
        }, controller.args[0], "lstboxCity"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [3, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lstboxCity"), extendConfig({
            "hoverSkin": "sknlstbxcursor",
            "multiSelect": false
        }, controller.args[2], "lstboxCity"));
        var txtboxCity = new kony.ui.TextBox2(extendConfig({
            "autoCapitalize": constants.TEXTBOX_AUTO_CAPITALIZE_NONE,
            "focusSkin": "defTextBoxFocus",
            "height": "40dp",
            "id": "txtboxCity",
            "isVisible": true,
            "keyBoardStyle": constants.TEXTBOX_KEY_BOARD_STYLE_DEFAULT,
            "left": "0px",
            "placeholder": "Enter City",
            "secureTextEntry": false,
            "skin": "sknTbxBgFFFFFFBrD7D9E01pxR3px",
            "textInputMode": constants.TEXTBOX_INPUT_MODE_ANY,
            "top": "25dp",
            "width": "100%",
            "zIndex": 2
        }, controller.args[0], "txtboxCity"), extendConfig({
            "containerHeightMode": constants.TEXTBOX_FONT_METRICS_DRIVEN_HEIGHT,
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [3, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "txtboxCity"), extendConfig({
            "autoCorrect": false,
            "placeholderSkin": "defTextBoxPlaceholder"
        }, controller.args[2], "txtboxCity"));
        var lblErrorCity = new kony.ui.Label(extendConfig({
            "bottom": "0px",
            "id": "lblErrorCity",
            "isVisible": false,
            "left": "0dp",
            "skin": "sknlblError",
            "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmCustomers.ErrorCity\")",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, controller.args[0], "lblErrorCity"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblErrorCity"), extendConfig({}, controller.args[2], "lblErrorCity"));
        flxAddressCity.add(lblContactParam3, lstboxCity, txtboxCity, lblErrorCity);
        flxRow4.add(flxAddressCity);
        flxAddressDetails.add(flxRow1, flxRow2, flxRow3, flxRow4);
        flxViewContact.add(flxContactHeader, flxAddressDetails);
        EditAddress.add(flxViewContact);
        return EditAddress;
    }
})