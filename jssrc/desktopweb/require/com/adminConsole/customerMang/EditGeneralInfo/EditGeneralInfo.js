define(function() {
    return function(controller) {
        var EditGeneralInfo = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
            "clipBounds": true,
            "isMaster": true,
            "id": "EditGeneralInfo",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "left": "0dp",
            "isModalContainer": false,
            "preShow": function(eventobject) {
                controller.AS_FlexContainer_ba7818631ed740b9b8f871667b666d56(eventobject);
            },
            "right": "0px",
            "skin": "slFbox0hfd18814fd664dCM"
        }, controller.args[0], "EditGeneralInfo"), extendConfig({}, controller.args[1], "EditGeneralInfo"), extendConfig({}, controller.args[2], "EditGeneralInfo"));
        EditGeneralInfo.setDefaultUnit(kony.flex.DP);
        var flxGeneralDetails = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
            "clipBounds": true,
            "id": "flxGeneralDetails",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "left": "20px",
            "isModalContainer": false,
            "right": "20px",
            "skin": "slFbox",
            "top": "0px",
            "zIndex": 2
        }, controller.args[0], "flxGeneralDetails"), extendConfig({}, controller.args[1], "flxGeneralDetails"), extendConfig({}, controller.args[2], "flxGeneralDetails"));
        flxGeneralDetails.setDefaultUnit(kony.flex.DP);
        var flxRow1 = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "clipBounds": true,
            "height": "100px",
            "id": "flxRow1",
            "isVisible": true,
            "layoutType": kony.flex.FLOW_HORIZONTAL,
            "left": "0px",
            "isModalContainer": false,
            "skin": "sknNormalDefault",
            "top": "0px",
            "width": "100%",
            "zIndex": 1
        }, controller.args[0], "flxRow1"), extendConfig({}, controller.args[1], "flxRow1"), extendConfig({}, controller.args[2], "flxRow1"));
        flxRow1.setDefaultUnit(kony.flex.DP);
        var flxDetails1 = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
            "clipBounds": true,
            "id": "flxDetails1",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "left": "0%",
            "isModalContainer": false,
            "skin": "slFbox",
            "top": "0dp",
            "width": "32%",
            "zIndex": 2
        }, controller.args[0], "flxDetails1"), extendConfig({}, controller.args[1], "flxDetails1"), extendConfig({}, controller.args[2], "flxDetails1"));
        flxDetails1.setDefaultUnit(kony.flex.DP);
        var lblContactDetails1 = new kony.ui.Label(extendConfig({
            "id": "lblContactDetails1",
            "isVisible": true,
            "left": "0px",
            "skin": "sknLbl485C75LatoRegular13Px",
            "i18n_text": "kony.i18n.getLocalizedString(\"i18n.userwidgetmodel.lblContactDetails1\")",
            "top": "5px",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, controller.args[0], "lblContactDetails1"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblContactDetails1"), extendConfig({}, controller.args[2], "lblContactDetails1"));
        var lstboxDetails1 = new kony.ui.ListBox(extendConfig({
            "focusSkin": "sknlbxBgffffffBorder11abebRadius3Px14px",
            "height": "40dp",
            "id": "lstboxDetails1",
            "isVisible": true,
            "left": "0px",
            "masterData": [
                ["k1", "Select Salutation"],
                ["k2", "Mrs."],
                ["k3", "Dr."],
                ["k4", "Ms."]
            ],
            "selectedKey": "k1",
            "skin": "sknLbxborderd7d9e03pxradius",
            "top": "30dp",
            "width": "100%",
            "zIndex": 2
        }, controller.args[0], "lstboxDetails1"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [3, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lstboxDetails1"), extendConfig({
            "hoverSkin": "sknlstbxcursor",
            "multiSelect": false
        }, controller.args[2], "lstboxDetails1"));
        var lblError1 = new kony.ui.Label(extendConfig({
            "bottom": "0px",
            "id": "lblError1",
            "isVisible": true,
            "left": "0dp",
            "skin": "sknlblError",
            "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmCustomerManagementController.selectSalutaion\")",
            "top": 75,
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, controller.args[0], "lblError1"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblError1"), extendConfig({}, controller.args[2], "lblError1"));
        flxDetails1.add(lblContactDetails1, lstboxDetails1, lblError1);
        var flxDetails9 = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
            "clipBounds": true,
            "id": "flxDetails9",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "left": "2%",
            "isModalContainer": false,
            "skin": "slFbox",
            "top": "0dp",
            "width": "32%",
            "zIndex": 2
        }, controller.args[0], "flxDetails9"), extendConfig({}, controller.args[1], "flxDetails9"), extendConfig({}, controller.args[2], "flxDetails9"));
        flxDetails9.setDefaultUnit(kony.flex.DP);
        var lblDetails9 = new kony.ui.Label(extendConfig({
            "id": "lblDetails9",
            "isVisible": true,
            "left": "0px",
            "skin": "sknLbl485C75LatoRegular13Px",
            "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmCustomerManagementController.Customer_Name\")",
            "top": "5px",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, controller.args[0], "lblDetails9"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblDetails9"), extendConfig({}, controller.args[2], "lblDetails9"));
        var lblDetails9Name = new kony.ui.Label(extendConfig({
            "height": "40px",
            "id": "lblDetails9Name",
            "isVisible": true,
            "left": "0px",
            "skin": "sknLblBGf3f3f3Borderd79e0",
            "text": "Label",
            "top": "30dp",
            "width": "100%",
            "zIndex": 2
        }, controller.args[0], "lblDetails9Name"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [3, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblDetails9Name"), extendConfig({}, controller.args[2], "lblDetails9Name"));
        flxDetails9.add(lblDetails9, lblDetails9Name);
        var flxDetails3 = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
            "clipBounds": true,
            "id": "flxDetails3",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "left": "2%",
            "isModalContainer": false,
            "skin": "slFbox",
            "top": "0dp",
            "width": "32%",
            "zIndex": 2
        }, controller.args[0], "flxDetails3"), extendConfig({}, controller.args[1], "flxDetails3"), extendConfig({}, controller.args[2], "flxDetails3"));
        flxDetails3.setDefaultUnit(kony.flex.DP);
        var lblDetails3 = new kony.ui.Label(extendConfig({
            "id": "lblDetails3",
            "isVisible": true,
            "left": "0px",
            "skin": "sknLbl485C75LatoRegular13Px",
            "i18n_text": "kony.i18n.getLocalizedString(\"i18n.userwidgetmodel.lblDetails3\")",
            "top": "5px",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, controller.args[0], "lblDetails3"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblDetails3"), extendConfig({}, controller.args[2], "lblDetails3"));
        var lstboxDetails3 = new kony.ui.ListBox(extendConfig({
            "focusSkin": "sknlbxBgffffffBorder11abebRadius3Px14px",
            "height": "40dp",
            "id": "lstboxDetails3",
            "isVisible": true,
            "left": "0px",
            "masterData": [
                ["k1", "Married"],
                ["k2", "Unmarried"],
                ["k3", "Divorced"],
                ["k4", "Widowed"],
                ["k5", "Unknown"]
            ],
            "selectedKey": "k1",
            "skin": "sknLbxborderd7d9e03pxradius",
            "top": "30dp",
            "width": "100%",
            "zIndex": 2
        }, controller.args[0], "lstboxDetails3"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [3, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lstboxDetails3"), extendConfig({
            "hoverSkin": "sknlstbxcursor",
            "multiSelect": false
        }, controller.args[2], "lstboxDetails3"));
        var lblError3 = new kony.ui.Label(extendConfig({
            "bottom": "0px",
            "id": "lblError3",
            "isVisible": true,
            "left": "0dp",
            "skin": "sknlblError",
            "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmCustomerManagementController.SelectAStatus\")",
            "top": 75,
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, controller.args[0], "lblError3"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblError3"), extendConfig({}, controller.args[2], "lblError3"));
        flxDetails3.add(lblDetails3, lstboxDetails3, lblError3);
        flxRow1.add(flxDetails1, flxDetails9, flxDetails3);
        var flxRow2 = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "clipBounds": true,
            "height": "65px",
            "id": "flxRow2",
            "isVisible": false,
            "layoutType": kony.flex.FREE_FORM,
            "left": "0px",
            "isModalContainer": false,
            "skin": "sknNormalDefault",
            "top": "25dp",
            "width": "100%",
            "zIndex": 1
        }, controller.args[0], "flxRow2"), extendConfig({}, controller.args[1], "flxRow2"), extendConfig({}, controller.args[2], "flxRow2"));
        flxRow2.setDefaultUnit(kony.flex.DP);
        var flxDetails5 = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
            "clipBounds": true,
            "id": "flxDetails5",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "left": "34%",
            "isModalContainer": false,
            "skin": "slFbox",
            "top": "0dp",
            "width": "32%",
            "zIndex": 2
        }, controller.args[0], "flxDetails5"), extendConfig({}, controller.args[1], "flxDetails5"), extendConfig({}, controller.args[2], "flxDetails5"));
        flxDetails5.setDefaultUnit(kony.flex.DP);
        var lblContactDetails5 = new kony.ui.Label(extendConfig({
            "id": "lblContactDetails5",
            "isVisible": true,
            "left": "0px",
            "skin": "sknLbl485C75LatoRegular13Px",
            "i18n_text": "kony.i18n.getLocalizedString(\"i18n.userwidgetmodel.lblContactDetails5\")",
            "top": "0px",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, controller.args[0], "lblContactDetails5"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblContactDetails5"), extendConfig({}, controller.args[2], "lblContactDetails5"));
        var lstboxDetails5 = new kony.ui.ListBox(extendConfig({
            "focusSkin": "sknlbxBgffffffBorder11abebRadius3Px14px",
            "height": "40dp",
            "id": "lstboxDetails5",
            "isVisible": true,
            "left": "0px",
            "masterData": [
                ["k1", "Employed"],
                ["k2", "Unemployed"],
                ["k3", "Retired"],
                ["k4", "Student"]
            ],
            "selectedKey": "k1",
            "skin": "sknLbxborderd7d9e03pxradius",
            "top": "25dp",
            "width": "100%",
            "zIndex": 2
        }, controller.args[0], "lstboxDetails5"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lstboxDetails5"), extendConfig({
            "multiSelect": false
        }, controller.args[2], "lstboxDetails5"));
        flxDetails5.add(lblContactDetails5, lstboxDetails5);
        flxRow2.add(flxDetails5);
        var flxRow4 = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "clipBounds": true,
            "height": "100px",
            "id": "flxRow4",
            "isVisible": true,
            "layoutType": kony.flex.FLOW_HORIZONTAL,
            "left": "0px",
            "isModalContainer": false,
            "skin": "slFbox",
            "top": "105px",
            "width": "100%",
            "zIndex": 1
        }, controller.args[0], "flxRow4"), extendConfig({}, controller.args[1], "flxRow4"), extendConfig({}, controller.args[2], "flxRow4"));
        flxRow4.setDefaultUnit(kony.flex.DP);
        var flxDetails4 = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
            "clipBounds": true,
            "id": "flxDetails4",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "left": "0px",
            "isModalContainer": false,
            "skin": "slFbox",
            "top": "0dp",
            "width": "32%",
            "zIndex": 2
        }, controller.args[0], "flxDetails4"), extendConfig({}, controller.args[1], "flxDetails4"), extendConfig({}, controller.args[2], "flxDetails4"));
        flxDetails4.setDefaultUnit(kony.flex.DP);
        var lblContactDetails4 = new kony.ui.Label(extendConfig({
            "id": "lblContactDetails4",
            "isVisible": true,
            "left": "0px",
            "skin": "sknLbl485C75LatoRegular13Px",
            "i18n_text": "kony.i18n.getLocalizedString(\"i18n.userwidgetmodel.lblContactDetails4\")",
            "top": "5px",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, controller.args[0], "lblContactDetails4"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblContactDetails4"), extendConfig({}, controller.args[2], "lblContactDetails4"));
        var lstboxDetails4 = new kony.ui.ListBox(extendConfig({
            "focusSkin": "sknlbxBgffffffBorder11abebRadius3Px14px",
            "height": "40dp",
            "id": "lstboxDetails4",
            "isVisible": true,
            "masterData": [
                ["k1", "Employed"],
                ["k2", "Unemployed"],
                ["k3", "Retired"],
                ["k4", "Student"]
            ],
            "selectedKey": "k1",
            "skin": "sknLbxborderd7d9e03pxradius",
            "top": "30dp",
            "width": "100%",
            "zIndex": 2
        }, controller.args[0], "lstboxDetails4"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [3, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lstboxDetails4"), extendConfig({
            "multiSelect": false
        }, controller.args[2], "lstboxDetails4"));
        var lblError4 = new kony.ui.Label(extendConfig({
            "bottom": "0px",
            "id": "lblError4",
            "isVisible": true,
            "left": "0dp",
            "skin": "sknlblError",
            "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmCustomerManagementController.SelectAStatus\")",
            "top": "75dp",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, controller.args[0], "lblError4"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblError4"), extendConfig({}, controller.args[2], "lblError4"));
        flxDetails4.add(lblContactDetails4, lstboxDetails4, lblError4);
        var flxDetails2 = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
            "clipBounds": true,
            "id": "flxDetails2",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "left": "2%",
            "isModalContainer": false,
            "skin": "slFbox",
            "top": "0dp",
            "width": "32%",
            "zIndex": 2
        }, controller.args[0], "flxDetails2"), extendConfig({}, controller.args[1], "flxDetails2"), extendConfig({}, controller.args[2], "flxDetails2"));
        flxDetails2.setDefaultUnit(kony.flex.DP);
        var lblDetails2 = new kony.ui.Label(extendConfig({
            "id": "lblDetails2",
            "isVisible": true,
            "left": "0px",
            "skin": "sknLbl485C75LatoRegular13Px",
            "i18n_text": "kony.i18n.getLocalizedString(\"i18n.userwidgetmodel.lblCol13\")",
            "top": "5px",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, controller.args[0], "lblDetails2"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblDetails2"), extendConfig({}, controller.args[2], "lblDetails2"));
        var lstboxDetails2 = new kony.ui.ListBox(extendConfig({
            "focusSkin": "sknlbxBgffffffBorder11abebRadius3Px14px",
            "height": "40dp",
            "id": "lstboxDetails2",
            "isVisible": true,
            "left": "0px",
            "masterData": [
                ["key1", "Active"],
                ["key2", "Suspended"]
            ],
            "skin": "sknLbxborderd7d9e03pxradius",
            "top": "30dp",
            "width": "100%",
            "zIndex": 2
        }, controller.args[0], "lstboxDetails2"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [3, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lstboxDetails2"), extendConfig({
            "hoverSkin": "sknlstbxcursor",
            "multiSelect": false
        }, controller.args[2], "lstboxDetails2"));
        var lblError2 = new kony.ui.Label(extendConfig({
            "bottom": "0px",
            "id": "lblError2",
            "isVisible": true,
            "left": "0dp",
            "skin": "sknlblError",
            "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmCustomerManagementController.SelectAStatus\")",
            "top": "75dp",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, controller.args[0], "lblError2"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblError2"), extendConfig({}, controller.args[2], "lblError2"));
        flxDetails2.add(lblDetails2, lstboxDetails2, lblError2);
        flxRow4.add(flxDetails4, flxDetails2);
        var flxRow3 = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "clipBounds": true,
            "height": "100px",
            "id": "flxRow3",
            "isVisible": true,
            "layoutType": kony.flex.FLOW_HORIZONTAL,
            "left": "0px",
            "isModalContainer": false,
            "skin": "sknNormalDefault",
            "top": "210px",
            "width": "100%",
            "zIndex": 1
        }, controller.args[0], "flxRow3"), extendConfig({}, controller.args[1], "flxRow3"), extendConfig({}, controller.args[2], "flxRow3"));
        flxRow3.setDefaultUnit(kony.flex.DP);
        var flxDetails8 = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
            "clipBounds": true,
            "id": "flxDetails8",
            "isVisible": true,
            "layoutType": kony.flex.FLOW_VERTICAL,
            "left": "0%",
            "isModalContainer": false,
            "skin": "slFbox",
            "top": "0dp",
            "width": "42%"
        }, controller.args[0], "flxDetails8"), extendConfig({}, controller.args[1], "flxDetails8"), extendConfig({}, controller.args[2], "flxDetails8"));
        flxDetails8.setDefaultUnit(kony.flex.DP);
        var lblRisksHeader = new kony.ui.Label(extendConfig({
            "id": "lblRisksHeader",
            "isVisible": true,
            "left": "0px",
            "skin": "sknLbl485C75LatoRegular13Px",
            "i18n_text": "kony.i18n.getLocalizedString(\"i18n.userwidgetmodel.lblDetails8\")",
            "top": "0px",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, controller.args[0], "lblRisksHeader"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblRisksHeader"), extendConfig({}, controller.args[2], "lblRisksHeader"));
        var flxSubDetails8 = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "clipBounds": true,
            "height": "36px",
            "id": "flxSubDetails8",
            "isVisible": true,
            "layoutType": kony.flex.FLOW_HORIZONTAL,
            "left": "0dp",
            "isModalContainer": false,
            "skin": "sknflxOuterBorder",
            "top": "10px",
            "width": "100%",
            "zIndex": 2
        }, controller.args[0], "flxSubDetails8"), extendConfig({}, controller.args[1], "flxSubDetails8"), extendConfig({}, controller.args[2], "flxSubDetails8"));
        flxSubDetails8.setDefaultUnit(kony.flex.DP);
        var flxRiskStatus = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "clipBounds": true,
            "height": "36dp",
            "id": "flxRiskStatus",
            "isVisible": false,
            "layoutType": kony.flex.FLOW_HORIZONTAL,
            "left": "0dp",
            "isModalContainer": false,
            "skin": "sknflxfdf6f1bg100px",
            "top": "0dp",
            "width": "100dp",
            "zIndex": 111
        }, controller.args[0], "flxRiskStatus"), extendConfig({}, controller.args[1], "flxRiskStatus"), extendConfig({}, controller.args[2], "flxRiskStatus"));
        flxRiskStatus.setDefaultUnit(kony.flex.DP);
        var lblDetails8 = new kony.ui.Label(extendConfig({
            "centerX": "50%",
            "centerY": "50%",
            "id": "lblDetails8",
            "isVisible": true,
            "skin": "sknlblLatoBold35475f14px",
            "i18n_text": "kony.i18n.getLocalizedString(\"i18n.userwidgetmodel.lblDetails8\")",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, controller.args[0], "lblDetails8"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblDetails8"), extendConfig({}, controller.args[2], "lblDetails8"));
        flxRiskStatus.add(lblDetails8);
        var flxFlagSeperator = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "clipBounds": true,
            "height": "36dp",
            "id": "flxFlagSeperator",
            "isVisible": false,
            "layoutType": kony.flex.FREE_FORM,
            "left": "0dp",
            "isModalContainer": false,
            "skin": "sknflxOuterBorder",
            "top": "0dp",
            "width": "1px",
            "zIndex": 111
        }, controller.args[0], "flxFlagSeperator"), extendConfig({}, controller.args[1], "flxFlagSeperator"), extendConfig({}, controller.args[2], "flxFlagSeperator"));
        flxFlagSeperator.setDefaultUnit(kony.flex.DP);
        flxFlagSeperator.add();
        var flxSelectFlags = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "clipBounds": false,
            "height": "36px",
            "id": "flxSelectFlags",
            "isVisible": true,
            "layoutType": kony.flex.FLOW_HORIZONTAL,
            "left": "0dp",
            "isModalContainer": false,
            "right": "0dp",
            "skin": "slFbox",
            "top": "0px",
            "width": "100%",
            "zIndex": 111
        }, controller.args[0], "flxSelectFlags"), extendConfig({}, controller.args[1], "flxSelectFlags"), extendConfig({}, controller.args[2], "flxSelectFlags"));
        flxSelectFlags.setDefaultUnit(kony.flex.DP);
        var flxFlag3 = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "centerY": "50%",
            "clipBounds": true,
            "height": "13dp",
            "id": "flxFlag3",
            "isVisible": true,
            "layoutType": kony.flex.FLOW_HORIZONTAL,
            "left": "20px",
            "isModalContainer": false,
            "skin": "sknCursor",
            "top": "0dp",
            "width": "13px",
            "zIndex": 1
        }, controller.args[0], "flxFlag3"), extendConfig({}, controller.args[1], "flxFlag3"), extendConfig({}, controller.args[2], "flxFlag3"));
        flxFlag3.setDefaultUnit(kony.flex.DP);
        var imgFlag3 = new kony.ui.Image2(extendConfig({
            "centerX": "50%",
            "centerY": "50%",
            "height": "13dp",
            "id": "imgFlag3",
            "isVisible": true,
            "left": "15dp",
            "skin": "slImage",
            "src": "checkbox.png",
            "width": "13dp",
            "zIndex": 1
        }, controller.args[0], "imgFlag3"), extendConfig({
            "imageScaleMode": constants.IMAGE_SCALE_MODE_FIT_TO_DIMENSIONS,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "imgFlag3"), extendConfig({
            "toolTip": "Delete"
        }, controller.args[2], "imgFlag3"));
        flxFlag3.add(imgFlag3);
        var lblFlag3 = new kony.ui.Label(extendConfig({
            "centerY": "50%",
            "id": "lblFlag3",
            "isVisible": true,
            "left": "6px",
            "skin": "sknLbl485C75LatoRegular13Px",
            "i18n_text": "kony.i18n.getLocalizedString(\"i18n.userwidgetmodel.lblFlag3\")",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, controller.args[0], "lblFlag3"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblFlag3"), extendConfig({}, controller.args[2], "lblFlag3"));
        var flxFlag2 = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "centerY": "50%",
            "clipBounds": true,
            "height": "13dp",
            "id": "flxFlag2",
            "isVisible": true,
            "layoutType": kony.flex.FLOW_HORIZONTAL,
            "left": "10%",
            "isModalContainer": false,
            "skin": "sknCursor",
            "top": "0dp",
            "width": "13px",
            "zIndex": 1
        }, controller.args[0], "flxFlag2"), extendConfig({}, controller.args[1], "flxFlag2"), extendConfig({}, controller.args[2], "flxFlag2"));
        flxFlag2.setDefaultUnit(kony.flex.DP);
        var imgFlag2 = new kony.ui.Image2(extendConfig({
            "centerX": "50%",
            "centerY": "50%",
            "height": "13dp",
            "id": "imgFlag2",
            "isVisible": true,
            "skin": "slImage",
            "src": "checkbox.png",
            "width": "13dp",
            "zIndex": 1
        }, controller.args[0], "imgFlag2"), extendConfig({
            "imageScaleMode": constants.IMAGE_SCALE_MODE_FIT_TO_DIMENSIONS,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "imgFlag2"), extendConfig({
            "toolTip": "Deactive"
        }, controller.args[2], "imgFlag2"));
        flxFlag2.add(imgFlag2);
        var lblFlag2 = new kony.ui.Label(extendConfig({
            "centerY": "50%",
            "id": "lblFlag2",
            "isVisible": true,
            "left": "6px",
            "skin": "sknLbl485C75LatoRegular13Px",
            "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmCustomerManagementController.Defaulter\")",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, controller.args[0], "lblFlag2"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblFlag2"), extendConfig({}, controller.args[2], "lblFlag2"));
        var flxFlag1 = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "centerY": "50%",
            "clipBounds": true,
            "height": "13dp",
            "id": "flxFlag1",
            "isVisible": true,
            "layoutType": kony.flex.FLOW_HORIZONTAL,
            "left": "10%",
            "isModalContainer": false,
            "skin": "sknCursor",
            "top": 15,
            "width": "13px",
            "zIndex": 2
        }, controller.args[0], "flxFlag1"), extendConfig({}, controller.args[1], "flxFlag1"), extendConfig({}, controller.args[2], "flxFlag1"));
        flxFlag1.setDefaultUnit(kony.flex.DP);
        var imgFlag1 = new kony.ui.Image2(extendConfig({
            "centerX": "50%",
            "centerY": "50%",
            "height": "13dp",
            "id": "imgFlag1",
            "isVisible": true,
            "skin": "slImage",
            "src": "checkbox.png",
            "width": "13dp",
            "zIndex": 1
        }, controller.args[0], "imgFlag1"), extendConfig({
            "imageScaleMode": constants.IMAGE_SCALE_MODE_FIT_TO_DIMENSIONS,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "imgFlag1"), extendConfig({
            "toolTip": "Active"
        }, controller.args[2], "imgFlag1"));
        flxFlag1.add(imgFlag1);
        var lblFlag1 = new kony.ui.Label(extendConfig({
            "centerY": "50%",
            "id": "lblFlag1",
            "isVisible": true,
            "left": "6px",
            "skin": "sknLbl485C75LatoRegular13Px",
            "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmCustomerManagementController.High_Risk\")",
            "top": "15px",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, controller.args[0], "lblFlag1"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblFlag1"), extendConfig({}, controller.args[2], "lblFlag1"));
        flxSelectFlags.add(flxFlag3, lblFlag3, flxFlag2, lblFlag2, flxFlag1, lblFlag1);
        flxSubDetails8.add(flxRiskStatus, flxFlagSeperator, flxSelectFlags);
        flxDetails8.add(lblRisksHeader, flxSubDetails8);
        var flxDetails7 = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
            "clipBounds": true,
            "id": "flxDetails7",
            "isVisible": true,
            "layoutType": kony.flex.FLOW_VERTICAL,
            "left": "2%",
            "isModalContainer": false,
            "skin": "slFbox",
            "top": "0dp",
            "width": "27%",
            "zIndex": 2
        }, controller.args[0], "flxDetails7"), extendConfig({}, controller.args[1], "flxDetails7"), extendConfig({}, controller.args[2], "flxDetails7"));
        flxDetails7.setDefaultUnit(kony.flex.DP);
        var lblContactDetails7 = new kony.ui.Label(extendConfig({
            "id": "lblContactDetails7",
            "isVisible": true,
            "left": "0px",
            "skin": "sknLbl485C75LatoRegular13Px",
            "i18n_text": "kony.i18n.getLocalizedString(\"i18n.userwidgetmodel.lblContactDetails7\")",
            "top": "0px",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, controller.args[0], "lblContactDetails7"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblContactDetails7"), extendConfig({}, controller.args[2], "lblContactDetails7"));
        var switchToggle = new kony.ui.Switch(extendConfig({
            "height": "25dp",
            "id": "switchToggle",
            "isVisible": true,
            "left": "0dp",
            "leftSideText": "ON",
            "rightSideText": "OFF",
            "selectedIndex": 0,
            "skin": "sknSwitchServiceManagement",
            "top": "17px",
            "width": "38dp",
            "zIndex": 1
        }, controller.args[0], "switchToggle"), extendConfig({
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "switchToggle"), extendConfig({}, controller.args[2], "switchToggle"));
        flxDetails7.add(lblContactDetails7, switchToggle);
        flxRow3.add(flxDetails8, flxDetails7);
        flxGeneralDetails.add(flxRow1, flxRow2, flxRow4, flxRow3);
        var flxGeneralDetailsCombinedEdit = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
            "clipBounds": true,
            "id": "flxGeneralDetailsCombinedEdit",
            "isVisible": false,
            "layoutType": kony.flex.FLOW_VERTICAL,
            "left": "20px",
            "isModalContainer": false,
            "right": "20px",
            "skin": "slFbox",
            "top": "10px",
            "zIndex": 2
        }, controller.args[0], "flxGeneralDetailsCombinedEdit"), extendConfig({}, controller.args[1], "flxGeneralDetailsCombinedEdit"), extendConfig({}, controller.args[2], "flxGeneralDetailsCombinedEdit"));
        flxGeneralDetailsCombinedEdit.setDefaultUnit(kony.flex.DP);
        var flxCombinedEditRow1 = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "clipBounds": true,
            "height": "100px",
            "id": "flxCombinedEditRow1",
            "isVisible": true,
            "layoutType": kony.flex.FLOW_HORIZONTAL,
            "left": "0px",
            "isModalContainer": false,
            "skin": "sknNormalDefault",
            "top": "0px",
            "width": "100%",
            "zIndex": 1
        }, controller.args[0], "flxCombinedEditRow1"), extendConfig({}, controller.args[1], "flxCombinedEditRow1"), extendConfig({}, controller.args[2], "flxCombinedEditRow1"));
        flxCombinedEditRow1.setDefaultUnit(kony.flex.DP);
        var flxEditDetails11 = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
            "clipBounds": true,
            "id": "flxEditDetails11",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "left": "0%",
            "isModalContainer": false,
            "skin": "slFbox",
            "top": "0dp",
            "width": "32%",
            "zIndex": 2
        }, controller.args[0], "flxEditDetails11"), extendConfig({}, controller.args[1], "flxEditDetails11"), extendConfig({}, controller.args[2], "flxEditDetails11"));
        flxEditDetails11.setDefaultUnit(kony.flex.DP);
        var lblEditDetailsHeading11 = new kony.ui.Label(extendConfig({
            "id": "lblEditDetailsHeading11",
            "isVisible": true,
            "left": "0px",
            "skin": "sknLbl485C75LatoRegular13Px",
            "i18n_text": "kony.i18n.getLocalizedString(\"i18n.userwidgetmodel.lblContactDetails1\")",
            "top": "5px",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, controller.args[0], "lblEditDetailsHeading11"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblEditDetailsHeading11"), extendConfig({}, controller.args[2], "lblEditDetailsHeading11"));
        var lstBoxEditDetails11 = new kony.ui.ListBox(extendConfig({
            "focusSkin": "sknlbxBgffffffBorder11abebRadius3Px14px",
            "height": "40dp",
            "id": "lstBoxEditDetails11",
            "isVisible": true,
            "left": "0px",
            "masterData": [
                ["k1", "Select Salutation"],
                ["k2", "Mrs."],
                ["k3", "Dr."],
                ["k4", "Ms."]
            ],
            "selectedKey": "k1",
            "skin": "sknlstbxNormal0f9abd8e88aa64a",
            "top": "30dp",
            "width": "100%",
            "zIndex": 2
        }, controller.args[0], "lstBoxEditDetails11"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [3, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lstBoxEditDetails11"), extendConfig({
            "hoverSkin": "sknlstbxcursor",
            "multiSelect": false
        }, controller.args[2], "lstBoxEditDetails11"));
        var flxInlineError11 = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
            "clipBounds": true,
            "id": "flxInlineError11",
            "isVisible": false,
            "layoutType": kony.flex.FLOW_HORIZONTAL,
            "left": "0dp",
            "isModalContainer": false,
            "right": "0dp",
            "skin": "slFbox",
            "top": "72dp",
            "width": "100%"
        }, controller.args[0], "flxInlineError11"), extendConfig({}, controller.args[1], "flxInlineError11"), extendConfig({}, controller.args[2], "flxInlineError11"));
        flxInlineError11.setDefaultUnit(kony.flex.DP);
        var lblErrorIcon11 = new kony.ui.Label(extendConfig({
            "id": "lblErrorIcon11",
            "isVisible": true,
            "left": "0dp",
            "skin": "sknFontIconError",
            "text": "",
            "top": "1dp",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, controller.args[0], "lblErrorIcon11"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblErrorIcon11"), extendConfig({}, controller.args[2], "lblErrorIcon11"));
        var lblErrorText11 = new kony.ui.Label(extendConfig({
            "id": "lblErrorText11",
            "isVisible": true,
            "left": "5dp",
            "skin": "sknlblError",
            "text": "Error",
            "top": "0dp",
            "width": "85%",
            "zIndex": 1
        }, controller.args[0], "lblErrorText11"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblErrorText11"), extendConfig({}, controller.args[2], "lblErrorText11"));
        flxInlineError11.add(lblErrorIcon11, lblErrorText11);
        flxEditDetails11.add(lblEditDetailsHeading11, lstBoxEditDetails11, flxInlineError11);
        var editDetails12 = new com.adminConsole.common.textBoxEntry(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
            "id": "editDetails12",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "left": "15dp",
            "masterType": constants.MASTER_TYPE_DEFAULT,
            "isModalContainer": false,
            "skin": "slFbox",
            "top": "5dp",
            "width": "32%",
            "zIndex": 1,
            "overrides": {
                "flxEnterValue": {
                    "left": "0dp"
                },
                "flxHeading": {
                    "left": "0dp"
                },
                "flxInlineError": {
                    "left": "0dp"
                },
                "lblHeadingText": {
                    "i18n_text": "kony.i18n.getLocalizedString(\"i18n.Appliactions.FirstNamePlaceholder\")"
                },
                "tbxEnterValue": {
                    "i18n_placeholder": "kony.i18n.getLocalizedString(\"i18n.Appliactions.FirstNamePlaceholder\")"
                },
                "textBoxEntry": {
                    "left": "15dp",
                    "top": "5dp",
                    "width": "32%"
                }
            }
        }, controller.args[0], "editDetails12"), extendConfig({
            "overrides": {}
        }, controller.args[1], "editDetails12"), extendConfig({
            "overrides": {}
        }, controller.args[2], "editDetails12"));
        var editDetails13 = new com.adminConsole.common.textBoxEntry(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
            "id": "editDetails13",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "left": "15dp",
            "masterType": constants.MASTER_TYPE_DEFAULT,
            "isModalContainer": false,
            "skin": "slFbox",
            "top": "5dp",
            "width": "32%",
            "zIndex": 1,
            "overrides": {
                "flxEnterValue": {
                    "left": "0dp",
                    "top": "25dp"
                },
                "flxHeading": {
                    "left": "0dp"
                },
                "flxInlineError": {
                    "left": "0dp"
                },
                "lblHeadingText": {
                    "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmUsers.lblMiddleName\")"
                },
                "tbxEnterValue": {
                    "i18n_placeholder": "kony.i18n.getLocalizedString(\"i18n.frmUsers.lblMiddleName\")"
                },
                "textBoxEntry": {
                    "left": "15dp",
                    "top": "5dp",
                    "width": "32%"
                }
            }
        }, controller.args[0], "editDetails13"), extendConfig({
            "overrides": {}
        }, controller.args[1], "editDetails13"), extendConfig({
            "overrides": {}
        }, controller.args[2], "editDetails13"));
        flxCombinedEditRow1.add(flxEditDetails11, editDetails12, editDetails13);
        var flxCombinedEditRow2 = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "clipBounds": true,
            "height": "100px",
            "id": "flxCombinedEditRow2",
            "isVisible": true,
            "layoutType": kony.flex.FLOW_HORIZONTAL,
            "left": "0px",
            "isModalContainer": false,
            "skin": "sknNormalDefault",
            "top": "0dp",
            "width": "100%",
            "zIndex": 1
        }, controller.args[0], "flxCombinedEditRow2"), extendConfig({}, controller.args[1], "flxCombinedEditRow2"), extendConfig({}, controller.args[2], "flxCombinedEditRow2"));
        flxCombinedEditRow2.setDefaultUnit(kony.flex.DP);
        var editDetails21 = new com.adminConsole.common.textBoxEntry(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
            "id": "editDetails21",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "left": "0dp",
            "masterType": constants.MASTER_TYPE_DEFAULT,
            "isModalContainer": false,
            "skin": "slFbox",
            "top": "0dp",
            "width": "32%",
            "zIndex": 1,
            "overrides": {
                "flxEnterValue": {
                    "left": "0dp"
                },
                "flxHeading": {
                    "left": "0dp"
                },
                "flxInlineError": {
                    "left": "0dp"
                },
                "lblHeadingText": {
                    "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmUsers.lblLastName\")"
                },
                "tbxEnterValue": {
                    "i18n_placeholder": "kony.i18n.getLocalizedString(\"i18n.frmUsers.lblLastName\")"
                },
                "textBoxEntry": {
                    "left": "0dp",
                    "top": "0dp",
                    "width": "32%"
                }
            }
        }, controller.args[0], "editDetails21"), extendConfig({
            "overrides": {}
        }, controller.args[1], "editDetails21"), extendConfig({
            "overrides": {}
        }, controller.args[2], "editDetails21"));
        var editDetails22 = new com.adminConsole.common.textBoxEntry(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
            "id": "editDetails22",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "left": "15dp",
            "masterType": constants.MASTER_TYPE_DEFAULT,
            "isModalContainer": false,
            "skin": "slFbox",
            "top": "0dp",
            "width": "32%",
            "zIndex": 1,
            "overrides": {
                "flxEnterValue": {
                    "left": "0dp"
                },
                "flxHeading": {
                    "left": "0dp"
                },
                "flxInlineError": {
                    "left": "0dp"
                },
                "lblHeadingText": {
                    "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmGroupsController.Username\")"
                },
                "tbxEnterValue": {
                    "i18n_placeholder": "kony.i18n.getLocalizedString(\"i18n.frmGroupsController.Username\")"
                },
                "textBoxEntry": {
                    "left": "15dp",
                    "top": "0dp",
                    "width": "32%"
                }
            }
        }, controller.args[0], "editDetails22"), extendConfig({
            "overrides": {}
        }, controller.args[1], "editDetails22"), extendConfig({
            "overrides": {}
        }, controller.args[2], "editDetails22"));
        var editDetails23 = new com.adminConsole.common.textBoxEntry(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
            "id": "editDetails23",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "left": "15dp",
            "masterType": constants.MASTER_TYPE_DEFAULT,
            "isModalContainer": false,
            "skin": "slFbox",
            "top": "0dp",
            "width": "32%",
            "zIndex": 1,
            "overrides": {
                "flxEnterValue": {
                    "left": "0dp"
                },
                "flxHeading": {
                    "left": "0dp"
                },
                "flxInlineError": {
                    "left": "0dp"
                },
                "lblHeadingText": {
                    "i18n_text": "kony.i18n.getLocalizedString(\"i18n.Applications.DateOfBirth\")"
                },
                "tbxEnterValue": {
                    "i18n_placeholder": "kony.i18n.getLocalizedString(\"i18n.Applications.DateOfBirth\")"
                },
                "textBoxEntry": {
                    "left": "15dp",
                    "top": "0dp",
                    "width": "32%"
                }
            }
        }, controller.args[0], "editDetails23"), extendConfig({
            "overrides": {}
        }, controller.args[1], "editDetails23"), extendConfig({
            "overrides": {}
        }, controller.args[2], "editDetails23"));
        flxCombinedEditRow2.add(editDetails21, editDetails22, editDetails23);
        var flxCombinedEditRow3 = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "clipBounds": true,
            "height": "100px",
            "id": "flxCombinedEditRow3",
            "isVisible": true,
            "layoutType": kony.flex.FLOW_HORIZONTAL,
            "left": "0px",
            "isModalContainer": false,
            "skin": "slFbox",
            "top": "0px",
            "width": "100%",
            "zIndex": 1
        }, controller.args[0], "flxCombinedEditRow3"), extendConfig({}, controller.args[1], "flxCombinedEditRow3"), extendConfig({}, controller.args[2], "flxCombinedEditRow3"));
        flxCombinedEditRow3.setDefaultUnit(kony.flex.DP);
        var flxEditDetails33 = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
            "clipBounds": true,
            "id": "flxEditDetails33",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "left": "0px",
            "isModalContainer": false,
            "skin": "slFbox",
            "top": "0dp",
            "width": "32%",
            "zIndex": 2
        }, controller.args[0], "flxEditDetails33"), extendConfig({}, controller.args[1], "flxEditDetails33"), extendConfig({}, controller.args[2], "flxEditDetails33"));
        flxEditDetails33.setDefaultUnit(kony.flex.DP);
        var lblEditDetailsHeading31 = new kony.ui.Label(extendConfig({
            "id": "lblEditDetailsHeading31",
            "isVisible": true,
            "left": "0px",
            "skin": "sknLbl485C75LatoRegular13Px",
            "i18n_text": "kony.i18n.getLocalizedString(\"i18n.userwidgetmodel.lblDetails3\")",
            "top": "5px",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, controller.args[0], "lblEditDetailsHeading31"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblEditDetailsHeading31"), extendConfig({}, controller.args[2], "lblEditDetailsHeading31"));
        var lstBoxEditDetails31 = new kony.ui.ListBox(extendConfig({
            "focusSkin": "sknlbxBgffffffBorder11abebRadius3Px14px",
            "height": "40dp",
            "id": "lstBoxEditDetails31",
            "isVisible": true,
            "masterData": [
                ["k1", "Employed"],
                ["k2", "Unemployed"],
                ["k3", "Retired"],
                ["k4", "Student"]
            ],
            "selectedKey": "k1",
            "skin": "sknlstbxNormal0f9abd8e88aa64a",
            "top": "30dp",
            "width": "100%",
            "zIndex": 2
        }, controller.args[0], "lstBoxEditDetails31"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [3, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lstBoxEditDetails31"), extendConfig({
            "multiSelect": false
        }, controller.args[2], "lstBoxEditDetails31"));
        var flxInlineError31 = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
            "clipBounds": true,
            "id": "flxInlineError31",
            "isVisible": false,
            "layoutType": kony.flex.FLOW_HORIZONTAL,
            "left": "0dp",
            "isModalContainer": false,
            "right": "0dp",
            "skin": "slFbox",
            "top": "72dp",
            "width": "100%"
        }, controller.args[0], "flxInlineError31"), extendConfig({}, controller.args[1], "flxInlineError31"), extendConfig({}, controller.args[2], "flxInlineError31"));
        flxInlineError31.setDefaultUnit(kony.flex.DP);
        var lblErrorIcon31 = new kony.ui.Label(extendConfig({
            "id": "lblErrorIcon31",
            "isVisible": true,
            "left": "0dp",
            "skin": "sknFontIconError",
            "text": "",
            "top": "1dp",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, controller.args[0], "lblErrorIcon31"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblErrorIcon31"), extendConfig({}, controller.args[2], "lblErrorIcon31"));
        var lblErrorText31 = new kony.ui.Label(extendConfig({
            "id": "lblErrorText31",
            "isVisible": true,
            "left": "5dp",
            "skin": "sknlblError",
            "text": "Error",
            "top": "0dp",
            "width": "85%",
            "zIndex": 1
        }, controller.args[0], "lblErrorText31"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblErrorText31"), extendConfig({}, controller.args[2], "lblErrorText31"));
        flxInlineError31.add(lblErrorIcon31, lblErrorText31);
        flxEditDetails33.add(lblEditDetailsHeading31, lstBoxEditDetails31, flxInlineError31);
        var editDetails32 = new com.adminConsole.common.textBoxEntry(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
            "id": "editDetails32",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "left": "15dp",
            "masterType": constants.MASTER_TYPE_DEFAULT,
            "isModalContainer": false,
            "skin": "slFbox",
            "top": "5dp",
            "width": "32%",
            "zIndex": 1,
            "overrides": {
                "flxEnterValue": {
                    "left": "0dp"
                },
                "flxHeading": {
                    "left": "0dp"
                },
                "flxInlineError": {
                    "left": "0dp"
                },
                "lblHeadingText": {
                    "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmCustomerManagementController.RetailCustomerSSN\")"
                },
                "tbxEnterValue": {
                    "i18n_placeholder": "kony.i18n.getLocalizedString(\"i18n.frmCustomerManagementController.RetailCustomerSSN\")"
                },
                "textBoxEntry": {
                    "left": "15dp",
                    "top": "5dp",
                    "width": "32%"
                }
            }
        }, controller.args[0], "editDetails32"), extendConfig({
            "overrides": {}
        }, controller.args[1], "editDetails32"), extendConfig({
            "overrides": {}
        }, controller.args[2], "editDetails32"));
        var flxEditDetailsGroup33 = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
            "clipBounds": true,
            "id": "flxEditDetailsGroup33",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "left": "15dp",
            "isModalContainer": false,
            "skin": "slFbox",
            "top": "0dp",
            "width": "32%",
            "zIndex": 2
        }, controller.args[0], "flxEditDetailsGroup33"), extendConfig({}, controller.args[1], "flxEditDetailsGroup33"), extendConfig({}, controller.args[2], "flxEditDetailsGroup33"));
        flxEditDetailsGroup33.setDefaultUnit(kony.flex.DP);
        var lblEditDetailsHeading33 = new kony.ui.Label(extendConfig({
            "id": "lblEditDetailsHeading33",
            "isVisible": true,
            "left": "0px",
            "skin": "sknLbl485C75LatoRegular13Px",
            "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmCustomerManagementProfile.EmploymentStatus\")",
            "top": "5px",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, controller.args[0], "lblEditDetailsHeading33"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblEditDetailsHeading33"), extendConfig({}, controller.args[2], "lblEditDetailsHeading33"));
        var lstBoxEditDetails33 = new kony.ui.ListBox(extendConfig({
            "focusSkin": "sknlbxBgffffffBorder11abebRadius3Px14px",
            "height": "40dp",
            "id": "lstBoxEditDetails33",
            "isVisible": true,
            "left": "0px",
            "masterData": [
                ["key1", "Active"],
                ["key2", "Suspended"]
            ],
            "skin": "sknlstbxNormal0f9abd8e88aa64a",
            "top": "30dp",
            "width": "100%",
            "zIndex": 2
        }, controller.args[0], "lstBoxEditDetails33"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [3, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lstBoxEditDetails33"), extendConfig({
            "hoverSkin": "sknlstbxcursor",
            "multiSelect": false
        }, controller.args[2], "lstBoxEditDetails33"));
        var flxInlineError33 = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
            "clipBounds": true,
            "id": "flxInlineError33",
            "isVisible": false,
            "layoutType": kony.flex.FLOW_HORIZONTAL,
            "left": "0dp",
            "isModalContainer": false,
            "right": "0dp",
            "skin": "slFbox",
            "top": "72dp",
            "width": "100%"
        }, controller.args[0], "flxInlineError33"), extendConfig({}, controller.args[1], "flxInlineError33"), extendConfig({}, controller.args[2], "flxInlineError33"));
        flxInlineError33.setDefaultUnit(kony.flex.DP);
        var lblErrorIcon33 = new kony.ui.Label(extendConfig({
            "id": "lblErrorIcon33",
            "isVisible": true,
            "left": "0dp",
            "skin": "sknFontIconError",
            "text": "",
            "top": "1dp",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, controller.args[0], "lblErrorIcon33"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblErrorIcon33"), extendConfig({}, controller.args[2], "lblErrorIcon33"));
        var lblErrorText33 = new kony.ui.Label(extendConfig({
            "id": "lblErrorText33",
            "isVisible": true,
            "left": "5dp",
            "skin": "sknlblError",
            "text": "Error",
            "top": "0dp",
            "width": "85%",
            "zIndex": 1
        }, controller.args[0], "lblErrorText33"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblErrorText33"), extendConfig({}, controller.args[2], "lblErrorText33"));
        flxInlineError33.add(lblErrorIcon33, lblErrorText33);
        flxEditDetailsGroup33.add(lblEditDetailsHeading33, lstBoxEditDetails33, flxInlineError33);
        flxCombinedEditRow3.add(flxEditDetails33, editDetails32, flxEditDetailsGroup33);
        var flxCombinedEditRow4 = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "clipBounds": true,
            "height": "100px",
            "id": "flxCombinedEditRow4",
            "isVisible": true,
            "layoutType": kony.flex.FLOW_HORIZONTAL,
            "left": "0px",
            "isModalContainer": false,
            "skin": "sknNormalDefault",
            "top": "0px",
            "width": "100%",
            "zIndex": 1
        }, controller.args[0], "flxCombinedEditRow4"), extendConfig({}, controller.args[1], "flxCombinedEditRow4"), extendConfig({}, controller.args[2], "flxCombinedEditRow4"));
        flxCombinedEditRow4.setDefaultUnit(kony.flex.DP);
        var editDetails41 = new com.adminConsole.common.textBoxEntry(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
            "id": "editDetails41",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "left": "0dp",
            "masterType": constants.MASTER_TYPE_DEFAULT,
            "isModalContainer": false,
            "skin": "slFbox",
            "top": "0dp",
            "width": "32%",
            "zIndex": 1,
            "overrides": {
                "flxEnterValue": {
                    "left": "0dp"
                },
                "flxHeading": {
                    "left": "0dp"
                },
                "flxInlineError": {
                    "left": "0dp"
                },
                "lblHeadingText": {
                    "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmCustomerManagementController.DriversLicense\")"
                },
                "tbxEnterValue": {
                    "i18n_placeholder": "kony.i18n.getLocalizedString(\"i18n.frmCustomerManagementController.DriversLicense\")"
                },
                "textBoxEntry": {
                    "left": "0dp",
                    "top": "0dp",
                    "width": "32%"
                }
            }
        }, controller.args[0], "editDetails41"), extendConfig({
            "overrides": {}
        }, controller.args[1], "editDetails41"), extendConfig({
            "overrides": {}
        }, controller.args[2], "editDetails41"));
        var flxEditDetails42 = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
            "clipBounds": true,
            "id": "flxEditDetails42",
            "isVisible": true,
            "layoutType": kony.flex.FLOW_VERTICAL,
            "left": "15dp",
            "isModalContainer": false,
            "skin": "slFbox",
            "top": "0dp",
            "width": "42%"
        }, controller.args[0], "flxEditDetails42"), extendConfig({}, controller.args[1], "flxEditDetails42"), extendConfig({}, controller.args[2], "flxEditDetails42"));
        flxEditDetails42.setDefaultUnit(kony.flex.DP);
        var lblEditDetailsHeader42 = new kony.ui.Label(extendConfig({
            "id": "lblEditDetailsHeader42",
            "isVisible": true,
            "left": "0px",
            "skin": "sknLbl485C75LatoRegular13Px",
            "i18n_text": "kony.i18n.getLocalizedString(\"i18n.userwidgetmodel.lblDetails8\")",
            "top": "0px",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, controller.args[0], "lblEditDetailsHeader42"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblEditDetailsHeader42"), extendConfig({}, controller.args[2], "lblEditDetailsHeader42"));
        var flxSubDetails42 = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "clipBounds": true,
            "height": "36px",
            "id": "flxSubDetails42",
            "isVisible": true,
            "layoutType": kony.flex.FLOW_HORIZONTAL,
            "left": "0dp",
            "isModalContainer": false,
            "skin": "sknflxOuterBorder",
            "top": "13px",
            "width": "100%",
            "zIndex": 2
        }, controller.args[0], "flxSubDetails42"), extendConfig({}, controller.args[1], "flxSubDetails42"), extendConfig({}, controller.args[2], "flxSubDetails42"));
        flxSubDetails42.setDefaultUnit(kony.flex.DP);
        var flxRiskStatus42 = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "clipBounds": true,
            "height": "36dp",
            "id": "flxRiskStatus42",
            "isVisible": false,
            "layoutType": kony.flex.FLOW_HORIZONTAL,
            "left": "0dp",
            "isModalContainer": false,
            "skin": "sknflxfdf6f1bg100px",
            "top": "0dp",
            "width": "100dp",
            "zIndex": 111
        }, controller.args[0], "flxRiskStatus42"), extendConfig({}, controller.args[1], "flxRiskStatus42"), extendConfig({}, controller.args[2], "flxRiskStatus42"));
        flxRiskStatus42.setDefaultUnit(kony.flex.DP);
        var lblRiskStatusVal42 = new kony.ui.Label(extendConfig({
            "centerX": "50%",
            "centerY": "50%",
            "id": "lblRiskStatusVal42",
            "isVisible": true,
            "skin": "sknlblLatoBold35475f14px",
            "i18n_text": "kony.i18n.getLocalizedString(\"i18n.userwidgetmodel.lblDetails8\")",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, controller.args[0], "lblRiskStatusVal42"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblRiskStatusVal42"), extendConfig({}, controller.args[2], "lblRiskStatusVal42"));
        flxRiskStatus42.add(lblRiskStatusVal42);
        var flxFlagSeperator42 = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "clipBounds": true,
            "height": "36dp",
            "id": "flxFlagSeperator42",
            "isVisible": false,
            "layoutType": kony.flex.FREE_FORM,
            "left": "0dp",
            "isModalContainer": false,
            "skin": "sknflxOuterBorder",
            "top": "0dp",
            "width": "1px",
            "zIndex": 111
        }, controller.args[0], "flxFlagSeperator42"), extendConfig({}, controller.args[1], "flxFlagSeperator42"), extendConfig({}, controller.args[2], "flxFlagSeperator42"));
        flxFlagSeperator42.setDefaultUnit(kony.flex.DP);
        flxFlagSeperator42.add();
        var flxSelectFlagContainer42 = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "clipBounds": false,
            "height": "36px",
            "id": "flxSelectFlagContainer42",
            "isVisible": true,
            "layoutType": kony.flex.FLOW_HORIZONTAL,
            "left": "0dp",
            "isModalContainer": false,
            "right": "0dp",
            "skin": "slFbox",
            "top": "0px",
            "width": "100%",
            "zIndex": 111
        }, controller.args[0], "flxSelectFlagContainer42"), extendConfig({}, controller.args[1], "flxSelectFlagContainer42"), extendConfig({}, controller.args[2], "flxSelectFlagContainer42"));
        flxSelectFlagContainer42.setDefaultUnit(kony.flex.DP);
        var flxFlagCheckbox1 = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "centerY": "50%",
            "clipBounds": true,
            "height": "13dp",
            "id": "flxFlagCheckbox1",
            "isVisible": true,
            "layoutType": kony.flex.FLOW_HORIZONTAL,
            "left": "20px",
            "isModalContainer": false,
            "skin": "sknCursor",
            "top": "0dp",
            "width": "13px",
            "zIndex": 1
        }, controller.args[0], "flxFlagCheckbox1"), extendConfig({}, controller.args[1], "flxFlagCheckbox1"), extendConfig({}, controller.args[2], "flxFlagCheckbox1"));
        flxFlagCheckbox1.setDefaultUnit(kony.flex.DP);
        var imgFlagCheckbox1 = new kony.ui.Image2(extendConfig({
            "centerX": "50%",
            "centerY": "50%",
            "height": "13dp",
            "id": "imgFlagCheckbox1",
            "isVisible": true,
            "left": "15dp",
            "skin": "slImage",
            "src": "checkbox.png",
            "width": "13dp",
            "zIndex": 1
        }, controller.args[0], "imgFlagCheckbox1"), extendConfig({
            "imageScaleMode": constants.IMAGE_SCALE_MODE_FIT_TO_DIMENSIONS,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "imgFlagCheckbox1"), extendConfig({
            "toolTip": "Delete"
        }, controller.args[2], "imgFlagCheckbox1"));
        flxFlagCheckbox1.add(imgFlagCheckbox1);
        var lblFlagVal1 = new kony.ui.Label(extendConfig({
            "centerY": "50%",
            "id": "lblFlagVal1",
            "isVisible": true,
            "left": "6px",
            "skin": "sknLbl485C75LatoRegular13Px",
            "i18n_text": "kony.i18n.getLocalizedString(\"i18n.userwidgetmodel.lblFlag3\")",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, controller.args[0], "lblFlagVal1"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblFlagVal1"), extendConfig({}, controller.args[2], "lblFlagVal1"));
        var flxFlagCheckbox2 = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "centerY": "50%",
            "clipBounds": true,
            "height": "13dp",
            "id": "flxFlagCheckbox2",
            "isVisible": true,
            "layoutType": kony.flex.FLOW_HORIZONTAL,
            "left": "10%",
            "isModalContainer": false,
            "skin": "sknCursor",
            "top": "0dp",
            "width": "13px",
            "zIndex": 1
        }, controller.args[0], "flxFlagCheckbox2"), extendConfig({}, controller.args[1], "flxFlagCheckbox2"), extendConfig({}, controller.args[2], "flxFlagCheckbox2"));
        flxFlagCheckbox2.setDefaultUnit(kony.flex.DP);
        var imgFlagCheckbox2 = new kony.ui.Image2(extendConfig({
            "centerX": "50%",
            "centerY": "50%",
            "height": "13dp",
            "id": "imgFlagCheckbox2",
            "isVisible": true,
            "skin": "slImage",
            "src": "checkbox.png",
            "width": "13dp",
            "zIndex": 1
        }, controller.args[0], "imgFlagCheckbox2"), extendConfig({
            "imageScaleMode": constants.IMAGE_SCALE_MODE_FIT_TO_DIMENSIONS,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "imgFlagCheckbox2"), extendConfig({
            "toolTip": "Deactive"
        }, controller.args[2], "imgFlagCheckbox2"));
        flxFlagCheckbox2.add(imgFlagCheckbox2);
        var lblFlagVal2 = new kony.ui.Label(extendConfig({
            "centerY": "50%",
            "id": "lblFlagVal2",
            "isVisible": true,
            "left": "6px",
            "skin": "sknLbl485C75LatoRegular13Px",
            "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmCustomerManagementController.Defaulter\")",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, controller.args[0], "lblFlagVal2"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblFlagVal2"), extendConfig({}, controller.args[2], "lblFlagVal2"));
        var flxFlagCheckbox3 = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "centerY": "50%",
            "clipBounds": true,
            "height": "13dp",
            "id": "flxFlagCheckbox3",
            "isVisible": true,
            "layoutType": kony.flex.FLOW_HORIZONTAL,
            "left": "10%",
            "isModalContainer": false,
            "skin": "sknCursor",
            "top": 15,
            "width": "13px",
            "zIndex": 2
        }, controller.args[0], "flxFlagCheckbox3"), extendConfig({}, controller.args[1], "flxFlagCheckbox3"), extendConfig({}, controller.args[2], "flxFlagCheckbox3"));
        flxFlagCheckbox3.setDefaultUnit(kony.flex.DP);
        var imgFlagCheckbox3 = new kony.ui.Image2(extendConfig({
            "centerX": "50%",
            "centerY": "50%",
            "height": "13dp",
            "id": "imgFlagCheckbox3",
            "isVisible": true,
            "skin": "slImage",
            "src": "checkbox.png",
            "width": "13dp",
            "zIndex": 1
        }, controller.args[0], "imgFlagCheckbox3"), extendConfig({
            "imageScaleMode": constants.IMAGE_SCALE_MODE_FIT_TO_DIMENSIONS,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "imgFlagCheckbox3"), extendConfig({
            "toolTip": "Active"
        }, controller.args[2], "imgFlagCheckbox3"));
        flxFlagCheckbox3.add(imgFlagCheckbox3);
        var lblFlagVal3 = new kony.ui.Label(extendConfig({
            "centerY": "50%",
            "id": "lblFlagVal3",
            "isVisible": true,
            "left": "6px",
            "skin": "sknLbl485C75LatoRegular13Px",
            "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmCustomerManagementController.High_Risk\")",
            "top": "15px",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, controller.args[0], "lblFlagVal3"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblFlagVal3"), extendConfig({}, controller.args[2], "lblFlagVal3"));
        flxSelectFlagContainer42.add(flxFlagCheckbox1, lblFlagVal1, flxFlagCheckbox2, lblFlagVal2, flxFlagCheckbox3, lblFlagVal3);
        flxSubDetails42.add(flxRiskStatus42, flxFlagSeperator42, flxSelectFlagContainer42);
        flxEditDetails42.add(lblEditDetailsHeader42, flxSubDetails42);
        flxCombinedEditRow4.add(editDetails41, flxEditDetails42);
        var flxCombinedEditRow5 = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "clipBounds": true,
            "height": "60px",
            "id": "flxCombinedEditRow5",
            "isVisible": true,
            "layoutType": kony.flex.FLOW_HORIZONTAL,
            "left": "0px",
            "isModalContainer": false,
            "skin": "sknNormalDefault",
            "top": "0px",
            "width": "100%",
            "zIndex": 1
        }, controller.args[0], "flxCombinedEditRow5"), extendConfig({}, controller.args[1], "flxCombinedEditRow5"), extendConfig({}, controller.args[2], "flxCombinedEditRow5"));
        flxCombinedEditRow5.setDefaultUnit(kony.flex.DP);
        var flxEditDetails51 = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
            "clipBounds": true,
            "id": "flxEditDetails51",
            "isVisible": true,
            "layoutType": kony.flex.FLOW_VERTICAL,
            "left": "0dp",
            "isModalContainer": false,
            "skin": "slFbox",
            "top": "0dp",
            "width": "27%",
            "zIndex": 2
        }, controller.args[0], "flxEditDetails51"), extendConfig({}, controller.args[1], "flxEditDetails51"), extendConfig({}, controller.args[2], "flxEditDetails51"));
        flxEditDetails51.setDefaultUnit(kony.flex.DP);
        var lblEditDetailsHeading51 = new kony.ui.Label(extendConfig({
            "id": "lblEditDetailsHeading51",
            "isVisible": true,
            "left": "0px",
            "skin": "sknLbl485C75LatoRegular13Px",
            "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmCustomerManagementController.Edit_eagreementStatus\")",
            "top": "0px",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, controller.args[0], "lblEditDetailsHeading51"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblEditDetailsHeading51"), extendConfig({}, controller.args[2], "lblEditDetailsHeading51"));
        var switchEAgreement51 = new kony.ui.Switch(extendConfig({
            "height": "25dp",
            "id": "switchEAgreement51",
            "isVisible": true,
            "left": "0dp",
            "leftSideText": "ON",
            "rightSideText": "OFF",
            "selectedIndex": 0,
            "skin": "sknSwitchServiceManagement",
            "top": "17px",
            "width": "38dp",
            "zIndex": 1
        }, controller.args[0], "switchEAgreement51"), extendConfig({
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "switchEAgreement51"), extendConfig({}, controller.args[2], "switchEAgreement51"));
        flxEditDetails51.add(lblEditDetailsHeading51, switchEAgreement51);
        flxCombinedEditRow5.add(flxEditDetails51);
        flxGeneralDetailsCombinedEdit.add(flxCombinedEditRow1, flxCombinedEditRow2, flxCombinedEditRow3, flxCombinedEditRow4, flxCombinedEditRow5);
        EditGeneralInfo.add(flxGeneralDetails, flxGeneralDetailsCombinedEdit);
        return EditGeneralInfo;
    }
})