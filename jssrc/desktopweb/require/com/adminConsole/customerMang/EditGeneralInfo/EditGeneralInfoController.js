define("com/adminConsole/customerMang/EditGeneralInfo/userEditGeneralInfoController", {
    editGeneralInfoPreshow: function() {
        //this.setFlowActions();
    },
    setFlowActions: function() {
        var scopeObj = this;
        this.view.flxFlag1.onClick = function() {
            if (scopeObj.view.imgFlag1.src === "checkbox.png") {
                scopeObj.view.imgFlag1.src = "checkboxselected.png";
            } else {
                scopeObj.view.imgFlag1.src = "checkbox.png";
            }
        };
        this.view.flxFlag2.onClick = function() {
            if (scopeObj.view.imgFlag2.src === "checkbox.png") {
                scopeObj.view.imgFlag2.src = "checkboxselected.png";
            } else {
                scopeObj.view.imgFlag2.src = "checkbox.png";
            }
        };
        this.view.flxFlag3.onClick = function() {
            if (scopeObj.view.imgFlag3.src === "checkbox.png") {
                scopeObj.view.imgFlag3.src = "checkboxselected.png";
            } else {
                scopeObj.view.imgFlag3.src = "checkbox.png";
            }
        };
    }
});
define("com/adminConsole/customerMang/EditGeneralInfo/EditGeneralInfoControllerActions", {
    /*
        This is an auto generated file and any modifications to it may result in corruption of the action sequence.
    */
    AS_FlexContainer_ba7818631ed740b9b8f871667b666d56: function AS_FlexContainer_ba7818631ed740b9b8f871667b666d56(eventobject) {
        var self = this;
        this.editGeneralInfoPreshow();
    }
});
define("com/adminConsole/customerMang/EditGeneralInfo/EditGeneralInfoController", ["com/adminConsole/customerMang/EditGeneralInfo/userEditGeneralInfoController", "com/adminConsole/customerMang/EditGeneralInfo/EditGeneralInfoControllerActions"], function() {
    var controller = require("com/adminConsole/customerMang/EditGeneralInfo/userEditGeneralInfoController");
    var actions = require("com/adminConsole/customerMang/EditGeneralInfo/EditGeneralInfoControllerActions");
    for (var key in actions) {
        controller[key] = actions[key];
    }
    return controller;
});
