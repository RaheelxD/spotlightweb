define(function() {
    return function(controller) {
        var selectOptions = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
            "clipBounds": true,
            "isMaster": true,
            "id": "selectOptions",
            "isVisible": true,
            "layoutType": kony.flex.FLOW_VERTICAL,
            "left": "0dp",
            "isModalContainer": false,
            "skin": "sknFlxTrans",
            "top": "0dp",
            "width": "200px"
        }, controller.args[0], "selectOptions"), extendConfig({}, controller.args[1], "selectOptions"), extendConfig({}, controller.args[2], "selectOptions"));
        selectOptions.setDefaultUnit(kony.flex.DP);
        var flxArrowImage = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "clipBounds": true,
            "height": "12dp",
            "id": "flxArrowImage",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "left": "0dp",
            "isModalContainer": false,
            "skin": "slFbox",
            "top": "0dp",
            "width": "100%",
            "zIndex": 2
        }, controller.args[0], "flxArrowImage"), extendConfig({}, controller.args[1], "flxArrowImage"), extendConfig({}, controller.args[2], "flxArrowImage"));
        flxArrowImage.setDefaultUnit(kony.flex.DP);
        var imgUpArrow = new kony.ui.Image2(extendConfig({
            "height": "12dp",
            "id": "imgUpArrow",
            "isVisible": true,
            "right": "15dp",
            "skin": "slImage",
            "src": "uparrow_2x.png",
            "top": "0dp",
            "width": "15dp",
            "zIndex": 1
        }, controller.args[0], "imgUpArrow"), extendConfig({
            "imageScaleMode": constants.IMAGE_SCALE_MODE_FIT_TO_DIMENSIONS,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "imgUpArrow"), extendConfig({}, controller.args[2], "imgUpArrow"));
        flxArrowImage.add(imgUpArrow);
        var flxContainer = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
            "clipBounds": true,
            "id": "flxContainer",
            "isVisible": true,
            "layoutType": kony.flex.FLOW_VERTICAL,
            "left": "0dp",
            "isModalContainer": false,
            "skin": "sknflxffffffop100dbdbe6Radius3px",
            "top": "-1dp",
            "width": "100%"
        }, controller.args[0], "flxContainer"), extendConfig({}, controller.args[1], "flxContainer"), extendConfig({}, controller.args[2], "flxContainer"));
        flxContainer.setDefaultUnit(kony.flex.DP);
        var flxUpgrade = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "clipBounds": true,
            "height": "45dp",
            "id": "flxUpgrade",
            "isVisible": true,
            "layoutType": kony.flex.FLOW_HORIZONTAL,
            "left": "0dp",
            "isModalContainer": false,
            "skin": "CopyslFbox0a35230cb45374d",
            "top": "0dp",
            "width": "100%",
            "zIndex": 1
        }, controller.args[0], "flxUpgrade"), extendConfig({}, controller.args[1], "flxUpgrade"), extendConfig({}, controller.args[2], "flxUpgrade"));
        flxUpgrade.setDefaultUnit(kony.flex.DP);
        var fonticonDeactive = new kony.ui.Label(extendConfig({
            "centerY": "50%",
            "id": "fonticonDeactive",
            "isVisible": true,
            "left": "15px",
            "skin": "sknFontIconOptionMenuRow",
            "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmCustomerManagementPersonal.fontIconUpgrade\")",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, controller.args[0], "fonticonDeactive"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "fonticonDeactive"), extendConfig({}, controller.args[2], "fonticonDeactive"));
        var lblOption2 = new kony.ui.Label(extendConfig({
            "centerY": "50%",
            "id": "lblOption2",
            "isVisible": true,
            "left": "15dp",
            "skin": "sknlblLatoBold35475f14px",
            "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmCustomerManagementPersonal.upgradeToMicroBusiness\")",
            "top": "4dp",
            "width": "75%",
            "zIndex": 1
        }, controller.args[0], "lblOption2"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblOption2"), extendConfig({}, controller.args[2], "lblOption2"));
        flxUpgrade.add(fonticonDeactive, lblOption2);
        var flxSuspend = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "clipBounds": true,
            "height": "45dp",
            "id": "flxSuspend",
            "isVisible": true,
            "layoutType": kony.flex.FLOW_HORIZONTAL,
            "left": "0dp",
            "isModalContainer": false,
            "skin": "CopyslFbox0i3963a83703242",
            "top": "0dp",
            "width": "100%",
            "zIndex": 1
        }, controller.args[0], "flxSuspend"), extendConfig({}, controller.args[1], "flxSuspend"), extendConfig({}, controller.args[2], "flxSuspend"));
        flxSuspend.setDefaultUnit(kony.flex.DP);
        var fonticonEdit = new kony.ui.Label(extendConfig({
            "centerY": "50%",
            "id": "fonticonEdit",
            "isVisible": true,
            "left": "15px",
            "skin": "sknFontIconOptionMenuRow",
            "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmPermissions.fontIconOption3\")",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, controller.args[0], "fonticonEdit"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "fonticonEdit"), extendConfig({}, controller.args[2], "fonticonEdit"));
        var lblOption1 = new kony.ui.Label(extendConfig({
            "centerY": "50%",
            "id": "lblOption1",
            "isVisible": true,
            "left": "15dp",
            "skin": "sknlblLatoBold35475f14px",
            "i18n_text": "kony.i18n.getLocalizedString(\"i18n.permission.Suspend\")",
            "top": "4dp",
            "width": "70px",
            "zIndex": 1
        }, controller.args[0], "lblOption1"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblOption1"), extendConfig({}, controller.args[2], "lblOption1"));
        flxSuspend.add(fonticonEdit, lblOption1);
        flxContainer.add(flxUpgrade, flxSuspend);
        selectOptions.add(flxArrowImage, flxContainer);
        return selectOptions;
    }
})