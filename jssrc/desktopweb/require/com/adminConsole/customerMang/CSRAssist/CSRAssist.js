define(function() {
    return function(controller) {
        var CSRAssist = new kony.ui.FlexContainer(extendConfig({
            "clipBounds": true,
            "isMaster": true,
            "height": "100%",
            "id": "CSRAssist",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "left": "0dp",
            "isModalContainer": false,
            "preShow": function(eventobject) {
                controller.AS_FlexContainer_ef86433e7e8c4396afe7e9873d50990c(eventobject);
            },
            "skin": "sknLoadingBlur",
            "top": "0dp",
            "width": "100%",
            "zIndex": 50
        }, controller.args[0], "CSRAssist"), extendConfig({}, controller.args[1], "CSRAssist"), extendConfig({}, controller.args[2], "CSRAssist"));
        CSRAssist.setDefaultUnit(kony.flex.DP);
        var flxCSRAssistLoading = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "clipBounds": true,
            "height": "100%",
            "id": "flxCSRAssistLoading",
            "isVisible": false,
            "layoutType": kony.flex.FREE_FORM,
            "left": "0px",
            "isModalContainer": false,
            "skin": "sknflxBg000000Op50",
            "top": "0px",
            "width": "100%",
            "zIndex": 200
        }, controller.args[0], "flxCSRAssistLoading"), extendConfig({}, controller.args[1], "flxCSRAssistLoading"), extendConfig({}, controller.args[2], "flxCSRAssistLoading"));
        flxCSRAssistLoading.setDefaultUnit(kony.flex.DP);
        var flxCSRLoading = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "centerX": "50%",
            "centerY": "50%",
            "clipBounds": true,
            "height": "300px",
            "id": "flxCSRLoading",
            "isVisible": true,
            "layoutType": kony.flex.FLOW_VERTICAL,
            "isModalContainer": false,
            "skin": "sknflxUploadImage0e65d5a93fe0f45",
            "width": "600px",
            "zIndex": 5
        }, controller.args[0], "flxCSRLoading"), extendConfig({}, controller.args[1], "flxCSRLoading"), extendConfig({}, controller.args[2], "flxCSRLoading"));
        flxCSRLoading.setDefaultUnit(kony.flex.DP);
        var flxCSRLoadingIndicator = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "centerX": "50%",
            "clipBounds": true,
            "height": "75px",
            "id": "flxCSRLoadingIndicator",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "isModalContainer": false,
            "skin": "slFbox",
            "top": "70px",
            "width": "75px",
            "zIndex": 1
        }, controller.args[0], "flxCSRLoadingIndicator"), extendConfig({}, controller.args[1], "flxCSRLoadingIndicator"), extendConfig({}, controller.args[2], "flxCSRLoadingIndicator"));
        flxCSRLoadingIndicator.setDefaultUnit(kony.flex.DP);
        var imgCSRLoading = new kony.ui.Image2(extendConfig({
            "centerX": "50%",
            "centerY": "50%",
            "height": "75px",
            "id": "imgCSRLoading",
            "isVisible": true,
            "skin": "slImage",
            "src": "loadingscreenimage.gif",
            "width": "75px",
            "zIndex": 1
        }, controller.args[0], "imgCSRLoading"), extendConfig({
            "imageScaleMode": constants.IMAGE_SCALE_MODE_MAINTAIN_ASPECT_RATIO,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "imgCSRLoading"), extendConfig({}, controller.args[2], "imgCSRLoading"));
        flxCSRLoadingIndicator.add(imgCSRLoading);
        var lblInitializeAssist = new kony.ui.Label(extendConfig({
            "centerX": "51%",
            "height": "15px",
            "id": "lblInitializeAssist",
            "isVisible": true,
            "skin": "sknlbllatoRegular0bc8f2143d6204e",
            "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmCustomerManagementController.Initiating_assist\")",
            "top": "15px",
            "width": "40%",
            "zIndex": 1
        }, controller.args[0], "lblInitializeAssist"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_CENTER,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblInitializeAssist"), extendConfig({}, controller.args[2], "lblInitializeAssist"));
        flxCSRLoading.add(flxCSRLoadingIndicator, lblInitializeAssist);
        flxCSRAssistLoading.add(flxCSRLoading);
        var CSRBrowser = new kony.ui.Browser(extendConfig({
            "bottom": "10px",
            "detectTelNumber": true,
            "enableZoom": false,
            "htmlString": "<!DOCTYPE html>\n<html>\n<body>\n  <p>CSR Assist</p>\n</body>\n</html>",
            "id": "CSRBrowser",
            "isVisible": true,
            "left": "13px",
            "setAsContent": false,
            "right": "15px",
            "top": "20px",
            "zIndex": 1
        }, controller.args[0], "CSRBrowser"), extendConfig({}, controller.args[1], "CSRBrowser"), extendConfig({}, controller.args[2], "CSRBrowser"));
        var flxClose = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "clipBounds": true,
            "height": "34px",
            "id": "flxClose",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "isModalContainer": false,
            "right": "10px",
            "skin": "sknCursor",
            "top": "10px",
            "width": "34px",
            "zIndex": 100
        }, controller.args[0], "flxClose"), extendConfig({}, controller.args[1], "flxClose"), extendConfig({}, controller.args[2], "flxClose"));
        flxClose.setDefaultUnit(kony.flex.DP);
        var fonticonClose = new kony.ui.Label(extendConfig({
            "centerX": "50%",
            "centerY": "50%",
            "id": "fonticonClose",
            "isVisible": true,
            "skin": "sknCSRAssistClose",
            "text": "",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 100
        }, controller.args[0], "fonticonClose"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "fonticonClose"), extendConfig({
            "toolTip": "Close"
        }, controller.args[2], "fonticonClose"));
        var flxBackground = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "bottom": 0,
            "centerX": "50%",
            "centerY": "50%",
            "clipBounds": true,
            "id": "flxBackground",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "isModalContainer": false,
            "right": 0,
            "skin": "flxBackground",
            "zIndex": 1
        }, controller.args[0], "flxBackground"), extendConfig({}, controller.args[1], "flxBackground"), extendConfig({}, controller.args[2], "flxBackground"));
        flxBackground.setDefaultUnit(kony.flex.DP);
        flxBackground.add();
        flxClose.add(fonticonClose, flxBackground);
        CSRAssist.add(flxCSRAssistLoading, CSRBrowser, flxClose);
        return CSRAssist;
    }
})