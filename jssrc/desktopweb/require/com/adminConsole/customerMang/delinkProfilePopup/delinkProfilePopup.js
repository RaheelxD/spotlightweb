define(function() {
    return function(controller) {
        var delinkProfilePopup = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "clipBounds": true,
            "isMaster": true,
            "height": "100%",
            "id": "delinkProfilePopup",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "left": "0dp",
            "isModalContainer": false,
            "skin": "sknFlxBg222B35Op50PopupBg",
            "top": "0dp",
            "width": "100%"
        }, controller.args[0], "delinkProfilePopup"), extendConfig({}, controller.args[1], "delinkProfilePopup"), extendConfig({}, controller.args[2], "delinkProfilePopup"));
        delinkProfilePopup.setDefaultUnit(kony.flex.DP);
        var flxDelinkProfilePopup = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "centerX": "50%",
            "centerY": "50%",
            "clipBounds": false,
            "height": "340dp",
            "id": "flxDelinkProfilePopup",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "isModalContainer": false,
            "skin": "slFbox0j9f841cc563e4e",
            "width": "600px",
            "zIndex": 10
        }, controller.args[0], "flxDelinkProfilePopup"), extendConfig({}, controller.args[1], "flxDelinkProfilePopup"), extendConfig({}, controller.args[2], "flxDelinkProfilePopup"));
        flxDelinkProfilePopup.setDefaultUnit(kony.flex.DP);
        var flxPopUpTopColor = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "clipBounds": true,
            "height": "10px",
            "id": "flxPopUpTopColor",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "left": "0dp",
            "isModalContainer": false,
            "skin": "sknFlxInfoToastBg357C9ENoRadius",
            "top": "0dp",
            "width": "100%",
            "zIndex": 1
        }, controller.args[0], "flxPopUpTopColor"), extendConfig({}, controller.args[1], "flxPopUpTopColor"), extendConfig({}, controller.args[2], "flxPopUpTopColor"));
        flxPopUpTopColor.setDefaultUnit(kony.flex.DP);
        flxPopUpTopColor.add();
        var flxPopupHeader = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
            "clipBounds": true,
            "id": "flxPopupHeader",
            "isVisible": true,
            "layoutType": kony.flex.FLOW_VERTICAL,
            "left": "0dp",
            "isModalContainer": false,
            "skin": "slFbox0d9c3974835234d",
            "top": "10px",
            "width": "100%",
            "zIndex": 1
        }, controller.args[0], "flxPopupHeader"), extendConfig({}, controller.args[1], "flxPopupHeader"), extendConfig({}, controller.args[2], "flxPopupHeader"));
        flxPopupHeader.setDefaultUnit(kony.flex.DP);
        var flxPopUpClose = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "clipBounds": true,
            "height": "25px",
            "id": "flxPopUpClose",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "isModalContainer": false,
            "right": 15,
            "skin": "CopyslFbox0efc73ba91cad4b",
            "top": "15dp",
            "width": "25px",
            "zIndex": 20
        }, controller.args[0], "flxPopUpClose"), extendConfig({}, controller.args[1], "flxPopUpClose"), extendConfig({}, controller.args[2], "flxPopUpClose"));
        flxPopUpClose.setDefaultUnit(kony.flex.DP);
        var lblPopUpClose = new kony.ui.Label(extendConfig({
            "centerY": "50%",
            "id": "lblPopUpClose",
            "isVisible": true,
            "left": "4dp",
            "skin": "sknIcon18pxGray",
            "i18n_text": "kony.i18n.getLocalizedString(\"i18n.userwidgetmodel.lblClearSearch\")",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, controller.args[0], "lblPopUpClose"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblPopUpClose"), extendConfig({}, controller.args[2], "lblPopUpClose"));
        var fontIconImgCLose = new kony.ui.Label(extendConfig({
            "centerX": "50%",
            "centerY": "50.00%",
            "height": "15dp",
            "id": "fontIconImgCLose",
            "isVisible": true,
            "skin": "sknFontIconSearchCross16px",
            "i18n_text": "kony.i18n.getLocalizedString(\"i18n.userwidgetmodel.lblClearSearch\")",
            "width": "15dp",
            "zIndex": 2
        }, controller.args[0], "fontIconImgCLose"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "fontIconImgCLose"), extendConfig({}, controller.args[2], "fontIconImgCLose"));
        flxPopUpClose.add(lblPopUpClose, fontIconImgCLose);
        var lblPopUpMainMessage = new kony.ui.Label(extendConfig({
            "id": "lblPopUpMainMessage",
            "isVisible": true,
            "left": "20px",
            "skin": "sknLbl192b45LatoReg16px",
            "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmCustomerManagement.CreateRetailProfile\")",
            "top": "0px",
            "width": "80%",
            "zIndex": 1
        }, controller.args[0], "lblPopUpMainMessage"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblPopUpMainMessage"), extendConfig({}, controller.args[2], "lblPopUpMainMessage"));
        var flxUsernameContainer = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "clipBounds": true,
            "height": "180dp",
            "id": "flxUsernameContainer",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "left": "0dp",
            "isModalContainer": false,
            "skin": "slFbox",
            "top": "0dp",
            "width": "100%",
            "zIndex": 20
        }, controller.args[0], "flxUsernameContainer"), extendConfig({}, controller.args[1], "flxUsernameContainer"), extendConfig({}, controller.args[2], "flxUsernameContainer"));
        flxUsernameContainer.setDefaultUnit(kony.flex.DP);
        var lblSubHeaderValue = new kony.ui.Label(extendConfig({
            "id": "lblSubHeaderValue",
            "isVisible": true,
            "left": "20px",
            "skin": "sknLbl485C75LatoRegular13Px",
            "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmCustomerManagement.EnterUsernameToCreateRetailProfile\")",
            "top": "30px",
            "width": "80%",
            "zIndex": 1
        }, controller.args[0], "lblSubHeaderValue"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblSubHeaderValue"), extendConfig({}, controller.args[2], "lblSubHeaderValue"));
        var usernameEntry = new com.adminConsole.common.textBoxEntry(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
            "id": "usernameEntry",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "left": "10dp",
            "masterType": constants.MASTER_TYPE_DEFAULT,
            "isModalContainer": false,
            "skin": "slFbox",
            "top": "65dp",
            "width": "305px",
            "zIndex": 1,
            "overrides": {
                "flxBtnCheck": {
                    "isVisible": true
                },
                "flxInlineError": {
                    "isVisible": false
                },
                "flxUserNameError": {
                    "isVisible": false
                },
                "lblCount": {
                    "isVisible": true,
                    "text": "Rules"
                },
                "lblHeadingText": {
                    "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmGroupsController.Username\")"
                },
                "textBoxEntry": {
                    "left": "10dp",
                    "top": "65dp",
                    "width": "305px"
                }
            }
        }, controller.args[0], "usernameEntry"), extendConfig({
            "overrides": {}
        }, controller.args[1], "usernameEntry"), extendConfig({
            "overrides": {}
        }, controller.args[2], "usernameEntry"));
        flxUsernameContainer.add(lblSubHeaderValue, usernameEntry);
        flxPopupHeader.add(flxPopUpClose, lblPopUpMainMessage, flxUsernameContainer);
        var flxUsernameRules = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
            "bottom": "17dp",
            "clipBounds": true,
            "id": "flxUsernameRules",
            "isVisible": false,
            "layoutType": kony.flex.FREE_FORM,
            "isModalContainer": false,
            "right": "50dp",
            "skin": "slFbox0a30851fc80b24b",
            "width": "370px",
            "zIndex": 10
        }, controller.args[0], "flxUsernameRules"), extendConfig({}, controller.args[1], "flxUsernameRules"), extendConfig({}, controller.args[2], "flxUsernameRules"));
        flxUsernameRules.setDefaultUnit(kony.flex.DP);
        var lblUsernameRules = new kony.ui.Label(extendConfig({
            "height": "15px",
            "id": "lblUsernameRules",
            "isVisible": true,
            "left": "10px",
            "skin": "slLabel0ff9cfa99545646",
            "text": "Username rules:",
            "top": "10px",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, controller.args[0], "lblUsernameRules"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblUsernameRules"), extendConfig({}, controller.args[2], "lblUsernameRules"));
        var flxUsernameRulesInner = new kony.ui.FlexScrollContainer(extendConfig({
            "allowHorizontalBounce": false,
            "allowVerticalBounce": true,
            "bottom": "10px",
            "bounces": true,
            "clipBounds": true,
            "enableScrolling": true,
            "height": "120px",
            "horizontalScrollIndicator": true,
            "id": "flxUsernameRulesInner",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "left": "10px",
            "pagingEnabled": false,
            "right": "10px",
            "scrollDirection": kony.flex.SCROLL_VERTICAL,
            "skin": "slFSbox",
            "top": "40px",
            "verticalScrollIndicator": true,
            "zIndex": 1
        }, controller.args[0], "flxUsernameRulesInner"), extendConfig({}, controller.args[1], "flxUsernameRulesInner"), extendConfig({}, controller.args[2], "flxUsernameRulesInner"));
        flxUsernameRulesInner.setDefaultUnit(kony.flex.DP);
        var rtxUsernameRules = new kony.ui.RichText(extendConfig({
            "bottom": "10px",
            "height": "100%",
            "id": "rtxUsernameRules",
            "isVisible": true,
            "left": "0px",
            "linkSkin": "defRichTextLink",
            "skin": "sknrtxLato485c7514px",
            "text": "<ul><li>Minimum Length- 8, Maximum Length- 24</li> <li>Can be alpha-numeric (a-z, A-Z, 0-9)</li> <li>No spaces or special characters allowed except period '.'</li> <li>Should Start with an alphabet or digit.</li> <li>Username is not case sensitive.</li> <li>Period '.' cannot be used more than once.</li></ul>",
            "top": "0px",
            "width": "100%",
            "zIndex": 1
        }, controller.args[0], "rtxUsernameRules"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_TOP_LEFT,
            "padding": [5, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "rtxUsernameRules"), extendConfig({}, controller.args[2], "rtxUsernameRules"));
        flxUsernameRulesInner.add(rtxUsernameRules);
        flxUsernameRules.add(lblUsernameRules, flxUsernameRulesInner);
        var flxPopUpButtons = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "bottom": "0dp",
            "clipBounds": true,
            "height": "80px",
            "id": "flxPopUpButtons",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "left": "0%",
            "isModalContainer": false,
            "skin": "slFbox0dc900c4572aa40",
            "width": "100%",
            "zIndex": 1
        }, controller.args[0], "flxPopUpButtons"), extendConfig({}, controller.args[1], "flxPopUpButtons"), extendConfig({}, controller.args[2], "flxPopUpButtons"));
        flxPopUpButtons.setDefaultUnit(kony.flex.DP);
        var btnPopUpCancel = new kony.ui.Button(extendConfig({
            "centerY": "50%",
            "focusSkin": "sknbtnf7f7faLatoRegular4f555dBorder1px8b96a5",
            "height": "40px",
            "id": "btnPopUpCancel",
            "isVisible": true,
            "right": "145px",
            "skin": "sknbtnf7f7faLatoRegular4f555dBorder1px8b96a5",
            "i18n_text": "kony.i18n.getLocalizedString(\"i18n.permission.CANCEL\")",
            "top": "0%",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 20
        }, controller.args[0], "btnPopUpCancel"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_CENTER,
            "displayText": true,
            "padding": [5, 0, 5, 0],
            "paddingInPixel": false
        }, controller.args[1], "btnPopUpCancel"), extendConfig({
            "hoverSkin": "sknBtnPopupHover485C75"
        }, controller.args[2], "btnPopUpCancel"));
        var btnCreatePopup = new kony.ui.Button(extendConfig({
            "centerY": "50%",
            "focusSkin": "sknBtn003E75LatoRegular13pxFFFFFFRad20px",
            "height": "40px",
            "id": "btnCreatePopup",
            "isVisible": true,
            "right": "20px",
            "skin": "sknBtn003E75LatoRegular13pxFFFFFFRad20px",
            "text": "CREATE",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 20
        }, controller.args[0], "btnCreatePopup"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_CENTER,
            "displayText": true,
            "padding": [5, 0, 5, 0],
            "paddingInPixel": false
        }, controller.args[1], "btnCreatePopup"), extendConfig({
            "hoverSkin": "sknBtn005198LatoRegular13pxFFFFFFRad20px"
        }, controller.args[2], "btnCreatePopup"));
        flxPopUpButtons.add(btnPopUpCancel, btnCreatePopup);
        flxDelinkProfilePopup.add(flxPopUpTopColor, flxPopupHeader, flxUsernameRules, flxPopUpButtons);
        delinkProfilePopup.add(flxDelinkProfilePopup);
        return delinkProfilePopup;
    }
})