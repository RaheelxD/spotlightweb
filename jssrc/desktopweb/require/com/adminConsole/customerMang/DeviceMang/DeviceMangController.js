define("com/adminConsole/customerMang/DeviceMang/userDeviceMangController", function() {
    var controller = {
        preShow: function() {
            var dataMap = {
                "lblSeperator": "lblSeperator"
            };
            var data = [{
                "lblSeperator": "-",
                "template": "flxDeviceManagement"
            }];
            this.view.segListing.widgetDataMap = dataMap;
            this.view.segListing.setData(data);
        },
    };
    return controller;
});
define("com/adminConsole/customerMang/DeviceMang/DeviceMangControllerActions", {
    /*
        This is an auto generated file and any modifications to it may result in corruption of the action sequence.
    */
    AS_FlexContainer_c44a677cfa71454da74841ec9b59ab31: function AS_FlexContainer_c44a677cfa71454da74841ec9b59ab31(eventobject) {
        var self = this;
        this.preshow();
    },
});
define("com/adminConsole/customerMang/DeviceMang/DeviceMangController", ["com/adminConsole/customerMang/DeviceMang/userDeviceMangController", "com/adminConsole/customerMang/DeviceMang/DeviceMangControllerActions"], function() {
    var controller = require("com/adminConsole/customerMang/DeviceMang/userDeviceMangController");
    var actions = require("com/adminConsole/customerMang/DeviceMang/DeviceMangControllerActions");
    for (var key in actions) {
        controller[key] = actions[key];
    }
    return controller;
});
