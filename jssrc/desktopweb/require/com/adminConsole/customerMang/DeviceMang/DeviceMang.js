define(function() {
    return function(controller) {
        var DeviceMang = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
            "clipBounds": true,
            "isMaster": true,
            "id": "DeviceMang",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "left": "0dp",
            "isModalContainer": false,
            "preShow": function(eventobject) {
                controller.AS_FlexContainer_c44a677cfa71454da74841ec9b59ab31(eventobject);
            },
            "skin": "sknflxffffffop100",
            "top": "0dp",
            "width": "100%"
        }, controller.args[0], "DeviceMang"), extendConfig({}, controller.args[1], "DeviceMang"), extendConfig({}, controller.args[2], "DeviceMang"));
        DeviceMang.setDefaultUnit(kony.flex.DP);
        var flxHeader = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "clipBounds": true,
            "height": "60px",
            "id": "flxHeader",
            "isVisible": true,
            "layoutType": kony.flex.FLOW_HORIZONTAL,
            "left": "35dp",
            "isModalContainer": false,
            "right": "35dp",
            "skin": "sknflxffffffop100",
            "top": "0"
        }, controller.args[0], "flxHeader"), extendConfig({}, controller.args[1], "flxHeader"), extendConfig({}, controller.args[2], "flxHeader"));
        flxHeader.setDefaultUnit(kony.flex.DP);
        var flxDeviceName = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "clipBounds": true,
            "height": "100%",
            "id": "flxDeviceName",
            "isVisible": true,
            "layoutType": kony.flex.FLOW_HORIZONTAL,
            "left": "0px",
            "isModalContainer": false,
            "onClick": controller.AS_FlexContainer_a43b80cc872f4831807a918bd48f0ac3,
            "skin": "slFbox",
            "top": "0dp",
            "width": "20.30%",
            "zIndex": 1
        }, controller.args[0], "flxDeviceName"), extendConfig({}, controller.args[1], "flxDeviceName"), extendConfig({}, controller.args[2], "flxDeviceName"));
        flxDeviceName.setDefaultUnit(kony.flex.DP);
        var lblDeviceName = new kony.ui.Label(extendConfig({
            "centerY": "50%",
            "id": "lblDeviceName",
            "isVisible": true,
            "left": "35px",
            "skin": "sknlblLato5d6c7f12px",
            "i18n_text": "kony.i18n.getLocalizedString(\"i18n.permission.NAME\")",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, controller.args[0], "lblDeviceName"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblDeviceName"), extendConfig({}, controller.args[2], "lblDeviceName"));
        flxDeviceName.add(lblDeviceName);
        var flxDeviceVersion = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "clipBounds": true,
            "height": "100%",
            "id": "flxDeviceVersion",
            "isVisible": true,
            "layoutType": kony.flex.FLOW_HORIZONTAL,
            "left": "0px",
            "isModalContainer": false,
            "onClick": controller.AS_FlexContainer_a43b80cc872f4831807a918bd48f0ac3,
            "skin": "slFbox",
            "top": "0dp",
            "width": "14.20%",
            "zIndex": 1
        }, controller.args[0], "flxDeviceVersion"), extendConfig({}, controller.args[1], "flxDeviceVersion"), extendConfig({}, controller.args[2], "flxDeviceVersion"));
        flxDeviceVersion.setDefaultUnit(kony.flex.DP);
        var lblDeviceVersion = new kony.ui.Label(extendConfig({
            "centerY": "50%",
            "id": "lblDeviceVersion",
            "isVisible": true,
            "left": "0px",
            "skin": "sknlblLato5d6c7f12px",
            "i18n_text": "kony.i18n.getLocalizedString(\"i18n.customermanagement.deviceversion\")",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, controller.args[0], "lblDeviceVersion"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblDeviceVersion"), extendConfig({}, controller.args[2], "lblDeviceVersion"));
        flxDeviceVersion.add(lblDeviceVersion);
        var flxChannel = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "clipBounds": true,
            "height": "100%",
            "id": "flxChannel",
            "isVisible": true,
            "layoutType": kony.flex.FLOW_HORIZONTAL,
            "isModalContainer": false,
            "onClick": controller.AS_FlexContainer_f184888da9f042978e3b0332711e0319,
            "skin": "slFbox",
            "top": "0dp",
            "width": "13%",
            "zIndex": 1
        }, controller.args[0], "flxChannel"), extendConfig({}, controller.args[1], "flxChannel"), extendConfig({}, controller.args[2], "flxChannel"));
        flxChannel.setDefaultUnit(kony.flex.DP);
        var lblChannel = new kony.ui.Label(extendConfig({
            "centerY": "50%",
            "id": "lblChannel",
            "isVisible": true,
            "left": "0px",
            "skin": "sknlblLato5d6c7f12px",
            "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmCustomerManagementController.CHANNEL\")",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, controller.args[0], "lblChannel"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblChannel"), extendConfig({
            "hoverSkin": "sknlblLatoBold00000012px"
        }, controller.args[2], "lblChannel"));
        var fontIconSortChannel = new kony.ui.Label(extendConfig({
            "centerY": "50%",
            "id": "fontIconSortChannel",
            "isVisible": true,
            "left": "2dp",
            "skin": "sknIcon15px",
            "text": "",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, controller.args[0], "fontIconSortChannel"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "fontIconSortChannel"), extendConfig({
            "hoverSkin": "sknlblCursorFont"
        }, controller.args[2], "fontIconSortChannel"));
        flxChannel.add(lblChannel, fontIconSortChannel);
        var flxLastUserIP = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "clipBounds": true,
            "height": "100%",
            "id": "flxLastUserIP",
            "isVisible": true,
            "layoutType": kony.flex.FLOW_HORIZONTAL,
            "isModalContainer": false,
            "onClick": controller.AS_FlexContainer_if8688b9f1b14c0796ccfa73761ffbf7,
            "skin": "slFbox",
            "top": "0dp",
            "width": "20%",
            "zIndex": 1
        }, controller.args[0], "flxLastUserIP"), extendConfig({}, controller.args[1], "flxLastUserIP"), extendConfig({}, controller.args[2], "flxLastUserIP"));
        flxLastUserIP.setDefaultUnit(kony.flex.DP);
        var lblLastUserIP = new kony.ui.Label(extendConfig({
            "centerY": "50%",
            "id": "lblLastUserIP",
            "isVisible": true,
            "left": "0px",
            "skin": "sknlblLato5d6c7f12px",
            "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmCustomerManagementController.LastIP\")",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, controller.args[0], "lblLastUserIP"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblLastUserIP"), extendConfig({
            "hoverSkin": "sknlblLatoBold00000012px"
        }, controller.args[2], "lblLastUserIP"));
        var fontIconSortLastUserIP = new kony.ui.Label(extendConfig({
            "centerY": "50%",
            "id": "fontIconSortLastUserIP",
            "isVisible": true,
            "left": "2dp",
            "skin": "sknIcon15px",
            "text": "",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, controller.args[0], "fontIconSortLastUserIP"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "fontIconSortLastUserIP"), extendConfig({
            "hoverSkin": "sknlblCursorFont"
        }, controller.args[2], "fontIconSortLastUserIP"));
        flxLastUserIP.add(lblLastUserIP, fontIconSortLastUserIP);
        var flxLastAcessedOn = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "clipBounds": true,
            "height": "100%",
            "id": "flxLastAcessedOn",
            "isVisible": true,
            "layoutType": kony.flex.FLOW_HORIZONTAL,
            "isModalContainer": false,
            "onClick": controller.AS_FlexContainer_e8c18fc5a3a34ece96d2cc198ff5d25a,
            "skin": "slFbox",
            "top": "0dp",
            "width": "22.30%",
            "zIndex": 1
        }, controller.args[0], "flxLastAcessedOn"), extendConfig({}, controller.args[1], "flxLastAcessedOn"), extendConfig({}, controller.args[2], "flxLastAcessedOn"));
        flxLastAcessedOn.setDefaultUnit(kony.flex.DP);
        var lblLastAcessedOn = new kony.ui.Label(extendConfig({
            "centerY": "50%",
            "id": "lblLastAcessedOn",
            "isVisible": true,
            "left": "0px",
            "skin": "sknlblLato5d6c7f12px",
            "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmCustomerManagementController.LastAccessedOn\")",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, controller.args[0], "lblLastAcessedOn"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblLastAcessedOn"), extendConfig({
            "hoverSkin": "sknlblLatoBold00000012px"
        }, controller.args[2], "lblLastAcessedOn"));
        var fontIconSortLastAcessedOn = new kony.ui.Label(extendConfig({
            "centerY": "50%",
            "id": "fontIconSortLastAcessedOn",
            "isVisible": true,
            "left": "2dp",
            "skin": "sknIcon15px",
            "text": "",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, controller.args[0], "fontIconSortLastAcessedOn"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "fontIconSortLastAcessedOn"), extendConfig({
            "hoverSkin": "sknlblCursorFont"
        }, controller.args[2], "fontIconSortLastAcessedOn"));
        flxLastAcessedOn.add(lblLastAcessedOn, fontIconSortLastAcessedOn);
        var flxStatus = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "clipBounds": true,
            "height": "100%",
            "id": "flxStatus",
            "isVisible": true,
            "layoutType": kony.flex.FLOW_HORIZONTAL,
            "isModalContainer": false,
            "onClick": controller.AS_FlexContainer_ia2738f1fdda45cf914f716c251ff157,
            "skin": "slFbox",
            "top": "0dp",
            "width": "10%",
            "zIndex": 1
        }, controller.args[0], "flxStatus"), extendConfig({}, controller.args[1], "flxStatus"), extendConfig({}, controller.args[2], "flxStatus"));
        flxStatus.setDefaultUnit(kony.flex.DP);
        var lblStatus = new kony.ui.Label(extendConfig({
            "centerY": "50%",
            "id": "lblStatus",
            "isVisible": true,
            "left": "0px",
            "skin": "sknlblLato5d6c7f12px",
            "i18n_text": "kony.i18n.getLocalizedString(\"i18n.roles.STATUS\")",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, controller.args[0], "lblStatus"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblStatus"), extendConfig({
            "hoverSkin": "sknlblLatoBold00000012px"
        }, controller.args[2], "lblStatus"));
        var fontIconFilterStatus = new kony.ui.Label(extendConfig({
            "centerY": "50%",
            "id": "fontIconFilterStatus",
            "isVisible": true,
            "left": "2dp",
            "skin": "sknIcon15px",
            "text": "",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, controller.args[0], "fontIconFilterStatus"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "fontIconFilterStatus"), extendConfig({
            "hoverSkin": "sknlblCursorFont"
        }, controller.args[2], "fontIconFilterStatus"));
        flxStatus.add(lblStatus, fontIconFilterStatus);
        flxHeader.add(flxDeviceName, flxDeviceVersion, flxChannel, flxLastUserIP, flxLastAcessedOn, flxStatus);
        var lblHeaderSeperator = new kony.ui.Label(extendConfig({
            "bottom": 0,
            "height": "1px",
            "id": "lblHeaderSeperator",
            "isVisible": true,
            "left": "35dp",
            "right": 35,
            "skin": "sknLblTableHeaderLine",
            "text": ".",
            "top": "60px",
            "zIndex": 1
        }, controller.args[0], "lblHeaderSeperator"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblHeaderSeperator"), extendConfig({}, controller.args[2], "lblHeaderSeperator"));
        var flxDevicesSegment = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
            "clipBounds": true,
            "id": "flxDevicesSegment",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "left": "35dp",
            "isModalContainer": false,
            "right": "35dp",
            "skin": "slFbox",
            "top": "61dp",
            "zIndex": 1
        }, controller.args[0], "flxDevicesSegment"), extendConfig({}, controller.args[1], "flxDevicesSegment"), extendConfig({}, controller.args[2], "flxDevicesSegment"));
        flxDevicesSegment.setDefaultUnit(kony.flex.DP);
        var segListing = new kony.ui.SegmentedUI2(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "bottom": "20px",
            "data": [
                [{
                        "imgSortCstomers": "sorting3x.png",
                        "imgSortEntitlements": "sorting3x.png",
                        "imgSortGroupRoles": "sorting3x.png",
                        "imgSortGroupStatus": "u195.png",
                        "imgSortName": "sorting3x.png",
                        "lblGroupCustomerss": "kony.i18n.getLocalizedString(\"i18n.Group.Customers\")",
                        "lblGroupEntitlements": "kony.i18n.getLocalizedString(\"i18n.Group.Entitlements\")",
                        "lblGroupName": "kony.i18n.getLocalizedString(\"i18n.permission.NAME\")",
                        "lblGroupStatus": "kony.i18n.getLocalizedString(\"i18n.permission.STATUS\")",
                        "lblGroupValidTill": "kony.i18n.getLocalizedString(\"i18n.Group.ValidTill\")",
                        "lblHeaderGroupDescription": "kony.i18n.getLocalizedString(\"i18n.permission.DESCRIPTION\")"
                    },
                    [{
                        "fontIconDeviceInfo": "",
                        "lblChannel": "kony.i18n.getLocalizedString(\"i18n.frmCustomerManagementController.CHANNEL\")",
                        "lblDeviceName": "kony.i18n.getLocalizedString(\"i18n.permission.NAME\")",
                        "lblLastAcessedOn": "kony.i18n.getLocalizedString(\"i18n.frmCustomerManagementController.LastAccessedOn\")",
                        "lblLastUserIP": "kony.i18n.getLocalizedString(\"i18n.frmCustomerManagementController.LastIP\")",
                        "lblSeperator": ".",
                        "lblStatus": "kony.i18n.getLocalizedString(\"i18n.roles.STATUS\")"
                    }, {
                        "fontIconDeviceInfo": "",
                        "lblChannel": "kony.i18n.getLocalizedString(\"i18n.frmCustomerManagementController.CHANNEL\")",
                        "lblDeviceName": "kony.i18n.getLocalizedString(\"i18n.permission.NAME\")",
                        "lblLastAcessedOn": "kony.i18n.getLocalizedString(\"i18n.frmCustomerManagementController.LastAccessedOn\")",
                        "lblLastUserIP": "kony.i18n.getLocalizedString(\"i18n.frmCustomerManagementController.LastIP\")",
                        "lblSeperator": ".",
                        "lblStatus": "kony.i18n.getLocalizedString(\"i18n.roles.STATUS\")"
                    }, {
                        "fontIconDeviceInfo": "",
                        "lblChannel": "kony.i18n.getLocalizedString(\"i18n.frmCustomerManagementController.CHANNEL\")",
                        "lblDeviceName": "kony.i18n.getLocalizedString(\"i18n.permission.NAME\")",
                        "lblLastAcessedOn": "kony.i18n.getLocalizedString(\"i18n.frmCustomerManagementController.LastAccessedOn\")",
                        "lblLastUserIP": "kony.i18n.getLocalizedString(\"i18n.frmCustomerManagementController.LastIP\")",
                        "lblSeperator": ".",
                        "lblStatus": "kony.i18n.getLocalizedString(\"i18n.roles.STATUS\")"
                    }, {
                        "fontIconDeviceInfo": "",
                        "lblChannel": "kony.i18n.getLocalizedString(\"i18n.frmCustomerManagementController.CHANNEL\")",
                        "lblDeviceName": "kony.i18n.getLocalizedString(\"i18n.permission.NAME\")",
                        "lblLastAcessedOn": "kony.i18n.getLocalizedString(\"i18n.frmCustomerManagementController.LastAccessedOn\")",
                        "lblLastUserIP": "kony.i18n.getLocalizedString(\"i18n.frmCustomerManagementController.LastIP\")",
                        "lblSeperator": ".",
                        "lblStatus": "kony.i18n.getLocalizedString(\"i18n.roles.STATUS\")"
                    }, {
                        "fontIconDeviceInfo": "",
                        "lblChannel": "kony.i18n.getLocalizedString(\"i18n.frmCustomerManagementController.CHANNEL\")",
                        "lblDeviceName": "kony.i18n.getLocalizedString(\"i18n.permission.NAME\")",
                        "lblLastAcessedOn": "kony.i18n.getLocalizedString(\"i18n.frmCustomerManagementController.LastAccessedOn\")",
                        "lblLastUserIP": "kony.i18n.getLocalizedString(\"i18n.frmCustomerManagementController.LastIP\")",
                        "lblSeperator": ".",
                        "lblStatus": "kony.i18n.getLocalizedString(\"i18n.roles.STATUS\")"
                    }, {
                        "fontIconDeviceInfo": "",
                        "lblChannel": "kony.i18n.getLocalizedString(\"i18n.frmCustomerManagementController.CHANNEL\")",
                        "lblDeviceName": "kony.i18n.getLocalizedString(\"i18n.permission.NAME\")",
                        "lblLastAcessedOn": "kony.i18n.getLocalizedString(\"i18n.frmCustomerManagementController.LastAccessedOn\")",
                        "lblLastUserIP": "kony.i18n.getLocalizedString(\"i18n.frmCustomerManagementController.LastIP\")",
                        "lblSeperator": ".",
                        "lblStatus": "kony.i18n.getLocalizedString(\"i18n.roles.STATUS\")"
                    }]
                ],
                [{
                        "imgSortCstomers": "sorting3x.png",
                        "imgSortEntitlements": "sorting3x.png",
                        "imgSortGroupRoles": "sorting3x.png",
                        "imgSortGroupStatus": "u195.png",
                        "imgSortName": "sorting3x.png",
                        "lblGroupCustomerss": "kony.i18n.getLocalizedString(\"i18n.Group.Customers\")",
                        "lblGroupEntitlements": "kony.i18n.getLocalizedString(\"i18n.Group.Entitlements\")",
                        "lblGroupName": "kony.i18n.getLocalizedString(\"i18n.permission.NAME\")",
                        "lblGroupStatus": "kony.i18n.getLocalizedString(\"i18n.permission.STATUS\")",
                        "lblGroupValidTill": "kony.i18n.getLocalizedString(\"i18n.Group.ValidTill\")",
                        "lblHeaderGroupDescription": "kony.i18n.getLocalizedString(\"i18n.permission.DESCRIPTION\")"
                    },
                    [{
                        "fontIconDeviceInfo": "",
                        "lblChannel": "kony.i18n.getLocalizedString(\"i18n.frmCustomerManagementController.CHANNEL\")",
                        "lblDeviceName": "kony.i18n.getLocalizedString(\"i18n.permission.NAME\")",
                        "lblLastAcessedOn": "kony.i18n.getLocalizedString(\"i18n.frmCustomerManagementController.LastAccessedOn\")",
                        "lblLastUserIP": "kony.i18n.getLocalizedString(\"i18n.frmCustomerManagementController.LastIP\")",
                        "lblSeperator": ".",
                        "lblStatus": "kony.i18n.getLocalizedString(\"i18n.roles.STATUS\")"
                    }, {
                        "fontIconDeviceInfo": "",
                        "lblChannel": "kony.i18n.getLocalizedString(\"i18n.frmCustomerManagementController.CHANNEL\")",
                        "lblDeviceName": "kony.i18n.getLocalizedString(\"i18n.permission.NAME\")",
                        "lblLastAcessedOn": "kony.i18n.getLocalizedString(\"i18n.frmCustomerManagementController.LastAccessedOn\")",
                        "lblLastUserIP": "kony.i18n.getLocalizedString(\"i18n.frmCustomerManagementController.LastIP\")",
                        "lblSeperator": ".",
                        "lblStatus": "kony.i18n.getLocalizedString(\"i18n.roles.STATUS\")"
                    }, {
                        "fontIconDeviceInfo": "",
                        "lblChannel": "kony.i18n.getLocalizedString(\"i18n.frmCustomerManagementController.CHANNEL\")",
                        "lblDeviceName": "kony.i18n.getLocalizedString(\"i18n.permission.NAME\")",
                        "lblLastAcessedOn": "kony.i18n.getLocalizedString(\"i18n.frmCustomerManagementController.LastAccessedOn\")",
                        "lblLastUserIP": "kony.i18n.getLocalizedString(\"i18n.frmCustomerManagementController.LastIP\")",
                        "lblSeperator": ".",
                        "lblStatus": "kony.i18n.getLocalizedString(\"i18n.roles.STATUS\")"
                    }, {
                        "fontIconDeviceInfo": "",
                        "lblChannel": "kony.i18n.getLocalizedString(\"i18n.frmCustomerManagementController.CHANNEL\")",
                        "lblDeviceName": "kony.i18n.getLocalizedString(\"i18n.permission.NAME\")",
                        "lblLastAcessedOn": "kony.i18n.getLocalizedString(\"i18n.frmCustomerManagementController.LastAccessedOn\")",
                        "lblLastUserIP": "kony.i18n.getLocalizedString(\"i18n.frmCustomerManagementController.LastIP\")",
                        "lblSeperator": ".",
                        "lblStatus": "kony.i18n.getLocalizedString(\"i18n.roles.STATUS\")"
                    }, {
                        "fontIconDeviceInfo": "",
                        "lblChannel": "kony.i18n.getLocalizedString(\"i18n.frmCustomerManagementController.CHANNEL\")",
                        "lblDeviceName": "kony.i18n.getLocalizedString(\"i18n.permission.NAME\")",
                        "lblLastAcessedOn": "kony.i18n.getLocalizedString(\"i18n.frmCustomerManagementController.LastAccessedOn\")",
                        "lblLastUserIP": "kony.i18n.getLocalizedString(\"i18n.frmCustomerManagementController.LastIP\")",
                        "lblSeperator": ".",
                        "lblStatus": "kony.i18n.getLocalizedString(\"i18n.roles.STATUS\")"
                    }, {
                        "fontIconDeviceInfo": "",
                        "lblChannel": "kony.i18n.getLocalizedString(\"i18n.frmCustomerManagementController.CHANNEL\")",
                        "lblDeviceName": "kony.i18n.getLocalizedString(\"i18n.permission.NAME\")",
                        "lblLastAcessedOn": "kony.i18n.getLocalizedString(\"i18n.frmCustomerManagementController.LastAccessedOn\")",
                        "lblLastUserIP": "kony.i18n.getLocalizedString(\"i18n.frmCustomerManagementController.LastIP\")",
                        "lblSeperator": ".",
                        "lblStatus": "kony.i18n.getLocalizedString(\"i18n.roles.STATUS\")"
                    }]
                ]
            ],
            "groupCells": false,
            "height": "400px",
            "id": "segListing",
            "isVisible": true,
            "left": "0px",
            "needPageIndicator": true,
            "pageOffDotImage": "pageoffdot.png",
            "pageOnDotImage": "pageondot.png",
            "retainSelection": false,
            "rowFocusSkin": "seg2Focus",
            "rowSkin": "seg2Normal",
            "rowTemplate": "flxDeviceManagement",
            "sectionHeaderSkin": "sliPhoneSegmentHeader",
            "selectionBehavior": constants.SEGUI_DEFAULT_BEHAVIOR,
            "separatorRequired": false,
            "showScrollbars": false,
            "top": "0px",
            "viewType": constants.SEGUI_VIEW_TYPE_TABLEVIEW,
            "widgetDataMap": {
                "flxDeviceInfo": "flxDeviceInfo",
                "flxDeviceManagement": "flxDeviceManagement",
                "flxDeviceName": "flxDeviceName",
                "flxRow": "flxRow",
                "fontIconDeviceInfo": "fontIconDeviceInfo",
                "lblChannel": "lblChannel",
                "lblDeviceName": "lblDeviceName",
                "lblLastAcessedOn": "lblLastAcessedOn",
                "lblLastUserIP": "lblLastUserIP",
                "lblSeperator": "lblSeperator",
                "lblStatus": "lblStatus"
            },
            "width": "100%",
            "zIndex": 1
        }, controller.args[0], "segListing"), extendConfig({
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "segListing"), extendConfig({}, controller.args[2], "segListing"));
        flxDevicesSegment.add(segListing);
        var flxDeviceInfo = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "clipBounds": true,
            "height": "85px",
            "id": "flxDeviceInfo",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "left": "202dp",
            "isModalContainer": false,
            "right": "35dp",
            "skin": "slFbox",
            "top": "71dp",
            "width": "250px",
            "zIndex": 10
        }, controller.args[0], "flxDeviceInfo"), extendConfig({}, controller.args[1], "flxDeviceInfo"), extendConfig({}, controller.args[2], "flxDeviceInfo"));
        flxDeviceInfo.setDefaultUnit(kony.flex.DP);
        var deviceInfo = new com.adminConsole.customerMang.deviceInfo(extendConfig({
            "height": "85dp",
            "id": "deviceInfo",
            "isVisible": false,
            "layoutType": kony.flex.FREE_FORM,
            "left": "0dp",
            "masterType": constants.MASTER_TYPE_DEFAULT,
            "isModalContainer": false,
            "skin": "CopyslFbox0a8186fa22dd549",
            "top": "0dp",
            "width": "250px",
            "overrides": {
                "deviceInfo": {
                    "isVisible": false
                },
                "flxCopyTooltip": {
                    "isVisible": false
                },
                "fonticonCopy": {
                    "isVisible": false
                },
                "imgLeftArrow": {
                    "src": "arrowvariableref1x.png"
                }
            }
        }, controller.args[0], "deviceInfo"), extendConfig({
            "overrides": {}
        }, controller.args[1], "deviceInfo"), extendConfig({
            "overrides": {}
        }, controller.args[2], "deviceInfo"));
        flxDeviceInfo.add(deviceInfo);
        var flxStatusInfo = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "clipBounds": true,
            "height": "20px",
            "id": "flxStatusInfo",
            "isVisible": false,
            "layoutType": kony.flex.FREE_FORM,
            "isModalContainer": false,
            "right": "70dp",
            "skin": "slFbox",
            "top": "77dp",
            "width": "20px",
            "zIndex": 10
        }, controller.args[0], "flxStatusInfo"), extendConfig({}, controller.args[1], "flxStatusInfo"), extendConfig({
            "hoverSkin": "sknCursor"
        }, controller.args[2], "flxStatusInfo"));
        flxStatusInfo.setDefaultUnit(kony.flex.DP);
        var fontIconStatusInfo = new kony.ui.Label(extendConfig({
            "centerY": "50%",
            "id": "fontIconStatusInfo",
            "isVisible": true,
            "left": "0dp",
            "skin": "sknfontIcon13px",
            "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmFAQ.fonticonActiveSearch\")",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, controller.args[0], "fontIconStatusInfo"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "fontIconStatusInfo"), extendConfig({}, controller.args[2], "fontIconStatusInfo"));
        flxStatusInfo.add(fontIconStatusInfo);
        var flxTooltip = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "clipBounds": true,
            "height": "30px",
            "id": "flxTooltip",
            "isVisible": false,
            "layoutType": kony.flex.FREE_FORM,
            "left": "36px",
            "isModalContainer": false,
            "skin": "slFbox",
            "top": "2dp",
            "width": "100px",
            "zIndex": 20
        }, controller.args[0], "flxTooltip"), extendConfig({}, controller.args[1], "flxTooltip"), extendConfig({}, controller.args[2], "flxTooltip"));
        flxTooltip.setDefaultUnit(kony.flex.DP);
        var customTooltip = new com.adminConsole.common.tooltip(extendConfig({
            "height": "30px",
            "id": "customTooltip",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "left": "0dp",
            "masterType": constants.MASTER_TYPE_DEFAULT,
            "isModalContainer": false,
            "skin": "slFbox",
            "top": "0dp",
            "width": "100%",
            "zIndex": 10,
            "overrides": {
                "lblNoConcentToolTip": {
                    "text": "Member "
                },
                "tooltip": {
                    "isVisible": true,
                    "zIndex": 10
                }
            }
        }, controller.args[0], "customTooltip"), extendConfig({
            "overrides": {}
        }, controller.args[1], "customTooltip"), extendConfig({
            "overrides": {}
        }, controller.args[2], "customTooltip"));
        flxTooltip.add(customTooltip);
        var flxStatusFilter = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
            "clipBounds": true,
            "id": "flxStatusFilter",
            "isVisible": false,
            "layoutType": kony.flex.FREE_FORM,
            "isModalContainer": false,
            "right": "100dp",
            "skin": "slFbox",
            "top": "33dp",
            "width": "125px",
            "zIndex": 10
        }, controller.args[0], "flxStatusFilter"), extendConfig({}, controller.args[1], "flxStatusFilter"), extendConfig({}, controller.args[2], "flxStatusFilter"));
        flxStatusFilter.setDefaultUnit(kony.flex.DP);
        var statusFilterMenu = new com.adminConsole.common.statusFilterMenu(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
            "id": "statusFilterMenu",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "left": "0dp",
            "masterType": constants.MASTER_TYPE_DEFAULT,
            "isModalContainer": false,
            "skin": "slFbox",
            "top": "5dp",
            "width": "100%",
            "zIndex": 5,
            "overrides": {
                "imgUpArrow": {
                    "src": "uparrow_2x.png"
                }
            }
        }, controller.args[0], "statusFilterMenu"), extendConfig({
            "overrides": {}
        }, controller.args[1], "statusFilterMenu"), extendConfig({
            "overrides": {}
        }, controller.args[2], "statusFilterMenu"));
        flxStatusFilter.add(statusFilterMenu);
        var flxChannelFilter = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
            "clipBounds": true,
            "id": "flxChannelFilter",
            "isVisible": false,
            "layoutType": kony.flex.FREE_FORM,
            "isModalContainer": false,
            "right": "240dp",
            "skin": "slFbox",
            "top": "33dp",
            "width": "110px",
            "zIndex": 10
        }, controller.args[0], "flxChannelFilter"), extendConfig({}, controller.args[1], "flxChannelFilter"), extendConfig({}, controller.args[2], "flxChannelFilter"));
        flxChannelFilter.setDefaultUnit(kony.flex.DP);
        var channelFilterMenu = new com.adminConsole.common.statusFilterMenu(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
            "id": "channelFilterMenu",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "left": "0dp",
            "masterType": constants.MASTER_TYPE_DEFAULT,
            "isModalContainer": false,
            "skin": "slFbox",
            "top": "5dp",
            "width": "100%",
            "zIndex": 5,
            "overrides": {
                "imgUpArrow": {
                    "src": "uparrow_2x.png"
                }
            }
        }, controller.args[0], "channelFilterMenu"), extendConfig({
            "overrides": {}
        }, controller.args[1], "channelFilterMenu"), extendConfig({
            "overrides": {}
        }, controller.args[2], "channelFilterMenu"));
        flxChannelFilter.add(channelFilterMenu);
        DeviceMang.add(flxHeader, lblHeaderSeperator, flxDevicesSegment, flxDeviceInfo, flxStatusInfo, flxTooltip, flxStatusFilter, flxChannelFilter);
        return DeviceMang;
    }
})