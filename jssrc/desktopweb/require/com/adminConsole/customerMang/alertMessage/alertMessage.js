define(function() {
    return function(controller) {
        var alertMessage = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "clipBounds": true,
            "isMaster": true,
            "height": "70px",
            "id": "alertMessage",
            "isVisible": true,
            "layoutType": kony.flex.FLOW_HORIZONTAL,
            "left": "0dp",
            "isModalContainer": false,
            "skin": "sknYellowBorder",
            "top": "0dp",
            "width": "100%"
        }, controller.args[0], "alertMessage"), extendConfig({}, controller.args[1], "alertMessage"), extendConfig({}, controller.args[2], "alertMessage"));
        alertMessage.setDefaultUnit(kony.flex.DP);
        var flxLeftImage = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "clipBounds": true,
            "height": "100%",
            "id": "flxLeftImage",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "left": "0dp",
            "isModalContainer": false,
            "skin": "sknYellowFill",
            "top": "0dp",
            "width": "50px"
        }, controller.args[0], "flxLeftImage"), extendConfig({}, controller.args[1], "flxLeftImage"), extendConfig({}, controller.args[2], "flxLeftImage"));
        flxLeftImage.setDefaultUnit(kony.flex.DP);
        var fontIconImgLeft = new kony.ui.Label(extendConfig({
            "centerX": "50%",
            "centerY": "50%",
            "height": "20dp",
            "id": "fontIconImgLeft",
            "isVisible": true,
            "left": "15dp",
            "skin": "sknFontIconWhite",
            "text": "",
            "top": "15dp",
            "width": "20dp",
            "zIndex": 1
        }, controller.args[0], "fontIconImgLeft"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "fontIconImgLeft"), extendConfig({}, controller.args[2], "fontIconImgLeft"));
        flxLeftImage.add(fontIconImgLeft);
        var flxMessage = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "clipBounds": true,
            "height": "100%",
            "id": "flxMessage",
            "isVisible": true,
            "layoutType": kony.flex.FLOW_VERTICAL,
            "left": "0px",
            "isModalContainer": false,
            "skin": "slFbox",
            "top": "0dp",
            "width": "100%",
            "zIndex": 1
        }, controller.args[0], "flxMessage"), extendConfig({}, controller.args[1], "flxMessage"), extendConfig({}, controller.args[2], "flxMessage"));
        flxMessage.setDefaultUnit(kony.flex.DP);
        var flxRow1 = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "clipBounds": true,
            "height": "35%",
            "id": "flxRow1",
            "isVisible": true,
            "layoutType": kony.flex.FLOW_HORIZONTAL,
            "left": "20dp",
            "isModalContainer": false,
            "skin": "slFbox",
            "top": "15%",
            "width": "100%"
        }, controller.args[0], "flxRow1"), extendConfig({}, controller.args[1], "flxRow1"), extendConfig({}, controller.args[2], "flxRow1"));
        flxRow1.setDefaultUnit(kony.flex.DP);
        var fonticonBullet1 = new kony.ui.Label(extendConfig({
            "centerY": "50%",
            "id": "fonticonBullet1",
            "isVisible": true,
            "left": "0px",
            "skin": "sknCustomersBullet",
            "text": "",
            "top": "0px",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, controller.args[0], "fonticonBullet1"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "fonticonBullet1"), extendConfig({}, controller.args[2], "fonticonBullet1"));
        var lblData = new kony.ui.Label(extendConfig({
            "centerY": "50%",
            "id": "lblData",
            "isVisible": true,
            "left": "5px",
            "skin": "sknlblLatoBold35475f14px",
            "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmCustomerManagementController.LockInformation\")",
            "top": "0px",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, controller.args[0], "lblData"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblData"), extendConfig({}, controller.args[2], "lblData"));
        flxRow1.add(fonticonBullet1, lblData);
        var flxRow2 = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "bottom": "15%",
            "clipBounds": true,
            "height": "35%",
            "id": "flxRow2",
            "isVisible": true,
            "layoutType": kony.flex.FLOW_HORIZONTAL,
            "left": "20dp",
            "isModalContainer": false,
            "skin": "slFbox",
            "width": "100%",
            "zIndex": 1
        }, controller.args[0], "flxRow2"), extendConfig({}, controller.args[1], "flxRow2"), extendConfig({}, controller.args[2], "flxRow2"));
        flxRow2.setDefaultUnit(kony.flex.DP);
        var fonticonBullet2 = new kony.ui.Label(extendConfig({
            "centerY": "50%",
            "id": "fonticonBullet2",
            "isVisible": true,
            "left": "0px",
            "skin": "sknCustomersBullet",
            "text": "",
            "top": "0px",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, controller.args[0], "fonticonBullet2"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "fonticonBullet2"), extendConfig({}, controller.args[2], "fonticonBullet2"));
        var lblData1 = new kony.ui.Label(extendConfig({
            "centerY": "50%",
            "id": "lblData1",
            "isVisible": true,
            "left": "5px",
            "skin": "sknlblLatoBold35475f14px",
            "text": "There are ",
            "top": "0px",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, controller.args[0], "lblData1"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblData1"), extendConfig({}, controller.args[2], "lblData1"));
        var btnDataLink2 = new kony.ui.Button(extendConfig({
            "centerY": "50%",
            "id": "btnDataLink2",
            "isVisible": true,
            "left": "5dp",
            "skin": "sknBtnLato117eb013Px",
            "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmCustomerManagementController.notificationsCount\")",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, controller.args[0], "btnDataLink2"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_CENTER,
            "displayText": true,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "btnDataLink2"), extendConfig({}, controller.args[2], "btnDataLink2"));
        var lblData2 = new kony.ui.Label(extendConfig({
            "centerY": "50%",
            "id": "lblData2",
            "isVisible": true,
            "left": "8px",
            "skin": "sknlblLatoBold35475f14px",
            "text": "and",
            "top": "0px",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, controller.args[0], "lblData2"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblData2"), extendConfig({}, controller.args[2], "lblData2"));
        var btnDataLink1 = new kony.ui.Button(extendConfig({
            "centerY": "50%",
            "id": "btnDataLink1",
            "isVisible": true,
            "left": "5dp",
            "right": "1px",
            "skin": "sknBtnLato117eb013Px",
            "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmCustomerManagementController.Active_requests\")",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, controller.args[0], "btnDataLink1"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_CENTER,
            "displayText": true,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "btnDataLink1"), extendConfig({}, controller.args[2], "btnDataLink1"));
        var lblData3 = new kony.ui.Label(extendConfig({
            "centerY": "50%",
            "id": "lblData3",
            "isVisible": true,
            "left": "2px",
            "skin": "sknlblLatoBold35475f14px",
            "text": ".",
            "top": "0px",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, controller.args[0], "lblData3"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblData3"), extendConfig({}, controller.args[2], "lblData3"));
        flxRow2.add(fonticonBullet2, lblData1, btnDataLink2, lblData2, btnDataLink1, lblData3);
        flxMessage.add(flxRow1, flxRow2);
        alertMessage.add(flxLeftImage, flxMessage);
        return alertMessage;
    }
})