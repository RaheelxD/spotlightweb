define(function() {
    return function(controller) {
        var backToPageHeader = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "clipBounds": true,
            "isMaster": true,
            "height": "20px",
            "id": "backToPageHeader",
            "isVisible": true,
            "layoutType": kony.flex.FLOW_HORIZONTAL,
            "left": "0dp",
            "isModalContainer": false,
            "skin": "slFbox",
            "top": "0dp",
            "width": "100%"
        }, controller.args[0], "backToPageHeader"), extendConfig({}, controller.args[1], "backToPageHeader"), extendConfig({}, controller.args[2], "backToPageHeader"));
        backToPageHeader.setDefaultUnit(kony.flex.DP);
        var flxBack = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "centerY": "50%",
            "clipBounds": true,
            "height": "20dp",
            "id": "flxBack",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "left": "35px",
            "isModalContainer": false,
            "skin": "sknCursor",
            "top": "16dp",
            "width": "20px",
            "zIndex": 1
        }, controller.args[0], "flxBack"), extendConfig({}, controller.args[1], "flxBack"), extendConfig({}, controller.args[2], "flxBack"));
        flxBack.setDefaultUnit(kony.flex.DP);
        var fontIconBack = new kony.ui.Label(extendConfig({
            "centerX": "50%",
            "centerY": "50%",
            "height": "14dp",
            "id": "fontIconBack",
            "isVisible": true,
            "left": "0dp",
            "skin": "sknicon15pxBlackBold",
            "text": "",
            "top": "0dp",
            "width": "16dp",
            "zIndex": 1
        }, controller.args[0], "fontIconBack"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "fontIconBack"), extendConfig({}, controller.args[2], "fontIconBack"));
        flxBack.add(fontIconBack);
        var btnBack = new kony.ui.Button(extendConfig({
            "centerY": "50%",
            "id": "btnBack",
            "isVisible": true,
            "left": "5dp",
            "skin": "sknBtnLato117eb013Px",
            "i18n_text": "kony.i18n.getLocalizedString(\"i18n.userwidgetmodel.btnBack\")",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, controller.args[0], "btnBack"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_CENTER,
            "displayText": true,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "btnBack"), extendConfig({}, controller.args[2], "btnBack"));
        backToPageHeader.add(flxBack, btnBack);
        return backToPageHeader;
    }
})