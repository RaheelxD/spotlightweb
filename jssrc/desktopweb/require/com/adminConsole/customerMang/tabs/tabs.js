define(function() {
    return function(controller) {
        var tabs = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "clipBounds": true,
            "isMaster": true,
            "height": "60px",
            "id": "tabs",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "left": "0dp",
            "isModalContainer": false,
            "preShow": function(eventobject) {
                controller.AS_FlexContainer_b1a3158c443a4c42acb8d6a705fbc11f(eventobject);
            },
            "skin": "slFbox0hfd18814fd664dCM",
            "width": "100%"
        }, controller.args[0], "tabs"), extendConfig({}, controller.args[1], "tabs"), extendConfig({}, controller.args[2], "tabs"));
        tabs.setDefaultUnit(kony.flex.DP);
        var flxLeftArrow = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "clipBounds": true,
            "height": "100%",
            "id": "flxLeftArrow",
            "isVisible": false,
            "layoutType": kony.flex.FREE_FORM,
            "left": "0dp",
            "isModalContainer": false,
            "skin": "slFbox",
            "top": "0dp",
            "width": "30dp",
            "zIndex": 1
        }, controller.args[0], "flxLeftArrow"), extendConfig({}, controller.args[1], "flxLeftArrow"), extendConfig({}, controller.args[2], "flxLeftArrow"));
        flxLeftArrow.setDefaultUnit(kony.flex.DP);
        var lblLeftArrow = new kony.ui.Label(extendConfig({
            "centerY": "50%",
            "id": "lblLeftArrow",
            "isVisible": true,
            "left": "0dp",
            "skin": "sknIcon12px003E75",
            "text": "",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, controller.args[0], "lblLeftArrow"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblLeftArrow"), extendConfig({}, controller.args[2], "lblLeftArrow"));
        var flxVerticalSeparator1 = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "centerY": "50%",
            "clipBounds": true,
            "height": "33px",
            "id": "flxVerticalSeparator1",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "isModalContainer": false,
            "right": "0px",
            "skin": "sknflxSepratorE1E5ED",
            "width": "1px",
            "zIndex": 1
        }, controller.args[0], "flxVerticalSeparator1"), extendConfig({}, controller.args[1], "flxVerticalSeparator1"), extendConfig({}, controller.args[2], "flxVerticalSeparator1"));
        flxVerticalSeparator1.setDefaultUnit(kony.flex.DP);
        flxVerticalSeparator1.add();
        flxLeftArrow.add(lblLeftArrow, flxVerticalSeparator1);
        var flxTabs = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "clipBounds": true,
            "height": "100%",
            "id": "flxTabs",
            "isVisible": true,
            "layoutType": kony.flex.FLOW_HORIZONTAL,
            "left": "0dp",
            "isModalContainer": false,
            "skin": "slFbox",
            "top": "0dp",
            "width": kony.flex.USE_PREFFERED_SIZE
        }, controller.args[0], "flxTabs"), extendConfig({}, controller.args[1], "flxTabs"), extendConfig({}, controller.args[2], "flxTabs"));
        flxTabs.setDefaultUnit(kony.flex.DP);
        var btnTabName1 = new kony.ui.Button(extendConfig({
            "centerY": "50%",
            "height": "100%",
            "id": "btnTabName1",
            "isVisible": true,
            "left": "0dp",
            "right": "20px",
            "skin": "sknBtnUtilRest696C73LatoSemibold12Px",
            "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmCustomerManagementController.CONTACT\")",
            "top": "19dp",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, controller.args[0], "btnTabName1"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_CENTER,
            "displayText": true,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "btnTabName1"), extendConfig({
            "hoverSkin": "sknBtnUtilHoverLatoSemibold192B4512Px"
        }, controller.args[2], "btnTabName1"));
        var btnTabName9 = new kony.ui.Button(extendConfig({
            "centerY": "50%",
            "height": "100%",
            "id": "btnTabName9",
            "isVisible": true,
            "left": "0dp",
            "right": "20px",
            "skin": "sknBtnUtilRest696C73LatoSemibold12Px",
            "text": "DUE DILIGENCE",
            "top": "19dp",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, controller.args[0], "btnTabName9"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_CENTER,
            "displayText": true,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "btnTabName9"), extendConfig({
            "hoverSkin": "sknBtnUtilHoverLatoSemibold192B4512Px"
        }, controller.args[2], "btnTabName9"));
        var btnTabName4 = new kony.ui.Button(extendConfig({
            "centerY": "50%",
            "height": "100%",
            "id": "btnTabName4",
            "isVisible": true,
            "left": "0dp",
            "right": "20px",
            "skin": "sknBtnUtilRest696C73LatoSemibold12Px",
            "text": "CONTRACTS",
            "top": "19dp",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, controller.args[0], "btnTabName4"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_CENTER,
            "displayText": true,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "btnTabName4"), extendConfig({
            "hoverSkin": "sknBtnUtilHoverLatoSemibold192B4512Px"
        }, controller.args[2], "btnTabName4"));
        var btnTabName2 = new kony.ui.Button(extendConfig({
            "centerY": "50%",
            "height": "100%",
            "id": "btnTabName2",
            "isVisible": true,
            "left": "0dp",
            "right": "20px",
            "skin": "sknBtnUtilRest696C73LatoSemibold12Px",
            "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmCustomerManagementController.PRODUCTS\")",
            "top": "19dp",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, controller.args[0], "btnTabName2"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_CENTER,
            "displayText": true,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "btnTabName2"), extendConfig({
            "hoverSkin": "sknBtnUtilHoverLatoSemibold192B4512Px"
        }, controller.args[2], "btnTabName2"));
        var btnTabName3 = new kony.ui.Button(extendConfig({
            "centerY": "50%",
            "height": "100%",
            "id": "btnTabName3",
            "isVisible": true,
            "left": "0dp",
            "right": "20px",
            "skin": "sknBtnUtilRest696C73LatoSemibold12Px",
            "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmCustomerManagementController.HELP_CENTER\")",
            "top": "19dp",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, controller.args[0], "btnTabName3"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_CENTER,
            "displayText": true,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "btnTabName3"), extendConfig({
            "hoverSkin": "sknBtnUtilHoverLatoSemibold192B4512Px"
        }, controller.args[2], "btnTabName3"));
        var btnTabName5 = new kony.ui.Button(extendConfig({
            "centerY": "50%",
            "height": "100%",
            "id": "btnTabName5",
            "isVisible": true,
            "left": "0dp",
            "right": "20px",
            "skin": "sknBtnUtilRest696C73LatoSemibold12Px",
            "text": "FEATURES AND ACTIONS",
            "top": "19dp",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, controller.args[0], "btnTabName5"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_CENTER,
            "displayText": true,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "btnTabName5"), extendConfig({
            "hoverSkin": "sknBtnUtilHoverLatoSemibold192B4512Px"
        }, controller.args[2], "btnTabName5"));
        var btnTabName10 = new kony.ui.Button(extendConfig({
            "centerY": "50.00%",
            "height": "100%",
            "id": "btnTabName10",
            "isVisible": true,
            "left": "0dp",
            "right": "20px",
            "skin": "sknBtnUtilRest696C73LatoSemibold12Px",
            "i18n_text": "kony.i18n.getLocalizedString(\"i18n.Group.Limits\")",
            "top": "19dp",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, controller.args[0], "btnTabName10"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_CENTER,
            "displayText": true,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "btnTabName10"), extendConfig({
            "hoverSkin": "sknBtnUtilHoverLatoSemibold192B4512Px"
        }, controller.args[2], "btnTabName10"));
        var btnTabName6 = new kony.ui.Button(extendConfig({
            "centerY": "50%",
            "height": "100%",
            "id": "btnTabName6",
            "isVisible": true,
            "left": "0dp",
            "right": "20px",
            "skin": "sknBtnUtilRest696C73LatoSemibold12Px",
            "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmCustomerManagementController.ACTIVITY_HISTORY\")",
            "top": "19dp",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, controller.args[0], "btnTabName6"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_CENTER,
            "displayText": true,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "btnTabName6"), extendConfig({
            "hoverSkin": "sknBtnUtilHoverLatoSemibold192B4512Px"
        }, controller.args[2], "btnTabName6"));
        var btnTabName7 = new kony.ui.Button(extendConfig({
            "centerY": "50%",
            "height": "100%",
            "id": "btnTabName7",
            "isVisible": true,
            "left": "0dp",
            "right": "20px",
            "skin": "sknBtnUtilRest696C73LatoSemibold12Px",
            "i18n_text": "kony.i18n.getLocalizedString(\"i18n.AlertsManagement.ALERT\")",
            "top": "19dp",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, controller.args[0], "btnTabName7"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_CENTER,
            "displayText": true,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "btnTabName7"), extendConfig({
            "hoverSkin": "sknBtnUtilHoverLatoSemibold192B4512Px"
        }, controller.args[2], "btnTabName7"));
        var btnTabName8 = new kony.ui.Button(extendConfig({
            "centerY": "50%",
            "height": "100%",
            "id": "btnTabName8",
            "isVisible": true,
            "left": "0dp",
            "right": "20px",
            "skin": "sknBtnUtilRest696C73LatoSemibold12Px",
            "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmCustomerManagementController.DEVICE_INFO\")",
            "top": "19dp",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, controller.args[0], "btnTabName8"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_CENTER,
            "displayText": true,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "btnTabName8"), extendConfig({
            "hoverSkin": "sknBtnUtilHoverLatoSemibold192B4512Px"
        }, controller.args[2], "btnTabName8"));
        flxTabs.add(btnTabName1, btnTabName9, btnTabName4, btnTabName2, btnTabName3, btnTabName5, btnTabName10, btnTabName6, btnTabName7, btnTabName8);
        var flxRightArrow = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "clipBounds": true,
            "height": "100%",
            "id": "flxRightArrow",
            "isVisible": false,
            "layoutType": kony.flex.FREE_FORM,
            "isModalContainer": false,
            "right": "0dp",
            "skin": "slFbox",
            "top": "0dp",
            "width": "30dp",
            "zIndex": 1
        }, controller.args[0], "flxRightArrow"), extendConfig({}, controller.args[1], "flxRightArrow"), extendConfig({}, controller.args[2], "flxRightArrow"));
        flxRightArrow.setDefaultUnit(kony.flex.DP);
        var flxVerticalSeparator2 = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "centerY": "50%",
            "clipBounds": true,
            "height": "33px",
            "id": "flxVerticalSeparator2",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "left": "0dp",
            "isModalContainer": false,
            "skin": "sknflxD5D9DD",
            "width": "1px",
            "zIndex": 1
        }, controller.args[0], "flxVerticalSeparator2"), extendConfig({}, controller.args[1], "flxVerticalSeparator2"), extendConfig({}, controller.args[2], "flxVerticalSeparator2"));
        flxVerticalSeparator2.setDefaultUnit(kony.flex.DP);
        flxVerticalSeparator2.add();
        var lblRightArrow = new kony.ui.Label(extendConfig({
            "centerY": "50%",
            "id": "lblRightArrow",
            "isVisible": true,
            "left": "10dp",
            "skin": "sknIcon12px003E75",
            "text": "",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, controller.args[0], "lblRightArrow"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblRightArrow"), extendConfig({}, controller.args[2], "lblRightArrow"));
        flxRightArrow.add(flxVerticalSeparator2, lblRightArrow);
        tabs.add(flxLeftArrow, flxTabs, flxRightArrow);
        return tabs;
    }
})