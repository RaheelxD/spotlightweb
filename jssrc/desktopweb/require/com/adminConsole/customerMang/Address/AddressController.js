define("com/adminConsole/customerMang/Address/userAddressController", function() {
    return {};
});
define("com/adminConsole/customerMang/Address/AddressControllerActions", {
    /*
          This is an auto generated file and any modifications to it may result in corruption of the action sequence.
        */
});
define("com/adminConsole/customerMang/Address/AddressController", ["com/adminConsole/customerMang/Address/userAddressController", "com/adminConsole/customerMang/Address/AddressControllerActions"], function() {
    var controller = require("com/adminConsole/customerMang/Address/userAddressController");
    var actions = require("com/adminConsole/customerMang/Address/AddressControllerActions");
    for (var key in actions) {
        controller[key] = actions[key];
    }
    return controller;
});
