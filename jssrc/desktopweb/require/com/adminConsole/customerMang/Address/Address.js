define(function() {
    return function(controller) {
        var Address = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
            "clipBounds": true,
            "isMaster": true,
            "id": "Address",
            "isVisible": true,
            "layoutType": kony.flex.FLOW_VERTICAL,
            "isModalContainer": false,
            "skin": "slFbox",
            "width": "100%"
        }, controller.args[0], "Address"), extendConfig({}, controller.args[1], "Address"), extendConfig({}, controller.args[2], "Address"));
        Address.setDefaultUnit(kony.flex.DP);
        var flxAdvancedSearchHeader = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "clipBounds": true,
            "height": "22px",
            "id": "flxAdvancedSearchHeader",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "left": "0px",
            "isModalContainer": false,
            "skin": "slFbox",
            "top": "20px",
            "width": "100%",
            "zIndex": 1
        }, controller.args[0], "flxAdvancedSearchHeader"), extendConfig({}, controller.args[1], "flxAdvancedSearchHeader"), extendConfig({}, controller.args[2], "flxAdvancedSearchHeader"));
        flxAdvancedSearchHeader.setDefaultUnit(kony.flex.DP);
        var lblAdvSearchHeader = new kony.ui.Label(extendConfig({
            "bottom": "0px",
            "height": "22px",
            "id": "lblAdvSearchHeader",
            "isVisible": true,
            "left": "20px",
            "skin": "sknLbl192B45LatoRegular14px",
            "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmCustomerManagementController.Address\")",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, controller.args[0], "lblAdvSearchHeader"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblAdvSearchHeader"), extendConfig({}, controller.args[2], "lblAdvSearchHeader"));
        var flxButtons = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "bottom": "0px",
            "clipBounds": true,
            "height": "22px",
            "id": "flxButtons",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "isModalContainer": false,
            "right": "20px",
            "skin": "slFbox",
            "width": "115px",
            "zIndex": 1
        }, controller.args[0], "flxButtons"), extendConfig({}, controller.args[1], "flxButtons"), extendConfig({}, controller.args[2], "flxButtons"));
        flxButtons.setDefaultUnit(kony.flex.DP);
        var btnEdit = new kony.ui.Button(extendConfig({
            "bottom": "0px",
            "focusSkin": "sknbtnf7f7faLatoRegular12Px485c75Border1px485C75Radius20px",
            "height": "22px",
            "id": "btnEdit",
            "isVisible": true,
            "right": "0px",
            "skin": "sknbtnf7f7faLatoRegular12Px485c75Border1px485C75Radius20px",
            "i18n_text": "kony.i18n.getLocalizedString(\"i18n.roles.Edit\")",
            "width": "50px",
            "zIndex": 1
        }, controller.args[0], "btnEdit"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_CENTER,
            "displayText": true,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "btnEdit"), extendConfig({
            "hoverSkin": "sknbtnffffffLatoRegular12Px485c75Border1px485C75Radius20px"
        }, controller.args[2], "btnEdit"));
        var btnAdd = new kony.ui.Button(extendConfig({
            "bottom": "0px",
            "focusSkin": "sknbtnf7f7faLatoRegular12Px485c75Border1px485C75Radius20px",
            "height": "22px",
            "id": "btnAdd",
            "isVisible": true,
            "left": 0,
            "skin": "sknbtnf7f7faLatoRegular12Px485c75Border1px485C75Radius20px",
            "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmPermissionsController.Add\")",
            "width": "50px",
            "zIndex": 1
        }, controller.args[0], "btnAdd"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_CENTER,
            "displayText": true,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "btnAdd"), extendConfig({
            "hoverSkin": "sknbtnffffffLatoRegular12Px485c75Border1px485C75Radius20px"
        }, controller.args[2], "btnAdd"));
        flxButtons.add(btnEdit, btnAdd);
        flxAdvancedSearchHeader.add(lblAdvSearchHeader, flxButtons);
        var flxAddressRow1 = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
            "clipBounds": true,
            "id": "flxAddressRow1",
            "isVisible": true,
            "layoutType": kony.flex.FLOW_HORIZONTAL,
            "left": "0px",
            "isModalContainer": false,
            "skin": "sknNormalDefault",
            "top": "0dp",
            "width": "100%",
            "zIndex": 1
        }, controller.args[0], "flxAddressRow1"), extendConfig({}, controller.args[1], "flxAddressRow1"), extendConfig({}, controller.args[2], "flxAddressRow1"));
        flxAddressRow1.setDefaultUnit(kony.flex.DP);
        var flxAddressPrimary = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
            "clipBounds": true,
            "id": "flxAddressPrimary",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "left": "20px",
            "isModalContainer": false,
            "skin": "slFbox",
            "top": "10px",
            "width": "35%",
            "zIndex": 2
        }, controller.args[0], "flxAddressPrimary"), extendConfig({}, controller.args[1], "flxAddressPrimary"), extendConfig({}, controller.args[2], "flxAddressPrimary"));
        flxAddressPrimary.setDefaultUnit(kony.flex.DP);
        var rtxAddressPrimary = new kony.ui.RichText(extendConfig({
            "id": "rtxAddressPrimary",
            "isVisible": true,
            "left": "0px",
            "skin": "sknRtx485C75LatoSemiBold13px",
            "i18n_text": "kony.i18n.getLocalizedString(\"i18n.userwidgetmodel.rtxAddressAdditional1\")",
            "top": "30dp",
            "width": "50%",
            "zIndex": 3
        }, controller.args[0], "rtxAddressPrimary"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "rtxAddressPrimary"), extendConfig({}, controller.args[2], "rtxAddressPrimary"));
        var flxPrimaryLabelAndTag = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "clipBounds": true,
            "height": "18px",
            "id": "flxPrimaryLabelAndTag",
            "isVisible": true,
            "layoutType": kony.flex.FLOW_HORIZONTAL,
            "left": "0dp",
            "isModalContainer": false,
            "skin": "slFbox",
            "top": "0dp",
            "width": "100%",
            "zIndex": 2
        }, controller.args[0], "flxPrimaryLabelAndTag"), extendConfig({}, controller.args[1], "flxPrimaryLabelAndTag"), extendConfig({}, controller.args[2], "flxPrimaryLabelAndTag"));
        flxPrimaryLabelAndTag.setDefaultUnit(kony.flex.DP);
        var lblAddressPrimary = new kony.ui.Label(extendConfig({
            "id": "lblAddressPrimary",
            "isVisible": true,
            "left": "0px",
            "skin": "sknlblLato696c7311px",
            "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmCustomerManagementController.Work\")",
            "top": "2px",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 3
        }, controller.args[0], "lblAddressPrimary"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblAddressPrimary"), extendConfig({}, controller.args[2], "lblAddressPrimary"));
        var flxPrimary = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "clipBounds": true,
            "height": "18dp",
            "id": "flxPrimary",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "left": "40px",
            "isModalContainer": false,
            "skin": "sknflx1F844DRadius20px",
            "top": "0dp",
            "width": "200px",
            "zIndex": 3
        }, controller.args[0], "flxPrimary"), extendConfig({}, controller.args[1], "flxPrimary"), extendConfig({}, controller.args[2], "flxPrimary"));
        flxPrimary.setDefaultUnit(kony.flex.DP);
        var lblPrimaryCommunicationAddress = new kony.ui.Label(extendConfig({
            "centerX": "50%",
            "centerY": "50%",
            "id": "lblPrimaryCommunicationAddress",
            "isVisible": true,
            "skin": "sknLblFFFFFLatoRegular12Px",
            "i18n_text": "kony.i18n.getLocalizedString(\"i18n.userwidgetmodel.lblPrimaryCommunicationAddress\")",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 2
        }, controller.args[0], "lblPrimaryCommunicationAddress"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblPrimaryCommunicationAddress"), extendConfig({}, controller.args[2], "lblPrimaryCommunicationAddress"));
        flxPrimary.add(lblPrimaryCommunicationAddress);
        flxPrimaryLabelAndTag.add(lblAddressPrimary, flxPrimary);
        flxAddressPrimary.add(rtxAddressPrimary, flxPrimaryLabelAndTag);
        var flxAddressAdditional1 = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
            "clipBounds": true,
            "id": "flxAddressAdditional1",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "left": "35px",
            "isModalContainer": false,
            "skin": "slFbox",
            "top": "10px",
            "width": "35%",
            "zIndex": 1
        }, controller.args[0], "flxAddressAdditional1"), extendConfig({}, controller.args[1], "flxAddressAdditional1"), extendConfig({}, controller.args[2], "flxAddressAdditional1"));
        flxAddressAdditional1.setDefaultUnit(kony.flex.DP);
        var lblAddressAdditional1 = new kony.ui.Label(extendConfig({
            "id": "lblAddressAdditional1",
            "isVisible": true,
            "left": "0px",
            "skin": "sknlblLato696c7311px",
            "i18n_text": "kony.i18n.getLocalizedString(\"i18n.userwidgetmodel.lblAddressAdditional1\")",
            "top": "2px",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, controller.args[0], "lblAddressAdditional1"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblAddressAdditional1"), extendConfig({}, controller.args[2], "lblAddressAdditional1"));
        var rtxAddressAdditional1 = new kony.ui.RichText(extendConfig({
            "id": "rtxAddressAdditional1",
            "isVisible": true,
            "left": "0px",
            "skin": "sknRtx485C75LatoSemiBold13px",
            "i18n_text": "kony.i18n.getLocalizedString(\"i18n.userwidgetmodel.rtxAddressAdditional1\")",
            "top": "30dp",
            "width": "60%",
            "zIndex": 3
        }, controller.args[0], "rtxAddressAdditional1"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "rtxAddressAdditional1"), extendConfig({}, controller.args[2], "rtxAddressAdditional1"));
        var btnDeleteAddress1 = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "clipBounds": true,
            "height": 35,
            "id": "btnDeleteAddress1",
            "isVisible": true,
            "layoutType": kony.flex.FLOW_HORIZONTAL,
            "left": "63%",
            "isModalContainer": false,
            "skin": "sknCursor",
            "top": "30dp",
            "width": "35px",
            "zIndex": 1
        }, controller.args[0], "btnDeleteAddress1"), extendConfig({}, controller.args[1], "btnDeleteAddress1"), extendConfig({}, controller.args[2], "btnDeleteAddress1"));
        btnDeleteAddress1.setDefaultUnit(kony.flex.DP);
        var imgDelete = new kony.ui.Image2(extendConfig({
            "centerX": "50%",
            "centerY": "50%",
            "height": "15dp",
            "id": "imgDelete",
            "isVisible": false,
            "left": "15dp",
            "skin": "slImage",
            "src": "delete_2x.png",
            "width": "15dp",
            "zIndex": 1
        }, controller.args[0], "imgDelete"), extendConfig({
            "imageScaleMode": constants.IMAGE_SCALE_MODE_FIT_TO_DIMENSIONS,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "imgDelete"), extendConfig({
            "toolTip": "Delete"
        }, controller.args[2], "imgDelete"));
        var fonticonDeleteSearch = new kony.ui.Label(extendConfig({
            "centerX": "50.00%",
            "centerY": "50%",
            "id": "fonticonDeleteSearch",
            "isVisible": true,
            "skin": "sknFontIconOptionMenuRow",
            "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmFAQ.fonticonDelete\")",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, controller.args[0], "fonticonDeleteSearch"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "fonticonDeleteSearch"), extendConfig({
            "toolTip": "Delete"
        }, controller.args[2], "fonticonDeleteSearch"));
        btnDeleteAddress1.add(imgDelete, fonticonDeleteSearch);
        var flxSeprator1 = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "bottom": 10,
            "clipBounds": true,
            "id": "flxSeprator1",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "left": "62%",
            "isModalContainer": false,
            "skin": "sknflxd6dbe7",
            "top": "30dp",
            "width": "1dp",
            "zIndex": 1
        }, controller.args[0], "flxSeprator1"), extendConfig({}, controller.args[1], "flxSeprator1"), extendConfig({}, controller.args[2], "flxSeprator1"));
        flxSeprator1.setDefaultUnit(kony.flex.DP);
        flxSeprator1.add();
        flxAddressAdditional1.add(lblAddressAdditional1, rtxAddressAdditional1, btnDeleteAddress1, flxSeprator1);
        flxAddressRow1.add(flxAddressPrimary, flxAddressAdditional1);
        var flxAddressRow2 = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
            "clipBounds": true,
            "id": "flxAddressRow2",
            "isVisible": true,
            "layoutType": kony.flex.FLOW_HORIZONTAL,
            "left": "0px",
            "isModalContainer": false,
            "skin": "sknNormalDefault",
            "top": "0dp",
            "width": "100%",
            "zIndex": 1
        }, controller.args[0], "flxAddressRow2"), extendConfig({}, controller.args[1], "flxAddressRow2"), extendConfig({}, controller.args[2], "flxAddressRow2"));
        flxAddressRow2.setDefaultUnit(kony.flex.DP);
        var flxAddressAdditional2 = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
            "clipBounds": true,
            "id": "flxAddressAdditional2",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "left": "20px",
            "isModalContainer": false,
            "skin": "slFbox",
            "top": "10px",
            "width": "35%",
            "zIndex": 1
        }, controller.args[0], "flxAddressAdditional2"), extendConfig({}, controller.args[1], "flxAddressAdditional2"), extendConfig({}, controller.args[2], "flxAddressAdditional2"));
        flxAddressAdditional2.setDefaultUnit(kony.flex.DP);
        var lblAddressAdditional2 = new kony.ui.Label(extendConfig({
            "id": "lblAddressAdditional2",
            "isVisible": true,
            "left": "0px",
            "skin": "sknlblLato696c7311px",
            "i18n_text": "kony.i18n.getLocalizedString(\"i18n.userwidgetmodel.lblAddressAdditional2\")",
            "top": "2px",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, controller.args[0], "lblAddressAdditional2"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblAddressAdditional2"), extendConfig({}, controller.args[2], "lblAddressAdditional2"));
        var rtxAddressAdditional2 = new kony.ui.RichText(extendConfig({
            "id": "rtxAddressAdditional2",
            "isVisible": true,
            "left": "0px",
            "skin": "sknRtx485C75LatoSemiBold13px",
            "i18n_text": "kony.i18n.getLocalizedString(\"i18n.userwidgetmodel.rtxAddressAdditional1\")",
            "top": "30dp",
            "width": "60%",
            "zIndex": 3
        }, controller.args[0], "rtxAddressAdditional2"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "rtxAddressAdditional2"), extendConfig({}, controller.args[2], "rtxAddressAdditional2"));
        var btnDeleteAddress2 = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "clipBounds": true,
            "height": 35,
            "id": "btnDeleteAddress2",
            "isVisible": true,
            "layoutType": kony.flex.FLOW_HORIZONTAL,
            "left": "63%",
            "isModalContainer": false,
            "skin": "sknCursor",
            "top": "30dp",
            "width": "35px",
            "zIndex": 1
        }, controller.args[0], "btnDeleteAddress2"), extendConfig({}, controller.args[1], "btnDeleteAddress2"), extendConfig({}, controller.args[2], "btnDeleteAddress2"));
        btnDeleteAddress2.setDefaultUnit(kony.flex.DP);
        var imgDelete2 = new kony.ui.Image2(extendConfig({
            "centerX": "50%",
            "centerY": "50%",
            "height": "15dp",
            "id": "imgDelete2",
            "isVisible": false,
            "left": "15dp",
            "skin": "slImage",
            "src": "delete_2x.png",
            "width": "15dp",
            "zIndex": 1
        }, controller.args[0], "imgDelete2"), extendConfig({
            "imageScaleMode": constants.IMAGE_SCALE_MODE_FIT_TO_DIMENSIONS,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "imgDelete2"), extendConfig({
            "toolTip": "Delete"
        }, controller.args[2], "imgDelete2"));
        var fonticonDelete2 = new kony.ui.Label(extendConfig({
            "centerX": "50.00%",
            "centerY": "50%",
            "id": "fonticonDelete2",
            "isVisible": true,
            "skin": "sknFontIconOptionMenuRow",
            "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmFAQ.fonticonDelete\")",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, controller.args[0], "fonticonDelete2"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "fonticonDelete2"), extendConfig({
            "toolTip": "Delete"
        }, controller.args[2], "fonticonDelete2"));
        btnDeleteAddress2.add(imgDelete2, fonticonDelete2);
        var flxSeprator2 = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "bottom": 10,
            "clipBounds": true,
            "id": "flxSeprator2",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "left": "62%",
            "isModalContainer": false,
            "skin": "sknflxd6dbe7",
            "top": "30dp",
            "width": "1dp",
            "zIndex": 1
        }, controller.args[0], "flxSeprator2"), extendConfig({}, controller.args[1], "flxSeprator2"), extendConfig({}, controller.args[2], "flxSeprator2"));
        flxSeprator2.setDefaultUnit(kony.flex.DP);
        flxSeprator2.add();
        flxAddressAdditional2.add(lblAddressAdditional2, rtxAddressAdditional2, btnDeleteAddress2, flxSeprator2);
        flxAddressRow2.add(flxAddressAdditional2);
        Address.add(flxAdvancedSearchHeader, flxAddressRow1, flxAddressRow2);
        return Address;
    }
})