define(function() {
    return function(controller) {
        var contractsList = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
            "clipBounds": true,
            "isMaster": true,
            "id": "contractsList",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "left": "0dp",
            "isModalContainer": false,
            "preShow": function(eventobject) {
                controller.AS_FlexContainer_a44d23a376ef4b74afaad8170bc45392(eventobject);
            },
            "skin": "slFbox",
            "top": "0dp",
            "width": "100%"
        }, controller.args[0], "contractsList"), extendConfig({}, controller.args[1], "contractsList"), extendConfig({}, controller.args[2], "contractsList"));
        contractsList.setDefaultUnit(kony.flex.DP);
        var flxContractRowContainer = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
            "clipBounds": true,
            "id": "flxContractRowContainer",
            "isVisible": true,
            "layoutType": kony.flex.FLOW_VERTICAL,
            "left": "0",
            "isModalContainer": false,
            "skin": "sknFlxbgFFFFFFbdrD7D9E0rd4px",
            "top": "0",
            "width": "100%"
        }, controller.args[0], "flxContractRowContainer"), extendConfig({}, controller.args[1], "flxContractRowContainer"), extendConfig({}, controller.args[2], "flxContractRowContainer"));
        flxContractRowContainer.setDefaultUnit(kony.flex.DP);
        var flxTopContainer = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "clipBounds": true,
            "height": "50dp",
            "id": "flxTopContainer",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "left": "0",
            "isModalContainer": false,
            "skin": "slFbox",
            "top": "0",
            "width": "100%"
        }, controller.args[0], "flxTopContainer"), extendConfig({}, controller.args[1], "flxTopContainer"), extendConfig({}, controller.args[2], "flxTopContainer"));
        flxTopContainer.setDefaultUnit(kony.flex.DP);
        var flxContractLeft = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "clipBounds": true,
            "height": "100%",
            "id": "flxContractLeft",
            "isVisible": true,
            "layoutType": kony.flex.FLOW_HORIZONTAL,
            "left": "5dp",
            "isModalContainer": false,
            "skin": "slFbox",
            "top": "0dp",
            "width": "70%"
        }, controller.args[0], "flxContractLeft"), extendConfig({}, controller.args[1], "flxContractLeft"), extendConfig({}, controller.args[2], "flxContractLeft"));
        flxContractLeft.setDefaultUnit(kony.flex.DP);
        var flxCheckbox = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "centerY": "50%",
            "clipBounds": true,
            "height": "15dp",
            "id": "flxCheckbox",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "left": "10dp",
            "isModalContainer": false,
            "skin": "slFbox",
            "width": "15dp"
        }, controller.args[0], "flxCheckbox"), extendConfig({}, controller.args[1], "flxCheckbox"), extendConfig({}, controller.args[2], "flxCheckbox"));
        flxCheckbox.setDefaultUnit(kony.flex.DP);
        var imgCheckbox = new kony.ui.Image2(extendConfig({
            "height": "100%",
            "id": "imgCheckbox",
            "isVisible": true,
            "left": "0",
            "skin": "slImage",
            "src": "radio_notselected.png",
            "top": "0",
            "width": "100%"
        }, controller.args[0], "imgCheckbox"), extendConfig({
            "imageScaleMode": constants.IMAGE_SCALE_MODE_MAINTAIN_ASPECT_RATIO,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "imgCheckbox"), extendConfig({}, controller.args[2], "imgCheckbox"));
        flxCheckbox.add(imgCheckbox);
        var lblContractName = new kony.ui.Label(extendConfig({
            "centerY": "50%",
            "id": "lblContractName",
            "isVisible": true,
            "left": "10dp",
            "skin": "sknLbl192b45LatoReg16px",
            "text": "Label",
            "width": kony.flex.USE_PREFFERED_SIZE
        }, controller.args[0], "lblContractName"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_TOP_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblContractName"), extendConfig({}, controller.args[2], "lblContractName"));
        var flxContractTag = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "centerY": "50%",
            "clipBounds": true,
            "height": "20px",
            "id": "flxContractTag",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "left": "10dp",
            "isModalContainer": false,
            "skin": "sknFlxBg3146A4Rd3px",
            "width": "58px"
        }, controller.args[0], "flxContractTag"), extendConfig({}, controller.args[1], "flxContractTag"), extendConfig({}, controller.args[2], "flxContractTag"));
        flxContractTag.setDefaultUnit(kony.flex.DP);
        var lblContractTag = new kony.ui.Label(extendConfig({
            "centerX": "50%",
            "centerY": "50%",
            "id": "lblContractTag",
            "isVisible": true,
            "skin": "sknCustomerTypeTag",
            "text": "Contract",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, controller.args[0], "lblContractTag"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblContractTag"), extendConfig({}, controller.args[2], "lblContractTag"));
        flxContractTag.add(lblContractTag);
        flxContractLeft.add(flxCheckbox, lblContractName, flxContractTag);
        var flxTopSeperator = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "bottom": "0dp",
            "clipBounds": true,
            "height": "1dp",
            "id": "flxTopSeperator",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "left": "0",
            "isModalContainer": false,
            "skin": "sknFlxD7D9E0Separator",
            "width": "100%"
        }, controller.args[0], "flxTopSeperator"), extendConfig({}, controller.args[1], "flxTopSeperator"), extendConfig({}, controller.args[2], "flxTopSeperator"));
        flxTopSeperator.setDefaultUnit(kony.flex.DP);
        flxTopSeperator.add();
        flxTopContainer.add(flxContractLeft, flxTopSeperator);
        var flxHeadingContainer = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "clipBounds": true,
            "height": "45dp",
            "id": "flxHeadingContainer",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "left": "0",
            "isModalContainer": false,
            "skin": "sknflxf5f6f8Op100",
            "top": "0dp",
            "width": "100%"
        }, controller.args[0], "flxHeadingContainer"), extendConfig({}, controller.args[1], "flxHeadingContainer"), extendConfig({}, controller.args[2], "flxHeadingContainer"));
        flxHeadingContainer.setDefaultUnit(kony.flex.DP);
        var flxHeading = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "clipBounds": true,
            "height": "100%",
            "id": "flxHeading",
            "isVisible": true,
            "layoutType": kony.flex.FLOW_HORIZONTAL,
            "left": "0dp",
            "isModalContainer": false,
            "skin": "sknflxf5f6f8Op100",
            "top": "0dp",
            "width": "80%"
        }, controller.args[0], "flxHeading"), extendConfig({}, controller.args[1], "flxHeading"), extendConfig({}, controller.args[2], "flxHeading"));
        flxHeading.setDefaultUnit(kony.flex.DP);
        var flxArrow = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "centerY": "50%",
            "clipBounds": true,
            "height": "20dp",
            "id": "flxArrow",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "left": "15dp",
            "isModalContainer": false,
            "skin": "sknCursor",
            "width": "20dp"
        }, controller.args[0], "flxArrow"), extendConfig({}, controller.args[1], "flxArrow"), extendConfig({}, controller.args[2], "flxArrow"));
        flxArrow.setDefaultUnit(kony.flex.DP);
        var lblArrow = new kony.ui.Label(extendConfig({
            "centerY": "50%",
            "id": "lblArrow",
            "isVisible": true,
            "left": "0",
            "skin": "sknIcon485C7513px",
            "text": "",
            "width": kony.flex.USE_PREFFERED_SIZE
        }, controller.args[0], "lblArrow"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblArrow"), extendConfig({}, controller.args[2], "lblArrow"));
        flxArrow.add(lblArrow);
        var lblHeading = new kony.ui.Label(extendConfig({
            "centerY": "50%",
            "id": "lblHeading",
            "isVisible": true,
            "left": "5dp",
            "skin": "sknLbl192b45LatoReg16px",
            "text": "Customers",
            "width": kony.flex.USE_PREFFERED_SIZE
        }, controller.args[0], "lblHeading"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblHeading"), extendConfig({}, controller.args[2], "lblHeading"));
        flxHeading.add(flxArrow, lblHeading);
        var flxBottomSeperator = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "bottom": "0dp",
            "clipBounds": true,
            "height": "1dp",
            "id": "flxBottomSeperator",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "left": "0dp",
            "isModalContainer": false,
            "skin": "sknFlxD7D9E0Separator",
            "width": "100%"
        }, controller.args[0], "flxBottomSeperator"), extendConfig({}, controller.args[1], "flxBottomSeperator"), extendConfig({}, controller.args[2], "flxBottomSeperator"));
        flxBottomSeperator.setDefaultUnit(kony.flex.DP);
        flxBottomSeperator.add();
        flxHeadingContainer.add(flxHeading, flxBottomSeperator);
        var flxBottomContainer = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
            "clipBounds": true,
            "id": "flxBottomContainer",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "left": "0",
            "isModalContainer": false,
            "skin": "slFbox",
            "top": "0dp",
            "width": "100%"
        }, controller.args[0], "flxBottomContainer"), extendConfig({}, controller.args[1], "flxBottomContainer"), extendConfig({}, controller.args[2], "flxBottomContainer"));
        flxBottomContainer.setDefaultUnit(kony.flex.DP);
        var flxCustomersListHeader = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "clipBounds": true,
            "height": "55dp",
            "id": "flxCustomersListHeader",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "left": "0dp",
            "isModalContainer": false,
            "skin": "sknFlxFFFFFF100O",
            "top": "0dp",
            "width": "100%"
        }, controller.args[0], "flxCustomersListHeader"), extendConfig({}, controller.args[1], "flxCustomersListHeader"), extendConfig({}, controller.args[2], "flxCustomersListHeader"));
        flxCustomersListHeader.setDefaultUnit(kony.flex.DP);
        var flxCheckBoxHeader = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "centerY": "50%",
            "clipBounds": true,
            "height": "15dp",
            "id": "flxCheckBoxHeader",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "left": "15dp",
            "isModalContainer": false,
            "skin": "slFbox",
            "width": "15dp"
        }, controller.args[0], "flxCheckBoxHeader"), extendConfig({}, controller.args[1], "flxCheckBoxHeader"), extendConfig({}, controller.args[2], "flxCheckBoxHeader"));
        flxCheckBoxHeader.setDefaultUnit(kony.flex.DP);
        var imgCheckBoxHeader = new kony.ui.Image2(extendConfig({
            "height": "100%",
            "id": "imgCheckBoxHeader",
            "isVisible": true,
            "left": "0",
            "skin": "slImage",
            "src": "checkboxnormal.png",
            "top": "0",
            "width": "100%"
        }, controller.args[0], "imgCheckBoxHeader"), extendConfig({
            "imageScaleMode": constants.IMAGE_SCALE_MODE_MAINTAIN_ASPECT_RATIO,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "imgCheckBoxHeader"), extendConfig({}, controller.args[2], "imgCheckBoxHeader"));
        flxCheckBoxHeader.add(imgCheckBoxHeader);
        var lblContractNameHeader = new kony.ui.Label(extendConfig({
            "centerY": "50%",
            "id": "lblContractNameHeader",
            "isVisible": true,
            "left": "40dp",
            "skin": "sknlblLato696c7311px",
            "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmEnrollCustomer.CustomerName_UC\")",
            "width": "30%"
        }, controller.args[0], "lblContractNameHeader"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_TOP_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblContractNameHeader"), extendConfig({}, controller.args[2], "lblContractNameHeader"));
        var lblContractIdHeader = new kony.ui.Label(extendConfig({
            "centerY": "50%",
            "id": "lblContractIdHeader",
            "isVisible": true,
            "left": "35%",
            "skin": "sknlblLato696c7311px",
            "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmCustomerManagementController.CUSTOMER_ID\")",
            "width": "28%"
        }, controller.args[0], "lblContractIdHeader"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_TOP_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblContractIdHeader"), extendConfig({}, controller.args[2], "lblContractIdHeader"));
        var lblContractAddrHeader = new kony.ui.Label(extendConfig({
            "centerY": "50%",
            "id": "lblContractAddrHeader",
            "isVisible": true,
            "left": "63%",
            "right": "20dp",
            "skin": "sknlblLato696c7311px",
            "i18n_text": "kony.i18n.getLocalizedString(\"i18n.View.ADDRESS\")",
            "width": kony.flex.USE_PREFFERED_SIZE
        }, controller.args[0], "lblContractAddrHeader"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_TOP_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblContractAddrHeader"), extendConfig({}, controller.args[2], "lblContractAddrHeader"));
        var lblSeperator = new kony.ui.Label(extendConfig({
            "bottom": "0dp",
            "height": "1dp",
            "id": "lblSeperator",
            "isVisible": true,
            "left": "0dp",
            "skin": "sknLblSeparator696C73",
            "text": "Label",
            "width": "100%"
        }, controller.args[0], "lblSeperator"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblSeperator"), extendConfig({}, controller.args[2], "lblSeperator"));
        flxCustomersListHeader.add(flxCheckBoxHeader, lblContractNameHeader, lblContractIdHeader, lblContractAddrHeader, lblSeperator);
        var segRelatedContractsList = new kony.ui.SegmentedUI2(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
            "data": [{
                "imgCheckbox": "checkboxnormal.png",
                "imgSectionCheckbox": "checkboxnormal.png",
                "lblContractAddress": "Label",
                "lblContractId": "Label",
                "lblContractName": "Label",
                "lblSeperator": "Label"
            }],
            "groupCells": false,
            "id": "segRelatedContractsList",
            "isVisible": true,
            "left": "0",
            "needPageIndicator": true,
            "pageOffDotImage": "pageoffdot.png",
            "pageOnDotImage": "pageondot.png",
            "retainSelection": false,
            "rowFocusSkin": "seg2Focus",
            "rowSkin": "seg2Normal",
            "rowTemplate": "flxEnrollCustomerContractList",
            "sectionHeaderSkin": "sliPhoneSegmentHeader",
            "selectionBehavior": constants.SEGUI_DEFAULT_BEHAVIOR,
            "separatorColor": "aaaaaa00",
            "separatorRequired": false,
            "separatorThickness": 0,
            "showScrollbars": false,
            "top": "55dp",
            "viewType": constants.SEGUI_VIEW_TYPE_TABLEVIEW,
            "widgetDataMap": {
                "flxCheckbox": "flxCheckbox",
                "flxEnrollCustomerContractList": "flxEnrollCustomerContractList",
                "flxSectionCheckbox": "flxSectionCheckbox",
                "imgCheckbox": "imgCheckbox",
                "imgSectionCheckbox": "imgSectionCheckbox",
                "lblContractAddress": "lblContractAddress",
                "lblContractId": "lblContractId",
                "lblContractName": "lblContractName",
                "lblSeperator": "lblSeperator"
            },
            "width": "100%"
        }, controller.args[0], "segRelatedContractsList"), extendConfig({
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "segRelatedContractsList"), extendConfig({}, controller.args[2], "segRelatedContractsList"));
        flxBottomContainer.add(flxCustomersListHeader, segRelatedContractsList);
        flxContractRowContainer.add(flxTopContainer, flxHeadingContainer, flxBottomContainer);
        contractsList.add(flxContractRowContainer);
        return contractsList;
    }
})