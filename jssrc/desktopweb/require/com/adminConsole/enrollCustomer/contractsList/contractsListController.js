define("com/adminConsole/enrollCustomer/contractsList/usercontractsListController", function() {
    return {
        callPreshow: function() {
            this.initializeActions();
        },
        initializeActions: function() {},
        /*
         * set the collapse arrow images based on visibility
         */
        toggleCollapseArrow: function(opt) {
            if (opt === true) {
                this.view.lblArrow.text = "\ue915"; //down-arrow
                this.view.lblArrow.skin = "sknIcon00000014px";
                this.view.flxBottomContainer.setVisibility(true);
                this.view.flxBottomSeperator.setVisibility(true);
            } else {
                this.view.lblArrow.text = "\ue922"; //right-arrow
                this.view.lblArrow.skin = "sknIcon00000015px";
                this.view.flxBottomContainer.setVisibility(false);
                this.view.flxBottomSeperator.setVisibility(false);
            }
        },
    };
});
define("com/adminConsole/enrollCustomer/contractsList/contractsListControllerActions", {
    /*
        This is an auto generated file and any modifications to it may result in corruption of the action sequence.
    */
    AS_FlexContainer_a44d23a376ef4b74afaad8170bc45392: function AS_FlexContainer_a44d23a376ef4b74afaad8170bc45392(eventobject) {
        var self = this;
        this.callPreshow();
    }
});
define("com/adminConsole/enrollCustomer/contractsList/contractsListController", ["com/adminConsole/enrollCustomer/contractsList/usercontractsListController", "com/adminConsole/enrollCustomer/contractsList/contractsListControllerActions"], function() {
    var controller = require("com/adminConsole/enrollCustomer/contractsList/usercontractsListController");
    var actions = require("com/adminConsole/enrollCustomer/contractsList/contractsListControllerActions");
    for (var key in actions) {
        controller[key] = actions[key];
    }
    return controller;
});
