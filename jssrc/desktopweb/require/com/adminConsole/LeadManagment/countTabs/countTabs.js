define(function() {
    return function(controller) {
        var countTabs = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "clipBounds": true,
            "isMaster": true,
            "height": "60px",
            "id": "countTabs",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "left": "0px",
            "isModalContainer": false,
            "skin": "slFbox",
            "top": "0px",
            "width": "100%"
        }, controller.args[0], "countTabs"), extendConfig({}, controller.args[1], "countTabs"), extendConfig({}, controller.args[2], "countTabs"));
        countTabs.setDefaultUnit(kony.flex.DP);
        var flxMainContainer = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "clipBounds": true,
            "height": "60px",
            "id": "flxMainContainer",
            "isVisible": true,
            "layoutType": kony.flex.FLOW_HORIZONTAL,
            "left": "0px",
            "isModalContainer": false,
            "skin": "sknBackgroundFFFFFF",
            "top": "0px",
            "width": "100%",
            "zIndex": 1
        }, controller.args[0], "flxMainContainer"), extendConfig({}, controller.args[1], "flxMainContainer"), extendConfig({}, controller.args[2], "flxMainContainer"));
        flxMainContainer.setDefaultUnit(kony.flex.DP);
        var flxCounter1 = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "clipBounds": true,
            "height": "100%",
            "id": "flxCounter1",
            "isVisible": true,
            "layoutType": kony.flex.FLOW_VERTICAL,
            "left": "0px",
            "isModalContainer": false,
            "skin": "sknFlxbgFFFFFFBorder006cca",
            "top": "0px",
            "width": "100px",
            "zIndex": 1
        }, controller.args[0], "flxCounter1"), extendConfig({}, controller.args[1], "flxCounter1"), extendConfig({}, controller.args[2], "flxCounter1"));
        flxCounter1.setDefaultUnit(kony.flex.DP);
        var flxGroup1Cursor = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "clipBounds": true,
            "height": "100%",
            "id": "flxGroup1Cursor",
            "isVisible": true,
            "layoutType": kony.flex.FLOW_VERTICAL,
            "left": "0dp",
            "isModalContainer": false,
            "skin": "sknFlxPointer",
            "top": "0dp",
            "width": "100%"
        }, controller.args[0], "flxGroup1Cursor"), extendConfig({}, controller.args[1], "flxGroup1Cursor"), extendConfig({}, controller.args[2], "flxGroup1Cursor"));
        flxGroup1Cursor.setDefaultUnit(kony.flex.DP);
        var lblCounter1 = new kony.ui.Label(extendConfig({
            "centerX": "50%",
            "id": "lblCounter1",
            "isVisible": true,
            "skin": "sknlblLatoBold13px000000",
            "text": "23",
            "top": "10px",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, controller.args[0], "lblCounter1"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_CENTER,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblCounter1"), extendConfig({}, controller.args[2], "lblCounter1"));
        var lblType1 = new kony.ui.Label(extendConfig({
            "centerX": "50%",
            "id": "lblType1",
            "isVisible": true,
            "skin": "sknlbl485c7511px",
            "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmCSRController.NEW\")",
            "top": "2px",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, controller.args[0], "lblType1"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_CENTER,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblType1"), extendConfig({}, controller.args[2], "lblType1"));
        flxGroup1Cursor.add(lblCounter1, lblType1);
        flxCounter1.add(flxGroup1Cursor);
        var flxTabSeparator1 = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "clipBounds": true,
            "height": "60px",
            "id": "flxTabSeparator1",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "left": "0px",
            "isModalContainer": false,
            "skin": "sknflxConfigurationBundlesFillLightGrey",
            "top": "0px",
            "width": "1px",
            "zIndex": 1
        }, controller.args[0], "flxTabSeparator1"), extendConfig({}, controller.args[1], "flxTabSeparator1"), extendConfig({}, controller.args[2], "flxTabSeparator1"));
        flxTabSeparator1.setDefaultUnit(kony.flex.DP);
        flxTabSeparator1.add();
        var flxCounter2 = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "clipBounds": true,
            "height": "100%",
            "id": "flxCounter2",
            "isVisible": true,
            "layoutType": kony.flex.FLOW_VERTICAL,
            "left": "0dp",
            "isModalContainer": false,
            "skin": "sknflxLeadTabs",
            "top": "0dp",
            "width": "100dp",
            "zIndex": 1
        }, controller.args[0], "flxCounter2"), extendConfig({}, controller.args[1], "flxCounter2"), extendConfig({}, controller.args[2], "flxCounter2"));
        flxCounter2.setDefaultUnit(kony.flex.DP);
        var flxGroup2Cursor = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "clipBounds": true,
            "height": "100%",
            "id": "flxGroup2Cursor",
            "isVisible": true,
            "layoutType": kony.flex.FLOW_VERTICAL,
            "left": "0dp",
            "isModalContainer": false,
            "skin": "sknFlxPointer",
            "top": "0dp",
            "width": "100%"
        }, controller.args[0], "flxGroup2Cursor"), extendConfig({}, controller.args[1], "flxGroup2Cursor"), extendConfig({}, controller.args[2], "flxGroup2Cursor"));
        flxGroup2Cursor.setDefaultUnit(kony.flex.DP);
        var lblCounter2 = new kony.ui.Label(extendConfig({
            "centerX": "50%",
            "id": "lblCounter2",
            "isVisible": true,
            "skin": "sknlblLatoBold13px000000",
            "text": "208",
            "top": "10dp",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, controller.args[0], "lblCounter2"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_CENTER,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblCounter2"), extendConfig({}, controller.args[2], "lblCounter2"));
        var lblType2 = new kony.ui.Label(extendConfig({
            "centerX": "50%",
            "id": "lblType2",
            "isVisible": true,
            "skin": "sknlbl485c7511px",
            "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmCSRController.IN_PROGRESS\")",
            "top": "2dp",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, controller.args[0], "lblType2"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_CENTER,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblType2"), extendConfig({}, controller.args[2], "lblType2"));
        flxGroup2Cursor.add(lblCounter2, lblType2);
        flxCounter2.add(flxGroup2Cursor);
        var flxTabSeparator2 = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "clipBounds": true,
            "height": "60px",
            "id": "flxTabSeparator2",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "left": "0px",
            "isModalContainer": false,
            "skin": "sknflxConfigurationBundlesFillLightGrey",
            "top": "0px",
            "width": "1px",
            "zIndex": 1
        }, controller.args[0], "flxTabSeparator2"), extendConfig({}, controller.args[1], "flxTabSeparator2"), extendConfig({}, controller.args[2], "flxTabSeparator2"));
        flxTabSeparator2.setDefaultUnit(kony.flex.DP);
        flxTabSeparator2.add();
        var flxCounter3 = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "clipBounds": true,
            "height": "100%",
            "id": "flxCounter3",
            "isVisible": true,
            "layoutType": kony.flex.FLOW_VERTICAL,
            "left": "0dp",
            "isModalContainer": false,
            "skin": "sknflxLeadTabs",
            "top": "0dp",
            "width": "100dp",
            "zIndex": 1
        }, controller.args[0], "flxCounter3"), extendConfig({}, controller.args[1], "flxCounter3"), extendConfig({}, controller.args[2], "flxCounter3"));
        flxCounter3.setDefaultUnit(kony.flex.DP);
        var flxGroup3Cursor = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "clipBounds": true,
            "height": "100%",
            "id": "flxGroup3Cursor",
            "isVisible": true,
            "layoutType": kony.flex.FLOW_VERTICAL,
            "left": "0dp",
            "isModalContainer": false,
            "skin": "sknFlxPointer",
            "top": "0dp",
            "width": "100%"
        }, controller.args[0], "flxGroup3Cursor"), extendConfig({}, controller.args[1], "flxGroup3Cursor"), extendConfig({}, controller.args[2], "flxGroup3Cursor"));
        flxGroup3Cursor.setDefaultUnit(kony.flex.DP);
        var lblCounter3 = new kony.ui.Label(extendConfig({
            "centerX": "50%",
            "id": "lblCounter3",
            "isVisible": true,
            "skin": "sknlblLatoBold13px000000",
            "text": "2309",
            "top": "10dp",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, controller.args[0], "lblCounter3"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_CENTER,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblCounter3"), extendConfig({}, controller.args[2], "lblCounter3"));
        var lbltype3 = new kony.ui.Label(extendConfig({
            "centerX": "50%",
            "id": "lbltype3",
            "isVisible": true,
            "skin": "sknlbl485c7511px",
            "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmCSRController.ARCHIVED\")",
            "top": "2dp",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, controller.args[0], "lbltype3"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_CENTER,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lbltype3"), extendConfig({}, controller.args[2], "lbltype3"));
        flxGroup3Cursor.add(lblCounter3, lbltype3);
        flxCounter3.add(flxGroup3Cursor);
        var flxTabSeparator3 = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "clipBounds": true,
            "height": "60px",
            "id": "flxTabSeparator3",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "left": "0px",
            "isModalContainer": false,
            "skin": "sknflxConfigurationBundlesFillLightGrey",
            "top": "0px",
            "width": "1px",
            "zIndex": 1
        }, controller.args[0], "flxTabSeparator3"), extendConfig({}, controller.args[1], "flxTabSeparator3"), extendConfig({}, controller.args[2], "flxTabSeparator3"));
        flxTabSeparator3.setDefaultUnit(kony.flex.DP);
        flxTabSeparator3.add();
        var flxCounter4 = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "clipBounds": true,
            "height": "100%",
            "id": "flxCounter4",
            "isVisible": false,
            "layoutType": kony.flex.FLOW_VERTICAL,
            "left": "0dp",
            "isModalContainer": false,
            "skin": "sknflxLeadTabs",
            "top": "0dp",
            "width": "100dp",
            "zIndex": 1
        }, controller.args[0], "flxCounter4"), extendConfig({}, controller.args[1], "flxCounter4"), extendConfig({}, controller.args[2], "flxCounter4"));
        flxCounter4.setDefaultUnit(kony.flex.DP);
        var flxGroup4Cursor = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "clipBounds": true,
            "height": "100%",
            "id": "flxGroup4Cursor",
            "isVisible": true,
            "layoutType": kony.flex.FLOW_VERTICAL,
            "left": "0dp",
            "isModalContainer": false,
            "skin": "sknFlxPointer",
            "top": "0dp",
            "width": "100%"
        }, controller.args[0], "flxGroup4Cursor"), extendConfig({}, controller.args[1], "flxGroup4Cursor"), extendConfig({}, controller.args[2], "flxGroup4Cursor"));
        flxGroup4Cursor.setDefaultUnit(kony.flex.DP);
        var lblType4 = new kony.ui.Label(extendConfig({
            "centerX": "50%",
            "id": "lblType4",
            "isVisible": true,
            "skin": "sknlbl485c7511px",
            "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmDashboard.lblMyQueue\")",
            "top": "2dp",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, controller.args[0], "lblType4"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_CENTER,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblType4"), extendConfig({}, controller.args[2], "lblType4"));
        var lblCounter4 = new kony.ui.Label(extendConfig({
            "centerX": "50%",
            "id": "lblCounter4",
            "isVisible": true,
            "skin": "sknlblLatoBold13px000000",
            "text": "4",
            "top": "10dp",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, controller.args[0], "lblCounter4"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_CENTER,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblCounter4"), extendConfig({}, controller.args[2], "lblCounter4"));
        flxGroup4Cursor.add(lblType4, lblCounter4);
        flxCounter4.add(flxGroup4Cursor);
        var flxTabSeparator4 = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "clipBounds": true,
            "height": "60px",
            "id": "flxTabSeparator4",
            "isVisible": false,
            "layoutType": kony.flex.FREE_FORM,
            "left": "0px",
            "isModalContainer": false,
            "skin": "sknflxConfigurationBundlesFillLightGrey",
            "top": "0px",
            "width": "1px",
            "zIndex": 1
        }, controller.args[0], "flxTabSeparator4"), extendConfig({}, controller.args[1], "flxTabSeparator4"), extendConfig({}, controller.args[2], "flxTabSeparator4"));
        flxTabSeparator4.setDefaultUnit(kony.flex.DP);
        flxTabSeparator4.add();
        flxMainContainer.add(flxCounter1, flxTabSeparator1, flxCounter2, flxTabSeparator2, flxCounter3, flxTabSeparator3, flxCounter4, flxTabSeparator4);
        countTabs.add(flxMainContainer);
        return countTabs;
    }
})