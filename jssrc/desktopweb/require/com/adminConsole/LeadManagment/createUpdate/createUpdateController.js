define("com/adminConsole/LeadManagment/createUpdate/usercreateUpdateController", function() {
    return {};
});
define("com/adminConsole/LeadManagment/createUpdate/createUpdateControllerActions", {
    /*
          This is an auto generated file and any modifications to it may result in corruption of the action sequence.
        */
});
define("com/adminConsole/LeadManagment/createUpdate/createUpdateController", ["com/adminConsole/LeadManagment/createUpdate/usercreateUpdateController", "com/adminConsole/LeadManagment/createUpdate/createUpdateControllerActions"], function() {
    var controller = require("com/adminConsole/LeadManagment/createUpdate/usercreateUpdateController");
    var actions = require("com/adminConsole/LeadManagment/createUpdate/createUpdateControllerActions");
    for (var key in actions) {
        controller[key] = actions[key];
    }
    return controller;
});
