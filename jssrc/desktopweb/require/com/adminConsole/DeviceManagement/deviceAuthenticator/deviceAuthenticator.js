define(function() {
    return function(controller) {
        var deviceAuthenticator = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "bottom": "0dp",
            "clipBounds": true,
            "isMaster": true,
            "id": "deviceAuthenticator",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "left": "0dp",
            "isModalContainer": false,
            "preShow": function(eventobject) {
                controller.AS_FlexContainer_g4d2322561b240d6828da28d2b9a5e38(eventobject);
            },
            "skin": "slFbox",
            "top": "0dp",
            "width": "100%"
        }, controller.args[0], "deviceAuthenticator"), extendConfig({}, controller.args[1], "deviceAuthenticator"), extendConfig({}, controller.args[2], "deviceAuthenticator"));
        deviceAuthenticator.setDefaultUnit(kony.flex.DP);
        var flxDeviceAuthenticator = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "clipBounds": true,
            "height": "100%",
            "id": "flxDeviceAuthenticator",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "left": "0dp",
            "isModalContainer": false,
            "skin": "sknBackgroundFFFFFF",
            "top": "0dp",
            "width": "100%"
        }, controller.args[0], "flxDeviceAuthenticator"), extendConfig({}, controller.args[1], "flxDeviceAuthenticator"), extendConfig({}, controller.args[2], "flxDeviceAuthenticator"));
        flxDeviceAuthenticator.setDefaultUnit(kony.flex.DP);
        var flxDeviceAuthHeaders = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "clipBounds": true,
            "height": "52px",
            "id": "flxDeviceAuthHeaders",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "left": "20px",
            "isModalContainer": false,
            "right": "20px",
            "skin": "slFbox",
            "top": "0px",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, controller.args[0], "flxDeviceAuthHeaders"), extendConfig({}, controller.args[1], "flxDeviceAuthHeaders"), extendConfig({}, controller.args[2], "flxDeviceAuthHeaders"));
        flxDeviceAuthHeaders.setDefaultUnit(kony.flex.DP);
        var flxFriendlyNameHeader = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
            "centerY": "50%",
            "clipBounds": true,
            "id": "flxFriendlyNameHeader",
            "isVisible": true,
            "layoutType": kony.flex.FLOW_HORIZONTAL,
            "left": "10px",
            "isModalContainer": false,
            "skin": "slFbox",
            "width": "110px"
        }, controller.args[0], "flxFriendlyNameHeader"), extendConfig({}, controller.args[1], "flxFriendlyNameHeader"), extendConfig({}, controller.args[2], "flxFriendlyNameHeader"));
        flxFriendlyNameHeader.setDefaultUnit(kony.flex.DP);
        var lblFriendlyNameHeader = new kony.ui.Label(extendConfig({
            "id": "lblFriendlyNameHeader",
            "isVisible": true,
            "left": "0px",
            "skin": "sknlblLato696c7311px",
            "text": "FRIENDLY NAME",
            "top": "1px",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, controller.args[0], "lblFriendlyNameHeader"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblFriendlyNameHeader"), extendConfig({}, controller.args[2], "lblFriendlyNameHeader"));
        var fontIconFriendlyNameSort = new kony.ui.Label(extendConfig({
            "id": "fontIconFriendlyNameSort",
            "isVisible": true,
            "left": "5px",
            "skin": "sknIcon15px",
            "text": "",
            "top": "0px",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, controller.args[0], "fontIconFriendlyNameSort"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "fontIconFriendlyNameSort"), extendConfig({
            "hoverSkin": "sknlblCursorFont"
        }, controller.args[2], "fontIconFriendlyNameSort"));
        flxFriendlyNameHeader.add(lblFriendlyNameHeader, fontIconFriendlyNameSort);
        var flxDeviceIdHeader = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
            "centerY": "50%",
            "clipBounds": true,
            "id": "flxDeviceIdHeader",
            "isVisible": true,
            "layoutType": kony.flex.FLOW_HORIZONTAL,
            "left": "41%",
            "isModalContainer": false,
            "skin": "hoverhandSkin",
            "width": "140px"
        }, controller.args[0], "flxDeviceIdHeader"), extendConfig({}, controller.args[1], "flxDeviceIdHeader"), extendConfig({}, controller.args[2], "flxDeviceIdHeader"));
        flxDeviceIdHeader.setDefaultUnit(kony.flex.DP);
        var lblDeviceIdHeader = new kony.ui.Label(extendConfig({
            "id": "lblDeviceIdHeader",
            "isVisible": true,
            "left": "0px",
            "skin": "sknlblLato696c7311px",
            "text": "DEVICE ID",
            "top": "1px",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, controller.args[0], "lblDeviceIdHeader"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblDeviceIdHeader"), extendConfig({}, controller.args[2], "lblDeviceIdHeader"));
        var fontIconDeviceIdSort = new kony.ui.Label(extendConfig({
            "id": "fontIconDeviceIdSort",
            "isVisible": true,
            "left": "5px",
            "skin": "sknIcon15px",
            "text": "",
            "top": "0px",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, controller.args[0], "fontIconDeviceIdSort"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "fontIconDeviceIdSort"), extendConfig({
            "hoverSkin": "sknlblCursorFont"
        }, controller.args[2], "fontIconDeviceIdSort"));
        flxDeviceIdHeader.add(lblDeviceIdHeader, fontIconDeviceIdSort);
        var flxStatusHeader = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
            "centerY": "50%",
            "clipBounds": true,
            "id": "flxStatusHeader",
            "isVisible": true,
            "layoutType": kony.flex.FLOW_HORIZONTAL,
            "left": "77%",
            "isModalContainer": false,
            "skin": "hoverhandSkin",
            "width": "140px"
        }, controller.args[0], "flxStatusHeader"), extendConfig({}, controller.args[1], "flxStatusHeader"), extendConfig({}, controller.args[2], "flxStatusHeader"));
        flxStatusHeader.setDefaultUnit(kony.flex.DP);
        var lblDeviceStatus = new kony.ui.Label(extendConfig({
            "id": "lblDeviceStatus",
            "isVisible": true,
            "left": "0px",
            "skin": "sknlblLato696c7311px",
            "text": "STATUS",
            "top": "1px",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, controller.args[0], "lblDeviceStatus"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblDeviceStatus"), extendConfig({}, controller.args[2], "lblDeviceStatus"));
        flxStatusHeader.add(lblDeviceStatus);
        var lblSeparatorHeader = new kony.ui.Label(extendConfig({
            "bottom": "0px",
            "height": "1px",
            "id": "lblSeparatorHeader",
            "isVisible": true,
            "left": "0px",
            "right": "0px",
            "skin": "sknLblSeparator696C73",
            "text": "Label",
            "zIndex": 2
        }, controller.args[0], "lblSeparatorHeader"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_TOP_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblSeparatorHeader"), extendConfig({}, controller.args[2], "lblSeparatorHeader"));
        flxDeviceAuthHeaders.add(flxFriendlyNameHeader, flxDeviceIdHeader, flxStatusHeader, lblSeparatorHeader);
        var flxSegDeviceAuth = new kony.ui.FlexScrollContainer(extendConfig({
            "allowHorizontalBounce": false,
            "allowVerticalBounce": true,
            "bottom": "0px",
            "bounces": true,
            "clipBounds": true,
            "enableScrolling": true,
            "horizontalScrollIndicator": true,
            "id": "flxSegDeviceAuth",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "left": "0px",
            "pagingEnabled": false,
            "scrollDirection": kony.flex.SCROLL_VERTICAL,
            "skin": "slFSbox",
            "top": "53px",
            "verticalScrollIndicator": true,
            "width": "100%"
        }, controller.args[0], "flxSegDeviceAuth"), extendConfig({}, controller.args[1], "flxSegDeviceAuth"), extendConfig({}, controller.args[2], "flxSegDeviceAuth"));
        flxSegDeviceAuth.setDefaultUnit(kony.flex.DP);
        var segDeviceAuth = new kony.ui.SegmentedUI2(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
            "data": [{
                "fontIconStatusInfo": "",
                "lblCopy": "",
                "lblDeviceId": "Device Id",
                "lblName": "Name",
                "lblOptions": "",
                "lblSeparator": "Label",
                "lblStatus": "STATUS"
            }, {
                "fontIconStatusInfo": "",
                "lblCopy": "",
                "lblDeviceId": "Device Id",
                "lblName": "Name",
                "lblOptions": "",
                "lblSeparator": "Label",
                "lblStatus": "STATUS"
            }, {
                "fontIconStatusInfo": "",
                "lblCopy": "",
                "lblDeviceId": "Device Id",
                "lblName": "Name",
                "lblOptions": "",
                "lblSeparator": "Label",
                "lblStatus": "STATUS"
            }],
            "groupCells": false,
            "id": "segDeviceAuth",
            "isVisible": true,
            "left": "20px",
            "needPageIndicator": true,
            "pageOffDotImage": "pageoffdot.png",
            "pageOnDotImage": "pageondot.png",
            "retainSelection": false,
            "right": "20px",
            "rowFocusSkin": "seg2Focus",
            "rowSkin": "sknseg3pxRadius",
            "rowTemplate": "flxDeviceAuthenticator",
            "sectionHeaderSkin": "sliPhoneSegmentHeader",
            "selectionBehavior": constants.SEGUI_DEFAULT_BEHAVIOR,
            "separatorRequired": false,
            "showScrollbars": false,
            "top": "0px",
            "viewType": constants.SEGUI_VIEW_TYPE_TABLEVIEW,
            "widgetDataMap": {
                "flxCopy": "flxCopy",
                "flxDeviceAuthenticator": "flxDeviceAuthenticator",
                "flxDeviceId": "flxDeviceId",
                "flxOptions": "flxOptions",
                "fontIconStatusInfo": "fontIconStatusInfo",
                "lblCopy": "lblCopy",
                "lblDeviceId": "lblDeviceId",
                "lblName": "lblName",
                "lblOptions": "lblOptions",
                "lblSeparator": "lblSeparator",
                "lblStatus": "lblStatus"
            },
            "zIndex": 1
        }, controller.args[0], "segDeviceAuth"), extendConfig({
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "segDeviceAuth"), extendConfig({}, controller.args[2], "segDeviceAuth"));
        flxSegDeviceAuth.add(segDeviceAuth);
        var flxNoResultFound = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "clipBounds": true,
            "height": "200px",
            "id": "flxNoResultFound",
            "isVisible": false,
            "layoutType": kony.flex.FREE_FORM,
            "left": "0px",
            "isModalContainer": false,
            "skin": "slFbox",
            "top": "53px",
            "width": "100%",
            "zIndex": 1
        }, controller.args[0], "flxNoResultFound"), extendConfig({}, controller.args[1], "flxNoResultFound"), extendConfig({}, controller.args[2], "flxNoResultFound"));
        flxNoResultFound.setDefaultUnit(kony.flex.DP);
        var lblNoResultFound = new kony.ui.Label(extendConfig({
            "centerX": "50%",
            "centerY": "50%",
            "id": "lblNoResultFound",
            "isVisible": true,
            "skin": "sknLbl485C75LatoRegular13Px",
            "text": "No result found.",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, controller.args[0], "lblNoResultFound"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblNoResultFound"), extendConfig({}, controller.args[2], "lblNoResultFound"));
        flxNoResultFound.add(lblNoResultFound);
        flxDeviceAuthenticator.add(flxDeviceAuthHeaders, flxSegDeviceAuth, flxNoResultFound);
        var selectOptions = new com.adminConsole.adManagement.selectOptions(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
            "id": "selectOptions",
            "isVisible": false,
            "layoutType": kony.flex.FLOW_VERTICAL,
            "left": "1024px",
            "masterType": constants.MASTER_TYPE_DEFAULT,
            "isModalContainer": false,
            "right": "20dp",
            "skin": "slFbox",
            "top": "100dp",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "overrides": {
                "flxOption1": {
                    "width": kony.flex.USE_PREFFERED_SIZE
                },
                "flxOption2": {
                    "width": kony.flex.USE_PREFFERED_SIZE
                },
                "flxOption3": {
                    "isVisible": false,
                    "width": kony.flex.USE_PREFFERED_SIZE
                },
                "flxOption4": {
                    "isVisible": false,
                    "width": kony.flex.USE_PREFFERED_SIZE
                },
                "flxSelectOptionsInner": {
                    "width": kony.flex.USE_PREFFERED_SIZE
                },
                "fontIconOption1": {
                    "text": ""
                },
                "fontIconOption2": {
                    "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmPermissions.fontIconOption3\")"
                },
                "imgDownArrow": {
                    "src": "downarrow_2x.png"
                },
                "imgUpArrow": {
                    "src": "uparrow_2x.png"
                },
                "lblOption1": {
                    "text": "Revoke",
                    "width": kony.flex.USE_PREFFERED_SIZE
                },
                "lblOption2": {
                    "text": "Suspend"
                },
                "lblSeperator": {
                    "isVisible": false,
                    "width": kony.flex.USE_PREFFERED_SIZE
                },
                "selectOptions": {
                    "isVisible": false,
                    "left": "1024px",
                    "right": "20dp",
                    "top": "100dp",
                    "width": kony.flex.USE_PREFFERED_SIZE
                }
            }
        }, controller.args[0], "selectOptions"), extendConfig({
            "overrides": {}
        }, controller.args[1], "selectOptions"), extendConfig({
            "overrides": {}
        }, controller.args[2], "selectOptions"));
        deviceAuthenticator.add(flxDeviceAuthenticator, selectOptions);
        return deviceAuthenticator;
    }
})