define(function() {
    return function(controller) {
        var supportedCurrency = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "clipBounds": true,
            "isMaster": true,
            "height": "30px",
            "id": "supportedCurrency",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "left": "0px",
            "isModalContainer": false,
            "skin": "sknflxLocationCurrency",
            "top": "0px",
            "width": "80px"
        }, controller.args[0], "supportedCurrency"), extendConfig({}, controller.args[1], "supportedCurrency"), extendConfig({
            "hoverSkin": "tabsHoverSkin"
        }, controller.args[2], "supportedCurrency"));
        supportedCurrency.setDefaultUnit(kony.flex.DP);
        var flxcbSupportedCurrency = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "centerY": "50%",
            "clipBounds": true,
            "height": "15px",
            "id": "flxcbSupportedCurrency",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "left": "5px",
            "isModalContainer": false,
            "skin": "sknCursor",
            "width": "15px",
            "zIndex": 1
        }, controller.args[0], "flxcbSupportedCurrency"), extendConfig({}, controller.args[1], "flxcbSupportedCurrency"), extendConfig({}, controller.args[2], "flxcbSupportedCurrency"));
        flxcbSupportedCurrency.setDefaultUnit(kony.flex.DP);
        var imgcbSupportedCurrency = new kony.ui.Image2(extendConfig({
            "centerX": "50%",
            "centerY": "50%",
            "height": "100%",
            "id": "imgcbSupportedCurrency",
            "isVisible": true,
            "left": "0px",
            "skin": "slImage",
            "src": "checkboxselected.png",
            "top": "0px",
            "width": "98%",
            "zIndex": 1
        }, controller.args[0], "imgcbSupportedCurrency"), extendConfig({
            "imageScaleMode": constants.IMAGE_SCALE_MODE_MAINTAIN_ASPECT_RATIO,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "imgcbSupportedCurrency"), extendConfig({}, controller.args[2], "imgcbSupportedCurrency"));
        flxcbSupportedCurrency.add(imgcbSupportedCurrency);
        var lblSupportedCurrency = new kony.ui.Label(extendConfig({
            "centerY": "50%",
            "id": "lblSupportedCurrency",
            "isVisible": true,
            "left": "25px",
            "skin": "slLabel0d20174dce8ea42",
            "text": "USD",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, controller.args[0], "lblSupportedCurrency"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblSupportedCurrency"), extendConfig({}, controller.args[2], "lblSupportedCurrency"));
        supportedCurrency.add(flxcbSupportedCurrency, lblSupportedCurrency);
        return supportedCurrency;
    }
})