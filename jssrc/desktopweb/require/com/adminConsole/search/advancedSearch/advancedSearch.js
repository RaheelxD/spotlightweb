define(function() {
    return function(controller) {
        var advancedSearch = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "clipBounds": true,
            "isMaster": true,
            "height": "85px",
            "id": "advancedSearch",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "left": "0dp",
            "isModalContainer": false,
            "skin": "slFbox0hfd18814fd664dCM",
            "top": "0dp",
            "width": "100%"
        }, controller.args[0], "advancedSearch"), extendConfig({}, controller.args[1], "advancedSearch"), extendConfig({}, controller.args[2], "advancedSearch"));
        advancedSearch.setDefaultUnit(kony.flex.DP);
        var flxSearch = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "clipBounds": true,
            "height": "100%",
            "id": "flxSearch",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "left": "0dp",
            "isModalContainer": false,
            "skin": "slFbox",
            "top": "0dp",
            "width": "100%",
            "zIndex": 1
        }, controller.args[0], "flxSearch"), extendConfig({}, controller.args[1], "flxSearch"), extendConfig({}, controller.args[2], "flxSearch"));
        flxSearch.setDefaultUnit(kony.flex.DP);
        var flxNames = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "clipBounds": true,
            "height": "50px",
            "id": "flxNames",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "left": "0dp",
            "isModalContainer": false,
            "skin": "slFbox",
            "top": "10dp",
            "width": "100%",
            "zIndex": 1
        }, controller.args[0], "flxNames"), extendConfig({}, controller.args[1], "flxNames"), extendConfig({}, controller.args[2], "flxNames"));
        flxNames.setDefaultUnit(kony.flex.DP);
        var lblFilter = new kony.ui.Label(extendConfig({
            "centerY": "50%",
            "id": "lblFilter",
            "isVisible": true,
            "left": "35px",
            "skin": "CopyslLabel0d655f1f7269c45",
            "i18n_text": "kony.i18n.getLocalizedString(\"i18n.userwidgetmodel.lblFilter\")",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, controller.args[0], "lblFilter"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblFilter"), extendConfig({}, controller.args[2], "lblFilter"));
        var lblRole = new kony.ui.Label(extendConfig({
            "centerY": "50%",
            "id": "lblRole",
            "isVisible": true,
            "left": "100dp",
            "skin": "CopyslLabel0cee60b34793c4a",
            "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmPermissions.btnRoles\")",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, controller.args[0], "lblRole"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblRole"), extendConfig({}, controller.args[2], "lblRole"));
        var imgDownArrow = new kony.ui.Image2(extendConfig({
            "centerY": "50%",
            "height": "10dp",
            "id": "imgDownArrow",
            "isVisible": true,
            "left": "130dp",
            "skin": "slImage",
            "src": "arrowdown_1x.png",
            "width": "10dp",
            "zIndex": 1
        }, controller.args[0], "imgDownArrow"), extendConfig({
            "imageScaleMode": constants.IMAGE_SCALE_MODE_MAINTAIN_ASPECT_RATIO,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "imgDownArrow"), extendConfig({}, controller.args[2], "imgDownArrow"));
        var lblStatus = new kony.ui.Label(extendConfig({
            "centerY": "50%",
            "id": "lblStatus",
            "isVisible": true,
            "left": "195px",
            "skin": "CopyslLabel0c81403875fd642",
            "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmAlertsManagement.lblEditAlertStatus\")",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, controller.args[0], "lblStatus"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblStatus"), extendConfig({}, controller.args[2], "lblStatus"));
        var imgDownArrow1 = new kony.ui.Image2(extendConfig({
            "centerY": "50%",
            "height": "10dp",
            "id": "imgDownArrow1",
            "isVisible": true,
            "left": "235dp",
            "skin": "slImage",
            "src": "downrow.png",
            "width": "10dp",
            "zIndex": 1
        }, controller.args[0], "imgDownArrow1"), extendConfig({
            "imageScaleMode": constants.IMAGE_SCALE_MODE_MAINTAIN_ASPECT_RATIO,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "imgDownArrow1"), extendConfig({}, controller.args[2], "imgDownArrow1"));
        var calUpdatedDate = new kony.ui.Calendar(extendConfig({
            "calendarIcon": "calbtn.png",
            "centerY": "50%",
            "dateComponents": [27, 10, 2017],
            "dateFormat": "MM/dd/yyyy",
            "day": 27,
            "formattedDate": "10/27/2017",
            "height": "40dp",
            "hour": 0,
            "id": "calUpdatedDate",
            "isVisible": false,
            "left": "290dp",
            "minutes": 0,
            "month": 10,
            "placeholder": "Updated Date",
            "seconds": 0,
            "skin": "sknCalUpdateDate",
            "viewType": constants.CALENDAR_VIEW_TYPE_DEFAULT,
            "width": "160dp",
            "year": 2017,
            "zIndex": 1
        }, controller.args[0], "calUpdatedDate"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_CENTER,
            "padding": [0, 0, 5, 0],
            "paddingInPixel": false
        }, controller.args[1], "calUpdatedDate"), extendConfig({
            "noOfMonths": 1
        }, controller.args[2], "calUpdatedDate"));
        var calCreateDate = new kony.ui.Calendar(extendConfig({
            "calendarIcon": "calbtn.png",
            "centerY": "50%",
            "dateComponents": [27, 10, 2017],
            "dateFormat": "MM/dd/yyyy",
            "day": 27,
            "formattedDate": "10/27/2017",
            "height": "40dp",
            "hour": 0,
            "id": "calCreateDate",
            "isVisible": false,
            "left": "450dp",
            "minutes": 0,
            "month": 10,
            "placeholder": "Created Date",
            "seconds": 0,
            "skin": "skncalCreateDate",
            "viewType": constants.CALENDAR_VIEW_TYPE_DEFAULT,
            "width": "160dp",
            "year": 2017,
            "zIndex": 1
        }, controller.args[0], "calCreateDate"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_CENTER,
            "padding": [0, 0, 5, 0],
            "paddingInPixel": false
        }, controller.args[1], "calCreateDate"), extendConfig({
            "noOfMonths": 1
        }, controller.args[2], "calCreateDate"));
        flxNames.add(lblFilter, lblRole, imgDownArrow, lblStatus, imgDownArrow1, calUpdatedDate, calCreateDate);
        var lblShowingResults = new kony.ui.Label(extendConfig({
            "id": "lblShowingResults",
            "isVisible": true,
            "left": "35px",
            "skin": "CopyslLabel0db14572bc5ef4f",
            "i18n_text": "kony.i18n.getLocalizedString(\"i18n.userwidgetmodel.lblShowingResults\")",
            "top": "60dp",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, controller.args[0], "lblShowingResults"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblShowingResults"), extendConfig({}, controller.args[2], "lblShowingResults"));
        flxSearch.add(flxNames, lblShowingResults);
        var flxLine = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "bottom": "0px",
            "clipBounds": true,
            "height": "1px",
            "id": "flxLine",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "left": "0px",
            "isModalContainer": false,
            "skin": "CopyslFbox0j2fe4355415c4b",
            "width": "100%",
            "zIndex": 1
        }, controller.args[0], "flxLine"), extendConfig({}, controller.args[1], "flxLine"), extendConfig({}, controller.args[2], "flxLine"));
        flxLine.setDefaultUnit(kony.flex.DP);
        flxLine.add();
        advancedSearch.add(flxSearch, flxLine);
        return advancedSearch;
    }
})