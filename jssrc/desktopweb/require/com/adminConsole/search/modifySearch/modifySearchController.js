define("com/adminConsole/search/modifySearch/usermodifySearchController", function() {
    return {};
});
define("com/adminConsole/search/modifySearch/modifySearchControllerActions", {
    /*
          This is an auto generated file and any modifications to it may result in corruption of the action sequence.
        */
});
define("com/adminConsole/search/modifySearch/modifySearchController", ["com/adminConsole/search/modifySearch/usermodifySearchController", "com/adminConsole/search/modifySearch/modifySearchControllerActions"], function() {
    var controller = require("com/adminConsole/search/modifySearch/usermodifySearchController");
    var actions = require("com/adminConsole/search/modifySearch/modifySearchControllerActions");
    for (var key in actions) {
        controller[key] = actions[key];
    }
    return controller;
});
