define(function() {
    return function(controller) {
        var modifySearch = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "clipBounds": true,
            "isMaster": true,
            "height": "20px",
            "id": "modifySearch",
            "isVisible": true,
            "layoutType": kony.flex.FLOW_HORIZONTAL,
            "left": "0dp",
            "isModalContainer": false,
            "skin": "slFbox",
            "top": "0dp",
            "width": "100%"
        }, controller.args[0], "modifySearch"), extendConfig({}, controller.args[1], "modifySearch"), extendConfig({}, controller.args[2], "modifySearch"));
        modifySearch.setDefaultUnit(kony.flex.DP);
        var lblCurrentScreen = new kony.ui.Label(extendConfig({
            "centerY": "50%",
            "height": "20px",
            "id": "lblCurrentScreen",
            "isVisible": true,
            "left": "35px",
            "skin": "sknlblLatoRegular00000013px",
            "i18n_text": "kony.i18n.getLocalizedString(\"i18n.userwidgetmodel.lblCurrentScreen\")",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, controller.args[0], "lblCurrentScreen"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblCurrentScreen"), extendConfig({}, controller.args[2], "lblCurrentScreen"));
        var lblName = new kony.ui.Label(extendConfig({
            "centerY": "50%",
            "height": "20px",
            "id": "lblName",
            "isVisible": true,
            "left": "3px",
            "skin": "sknlblLatoBold00000011px",
            "i18n_text": "kony.i18n.getLocalizedString(\"i18n.userwidgetmodel.lblName\")",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, controller.args[0], "lblName"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblName"), extendConfig({}, controller.args[2], "lblName"));
        var btnBackToMain = new kony.ui.Button(extendConfig({
            "centerY": "50%",
            "height": "20px",
            "id": "btnBackToMain",
            "isVisible": true,
            "left": "10px",
            "skin": "sknBtnLato11ABEB11Px",
            "i18n_text": "kony.i18n.getLocalizedString(\"i18n.userwidgetmodel.btnBackToMain\")",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, controller.args[0], "btnBackToMain"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "displayText": true,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "btnBackToMain"), extendConfig({}, controller.args[2], "btnBackToMain"));
        modifySearch.add(lblCurrentScreen, lblName, btnBackToMain);
        return modifySearch;
    }
})