define("com/adminConsole/ServiceManagement/displayContentEdit/userdisplayContentEditController", function() {
    return {
        setFlowActions: function() {
            var scopeObj = this;
            this.view.tbxEditName.onKeyUp = function() {
                scopeObj.view.tbxEditName.skin = "skntbxLato35475f14px";
                scopeObj.view.flxNoEditNameError.isVisible = false;
                if (scopeObj.view.tbxEditName.text.length === 0) {
                    scopeObj.view.lblNameCount.setVisibility(false);
                } else {
                    scopeObj.view.lblNameCount.setVisibility(true);
                    scopeObj.view.lblNameCount.text = scopeObj.view.tbxEditName.text.length + "/60";
                }
                scopeObj.view.forceLayout();
            };
            this.view.txtareaEditDescription.onKeyUp = function() {
                scopeObj.view.txtareaEditDescription.skin = "skntxtAreaLato35475f14Px";
                scopeObj.view.flxNoEditDescriptionError.isVisible = false;
                if (scopeObj.view.txtareaEditDescription.text.length === 0) {
                    scopeObj.view.lblDescriptionCount.setVisibility(false);
                } else {
                    scopeObj.view.lblDescriptionCount.setVisibility(true);
                    scopeObj.view.lblDescriptionCount.text = scopeObj.view.txtareaEditDescription.text.length + "/100";
                }
                scopeObj.view.forceLayout();
            };
        },
    };
});
define("com/adminConsole/ServiceManagement/displayContentEdit/displayContentEditControllerActions", {
    /*
          This is an auto generated file and any modifications to it may result in corruption of the action sequence.
        */
});
define("com/adminConsole/ServiceManagement/displayContentEdit/displayContentEditController", ["com/adminConsole/ServiceManagement/displayContentEdit/userdisplayContentEditController", "com/adminConsole/ServiceManagement/displayContentEdit/displayContentEditControllerActions"], function() {
    var controller = require("com/adminConsole/ServiceManagement/displayContentEdit/userdisplayContentEditController");
    var actions = require("com/adminConsole/ServiceManagement/displayContentEdit/displayContentEditControllerActions");
    for (var key in actions) {
        controller[key] = actions[key];
    }
    return controller;
});
