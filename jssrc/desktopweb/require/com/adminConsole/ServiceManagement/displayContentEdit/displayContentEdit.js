define(function() {
    return function(controller) {
        var displayContentEdit = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
            "clipBounds": true,
            "isMaster": true,
            "id": "displayContentEdit",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "left": "0dp",
            "isModalContainer": false,
            "skin": "slFbox",
            "top": "0dp",
            "width": "100%"
        }, controller.args[0], "displayContentEdit"), extendConfig({}, controller.args[1], "displayContentEdit"), extendConfig({}, controller.args[2], "displayContentEdit"));
        displayContentEdit.setDefaultUnit(kony.flex.DP);
        var flxDisplayContentEdit = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "clipBounds": true,
            "height": "300dp",
            "id": "flxDisplayContentEdit",
            "isVisible": true,
            "layoutType": kony.flex.FLOW_VERTICAL,
            "left": "20px",
            "isModalContainer": false,
            "right": "20px",
            "skin": "sknFlxBgF8F9FABrE1E5EER3px",
            "top": "0px",
            "zIndex": 1
        }, controller.args[0], "flxDisplayContentEdit"), extendConfig({}, controller.args[1], "flxDisplayContentEdit"), extendConfig({}, controller.args[2], "flxDisplayContentEdit"));
        flxDisplayContentEdit.setDefaultUnit(kony.flex.DP);
        var flxDisplayContentHeaderEdit = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
            "clipBounds": true,
            "id": "flxDisplayContentHeaderEdit",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "left": "0dp",
            "isModalContainer": false,
            "skin": "slFbox",
            "top": "0dp",
            "width": "100%"
        }, controller.args[0], "flxDisplayContentHeaderEdit"), extendConfig({}, controller.args[1], "flxDisplayContentHeaderEdit"), extendConfig({}, controller.args[2], "flxDisplayContentHeaderEdit"));
        flxDisplayContentHeaderEdit.setDefaultUnit(kony.flex.DP);
        var lblDisplayContentHeader = new kony.ui.Label(extendConfig({
            "id": "lblDisplayContentHeader",
            "isVisible": true,
            "left": "20dp",
            "skin": "sknLatoRegular485C7518px",
            "text": "Display Content",
            "top": "17dp",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, controller.args[0], "lblDisplayContentHeader"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblDisplayContentHeader"), extendConfig({}, controller.args[2], "lblDisplayContentHeader"));
        var flxLanguageHeader = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "clipBounds": true,
            "height": "30dp",
            "id": "flxLanguageHeader",
            "isVisible": true,
            "layoutType": kony.flex.FLOW_HORIZONTAL,
            "left": "165dp",
            "isModalContainer": false,
            "skin": "slFbox",
            "top": "14dp",
            "width": "200dp"
        }, controller.args[0], "flxLanguageHeader"), extendConfig({}, controller.args[1], "flxLanguageHeader"), extendConfig({}, controller.args[2], "flxLanguageHeader"));
        flxLanguageHeader.setDefaultUnit(kony.flex.DP);
        var lblSelectedLanguage = new kony.ui.Label(extendConfig({
            "centerY": "50%",
            "id": "lblSelectedLanguage",
            "isVisible": true,
            "left": "0dp",
            "skin": "sknlblLatoBold35475f14px",
            "text": "English (Default)",
            "top": "0dp",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, controller.args[0], "lblSelectedLanguage"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblSelectedLanguage"), extendConfig({}, controller.args[2], "lblSelectedLanguage"));
        var lblSelectedLanguageIcon = new kony.ui.Label(extendConfig({
            "centerY": "50%",
            "id": "lblSelectedLanguageIcon",
            "isVisible": true,
            "left": "5dp",
            "skin": "sknIcon12pxBlack",
            "text": "",
            "top": "0dp",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 11
        }, controller.args[0], "lblSelectedLanguageIcon"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblSelectedLanguageIcon"), extendConfig({}, controller.args[2], "lblSelectedLanguageIcon"));
        flxLanguageHeader.add(lblSelectedLanguage, lblSelectedLanguageIcon);
        flxDisplayContentHeaderEdit.add(lblDisplayContentHeader, flxLanguageHeader);
        var flxNameContainer = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
            "clipBounds": true,
            "id": "flxNameContainer",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "left": "20dp",
            "isModalContainer": false,
            "skin": "slFbox",
            "top": "10dp",
            "width": "95%"
        }, controller.args[0], "flxNameContainer"), extendConfig({}, controller.args[1], "flxNameContainer"), extendConfig({}, controller.args[2], "flxNameContainer"));
        flxNameContainer.setDefaultUnit(kony.flex.DP);
        var lblEditName = new kony.ui.Label(extendConfig({
            "height": "20px",
            "id": "lblEditName",
            "isVisible": true,
            "left": "0dp",
            "skin": "sknlblLatoBold35475f14px",
            "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmAlertsManagement.lblEditAlertMainName\")",
            "top": "0px",
            "width": "150dp",
            "zIndex": 1
        }, controller.args[0], "lblEditName"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblEditName"), extendConfig({}, controller.args[2], "lblEditName"));
        var lblNameCount = new kony.ui.Label(extendConfig({
            "height": "20px",
            "id": "lblNameCount",
            "isVisible": true,
            "right": "0dp",
            "skin": "sknlblLatoBold35475f14px",
            "text": "0/60",
            "top": "0dp",
            "width": "150dp",
            "zIndex": 1
        }, controller.args[0], "lblNameCount"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_RIGHT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblNameCount"), extendConfig({}, controller.args[2], "lblNameCount"));
        var tbxEditName = new kony.ui.TextBox2(extendConfig({
            "autoCapitalize": constants.TEXTBOX_AUTO_CAPITALIZE_NONE,
            "focusSkin": "skntxtbxDetailsFocus14px",
            "height": "40px",
            "id": "tbxEditName",
            "isVisible": true,
            "keyBoardStyle": constants.TEXTBOX_KEY_BOARD_STYLE_DEFAULT,
            "left": "0px",
            "maxTextLength": 60,
            "placeholder": "Name",
            "secureTextEntry": false,
            "skin": "skntbxLato35475f14px",
            "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmAlertsManagement.Default\")",
            "textInputMode": constants.TEXTBOX_INPUT_MODE_ANY,
            "top": "30px",
            "width": "100%",
            "zIndex": 1
        }, controller.args[0], "tbxEditName"), extendConfig({
            "containerHeightMode": constants.TEXTBOX_FONT_METRICS_DRIVEN_HEIGHT,
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [1, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "tbxEditName"), extendConfig({
            "autoCorrect": false
        }, controller.args[2], "tbxEditName"));
        var flxNoEditNameError = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "bottom": "0dp",
            "clipBounds": true,
            "height": "15px",
            "id": "flxNoEditNameError",
            "isVisible": false,
            "layoutType": kony.flex.FREE_FORM,
            "left": "0px",
            "isModalContainer": false,
            "skin": "slFbox",
            "top": "75dp",
            "width": "100%",
            "zIndex": 2
        }, controller.args[0], "flxNoEditNameError"), extendConfig({}, controller.args[1], "flxNoEditNameError"), extendConfig({}, controller.args[2], "flxNoEditNameError"));
        flxNoEditNameError.setDefaultUnit(kony.flex.DP);
        var lblNoEditNameErrorIcon = new kony.ui.Label(extendConfig({
            "height": "15px",
            "id": "lblNoEditNameErrorIcon",
            "isVisible": true,
            "left": "0px",
            "skin": "sknErrorIcon",
            "text": "",
            "top": "0px",
            "width": "15px",
            "zIndex": 1
        }, controller.args[0], "lblNoEditNameErrorIcon"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblNoEditNameErrorIcon"), extendConfig({}, controller.args[2], "lblNoEditNameErrorIcon"));
        var lblNoEditNameError = new kony.ui.Label(extendConfig({
            "height": "15px",
            "id": "lblNoEditNameError",
            "isVisible": true,
            "left": "15px",
            "skin": "sknlblError",
            "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmAlertsManagement.lblNoEditAlertMainNameError\")",
            "top": "0px",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, controller.args[0], "lblNoEditNameError"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblNoEditNameError"), extendConfig({}, controller.args[2], "lblNoEditNameError"));
        flxNoEditNameError.add(lblNoEditNameErrorIcon, lblNoEditNameError);
        flxNameContainer.add(lblEditName, lblNameCount, tbxEditName, flxNoEditNameError);
        var flxDescriptionContainer = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
            "clipBounds": true,
            "id": "flxDescriptionContainer",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "left": "20dp",
            "isModalContainer": false,
            "right": "20dp",
            "skin": "slFbox",
            "top": "20dp",
            "width": "95%"
        }, controller.args[0], "flxDescriptionContainer"), extendConfig({}, controller.args[1], "flxDescriptionContainer"), extendConfig({}, controller.args[2], "flxDescriptionContainer"));
        flxDescriptionContainer.setDefaultUnit(kony.flex.DP);
        var lblEditDescription = new kony.ui.Label(extendConfig({
            "height": "20px",
            "id": "lblEditDescription",
            "isVisible": true,
            "left": "0px",
            "skin": "sknlblLatoBold35475f14px",
            "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmAlertsManagement.lblEditAlertDescription\")",
            "top": "0px",
            "width": "150px",
            "zIndex": 1
        }, controller.args[0], "lblEditDescription"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblEditDescription"), extendConfig({}, controller.args[2], "lblEditDescription"));
        var lblDescriptionCount = new kony.ui.Label(extendConfig({
            "height": "20px",
            "id": "lblDescriptionCount",
            "isVisible": true,
            "right": "0px",
            "skin": "sknlblLatoBold35475f14px",
            "text": "0/100",
            "top": "0px",
            "width": "150dp",
            "zIndex": 1
        }, controller.args[0], "lblDescriptionCount"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_RIGHT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblDescriptionCount"), extendConfig({}, controller.args[2], "lblDescriptionCount"));
        var txtareaEditDescription = new kony.ui.TextArea2(extendConfig({
            "autoCapitalize": constants.TEXTAREA_AUTO_CAPITALIZE_NONE,
            "focusSkin": "skntxtAreaLato35475f14Px",
            "height": "85px",
            "id": "txtareaEditDescription",
            "isVisible": true,
            "keyBoardStyle": constants.TEXTAREA_KEY_BOARD_STYLE_DEFAULT,
            "left": "0px",
            "maxTextLength": 100,
            "numberOfVisibleLines": 3,
            "placeholder": "Description",
            "skin": "skntxtAreaLato35475f14Px",
            "text": "Feature description",
            "textInputMode": constants.TEXTAREA_INPUT_MODE_ANY,
            "top": "30px",
            "width": "100%",
            "zIndex": 1
        }, controller.args[0], "txtareaEditDescription"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_TOP_LEFT,
            "padding": [1, 1, 0, 1],
            "paddingInPixel": false
        }, controller.args[1], "txtareaEditDescription"), extendConfig({
            "autoCorrect": false
        }, controller.args[2], "txtareaEditDescription"));
        var flxNoEditDescriptionError = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "clipBounds": true,
            "height": "15px",
            "id": "flxNoEditDescriptionError",
            "isVisible": false,
            "layoutType": kony.flex.FREE_FORM,
            "left": "0px",
            "isModalContainer": false,
            "skin": "slFbox",
            "top": "120px",
            "width": "100%",
            "zIndex": 2
        }, controller.args[0], "flxNoEditDescriptionError"), extendConfig({}, controller.args[1], "flxNoEditDescriptionError"), extendConfig({}, controller.args[2], "flxNoEditDescriptionError"));
        flxNoEditDescriptionError.setDefaultUnit(kony.flex.DP);
        var lblNoEditFeatureDescriptionErrorIcon = new kony.ui.Label(extendConfig({
            "height": "15px",
            "id": "lblNoEditFeatureDescriptionErrorIcon",
            "isVisible": true,
            "left": "0px",
            "skin": "sknErrorIcon",
            "text": "",
            "top": "0px",
            "width": "15px",
            "zIndex": 1
        }, controller.args[0], "lblNoEditFeatureDescriptionErrorIcon"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblNoEditFeatureDescriptionErrorIcon"), extendConfig({}, controller.args[2], "lblNoEditFeatureDescriptionErrorIcon"));
        var lblNoEditFeatureDescriptionError = new kony.ui.Label(extendConfig({
            "height": "15px",
            "id": "lblNoEditFeatureDescriptionError",
            "isVisible": true,
            "left": "15px",
            "skin": "sknlblError",
            "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmAlertsManagement.lblNoEditAlertDescriptionError\")",
            "top": "0px",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, controller.args[0], "lblNoEditFeatureDescriptionError"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblNoEditFeatureDescriptionError"), extendConfig({}, controller.args[2], "lblNoEditFeatureDescriptionError"));
        flxNoEditDescriptionError.add(lblNoEditFeatureDescriptionErrorIcon, lblNoEditFeatureDescriptionError);
        flxDescriptionContainer.add(lblEditDescription, lblDescriptionCount, txtareaEditDescription, flxNoEditDescriptionError);
        flxDisplayContentEdit.add(flxDisplayContentHeaderEdit, flxNameContainer, flxDescriptionContainer);
        var flxLanguagesEdit = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
            "clipBounds": true,
            "id": "flxLanguagesEdit",
            "isVisible": false,
            "layoutType": kony.flex.FREE_FORM,
            "left": "143dp",
            "isModalContainer": false,
            "skin": "slFbox",
            "top": "35px",
            "width": "170dp",
            "zIndex": 11
        }, controller.args[0], "flxLanguagesEdit"), extendConfig({}, controller.args[1], "flxLanguagesEdit"), extendConfig({}, controller.args[2], "flxLanguagesEdit"));
        flxLanguagesEdit.setDefaultUnit(kony.flex.DP);
        var EditLangList = new com.adminConsole.Products.productTypeFilterMenu(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
            "id": "EditLangList",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "left": "0dp",
            "masterType": constants.MASTER_TYPE_DEFAULT,
            "isModalContainer": false,
            "skin": "slFbox",
            "top": "5dp",
            "width": "100%",
            "zIndex": 5,
            "overrides": {
                "flxCheckboxInner": {
                    "height": "120px"
                },
                "flxFilterSort": {
                    "isVisible": false
                },
                "imgUpArrow": {
                    "src": "uparrow_2x.png"
                },
                "lblSeperator1": {
                    "isVisible": false
                },
                "segStatusFilterDropdown": {
                    "data": [{
                        "imgCheckBox": "checkbox.png",
                        "lblDescription": "Active"
                    }, {
                        "imgCheckBox": "checkbox.png",
                        "lblDescription": "Inactive"
                    }, {
                        "imgCheckBox": "checkbox.png",
                        "lblDescription": "Suspended"
                    }]
                }
            }
        }, controller.args[0], "EditLangList"), extendConfig({
            "overrides": {}
        }, controller.args[1], "EditLangList"), extendConfig({
            "overrides": {}
        }, controller.args[2], "EditLangList"));
        flxLanguagesEdit.add(EditLangList);
        displayContentEdit.add(flxDisplayContentEdit, flxLanguagesEdit);
        return displayContentEdit;
    }
})