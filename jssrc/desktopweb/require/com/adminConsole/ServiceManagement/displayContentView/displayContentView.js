define(function() {
    return function(controller) {
        var displayContentView = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
            "clipBounds": true,
            "isMaster": true,
            "id": "displayContentView",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "left": "0dp",
            "isModalContainer": false,
            "skin": "slFbox",
            "top": "0dp",
            "width": "100%"
        }, controller.args[0], "displayContentView"), extendConfig({}, controller.args[1], "displayContentView"), extendConfig({}, controller.args[2], "displayContentView"));
        displayContentView.setDefaultUnit(kony.flex.DP);
        var flxDisplayContent = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
            "clipBounds": true,
            "id": "flxDisplayContent",
            "isVisible": true,
            "layoutType": kony.flex.FLOW_VERTICAL,
            "left": "0dp",
            "isModalContainer": false,
            "skin": "sknFlxBgF8F9FABrE1E5EER3px",
            "top": "0dp",
            "width": "100%",
            "zIndex": 1
        }, controller.args[0], "flxDisplayContent"), extendConfig({}, controller.args[1], "flxDisplayContent"), extendConfig({}, controller.args[2], "flxDisplayContent"));
        flxDisplayContent.setDefaultUnit(kony.flex.DP);
        var flxDescriptionHeader = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "clipBounds": true,
            "height": "20dp",
            "id": "flxDescriptionHeader",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "left": "20dp",
            "isModalContainer": false,
            "skin": "slFbox",
            "top": "20dp",
            "width": "400dp",
            "zIndex": 1
        }, controller.args[0], "flxDescriptionHeader"), extendConfig({}, controller.args[1], "flxDescriptionHeader"), extendConfig({
            "hoverSkin": "sknCursor"
        }, controller.args[2], "flxDescriptionHeader"));
        flxDescriptionHeader.setDefaultUnit(kony.flex.DP);
        var lblDescriptionHeader = new kony.ui.Label(extendConfig({
            "centerY": "50%",
            "id": "lblDescriptionHeader",
            "isVisible": true,
            "left": "0dp",
            "skin": "sknLbl192b45LatoReg16px",
            "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmServiceManagement.DisplayContent\")",
            "width": "130dp",
            "zIndex": 1
        }, controller.args[0], "lblDescriptionHeader"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblDescriptionHeader"), extendConfig({}, controller.args[2], "lblDescriptionHeader"));
        var flxDropdownLanguages = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "centerY": "50%",
            "clipBounds": true,
            "height": "25dp",
            "id": "flxDropdownLanguages",
            "isVisible": true,
            "layoutType": kony.flex.FLOW_HORIZONTAL,
            "left": "130dp",
            "isModalContainer": false,
            "skin": "slFbox",
            "width": "180dp",
            "zIndex": 2
        }, controller.args[0], "flxDropdownLanguages"), extendConfig({}, controller.args[1], "flxDropdownLanguages"), extendConfig({}, controller.args[2], "flxDropdownLanguages"));
        flxDropdownLanguages.setDefaultUnit(kony.flex.DP);
        var lblDetailsSelectedLang = new kony.ui.Label(extendConfig({
            "centerY": "50%",
            "id": "lblDetailsSelectedLang",
            "isVisible": true,
            "left": "0dp",
            "skin": "sknLbl485C75LatoRegular13Px",
            "text": "English (United States)",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, controller.args[0], "lblDetailsSelectedLang"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_TOP_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblDetailsSelectedLang"), extendConfig({}, controller.args[2], "lblDetailsSelectedLang"));
        var lblIconArrowDescription = new kony.ui.Label(extendConfig({
            "centerY": "50%",
            "id": "lblIconArrowDescription",
            "isVisible": true,
            "left": "10dp",
            "skin": "sknicon15pxBlack",
            "text": "",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, controller.args[0], "lblIconArrowDescription"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblIconArrowDescription"), extendConfig({}, controller.args[2], "lblIconArrowDescription"));
        flxDropdownLanguages.add(lblDetailsSelectedLang, lblIconArrowDescription);
        flxDescriptionHeader.add(lblDescriptionHeader, flxDropdownLanguages);
        var flxDescriptionContent = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
            "bottom": "20dp",
            "clipBounds": true,
            "id": "flxDescriptionContent",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "left": "20dp",
            "isModalContainer": false,
            "skin": "sknServiceDetailsContent",
            "top": "15dp",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, controller.args[0], "flxDescriptionContent"), extendConfig({}, controller.args[1], "flxDescriptionContent"), extendConfig({}, controller.args[2], "flxDescriptionContent"));
        flxDescriptionContent.setDefaultUnit(kony.flex.DP);
        var lblDisplayNameHeader = new kony.ui.Label(extendConfig({
            "id": "lblDisplayNameHeader",
            "isVisible": true,
            "left": "0dp",
            "skin": "sknlblLato696c7312px",
            "i18n_text": "kony.i18n.getLocalizedString(\"i18n.permission.NAME\")",
            "top": "0dp",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, controller.args[0], "lblDisplayNameHeader"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblDisplayNameHeader"), extendConfig({}, controller.args[2], "lblDisplayNameHeader"));
        var lblDisplayNameValue = new kony.ui.Label(extendConfig({
            "id": "lblDisplayNameValue",
            "isVisible": true,
            "left": "0dp",
            "skin": "sknLatoSemibold485c7313px",
            "text": "ACH colletion",
            "top": "20dp",
            "width": "28%",
            "zIndex": 1
        }, controller.args[0], "lblDisplayNameValue"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_TOP_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblDisplayNameValue"), extendConfig({}, controller.args[2], "lblDisplayNameValue"));
        var lblDisplayDescHeader = new kony.ui.Label(extendConfig({
            "id": "lblDisplayDescHeader",
            "isVisible": true,
            "left": "31%",
            "skin": "sknlblLato696c7312px",
            "i18n_text": "kony.i18n.getLocalizedString(\"i18n.permission.DESCRIPTION\")",
            "top": "0dp",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, controller.args[0], "lblDisplayDescHeader"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblDisplayDescHeader"), extendConfig({}, controller.args[2], "lblDisplayDescHeader"));
        var lblDisplayDescValue = new kony.ui.Label(extendConfig({
            "id": "lblDisplayDescValue",
            "isVisible": true,
            "left": "31%",
            "skin": "sknLatoSemibold485c7313px",
            "text": "ACH colletion",
            "top": "20dp",
            "width": "60%",
            "zIndex": 1
        }, controller.args[0], "lblDisplayDescValue"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_TOP_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblDisplayDescValue"), extendConfig({}, controller.args[2], "lblDisplayDescValue"));
        flxDescriptionContent.add(lblDisplayNameHeader, lblDisplayNameValue, lblDisplayDescHeader, lblDisplayDescValue);
        var flxDetailsNoDisplayContent = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "bottom": "20dp",
            "clipBounds": true,
            "height": "50dp",
            "id": "flxDetailsNoDisplayContent",
            "isVisible": false,
            "layoutType": kony.flex.FREE_FORM,
            "left": "20dp",
            "isModalContainer": false,
            "skin": "sknServiceDetailsContent",
            "top": "15dp",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, controller.args[0], "flxDetailsNoDisplayContent"), extendConfig({}, controller.args[1], "flxDetailsNoDisplayContent"), extendConfig({}, controller.args[2], "flxDetailsNoDisplayContent"));
        flxDetailsNoDisplayContent.setDefaultUnit(kony.flex.DP);
        var lblDetailsNoDisplayContent = new kony.ui.Label(extendConfig({
            "centerX": "45%",
            "centerY": "50%",
            "id": "lblDetailsNoDisplayContent",
            "isVisible": true,
            "left": "0dp",
            "skin": "sknLblLato84939E12px",
            "text": "No display content avaialble.",
            "width": "60%",
            "zIndex": 1
        }, controller.args[0], "lblDetailsNoDisplayContent"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_CENTER,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblDetailsNoDisplayContent"), extendConfig({}, controller.args[2], "lblDetailsNoDisplayContent"));
        flxDetailsNoDisplayContent.add(lblDetailsNoDisplayContent);
        flxDisplayContent.add(flxDescriptionHeader, flxDescriptionContent, flxDetailsNoDisplayContent);
        var flxLanguages = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
            "clipBounds": true,
            "id": "flxLanguages",
            "isVisible": false,
            "layoutType": kony.flex.FREE_FORM,
            "left": "143dp",
            "isModalContainer": false,
            "skin": "slFbox",
            "top": "35px",
            "width": "170dp",
            "zIndex": 11
        }, controller.args[0], "flxLanguages"), extendConfig({}, controller.args[1], "flxLanguages"), extendConfig({}, controller.args[2], "flxLanguages"));
        flxLanguages.setDefaultUnit(kony.flex.DP);
        var EditLangList = new com.adminConsole.Products.productTypeFilterMenu(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
            "id": "EditLangList",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "left": "0dp",
            "masterType": constants.MASTER_TYPE_DEFAULT,
            "isModalContainer": false,
            "skin": "slFbox",
            "top": "5dp",
            "width": "100%",
            "zIndex": 5,
            "overrides": {
                "flxCheckboxInner": {
                    "height": "120px"
                },
                "flxFilterSort": {
                    "isVisible": false
                },
                "imgUpArrow": {
                    "src": "uparrow_2x.png"
                },
                "lblSeperator1": {
                    "isVisible": false
                },
                "segStatusFilterDropdown": {
                    "data": [{
                        "imgCheckBox": "checkbox.png",
                        "lblDescription": "Active"
                    }, {
                        "imgCheckBox": "checkbox.png",
                        "lblDescription": "Inactive"
                    }, {
                        "imgCheckBox": "checkbox.png",
                        "lblDescription": "Suspended"
                    }]
                }
            }
        }, controller.args[0], "EditLangList"), extendConfig({
            "overrides": {}
        }, controller.args[1], "EditLangList"), extendConfig({
            "overrides": {}
        }, controller.args[2], "EditLangList"));
        flxLanguages.add(EditLangList);
        displayContentView.add(flxDisplayContent, flxLanguages);
        return displayContentView;
    }
})