define("com/adminConsole/ServiceManagement/displayContentView/userdisplayContentViewController", function() {
    return {};
});
define("com/adminConsole/ServiceManagement/displayContentView/displayContentViewControllerActions", {
    /*
          This is an auto generated file and any modifications to it may result in corruption of the action sequence.
        */
});
define("com/adminConsole/ServiceManagement/displayContentView/displayContentViewController", ["com/adminConsole/ServiceManagement/displayContentView/userdisplayContentViewController", "com/adminConsole/ServiceManagement/displayContentView/displayContentViewControllerActions"], function() {
    var controller = require("com/adminConsole/ServiceManagement/displayContentView/userdisplayContentViewController");
    var actions = require("com/adminConsole/ServiceManagement/displayContentView/displayContentViewControllerActions");
    for (var key in actions) {
        controller[key] = actions[key];
    }
    return controller;
});
