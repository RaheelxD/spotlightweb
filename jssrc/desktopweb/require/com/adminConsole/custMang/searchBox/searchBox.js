define(function() {
    return function(controller) {
        var searchBox = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "clipBounds": true,
            "isMaster": true,
            "height": "40dp",
            "id": "searchBox",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "left": "0dp",
            "isModalContainer": false,
            "skin": "slFbox",
            "top": "0dp",
            "width": "508dp"
        }, controller.args[0], "searchBox"), extendConfig({}, controller.args[1], "searchBox"), extendConfig({}, controller.args[2], "searchBox"));
        searchBox.setDefaultUnit(kony.flex.DP);
        var flxSearchBoxContainer = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "clipBounds": true,
            "height": "40px",
            "id": "flxSearchBoxContainer",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "left": "0dp",
            "isModalContainer": false,
            "skin": "sknflxd5d9ddop100",
            "top": "0px",
            "width": "100%",
            "zIndex": 1
        }, controller.args[0], "flxSearchBoxContainer"), extendConfig({}, controller.args[1], "flxSearchBoxContainer"), extendConfig({}, controller.args[2], "flxSearchBoxContainer"));
        flxSearchBoxContainer.setDefaultUnit(kony.flex.DP);
        var flxIconBackground = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "centerY": "50%",
            "clipBounds": true,
            "height": "28dp",
            "id": "flxIconBackground",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "isModalContainer": false,
            "right": "7dp",
            "skin": "sknflxroundedborder003e75",
            "width": "28dp"
        }, controller.args[0], "flxIconBackground"), extendConfig({}, controller.args[1], "flxIconBackground"), extendConfig({}, controller.args[2], "flxIconBackground"));
        flxIconBackground.setDefaultUnit(kony.flex.DP);
        var lblIconSearch = new kony.ui.Label(extendConfig({
            "centerX": "50%",
            "centerY": "50%",
            "id": "lblIconSearch",
            "isVisible": true,
            "skin": "sknFontIconSearchImgFFFFFF",
            "i18n_text": "kony.i18n.getLocalizedString(\"i18n.userwidgetmodel.lblSearchIcon\")",
            "top": "0dp",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, controller.args[0], "lblIconSearch"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblIconSearch"), extendConfig({}, controller.args[2], "lblIconSearch"));
        flxIconBackground.add(lblIconSearch);
        var lblHorizSeperator = new kony.ui.Label(extendConfig({
            "centerY": "50%",
            "height": "26dp",
            "id": "lblHorizSeperator",
            "isVisible": true,
            "right": "42dp",
            "skin": "sknlblSeperatorD7D9E0",
            "text": ".",
            "top": "0",
            "width": "1dp"
        }, controller.args[0], "lblHorizSeperator"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblHorizSeperator"), extendConfig({}, controller.args[2], "lblHorizSeperator"));
        var tbxSearchBox = new kony.ui.TextBox2(extendConfig({
            "autoCapitalize": constants.TEXTBOX_AUTO_CAPITALIZE_NONE,
            "centerY": "50%",
            "height": "30px",
            "id": "tbxSearchBox",
            "isVisible": true,
            "keyBoardStyle": constants.TEXTBOX_KEY_BOARD_STYLE_DEFAULT,
            "left": "5px",
            "onTouchStart": controller.AS_TextField_cfab06859f0f43eca99495891ee290d5,
            "i18n_placeholder": "kony.i18n.getLocalizedString(\"i18n.frmCompanies.searchByComp\")",
            "right": "70px",
            "secureTextEntry": false,
            "skin": "sknSearchtbxConfigurationBundlesNormal",
            "textInputMode": constants.TEXTBOX_INPUT_MODE_ANY,
            "zIndex": 1
        }, controller.args[0], "tbxSearchBox"), extendConfig({
            "containerHeightMode": constants.TEXTBOX_FONT_METRICS_DRIVEN_HEIGHT,
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [2, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "tbxSearchBox"), extendConfig({
            "autoCorrect": false
        }, controller.args[2], "tbxSearchBox"));
        var flxClearSearch = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "clipBounds": true,
            "height": "100%",
            "id": "flxClearSearch",
            "isVisible": false,
            "layoutType": kony.flex.FREE_FORM,
            "isModalContainer": false,
            "right": "40dp",
            "skin": "sknCursor",
            "top": "0dp",
            "width": "30px",
            "zIndex": 1
        }, controller.args[0], "flxClearSearch"), extendConfig({}, controller.args[1], "flxClearSearch"), extendConfig({
            "hoverSkin": "sknCursor"
        }, controller.args[2], "flxClearSearch"));
        flxClearSearch.setDefaultUnit(kony.flex.DP);
        var lblIconClose = new kony.ui.Label(extendConfig({
            "centerY": "50%",
            "id": "lblIconClose",
            "isVisible": true,
            "left": "5px",
            "skin": "sknFontIconSearchCross16px",
            "i18n_text": "kony.i18n.getLocalizedString(\"i18n.userwidgetmodel.lblClearSearch\")",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, controller.args[0], "lblIconClose"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblIconClose"), extendConfig({}, controller.args[2], "lblIconClose"));
        flxClearSearch.add(lblIconClose);
        flxSearchBoxContainer.add(flxIconBackground, lblHorizSeperator, tbxSearchBox, flxClearSearch);
        searchBox.add(flxSearchBoxContainer);
        return searchBox;
    }
})