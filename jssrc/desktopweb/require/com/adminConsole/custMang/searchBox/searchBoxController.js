define("com/adminConsole/custMang/searchBox/usersearchBoxController", function() {
    return {
        /*
         * focus search box
         * @param: opt - true/false
         */
        setSearchBoxFocus: function(opt) {
            if (opt === true) {
                this.view.flxSearchBoxContainer.skin = "sknflxBgffffffBorder1293ccRadius30Px";
            } else if (opt === false) {
                this.view.flxSearchBoxContainer.skin = "sknflxd5d9ddop100";
            }
        },
        /*
         * clear search text
         */
        clearSearchBox: function() {
            this.setSearchBoxFocus(false);
            this.view.flxClearSearch.setVisibility(false);
            this.view.tbxSearchBox.text = "";
        },
    };
});
define("com/adminConsole/custMang/searchBox/searchBoxControllerActions", {
    /*
        This is an auto generated file and any modifications to it may result in corruption of the action sequence.
    */
});
define("com/adminConsole/custMang/searchBox/searchBoxController", ["com/adminConsole/custMang/searchBox/usersearchBoxController", "com/adminConsole/custMang/searchBox/searchBoxControllerActions"], function() {
    var controller = require("com/adminConsole/custMang/searchBox/usersearchBoxController");
    var actions = require("com/adminConsole/custMang/searchBox/searchBoxControllerActions");
    for (var key in actions) {
        controller[key] = actions[key];
    }
    return controller;
});
