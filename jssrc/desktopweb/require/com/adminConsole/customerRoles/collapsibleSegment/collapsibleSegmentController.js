define("com/adminConsole/customerRoles/collapsibleSegment/usercollapsibleSegmentController", function() {
    return {
        setFlowActions: function() {
            var scopeObj = this;
            this.view.flxToggle.onClick = function() {
                scopeObj.toggleContent();
            };
            this.view.lblFeatureName.onClick = function() {
                scopeObj.toggleContent();
            };
        },
        toggleContent: function() {
            if (this.view.flxHideDisplayContent.isVisible) {
                this.view.lblToggle.text = "\ue922";
                this.view.flxHideDisplayContent.setVisibility(false);
            } else {
                this.view.lblToggle.text = "\ue915";
                this.view.flxHideDisplayContent.setVisibility(true);
            }
        }
    };
});
define("com/adminConsole/customerRoles/collapsibleSegment/collapsibleSegmentControllerActions", {
    /*
        This is an auto generated file and any modifications to it may result in corruption of the action sequence.
    */
    AS_FlexContainer_j38cb78661d54e15a106fac8d207e4f1: function AS_FlexContainer_j38cb78661d54e15a106fac8d207e4f1(eventobject) {
        var self = this;
        this.setFlowActions();
    }
});
define("com/adminConsole/customerRoles/collapsibleSegment/collapsibleSegmentController", ["com/adminConsole/customerRoles/collapsibleSegment/usercollapsibleSegmentController", "com/adminConsole/customerRoles/collapsibleSegment/collapsibleSegmentControllerActions"], function() {
    var controller = require("com/adminConsole/customerRoles/collapsibleSegment/usercollapsibleSegmentController");
    var actions = require("com/adminConsole/customerRoles/collapsibleSegment/collapsibleSegmentControllerActions");
    for (var key in actions) {
        controller[key] = actions[key];
    }
    return controller;
});
