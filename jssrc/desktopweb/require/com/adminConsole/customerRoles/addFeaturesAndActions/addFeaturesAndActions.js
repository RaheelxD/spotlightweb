define(function() {
    return function(controller) {
        var addFeaturesAndActions = new kony.ui.FlexContainer(extendConfig({
            "clipBounds": true,
            "isMaster": true,
            "height": "100%",
            "id": "addFeaturesAndActions",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "left": "0dp",
            "isModalContainer": false,
            "skin": "slFbox",
            "top": "0dp",
            "width": "100%"
        }, controller.args[0], "addFeaturesAndActions"), extendConfig({}, controller.args[1], "addFeaturesAndActions"), extendConfig({}, controller.args[2], "addFeaturesAndActions"));
        addFeaturesAndActions.setDefaultUnit(kony.flex.DP);
        var flxAvailableOptionsWrapper = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "bottom": "0px",
            "clipBounds": true,
            "id": "flxAvailableOptionsWrapper",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "left": "0px",
            "isModalContainer": false,
            "skin": "slFbox",
            "top": "20px",
            "width": "50%",
            "zIndex": 1
        }, controller.args[0], "flxAvailableOptionsWrapper"), extendConfig({}, controller.args[1], "flxAvailableOptionsWrapper"), extendConfig({}, controller.args[2], "flxAvailableOptionsWrapper"));
        flxAvailableOptionsWrapper.setDefaultUnit(kony.flex.DP);
        var flxAvailableOptions = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "clipBounds": true,
            "height": "100%",
            "id": "flxAvailableOptions",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "left": "20px",
            "isModalContainer": false,
            "right": "15px",
            "skin": "slFbox",
            "top": "0px",
            "zIndex": 1
        }, controller.args[0], "flxAvailableOptions"), extendConfig({}, controller.args[1], "flxAvailableOptions"), extendConfig({}, controller.args[2], "flxAvailableOptions"));
        flxAvailableOptions.setDefaultUnit(kony.flex.DP);
        var lblAvailableOptionsHeading = new kony.ui.Label(extendConfig({
            "id": "lblAvailableOptionsHeading",
            "isVisible": true,
            "left": "0px",
            "skin": "sknlblLatoRegular00000013px",
            "i18n_text": "kony.i18n.getLocalizedString(\"i18n.userwidgetmodel.lblAvailableOptionsHeading\")",
            "top": 0,
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, controller.args[0], "lblAvailableOptionsHeading"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblAvailableOptionsHeading"), extendConfig({}, controller.args[2], "lblAvailableOptionsHeading"));
        var flxError = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "clipBounds": true,
            "height": "15dp",
            "id": "flxError",
            "isVisible": false,
            "layoutType": kony.flex.FREE_FORM,
            "left": "17%",
            "isModalContainer": false,
            "skin": "slFbox",
            "top": "0dp",
            "width": "40%",
            "zIndex": 1
        }, controller.args[0], "flxError"), extendConfig({}, controller.args[1], "flxError"), extendConfig({}, controller.args[2], "flxError"));
        flxError.setDefaultUnit(kony.flex.DP);
        var lblErrorIcon = new kony.ui.Label(extendConfig({
            "height": "15dp",
            "id": "lblErrorIcon",
            "isVisible": true,
            "left": "0dp",
            "skin": "sknErrorIcon",
            "text": "",
            "top": "0dp",
            "width": "15dp",
            "zIndex": 1
        }, controller.args[0], "lblErrorIcon"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblErrorIcon"), extendConfig({}, controller.args[2], "lblErrorIcon"));
        var lblErrorText = new kony.ui.Label(extendConfig({
            "height": "15dp",
            "id": "lblErrorText",
            "isVisible": true,
            "left": "15dp",
            "skin": "sknlblError",
            "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmCompanies.Select_atleast_one_account_to_proceed\")",
            "top": "0dp",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, controller.args[0], "lblErrorText"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblErrorText"), extendConfig({}, controller.args[2], "lblErrorText"));
        flxError.add(lblErrorIcon, lblErrorText);
        var btnSelectAll = new kony.ui.Button(extendConfig({
            "id": "btnSelectAll",
            "isVisible": true,
            "right": "0px",
            "skin": "contextuallinks",
            "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmGroupsController.Add_All\")",
            "top": 0,
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, controller.args[0], "btnSelectAll"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "displayText": true,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "btnSelectAll"), extendConfig({}, controller.args[2], "btnSelectAll"));
        var flxSegAvailableOptions = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "bottom": "15dp",
            "clipBounds": true,
            "id": "flxSegAvailableOptions",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "left": "0dp",
            "isModalContainer": false,
            "skin": "sknflxffffffBorderE1E5EERadius3px",
            "top": "30dp",
            "width": "100%"
        }, controller.args[0], "flxSegAvailableOptions"), extendConfig({}, controller.args[1], "flxSegAvailableOptions"), extendConfig({}, controller.args[2], "flxSegAvailableOptions"));
        flxSegAvailableOptions.setDefaultUnit(kony.flex.DP);
        var flxSearch = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "clipBounds": true,
            "height": "62px",
            "id": "flxSearch",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "left": "0px",
            "isModalContainer": false,
            "right": "0px",
            "skin": "sknflxf5f6f8Op100",
            "top": "0px",
            "width": "100%",
            "zIndex": 1
        }, controller.args[0], "flxSearch"), extendConfig({}, controller.args[1], "flxSearch"), extendConfig({}, controller.args[2], "flxSearch"));
        flxSearch.setDefaultUnit(kony.flex.DP);
        var flxSearchCont = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "centerY": "50%",
            "clipBounds": true,
            "focusSkin": "sknflxBgffffffBorder1293ccRadius30Px",
            "height": "40px",
            "id": "flxSearchCont",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "left": "10dp",
            "isModalContainer": false,
            "right": "40dp",
            "skin": "sknflxd5d9ddop100",
            "zIndex": 1
        }, controller.args[0], "flxSearchCont"), extendConfig({}, controller.args[1], "flxSearchCont"), extendConfig({}, controller.args[2], "flxSearchCont"));
        flxSearchCont.setDefaultUnit(kony.flex.DP);
        var flxSearchImage = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "clipBounds": true,
            "height": "100%",
            "id": "flxSearchImage",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "left": "0dp",
            "isModalContainer": false,
            "skin": "slFbox",
            "top": "0dp",
            "width": "35px",
            "zIndex": 1
        }, controller.args[0], "flxSearchImage"), extendConfig({}, controller.args[1], "flxSearchImage"), extendConfig({}, controller.args[2], "flxSearchImage"));
        flxSearchImage.setDefaultUnit(kony.flex.DP);
        var lblSearchIcon = new kony.ui.Label(extendConfig({
            "centerY": "50%",
            "height": "20dp",
            "id": "lblSearchIcon",
            "isVisible": true,
            "left": "14dp",
            "skin": "sknFontIconSearchImg20px",
            "i18n_text": "kony.i18n.getLocalizedString(\"i18n.userwidgetmodel.lblSearchIcon\")",
            "width": "20dp",
            "zIndex": 1
        }, controller.args[0], "lblSearchIcon"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblSearchIcon"), extendConfig({}, controller.args[2], "lblSearchIcon"));
        flxSearchImage.add(lblSearchIcon);
        var tbxSearchAvailableItems = new kony.ui.TextBox2(extendConfig({
            "autoCapitalize": constants.TEXTBOX_AUTO_CAPITALIZE_NONE,
            "centerY": "50%",
            "height": "100%",
            "id": "tbxSearchAvailableItems",
            "isVisible": true,
            "keyBoardStyle": constants.TEXTBOX_KEY_BOARD_STYLE_DEFAULT,
            "left": "35dp",
            "i18n_placeholder": "kony.i18n.getLocalizedString(\"i18n.permission.search\")",
            "right": "35px",
            "secureTextEntry": false,
            "skin": "skntbxffffffNoBorderlato35475f14px",
            "textInputMode": constants.TEXTBOX_INPUT_MODE_ANY,
            "zIndex": 1
        }, controller.args[0], "tbxSearchAvailableItems"), extendConfig({
            "containerHeightMode": constants.TEXTBOX_FONT_METRICS_DRIVEN_HEIGHT,
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [3, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "tbxSearchAvailableItems"), extendConfig({
            "autoCorrect": false
        }, controller.args[2], "tbxSearchAvailableItems"));
        var flxClearSearch = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "clipBounds": true,
            "height": "100%",
            "id": "flxClearSearch",
            "isVisible": false,
            "layoutType": kony.flex.FREE_FORM,
            "isModalContainer": false,
            "right": "0px",
            "skin": "slFbox",
            "top": "0dp",
            "width": "35px",
            "zIndex": 1
        }, controller.args[0], "flxClearSearch"), extendConfig({}, controller.args[1], "flxClearSearch"), extendConfig({
            "hoverSkin": "sknCursor"
        }, controller.args[2], "flxClearSearch"));
        flxClearSearch.setDefaultUnit(kony.flex.DP);
        var lblIconClose = new kony.ui.Label(extendConfig({
            "centerY": "49%",
            "id": "lblIconClose",
            "isVisible": true,
            "left": "4px",
            "skin": "sknFontIconSearchCross20px",
            "i18n_text": "kony.i18n.getLocalizedString(\"i18n.userwidgetmodel.lblClearSearch\")",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, controller.args[0], "lblIconClose"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblIconClose"), extendConfig({}, controller.args[2], "lblIconClose"));
        flxClearSearch.add(lblIconClose);
        flxSearchCont.add(flxSearchImage, tbxSearchAvailableItems, flxClearSearch);
        var flxFilteredSearch = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "centerY": "50%",
            "clipBounds": true,
            "height": "35dp",
            "id": "flxFilteredSearch",
            "isVisible": false,
            "layoutType": kony.flex.FLOW_VERTICAL,
            "left": "10dp",
            "isModalContainer": false,
            "right": "10dp",
            "skin": "slFbox",
            "zIndex": 1
        }, controller.args[0], "flxFilteredSearch"), extendConfig({}, controller.args[1], "flxFilteredSearch"), extendConfig({}, controller.args[2], "flxFilteredSearch"));
        flxFilteredSearch.setDefaultUnit(kony.flex.DP);
        var flxSearchFilterTextBox = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "clipBounds": true,
            "height": "35dp",
            "id": "flxSearchFilterTextBox",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "left": "0dp",
            "isModalContainer": false,
            "skin": "slFbox",
            "top": "0dp",
            "width": "100%"
        }, controller.args[0], "flxSearchFilterTextBox"), extendConfig({}, controller.args[1], "flxSearchFilterTextBox"), extendConfig({}, controller.args[2], "flxSearchFilterTextBox"));
        flxSearchFilterTextBox.setDefaultUnit(kony.flex.DP);
        var flxSelectedType = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "clipBounds": true,
            "height": "35dp",
            "id": "flxSelectedType",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "left": "0dp",
            "isModalContainer": false,
            "skin": "sknflxd5d9ddop100",
            "top": "0dp",
            "width": "150dp",
            "zIndex": 1
        }, controller.args[0], "flxSelectedType"), extendConfig({}, controller.args[1], "flxSelectedType"), extendConfig({
            "hoverSkin": "sknflxFFFFFFBrd5d9dd1pxR30pxHover"
        }, controller.args[2], "flxSelectedType"));
        flxSelectedType.setDefaultUnit(kony.flex.DP);
        var lblSelSearchType = new kony.ui.Label(extendConfig({
            "centerY": "50%",
            "id": "lblSelSearchType",
            "isVisible": true,
            "left": "15dp",
            "right": "50dp",
            "skin": "sknLbl485C75Font12pxKA",
            "text": "Membership id",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, controller.args[0], "lblSelSearchType"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblSelSearchType"), extendConfig({}, controller.args[2], "lblSelSearchType"));
        var lblIconDropDown = new kony.ui.Label(extendConfig({
            "id": "lblIconDropDown",
            "isVisible": true,
            "right": "30dp",
            "skin": "sknIcon12pxBlack",
            "text": "",
            "top": "10dp",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, controller.args[0], "lblIconDropDown"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblIconDropDown"), extendConfig({}, controller.args[2], "lblIconDropDown"));
        flxSelectedType.add(lblSelSearchType, lblIconDropDown);
        var tbxFilterSearch = new kony.ui.TextBox2(extendConfig({
            "autoCapitalize": constants.TEXTBOX_AUTO_CAPITALIZE_NONE,
            "focusSkin": "defTextBoxFocus",
            "height": "35dp",
            "id": "tbxFilterSearch",
            "isVisible": true,
            "keyBoardStyle": constants.TEXTBOX_KEY_BOARD_STYLE_DEFAULT,
            "left": "130dp",
            "i18n_placeholder": "kony.i18n.getLocalizedString(\"i18n.frmCompanies.Search_accounts_by\")",
            "right": "40dp",
            "secureTextEntry": false,
            "skin": "sknTbx13pxBor1pxccced1",
            "textInputMode": constants.TEXTBOX_INPUT_MODE_ANY,
            "top": "0dp",
            "zIndex": 1
        }, controller.args[0], "tbxFilterSearch"), extendConfig({
            "containerHeightMode": constants.TEXTBOX_FONT_METRICS_DRIVEN_HEIGHT,
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [3, 0, 1, 0],
            "paddingInPixel": false
        }, controller.args[1], "tbxFilterSearch"), extendConfig({
            "autoCorrect": false,
            "placeholderSkin": "defTextBoxPlaceholder"
        }, controller.args[2], "tbxFilterSearch"));
        var flxSearchClick = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "clipBounds": true,
            "height": "35dp",
            "id": "flxSearchClick",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "isModalContainer": false,
            "right": "0dp",
            "skin": "sknFlxRightRounded20pxccced1Bor1px",
            "top": "0dp",
            "width": "40dp",
            "zIndex": 1
        }, controller.args[0], "flxSearchClick"), extendConfig({}, controller.args[1], "flxSearchClick"), extendConfig({
            "hoverSkin": "sknFlxRightRounded20pxBor1pxHov"
        }, controller.args[2], "flxSearchClick"));
        flxSearchClick.setDefaultUnit(kony.flex.DP);
        var lblIconSearchBtn = new kony.ui.Label(extendConfig({
            "centerY": "50%",
            "id": "lblIconSearchBtn",
            "isVisible": true,
            "right": "12dp",
            "skin": "sknFontIconSearchImg20px",
            "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmGroups.fontIconSearchimg\")",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, controller.args[0], "lblIconSearchBtn"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblIconSearchBtn"), extendConfig({}, controller.args[2], "lblIconSearchBtn"));
        flxSearchClick.add(lblIconSearchBtn);
        flxSearchFilterTextBox.add(flxSelectedType, tbxFilterSearch, flxSearchClick);
        flxFilteredSearch.add(flxSearchFilterTextBox);
        var fontIconFeatureTypeFilter = new kony.ui.Label(extendConfig({
            "centerY": "50%",
            "id": "fontIconFeatureTypeFilter",
            "isVisible": true,
            "right": "15dp",
            "skin": "sknIcon15px",
            "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmAlertsManagement.lblIconChannels\")",
            "top": "0dp",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, controller.args[0], "fontIconFeatureTypeFilter"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "fontIconFeatureTypeFilter"), extendConfig({
            "hoverSkin": "sknlblCursorFont"
        }, controller.args[2], "fontIconFeatureTypeFilter"));
        var lblFeatureHeaderLine = new kony.ui.Label(extendConfig({
            "bottom": "0dp",
            "height": "1dp",
            "id": "lblFeatureHeaderLine",
            "isVisible": true,
            "left": "0dp",
            "right": "0dp",
            "skin": "sknlblSeperatorD7D9E0",
            "text": "Label",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, controller.args[0], "lblFeatureHeaderLine"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblFeatureHeaderLine"), extendConfig({}, controller.args[2], "lblFeatureHeaderLine"));
        flxSearch.add(flxSearchCont, flxFilteredSearch, fontIconFeatureTypeFilter, lblFeatureHeaderLine);
        var flxSegSearchType = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "bottom": "5dp",
            "clipBounds": true,
            "height": "80dp",
            "id": "flxSegSearchType",
            "isVisible": false,
            "layoutType": kony.flex.FREE_FORM,
            "left": "20dp",
            "isModalContainer": false,
            "skin": "sknflxffffffop100Bordercbcdd1Radius3px",
            "top": "48dp",
            "width": "125dp",
            "zIndex": 2
        }, controller.args[0], "flxSegSearchType"), extendConfig({}, controller.args[1], "flxSegSearchType"), extendConfig({}, controller.args[2], "flxSegSearchType"));
        flxSegSearchType.setDefaultUnit(kony.flex.DP);
        var segSearchType = new kony.ui.SegmentedUI2(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
            "data": [{
                "lblValue": "Account Id"
            }, {
                "lblValue": "Tax Number"
            }],
            "groupCells": false,
            "id": "segSearchType",
            "isVisible": true,
            "left": "10dp",
            "needPageIndicator": true,
            "pageOffDotImage": "pageoffdot.png",
            "pageOnDotImage": "pageondot.png",
            "retainSelection": false,
            "right": "10dp",
            "rowFocusSkin": "seg2Focus",
            "rowSkin": "seg2Normal",
            "rowTemplate": "flxDropDown",
            "sectionHeaderSkin": "sliPhoneSegmentHeader",
            "selectionBehavior": constants.SEGUI_DEFAULT_BEHAVIOR,
            "separatorColor": "aaaaaa47",
            "separatorRequired": true,
            "separatorThickness": 1,
            "showScrollbars": false,
            "top": "0dp",
            "viewType": constants.SEGUI_VIEW_TYPE_TABLEVIEW,
            "widgetDataMap": {
                "flxDropDown": "flxDropDown",
                "flxDropDownContainer": "flxDropDownContainer",
                "lblValue": "lblValue"
            },
            "zIndex": 1
        }, controller.args[0], "segSearchType"), extendConfig({
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "segSearchType"), extendConfig({}, controller.args[2], "segSearchType"));
        flxSegSearchType.add(segSearchType);
        var flxAddOptionsSegment = new kony.ui.FlexScrollContainer(extendConfig({
            "allowHorizontalBounce": false,
            "allowVerticalBounce": true,
            "bottom": "15px",
            "bounces": true,
            "clipBounds": true,
            "enableScrolling": true,
            "horizontalScrollIndicator": true,
            "id": "flxAddOptionsSegment",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "left": "0px",
            "pagingEnabled": false,
            "scrollDirection": kony.flex.SCROLL_VERTICAL,
            "skin": "slFSbox",
            "top": "64px",
            "verticalScrollIndicator": true,
            "width": "100%",
            "zIndex": 1
        }, controller.args[0], "flxAddOptionsSegment"), extendConfig({}, controller.args[1], "flxAddOptionsSegment"), extendConfig({}, controller.args[2], "flxAddOptionsSegment"));
        flxAddOptionsSegment.setDefaultUnit(kony.flex.DP);
        var segAddOptions = new kony.ui.SegmentedUI2(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
            "data": [{
                "imgFeatureCheckbox": "checkboxnormal.png",
                "lblFeatureName": "Label",
                "lblFeatureStatusValue": "Active",
                "lblIconSelectedArrow": "",
                "lblIconStatus": "kony.i18n.getLocalizedString(\"i18n.frmAlertsManagement.lblViewAlertStatusKey\")"
            }, {
                "imgFeatureCheckbox": "checkboxnormal.png",
                "lblFeatureName": "Label",
                "lblFeatureStatusValue": "Active",
                "lblIconSelectedArrow": "",
                "lblIconStatus": "kony.i18n.getLocalizedString(\"i18n.frmAlertsManagement.lblViewAlertStatusKey\")"
            }, {
                "imgFeatureCheckbox": "checkboxnormal.png",
                "lblFeatureName": "Label",
                "lblFeatureStatusValue": "Active",
                "lblIconSelectedArrow": "",
                "lblIconStatus": "kony.i18n.getLocalizedString(\"i18n.frmAlertsManagement.lblViewAlertStatusKey\")"
            }, {
                "imgFeatureCheckbox": "checkboxnormal.png",
                "lblFeatureName": "Label",
                "lblFeatureStatusValue": "Active",
                "lblIconSelectedArrow": "",
                "lblIconStatus": "kony.i18n.getLocalizedString(\"i18n.frmAlertsManagement.lblViewAlertStatusKey\")"
            }, {
                "imgFeatureCheckbox": "checkboxnormal.png",
                "lblFeatureName": "Label",
                "lblFeatureStatusValue": "Active",
                "lblIconSelectedArrow": "",
                "lblIconStatus": "kony.i18n.getLocalizedString(\"i18n.frmAlertsManagement.lblViewAlertStatusKey\")"
            }, {
                "imgFeatureCheckbox": "checkboxnormal.png",
                "lblFeatureName": "Label",
                "lblFeatureStatusValue": "Active",
                "lblIconSelectedArrow": "",
                "lblIconStatus": "kony.i18n.getLocalizedString(\"i18n.frmAlertsManagement.lblViewAlertStatusKey\")"
            }, {
                "imgFeatureCheckbox": "checkboxnormal.png",
                "lblFeatureName": "Label",
                "lblFeatureStatusValue": "Active",
                "lblIconSelectedArrow": "",
                "lblIconStatus": "kony.i18n.getLocalizedString(\"i18n.frmAlertsManagement.lblViewAlertStatusKey\")"
            }, {
                "imgFeatureCheckbox": "checkboxnormal.png",
                "lblFeatureName": "Label",
                "lblFeatureStatusValue": "Active",
                "lblIconSelectedArrow": "",
                "lblIconStatus": "kony.i18n.getLocalizedString(\"i18n.frmAlertsManagement.lblViewAlertStatusKey\")"
            }, {
                "imgFeatureCheckbox": "checkboxnormal.png",
                "lblFeatureName": "Label",
                "lblFeatureStatusValue": "Active",
                "lblIconSelectedArrow": "",
                "lblIconStatus": "kony.i18n.getLocalizedString(\"i18n.frmAlertsManagement.lblViewAlertStatusKey\")"
            }, {
                "imgFeatureCheckbox": "checkboxnormal.png",
                "lblFeatureName": "Label",
                "lblFeatureStatusValue": "Active",
                "lblIconSelectedArrow": "",
                "lblIconStatus": "kony.i18n.getLocalizedString(\"i18n.frmAlertsManagement.lblViewAlertStatusKey\")"
            }],
            "groupCells": false,
            "id": "segAddOptions",
            "isVisible": true,
            "left": "0dp",
            "needPageIndicator": true,
            "pageOffDotImage": "pageoffdot.png",
            "pageOnDotImage": "pageondot.png",
            "retainSelection": false,
            "right": "0dp",
            "rowFocusSkin": "seg2Focus",
            "rowSkin": "seg2Normal",
            "rowTemplate": "flxCustRolesFeatures",
            "sectionHeaderSkin": "sliPhoneSegmentHeader",
            "selectionBehavior": constants.SEGUI_DEFAULT_BEHAVIOR,
            "separatorRequired": false,
            "showScrollbars": true,
            "top": "0dp",
            "viewType": constants.SEGUI_VIEW_TYPE_TABLEVIEW,
            "widgetDataMap": {
                "flxCustRolesFeatures": "flxCustRolesFeatures",
                "flxFeatureCheckbox": "flxFeatureCheckbox",
                "flxFeatureNameContainer": "flxFeatureNameContainer",
                "flxFeatureStatus": "flxFeatureStatus",
                "flxSelectedArrow": "flxSelectedArrow",
                "imgFeatureCheckbox": "imgFeatureCheckbox",
                "lblFeatureName": "lblFeatureName",
                "lblFeatureStatusValue": "lblFeatureStatusValue",
                "lblIconSelectedArrow": "lblIconSelectedArrow",
                "lblIconStatus": "lblIconStatus"
            },
            "zIndex": 1
        }, controller.args[0], "segAddOptions"), extendConfig({
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "segAddOptions"), extendConfig({}, controller.args[2], "segAddOptions"));
        var rtxAvailableOptionsMessage = new kony.ui.RichText(extendConfig({
            "centerX": "50%",
            "centerY": "50%",
            "id": "rtxAvailableOptionsMessage",
            "isVisible": false,
            "skin": "sknRtxLato84939e12Px",
            "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmCSR.richtextNoResult\")",
            "width": "80%",
            "zIndex": 1
        }, controller.args[0], "rtxAvailableOptionsMessage"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_CENTER,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "rtxAvailableOptionsMessage"), extendConfig({}, controller.args[2], "rtxAvailableOptionsMessage"));
        var flxAccountSearchLoading = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "clipBounds": true,
            "height": "100%",
            "id": "flxAccountSearchLoading",
            "isVisible": false,
            "layoutType": kony.flex.FREE_FORM,
            "left": "0px",
            "isModalContainer": false,
            "right": "0px",
            "skin": "sknLoadingBlur",
            "top": "0px",
            "width": "100%",
            "zIndex": 10
        }, controller.args[0], "flxAccountSearchLoading"), extendConfig({}, controller.args[1], "flxAccountSearchLoading"), extendConfig({}, controller.args[2], "flxAccountSearchLoading"));
        flxAccountSearchLoading.setDefaultUnit(kony.flex.DP);
        var flxImageContainerAccountSearch = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "centerX": "50%",
            "centerY": "50%",
            "clipBounds": true,
            "height": "75px",
            "id": "flxImageContainerAccountSearch",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "isModalContainer": false,
            "skin": "slFbox",
            "top": "250px",
            "width": "75px",
            "zIndex": 1
        }, controller.args[0], "flxImageContainerAccountSearch"), extendConfig({}, controller.args[1], "flxImageContainerAccountSearch"), extendConfig({}, controller.args[2], "flxImageContainerAccountSearch"));
        flxImageContainerAccountSearch.setDefaultUnit(kony.flex.DP);
        var imgLoadingAccountSearch = new kony.ui.Image2(extendConfig({
            "centerX": "50%",
            "centerY": "50%",
            "height": "50px",
            "id": "imgLoadingAccountSearch",
            "isVisible": true,
            "skin": "slImage",
            "src": "loadingscreenimage.gif",
            "width": "50px",
            "zIndex": 1
        }, controller.args[0], "imgLoadingAccountSearch"), extendConfig({
            "imageScaleMode": constants.IMAGE_SCALE_MODE_MAINTAIN_ASPECT_RATIO,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "imgLoadingAccountSearch"), extendConfig({}, controller.args[2], "imgLoadingAccountSearch"));
        flxImageContainerAccountSearch.add(imgLoadingAccountSearch);
        flxAccountSearchLoading.add(flxImageContainerAccountSearch);
        flxAddOptionsSegment.add(segAddOptions, rtxAvailableOptionsMessage, flxAccountSearchLoading);
        flxSegAvailableOptions.add(flxSearch, flxSegSearchType, flxAddOptionsSegment);
        var flxFeatureTypeFilter = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
            "clipBounds": true,
            "id": "flxFeatureTypeFilter",
            "isVisible": false,
            "layoutType": kony.flex.FREE_FORM,
            "left": "237px",
            "isModalContainer": false,
            "skin": "slFbox",
            "top": "65px",
            "width": "120px",
            "zIndex": 5
        }, controller.args[0], "flxFeatureTypeFilter"), extendConfig({}, controller.args[1], "flxFeatureTypeFilter"), extendConfig({}, controller.args[2], "flxFeatureTypeFilter"));
        flxFeatureTypeFilter.setDefaultUnit(kony.flex.DP);
        var FeatureTypeFilterMenu = new com.adminConsole.common.statusFilterMenu(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
            "id": "FeatureTypeFilterMenu",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "left": "0dp",
            "masterType": constants.MASTER_TYPE_DEFAULT,
            "isModalContainer": false,
            "skin": "slFbox",
            "top": "5dp",
            "width": "100%",
            "zIndex": 5,
            "overrides": {
                "imgUpArrow": {
                    "right": "10dp",
                    "src": "uparrow_2x.png"
                },
                "segStatusFilterDropdown": {
                    "data": [{
                        "imgCheckBox": "checkbox.png",
                        "lblDescription": "Active"
                    }, {
                        "imgCheckBox": "checkbox.png",
                        "lblDescription": "Inactive"
                    }, {
                        "imgCheckBox": "checkbox.png",
                        "lblDescription": "Suspended"
                    }],
                    "left": "0dp",
                    "top": "5dp"
                }
            }
        }, controller.args[0], "FeatureTypeFilterMenu"), extendConfig({
            "overrides": {}
        }, controller.args[1], "FeatureTypeFilterMenu"), extendConfig({
            "overrides": {}
        }, controller.args[2], "FeatureTypeFilterMenu"));
        flxFeatureTypeFilter.add(FeatureTypeFilterMenu);
        flxAvailableOptions.add(lblAvailableOptionsHeading, flxError, btnSelectAll, flxSegAvailableOptions, flxFeatureTypeFilter);
        flxAvailableOptionsWrapper.add(flxAvailableOptions);
        var flxSelectedOptionsWrapper = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "bottom": "0px",
            "clipBounds": true,
            "id": "flxSelectedOptionsWrapper",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "isModalContainer": false,
            "right": "0px",
            "skin": "slFbox",
            "top": "20px",
            "width": "50%",
            "zIndex": 1
        }, controller.args[0], "flxSelectedOptionsWrapper"), extendConfig({}, controller.args[1], "flxSelectedOptionsWrapper"), extendConfig({}, controller.args[2], "flxSelectedOptionsWrapper"));
        flxSelectedOptionsWrapper.setDefaultUnit(kony.flex.DP);
        var flxSelectedOptions = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "bottom": "0dp",
            "clipBounds": true,
            "id": "flxSelectedOptions",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "left": "15px",
            "isModalContainer": false,
            "right": "20px",
            "skin": "slFbox",
            "top": "0px",
            "zIndex": 1
        }, controller.args[0], "flxSelectedOptions"), extendConfig({}, controller.args[1], "flxSelectedOptions"), extendConfig({}, controller.args[2], "flxSelectedOptions"));
        flxSelectedOptions.setDefaultUnit(kony.flex.DP);
        var lblSelectedOption = new kony.ui.Label(extendConfig({
            "id": "lblSelectedOption",
            "isVisible": true,
            "left": "0px",
            "skin": "sknlblLatoRegular00000013px",
            "i18n_text": "kony.i18n.getLocalizedString(\"i18n.permission.SelectedUsers\")",
            "top": "0px",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, controller.args[0], "lblSelectedOption"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblSelectedOption"), extendConfig({}, controller.args[2], "lblSelectedOption"));
        var btnResetActions = new kony.ui.Button(extendConfig({
            "id": "btnResetActions",
            "isVisible": true,
            "right": "0px",
            "skin": "sknBtnLato11ABEB11Px",
            "i18n_text": "kony.i18n.getLocalizedString(\"i18n.Common.button.Reset\")",
            "top": "0px",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, controller.args[0], "btnResetActions"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "displayText": true,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "btnResetActions"), extendConfig({}, controller.args[2], "btnResetActions"));
        var flxSegSelectedOptions = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "bottom": "15px",
            "clipBounds": true,
            "id": "flxSegSelectedOptions",
            "isVisible": true,
            "layoutType": kony.flex.FLOW_VERTICAL,
            "left": "0dp",
            "isModalContainer": false,
            "skin": "sknflxffffffBorderE1E5EERadius3px",
            "top": "30dp",
            "width": "100%",
            "zIndex": 1
        }, controller.args[0], "flxSegSelectedOptions"), extendConfig({}, controller.args[1], "flxSegSelectedOptions"), extendConfig({}, controller.args[2], "flxSegSelectedOptions"));
        flxSegSelectedOptions.setDefaultUnit(kony.flex.DP);
        var flxActionDetailsContainer = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
            "clipBounds": true,
            "id": "flxActionDetailsContainer",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "left": "0dp",
            "isModalContainer": false,
            "skin": "slFboxBGF9F9F9",
            "top": "0dp",
            "width": "100%",
            "zIndex": 1
        }, controller.args[0], "flxActionDetailsContainer"), extendConfig({}, controller.args[1], "flxActionDetailsContainer"), extendConfig({}, controller.args[2], "flxActionDetailsContainer"));
        flxActionDetailsContainer.setDefaultUnit(kony.flex.DP);
        var flxNameDetailsBtnCont = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
            "bottom": "10dp",
            "clipBounds": true,
            "id": "flxNameDetailsBtnCont",
            "isVisible": true,
            "layoutType": kony.flex.FLOW_VERTICAL,
            "left": "0dp",
            "isModalContainer": false,
            "skin": "slFbox",
            "top": "0dp",
            "width": "100%"
        }, controller.args[0], "flxNameDetailsBtnCont"), extendConfig({}, controller.args[1], "flxNameDetailsBtnCont"), extendConfig({}, controller.args[2], "flxNameDetailsBtnCont"));
        flxNameDetailsBtnCont.setDefaultUnit(kony.flex.DP);
        var lblFeatureNameValue = new kony.ui.Label(extendConfig({
            "id": "lblFeatureNameValue",
            "isVisible": true,
            "left": "12dp",
            "skin": "sknLbl192B45LatoRegular14px",
            "text": "Label",
            "top": "13dp",
            "width": "90%",
            "zIndex": 1
        }, controller.args[0], "lblFeatureNameValue"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblFeatureNameValue"), extendConfig({}, controller.args[2], "lblFeatureNameValue"));
        var btnViewActions = new kony.ui.Button(extendConfig({
            "id": "btnViewActions",
            "isVisible": true,
            "left": "12dp",
            "skin": "sknBtnLato117eb013Px",
            "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmGroupsController.viewDetails\")",
            "top": "6dp",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, controller.args[0], "btnViewActions"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "displayText": true,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "btnViewActions"), extendConfig({}, controller.args[2], "btnViewActions"));
        flxNameDetailsBtnCont.add(lblFeatureNameValue, btnViewActions);
        var lblActionHeaderLine = new kony.ui.Label(extendConfig({
            "bottom": "0dp",
            "height": "1dp",
            "id": "lblActionHeaderLine",
            "isVisible": true,
            "left": "0dp",
            "right": "0dp",
            "skin": "sknlblSeperatorD7D9E0",
            "text": "Label",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, controller.args[0], "lblActionHeaderLine"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblActionHeaderLine"), extendConfig({}, controller.args[2], "lblActionHeaderLine"));
        flxActionDetailsContainer.add(flxNameDetailsBtnCont, lblActionHeaderLine);
        var flxActionsMessage = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "clipBounds": true,
            "height": "50dp",
            "id": "flxActionsMessage",
            "isVisible": false,
            "layoutType": kony.flex.FREE_FORM,
            "left": "0dp",
            "isModalContainer": false,
            "skin": "slFbox",
            "top": "0dp",
            "width": "100%",
            "zIndex": 5
        }, controller.args[0], "flxActionsMessage"), extendConfig({}, controller.args[1], "flxActionsMessage"), extendConfig({}, controller.args[2], "flxActionsMessage"));
        flxActionsMessage.setDefaultUnit(kony.flex.DP);
        var flxActionMessageBorder = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "clipBounds": true,
            "height": "40dp",
            "id": "flxActionMessageBorder",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "left": "12dp",
            "isModalContainer": false,
            "right": "20dp",
            "skin": "sknFlxBgFFFFFFBor4A77A0S1pxR1px",
            "top": "10dp",
            "zIndex": 1
        }, controller.args[0], "flxActionMessageBorder"), extendConfig({}, controller.args[1], "flxActionMessageBorder"), extendConfig({}, controller.args[2], "flxActionMessageBorder"));
        flxActionMessageBorder.setDefaultUnit(kony.flex.DP);
        var flxIconContainer = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "clipBounds": true,
            "height": "40dp",
            "id": "flxIconContainer",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "left": "0dp",
            "isModalContainer": false,
            "right": "20dp",
            "skin": "sknFlxBg4A77A0",
            "top": "0dp",
            "width": "40dp",
            "zIndex": 1
        }, controller.args[0], "flxIconContainer"), extendConfig({}, controller.args[1], "flxIconContainer"), extendConfig({}, controller.args[2], "flxIconContainer"));
        flxIconContainer.setDefaultUnit(kony.flex.DP);
        var lblActionsMsgIcon = new kony.ui.Label(extendConfig({
            "centerX": "50%",
            "centerY": "50%",
            "height": "15dp",
            "id": "lblActionsMsgIcon",
            "isVisible": true,
            "skin": "sknIcon13pxWhite",
            "text": "",
            "width": "15dp",
            "zIndex": 1
        }, controller.args[0], "lblActionsMsgIcon"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_TOP_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblActionsMsgIcon"), extendConfig({}, controller.args[2], "lblActionsMsgIcon"));
        flxIconContainer.add(lblActionsMsgIcon);
        var lblActionMsg = new kony.ui.Label(extendConfig({
            "id": "lblActionMsg",
            "isVisible": true,
            "left": "50px",
            "right": "10dp",
            "skin": "sknlblLatoRegular00000013px",
            "text": "This feature will be available once it changes to active state.",
            "top": "5dp",
            "zIndex": 1
        }, controller.args[0], "lblActionMsg"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_TOP_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblActionMsg"), extendConfig({}, controller.args[2], "lblActionMsg"));
        flxActionMessageBorder.add(flxIconContainer, lblActionMsg);
        flxActionsMessage.add(flxActionMessageBorder);
        var flxSegActionsList = new kony.ui.FlexScrollContainer(extendConfig({
            "allowHorizontalBounce": false,
            "allowVerticalBounce": true,
            "bounces": true,
            "clipBounds": true,
            "enableScrolling": true,
            "height": "200dp",
            "horizontalScrollIndicator": true,
            "id": "flxSegActionsList",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "left": "0dp",
            "pagingEnabled": false,
            "scrollDirection": kony.flex.SCROLL_VERTICAL,
            "skin": "slFSbox",
            "top": "0dp",
            "verticalScrollIndicator": true,
            "width": "100%"
        }, controller.args[0], "flxSegActionsList"), extendConfig({}, controller.args[1], "flxSegActionsList"), extendConfig({}, controller.args[2], "flxSegActionsList"));
        flxSegActionsList.setDefaultUnit(kony.flex.DP);
        var segSelectedOptions = new kony.ui.SegmentedUI2(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
            "data": [{
                "imgActionCheckbox": "checkboxnormal.png",
                "lblActionName": "Label",
                "lblCurrencySymbol1": "",
                "lblCurrencySymbol2": "",
                "lblCurrencySymbol3": "",
                "lblCurrencySymbol4": "",
                "lblIconActionArrow": "",
                "lblLimitErrorIcon1": "",
                "lblLimitErrorIcon2": "",
                "lblLimitErrorIcon3": "",
                "lblLimitErrorIcon4": "",
                "lblLimitErrorMsg1": "kony.i18n.getLocalizedString(\"i18n.frmServiceManagement.lblNoServiceTypeError\")",
                "lblLimitErrorMsg2": "kony.i18n.getLocalizedString(\"i18n.frmServiceManagement.lblNoServiceTypeError\")",
                "lblLimitErrorMsg3": "kony.i18n.getLocalizedString(\"i18n.frmServiceManagement.lblNoServiceTypeError\")",
                "lblLimitErrorMsg4": "kony.i18n.getLocalizedString(\"i18n.frmServiceManagement.lblNoServiceTypeError\")",
                "lblMandatory": "Label",
                "lblNoDesc": "Label",
                "lblRowHeading1": "Label",
                "lblRowHeading2": "Label",
                "lblRowHeading3": "Label",
                "lblRowHeading4": "Label",
                "tbxLimitValue1": "",
                "tbxLimitValue2": "",
                "tbxLimitValue3": "",
                "tbxLimitValue4": ""
            }],
            "groupCells": false,
            "id": "segSelectedOptions",
            "isVisible": true,
            "left": "12px",
            "needPageIndicator": true,
            "pageOffDotImage": "pageoffdot.png",
            "pageOnDotImage": "pageondot.png",
            "retainSelection": false,
            "right": "25dp",
            "rowSkin": "seg2Normal",
            "rowTemplate": "flxCustRolesSelectedActions",
            "sectionHeaderSkin": "sliPhoneSegmentHeader0h3448bdd9d8941",
            "selectionBehavior": constants.SEGUI_DEFAULT_BEHAVIOR,
            "separatorRequired": false,
            "separatorThickness": 0,
            "showScrollbars": false,
            "top": "0px",
            "viewType": constants.SEGUI_VIEW_TYPE_TABLEVIEW,
            "widgetDataMap": {
                "flxActionArrow": "flxActionArrow",
                "flxActionCheckbox": "flxActionCheckbox",
                "flxActionDisabled": "flxActionDisabled",
                "flxActionLimits": "flxActionLimits",
                "flxActionLimitsGroup": "flxActionLimitsGroup",
                "flxActionName": "flxActionName",
                "flxActionNameContainer": "flxActionNameContainer",
                "flxActionRowCont": "flxActionRowCont",
                "flxCustRolesSelectedActions": "flxCustRolesSelectedActions",
                "flxLimitError1": "flxLimitError1",
                "flxLimitError2": "flxLimitError2",
                "flxLimitError3": "flxLimitError3",
                "flxLimitError4": "flxLimitError4",
                "flxLimitValue1": "flxLimitValue1",
                "flxLimitValue2": "flxLimitValue2",
                "flxLimitValue3": "flxLimitValue3",
                "flxLimitValue4": "flxLimitValue4",
                "flxLimitsRow1": "flxLimitsRow1",
                "flxLimitsRow2": "flxLimitsRow2",
                "flxLimitsRow3": "flxLimitsRow3",
                "flxLimitsRow4": "flxLimitsRow4",
                "imgActionCheckbox": "imgActionCheckbox",
                "lblActionName": "lblActionName",
                "lblCurrencySymbol1": "lblCurrencySymbol1",
                "lblCurrencySymbol2": "lblCurrencySymbol2",
                "lblCurrencySymbol3": "lblCurrencySymbol3",
                "lblCurrencySymbol4": "lblCurrencySymbol4",
                "lblIconActionArrow": "lblIconActionArrow",
                "lblLimitErrorIcon1": "lblLimitErrorIcon1",
                "lblLimitErrorIcon2": "lblLimitErrorIcon2",
                "lblLimitErrorIcon3": "lblLimitErrorIcon3",
                "lblLimitErrorIcon4": "lblLimitErrorIcon4",
                "lblLimitErrorMsg1": "lblLimitErrorMsg1",
                "lblLimitErrorMsg2": "lblLimitErrorMsg2",
                "lblLimitErrorMsg3": "lblLimitErrorMsg3",
                "lblLimitErrorMsg4": "lblLimitErrorMsg4",
                "lblMandatory": "lblMandatory",
                "lblNoDesc": "lblNoDesc",
                "lblRowHeading1": "lblRowHeading1",
                "lblRowHeading2": "lblRowHeading2",
                "lblRowHeading3": "lblRowHeading3",
                "lblRowHeading4": "lblRowHeading4",
                "tbxLimitValue1": "tbxLimitValue1",
                "tbxLimitValue2": "tbxLimitValue2",
                "tbxLimitValue3": "tbxLimitValue3",
                "tbxLimitValue4": "tbxLimitValue4"
            },
            "zIndex": 1
        }, controller.args[0], "segSelectedOptions"), extendConfig({
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "segSelectedOptions"), extendConfig({}, controller.args[2], "segSelectedOptions"));
        flxSegActionsList.add(segSelectedOptions);
        var rtxSelectedOptionsMessage = new kony.ui.RichText(extendConfig({
            "centerX": "50%",
            "centerY": "50%",
            "id": "rtxSelectedOptionsMessage",
            "isVisible": false,
            "skin": "sknRtxLato84939e12Px",
            "text": "Select any one of the features to view its corresponding actions.",
            "width": "80%",
            "zIndex": 1
        }, controller.args[0], "rtxSelectedOptionsMessage"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_CENTER,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "rtxSelectedOptionsMessage"), extendConfig({}, controller.args[2], "rtxSelectedOptionsMessage"));
        flxSegSelectedOptions.add(flxActionDetailsContainer, flxActionsMessage, flxSegActionsList, rtxSelectedOptionsMessage);
        flxSelectedOptions.add(lblSelectedOption, btnResetActions, flxSegSelectedOptions);
        flxSelectedOptionsWrapper.add(flxSelectedOptions);
        addFeaturesAndActions.add(flxAvailableOptionsWrapper, flxSelectedOptionsWrapper);
        return addFeaturesAndActions;
    }
})