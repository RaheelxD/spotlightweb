define("com/adminConsole/customerRoles/addFeaturesAndActions/useraddFeaturesAndActionsController", function() {
    return {};
});
define("com/adminConsole/customerRoles/addFeaturesAndActions/addFeaturesAndActionsControllerActions", {
    /*
          This is an auto generated file and any modifications to it may result in corruption of the action sequence.
        */
});
define("com/adminConsole/customerRoles/addFeaturesAndActions/addFeaturesAndActionsController", ["com/adminConsole/customerRoles/addFeaturesAndActions/useraddFeaturesAndActionsController", "com/adminConsole/customerRoles/addFeaturesAndActions/addFeaturesAndActionsControllerActions"], function() {
    var controller = require("com/adminConsole/customerRoles/addFeaturesAndActions/useraddFeaturesAndActionsController");
    var actions = require("com/adminConsole/customerRoles/addFeaturesAndActions/addFeaturesAndActionsControllerActions");
    for (var key in actions) {
        controller[key] = actions[key];
    }
    return controller;
});
