define("com/adminConsole/customerRoles/ViewRoleFeaturesActions/userViewRoleFeaturesActionsController", function() {
    return {};
});
define("com/adminConsole/customerRoles/ViewRoleFeaturesActions/ViewRoleFeaturesActionsControllerActions", {
    /*
      This is an auto generated file and any modifications to it may result in corruption of the action sequence.
    */
});
define("com/adminConsole/customerRoles/ViewRoleFeaturesActions/ViewRoleFeaturesActionsController", ["com/adminConsole/customerRoles/ViewRoleFeaturesActions/userViewRoleFeaturesActionsController", "com/adminConsole/customerRoles/ViewRoleFeaturesActions/ViewRoleFeaturesActionsControllerActions"], function() {
    var controller = require("com/adminConsole/customerRoles/ViewRoleFeaturesActions/userViewRoleFeaturesActionsController");
    var actions = require("com/adminConsole/customerRoles/ViewRoleFeaturesActions/ViewRoleFeaturesActionsControllerActions");
    for (var key in actions) {
        controller[key] = actions[key];
    }
    return controller;
});
