define(function() {
    return function(controller) {
        var ViewRoleFeaturesActions = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
            "clipBounds": true,
            "isMaster": true,
            "id": "ViewRoleFeaturesActions",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "left": "0dp",
            "isModalContainer": false,
            "right": "20dp",
            "skin": "slFbox",
            "top": "20dp",
            "width": "100%"
        }, controller.args[0], "ViewRoleFeaturesActions"), extendConfig({}, controller.args[1], "ViewRoleFeaturesActions"), extendConfig({}, controller.args[2], "ViewRoleFeaturesActions"));
        ViewRoleFeaturesActions.setDefaultUnit(kony.flex.DP);
        var flxViewRoleFeatureActions = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
            "clipBounds": true,
            "id": "flxViewRoleFeatureActions",
            "isVisible": true,
            "layoutType": kony.flex.FLOW_VERTICAL,
            "left": "20dp",
            "isModalContainer": false,
            "right": "25dp",
            "skin": "sknFlxbgFFFFFFbdrD7D9E0rd3px",
            "top": "0dp"
        }, controller.args[0], "flxViewRoleFeatureActions"), extendConfig({}, controller.args[1], "flxViewRoleFeatureActions"), extendConfig({}, controller.args[2], "flxViewRoleFeatureActions"));
        flxViewRoleFeatureActions.setDefaultUnit(kony.flex.DP);
        var flxHeader = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "clipBounds": true,
            "height": "40dp",
            "id": "flxHeader",
            "isVisible": true,
            "layoutType": kony.flex.FLOW_VERTICAL,
            "left": "0px",
            "isModalContainer": false,
            "right": "0px",
            "skin": "sknf5f6f8border0px",
            "top": "0px",
            "zIndex": 1
        }, controller.args[0], "flxHeader"), extendConfig({}, controller.args[1], "flxHeader"), extendConfig({}, controller.args[2], "flxHeader"));
        flxHeader.setDefaultUnit(kony.flex.DP);
        var flxTopRow1 = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
            "clipBounds": true,
            "id": "flxTopRow1",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "left": "0dp",
            "isModalContainer": false,
            "skin": "slFbox",
            "top": "12dp",
            "width": "100%"
        }, controller.args[0], "flxTopRow1"), extendConfig({}, controller.args[1], "flxTopRow1"), extendConfig({}, controller.args[2], "flxTopRow1"));
        flxTopRow1.setDefaultUnit(kony.flex.DP);
        var lblFeatureName = new kony.ui.Label(extendConfig({
            "id": "lblFeatureName",
            "isVisible": true,
            "left": "15dp",
            "skin": "sknLbl192B45LatoRegular14px",
            "text": "Bill Payment",
            "top": "0",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, controller.args[0], "lblFeatureName"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblFeatureName"), extendConfig({}, controller.args[2], "lblFeatureName"));
        var flxFeatureStatus = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
            "clipBounds": true,
            "id": "flxFeatureStatus",
            "isVisible": true,
            "layoutType": kony.flex.FLOW_HORIZONTAL,
            "reverseLayoutDirection": true,
            "isModalContainer": false,
            "right": "0dp",
            "skin": "slFbox",
            "top": "0dp",
            "width": "50%"
        }, controller.args[0], "flxFeatureStatus"), extendConfig({}, controller.args[1], "flxFeatureStatus"), extendConfig({}, controller.args[2], "flxFeatureStatus"));
        flxFeatureStatus.setDefaultUnit(kony.flex.DP);
        var statusValue = new kony.ui.Label(extendConfig({
            "id": "statusValue",
            "isVisible": true,
            "right": "20dp",
            "skin": "sknlblLato5bc06cBold14px",
            "i18n_text": "kony.i18n.getLocalizedString(\"i18n.secureimage.Active\")",
            "top": "0px",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, controller.args[0], "statusValue"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "statusValue"), extendConfig({}, controller.args[2], "statusValue"));
        var statusIcon = new kony.ui.Label(extendConfig({
            "id": "statusIcon",
            "isVisible": true,
            "right": "5dp",
            "skin": "sknFontIconActivate",
            "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmAlertsManagement.lblViewAlertStatusKey\")",
            "top": 2,
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, controller.args[0], "statusIcon"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "statusIcon"), extendConfig({}, controller.args[2], "statusIcon"));
        flxFeatureStatus.add(statusValue, statusIcon);
        flxTopRow1.add(lblFeatureName, flxFeatureStatus);
        flxHeader.add(flxTopRow1);
        var flxActionsList = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
            "clipBounds": true,
            "id": "flxActionsList",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "left": "0dp",
            "isModalContainer": false,
            "skin": "slFbox",
            "top": "0dp",
            "width": "100%"
        }, controller.args[0], "flxActionsList"), extendConfig({}, controller.args[1], "flxActionsList"), extendConfig({}, controller.args[2], "flxActionsList"));
        flxActionsList.setDefaultUnit(kony.flex.DP);
        var flxFeatureActionsHeader = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "clipBounds": true,
            "height": "40px",
            "id": "flxFeatureActionsHeader",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "left": "0dp",
            "isModalContainer": false,
            "skin": "sknflxffffffop100",
            "top": "0",
            "width": "100%"
        }, controller.args[0], "flxFeatureActionsHeader"), extendConfig({}, controller.args[1], "flxFeatureActionsHeader"), extendConfig({
            "hoverSkin": "sknfbfcfc"
        }, controller.args[2], "flxFeatureActionsHeader"));
        flxFeatureActionsHeader.setDefaultUnit(kony.flex.DP);
        var lblActionName = new kony.ui.Label(extendConfig({
            "centerY": "50%",
            "id": "lblActionName",
            "isVisible": true,
            "left": "15px",
            "skin": "sknlblLato696c7312px",
            "text": "ACTION",
            "width": "30%",
            "zIndex": 1
        }, controller.args[0], "lblActionName"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblActionName"), extendConfig({}, controller.args[2], "lblActionName"));
        var lblDescription = new kony.ui.Label(extendConfig({
            "centerY": "50%",
            "id": "lblDescription",
            "isVisible": true,
            "left": "40%",
            "skin": "sknlblLato696c7312px",
            "i18n_text": "kony.i18n.getLocalizedString(\"i18n.ConfigurationBundles.descriptionInCaps\")",
            "width": "60%",
            "zIndex": 1
        }, controller.args[0], "lblDescription"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblDescription"), extendConfig({}, controller.args[2], "lblDescription"));
        var lblActionStatus = new kony.ui.Label(extendConfig({
            "centerY": "60%",
            "id": "lblActionStatus",
            "isVisible": false,
            "right": "20dp",
            "skin": "sknlblLato696c7312px",
            "i18n_text": "kony.i18n.getLocalizedString(\"i18n.roles.STATUS\")",
            "width": "11%",
            "zIndex": 1
        }, controller.args[0], "lblActionStatus"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblActionStatus"), extendConfig({}, controller.args[2], "lblActionStatus"));
        flxFeatureActionsHeader.add(lblActionName, lblDescription, lblActionStatus);
        var lblSeperator = new kony.ui.Label(extendConfig({
            "height": "1px",
            "id": "lblSeperator",
            "isVisible": true,
            "left": "15dp",
            "right": "15dp",
            "skin": "sknlblSeperator",
            "text": ".",
            "top": "40dp",
            "zIndex": 1
        }, controller.args[0], "lblSeperator"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblSeperator"), extendConfig({}, controller.args[2], "lblSeperator"));
        var flxSegActions = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
            "clipBounds": true,
            "id": "flxSegActions",
            "isVisible": true,
            "layoutType": kony.flex.FLOW_VERTICAL,
            "left": "0dp",
            "isModalContainer": false,
            "skin": "slFbox",
            "top": "41dp",
            "width": "100%"
        }, controller.args[0], "flxSegActions"), extendConfig({}, controller.args[1], "flxSegActions"), extendConfig({}, controller.args[2], "flxSegActions"));
        flxSegActions.setDefaultUnit(kony.flex.DP);
        var SegActions = new kony.ui.SegmentedUI2(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
            "data": [{
                "lbActionStatus": "Active",
                "lblActionDescription": "Handle your business Payables  efficiently",
                "lblActionName": "Mobile App",
                "lblIconStatus": ""
            }, {
                "lbActionStatus": "Active",
                "lblActionDescription": "Handle your business Payables  efficiently",
                "lblActionName": "Mobile App",
                "lblIconStatus": ""
            }, {
                "lbActionStatus": "Active",
                "lblActionDescription": "Handle your business Payables  efficiently",
                "lblActionName": "Mobile App",
                "lblIconStatus": ""
            }],
            "groupCells": false,
            "id": "SegActions",
            "isVisible": true,
            "left": "0dp",
            "needPageIndicator": true,
            "pageOffDotImage": "pageoffdot.png",
            "pageOnDotImage": "pageondot.png",
            "retainSelection": false,
            "rowFocusSkin": "seg2Focus",
            "rowSkin": "seg2Normal",
            "rowTemplate": "flxRoleDetailsActions",
            "sectionHeaderSkin": "sliPhoneSegmentHeader",
            "selectionBehavior": constants.SEGUI_SINGLE_SELECT_BEHAVIOR,
            "selectionBehaviorConfig": {
                "imageIdentifier": "imgCheckBoxMsg",
                "selectedStateImage": "checkboxselected.png",
                "unselectedStateImage": "checkboxnormal.png"
            },
            "separatorRequired": false,
            "showScrollbars": false,
            "top": "0dp",
            "viewType": constants.SEGUI_VIEW_TYPE_TABLEVIEW,
            "widgetDataMap": {
                "flxRoleDetailsActions": "flxRoleDetailsActions",
                "flxStatus": "flxStatus",
                "lbActionStatus": "lbActionStatus",
                "lblActionDescription": "lblActionDescription",
                "lblActionName": "lblActionName",
                "lblIconStatus": "lblIconStatus"
            },
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 10
        }, controller.args[0], "SegActions"), extendConfig({
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "SegActions"), extendConfig({}, controller.args[2], "SegActions"));
        flxSegActions.add(SegActions);
        flxActionsList.add(flxFeatureActionsHeader, lblSeperator, flxSegActions);
        flxViewRoleFeatureActions.add(flxHeader, flxActionsList);
        ViewRoleFeaturesActions.add(flxViewRoleFeatureActions);
        return ViewRoleFeaturesActions;
    }
})