define("com/adminConsole/view/detailsMultiLine/userdetailsMultiLineController", function() {
    return {};
});
define("com/adminConsole/view/detailsMultiLine/detailsMultiLineControllerActions", {
    /*
          This is an auto generated file and any modifications to it may result in corruption of the action sequence.
        */
});
define("com/adminConsole/view/detailsMultiLine/detailsMultiLineController", ["com/adminConsole/view/detailsMultiLine/userdetailsMultiLineController", "com/adminConsole/view/detailsMultiLine/detailsMultiLineControllerActions"], function() {
    var controller = require("com/adminConsole/view/detailsMultiLine/userdetailsMultiLineController");
    var actions = require("com/adminConsole/view/detailsMultiLine/detailsMultiLineControllerActions");
    for (var key in actions) {
        controller[key] = actions[key];
    }
    return controller;
});
