define(function() {
    return function(controller) {
        var detailsMultiLine = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
            "clipBounds": true,
            "isMaster": true,
            "id": "detailsMultiLine",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "left": "0dp",
            "isModalContainer": false,
            "skin": "slFbox",
            "top": "0dp",
            "width": "100%"
        }, controller.args[0], "detailsMultiLine"), extendConfig({}, controller.args[1], "detailsMultiLine"), extendConfig({}, controller.args[2], "detailsMultiLine"));
        detailsMultiLine.setDefaultUnit(kony.flex.DP);
        var flxColumn1 = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
            "clipBounds": true,
            "id": "flxColumn1",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "left": "20px",
            "isModalContainer": false,
            "skin": "slFbox",
            "top": "0dp",
            "width": "30%",
            "zIndex": 1
        }, controller.args[0], "flxColumn1"), extendConfig({}, controller.args[1], "flxColumn1"), extendConfig({}, controller.args[2], "flxColumn1"));
        flxColumn1.setDefaultUnit(kony.flex.DP);
        var lblHeading1 = new kony.ui.Label(extendConfig({
            "id": "lblHeading1",
            "isVisible": true,
            "left": "0px",
            "skin": "sknlblLato696c7311px",
            "i18n_text": "kony.i18n.getLocalizedString(\"i18n.userwidgetmodel.lblHeading1\")",
            "top": "0px",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, controller.args[0], "lblHeading1"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblHeading1"), extendConfig({}, controller.args[2], "lblHeading1"));
        var rtxData1 = new kony.ui.RichText(extendConfig({
            "id": "rtxData1",
            "isVisible": true,
            "left": "0dp",
            "skin": "sknRtx485C75LatoSemiBold13px",
            "i18n_text": "kony.i18n.getLocalizedString(\"i18n.userwidgetmodel.rtxData1\")",
            "top": "25px",
            "width": "100%",
            "zIndex": 1
        }, controller.args[0], "rtxData1"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "rtxData1"), extendConfig({}, controller.args[2], "rtxData1"));
        flxColumn1.add(lblHeading1, rtxData1);
        var flxColumn2 = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
            "clipBounds": true,
            "id": "flxColumn2",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "left": "40%",
            "isModalContainer": false,
            "skin": "slFbox",
            "top": "0dp",
            "width": "25%",
            "zIndex": 1
        }, controller.args[0], "flxColumn2"), extendConfig({}, controller.args[1], "flxColumn2"), extendConfig({}, controller.args[2], "flxColumn2"));
        flxColumn2.setDefaultUnit(kony.flex.DP);
        var lblHeading2 = new kony.ui.Label(extendConfig({
            "id": "lblHeading2",
            "isVisible": true,
            "left": "0px",
            "skin": "sknlblLato696c7311px",
            "i18n_text": "kony.i18n.getLocalizedString(\"i18n.userwidgetmodel.lblHeading1\")",
            "top": "0px",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, controller.args[0], "lblHeading2"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblHeading2"), extendConfig({}, controller.args[2], "lblHeading2"));
        var rtxData2 = new kony.ui.RichText(extendConfig({
            "id": "rtxData2",
            "isVisible": true,
            "left": "0dp",
            "skin": "sknRtx485C75LatoSemiBold13px",
            "i18n_text": "kony.i18n.getLocalizedString(\"i18n.userwidgetmodel.rtxData1\")",
            "top": "25px",
            "width": "100%",
            "zIndex": 1
        }, controller.args[0], "rtxData2"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "rtxData2"), extendConfig({}, controller.args[2], "rtxData2"));
        flxColumn2.add(lblHeading2, rtxData2);
        var flxColumn3 = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
            "clipBounds": true,
            "id": "flxColumn3",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "left": "75%",
            "isModalContainer": false,
            "right": "35px",
            "skin": "slFbox",
            "top": "0dp",
            "zIndex": 1
        }, controller.args[0], "flxColumn3"), extendConfig({}, controller.args[1], "flxColumn3"), extendConfig({}, controller.args[2], "flxColumn3"));
        flxColumn3.setDefaultUnit(kony.flex.DP);
        var lblHeading3 = new kony.ui.Label(extendConfig({
            "id": "lblHeading3",
            "isVisible": true,
            "left": "0px",
            "skin": "sknlblLato696c7311px",
            "i18n_text": "kony.i18n.getLocalizedString(\"i18n.userwidgetmodel.lblHeading1\")",
            "top": "0px",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, controller.args[0], "lblHeading3"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblHeading3"), extendConfig({}, controller.args[2], "lblHeading3"));
        var rtxData3 = new kony.ui.RichText(extendConfig({
            "id": "rtxData3",
            "isVisible": true,
            "left": "0dp",
            "skin": "sknRtx485C75LatoSemiBold13px",
            "i18n_text": "kony.i18n.getLocalizedString(\"i18n.userwidgetmodel.rtxData1\")",
            "top": "25px",
            "width": "100%",
            "zIndex": 1
        }, controller.args[0], "rtxData3"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "rtxData3"), extendConfig({}, controller.args[2], "rtxData3"));
        flxColumn3.add(lblHeading3, rtxData3);
        detailsMultiLine.add(flxColumn1, flxColumn2, flxColumn3);
        return detailsMultiLine;
    }
})