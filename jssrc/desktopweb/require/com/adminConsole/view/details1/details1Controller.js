define("com/adminConsole/view/details1/userdetails1Controller", function() {
    return {};
});
define("com/adminConsole/view/details1/details1ControllerActions", {
    /*
          This is an auto generated file and any modifications to it may result in corruption of the action sequence.
        */
});
define("com/adminConsole/view/details1/details1Controller", ["com/adminConsole/view/details1/userdetails1Controller", "com/adminConsole/view/details1/details1ControllerActions"], function() {
    var controller = require("com/adminConsole/view/details1/userdetails1Controller");
    var actions = require("com/adminConsole/view/details1/details1ControllerActions");
    for (var key in actions) {
        controller[key] = actions[key];
    }
    return controller;
});
