define(function() {
    return function(controller) {
        var featuresTemplate = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
            "clipBounds": true,
            "isMaster": true,
            "id": "featuresTemplate",
            "isVisible": true,
            "layoutType": kony.flex.FLOW_VERTICAL,
            "left": "0dp",
            "isModalContainer": false,
            "preShow": function(eventobject) {
                controller.AS_FlexContainer_d2f3ddd2a6554a3f9532e0383726d76a(eventobject);
            },
            "skin": "sknFlxbgFFFFFFbdrD7D9E0rd3px",
            "top": "0dp",
            "width": "100%"
        }, controller.args[0], "featuresTemplate"), extendConfig({}, controller.args[1], "featuresTemplate"), extendConfig({}, controller.args[2], "featuresTemplate"));
        featuresTemplate.setDefaultUnit(kony.flex.DP);
        var flxHeader = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "clipBounds": true,
            "height": "70dp",
            "id": "flxHeader",
            "isVisible": true,
            "layoutType": kony.flex.FLOW_VERTICAL,
            "left": "0px",
            "isModalContainer": false,
            "right": "0px",
            "skin": "sknFlxBgFFFFFFBorE4E6ECsz1pxR3px",
            "top": "0px",
            "zIndex": 1
        }, controller.args[0], "flxHeader"), extendConfig({}, controller.args[1], "flxHeader"), extendConfig({}, controller.args[2], "flxHeader"));
        flxHeader.setDefaultUnit(kony.flex.DP);
        var flxTopRow1 = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
            "clipBounds": true,
            "id": "flxTopRow1",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "left": "0dp",
            "isModalContainer": false,
            "skin": "slFbox",
            "top": "15dp",
            "width": "100%"
        }, controller.args[0], "flxTopRow1"), extendConfig({}, controller.args[1], "flxTopRow1"), extendConfig({}, controller.args[2], "flxTopRow1"));
        flxTopRow1.setDefaultUnit(kony.flex.DP);
        var flxRight = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
            "clipBounds": true,
            "id": "flxRight",
            "isVisible": true,
            "layoutType": kony.flex.FLOW_HORIZONTAL,
            "reverseLayoutDirection": true,
            "isModalContainer": false,
            "right": "0dp",
            "skin": "slFbox",
            "top": "0dp",
            "width": "50%"
        }, controller.args[0], "flxRight"), extendConfig({}, controller.args[1], "flxRight"), extendConfig({}, controller.args[2], "flxRight"));
        flxRight.setDefaultUnit(kony.flex.DP);
        var statusValue = new kony.ui.Label(extendConfig({
            "id": "statusValue",
            "isVisible": true,
            "right": "20dp",
            "skin": "sknlblLato5bc06cBold14px",
            "i18n_text": "kony.i18n.getLocalizedString(\"i18n.secureimage.Active\")",
            "top": "0px",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, controller.args[0], "statusValue"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "statusValue"), extendConfig({}, controller.args[2], "statusValue"));
        var statusIcon = new kony.ui.Label(extendConfig({
            "id": "statusIcon",
            "isVisible": true,
            "right": "5dp",
            "skin": "sknFontIconActivate",
            "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmAlertsManagement.lblViewAlertStatusKey\")",
            "top": 0,
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, controller.args[0], "statusIcon"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "statusIcon"), extendConfig({}, controller.args[2], "statusIcon"));
        flxRight.add(statusValue, statusIcon);
        var flxLeft = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
            "clipBounds": true,
            "id": "flxLeft",
            "isVisible": true,
            "layoutType": kony.flex.FLOW_HORIZONTAL,
            "left": "20dp",
            "isModalContainer": false,
            "skin": "slFbox",
            "top": "0dp",
            "width": "100%"
        }, controller.args[0], "flxLeft"), extendConfig({}, controller.args[1], "flxLeft"), extendConfig({}, controller.args[2], "flxLeft"));
        flxLeft.setDefaultUnit(kony.flex.DP);
        var lblFeatureName = new kony.ui.Label(extendConfig({
            "id": "lblFeatureName",
            "isVisible": true,
            "left": "0dp",
            "skin": "sknLbl192B45LatoRegular14px",
            "text": "Transfer to account in other FIs",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, controller.args[0], "lblFeatureName"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblFeatureName"), extendConfig({}, controller.args[2], "lblFeatureName"));
        flxLeft.add(lblFeatureName);
        flxTopRow1.add(flxRight, flxLeft);
        var flxViewDetails = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
            "clipBounds": true,
            "id": "flxViewDetails",
            "isVisible": true,
            "layoutType": kony.flex.FLOW_VERTICAL,
            "left": "21dp",
            "isModalContainer": false,
            "skin": "slFbox",
            "top": "10dp",
            "width": "100dp"
        }, controller.args[0], "flxViewDetails"), extendConfig({}, controller.args[1], "flxViewDetails"), extendConfig({
            "hoverSkin": "sknFlxPointer"
        }, controller.args[2], "flxViewDetails"));
        flxViewDetails.setDefaultUnit(kony.flex.DP);
        var lblViewDetails = new kony.ui.Label(extendConfig({
            "id": "lblViewDetails",
            "isVisible": true,
            "left": "0dp",
            "skin": "sknlblLato11ABEB13px",
            "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmGroupsController.viewDetails\")",
            "top": "0dp",
            "width": "80dp",
            "zIndex": 1
        }, controller.args[0], "lblViewDetails"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblViewDetails"), extendConfig({
            "hoverSkin": "sknlblcursor"
        }, controller.args[2], "lblViewDetails"));
        flxViewDetails.add(lblViewDetails);
        flxHeader.add(flxTopRow1, flxViewDetails);
        featuresTemplate.add(flxHeader);
        return featuresTemplate;
    }
})