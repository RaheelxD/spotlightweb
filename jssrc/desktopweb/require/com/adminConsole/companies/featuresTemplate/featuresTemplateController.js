define("com/adminConsole/companies/featuresTemplate/userfeaturesTemplateController", function() {
    return {
        setFlowActions: function() {}
    };
});
define("com/adminConsole/companies/featuresTemplate/featuresTemplateControllerActions", {
    /*
        This is an auto generated file and any modifications to it may result in corruption of the action sequence.
    */
    AS_FlexContainer_d2f3ddd2a6554a3f9532e0383726d76a: function AS_FlexContainer_d2f3ddd2a6554a3f9532e0383726d76a(eventobject) {
        var self = this;
        this.setFlowActions();
    }
});
define("com/adminConsole/companies/featuresTemplate/featuresTemplateController", ["com/adminConsole/companies/featuresTemplate/userfeaturesTemplateController", "com/adminConsole/companies/featuresTemplate/featuresTemplateControllerActions"], function() {
    var controller = require("com/adminConsole/companies/featuresTemplate/userfeaturesTemplateController");
    var actions = require("com/adminConsole/companies/featuresTemplate/featuresTemplateControllerActions");
    for (var key in actions) {
        controller[key] = actions[key];
    }
    return controller;
});
