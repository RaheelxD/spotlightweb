define(function() {
    return function(controller) {
        var featureLimitsTemplate = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
            "clipBounds": true,
            "isMaster": true,
            "id": "featureLimitsTemplate",
            "isVisible": true,
            "layoutType": kony.flex.FLOW_VERTICAL,
            "left": "0dp",
            "isModalContainer": false,
            "skin": "sknFlxbgFFFFFFbdrD7D9E0rd3px",
            "top": "0dp",
            "width": "100%"
        }, controller.args[0], "featureLimitsTemplate"), extendConfig({}, controller.args[1], "featureLimitsTemplate"), extendConfig({}, controller.args[2], "featureLimitsTemplate"));
        featureLimitsTemplate.setDefaultUnit(kony.flex.DP);
        var flxHeader = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
            "clipBounds": true,
            "id": "flxHeader",
            "isVisible": true,
            "layoutType": kony.flex.FLOW_VERTICAL,
            "left": "0px",
            "isModalContainer": false,
            "right": "0px",
            "skin": "sknf5f6f8border0px",
            "top": "0px",
            "zIndex": 1
        }, controller.args[0], "flxHeader"), extendConfig({}, controller.args[1], "flxHeader"), extendConfig({}, controller.args[2], "flxHeader"));
        flxHeader.setDefaultUnit(kony.flex.DP);
        var flxTopRow1 = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
            "bottom": "15dp",
            "clipBounds": true,
            "id": "flxTopRow1",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "left": "0dp",
            "isModalContainer": false,
            "skin": "slFbox",
            "top": "15dp",
            "width": "100%"
        }, controller.args[0], "flxTopRow1"), extendConfig({}, controller.args[1], "flxTopRow1"), extendConfig({}, controller.args[2], "flxTopRow1"));
        flxTopRow1.setDefaultUnit(kony.flex.DP);
        var flxRight = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
            "clipBounds": true,
            "id": "flxRight",
            "isVisible": true,
            "layoutType": kony.flex.FLOW_HORIZONTAL,
            "reverseLayoutDirection": true,
            "isModalContainer": false,
            "right": "0dp",
            "skin": "slFbox",
            "top": "0dp",
            "width": "50%"
        }, controller.args[0], "flxRight"), extendConfig({}, controller.args[1], "flxRight"), extendConfig({}, controller.args[2], "flxRight"));
        flxRight.setDefaultUnit(kony.flex.DP);
        var statusValue = new kony.ui.Label(extendConfig({
            "id": "statusValue",
            "isVisible": true,
            "right": "20dp",
            "skin": "sknlblLato5bc06cBold14px",
            "i18n_text": "kony.i18n.getLocalizedString(\"i18n.secureimage.Active\")",
            "top": "0px",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, controller.args[0], "statusValue"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "statusValue"), extendConfig({}, controller.args[2], "statusValue"));
        var statusIcon = new kony.ui.Label(extendConfig({
            "id": "statusIcon",
            "isVisible": true,
            "right": "5dp",
            "skin": "sknFontIconActivate",
            "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmAlertsManagement.lblViewAlertStatusKey\")",
            "top": "2dp",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, controller.args[0], "statusIcon"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "statusIcon"), extendConfig({}, controller.args[2], "statusIcon"));
        flxRight.add(statusValue, statusIcon);
        var flxLeft = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
            "clipBounds": true,
            "id": "flxLeft",
            "isVisible": true,
            "layoutType": kony.flex.FLOW_HORIZONTAL,
            "left": "0dp",
            "isModalContainer": false,
            "skin": "slFbox",
            "top": "0dp",
            "width": "100%"
        }, controller.args[0], "flxLeft"), extendConfig({}, controller.args[1], "flxLeft"), extendConfig({}, controller.args[2], "flxLeft"));
        flxLeft.setDefaultUnit(kony.flex.DP);
        var lblFeatureName = new kony.ui.Label(extendConfig({
            "id": "lblFeatureName",
            "isVisible": true,
            "left": "20dp",
            "skin": "sknLbl192B45LatoRegular14px",
            "text": "Transfer to account in other FIs",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, controller.args[0], "lblFeatureName"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblFeatureName"), extendConfig({}, controller.args[2], "lblFeatureName"));
        flxLeft.add(lblFeatureName);
        flxTopRow1.add(flxRight, flxLeft);
        flxHeader.add(flxTopRow1);
        var flxHideDisplayContent = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
            "clipBounds": true,
            "id": "flxHideDisplayContent",
            "isVisible": true,
            "layoutType": kony.flex.FLOW_VERTICAL,
            "left": "0dp",
            "isModalContainer": false,
            "skin": "slFbox",
            "top": "0dp",
            "width": "100%"
        }, controller.args[0], "flxHideDisplayContent"), extendConfig({}, controller.args[1], "flxHideDisplayContent"), extendConfig({}, controller.args[2], "flxHideDisplayContent"));
        flxHideDisplayContent.setDefaultUnit(kony.flex.DP);
        var flxFeatureActionsHeader = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "clipBounds": true,
            "height": "35px",
            "id": "flxFeatureActionsHeader",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "left": "0dp",
            "isModalContainer": false,
            "skin": "sknflxffffffop100",
            "top": "0",
            "width": "100%"
        }, controller.args[0], "flxFeatureActionsHeader"), extendConfig({}, controller.args[1], "flxFeatureActionsHeader"), extendConfig({
            "hoverSkin": "sknfbfcfc"
        }, controller.args[2], "flxFeatureActionsHeader"));
        flxFeatureActionsHeader.setDefaultUnit(kony.flex.DP);
        var lblActionName = new kony.ui.Label(extendConfig({
            "centerY": "50%",
            "id": "lblActionName",
            "isVisible": true,
            "left": "20px",
            "skin": "sknlblLato696c7312px",
            "text": "APPROVAL LIMITS",
            "width": "30%",
            "zIndex": 1
        }, controller.args[0], "lblActionName"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblActionName"), extendConfig({}, controller.args[2], "lblActionName"));
        var lblDescription = new kony.ui.Label(extendConfig({
            "centerY": "50%",
            "id": "lblDescription",
            "isVisible": true,
            "left": "32%",
            "skin": "sknlblLato696c7312px",
            "text": "APPROVERS",
            "width": "34%",
            "zIndex": 1
        }, controller.args[0], "lblDescription"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblDescription"), extendConfig({}, controller.args[2], "lblDescription"));
        var lblView = new kony.ui.Label(extendConfig({
            "centerY": "50%",
            "id": "lblView",
            "isVisible": true,
            "left": "70%",
            "right": "20dp",
            "skin": "sknlblLato696c7312px",
            "text": "APPROVAL RULE",
            "width": "27%",
            "zIndex": 1
        }, controller.args[0], "lblView"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblView"), extendConfig({}, controller.args[2], "lblView"));
        flxFeatureActionsHeader.add(lblActionName, lblDescription, lblView);
        var lblSeperator = new kony.ui.Label(extendConfig({
            "height": "1px",
            "id": "lblSeperator",
            "isVisible": true,
            "left": "0dp",
            "skin": "sknlblSeperator",
            "text": ".",
            "top": "0dp",
            "width": "100%",
            "zIndex": 1
        }, controller.args[0], "lblSeperator"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblSeperator"), extendConfig({}, controller.args[2], "lblSeperator"));
        var flxSegActions = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
            "clipBounds": true,
            "id": "flxSegActions",
            "isVisible": true,
            "layoutType": kony.flex.FLOW_VERTICAL,
            "left": "0dp",
            "isModalContainer": false,
            "skin": "slFbox",
            "top": "0dp",
            "width": "100%"
        }, controller.args[0], "flxSegActions"), extendConfig({}, controller.args[1], "flxSegActions"), extendConfig({}, controller.args[2], "flxSegActions"));
        flxSegActions.setDefaultUnit(kony.flex.DP);
        var SegActions = new kony.ui.SegmentedUI2(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
            "data": [{
                "lblApprovalLimits": "Admin Role",
                "lblApprovalRule": "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do",
                "lblArrovers": "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do",
                "lblSeperator": "."
            }, {
                "lblApprovalLimits": "Admin Role",
                "lblApprovalRule": "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do",
                "lblArrovers": "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do",
                "lblSeperator": "."
            }, {
                "lblApprovalLimits": "Admin Role",
                "lblApprovalRule": "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do",
                "lblArrovers": "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do",
                "lblSeperator": "."
            }],
            "groupCells": false,
            "id": "SegActions",
            "isVisible": true,
            "left": "0dp",
            "needPageIndicator": true,
            "pageOffDotImage": "pageoffdot.png",
            "pageOnDotImage": "pageondot.png",
            "retainSelection": false,
            "rowFocusSkin": "seg2Focus",
            "rowSkin": "seg2Normal",
            "rowTemplate": "flxFeatureLimits",
            "sectionHeaderSkin": "sliPhoneSegmentHeader",
            "selectionBehavior": constants.SEGUI_SINGLE_SELECT_BEHAVIOR,
            "selectionBehaviorConfig": {
                "imageIdentifier": "imgCheckBoxMsg",
                "selectedStateImage": "checkboxselected.png",
                "unselectedStateImage": "checkboxnormal.png"
            },
            "separatorRequired": false,
            "showScrollbars": false,
            "top": "0dp",
            "viewType": constants.SEGUI_VIEW_TYPE_TABLEVIEW,
            "widgetDataMap": {
                "flxFeatureLimits": "flxFeatureLimits",
                "lblApprovalLimits": "lblApprovalLimits",
                "lblApprovalRule": "lblApprovalRule",
                "lblArrovers": "lblArrovers",
                "lblSeperator": "lblSeperator"
            },
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 10
        }, controller.args[0], "SegActions"), extendConfig({
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "SegActions"), extendConfig({}, controller.args[2], "SegActions"));
        flxSegActions.add(SegActions);
        var flxNoApprovals = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "clipBounds": true,
            "height": "57dp",
            "id": "flxNoApprovals",
            "isVisible": false,
            "layoutType": kony.flex.FLOW_VERTICAL,
            "left": "0dp",
            "isModalContainer": false,
            "skin": "slFbox",
            "top": "0dp",
            "width": "100%"
        }, controller.args[0], "flxNoApprovals"), extendConfig({}, controller.args[1], "flxNoApprovals"), extendConfig({}, controller.args[2], "flxNoApprovals"));
        flxNoApprovals.setDefaultUnit(kony.flex.DP);
        var lblNoApprovals = new kony.ui.Label(extendConfig({
            "centerX": "50%",
            "centerY": "50%",
            "id": "lblNoApprovals",
            "isVisible": true,
            "left": "0dp",
            "skin": "sknlbl485C75LatoSemiBold13px",
            "text": "No Approvers Configured",
            "width": "60%",
            "zIndex": 1
        }, controller.args[0], "lblNoApprovals"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_CENTER,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblNoApprovals"), extendConfig({}, controller.args[2], "lblNoApprovals"));
        flxNoApprovals.add(lblNoApprovals);
        flxHideDisplayContent.add(flxFeatureActionsHeader, lblSeperator, flxSegActions, flxNoApprovals);
        featureLimitsTemplate.add(flxHeader, flxHideDisplayContent);
        return featureLimitsTemplate;
    }
})