define("com/adminConsole/companies/featureLimitsTemplate/userfeatureLimitsTemplateController", function() {
    return {
        setFlowActions: function() {}
    };
});
define("com/adminConsole/companies/featureLimitsTemplate/featureLimitsTemplateControllerActions", {
    /*
      This is an auto generated file and any modifications to it may result in corruption of the action sequence.
    */
    /** preShow defined for featureLimitsTemplate **/
    AS_FlexContainer_c38bbbe628ae489fa87643cf39c32f5f: function AS_FlexContainer_c38bbbe628ae489fa87643cf39c32f5f(eventobject) {
        var self = this;
        this.setFlowActions();
    }
});
define("com/adminConsole/companies/featureLimitsTemplate/featureLimitsTemplateController", ["com/adminConsole/companies/featureLimitsTemplate/userfeatureLimitsTemplateController", "com/adminConsole/companies/featureLimitsTemplate/featureLimitsTemplateControllerActions"], function() {
    var controller = require("com/adminConsole/companies/featureLimitsTemplate/userfeatureLimitsTemplateController");
    var actions = require("com/adminConsole/companies/featureLimitsTemplate/featureLimitsTemplateControllerActions");
    for (var key in actions) {
        controller[key] = actions[key];
    }
    return controller;
});
