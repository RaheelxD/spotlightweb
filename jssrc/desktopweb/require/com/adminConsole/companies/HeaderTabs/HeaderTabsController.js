define("com/adminConsole/companies/HeaderTabs/userHeaderTabsController", function() {
    return {};
});
define("com/adminConsole/companies/HeaderTabs/HeaderTabsControllerActions", {
    /*
          This is an auto generated file and any modifications to it may result in corruption of the action sequence.
        */
});
define("com/adminConsole/companies/HeaderTabs/HeaderTabsController", ["com/adminConsole/companies/HeaderTabs/userHeaderTabsController", "com/adminConsole/companies/HeaderTabs/HeaderTabsControllerActions"], function() {
    var controller = require("com/adminConsole/companies/HeaderTabs/userHeaderTabsController");
    var actions = require("com/adminConsole/companies/HeaderTabs/HeaderTabsControllerActions");
    for (var key in actions) {
        controller[key] = actions[key];
    }
    return controller;
});
