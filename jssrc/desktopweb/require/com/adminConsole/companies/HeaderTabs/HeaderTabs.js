define(function() {
    return function(controller) {
        var HeaderTabs = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "clipBounds": true,
            "isMaster": true,
            "height": "45px",
            "id": "HeaderTabs",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "left": "0dp",
            "isModalContainer": false,
            "skin": "sknbackGroundffffff100",
            "top": "0dp",
            "width": "100%"
        }, controller.args[0], "HeaderTabs"), extendConfig({}, controller.args[1], "HeaderTabs"), extendConfig({}, controller.args[2], "HeaderTabs"));
        HeaderTabs.setDefaultUnit(kony.flex.DP);
        var flxDetailHeader = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "clipBounds": true,
            "height": "45px",
            "id": "flxDetailHeader",
            "isVisible": true,
            "layoutType": kony.flex.FLOW_HORIZONTAL,
            "left": "0dp",
            "isModalContainer": false,
            "right": "0px",
            "skin": "slFbox",
            "top": "0dp",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 4
        }, controller.args[0], "flxDetailHeader"), extendConfig({}, controller.args[1], "flxDetailHeader"), extendConfig({}, controller.args[2], "flxDetailHeader"));
        flxDetailHeader.setDefaultUnit(kony.flex.DP);
        var flxHeader1 = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "clipBounds": true,
            "height": "100%",
            "id": "flxHeader1",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "left": "0dp",
            "isModalContainer": false,
            "skin": "sknFlxTabSelected1BorderFFFFFF",
            "top": "0dp",
            "width": "109px",
            "zIndex": 3
        }, controller.args[0], "flxHeader1"), extendConfig({}, controller.args[1], "flxHeader1"), extendConfig({}, controller.args[2], "flxHeader1"));
        flxHeader1.setDefaultUnit(kony.flex.DP);
        var lblCount1 = new kony.ui.Label(extendConfig({
            "centerX": "50%",
            "id": "lblCount1",
            "isVisible": true,
            "skin": "sknnumericCsr",
            "i18n_text": "kony.i18n.getLocalizedString(\"i18n.userwidgetmodel.lblCount1\")",
            "top": "9px",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, controller.args[0], "lblCount1"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblCount1"), extendConfig({}, controller.args[2], "lblCount1"));
        var lblStatus1 = new kony.ui.Label(extendConfig({
            "centerX": "50%",
            "id": "lblStatus1",
            "isVisible": true,
            "left": "5px",
            "skin": "sknlbl485c7511px",
            "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmCustomerManagementController.Pending_LC\")",
            "top": "25px",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, controller.args[0], "lblStatus1"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_CENTER,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblStatus1"), extendConfig({}, controller.args[2], "lblStatus1"));
        var flxSelected = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "clipBounds": true,
            "height": "3px",
            "id": "flxSelected",
            "isVisible": true,
            "layoutType": kony.flex.FLOW_HORIZONTAL,
            "left": "0px",
            "isModalContainer": false,
            "right": "0px",
            "skin": "sknFlx006CCA",
            "top": "0dp",
            "width": "109px",
            "zIndex": 3
        }, controller.args[0], "flxSelected"), extendConfig({}, controller.args[1], "flxSelected"), extendConfig({}, controller.args[2], "flxSelected"));
        flxSelected.setDefaultUnit(kony.flex.DP);
        flxSelected.add();
        var flxseparator = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "clipBounds": true,
            "height": "45px",
            "id": "flxseparator",
            "isVisible": false,
            "layoutType": kony.flex.FREE_FORM,
            "isModalContainer": false,
            "right": "0px",
            "skin": "flxe4e6ec1px",
            "top": 0,
            "width": "1px",
            "zIndex": 1
        }, controller.args[0], "flxseparator"), extendConfig({}, controller.args[1], "flxseparator"), extendConfig({}, controller.args[2], "flxseparator"));
        flxseparator.setDefaultUnit(kony.flex.DP);
        flxseparator.add();
        flxHeader1.add(lblCount1, lblStatus1, flxSelected, flxseparator);
        var flxHeader2 = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "clipBounds": true,
            "height": "100%",
            "id": "flxHeader2",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "left": "0dp",
            "isModalContainer": false,
            "right": "2px",
            "skin": "sknFlxPointer",
            "top": "0dp",
            "width": "107px",
            "zIndex": 2
        }, controller.args[0], "flxHeader2"), extendConfig({}, controller.args[1], "flxHeader2"), extendConfig({
            "hoverSkin": "sknFlxPointer"
        }, controller.args[2], "flxHeader2"));
        flxHeader2.setDefaultUnit(kony.flex.DP);
        var lblCount2 = new kony.ui.Label(extendConfig({
            "centerX": "50%",
            "id": "lblCount2",
            "isVisible": true,
            "skin": "sknnumericCsr",
            "text": "7",
            "top": "9px",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, controller.args[0], "lblCount2"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblCount2"), extendConfig({}, controller.args[2], "lblCount2"));
        var lblStatus2 = new kony.ui.Label(extendConfig({
            "centerX": "50%",
            "id": "lblStatus2",
            "isVisible": true,
            "left": "5px",
            "skin": "sknlbl485c7511px",
            "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmCustomerManagementController.Rejected\")",
            "top": "25px",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, controller.args[0], "lblStatus2"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_CENTER,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblStatus2"), extendConfig({}, controller.args[2], "lblStatus2"));
        var flxSelected2 = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "clipBounds": true,
            "height": "3dp",
            "id": "flxSelected2",
            "isVisible": false,
            "layoutType": kony.flex.FLOW_HORIZONTAL,
            "left": "0",
            "isModalContainer": false,
            "right": "0px",
            "skin": "sknFlx006CCA",
            "top": "0px",
            "width": "107px",
            "zIndex": 3
        }, controller.args[0], "flxSelected2"), extendConfig({}, controller.args[1], "flxSelected2"), extendConfig({}, controller.args[2], "flxSelected2"));
        flxSelected2.setDefaultUnit(kony.flex.DP);
        flxSelected2.add();
        var flxSeparator2 = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "clipBounds": true,
            "height": "45px",
            "id": "flxSeparator2",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "isModalContainer": false,
            "right": "0px",
            "skin": "flxe4e6ec1px",
            "top": 0,
            "width": "1px",
            "zIndex": 1
        }, controller.args[0], "flxSeparator2"), extendConfig({}, controller.args[1], "flxSeparator2"), extendConfig({}, controller.args[2], "flxSeparator2"));
        flxSeparator2.setDefaultUnit(kony.flex.DP);
        flxSeparator2.add();
        flxHeader2.add(lblCount2, lblStatus2, flxSelected2, flxSeparator2);
        flxDetailHeader.add(flxHeader1, flxHeader2);
        var lblBottomSeperatorCommonTabs = new kony.ui.Label(extendConfig({
            "bottom": "0dp",
            "height": "1px",
            "id": "lblBottomSeperatorCommonTabs",
            "isVisible": true,
            "left": "0px",
            "right": "0px",
            "skin": "sknLble1e5edOp100",
            "zIndex": 2
        }, controller.args[0], "lblBottomSeperatorCommonTabs"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblBottomSeperatorCommonTabs"), extendConfig({}, controller.args[2], "lblBottomSeperatorCommonTabs"));
        HeaderTabs.add(flxDetailHeader, lblBottomSeperatorCommonTabs);
        return HeaderTabs;
    }
})