define(function() {
    return function(controller) {
        var viewPersonalInfo = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
            "clipBounds": true,
            "isMaster": true,
            "id": "viewPersonalInfo",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "left": "0dp",
            "isModalContainer": false,
            "skin": "slFbox",
            "top": "30dp",
            "width": "100%"
        }, controller.args[0], "viewPersonalInfo"), extendConfig({}, controller.args[1], "viewPersonalInfo"), extendConfig({}, controller.args[2], "viewPersonalInfo"));
        viewPersonalInfo.setDefaultUnit(kony.flex.DP);
        var loansSectionHeader = new com.adminConsole.loans.applications.loansSectionHeader(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
            "id": "loansSectionHeader",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "left": "0dp",
            "masterType": constants.MASTER_TYPE_DEFAULT,
            "isModalContainer": false,
            "skin": "slFbox",
            "top": "0dp",
            "width": "100%",
            "overrides": {
                "lblSectionHeader": {
                    "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmTrackApplication.PersonalDetails\")",
                    "top": "0dp"
                }
            }
        }, controller.args[0], "loansSectionHeader"), extendConfig({
            "overrides": {}
        }, controller.args[1], "loansSectionHeader"), extendConfig({
            "overrides": {}
        }, controller.args[2], "loansSectionHeader"));
        var flxPersonalInfo = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
            "clipBounds": true,
            "id": "flxPersonalInfo",
            "isVisible": true,
            "layoutType": kony.flex.FLOW_VERTICAL,
            "left": "20dp",
            "isModalContainer": false,
            "right": "20dp",
            "skin": "slFbox",
            "top": "42dp",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, controller.args[0], "flxPersonalInfo"), extendConfig({}, controller.args[1], "flxPersonalInfo"), extendConfig({}, controller.args[2], "flxPersonalInfo"));
        flxPersonalInfo.setDefaultUnit(kony.flex.DP);
        var flxRow1 = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
            "clipBounds": true,
            "id": "flxRow1",
            "isVisible": true,
            "layoutType": kony.flex.FLOW_HORIZONTAL,
            "left": "0dp",
            "isModalContainer": false,
            "skin": "slFbox",
            "top": "30dp",
            "width": "100%"
        }, controller.args[0], "flxRow1"), extendConfig({}, controller.args[1], "flxRow1"), extendConfig({}, controller.args[2], "flxRow1"));
        flxRow1.setDefaultUnit(kony.flex.DP);
        var flxFirstName = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
            "clipBounds": true,
            "id": "flxFirstName",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "isModalContainer": false,
            "skin": "slFbox",
            "width": "32%",
            "zIndex": 1
        }, controller.args[0], "flxFirstName"), extendConfig({}, controller.args[1], "flxFirstName"), extendConfig({}, controller.args[2], "flxFirstName"));
        flxFirstName.setDefaultUnit(kony.flex.DP);
        var lblName = new kony.ui.Label(extendConfig({
            "id": "lblName",
            "isVisible": true,
            "left": "0px",
            "skin": "sknLbl78818A11px",
            "i18n_text": "kony.i18n.getLocalizedString(\"i18n.Applications.FirstNameCAPS\")",
            "top": "0px",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, controller.args[0], "lblName"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblName"), extendConfig({}, controller.args[2], "lblName"));
        var lblNameValue = new kony.ui.Label(extendConfig({
            "id": "lblNameValue",
            "isVisible": true,
            "left": "0px",
            "skin": "sknLbl485C75LatoRegular13Px",
            "text": "First Name",
            "top": "15dp",
            "width": "100%",
            "zIndex": 1
        }, controller.args[0], "lblNameValue"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblNameValue"), extendConfig({}, controller.args[2], "lblNameValue"));
        flxFirstName.add(lblName, lblNameValue);
        var flxMiddleName = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
            "clipBounds": true,
            "id": "flxMiddleName",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "left": "2%",
            "isModalContainer": false,
            "skin": "slFbox",
            "width": "32%",
            "zIndex": 1
        }, controller.args[0], "flxMiddleName"), extendConfig({}, controller.args[1], "flxMiddleName"), extendConfig({}, controller.args[2], "flxMiddleName"));
        flxMiddleName.setDefaultUnit(kony.flex.DP);
        var lblMiddleNameStatic = new kony.ui.Label(extendConfig({
            "id": "lblMiddleNameStatic",
            "isVisible": true,
            "left": "0px",
            "skin": "sknLbl78818A11px",
            "i18n_text": "kony.i18n.getLocalizedString(\"i18n.Applications.MiddleNameOptional\")",
            "top": "0px",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, controller.args[0], "lblMiddleNameStatic"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblMiddleNameStatic"), extendConfig({}, controller.args[2], "lblMiddleNameStatic"));
        var lblMiddleNameValue = new kony.ui.Label(extendConfig({
            "id": "lblMiddleNameValue",
            "isVisible": true,
            "left": "0px",
            "skin": "sknLbl485C75LatoRegular13Px",
            "text": "Middle Name",
            "top": "15dp",
            "width": "100%",
            "zIndex": 1
        }, controller.args[0], "lblMiddleNameValue"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblMiddleNameValue"), extendConfig({}, controller.args[2], "lblMiddleNameValue"));
        flxMiddleName.add(lblMiddleNameStatic, lblMiddleNameValue);
        var flxLastName = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
            "clipBounds": true,
            "id": "flxLastName",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "left": "2%",
            "isModalContainer": false,
            "skin": "slFbox",
            "width": "32%",
            "zIndex": 1
        }, controller.args[0], "flxLastName"), extendConfig({}, controller.args[1], "flxLastName"), extendConfig({}, controller.args[2], "flxLastName"));
        flxLastName.setDefaultUnit(kony.flex.DP);
        var lbllastName = new kony.ui.Label(extendConfig({
            "id": "lbllastName",
            "isVisible": true,
            "left": "0px",
            "skin": "sknLbl78818A11px",
            "i18n_text": "kony.i18n.getLocalizedString(\"i18n.Applications.LastNameCAPS\")",
            "top": "0px",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, controller.args[0], "lbllastName"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lbllastName"), extendConfig({}, controller.args[2], "lbllastName"));
        var lblLastNameValue = new kony.ui.Label(extendConfig({
            "id": "lblLastNameValue",
            "isVisible": true,
            "left": "0px",
            "skin": "sknLbl485C75LatoRegular13Px",
            "text": "Last Name",
            "top": "15dp",
            "width": "100%",
            "zIndex": 1
        }, controller.args[0], "lblLastNameValue"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblLastNameValue"), extendConfig({}, controller.args[2], "lblLastNameValue"));
        flxLastName.add(lbllastName, lblLastNameValue);
        flxRow1.add(flxFirstName, flxMiddleName, flxLastName);
        var flxRow2 = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
            "clipBounds": true,
            "id": "flxRow2",
            "isVisible": true,
            "layoutType": kony.flex.FLOW_HORIZONTAL,
            "left": "0dp",
            "isModalContainer": false,
            "skin": "slFbox",
            "top": "30dp",
            "width": "100%"
        }, controller.args[0], "flxRow2"), extendConfig({}, controller.args[1], "flxRow2"), extendConfig({}, controller.args[2], "flxRow2"));
        flxRow2.setDefaultUnit(kony.flex.DP);
        var flxSuffix = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
            "bottom": "0px",
            "clipBounds": true,
            "id": "flxSuffix",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "isModalContainer": false,
            "skin": "slFbox",
            "top": "0dp",
            "width": "32%",
            "zIndex": 1
        }, controller.args[0], "flxSuffix"), extendConfig({}, controller.args[1], "flxSuffix"), extendConfig({}, controller.args[2], "flxSuffix"));
        flxSuffix.setDefaultUnit(kony.flex.DP);
        var lblSuffix = new kony.ui.Label(extendConfig({
            "id": "lblSuffix",
            "isVisible": true,
            "left": "0px",
            "skin": "sknLbl78818A11px",
            "i18n_text": "kony.i18n.getLocalizedString(\"i18n.Applications.SuffixCAPS\")",
            "top": "0px",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, controller.args[0], "lblSuffix"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblSuffix"), extendConfig({}, controller.args[2], "lblSuffix"));
        var lblSuffixValue = new kony.ui.Label(extendConfig({
            "id": "lblSuffixValue",
            "isVisible": true,
            "left": "0px",
            "skin": "sknLbl485C75LatoRegular13Px",
            "text": "SR.",
            "top": "15dp",
            "width": "100%",
            "zIndex": 1
        }, controller.args[0], "lblSuffixValue"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblSuffixValue"), extendConfig({}, controller.args[2], "lblSuffixValue"));
        flxSuffix.add(lblSuffix, lblSuffixValue);
        var flxDateOfBirthPersonalInfo = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
            "clipBounds": true,
            "id": "flxDateOfBirthPersonalInfo",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "left": "2%",
            "isModalContainer": false,
            "skin": "slFbox",
            "top": "0dp",
            "width": "32%",
            "zIndex": 1
        }, controller.args[0], "flxDateOfBirthPersonalInfo"), extendConfig({}, controller.args[1], "flxDateOfBirthPersonalInfo"), extendConfig({}, controller.args[2], "flxDateOfBirthPersonalInfo"));
        flxDateOfBirthPersonalInfo.setDefaultUnit(kony.flex.DP);
        var lblSateOfBirth = new kony.ui.Label(extendConfig({
            "id": "lblSateOfBirth",
            "isVisible": true,
            "left": "0dp",
            "skin": "sknLbl78818A11px",
            "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmAssistedOnboarding.DATEOFBIRTH\")",
            "top": "0px",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, controller.args[0], "lblSateOfBirth"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblSateOfBirth"), extendConfig({}, controller.args[2], "lblSateOfBirth"));
        var flxDropDownAdmin2 = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "clipBounds": true,
            "height": "40dp",
            "id": "flxDropDownAdmin2",
            "isVisible": false,
            "layoutType": kony.flex.FREE_FORM,
            "left": "0dp",
            "isModalContainer": false,
            "skin": "slFbox",
            "top": "20dp",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, controller.args[0], "flxDropDownAdmin2"), extendConfig({}, controller.args[1], "flxDropDownAdmin2"), extendConfig({}, controller.args[2], "flxDropDownAdmin2"));
        flxDropDownAdmin2.setDefaultUnit(kony.flex.DP);
        var flxCloseCalPersonalInfo = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "centerY": "50%",
            "clipBounds": true,
            "height": "10px",
            "id": "flxCloseCalPersonalInfo",
            "isVisible": false,
            "layoutType": kony.flex.FREE_FORM,
            "isModalContainer": false,
            "right": "10px",
            "skin": "slFbox",
            "width": "12px",
            "zIndex": 2
        }, controller.args[0], "flxCloseCalPersonalInfo"), extendConfig({}, controller.args[1], "flxCloseCalPersonalInfo"), extendConfig({
            "hoverSkin": "sknCursor"
        }, controller.args[2], "flxCloseCalPersonalInfo"));
        flxCloseCalPersonalInfo.setDefaultUnit(kony.flex.DP);
        var lblCloseCalPersonalInfo = new kony.ui.Label(extendConfig({
            "centerY": "50%",
            "id": "lblCloseCalPersonalInfo",
            "isVisible": true,
            "right": "0dp",
            "skin": "sknfontIconDescDownArrow12px",
            "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmAlertsManagement.lblAlertPreviewClose\")",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, controller.args[0], "lblCloseCalPersonalInfo"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblCloseCalPersonalInfo"), extendConfig({}, controller.args[2], "lblCloseCalPersonalInfo"));
        flxCloseCalPersonalInfo.add(lblCloseCalPersonalInfo);
        flxDropDownAdmin2.add(flxCloseCalPersonalInfo);
        var lblDobValue = new kony.ui.Label(extendConfig({
            "id": "lblDobValue",
            "isVisible": true,
            "left": "0px",
            "skin": "sknLbl485C75LatoRegular13Px",
            "text": "03/03/2003",
            "top": "15dp",
            "width": "100%",
            "zIndex": 1
        }, controller.args[0], "lblDobValue"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblDobValue"), extendConfig({}, controller.args[2], "lblDobValue"));
        flxDateOfBirthPersonalInfo.add(lblSateOfBirth, flxDropDownAdmin2, lblDobValue);
        var flxEmailAddress = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
            "clipBounds": true,
            "id": "flxEmailAddress",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "left": "2%",
            "isModalContainer": false,
            "skin": "slFbox",
            "top": "0dp",
            "width": "32%",
            "zIndex": 1
        }, controller.args[0], "flxEmailAddress"), extendConfig({}, controller.args[1], "flxEmailAddress"), extendConfig({}, controller.args[2], "flxEmailAddress"));
        flxEmailAddress.setDefaultUnit(kony.flex.DP);
        var lblEmailAddress = new kony.ui.Label(extendConfig({
            "id": "lblEmailAddress",
            "isVisible": true,
            "left": "0px",
            "skin": "sknLbl78818A11px",
            "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmAlertsManagement.EMAILCaps\")",
            "top": "0px",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, controller.args[0], "lblEmailAddress"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblEmailAddress"), extendConfig({}, controller.args[2], "lblEmailAddress"));
        var lblEmailAddressValue = new kony.ui.Label(extendConfig({
            "id": "lblEmailAddressValue",
            "isVisible": true,
            "left": "0px",
            "skin": "sknLbl485C75LatoRegular13Px",
            "text": "test@test.com",
            "top": "15dp",
            "width": "100%",
            "zIndex": 1
        }, controller.args[0], "lblEmailAddressValue"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblEmailAddressValue"), extendConfig({}, controller.args[2], "lblEmailAddressValue"));
        flxEmailAddress.add(lblEmailAddress, lblEmailAddressValue);
        flxRow2.add(flxSuffix, flxDateOfBirthPersonalInfo, flxEmailAddress);
        var flxRow3 = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
            "clipBounds": true,
            "id": "flxRow3",
            "isVisible": true,
            "layoutType": kony.flex.FLOW_HORIZONTAL,
            "left": "0dp",
            "isModalContainer": false,
            "skin": "slFbox",
            "top": "30dp",
            "width": "100%"
        }, controller.args[0], "flxRow3"), extendConfig({}, controller.args[1], "flxRow3"), extendConfig({}, controller.args[2], "flxRow3"));
        flxRow3.setDefaultUnit(kony.flex.DP);
        var flxSSN = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
            "clipBounds": true,
            "id": "flxSSN",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "isModalContainer": false,
            "skin": "slFbox",
            "top": "0dp",
            "width": "32%",
            "zIndex": 1
        }, controller.args[0], "flxSSN"), extendConfig({}, controller.args[1], "flxSSN"), extendConfig({}, controller.args[2], "flxSSN"));
        flxSSN.setDefaultUnit(kony.flex.DP);
        var lblSSN = new kony.ui.Label(extendConfig({
            "id": "lblSSN",
            "isVisible": true,
            "left": "0px",
            "skin": "sknLbl78818A11px",
            "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmCustomerManagementController.SSN\")",
            "top": "0px",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, controller.args[0], "lblSSN"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblSSN"), extendConfig({}, controller.args[2], "lblSSN"));
        var lblSSNValue = new kony.ui.Label(extendConfig({
            "id": "lblSSNValue",
            "isVisible": true,
            "left": "0px",
            "skin": "sknLbl485C75LatoRegular13Px",
            "text": "12345",
            "top": "15dp",
            "width": "100%",
            "zIndex": 1
        }, controller.args[0], "lblSSNValue"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblSSNValue"), extendConfig({}, controller.args[2], "lblSSNValue"));
        flxSSN.add(lblSSN, lblSSNValue);
        var flxDLNumber = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
            "clipBounds": true,
            "id": "flxDLNumber",
            "isVisible": false,
            "layoutType": kony.flex.FREE_FORM,
            "left": "2%",
            "isModalContainer": false,
            "skin": "slFbox",
            "top": "0dp",
            "width": "32%",
            "zIndex": 1
        }, controller.args[0], "flxDLNumber"), extendConfig({}, controller.args[1], "flxDLNumber"), extendConfig({}, controller.args[2], "flxDLNumber"));
        flxDLNumber.setDefaultUnit(kony.flex.DP);
        var lblDLNumber = new kony.ui.Label(extendConfig({
            "id": "lblDLNumber",
            "isVisible": true,
            "left": "0px",
            "skin": "sknLbl78818A11px",
            "i18n_text": "kony.i18n.getLocalizedString(\"i18n.Applications.DriverLicenseOptional\")",
            "top": "0px",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, controller.args[0], "lblDLNumber"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblDLNumber"), extendConfig({}, controller.args[2], "lblDLNumber"));
        var lblDLNumberValue = new kony.ui.Label(extendConfig({
            "id": "lblDLNumberValue",
            "isVisible": true,
            "left": "0px",
            "skin": "sknLbl485C75LatoRegular13Px",
            "text": "12345",
            "top": "15dp",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, controller.args[0], "lblDLNumberValue"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblDLNumberValue"), extendConfig({}, controller.args[2], "lblDLNumberValue"));
        flxDLNumber.add(lblDLNumber, lblDLNumberValue);
        var flxState = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
            "bottom": "0px",
            "clipBounds": true,
            "id": "flxState",
            "isVisible": false,
            "layoutType": kony.flex.FREE_FORM,
            "left": "2%",
            "isModalContainer": false,
            "skin": "slFbox",
            "top": "0dp",
            "width": "32%",
            "zIndex": 1
        }, controller.args[0], "flxState"), extendConfig({}, controller.args[1], "flxState"), extendConfig({}, controller.args[2], "flxState"));
        flxState.setDefaultUnit(kony.flex.DP);
        var lblState = new kony.ui.Label(extendConfig({
            "id": "lblState",
            "isVisible": true,
            "left": "0px",
            "skin": "sknLbl78818A11px",
            "i18n_text": "kony.i18n.getLocalizedString(\"i18n.Applications.StateOptional\")",
            "top": "0px",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, controller.args[0], "lblState"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblState"), extendConfig({}, controller.args[2], "lblState"));
        var lblStateValue = new kony.ui.Label(extendConfig({
            "id": "lblStateValue",
            "isVisible": true,
            "left": "0px",
            "skin": "sknLbl485C75LatoRegular13Px",
            "text": "CA",
            "top": "15dp",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, controller.args[0], "lblStateValue"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblStateValue"), extendConfig({}, controller.args[2], "lblStateValue"));
        flxState.add(lblState, lblStateValue);
        flxRow3.add(flxSSN, flxDLNumber, flxState);
        flxPersonalInfo.add(flxRow1, flxRow2, flxRow3);
        viewPersonalInfo.add(loansSectionHeader, flxPersonalInfo);
        return viewPersonalInfo;
    }
})