define(function() {
    return function(controller) {
        var viewIncomeSource = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
            "clipBounds": true,
            "isMaster": true,
            "id": "viewIncomeSource",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "left": "0dp",
            "isModalContainer": false,
            "skin": "slFbox",
            "top": "30dp",
            "width": "100%"
        }, controller.args[0], "viewIncomeSource"), extendConfig({}, controller.args[1], "viewIncomeSource"), extendConfig({}, controller.args[2], "viewIncomeSource"));
        viewIncomeSource.setDefaultUnit(kony.flex.DP);
        var loansSectionHeader = new com.adminConsole.loans.applications.loansSectionHeader(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
            "id": "loansSectionHeader",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "left": "0dp",
            "masterType": constants.MASTER_TYPE_DEFAULT,
            "isModalContainer": false,
            "skin": "slFbox",
            "top": "0dp",
            "width": "100%",
            "overrides": {
                "lblSectionHeader": {
                    "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmTrackApplication.IncomeSource\")"
                },
                "loansSectionHeader": {
                    "top": "0dp"
                }
            }
        }, controller.args[0], "loansSectionHeader"), extendConfig({
            "overrides": {}
        }, controller.args[1], "loansSectionHeader"), extendConfig({
            "overrides": {}
        }, controller.args[2], "loansSectionHeader"));
        var flxIncomeSource = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
            "clipBounds": true,
            "id": "flxIncomeSource",
            "isVisible": true,
            "layoutType": kony.flex.FLOW_VERTICAL,
            "left": "20dp",
            "isModalContainer": false,
            "right": "20dp",
            "skin": "slFbox",
            "top": "42dp",
            "zIndex": 1
        }, controller.args[0], "flxIncomeSource"), extendConfig({}, controller.args[1], "flxIncomeSource"), extendConfig({}, controller.args[2], "flxIncomeSource"));
        flxIncomeSource.setDefaultUnit(kony.flex.DP);
        var flxEmpStatus = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
            "clipBounds": true,
            "id": "flxEmpStatus",
            "isVisible": true,
            "layoutType": kony.flex.FLOW_VERTICAL,
            "left": "0dp",
            "isModalContainer": false,
            "skin": "slFbox",
            "top": "30dp",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, controller.args[0], "flxEmpStatus"), extendConfig({}, controller.args[1], "flxEmpStatus"), extendConfig({}, controller.args[2], "flxEmpStatus"));
        flxEmpStatus.setDefaultUnit(kony.flex.DP);
        var lblEmpStatus = new kony.ui.Label(extendConfig({
            "id": "lblEmpStatus",
            "isVisible": true,
            "left": "0px",
            "skin": "sknLbl78818A11px",
            "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmTrackApplication.EmploymentStatusHeaderCAPS\")",
            "top": "0px",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, controller.args[0], "lblEmpStatus"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblEmpStatus"), extendConfig({}, controller.args[2], "lblEmpStatus"));
        var lblEmpStatusValue = new kony.ui.Label(extendConfig({
            "id": "lblEmpStatusValue",
            "isVisible": true,
            "left": "0px",
            "skin": "sknLbl485C75LatoRegular13Px",
            "text": "Fulltime Employed",
            "top": "15dp",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, controller.args[0], "lblEmpStatusValue"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblEmpStatusValue"), extendConfig({}, controller.args[2], "lblEmpStatusValue"));
        flxEmpStatus.add(lblEmpStatus, lblEmpStatusValue);
        flxIncomeSource.add(flxEmpStatus);
        viewIncomeSource.add(loansSectionHeader, flxIncomeSource);
        return viewIncomeSource;
    }
})