define(function() {
    return function(controller) {
        var viewContactInfo = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
            "clipBounds": true,
            "isMaster": true,
            "id": "viewContactInfo",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "left": "0dp",
            "isModalContainer": false,
            "skin": "slFbox",
            "top": "30dp",
            "width": "100%"
        }, controller.args[0], "viewContactInfo"), extendConfig({}, controller.args[1], "viewContactInfo"), extendConfig({}, controller.args[2], "viewContactInfo"));
        viewContactInfo.setDefaultUnit(kony.flex.DP);
        var loansSectionHeader = new com.adminConsole.loans.applications.loansSectionHeader(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
            "id": "loansSectionHeader",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "left": "0dp",
            "masterType": constants.MASTER_TYPE_DEFAULT,
            "isModalContainer": false,
            "skin": "slFbox",
            "top": "0dp",
            "width": "100%",
            "overrides": {
                "lblSectionHeader": {
                    "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmTrackApplication.ContactDetails\")"
                }
            }
        }, controller.args[0], "loansSectionHeader"), extendConfig({
            "overrides": {}
        }, controller.args[1], "loansSectionHeader"), extendConfig({
            "overrides": {}
        }, controller.args[2], "loansSectionHeader"));
        var flxContactInfo = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
            "clipBounds": true,
            "id": "flxContactInfo",
            "isVisible": true,
            "layoutType": kony.flex.FLOW_VERTICAL,
            "left": "20dp",
            "isModalContainer": false,
            "right": "20dp",
            "skin": "slFbox",
            "top": "42dp",
            "zIndex": 1
        }, controller.args[0], "flxContactInfo"), extendConfig({}, controller.args[1], "flxContactInfo"), extendConfig({}, controller.args[2], "flxContactInfo"));
        flxContactInfo.setDefaultUnit(kony.flex.DP);
        var flxRow1 = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
            "clipBounds": true,
            "id": "flxRow1",
            "isVisible": true,
            "layoutType": kony.flex.FLOW_HORIZONTAL,
            "left": "0dp",
            "isModalContainer": false,
            "skin": "slFbox",
            "top": "30dp",
            "width": "100%"
        }, controller.args[0], "flxRow1"), extendConfig({}, controller.args[1], "flxRow1"), extendConfig({}, controller.args[2], "flxRow1"));
        flxRow1.setDefaultUnit(kony.flex.DP);
        var flxHomePhone = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
            "clipBounds": true,
            "id": "flxHomePhone",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "isModalContainer": false,
            "skin": "slFbox",
            "width": "32%",
            "zIndex": 1
        }, controller.args[0], "flxHomePhone"), extendConfig({}, controller.args[1], "flxHomePhone"), extendConfig({}, controller.args[2], "flxHomePhone"));
        flxHomePhone.setDefaultUnit(kony.flex.DP);
        var lblHomePhone = new kony.ui.Label(extendConfig({
            "id": "lblHomePhone",
            "isVisible": true,
            "left": "0px",
            "skin": "sknLbl78818A11px",
            "i18n_text": "kony.i18n.getLocalizedString(\"i18n.Applications.HomePhoneCAPS\")",
            "top": "0px",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, controller.args[0], "lblHomePhone"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblHomePhone"), extendConfig({}, controller.args[2], "lblHomePhone"));
        var lblHomePhoneValue = new kony.ui.Label(extendConfig({
            "id": "lblHomePhoneValue",
            "isVisible": true,
            "left": "0px",
            "skin": "sknLbl485C75LatoRegular13Px",
            "text": "123456",
            "top": "15dp",
            "width": "100%",
            "zIndex": 1
        }, controller.args[0], "lblHomePhoneValue"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblHomePhoneValue"), extendConfig({}, controller.args[2], "lblHomePhoneValue"));
        flxHomePhone.add(lblHomePhone, lblHomePhoneValue);
        var flxBusinessPhone = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
            "clipBounds": true,
            "id": "flxBusinessPhone",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "left": "2%",
            "isModalContainer": false,
            "skin": "slFbox",
            "width": "32%",
            "zIndex": 1
        }, controller.args[0], "flxBusinessPhone"), extendConfig({}, controller.args[1], "flxBusinessPhone"), extendConfig({}, controller.args[2], "flxBusinessPhone"));
        flxBusinessPhone.setDefaultUnit(kony.flex.DP);
        var lblBusinessPhone = new kony.ui.Label(extendConfig({
            "id": "lblBusinessPhone",
            "isVisible": true,
            "left": "0px",
            "skin": "sknLbl78818A11px",
            "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmLoansDashboard.BusinessPhoneCAPS\")",
            "top": "0px",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, controller.args[0], "lblBusinessPhone"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblBusinessPhone"), extendConfig({}, controller.args[2], "lblBusinessPhone"));
        var lblBusinessPhoneValue = new kony.ui.Label(extendConfig({
            "id": "lblBusinessPhoneValue",
            "isVisible": true,
            "left": "0px",
            "skin": "sknLbl485C75LatoRegular13Px",
            "text": "123456",
            "top": "15dp",
            "width": "100%",
            "zIndex": 1
        }, controller.args[0], "lblBusinessPhoneValue"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblBusinessPhoneValue"), extendConfig({}, controller.args[2], "lblBusinessPhoneValue"));
        flxBusinessPhone.add(lblBusinessPhone, lblBusinessPhoneValue);
        var flxCellPhone = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
            "clipBounds": true,
            "id": "flxCellPhone",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "left": "2%",
            "isModalContainer": false,
            "skin": "slFbox",
            "width": "32%",
            "zIndex": 1
        }, controller.args[0], "flxCellPhone"), extendConfig({}, controller.args[1], "flxCellPhone"), extendConfig({}, controller.args[2], "flxCellPhone"));
        flxCellPhone.setDefaultUnit(kony.flex.DP);
        var lblCellPhone = new kony.ui.Label(extendConfig({
            "id": "lblCellPhone",
            "isVisible": true,
            "left": "0px",
            "skin": "sknLbl78818A11px",
            "i18n_text": "kony.i18n.getLocalizedString(\"i18n.Applications.MobileNumberCAPS\")",
            "top": "0px",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, controller.args[0], "lblCellPhone"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblCellPhone"), extendConfig({}, controller.args[2], "lblCellPhone"));
        var lblCellPhoneValue = new kony.ui.Label(extendConfig({
            "id": "lblCellPhoneValue",
            "isVisible": true,
            "left": "0px",
            "skin": "sknLbl485C75LatoRegular13Px",
            "text": "123456",
            "top": "15dp",
            "width": "100%",
            "zIndex": 1
        }, controller.args[0], "lblCellPhoneValue"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblCellPhoneValue"), extendConfig({}, controller.args[2], "lblCellPhoneValue"));
        flxCellPhone.add(lblCellPhone, lblCellPhoneValue);
        flxRow1.add(flxHomePhone, flxBusinessPhone, flxCellPhone);
        var flxPrimaryContact = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
            "clipBounds": true,
            "id": "flxPrimaryContact",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "left": "0dp",
            "isModalContainer": false,
            "skin": "slFbox",
            "top": "30dp",
            "width": "32%",
            "zIndex": 1
        }, controller.args[0], "flxPrimaryContact"), extendConfig({}, controller.args[1], "flxPrimaryContact"), extendConfig({}, controller.args[2], "flxPrimaryContact"));
        flxPrimaryContact.setDefaultUnit(kony.flex.DP);
        var lblPrimaryContact = new kony.ui.Label(extendConfig({
            "id": "lblPrimaryContact",
            "isVisible": true,
            "left": "0px",
            "skin": "sknLbl78818A11px",
            "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmLoansDashboard.PrimaryContactMethodCAPS\")",
            "top": "0px",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, controller.args[0], "lblPrimaryContact"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblPrimaryContact"), extendConfig({}, controller.args[2], "lblPrimaryContact"));
        var lblPrimaryContactValue = new kony.ui.Label(extendConfig({
            "id": "lblPrimaryContactValue",
            "isVisible": true,
            "left": "0px",
            "skin": "sknLbl485C75LatoRegular13Px",
            "text": "Cell Phone",
            "top": "15dp",
            "width": "100%",
            "zIndex": 1
        }, controller.args[0], "lblPrimaryContactValue"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblPrimaryContactValue"), extendConfig({}, controller.args[2], "lblPrimaryContactValue"));
        flxPrimaryContact.add(lblPrimaryContact, lblPrimaryContactValue);
        flxContactInfo.add(flxRow1, flxPrimaryContact);
        viewContactInfo.add(loansSectionHeader, flxContactInfo);
        return viewContactInfo;
    }
})