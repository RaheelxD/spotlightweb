define(function() {
    return function(controller) {
        var viewAUDetails = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
            "clipBounds": true,
            "isMaster": true,
            "id": "viewAUDetails",
            "isVisible": true,
            "layoutType": kony.flex.FLOW_VERTICAL,
            "left": "0dp",
            "isModalContainer": false,
            "right": 0,
            "skin": "slFbox",
            "top": "40dp",
            "width": kony.flex.USE_PREFFERED_SIZE
        }, controller.args[0], "viewAUDetails"), extendConfig({}, controller.args[1], "viewAUDetails"), extendConfig({}, controller.args[2], "viewAUDetails"));
        viewAUDetails.setDefaultUnit(kony.flex.DP);
        var loansSectionHeader = new com.adminConsole.loans.applications.loansSectionHeader(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
            "id": "loansSectionHeader",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "left": "0dp",
            "masterType": constants.MASTER_TYPE_DEFAULT,
            "isModalContainer": false,
            "skin": "slFbox",
            "top": "0dp",
            "width": "100%",
            "overrides": {
                "lblSectionHeader": {
                    "text": "Authorized User"
                }
            }
        }, controller.args[0], "loansSectionHeader"), extendConfig({
            "overrides": {}
        }, controller.args[1], "loansSectionHeader"), extendConfig({
            "overrides": {}
        }, controller.args[2], "loansSectionHeader"));
        var flxAddAU = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
            "clipBounds": true,
            "id": "flxAddAU",
            "isVisible": true,
            "layoutType": kony.flex.FLOW_VERTICAL,
            "left": "20dp",
            "isModalContainer": false,
            "right": "20dp",
            "skin": "slFbox",
            "top": "40dp",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, controller.args[0], "flxAddAU"), extendConfig({}, controller.args[1], "flxAddAU"), extendConfig({}, controller.args[2], "flxAddAU"));
        flxAddAU.setDefaultUnit(kony.flex.DP);
        var flxAddAUQuestion = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
            "clipBounds": true,
            "id": "flxAddAUQuestion",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "left": "0",
            "isModalContainer": false,
            "skin": "slFbox",
            "top": "0",
            "width": "100%",
            "zIndex": 1
        }, controller.args[0], "flxAddAUQuestion"), extendConfig({}, controller.args[1], "flxAddAUQuestion"), extendConfig({}, controller.args[2], "flxAddAUQuestion"));
        flxAddAUQuestion.setDefaultUnit(kony.flex.DP);
        var lblAddAU = new kony.ui.Label(extendConfig({
            "id": "lblAddAU",
            "isVisible": true,
            "left": "0px",
            "skin": "sknLbl78818A11px",
            "text": "WOULD YOU LIKE TO ADD AUTHORIZED USER",
            "top": "0px",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, controller.args[0], "lblAddAU"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblAddAU"), extendConfig({}, controller.args[2], "lblAddAU"));
        var lblAUValue = new kony.ui.Label(extendConfig({
            "id": "lblAUValue",
            "isVisible": true,
            "left": "0px",
            "skin": "sknLbl485C75LatoRegular13Px",
            "text": "NO",
            "top": "15dp",
            "width": "100%",
            "zIndex": 1
        }, controller.args[0], "lblAUValue"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblAUValue"), extendConfig({}, controller.args[2], "lblAUValue"));
        flxAddAUQuestion.add(lblAddAU, lblAUValue);
        flxAddAU.add(flxAddAUQuestion);
        var viewAUPersonalInfo = new com.adminConsole.loans.applications.viewPersonalInfo(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
            "id": "viewAUPersonalInfo",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "left": "0dp",
            "masterType": constants.MASTER_TYPE_DEFAULT,
            "isModalContainer": false,
            "skin": "slFbox",
            "top": "40dp",
            "width": "100%",
            "overrides": {}
        }, controller.args[0], "viewAUPersonalInfo"), extendConfig({
            "overrides": {}
        }, controller.args[1], "viewAUPersonalInfo"), extendConfig({
            "overrides": {}
        }, controller.args[2], "viewAUPersonalInfo"));
        var viewAUContactInfo = new com.adminConsole.loans.applications.viewContactInfo(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
            "id": "viewAUContactInfo",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "left": "0dp",
            "masterType": constants.MASTER_TYPE_DEFAULT,
            "isModalContainer": false,
            "skin": "slFbox",
            "top": "40dp",
            "width": "100%",
            "overrides": {}
        }, controller.args[0], "viewAUContactInfo"), extendConfig({
            "overrides": {}
        }, controller.args[1], "viewAUContactInfo"), extendConfig({
            "overrides": {}
        }, controller.args[2], "viewAUContactInfo"));
        var viewAUAddressInfo = new com.adminConsole.loans.applications.viewAddressInfo(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
            "id": "viewAUAddressInfo",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "left": "0dp",
            "masterType": constants.MASTER_TYPE_DEFAULT,
            "isModalContainer": false,
            "skin": "slFbox",
            "top": "40dp",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "overrides": {}
        }, controller.args[0], "viewAUAddressInfo"), extendConfig({
            "overrides": {}
        }, controller.args[1], "viewAUAddressInfo"), extendConfig({
            "overrides": {}
        }, controller.args[2], "viewAUAddressInfo"));
        viewAUDetails.add(loansSectionHeader, flxAddAU, viewAUPersonalInfo, viewAUContactInfo, viewAUAddressInfo);
        return viewAUDetails;
    }
})