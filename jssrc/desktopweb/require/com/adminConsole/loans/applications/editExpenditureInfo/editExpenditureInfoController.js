define("com/adminConsole/loans/applications/editExpenditureInfo/usereditExpenditureInfoController", function() {
    return {
        editExpenditurePreshow: function() {
            var scopeObj = this;
            scopeObj.view.rentValueErrorMsg.setVisibility(false);
            scopeObj.view.txtRentValue.skin = "skntxtbxDetails0bbf1235271384a";
            scopeObj.view.mortgageValueErrorMsg.setVisibility(false);
            scopeObj.view.txtMortgageValue.skin = "skntxtbxDetails0bbf1235271384a";
        },
    };
});
define("com/adminConsole/loans/applications/editExpenditureInfo/editExpenditureInfoControllerActions", {
    /*
      This is an auto generated file and any modifications to it may result in corruption of the action sequence.
    */
    /** preShow defined for editExpenditureInfo **/
    AS_FlexContainer_cf60d3314ed24acbb94d4679219342ae: function AS_FlexContainer_cf60d3314ed24acbb94d4679219342ae(eventobject) {
        var self = this;
    }
});
define("com/adminConsole/loans/applications/editExpenditureInfo/editExpenditureInfoController", ["com/adminConsole/loans/applications/editExpenditureInfo/usereditExpenditureInfoController", "com/adminConsole/loans/applications/editExpenditureInfo/editExpenditureInfoControllerActions"], function() {
    var controller = require("com/adminConsole/loans/applications/editExpenditureInfo/usereditExpenditureInfoController");
    var actions = require("com/adminConsole/loans/applications/editExpenditureInfo/editExpenditureInfoControllerActions");
    for (var key in actions) {
        controller[key] = actions[key];
    }
    return controller;
});
