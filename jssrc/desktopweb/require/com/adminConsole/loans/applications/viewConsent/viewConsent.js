define(function() {
    return function(controller) {
        var viewConsent = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
            "clipBounds": true,
            "isMaster": true,
            "id": "viewConsent",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "left": "0dp",
            "isModalContainer": false,
            "skin": "slFbox",
            "top": "40dp",
            "width": kony.flex.USE_PREFFERED_SIZE
        }, controller.args[0], "viewConsent"), extendConfig({}, controller.args[1], "viewConsent"), extendConfig({}, controller.args[2], "viewConsent"));
        viewConsent.setDefaultUnit(kony.flex.DP);
        var loansSectionHeader = new com.adminConsole.loans.applications.loansSectionHeader(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
            "id": "loansSectionHeader",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "left": "0dp",
            "masterType": constants.MASTER_TYPE_DEFAULT,
            "isModalContainer": false,
            "skin": "slFbox",
            "top": "0dp",
            "width": "100%",
            "overrides": {
                "lblSectionHeader": {
                    "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmLoansDashboard.Consent\")"
                }
            }
        }, controller.args[0], "loansSectionHeader"), extendConfig({
            "overrides": {}
        }, controller.args[1], "loansSectionHeader"), extendConfig({
            "overrides": {}
        }, controller.args[2], "loansSectionHeader"));
        var flxConsent = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
            "bottom": "30dp",
            "clipBounds": true,
            "id": "flxConsent",
            "isVisible": true,
            "layoutType": kony.flex.FLOW_HORIZONTAL,
            "left": "20dp",
            "isModalContainer": false,
            "right": 20,
            "skin": "slFbox",
            "top": "72dp",
            "width": kony.flex.USE_PREFFERED_SIZE
        }, controller.args[0], "flxConsent"), extendConfig({}, controller.args[1], "flxConsent"), extendConfig({}, controller.args[2], "flxConsent"));
        flxConsent.setDefaultUnit(kony.flex.DP);
        var lblIconGreenTick = new kony.ui.Label(extendConfig({
            "height": "15dp",
            "id": "lblIconGreenTick",
            "isVisible": true,
            "left": "0dp",
            "skin": "sknIconGreen16px",
            "text": "",
            "top": "0dp",
            "width": "15dp",
            "zIndex": 1
        }, controller.args[0], "lblIconGreenTick"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblIconGreenTick"), extendConfig({}, controller.args[2], "lblIconGreenTick"));
        var lblConsentMsg = new kony.ui.Label(extendConfig({
            "id": "lblConsentMsg",
            "isVisible": true,
            "left": "10dp",
            "skin": "sknLbl485C75LatoRegular13Px",
            "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmLoansDashboard.I\")",
            "top": "0dp",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, controller.args[0], "lblConsentMsg"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblConsentMsg"), extendConfig({}, controller.args[2], "lblConsentMsg"));
        flxConsent.add(lblIconGreenTick, lblConsentMsg);
        viewConsent.add(loansSectionHeader, flxConsent);
        return viewConsent;
    }
})