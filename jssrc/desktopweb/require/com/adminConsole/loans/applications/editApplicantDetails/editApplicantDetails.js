define(function() {
    return function(controller) {
        var editApplicantDetails = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
            "clipBounds": true,
            "isMaster": true,
            "id": "editApplicantDetails",
            "isVisible": true,
            "layoutType": kony.flex.FLOW_VERTICAL,
            "left": "0dp",
            "isModalContainer": false,
            "skin": "slFbox",
            "top": "0dp",
            "width": kony.flex.USE_PREFFERED_SIZE
        }, controller.args[0], "editApplicantDetails"), extendConfig({}, controller.args[1], "editApplicantDetails"), extendConfig({}, controller.args[2], "editApplicantDetails"));
        editApplicantDetails.setDefaultUnit(kony.flex.DP);
        var lblApplicantHeader = new kony.ui.Label(extendConfig({
            "id": "lblApplicantHeader",
            "isVisible": true,
            "left": "20dp",
            "skin": "LblLatoRegular485c7516px",
            "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmLoansDashboard.Summary.LoanInfo.ApplicantDetails\")",
            "top": "40dp",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 10
        }, controller.args[0], "lblApplicantHeader"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblApplicantHeader"), extendConfig({}, controller.args[2], "lblApplicantHeader"));
        var editPersonalInfo = new com.adminConsole.loans.applications.editPersonalInfo(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
            "id": "editPersonalInfo",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "left": "0dp",
            "masterType": constants.MASTER_TYPE_DEFAULT,
            "isModalContainer": false,
            "skin": "slFbox",
            "top": "40dp",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "overrides": {}
        }, controller.args[0], "editPersonalInfo"), extendConfig({
            "overrides": {}
        }, controller.args[1], "editPersonalInfo"), extendConfig({
            "overrides": {}
        }, controller.args[2], "editPersonalInfo"));
        var editContactInfo = new com.adminConsole.loans.applications.editContactInfo(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
            "id": "editContactInfo",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "left": "0dp",
            "masterType": constants.MASTER_TYPE_DEFAULT,
            "isModalContainer": false,
            "skin": "slFbox",
            "top": "40dp",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "overrides": {}
        }, controller.args[0], "editContactInfo"), extendConfig({
            "overrides": {}
        }, controller.args[1], "editContactInfo"), extendConfig({
            "overrides": {}
        }, controller.args[2], "editContactInfo"));
        var editAddressInfo = new com.adminConsole.loans.applications.editAddressInfo(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
            "id": "editAddressInfo",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "left": "0dp",
            "masterType": constants.MASTER_TYPE_DEFAULT,
            "isModalContainer": false,
            "skin": "slFbox",
            "top": "40dp",
            "width": "100%",
            "overrides": {}
        }, controller.args[0], "editAddressInfo"), extendConfig({
            "overrides": {}
        }, controller.args[1], "editAddressInfo"), extendConfig({
            "overrides": {}
        }, controller.args[2], "editAddressInfo"));
        var editEmployementInfo = new com.adminConsole.loans.applications.editEmployementInfo(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
            "id": "editEmployementInfo",
            "isVisible": true,
            "layoutType": kony.flex.FLOW_VERTICAL,
            "left": "0dp",
            "masterType": constants.MASTER_TYPE_DEFAULT,
            "isModalContainer": false,
            "skin": "slFbox",
            "top": "0dp",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "overrides": {}
        }, controller.args[0], "editEmployementInfo"), extendConfig({
            "overrides": {}
        }, controller.args[1], "editEmployementInfo"), extendConfig({
            "overrides": {}
        }, controller.args[2], "editEmployementInfo"));
        var editAdditionalIncome = new com.adminConsole.loans.applications.editAdditionalIncome(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
            "id": "editAdditionalIncome",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "left": "0dp",
            "masterType": constants.MASTER_TYPE_DEFAULT,
            "isModalContainer": false,
            "skin": "slFbox",
            "top": "40dp",
            "width": "100%",
            "overrides": {}
        }, controller.args[0], "editAdditionalIncome"), extendConfig({
            "overrides": {}
        }, controller.args[1], "editAdditionalIncome"), extendConfig({
            "overrides": {}
        }, controller.args[2], "editAdditionalIncome"));
        var editExpenditureInfo = new com.adminConsole.loans.applications.editExpenditureInfo(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
            "id": "editExpenditureInfo",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "left": "0dp",
            "masterType": constants.MASTER_TYPE_DEFAULT,
            "isModalContainer": false,
            "skin": "slFbox",
            "top": "40dp",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "overrides": {}
        }, controller.args[0], "editExpenditureInfo"), extendConfig({
            "overrides": {}
        }, controller.args[1], "editExpenditureInfo"), extendConfig({
            "overrides": {}
        }, controller.args[2], "editExpenditureInfo"));
        editApplicantDetails.add(lblApplicantHeader, editPersonalInfo, editContactInfo, editAddressInfo, editEmployementInfo, editAdditionalIncome, editExpenditureInfo);
        return editApplicantDetails;
    }
})