define(function() {
    return function(controller) {
        var editAddressInfo = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
            "clipBounds": true,
            "isMaster": true,
            "id": "editAddressInfo",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "left": "0dp",
            "isModalContainer": false,
            "skin": "slFbox",
            "top": "30dp",
            "width": kony.flex.USE_PREFFERED_SIZE
        }, controller.args[0], "editAddressInfo"), extendConfig({}, controller.args[1], "editAddressInfo"), extendConfig({}, controller.args[2], "editAddressInfo"));
        editAddressInfo.setDefaultUnit(kony.flex.DP);
        var loansSectionHeader = new com.adminConsole.loans.applications.loansSectionHeader(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
            "id": "loansSectionHeader",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "left": "0dp",
            "masterType": constants.MASTER_TYPE_DEFAULT,
            "isModalContainer": false,
            "skin": "slFbox",
            "top": "0dp",
            "width": "100%",
            "overrides": {
                "btnEdit": {
                    "isVisible": false
                },
                "lblSectionHeader": {
                    "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmTrackApplication.PresentAddress\")"
                }
            }
        }, controller.args[0], "loansSectionHeader"), extendConfig({
            "overrides": {}
        }, controller.args[1], "loansSectionHeader"), extendConfig({
            "overrides": {}
        }, controller.args[2], "loansSectionHeader"));
        var flxAddressInfoSection = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
            "clipBounds": true,
            "id": "flxAddressInfoSection",
            "isVisible": true,
            "layoutType": kony.flex.FLOW_VERTICAL,
            "left": "20dp",
            "isModalContainer": false,
            "right": "20dp",
            "skin": "slFbox",
            "top": "42dp",
            "zIndex": 1
        }, controller.args[0], "flxAddressInfoSection"), extendConfig({}, controller.args[1], "flxAddressInfoSection"), extendConfig({}, controller.args[2], "flxAddressInfoSection"));
        flxAddressInfoSection.setDefaultUnit(kony.flex.DP);
        var flxAddressInfo = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
            "clipBounds": true,
            "id": "flxAddressInfo",
            "isVisible": true,
            "layoutType": kony.flex.FLOW_VERTICAL,
            "left": "0dp",
            "isModalContainer": false,
            "skin": "slFbox",
            "top": "0dp",
            "width": "100%"
        }, controller.args[0], "flxAddressInfo"), extendConfig({}, controller.args[1], "flxAddressInfo"), extendConfig({}, controller.args[2], "flxAddressInfo"));
        flxAddressInfo.setDefaultUnit(kony.flex.DP);
        var flxAddressLine1 = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
            "clipBounds": true,
            "id": "flxAddressLine1",
            "isVisible": true,
            "layoutType": kony.flex.FLOW_VERTICAL,
            "left": "0dp",
            "isModalContainer": false,
            "skin": "slFbox",
            "top": "30dp",
            "width": "100%",
            "zIndex": 1
        }, controller.args[0], "flxAddressLine1"), extendConfig({}, controller.args[1], "flxAddressLine1"), extendConfig({}, controller.args[2], "flxAddressLine1"));
        flxAddressLine1.setDefaultUnit(kony.flex.DP);
        var flxAddressLine1Labels = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
            "clipBounds": true,
            "id": "flxAddressLine1Labels",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "left": "0dp",
            "isModalContainer": false,
            "skin": "slFbox",
            "top": "0dp",
            "width": "100%"
        }, controller.args[0], "flxAddressLine1Labels"), extendConfig({}, controller.args[1], "flxAddressLine1Labels"), extendConfig({}, controller.args[2], "flxAddressLine1Labels"));
        flxAddressLine1Labels.setDefaultUnit(kony.flex.DP);
        var lblAddressLine1 = new kony.ui.Label(extendConfig({
            "id": "lblAddressLine1",
            "isVisible": true,
            "left": "0dp",
            "skin": "sknLblLatoReg78818912px",
            "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmTrackApplication.AddressLine1\")",
            "top": "0px",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, controller.args[0], "lblAddressLine1"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblAddressLine1"), extendConfig({}, controller.args[2], "lblAddressLine1"));
        var lblAddrLine1Size = new kony.ui.Label(extendConfig({
            "id": "lblAddrLine1Size",
            "isVisible": false,
            "right": "5dp",
            "skin": "sknLblLatoReg78818912px",
            "text": "0/40",
            "top": "0dp",
            "width": "50dp",
            "zIndex": 1
        }, controller.args[0], "lblAddrLine1Size"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_TOP_RIGHT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblAddrLine1Size"), extendConfig({}, controller.args[2], "lblAddrLine1Size"));
        flxAddressLine1Labels.add(lblAddressLine1, lblAddrLine1Size);
        var txtAddressLine1Value = new kony.ui.TextBox2(extendConfig({
            "autoCapitalize": constants.TEXTBOX_AUTO_CAPITALIZE_NONE,
            "focusSkin": "skntxtbxDetailsFocus14px",
            "height": "40dp",
            "id": "txtAddressLine1Value",
            "isVisible": true,
            "keyBoardStyle": constants.TEXTBOX_KEY_BOARD_STYLE_DEFAULT,
            "left": "0dp",
            "maxTextLength": 40,
            "i18n_placeholder": "kony.i18n.getLocalizedString(\"i18n.frmCreateNewUser.lblAddressLine1User\")",
            "secureTextEntry": false,
            "skin": "skntxtbxDetails0bbf1235271384a",
            "textInputMode": constants.TEXTBOX_INPUT_MODE_ANY,
            "top": "15dp",
            "width": "100%",
            "zIndex": 1
        }, controller.args[0], "txtAddressLine1Value"), extendConfig({
            "containerHeightMode": constants.TEXTBOX_FONT_METRICS_DRIVEN_HEIGHT,
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [1, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "txtAddressLine1Value"), extendConfig({
            "autoCorrect": false,
            "onKeyUp": controller.AS_TextField_gcfbe06ed3694c67adf00d1b7e13a375
        }, controller.args[2], "txtAddressLine1Value"));
        var currentAddressErrorMsg = new com.adminConsole.loans.applications.errorMsg(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
            "id": "currentAddressErrorMsg",
            "isVisible": false,
            "layoutType": kony.flex.FLOW_HORIZONTAL,
            "left": "0dp",
            "masterType": constants.MASTER_TYPE_DEFAULT,
            "isModalContainer": false,
            "skin": "slFbox",
            "top": "10dp",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "overrides": {
                "errorMsg": {
                    "isVisible": false
                }
            }
        }, controller.args[0], "currentAddressErrorMsg"), extendConfig({
            "overrides": {}
        }, controller.args[1], "currentAddressErrorMsg"), extendConfig({
            "overrides": {}
        }, controller.args[2], "currentAddressErrorMsg"));
        flxAddressLine1.add(flxAddressLine1Labels, txtAddressLine1Value, currentAddressErrorMsg);
        var flxAddressLine2 = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
            "clipBounds": true,
            "id": "flxAddressLine2",
            "isVisible": true,
            "layoutType": kony.flex.FLOW_VERTICAL,
            "left": "0",
            "isModalContainer": false,
            "skin": "slFbox",
            "top": "30dp",
            "width": "100%",
            "zIndex": 1
        }, controller.args[0], "flxAddressLine2"), extendConfig({}, controller.args[1], "flxAddressLine2"), extendConfig({}, controller.args[2], "flxAddressLine2"));
        flxAddressLine2.setDefaultUnit(kony.flex.DP);
        var flxAddressLine2Label = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
            "clipBounds": true,
            "id": "flxAddressLine2Label",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "left": "0dp",
            "isModalContainer": false,
            "skin": "slFbox",
            "top": "0dp",
            "width": "100%"
        }, controller.args[0], "flxAddressLine2Label"), extendConfig({}, controller.args[1], "flxAddressLine2Label"), extendConfig({}, controller.args[2], "flxAddressLine2Label"));
        flxAddressLine2Label.setDefaultUnit(kony.flex.DP);
        var lblAddressLine2 = new kony.ui.Label(extendConfig({
            "id": "lblAddressLine2",
            "isVisible": true,
            "left": 0,
            "skin": "sknLblLatoReg78818912px",
            "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmTrackApplication.AddressLine2\")",
            "top": "0px",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, controller.args[0], "lblAddressLine2"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblAddressLine2"), extendConfig({}, controller.args[2], "lblAddressLine2"));
        var lblAddreLine2Size = new kony.ui.Label(extendConfig({
            "id": "lblAddreLine2Size",
            "isVisible": false,
            "right": "5dp",
            "skin": "sknLblLatoReg78818912px",
            "text": "0/40",
            "top": "0dp",
            "width": "50dp",
            "zIndex": 1
        }, controller.args[0], "lblAddreLine2Size"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_TOP_RIGHT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblAddreLine2Size"), extendConfig({}, controller.args[2], "lblAddreLine2Size"));
        flxAddressLine2Label.add(lblAddressLine2, lblAddreLine2Size);
        var txtAddressLine2Value = new kony.ui.TextBox2(extendConfig({
            "autoCapitalize": constants.TEXTBOX_AUTO_CAPITALIZE_NONE,
            "focusSkin": "skntxtbxDetailsFocus14px",
            "height": "40dp",
            "id": "txtAddressLine2Value",
            "isVisible": true,
            "keyBoardStyle": constants.TEXTBOX_KEY_BOARD_STYLE_DEFAULT,
            "left": "0dp",
            "maxTextLength": 40,
            "i18n_placeholder": "kony.i18n.getLocalizedString(\"i18n.frmCreateNewUser.lblAddressLine2User\")",
            "right": "0dp",
            "secureTextEntry": false,
            "skin": "skntxtbxDetails0bbf1235271384a",
            "textInputMode": constants.TEXTBOX_INPUT_MODE_ANY,
            "top": "15dp",
            "width": "100%",
            "zIndex": 1
        }, controller.args[0], "txtAddressLine2Value"), extendConfig({
            "containerHeightMode": constants.TEXTBOX_FONT_METRICS_DRIVEN_HEIGHT,
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [1, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "txtAddressLine2Value"), extendConfig({
            "autoCorrect": false
        }, controller.args[2], "txtAddressLine2Value"));
        var AddressLine2ErrorMsg = new com.adminConsole.loans.applications.errorMsg(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
            "id": "AddressLine2ErrorMsg",
            "isVisible": false,
            "layoutType": kony.flex.FLOW_HORIZONTAL,
            "left": "0dp",
            "masterType": constants.MASTER_TYPE_DEFAULT,
            "isModalContainer": false,
            "skin": "slFbox",
            "top": "10dp",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "overrides": {
                "errorMsg": {
                    "isVisible": false
                }
            }
        }, controller.args[0], "AddressLine2ErrorMsg"), extendConfig({
            "overrides": {}
        }, controller.args[1], "AddressLine2ErrorMsg"), extendConfig({
            "overrides": {}
        }, controller.args[2], "AddressLine2ErrorMsg"));
        flxAddressLine2.add(flxAddressLine2Label, txtAddressLine2Value, AddressLine2ErrorMsg);
        var flxAddressSection3 = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
            "clipBounds": true,
            "id": "flxAddressSection3",
            "isVisible": true,
            "layoutType": kony.flex.FLOW_HORIZONTAL,
            "left": "0dp",
            "isModalContainer": false,
            "skin": "slFbox",
            "top": "30dp",
            "width": "100%",
            "zIndex": 1
        }, controller.args[0], "flxAddressSection3"), extendConfig({}, controller.args[1], "flxAddressSection3"), extendConfig({}, controller.args[2], "flxAddressSection3"));
        flxAddressSection3.setDefaultUnit(kony.flex.DP);
        var flxAddressCountry = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
            "clipBounds": true,
            "id": "flxAddressCountry",
            "isVisible": true,
            "layoutType": kony.flex.FLOW_VERTICAL,
            "isModalContainer": false,
            "skin": "slFbox",
            "top": "0dp",
            "width": "32%",
            "zIndex": 1
        }, controller.args[0], "flxAddressCountry"), extendConfig({}, controller.args[1], "flxAddressCountry"), extendConfig({}, controller.args[2], "flxAddressCountry"));
        flxAddressCountry.setDefaultUnit(kony.flex.DP);
        var lblAddress1Country = new kony.ui.Label(extendConfig({
            "id": "lblAddress1Country",
            "isVisible": true,
            "left": "0px",
            "skin": "sknLblLatoReg78818912px",
            "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmTrackApplication.Country\")",
            "top": "0px",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, controller.args[0], "lblAddress1Country"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblAddress1Country"), extendConfig({}, controller.args[2], "lblAddress1Country"));
        var txtCountryValue = new kony.ui.TextBox2(extendConfig({
            "autoCapitalize": constants.TEXTBOX_AUTO_CAPITALIZE_NONE,
            "focusSkin": "skntxtbxDetailsFocus14px",
            "height": "40dp",
            "id": "txtCountryValue",
            "isVisible": true,
            "keyBoardStyle": constants.TEXTBOX_KEY_BOARD_STYLE_DEFAULT,
            "left": "0dp",
            "i18n_placeholder": "kony.i18n.getLocalizedString(\"i18n.frmCreateNewUser.lblCountryUser\")",
            "secureTextEntry": false,
            "skin": "skntxtbxDetails0bbf1235271384a",
            "textInputMode": constants.TEXTBOX_INPUT_MODE_ANY,
            "top": "15dp",
            "width": "100%",
            "zIndex": 1
        }, controller.args[0], "txtCountryValue"), extendConfig({
            "containerHeightMode": constants.TEXTBOX_FONT_METRICS_DRIVEN_HEIGHT,
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [3, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "txtCountryValue"), extendConfig({
            "autoCorrect": false
        }, controller.args[2], "txtCountryValue"));
        var currentCountryErrorMsg = new com.adminConsole.loans.applications.errorMsg(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
            "id": "currentCountryErrorMsg",
            "isVisible": false,
            "layoutType": kony.flex.FLOW_HORIZONTAL,
            "left": "0dp",
            "masterType": constants.MASTER_TYPE_DEFAULT,
            "isModalContainer": false,
            "skin": "slFbox",
            "top": "10dp",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "overrides": {
                "errorMsg": {
                    "isVisible": false
                }
            }
        }, controller.args[0], "currentCountryErrorMsg"), extendConfig({
            "overrides": {}
        }, controller.args[1], "currentCountryErrorMsg"), extendConfig({
            "overrides": {}
        }, controller.args[2], "currentCountryErrorMsg"));
        flxAddressCountry.add(lblAddress1Country, txtCountryValue, currentCountryErrorMsg);
        var flxAddressState = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
            "clipBounds": true,
            "id": "flxAddressState",
            "isVisible": true,
            "layoutType": kony.flex.FLOW_VERTICAL,
            "left": "2%",
            "isModalContainer": false,
            "skin": "slFbox",
            "top": "0",
            "width": "32%",
            "zIndex": 1
        }, controller.args[0], "flxAddressState"), extendConfig({}, controller.args[1], "flxAddressState"), extendConfig({}, controller.args[2], "flxAddressState"));
        flxAddressState.setDefaultUnit(kony.flex.DP);
        var lblAddressState = new kony.ui.Label(extendConfig({
            "id": "lblAddressState",
            "isVisible": true,
            "left": "0px",
            "skin": "sknLblLatoReg78818912px",
            "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmTrackApplication.State\")",
            "top": "0px",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, controller.args[0], "lblAddressState"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblAddressState"), extendConfig({}, controller.args[2], "lblAddressState"));
        var txtStateValue = new kony.ui.TextBox2(extendConfig({
            "autoCapitalize": constants.TEXTBOX_AUTO_CAPITALIZE_NONE,
            "focusSkin": "skntxtbxDetailsFocus14px",
            "height": "40dp",
            "id": "txtStateValue",
            "isVisible": true,
            "keyBoardStyle": constants.TEXTBOX_KEY_BOARD_STYLE_DEFAULT,
            "left": "0dp",
            "i18n_placeholder": "kony.i18n.getLocalizedString(\"i18n.frmCreateNewUser.lblStateUser\")",
            "secureTextEntry": false,
            "skin": "skntxtbxDetails0bbf1235271384a",
            "textInputMode": constants.TEXTBOX_INPUT_MODE_ANY,
            "top": "15dp",
            "width": "100%",
            "zIndex": 1
        }, controller.args[0], "txtStateValue"), extendConfig({
            "containerHeightMode": constants.TEXTBOX_FONT_METRICS_DRIVEN_HEIGHT,
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [3, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "txtStateValue"), extendConfig({
            "autoCorrect": false
        }, controller.args[2], "txtStateValue"));
        var currentStateErrorMsg = new com.adminConsole.loans.applications.errorMsg(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
            "id": "currentStateErrorMsg",
            "isVisible": false,
            "layoutType": kony.flex.FLOW_HORIZONTAL,
            "left": "0dp",
            "masterType": constants.MASTER_TYPE_DEFAULT,
            "isModalContainer": false,
            "skin": "slFbox",
            "top": "10dp",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "overrides": {
                "errorMsg": {
                    "isVisible": false
                }
            }
        }, controller.args[0], "currentStateErrorMsg"), extendConfig({
            "overrides": {}
        }, controller.args[1], "currentStateErrorMsg"), extendConfig({
            "overrides": {}
        }, controller.args[2], "currentStateErrorMsg"));
        flxAddressState.add(lblAddressState, txtStateValue, currentStateErrorMsg);
        var flxAddressCity = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
            "clipBounds": true,
            "id": "flxAddressCity",
            "isVisible": true,
            "layoutType": kony.flex.FLOW_VERTICAL,
            "left": "2%",
            "isModalContainer": false,
            "skin": "slFbox",
            "top": "0dp",
            "width": "32%",
            "zIndex": 1
        }, controller.args[0], "flxAddressCity"), extendConfig({}, controller.args[1], "flxAddressCity"), extendConfig({}, controller.args[2], "flxAddressCity"));
        flxAddressCity.setDefaultUnit(kony.flex.DP);
        var lblAddressCity = new kony.ui.Label(extendConfig({
            "id": "lblAddressCity",
            "isVisible": true,
            "left": "0px",
            "skin": "sknLblLatoReg78818912px",
            "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmTrackApplication.City\")",
            "top": "0px",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, controller.args[0], "lblAddressCity"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblAddressCity"), extendConfig({}, controller.args[2], "lblAddressCity"));
        var txtCityValue = new kony.ui.TextBox2(extendConfig({
            "autoCapitalize": constants.TEXTBOX_AUTO_CAPITALIZE_NONE,
            "focusSkin": "defTextBoxFocus",
            "height": "40dp",
            "id": "txtCityValue",
            "isVisible": true,
            "keyBoardStyle": constants.TEXTBOX_KEY_BOARD_STYLE_DEFAULT,
            "left": "0dp",
            "i18n_placeholder": "kony.i18n.getLocalizedString(\"i18n.frmCreateNewUser.lblCityUser\")",
            "secureTextEntry": false,
            "skin": "skntxtbxDetails0bbf1235271384a",
            "textInputMode": constants.TEXTBOX_INPUT_MODE_ANY,
            "top": "15dp",
            "width": "100%",
            "zIndex": 10
        }, controller.args[0], "txtCityValue"), extendConfig({
            "containerHeightMode": constants.TEXTBOX_FONT_METRICS_DRIVEN_HEIGHT,
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [3, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "txtCityValue"), extendConfig({
            "autoCorrect": false,
            "placeholderSkin": "defTextBoxPlaceholder"
        }, controller.args[2], "txtCityValue"));
        var currentCityErrorMsg = new com.adminConsole.loans.applications.errorMsg(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
            "id": "currentCityErrorMsg",
            "isVisible": false,
            "layoutType": kony.flex.FLOW_HORIZONTAL,
            "left": "0dp",
            "masterType": constants.MASTER_TYPE_DEFAULT,
            "isModalContainer": false,
            "skin": "slFbox",
            "top": "10dp",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "overrides": {
                "errorMsg": {
                    "isVisible": false
                }
            }
        }, controller.args[0], "currentCityErrorMsg"), extendConfig({
            "overrides": {}
        }, controller.args[1], "currentCityErrorMsg"), extendConfig({
            "overrides": {}
        }, controller.args[2], "currentCityErrorMsg"));
        flxAddressCity.add(lblAddressCity, txtCityValue, currentCityErrorMsg);
        flxAddressSection3.add(flxAddressCountry, flxAddressState, flxAddressCity);
        var flxAddressSection4 = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
            "clipBounds": true,
            "id": "flxAddressSection4",
            "isVisible": true,
            "layoutType": kony.flex.FLOW_HORIZONTAL,
            "left": "0",
            "isModalContainer": false,
            "skin": "slFbox",
            "top": "30dp",
            "width": "100%",
            "zIndex": 10
        }, controller.args[0], "flxAddressSection4"), extendConfig({}, controller.args[1], "flxAddressSection4"), extendConfig({}, controller.args[2], "flxAddressSection4"));
        flxAddressSection4.setDefaultUnit(kony.flex.DP);
        var flxAddressZipCode = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
            "clipBounds": true,
            "id": "flxAddressZipCode",
            "isVisible": true,
            "layoutType": kony.flex.FLOW_VERTICAL,
            "left": "0dp",
            "isModalContainer": false,
            "skin": "slFbox",
            "top": "0dp",
            "width": "32%",
            "zIndex": 1
        }, controller.args[0], "flxAddressZipCode"), extendConfig({}, controller.args[1], "flxAddressZipCode"), extendConfig({}, controller.args[2], "flxAddressZipCode"));
        flxAddressZipCode.setDefaultUnit(kony.flex.DP);
        var lblAddressZipCode = new kony.ui.Label(extendConfig({
            "id": "lblAddressZipCode",
            "isVisible": true,
            "left": "0px",
            "skin": "sknLblLatoReg78818912px",
            "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmTrackApplication.zip\")",
            "top": "0px",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, controller.args[0], "lblAddressZipCode"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblAddressZipCode"), extendConfig({}, controller.args[2], "lblAddressZipCode"));
        var txtZipCodeValue = new kony.ui.TextBox2(extendConfig({
            "autoCapitalize": constants.TEXTBOX_AUTO_CAPITALIZE_NONE,
            "focusSkin": "skntxtbxDetailsFocus14px",
            "height": "40dp",
            "id": "txtZipCodeValue",
            "isVisible": true,
            "keyBoardStyle": constants.TEXTBOX_KEY_BOARD_STYLE_PHONE_PAD,
            "left": "0px",
            "i18n_placeholder": "kony.i18n.getLocalizedString(\"i18n.frmCreateNewUser.lblPostalCodeUser\")",
            "secureTextEntry": false,
            "skin": "skntxtbxDetails0bbf1235271384a",
            "textInputMode": constants.TEXTBOX_INPUT_MODE_NUMERIC,
            "top": "15dp",
            "width": "100%",
            "zIndex": 1
        }, controller.args[0], "txtZipCodeValue"), extendConfig({
            "containerHeightMode": constants.TEXTBOX_FONT_METRICS_DRIVEN_HEIGHT,
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [3, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "txtZipCodeValue"), extendConfig({
            "autoCorrect": false
        }, controller.args[2], "txtZipCodeValue"));
        var currentZipCodeErrorMsg = new com.adminConsole.loans.applications.errorMsg(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
            "id": "currentZipCodeErrorMsg",
            "isVisible": false,
            "layoutType": kony.flex.FLOW_HORIZONTAL,
            "left": "0dp",
            "masterType": constants.MASTER_TYPE_DEFAULT,
            "isModalContainer": false,
            "skin": "slFbox",
            "top": "10dp",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "overrides": {
                "errorMsg": {
                    "isVisible": false
                }
            }
        }, controller.args[0], "currentZipCodeErrorMsg"), extendConfig({
            "overrides": {}
        }, controller.args[1], "currentZipCodeErrorMsg"), extendConfig({
            "overrides": {}
        }, controller.args[2], "currentZipCodeErrorMsg"));
        flxAddressZipCode.add(lblAddressZipCode, txtZipCodeValue, currentZipCodeErrorMsg);
        flxAddressSection4.add(flxAddressZipCode);
        var flxAddressValidation = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "clipBounds": true,
            "height": "40dp",
            "id": "flxAddressValidation",
            "isVisible": true,
            "layoutType": kony.flex.FLOW_HORIZONTAL,
            "left": "0",
            "isModalContainer": false,
            "skin": "sknflxf9f9f9NoBorder",
            "top": "30dp",
            "width": "100%",
            "zIndex": 10
        }, controller.args[0], "flxAddressValidation"), extendConfig({}, controller.args[1], "flxAddressValidation"), extendConfig({}, controller.args[2], "flxAddressValidation"));
        flxAddressValidation.setDefaultUnit(kony.flex.DP);
        var flxErrorIconConatiner = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "clipBounds": true,
            "height": "100%",
            "id": "flxErrorIconConatiner",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "left": "0dp",
            "isModalContainer": false,
            "skin": "slFbox",
            "top": "0dp",
            "width": "44%"
        }, controller.args[0], "flxErrorIconConatiner"), extendConfig({}, controller.args[1], "flxErrorIconConatiner"), extendConfig({}, controller.args[2], "flxErrorIconConatiner"));
        flxErrorIconConatiner.setDefaultUnit(kony.flex.DP);
        var lblUspsRecommendationErrorIcon = new kony.ui.Label(extendConfig({
            "height": "15dp",
            "id": "lblUspsRecommendationErrorIcon",
            "isVisible": false,
            "right": "0dp",
            "skin": "sknErrorIcon",
            "text": "",
            "top": "11dp",
            "width": "15dp",
            "zIndex": 1
        }, controller.args[0], "lblUspsRecommendationErrorIcon"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblUspsRecommendationErrorIcon"), extendConfig({}, controller.args[2], "lblUspsRecommendationErrorIcon"));
        flxErrorIconConatiner.add(lblUspsRecommendationErrorIcon);
        var lblUspsRecommendation = new kony.ui.Label(extendConfig({
            "id": "lblUspsRecommendation",
            "isVisible": true,
            "left": "0px",
            "skin": "sknLblLatoReg78818A13px",
            "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmTrackApplication.USPSRecommendation\")",
            "top": "11px",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, controller.args[0], "lblUspsRecommendation"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblUspsRecommendation"), extendConfig({}, controller.args[2], "lblUspsRecommendation"));
        var flxValidate = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
            "clipBounds": true,
            "id": "flxValidate",
            "isVisible": true,
            "layoutType": kony.flex.FLOW_HORIZONTAL,
            "left": "10dp",
            "isModalContainer": false,
            "skin": "sknFlxPointer",
            "top": "11dp",
            "width": "50dp"
        }, controller.args[0], "flxValidate"), extendConfig({}, controller.args[1], "flxValidate"), extendConfig({}, controller.args[2], "flxValidate"));
        flxValidate.setDefaultUnit(kony.flex.DP);
        var lblValidate = new kony.ui.Label(extendConfig({
            "id": "lblValidate",
            "isVisible": true,
            "left": "0",
            "skin": "sknLblHyperLink14PX0a9c098be02ca43",
            "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmTrackApplication.Validate\")",
            "top": "0",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, controller.args[0], "lblValidate"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblValidate"), extendConfig({}, controller.args[2], "lblValidate"));
        flxValidate.add(lblValidate);
        flxAddressValidation.add(flxErrorIconConatiner, lblUspsRecommendation, flxValidate);
        flxAddressInfo.add(flxAddressLine1, flxAddressLine2, flxAddressSection3, flxAddressSection4, flxAddressValidation);
        var flxOwnerShip = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
            "clipBounds": true,
            "id": "flxOwnerShip",
            "isVisible": true,
            "layoutType": kony.flex.FLOW_HORIZONTAL,
            "left": "0",
            "isModalContainer": false,
            "skin": "slFbox",
            "top": "30dp",
            "width": "100%",
            "zIndex": 10
        }, controller.args[0], "flxOwnerShip"), extendConfig({}, controller.args[1], "flxOwnerShip"), extendConfig({}, controller.args[2], "flxOwnerShip"));
        flxOwnerShip.setDefaultUnit(kony.flex.DP);
        var flxHomeOwnership = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
            "clipBounds": true,
            "id": "flxHomeOwnership",
            "isVisible": true,
            "layoutType": kony.flex.FLOW_VERTICAL,
            "isModalContainer": false,
            "skin": "slFbox",
            "top": "0dp",
            "width": "32%",
            "zIndex": 1
        }, controller.args[0], "flxHomeOwnership"), extendConfig({}, controller.args[1], "flxHomeOwnership"), extendConfig({}, controller.args[2], "flxHomeOwnership"));
        flxHomeOwnership.setDefaultUnit(kony.flex.DP);
        var lblHomeOwnership = new kony.ui.Label(extendConfig({
            "id": "lblHomeOwnership",
            "isVisible": true,
            "left": "0px",
            "skin": "sknLblLatoReg78818912px",
            "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmTrackApplication.HomeOwnership\")",
            "top": "0px",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, controller.args[0], "lblHomeOwnership"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblHomeOwnership"), extendConfig({}, controller.args[2], "lblHomeOwnership"));
        var lstHomeOwnershipValue = new kony.ui.ListBox(extendConfig({
            "height": "40px",
            "id": "lstHomeOwnershipValue",
            "isVisible": true,
            "left": "0px",
            "masterData": [
                ["Own and Mortgage Free", "Own and Mortgage Free"],
                ["Own with Mortgage", "Own with Mortgage"],
                ["Rented Place", "Rented Place"],
                ["Rent Free", "Rent Free"]
            ],
            "skin": "sknlbxBgffffffBorderc1c9ceRadius3Px",
            "top": "15dp",
            "width": "100%",
            "zIndex": 1
        }, controller.args[0], "lstHomeOwnershipValue"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [2, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lstHomeOwnershipValue"), extendConfig({
            "multiSelect": false
        }, controller.args[2], "lstHomeOwnershipValue"));
        flxHomeOwnership.add(lblHomeOwnership, lstHomeOwnershipValue);
        var flxDuration = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
            "clipBounds": true,
            "id": "flxDuration",
            "isVisible": true,
            "layoutType": kony.flex.FLOW_VERTICAL,
            "left": "2%",
            "isModalContainer": false,
            "skin": "slFbox",
            "top": "0dp",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, controller.args[0], "flxDuration"), extendConfig({}, controller.args[1], "flxDuration"), extendConfig({}, controller.args[2], "flxDuration"));
        flxDuration.setDefaultUnit(kony.flex.DP);
        var lblDuration = new kony.ui.Label(extendConfig({
            "id": "lblDuration",
            "isVisible": true,
            "left": "0px",
            "skin": "sknLblLatoReg78818912px",
            "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmTrackApplication.DurationOfStay\")",
            "top": "0px",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, controller.args[0], "lblDuration"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblDuration"), extendConfig({}, controller.args[2], "lblDuration"));
        var flxOptions = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "clipBounds": true,
            "height": "40dp",
            "id": "flxOptions",
            "isVisible": true,
            "layoutType": kony.flex.FLOW_HORIZONTAL,
            "left": "0dp",
            "isModalContainer": false,
            "skin": "slFbox",
            "top": "15dp",
            "width": "100%"
        }, controller.args[0], "flxOptions"), extendConfig({}, controller.args[1], "flxOptions"), extendConfig({}, controller.args[2], "flxOptions"));
        flxOptions.setDefaultUnit(kony.flex.DP);
        var flxRadioButton1 = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "clipBounds": true,
            "height": "100%",
            "id": "flxRadioButton1",
            "isVisible": true,
            "layoutType": kony.flex.FLOW_HORIZONTAL,
            "left": "0dp",
            "isModalContainer": false,
            "skin": "slFbox",
            "top": "0dp",
            "width": "150dp",
            "zIndex": 1
        }, controller.args[0], "flxRadioButton1"), extendConfig({}, controller.args[1], "flxRadioButton1"), extendConfig({}, controller.args[2], "flxRadioButton1"));
        flxRadioButton1.setDefaultUnit(kony.flex.DP);
        var imgRadioButton1 = new kony.ui.Image2(extendConfig({
            "centerY": "50%",
            "height": "15dp",
            "id": "imgRadioButton1",
            "isVisible": true,
            "left": "0dp",
            "skin": "slImage",
            "src": "radionormal_1x.png",
            "top": "0dp",
            "width": "15dp",
            "zIndex": 1
        }, controller.args[0], "imgRadioButton1"), extendConfig({
            "imageScaleMode": constants.IMAGE_SCALE_MODE_MAINTAIN_ASPECT_RATIO,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "imgRadioButton1"), extendConfig({}, controller.args[2], "imgRadioButton1"));
        var lblRadioButtonValue1 = new kony.ui.Label(extendConfig({
            "centerY": "50%",
            "id": "lblRadioButtonValue1",
            "isVisible": true,
            "left": "15dp",
            "skin": "sknLbl485C75LatoRegular13Px",
            "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmTrackApplication.Morethantwoyears\")",
            "top": "0dp",
            "width": "120dp",
            "zIndex": 1
        }, controller.args[0], "lblRadioButtonValue1"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_TOP_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblRadioButtonValue1"), extendConfig({}, controller.args[2], "lblRadioButtonValue1"));
        flxRadioButton1.add(imgRadioButton1, lblRadioButtonValue1);
        var flxRadioButton2 = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "clipBounds": true,
            "height": "100%",
            "id": "flxRadioButton2",
            "isVisible": true,
            "layoutType": kony.flex.FLOW_HORIZONTAL,
            "left": "15dp",
            "isModalContainer": false,
            "skin": "slFbox",
            "top": "0dp",
            "width": "150dp",
            "zIndex": 1
        }, controller.args[0], "flxRadioButton2"), extendConfig({}, controller.args[1], "flxRadioButton2"), extendConfig({}, controller.args[2], "flxRadioButton2"));
        flxRadioButton2.setDefaultUnit(kony.flex.DP);
        var imgRadioButton2 = new kony.ui.Image2(extendConfig({
            "centerY": "50%",
            "height": "15dp",
            "id": "imgRadioButton2",
            "isVisible": true,
            "left": "0dp",
            "skin": "slImage",
            "src": "radionormal_1x.png",
            "top": "0dp",
            "width": "15dp",
            "zIndex": 1
        }, controller.args[0], "imgRadioButton2"), extendConfig({
            "imageScaleMode": constants.IMAGE_SCALE_MODE_MAINTAIN_ASPECT_RATIO,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "imgRadioButton2"), extendConfig({}, controller.args[2], "imgRadioButton2"));
        var lblRadioButtonValue2 = new kony.ui.Label(extendConfig({
            "centerY": "50%",
            "id": "lblRadioButtonValue2",
            "isVisible": true,
            "left": "15dp",
            "skin": "sknLbl485C75LatoRegular13Px",
            "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmTrackApplication.Lessthantwoyears\")",
            "top": "0dp",
            "width": "120dp",
            "zIndex": 1
        }, controller.args[0], "lblRadioButtonValue2"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_TOP_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblRadioButtonValue2"), extendConfig({}, controller.args[2], "lblRadioButtonValue2"));
        flxRadioButton2.add(imgRadioButton2, lblRadioButtonValue2);
        flxOptions.add(flxRadioButton1, flxRadioButton2);
        flxDuration.add(lblDuration, flxOptions);
        flxOwnerShip.add(flxHomeOwnership, flxDuration);
        var flxPreviousAddressSection = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
            "clipBounds": true,
            "id": "flxPreviousAddressSection",
            "isVisible": true,
            "layoutType": kony.flex.FLOW_VERTICAL,
            "left": "0",
            "isModalContainer": false,
            "skin": "slFbox",
            "top": 30,
            "width": "100%",
            "zIndex": 1
        }, controller.args[0], "flxPreviousAddressSection"), extendConfig({}, controller.args[1], "flxPreviousAddressSection"), extendConfig({}, controller.args[2], "flxPreviousAddressSection"));
        flxPreviousAddressSection.setDefaultUnit(kony.flex.DP);
        var loansSectionHeader1 = new com.adminConsole.loans.applications.loansSectionHeader(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
            "id": "loansSectionHeader1",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "left": "0dp",
            "masterType": constants.MASTER_TYPE_DEFAULT,
            "isModalContainer": false,
            "skin": "slFbox",
            "top": "0dp",
            "width": "100%",
            "overrides": {
                "btnEdit": {
                    "isVisible": false
                },
                "flxSectionHeader": {
                    "left": "0dp",
                    "right": "0dp"
                },
                "lblSectionHeader": {
                    "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmTrackApplication.PreviousAddress\")"
                }
            }
        }, controller.args[0], "loansSectionHeader1"), extendConfig({
            "overrides": {}
        }, controller.args[1], "loansSectionHeader1"), extendConfig({
            "overrides": {}
        }, controller.args[2], "loansSectionHeader1"));
        var flxPreviousAddress = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
            "clipBounds": true,
            "id": "flxPreviousAddress",
            "isVisible": true,
            "layoutType": kony.flex.FLOW_VERTICAL,
            "left": "0dp",
            "isModalContainer": false,
            "skin": "slFbox",
            "top": "0dp",
            "width": "100%"
        }, controller.args[0], "flxPreviousAddress"), extendConfig({}, controller.args[1], "flxPreviousAddress"), extendConfig({}, controller.args[2], "flxPreviousAddress"));
        flxPreviousAddress.setDefaultUnit(kony.flex.DP);
        var flxPrevAddressLine1 = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
            "clipBounds": true,
            "id": "flxPrevAddressLine1",
            "isVisible": true,
            "layoutType": kony.flex.FLOW_VERTICAL,
            "left": "0dp",
            "isModalContainer": false,
            "skin": "slFbox",
            "top": "30dp",
            "width": "100%",
            "zIndex": 1
        }, controller.args[0], "flxPrevAddressLine1"), extendConfig({}, controller.args[1], "flxPrevAddressLine1"), extendConfig({}, controller.args[2], "flxPrevAddressLine1"));
        flxPrevAddressLine1.setDefaultUnit(kony.flex.DP);
        var flxPrevAddressLine1Labels = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
            "clipBounds": true,
            "id": "flxPrevAddressLine1Labels",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "left": "0dp",
            "isModalContainer": false,
            "skin": "slFbox",
            "top": "0dp",
            "width": "100%"
        }, controller.args[0], "flxPrevAddressLine1Labels"), extendConfig({}, controller.args[1], "flxPrevAddressLine1Labels"), extendConfig({}, controller.args[2], "flxPrevAddressLine1Labels"));
        flxPrevAddressLine1Labels.setDefaultUnit(kony.flex.DP);
        var lblPrevAddressLine1 = new kony.ui.Label(extendConfig({
            "id": "lblPrevAddressLine1",
            "isVisible": true,
            "left": "0dp",
            "skin": "sknLblLatoReg78818912px",
            "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmTrackApplication.AddressLine1\")",
            "top": "0px",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, controller.args[0], "lblPrevAddressLine1"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblPrevAddressLine1"), extendConfig({}, controller.args[2], "lblPrevAddressLine1"));
        var lblAddrLine1Size2 = new kony.ui.Label(extendConfig({
            "id": "lblAddrLine1Size2",
            "isVisible": false,
            "right": "5dp",
            "skin": "sknLblLatoReg78818912px",
            "text": "0/40",
            "top": "0dp",
            "width": "50dp",
            "zIndex": 1
        }, controller.args[0], "lblAddrLine1Size2"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_TOP_RIGHT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblAddrLine1Size2"), extendConfig({}, controller.args[2], "lblAddrLine1Size2"));
        flxPrevAddressLine1Labels.add(lblPrevAddressLine1, lblAddrLine1Size2);
        var txtAddressLine1Value2 = new kony.ui.TextBox2(extendConfig({
            "autoCapitalize": constants.TEXTBOX_AUTO_CAPITALIZE_NONE,
            "focusSkin": "skntxtbxDetailsFocus14px",
            "height": "40dp",
            "id": "txtAddressLine1Value2",
            "isVisible": true,
            "keyBoardStyle": constants.TEXTBOX_KEY_BOARD_STYLE_DEFAULT,
            "left": "0dp",
            "maxTextLength": 40,
            "i18n_placeholder": "kony.i18n.getLocalizedString(\"i18n.frmCreateNewUser.lblAddressLine1User\")",
            "secureTextEntry": false,
            "skin": "skntxtbxDetails0bbf1235271384a",
            "textInputMode": constants.TEXTBOX_INPUT_MODE_ANY,
            "top": "15dp",
            "width": "100%",
            "zIndex": 1
        }, controller.args[0], "txtAddressLine1Value2"), extendConfig({
            "containerHeightMode": constants.TEXTBOX_FONT_METRICS_DRIVEN_HEIGHT,
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [1, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "txtAddressLine1Value2"), extendConfig({
            "autoCorrect": false,
            "onKeyUp": controller.AS_TextField_gcfbe06ed3694c67adf00d1b7e13a375
        }, controller.args[2], "txtAddressLine1Value2"));
        var currentAddressError2Msg = new com.adminConsole.loans.applications.errorMsg(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
            "id": "currentAddressError2Msg",
            "isVisible": false,
            "layoutType": kony.flex.FLOW_HORIZONTAL,
            "left": "0dp",
            "masterType": constants.MASTER_TYPE_DEFAULT,
            "isModalContainer": false,
            "skin": "slFbox",
            "top": "10dp",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "overrides": {
                "errorMsg": {
                    "isVisible": false
                }
            }
        }, controller.args[0], "currentAddressError2Msg"), extendConfig({
            "overrides": {}
        }, controller.args[1], "currentAddressError2Msg"), extendConfig({
            "overrides": {}
        }, controller.args[2], "currentAddressError2Msg"));
        flxPrevAddressLine1.add(flxPrevAddressLine1Labels, txtAddressLine1Value2, currentAddressError2Msg);
        var flxPrevAddressLine2 = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
            "clipBounds": true,
            "id": "flxPrevAddressLine2",
            "isVisible": true,
            "layoutType": kony.flex.FLOW_VERTICAL,
            "left": "0",
            "isModalContainer": false,
            "skin": "slFbox",
            "top": "30dp",
            "width": "100%",
            "zIndex": 1
        }, controller.args[0], "flxPrevAddressLine2"), extendConfig({}, controller.args[1], "flxPrevAddressLine2"), extendConfig({}, controller.args[2], "flxPrevAddressLine2"));
        flxPrevAddressLine2.setDefaultUnit(kony.flex.DP);
        var flxPrevAddressLine2Label = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
            "clipBounds": true,
            "id": "flxPrevAddressLine2Label",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "left": "0dp",
            "isModalContainer": false,
            "skin": "slFbox",
            "top": "0dp",
            "width": "100%"
        }, controller.args[0], "flxPrevAddressLine2Label"), extendConfig({}, controller.args[1], "flxPrevAddressLine2Label"), extendConfig({}, controller.args[2], "flxPrevAddressLine2Label"));
        flxPrevAddressLine2Label.setDefaultUnit(kony.flex.DP);
        var lblPrevAddressLine2 = new kony.ui.Label(extendConfig({
            "id": "lblPrevAddressLine2",
            "isVisible": true,
            "left": 0,
            "skin": "sknLblLatoReg78818912px",
            "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmTrackApplication.AddressLine2\")",
            "top": "0px",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, controller.args[0], "lblPrevAddressLine2"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblPrevAddressLine2"), extendConfig({}, controller.args[2], "lblPrevAddressLine2"));
        var lblAddreLine2Size2 = new kony.ui.Label(extendConfig({
            "id": "lblAddreLine2Size2",
            "isVisible": false,
            "right": "3dp",
            "skin": "sknLblLatoReg78818912px",
            "text": "0/40",
            "top": "0dp",
            "width": "50dp",
            "zIndex": 1
        }, controller.args[0], "lblAddreLine2Size2"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_TOP_RIGHT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblAddreLine2Size2"), extendConfig({}, controller.args[2], "lblAddreLine2Size2"));
        flxPrevAddressLine2Label.add(lblPrevAddressLine2, lblAddreLine2Size2);
        var txtAddressLine2Value2 = new kony.ui.TextBox2(extendConfig({
            "autoCapitalize": constants.TEXTBOX_AUTO_CAPITALIZE_NONE,
            "focusSkin": "skntxtbxDetailsFocus14px",
            "height": "40dp",
            "id": "txtAddressLine2Value2",
            "isVisible": true,
            "keyBoardStyle": constants.TEXTBOX_KEY_BOARD_STYLE_DEFAULT,
            "left": "0dp",
            "maxTextLength": 40,
            "i18n_placeholder": "kony.i18n.getLocalizedString(\"i18n.frmCreateNewUser.lblAddressLine2User\")",
            "right": "0dp",
            "secureTextEntry": false,
            "skin": "skntxtbxDetails0bbf1235271384a",
            "textInputMode": constants.TEXTBOX_INPUT_MODE_ANY,
            "top": "15dp",
            "width": "100%",
            "zIndex": 1
        }, controller.args[0], "txtAddressLine2Value2"), extendConfig({
            "containerHeightMode": constants.TEXTBOX_FONT_METRICS_DRIVEN_HEIGHT,
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [1, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "txtAddressLine2Value2"), extendConfig({
            "autoCorrect": false
        }, controller.args[2], "txtAddressLine2Value2"));
        var addressLine2Error2Msg = new com.adminConsole.loans.applications.errorMsg(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
            "id": "addressLine2Error2Msg",
            "isVisible": false,
            "layoutType": kony.flex.FLOW_HORIZONTAL,
            "left": "0dp",
            "masterType": constants.MASTER_TYPE_DEFAULT,
            "isModalContainer": false,
            "skin": "slFbox",
            "top": "10dp",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "overrides": {
                "errorMsg": {
                    "isVisible": false
                }
            }
        }, controller.args[0], "addressLine2Error2Msg"), extendConfig({
            "overrides": {}
        }, controller.args[1], "addressLine2Error2Msg"), extendConfig({
            "overrides": {}
        }, controller.args[2], "addressLine2Error2Msg"));
        flxPrevAddressLine2.add(flxPrevAddressLine2Label, txtAddressLine2Value2, addressLine2Error2Msg);
        var flxPrevAddressSection3 = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
            "clipBounds": true,
            "id": "flxPrevAddressSection3",
            "isVisible": true,
            "layoutType": kony.flex.FLOW_HORIZONTAL,
            "left": "0dp",
            "isModalContainer": false,
            "skin": "slFbox",
            "top": "30dp",
            "width": "100%",
            "zIndex": 1
        }, controller.args[0], "flxPrevAddressSection3"), extendConfig({}, controller.args[1], "flxPrevAddressSection3"), extendConfig({}, controller.args[2], "flxPrevAddressSection3"));
        flxPrevAddressSection3.setDefaultUnit(kony.flex.DP);
        var flxPrevAddressCountry = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
            "clipBounds": true,
            "id": "flxPrevAddressCountry",
            "isVisible": true,
            "layoutType": kony.flex.FLOW_VERTICAL,
            "left": "0",
            "isModalContainer": false,
            "skin": "slFbox",
            "top": "0dp",
            "width": "32%",
            "zIndex": 1
        }, controller.args[0], "flxPrevAddressCountry"), extendConfig({}, controller.args[1], "flxPrevAddressCountry"), extendConfig({}, controller.args[2], "flxPrevAddressCountry"));
        flxPrevAddressCountry.setDefaultUnit(kony.flex.DP);
        var lblPrevAddressCountry = new kony.ui.Label(extendConfig({
            "id": "lblPrevAddressCountry",
            "isVisible": true,
            "left": "0px",
            "skin": "sknLblLatoReg78818912px",
            "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmTrackApplication.Country\")",
            "top": "0px",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, controller.args[0], "lblPrevAddressCountry"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblPrevAddressCountry"), extendConfig({}, controller.args[2], "lblPrevAddressCountry"));
        var txtCountryValue2 = new kony.ui.TextBox2(extendConfig({
            "autoCapitalize": constants.TEXTBOX_AUTO_CAPITALIZE_NONE,
            "focusSkin": "skntxtbxDetailsFocus14px",
            "height": "40dp",
            "id": "txtCountryValue2",
            "isVisible": true,
            "keyBoardStyle": constants.TEXTBOX_KEY_BOARD_STYLE_DEFAULT,
            "left": "0dp",
            "i18n_placeholder": "kony.i18n.getLocalizedString(\"i18n.frmCreateNewUser.lblCountryUser\")",
            "secureTextEntry": false,
            "skin": "skntxtbxDetails0bbf1235271384a",
            "textInputMode": constants.TEXTBOX_INPUT_MODE_ANY,
            "top": "15dp",
            "width": "100%",
            "zIndex": 1
        }, controller.args[0], "txtCountryValue2"), extendConfig({
            "containerHeightMode": constants.TEXTBOX_FONT_METRICS_DRIVEN_HEIGHT,
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [3, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "txtCountryValue2"), extendConfig({
            "autoCorrect": false
        }, controller.args[2], "txtCountryValue2"));
        var currentCountryError2Msg = new com.adminConsole.loans.applications.errorMsg(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
            "id": "currentCountryError2Msg",
            "isVisible": false,
            "layoutType": kony.flex.FLOW_HORIZONTAL,
            "left": "0dp",
            "masterType": constants.MASTER_TYPE_DEFAULT,
            "isModalContainer": false,
            "skin": "slFbox",
            "top": "10dp",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "overrides": {
                "errorMsg": {
                    "isVisible": false
                }
            }
        }, controller.args[0], "currentCountryError2Msg"), extendConfig({
            "overrides": {}
        }, controller.args[1], "currentCountryError2Msg"), extendConfig({
            "overrides": {}
        }, controller.args[2], "currentCountryError2Msg"));
        flxPrevAddressCountry.add(lblPrevAddressCountry, txtCountryValue2, currentCountryError2Msg);
        var flxPrevAddressState = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
            "clipBounds": true,
            "id": "flxPrevAddressState",
            "isVisible": true,
            "layoutType": kony.flex.FLOW_VERTICAL,
            "left": "2%",
            "isModalContainer": false,
            "skin": "slFbox",
            "top": "0",
            "width": "32%",
            "zIndex": 1
        }, controller.args[0], "flxPrevAddressState"), extendConfig({}, controller.args[1], "flxPrevAddressState"), extendConfig({}, controller.args[2], "flxPrevAddressState"));
        flxPrevAddressState.setDefaultUnit(kony.flex.DP);
        var lblPrevAddressState = new kony.ui.Label(extendConfig({
            "id": "lblPrevAddressState",
            "isVisible": true,
            "left": "0px",
            "skin": "sknLblLatoReg78818912px",
            "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmTrackApplication.State\")",
            "top": "0px",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, controller.args[0], "lblPrevAddressState"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblPrevAddressState"), extendConfig({}, controller.args[2], "lblPrevAddressState"));
        var txtStateValue2 = new kony.ui.TextBox2(extendConfig({
            "autoCapitalize": constants.TEXTBOX_AUTO_CAPITALIZE_NONE,
            "focusSkin": "skntxtbxDetailsFocus14px",
            "height": "40dp",
            "id": "txtStateValue2",
            "isVisible": true,
            "keyBoardStyle": constants.TEXTBOX_KEY_BOARD_STYLE_DEFAULT,
            "left": "0dp",
            "i18n_placeholder": "kony.i18n.getLocalizedString(\"i18n.frmCreateNewUser.lblStateUser\")",
            "secureTextEntry": false,
            "skin": "skntxtbxDetails0bbf1235271384a",
            "textInputMode": constants.TEXTBOX_INPUT_MODE_ANY,
            "top": "15dp",
            "width": "100%",
            "zIndex": 1
        }, controller.args[0], "txtStateValue2"), extendConfig({
            "containerHeightMode": constants.TEXTBOX_FONT_METRICS_DRIVEN_HEIGHT,
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [3, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "txtStateValue2"), extendConfig({
            "autoCorrect": false
        }, controller.args[2], "txtStateValue2"));
        var currentStateError2Msg = new com.adminConsole.loans.applications.errorMsg(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
            "id": "currentStateError2Msg",
            "isVisible": false,
            "layoutType": kony.flex.FLOW_HORIZONTAL,
            "left": "0dp",
            "masterType": constants.MASTER_TYPE_DEFAULT,
            "isModalContainer": false,
            "skin": "slFbox",
            "top": "10dp",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "overrides": {
                "errorMsg": {
                    "isVisible": false
                }
            }
        }, controller.args[0], "currentStateError2Msg"), extendConfig({
            "overrides": {}
        }, controller.args[1], "currentStateError2Msg"), extendConfig({
            "overrides": {}
        }, controller.args[2], "currentStateError2Msg"));
        flxPrevAddressState.add(lblPrevAddressState, txtStateValue2, currentStateError2Msg);
        var flxPrevAddressCity = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
            "clipBounds": true,
            "id": "flxPrevAddressCity",
            "isVisible": true,
            "layoutType": kony.flex.FLOW_VERTICAL,
            "left": "2%",
            "isModalContainer": false,
            "skin": "slFbox",
            "top": "0dp",
            "width": "32%",
            "zIndex": 1
        }, controller.args[0], "flxPrevAddressCity"), extendConfig({}, controller.args[1], "flxPrevAddressCity"), extendConfig({}, controller.args[2], "flxPrevAddressCity"));
        flxPrevAddressCity.setDefaultUnit(kony.flex.DP);
        var lblPrevAddressCity = new kony.ui.Label(extendConfig({
            "id": "lblPrevAddressCity",
            "isVisible": true,
            "left": "0px",
            "skin": "sknLblLatoReg78818912px",
            "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmTrackApplication.City\")",
            "top": "0px",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, controller.args[0], "lblPrevAddressCity"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblPrevAddressCity"), extendConfig({}, controller.args[2], "lblPrevAddressCity"));
        var txtCityValue2 = new kony.ui.TextBox2(extendConfig({
            "autoCapitalize": constants.TEXTBOX_AUTO_CAPITALIZE_NONE,
            "focusSkin": "defTextBoxFocus",
            "height": "40dp",
            "id": "txtCityValue2",
            "isVisible": true,
            "keyBoardStyle": constants.TEXTBOX_KEY_BOARD_STYLE_DEFAULT,
            "left": "0dp",
            "i18n_placeholder": "kony.i18n.getLocalizedString(\"i18n.frmCreateNewUser.lblCityUser\")",
            "secureTextEntry": false,
            "skin": "skntxtbxDetails0bbf1235271384a",
            "textInputMode": constants.TEXTBOX_INPUT_MODE_ANY,
            "top": "15dp",
            "width": "100%",
            "zIndex": 10
        }, controller.args[0], "txtCityValue2"), extendConfig({
            "containerHeightMode": constants.TEXTBOX_FONT_METRICS_DRIVEN_HEIGHT,
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [3, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "txtCityValue2"), extendConfig({
            "autoCorrect": false,
            "placeholderSkin": "defTextBoxPlaceholder"
        }, controller.args[2], "txtCityValue2"));
        var currentCityError2Msg = new com.adminConsole.loans.applications.errorMsg(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
            "id": "currentCityError2Msg",
            "isVisible": false,
            "layoutType": kony.flex.FLOW_HORIZONTAL,
            "left": "0dp",
            "masterType": constants.MASTER_TYPE_DEFAULT,
            "isModalContainer": false,
            "skin": "slFbox",
            "top": "10dp",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "overrides": {
                "errorMsg": {
                    "isVisible": false
                }
            }
        }, controller.args[0], "currentCityError2Msg"), extendConfig({
            "overrides": {}
        }, controller.args[1], "currentCityError2Msg"), extendConfig({
            "overrides": {}
        }, controller.args[2], "currentCityError2Msg"));
        flxPrevAddressCity.add(lblPrevAddressCity, txtCityValue2, currentCityError2Msg);
        flxPrevAddressSection3.add(flxPrevAddressCountry, flxPrevAddressState, flxPrevAddressCity);
        var flxPrevAddressSection4 = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
            "clipBounds": true,
            "id": "flxPrevAddressSection4",
            "isVisible": true,
            "layoutType": kony.flex.FLOW_HORIZONTAL,
            "left": "0",
            "isModalContainer": false,
            "skin": "slFbox",
            "top": "30dp",
            "width": "100%",
            "zIndex": 10
        }, controller.args[0], "flxPrevAddressSection4"), extendConfig({}, controller.args[1], "flxPrevAddressSection4"), extendConfig({}, controller.args[2], "flxPrevAddressSection4"));
        flxPrevAddressSection4.setDefaultUnit(kony.flex.DP);
        var flxPrevAddressZip = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
            "clipBounds": true,
            "id": "flxPrevAddressZip",
            "isVisible": true,
            "layoutType": kony.flex.FLOW_VERTICAL,
            "left": "0dp",
            "isModalContainer": false,
            "skin": "slFbox",
            "top": "0dp",
            "width": "32%",
            "zIndex": 1
        }, controller.args[0], "flxPrevAddressZip"), extendConfig({}, controller.args[1], "flxPrevAddressZip"), extendConfig({}, controller.args[2], "flxPrevAddressZip"));
        flxPrevAddressZip.setDefaultUnit(kony.flex.DP);
        var lblPrevAddresszip = new kony.ui.Label(extendConfig({
            "id": "lblPrevAddresszip",
            "isVisible": true,
            "left": "0px",
            "skin": "sknLblLatoReg78818912px",
            "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmTrackApplication.zip\")",
            "top": "0px",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, controller.args[0], "lblPrevAddresszip"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblPrevAddresszip"), extendConfig({}, controller.args[2], "lblPrevAddresszip"));
        var txtZipCodeValue2 = new kony.ui.TextBox2(extendConfig({
            "autoCapitalize": constants.TEXTBOX_AUTO_CAPITALIZE_NONE,
            "focusSkin": "skntxtbxDetailsFocus14px",
            "height": "40dp",
            "id": "txtZipCodeValue2",
            "isVisible": true,
            "keyBoardStyle": constants.TEXTBOX_KEY_BOARD_STYLE_PHONE_PAD,
            "left": "0px",
            "i18n_placeholder": "kony.i18n.getLocalizedString(\"i18n.frmCreateNewUser.lblPostalCodeUser\")",
            "secureTextEntry": false,
            "skin": "skntxtbxDetails0bbf1235271384a",
            "textInputMode": constants.TEXTBOX_INPUT_MODE_NUMERIC,
            "top": "15dp",
            "width": "100%",
            "zIndex": 1
        }, controller.args[0], "txtZipCodeValue2"), extendConfig({
            "containerHeightMode": constants.TEXTBOX_FONT_METRICS_DRIVEN_HEIGHT,
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [3, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "txtZipCodeValue2"), extendConfig({
            "autoCorrect": false
        }, controller.args[2], "txtZipCodeValue2"));
        var currentZipCodeError2Msg = new com.adminConsole.loans.applications.errorMsg(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
            "id": "currentZipCodeError2Msg",
            "isVisible": false,
            "layoutType": kony.flex.FLOW_HORIZONTAL,
            "left": "0dp",
            "masterType": constants.MASTER_TYPE_DEFAULT,
            "isModalContainer": false,
            "skin": "slFbox",
            "top": "10dp",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "overrides": {
                "errorMsg": {
                    "isVisible": false
                }
            }
        }, controller.args[0], "currentZipCodeError2Msg"), extendConfig({
            "overrides": {}
        }, controller.args[1], "currentZipCodeError2Msg"), extendConfig({
            "overrides": {}
        }, controller.args[2], "currentZipCodeError2Msg"));
        flxPrevAddressZip.add(lblPrevAddresszip, txtZipCodeValue2, currentZipCodeError2Msg);
        flxPrevAddressSection4.add(flxPrevAddressZip);
        var flxPrevAddressValidation = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "clipBounds": true,
            "height": "40dp",
            "id": "flxPrevAddressValidation",
            "isVisible": true,
            "layoutType": kony.flex.FLOW_HORIZONTAL,
            "left": "0",
            "isModalContainer": false,
            "skin": "sknflxf9f9f9NoBorder",
            "top": "30dp",
            "width": "100%",
            "zIndex": 10
        }, controller.args[0], "flxPrevAddressValidation"), extendConfig({}, controller.args[1], "flxPrevAddressValidation"), extendConfig({}, controller.args[2], "flxPrevAddressValidation"));
        flxPrevAddressValidation.setDefaultUnit(kony.flex.DP);
        var flxErrorIconConatiner2 = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "clipBounds": true,
            "height": "100%",
            "id": "flxErrorIconConatiner2",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "left": "0dp",
            "isModalContainer": false,
            "skin": "slFbox",
            "top": "0dp",
            "width": "44%"
        }, controller.args[0], "flxErrorIconConatiner2"), extendConfig({}, controller.args[1], "flxErrorIconConatiner2"), extendConfig({}, controller.args[2], "flxErrorIconConatiner2"));
        flxErrorIconConatiner2.setDefaultUnit(kony.flex.DP);
        var lblUspsRecommendationErrorIcon2 = new kony.ui.Label(extendConfig({
            "height": "15dp",
            "id": "lblUspsRecommendationErrorIcon2",
            "isVisible": false,
            "right": "0dp",
            "skin": "sknErrorIcon",
            "text": "",
            "top": "11dp",
            "width": "15dp",
            "zIndex": 1
        }, controller.args[0], "lblUspsRecommendationErrorIcon2"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblUspsRecommendationErrorIcon2"), extendConfig({}, controller.args[2], "lblUspsRecommendationErrorIcon2"));
        flxErrorIconConatiner2.add(lblUspsRecommendationErrorIcon2);
        var lblUspsRecommendation2 = new kony.ui.Label(extendConfig({
            "id": "lblUspsRecommendation2",
            "isVisible": true,
            "left": "0px",
            "skin": "sknLblLatoReg78818A13px",
            "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmTrackApplication.USPSRecommendation\")",
            "top": "11px",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, controller.args[0], "lblUspsRecommendation2"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblUspsRecommendation2"), extendConfig({}, controller.args[2], "lblUspsRecommendation2"));
        var flxValidate2 = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
            "clipBounds": true,
            "id": "flxValidate2",
            "isVisible": true,
            "layoutType": kony.flex.FLOW_HORIZONTAL,
            "left": "10dp",
            "isModalContainer": false,
            "skin": "sknFlxPointer",
            "top": "11dp",
            "width": "50dp"
        }, controller.args[0], "flxValidate2"), extendConfig({}, controller.args[1], "flxValidate2"), extendConfig({}, controller.args[2], "flxValidate2"));
        flxValidate2.setDefaultUnit(kony.flex.DP);
        var lblValidate2 = new kony.ui.Label(extendConfig({
            "id": "lblValidate2",
            "isVisible": true,
            "left": "0",
            "skin": "sknLblHyperLink14PX0a9c098be02ca43",
            "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmTrackApplication.Validate\")",
            "top": "0",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, controller.args[0], "lblValidate2"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblValidate2"), extendConfig({}, controller.args[2], "lblValidate2"));
        flxValidate2.add(lblValidate2);
        flxPrevAddressValidation.add(flxErrorIconConatiner2, lblUspsRecommendation2, flxValidate2);
        flxPreviousAddress.add(flxPrevAddressLine1, flxPrevAddressLine2, flxPrevAddressSection3, flxPrevAddressSection4, flxPrevAddressValidation);
        flxPreviousAddressSection.add(loansSectionHeader1, flxPreviousAddress);
        flxAddressInfoSection.add(flxAddressInfo, flxOwnerShip, flxPreviousAddressSection);
        editAddressInfo.add(loansSectionHeader, flxAddressInfoSection);
        return editAddressInfo;
    }
})