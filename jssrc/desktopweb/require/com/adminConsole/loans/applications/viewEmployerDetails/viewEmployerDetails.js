define(function() {
    return function(controller) {
        var viewEmployerDetails = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
            "clipBounds": true,
            "isMaster": true,
            "id": "viewEmployerDetails",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "left": "0dp",
            "isModalContainer": false,
            "skin": "slFbox",
            "top": "30dp",
            "width": "100%"
        }, controller.args[0], "viewEmployerDetails"), extendConfig({}, controller.args[1], "viewEmployerDetails"), extendConfig({}, controller.args[2], "viewEmployerDetails"));
        viewEmployerDetails.setDefaultUnit(kony.flex.DP);
        var loansSectionHeader = new com.adminConsole.loans.applications.loansSectionHeader(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
            "id": "loansSectionHeader",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "left": "0dp",
            "masterType": constants.MASTER_TYPE_DEFAULT,
            "isModalContainer": false,
            "skin": "slFbox",
            "top": "0dp",
            "width": "100%",
            "overrides": {
                "lblSectionHeader": {
                    "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmTrackApplication.PresentEmployerDetailsHeader\")"
                },
                "loansSectionHeader": {
                    "top": "0dp"
                }
            }
        }, controller.args[0], "loansSectionHeader"), extendConfig({
            "overrides": {}
        }, controller.args[1], "loansSectionHeader"), extendConfig({
            "overrides": {}
        }, controller.args[2], "loansSectionHeader"));
        var flxEmployerInfo = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
            "clipBounds": true,
            "id": "flxEmployerInfo",
            "isVisible": true,
            "layoutType": kony.flex.FLOW_VERTICAL,
            "left": "20dp",
            "isModalContainer": false,
            "right": "20dp",
            "skin": "slFbox",
            "top": "42dp",
            "zIndex": 1
        }, controller.args[0], "flxEmployerInfo"), extendConfig({}, controller.args[1], "flxEmployerInfo"), extendConfig({}, controller.args[2], "flxEmployerInfo"));
        flxEmployerInfo.setDefaultUnit(kony.flex.DP);
        var flxEmployerDtls = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
            "clipBounds": true,
            "id": "flxEmployerDtls",
            "isVisible": true,
            "layoutType": kony.flex.FLOW_VERTICAL,
            "left": "0dp",
            "isModalContainer": false,
            "skin": "slFbox",
            "top": "0dp",
            "width": "100%"
        }, controller.args[0], "flxEmployerDtls"), extendConfig({}, controller.args[1], "flxEmployerDtls"), extendConfig({}, controller.args[2], "flxEmployerDtls"));
        flxEmployerDtls.setDefaultUnit(kony.flex.DP);
        var flxPresentEmployment = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
            "clipBounds": true,
            "id": "flxPresentEmployment",
            "isVisible": true,
            "layoutType": kony.flex.FLOW_VERTICAL,
            "left": "0dp",
            "isModalContainer": false,
            "skin": "slFbox",
            "top": "0dp",
            "width": "100%"
        }, controller.args[0], "flxPresentEmployment"), extendConfig({}, controller.args[1], "flxPresentEmployment"), extendConfig({}, controller.args[2], "flxPresentEmployment"));
        flxPresentEmployment.setDefaultUnit(kony.flex.DP);
        var flxRow1 = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
            "clipBounds": true,
            "id": "flxRow1",
            "isVisible": true,
            "layoutType": kony.flex.FLOW_HORIZONTAL,
            "left": "0",
            "isModalContainer": false,
            "skin": "slFbox",
            "top": "30dp",
            "width": "100%"
        }, controller.args[0], "flxRow1"), extendConfig({}, controller.args[1], "flxRow1"), extendConfig({}, controller.args[2], "flxRow1"));
        flxRow1.setDefaultUnit(kony.flex.DP);
        var flxEmployerName = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
            "clipBounds": true,
            "id": "flxEmployerName",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "isModalContainer": false,
            "skin": "slFbox",
            "width": "32%",
            "zIndex": 1
        }, controller.args[0], "flxEmployerName"), extendConfig({}, controller.args[1], "flxEmployerName"), extendConfig({}, controller.args[2], "flxEmployerName"));
        flxEmployerName.setDefaultUnit(kony.flex.DP);
        var lblEmployerName = new kony.ui.Label(extendConfig({
            "id": "lblEmployerName",
            "isVisible": true,
            "left": "0px",
            "skin": "sknLbl78818A11px",
            "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmTrackApplication.EmployerNameCAPS\")",
            "top": "0px",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, controller.args[0], "lblEmployerName"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblEmployerName"), extendConfig({}, controller.args[2], "lblEmployerName"));
        var lblEmployerNameValue = new kony.ui.Label(extendConfig({
            "id": "lblEmployerNameValue",
            "isVisible": true,
            "left": "0px",
            "skin": "sknLbl485C75LatoRegular13Px",
            "text": "Employer Name",
            "top": "15dp",
            "width": "100%",
            "zIndex": 1
        }, controller.args[0], "lblEmployerNameValue"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblEmployerNameValue"), extendConfig({}, controller.args[2], "lblEmployerNameValue"));
        flxEmployerName.add(lblEmployerName, lblEmployerNameValue);
        var flxDesignation = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
            "clipBounds": true,
            "id": "flxDesignation",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "left": "2%",
            "isModalContainer": false,
            "skin": "slFbox",
            "width": "32%",
            "zIndex": 1
        }, controller.args[0], "flxDesignation"), extendConfig({}, controller.args[1], "flxDesignation"), extendConfig({}, controller.args[2], "flxDesignation"));
        flxDesignation.setDefaultUnit(kony.flex.DP);
        var lblDesignation = new kony.ui.Label(extendConfig({
            "id": "lblDesignation",
            "isVisible": true,
            "left": "0px",
            "skin": "sknLbl78818A11px",
            "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmTrackApplication.DesignationCAPS\")",
            "top": "0px",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, controller.args[0], "lblDesignation"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblDesignation"), extendConfig({}, controller.args[2], "lblDesignation"));
        var lblDesignationValue = new kony.ui.Label(extendConfig({
            "id": "lblDesignationValue",
            "isVisible": true,
            "left": "0px",
            "skin": "sknLbl485C75LatoRegular13Px",
            "text": "Designation",
            "top": "15dp",
            "width": "100%",
            "zIndex": 1
        }, controller.args[0], "lblDesignationValue"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblDesignationValue"), extendConfig({}, controller.args[2], "lblDesignationValue"));
        flxDesignation.add(lblDesignation, lblDesignationValue);
        var flxStartDate = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
            "clipBounds": true,
            "id": "flxStartDate",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "left": "2%",
            "isModalContainer": false,
            "skin": "slFbox",
            "width": "32%",
            "zIndex": 1
        }, controller.args[0], "flxStartDate"), extendConfig({}, controller.args[1], "flxStartDate"), extendConfig({}, controller.args[2], "flxStartDate"));
        flxStartDate.setDefaultUnit(kony.flex.DP);
        var lblStartDate = new kony.ui.Label(extendConfig({
            "id": "lblStartDate",
            "isVisible": true,
            "left": "0px",
            "skin": "sknLbl78818A11px",
            "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmCustomerManagement.lblStartDate\")",
            "top": "0px",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, controller.args[0], "lblStartDate"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblStartDate"), extendConfig({}, controller.args[2], "lblStartDate"));
        var lblStartDateValue = new kony.ui.Label(extendConfig({
            "id": "lblStartDateValue",
            "isVisible": true,
            "left": "0px",
            "skin": "sknLbl485C75LatoRegular13Px",
            "text": "Start Date",
            "top": "15dp",
            "width": "100%",
            "zIndex": 1
        }, controller.args[0], "lblStartDateValue"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblStartDateValue"), extendConfig({}, controller.args[2], "lblStartDateValue"));
        flxStartDate.add(lblStartDate, lblStartDateValue);
        flxRow1.add(flxEmployerName, flxDesignation, flxStartDate);
        var flxRow2 = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
            "clipBounds": true,
            "id": "flxRow2",
            "isVisible": true,
            "layoutType": kony.flex.FLOW_HORIZONTAL,
            "left": "0dp",
            "isModalContainer": false,
            "skin": "slFbox",
            "top": "30dp",
            "width": "100%"
        }, controller.args[0], "flxRow2"), extendConfig({}, controller.args[1], "flxRow2"), extendConfig({}, controller.args[2], "flxRow2"));
        flxRow2.setDefaultUnit(kony.flex.DP);
        var flxGrossIncome = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
            "clipBounds": true,
            "id": "flxGrossIncome",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "isModalContainer": false,
            "skin": "slFbox",
            "width": "32%",
            "zIndex": 1
        }, controller.args[0], "flxGrossIncome"), extendConfig({}, controller.args[1], "flxGrossIncome"), extendConfig({}, controller.args[2], "flxGrossIncome"));
        flxGrossIncome.setDefaultUnit(kony.flex.DP);
        var lblGrossIncome = new kony.ui.Label(extendConfig({
            "id": "lblGrossIncome",
            "isVisible": true,
            "left": "0px",
            "skin": "sknLbl78818A11px",
            "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmTrackApplication.GrossIncomeCAPS\")",
            "top": "0px",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, controller.args[0], "lblGrossIncome"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblGrossIncome"), extendConfig({}, controller.args[2], "lblGrossIncome"));
        var lblGrossIncomeValue = new kony.ui.Label(extendConfig({
            "id": "lblGrossIncomeValue",
            "isVisible": true,
            "left": "0px",
            "skin": "sknLbl485C75LatoRegular13Px",
            "text": "Gross Income",
            "top": "15dp",
            "width": "100%",
            "zIndex": 1
        }, controller.args[0], "lblGrossIncomeValue"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblGrossIncomeValue"), extendConfig({}, controller.args[2], "lblGrossIncomeValue"));
        flxGrossIncome.add(lblGrossIncome, lblGrossIncomeValue);
        var flxPayPeriod = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
            "clipBounds": true,
            "id": "flxPayPeriod",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "left": "2%",
            "isModalContainer": false,
            "skin": "slFbox",
            "top": "0dp",
            "width": "32%",
            "zIndex": 1
        }, controller.args[0], "flxPayPeriod"), extendConfig({}, controller.args[1], "flxPayPeriod"), extendConfig({}, controller.args[2], "flxPayPeriod"));
        flxPayPeriod.setDefaultUnit(kony.flex.DP);
        var lblPayPeriod = new kony.ui.Label(extendConfig({
            "id": "lblPayPeriod",
            "isVisible": true,
            "left": "0dp",
            "skin": "sknLbl78818A11px",
            "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmTrackApplication.PayPeriodCAPS\")",
            "top": "0px",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, controller.args[0], "lblPayPeriod"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblPayPeriod"), extendConfig({}, controller.args[2], "lblPayPeriod"));
        var lblPayPeriodValue = new kony.ui.Label(extendConfig({
            "id": "lblPayPeriodValue",
            "isVisible": true,
            "left": "0px",
            "skin": "sknLbl485C75LatoRegular13Px",
            "text": "Pay Period",
            "top": "15dp",
            "width": "100%",
            "zIndex": 1
        }, controller.args[0], "lblPayPeriodValue"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblPayPeriodValue"), extendConfig({}, controller.args[2], "lblPayPeriodValue"));
        flxPayPeriod.add(lblPayPeriod, lblPayPeriodValue);
        var flxTotalWorkingHrs = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
            "clipBounds": true,
            "id": "flxTotalWorkingHrs",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "left": "2%",
            "isModalContainer": false,
            "skin": "slFbox",
            "top": "0dp",
            "width": "32%",
            "zIndex": 1
        }, controller.args[0], "flxTotalWorkingHrs"), extendConfig({}, controller.args[1], "flxTotalWorkingHrs"), extendConfig({}, controller.args[2], "flxTotalWorkingHrs"));
        flxTotalWorkingHrs.setDefaultUnit(kony.flex.DP);
        var lblTotalWorkingHrs = new kony.ui.Label(extendConfig({
            "id": "lblTotalWorkingHrs",
            "isVisible": true,
            "left": "0px",
            "skin": "sknLbl78818A11px",
            "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmTrackApplication.TotalWorkingHoursCAPS\")",
            "top": "0px",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, controller.args[0], "lblTotalWorkingHrs"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblTotalWorkingHrs"), extendConfig({}, controller.args[2], "lblTotalWorkingHrs"));
        var lblTotalWorkingHrsValue = new kony.ui.Label(extendConfig({
            "id": "lblTotalWorkingHrsValue",
            "isVisible": true,
            "left": "0px",
            "skin": "sknLbl485C75LatoRegular13Px",
            "text": "Total Working Hours",
            "top": "15dp",
            "width": "100%",
            "zIndex": 1
        }, controller.args[0], "lblTotalWorkingHrsValue"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblTotalWorkingHrsValue"), extendConfig({}, controller.args[2], "lblTotalWorkingHrsValue"));
        flxTotalWorkingHrs.add(lblTotalWorkingHrs, lblTotalWorkingHrsValue);
        flxRow2.add(flxGrossIncome, flxPayPeriod, flxTotalWorkingHrs);
        var flxAddressLine1 = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
            "clipBounds": true,
            "id": "flxAddressLine1",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "left": "0",
            "isModalContainer": false,
            "skin": "slFbox",
            "top": "30dp",
            "width": "100%",
            "zIndex": 1
        }, controller.args[0], "flxAddressLine1"), extendConfig({}, controller.args[1], "flxAddressLine1"), extendConfig({}, controller.args[2], "flxAddressLine1"));
        flxAddressLine1.setDefaultUnit(kony.flex.DP);
        var lblAddressLine1 = new kony.ui.Label(extendConfig({
            "id": "lblAddressLine1",
            "isVisible": true,
            "left": "0dp",
            "skin": "sknLbl78818A11px",
            "i18n_text": "kony.i18n.getLocalizedString(\"i18n.Applications.Summary.AddressLine1\")",
            "top": "0px",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, controller.args[0], "lblAddressLine1"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblAddressLine1"), extendConfig({}, controller.args[2], "lblAddressLine1"));
        var lblAddressLine1Value = new kony.ui.Label(extendConfig({
            "id": "lblAddressLine1Value",
            "isVisible": true,
            "left": "0dp",
            "skin": "sknLbl485C75LatoRegular13Px",
            "text": "Address Line 1",
            "top": "15dp",
            "width": "100%",
            "zIndex": 1
        }, controller.args[0], "lblAddressLine1Value"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblAddressLine1Value"), extendConfig({}, controller.args[2], "lblAddressLine1Value"));
        flxAddressLine1.add(lblAddressLine1, lblAddressLine1Value);
        var flxAddressLine2 = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
            "clipBounds": true,
            "id": "flxAddressLine2",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "left": "0",
            "isModalContainer": false,
            "skin": "slFbox",
            "top": "30dp",
            "width": "100%",
            "zIndex": 1
        }, controller.args[0], "flxAddressLine2"), extendConfig({}, controller.args[1], "flxAddressLine2"), extendConfig({}, controller.args[2], "flxAddressLine2"));
        flxAddressLine2.setDefaultUnit(kony.flex.DP);
        var lblAddressLine2 = new kony.ui.Label(extendConfig({
            "id": "lblAddressLine2",
            "isVisible": true,
            "left": "0dp",
            "skin": "sknLbl78818A11px",
            "i18n_text": "kony.i18n.getLocalizedString(\"i18n.Applications.Summary.AddressLine2CAPS\")",
            "top": "0px",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, controller.args[0], "lblAddressLine2"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblAddressLine2"), extendConfig({}, controller.args[2], "lblAddressLine2"));
        var lblAddressLine2Value = new kony.ui.Label(extendConfig({
            "id": "lblAddressLine2Value",
            "isVisible": true,
            "left": "0dp",
            "skin": "sknLbl485C75LatoRegular13Px",
            "text": "Address Line 2",
            "top": "15dp",
            "width": "100%",
            "zIndex": 1
        }, controller.args[0], "lblAddressLine2Value"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblAddressLine2Value"), extendConfig({}, controller.args[2], "lblAddressLine2Value"));
        flxAddressLine2.add(lblAddressLine2, lblAddressLine2Value);
        var flxAddressCountryDtls = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
            "clipBounds": true,
            "id": "flxAddressCountryDtls",
            "isVisible": true,
            "layoutType": kony.flex.FLOW_HORIZONTAL,
            "left": "0dp",
            "isModalContainer": false,
            "skin": "slFbox",
            "top": "30dp",
            "width": "100%",
            "zIndex": 1
        }, controller.args[0], "flxAddressCountryDtls"), extendConfig({}, controller.args[1], "flxAddressCountryDtls"), extendConfig({}, controller.args[2], "flxAddressCountryDtls"));
        flxAddressCountryDtls.setDefaultUnit(kony.flex.DP);
        var flxAddressCountry = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
            "clipBounds": true,
            "id": "flxAddressCountry",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "left": "0dp",
            "isModalContainer": false,
            "skin": "slFbox",
            "top": "0dp",
            "width": "32%",
            "zIndex": 1
        }, controller.args[0], "flxAddressCountry"), extendConfig({}, controller.args[1], "flxAddressCountry"), extendConfig({}, controller.args[2], "flxAddressCountry"));
        flxAddressCountry.setDefaultUnit(kony.flex.DP);
        var lblAddress1Country = new kony.ui.Label(extendConfig({
            "id": "lblAddress1Country",
            "isVisible": true,
            "left": "0px",
            "skin": "sknLbl78818A11px",
            "i18n_text": "kony.i18n.getLocalizedString(\"i18n.Applications.Summary.Country\")",
            "top": "0px",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, controller.args[0], "lblAddress1Country"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblAddress1Country"), extendConfig({}, controller.args[2], "lblAddress1Country"));
        var lblCountryValue = new kony.ui.Label(extendConfig({
            "id": "lblCountryValue",
            "isVisible": true,
            "left": "0px",
            "skin": "sknLbl485C75LatoRegular13Px",
            "text": "Contry",
            "top": "15dp",
            "width": "100%",
            "zIndex": 1
        }, controller.args[0], "lblCountryValue"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblCountryValue"), extendConfig({}, controller.args[2], "lblCountryValue"));
        flxAddressCountry.add(lblAddress1Country, lblCountryValue);
        var flxAddressState = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
            "bottom": "0px",
            "clipBounds": true,
            "id": "flxAddressState",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "left": "2%",
            "isModalContainer": false,
            "skin": "slFbox",
            "top": "0dp",
            "width": "32%",
            "zIndex": 1
        }, controller.args[0], "flxAddressState"), extendConfig({}, controller.args[1], "flxAddressState"), extendConfig({}, controller.args[2], "flxAddressState"));
        flxAddressState.setDefaultUnit(kony.flex.DP);
        var lblAddressState = new kony.ui.Label(extendConfig({
            "id": "lblAddressState",
            "isVisible": true,
            "left": "0px",
            "skin": "sknLbl78818A11px",
            "i18n_text": "kony.i18n.getLocalizedString(\"i18n.Applications.Summary.State\")",
            "top": "0px",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, controller.args[0], "lblAddressState"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblAddressState"), extendConfig({}, controller.args[2], "lblAddressState"));
        var lblStateValue = new kony.ui.Label(extendConfig({
            "id": "lblStateValue",
            "isVisible": true,
            "left": "0px",
            "skin": "sknLbl485C75LatoRegular13Px",
            "text": "State",
            "top": "15dp",
            "width": "100%",
            "zIndex": 1
        }, controller.args[0], "lblStateValue"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblStateValue"), extendConfig({}, controller.args[2], "lblStateValue"));
        flxAddressState.add(lblAddressState, lblStateValue);
        var flxAddressCity = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
            "clipBounds": true,
            "id": "flxAddressCity",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "left": "2%",
            "isModalContainer": false,
            "skin": "slFbox",
            "top": "0dp",
            "width": "32%",
            "zIndex": 1
        }, controller.args[0], "flxAddressCity"), extendConfig({}, controller.args[1], "flxAddressCity"), extendConfig({}, controller.args[2], "flxAddressCity"));
        flxAddressCity.setDefaultUnit(kony.flex.DP);
        var lblAddressCity = new kony.ui.Label(extendConfig({
            "id": "lblAddressCity",
            "isVisible": true,
            "left": "0px",
            "skin": "sknLbl78818A11px",
            "i18n_text": "kony.i18n.getLocalizedString(\"i18n.Applications.Summary.City\")",
            "top": "0px",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, controller.args[0], "lblAddressCity"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblAddressCity"), extendConfig({}, controller.args[2], "lblAddressCity"));
        var lblCityValue = new kony.ui.Label(extendConfig({
            "id": "lblCityValue",
            "isVisible": true,
            "left": "0px",
            "skin": "sknLbl485C75LatoRegular13Px",
            "text": "City",
            "top": "15dp",
            "width": "100%",
            "zIndex": 1
        }, controller.args[0], "lblCityValue"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblCityValue"), extendConfig({}, controller.args[2], "lblCityValue"));
        flxAddressCity.add(lblAddressCity, lblCityValue);
        flxAddressCountryDtls.add(flxAddressCountry, flxAddressState, flxAddressCity);
        var flxAddressZip = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
            "clipBounds": true,
            "id": "flxAddressZip",
            "isVisible": true,
            "layoutType": kony.flex.FLOW_HORIZONTAL,
            "left": "0",
            "isModalContainer": false,
            "skin": "slFbox",
            "top": "30dp",
            "width": "100%",
            "zIndex": 10
        }, controller.args[0], "flxAddressZip"), extendConfig({}, controller.args[1], "flxAddressZip"), extendConfig({}, controller.args[2], "flxAddressZip"));
        flxAddressZip.setDefaultUnit(kony.flex.DP);
        var flxAddressZipCode = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
            "clipBounds": true,
            "id": "flxAddressZipCode",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "left": "0dp",
            "isModalContainer": false,
            "skin": "slFbox",
            "top": "0dp",
            "width": "32%",
            "zIndex": 1
        }, controller.args[0], "flxAddressZipCode"), extendConfig({}, controller.args[1], "flxAddressZipCode"), extendConfig({}, controller.args[2], "flxAddressZipCode"));
        flxAddressZipCode.setDefaultUnit(kony.flex.DP);
        var lblAddressZipCode = new kony.ui.Label(extendConfig({
            "id": "lblAddressZipCode",
            "isVisible": true,
            "left": "0px",
            "skin": "sknLbl78818A11px",
            "i18n_text": "kony.i18n.getLocalizedString(\"i18n.Applications.Summary.ZipCode\")",
            "top": "0px",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, controller.args[0], "lblAddressZipCode"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblAddressZipCode"), extendConfig({}, controller.args[2], "lblAddressZipCode"));
        var lblZipCodeValue = new kony.ui.Label(extendConfig({
            "id": "lblZipCodeValue",
            "isVisible": true,
            "left": "0px",
            "skin": "sknLbl485C75LatoRegular13Px",
            "text": "zip code",
            "top": "15dp",
            "width": "100%",
            "zIndex": 1
        }, controller.args[0], "lblZipCodeValue"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblZipCodeValue"), extendConfig({}, controller.args[2], "lblZipCodeValue"));
        flxAddressZipCode.add(lblAddressZipCode, lblZipCodeValue);
        flxAddressZip.add(flxAddressZipCode);
        flxPresentEmployment.add(flxRow1, flxRow2, flxAddressLine1, flxAddressLine2, flxAddressCountryDtls, flxAddressZip);
        var flxRowMilitary = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
            "clipBounds": true,
            "id": "flxRowMilitary",
            "isVisible": false,
            "layoutType": kony.flex.FLOW_HORIZONTAL,
            "left": "0dp",
            "isModalContainer": false,
            "skin": "slFbox",
            "top": "30dp",
            "width": "100%"
        }, controller.args[0], "flxRowMilitary"), extendConfig({}, controller.args[1], "flxRowMilitary"), extendConfig({}, controller.args[2], "flxRowMilitary"));
        flxRowMilitary.setDefaultUnit(kony.flex.DP);
        var flxDesignationMilitary = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
            "clipBounds": true,
            "id": "flxDesignationMilitary",
            "isVisible": true,
            "layoutType": kony.flex.FLOW_VERTICAL,
            "left": "0%",
            "isModalContainer": false,
            "skin": "slFbox",
            "top": "0dp",
            "width": "32%",
            "zIndex": 1
        }, controller.args[0], "flxDesignationMilitary"), extendConfig({}, controller.args[1], "flxDesignationMilitary"), extendConfig({}, controller.args[2], "flxDesignationMilitary"));
        flxDesignationMilitary.setDefaultUnit(kony.flex.DP);
        var lblDesignationMilitary = new kony.ui.Label(extendConfig({
            "id": "lblDesignationMilitary",
            "isVisible": true,
            "left": "0px",
            "skin": "sknLbl78818A11px",
            "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmTrackApplication.DesignationCAPS\")",
            "top": "0px",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, controller.args[0], "lblDesignationMilitary"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblDesignationMilitary"), extendConfig({}, controller.args[2], "lblDesignationMilitary"));
        var lblDesignationValueMilitary = new kony.ui.Label(extendConfig({
            "id": "lblDesignationValueMilitary",
            "isVisible": true,
            "left": "0px",
            "skin": "sknLbl485C75LatoRegular13Px",
            "text": "Designation",
            "top": "15dp",
            "width": "100%",
            "zIndex": 1
        }, controller.args[0], "lblDesignationValueMilitary"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblDesignationValueMilitary"), extendConfig({}, controller.args[2], "lblDesignationValueMilitary"));
        flxDesignationMilitary.add(lblDesignationMilitary, lblDesignationValueMilitary);
        var flxEmpStartDateMilitary = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
            "clipBounds": true,
            "id": "flxEmpStartDateMilitary",
            "isVisible": true,
            "layoutType": kony.flex.FLOW_VERTICAL,
            "left": "2%",
            "isModalContainer": false,
            "skin": "slFbox",
            "top": "0dp",
            "width": "32%",
            "zIndex": 1
        }, controller.args[0], "flxEmpStartDateMilitary"), extendConfig({}, controller.args[1], "flxEmpStartDateMilitary"), extendConfig({}, controller.args[2], "flxEmpStartDateMilitary"));
        flxEmpStartDateMilitary.setDefaultUnit(kony.flex.DP);
        var lblStartDateMilitary = new kony.ui.Label(extendConfig({
            "id": "lblStartDateMilitary",
            "isVisible": true,
            "left": "0dp",
            "skin": "sknLbl78818A11px",
            "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmCustomerManagement.lblStartDate\")",
            "top": "0px",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, controller.args[0], "lblStartDateMilitary"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblStartDateMilitary"), extendConfig({}, controller.args[2], "lblStartDateMilitary"));
        var lblStartDateMilitaryValue = new kony.ui.Label(extendConfig({
            "id": "lblStartDateMilitaryValue",
            "isVisible": true,
            "left": "0px",
            "skin": "sknLbl485C75LatoRegular13Px",
            "text": "Start Date",
            "top": "15dp",
            "width": "100%",
            "zIndex": 1
        }, controller.args[0], "lblStartDateMilitaryValue"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblStartDateMilitaryValue"), extendConfig({}, controller.args[2], "lblStartDateMilitaryValue"));
        flxEmpStartDateMilitary.add(lblStartDateMilitary, lblStartDateMilitaryValue);
        var flxGrossIncomeMilitary = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
            "clipBounds": true,
            "id": "flxGrossIncomeMilitary",
            "isVisible": true,
            "layoutType": kony.flex.FLOW_VERTICAL,
            "left": "2%",
            "isModalContainer": false,
            "skin": "slFbox",
            "top": "0",
            "width": "32%",
            "zIndex": 1
        }, controller.args[0], "flxGrossIncomeMilitary"), extendConfig({}, controller.args[1], "flxGrossIncomeMilitary"), extendConfig({}, controller.args[2], "flxGrossIncomeMilitary"));
        flxGrossIncomeMilitary.setDefaultUnit(kony.flex.DP);
        var lblGrossIncomeMilitary = new kony.ui.Label(extendConfig({
            "id": "lblGrossIncomeMilitary",
            "isVisible": true,
            "left": "0px",
            "skin": "sknLbl78818A11px",
            "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmTrackApplication.GrossIncomeCAPS\")",
            "top": "0px",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, controller.args[0], "lblGrossIncomeMilitary"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblGrossIncomeMilitary"), extendConfig({}, controller.args[2], "lblGrossIncomeMilitary"));
        var lblGrossIncomeMilitaryValue = new kony.ui.Label(extendConfig({
            "id": "lblGrossIncomeMilitaryValue",
            "isVisible": true,
            "left": "0px",
            "skin": "sknLbl485C75LatoRegular13Px",
            "text": "Gross Income",
            "top": "15dp",
            "width": "100%",
            "zIndex": 1
        }, controller.args[0], "lblGrossIncomeMilitaryValue"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblGrossIncomeMilitaryValue"), extendConfig({}, controller.args[2], "lblGrossIncomeMilitaryValue"));
        flxGrossIncomeMilitary.add(lblGrossIncomeMilitary, lblGrossIncomeMilitaryValue);
        flxRowMilitary.add(flxDesignationMilitary, flxEmpStartDateMilitary, flxGrossIncomeMilitary);
        var flxOtherEmp = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
            "clipBounds": false,
            "id": "flxOtherEmp",
            "isVisible": false,
            "layoutType": kony.flex.FLOW_VERTICAL,
            "left": "0dp",
            "isModalContainer": false,
            "skin": "slFbox",
            "top": "30dp",
            "width": "100%"
        }, controller.args[0], "flxOtherEmp"), extendConfig({}, controller.args[1], "flxOtherEmp"), extendConfig({}, controller.args[2], "flxOtherEmp"));
        flxOtherEmp.setDefaultUnit(kony.flex.DP);
        var flxEmpType = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
            "clipBounds": true,
            "id": "flxEmpType",
            "isVisible": true,
            "layoutType": kony.flex.FLOW_VERTICAL,
            "left": "0%",
            "isModalContainer": false,
            "skin": "slFbox",
            "top": "0dp",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, controller.args[0], "flxEmpType"), extendConfig({}, controller.args[1], "flxEmpType"), extendConfig({}, controller.args[2], "flxEmpType"));
        flxEmpType.setDefaultUnit(kony.flex.DP);
        var lblEmpType = new kony.ui.Label(extendConfig({
            "id": "lblEmpType",
            "isVisible": true,
            "left": "0px",
            "skin": "sknLbl78818A11px",
            "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmTrackApplication.EmploymentTypeCAPS\")",
            "top": "0px",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, controller.args[0], "lblEmpType"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblEmpType"), extendConfig({}, controller.args[2], "lblEmpType"));
        var lblEmpTypeValue = new kony.ui.Label(extendConfig({
            "id": "lblEmpTypeValue",
            "isVisible": true,
            "left": "0px",
            "skin": "sknLbl485C75LatoRegular13Px",
            "text": "Employment Type",
            "top": "15dp",
            "width": "100%",
            "zIndex": 1
        }, controller.args[0], "lblEmpTypeValue"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblEmpTypeValue"), extendConfig({}, controller.args[2], "lblEmpTypeValue"));
        flxEmpType.add(lblEmpType, lblEmpTypeValue);
        var flxOtherRow2 = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
            "clipBounds": true,
            "id": "flxOtherRow2",
            "isVisible": true,
            "layoutType": kony.flex.FLOW_HORIZONTAL,
            "left": "0dp",
            "isModalContainer": false,
            "skin": "slFbox",
            "top": "30dp",
            "width": "100%"
        }, controller.args[0], "flxOtherRow2"), extendConfig({}, controller.args[1], "flxOtherRow2"), extendConfig({}, controller.args[2], "flxOtherRow2"));
        flxOtherRow2.setDefaultUnit(kony.flex.DP);
        var flxGrossIncomeOther = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
            "clipBounds": true,
            "id": "flxGrossIncomeOther",
            "isVisible": true,
            "layoutType": kony.flex.FLOW_VERTICAL,
            "left": "0%",
            "isModalContainer": false,
            "skin": "slFbox",
            "top": "0",
            "width": "32%",
            "zIndex": 1
        }, controller.args[0], "flxGrossIncomeOther"), extendConfig({}, controller.args[1], "flxGrossIncomeOther"), extendConfig({}, controller.args[2], "flxGrossIncomeOther"));
        flxGrossIncomeOther.setDefaultUnit(kony.flex.DP);
        var lblGrossIncomeOther = new kony.ui.Label(extendConfig({
            "id": "lblGrossIncomeOther",
            "isVisible": true,
            "left": "0px",
            "skin": "sknLbl78818A11px",
            "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmTrackApplication.GrossIncomeCAPS\")",
            "top": "0px",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, controller.args[0], "lblGrossIncomeOther"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblGrossIncomeOther"), extendConfig({}, controller.args[2], "lblGrossIncomeOther"));
        var lblGrossIncomeOtherValue = new kony.ui.Label(extendConfig({
            "id": "lblGrossIncomeOtherValue",
            "isVisible": true,
            "left": "0px",
            "skin": "sknLbl485C75LatoRegular13Px",
            "text": "Other Gross Income",
            "top": "15dp",
            "width": "100%",
            "zIndex": 1
        }, controller.args[0], "lblGrossIncomeOtherValue"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblGrossIncomeOtherValue"), extendConfig({}, controller.args[2], "lblGrossIncomeOtherValue"));
        flxGrossIncomeOther.add(lblGrossIncomeOther, lblGrossIncomeOtherValue);
        var flxPayPeriodOther = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
            "bottom": "0px",
            "clipBounds": true,
            "id": "flxPayPeriodOther",
            "isVisible": true,
            "layoutType": kony.flex.FLOW_VERTICAL,
            "left": "2%",
            "isModalContainer": false,
            "skin": "slFbox",
            "top": "0dp",
            "width": "32%",
            "zIndex": 1
        }, controller.args[0], "flxPayPeriodOther"), extendConfig({}, controller.args[1], "flxPayPeriodOther"), extendConfig({}, controller.args[2], "flxPayPeriodOther"));
        flxPayPeriodOther.setDefaultUnit(kony.flex.DP);
        var lblPayPeriodOther = new kony.ui.Label(extendConfig({
            "id": "lblPayPeriodOther",
            "isVisible": true,
            "left": "0px",
            "skin": "sknLbl78818A11px",
            "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmTrackApplication.PayPeriodCAPS\")",
            "top": "0px",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, controller.args[0], "lblPayPeriodOther"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblPayPeriodOther"), extendConfig({}, controller.args[2], "lblPayPeriodOther"));
        var lblPayPeriodOtherValue = new kony.ui.Label(extendConfig({
            "id": "lblPayPeriodOtherValue",
            "isVisible": true,
            "left": "0px",
            "skin": "sknLbl485C75LatoRegular13Px",
            "text": "Other Pay Period",
            "top": "15dp",
            "width": "100%",
            "zIndex": 1
        }, controller.args[0], "lblPayPeriodOtherValue"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblPayPeriodOtherValue"), extendConfig({}, controller.args[2], "lblPayPeriodOtherValue"));
        flxPayPeriodOther.add(lblPayPeriodOther, lblPayPeriodOtherValue);
        flxOtherRow2.add(flxGrossIncomeOther, flxPayPeriodOther);
        var flxDescription = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
            "clipBounds": true,
            "id": "flxDescription",
            "isVisible": true,
            "layoutType": kony.flex.FLOW_VERTICAL,
            "left": "0",
            "isModalContainer": false,
            "skin": "slFbox",
            "top": "30dp",
            "width": "100%",
            "zIndex": 1
        }, controller.args[0], "flxDescription"), extendConfig({}, controller.args[1], "flxDescription"), extendConfig({}, controller.args[2], "flxDescription"));
        flxDescription.setDefaultUnit(kony.flex.DP);
        var lblDescription = new kony.ui.Label(extendConfig({
            "id": "lblDescription",
            "isVisible": true,
            "left": "0px",
            "skin": "sknLbl78818A11px",
            "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmTrackApplication.DescriptionCAPS\")",
            "top": "0px",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, controller.args[0], "lblDescription"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblDescription"), extendConfig({}, controller.args[2], "lblDescription"));
        var lblDescriptionValue = new kony.ui.Label(extendConfig({
            "id": "lblDescriptionValue",
            "isVisible": true,
            "left": "0px",
            "skin": "sknLbl485C75LatoRegular13Px",
            "text": "Other Pay Period",
            "top": "15dp",
            "width": "100%",
            "zIndex": 1
        }, controller.args[0], "lblDescriptionValue"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblDescriptionValue"), extendConfig({}, controller.args[2], "lblDescriptionValue"));
        flxDescription.add(lblDescription, lblDescriptionValue);
        flxOtherEmp.add(flxEmpType, flxOtherRow2, flxDescription);
        flxEmployerDtls.add(flxPresentEmployment, flxRowMilitary, flxOtherEmp);
        var flxPreviousEmpDtls = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
            "clipBounds": true,
            "id": "flxPreviousEmpDtls",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "left": "0dp",
            "isModalContainer": false,
            "skin": "slFbox",
            "top": "30dp",
            "width": "32%",
            "zIndex": 1
        }, controller.args[0], "flxPreviousEmpDtls"), extendConfig({}, controller.args[1], "flxPreviousEmpDtls"), extendConfig({}, controller.args[2], "flxPreviousEmpDtls"));
        flxPreviousEmpDtls.setDefaultUnit(kony.flex.DP);
        var lblPreviousEmp = new kony.ui.Label(extendConfig({
            "id": "lblPreviousEmp",
            "isVisible": true,
            "left": "0px",
            "skin": "sknLbl78818A11px",
            "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmTrackApplication.PreviousEmploymentCAPS\")",
            "top": "0px",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, controller.args[0], "lblPreviousEmp"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblPreviousEmp"), extendConfig({}, controller.args[2], "lblPreviousEmp"));
        var lblPreviousEmpValue = new kony.ui.Label(extendConfig({
            "id": "lblPreviousEmpValue",
            "isVisible": true,
            "left": "0px",
            "skin": "sknLbl485C75LatoRegular13Px",
            "text": "Previous Employment",
            "top": "15dp",
            "width": "100%",
            "zIndex": 1
        }, controller.args[0], "lblPreviousEmpValue"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblPreviousEmpValue"), extendConfig({}, controller.args[2], "lblPreviousEmpValue"));
        flxPreviousEmpDtls.add(lblPreviousEmp, lblPreviousEmpValue);
        var flxPreviousEmployer = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
            "clipBounds": true,
            "id": "flxPreviousEmployer",
            "isVisible": false,
            "layoutType": kony.flex.FLOW_VERTICAL,
            "left": "0",
            "isModalContainer": false,
            "skin": "slFbox",
            "top": "30dp",
            "width": "100%",
            "zIndex": 10
        }, controller.args[0], "flxPreviousEmployer"), extendConfig({}, controller.args[1], "flxPreviousEmployer"), extendConfig({}, controller.args[2], "flxPreviousEmployer"));
        flxPreviousEmployer.setDefaultUnit(kony.flex.DP);
        var loansSectionHeader1 = new com.adminConsole.loans.applications.loansSectionHeader(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
            "id": "loansSectionHeader1",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "left": "0dp",
            "masterType": constants.MASTER_TYPE_DEFAULT,
            "isModalContainer": false,
            "skin": "slFbox",
            "top": "0dp",
            "width": "100%",
            "overrides": {
                "flxSectionHeader": {
                    "left": "0dp",
                    "right": "0dp"
                },
                "lblSectionHeader": {
                    "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmTrackApplication.PreviousEmployerDetails\")"
                }
            }
        }, controller.args[0], "loansSectionHeader1"), extendConfig({
            "overrides": {}
        }, controller.args[1], "loansSectionHeader1"), extendConfig({
            "overrides": {}
        }, controller.args[2], "loansSectionHeader1"));
        var flxPrevEmployerDtls = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
            "clipBounds": true,
            "id": "flxPrevEmployerDtls",
            "isVisible": true,
            "layoutType": kony.flex.FLOW_VERTICAL,
            "left": "0dp",
            "isModalContainer": false,
            "skin": "slFbox",
            "top": "0dp",
            "width": "100%"
        }, controller.args[0], "flxPrevEmployerDtls"), extendConfig({}, controller.args[1], "flxPrevEmployerDtls"), extendConfig({}, controller.args[2], "flxPrevEmployerDtls"));
        flxPrevEmployerDtls.setDefaultUnit(kony.flex.DP);
        var flxPrevRow1 = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
            "clipBounds": true,
            "id": "flxPrevRow1",
            "isVisible": true,
            "layoutType": kony.flex.FLOW_HORIZONTAL,
            "left": "0",
            "isModalContainer": false,
            "skin": "slFbox",
            "top": "30dp",
            "width": "100%"
        }, controller.args[0], "flxPrevRow1"), extendConfig({}, controller.args[1], "flxPrevRow1"), extendConfig({}, controller.args[2], "flxPrevRow1"));
        flxPrevRow1.setDefaultUnit(kony.flex.DP);
        var flxPrevEmployerName = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
            "clipBounds": true,
            "id": "flxPrevEmployerName",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "left": "0",
            "isModalContainer": false,
            "skin": "slFbox",
            "top": "0",
            "width": "32%",
            "zIndex": 1
        }, controller.args[0], "flxPrevEmployerName"), extendConfig({}, controller.args[1], "flxPrevEmployerName"), extendConfig({}, controller.args[2], "flxPrevEmployerName"));
        flxPrevEmployerName.setDefaultUnit(kony.flex.DP);
        var lblPrevEmployerName = new kony.ui.Label(extendConfig({
            "id": "lblPrevEmployerName",
            "isVisible": true,
            "left": "0px",
            "skin": "sknLbl78818A11px",
            "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmTrackApplication.EmployerNameCAPS\")",
            "top": "0px",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, controller.args[0], "lblPrevEmployerName"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblPrevEmployerName"), extendConfig({}, controller.args[2], "lblPrevEmployerName"));
        var lblPrevEmployerNameValue = new kony.ui.Label(extendConfig({
            "id": "lblPrevEmployerNameValue",
            "isVisible": true,
            "left": "0px",
            "skin": "sknLbl485C75LatoRegular13Px",
            "text": "Employer Name",
            "top": "15dp",
            "width": "100%",
            "zIndex": 1
        }, controller.args[0], "lblPrevEmployerNameValue"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblPrevEmployerNameValue"), extendConfig({}, controller.args[2], "lblPrevEmployerNameValue"));
        flxPrevEmployerName.add(lblPrevEmployerName, lblPrevEmployerNameValue);
        var flxPrevDesignation = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
            "clipBounds": true,
            "id": "flxPrevDesignation",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "left": "2%",
            "isModalContainer": false,
            "skin": "slFbox",
            "top": "0",
            "width": "32%",
            "zIndex": 1
        }, controller.args[0], "flxPrevDesignation"), extendConfig({}, controller.args[1], "flxPrevDesignation"), extendConfig({}, controller.args[2], "flxPrevDesignation"));
        flxPrevDesignation.setDefaultUnit(kony.flex.DP);
        var lblPrevDesignation = new kony.ui.Label(extendConfig({
            "id": "lblPrevDesignation",
            "isVisible": true,
            "left": "0px",
            "skin": "sknLbl78818A11px",
            "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmTrackApplication.DesignationCAPS\")",
            "top": "0px",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, controller.args[0], "lblPrevDesignation"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblPrevDesignation"), extendConfig({}, controller.args[2], "lblPrevDesignation"));
        var lblPrevDesignationValue = new kony.ui.Label(extendConfig({
            "id": "lblPrevDesignationValue",
            "isVisible": true,
            "left": "0px",
            "skin": "sknLbl485C75LatoRegular13Px",
            "text": "Designation",
            "top": "15dp",
            "width": "100%",
            "zIndex": 1
        }, controller.args[0], "lblPrevDesignationValue"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblPrevDesignationValue"), extendConfig({}, controller.args[2], "lblPrevDesignationValue"));
        flxPrevDesignation.add(lblPrevDesignation, lblPrevDesignationValue);
        flxPrevRow1.add(flxPrevEmployerName, flxPrevDesignation);
        var flxPrevAddressLine1 = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
            "clipBounds": true,
            "id": "flxPrevAddressLine1",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "left": "0",
            "isModalContainer": false,
            "skin": "slFbox",
            "top": "30dp",
            "width": "100%",
            "zIndex": 1
        }, controller.args[0], "flxPrevAddressLine1"), extendConfig({}, controller.args[1], "flxPrevAddressLine1"), extendConfig({}, controller.args[2], "flxPrevAddressLine1"));
        flxPrevAddressLine1.setDefaultUnit(kony.flex.DP);
        var lblPrevAddressLine1 = new kony.ui.Label(extendConfig({
            "id": "lblPrevAddressLine1",
            "isVisible": true,
            "left": "0dp",
            "skin": "sknLbl78818A11px",
            "i18n_text": "kony.i18n.getLocalizedString(\"i18n.Applications.Summary.AddressLine1\")",
            "top": "0px",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, controller.args[0], "lblPrevAddressLine1"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblPrevAddressLine1"), extendConfig({}, controller.args[2], "lblPrevAddressLine1"));
        var lblPrevAddressLine1Value = new kony.ui.Label(extendConfig({
            "id": "lblPrevAddressLine1Value",
            "isVisible": true,
            "left": "0dp",
            "skin": "sknLbl485C75LatoRegular13Px",
            "text": "Address Line 1",
            "top": "15dp",
            "width": "100%",
            "zIndex": 1
        }, controller.args[0], "lblPrevAddressLine1Value"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblPrevAddressLine1Value"), extendConfig({}, controller.args[2], "lblPrevAddressLine1Value"));
        flxPrevAddressLine1.add(lblPrevAddressLine1, lblPrevAddressLine1Value);
        var flxPrevAddressLine2 = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
            "clipBounds": true,
            "id": "flxPrevAddressLine2",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "left": "0",
            "isModalContainer": false,
            "skin": "slFbox",
            "top": "30dp",
            "width": "100%",
            "zIndex": 1
        }, controller.args[0], "flxPrevAddressLine2"), extendConfig({}, controller.args[1], "flxPrevAddressLine2"), extendConfig({}, controller.args[2], "flxPrevAddressLine2"));
        flxPrevAddressLine2.setDefaultUnit(kony.flex.DP);
        var lblPrevAddressLine2 = new kony.ui.Label(extendConfig({
            "id": "lblPrevAddressLine2",
            "isVisible": true,
            "left": "0dp",
            "skin": "sknLbl78818A11px",
            "i18n_text": "kony.i18n.getLocalizedString(\"i18n.Applications.Summary.AddressLine2CAPS\")",
            "top": "0px",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, controller.args[0], "lblPrevAddressLine2"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblPrevAddressLine2"), extendConfig({}, controller.args[2], "lblPrevAddressLine2"));
        var lblPrevAddressLine2Value = new kony.ui.Label(extendConfig({
            "id": "lblPrevAddressLine2Value",
            "isVisible": true,
            "left": "0dp",
            "skin": "sknLbl485C75LatoRegular13Px",
            "text": "Address Line 2",
            "top": "15dp",
            "width": "100%",
            "zIndex": 1
        }, controller.args[0], "lblPrevAddressLine2Value"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblPrevAddressLine2Value"), extendConfig({}, controller.args[2], "lblPrevAddressLine2Value"));
        flxPrevAddressLine2.add(lblPrevAddressLine2, lblPrevAddressLine2Value);
        var flxPrevAddressCountryDtls = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
            "clipBounds": true,
            "id": "flxPrevAddressCountryDtls",
            "isVisible": true,
            "layoutType": kony.flex.FLOW_HORIZONTAL,
            "left": "0dp",
            "isModalContainer": false,
            "skin": "slFbox",
            "top": "30dp",
            "width": "100%",
            "zIndex": 1
        }, controller.args[0], "flxPrevAddressCountryDtls"), extendConfig({}, controller.args[1], "flxPrevAddressCountryDtls"), extendConfig({}, controller.args[2], "flxPrevAddressCountryDtls"));
        flxPrevAddressCountryDtls.setDefaultUnit(kony.flex.DP);
        var flxPrevAddressCountry = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
            "clipBounds": true,
            "id": "flxPrevAddressCountry",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "left": "0dp",
            "isModalContainer": false,
            "skin": "slFbox",
            "top": "0dp",
            "width": "32%",
            "zIndex": 1
        }, controller.args[0], "flxPrevAddressCountry"), extendConfig({}, controller.args[1], "flxPrevAddressCountry"), extendConfig({}, controller.args[2], "flxPrevAddressCountry"));
        flxPrevAddressCountry.setDefaultUnit(kony.flex.DP);
        var lblPrevAddress1 = new kony.ui.Label(extendConfig({
            "id": "lblPrevAddress1",
            "isVisible": true,
            "left": "0px",
            "skin": "sknLbl78818A11px",
            "i18n_text": "kony.i18n.getLocalizedString(\"i18n.Applications.Summary.Country\")",
            "top": "0px",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, controller.args[0], "lblPrevAddress1"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblPrevAddress1"), extendConfig({}, controller.args[2], "lblPrevAddress1"));
        var lblPrevCountryValue = new kony.ui.Label(extendConfig({
            "id": "lblPrevCountryValue",
            "isVisible": true,
            "left": "0px",
            "skin": "sknLbl485C75LatoRegular13Px",
            "text": "Contry",
            "top": "15dp",
            "width": "100%",
            "zIndex": 1
        }, controller.args[0], "lblPrevCountryValue"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblPrevCountryValue"), extendConfig({}, controller.args[2], "lblPrevCountryValue"));
        flxPrevAddressCountry.add(lblPrevAddress1, lblPrevCountryValue);
        var flxPrevAddressState = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
            "bottom": "0px",
            "clipBounds": true,
            "id": "flxPrevAddressState",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "left": "2%",
            "isModalContainer": false,
            "skin": "slFbox",
            "top": "0dp",
            "width": "32%",
            "zIndex": 1
        }, controller.args[0], "flxPrevAddressState"), extendConfig({}, controller.args[1], "flxPrevAddressState"), extendConfig({}, controller.args[2], "flxPrevAddressState"));
        flxPrevAddressState.setDefaultUnit(kony.flex.DP);
        var lblPrevAddressState = new kony.ui.Label(extendConfig({
            "id": "lblPrevAddressState",
            "isVisible": true,
            "left": "0px",
            "skin": "sknLbl78818A11px",
            "i18n_text": "kony.i18n.getLocalizedString(\"i18n.Applications.Summary.State\")",
            "top": "0px",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, controller.args[0], "lblPrevAddressState"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblPrevAddressState"), extendConfig({}, controller.args[2], "lblPrevAddressState"));
        var lblPrevStateValue = new kony.ui.Label(extendConfig({
            "id": "lblPrevStateValue",
            "isVisible": true,
            "left": "0px",
            "skin": "sknLbl485C75LatoRegular13Px",
            "text": "State",
            "top": "15dp",
            "width": "100%",
            "zIndex": 1
        }, controller.args[0], "lblPrevStateValue"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblPrevStateValue"), extendConfig({}, controller.args[2], "lblPrevStateValue"));
        flxPrevAddressState.add(lblPrevAddressState, lblPrevStateValue);
        var flxPrevAddressCity = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
            "clipBounds": true,
            "id": "flxPrevAddressCity",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "left": "2%",
            "isModalContainer": false,
            "skin": "slFbox",
            "top": "0dp",
            "width": "32%",
            "zIndex": 1
        }, controller.args[0], "flxPrevAddressCity"), extendConfig({}, controller.args[1], "flxPrevAddressCity"), extendConfig({}, controller.args[2], "flxPrevAddressCity"));
        flxPrevAddressCity.setDefaultUnit(kony.flex.DP);
        var lblPrevAddressCity = new kony.ui.Label(extendConfig({
            "id": "lblPrevAddressCity",
            "isVisible": true,
            "left": "0px",
            "skin": "sknLbl78818A11px",
            "i18n_text": "kony.i18n.getLocalizedString(\"i18n.Applications.Summary.City\")",
            "top": "0px",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, controller.args[0], "lblPrevAddressCity"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblPrevAddressCity"), extendConfig({}, controller.args[2], "lblPrevAddressCity"));
        var lblPrevCityValue = new kony.ui.Label(extendConfig({
            "id": "lblPrevCityValue",
            "isVisible": true,
            "left": "0px",
            "skin": "sknLbl485C75LatoRegular13Px",
            "text": "City",
            "top": "15dp",
            "width": "100%",
            "zIndex": 1
        }, controller.args[0], "lblPrevCityValue"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblPrevCityValue"), extendConfig({}, controller.args[2], "lblPrevCityValue"));
        flxPrevAddressCity.add(lblPrevAddressCity, lblPrevCityValue);
        flxPrevAddressCountryDtls.add(flxPrevAddressCountry, flxPrevAddressState, flxPrevAddressCity);
        var flxPrevAddressZip = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
            "clipBounds": true,
            "id": "flxPrevAddressZip",
            "isVisible": true,
            "layoutType": kony.flex.FLOW_HORIZONTAL,
            "left": "0",
            "isModalContainer": false,
            "skin": "slFbox",
            "top": "30dp",
            "width": "100%",
            "zIndex": 10
        }, controller.args[0], "flxPrevAddressZip"), extendConfig({}, controller.args[1], "flxPrevAddressZip"), extendConfig({}, controller.args[2], "flxPrevAddressZip"));
        flxPrevAddressZip.setDefaultUnit(kony.flex.DP);
        var flxPrevAddressZipCode = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
            "clipBounds": true,
            "id": "flxPrevAddressZipCode",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "left": "0dp",
            "isModalContainer": false,
            "skin": "slFbox",
            "top": "0dp",
            "width": "32%",
            "zIndex": 1
        }, controller.args[0], "flxPrevAddressZipCode"), extendConfig({}, controller.args[1], "flxPrevAddressZipCode"), extendConfig({}, controller.args[2], "flxPrevAddressZipCode"));
        flxPrevAddressZipCode.setDefaultUnit(kony.flex.DP);
        var lblPrevAddressZipCode = new kony.ui.Label(extendConfig({
            "id": "lblPrevAddressZipCode",
            "isVisible": true,
            "left": "0px",
            "skin": "sknLbl78818A11px",
            "i18n_text": "kony.i18n.getLocalizedString(\"i18n.Applications.Summary.ZipCode\")",
            "top": "0px",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, controller.args[0], "lblPrevAddressZipCode"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblPrevAddressZipCode"), extendConfig({}, controller.args[2], "lblPrevAddressZipCode"));
        var lblPrevZipCodeValue = new kony.ui.Label(extendConfig({
            "id": "lblPrevZipCodeValue",
            "isVisible": true,
            "left": "0px",
            "skin": "sknLbl485C75LatoRegular13Px",
            "text": "zip code",
            "top": "15dp",
            "width": "100%",
            "zIndex": 1
        }, controller.args[0], "lblPrevZipCodeValue"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblPrevZipCodeValue"), extendConfig({}, controller.args[2], "lblPrevZipCodeValue"));
        flxPrevAddressZipCode.add(lblPrevAddressZipCode, lblPrevZipCodeValue);
        flxPrevAddressZip.add(flxPrevAddressZipCode);
        flxPrevEmployerDtls.add(flxPrevRow1, flxPrevAddressLine1, flxPrevAddressLine2, flxPrevAddressCountryDtls, flxPrevAddressZip);
        flxPreviousEmployer.add(loansSectionHeader1, flxPrevEmployerDtls);
        flxEmployerInfo.add(flxEmployerDtls, flxPreviousEmpDtls, flxPreviousEmployer);
        viewEmployerDetails.add(loansSectionHeader, flxEmployerInfo);
        return viewEmployerDetails;
    }
})