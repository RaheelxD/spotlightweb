define(function() {
    return function(controller) {
        var editPersonalInfo = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
            "clipBounds": true,
            "isMaster": true,
            "id": "editPersonalInfo",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "left": "0dp",
            "isModalContainer": false,
            "skin": "slFbox",
            "top": "30dp",
            "width": kony.flex.USE_PREFFERED_SIZE
        }, controller.args[0], "editPersonalInfo"), extendConfig({}, controller.args[1], "editPersonalInfo"), extendConfig({}, controller.args[2], "editPersonalInfo"));
        editPersonalInfo.setDefaultUnit(kony.flex.DP);
        var loansSectionHeader = new com.adminConsole.loans.applications.loansSectionHeader(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
            "id": "loansSectionHeader",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "left": "0dp",
            "masterType": constants.MASTER_TYPE_DEFAULT,
            "isModalContainer": false,
            "skin": "slFbox",
            "top": "0dp",
            "width": "100%",
            "overrides": {
                "btnEdit": {
                    "isVisible": false
                },
                "lblSectionHeader": {
                    "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmTrackApplication.PersonalDetails\")"
                }
            }
        }, controller.args[0], "loansSectionHeader"), extendConfig({
            "overrides": {}
        }, controller.args[1], "loansSectionHeader"), extendConfig({
            "overrides": {}
        }, controller.args[2], "loansSectionHeader"));
        var flxSection = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
            "clipBounds": true,
            "id": "flxSection",
            "isVisible": true,
            "layoutType": kony.flex.FLOW_VERTICAL,
            "left": "20dp",
            "isModalContainer": false,
            "right": "20dp",
            "skin": "slFbox",
            "top": "42dp",
            "width": kony.flex.USE_PREFFERED_SIZE
        }, controller.args[0], "flxSection"), extendConfig({}, controller.args[1], "flxSection"), extendConfig({}, controller.args[2], "flxSection"));
        flxSection.setDefaultUnit(kony.flex.DP);
        var flxRow1 = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
            "clipBounds": true,
            "id": "flxRow1",
            "isVisible": true,
            "layoutType": kony.flex.FLOW_HORIZONTAL,
            "left": "0dp",
            "isModalContainer": false,
            "skin": "slFbox",
            "top": "30dp",
            "width": "100%"
        }, controller.args[0], "flxRow1"), extendConfig({}, controller.args[1], "flxRow1"), extendConfig({}, controller.args[2], "flxRow1"));
        flxRow1.setDefaultUnit(kony.flex.DP);
        var flxFirstName = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
            "clipBounds": true,
            "id": "flxFirstName",
            "isVisible": true,
            "layoutType": kony.flex.FLOW_VERTICAL,
            "isModalContainer": false,
            "skin": "slFbox",
            "width": "32%",
            "zIndex": 1
        }, controller.args[0], "flxFirstName"), extendConfig({}, controller.args[1], "flxFirstName"), extendConfig({}, controller.args[2], "flxFirstName"));
        flxFirstName.setDefaultUnit(kony.flex.DP);
        var flxFirstNameLabel = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
            "clipBounds": true,
            "id": "flxFirstNameLabel",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "left": "0dp",
            "isModalContainer": false,
            "skin": "slFbox",
            "top": "0dp",
            "width": "100%"
        }, controller.args[0], "flxFirstNameLabel"), extendConfig({}, controller.args[1], "flxFirstNameLabel"), extendConfig({}, controller.args[2], "flxFirstNameLabel"));
        flxFirstNameLabel.setDefaultUnit(kony.flex.DP);
        var lblName = new kony.ui.Label(extendConfig({
            "id": "lblName",
            "isVisible": true,
            "left": "0px",
            "skin": "sknLblLatoReg78818912px",
            "i18n_text": "kony.i18n.getLocalizedString(\"i18n.Appliactions.FirstNamePlaceholder\")",
            "top": "0px",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, controller.args[0], "lblName"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblName"), extendConfig({}, controller.args[2], "lblName"));
        var lblFirstNameSize = new kony.ui.Label(extendConfig({
            "id": "lblFirstNameSize",
            "isVisible": false,
            "right": "5dp",
            "skin": "sknLblLatoReg78818912px",
            "text": "0/35",
            "top": "0dp",
            "width": "50dp",
            "zIndex": 1
        }, controller.args[0], "lblFirstNameSize"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_TOP_RIGHT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblFirstNameSize"), extendConfig({}, controller.args[2], "lblFirstNameSize"));
        flxFirstNameLabel.add(lblName, lblFirstNameSize);
        var txtNameValue = new kony.ui.TextBox2(extendConfig({
            "autoCapitalize": constants.TEXTBOX_AUTO_CAPITALIZE_NONE,
            "focusSkin": "skntxtbxDetailsFocus14px",
            "height": "40dp",
            "id": "txtNameValue",
            "isVisible": true,
            "keyBoardStyle": constants.TEXTBOX_KEY_BOARD_STYLE_DEFAULT,
            "left": "0px",
            "maxTextLength": 64,
            "placeholder": "First Name",
            "right": "5dp",
            "secureTextEntry": false,
            "skin": "skntxtbxDetails0bbf1235271384a",
            "textInputMode": constants.TEXTBOX_INPUT_MODE_ANY,
            "top": "15dp",
            "width": "100%",
            "zIndex": 1
        }, controller.args[0], "txtNameValue"), extendConfig({
            "containerHeightMode": constants.TEXTBOX_FONT_METRICS_DRIVEN_HEIGHT,
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [3, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "txtNameValue"), extendConfig({
            "autoCorrect": false
        }, controller.args[2], "txtNameValue"));
        var firstNameErrorMsg = new com.adminConsole.loans.applications.errorMsg(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
            "id": "firstNameErrorMsg",
            "isVisible": false,
            "layoutType": kony.flex.FLOW_HORIZONTAL,
            "left": "0dp",
            "masterType": constants.MASTER_TYPE_DEFAULT,
            "isModalContainer": false,
            "skin": "slFbox",
            "top": "10dp",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "overrides": {
                "errorMsg": {
                    "isVisible": false
                }
            }
        }, controller.args[0], "firstNameErrorMsg"), extendConfig({
            "overrides": {}
        }, controller.args[1], "firstNameErrorMsg"), extendConfig({
            "overrides": {}
        }, controller.args[2], "firstNameErrorMsg"));
        flxFirstName.add(flxFirstNameLabel, txtNameValue, firstNameErrorMsg);
        var flxMiddleName = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
            "clipBounds": true,
            "id": "flxMiddleName",
            "isVisible": true,
            "layoutType": kony.flex.FLOW_VERTICAL,
            "left": "2%",
            "isModalContainer": false,
            "skin": "slFbox",
            "width": "32%",
            "zIndex": 1
        }, controller.args[0], "flxMiddleName"), extendConfig({}, controller.args[1], "flxMiddleName"), extendConfig({}, controller.args[2], "flxMiddleName"));
        flxMiddleName.setDefaultUnit(kony.flex.DP);
        var flxMiddleNameLabel = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
            "clipBounds": true,
            "id": "flxMiddleNameLabel",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "left": "0dp",
            "isModalContainer": false,
            "skin": "slFbox",
            "top": "0dp",
            "width": "100%"
        }, controller.args[0], "flxMiddleNameLabel"), extendConfig({}, controller.args[1], "flxMiddleNameLabel"), extendConfig({}, controller.args[2], "flxMiddleNameLabel"));
        flxMiddleNameLabel.setDefaultUnit(kony.flex.DP);
        var lblMiddleNameStatic = new kony.ui.Label(extendConfig({
            "id": "lblMiddleNameStatic",
            "isVisible": true,
            "left": "0px",
            "skin": "sknLblLatoReg78818912px",
            "i18n_text": "kony.i18n.getLocalizedString(\"i18n.Applications.MiddleName\")",
            "top": "0px",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, controller.args[0], "lblMiddleNameStatic"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblMiddleNameStatic"), extendConfig({}, controller.args[2], "lblMiddleNameStatic"));
        var lblMiddleNameSize = new kony.ui.Label(extendConfig({
            "id": "lblMiddleNameSize",
            "isVisible": false,
            "right": "5dp",
            "skin": "sknLblLatoReg78818912px",
            "text": "0/35",
            "top": "0dp",
            "width": "50dp",
            "zIndex": 1
        }, controller.args[0], "lblMiddleNameSize"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_TOP_RIGHT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblMiddleNameSize"), extendConfig({}, controller.args[2], "lblMiddleNameSize"));
        flxMiddleNameLabel.add(lblMiddleNameStatic, lblMiddleNameSize);
        var txtMiddleNameValue = new kony.ui.TextBox2(extendConfig({
            "autoCapitalize": constants.TEXTBOX_AUTO_CAPITALIZE_NONE,
            "focusSkin": "skntxtbxDetailsFocus14px",
            "height": "40dp",
            "id": "txtMiddleNameValue",
            "isVisible": true,
            "keyBoardStyle": constants.TEXTBOX_KEY_BOARD_STYLE_DEFAULT,
            "left": "0px",
            "maxTextLength": 64,
            "i18n_placeholder": "kony.i18n.getLocalizedString(\"i18n.frmUsers.lblMiddleName\")",
            "right": "5dp",
            "secureTextEntry": false,
            "skin": "skntxtbxDetails0bbf1235271384a",
            "textInputMode": constants.TEXTBOX_INPUT_MODE_ANY,
            "top": "15dp",
            "width": "100%",
            "zIndex": 1
        }, controller.args[0], "txtMiddleNameValue"), extendConfig({
            "containerHeightMode": constants.TEXTBOX_FONT_METRICS_DRIVEN_HEIGHT,
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [3, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "txtMiddleNameValue"), extendConfig({
            "autoCorrect": false
        }, controller.args[2], "txtMiddleNameValue"));
        var middleNameErrorMsg = new com.adminConsole.loans.applications.errorMsg(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
            "id": "middleNameErrorMsg",
            "isVisible": false,
            "layoutType": kony.flex.FLOW_HORIZONTAL,
            "left": "0dp",
            "masterType": constants.MASTER_TYPE_DEFAULT,
            "isModalContainer": false,
            "skin": "slFbox",
            "top": "10dp",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "overrides": {
                "errorMsg": {
                    "isVisible": false
                }
            }
        }, controller.args[0], "middleNameErrorMsg"), extendConfig({
            "overrides": {}
        }, controller.args[1], "middleNameErrorMsg"), extendConfig({
            "overrides": {}
        }, controller.args[2], "middleNameErrorMsg"));
        flxMiddleName.add(flxMiddleNameLabel, txtMiddleNameValue, middleNameErrorMsg);
        var flxLastName = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
            "clipBounds": true,
            "id": "flxLastName",
            "isVisible": true,
            "layoutType": kony.flex.FLOW_VERTICAL,
            "left": "2%",
            "isModalContainer": false,
            "skin": "slFbox",
            "width": "32%",
            "zIndex": 1
        }, controller.args[0], "flxLastName"), extendConfig({}, controller.args[1], "flxLastName"), extendConfig({}, controller.args[2], "flxLastName"));
        flxLastName.setDefaultUnit(kony.flex.DP);
        var flxLastNameLabel = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
            "clipBounds": true,
            "id": "flxLastNameLabel",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "left": "0dp",
            "isModalContainer": false,
            "skin": "slFbox",
            "top": "0dp",
            "width": "100%"
        }, controller.args[0], "flxLastNameLabel"), extendConfig({}, controller.args[1], "flxLastNameLabel"), extendConfig({}, controller.args[2], "flxLastNameLabel"));
        flxLastNameLabel.setDefaultUnit(kony.flex.DP);
        var lbllastName = new kony.ui.Label(extendConfig({
            "id": "lbllastName",
            "isVisible": true,
            "left": "0px",
            "skin": "sknLblLatoReg78818912px",
            "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmUsers.lblLastName\")",
            "top": "0px",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, controller.args[0], "lbllastName"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lbllastName"), extendConfig({}, controller.args[2], "lbllastName"));
        var lblLastNameSize = new kony.ui.Label(extendConfig({
            "id": "lblLastNameSize",
            "isVisible": false,
            "right": "5dp",
            "skin": "sknLblLatoReg78818912px",
            "text": "0/35",
            "top": "0dp",
            "width": "50dp",
            "zIndex": 1
        }, controller.args[0], "lblLastNameSize"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_TOP_RIGHT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblLastNameSize"), extendConfig({}, controller.args[2], "lblLastNameSize"));
        flxLastNameLabel.add(lbllastName, lblLastNameSize);
        var txtLastNameValue = new kony.ui.TextBox2(extendConfig({
            "autoCapitalize": constants.TEXTBOX_AUTO_CAPITALIZE_NONE,
            "focusSkin": "skntxtbxDetailsFocus14px",
            "height": "40dp",
            "id": "txtLastNameValue",
            "isVisible": true,
            "keyBoardStyle": constants.TEXTBOX_KEY_BOARD_STYLE_DEFAULT,
            "left": "0px",
            "maxTextLength": 64,
            "i18n_placeholder": "kony.i18n.getLocalizedString(\"i18n.frmUsers.lblLastName\")",
            "right": "5dp",
            "secureTextEntry": false,
            "skin": "skntxtbxDetails0bbf1235271384a",
            "textInputMode": constants.TEXTBOX_INPUT_MODE_ANY,
            "top": "15dp",
            "width": "100%",
            "zIndex": 1
        }, controller.args[0], "txtLastNameValue"), extendConfig({
            "containerHeightMode": constants.TEXTBOX_FONT_METRICS_DRIVEN_HEIGHT,
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [3, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "txtLastNameValue"), extendConfig({
            "autoCorrect": false
        }, controller.args[2], "txtLastNameValue"));
        var lastNameErrorMsg = new com.adminConsole.loans.applications.errorMsg(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
            "id": "lastNameErrorMsg",
            "isVisible": false,
            "layoutType": kony.flex.FLOW_HORIZONTAL,
            "left": "0dp",
            "masterType": constants.MASTER_TYPE_DEFAULT,
            "isModalContainer": false,
            "skin": "slFbox",
            "top": "10dp",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "overrides": {
                "errorMsg": {
                    "isVisible": false
                }
            }
        }, controller.args[0], "lastNameErrorMsg"), extendConfig({
            "overrides": {}
        }, controller.args[1], "lastNameErrorMsg"), extendConfig({
            "overrides": {}
        }, controller.args[2], "lastNameErrorMsg"));
        flxLastName.add(flxLastNameLabel, txtLastNameValue, lastNameErrorMsg);
        flxRow1.add(flxFirstName, flxMiddleName, flxLastName);
        var flxRow2 = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
            "clipBounds": true,
            "id": "flxRow2",
            "isVisible": true,
            "layoutType": kony.flex.FLOW_HORIZONTAL,
            "left": "0dp",
            "isModalContainer": false,
            "skin": "slFbox",
            "top": "30dp",
            "width": "100%"
        }, controller.args[0], "flxRow2"), extendConfig({}, controller.args[1], "flxRow2"), extendConfig({}, controller.args[2], "flxRow2"));
        flxRow2.setDefaultUnit(kony.flex.DP);
        var flxSuffix = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
            "bottom": "0px",
            "clipBounds": true,
            "id": "flxSuffix",
            "isVisible": true,
            "layoutType": kony.flex.FLOW_VERTICAL,
            "isModalContainer": false,
            "skin": "slFbox",
            "top": "0dp",
            "width": "32%",
            "zIndex": 1
        }, controller.args[0], "flxSuffix"), extendConfig({}, controller.args[1], "flxSuffix"), extendConfig({}, controller.args[2], "flxSuffix"));
        flxSuffix.setDefaultUnit(kony.flex.DP);
        var lblSuffix = new kony.ui.Label(extendConfig({
            "id": "lblSuffix",
            "isVisible": true,
            "left": "0px",
            "skin": "sknLblLatoReg78818912px",
            "i18n_text": "kony.i18n.getLocalizedString(\"i18n.Applications.Suffix\")",
            "top": "0px",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, controller.args[0], "lblSuffix"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblSuffix"), extendConfig({}, controller.args[2], "lblSuffix"));
        var lstSuffixValue = new kony.ui.ListBox(extendConfig({
            "height": "40px",
            "id": "lstSuffixValue",
            "isVisible": true,
            "left": "0px",
            "skin": "sknlbxBgffffffBorderc1c9ceRadius3Px",
            "top": "15dp",
            "width": "100%",
            "zIndex": 1
        }, controller.args[0], "lstSuffixValue"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [3, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lstSuffixValue"), extendConfig({
            "hoverSkin": "sknlstbxcursor",
            "multiSelect": false
        }, controller.args[2], "lstSuffixValue"));
        flxSuffix.add(lblSuffix, lstSuffixValue);
        var flxDateOfBirthPersonalInfo = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
            "clipBounds": true,
            "id": "flxDateOfBirthPersonalInfo",
            "isVisible": true,
            "layoutType": kony.flex.FLOW_VERTICAL,
            "left": "2%",
            "isModalContainer": false,
            "skin": "slFbox",
            "top": "0dp",
            "width": "32%",
            "zIndex": 1
        }, controller.args[0], "flxDateOfBirthPersonalInfo"), extendConfig({}, controller.args[1], "flxDateOfBirthPersonalInfo"), extendConfig({}, controller.args[2], "flxDateOfBirthPersonalInfo"));
        flxDateOfBirthPersonalInfo.setDefaultUnit(kony.flex.DP);
        var lblDateOfBirth = new kony.ui.Label(extendConfig({
            "id": "lblDateOfBirth",
            "isVisible": true,
            "left": "0dp",
            "skin": "sknLblLatoReg78818912px",
            "i18n_text": "kony.i18n.getLocalizedString(\"i18n.Applications.DateOfBirth\")",
            "top": "0px",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, controller.args[0], "lblDateOfBirth"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblDateOfBirth"), extendConfig({}, controller.args[2], "lblDateOfBirth"));
        var flxDropDownAdmin2 = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "clipBounds": true,
            "height": "40dp",
            "id": "flxDropDownAdmin2",
            "isVisible": false,
            "layoutType": kony.flex.FREE_FORM,
            "left": "0dp",
            "isModalContainer": false,
            "skin": "slFbox",
            "top": "15dp",
            "width": "100%",
            "zIndex": 1
        }, controller.args[0], "flxDropDownAdmin2"), extendConfig({}, controller.args[1], "flxDropDownAdmin2"), extendConfig({}, controller.args[2], "flxDropDownAdmin2"));
        flxDropDownAdmin2.setDefaultUnit(kony.flex.DP);
        flxDropDownAdmin2.add();
        var txtDobValue = new kony.ui.TextBox2(extendConfig({
            "autoCapitalize": constants.TEXTBOX_AUTO_CAPITALIZE_NONE,
            "focusSkin": "skntxtbxDetailsFocus14px",
            "height": "40dp",
            "id": "txtDobValue",
            "isVisible": true,
            "keyBoardStyle": constants.TEXTBOX_KEY_BOARD_STYLE_DEFAULT,
            "left": "0px",
            "maxTextLength": 10,
            "placeholder": "MM/DD/YYYY",
            "right": "5dp",
            "secureTextEntry": false,
            "skin": "skntxtbxDetails0bbf1235271384a",
            "textInputMode": constants.TEXTBOX_INPUT_MODE_ANY,
            "top": "15dp",
            "width": "100%",
            "zIndex": 1
        }, controller.args[0], "txtDobValue"), extendConfig({
            "containerHeightMode": constants.TEXTBOX_FONT_METRICS_DRIVEN_HEIGHT,
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [2, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "txtDobValue"), extendConfig({
            "autoCorrect": false
        }, controller.args[2], "txtDobValue"));
        var lblDobTip = new kony.ui.Label(extendConfig({
            "bottom": "0dp",
            "id": "lblDobTip",
            "isVisible": false,
            "left": "0dp",
            "right": "20dp",
            "skin": "skn11px9CA9BA",
            "text": "Tip : Age not less than 18 years",
            "top": "10dp",
            "zIndex": 1
        }, controller.args[0], "lblDobTip"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblDobTip"), extendConfig({}, controller.args[2], "lblDobTip"));
        var dobErrorMsg = new com.adminConsole.loans.applications.errorMsg(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
            "id": "dobErrorMsg",
            "isVisible": false,
            "layoutType": kony.flex.FLOW_HORIZONTAL,
            "left": "0dp",
            "masterType": constants.MASTER_TYPE_DEFAULT,
            "isModalContainer": false,
            "skin": "slFbox",
            "top": "10dp",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "overrides": {
                "errorMsg": {
                    "isVisible": false
                }
            }
        }, controller.args[0], "dobErrorMsg"), extendConfig({
            "overrides": {}
        }, controller.args[1], "dobErrorMsg"), extendConfig({
            "overrides": {}
        }, controller.args[2], "dobErrorMsg"));
        flxDateOfBirthPersonalInfo.add(lblDateOfBirth, flxDropDownAdmin2, txtDobValue, lblDobTip, dobErrorMsg);
        var flxEmailAddress = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
            "clipBounds": true,
            "id": "flxEmailAddress",
            "isVisible": true,
            "layoutType": kony.flex.FLOW_VERTICAL,
            "left": "2%",
            "isModalContainer": false,
            "skin": "slFbox",
            "top": "0dp",
            "width": "32%",
            "zIndex": 1
        }, controller.args[0], "flxEmailAddress"), extendConfig({}, controller.args[1], "flxEmailAddress"), extendConfig({}, controller.args[2], "flxEmailAddress"));
        flxEmailAddress.setDefaultUnit(kony.flex.DP);
        var flxEmailAddressLabel = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
            "clipBounds": true,
            "id": "flxEmailAddressLabel",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "left": "0dp",
            "isModalContainer": false,
            "skin": "slFbox",
            "top": "0dp",
            "width": "100%"
        }, controller.args[0], "flxEmailAddressLabel"), extendConfig({}, controller.args[1], "flxEmailAddressLabel"), extendConfig({}, controller.args[2], "flxEmailAddressLabel"));
        flxEmailAddressLabel.setDefaultUnit(kony.flex.DP);
        var lblEmailAddress = new kony.ui.Label(extendConfig({
            "id": "lblEmailAddress",
            "isVisible": true,
            "left": "0px",
            "skin": "sknLblLatoReg78818912px",
            "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmAlertsManagement.Email\")",
            "top": "0px",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, controller.args[0], "lblEmailAddress"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblEmailAddress"), extendConfig({}, controller.args[2], "lblEmailAddress"));
        var lblEmailSize = new kony.ui.Label(extendConfig({
            "id": "lblEmailSize",
            "isVisible": false,
            "right": "5dp",
            "skin": "sknLblLatoReg78818912px",
            "text": "0/70",
            "top": "0dp",
            "width": "50dp",
            "zIndex": 1
        }, controller.args[0], "lblEmailSize"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_TOP_RIGHT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblEmailSize"), extendConfig({}, controller.args[2], "lblEmailSize"));
        flxEmailAddressLabel.add(lblEmailAddress, lblEmailSize);
        var txtEmailAddressValue = new kony.ui.TextBox2(extendConfig({
            "autoCapitalize": constants.TEXTBOX_AUTO_CAPITALIZE_NONE,
            "focusSkin": "skntxtbxDetailsFocus14px",
            "height": "40dp",
            "id": "txtEmailAddressValue",
            "isVisible": true,
            "keyBoardStyle": constants.TEXTBOX_KEY_BOARD_STYLE_DEFAULT,
            "left": "0px",
            "maxTextLength": 70,
            "i18n_placeholder": "kony.i18n.getLocalizedString(\"i18n.Applications.EmailAddress\")",
            "right": "5dp",
            "secureTextEntry": false,
            "skin": "skntxtbxDetails0bbf1235271384a",
            "textInputMode": constants.TEXTBOX_INPUT_MODE_ANY,
            "top": "15dp",
            "width": "100%",
            "zIndex": 1
        }, controller.args[0], "txtEmailAddressValue"), extendConfig({
            "containerHeightMode": constants.TEXTBOX_FONT_METRICS_DRIVEN_HEIGHT,
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [3, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "txtEmailAddressValue"), extendConfig({
            "autoCorrect": false
        }, controller.args[2], "txtEmailAddressValue"));
        var emailAddressErrorMsg = new com.adminConsole.loans.applications.errorMsg(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
            "id": "emailAddressErrorMsg",
            "isVisible": false,
            "layoutType": kony.flex.FLOW_HORIZONTAL,
            "left": "0dp",
            "masterType": constants.MASTER_TYPE_DEFAULT,
            "isModalContainer": false,
            "skin": "slFbox",
            "top": "10dp",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "overrides": {
                "errorMsg": {
                    "isVisible": false
                }
            }
        }, controller.args[0], "emailAddressErrorMsg"), extendConfig({
            "overrides": {}
        }, controller.args[1], "emailAddressErrorMsg"), extendConfig({
            "overrides": {}
        }, controller.args[2], "emailAddressErrorMsg"));
        flxEmailAddress.add(flxEmailAddressLabel, txtEmailAddressValue, emailAddressErrorMsg);
        flxRow2.add(flxSuffix, flxDateOfBirthPersonalInfo, flxEmailAddress);
        var flxRow3 = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
            "clipBounds": true,
            "id": "flxRow3",
            "isVisible": true,
            "layoutType": kony.flex.FLOW_HORIZONTAL,
            "left": "0dp",
            "isModalContainer": false,
            "skin": "slFbox",
            "top": "30dp",
            "width": "100%"
        }, controller.args[0], "flxRow3"), extendConfig({}, controller.args[1], "flxRow3"), extendConfig({}, controller.args[2], "flxRow3"));
        flxRow3.setDefaultUnit(kony.flex.DP);
        var flxSSN = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
            "clipBounds": true,
            "id": "flxSSN",
            "isVisible": true,
            "layoutType": kony.flex.FLOW_VERTICAL,
            "isModalContainer": false,
            "skin": "slFbox",
            "top": "0dp",
            "width": "32%",
            "zIndex": 1
        }, controller.args[0], "flxSSN"), extendConfig({}, controller.args[1], "flxSSN"), extendConfig({}, controller.args[2], "flxSSN"));
        flxSSN.setDefaultUnit(kony.flex.DP);
        var flxSSNLabel = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
            "clipBounds": true,
            "id": "flxSSNLabel",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "left": "0dp",
            "isModalContainer": false,
            "skin": "slFbox",
            "top": "0dp",
            "width": "100%"
        }, controller.args[0], "flxSSNLabel"), extendConfig({}, controller.args[1], "flxSSNLabel"), extendConfig({}, controller.args[2], "flxSSNLabel"));
        flxSSNLabel.setDefaultUnit(kony.flex.DP);
        var lblSSN = new kony.ui.Label(extendConfig({
            "id": "lblSSN",
            "isVisible": true,
            "left": "0px",
            "skin": "sknLblLatoReg78818912px",
            "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmCustomerManagementController.SSN\")",
            "top": "0px",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, controller.args[0], "lblSSN"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblSSN"), extendConfig({}, controller.args[2], "lblSSN"));
        var lblSSNSize = new kony.ui.Label(extendConfig({
            "id": "lblSSNSize",
            "isVisible": false,
            "right": "5dp",
            "skin": "sknLblLatoReg78818912px",
            "text": "0/10",
            "top": "0dp",
            "width": "50dp",
            "zIndex": 1
        }, controller.args[0], "lblSSNSize"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_TOP_RIGHT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblSSNSize"), extendConfig({}, controller.args[2], "lblSSNSize"));
        flxSSNLabel.add(lblSSN, lblSSNSize);
        var txtSSNValue = new kony.ui.TextBox2(extendConfig({
            "autoCapitalize": constants.TEXTBOX_AUTO_CAPITALIZE_NONE,
            "focusSkin": "skntxtbxDetailsFocus14px",
            "height": "40dp",
            "id": "txtSSNValue",
            "isVisible": true,
            "keyBoardStyle": constants.TEXTBOX_KEY_BOARD_STYLE_PHONE_PAD,
            "left": "0px",
            "maxTextLength": 9,
            "i18n_placeholder": "kony.i18n.getLocalizedString(\"i18n.frmCustomerManagementController.SSN\")",
            "right": "0px",
            "secureTextEntry": false,
            "skin": "skntxtbxDetails0bbf1235271384a",
            "textInputMode": constants.TEXTBOX_INPUT_MODE_NUMERIC,
            "top": "15dp",
            "width": "100%",
            "zIndex": 1
        }, controller.args[0], "txtSSNValue"), extendConfig({
            "containerHeightMode": constants.TEXTBOX_FONT_METRICS_DRIVEN_HEIGHT,
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [3, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "txtSSNValue"), extendConfig({
            "autoCorrect": false
        }, controller.args[2], "txtSSNValue"));
        var SSNErrorMsg = new com.adminConsole.loans.applications.errorMsg(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
            "id": "SSNErrorMsg",
            "isVisible": false,
            "layoutType": kony.flex.FLOW_HORIZONTAL,
            "left": "0dp",
            "masterType": constants.MASTER_TYPE_DEFAULT,
            "isModalContainer": false,
            "skin": "slFbox",
            "top": "10dp",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "overrides": {
                "errorMsg": {
                    "isVisible": false
                }
            }
        }, controller.args[0], "SSNErrorMsg"), extendConfig({
            "overrides": {}
        }, controller.args[1], "SSNErrorMsg"), extendConfig({
            "overrides": {}
        }, controller.args[2], "SSNErrorMsg"));
        flxSSN.add(flxSSNLabel, txtSSNValue, SSNErrorMsg);
        var flxDLNumber = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
            "clipBounds": true,
            "id": "flxDLNumber",
            "isVisible": false,
            "layoutType": kony.flex.FLOW_VERTICAL,
            "left": "2%",
            "isModalContainer": false,
            "skin": "slFbox",
            "top": "0dp",
            "width": "32%",
            "zIndex": 1
        }, controller.args[0], "flxDLNumber"), extendConfig({}, controller.args[1], "flxDLNumber"), extendConfig({}, controller.args[2], "flxDLNumber"));
        flxDLNumber.setDefaultUnit(kony.flex.DP);
        var flxDLNumberLabel = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
            "clipBounds": true,
            "id": "flxDLNumberLabel",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "left": "0dp",
            "isModalContainer": false,
            "skin": "slFbox",
            "top": "0dp",
            "width": "100%"
        }, controller.args[0], "flxDLNumberLabel"), extendConfig({}, controller.args[1], "flxDLNumberLabel"), extendConfig({}, controller.args[2], "flxDLNumberLabel"));
        flxDLNumberLabel.setDefaultUnit(kony.flex.DP);
        var lblDLNumber = new kony.ui.Label(extendConfig({
            "id": "lblDLNumber",
            "isVisible": true,
            "left": "0px",
            "skin": "sknLblLatoReg78818912px",
            "i18n_text": "kony.i18n.getLocalizedString(\"i18n.Applications.DriverLicenseOptional\")",
            "top": "0px",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, controller.args[0], "lblDLNumber"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblDLNumber"), extendConfig({}, controller.args[2], "lblDLNumber"));
        var lblDLSize = new kony.ui.Label(extendConfig({
            "id": "lblDLSize",
            "isVisible": false,
            "right": "5dp",
            "skin": "sknLblLatoReg78818912px",
            "text": "0/70",
            "top": "0dp",
            "width": "50dp",
            "zIndex": 1
        }, controller.args[0], "lblDLSize"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_TOP_RIGHT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblDLSize"), extendConfig({}, controller.args[2], "lblDLSize"));
        flxDLNumberLabel.add(lblDLNumber, lblDLSize);
        var txtDLNumberValue = new kony.ui.TextBox2(extendConfig({
            "autoCapitalize": constants.TEXTBOX_AUTO_CAPITALIZE_NONE,
            "focusSkin": "skntxtbxDetailsFocus14px",
            "height": "40dp",
            "id": "txtDLNumberValue",
            "isVisible": true,
            "keyBoardStyle": constants.TEXTBOX_KEY_BOARD_STYLE_DEFAULT,
            "left": "0px",
            "i18n_placeholder": "kony.i18n.getLocalizedString(\"i18n.Applications.DriverLicenseNumberPlaceholder\")",
            "right": "0px",
            "secureTextEntry": false,
            "skin": "skntxtbxDetails0bbf1235271384a",
            "textInputMode": constants.TEXTBOX_INPUT_MODE_ANY,
            "top": "15dp",
            "width": "100%",
            "zIndex": 1
        }, controller.args[0], "txtDLNumberValue"), extendConfig({
            "containerHeightMode": constants.TEXTBOX_FONT_METRICS_DRIVEN_HEIGHT,
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [3, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "txtDLNumberValue"), extendConfig({
            "autoCorrect": false
        }, controller.args[2], "txtDLNumberValue"));
        var DLNumberErrorMsg = new com.adminConsole.loans.applications.errorMsg(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
            "id": "DLNumberErrorMsg",
            "isVisible": false,
            "layoutType": kony.flex.FLOW_HORIZONTAL,
            "left": "0dp",
            "masterType": constants.MASTER_TYPE_DEFAULT,
            "isModalContainer": false,
            "skin": "slFbox",
            "top": "10dp",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "overrides": {
                "errorMsg": {
                    "isVisible": false
                }
            }
        }, controller.args[0], "DLNumberErrorMsg"), extendConfig({
            "overrides": {}
        }, controller.args[1], "DLNumberErrorMsg"), extendConfig({
            "overrides": {}
        }, controller.args[2], "DLNumberErrorMsg"));
        flxDLNumber.add(flxDLNumberLabel, txtDLNumberValue, DLNumberErrorMsg);
        var flxState = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
            "bottom": "0px",
            "clipBounds": true,
            "id": "flxState",
            "isVisible": false,
            "layoutType": kony.flex.FLOW_VERTICAL,
            "left": "2%",
            "isModalContainer": false,
            "skin": "slFbox",
            "top": "0dp",
            "width": "32%",
            "zIndex": 1
        }, controller.args[0], "flxState"), extendConfig({}, controller.args[1], "flxState"), extendConfig({}, controller.args[2], "flxState"));
        flxState.setDefaultUnit(kony.flex.DP);
        var lblState = new kony.ui.Label(extendConfig({
            "id": "lblState",
            "isVisible": true,
            "left": "0px",
            "skin": "sknLblLatoReg78818912px",
            "i18n_text": "kony.i18n.getLocalizedString(\"i18n.Applications.StateOptional\")",
            "top": "0px",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, controller.args[0], "lblState"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblState"), extendConfig({}, controller.args[2], "lblState"));
        var lstStateValue = new kony.ui.ListBox(extendConfig({
            "height": "40px",
            "id": "lstStateValue",
            "isVisible": true,
            "left": "0px",
            "skin": "sknlbxBgffffffBorderc1c9ceRadius3Px",
            "top": "15dp",
            "width": "100%",
            "zIndex": 1
        }, controller.args[0], "lstStateValue"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [3, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lstStateValue"), extendConfig({
            "hoverSkin": "sknlstbxcursor",
            "multiSelect": false
        }, controller.args[2], "lstStateValue"));
        flxState.add(lblState, lstStateValue);
        flxRow3.add(flxSSN, flxDLNumber, flxState);
        flxSection.add(flxRow1, flxRow2, flxRow3);
        editPersonalInfo.add(loansSectionHeader, flxSection);
        return editPersonalInfo;
    }
})