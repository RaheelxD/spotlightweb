define(function() {
    return function(controller) {
        var editLoaninformation = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
            "clipBounds": true,
            "isMaster": true,
            "id": "editLoaninformation",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "left": "0dp",
            "isModalContainer": false,
            "skin": "slFbox",
            "top": "10dp",
            "width": "100%"
        }, controller.args[0], "editLoaninformation"), extendConfig({}, controller.args[1], "editLoaninformation"), extendConfig({}, controller.args[2], "editLoaninformation"));
        editLoaninformation.setDefaultUnit(kony.flex.DP);
        var loansSectionHeader = new com.adminConsole.loans.applications.loansSectionHeader(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
            "id": "loansSectionHeader",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "left": "0dp",
            "masterType": constants.MASTER_TYPE_DEFAULT,
            "isModalContainer": false,
            "skin": "slFbox",
            "top": "0dp",
            "width": "100%",
            "overrides": {
                "btnEdit": {
                    "isVisible": false
                },
                "lblSectionHeader": {
                    "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmGuestDashboard.LoanInfo\")"
                }
            }
        }, controller.args[0], "loansSectionHeader"), extendConfig({
            "overrides": {}
        }, controller.args[1], "loansSectionHeader"), extendConfig({
            "overrides": {}
        }, controller.args[2], "loansSectionHeader"));
        var flxMainDetails = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
            "clipBounds": true,
            "id": "flxMainDetails",
            "isVisible": true,
            "layoutType": kony.flex.FLOW_VERTICAL,
            "left": "20dp",
            "isModalContainer": false,
            "right": "20dp",
            "skin": "slFbox",
            "top": "42px",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, controller.args[0], "flxMainDetails"), extendConfig({}, controller.args[1], "flxMainDetails"), extendConfig({}, controller.args[2], "flxMainDetails"));
        flxMainDetails.setDefaultUnit(kony.flex.DP);
        var flxPersonalLoanInfo = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
            "clipBounds": true,
            "id": "flxPersonalLoanInfo",
            "isVisible": true,
            "layoutType": kony.flex.FLOW_VERTICAL,
            "left": "0dp",
            "isModalContainer": false,
            "skin": "slFbox",
            "top": "30dp",
            "width": "100%"
        }, controller.args[0], "flxPersonalLoanInfo"), extendConfig({}, controller.args[1], "flxPersonalLoanInfo"), extendConfig({}, controller.args[2], "flxPersonalLoanInfo"));
        flxPersonalLoanInfo.setDefaultUnit(kony.flex.DP);
        var flxAmount = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
            "clipBounds": true,
            "id": "flxAmount",
            "isVisible": true,
            "layoutType": kony.flex.FLOW_HORIZONTAL,
            "left": "0px",
            "isModalContainer": false,
            "skin": "slFbox",
            "top": "0px",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, controller.args[0], "flxAmount"), extendConfig({}, controller.args[1], "flxAmount"), extendConfig({}, controller.args[2], "flxAmount"));
        flxAmount.setDefaultUnit(kony.flex.DP);
        var flxAmountRight = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
            "clipBounds": true,
            "id": "flxAmountRight",
            "isVisible": true,
            "layoutType": kony.flex.FLOW_VERTICAL,
            "left": "0dp",
            "isModalContainer": false,
            "skin": "slFbox",
            "top": "0dp",
            "width": "32%"
        }, controller.args[0], "flxAmountRight"), extendConfig({}, controller.args[1], "flxAmountRight"), extendConfig({}, controller.args[2], "flxAmountRight"));
        flxAmountRight.setDefaultUnit(kony.flex.DP);
        var lblAmountRequested = new kony.ui.Label(extendConfig({
            "id": "lblAmountRequested",
            "isVisible": true,
            "left": "0dp",
            "skin": "sknLblLatoReg78818912px",
            "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmLoansDashboard.LoanAmount\")",
            "top": "0dp",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, controller.args[0], "lblAmountRequested"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblAmountRequested"), extendConfig({}, controller.args[2], "lblAmountRequested"));
        var flxLoanAmount = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "clipBounds": true,
            "height": "40dp",
            "id": "flxLoanAmount",
            "isVisible": true,
            "layoutType": kony.flex.FLOW_HORIZONTAL,
            "left": "0dp",
            "isModalContainer": false,
            "skin": "slFbox",
            "top": "12px",
            "width": "100%",
            "zIndex": 1
        }, controller.args[0], "flxLoanAmount"), extendConfig({}, controller.args[1], "flxLoanAmount"), extendConfig({}, controller.args[2], "flxLoanAmount"));
        flxLoanAmount.setDefaultUnit(kony.flex.DP);
        var lblCurrency = new kony.ui.Label(extendConfig({
            "height": "100%",
            "id": "lblCurrency",
            "isVisible": true,
            "left": "0",
            "skin": "sknLble1e5ed100PerBg12PxB2BDCB1R",
            "i18n_text": "kony.i18n.getLocalizedString(\"i18n.Applications.DollarSymbol\")",
            "top": "0",
            "width": "20%",
            "zIndex": 10
        }, controller.args[0], "lblCurrency"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_CENTER,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblCurrency"), extendConfig({}, controller.args[2], "lblCurrency"));
        var txtLoanAmountValue = new kony.ui.TextBox2(extendConfig({
            "autoCapitalize": constants.TEXTBOX_AUTO_CAPITALIZE_NONE,
            "focusSkin": "sknTbxFFFFFFBorDEDEDE13pxKA",
            "id": "txtLoanAmountValue",
            "isVisible": true,
            "keyBoardStyle": constants.TEXTBOX_KEY_BOARD_STYLE_PHONE_PAD,
            "left": "0dp",
            "i18n_placeholder": "kony.i18n.getLocalizedString(\"i18n.frmLogs.blSearchParam3\")",
            "secureTextEntry": false,
            "skin": "skntxtbxDetails0bbf1235271384a",
            "textInputMode": constants.TEXTBOX_INPUT_MODE_NUMERIC,
            "top": "0dp",
            "width": "79%",
            "zIndex": 1
        }, controller.args[0], "txtLoanAmountValue"), extendConfig({
            "containerHeightMode": constants.TEXTBOX_FONT_METRICS_DRIVEN_HEIGHT,
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [4, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "txtLoanAmountValue"), extendConfig({
            "autoCorrect": false,
            "placeholderSkin": "defTextBoxPlaceholder"
        }, controller.args[2], "txtLoanAmountValue"));
        flxLoanAmount.add(lblCurrency, txtLoanAmountValue);
        flxAmountRight.add(lblAmountRequested, flxLoanAmount);
        var lblLoanAmountRange = new kony.ui.Label(extendConfig({
            "id": "lblLoanAmountRange",
            "isVisible": true,
            "left": "2%",
            "skin": "sknLblLatoReg78818A13px",
            "text": "(Range : $1K ~ $100K )",
            "top": "40dp",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, controller.args[0], "lblLoanAmountRange"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblLoanAmountRange"), extendConfig({}, controller.args[2], "lblLoanAmountRange"));
        flxAmount.add(flxAmountRight, lblLoanAmountRange);
        var loanAmountErrorMsg = new com.adminConsole.loans.applications.errorMsg(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
            "bottom": 0,
            "id": "loanAmountErrorMsg",
            "isVisible": false,
            "layoutType": kony.flex.FLOW_HORIZONTAL,
            "left": "0dp",
            "masterType": constants.MASTER_TYPE_DEFAULT,
            "isModalContainer": false,
            "skin": "slFbox",
            "top": "10dp",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "overrides": {
                "errorMsg": {
                    "bottom": 0,
                    "isVisible": false,
                    "top": "10dp"
                },
                "lblErrorText": {
                    "text": "Error Message"
                }
            }
        }, controller.args[0], "loanAmountErrorMsg"), extendConfig({
            "overrides": {}
        }, controller.args[1], "loanAmountErrorMsg"), extendConfig({
            "overrides": {}
        }, controller.args[2], "loanAmountErrorMsg"));
        var flxLoanTerm = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
            "clipBounds": true,
            "id": "flxLoanTerm",
            "isVisible": true,
            "layoutType": kony.flex.FLOW_HORIZONTAL,
            "left": "0px",
            "isModalContainer": false,
            "skin": "slFbox",
            "top": "30dp",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, controller.args[0], "flxLoanTerm"), extendConfig({}, controller.args[1], "flxLoanTerm"), extendConfig({}, controller.args[2], "flxLoanTerm"));
        flxLoanTerm.setDefaultUnit(kony.flex.DP);
        var flxLoanTermRight = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
            "clipBounds": true,
            "id": "flxLoanTermRight",
            "isVisible": true,
            "layoutType": kony.flex.FLOW_VERTICAL,
            "left": "0dp",
            "isModalContainer": false,
            "skin": "slFbox",
            "top": "0dp",
            "width": "32%"
        }, controller.args[0], "flxLoanTermRight"), extendConfig({}, controller.args[1], "flxLoanTermRight"), extendConfig({}, controller.args[2], "flxLoanTermRight"));
        flxLoanTermRight.setDefaultUnit(kony.flex.DP);
        var lblLoanTerm = new kony.ui.Label(extendConfig({
            "id": "lblLoanTerm",
            "isVisible": true,
            "left": "0dp",
            "skin": "sknLblLatoReg78818912px",
            "i18n_text": "kony.i18n.getLocalizedString(\"i18n.Applications.LoanTerm\")",
            "top": "0dp",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, controller.args[0], "lblLoanTerm"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblLoanTerm"), extendConfig({}, controller.args[2], "lblLoanTerm"));
        var flxLoanTermValue = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "clipBounds": true,
            "height": "40dp",
            "id": "flxLoanTermValue",
            "isVisible": true,
            "layoutType": kony.flex.FLOW_HORIZONTAL,
            "left": "0dp",
            "isModalContainer": false,
            "skin": "slFbox",
            "top": "12px",
            "width": "100%",
            "zIndex": 1
        }, controller.args[0], "flxLoanTermValue"), extendConfig({}, controller.args[1], "flxLoanTermValue"), extendConfig({}, controller.args[2], "flxLoanTermValue"));
        flxLoanTermValue.setDefaultUnit(kony.flex.DP);
        var lblLoanTermAmountText = new kony.ui.Label(extendConfig({
            "height": "100%",
            "id": "lblLoanTermAmountText",
            "isVisible": true,
            "left": "0",
            "skin": "sknLble1e5ed100PerBg12PxB2BDCB1R",
            "i18n_text": "kony.i18n.getLocalizedString(\"i18n.Applications.MonthsPlaceHolder\")",
            "top": "0",
            "width": "25%",
            "zIndex": 10
        }, controller.args[0], "lblLoanTermAmountText"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_CENTER,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblLoanTermAmountText"), extendConfig({}, controller.args[2], "lblLoanTermAmountText"));
        var txtLoanTermAmount = new kony.ui.TextBox2(extendConfig({
            "autoCapitalize": constants.TEXTBOX_AUTO_CAPITALIZE_NONE,
            "focusSkin": "sknTbxFFFFFFBorDEDEDE13pxKA",
            "id": "txtLoanTermAmount",
            "isVisible": true,
            "keyBoardStyle": constants.TEXTBOX_KEY_BOARD_STYLE_PHONE_PAD,
            "left": "0",
            "i18n_placeholder": "kony.i18n.getLocalizedString(\"i18n.frmLogs.blSearchParam3\")",
            "secureTextEntry": false,
            "skin": "skntxtbxDetails0bbf1235271384a",
            "textInputMode": constants.TEXTBOX_INPUT_MODE_NUMERIC,
            "top": "0",
            "width": "74%",
            "zIndex": 1
        }, controller.args[0], "txtLoanTermAmount"), extendConfig({
            "containerHeightMode": constants.TEXTBOX_FONT_METRICS_DRIVEN_HEIGHT,
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [4, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "txtLoanTermAmount"), extendConfig({
            "autoCorrect": false,
            "placeholderSkin": "defTextBoxPlaceholder"
        }, controller.args[2], "txtLoanTermAmount"));
        flxLoanTermValue.add(lblLoanTermAmountText, txtLoanTermAmount);
        flxLoanTermRight.add(lblLoanTerm, flxLoanTermValue);
        var lblLoanTermRange = new kony.ui.Label(extendConfig({
            "id": "lblLoanTermRange",
            "isVisible": true,
            "left": "2%",
            "skin": "sknLblLatoReg78818A13px",
            "text": "(Range : $1K ~ $100K )",
            "top": "40dp",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, controller.args[0], "lblLoanTermRange"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblLoanTermRange"), extendConfig({}, controller.args[2], "lblLoanTermRange"));
        flxLoanTerm.add(flxLoanTermRight, lblLoanTermRange);
        var loanTermErrorMsg = new com.adminConsole.loans.applications.errorMsg(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
            "bottom": "0dp",
            "id": "loanTermErrorMsg",
            "isVisible": false,
            "layoutType": kony.flex.FLOW_HORIZONTAL,
            "left": "0dp",
            "masterType": constants.MASTER_TYPE_DEFAULT,
            "isModalContainer": false,
            "skin": "slFbox",
            "top": "10dp",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "overrides": {
                "errorMsg": {
                    "bottom": "0dp",
                    "isVisible": false,
                    "top": "10dp"
                }
            }
        }, controller.args[0], "loanTermErrorMsg"), extendConfig({
            "overrides": {}
        }, controller.args[1], "loanTermErrorMsg"), extendConfig({
            "overrides": {}
        }, controller.args[2], "loanTermErrorMsg"));
        var flxPurposeCollateral = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
            "clipBounds": true,
            "id": "flxPurposeCollateral",
            "isVisible": true,
            "layoutType": kony.flex.FLOW_VERTICAL,
            "left": "0px",
            "isModalContainer": false,
            "skin": "slFbox",
            "top": "30px",
            "width": "32%",
            "zIndex": 1
        }, controller.args[0], "flxPurposeCollateral"), extendConfig({}, controller.args[1], "flxPurposeCollateral"), extendConfig({}, controller.args[2], "flxPurposeCollateral"));
        flxPurposeCollateral.setDefaultUnit(kony.flex.DP);
        var lblPurposeCollateral = new kony.ui.Label(extendConfig({
            "id": "lblPurposeCollateral",
            "isVisible": true,
            "left": "0dp",
            "skin": "sknLblLatoReg78818912px",
            "i18n_text": "kony.i18n.getLocalizedString(\"i18n.Applications.Purpose\")",
            "top": "0dp",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, controller.args[0], "lblPurposeCollateral"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblPurposeCollateral"), extendConfig({}, controller.args[2], "lblPurposeCollateral"));
        var lstLoanPurposeValue = new kony.ui.ListBox(extendConfig({
            "height": "40dp",
            "id": "lstLoanPurposeValue",
            "isVisible": true,
            "left": "0dp",
            "skin": "sknlbxBgffffffBorderc1c9ceRadius3Px",
            "top": "12dp",
            "width": "100%",
            "zIndex": 1
        }, controller.args[0], "lstLoanPurposeValue"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [1, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lstLoanPurposeValue"), extendConfig({
            "multiSelect": false
        }, controller.args[2], "lstLoanPurposeValue"));
        var purposeErrorMsg = new com.adminConsole.loans.applications.errorMsg(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
            "id": "purposeErrorMsg",
            "isVisible": false,
            "layoutType": kony.flex.FLOW_HORIZONTAL,
            "left": "0dp",
            "masterType": constants.MASTER_TYPE_DEFAULT,
            "isModalContainer": false,
            "skin": "slFbox",
            "top": "10dp",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "overrides": {
                "errorMsg": {
                    "isVisible": false,
                    "top": "10dp"
                }
            }
        }, controller.args[0], "purposeErrorMsg"), extendConfig({
            "overrides": {}
        }, controller.args[1], "purposeErrorMsg"), extendConfig({
            "overrides": {}
        }, controller.args[2], "purposeErrorMsg"));
        flxPurposeCollateral.add(lblPurposeCollateral, lstLoanPurposeValue, purposeErrorMsg);
        var flxApplicationType = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
            "clipBounds": true,
            "id": "flxApplicationType",
            "isVisible": true,
            "layoutType": kony.flex.FLOW_VERTICAL,
            "left": "0px",
            "isModalContainer": false,
            "skin": "slFbox",
            "top": "30dp",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, controller.args[0], "flxApplicationType"), extendConfig({}, controller.args[1], "flxApplicationType"), extendConfig({}, controller.args[2], "flxApplicationType"));
        flxApplicationType.setDefaultUnit(kony.flex.DP);
        var lblApplicationType = new kony.ui.Label(extendConfig({
            "id": "lblApplicationType",
            "isVisible": true,
            "left": "0dp",
            "skin": "sknLblLatoReg78818912px",
            "i18n_text": "kony.i18n.getLocalizedString(\"i18n.Applications.ApplicationType\")",
            "top": "0dp",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, controller.args[0], "lblApplicationType"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblApplicationType"), extendConfig({}, controller.args[2], "lblApplicationType"));
        var lblApplicationTypeValue = new kony.ui.Label(extendConfig({
            "id": "lblApplicationTypeValue",
            "isVisible": true,
            "left": "0dp",
            "skin": "sknLblLatoReg78818A13px",
            "i18n_text": "kony.i18n.getLocalizedString(\"i18n.Applications.AmountRequested\")",
            "top": "15dp",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, controller.args[0], "lblApplicationTypeValue"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblApplicationTypeValue"), extendConfig({}, controller.args[2], "lblApplicationTypeValue"));
        var flxApplicationTypeSelection = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "clipBounds": true,
            "height": "40dp",
            "id": "flxApplicationTypeSelection",
            "isVisible": false,
            "layoutType": kony.flex.FLOW_HORIZONTAL,
            "left": "0dp",
            "isModalContainer": false,
            "skin": "slFbox",
            "top": "15px",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, controller.args[0], "flxApplicationTypeSelection"), extendConfig({}, controller.args[1], "flxApplicationTypeSelection"), extendConfig({}, controller.args[2], "flxApplicationTypeSelection"));
        flxApplicationTypeSelection.setDefaultUnit(kony.flex.DP);
        var flxIndividual = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "clipBounds": true,
            "height": "40dp",
            "id": "flxIndividual",
            "isVisible": true,
            "layoutType": kony.flex.FLOW_HORIZONTAL,
            "left": "0dp",
            "isModalContainer": false,
            "skin": "sknflxCSRAssistBlueHover",
            "top": "0dp",
            "width": "10%",
            "zIndex": 1
        }, controller.args[0], "flxIndividual"), extendConfig({}, controller.args[1], "flxIndividual"), extendConfig({}, controller.args[2], "flxIndividual"));
        flxIndividual.setDefaultUnit(kony.flex.DP);
        var lblIndividual = new kony.ui.Label(extendConfig({
            "centerY": "50%",
            "height": "20px",
            "id": "lblIndividual",
            "isVisible": true,
            "left": "20dp",
            "skin": "lblIconGrey",
            "text": "",
            "width": "20px",
            "zIndex": 1
        }, controller.args[0], "lblIndividual"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblIndividual"), extendConfig({}, controller.args[2], "lblIndividual"));
        var lblIndividualText = new kony.ui.Label(extendConfig({
            "centerY": "50%",
            "id": "lblIndividualText",
            "isVisible": true,
            "left": "10dp",
            "skin": "sknIcon13pxWhite",
            "text": "Individual",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 200
        }, controller.args[0], "lblIndividualText"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblIndividualText"), extendConfig({}, controller.args[2], "lblIndividualText"));
        flxIndividual.add(lblIndividual, lblIndividualText);
        var flxIndividualWithCoApplicant = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "clipBounds": true,
            "height": "40dp",
            "id": "flxIndividualWithCoApplicant",
            "isVisible": true,
            "layoutType": kony.flex.FLOW_HORIZONTAL,
            "left": "15px",
            "isModalContainer": false,
            "skin": "sknBackgroundFFFFFF",
            "top": "0dp",
            "width": "18%",
            "zIndex": 1
        }, controller.args[0], "flxIndividualWithCoApplicant"), extendConfig({}, controller.args[1], "flxIndividualWithCoApplicant"), extendConfig({}, controller.args[2], "flxIndividualWithCoApplicant"));
        flxIndividualWithCoApplicant.setDefaultUnit(kony.flex.DP);
        var lblIndividualWithCoApplicant = new kony.ui.Label(extendConfig({
            "centerY": "50%",
            "height": "20px",
            "id": "lblIndividualWithCoApplicant",
            "isVisible": true,
            "left": "20dp",
            "skin": "lblIconGrey",
            "text": "",
            "width": "20px",
            "zIndex": 1
        }, controller.args[0], "lblIndividualWithCoApplicant"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblIndividualWithCoApplicant"), extendConfig({}, controller.args[2], "lblIndividualWithCoApplicant"));
        var lblIndividualWithCoApplicantText = new kony.ui.Label(extendConfig({
            "centerY": "50%",
            "id": "lblIndividualWithCoApplicantText",
            "isVisible": true,
            "left": "10dp",
            "skin": "sknLabel4F555D13px",
            "text": "Individual With Co-Applicant",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 200
        }, controller.args[0], "lblIndividualWithCoApplicantText"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblIndividualWithCoApplicantText"), extendConfig({}, controller.args[2], "lblIndividualWithCoApplicantText"));
        flxIndividualWithCoApplicant.add(lblIndividualWithCoApplicant, lblIndividualWithCoApplicantText);
        var flxJointWithCoApplicant = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "clipBounds": true,
            "height": "40dp",
            "id": "flxJointWithCoApplicant",
            "isVisible": true,
            "layoutType": kony.flex.FLOW_HORIZONTAL,
            "left": "15px",
            "isModalContainer": false,
            "skin": "sknBackgroundFFFFFF",
            "top": "0dp",
            "width": "18%",
            "zIndex": 1
        }, controller.args[0], "flxJointWithCoApplicant"), extendConfig({}, controller.args[1], "flxJointWithCoApplicant"), extendConfig({}, controller.args[2], "flxJointWithCoApplicant"));
        flxJointWithCoApplicant.setDefaultUnit(kony.flex.DP);
        var lblJointWithCoApplicant = new kony.ui.Label(extendConfig({
            "centerY": "50%",
            "height": "20px",
            "id": "lblJointWithCoApplicant",
            "isVisible": true,
            "left": "20dp",
            "skin": "lblIconGrey",
            "text": "",
            "width": "20px",
            "zIndex": 1
        }, controller.args[0], "lblJointWithCoApplicant"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblJointWithCoApplicant"), extendConfig({}, controller.args[2], "lblJointWithCoApplicant"));
        var lblJointWithCoApplicantText = new kony.ui.Label(extendConfig({
            "centerY": "50%",
            "id": "lblJointWithCoApplicantText",
            "isVisible": true,
            "left": "10dp",
            "skin": "sknLabel4F555D13px",
            "text": "Individual With Co-Applicant",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 200
        }, controller.args[0], "lblJointWithCoApplicantText"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblJointWithCoApplicantText"), extendConfig({}, controller.args[2], "lblJointWithCoApplicantText"));
        flxJointWithCoApplicant.add(lblJointWithCoApplicant, lblJointWithCoApplicantText);
        flxApplicationTypeSelection.add(flxIndividual, flxIndividualWithCoApplicant, flxJointWithCoApplicant);
        flxApplicationType.add(lblApplicationType, lblApplicationTypeValue, flxApplicationTypeSelection);
        flxPersonalLoanInfo.add(flxAmount, loanAmountErrorMsg, flxLoanTerm, loanTermErrorMsg, flxPurposeCollateral, flxApplicationType);
        var flxVehicleLoanInfo = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
            "clipBounds": true,
            "id": "flxVehicleLoanInfo",
            "isVisible": true,
            "layoutType": kony.flex.FLOW_VERTICAL,
            "left": "0dp",
            "isModalContainer": false,
            "skin": "slFbox",
            "top": "0dp",
            "width": "100%",
            "zIndex": 1
        }, controller.args[0], "flxVehicleLoanInfo"), extendConfig({}, controller.args[1], "flxVehicleLoanInfo"), extendConfig({}, controller.args[2], "flxVehicleLoanInfo"));
        flxVehicleLoanInfo.setDefaultUnit(kony.flex.DP);
        var flxPurposeCollateralVL = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
            "clipBounds": true,
            "id": "flxPurposeCollateralVL",
            "isVisible": true,
            "layoutType": kony.flex.FLOW_VERTICAL,
            "left": "0px",
            "isModalContainer": false,
            "skin": "slFbox",
            "top": "30px",
            "width": "32%",
            "zIndex": 1
        }, controller.args[0], "flxPurposeCollateralVL"), extendConfig({}, controller.args[1], "flxPurposeCollateralVL"), extendConfig({}, controller.args[2], "flxPurposeCollateralVL"));
        flxPurposeCollateralVL.setDefaultUnit(kony.flex.DP);
        var lblPurposeCollateralVL = new kony.ui.Label(extendConfig({
            "id": "lblPurposeCollateralVL",
            "isVisible": true,
            "left": "0dp",
            "skin": "sknLblLatoReg78818912px",
            "i18n_text": "kony.i18n.getLocalizedString(\"i18n.Applications.Purpose\")",
            "top": "0dp",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, controller.args[0], "lblPurposeCollateralVL"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblPurposeCollateralVL"), extendConfig({}, controller.args[2], "lblPurposeCollateralVL"));
        var lstLoanPurposeValueVL = new kony.ui.ListBox(extendConfig({
            "height": "40dp",
            "id": "lstLoanPurposeValueVL",
            "isVisible": true,
            "left": "0dp",
            "skin": "sknlbxBgffffffBorderc1c9ceRadius3Px",
            "top": "12dp",
            "width": "100%",
            "zIndex": 1
        }, controller.args[0], "lstLoanPurposeValueVL"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [1, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lstLoanPurposeValueVL"), extendConfig({
            "multiSelect": false
        }, controller.args[2], "lstLoanPurposeValueVL"));
        var purposeErrorMsgVL = new com.adminConsole.loans.applications.errorMsg(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
            "id": "purposeErrorMsgVL",
            "isVisible": false,
            "layoutType": kony.flex.FLOW_HORIZONTAL,
            "left": "0dp",
            "masterType": constants.MASTER_TYPE_DEFAULT,
            "isModalContainer": false,
            "skin": "slFbox",
            "top": "10dp",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "overrides": {
                "errorMsg": {
                    "isVisible": false,
                    "top": "10dp"
                }
            }
        }, controller.args[0], "purposeErrorMsgVL"), extendConfig({
            "overrides": {}
        }, controller.args[1], "purposeErrorMsgVL"), extendConfig({
            "overrides": {}
        }, controller.args[2], "purposeErrorMsgVL"));
        flxPurposeCollateralVL.add(lblPurposeCollateralVL, lstLoanPurposeValueVL, purposeErrorMsgVL);
        var flxVehicleIdentificationNumber = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
            "clipBounds": true,
            "id": "flxVehicleIdentificationNumber",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "left": "0px",
            "isModalContainer": false,
            "skin": "slFbox",
            "top": "30px",
            "width": "100%",
            "zIndex": 1
        }, controller.args[0], "flxVehicleIdentificationNumber"), extendConfig({}, controller.args[1], "flxVehicleIdentificationNumber"), extendConfig({}, controller.args[2], "flxVehicleIdentificationNumber"));
        flxVehicleIdentificationNumber.setDefaultUnit(kony.flex.DP);
        var lblVehicleIdentificationNumber = new kony.ui.Label(extendConfig({
            "id": "lblVehicleIdentificationNumber",
            "isVisible": true,
            "left": "0dp",
            "skin": "sknLblLatoReg78818912px",
            "text": "Do you have Vehicle Identification Number?",
            "top": "0dp",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, controller.args[0], "lblVehicleIdentificationNumber"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblVehicleIdentificationNumber"), extendConfig({}, controller.args[2], "lblVehicleIdentificationNumber"));
        var flxOptions = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
            "clipBounds": true,
            "id": "flxOptions",
            "isVisible": true,
            "layoutType": kony.flex.FLOW_HORIZONTAL,
            "left": "0dp",
            "isModalContainer": false,
            "skin": "slFbox",
            "top": "5dp",
            "width": "100%",
            "zIndex": 1
        }, controller.args[0], "flxOptions"), extendConfig({}, controller.args[1], "flxOptions"), extendConfig({}, controller.args[2], "flxOptions"));
        flxOptions.setDefaultUnit(kony.flex.DP);
        var flxRadioButton1 = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
            "clipBounds": true,
            "id": "flxRadioButton1",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "left": "0dp",
            "isModalContainer": false,
            "skin": "slFbox",
            "top": "15dp",
            "width": "110dp",
            "zIndex": 1
        }, controller.args[0], "flxRadioButton1"), extendConfig({}, controller.args[1], "flxRadioButton1"), extendConfig({}, controller.args[2], "flxRadioButton1"));
        flxRadioButton1.setDefaultUnit(kony.flex.DP);
        var imgRadioButton1 = new kony.ui.Image2(extendConfig({
            "height": "15dp",
            "id": "imgRadioButton1",
            "isVisible": true,
            "left": "0dp",
            "skin": "slImage",
            "src": "radionormal_1x.png",
            "top": "0dp",
            "width": "15dp",
            "zIndex": 1
        }, controller.args[0], "imgRadioButton1"), extendConfig({
            "imageScaleMode": constants.IMAGE_SCALE_MODE_MAINTAIN_ASPECT_RATIO,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "imgRadioButton1"), extendConfig({}, controller.args[2], "imgRadioButton1"));
        var lblRadioButtonValue1 = new kony.ui.Label(extendConfig({
            "id": "lblRadioButtonValue1",
            "isVisible": true,
            "left": "20dp",
            "right": "10dp",
            "skin": "sknLbl485C75LatoRegular13Px",
            "text": "Yes",
            "top": "0dp",
            "zIndex": 1
        }, controller.args[0], "lblRadioButtonValue1"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_TOP_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblRadioButtonValue1"), extendConfig({}, controller.args[2], "lblRadioButtonValue1"));
        flxRadioButton1.add(imgRadioButton1, lblRadioButtonValue1);
        var flxRadioButton2 = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
            "clipBounds": true,
            "id": "flxRadioButton2",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "left": "10dp",
            "isModalContainer": false,
            "skin": "slFbox",
            "top": "15dp",
            "width": "110dp",
            "zIndex": 1
        }, controller.args[0], "flxRadioButton2"), extendConfig({}, controller.args[1], "flxRadioButton2"), extendConfig({}, controller.args[2], "flxRadioButton2"));
        flxRadioButton2.setDefaultUnit(kony.flex.DP);
        var imgRadioButton2 = new kony.ui.Image2(extendConfig({
            "height": "15dp",
            "id": "imgRadioButton2",
            "isVisible": true,
            "left": "0dp",
            "skin": "slImage",
            "src": "radionormal_1x.png",
            "top": "0dp",
            "width": "15dp",
            "zIndex": 1
        }, controller.args[0], "imgRadioButton2"), extendConfig({
            "imageScaleMode": constants.IMAGE_SCALE_MODE_MAINTAIN_ASPECT_RATIO,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "imgRadioButton2"), extendConfig({}, controller.args[2], "imgRadioButton2"));
        var lblRadioButtonValue2 = new kony.ui.Label(extendConfig({
            "id": "lblRadioButtonValue2",
            "isVisible": true,
            "left": "20dp",
            "right": "10dp",
            "skin": "sknLbl485C75LatoRegular13Px",
            "text": "No",
            "top": "0dp",
            "zIndex": 1
        }, controller.args[0], "lblRadioButtonValue2"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_TOP_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblRadioButtonValue2"), extendConfig({}, controller.args[2], "lblRadioButtonValue2"));
        flxRadioButton2.add(imgRadioButton2, lblRadioButtonValue2);
        flxOptions.add(flxRadioButton1, flxRadioButton2);
        var flxNoVehicle = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
            "clipBounds": true,
            "id": "flxNoVehicle",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "isModalContainer": false,
            "right": "100dp",
            "skin": "slFbox",
            "top": "20dp",
            "width": "300px",
            "zIndex": 1
        }, controller.args[0], "flxNoVehicle"), extendConfig({}, controller.args[1], "flxNoVehicle"), extendConfig({}, controller.args[2], "flxNoVehicle"));
        flxNoVehicle.setDefaultUnit(kony.flex.DP);
        var imgNoVehicle = new kony.ui.Image2(extendConfig({
            "height": "15dp",
            "id": "imgNoVehicle",
            "isVisible": true,
            "left": "0dp",
            "skin": "slImage",
            "src": "radionormal_1x.png",
            "top": "0dp",
            "width": "15dp",
            "zIndex": 1
        }, controller.args[0], "imgNoVehicle"), extendConfig({
            "imageScaleMode": constants.IMAGE_SCALE_MODE_MAINTAIN_ASPECT_RATIO,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "imgNoVehicle"), extendConfig({}, controller.args[2], "imgNoVehicle"));
        var lblNoVehicle = new kony.ui.Label(extendConfig({
            "id": "lblNoVehicle",
            "isVisible": true,
            "left": "30dp",
            "skin": "sknLbl485C75LatoRegular13Px",
            "text": "Havent Found Vehicle Yet",
            "top": "0dp",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, controller.args[0], "lblNoVehicle"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblNoVehicle"), extendConfig({}, controller.args[2], "lblNoVehicle"));
        flxNoVehicle.add(imgNoVehicle, lblNoVehicle);
        flxVehicleIdentificationNumber.add(lblVehicleIdentificationNumber, flxOptions, flxNoVehicle);
        var flxRow1 = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
            "clipBounds": true,
            "id": "flxRow1",
            "isVisible": true,
            "layoutType": kony.flex.FLOW_HORIZONTAL,
            "left": "0dp",
            "isModalContainer": false,
            "skin": "slFbox",
            "top": "30dp",
            "width": "100%",
            "zIndex": 1
        }, controller.args[0], "flxRow1"), extendConfig({}, controller.args[1], "flxRow1"), extendConfig({}, controller.args[2], "flxRow1"));
        flxRow1.setDefaultUnit(kony.flex.DP);
        var flxVehicleType = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
            "clipBounds": true,
            "id": "flxVehicleType",
            "isVisible": true,
            "layoutType": kony.flex.FLOW_VERTICAL,
            "left": "0dp",
            "isModalContainer": false,
            "skin": "slFbox",
            "width": "32%",
            "zIndex": 1
        }, controller.args[0], "flxVehicleType"), extendConfig({}, controller.args[1], "flxVehicleType"), extendConfig({}, controller.args[2], "flxVehicleType"));
        flxVehicleType.setDefaultUnit(kony.flex.DP);
        var lblVehicleType = new kony.ui.Label(extendConfig({
            "id": "lblVehicleType",
            "isVisible": true,
            "left": "0dp",
            "skin": "sknLblLatoReg78818912px",
            "text": "VEHICLE  TYPE",
            "top": "0dp",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, controller.args[0], "lblVehicleType"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblVehicleType"), extendConfig({}, controller.args[2], "lblVehicleType"));
        var lstVehicleType = new kony.ui.ListBox(extendConfig({
            "id": "lstVehicleType",
            "isVisible": true,
            "left": "0dp",
            "masterData": [
                ["select", "Select"]
            ],
            "skin": "sknlbxBgffffffBorderc1c9ceRadius3Px",
            "top": "15dp",
            "width": "100%",
            "zIndex": 1
        }, controller.args[0], "lstVehicleType"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [1, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lstVehicleType"), extendConfig({
            "multiSelect": false
        }, controller.args[2], "lstVehicleType"));
        flxVehicleType.add(lblVehicleType, lstVehicleType);
        var flxYear = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
            "clipBounds": true,
            "id": "flxYear",
            "isVisible": true,
            "layoutType": kony.flex.FLOW_VERTICAL,
            "left": "2%",
            "isModalContainer": false,
            "skin": "slFbox",
            "top": "0dp",
            "width": "32%",
            "zIndex": 1
        }, controller.args[0], "flxYear"), extendConfig({}, controller.args[1], "flxYear"), extendConfig({}, controller.args[2], "flxYear"));
        flxYear.setDefaultUnit(kony.flex.DP);
        var lblYear = new kony.ui.Label(extendConfig({
            "id": "lblYear",
            "isVisible": true,
            "left": "0dp",
            "skin": "sknLblLatoReg78818912px",
            "text": "YEAR",
            "top": "0dp",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, controller.args[0], "lblYear"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblYear"), extendConfig({}, controller.args[2], "lblYear"));
        var lstYear = new kony.ui.ListBox(extendConfig({
            "id": "lstYear",
            "isVisible": true,
            "left": "0dp",
            "masterData": [
                ["select", "Select"]
            ],
            "skin": "sknlbxBgffffffBorderc1c9ceRadius3Px",
            "top": "15dp",
            "width": "100%",
            "zIndex": 1
        }, controller.args[0], "lstYear"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [1, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lstYear"), extendConfig({
            "multiSelect": false
        }, controller.args[2], "lstYear"));
        flxYear.add(lblYear, lstYear);
        var flxMake = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
            "clipBounds": true,
            "id": "flxMake",
            "isVisible": true,
            "layoutType": kony.flex.FLOW_VERTICAL,
            "left": "2%",
            "isModalContainer": false,
            "skin": "slFbox",
            "top": "0dp",
            "width": "32%",
            "zIndex": 1
        }, controller.args[0], "flxMake"), extendConfig({}, controller.args[1], "flxMake"), extendConfig({}, controller.args[2], "flxMake"));
        flxMake.setDefaultUnit(kony.flex.DP);
        var lblMake = new kony.ui.Label(extendConfig({
            "id": "lblMake",
            "isVisible": true,
            "left": "0dp",
            "skin": "sknLblLatoReg78818912px",
            "text": "MAKE",
            "top": "0dp",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, controller.args[0], "lblMake"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblMake"), extendConfig({}, controller.args[2], "lblMake"));
        var lstMake = new kony.ui.ListBox(extendConfig({
            "id": "lstMake",
            "isVisible": true,
            "left": "0dp",
            "masterData": [
                ["select", "Select"]
            ],
            "skin": "sknlbxBgffffffBorderc1c9ceRadius3Px",
            "top": "15dp",
            "width": "100%",
            "zIndex": 1
        }, controller.args[0], "lstMake"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [1, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lstMake"), extendConfig({
            "multiSelect": false
        }, controller.args[2], "lstMake"));
        flxMake.add(lblMake, lstMake);
        flxRow1.add(flxVehicleType, flxYear, flxMake);
        var flxRow2 = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
            "clipBounds": true,
            "id": "flxRow2",
            "isVisible": true,
            "layoutType": kony.flex.FLOW_HORIZONTAL,
            "left": "0dp",
            "isModalContainer": false,
            "skin": "slFbox",
            "top": "30dp",
            "width": "100%",
            "zIndex": 1
        }, controller.args[0], "flxRow2"), extendConfig({}, controller.args[1], "flxRow2"), extendConfig({}, controller.args[2], "flxRow2"));
        flxRow2.setDefaultUnit(kony.flex.DP);
        var flxModel = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
            "clipBounds": true,
            "id": "flxModel",
            "isVisible": true,
            "layoutType": kony.flex.FLOW_VERTICAL,
            "left": "0%",
            "isModalContainer": false,
            "skin": "slFbox",
            "top": "0dp",
            "width": "32%",
            "zIndex": 1
        }, controller.args[0], "flxModel"), extendConfig({}, controller.args[1], "flxModel"), extendConfig({}, controller.args[2], "flxModel"));
        flxModel.setDefaultUnit(kony.flex.DP);
        var lblModel = new kony.ui.Label(extendConfig({
            "id": "lblModel",
            "isVisible": true,
            "left": "0dp",
            "skin": "sknLblLatoReg78818912px",
            "text": "MODEL",
            "top": "0dp",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, controller.args[0], "lblModel"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblModel"), extendConfig({}, controller.args[2], "lblModel"));
        var lstModel = new kony.ui.ListBox(extendConfig({
            "id": "lstModel",
            "isVisible": true,
            "left": "0dp",
            "masterData": [
                ["select", "Select"]
            ],
            "skin": "sknlbxBgffffffBorderc1c9ceRadius3Px",
            "top": "15dp",
            "width": "100%",
            "zIndex": 1
        }, controller.args[0], "lstModel"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [1, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lstModel"), extendConfig({
            "multiSelect": false
        }, controller.args[2], "lstModel"));
        flxModel.add(lblModel, lstModel);
        var flxTrim = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
            "clipBounds": true,
            "id": "flxTrim",
            "isVisible": true,
            "layoutType": kony.flex.FLOW_VERTICAL,
            "left": "2%",
            "isModalContainer": false,
            "skin": "slFbox",
            "top": "0dp",
            "width": "32%",
            "zIndex": 1
        }, controller.args[0], "flxTrim"), extendConfig({}, controller.args[1], "flxTrim"), extendConfig({}, controller.args[2], "flxTrim"));
        flxTrim.setDefaultUnit(kony.flex.DP);
        var lblTrim = new kony.ui.Label(extendConfig({
            "id": "lblTrim",
            "isVisible": true,
            "left": "0dp",
            "skin": "sknLblLatoReg78818912px",
            "text": "TRIM   (OPTIONAL)",
            "top": "0dp",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, controller.args[0], "lblTrim"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblTrim"), extendConfig({}, controller.args[2], "lblTrim"));
        var txtTrim = new kony.ui.TextBox2(extendConfig({
            "autoCapitalize": constants.TEXTBOX_AUTO_CAPITALIZE_NONE,
            "focusSkin": "sknTbxFFFFFFBorDEDEDE13pxKA",
            "id": "txtTrim",
            "isVisible": true,
            "keyBoardStyle": constants.TEXTBOX_KEY_BOARD_STYLE_DEFAULT,
            "left": "0",
            "placeholder": "Trim",
            "secureTextEntry": false,
            "skin": "sknTbxFFFFFFBorDEDEDE13pxKA",
            "textInputMode": constants.TEXTBOX_INPUT_MODE_ANY,
            "top": "15px",
            "width": "100%",
            "zIndex": 1
        }, controller.args[0], "txtTrim"), extendConfig({
            "containerHeightMode": constants.TEXTBOX_FONT_METRICS_DRIVEN_HEIGHT,
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [4, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "txtTrim"), extendConfig({
            "autoCorrect": false,
            "placeholderSkin": "defTextBoxPlaceholder"
        }, controller.args[2], "txtTrim"));
        flxTrim.add(lblTrim, txtTrim);
        var flxPurchaseValue = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
            "clipBounds": true,
            "id": "flxPurchaseValue",
            "isVisible": true,
            "layoutType": kony.flex.FLOW_VERTICAL,
            "left": "2%",
            "isModalContainer": false,
            "skin": "slFbox",
            "top": "0px",
            "width": "32%",
            "zIndex": 1
        }, controller.args[0], "flxPurchaseValue"), extendConfig({}, controller.args[1], "flxPurchaseValue"), extendConfig({}, controller.args[2], "flxPurchaseValue"));
        flxPurchaseValue.setDefaultUnit(kony.flex.DP);
        var lblPurchaseValue = new kony.ui.Label(extendConfig({
            "id": "lblPurchaseValue",
            "isVisible": true,
            "left": "0dp",
            "skin": "sknLblLatoReg78818912px",
            "text": "PURCHASE VALUE",
            "top": "0dp",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, controller.args[0], "lblPurchaseValue"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblPurchaseValue"), extendConfig({}, controller.args[2], "lblPurchaseValue"));
        var flxPurchaseValueText = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "clipBounds": true,
            "height": "40dp",
            "id": "flxPurchaseValueText",
            "isVisible": true,
            "layoutType": kony.flex.FLOW_HORIZONTAL,
            "left": "0dp",
            "isModalContainer": false,
            "skin": "slFbox",
            "top": "15dp",
            "width": "100%",
            "zIndex": 1
        }, controller.args[0], "flxPurchaseValueText"), extendConfig({}, controller.args[1], "flxPurchaseValueText"), extendConfig({}, controller.args[2], "flxPurchaseValueText"));
        flxPurchaseValueText.setDefaultUnit(kony.flex.DP);
        var lblPurchaseValueCurrency = new kony.ui.Label(extendConfig({
            "height": "100%",
            "id": "lblPurchaseValueCurrency",
            "isVisible": true,
            "left": "0",
            "skin": "sknLble1e5ed100PerBg12PxB2BDCB1R",
            "i18n_text": "kony.i18n.getLocalizedString(\"i18n.Applications.DollarSymbol\")",
            "top": "0",
            "width": "20%",
            "zIndex": 10
        }, controller.args[0], "lblPurchaseValueCurrency"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_CENTER,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblPurchaseValueCurrency"), extendConfig({}, controller.args[2], "lblPurchaseValueCurrency"));
        var txtPurchaseValueAmount = new kony.ui.TextBox2(extendConfig({
            "autoCapitalize": constants.TEXTBOX_AUTO_CAPITALIZE_NONE,
            "focusSkin": "sknTbxFFFFFFBorDEDEDE13pxKA",
            "id": "txtPurchaseValueAmount",
            "isVisible": true,
            "keyBoardStyle": constants.TEXTBOX_KEY_BOARD_STYLE_PHONE_PAD,
            "left": "0",
            "i18n_placeholder": "kony.i18n.getLocalizedString(\"i18n.frmLogs.blSearchParam3\")",
            "secureTextEntry": false,
            "skin": "skntxtbxDetails0bbf1235271384a",
            "textInputMode": constants.TEXTBOX_INPUT_MODE_NUMERIC,
            "top": "0",
            "width": "79%",
            "zIndex": 1
        }, controller.args[0], "txtPurchaseValueAmount"), extendConfig({
            "containerHeightMode": constants.TEXTBOX_FONT_METRICS_DRIVEN_HEIGHT,
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [4, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "txtPurchaseValueAmount"), extendConfig({
            "autoCorrect": false,
            "placeholderSkin": "defTextBoxPlaceholder"
        }, controller.args[2], "txtPurchaseValueAmount"));
        flxPurchaseValueText.add(lblPurchaseValueCurrency, txtPurchaseValueAmount);
        var purchaseValueErrorMsg = new com.adminConsole.loans.applications.errorMsg(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
            "id": "purchaseValueErrorMsg",
            "isVisible": false,
            "layoutType": kony.flex.FLOW_HORIZONTAL,
            "left": "0dp",
            "masterType": constants.MASTER_TYPE_DEFAULT,
            "isModalContainer": false,
            "skin": "slFbox",
            "top": "10dp",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "overrides": {
                "errorMsg": {
                    "isVisible": false,
                    "top": "10dp"
                }
            }
        }, controller.args[0], "purchaseValueErrorMsg"), extendConfig({
            "overrides": {}
        }, controller.args[1], "purchaseValueErrorMsg"), extendConfig({
            "overrides": {}
        }, controller.args[2], "purchaseValueErrorMsg"));
        flxPurchaseValue.add(lblPurchaseValue, flxPurchaseValueText, purchaseValueErrorMsg);
        flxRow2.add(flxModel, flxTrim, flxPurchaseValue);
        var flxAmountVL = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
            "clipBounds": true,
            "id": "flxAmountVL",
            "isVisible": true,
            "layoutType": kony.flex.FLOW_HORIZONTAL,
            "left": "0px",
            "isModalContainer": false,
            "skin": "slFbox",
            "top": "30dp",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, controller.args[0], "flxAmountVL"), extendConfig({}, controller.args[1], "flxAmountVL"), extendConfig({}, controller.args[2], "flxAmountVL"));
        flxAmountVL.setDefaultUnit(kony.flex.DP);
        var flxAmountRightVL = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
            "clipBounds": true,
            "id": "flxAmountRightVL",
            "isVisible": true,
            "layoutType": kony.flex.FLOW_VERTICAL,
            "left": "0dp",
            "isModalContainer": false,
            "skin": "slFbox",
            "top": "0dp",
            "width": "32%"
        }, controller.args[0], "flxAmountRightVL"), extendConfig({}, controller.args[1], "flxAmountRightVL"), extendConfig({}, controller.args[2], "flxAmountRightVL"));
        flxAmountRightVL.setDefaultUnit(kony.flex.DP);
        var lblAmountRequestedVL = new kony.ui.Label(extendConfig({
            "id": "lblAmountRequestedVL",
            "isVisible": true,
            "left": "0dp",
            "skin": "sknLblLatoReg78818912px",
            "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmLoansDashboard.LoanAmount\")",
            "top": "0dp",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, controller.args[0], "lblAmountRequestedVL"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblAmountRequestedVL"), extendConfig({}, controller.args[2], "lblAmountRequestedVL"));
        var flxLoanAmountVL = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "clipBounds": true,
            "height": "40dp",
            "id": "flxLoanAmountVL",
            "isVisible": true,
            "layoutType": kony.flex.FLOW_HORIZONTAL,
            "left": "0dp",
            "isModalContainer": false,
            "skin": "slFbox",
            "top": "12px",
            "width": "100%",
            "zIndex": 1
        }, controller.args[0], "flxLoanAmountVL"), extendConfig({}, controller.args[1], "flxLoanAmountVL"), extendConfig({}, controller.args[2], "flxLoanAmountVL"));
        flxLoanAmountVL.setDefaultUnit(kony.flex.DP);
        var lblCurrencyVL = new kony.ui.Label(extendConfig({
            "height": "100%",
            "id": "lblCurrencyVL",
            "isVisible": true,
            "left": "0",
            "skin": "sknLble1e5ed100PerBg12PxB2BDCB1R",
            "i18n_text": "kony.i18n.getLocalizedString(\"i18n.Applications.DollarSymbol\")",
            "top": "0",
            "width": "20%",
            "zIndex": 10
        }, controller.args[0], "lblCurrencyVL"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_CENTER,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblCurrencyVL"), extendConfig({}, controller.args[2], "lblCurrencyVL"));
        var txtLoanAmountValueVL = new kony.ui.TextBox2(extendConfig({
            "autoCapitalize": constants.TEXTBOX_AUTO_CAPITALIZE_NONE,
            "focusSkin": "sknTbxFFFFFFBorDEDEDE13pxKA",
            "id": "txtLoanAmountValueVL",
            "isVisible": true,
            "keyBoardStyle": constants.TEXTBOX_KEY_BOARD_STYLE_PHONE_PAD,
            "left": "0dp",
            "i18n_placeholder": "kony.i18n.getLocalizedString(\"i18n.frmLogs.blSearchParam3\")",
            "secureTextEntry": false,
            "skin": "skntxtbxDetails0bbf1235271384a",
            "textInputMode": constants.TEXTBOX_INPUT_MODE_NUMERIC,
            "top": "0dp",
            "width": "79%",
            "zIndex": 1
        }, controller.args[0], "txtLoanAmountValueVL"), extendConfig({
            "containerHeightMode": constants.TEXTBOX_FONT_METRICS_DRIVEN_HEIGHT,
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [4, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "txtLoanAmountValueVL"), extendConfig({
            "autoCorrect": false,
            "placeholderSkin": "defTextBoxPlaceholder"
        }, controller.args[2], "txtLoanAmountValueVL"));
        flxLoanAmountVL.add(lblCurrencyVL, txtLoanAmountValueVL);
        var loanAmountErrorMsgVL = new com.adminConsole.loans.applications.errorMsg(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
            "bottom": 0,
            "id": "loanAmountErrorMsgVL",
            "isVisible": false,
            "layoutType": kony.flex.FLOW_HORIZONTAL,
            "left": "0dp",
            "masterType": constants.MASTER_TYPE_DEFAULT,
            "isModalContainer": false,
            "skin": "slFbox",
            "top": "10dp",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "overrides": {
                "errorMsg": {
                    "bottom": 0,
                    "isVisible": false,
                    "top": "10dp"
                },
                "lblErrorText": {
                    "text": "Error Message"
                }
            }
        }, controller.args[0], "loanAmountErrorMsgVL"), extendConfig({
            "overrides": {}
        }, controller.args[1], "loanAmountErrorMsgVL"), extendConfig({
            "overrides": {}
        }, controller.args[2], "loanAmountErrorMsgVL"));
        flxAmountRightVL.add(lblAmountRequestedVL, flxLoanAmountVL, loanAmountErrorMsgVL);
        var lblLoanAmountRangeVL = new kony.ui.Label(extendConfig({
            "id": "lblLoanAmountRangeVL",
            "isVisible": true,
            "left": "2%",
            "skin": "sknLblLatoReg78818A13px",
            "text": "(Range : $1K ~ $100K )",
            "top": "40dp",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, controller.args[0], "lblLoanAmountRangeVL"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblLoanAmountRangeVL"), extendConfig({}, controller.args[2], "lblLoanAmountRangeVL"));
        flxAmountVL.add(flxAmountRightVL, lblLoanAmountRangeVL);
        var flxLoanTermVL = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
            "clipBounds": true,
            "id": "flxLoanTermVL",
            "isVisible": true,
            "layoutType": kony.flex.FLOW_HORIZONTAL,
            "left": "0px",
            "isModalContainer": false,
            "skin": "slFbox",
            "top": "30dp",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, controller.args[0], "flxLoanTermVL"), extendConfig({}, controller.args[1], "flxLoanTermVL"), extendConfig({}, controller.args[2], "flxLoanTermVL"));
        flxLoanTermVL.setDefaultUnit(kony.flex.DP);
        var flxLoanTermRightVL = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
            "clipBounds": true,
            "id": "flxLoanTermRightVL",
            "isVisible": true,
            "layoutType": kony.flex.FLOW_VERTICAL,
            "left": "0dp",
            "isModalContainer": false,
            "skin": "slFbox",
            "top": "0dp",
            "width": "32%"
        }, controller.args[0], "flxLoanTermRightVL"), extendConfig({}, controller.args[1], "flxLoanTermRightVL"), extendConfig({}, controller.args[2], "flxLoanTermRightVL"));
        flxLoanTermRightVL.setDefaultUnit(kony.flex.DP);
        var lblLoanTermVL = new kony.ui.Label(extendConfig({
            "id": "lblLoanTermVL",
            "isVisible": true,
            "left": "0dp",
            "skin": "sknLblLatoReg78818912px",
            "i18n_text": "kony.i18n.getLocalizedString(\"i18n.Applications.LoanTerm\")",
            "top": "0dp",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, controller.args[0], "lblLoanTermVL"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblLoanTermVL"), extendConfig({}, controller.args[2], "lblLoanTermVL"));
        var flxLoanTermValueVL = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "clipBounds": true,
            "height": "40dp",
            "id": "flxLoanTermValueVL",
            "isVisible": true,
            "layoutType": kony.flex.FLOW_HORIZONTAL,
            "left": "0dp",
            "isModalContainer": false,
            "skin": "slFbox",
            "top": "12px",
            "width": "100%",
            "zIndex": 1
        }, controller.args[0], "flxLoanTermValueVL"), extendConfig({}, controller.args[1], "flxLoanTermValueVL"), extendConfig({}, controller.args[2], "flxLoanTermValueVL"));
        flxLoanTermValueVL.setDefaultUnit(kony.flex.DP);
        var lblLoanTermAmountTextVL = new kony.ui.Label(extendConfig({
            "height": "100%",
            "id": "lblLoanTermAmountTextVL",
            "isVisible": true,
            "left": "0",
            "skin": "sknLble1e5ed100PerBg12PxB2BDCB1R",
            "i18n_text": "kony.i18n.getLocalizedString(\"i18n.Applications.MonthsPlaceHolder\")",
            "top": "0",
            "width": "20%",
            "zIndex": 10
        }, controller.args[0], "lblLoanTermAmountTextVL"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_CENTER,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblLoanTermAmountTextVL"), extendConfig({}, controller.args[2], "lblLoanTermAmountTextVL"));
        var txtLoanTermAmountVL = new kony.ui.TextBox2(extendConfig({
            "autoCapitalize": constants.TEXTBOX_AUTO_CAPITALIZE_NONE,
            "focusSkin": "sknTbxFFFFFFBorDEDEDE13pxKA",
            "id": "txtLoanTermAmountVL",
            "isVisible": true,
            "keyBoardStyle": constants.TEXTBOX_KEY_BOARD_STYLE_PHONE_PAD,
            "left": "0",
            "i18n_placeholder": "kony.i18n.getLocalizedString(\"i18n.frmLogs.blSearchParam3\")",
            "secureTextEntry": false,
            "skin": "skntxtbxDetails0bbf1235271384a",
            "textInputMode": constants.TEXTBOX_INPUT_MODE_NUMERIC,
            "top": "0",
            "width": "79%",
            "zIndex": 1
        }, controller.args[0], "txtLoanTermAmountVL"), extendConfig({
            "containerHeightMode": constants.TEXTBOX_FONT_METRICS_DRIVEN_HEIGHT,
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [4, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "txtLoanTermAmountVL"), extendConfig({
            "autoCorrect": false,
            "placeholderSkin": "defTextBoxPlaceholder"
        }, controller.args[2], "txtLoanTermAmountVL"));
        flxLoanTermValueVL.add(lblLoanTermAmountTextVL, txtLoanTermAmountVL);
        var loanTermErrorMsgVL = new com.adminConsole.loans.applications.errorMsg(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
            "bottom": "0dp",
            "id": "loanTermErrorMsgVL",
            "isVisible": false,
            "layoutType": kony.flex.FLOW_HORIZONTAL,
            "left": "0dp",
            "masterType": constants.MASTER_TYPE_DEFAULT,
            "isModalContainer": false,
            "skin": "slFbox",
            "top": "10dp",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "overrides": {
                "errorMsg": {
                    "bottom": "0dp",
                    "isVisible": false,
                    "top": "10dp"
                }
            }
        }, controller.args[0], "loanTermErrorMsgVL"), extendConfig({
            "overrides": {}
        }, controller.args[1], "loanTermErrorMsgVL"), extendConfig({
            "overrides": {}
        }, controller.args[2], "loanTermErrorMsgVL"));
        flxLoanTermRightVL.add(lblLoanTermVL, flxLoanTermValueVL, loanTermErrorMsgVL);
        var lblLoanTermRangeVL = new kony.ui.Label(extendConfig({
            "id": "lblLoanTermRangeVL",
            "isVisible": true,
            "left": "2%",
            "skin": "sknLblLatoReg78818A13px",
            "text": "(Range : $1K ~ $100K )",
            "top": "40dp",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, controller.args[0], "lblLoanTermRangeVL"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblLoanTermRangeVL"), extendConfig({}, controller.args[2], "lblLoanTermRangeVL"));
        flxLoanTermVL.add(flxLoanTermRightVL, lblLoanTermRangeVL);
        var flxApplicationTypeVL = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
            "clipBounds": true,
            "id": "flxApplicationTypeVL",
            "isVisible": true,
            "layoutType": kony.flex.FLOW_VERTICAL,
            "left": "0px",
            "isModalContainer": false,
            "skin": "slFbox",
            "top": "30dp",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, controller.args[0], "flxApplicationTypeVL"), extendConfig({}, controller.args[1], "flxApplicationTypeVL"), extendConfig({}, controller.args[2], "flxApplicationTypeVL"));
        flxApplicationTypeVL.setDefaultUnit(kony.flex.DP);
        var lblApplicationTypeVL = new kony.ui.Label(extendConfig({
            "id": "lblApplicationTypeVL",
            "isVisible": true,
            "left": "0dp",
            "skin": "sknLblLatoReg78818912px",
            "i18n_text": "kony.i18n.getLocalizedString(\"i18n.Applications.ApplicationType\")",
            "top": "0dp",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, controller.args[0], "lblApplicationTypeVL"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblApplicationTypeVL"), extendConfig({}, controller.args[2], "lblApplicationTypeVL"));
        var lblApplicationTypeValueVL = new kony.ui.Label(extendConfig({
            "id": "lblApplicationTypeValueVL",
            "isVisible": true,
            "left": "0dp",
            "skin": "sknLblLatoReg78818A13px",
            "i18n_text": "kony.i18n.getLocalizedString(\"i18n.Applications.AmountRequested\")",
            "top": "15dp",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, controller.args[0], "lblApplicationTypeValueVL"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblApplicationTypeValueVL"), extendConfig({}, controller.args[2], "lblApplicationTypeValueVL"));
        var flxApplicationTypeSelectionVL = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "clipBounds": true,
            "height": "40dp",
            "id": "flxApplicationTypeSelectionVL",
            "isVisible": false,
            "layoutType": kony.flex.FLOW_HORIZONTAL,
            "left": "0dp",
            "isModalContainer": false,
            "skin": "slFbox",
            "top": "15px",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, controller.args[0], "flxApplicationTypeSelectionVL"), extendConfig({}, controller.args[1], "flxApplicationTypeSelectionVL"), extendConfig({}, controller.args[2], "flxApplicationTypeSelectionVL"));
        flxApplicationTypeSelectionVL.setDefaultUnit(kony.flex.DP);
        var flxIndividualVL = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "clipBounds": true,
            "height": "40dp",
            "id": "flxIndividualVL",
            "isVisible": true,
            "layoutType": kony.flex.FLOW_HORIZONTAL,
            "left": "0dp",
            "isModalContainer": false,
            "skin": "sknflxCSRAssistBlueHover",
            "top": "0dp",
            "width": "10%",
            "zIndex": 1
        }, controller.args[0], "flxIndividualVL"), extendConfig({}, controller.args[1], "flxIndividualVL"), extendConfig({}, controller.args[2], "flxIndividualVL"));
        flxIndividualVL.setDefaultUnit(kony.flex.DP);
        var lblIndividualVL = new kony.ui.Label(extendConfig({
            "centerY": "50%",
            "height": "20px",
            "id": "lblIndividualVL",
            "isVisible": true,
            "left": "20dp",
            "skin": "lblIconGrey",
            "text": "",
            "width": "20px",
            "zIndex": 1
        }, controller.args[0], "lblIndividualVL"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblIndividualVL"), extendConfig({}, controller.args[2], "lblIndividualVL"));
        var lblIndividualTextVL = new kony.ui.Label(extendConfig({
            "centerY": "50%",
            "id": "lblIndividualTextVL",
            "isVisible": true,
            "left": "10dp",
            "skin": "sknIcon13pxWhite",
            "text": "Individual",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 200
        }, controller.args[0], "lblIndividualTextVL"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblIndividualTextVL"), extendConfig({}, controller.args[2], "lblIndividualTextVL"));
        flxIndividualVL.add(lblIndividualVL, lblIndividualTextVL);
        var flxIndividualWithCoApplicantVL = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "clipBounds": true,
            "height": "40dp",
            "id": "flxIndividualWithCoApplicantVL",
            "isVisible": true,
            "layoutType": kony.flex.FLOW_HORIZONTAL,
            "left": "15px",
            "isModalContainer": false,
            "skin": "sknBackgroundFFFFFF",
            "top": "0dp",
            "width": "18%",
            "zIndex": 1
        }, controller.args[0], "flxIndividualWithCoApplicantVL"), extendConfig({}, controller.args[1], "flxIndividualWithCoApplicantVL"), extendConfig({}, controller.args[2], "flxIndividualWithCoApplicantVL"));
        flxIndividualWithCoApplicantVL.setDefaultUnit(kony.flex.DP);
        var lblIndividualWithCoApplicantVL = new kony.ui.Label(extendConfig({
            "centerY": "50%",
            "height": "20px",
            "id": "lblIndividualWithCoApplicantVL",
            "isVisible": true,
            "left": "20dp",
            "skin": "lblIconGrey",
            "text": "",
            "width": "20px",
            "zIndex": 1
        }, controller.args[0], "lblIndividualWithCoApplicantVL"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblIndividualWithCoApplicantVL"), extendConfig({}, controller.args[2], "lblIndividualWithCoApplicantVL"));
        var lblIndividualWithCoApplicantTextVL = new kony.ui.Label(extendConfig({
            "centerY": "50%",
            "id": "lblIndividualWithCoApplicantTextVL",
            "isVisible": true,
            "left": "10dp",
            "skin": "sknLabel4F555D13px",
            "text": "Individual With Co-Applicant",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 200
        }, controller.args[0], "lblIndividualWithCoApplicantTextVL"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblIndividualWithCoApplicantTextVL"), extendConfig({}, controller.args[2], "lblIndividualWithCoApplicantTextVL"));
        flxIndividualWithCoApplicantVL.add(lblIndividualWithCoApplicantVL, lblIndividualWithCoApplicantTextVL);
        var flxJointWithCoApplicantVL = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "clipBounds": true,
            "height": "40dp",
            "id": "flxJointWithCoApplicantVL",
            "isVisible": true,
            "layoutType": kony.flex.FLOW_HORIZONTAL,
            "left": "15px",
            "isModalContainer": false,
            "skin": "sknBackgroundFFFFFF",
            "top": "0dp",
            "width": "18%",
            "zIndex": 1
        }, controller.args[0], "flxJointWithCoApplicantVL"), extendConfig({}, controller.args[1], "flxJointWithCoApplicantVL"), extendConfig({}, controller.args[2], "flxJointWithCoApplicantVL"));
        flxJointWithCoApplicantVL.setDefaultUnit(kony.flex.DP);
        var lblJointWithCoApplicantVL = new kony.ui.Label(extendConfig({
            "centerY": "50%",
            "height": "20px",
            "id": "lblJointWithCoApplicantVL",
            "isVisible": true,
            "left": "20dp",
            "skin": "lblIconGrey",
            "text": "",
            "width": "20px",
            "zIndex": 1
        }, controller.args[0], "lblJointWithCoApplicantVL"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblJointWithCoApplicantVL"), extendConfig({}, controller.args[2], "lblJointWithCoApplicantVL"));
        var lblJointWithCoApplicantTextVL = new kony.ui.Label(extendConfig({
            "centerY": "50%",
            "id": "lblJointWithCoApplicantTextVL",
            "isVisible": true,
            "left": "10dp",
            "skin": "sknLabel4F555D13px",
            "text": "Individual With Co-Applicant",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 200
        }, controller.args[0], "lblJointWithCoApplicantTextVL"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblJointWithCoApplicantTextVL"), extendConfig({}, controller.args[2], "lblJointWithCoApplicantTextVL"));
        flxJointWithCoApplicantVL.add(lblJointWithCoApplicantVL, lblJointWithCoApplicantTextVL);
        flxApplicationTypeSelectionVL.add(flxIndividualVL, flxIndividualWithCoApplicantVL, flxJointWithCoApplicantVL);
        flxApplicationTypeVL.add(lblApplicationTypeVL, lblApplicationTypeValueVL, flxApplicationTypeSelectionVL);
        flxVehicleLoanInfo.add(flxPurposeCollateralVL, flxVehicleIdentificationNumber, flxRow1, flxRow2, flxAmountVL, flxLoanTermVL, flxApplicationTypeVL);
        var flxCreditCardInfo = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
            "clipBounds": true,
            "id": "flxCreditCardInfo",
            "isVisible": true,
            "layoutType": kony.flex.FLOW_VERTICAL,
            "left": "0dp",
            "isModalContainer": false,
            "skin": "slFbox",
            "top": "30dp",
            "width": "100%"
        }, controller.args[0], "flxCreditCardInfo"), extendConfig({}, controller.args[1], "flxCreditCardInfo"), extendConfig({}, controller.args[2], "flxCreditCardInfo"));
        flxCreditCardInfo.setDefaultUnit(kony.flex.DP);
        var flxCardType = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
            "clipBounds": true,
            "id": "flxCardType",
            "isVisible": true,
            "layoutType": kony.flex.FLOW_VERTICAL,
            "left": "0px",
            "isModalContainer": false,
            "skin": "slFbox",
            "top": "0px",
            "width": "32%",
            "zIndex": 1
        }, controller.args[0], "flxCardType"), extendConfig({}, controller.args[1], "flxCardType"), extendConfig({}, controller.args[2], "flxCardType"));
        flxCardType.setDefaultUnit(kony.flex.DP);
        var lblCardType = new kony.ui.Label(extendConfig({
            "id": "lblCardType",
            "isVisible": true,
            "left": "0dp",
            "skin": "sknLblLatoReg78818912px",
            "text": "CARD TYPE",
            "top": "0dp",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, controller.args[0], "lblCardType"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblCardType"), extendConfig({}, controller.args[2], "lblCardType"));
        var lblCardTypeValue = new kony.ui.Label(extendConfig({
            "id": "lblCardTypeValue",
            "isVisible": true,
            "left": "0dp",
            "skin": "sknLblLatoReg78818A13px",
            "text": "CARD TYPE",
            "top": "15px",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, controller.args[0], "lblCardTypeValue"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblCardTypeValue"), extendConfig({}, controller.args[2], "lblCardTypeValue"));
        flxCardType.add(lblCardType, lblCardTypeValue);
        var flxCreditLimit = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
            "clipBounds": true,
            "id": "flxCreditLimit",
            "isVisible": true,
            "layoutType": kony.flex.FLOW_HORIZONTAL,
            "left": "0px",
            "isModalContainer": false,
            "skin": "slFbox",
            "top": "30dp",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, controller.args[0], "flxCreditLimit"), extendConfig({}, controller.args[1], "flxCreditLimit"), extendConfig({}, controller.args[2], "flxCreditLimit"));
        flxCreditLimit.setDefaultUnit(kony.flex.DP);
        var flxCreditLimitRight = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
            "clipBounds": true,
            "id": "flxCreditLimitRight",
            "isVisible": true,
            "layoutType": kony.flex.FLOW_VERTICAL,
            "left": "0dp",
            "isModalContainer": false,
            "skin": "slFbox",
            "top": "0dp",
            "width": "32%"
        }, controller.args[0], "flxCreditLimitRight"), extendConfig({}, controller.args[1], "flxCreditLimitRight"), extendConfig({}, controller.args[2], "flxCreditLimitRight"));
        flxCreditLimitRight.setDefaultUnit(kony.flex.DP);
        var lblCreditLimit = new kony.ui.Label(extendConfig({
            "id": "lblCreditLimit",
            "isVisible": true,
            "left": "0dp",
            "skin": "sknLblLatoReg78818912px",
            "i18n_text": "kony.i18n.getLocalizedString(\"i18n.Applications.AmountRequested\")",
            "top": "0dp",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, controller.args[0], "lblCreditLimit"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblCreditLimit"), extendConfig({}, controller.args[2], "lblCreditLimit"));
        var flxCreditLimitValue = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "clipBounds": true,
            "height": "40dp",
            "id": "flxCreditLimitValue",
            "isVisible": true,
            "layoutType": kony.flex.FLOW_HORIZONTAL,
            "left": "0dp",
            "isModalContainer": false,
            "skin": "slFbox",
            "top": "12px",
            "width": "100%",
            "zIndex": 1
        }, controller.args[0], "flxCreditLimitValue"), extendConfig({}, controller.args[1], "flxCreditLimitValue"), extendConfig({}, controller.args[2], "flxCreditLimitValue"));
        flxCreditLimitValue.setDefaultUnit(kony.flex.DP);
        var lblCreditLimitCurrency = new kony.ui.Label(extendConfig({
            "height": "100%",
            "id": "lblCreditLimitCurrency",
            "isVisible": true,
            "left": "0",
            "skin": "sknLble1e5ed100PerBg12PxB2BDCB1R",
            "i18n_text": "kony.i18n.getLocalizedString(\"i18n.Applications.DollarSymbol\")",
            "top": "0",
            "width": "20%",
            "zIndex": 10
        }, controller.args[0], "lblCreditLimitCurrency"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_CENTER,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblCreditLimitCurrency"), extendConfig({}, controller.args[2], "lblCreditLimitCurrency"));
        var txtCreditLimitAmount = new kony.ui.TextBox2(extendConfig({
            "autoCapitalize": constants.TEXTBOX_AUTO_CAPITALIZE_NONE,
            "focusSkin": "sknTbxFFFFFFBorDEDEDE13pxKA",
            "id": "txtCreditLimitAmount",
            "isVisible": true,
            "keyBoardStyle": constants.TEXTBOX_KEY_BOARD_STYLE_PHONE_PAD,
            "left": "0",
            "i18n_placeholder": "kony.i18n.getLocalizedString(\"i18n.frmLogs.blSearchParam3\")",
            "secureTextEntry": false,
            "skin": "skntxtbxDetails0bbf1235271384a",
            "textInputMode": constants.TEXTBOX_INPUT_MODE_NUMERIC,
            "top": "0",
            "width": "79%",
            "zIndex": 1
        }, controller.args[0], "txtCreditLimitAmount"), extendConfig({
            "containerHeightMode": constants.TEXTBOX_FONT_METRICS_DRIVEN_HEIGHT,
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [4, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "txtCreditLimitAmount"), extendConfig({
            "autoCorrect": false,
            "placeholderSkin": "defTextBoxPlaceholder"
        }, controller.args[2], "txtCreditLimitAmount"));
        flxCreditLimitValue.add(lblCreditLimitCurrency, txtCreditLimitAmount);
        flxCreditLimitRight.add(lblCreditLimit, flxCreditLimitValue);
        var lblCreditLimitRange = new kony.ui.Label(extendConfig({
            "id": "lblCreditLimitRange",
            "isVisible": true,
            "left": "2%",
            "skin": "sknLblLatoReg78818A13px",
            "text": "(Range : $1K ~ $100K )",
            "top": "40px",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, controller.args[0], "lblCreditLimitRange"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblCreditLimitRange"), extendConfig({}, controller.args[2], "lblCreditLimitRange"));
        flxCreditLimit.add(flxCreditLimitRight, lblCreditLimitRange);
        var creditLimitErrorMsg = new com.adminConsole.loans.applications.errorMsg(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
            "id": "creditLimitErrorMsg",
            "isVisible": false,
            "layoutType": kony.flex.FLOW_HORIZONTAL,
            "left": "0dp",
            "masterType": constants.MASTER_TYPE_DEFAULT,
            "isModalContainer": false,
            "skin": "slFbox",
            "top": "10dp",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "overrides": {
                "errorMsg": {
                    "isVisible": false,
                    "top": "10dp"
                },
                "lblErrorText": {
                    "text": "Entered value is more than maximum allowed"
                }
            }
        }, controller.args[0], "creditLimitErrorMsg"), extendConfig({
            "overrides": {}
        }, controller.args[1], "creditLimitErrorMsg"), extendConfig({
            "overrides": {}
        }, controller.args[2], "creditLimitErrorMsg"));
        flxCreditCardInfo.add(flxCardType, flxCreditLimit, creditLimitErrorMsg);
        flxMainDetails.add(flxPersonalLoanInfo, flxVehicleLoanInfo, flxCreditCardInfo);
        editLoaninformation.add(loansSectionHeader, flxMainDetails);
        return editLoaninformation;
    }
})