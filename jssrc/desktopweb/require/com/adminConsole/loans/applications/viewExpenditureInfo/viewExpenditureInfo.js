define(function() {
    return function(controller) {
        var viewExpenditureInfo = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
            "clipBounds": true,
            "isMaster": true,
            "id": "viewExpenditureInfo",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "left": "0dp",
            "isModalContainer": false,
            "skin": "slFbox",
            "top": "30dp",
            "width": "100%"
        }, controller.args[0], "viewExpenditureInfo"), extendConfig({}, controller.args[1], "viewExpenditureInfo"), extendConfig({}, controller.args[2], "viewExpenditureInfo"));
        viewExpenditureInfo.setDefaultUnit(kony.flex.DP);
        var loansSectionHeader = new com.adminConsole.loans.applications.loansSectionHeader(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
            "id": "loansSectionHeader",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "left": "0dp",
            "masterType": constants.MASTER_TYPE_DEFAULT,
            "isModalContainer": false,
            "skin": "slFbox",
            "top": "0dp",
            "width": "100%",
            "overrides": {
                "lblSectionHeader": {
                    "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmTrackApplication.Expenditure\")"
                }
            }
        }, controller.args[0], "loansSectionHeader"), extendConfig({
            "overrides": {}
        }, controller.args[1], "loansSectionHeader"), extendConfig({
            "overrides": {}
        }, controller.args[2], "loansSectionHeader"));
        var flxSection = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
            "clipBounds": true,
            "id": "flxSection",
            "isVisible": true,
            "layoutType": kony.flex.FLOW_VERTICAL,
            "left": "20dp",
            "isModalContainer": false,
            "right": "20dp",
            "skin": "slFbox",
            "top": "42dp",
            "width": kony.flex.USE_PREFFERED_SIZE
        }, controller.args[0], "flxSection"), extendConfig({}, controller.args[1], "flxSection"), extendConfig({}, controller.args[2], "flxSection"));
        flxSection.setDefaultUnit(kony.flex.DP);
        var flxExpenditureInfo = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
            "clipBounds": true,
            "id": "flxExpenditureInfo",
            "isVisible": true,
            "layoutType": kony.flex.FLOW_HORIZONTAL,
            "left": 0,
            "isModalContainer": false,
            "right": 20,
            "skin": "slFbox",
            "top": "0dp",
            "zIndex": 1
        }, controller.args[0], "flxExpenditureInfo"), extendConfig({}, controller.args[1], "flxExpenditureInfo"), extendConfig({}, controller.args[2], "flxExpenditureInfo"));
        flxExpenditureInfo.setDefaultUnit(kony.flex.DP);
        var flxRent = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
            "clipBounds": true,
            "id": "flxRent",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "isModalContainer": false,
            "skin": "slFbox",
            "top": "30dp",
            "width": "32%",
            "zIndex": 1
        }, controller.args[0], "flxRent"), extendConfig({}, controller.args[1], "flxRent"), extendConfig({}, controller.args[2], "flxRent"));
        flxRent.setDefaultUnit(kony.flex.DP);
        var lblRent = new kony.ui.Label(extendConfig({
            "id": "lblRent",
            "isVisible": true,
            "left": "0px",
            "skin": "sknLbl78818A11px",
            "i18n_text": "kony.i18n.getLocalizedString(\"i18n.Applications.Summary.MonthlyHousingRentCAPS\")",
            "top": "0px",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, controller.args[0], "lblRent"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblRent"), extendConfig({}, controller.args[2], "lblRent"));
        var lblRentValue = new kony.ui.Label(extendConfig({
            "id": "lblRentValue",
            "isVisible": true,
            "left": "0px",
            "skin": "sknLbl485C75LatoRegular13Px",
            "text": "123456",
            "top": "15dp",
            "width": "100%",
            "zIndex": 1
        }, controller.args[0], "lblRentValue"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblRentValue"), extendConfig({}, controller.args[2], "lblRentValue"));
        flxRent.add(lblRent, lblRentValue);
        var flxMortgage = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
            "clipBounds": true,
            "id": "flxMortgage",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "left": "2%",
            "isModalContainer": false,
            "skin": "slFbox",
            "top": "30dp",
            "width": "32%",
            "zIndex": 1
        }, controller.args[0], "flxMortgage"), extendConfig({}, controller.args[1], "flxMortgage"), extendConfig({}, controller.args[2], "flxMortgage"));
        flxMortgage.setDefaultUnit(kony.flex.DP);
        var lblMortgage = new kony.ui.Label(extendConfig({
            "id": "lblMortgage",
            "isVisible": true,
            "left": "0px",
            "skin": "sknLbl78818A11px",
            "i18n_text": "kony.i18n.getLocalizedString(\"i18n.Applications.Summary.MonthlyHousingMortgageCAPS\")",
            "top": "0px",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, controller.args[0], "lblMortgage"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblMortgage"), extendConfig({}, controller.args[2], "lblMortgage"));
        var lblMortgageValue = new kony.ui.Label(extendConfig({
            "id": "lblMortgageValue",
            "isVisible": true,
            "left": "0px",
            "skin": "sknLbl485C75LatoRegular13Px",
            "text": "123456",
            "top": "15dp",
            "width": "100%",
            "zIndex": 1
        }, controller.args[0], "lblMortgageValue"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblMortgageValue"), extendConfig({}, controller.args[2], "lblMortgageValue"));
        flxMortgage.add(lblMortgage, lblMortgageValue);
        flxExpenditureInfo.add(flxRent, flxMortgage);
        flxSection.add(flxExpenditureInfo);
        viewExpenditureInfo.add(loansSectionHeader, flxSection);
        return viewExpenditureInfo;
    }
})