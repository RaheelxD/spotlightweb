define(function() {
    return function(controller) {
        var simulationTemplate = new kony.ui.FlexContainer(extendConfig({
            "clipBounds": true,
            "isMaster": true,
            "height": "100%",
            "id": "simulationTemplate",
            "isVisible": true,
            "layoutType": kony.flex.FLOW_VERTICAL,
            "left": "0dp",
            "isModalContainer": false,
            "preShow": function(eventobject) {
                controller.AS_FlexContainer_a0401bdee1ec4cc9a14184d4ef8782f8(eventobject);
            },
            "skin": "sknFlxFFFFFFKA",
            "top": "0dp",
            "width": "100%"
        }, controller.args[0], "simulationTemplate"), extendConfig({}, controller.args[1], "simulationTemplate"), extendConfig({}, controller.args[2], "simulationTemplate"));
        simulationTemplate.setDefaultUnit(kony.flex.DP);
        var flxHeaderSimulate = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "clipBounds": true,
            "height": "65dp",
            "id": "flxHeaderSimulate",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "left": "0dp",
            "isModalContainer": false,
            "skin": "slFbox",
            "top": "0dp",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, controller.args[0], "flxHeaderSimulate"), extendConfig({}, controller.args[1], "flxHeaderSimulate"), extendConfig({}, controller.args[2], "flxHeaderSimulate"));
        flxHeaderSimulate.setDefaultUnit(kony.flex.DP);
        var flxSimulationBack = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "centerY": "50%",
            "clipBounds": true,
            "height": "20dp",
            "id": "flxSimulationBack",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "isModalContainer": false,
            "onClick": controller.AS_FlexContainer_a7f0205210454baa867f00270105be86,
            "right": "20dp",
            "skin": "slFbox",
            "top": "30dp",
            "width": "135dp",
            "zIndex": 2
        }, controller.args[0], "flxSimulationBack"), extendConfig({}, controller.args[1], "flxSimulationBack"), extendConfig({
            "hoverSkin": "hoverhandSkin"
        }, controller.args[2], "flxSimulationBack"));
        flxSimulationBack.setDefaultUnit(kony.flex.DP);
        var lblSimulationBackIcon = new kony.ui.Label(extendConfig({
            "centerY": "50%",
            "id": "lblSimulationBackIcon",
            "isVisible": true,
            "left": "0dp",
            "skin": "sknBackIcon006CCA",
            "text": "",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 2
        }, controller.args[0], "lblSimulationBackIcon"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblSimulationBackIcon"), extendConfig({}, controller.args[2], "lblSimulationBackIcon"));
        var lblBackToLoansSimulation = new kony.ui.Label(extendConfig({
            "centerY": "50%",
            "id": "lblBackToLoansSimulation",
            "isVisible": false,
            "right": "0dp",
            "skin": "sknLbl006CCA13pxKA",
            "text": "Back to Dashboard",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 2
        }, controller.args[0], "lblBackToLoansSimulation"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblBackToLoansSimulation"), extendConfig({
            "hoverSkin": "sknLblHover006CCA13pxKA"
        }, controller.args[2], "lblBackToLoansSimulation"));
        flxSimulationBack.add(lblSimulationBackIcon, lblBackToLoansSimulation);
        var lblSimulate = new kony.ui.Label(extendConfig({
            "centerY": "50%",
            "id": "lblSimulate",
            "isVisible": true,
            "left": "20dp",
            "skin": "sknlblLatoRegular485c75size14pxKA",
            "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmLoansDashboard.Simulate\")",
            "top": "11dp",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 2
        }, controller.args[0], "lblSimulate"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblSimulate"), extendConfig({}, controller.args[2], "lblSimulate"));
        var lstLoanTypes = new kony.ui.ListBox(extendConfig({
            "centerY": "50%",
            "focusSkin": "lstFont12pxLatoRegular",
            "height": "40dp",
            "id": "lstLoanTypes",
            "isVisible": true,
            "left": "90dp",
            "masterData": [
                ["Home loan", "Home loan"],
                ["Mortgage", "Mortgage"],
                ["Vehicle loan", "Vehicle loan"],
                ["Personal loan", "Personal loan"]
            ],
            "skin": "lstFont12pxLatoRegular",
            "top": "20dp",
            "width": "160dp",
            "zIndex": 2
        }, controller.args[0], "lstLoanTypes"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [3, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lstLoanTypes"), extendConfig({
            "hoverSkin": "lstFont12pxLatoRegular",
            "multiSelect": false
        }, controller.args[2], "lstLoanTypes"));
        var btnBackToLoans = new kony.ui.Button(extendConfig({
            "focusSkin": "slButtonGlossRed",
            "height": "27dp",
            "id": "btnBackToLoans",
            "isVisible": true,
            "right": 55,
            "skin": "skbBtnFont2EBAED100O",
            "i18n_text": "kony.i18n.getLocalizedString(\"i18n.Applications.BackToLoans\")",
            "top": "18dp",
            "width": "83dp",
            "zIndex": 20
        }, controller.args[0], "btnBackToLoans"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_CENTER,
            "displayText": true,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "btnBackToLoans"), extendConfig({}, controller.args[2], "btnBackToLoans"));
        flxHeaderSimulate.add(flxSimulationBack, lblSimulate, lstLoanTypes, btnBackToLoans);
        var flxBodySimulate = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "bottom": "20dp",
            "clipBounds": true,
            "height": "85%",
            "id": "flxBodySimulate",
            "isVisible": true,
            "layoutType": kony.flex.FLOW_HORIZONTAL,
            "left": "20dp",
            "isModalContainer": false,
            "right": "20dp",
            "skin": "flx2e3448border1px",
            "width": "96.50%",
            "zIndex": 1
        }, controller.args[0], "flxBodySimulate"), extendConfig({}, controller.args[1], "flxBodySimulate"), extendConfig({}, controller.args[2], "flxBodySimulate"));
        flxBodySimulate.setDefaultUnit(kony.flex.DP);
        var flxBodyleft = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "clipBounds": true,
            "height": "100%",
            "id": "flxBodyleft",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "left": "0dp",
            "isModalContainer": false,
            "skin": "slFbox",
            "top": "0dp",
            "width": "58%",
            "zIndex": 1
        }, controller.args[0], "flxBodyleft"), extendConfig({}, controller.args[1], "flxBodyleft"), extendConfig({}, controller.args[2], "flxBodyleft"));
        flxBodyleft.setDefaultUnit(kony.flex.DP);
        var lblSimulationDetails = new kony.ui.Label(extendConfig({
            "id": "lblSimulationDetails",
            "isVisible": true,
            "left": "20dp",
            "skin": "sknLblLatoBold16px485c75KA",
            "text": "SIMULATION DETAILS",
            "top": "20dp",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, controller.args[0], "lblSimulationDetails"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblSimulationDetails"), extendConfig({}, controller.args[2], "lblSimulationDetails"));
        var flxloanAmountSimulate = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "clipBounds": true,
            "height": "113dp",
            "id": "flxloanAmountSimulate",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "left": "0dp",
            "isModalContainer": false,
            "skin": "slFbox",
            "top": "75dp",
            "width": "100%",
            "zIndex": 1
        }, controller.args[0], "flxloanAmountSimulate"), extendConfig({}, controller.args[1], "flxloanAmountSimulate"), extendConfig({}, controller.args[2], "flxloanAmountSimulate"));
        flxloanAmountSimulate.setDefaultUnit(kony.flex.DP);
        var lblLoanAmount = new kony.ui.Label(extendConfig({
            "id": "lblLoanAmount",
            "isVisible": true,
            "left": "20dp",
            "skin": "sknlblLatoBold35475f14px",
            "text": "Loan amount ( $1K - $50K )",
            "top": "0dp",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, controller.args[0], "lblLoanAmount"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblLoanAmount"), extendConfig({}, controller.args[2], "lblLoanAmount"));
        var flxAmountSlider = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "clipBounds": true,
            "height": "40dp",
            "id": "flxAmountSlider",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "left": "15dp",
            "isModalContainer": false,
            "skin": "flx2e3448border1px",
            "top": "28dp",
            "width": "440dp",
            "zIndex": 1
        }, controller.args[0], "flxAmountSlider"), extendConfig({}, controller.args[1], "flxAmountSlider"), extendConfig({}, controller.args[2], "flxAmountSlider"));
        flxAmountSlider.setDefaultUnit(kony.flex.DP);
        var flxDollar = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "clipBounds": true,
            "height": "100%",
            "id": "flxDollar",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "left": "0dp",
            "isModalContainer": false,
            "skin": "flx2e3448KA",
            "top": "0dp",
            "width": "40dp",
            "zIndex": 1
        }, controller.args[0], "flxDollar"), extendConfig({}, controller.args[1], "flxDollar"), extendConfig({}, controller.args[2], "flxDollar"));
        flxDollar.setDefaultUnit(kony.flex.DP);
        var lblDollar = new kony.ui.Label(extendConfig({
            "centerX": "50%",
            "centerY": "50%",
            "id": "lblDollar",
            "isVisible": true,
            "skin": "sknLblLatoRegular485C7513pxKA",
            "text": " $",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, controller.args[0], "lblDollar"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblDollar"), extendConfig({}, controller.args[2], "lblDollar"));
        flxDollar.add(lblDollar);
        var txtLoanAmount = new kony.ui.TextBox2(extendConfig({
            "autoCapitalize": constants.TEXTBOX_AUTO_CAPITALIZE_NONE,
            "focusSkin": "defTextBoxFocus",
            "height": "40dp",
            "id": "txtLoanAmount",
            "isVisible": true,
            "keyBoardStyle": constants.TEXTBOX_KEY_BOARD_STYLE_DEFAULT,
            "left": "40dp",
            "placeholder": "Amount",
            "secureTextEntry": false,
            "skin": "skntxtbxDetails0d702c6b4ee8b46brd1px",
            "textInputMode": constants.TEXTBOX_INPUT_MODE_ANY,
            "top": "-1dp",
            "width": "100dp",
            "zIndex": 1
        }, controller.args[0], "txtLoanAmount"), extendConfig({
            "containerHeightMode": constants.TEXTBOX_FONT_METRICS_DRIVEN_HEIGHT,
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [3, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "txtLoanAmount"), extendConfig({
            "autoCorrect": false,
            "placeholderSkin": "defTextBoxPlaceholder"
        }, controller.args[2], "txtLoanAmount"));
        var sliderSimulate = new kony.ui.Slider(extendConfig({
            "centerY": "50%",
            "focusThumbImage": "dragger_square.png",
            "height": "40dp",
            "id": "sliderSimulate",
            "isVisible": true,
            "left": "160dp",
            "leftSkin": "slSliderLeftBlue",
            "max": 50000,
            "min": 1000,
            "rightSkin": "slSliderRightBlue",
            "selectedValue": 1000,
            "step": 1,
            "thumbImage": "dragger_square.png",
            "top": "0dp",
            "width": "260dp",
            "zIndex": 1
        }, controller.args[0], "sliderSimulate"), extendConfig({}, controller.args[1], "sliderSimulate"), extendConfig({
            "orientation": constants.SLIDER_HORIZONTAL_ORIENTATION,
            "thickness": 1,
            "viewType": constants.SLIDER_VIEW_TYPE_DEFAULT
        }, controller.args[2], "sliderSimulate"));
        flxAmountSlider.add(flxDollar, txtLoanAmount, sliderSimulate);
        var lblLoanAmountError = new kony.ui.Label(extendConfig({
            "id": "lblLoanAmountError",
            "isVisible": false,
            "left": "15dp",
            "right": "20dp",
            "skin": "sknlblError",
            "text": "Loan amount cannot be empty",
            "top": 70,
            "width": "240dp",
            "zIndex": 1
        }, controller.args[0], "lblLoanAmountError"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblLoanAmountError"), extendConfig({}, controller.args[2], "lblLoanAmountError"));
        flxloanAmountSimulate.add(lblLoanAmount, flxAmountSlider, lblLoanAmountError);
        var flxCreditScore = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "clipBounds": true,
            "height": "143dp",
            "id": "flxCreditScore",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "left": "0dp",
            "isModalContainer": false,
            "skin": "slFbox",
            "top": "188dp",
            "width": "100%",
            "zIndex": 1
        }, controller.args[0], "flxCreditScore"), extendConfig({}, controller.args[1], "flxCreditScore"), extendConfig({}, controller.args[2], "flxCreditScore"));
        flxCreditScore.setDefaultUnit(kony.flex.DP);
        var lblCreditScore = new kony.ui.Label(extendConfig({
            "id": "lblCreditScore",
            "isVisible": true,
            "left": "20dp",
            "skin": "sknlblLatoBold35475f14px",
            "text": "Credit score",
            "top": "0dp",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, controller.args[0], "lblCreditScore"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblCreditScore"), extendConfig({}, controller.args[2], "lblCreditScore"));
        var flxCreditScoreTab = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "clipBounds": true,
            "height": "70dp",
            "id": "flxCreditScoreTab",
            "isVisible": true,
            "layoutType": kony.flex.FLOW_HORIZONTAL,
            "left": "0dp",
            "isModalContainer": false,
            "top": "28dp",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, controller.args[0], "flxCreditScoreTab"), extendConfig({}, controller.args[1], "flxCreditScoreTab"), extendConfig({}, controller.args[2], "flxCreditScoreTab"));
        flxCreditScoreTab.setDefaultUnit(kony.flex.DP);
        var flxTabExcellent = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "clipBounds": true,
            "height": "60dp",
            "id": "flxTabExcellent",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "left": "20dp",
            "isModalContainer": false,
            "skin": "flxSelTabLeftBrd6372901PX",
            "top": "0dp",
            "width": "120dp",
            "zIndex": 1
        }, controller.args[0], "flxTabExcellent"), extendConfig({}, controller.args[1], "flxTabExcellent"), extendConfig({}, controller.args[2], "flxTabExcellent"));
        flxTabExcellent.setDefaultUnit(kony.flex.DP);
        var lbl720plus = new kony.ui.Label(extendConfig({
            "centerX": "50%",
            "id": "lbl720plus",
            "isVisible": true,
            "left": "13dp",
            "skin": "lbl484B52LatoBold12Px",
            "text": "720+",
            "top": "14dp",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, controller.args[0], "lbl720plus"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lbl720plus"), extendConfig({}, controller.args[2], "lbl720plus"));
        var lblTagExcellent = new kony.ui.Label(extendConfig({
            "bottom": "14dp",
            "height": "12dp",
            "id": "lblTagExcellent",
            "isVisible": true,
            "left": "20dp",
            "skin": "lblBorderdedede1pxRadius12px",
            "width": "12dp",
            "zIndex": 1
        }, controller.args[0], "lblTagExcellent"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblTagExcellent"), extendConfig({}, controller.args[2], "lblTagExcellent"));
        var lblExcellent = new kony.ui.Label(extendConfig({
            "bottom": "14dp",
            "id": "lblExcellent",
            "isVisible": true,
            "right": "20dp",
            "skin": "sknlbl6f8198Font12px",
            "text": "EXCELLENT",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, controller.args[0], "lblExcellent"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblExcellent"), extendConfig({}, controller.args[2], "lblExcellent"));
        flxTabExcellent.add(lbl720plus, lblTagExcellent, lblExcellent);
        var flxTabGood = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "clipBounds": true,
            "height": "60dp",
            "id": "flxTabGood",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "left": "0dp",
            "isModalContainer": false,
            "skin": "flxTabFFFFFFBrde0eb919d90aac24d1Px",
            "top": "0dp",
            "width": "93dp",
            "zIndex": 1
        }, controller.args[0], "flxTabGood"), extendConfig({}, controller.args[1], "flxTabGood"), extendConfig({}, controller.args[2], "flxTabGood"));
        flxTabGood.setDefaultUnit(kony.flex.DP);
        var lblGoodScore = new kony.ui.Label(extendConfig({
            "centerX": "50%",
            "id": "lblGoodScore",
            "isVisible": true,
            "left": "13dp",
            "skin": "lbl484B52LatoBold12Px",
            "text": "720-680",
            "top": "14dp",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, controller.args[0], "lblGoodScore"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblGoodScore"), extendConfig({}, controller.args[2], "lblGoodScore"));
        var lblTagGood = new kony.ui.Label(extendConfig({
            "bottom": "14dp",
            "height": "12dp",
            "id": "lblTagGood",
            "isVisible": true,
            "left": "20dp",
            "skin": "sknTaglbl11abebBorderRadius12px",
            "width": "12dp",
            "zIndex": 1
        }, controller.args[0], "lblTagGood"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblTagGood"), extendConfig({}, controller.args[2], "lblTagGood"));
        var lblGood = new kony.ui.Label(extendConfig({
            "bottom": "14dp",
            "id": "lblGood",
            "isVisible": true,
            "right": "20dp",
            "skin": "sknlbl6f8198Font12px",
            "text": "GOOD",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, controller.args[0], "lblGood"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblGood"), extendConfig({}, controller.args[2], "lblGood"));
        flxTabGood.add(lblGoodScore, lblTagGood, lblGood);
        var flxFair = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "clipBounds": true,
            "height": "60dp",
            "id": "flxFair",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "left": "0dp",
            "isModalContainer": false,
            "skin": "flxTabFFFFFFBrde0eb919d90aac24d1Px",
            "top": "0dp",
            "width": "85dp",
            "zIndex": 1
        }, controller.args[0], "flxFair"), extendConfig({}, controller.args[1], "flxFair"), extendConfig({}, controller.args[2], "flxFair"));
        flxFair.setDefaultUnit(kony.flex.DP);
        var flxFairScore = new kony.ui.Label(extendConfig({
            "centerX": "50%",
            "id": "flxFairScore",
            "isVisible": true,
            "left": "13dp",
            "skin": "lbl484B52LatoBold12Px",
            "text": "680-640",
            "top": "14dp",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, controller.args[0], "flxFairScore"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "flxFairScore"), extendConfig({}, controller.args[2], "flxFairScore"));
        var lblTagFair = new kony.ui.Label(extendConfig({
            "bottom": "14dp",
            "height": "12dp",
            "id": "lblTagFair",
            "isVisible": true,
            "left": "20dp",
            "skin": "lblBorderdedede1pxRadius12px",
            "width": "12dp",
            "zIndex": 1
        }, controller.args[0], "lblTagFair"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblTagFair"), extendConfig({}, controller.args[2], "lblTagFair"));
        var lblFair = new kony.ui.Label(extendConfig({
            "bottom": "14dp",
            "id": "lblFair",
            "isVisible": true,
            "right": "20dp",
            "skin": "sknlbl6f8198Font12px",
            "text": "FAIR",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, controller.args[0], "lblFair"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblFair"), extendConfig({}, controller.args[2], "lblFair"));
        flxFair.add(flxFairScore, lblTagFair, lblFair);
        var flxPoor = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "clipBounds": true,
            "height": "60dp",
            "id": "flxPoor",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "left": "0dp",
            "isModalContainer": false,
            "skin": "sknlblSelTabRightBorder637290",
            "top": "0dp",
            "width": "93dp",
            "zIndex": 1
        }, controller.args[0], "flxPoor"), extendConfig({}, controller.args[1], "flxPoor"), extendConfig({}, controller.args[2], "flxPoor"));
        flxPoor.setDefaultUnit(kony.flex.DP);
        var lblPoorScore = new kony.ui.Label(extendConfig({
            "centerX": "50%",
            "id": "lblPoorScore",
            "isVisible": true,
            "left": "13dp",
            "skin": "lbl484B52LatoBold12Px",
            "text": "<640",
            "top": "14dp",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, controller.args[0], "lblPoorScore"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblPoorScore"), extendConfig({}, controller.args[2], "lblPoorScore"));
        var lblTagPoor = new kony.ui.Label(extendConfig({
            "bottom": "14dp",
            "height": "12dp",
            "id": "lblTagPoor",
            "isVisible": true,
            "left": "20dp",
            "skin": "lblBorderdedede1pxRadius12px",
            "width": "12dp",
            "zIndex": 1
        }, controller.args[0], "lblTagPoor"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblTagPoor"), extendConfig({}, controller.args[2], "lblTagPoor"));
        var lblPoor = new kony.ui.Label(extendConfig({
            "bottom": "14dp",
            "id": "lblPoor",
            "isVisible": true,
            "right": "20dp",
            "skin": "sknlbl6f8198Font12px",
            "text": "POOR",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, controller.args[0], "lblPoor"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblPoor"), extendConfig({}, controller.args[2], "lblPoor"));
        flxPoor.add(lblPoorScore, lblTagPoor, lblPoor);
        flxCreditScoreTab.add(flxTabExcellent, flxTabGood, flxFair, flxPoor);
        var lblCreditScoreError = new kony.ui.Label(extendConfig({
            "id": "lblCreditScoreError",
            "isVisible": false,
            "left": "20dp",
            "right": "20dp",
            "skin": "sknlblError",
            "text": "Select an option",
            "top": 108,
            "width": "240dp",
            "zIndex": 1
        }, controller.args[0], "lblCreditScoreError"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblCreditScoreError"), extendConfig({}, controller.args[2], "lblCreditScoreError"));
        flxCreditScore.add(lblCreditScore, flxCreditScoreTab, lblCreditScoreError);
        var flxEmploymentStatus = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "clipBounds": true,
            "height": "108dp",
            "id": "flxEmploymentStatus",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "left": "0dp",
            "isModalContainer": false,
            "skin": "slFbox",
            "top": "331dp",
            "width": "100%",
            "zIndex": 1
        }, controller.args[0], "flxEmploymentStatus"), extendConfig({}, controller.args[1], "flxEmploymentStatus"), extendConfig({}, controller.args[2], "flxEmploymentStatus"));
        flxEmploymentStatus.setDefaultUnit(kony.flex.DP);
        var lblEmploymentStatus = new kony.ui.Label(extendConfig({
            "id": "lblEmploymentStatus",
            "isVisible": true,
            "left": "20dp",
            "skin": "sknlblLatoBold35475f14px",
            "text": "Employment status",
            "top": "0dp",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, controller.args[0], "lblEmploymentStatus"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblEmploymentStatus"), extendConfig({}, controller.args[2], "lblEmploymentStatus"));
        var lstEmploymentStatusSimulate = new kony.ui.ListBox(extendConfig({
            "focusSkin": "lstDEE2E95border3pxFnt12px485c75KA",
            "height": "40dp",
            "id": "lstEmploymentStatusSimulate",
            "isVisible": true,
            "left": "20dp",
            "masterData": [
                ["Full Time", "Full Time"],
                ["Part Time", "Part Time"],
                ["Self Employed", "Self Employed"],
                ["Uemployed", "Uemployed"],
                ["Other", "Other"]
            ],
            "skin": "lst485C75border3pxFnt12px485c75",
            "top": "28dp",
            "width": "260dp",
            "zIndex": 2
        }, controller.args[0], "lstEmploymentStatusSimulate"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [3, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lstEmploymentStatusSimulate"), extendConfig({
            "hoverSkin": "lstDEE2E95border3pxFnt12px485c75KA",
            "multiSelect": false
        }, controller.args[2], "lstEmploymentStatusSimulate"));
        var lblEmploymentStatusError = new kony.ui.Label(extendConfig({
            "id": "lblEmploymentStatusError",
            "isVisible": false,
            "left": "20dp",
            "right": "20dp",
            "skin": "sknlblError",
            "text": "Select Employment status",
            "top": 78,
            "width": "240dp",
            "zIndex": 1
        }, controller.args[0], "lblEmploymentStatusError"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblEmploymentStatusError"), extendConfig({}, controller.args[2], "lblEmploymentStatusError"));
        flxEmploymentStatus.add(lblEmploymentStatus, lstEmploymentStatusSimulate, lblEmploymentStatusError);
        var flxFooter = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "bottom": 20,
            "clipBounds": true,
            "height": "60dp",
            "id": "flxFooter",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "left": "0dp",
            "isModalContainer": false,
            "skin": "slFbox",
            "width": "100%",
            "zIndex": 1
        }, controller.args[0], "flxFooter"), extendConfig({}, controller.args[1], "flxFooter"), extendConfig({}, controller.args[2], "flxFooter"));
        flxFooter.setDefaultUnit(kony.flex.DP);
        var lblBottomSeperatorGuestSimulation = new kony.ui.Label(extendConfig({
            "centerX": "50%",
            "height": "1dp",
            "id": "lblBottomSeperatorGuestSimulation",
            "isVisible": true,
            "left": "35dp",
            "right": "375dp",
            "skin": "sknlblSeperatorBgD5D9DD",
            "top": "0dp",
            "zIndex": 2
        }, controller.args[0], "lblBottomSeperatorGuestSimulation"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblBottomSeperatorGuestSimulation"), extendConfig({}, controller.args[2], "lblBottomSeperatorGuestSimulation"));
        var btnSimulateSimulation = new kony.ui.Button(extendConfig({
            "focusSkin": "sknBtn004F93LatoRegKA",
            "height": "40dp",
            "id": "btnSimulateSimulation",
            "isVisible": true,
            "right": 20,
            "skin": "sknBtn006CCALatoRegKA",
            "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmLoansDashboard.Simulate\")",
            "top": "20dp",
            "width": "100dp",
            "zIndex": 2
        }, controller.args[0], "btnSimulateSimulation"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_CENTER,
            "displayText": true,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "btnSimulateSimulation"), extendConfig({
            "hoverSkin": "sknBtn004F93LatoRegKA"
        }, controller.args[2], "btnSimulateSimulation"));
        flxFooter.add(lblBottomSeperatorGuestSimulation, btnSimulateSimulation);
        flxBodyleft.add(lblSimulationDetails, flxloanAmountSimulate, flxCreditScore, flxEmploymentStatus, flxFooter);
        var flxBodyRight = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "clipBounds": true,
            "height": "100%",
            "id": "flxBodyRight",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "left": "0",
            "isModalContainer": false,
            "skin": "flx2e3448KA",
            "top": "0dp",
            "width": "42%",
            "zIndex": 1
        }, controller.args[0], "flxBodyRight"), extendConfig({}, controller.args[1], "flxBodyRight"), extendConfig({}, controller.args[2], "flxBodyRight"));
        flxBodyRight.setDefaultUnit(kony.flex.DP);
        var lblSimulationResults = new kony.ui.Label(extendConfig({
            "id": "lblSimulationResults",
            "isVisible": true,
            "left": "38dp",
            "skin": "sknLbl274060100PerKA",
            "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmLoansDashboard.SimulationResults\")",
            "top": "33dp",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, controller.args[0], "lblSimulationResults"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblSimulationResults"), extendConfig({}, controller.args[2], "lblSimulationResults"));
        var segSimulationResults = new kony.ui.SegmentedUI2(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
            "data": [{
                "lblAPR": "Label",
                "lblAPRValue": "Label",
                "lblApply": "kony.i18n.getLocalizedString(\"i18n.frmLoansDashboard.ApplyStaticMessage\")",
                "lblLoanType": "Label",
                "lblRate": "Label",
                "lblRateValue": "Label",
                "lblSeperator": "",
                "lblTerms": "Label",
                "lblTermsValue": "Label"
            }, {
                "lblAPR": "Label",
                "lblAPRValue": "Label",
                "lblApply": "kony.i18n.getLocalizedString(\"i18n.frmLoansDashboard.ApplyStaticMessage\")",
                "lblLoanType": "Label",
                "lblRate": "Label",
                "lblRateValue": "Label",
                "lblSeperator": "",
                "lblTerms": "Label",
                "lblTermsValue": "Label"
            }, {
                "lblAPR": "Label",
                "lblAPRValue": "Label",
                "lblApply": "kony.i18n.getLocalizedString(\"i18n.frmLoansDashboard.ApplyStaticMessage\")",
                "lblLoanType": "Label",
                "lblRate": "Label",
                "lblRateValue": "Label",
                "lblSeperator": "",
                "lblTerms": "Label",
                "lblTermsValue": "Label"
            }],
            "groupCells": false,
            "id": "segSimulationResults",
            "isVisible": true,
            "left": "20dp",
            "needPageIndicator": true,
            "pageOffDotImage": "pageoffdot.png",
            "pageOnDotImage": "pageondot.png",
            "retainSelection": false,
            "right": "20dp",
            "rowFocusSkin": "sknsegffffffOp100Bordere1e5ed",
            "rowSkin": "sknsegffffKA",
            "rowTemplate": "flxGuestSimulationResults",
            "sectionHeaderSkin": "sliPhoneSegmentHeader",
            "selectionBehavior": constants.SEGUI_DEFAULT_BEHAVIOR,
            "separatorRequired": false,
            "showScrollbars": false,
            "top": "70dp",
            "viewType": constants.SEGUI_VIEW_TYPE_TABLEVIEW,
            "widgetDataMap": {
                "flxGuestSimulationResults": "flxGuestSimulationResults",
                "flxHeaderSimulationResults": "flxHeaderSimulationResults",
                "flxRecommendedLoans": "flxRecommendedLoans",
                "lblAPR": "lblAPR",
                "lblAPRValue": "lblAPRValue",
                "lblApply": "lblApply",
                "lblLoanType": "lblLoanType",
                "lblRate": "lblRate",
                "lblRateValue": "lblRateValue",
                "lblSeperator": "lblSeperator",
                "lblTerms": "lblTerms",
                "lblTermsValue": "lblTermsValue"
            },
            "zIndex": 1
        }, controller.args[0], "segSimulationResults"), extendConfig({
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "segSimulationResults"), extendConfig({}, controller.args[2], "segSimulationResults"));
        var lblSimulateToGetRes = new kony.ui.Label(extendConfig({
            "centerX": "50%",
            "centerY": "50%",
            "id": "lblSimulateToGetRes",
            "isVisible": true,
            "skin": "sknlblLatoBold35475f14px",
            "text": "Simulate to view the results",
            "top": "0dp",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, controller.args[0], "lblSimulateToGetRes"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_CENTER,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblSimulateToGetRes"), extendConfig({}, controller.args[2], "lblSimulateToGetRes"));
        flxBodyRight.add(lblSimulationResults, segSimulationResults, lblSimulateToGetRes);
        flxBodySimulate.add(flxBodyleft, flxBodyRight);
        simulationTemplate.add(flxHeaderSimulate, flxBodySimulate);
        return simulationTemplate;
    }
})