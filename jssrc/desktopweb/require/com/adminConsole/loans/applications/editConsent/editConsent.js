define(function() {
    return function(controller) {
        var editConsent = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
            "clipBounds": true,
            "isMaster": true,
            "id": "editConsent",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "left": "0dp",
            "isModalContainer": false,
            "skin": "slFbox",
            "top": "40dp",
            "width": kony.flex.USE_PREFFERED_SIZE
        }, controller.args[0], "editConsent"), extendConfig({}, controller.args[1], "editConsent"), extendConfig({}, controller.args[2], "editConsent"));
        editConsent.setDefaultUnit(kony.flex.DP);
        var loansSectionHeader = new com.adminConsole.loans.applications.loansSectionHeader(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
            "id": "loansSectionHeader",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "left": "0dp",
            "masterType": constants.MASTER_TYPE_DEFAULT,
            "isModalContainer": false,
            "skin": "slFbox",
            "top": "0dp",
            "width": "100%",
            "overrides": {
                "btnEdit": {
                    "isVisible": false
                },
                "lblSectionHeader": {
                    "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmTrackApplication.Consent\")"
                }
            }
        }, controller.args[0], "loansSectionHeader"), extendConfig({
            "overrides": {}
        }, controller.args[1], "loansSectionHeader"), extendConfig({
            "overrides": {}
        }, controller.args[2], "loansSectionHeader"));
        var flxConsentSection = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
            "bottom": "30dp",
            "clipBounds": true,
            "id": "flxConsentSection",
            "isVisible": true,
            "layoutType": kony.flex.FLOW_HORIZONTAL,
            "left": "20dp",
            "isModalContainer": false,
            "right": "20dp",
            "skin": "slFbox",
            "top": "42dp",
            "zIndex": 1
        }, controller.args[0], "flxConsentSection"), extendConfig({}, controller.args[1], "flxConsentSection"), extendConfig({}, controller.args[2], "flxConsentSection"));
        flxConsentSection.setDefaultUnit(kony.flex.DP);
        var flxAcceptanceIcon = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "clipBounds": true,
            "height": "15dp",
            "id": "flxAcceptanceIcon",
            "isVisible": true,
            "layoutType": kony.flex.FLOW_HORIZONTAL,
            "left": "0dp",
            "isModalContainer": false,
            "skin": "flxB7B7B7",
            "top": "30dp",
            "width": "15dp"
        }, controller.args[0], "flxAcceptanceIcon"), extendConfig({}, controller.args[1], "flxAcceptanceIcon"), extendConfig({}, controller.args[2], "flxAcceptanceIcon"));
        flxAcceptanceIcon.setDefaultUnit(kony.flex.DP);
        var lblAcceptanceIcon = new kony.ui.Label(extendConfig({
            "centerY": "50%",
            "id": "lblAcceptanceIcon",
            "isVisible": true,
            "left": "0",
            "skin": "sknIcon13pxWhite",
            "text": "",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, controller.args[0], "lblAcceptanceIcon"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblAcceptanceIcon"), extendConfig({}, controller.args[2], "lblAcceptanceIcon"));
        flxAcceptanceIcon.add(lblAcceptanceIcon);
        var lblConsentMsg = new kony.ui.Label(extendConfig({
            "id": "lblConsentMsg",
            "isVisible": true,
            "left": "10dp",
            "skin": "sknLbl485C75LatoRegular13Px",
            "text": "I John Williams, Consent*",
            "top": "30dp",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, controller.args[0], "lblConsentMsg"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblConsentMsg"), extendConfig({}, controller.args[2], "lblConsentMsg"));
        var flxConsentTerms = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
            "clipBounds": true,
            "id": "flxConsentTerms",
            "isVisible": true,
            "layoutType": kony.flex.FLOW_HORIZONTAL,
            "left": "0dp",
            "isModalContainer": false,
            "skin": "sknFlxPointer",
            "top": "30dp",
            "width": "60dp"
        }, controller.args[0], "flxConsentTerms"), extendConfig({}, controller.args[1], "flxConsentTerms"), extendConfig({}, controller.args[2], "flxConsentTerms"));
        flxConsentTerms.setDefaultUnit(kony.flex.DP);
        var lblConsentTerms = new kony.ui.Label(extendConfig({
            "id": "lblConsentTerms",
            "isVisible": true,
            "skin": "sknLblHyperLink14PX0a9c098be02ca43",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, controller.args[0], "lblConsentTerms"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblConsentTerms"), extendConfig({}, controller.args[2], "lblConsentTerms"));
        flxConsentTerms.add(lblConsentTerms);
        flxConsentSection.add(flxAcceptanceIcon, lblConsentMsg, flxConsentTerms);
        editConsent.add(loansSectionHeader, flxConsentSection);
        return editConsent;
    }
})