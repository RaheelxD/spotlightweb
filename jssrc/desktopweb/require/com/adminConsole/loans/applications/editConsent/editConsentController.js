define("com/adminConsole/loans/applications/editConsent/usereditConsentController", function() {
    return {};
});
define("com/adminConsole/loans/applications/editConsent/editConsentControllerActions", {
    /*
      This is an auto generated file and any modifications to it may result in corruption of the action sequence.
    */
    /** onClick defined for btnEdit **/
    AS_Button_b4630188e8cb4e1788612fb028603a0f: function AS_Button_b4630188e8cb4e1788612fb028603a0f(eventobject) {
        var self = this;
        var self = this;
        var parentId = eventobject.parent.parent.parent.id;
    }
});
define("com/adminConsole/loans/applications/editConsent/editConsentController", ["com/adminConsole/loans/applications/editConsent/usereditConsentController", "com/adminConsole/loans/applications/editConsent/editConsentControllerActions"], function() {
    var controller = require("com/adminConsole/loans/applications/editConsent/usereditConsentController");
    var actions = require("com/adminConsole/loans/applications/editConsent/editConsentControllerActions");
    for (var key in actions) {
        controller[key] = actions[key];
    }
    return controller;
});
