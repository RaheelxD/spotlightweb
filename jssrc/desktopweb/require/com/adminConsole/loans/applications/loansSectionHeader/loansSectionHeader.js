define(function() {
    return function(controller) {
        var loansSectionHeader = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
            "clipBounds": true,
            "isMaster": true,
            "id": "loansSectionHeader",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "left": "0dp",
            "isModalContainer": false,
            "skin": "slFbox",
            "top": "0dp",
            "width": "100%"
        }, controller.args[0], "loansSectionHeader"), extendConfig({}, controller.args[1], "loansSectionHeader"), extendConfig({}, controller.args[2], "loansSectionHeader"));
        loansSectionHeader.setDefaultUnit(kony.flex.DP);
        var flxSectionHeader = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "clipBounds": true,
            "height": "42dp",
            "id": "flxSectionHeader",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "left": "20dp",
            "isModalContainer": false,
            "right": "20dp",
            "skin": "slFbox",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 10
        }, controller.args[0], "flxSectionHeader"), extendConfig({}, controller.args[1], "flxSectionHeader"), extendConfig({}, controller.args[2], "flxSectionHeader"));
        flxSectionHeader.setDefaultUnit(kony.flex.DP);
        var lblSectionHeader = new kony.ui.Label(extendConfig({
            "height": "20dp",
            "id": "lblSectionHeader",
            "isVisible": true,
            "left": "0dp",
            "skin": "sknLbl485C75LatoRegular14px",
            "i18n_text": "kony.i18n.getLocalizedString(\"i18n.Applications.Summary.PresentAddress\")",
            "top": "2dp",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 10
        }, controller.args[0], "lblSectionHeader"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblSectionHeader"), extendConfig({}, controller.args[2], "lblSectionHeader"));
        var btnEdit = new kony.ui.Button(extendConfig({
            "focusSkin": "sknbtnf7f7faLatoRegular12Px485c75Border1px485C75Radius20px",
            "height": "22dp",
            "id": "btnEdit",
            "isVisible": true,
            "right": "0dp",
            "skin": "sknbtnf7f7faLatoRegular12Px485c75Border1px485C75Radius20px",
            "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmLoansDashboard.Edit\")",
            "top": "0dp",
            "width": "50px",
            "zIndex": 1
        }, controller.args[0], "btnEdit"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_CENTER,
            "displayText": true,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "btnEdit"), extendConfig({
            "hoverSkin": "sknbtnffffffLatoRegular12Px485c75Border1px485C75Radius20px"
        }, controller.args[2], "btnEdit"));
        var lblSeparator = new kony.ui.Label(extendConfig({
            "height": "1dp",
            "id": "lblSeparator",
            "isVisible": true,
            "left": "0dp",
            "right": "0dp",
            "skin": "sknLble1e5edOp100O",
            "top": "40dp",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 10
        }, controller.args[0], "lblSeparator"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblSeparator"), extendConfig({}, controller.args[2], "lblSeparator"));
        flxSectionHeader.add(lblSectionHeader, btnEdit, lblSeparator);
        loansSectionHeader.add(flxSectionHeader);
        return loansSectionHeader;
    }
})