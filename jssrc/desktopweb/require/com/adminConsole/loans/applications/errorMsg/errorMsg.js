define(function() {
    return function(controller) {
        var errorMsg = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
            "clipBounds": true,
            "isMaster": true,
            "id": "errorMsg",
            "isVisible": true,
            "layoutType": kony.flex.FLOW_HORIZONTAL,
            "left": "0dp",
            "isModalContainer": false,
            "skin": "slFbox",
            "top": "10dp",
            "width": kony.flex.USE_PREFFERED_SIZE
        }, controller.args[0], "errorMsg"), extendConfig({}, controller.args[1], "errorMsg"), extendConfig({}, controller.args[2], "errorMsg"));
        errorMsg.setDefaultUnit(kony.flex.DP);
        var lblErrorIcon = new kony.ui.Label(extendConfig({
            "height": "15dp",
            "id": "lblErrorIcon",
            "isVisible": true,
            "left": "0dp",
            "skin": "sknErrorIcon",
            "text": "",
            "top": "0dp",
            "width": "15dp",
            "zIndex": 1
        }, controller.args[0], "lblErrorIcon"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblErrorIcon"), extendConfig({}, controller.args[2], "lblErrorIcon"));
        var lblErrorText = new kony.ui.Label(extendConfig({
            "height": "15dp",
            "id": "lblErrorText",
            "isVisible": true,
            "left": "0dp",
            "skin": "sknlblError",
            "text": "Error Message",
            "top": "0dp",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, controller.args[0], "lblErrorText"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblErrorText"), extendConfig({}, controller.args[2], "lblErrorText"));
        errorMsg.add(lblErrorIcon, lblErrorText);
        return errorMsg;
    }
})