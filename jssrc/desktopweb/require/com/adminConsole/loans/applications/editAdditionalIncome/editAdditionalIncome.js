define(function() {
    return function(controller) {
        var editAdditionalIncome = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
            "clipBounds": true,
            "isMaster": true,
            "id": "editAdditionalIncome",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "left": "0dp",
            "isModalContainer": false,
            "skin": "slFbox",
            "top": "30dp",
            "width": kony.flex.USE_PREFFERED_SIZE
        }, controller.args[0], "editAdditionalIncome"), extendConfig({}, controller.args[1], "editAdditionalIncome"), extendConfig({}, controller.args[2], "editAdditionalIncome"));
        editAdditionalIncome.setDefaultUnit(kony.flex.DP);
        var flxOtherSources = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "bottom": 30,
            "clipBounds": true,
            "height": "40dp",
            "id": "flxOtherSources",
            "isVisible": true,
            "layoutType": kony.flex.FLOW_HORIZONTAL,
            "left": 20,
            "isModalContainer": false,
            "right": 20,
            "skin": "slFbox",
            "top": "0dp",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, controller.args[0], "flxOtherSources"), extendConfig({}, controller.args[1], "flxOtherSources"), extendConfig({}, controller.args[2], "flxOtherSources"));
        flxOtherSources.setDefaultUnit(kony.flex.DP);
        var lblOtherSources = new kony.ui.Label(extendConfig({
            "centerY": "50%",
            "id": "lblOtherSources",
            "isVisible": true,
            "left": "0px",
            "skin": "sknLblLatoReg78818912px",
            "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmTrackApplication.OtherSourcesofIncome\")",
            "top": "0px",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, controller.args[0], "lblOtherSources"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblOtherSources"), extendConfig({}, controller.args[2], "lblOtherSources"));
        var flxOptions = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "clipBounds": true,
            "height": "40dp",
            "id": "flxOptions",
            "isVisible": true,
            "layoutType": kony.flex.FLOW_HORIZONTAL,
            "left": "20dp",
            "isModalContainer": false,
            "skin": "slFbox",
            "top": "0",
            "width": kony.flex.USE_PREFFERED_SIZE
        }, controller.args[0], "flxOptions"), extendConfig({}, controller.args[1], "flxOptions"), extendConfig({}, controller.args[2], "flxOptions"));
        flxOptions.setDefaultUnit(kony.flex.DP);
        var flxRadioButtonYes = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "clipBounds": true,
            "height": "100%",
            "id": "flxRadioButtonYes",
            "isVisible": true,
            "layoutType": kony.flex.FLOW_HORIZONTAL,
            "left": "0dp",
            "isModalContainer": false,
            "skin": "slFbox",
            "top": "0dp",
            "width": "55dp",
            "zIndex": 1
        }, controller.args[0], "flxRadioButtonYes"), extendConfig({}, controller.args[1], "flxRadioButtonYes"), extendConfig({}, controller.args[2], "flxRadioButtonYes"));
        flxRadioButtonYes.setDefaultUnit(kony.flex.DP);
        var imgRadioButtonYes = new kony.ui.Image2(extendConfig({
            "centerY": "50%",
            "height": "15dp",
            "id": "imgRadioButtonYes",
            "isVisible": true,
            "left": "0dp",
            "skin": "slImage",
            "src": "radionormal_1x.png",
            "top": "0dp",
            "width": "15dp",
            "zIndex": 1
        }, controller.args[0], "imgRadioButtonYes"), extendConfig({
            "imageScaleMode": constants.IMAGE_SCALE_MODE_MAINTAIN_ASPECT_RATIO,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "imgRadioButtonYes"), extendConfig({}, controller.args[2], "imgRadioButtonYes"));
        var lblRadioButtonYes = new kony.ui.Label(extendConfig({
            "centerY": "50%",
            "id": "lblRadioButtonYes",
            "isVisible": true,
            "left": "15dp",
            "skin": "sknLbl485C75LatoRegular13Px",
            "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmTrackApplication.Yes\")",
            "top": "0dp",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, controller.args[0], "lblRadioButtonYes"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_TOP_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblRadioButtonYes"), extendConfig({}, controller.args[2], "lblRadioButtonYes"));
        flxRadioButtonYes.add(imgRadioButtonYes, lblRadioButtonYes);
        var flxRadioButtonNo = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "clipBounds": true,
            "height": "100%",
            "id": "flxRadioButtonNo",
            "isVisible": true,
            "layoutType": kony.flex.FLOW_HORIZONTAL,
            "left": "15dp",
            "isModalContainer": false,
            "skin": "slFbox",
            "top": "0dp",
            "width": "55dp",
            "zIndex": 1
        }, controller.args[0], "flxRadioButtonNo"), extendConfig({}, controller.args[1], "flxRadioButtonNo"), extendConfig({}, controller.args[2], "flxRadioButtonNo"));
        flxRadioButtonNo.setDefaultUnit(kony.flex.DP);
        var imgRadioButtonNo = new kony.ui.Image2(extendConfig({
            "centerY": "50%",
            "height": "15dp",
            "id": "imgRadioButtonNo",
            "isVisible": true,
            "left": "0dp",
            "skin": "slImage",
            "src": "radionormal_1x.png",
            "top": "0dp",
            "width": "15dp",
            "zIndex": 1
        }, controller.args[0], "imgRadioButtonNo"), extendConfig({
            "imageScaleMode": constants.IMAGE_SCALE_MODE_MAINTAIN_ASPECT_RATIO,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "imgRadioButtonNo"), extendConfig({}, controller.args[2], "imgRadioButtonNo"));
        var lblRadioButtonNo = new kony.ui.Label(extendConfig({
            "centerY": "50%",
            "id": "lblRadioButtonNo",
            "isVisible": true,
            "left": "15dp",
            "skin": "sknLbl485C75LatoRegular13Px",
            "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmTrackApplication.No\")",
            "top": "0dp",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, controller.args[0], "lblRadioButtonNo"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_TOP_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblRadioButtonNo"), extendConfig({}, controller.args[2], "lblRadioButtonNo"));
        flxRadioButtonNo.add(imgRadioButtonNo, lblRadioButtonNo);
        flxOptions.add(flxRadioButtonYes, flxRadioButtonNo);
        flxOtherSources.add(lblOtherSources, flxOptions);
        var loansSectionHeader = new com.adminConsole.loans.applications.loansSectionHeader(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
            "id": "loansSectionHeader",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "left": "0dp",
            "masterType": constants.MASTER_TYPE_DEFAULT,
            "isModalContainer": false,
            "skin": "slFbox",
            "top": 80,
            "width": "100%",
            "overrides": {
                "btnEdit": {
                    "isVisible": false
                },
                "lblSectionHeader": {
                    "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmTrackApplication.AdditionalIncomeSource\")"
                },
                "loansSectionHeader": {
                    "top": 80
                }
            }
        }, controller.args[0], "loansSectionHeader"), extendConfig({
            "overrides": {}
        }, controller.args[1], "loansSectionHeader"), extendConfig({
            "overrides": {}
        }, controller.args[2], "loansSectionHeader"));
        var flxSection = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
            "clipBounds": true,
            "id": "flxSection",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "left": "20dp",
            "isModalContainer": false,
            "right": "20dp",
            "skin": "slFbox",
            "top": "122dp",
            "zIndex": 1
        }, controller.args[0], "flxSection"), extendConfig({}, controller.args[1], "flxSection"), extendConfig({}, controller.args[2], "flxSection"));
        flxSection.setDefaultUnit(kony.flex.DP);
        var flxIncomeSources = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
            "clipBounds": true,
            "id": "flxIncomeSources",
            "isVisible": true,
            "layoutType": kony.flex.FLOW_VERTICAL,
            "left": 0,
            "isModalContainer": false,
            "right": 0,
            "skin": "slFbox",
            "top": "0dp",
            "width": "100%",
            "zIndex": 1
        }, controller.args[0], "flxIncomeSources"), extendConfig({}, controller.args[1], "flxIncomeSources"), extendConfig({}, controller.args[2], "flxIncomeSources"));
        flxIncomeSources.setDefaultUnit(kony.flex.DP);
        var flxIncomeGrp1 = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
            "clipBounds": true,
            "id": "flxIncomeGrp1",
            "isVisible": true,
            "layoutType": kony.flex.FLOW_VERTICAL,
            "left": "0dp",
            "isModalContainer": false,
            "skin": "slFbox",
            "top": "0dp",
            "width": "100%"
        }, controller.args[0], "flxIncomeGrp1"), extendConfig({}, controller.args[1], "flxIncomeGrp1"), extendConfig({}, controller.args[2], "flxIncomeGrp1"));
        flxIncomeGrp1.setDefaultUnit(kony.flex.DP);
        var flxIncomeDtls1 = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
            "clipBounds": true,
            "id": "flxIncomeDtls1",
            "isVisible": true,
            "layoutType": kony.flex.FLOW_VERTICAL,
            "left": "0",
            "isModalContainer": false,
            "skin": "slFbox",
            "top": "30dp",
            "width": "100%",
            "zIndex": 1
        }, controller.args[0], "flxIncomeDtls1"), extendConfig({}, controller.args[1], "flxIncomeDtls1"), extendConfig({}, controller.args[2], "flxIncomeDtls1"));
        flxIncomeDtls1.setDefaultUnit(kony.flex.DP);
        var flxIncome1 = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
            "clipBounds": true,
            "id": "flxIncome1",
            "isVisible": true,
            "layoutType": kony.flex.FLOW_HORIZONTAL,
            "left": "0dp",
            "isModalContainer": false,
            "skin": "slFbox",
            "top": "0dp",
            "width": "100%"
        }, controller.args[0], "flxIncome1"), extendConfig({}, controller.args[1], "flxIncome1"), extendConfig({}, controller.args[2], "flxIncome1"));
        flxIncome1.setDefaultUnit(kony.flex.DP);
        var flxTypeOfIncome1 = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
            "bottom": "0px",
            "clipBounds": true,
            "id": "flxTypeOfIncome1",
            "isVisible": true,
            "layoutType": kony.flex.FLOW_VERTICAL,
            "left": "0",
            "isModalContainer": false,
            "skin": "slFbox",
            "top": "0dp",
            "width": "32%",
            "zIndex": 1
        }, controller.args[0], "flxTypeOfIncome1"), extendConfig({}, controller.args[1], "flxTypeOfIncome1"), extendConfig({}, controller.args[2], "flxTypeOfIncome1"));
        flxTypeOfIncome1.setDefaultUnit(kony.flex.DP);
        var lblTypeOfIncome1 = new kony.ui.Label(extendConfig({
            "id": "lblTypeOfIncome1",
            "isVisible": true,
            "left": "0px",
            "skin": "sknLblLatoReg78818912px",
            "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmTrackApplication.TypeOfIncome\")",
            "top": "0px",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, controller.args[0], "lblTypeOfIncome1"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblTypeOfIncome1"), extendConfig({}, controller.args[2], "lblTypeOfIncome1"));
        var lstTypeOfIncome1 = new kony.ui.ListBox(extendConfig({
            "height": "40px",
            "id": "lstTypeOfIncome1",
            "isVisible": true,
            "left": "0px",
            "skin": "sknlbxBgffffffBorderc1c9ceRadius3Px",
            "top": "15dp",
            "width": "100%",
            "zIndex": 1
        }, controller.args[0], "lstTypeOfIncome1"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [3, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lstTypeOfIncome1"), extendConfig({
            "hoverSkin": "sknlstbxcursor",
            "multiSelect": false
        }, controller.args[2], "lstTypeOfIncome1"));
        flxTypeOfIncome1.add(lblTypeOfIncome1, lstTypeOfIncome1);
        var flxGrossIncome1 = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
            "clipBounds": true,
            "id": "flxGrossIncome1",
            "isVisible": true,
            "layoutType": kony.flex.FLOW_VERTICAL,
            "left": "2%",
            "isModalContainer": false,
            "skin": "slFbox",
            "top": "0",
            "width": "32%",
            "zIndex": 1
        }, controller.args[0], "flxGrossIncome1"), extendConfig({}, controller.args[1], "flxGrossIncome1"), extendConfig({}, controller.args[2], "flxGrossIncome1"));
        flxGrossIncome1.setDefaultUnit(kony.flex.DP);
        var lblGrossIncome1 = new kony.ui.Label(extendConfig({
            "id": "lblGrossIncome1",
            "isVisible": true,
            "left": "0px",
            "skin": "sknLblLatoReg78818912px",
            "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmTrackApplication.GrossIncome\")",
            "top": "0px",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, controller.args[0], "lblGrossIncome1"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblGrossIncome1"), extendConfig({}, controller.args[2], "lblGrossIncome1"));
        var flxGrossIncomeAmount1 = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "clipBounds": true,
            "height": "40dp",
            "id": "flxGrossIncomeAmount1",
            "isVisible": true,
            "layoutType": kony.flex.FLOW_HORIZONTAL,
            "left": "0dp",
            "isModalContainer": false,
            "skin": "slFbox",
            "top": "15dp",
            "width": "100%",
            "zIndex": 1
        }, controller.args[0], "flxGrossIncomeAmount1"), extendConfig({}, controller.args[1], "flxGrossIncomeAmount1"), extendConfig({}, controller.args[2], "flxGrossIncomeAmount1"));
        flxGrossIncomeAmount1.setDefaultUnit(kony.flex.DP);
        var lblCurrency1 = new kony.ui.Label(extendConfig({
            "height": "100%",
            "id": "lblCurrency1",
            "isVisible": true,
            "left": "0",
            "skin": "sknLble1e5ed100PerBg12PxB2BDCB1R",
            "i18n_text": "kony.i18n.getLocalizedString(\"i18n.Applications.DollarSymbol\")",
            "top": "0",
            "width": "20%",
            "zIndex": 10
        }, controller.args[0], "lblCurrency1"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_CENTER,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblCurrency1"), extendConfig({}, controller.args[2], "lblCurrency1"));
        var txtGrossIncomeValue1 = new kony.ui.TextBox2(extendConfig({
            "autoCapitalize": constants.TEXTBOX_AUTO_CAPITALIZE_NONE,
            "height": "100%",
            "id": "txtGrossIncomeValue1",
            "isVisible": true,
            "keyBoardStyle": constants.TEXTBOX_KEY_BOARD_STYLE_PHONE_PAD,
            "left": "0",
            "i18n_placeholder": "kony.i18n.getLocalizedString(\"i18n.frmLogs.blSearchParam3\")",
            "secureTextEntry": false,
            "skin": "skntxtbxDetails0bbf1235271384a",
            "textInputMode": constants.TEXTBOX_INPUT_MODE_NUMERIC,
            "top": "0",
            "width": "79%",
            "zIndex": 1
        }, controller.args[0], "txtGrossIncomeValue1"), extendConfig({
            "containerHeightMode": constants.TEXTBOX_FONT_METRICS_DRIVEN_HEIGHT,
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [4, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "txtGrossIncomeValue1"), extendConfig({
            "autoCorrect": false,
            "placeholderSkin": "defTextBoxPlaceholder"
        }, controller.args[2], "txtGrossIncomeValue1"));
        flxGrossIncomeAmount1.add(lblCurrency1, txtGrossIncomeValue1);
        var grossIncomeError1Msg = new com.adminConsole.loans.applications.errorMsg(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
            "id": "grossIncomeError1Msg",
            "isVisible": false,
            "layoutType": kony.flex.FLOW_HORIZONTAL,
            "left": "0dp",
            "masterType": constants.MASTER_TYPE_DEFAULT,
            "isModalContainer": false,
            "skin": "slFbox",
            "top": "10dp",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "overrides": {
                "errorMsg": {
                    "isVisible": false
                }
            }
        }, controller.args[0], "grossIncomeError1Msg"), extendConfig({
            "overrides": {}
        }, controller.args[1], "grossIncomeError1Msg"), extendConfig({
            "overrides": {}
        }, controller.args[2], "grossIncomeError1Msg"));
        flxGrossIncome1.add(lblGrossIncome1, flxGrossIncomeAmount1, grossIncomeError1Msg);
        var flxPayPeriod1 = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
            "bottom": "0px",
            "clipBounds": true,
            "id": "flxPayPeriod1",
            "isVisible": true,
            "layoutType": kony.flex.FLOW_VERTICAL,
            "left": "2%",
            "isModalContainer": false,
            "skin": "slFbox",
            "top": "0dp",
            "width": "32%",
            "zIndex": 1
        }, controller.args[0], "flxPayPeriod1"), extendConfig({}, controller.args[1], "flxPayPeriod1"), extendConfig({}, controller.args[2], "flxPayPeriod1"));
        flxPayPeriod1.setDefaultUnit(kony.flex.DP);
        var lblPayPeriod1 = new kony.ui.Label(extendConfig({
            "id": "lblPayPeriod1",
            "isVisible": true,
            "left": "0px",
            "skin": "sknLblLatoReg78818912px",
            "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmTrackApplication.PayPeriod\")",
            "top": "0px",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, controller.args[0], "lblPayPeriod1"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblPayPeriod1"), extendConfig({}, controller.args[2], "lblPayPeriod1"));
        var lstPayPeriodValue1 = new kony.ui.ListBox(extendConfig({
            "height": "40px",
            "id": "lstPayPeriodValue1",
            "isVisible": true,
            "left": "0px",
            "skin": "sknlbxBgffffffBorderc1c9ceRadius3Px",
            "top": "15dp",
            "width": "100%",
            "zIndex": 1
        }, controller.args[0], "lstPayPeriodValue1"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [3, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lstPayPeriodValue1"), extendConfig({
            "hoverSkin": "sknlstbxcursor",
            "multiSelect": false
        }, controller.args[2], "lstPayPeriodValue1"));
        flxPayPeriod1.add(lblPayPeriod1, lstPayPeriodValue1);
        flxIncome1.add(flxTypeOfIncome1, flxGrossIncome1, flxPayPeriod1);
        flxIncomeDtls1.add(flxIncome1);
        var flxDescription1 = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
            "clipBounds": true,
            "id": "flxDescription1",
            "isVisible": true,
            "layoutType": kony.flex.FLOW_VERTICAL,
            "left": "0",
            "isModalContainer": false,
            "skin": "slFbox",
            "top": "30dp",
            "width": "100%",
            "zIndex": 1
        }, controller.args[0], "flxDescription1"), extendConfig({}, controller.args[1], "flxDescription1"), extendConfig({}, controller.args[2], "flxDescription1"));
        flxDescription1.setDefaultUnit(kony.flex.DP);
        var flxDescriptionLabel1 = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
            "clipBounds": true,
            "id": "flxDescriptionLabel1",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "left": "0dp",
            "isModalContainer": false,
            "skin": "slFbox",
            "top": "0dp",
            "width": "100%"
        }, controller.args[0], "flxDescriptionLabel1"), extendConfig({}, controller.args[1], "flxDescriptionLabel1"), extendConfig({}, controller.args[2], "flxDescriptionLabel1"));
        flxDescriptionLabel1.setDefaultUnit(kony.flex.DP);
        var lblDescription1 = new kony.ui.Label(extendConfig({
            "id": "lblDescription1",
            "isVisible": true,
            "left": "0px",
            "skin": "sknLblLatoReg78818912px",
            "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmTrackApplication.Description\")",
            "top": "0px",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, controller.args[0], "lblDescription1"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblDescription1"), extendConfig({}, controller.args[2], "lblDescription1"));
        var lblDescriptionSize1 = new kony.ui.Label(extendConfig({
            "id": "lblDescriptionSize1",
            "isVisible": false,
            "right": "5dp",
            "skin": "sknLblLatoReg78818912px",
            "text": "0/200",
            "top": "0dp",
            "width": "50dp",
            "zIndex": 1
        }, controller.args[0], "lblDescriptionSize1"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_TOP_RIGHT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblDescriptionSize1"), extendConfig({}, controller.args[2], "lblDescriptionSize1"));
        flxDescriptionLabel1.add(lblDescription1, lblDescriptionSize1);
        var txtDescription1 = new kony.ui.TextArea2(extendConfig({
            "autoCapitalize": constants.TEXTAREA_AUTO_CAPITALIZE_NONE,
            "focusSkin": "defTextAreaFocus",
            "height": "60dp",
            "id": "txtDescription1",
            "isVisible": true,
            "keyBoardStyle": constants.TEXTAREA_KEY_BOARD_STYLE_DEFAULT,
            "left": "0",
            "maxTextLength": 200,
            "numberOfVisibleLines": 3,
            "placeholder": "Description",
            "skin": "txtArea485c7513px",
            "textInputMode": constants.TEXTAREA_INPUT_MODE_ANY,
            "top": "15dp",
            "width": "100%",
            "zIndex": 1
        }, controller.args[0], "txtDescription1"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_TOP_LEFT,
            "padding": [1, 1, 1, 1],
            "paddingInPixel": false
        }, controller.args[1], "txtDescription1"), extendConfig({
            "autoCorrect": false,
            "placeholderSkin": "defTextAreaPlaceholder"
        }, controller.args[2], "txtDescription1"));
        flxDescription1.add(flxDescriptionLabel1, txtDescription1);
        flxIncomeGrp1.add(flxIncomeDtls1, flxDescription1);
        var flxIncomeGrp2 = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
            "clipBounds": true,
            "id": "flxIncomeGrp2",
            "isVisible": true,
            "layoutType": kony.flex.FLOW_VERTICAL,
            "left": "0dp",
            "isModalContainer": false,
            "skin": "slFbox",
            "top": "0dp",
            "width": "100%"
        }, controller.args[0], "flxIncomeGrp2"), extendConfig({}, controller.args[1], "flxIncomeGrp2"), extendConfig({}, controller.args[2], "flxIncomeGrp2"));
        flxIncomeGrp2.setDefaultUnit(kony.flex.DP);
        var flxIncomeDtls2 = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
            "clipBounds": true,
            "id": "flxIncomeDtls2",
            "isVisible": true,
            "layoutType": kony.flex.FLOW_VERTICAL,
            "left": "0",
            "isModalContainer": false,
            "skin": "slFbox",
            "top": "30dp",
            "width": "100%",
            "zIndex": 1
        }, controller.args[0], "flxIncomeDtls2"), extendConfig({}, controller.args[1], "flxIncomeDtls2"), extendConfig({}, controller.args[2], "flxIncomeDtls2"));
        flxIncomeDtls2.setDefaultUnit(kony.flex.DP);
        var flxIncome2 = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
            "clipBounds": true,
            "id": "flxIncome2",
            "isVisible": true,
            "layoutType": kony.flex.FLOW_HORIZONTAL,
            "left": "0dp",
            "isModalContainer": false,
            "skin": "slFbox",
            "top": "0dp",
            "width": "100%"
        }, controller.args[0], "flxIncome2"), extendConfig({}, controller.args[1], "flxIncome2"), extendConfig({}, controller.args[2], "flxIncome2"));
        flxIncome2.setDefaultUnit(kony.flex.DP);
        var flxTypeOfIncome2 = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
            "bottom": "0px",
            "clipBounds": true,
            "id": "flxTypeOfIncome2",
            "isVisible": true,
            "layoutType": kony.flex.FLOW_VERTICAL,
            "left": "0",
            "isModalContainer": false,
            "skin": "slFbox",
            "top": "0dp",
            "width": "32%",
            "zIndex": 1
        }, controller.args[0], "flxTypeOfIncome2"), extendConfig({}, controller.args[1], "flxTypeOfIncome2"), extendConfig({}, controller.args[2], "flxTypeOfIncome2"));
        flxTypeOfIncome2.setDefaultUnit(kony.flex.DP);
        var lblTypeOfIncome2 = new kony.ui.Label(extendConfig({
            "id": "lblTypeOfIncome2",
            "isVisible": true,
            "left": "0px",
            "skin": "sknLblLatoReg78818912px",
            "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmTrackApplication.TypeOfIncome\")",
            "top": "0px",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, controller.args[0], "lblTypeOfIncome2"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblTypeOfIncome2"), extendConfig({}, controller.args[2], "lblTypeOfIncome2"));
        var lstTypeOfIncome2 = new kony.ui.ListBox(extendConfig({
            "height": "40px",
            "id": "lstTypeOfIncome2",
            "isVisible": true,
            "left": "0px",
            "skin": "sknlbxBgffffffBorderc1c9ceRadius3Px",
            "top": "15dp",
            "width": "100%",
            "zIndex": 1
        }, controller.args[0], "lstTypeOfIncome2"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [3, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lstTypeOfIncome2"), extendConfig({
            "hoverSkin": "sknlstbxcursor",
            "multiSelect": false
        }, controller.args[2], "lstTypeOfIncome2"));
        flxTypeOfIncome2.add(lblTypeOfIncome2, lstTypeOfIncome2);
        var flxGrossIncome2 = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
            "clipBounds": true,
            "id": "flxGrossIncome2",
            "isVisible": true,
            "layoutType": kony.flex.FLOW_VERTICAL,
            "left": "2%",
            "isModalContainer": false,
            "skin": "slFbox",
            "top": "0",
            "width": "32%",
            "zIndex": 1
        }, controller.args[0], "flxGrossIncome2"), extendConfig({}, controller.args[1], "flxGrossIncome2"), extendConfig({}, controller.args[2], "flxGrossIncome2"));
        flxGrossIncome2.setDefaultUnit(kony.flex.DP);
        var lblGrossIncome2 = new kony.ui.Label(extendConfig({
            "id": "lblGrossIncome2",
            "isVisible": true,
            "left": "0px",
            "skin": "sknLblLatoReg78818912px",
            "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmTrackApplication.GrossIncome\")",
            "top": "0px",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, controller.args[0], "lblGrossIncome2"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblGrossIncome2"), extendConfig({}, controller.args[2], "lblGrossIncome2"));
        var flxGrossIncomeAmount2 = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "clipBounds": true,
            "height": "40dp",
            "id": "flxGrossIncomeAmount2",
            "isVisible": true,
            "layoutType": kony.flex.FLOW_HORIZONTAL,
            "left": "0dp",
            "isModalContainer": false,
            "skin": "slFbox",
            "top": "15dp",
            "width": "100%",
            "zIndex": 1
        }, controller.args[0], "flxGrossIncomeAmount2"), extendConfig({}, controller.args[1], "flxGrossIncomeAmount2"), extendConfig({}, controller.args[2], "flxGrossIncomeAmount2"));
        flxGrossIncomeAmount2.setDefaultUnit(kony.flex.DP);
        var lblCurrency2 = new kony.ui.Label(extendConfig({
            "height": "100%",
            "id": "lblCurrency2",
            "isVisible": true,
            "left": "0",
            "skin": "sknLble1e5ed100PerBg12PxB2BDCB1R",
            "i18n_text": "kony.i18n.getLocalizedString(\"i18n.Applications.DollarSymbol\")",
            "top": "0",
            "width": "20%",
            "zIndex": 10
        }, controller.args[0], "lblCurrency2"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_CENTER,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblCurrency2"), extendConfig({}, controller.args[2], "lblCurrency2"));
        var txtGrossIncomeValue2 = new kony.ui.TextBox2(extendConfig({
            "autoCapitalize": constants.TEXTBOX_AUTO_CAPITALIZE_NONE,
            "height": "100%",
            "id": "txtGrossIncomeValue2",
            "isVisible": true,
            "keyBoardStyle": constants.TEXTBOX_KEY_BOARD_STYLE_PHONE_PAD,
            "left": "0",
            "i18n_placeholder": "kony.i18n.getLocalizedString(\"i18n.frmLogs.blSearchParam3\")",
            "secureTextEntry": false,
            "skin": "skntxtbxDetails0bbf1235271384a",
            "textInputMode": constants.TEXTBOX_INPUT_MODE_NUMERIC,
            "top": "0",
            "width": "79%",
            "zIndex": 1
        }, controller.args[0], "txtGrossIncomeValue2"), extendConfig({
            "containerHeightMode": constants.TEXTBOX_FONT_METRICS_DRIVEN_HEIGHT,
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [4, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "txtGrossIncomeValue2"), extendConfig({
            "autoCorrect": false,
            "placeholderSkin": "defTextBoxPlaceholder"
        }, controller.args[2], "txtGrossIncomeValue2"));
        flxGrossIncomeAmount2.add(lblCurrency2, txtGrossIncomeValue2);
        var grossIncomeError2Msg = new com.adminConsole.loans.applications.errorMsg(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
            "id": "grossIncomeError2Msg",
            "isVisible": false,
            "layoutType": kony.flex.FLOW_HORIZONTAL,
            "left": "0dp",
            "masterType": constants.MASTER_TYPE_DEFAULT,
            "isModalContainer": false,
            "skin": "slFbox",
            "top": "10dp",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "overrides": {
                "errorMsg": {
                    "isVisible": false
                }
            }
        }, controller.args[0], "grossIncomeError2Msg"), extendConfig({
            "overrides": {}
        }, controller.args[1], "grossIncomeError2Msg"), extendConfig({
            "overrides": {}
        }, controller.args[2], "grossIncomeError2Msg"));
        flxGrossIncome2.add(lblGrossIncome2, flxGrossIncomeAmount2, grossIncomeError2Msg);
        var flxPayPeriod2 = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
            "bottom": "0px",
            "clipBounds": true,
            "id": "flxPayPeriod2",
            "isVisible": true,
            "layoutType": kony.flex.FLOW_VERTICAL,
            "left": "2%",
            "isModalContainer": false,
            "skin": "slFbox",
            "top": "0dp",
            "width": "32%",
            "zIndex": 1
        }, controller.args[0], "flxPayPeriod2"), extendConfig({}, controller.args[1], "flxPayPeriod2"), extendConfig({}, controller.args[2], "flxPayPeriod2"));
        flxPayPeriod2.setDefaultUnit(kony.flex.DP);
        var lblPayPeriod2 = new kony.ui.Label(extendConfig({
            "id": "lblPayPeriod2",
            "isVisible": true,
            "left": "0px",
            "skin": "sknLblLatoReg78818912px",
            "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmTrackApplication.PayPeriod\")",
            "top": "0px",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, controller.args[0], "lblPayPeriod2"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblPayPeriod2"), extendConfig({}, controller.args[2], "lblPayPeriod2"));
        var lstPayPeriodValue2 = new kony.ui.ListBox(extendConfig({
            "height": "40px",
            "id": "lstPayPeriodValue2",
            "isVisible": true,
            "left": "0px",
            "skin": "sknlbxBgffffffBorderc1c9ceRadius3Px",
            "top": "15dp",
            "width": "100%",
            "zIndex": 1
        }, controller.args[0], "lstPayPeriodValue2"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [3, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lstPayPeriodValue2"), extendConfig({
            "hoverSkin": "sknlstbxcursor",
            "multiSelect": false
        }, controller.args[2], "lstPayPeriodValue2"));
        flxPayPeriod2.add(lblPayPeriod2, lstPayPeriodValue2);
        flxIncome2.add(flxTypeOfIncome2, flxGrossIncome2, flxPayPeriod2);
        flxIncomeDtls2.add(flxIncome2);
        var flxDescription2 = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
            "clipBounds": true,
            "id": "flxDescription2",
            "isVisible": true,
            "layoutType": kony.flex.FLOW_VERTICAL,
            "left": "0",
            "isModalContainer": false,
            "skin": "slFbox",
            "top": "30dp",
            "width": "100%",
            "zIndex": 1
        }, controller.args[0], "flxDescription2"), extendConfig({}, controller.args[1], "flxDescription2"), extendConfig({}, controller.args[2], "flxDescription2"));
        flxDescription2.setDefaultUnit(kony.flex.DP);
        var flxDescriptionLabel2 = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
            "clipBounds": true,
            "id": "flxDescriptionLabel2",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "left": "0dp",
            "isModalContainer": false,
            "skin": "slFbox",
            "top": "0dp",
            "width": "100%"
        }, controller.args[0], "flxDescriptionLabel2"), extendConfig({}, controller.args[1], "flxDescriptionLabel2"), extendConfig({}, controller.args[2], "flxDescriptionLabel2"));
        flxDescriptionLabel2.setDefaultUnit(kony.flex.DP);
        var lblDescription2 = new kony.ui.Label(extendConfig({
            "id": "lblDescription2",
            "isVisible": true,
            "left": "0px",
            "skin": "sknLblLatoReg78818912px",
            "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmTrackApplication.Description\")",
            "top": "0px",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, controller.args[0], "lblDescription2"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblDescription2"), extendConfig({}, controller.args[2], "lblDescription2"));
        var lblDescriptionSize2 = new kony.ui.Label(extendConfig({
            "id": "lblDescriptionSize2",
            "isVisible": false,
            "right": "5dp",
            "skin": "sknLblLatoReg78818912px",
            "text": "0/200",
            "top": "0dp",
            "width": "50dp",
            "zIndex": 1
        }, controller.args[0], "lblDescriptionSize2"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_TOP_RIGHT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblDescriptionSize2"), extendConfig({}, controller.args[2], "lblDescriptionSize2"));
        flxDescriptionLabel2.add(lblDescription2, lblDescriptionSize2);
        var txtDescription2 = new kony.ui.TextArea2(extendConfig({
            "autoCapitalize": constants.TEXTAREA_AUTO_CAPITALIZE_NONE,
            "focusSkin": "defTextAreaFocus",
            "height": "60dp",
            "id": "txtDescription2",
            "isVisible": true,
            "keyBoardStyle": constants.TEXTAREA_KEY_BOARD_STYLE_DEFAULT,
            "left": "0",
            "maxTextLength": 200,
            "numberOfVisibleLines": 3,
            "placeholder": "Description",
            "skin": "txtArea485c7513px",
            "textInputMode": constants.TEXTAREA_INPUT_MODE_ANY,
            "top": "15dp",
            "width": "100%",
            "zIndex": 1
        }, controller.args[0], "txtDescription2"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_TOP_LEFT,
            "padding": [1, 1, 1, 1],
            "paddingInPixel": false
        }, controller.args[1], "txtDescription2"), extendConfig({
            "autoCorrect": false,
            "placeholderSkin": "defTextAreaPlaceholder"
        }, controller.args[2], "txtDescription2"));
        flxDescription2.add(flxDescriptionLabel2, txtDescription2);
        flxIncomeGrp2.add(flxIncomeDtls2, flxDescription2);
        flxIncomeSources.add(flxIncomeGrp1, flxIncomeGrp2);
        flxSection.add(flxIncomeSources);
        editAdditionalIncome.add(flxOtherSources, loansSectionHeader, flxSection);
        return editAdditionalIncome;
    }
})