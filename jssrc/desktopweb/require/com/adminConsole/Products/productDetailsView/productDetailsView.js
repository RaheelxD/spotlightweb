define(function() {
    return function(controller) {
        var productDetailsView = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
            "clipBounds": true,
            "isMaster": true,
            "id": "productDetailsView",
            "isVisible": true,
            "layoutType": kony.flex.FLOW_VERTICAL,
            "left": "0dp",
            "isModalContainer": false,
            "skin": "sknflxffffffop100Bordercbcdd1Radius3px",
            "top": "0dp",
            "width": "100%"
        }, controller.args[0], "productDetailsView"), extendConfig({}, controller.args[1], "productDetailsView"), extendConfig({}, controller.args[2], "productDetailsView"));
        productDetailsView.setDefaultUnit(kony.flex.DP);
        var flxViewContainer1 = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
            "clipBounds": true,
            "id": "flxViewContainer1",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "left": "0dp",
            "isModalContainer": false,
            "skin": "slFbox",
            "top": "0dp",
            "width": "100%",
            "zIndex": 1
        }, controller.args[0], "flxViewContainer1"), extendConfig({}, controller.args[1], "flxViewContainer1"), extendConfig({}, controller.args[2], "flxViewContainer1"));
        flxViewContainer1.setDefaultUnit(kony.flex.DP);
        var flxViewKeyValue1 = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "clipBounds": true,
            "height": "50dp",
            "id": "flxViewKeyValue1",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "left": "35dp",
            "isModalContainer": false,
            "skin": "slFbox",
            "top": "15dp",
            "width": "25%",
            "zIndex": 1
        }, controller.args[0], "flxViewKeyValue1"), extendConfig({}, controller.args[1], "flxViewKeyValue1"), extendConfig({}, controller.args[2], "flxViewKeyValue1"));
        flxViewKeyValue1.setDefaultUnit(kony.flex.DP);
        var lblViewKey1 = new kony.ui.Label(extendConfig({
            "id": "lblViewKey1",
            "isVisible": true,
            "left": "20dp",
            "skin": "sknlblLato5d6c7f12px",
            "text": "PRODUCT NAME",
            "top": "1dp",
            "width": "100px",
            "zIndex": 1
        }, controller.args[0], "lblViewKey1"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblViewKey1"), extendConfig({}, controller.args[2], "lblViewKey1"));
        var lblViewValue1 = new kony.ui.Label(extendConfig({
            "id": "lblViewValue1",
            "isVisible": true,
            "left": "0px",
            "skin": "sknlblLatoBold35475f14px",
            "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmGroups.lblViewNameValue\")",
            "top": "25px",
            "width": "100%",
            "zIndex": 1
        }, controller.args[0], "lblViewValue1"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblViewValue1"), extendConfig({}, controller.args[2], "lblViewValue1"));
        var fontIconImgViewKey1 = new kony.ui.Label(extendConfig({
            "id": "fontIconImgViewKey1",
            "isVisible": true,
            "left": "0dp",
            "skin": "sknfontIconViewScreen",
            "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmGroups.fontIconViewName\")",
            "top": "1dp",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, controller.args[0], "fontIconImgViewKey1"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "fontIconImgViewKey1"), extendConfig({}, controller.args[2], "fontIconImgViewKey1"));
        flxViewKeyValue1.add(lblViewKey1, lblViewValue1, fontIconImgViewKey1);
        var flxViewKeyValue2 = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "clipBounds": true,
            "height": "50dp",
            "id": "flxViewKeyValue2",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "left": "60%",
            "isModalContainer": false,
            "skin": "slFbox",
            "top": "15dp",
            "width": "25%",
            "zIndex": 1
        }, controller.args[0], "flxViewKeyValue2"), extendConfig({}, controller.args[1], "flxViewKeyValue2"), extendConfig({}, controller.args[2], "flxViewKeyValue2"));
        flxViewKeyValue2.setDefaultUnit(kony.flex.DP);
        var lblViewKey2 = new kony.ui.Label(extendConfig({
            "id": "lblViewKey2",
            "isVisible": true,
            "left": "20dp",
            "skin": "sknlblLato5d6c7f12px",
            "i18n_text": "kony.i18n.getLocalizedString(\"i18n.roles.STATUS\")",
            "top": "1dp",
            "width": "90px",
            "zIndex": 1
        }, controller.args[0], "lblViewKey2"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblViewKey2"), extendConfig({}, controller.args[2], "lblViewKey2"));
        var lblViewValue2 = new kony.ui.Label(extendConfig({
            "id": "lblViewValue2",
            "isVisible": true,
            "left": "20px",
            "skin": "sknlblLato5bc06cBold14px",
            "i18n_text": "kony.i18n.getLocalizedString(\"i18n.secureimage.Active\")",
            "top": "25px",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, controller.args[0], "lblViewValue2"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblViewValue2"), extendConfig({}, controller.args[2], "lblViewValue2"));
        var fontIconImgViewKey2 = new kony.ui.Label(extendConfig({
            "id": "fontIconImgViewKey2",
            "isVisible": true,
            "left": "0dp",
            "skin": "sknfontIconViewScreen",
            "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmGroups.fontIconViewStatus\")",
            "top": "1dp",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, controller.args[0], "fontIconImgViewKey2"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "fontIconImgViewKey2"), extendConfig({}, controller.args[2], "fontIconImgViewKey2"));
        var fontIconViewValue2 = new kony.ui.Label(extendConfig({
            "id": "fontIconViewValue2",
            "isVisible": true,
            "left": "5dp",
            "skin": "sknFontIconActivate",
            "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmAlertsManagement.lblViewAlertStatusKey\")",
            "top": "29dp",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, controller.args[0], "fontIconViewValue2"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "fontIconViewValue2"), extendConfig({}, controller.args[2], "fontIconViewValue2"));
        flxViewKeyValue2.add(lblViewKey2, lblViewValue2, fontIconImgViewKey2, fontIconViewValue2);
        var flxViewKeyValue3 = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "clipBounds": true,
            "height": "50dp",
            "id": "flxViewKeyValue3",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "left": "30%",
            "isModalContainer": false,
            "skin": "slFbox",
            "top": "15dp",
            "width": "25%",
            "zIndex": 1
        }, controller.args[0], "flxViewKeyValue3"), extendConfig({}, controller.args[1], "flxViewKeyValue3"), extendConfig({}, controller.args[2], "flxViewKeyValue3"));
        flxViewKeyValue3.setDefaultUnit(kony.flex.DP);
        var lblViewKey3 = new kony.ui.Label(extendConfig({
            "id": "lblViewKey3",
            "isVisible": true,
            "left": "20dp",
            "skin": "sknlblLato5d6c7f12px",
            "text": "PRODUCT TYPE",
            "top": "1dp",
            "width": "90px",
            "zIndex": 1
        }, controller.args[0], "lblViewKey3"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblViewKey3"), extendConfig({}, controller.args[2], "lblViewKey3"));
        var lblViewValue3 = new kony.ui.Label(extendConfig({
            "id": "lblViewValue3",
            "isVisible": true,
            "left": "0px",
            "skin": "sknlblLatoBold35475f14px",
            "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmGroups.lblViewValidTillValue\")",
            "top": "25px",
            "width": "100%",
            "zIndex": 1
        }, controller.args[0], "lblViewValue3"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblViewValue3"), extendConfig({}, controller.args[2], "lblViewValue3"));
        var fontIconImgViewKey3 = new kony.ui.Label(extendConfig({
            "id": "fontIconImgViewKey3",
            "isVisible": true,
            "left": "0dp",
            "skin": "sknfontIconViewScreen",
            "text": "",
            "top": "1dp",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, controller.args[0], "fontIconImgViewKey3"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "fontIconImgViewKey3"), extendConfig({}, controller.args[2], "fontIconImgViewKey3"));
        flxViewKeyValue3.add(lblViewKey3, lblViewValue3, fontIconImgViewKey3);
        var flxViewKeyValue4 = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "clipBounds": true,
            "height": "50dp",
            "id": "flxViewKeyValue4",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "left": "35dp",
            "isModalContainer": false,
            "skin": "slFbox",
            "top": "75dp",
            "width": "25%",
            "zIndex": 1
        }, controller.args[0], "flxViewKeyValue4"), extendConfig({}, controller.args[1], "flxViewKeyValue4"), extendConfig({}, controller.args[2], "flxViewKeyValue4"));
        flxViewKeyValue4.setDefaultUnit(kony.flex.DP);
        var lblViewKey4 = new kony.ui.Label(extendConfig({
            "id": "lblViewKey4",
            "isVisible": true,
            "left": "0dp",
            "skin": "sknlblLato5d6c7f12px",
            "text": "PRODUCT TYPE",
            "top": "1dp",
            "width": "90px",
            "zIndex": 1
        }, controller.args[0], "lblViewKey4"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblViewKey4"), extendConfig({}, controller.args[2], "lblViewKey4"));
        var lblViewValue4 = new kony.ui.Label(extendConfig({
            "id": "lblViewValue4",
            "isVisible": true,
            "left": "0px",
            "skin": "sknlblLatoBold35475f14px",
            "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmGroups.lblViewValidTillValue\")",
            "top": "25px",
            "width": "100%",
            "zIndex": 1
        }, controller.args[0], "lblViewValue4"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblViewValue4"), extendConfig({}, controller.args[2], "lblViewValue4"));
        flxViewKeyValue4.add(lblViewKey4, lblViewValue4);
        var flxViewEditButton = new kony.ui.Button(extendConfig({
            "bottom": "0px",
            "focusSkin": "sknbtnf7f7faLatoRegular12Px485c75Border1px485C75Radius20px",
            "height": "22px",
            "id": "flxViewEditButton",
            "isVisible": true,
            "right": "35px",
            "skin": "sknbtnf7f7faLatoRegular12Px485c75Border1px485C75Radius20px",
            "i18n_text": "kony.i18n.getLocalizedString(\"i18n.roles.Edit\")",
            "top": "15px",
            "width": "50px",
            "zIndex": 1
        }, controller.args[0], "flxViewEditButton"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_CENTER,
            "displayText": true,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "flxViewEditButton"), extendConfig({
            "hoverSkin": "sknbtnffffffLatoRegular12Px485c75Border1px485C75Radius20px"
        }, controller.args[2], "flxViewEditButton"));
        flxViewContainer1.add(flxViewKeyValue1, flxViewKeyValue2, flxViewKeyValue3, flxViewKeyValue4, flxViewEditButton);
        var flxViewProductFeatures = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
            "bottom": 20,
            "clipBounds": true,
            "id": "flxViewProductFeatures",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "left": "35px",
            "isModalContainer": false,
            "skin": "slFbox",
            "top": "10dp",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, controller.args[0], "flxViewProductFeatures"), extendConfig({}, controller.args[1], "flxViewProductFeatures"), extendConfig({
            "hoverSkin": "sknCursor"
        }, controller.args[2], "flxViewProductFeatures"));
        flxViewProductFeatures.setDefaultUnit(kony.flex.DP);
        var flxProductFeatures = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
            "clipBounds": true,
            "id": "flxProductFeatures",
            "isVisible": true,
            "layoutType": kony.flex.FLOW_HORIZONTAL,
            "left": "0dp",
            "isModalContainer": false,
            "skin": "slFbox",
            "top": "0dp",
            "width": "120dp"
        }, controller.args[0], "flxProductFeatures"), extendConfig({}, controller.args[1], "flxProductFeatures"), extendConfig({}, controller.args[2], "flxProductFeatures"));
        flxProductFeatures.setDefaultUnit(kony.flex.DP);
        var lblProductFeatures = new kony.ui.Label(extendConfig({
            "id": "lblProductFeatures",
            "isVisible": true,
            "left": "0dp",
            "skin": "sknlblLato5d6c7f12px",
            "text": "PRODUCT FEATURES",
            "top": "1dp",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, controller.args[0], "lblProductFeatures"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblProductFeatures"), extendConfig({}, controller.args[2], "lblProductFeatures"));
        flxProductFeatures.add(lblProductFeatures);
        var flxDropDown1 = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "clipBounds": true,
            "height": "15dp",
            "id": "flxDropDown1",
            "isVisible": true,
            "layoutType": kony.flex.FLOW_HORIZONTAL,
            "left": "125dp",
            "isModalContainer": false,
            "skin": "slFbox",
            "top": "2dp",
            "width": "20dp"
        }, controller.args[0], "flxDropDown1"), extendConfig({}, controller.args[1], "flxDropDown1"), extendConfig({}, controller.args[2], "flxDropDown1"));
        flxDropDown1.setDefaultUnit(kony.flex.DP);
        var fontIconImgView1 = new kony.ui.Label(extendConfig({
            "centerY": "50%",
            "id": "fontIconImgView1",
            "isVisible": true,
            "left": "0dp",
            "skin": "sknfontIconDescRightArrow14px",
            "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmLogs.lblBreadcrumbsRight\")",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, controller.args[0], "fontIconImgView1"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "fontIconImgView1"), extendConfig({}, controller.args[2], "fontIconImgView1"));
        flxDropDown1.add(fontIconImgView1);
        var flxViewSeperator1 = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "clipBounds": true,
            "height": "1px",
            "id": "flxViewSeperator1",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "left": "0dp",
            "isModalContainer": false,
            "skin": "sknflxD5D9DD",
            "top": "25dp",
            "width": "90%",
            "zIndex": 1
        }, controller.args[0], "flxViewSeperator1"), extendConfig({}, controller.args[1], "flxViewSeperator1"), extendConfig({}, controller.args[2], "flxViewSeperator1"));
        flxViewSeperator1.setDefaultUnit(kony.flex.DP);
        flxViewSeperator1.add();
        var flxRtxProductFeatures = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
            "clipBounds": true,
            "id": "flxRtxProductFeatures",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "left": "0dp",
            "isModalContainer": false,
            "skin": "slFbox",
            "top": "35dp",
            "width": "90%"
        }, controller.args[0], "flxRtxProductFeatures"), extendConfig({}, controller.args[1], "flxRtxProductFeatures"), extendConfig({}, controller.args[2], "flxRtxProductFeatures"));
        flxRtxProductFeatures.setDefaultUnit(kony.flex.DP);
        var rtxViewDescription = new kony.ui.RichText(extendConfig({
            "id": "rtxViewDescription",
            "isVisible": false,
            "left": "0dp",
            "skin": "sknrtxLato485c7514px",
            "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmPermissions.rtxViewDescription\")",
            "top": "0dp",
            "width": "100%",
            "zIndex": 1
        }, controller.args[0], "rtxViewDescription"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "rtxViewDescription"), extendConfig({}, controller.args[2], "rtxViewDescription"));
        flxRtxProductFeatures.add(rtxViewDescription);
        flxViewProductFeatures.add(flxProductFeatures, flxDropDown1, flxViewSeperator1, flxRtxProductFeatures);
        var flxViewChargesAndFee = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
            "bottom": 20,
            "clipBounds": true,
            "id": "flxViewChargesAndFee",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "left": "35px",
            "isModalContainer": false,
            "skin": "slFbox",
            "top": "10dp",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, controller.args[0], "flxViewChargesAndFee"), extendConfig({}, controller.args[1], "flxViewChargesAndFee"), extendConfig({
            "hoverSkin": "sknCursor"
        }, controller.args[2], "flxViewChargesAndFee"));
        flxViewChargesAndFee.setDefaultUnit(kony.flex.DP);
        var flxChargesAndFee = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "clipBounds": true,
            "height": "20dp",
            "id": "flxChargesAndFee",
            "isVisible": true,
            "layoutType": kony.flex.FLOW_HORIZONTAL,
            "left": "0dp",
            "isModalContainer": false,
            "skin": "slFbox",
            "top": "0dp",
            "width": "100%"
        }, controller.args[0], "flxChargesAndFee"), extendConfig({}, controller.args[1], "flxChargesAndFee"), extendConfig({}, controller.args[2], "flxChargesAndFee"));
        flxChargesAndFee.setDefaultUnit(kony.flex.DP);
        var lblChargesAndFees = new kony.ui.Label(extendConfig({
            "id": "lblChargesAndFees",
            "isVisible": true,
            "left": "0dp",
            "skin": "sknlblLato5d6c7f12px",
            "text": "SERVICE CHARGES AND OTHER FEES",
            "top": "1dp",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, controller.args[0], "lblChargesAndFees"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblChargesAndFees"), extendConfig({}, controller.args[2], "lblChargesAndFees"));
        flxChargesAndFee.add(lblChargesAndFees);
        var flxDropDown2 = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "clipBounds": true,
            "height": "15dp",
            "id": "flxDropDown2",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "left": "215dp",
            "isModalContainer": false,
            "skin": "slFbox",
            "top": "2dp",
            "width": "20dp"
        }, controller.args[0], "flxDropDown2"), extendConfig({}, controller.args[1], "flxDropDown2"), extendConfig({}, controller.args[2], "flxDropDown2"));
        flxDropDown2.setDefaultUnit(kony.flex.DP);
        var fontIconImgView2 = new kony.ui.Label(extendConfig({
            "centerY": "50%",
            "id": "fontIconImgView2",
            "isVisible": true,
            "left": "0dp",
            "skin": "sknfontIconDescRightArrow14px",
            "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmLogs.lblBreadcrumbsRight\")",
            "top": "0",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, controller.args[0], "fontIconImgView2"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "fontIconImgView2"), extendConfig({}, controller.args[2], "fontIconImgView2"));
        flxDropDown2.add(fontIconImgView2);
        var flxViewSeperator2 = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "clipBounds": true,
            "height": "1px",
            "id": "flxViewSeperator2",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "left": "0dp",
            "isModalContainer": false,
            "skin": "sknflxD5D9DD",
            "top": "25dp",
            "width": "90%",
            "zIndex": 1
        }, controller.args[0], "flxViewSeperator2"), extendConfig({}, controller.args[1], "flxViewSeperator2"), extendConfig({}, controller.args[2], "flxViewSeperator2"));
        flxViewSeperator2.setDefaultUnit(kony.flex.DP);
        flxViewSeperator2.add();
        var flxDisplayContent1 = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
            "clipBounds": true,
            "id": "flxDisplayContent1",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "left": "0dp",
            "isModalContainer": false,
            "skin": "sknServiceDetailsContent",
            "top": "35dp",
            "width": "90%",
            "zIndex": 1
        }, controller.args[0], "flxDisplayContent1"), extendConfig({}, controller.args[1], "flxDisplayContent1"), extendConfig({}, controller.args[2], "flxDisplayContent1"));
        flxDisplayContent1.setDefaultUnit(kony.flex.DP);
        var rtxChargesFee = new kony.ui.RichText(extendConfig({
            "id": "rtxChargesFee",
            "isVisible": false,
            "left": "0dp",
            "skin": "sknrtxLato485c7514px",
            "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmPermissions.rtxViewDescription\")",
            "top": "0dp",
            "width": "100%",
            "zIndex": 1
        }, controller.args[0], "rtxChargesFee"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "rtxChargesFee"), extendConfig({}, controller.args[2], "rtxChargesFee"));
        flxDisplayContent1.add(rtxChargesFee);
        flxViewChargesAndFee.add(flxChargesAndFee, flxDropDown2, flxViewSeperator2, flxDisplayContent1);
        var flxViewAddditionalInfo = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
            "bottom": 20,
            "clipBounds": true,
            "id": "flxViewAddditionalInfo",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "left": "35px",
            "isModalContainer": false,
            "skin": "slFbox",
            "top": "10dp",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, controller.args[0], "flxViewAddditionalInfo"), extendConfig({}, controller.args[1], "flxViewAddditionalInfo"), extendConfig({
            "hoverSkin": "sknCursor"
        }, controller.args[2], "flxViewAddditionalInfo"));
        flxViewAddditionalInfo.setDefaultUnit(kony.flex.DP);
        var flxAdditionalInfo = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "clipBounds": true,
            "height": "25dp",
            "id": "flxAdditionalInfo",
            "isVisible": true,
            "layoutType": kony.flex.FLOW_HORIZONTAL,
            "left": "0dp",
            "isModalContainer": false,
            "skin": "slFbox",
            "top": "0dp",
            "width": "110dp"
        }, controller.args[0], "flxAdditionalInfo"), extendConfig({}, controller.args[1], "flxAdditionalInfo"), extendConfig({}, controller.args[2], "flxAdditionalInfo"));
        flxAdditionalInfo.setDefaultUnit(kony.flex.DP);
        var lblAdditionalInfo = new kony.ui.Label(extendConfig({
            "id": "lblAdditionalInfo",
            "isVisible": true,
            "left": "0dp",
            "skin": "sknlblLato5d6c7f12px",
            "text": "ADDITIONAL INFO",
            "top": "1dp",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, controller.args[0], "lblAdditionalInfo"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblAdditionalInfo"), extendConfig({}, controller.args[2], "lblAdditionalInfo"));
        flxAdditionalInfo.add(lblAdditionalInfo);
        var flxDropDown3 = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "clipBounds": true,
            "height": "15dp",
            "id": "flxDropDown3",
            "isVisible": true,
            "layoutType": kony.flex.FLOW_HORIZONTAL,
            "left": "110dp",
            "isModalContainer": false,
            "skin": "slFbox",
            "top": "2dp",
            "width": "20dp"
        }, controller.args[0], "flxDropDown3"), extendConfig({}, controller.args[1], "flxDropDown3"), extendConfig({}, controller.args[2], "flxDropDown3"));
        flxDropDown3.setDefaultUnit(kony.flex.DP);
        var fontIconImgView3 = new kony.ui.Label(extendConfig({
            "centerY": "50%",
            "id": "fontIconImgView3",
            "isVisible": true,
            "left": "0dp",
            "skin": "sknfontIconDescRightArrow14px",
            "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmLogs.lblBreadcrumbsRight\")",
            "top": "1dp",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, controller.args[0], "fontIconImgView3"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "fontIconImgView3"), extendConfig({}, controller.args[2], "fontIconImgView3"));
        flxDropDown3.add(fontIconImgView3);
        var flxViewSeperator3 = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "clipBounds": true,
            "height": "1px",
            "id": "flxViewSeperator3",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "left": "0dp",
            "isModalContainer": false,
            "skin": "sknflxD5D9DD",
            "top": "25dp",
            "width": "90%",
            "zIndex": 1
        }, controller.args[0], "flxViewSeperator3"), extendConfig({}, controller.args[1], "flxViewSeperator3"), extendConfig({}, controller.args[2], "flxViewSeperator3"));
        flxViewSeperator3.setDefaultUnit(kony.flex.DP);
        flxViewSeperator3.add();
        var flxDisplayContent2 = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
            "clipBounds": true,
            "id": "flxDisplayContent2",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "left": "0dp",
            "isModalContainer": false,
            "skin": "sknServiceDetailsContent",
            "top": "35dp",
            "width": "90%",
            "zIndex": 1
        }, controller.args[0], "flxDisplayContent2"), extendConfig({}, controller.args[1], "flxDisplayContent2"), extendConfig({}, controller.args[2], "flxDisplayContent2"));
        flxDisplayContent2.setDefaultUnit(kony.flex.DP);
        var rtxAdditionalInfo = new kony.ui.RichText(extendConfig({
            "id": "rtxAdditionalInfo",
            "isVisible": false,
            "left": "0dp",
            "skin": "sknrtxLato485c7514px",
            "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmPermissions.rtxViewDescription\")",
            "top": "0dp",
            "width": "100%",
            "zIndex": 1
        }, controller.args[0], "rtxAdditionalInfo"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "rtxAdditionalInfo"), extendConfig({}, controller.args[2], "rtxAdditionalInfo"));
        flxDisplayContent2.add(rtxAdditionalInfo);
        flxViewAddditionalInfo.add(flxAdditionalInfo, flxDropDown3, flxViewSeperator3, flxDisplayContent2);
        productDetailsView.add(flxViewContainer1, flxViewProductFeatures, flxViewChargesAndFee, flxViewAddditionalInfo);
        return productDetailsView;
    }
})