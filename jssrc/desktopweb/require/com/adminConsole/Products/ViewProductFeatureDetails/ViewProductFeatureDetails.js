define(function() {
    return function(controller) {
        var ViewProductFeatureDetails = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
            "clipBounds": true,
            "isMaster": true,
            "id": "ViewProductFeatureDetails",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "left": "0dp",
            "isModalContainer": false,
            "preShow": function(eventobject) {
                controller.AS_FlexContainer_abf4233351fb4d0e859e0acf28338865(eventobject);
            },
            "skin": "slFbox",
            "top": "0dp",
            "width": "100%"
        }, controller.args[0], "ViewProductFeatureDetails"), extendConfig({}, controller.args[1], "ViewProductFeatureDetails"), extendConfig({}, controller.args[2], "ViewProductFeatureDetails"));
        ViewProductFeatureDetails.setDefaultUnit(kony.flex.DP);
        var flxProductFeatureDetails = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
            "clipBounds": true,
            "id": "flxProductFeatureDetails",
            "isVisible": true,
            "layoutType": kony.flex.FLOW_VERTICAL,
            "left": "0dp",
            "isModalContainer": false,
            "skin": "sknFlxbgFFFFFFbdrD7D9E0rd3px",
            "top": "0dp",
            "width": "100%"
        }, controller.args[0], "flxProductFeatureDetails"), extendConfig({}, controller.args[1], "flxProductFeatureDetails"), extendConfig({}, controller.args[2], "flxProductFeatureDetails"));
        flxProductFeatureDetails.setDefaultUnit(kony.flex.DP);
        var flxCollapse = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "clipBounds": true,
            "height": "60dp",
            "id": "flxCollapse",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "left": "0dp",
            "isModalContainer": false,
            "skin": "sknflxf5f6f8Op100",
            "top": "0dp",
            "width": "100%"
        }, controller.args[0], "flxCollapse"), extendConfig({}, controller.args[1], "flxCollapse"), extendConfig({}, controller.args[2], "flxCollapse"));
        flxCollapse.setDefaultUnit(kony.flex.DP);
        var flxDropdown = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "clipBounds": true,
            "height": "14px",
            "id": "flxDropdown",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "left": "10px",
            "isModalContainer": false,
            "onClick": controller.AS_FlexContainer_aeb8168ea3624740bf06d583316c5596,
            "skin": "sknCursor",
            "top": "17px",
            "width": "14px",
            "zIndex": 2
        }, controller.args[0], "flxDropdown"), extendConfig({}, controller.args[1], "flxDropdown"), extendConfig({}, controller.args[2], "flxDropdown"));
        flxDropdown.setDefaultUnit(kony.flex.DP);
        var fonticonArrow = new kony.ui.Label(extendConfig({
            "centerX": "50%",
            "centerY": "50%",
            "height": "14dp",
            "id": "fonticonArrow",
            "isVisible": true,
            "skin": "sknfontIconDescRightArrow14px",
            "text": "",
            "width": "14dp",
            "zIndex": 1
        }, controller.args[0], "fonticonArrow"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "fonticonArrow"), extendConfig({}, controller.args[2], "fonticonArrow"));
        flxDropdown.add(fonticonArrow);
        var lblFeatureName = new kony.ui.Label(extendConfig({
            "id": "lblFeatureName",
            "isVisible": true,
            "left": "30dp",
            "skin": "sknLbl192B45LatoRegular14px",
            "text": "Feature 2",
            "top": "15px",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, controller.args[0], "lblFeatureName"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblFeatureName"), extendConfig({}, controller.args[2], "lblFeatureName"));
        var lblFeatureTypeHeader = new kony.ui.Label(extendConfig({
            "id": "lblFeatureTypeHeader",
            "isVisible": true,
            "left": "30px",
            "skin": "sknLbl485C75LatoRegular13Px",
            "text": "Type:",
            "top": "34px",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, controller.args[0], "lblFeatureTypeHeader"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblFeatureTypeHeader"), extendConfig({}, controller.args[2], "lblFeatureTypeHeader"));
        var lblFeatureType = new kony.ui.Label(extendConfig({
            "id": "lblFeatureType",
            "isVisible": true,
            "left": "72px",
            "skin": "sknLbl192b45LatoReg13px",
            "text": "Overdraft Protection",
            "top": "34px",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, controller.args[0], "lblFeatureType"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblFeatureType"), extendConfig({}, controller.args[2], "lblFeatureType"));
        flxCollapse.add(flxDropdown, lblFeatureName, lblFeatureTypeHeader, lblFeatureType);
        var flxExpand = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
            "clipBounds": true,
            "id": "flxExpand",
            "isVisible": false,
            "layoutType": kony.flex.FLOW_VERTICAL,
            "left": "0dp",
            "isModalContainer": false,
            "skin": "slFbox",
            "top": "0dp",
            "width": "100%",
            "zIndex": 2
        }, controller.args[0], "flxExpand"), extendConfig({}, controller.args[1], "flxExpand"), extendConfig({}, controller.args[2], "flxExpand"));
        flxExpand.setDefaultUnit(kony.flex.DP);
        var flxFeatureDetails = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
            "clipBounds": true,
            "id": "flxFeatureDetails",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "left": "0dp",
            "isModalContainer": false,
            "skin": "slFbox",
            "top": "0dp",
            "width": "100%"
        }, controller.args[0], "flxFeatureDetails"), extendConfig({}, controller.args[1], "flxFeatureDetails"), extendConfig({}, controller.args[2], "flxFeatureDetails"));
        flxFeatureDetails.setDefaultUnit(kony.flex.DP);
        var lblSeparator = new kony.ui.Label(extendConfig({
            "centerX": "50%",
            "height": "1px",
            "id": "lblSeparator",
            "isVisible": true,
            "skin": "sknlblSeperatorD7D9E0",
            "text": ".",
            "top": "0px",
            "width": "100%",
            "zIndex": 1
        }, controller.args[0], "lblSeparator"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_BOTTOM_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblSeparator"), extendConfig({}, controller.args[2], "lblSeparator"));
        var lbFeatureGrpHeader = new kony.ui.Label(extendConfig({
            "id": "lbFeatureGrpHeader",
            "isVisible": true,
            "left": "20px",
            "skin": "sknlblLato696c7311px",
            "text": "FEATURE GROUP",
            "top": "18px",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, controller.args[0], "lbFeatureGrpHeader"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lbFeatureGrpHeader"), extendConfig({}, controller.args[2], "lbFeatureGrpHeader"));
        var lbFeatureGrp = new kony.ui.Label(extendConfig({
            "id": "lbFeatureGrp",
            "isVisible": true,
            "left": "20px",
            "skin": "sknlbl485C75LatoSemiBold13px",
            "text": "Bundles",
            "top": "38px",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, controller.args[0], "lbFeatureGrp"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lbFeatureGrp"), extendConfig({}, controller.args[2], "lbFeatureGrp"));
        var lblSequenceNumberHeader = new kony.ui.Label(extendConfig({
            "id": "lblSequenceNumberHeader",
            "isVisible": true,
            "left": "40%",
            "skin": "sknlblLato696c7311px",
            "text": "SEQUENCE NUMBER",
            "top": "18dp",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, controller.args[0], "lblSequenceNumberHeader"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblSequenceNumberHeader"), extendConfig({}, controller.args[2], "lblSequenceNumberHeader"));
        var lblSequenceNumber = new kony.ui.Label(extendConfig({
            "id": "lblSequenceNumber",
            "isVisible": true,
            "left": "40%",
            "skin": "sknlbl485C75LatoSemiBold13px",
            "text": "06",
            "top": "38dp",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, controller.args[0], "lblSequenceNumber"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblSequenceNumber"), extendConfig({}, controller.args[2], "lblSequenceNumber"));
        var lblMandatoryHeader = new kony.ui.Label(extendConfig({
            "id": "lblMandatoryHeader",
            "isVisible": true,
            "left": "75%",
            "skin": "sknlblLato696c7311px",
            "text": "MANDATORY",
            "top": "18dp",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, controller.args[0], "lblMandatoryHeader"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblMandatoryHeader"), extendConfig({}, controller.args[2], "lblMandatoryHeader"));
        var lblMandatory = new kony.ui.Label(extendConfig({
            "id": "lblMandatory",
            "isVisible": true,
            "left": "75%",
            "skin": "sknlbl485C75LatoSemiBold13px",
            "text": "true",
            "top": "38dp",
            "width": "130dp",
            "zIndex": 1
        }, controller.args[0], "lblMandatory"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblMandatory"), extendConfig({}, controller.args[2], "lblMandatory"));
        var lblDescHeader = new kony.ui.Label(extendConfig({
            "id": "lblDescHeader",
            "isVisible": true,
            "left": "20dp",
            "skin": "sknLbl78818A11px",
            "i18n_text": "kony.i18n.getLocalizedString(\"i18n.ConfigurationBundles.descriptionInCaps\")",
            "top": "75dp",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, controller.args[0], "lblDescHeader"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblDescHeader"), extendConfig({}, controller.args[2], "lblDescHeader"));
        var lblDescription = new kony.ui.Label(extendConfig({
            "bottom": "28dp",
            "id": "lblDescription",
            "isVisible": true,
            "left": "20dp",
            "right": "28dp",
            "skin": "sknlbl485C75LatoSemiBold13px",
            "text": "Promotion for Sapphire Credit Card, targeted  at all customers who live in Austin, TX Promotion for Sapphire Credit Card, targeted  at all customers who live in Austin, TX Promotion for Sapphire Credit Card, targeted  at all customers who live in Austin, TX Promotion for Sapphire Credit Card, targeted  at all customers who live in Austin, TX Promotion for Sapphire Credit Card, targeted  at all customers who live in Austin, TX",
            "top": "100dp",
            "zIndex": 1
        }, controller.args[0], "lblDescription"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblDescription"), extendConfig({}, controller.args[2], "lblDescription"));
        flxFeatureDetails.add(lblSeparator, lbFeatureGrpHeader, lbFeatureGrp, lblSequenceNumberHeader, lblSequenceNumber, lblMandatoryHeader, lblMandatory, lblDescHeader, lblDescription);
        var flxOptionDetails = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
            "clipBounds": true,
            "id": "flxOptionDetails",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "left": "0px",
            "isModalContainer": false,
            "skin": "slFbox",
            "top": "0px",
            "width": "100%",
            "zIndex": 1
        }, controller.args[0], "flxOptionDetails"), extendConfig({}, controller.args[1], "flxOptionDetails"), extendConfig({}, controller.args[2], "flxOptionDetails"));
        flxOptionDetails.setDefaultUnit(kony.flex.DP);
        var lblSeparatorOptionDetails = new kony.ui.Label(extendConfig({
            "height": "1px",
            "id": "lblSeparatorOptionDetails",
            "isVisible": true,
            "left": "20px",
            "right": "20px",
            "skin": "sknlblSeperatorD7D9E0",
            "text": ".",
            "top": "0px",
            "zIndex": 1
        }, controller.args[0], "lblSeparatorOptionDetails"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_BOTTOM_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblSeparatorOptionDetails"), extendConfig({}, controller.args[2], "lblSeparatorOptionDetails"));
        var lblOptionDetails = new kony.ui.Label(extendConfig({
            "id": "lblOptionDetails",
            "isVisible": true,
            "left": "20px",
            "skin": "sknLbl192b45LatoReg16px",
            "text": "Option Details",
            "top": "25px",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, controller.args[0], "lblOptionDetails"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblOptionDetails"), extendConfig({}, controller.args[2], "lblOptionDetails"));
        var lblOptionDisplayTypeHeader = new kony.ui.Label(extendConfig({
            "id": "lblOptionDisplayTypeHeader",
            "isVisible": true,
            "left": "20px",
            "skin": "sknlblLato696c7311px",
            "text": "OPTION DISPLAY TYPE",
            "top": "72px",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, controller.args[0], "lblOptionDisplayTypeHeader"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblOptionDisplayTypeHeader"), extendConfig({}, controller.args[2], "lblOptionDisplayTypeHeader"));
        var lblOptionDisplayType = new kony.ui.Label(extendConfig({
            "id": "lblOptionDisplayType",
            "isVisible": true,
            "left": "20px",
            "skin": "sknlbl485C75LatoSemiBold13px",
            "text": "Single Selection",
            "top": "95px",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, controller.args[0], "lblOptionDisplayType"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblOptionDisplayType"), extendConfig({}, controller.args[2], "lblOptionDisplayType"));
        var flxOptionValuesList = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
            "bottom": "20px",
            "clipBounds": true,
            "id": "flxOptionValuesList",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "left": "20px",
            "isModalContainer": false,
            "right": "20px",
            "skin": "sknFlxBgFFFFFFBorE4E6ECsz1pxR3px",
            "top": "135px",
            "zIndex": 1
        }, controller.args[0], "flxOptionValuesList"), extendConfig({}, controller.args[1], "flxOptionValuesList"), extendConfig({}, controller.args[2], "flxOptionValuesList"));
        flxOptionValuesList.setDefaultUnit(kony.flex.DP);
        var flxOptionValueHeaders = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
            "clipBounds": true,
            "id": "flxOptionValueHeaders",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "left": "0px",
            "isModalContainer": false,
            "right": "0px",
            "skin": "slFbox",
            "top": "0px",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, controller.args[0], "flxOptionValueHeaders"), extendConfig({}, controller.args[1], "flxOptionValueHeaders"), extendConfig({}, controller.args[2], "flxOptionValueHeaders"));
        flxOptionValueHeaders.setDefaultUnit(kony.flex.DP);
        var lblOptionValueHeader = new kony.ui.Label(extendConfig({
            "id": "lblOptionValueHeader",
            "isVisible": true,
            "left": "10px",
            "skin": "sknlblLato696c7311px",
            "text": "VALUE",
            "top": "16px",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, controller.args[0], "lblOptionValueHeader"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblOptionValueHeader"), extendConfig({}, controller.args[2], "lblOptionValueHeader"));
        var lblValueDescHeader = new kony.ui.Label(extendConfig({
            "id": "lblValueDescHeader",
            "isVisible": true,
            "left": "30%",
            "skin": "sknlblLato696c7311px",
            "text": "DESCRIPTION",
            "top": "16px",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, controller.args[0], "lblValueDescHeader"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblValueDescHeader"), extendConfig({}, controller.args[2], "lblValueDescHeader"));
        var lblSeparatorHeaderOptionValues = new kony.ui.Label(extendConfig({
            "height": "1px",
            "id": "lblSeparatorHeaderOptionValues",
            "isVisible": true,
            "left": "0px",
            "right": "0px",
            "skin": "sknlblSeperatorD7D9E0",
            "text": "Label",
            "top": "43px",
            "zIndex": 2
        }, controller.args[0], "lblSeparatorHeaderOptionValues"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_TOP_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblSeparatorHeaderOptionValues"), extendConfig({}, controller.args[2], "lblSeparatorHeaderOptionValues"));
        flxOptionValueHeaders.add(lblOptionValueHeader, lblValueDescHeader, lblSeparatorHeaderOptionValues);
        var segOptionValues = new kony.ui.SegmentedUI2(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
            "data": [{
                "lblDefault": "Deafult",
                "lblDescription": "Description",
                "lblSeparator": "Label",
                "lblValue": "Value"
            }, {
                "lblDefault": "Deafult",
                "lblDescription": "Description",
                "lblSeparator": "Label",
                "lblValue": "Value"
            }, {
                "lblDefault": "Deafult",
                "lblDescription": "Description",
                "lblSeparator": "Label",
                "lblValue": "Value"
            }],
            "groupCells": false,
            "id": "segOptionValues",
            "isVisible": false,
            "left": "0px",
            "needPageIndicator": true,
            "pageOffDotImage": "pageoffdot.png",
            "pageOnDotImage": "pageondot.png",
            "retainSelection": false,
            "right": "0px",
            "rowFocusSkin": "seg2Focus",
            "rowSkin": "sknseg3pxRadius",
            "rowTemplate": "flxOptionValue",
            "sectionHeaderSkin": "sliPhoneSegmentHeader",
            "selectionBehavior": constants.SEGUI_DEFAULT_BEHAVIOR,
            "separatorRequired": false,
            "showScrollbars": false,
            "top": "44px",
            "viewType": constants.SEGUI_VIEW_TYPE_TABLEVIEW,
            "widgetDataMap": {
                "flxOptionValue": "flxOptionValue",
                "flxValue": "flxValue",
                "lblDefault": "lblDefault",
                "lblDescription": "lblDescription",
                "lblSeparator": "lblSeparator",
                "lblValue": "lblValue"
            },
            "zIndex": 1
        }, controller.args[0], "segOptionValues"), extendConfig({
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "segOptionValues"), extendConfig({}, controller.args[2], "segOptionValues"));
        var flxNoResultFoundOptionValues = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "clipBounds": true,
            "height": "100px",
            "id": "flxNoResultFoundOptionValues",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "left": "0px",
            "isModalContainer": false,
            "skin": "slFbox",
            "top": "44px",
            "width": "100%",
            "zIndex": 1
        }, controller.args[0], "flxNoResultFoundOptionValues"), extendConfig({}, controller.args[1], "flxNoResultFoundOptionValues"), extendConfig({}, controller.args[2], "flxNoResultFoundOptionValues"));
        flxNoResultFoundOptionValues.setDefaultUnit(kony.flex.DP);
        var lblNoResultFoundOptionValues = new kony.ui.Label(extendConfig({
            "centerX": "50%",
            "centerY": "50%",
            "id": "lblNoResultFoundOptionValues",
            "isVisible": true,
            "skin": "sknLbl485C75LatoRegular13Px",
            "text": "No images found.",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, controller.args[0], "lblNoResultFoundOptionValues"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblNoResultFoundOptionValues"), extendConfig({}, controller.args[2], "lblNoResultFoundOptionValues"));
        flxNoResultFoundOptionValues.add(lblNoResultFoundOptionValues);
        flxOptionValuesList.add(flxOptionValueHeaders, segOptionValues, flxNoResultFoundOptionValues);
        flxOptionDetails.add(lblSeparatorOptionDetails, lblOptionDetails, lblOptionDisplayTypeHeader, lblOptionDisplayType, flxOptionValuesList);
        flxExpand.add(flxFeatureDetails, flxOptionDetails);
        flxProductFeatureDetails.add(flxCollapse, flxExpand);
        ViewProductFeatureDetails.add(flxProductFeatureDetails);
        return ViewProductFeatureDetails;
    }
})