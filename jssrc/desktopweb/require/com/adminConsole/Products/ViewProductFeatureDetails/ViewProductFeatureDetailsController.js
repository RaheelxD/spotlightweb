define("com/adminConsole/Products/ViewProductFeatureDetails/userViewProductFeatureDetailsController", function() {
    return {
        viewFeatureDetailsPreShow: function() {
            this.setFlowActions();
        },
        showFeatureDetails: function() {
            //setting Details visibility off and changing icon to collapse
            if (this.view.flxExpand.isVisible) {
                this.view.flxExpand.setVisibility(false);
                this.view.fonticonArrow.text = "\ue922";
            } else {
                this.view.flxExpand.setVisibility(true);
                this.view.fonticonArrow.text = "\ue915";
            }
        },
        setFlowActions: function() {
            var scopeObj = this;
            this.view.flxDropdown.onClick = function() {
                scopeObj.showFeatureDetails();
            };
        }
    };
});
define("com/adminConsole/Products/ViewProductFeatureDetails/ViewProductFeatureDetailsControllerActions", {
    /*
        This is an auto generated file and any modifications to it may result in corruption of the action sequence.
    */
    AS_FlexContainer_abf4233351fb4d0e859e0acf28338865: function AS_FlexContainer_abf4233351fb4d0e859e0acf28338865(eventobject) {
        var self = this;
        return self.viewFeatureDetailsPreShow.call(this);
    },
});
define("com/adminConsole/Products/ViewProductFeatureDetails/ViewProductFeatureDetailsController", ["com/adminConsole/Products/ViewProductFeatureDetails/userViewProductFeatureDetailsController", "com/adminConsole/Products/ViewProductFeatureDetails/ViewProductFeatureDetailsControllerActions"], function() {
    var controller = require("com/adminConsole/Products/ViewProductFeatureDetails/userViewProductFeatureDetailsController");
    var actions = require("com/adminConsole/Products/ViewProductFeatureDetails/ViewProductFeatureDetailsControllerActions");
    for (var key in actions) {
        controller[key] = actions[key];
    }
    return controller;
});
