define(function() {
    return function(controller) {
        var addProduct = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "clipBounds": true,
            "isMaster": true,
            "height": "100%",
            "id": "addProduct",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "left": "0dp",
            "isModalContainer": false,
            "preShow": function(eventobject) {
                controller.AS_FlexContainer_c585c715f06042fdb6b1537a934f34cb(eventobject);
            },
            "skin": "slFbox",
            "top": "0dp",
            "width": "100%"
        }, controller.args[0], "addProduct"), extendConfig({}, controller.args[1], "addProduct"), extendConfig({}, controller.args[2], "addProduct"));
        addProduct.setDefaultUnit(kony.flex.DP);
        var flxAddProductMainContainer = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "bottom": "0px",
            "clipBounds": true,
            "id": "flxAddProductMainContainer",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "left": "0px",
            "isModalContainer": false,
            "right": "0px",
            "skin": "slFbox",
            "top": "0px"
        }, controller.args[0], "flxAddProductMainContainer"), extendConfig({}, controller.args[1], "flxAddProductMainContainer"), extendConfig({}, controller.args[2], "flxAddProductMainContainer"));
        flxAddProductMainContainer.setDefaultUnit(kony.flex.DP);
        var flxVerticalTabs = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "clipBounds": true,
            "height": "100%",
            "id": "flxVerticalTabs",
            "isVisible": true,
            "layoutType": kony.flex.FLOW_VERTICAL,
            "left": "0px",
            "isModalContainer": false,
            "skin": "sknflxBgF9F9F9Border1pxE4E6EC",
            "top": "0px",
            "width": "240px",
            "zIndex": 1
        }, controller.args[0], "flxVerticalTabs"), extendConfig({}, controller.args[1], "flxVerticalTabs"), extendConfig({}, controller.args[2], "flxVerticalTabs"));
        flxVerticalTabs.setDefaultUnit(kony.flex.DP);
        var flxOption1Container = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
            "clipBounds": true,
            "id": "flxOption1Container",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "left": "0px",
            "isModalContainer": false,
            "skin": "slFbox",
            "top": "20px",
            "width": "100%",
            "zIndex": 1
        }, controller.args[0], "flxOption1Container"), extendConfig({}, controller.args[1], "flxOption1Container"), extendConfig({}, controller.args[2], "flxOption1Container"));
        flxOption1Container.setDefaultUnit(kony.flex.DP);
        var flxOption1Main = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "bottom": "15px",
            "clipBounds": true,
            "height": "14px",
            "id": "flxOption1Main",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "left": "20px",
            "isModalContainer": false,
            "right": "20px",
            "skin": "slFbox",
            "top": "0px",
            "zIndex": 1
        }, controller.args[0], "flxOption1Main"), extendConfig({}, controller.args[1], "flxOption1Main"), extendConfig({}, controller.args[2], "flxOption1Main"));
        flxOption1Main.setDefaultUnit(kony.flex.DP);
        var lblFontIconArrow = new kony.ui.Label(extendConfig({
            "id": "lblFontIconArrow",
            "isVisible": true,
            "left": "0px",
            "skin": "sknIcon485C7513px",
            "text": "",
            "top": "0px",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, controller.args[0], "lblFontIconArrow"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblFontIconArrow"), extendConfig({}, controller.args[2], "lblFontIconArrow"));
        var lblOption1 = new kony.ui.Label(extendConfig({
            "height": "14px",
            "id": "lblOption1",
            "isVisible": true,
            "left": "22px",
            "skin": "sknlblLatoBold485c7512px",
            "i18n_text": "kony.i18n.getLocalizedString(\"i18n.products.Product_Details_CAPS\")",
            "top": "0px",
            "width": "150px",
            "zIndex": 1
        }, controller.args[0], "lblOption1"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblOption1"), extendConfig({
            "hoverSkin": "sknlblLatoBoldCursorHover485c7512px"
        }, controller.args[2], "lblOption1"));
        flxOption1Main.add(lblFontIconArrow, lblOption1);
        var flxSubOptions = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "bottom": "15px",
            "clipBounds": true,
            "height": "47px",
            "id": "flxSubOptions",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "left": "40px",
            "isModalContainer": false,
            "right": "20px",
            "skin": "slFbox",
            "top": "30px",
            "zIndex": 1
        }, controller.args[0], "flxSubOptions"), extendConfig({}, controller.args[1], "flxSubOptions"), extendConfig({}, controller.args[2], "flxSubOptions"));
        flxSubOptions.setDefaultUnit(kony.flex.DP);
        var flxSubOption1 = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "clipBounds": true,
            "height": "16px",
            "id": "flxSubOption1",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "left": "0px",
            "isModalContainer": false,
            "skin": "slFbox",
            "top": "0px",
            "width": "100%",
            "zIndex": 1
        }, controller.args[0], "flxSubOption1"), extendConfig({}, controller.args[1], "flxSubOption1"), extendConfig({}, controller.args[2], "flxSubOption1"));
        flxSubOption1.setDefaultUnit(kony.flex.DP);
        var lblSubOption1 = new kony.ui.Label(extendConfig({
            "id": "lblSubOption1",
            "isVisible": true,
            "left": "0px",
            "skin": "sknlblLatoBold485c7512px",
            "i18n_text": "kony.i18n.getLocalizedString(\"i18n.products.Details\")",
            "top": "0px",
            "width": "80px",
            "zIndex": 1
        }, controller.args[0], "lblSubOption1"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblSubOption1"), extendConfig({
            "hoverSkin": "sknlblLatoBoldCursorHover485c7512px"
        }, controller.args[2], "lblSubOption1"));
        var lblFontIconRightArrow1 = new kony.ui.Label(extendConfig({
            "centerY": "50%",
            "height": "13px",
            "id": "lblFontIconRightArrow1",
            "isVisible": true,
            "right": "0px",
            "skin": "sknIcon485C7513px",
            "text": "",
            "top": "0px",
            "width": "13px",
            "zIndex": 1
        }, controller.args[0], "lblFontIconRightArrow1"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblFontIconRightArrow1"), extendConfig({}, controller.args[2], "lblFontIconRightArrow1"));
        flxSubOption1.add(lblSubOption1, lblFontIconRightArrow1);
        var flxSubOption2 = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "clipBounds": true,
            "height": "16px",
            "id": "flxSubOption2",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "left": "0px",
            "isModalContainer": false,
            "skin": "slFbox",
            "top": "31px",
            "width": "100%",
            "zIndex": 1
        }, controller.args[0], "flxSubOption2"), extendConfig({}, controller.args[1], "flxSubOption2"), extendConfig({}, controller.args[2], "flxSubOption2"));
        flxSubOption2.setDefaultUnit(kony.flex.DP);
        var lblSubOption2 = new kony.ui.Label(extendConfig({
            "id": "lblSubOption2",
            "isVisible": true,
            "left": "0px",
            "skin": "sknLbl485C75LatoRegular12Px",
            "i18n_text": "kony.i18n.getLocalizedString(\"i18n.products.Description\")",
            "top": "0px",
            "width": "80px",
            "zIndex": 1
        }, controller.args[0], "lblSubOption2"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblSubOption2"), extendConfig({
            "hoverSkin": "sknlblLatoBoldCursorHover485c7512px"
        }, controller.args[2], "lblSubOption2"));
        var lblFontIconRightArrow2 = new kony.ui.Label(extendConfig({
            "centerY": "50%",
            "height": "13px",
            "id": "lblFontIconRightArrow2",
            "isVisible": false,
            "right": "0px",
            "skin": "sknIcon485C7513px",
            "text": "",
            "top": "0px",
            "width": "13px",
            "zIndex": 1
        }, controller.args[0], "lblFontIconRightArrow2"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblFontIconRightArrow2"), extendConfig({}, controller.args[2], "lblFontIconRightArrow2"));
        flxSubOption2.add(lblSubOption2, lblFontIconRightArrow2);
        flxSubOptions.add(flxSubOption1, flxSubOption2);
        var flxTabSeparator1 = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "bottom": "0px",
            "clipBounds": true,
            "height": "1px",
            "id": "flxTabSeparator1",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "left": "20px",
            "isModalContainer": false,
            "right": "20px",
            "skin": "sknflxD5D9DD",
            "zIndex": 1
        }, controller.args[0], "flxTabSeparator1"), extendConfig({}, controller.args[1], "flxTabSeparator1"), extendConfig({}, controller.args[2], "flxTabSeparator1"));
        flxTabSeparator1.setDefaultUnit(kony.flex.DP);
        flxTabSeparator1.add();
        flxOption1Container.add(flxOption1Main, flxSubOptions, flxTabSeparator1);
        var flxOption2Container = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
            "clipBounds": true,
            "id": "flxOption2Container",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "left": "0px",
            "isModalContainer": false,
            "skin": "slFbox",
            "top": "15px",
            "width": "100%",
            "zIndex": 1
        }, controller.args[0], "flxOption2Container"), extendConfig({}, controller.args[1], "flxOption2Container"), extendConfig({}, controller.args[2], "flxOption2Container"));
        flxOption2Container.setDefaultUnit(kony.flex.DP);
        var flxOption2Main = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
            "bottom": "15px",
            "clipBounds": true,
            "id": "flxOption2Main",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "left": "20px",
            "isModalContainer": false,
            "right": "20px",
            "skin": "slFbox",
            "top": "0px",
            "zIndex": 1
        }, controller.args[0], "flxOption2Main"), extendConfig({}, controller.args[1], "flxOption2Main"), extendConfig({}, controller.args[2], "flxOption2Main"));
        flxOption2Main.setDefaultUnit(kony.flex.DP);
        var lblOption2 = new kony.ui.Label(extendConfig({
            "id": "lblOption2",
            "isVisible": true,
            "left": "0px",
            "skin": "sknLbl485C75LatoRegular12Px",
            "i18n_text": "kony.i18n.getLocalizedString(\"i18n.products.Product_Features_CAPS\")",
            "top": "0px",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, controller.args[0], "lblOption2"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblOption2"), extendConfig({
            "hoverSkin": "sknlblLatoBoldCursorHover485c7512px"
        }, controller.args[2], "lblOption2"));
        var lblFontIconRightArrow3 = new kony.ui.Label(extendConfig({
            "height": "13px",
            "id": "lblFontIconRightArrow3",
            "isVisible": false,
            "right": "0px",
            "skin": "sknIcon485C7513px",
            "text": "",
            "top": "0px",
            "width": "13px",
            "zIndex": 1
        }, controller.args[0], "lblFontIconRightArrow3"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblFontIconRightArrow3"), extendConfig({}, controller.args[2], "lblFontIconRightArrow3"));
        flxOption2Main.add(lblOption2, lblFontIconRightArrow3);
        var flxTabSeparator2 = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "bottom": "0px",
            "clipBounds": true,
            "height": "1px",
            "id": "flxTabSeparator2",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "left": "20px",
            "isModalContainer": false,
            "right": "20px",
            "skin": "sknflxD5D9DD",
            "zIndex": 1
        }, controller.args[0], "flxTabSeparator2"), extendConfig({}, controller.args[1], "flxTabSeparator2"), extendConfig({}, controller.args[2], "flxTabSeparator2"));
        flxTabSeparator2.setDefaultUnit(kony.flex.DP);
        flxTabSeparator2.add();
        flxOption2Container.add(flxOption2Main, flxTabSeparator2);
        var flxOption3Container = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
            "clipBounds": true,
            "id": "flxOption3Container",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "left": "0px",
            "isModalContainer": false,
            "skin": "slFbox",
            "top": "15px",
            "width": "100%",
            "zIndex": 1
        }, controller.args[0], "flxOption3Container"), extendConfig({}, controller.args[1], "flxOption3Container"), extendConfig({}, controller.args[2], "flxOption3Container"));
        flxOption3Container.setDefaultUnit(kony.flex.DP);
        var flxOption3Main = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
            "bottom": "15px",
            "clipBounds": true,
            "id": "flxOption3Main",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "left": "20px",
            "isModalContainer": false,
            "right": "20px",
            "skin": "slFbox",
            "top": "0px",
            "zIndex": 1
        }, controller.args[0], "flxOption3Main"), extendConfig({}, controller.args[1], "flxOption3Main"), extendConfig({}, controller.args[2], "flxOption3Main"));
        flxOption3Main.setDefaultUnit(kony.flex.DP);
        var lblOption3 = new kony.ui.Label(extendConfig({
            "id": "lblOption3",
            "isVisible": true,
            "left": "0px",
            "skin": "sknLbl485C75LatoRegular12Px",
            "i18n_text": "kony.i18n.getLocalizedString(\"i18n.products.Image_Details_CAPS\")",
            "top": "0px",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, controller.args[0], "lblOption3"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblOption3"), extendConfig({
            "hoverSkin": "sknlblLatoBoldCursorHover485c7512px"
        }, controller.args[2], "lblOption3"));
        var lblFontIconRightArrow4 = new kony.ui.Label(extendConfig({
            "height": "13px",
            "id": "lblFontIconRightArrow4",
            "isVisible": false,
            "right": "0px",
            "skin": "sknIcon485C7513px",
            "text": "",
            "top": "0px",
            "width": "13px",
            "zIndex": 1
        }, controller.args[0], "lblFontIconRightArrow4"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblFontIconRightArrow4"), extendConfig({}, controller.args[2], "lblFontIconRightArrow4"));
        flxOption3Main.add(lblOption3, lblFontIconRightArrow4);
        var flxTabSeparator3 = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "bottom": "0px",
            "clipBounds": true,
            "height": "1px",
            "id": "flxTabSeparator3",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "left": "20px",
            "isModalContainer": false,
            "right": "20px",
            "skin": "sknflxD5D9DD",
            "zIndex": 1
        }, controller.args[0], "flxTabSeparator3"), extendConfig({}, controller.args[1], "flxTabSeparator3"), extendConfig({}, controller.args[2], "flxTabSeparator3"));
        flxTabSeparator3.setDefaultUnit(kony.flex.DP);
        flxTabSeparator3.add();
        flxOption3Container.add(flxOption3Main, flxTabSeparator3);
        var flxOption4Container = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
            "clipBounds": true,
            "id": "flxOption4Container",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "left": "0px",
            "isModalContainer": false,
            "skin": "slFbox",
            "top": "15px",
            "width": "100%",
            "zIndex": 1
        }, controller.args[0], "flxOption4Container"), extendConfig({}, controller.args[1], "flxOption4Container"), extendConfig({}, controller.args[2], "flxOption4Container"));
        flxOption4Container.setDefaultUnit(kony.flex.DP);
        var flxOption4Main = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
            "bottom": "15px",
            "clipBounds": true,
            "id": "flxOption4Main",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "left": "20px",
            "isModalContainer": false,
            "right": "20px",
            "skin": "slFbox",
            "top": "0px",
            "zIndex": 1
        }, controller.args[0], "flxOption4Main"), extendConfig({}, controller.args[1], "flxOption4Main"), extendConfig({}, controller.args[2], "flxOption4Main"));
        flxOption4Main.setDefaultUnit(kony.flex.DP);
        var lblOption4 = new kony.ui.Label(extendConfig({
            "id": "lblOption4",
            "isVisible": true,
            "left": "0px",
            "skin": "sknLbl485C75LatoRegular12Px",
            "i18n_text": "kony.i18n.getLocalizedString(\"i18n.products.Additional_Attributes_CAPS\")",
            "top": "0px",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, controller.args[0], "lblOption4"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblOption4"), extendConfig({
            "hoverSkin": "sknlblLatoBoldCursorHover485c7512px"
        }, controller.args[2], "lblOption4"));
        var lblFontIconRightArrow5 = new kony.ui.Label(extendConfig({
            "height": "13px",
            "id": "lblFontIconRightArrow5",
            "isVisible": false,
            "right": "0px",
            "skin": "sknIcon485C7513px",
            "text": "",
            "top": "0px",
            "width": "13px",
            "zIndex": 1
        }, controller.args[0], "lblFontIconRightArrow5"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblFontIconRightArrow5"), extendConfig({}, controller.args[2], "lblFontIconRightArrow5"));
        var lblOptionalIndicator = new kony.ui.Label(extendConfig({
            "id": "lblOptionalIndicator",
            "isVisible": true,
            "left": "0px",
            "skin": "sknLblLato84939E12px",
            "i18n_text": "kony.i18n.getLocalizedString(\"i18n.products.Optional\")",
            "top": "25px",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, controller.args[0], "lblOptionalIndicator"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblOptionalIndicator"), extendConfig({}, controller.args[2], "lblOptionalIndicator"));
        flxOption4Main.add(lblOption4, lblFontIconRightArrow5, lblOptionalIndicator);
        flxOption4Container.add(flxOption4Main);
        flxVerticalTabs.add(flxOption1Container, flxOption2Container, flxOption3Container, flxOption4Container);
        var flxProductContainer = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "clipBounds": true,
            "height": "100%",
            "id": "flxProductContainer",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "left": "240px",
            "isModalContainer": false,
            "right": "0%",
            "skin": "slFbox",
            "top": "0%"
        }, controller.args[0], "flxProductContainer"), extendConfig({}, controller.args[1], "flxProductContainer"), extendConfig({}, controller.args[2], "flxProductContainer"));
        flxProductContainer.setDefaultUnit(kony.flex.DP);
        var flxAddProductMain = new kony.ui.FlexScrollContainer(extendConfig({
            "allowHorizontalBounce": false,
            "allowVerticalBounce": true,
            "bottom": "80px",
            "bounces": true,
            "clipBounds": true,
            "enableScrolling": true,
            "horizontalScrollIndicator": true,
            "id": "flxAddProductMain",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "left": "0%",
            "pagingEnabled": false,
            "right": "0%",
            "scrollDirection": kony.flex.SCROLL_VERTICAL,
            "skin": "sknFlxBgFFFFFFBore4e6ec3Px",
            "top": "0%",
            "verticalScrollIndicator": true,
            "zIndex": 1
        }, controller.args[0], "flxAddProductMain"), extendConfig({}, controller.args[1], "flxAddProductMain"), extendConfig({}, controller.args[2], "flxAddProductMain"));
        flxAddProductMain.setDefaultUnit(kony.flex.DP);
        var flxProductDetails = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
            "clipBounds": true,
            "id": "flxProductDetails",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "left": "20dp",
            "isModalContainer": false,
            "right": "20dp",
            "skin": "slFbox",
            "top": "20dp",
            "zIndex": 1
        }, controller.args[0], "flxProductDetails"), extendConfig({}, controller.args[1], "flxProductDetails"), extendConfig({}, controller.args[2], "flxProductDetails"));
        flxProductDetails.setDefaultUnit(kony.flex.DP);
        var flxProductDetails1 = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "clipBounds": true,
            "height": "100px",
            "id": "flxProductDetails1",
            "isVisible": true,
            "layoutType": kony.flex.FLOW_HORIZONTAL,
            "left": "0dp",
            "isModalContainer": false,
            "skin": "slFbox",
            "top": "0dp",
            "width": "100%",
            "zIndex": 1
        }, controller.args[0], "flxProductDetails1"), extendConfig({}, controller.args[1], "flxProductDetails1"), extendConfig({}, controller.args[2], "flxProductDetails1"));
        flxProductDetails1.setDefaultUnit(kony.flex.DP);
        var flxProductDetailsContainer1 = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "clipBounds": true,
            "height": "100%",
            "id": "flxProductDetailsContainer1",
            "isVisible": true,
            "layoutType": kony.flex.FLOW_VERTICAL,
            "left": "0px",
            "isModalContainer": false,
            "skin": "slFbox",
            "top": "0px",
            "width": "31%",
            "zIndex": 1
        }, controller.args[0], "flxProductDetailsContainer1"), extendConfig({}, controller.args[1], "flxProductDetailsContainer1"), extendConfig({}, controller.args[2], "flxProductDetailsContainer1"));
        flxProductDetailsContainer1.setDefaultUnit(kony.flex.DP);
        var lblProductLine = new kony.ui.Label(extendConfig({
            "id": "lblProductLine",
            "isVisible": true,
            "left": "0px",
            "skin": "sknLbl485C75LatoRegular13Px",
            "i18n_text": "kony.i18n.getLocalizedString(\"i18n.products.Product_Line\")",
            "top": "0px",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, controller.args[0], "lblProductLine"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblProductLine"), extendConfig({}, controller.args[2], "lblProductLine"));
        var lstboxProductLine = new kony.ui.ListBox(extendConfig({
            "focusSkin": "defListBoxFocus",
            "height": "40dp",
            "id": "lstboxProductLine",
            "isVisible": true,
            "left": "0px",
            "masterData": [
                ["SELECT", "Select Product Line"]
            ],
            "selectedKey": "SELECT",
            "skin": "sknLbxborderd7d9e03pxradius",
            "top": "15px",
            "width": "100%",
            "zIndex": 1
        }, controller.args[0], "lstboxProductLine"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_CENTER,
            "padding": [3, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lstboxProductLine"), extendConfig({
            "multiSelect": false
        }, controller.args[2], "lstboxProductLine"));
        var flxErrorMsg1 = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
            "clipBounds": true,
            "id": "flxErrorMsg1",
            "isVisible": false,
            "layoutType": kony.flex.FLOW_HORIZONTAL,
            "left": "0dp",
            "isModalContainer": false,
            "skin": "slFbox",
            "top": "10dp",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, controller.args[0], "flxErrorMsg1"), extendConfig({}, controller.args[1], "flxErrorMsg1"), extendConfig({}, controller.args[2], "flxErrorMsg1"));
        flxErrorMsg1.setDefaultUnit(kony.flex.DP);
        var lblErrorIcon1 = new kony.ui.Label(extendConfig({
            "height": "15dp",
            "id": "lblErrorIcon1",
            "isVisible": true,
            "left": "0dp",
            "skin": "sknErrorIcon",
            "text": "",
            "top": "0dp",
            "width": "15dp",
            "zIndex": 1
        }, controller.args[0], "lblErrorIcon1"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblErrorIcon1"), extendConfig({}, controller.args[2], "lblErrorIcon1"));
        var lblErrorText1 = new kony.ui.Label(extendConfig({
            "height": "15dp",
            "id": "lblErrorText1",
            "isVisible": true,
            "left": "0dp",
            "skin": "sknlblError",
            "i18n_text": "kony.i18n.getLocalizedString(\"i18n.products.Please_Select_Product_Line\")",
            "top": "0dp",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, controller.args[0], "lblErrorText1"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblErrorText1"), extendConfig({}, controller.args[2], "lblErrorText1"));
        flxErrorMsg1.add(lblErrorIcon1, lblErrorText1);
        flxProductDetailsContainer1.add(lblProductLine, lstboxProductLine, flxErrorMsg1);
        var flxProductDetailsContainer2 = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "clipBounds": true,
            "height": "100%",
            "id": "flxProductDetailsContainer2",
            "isVisible": true,
            "layoutType": kony.flex.FLOW_VERTICAL,
            "left": "15px",
            "isModalContainer": false,
            "skin": "slFbox",
            "top": "0px",
            "width": "31%",
            "zIndex": 1
        }, controller.args[0], "flxProductDetailsContainer2"), extendConfig({}, controller.args[1], "flxProductDetailsContainer2"), extendConfig({}, controller.args[2], "flxProductDetailsContainer2"));
        flxProductDetailsContainer2.setDefaultUnit(kony.flex.DP);
        var lblProductGroup = new kony.ui.Label(extendConfig({
            "id": "lblProductGroup",
            "isVisible": true,
            "left": "0px",
            "skin": "sknLbl485C75LatoRegular13Px",
            "i18n_text": "kony.i18n.getLocalizedString(\"i18n.products.Product_Group\")",
            "top": "0px",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, controller.args[0], "lblProductGroup"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblProductGroup"), extendConfig({}, controller.args[2], "lblProductGroup"));
        var lstboxProductGroup = new kony.ui.ListBox(extendConfig({
            "focusSkin": "defListBoxFocus",
            "height": "40dp",
            "id": "lstboxProductGroup",
            "isVisible": true,
            "left": "0px",
            "masterData": [
                ["SELECT", "Select Product Group"]
            ],
            "selectedKey": "SELECT",
            "skin": "sknLbxborderd7d9e03pxradius",
            "top": "15px",
            "width": "100%",
            "zIndex": 1
        }, controller.args[0], "lstboxProductGroup"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_CENTER,
            "padding": [3, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lstboxProductGroup"), extendConfig({
            "multiSelect": false
        }, controller.args[2], "lstboxProductGroup"));
        var flxErrorMsg2 = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
            "clipBounds": true,
            "id": "flxErrorMsg2",
            "isVisible": false,
            "layoutType": kony.flex.FLOW_HORIZONTAL,
            "left": "0dp",
            "isModalContainer": false,
            "skin": "slFbox",
            "top": "10dp",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, controller.args[0], "flxErrorMsg2"), extendConfig({}, controller.args[1], "flxErrorMsg2"), extendConfig({}, controller.args[2], "flxErrorMsg2"));
        flxErrorMsg2.setDefaultUnit(kony.flex.DP);
        var lblErrorIcon2 = new kony.ui.Label(extendConfig({
            "height": "15dp",
            "id": "lblErrorIcon2",
            "isVisible": true,
            "left": "0dp",
            "skin": "sknErrorIcon",
            "text": "",
            "top": "0dp",
            "width": "15dp",
            "zIndex": 1
        }, controller.args[0], "lblErrorIcon2"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblErrorIcon2"), extendConfig({}, controller.args[2], "lblErrorIcon2"));
        var lblErrorText2 = new kony.ui.Label(extendConfig({
            "height": "15dp",
            "id": "lblErrorText2",
            "isVisible": true,
            "left": "0dp",
            "skin": "sknlblError",
            "i18n_text": "kony.i18n.getLocalizedString(\"i18n.products.Please_Select_Product_Group\")",
            "top": "0dp",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, controller.args[0], "lblErrorText2"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblErrorText2"), extendConfig({}, controller.args[2], "lblErrorText2"));
        flxErrorMsg2.add(lblErrorIcon2, lblErrorText2);
        flxProductDetailsContainer2.add(lblProductGroup, lstboxProductGroup, flxErrorMsg2);
        var flxProductDetailsContainer3 = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "clipBounds": true,
            "height": "100%",
            "id": "flxProductDetailsContainer3",
            "isVisible": true,
            "layoutType": kony.flex.FLOW_VERTICAL,
            "left": "15px",
            "isModalContainer": false,
            "skin": "slFbox",
            "top": "0px",
            "width": "31%",
            "zIndex": 1
        }, controller.args[0], "flxProductDetailsContainer3"), extendConfig({}, controller.args[1], "flxProductDetailsContainer3"), extendConfig({}, controller.args[2], "flxProductDetailsContainer3"));
        flxProductDetailsContainer3.setDefaultUnit(kony.flex.DP);
        var lblProductName = new kony.ui.Label(extendConfig({
            "id": "lblProductName",
            "isVisible": true,
            "left": "0px",
            "skin": "sknLbl485C75LatoRegular13Px",
            "i18n_text": "kony.i18n.getLocalizedString(\"i18n.products.Product_Name\")",
            "top": "0px",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, controller.args[0], "lblProductName"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblProductName"), extendConfig({}, controller.args[2], "lblProductName"));
        var tbxProductName = new kony.ui.TextBox2(extendConfig({
            "autoCapitalize": constants.TEXTBOX_AUTO_CAPITALIZE_NONE,
            "focusSkin": "defTextBoxFocus",
            "height": "40dp",
            "id": "tbxProductName",
            "isVisible": true,
            "keyBoardStyle": constants.TEXTBOX_KEY_BOARD_STYLE_DEFAULT,
            "left": "0dp",
            "i18n_placeholder": "kony.i18n.getLocalizedString(\"i18n.products.Product_Name\")",
            "secureTextEntry": false,
            "skin": "txtD7d9e0",
            "textInputMode": constants.TEXTBOX_INPUT_MODE_ANY,
            "top": "15px",
            "width": "100%",
            "zIndex": 1
        }, controller.args[0], "tbxProductName"), extendConfig({
            "containerHeightMode": constants.TEXTBOX_FONT_METRICS_DRIVEN_HEIGHT,
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [3, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "tbxProductName"), extendConfig({
            "autoCorrect": false,
            "placeholderSkin": "defTextBoxPlaceholder"
        }, controller.args[2], "tbxProductName"));
        var flxErrorMsg3 = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
            "clipBounds": true,
            "id": "flxErrorMsg3",
            "isVisible": false,
            "layoutType": kony.flex.FLOW_HORIZONTAL,
            "left": "0dp",
            "isModalContainer": false,
            "skin": "slFbox",
            "top": "10dp",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, controller.args[0], "flxErrorMsg3"), extendConfig({}, controller.args[1], "flxErrorMsg3"), extendConfig({}, controller.args[2], "flxErrorMsg3"));
        flxErrorMsg3.setDefaultUnit(kony.flex.DP);
        var lblErrorIcon3 = new kony.ui.Label(extendConfig({
            "height": "15dp",
            "id": "lblErrorIcon3",
            "isVisible": true,
            "left": "0dp",
            "skin": "sknErrorIcon",
            "text": "",
            "top": "0dp",
            "width": "15dp",
            "zIndex": 1
        }, controller.args[0], "lblErrorIcon3"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblErrorIcon3"), extendConfig({}, controller.args[2], "lblErrorIcon3"));
        var lblErrorText3 = new kony.ui.Label(extendConfig({
            "height": "15dp",
            "id": "lblErrorText3",
            "isVisible": true,
            "left": "0dp",
            "skin": "sknlblError",
            "i18n_text": "kony.i18n.getLocalizedString(\"i18n.products.Product_Name_cannot_be_empty\")",
            "top": "0dp",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, controller.args[0], "lblErrorText3"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblErrorText3"), extendConfig({}, controller.args[2], "lblErrorText3"));
        flxErrorMsg3.add(lblErrorIcon3, lblErrorText3);
        flxProductDetailsContainer3.add(lblProductName, tbxProductName, flxErrorMsg3);
        flxProductDetails1.add(flxProductDetailsContainer1, flxProductDetailsContainer2, flxProductDetailsContainer3);
        var flxProductDetails2 = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
            "clipBounds": true,
            "id": "flxProductDetails2",
            "isVisible": true,
            "layoutType": kony.flex.FLOW_HORIZONTAL,
            "left": "0px",
            "isModalContainer": false,
            "skin": "slFbox",
            "top": "100px",
            "width": "100%",
            "zIndex": 1
        }, controller.args[0], "flxProductDetails2"), extendConfig({}, controller.args[1], "flxProductDetails2"), extendConfig({}, controller.args[2], "flxProductDetails2"));
        flxProductDetails2.setDefaultUnit(kony.flex.DP);
        var flxProductDetailsContainer4 = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "clipBounds": true,
            "height": "100px",
            "id": "flxProductDetailsContainer4",
            "isVisible": true,
            "layoutType": kony.flex.FLOW_VERTICAL,
            "left": "0px",
            "isModalContainer": false,
            "skin": "slFbox",
            "top": "0px",
            "width": "31%",
            "zIndex": 1
        }, controller.args[0], "flxProductDetailsContainer4"), extendConfig({}, controller.args[1], "flxProductDetailsContainer4"), extendConfig({}, controller.args[2], "flxProductDetailsContainer4"));
        flxProductDetailsContainer4.setDefaultUnit(kony.flex.DP);
        var lblProductReference = new kony.ui.Label(extendConfig({
            "id": "lblProductReference",
            "isVisible": true,
            "left": "0px",
            "skin": "sknLbl485C75LatoRegular13Px",
            "i18n_text": "kony.i18n.getLocalizedString(\"i18n.products.Product_Reference\")",
            "top": "0px",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, controller.args[0], "lblProductReference"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblProductReference"), extendConfig({}, controller.args[2], "lblProductReference"));
        var tbxProductReference = new kony.ui.TextBox2(extendConfig({
            "autoCapitalize": constants.TEXTBOX_AUTO_CAPITALIZE_NONE,
            "focusSkin": "defTextBoxFocus",
            "height": "40dp",
            "id": "tbxProductReference",
            "isVisible": true,
            "keyBoardStyle": constants.TEXTBOX_KEY_BOARD_STYLE_DEFAULT,
            "left": "0dp",
            "i18n_placeholder": "kony.i18n.getLocalizedString(\"i18n.products.Product_Reference\")",
            "secureTextEntry": false,
            "skin": "txtD7d9e0",
            "textInputMode": constants.TEXTBOX_INPUT_MODE_ANY,
            "top": "15px",
            "width": "100%",
            "zIndex": 1
        }, controller.args[0], "tbxProductReference"), extendConfig({
            "containerHeightMode": constants.TEXTBOX_FONT_METRICS_DRIVEN_HEIGHT,
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [3, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "tbxProductReference"), extendConfig({
            "autoCorrect": false,
            "placeholderSkin": "defTextBoxPlaceholder"
        }, controller.args[2], "tbxProductReference"));
        var flxErrorMsg4 = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
            "clipBounds": true,
            "id": "flxErrorMsg4",
            "isVisible": false,
            "layoutType": kony.flex.FLOW_HORIZONTAL,
            "left": "0dp",
            "isModalContainer": false,
            "skin": "slFbox",
            "top": "10dp",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, controller.args[0], "flxErrorMsg4"), extendConfig({}, controller.args[1], "flxErrorMsg4"), extendConfig({}, controller.args[2], "flxErrorMsg4"));
        flxErrorMsg4.setDefaultUnit(kony.flex.DP);
        var lblErrorIcon4 = new kony.ui.Label(extendConfig({
            "height": "15dp",
            "id": "lblErrorIcon4",
            "isVisible": true,
            "left": "0dp",
            "skin": "sknErrorIcon",
            "text": "",
            "top": "0dp",
            "width": "15dp",
            "zIndex": 1
        }, controller.args[0], "lblErrorIcon4"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblErrorIcon4"), extendConfig({}, controller.args[2], "lblErrorIcon4"));
        var lblErrorText4 = new kony.ui.Label(extendConfig({
            "height": "15dp",
            "id": "lblErrorText4",
            "isVisible": true,
            "left": "0dp",
            "skin": "sknlblError",
            "i18n_text": "kony.i18n.getLocalizedString(\"i18n.products.Product_Reference_cannot_be_empty\")",
            "top": "0dp",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, controller.args[0], "lblErrorText4"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblErrorText4"), extendConfig({}, controller.args[2], "lblErrorText4"));
        flxErrorMsg4.add(lblErrorIcon4, lblErrorText4);
        flxProductDetailsContainer4.add(lblProductReference, tbxProductReference, flxErrorMsg4);
        var flxProductDetailsContainer5 = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "clipBounds": true,
            "height": "100px",
            "id": "flxProductDetailsContainer5",
            "isVisible": true,
            "layoutType": kony.flex.FLOW_VERTICAL,
            "left": "15px",
            "isModalContainer": false,
            "skin": "slFbox",
            "top": "0px",
            "width": "31%",
            "zIndex": 1
        }, controller.args[0], "flxProductDetailsContainer5"), extendConfig({}, controller.args[1], "flxProductDetailsContainer5"), extendConfig({}, controller.args[2], "flxProductDetailsContainer5"));
        flxProductDetailsContainer5.setDefaultUnit(kony.flex.DP);
        var lblAvailableFromDate = new kony.ui.Label(extendConfig({
            "id": "lblAvailableFromDate",
            "isVisible": true,
            "left": "0px",
            "skin": "sknLbl485C75LatoRegular13Px",
            "i18n_text": "kony.i18n.getLocalizedString(\"i18n.products.Available_From_Date\")",
            "top": "0px",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, controller.args[0], "lblAvailableFromDate"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblAvailableFromDate"), extendConfig({}, controller.args[2], "lblAvailableFromDate"));
        var flxShowStartDate = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "clipBounds": true,
            "height": "40dp",
            "id": "flxShowStartDate",
            "isVisible": false,
            "layoutType": kony.flex.FREE_FORM,
            "left": "0dp",
            "isModalContainer": false,
            "skin": "sknFlxbgF3F3F3bdrd7d9e0",
            "top": "15px",
            "width": "100%",
            "zIndex": 1
        }, controller.args[0], "flxShowStartDate"), extendConfig({}, controller.args[1], "flxShowStartDate"), extendConfig({}, controller.args[2], "flxShowStartDate"));
        flxShowStartDate.setDefaultUnit(kony.flex.DP);
        var lblIconCalendar1 = new kony.ui.Label(extendConfig({
            "centerY": "50%",
            "height": "20px",
            "id": "lblIconCalendar1",
            "isVisible": true,
            "left": "10px",
            "skin": "sknLblIcomoon20px696c75",
            "text": "",
            "width": "20px",
            "zIndex": 1
        }, controller.args[0], "lblIconCalendar1"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblIconCalendar1"), extendConfig({}, controller.args[2], "lblIconCalendar1"));
        var lblStartDate = new kony.ui.Label(extendConfig({
            "centerY": "50%",
            "id": "lblStartDate",
            "isVisible": true,
            "left": "40dp",
            "skin": "sknlblLato696c7313px",
            "text": "09-22-2020",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, controller.args[0], "lblStartDate"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblStartDate"), extendConfig({}, controller.args[2], "lblStartDate"));
        flxShowStartDate.add(lblIconCalendar1, lblStartDate);
        var flxErrorMsg5 = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
            "clipBounds": true,
            "id": "flxErrorMsg5",
            "isVisible": false,
            "layoutType": kony.flex.FLOW_HORIZONTAL,
            "left": "0dp",
            "isModalContainer": false,
            "skin": "slFbox",
            "top": "10dp",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, controller.args[0], "flxErrorMsg5"), extendConfig({}, controller.args[1], "flxErrorMsg5"), extendConfig({}, controller.args[2], "flxErrorMsg5"));
        flxErrorMsg5.setDefaultUnit(kony.flex.DP);
        var lblErrorIcon5 = new kony.ui.Label(extendConfig({
            "height": "15dp",
            "id": "lblErrorIcon5",
            "isVisible": true,
            "left": "0dp",
            "skin": "sknErrorIcon",
            "text": "",
            "top": "0dp",
            "width": "15dp",
            "zIndex": 1
        }, controller.args[0], "lblErrorIcon5"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblErrorIcon5"), extendConfig({}, controller.args[2], "lblErrorIcon5"));
        var lblErrorText5 = new kony.ui.Label(extendConfig({
            "height": "15dp",
            "id": "lblErrorText5",
            "isVisible": true,
            "left": "0dp",
            "skin": "sknlblError",
            "i18n_text": "kony.i18n.getLocalizedString(\"i18n.products.Please_select_a_valid_date\")",
            "top": "0dp",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, controller.args[0], "lblErrorText5"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblErrorText5"), extendConfig({}, controller.args[2], "lblErrorText5"));
        flxErrorMsg5.add(lblErrorIcon5, lblErrorText5);
        flxProductDetailsContainer5.add(lblAvailableFromDate, flxShowStartDate, flxErrorMsg5);
        var flxProductDetailsContainer6 = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "clipBounds": true,
            "height": "110px",
            "id": "flxProductDetailsContainer6",
            "isVisible": true,
            "layoutType": kony.flex.FLOW_VERTICAL,
            "left": "15px",
            "isModalContainer": false,
            "skin": "slFbox",
            "top": "0px",
            "width": "31%",
            "zIndex": 1
        }, controller.args[0], "flxProductDetailsContainer6"), extendConfig({}, controller.args[1], "flxProductDetailsContainer6"), extendConfig({}, controller.args[2], "flxProductDetailsContainer6"));
        flxProductDetailsContainer6.setDefaultUnit(kony.flex.DP);
        var blAvailableToDate = new kony.ui.Label(extendConfig({
            "id": "blAvailableToDate",
            "isVisible": true,
            "left": "0px",
            "skin": "sknLbl485C75LatoRegular13Px",
            "i18n_text": "kony.i18n.getLocalizedString(\"i18n.products.Available_To_Date\")",
            "top": "0px",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, controller.args[0], "blAvailableToDate"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "blAvailableToDate"), extendConfig({}, controller.args[2], "blAvailableToDate"));
        var flxShowEndDate = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "clipBounds": true,
            "height": "40dp",
            "id": "flxShowEndDate",
            "isVisible": false,
            "layoutType": kony.flex.FREE_FORM,
            "left": "0dp",
            "isModalContainer": false,
            "skin": "sknFlxbgF3F3F3bdrd7d9e0",
            "top": "15px",
            "width": "100%",
            "zIndex": 1
        }, controller.args[0], "flxShowEndDate"), extendConfig({}, controller.args[1], "flxShowEndDate"), extendConfig({}, controller.args[2], "flxShowEndDate"));
        flxShowEndDate.setDefaultUnit(kony.flex.DP);
        var lblIconCalendar2 = new kony.ui.Label(extendConfig({
            "centerY": "50%",
            "height": "20px",
            "id": "lblIconCalendar2",
            "isVisible": true,
            "left": "10px",
            "skin": "sknLblIcomoon20px696c75",
            "text": "",
            "width": "20px",
            "zIndex": 1
        }, controller.args[0], "lblIconCalendar2"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblIconCalendar2"), extendConfig({}, controller.args[2], "lblIconCalendar2"));
        var lblEndDate = new kony.ui.Label(extendConfig({
            "centerY": "50%",
            "id": "lblEndDate",
            "isVisible": true,
            "left": "40dp",
            "skin": "sknlblLato696c7313px",
            "text": "09-25-2020",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, controller.args[0], "lblEndDate"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblEndDate"), extendConfig({}, controller.args[2], "lblEndDate"));
        flxShowEndDate.add(lblIconCalendar2, lblEndDate);
        var flxErrorMsg6 = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
            "clipBounds": true,
            "id": "flxErrorMsg6",
            "isVisible": false,
            "layoutType": kony.flex.FREE_FORM,
            "left": "0dp",
            "isModalContainer": false,
            "skin": "slFbox",
            "top": "10dp",
            "width": "100%",
            "zIndex": 1
        }, controller.args[0], "flxErrorMsg6"), extendConfig({}, controller.args[1], "flxErrorMsg6"), extendConfig({}, controller.args[2], "flxErrorMsg6"));
        flxErrorMsg6.setDefaultUnit(kony.flex.DP);
        var lblErrorIcon6 = new kony.ui.Label(extendConfig({
            "height": "15dp",
            "id": "lblErrorIcon6",
            "isVisible": true,
            "left": "0dp",
            "skin": "sknErrorIcon",
            "text": "",
            "top": "0dp",
            "width": "15dp",
            "zIndex": 1
        }, controller.args[0], "lblErrorIcon6"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblErrorIcon6"), extendConfig({}, controller.args[2], "lblErrorIcon6"));
        var lblErrorText6 = new kony.ui.Label(extendConfig({
            "id": "lblErrorText6",
            "isVisible": true,
            "left": "15dp",
            "right": "0dp",
            "skin": "sknlblError",
            "i18n_text": "kony.i18n.getLocalizedString(\"i18n.products.Please_select_a_valid_date\")",
            "top": "0dp",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, controller.args[0], "lblErrorText6"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblErrorText6"), extendConfig({}, controller.args[2], "lblErrorText6"));
        flxErrorMsg6.add(lblErrorIcon6, lblErrorText6);
        flxProductDetailsContainer6.add(blAvailableToDate, flxShowEndDate, flxErrorMsg6);
        flxProductDetails2.add(flxProductDetailsContainer4, flxProductDetailsContainer5, flxProductDetailsContainer6);
        var flxProductDetailsContainer7 = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
            "clipBounds": true,
            "id": "flxProductDetailsContainer7",
            "isVisible": true,
            "layoutType": kony.flex.FLOW_VERTICAL,
            "left": "0px",
            "isModalContainer": false,
            "skin": "sknBackgroundFFFFFF",
            "top": "200px",
            "width": "31%",
            "zIndex": 10
        }, controller.args[0], "flxProductDetailsContainer7"), extendConfig({}, controller.args[1], "flxProductDetailsContainer7"), extendConfig({}, controller.args[2], "flxProductDetailsContainer7"));
        flxProductDetailsContainer7.setDefaultUnit(kony.flex.DP);
        var lblProductPurpose = new kony.ui.Label(extendConfig({
            "id": "lblProductPurpose",
            "isVisible": true,
            "left": "0px",
            "skin": "sknLbl485C75LatoRegular13Px",
            "i18n_text": "kony.i18n.getLocalizedString(\"i18n.products.Purpose\")",
            "top": "0px",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, controller.args[0], "lblProductPurpose"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblProductPurpose"), extendConfig({}, controller.args[2], "lblProductPurpose"));
        var flxProductPurposeCustomListbox = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "clipBounds": true,
            "height": "40px",
            "id": "flxProductPurposeCustomListbox",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "left": "0px",
            "isModalContainer": false,
            "skin": "slFbox",
            "top": "15px",
            "width": "100%",
            "zIndex": 10
        }, controller.args[0], "flxProductPurposeCustomListbox"), extendConfig({}, controller.args[1], "flxProductPurposeCustomListbox"), extendConfig({}, controller.args[2], "flxProductPurposeCustomListbox"));
        flxProductPurposeCustomListbox.setDefaultUnit(kony.flex.DP);
        var flxSelectedText = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "clipBounds": true,
            "height": "40dp",
            "id": "flxSelectedText",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "left": "0dp",
            "isModalContainer": false,
            "skin": "sknflxffffffop100Bordercbcdd1Radius3px",
            "top": "0dp",
            "width": "100%",
            "zIndex": 1
        }, controller.args[0], "flxSelectedText"), extendConfig({}, controller.args[1], "flxSelectedText"), extendConfig({
            "hoverSkin": "sknflxffffffBorderE1E5EERadius3pxPointer"
        }, controller.args[2], "flxSelectedText"));
        flxSelectedText.setDefaultUnit(kony.flex.DP);
        var lblSelectedValue = new kony.ui.Label(extendConfig({
            "centerY": "50%",
            "id": "lblSelectedValue",
            "isVisible": true,
            "left": "14dp",
            "skin": "sknLbl485C75LatoRegular13Px",
            "i18n_text": "kony.i18n.getLocalizedString(\"i18n.products.Select_Purpose\")",
            "top": "3dp",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, controller.args[0], "lblSelectedValue"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblSelectedValue"), extendConfig({}, controller.args[2], "lblSelectedValue"));
        var flxDropdown = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "centerY": "50%",
            "clipBounds": true,
            "height": "30dp",
            "id": "flxDropdown",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "isModalContainer": false,
            "right": "0dp",
            "skin": "slFbox",
            "top": "0dp",
            "width": "30dp"
        }, controller.args[0], "flxDropdown"), extendConfig({}, controller.args[1], "flxDropdown"), extendConfig({}, controller.args[2], "flxDropdown"));
        flxDropdown.setDefaultUnit(kony.flex.DP);
        var lblIconDropdown = new kony.ui.Label(extendConfig({
            "centerY": "50%",
            "id": "lblIconDropdown",
            "isVisible": true,
            "right": "10dp",
            "skin": "sknFontIconLimits13Px",
            "text": "",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, controller.args[0], "lblIconDropdown"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblIconDropdown"), extendConfig({}, controller.args[2], "lblIconDropdown"));
        flxDropdown.add(lblIconDropdown);
        flxSelectedText.add(lblSelectedValue, flxDropdown);
        flxProductPurposeCustomListbox.add(flxSelectedText);
        var flxSegmentList = new kony.ui.FlexScrollContainer(extendConfig({
            "allowHorizontalBounce": false,
            "allowVerticalBounce": true,
            "bounces": true,
            "clipBounds": false,
            "enableScrolling": true,
            "height": "100px",
            "horizontalScrollIndicator": true,
            "id": "flxSegmentList",
            "isVisible": false,
            "layoutType": kony.flex.FLOW_VERTICAL,
            "left": "0dp",
            "pagingEnabled": false,
            "scrollDirection": kony.flex.SCROLL_VERTICAL,
            "skin": "sknflxFFFFFFBr1pxe1e5edR3px3sided",
            "top": "0px",
            "verticalScrollIndicator": true,
            "width": "100%",
            "zIndex": 10
        }, controller.args[0], "flxSegmentList"), extendConfig({}, controller.args[1], "flxSegmentList"), extendConfig({}, controller.args[2], "flxSegmentList"));
        flxSegmentList.setDefaultUnit(kony.flex.DP);
        var segProductPurpose = new kony.ui.SegmentedUI2(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
            "data": [{
                "imgCheckBox": "checkboxnormal.png",
                "lblDescription": "Purpose 1"
            }, {
                "imgCheckBox": "checkboxnormal.png",
                "lblDescription": "Purpose 2"
            }, {
                "imgCheckBox": "checkboxnormal.png",
                "lblDescription": "Purpose 3"
            }, {
                "imgCheckBox": "checkboxnormal.png",
                "lblDescription": "Purpose 4"
            }],
            "groupCells": false,
            "id": "segProductPurpose",
            "isVisible": true,
            "left": "0px",
            "needPageIndicator": true,
            "pageOffDotImage": "pageoffdot.png",
            "pageOnDotImage": "pageondot.png",
            "retainSelection": false,
            "rowFocusSkin": "seg2Focus",
            "rowSkin": "seg2Normal",
            "rowTemplate": "flxSearchDropDown",
            "sectionHeaderSkin": "sliPhoneSegmentHeader",
            "selectionBehavior": constants.SEGUI_MULTI_SELECT_BEHAVIOR,
            "selectionBehaviorConfig": {
                "imageIdentifier": "imgCheckBox",
                "selectedStateImage": "checkboxselected.png",
                "unselectedStateImage": "checkboxnormal.png"
            },
            "separatorColor": "aaaaaa00",
            "separatorRequired": false,
            "separatorThickness": 1,
            "showScrollbars": false,
            "top": "5px",
            "viewType": constants.SEGUI_VIEW_TYPE_TABLEVIEW,
            "widgetDataMap": {
                "flxCheckBox": "flxCheckBox",
                "flxSearchDropDown": "flxSearchDropDown",
                "imgCheckBox": "imgCheckBox",
                "lblDescription": "lblDescription"
            },
            "width": "95%",
            "zIndex": 1
        }, controller.args[0], "segProductPurpose"), extendConfig({
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "segProductPurpose"), extendConfig({}, controller.args[2], "segProductPurpose"));
        flxSegmentList.add(segProductPurpose);
        var flxErrorMsg7 = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
            "clipBounds": true,
            "id": "flxErrorMsg7",
            "isVisible": false,
            "layoutType": kony.flex.FLOW_HORIZONTAL,
            "left": "0dp",
            "isModalContainer": false,
            "skin": "slFbox",
            "top": "10dp",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, controller.args[0], "flxErrorMsg7"), extendConfig({}, controller.args[1], "flxErrorMsg7"), extendConfig({}, controller.args[2], "flxErrorMsg7"));
        flxErrorMsg7.setDefaultUnit(kony.flex.DP);
        var lblErrorIcon7 = new kony.ui.Label(extendConfig({
            "height": "15dp",
            "id": "lblErrorIcon7",
            "isVisible": true,
            "left": "0dp",
            "skin": "sknErrorIcon",
            "text": "",
            "top": "0dp",
            "width": "15dp",
            "zIndex": 1
        }, controller.args[0], "lblErrorIcon7"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblErrorIcon7"), extendConfig({}, controller.args[2], "lblErrorIcon7"));
        var lblErrorText7 = new kony.ui.Label(extendConfig({
            "height": "15dp",
            "id": "lblErrorText7",
            "isVisible": true,
            "left": "0dp",
            "skin": "sknlblError",
            "text": "Product Purpose cannot be empty.",
            "top": "0dp",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, controller.args[0], "lblErrorText7"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblErrorText7"), extendConfig({}, controller.args[2], "lblErrorText7"));
        flxErrorMsg7.add(lblErrorIcon7, lblErrorText7);
        flxProductDetailsContainer7.add(lblProductPurpose, flxProductPurposeCustomListbox, flxSegmentList, flxErrorMsg7);
        var flxProductDetails3 = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "clipBounds": true,
            "height": "100px",
            "id": "flxProductDetails3",
            "isVisible": true,
            "layoutType": kony.flex.FLOW_HORIZONTAL,
            "left": "0px",
            "isModalContainer": false,
            "skin": "slFbox",
            "top": "200px",
            "width": "100%",
            "zIndex": 1
        }, controller.args[0], "flxProductDetails3"), extendConfig({}, controller.args[1], "flxProductDetails3"), extendConfig({}, controller.args[2], "flxProductDetails3"));
        flxProductDetails3.setDefaultUnit(kony.flex.DP);
        var flxProductDetailsContainer8 = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "clipBounds": true,
            "height": "100px",
            "id": "flxProductDetailsContainer8",
            "isVisible": true,
            "layoutType": kony.flex.FLOW_VERTICAL,
            "left": "33%",
            "isModalContainer": false,
            "skin": "slFbox",
            "top": "0px",
            "width": "31%",
            "zIndex": 1
        }, controller.args[0], "flxProductDetailsContainer8"), extendConfig({}, controller.args[1], "flxProductDetailsContainer8"), extendConfig({}, controller.args[2], "flxProductDetailsContainer8"));
        flxProductDetailsContainer8.setDefaultUnit(kony.flex.DP);
        var lblAdditionalField1 = new kony.ui.Label(extendConfig({
            "id": "lblAdditionalField1",
            "isVisible": true,
            "left": "0px",
            "skin": "sknLbl485C75LatoRegular13Px",
            "text": "Additional Field 1",
            "top": "0px",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, controller.args[0], "lblAdditionalField1"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblAdditionalField1"), extendConfig({}, controller.args[2], "lblAdditionalField1"));
        var tbxAdditionalField1 = new kony.ui.TextBox2(extendConfig({
            "autoCapitalize": constants.TEXTBOX_AUTO_CAPITALIZE_NONE,
            "focusSkin": "defTextBoxFocus",
            "height": "40dp",
            "id": "tbxAdditionalField1",
            "isVisible": true,
            "keyBoardStyle": constants.TEXTBOX_KEY_BOARD_STYLE_DEFAULT,
            "left": "0dp",
            "secureTextEntry": false,
            "skin": "txtD7d9e0",
            "textInputMode": constants.TEXTBOX_INPUT_MODE_ANY,
            "top": "15px",
            "width": "100%",
            "zIndex": 1
        }, controller.args[0], "tbxAdditionalField1"), extendConfig({
            "containerHeightMode": constants.TEXTBOX_FONT_METRICS_DRIVEN_HEIGHT,
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [3, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "tbxAdditionalField1"), extendConfig({
            "autoCorrect": false,
            "placeholderSkin": "defTextBoxPlaceholder"
        }, controller.args[2], "tbxAdditionalField1"));
        flxProductDetailsContainer8.add(lblAdditionalField1, tbxAdditionalField1);
        var flxProductDetailsContainer9 = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "clipBounds": true,
            "height": "100px",
            "id": "flxProductDetailsContainer9",
            "isVisible": true,
            "layoutType": kony.flex.FLOW_VERTICAL,
            "left": "15px",
            "isModalContainer": false,
            "skin": "slFbox",
            "top": "0px",
            "width": "31%",
            "zIndex": 1
        }, controller.args[0], "flxProductDetailsContainer9"), extendConfig({}, controller.args[1], "flxProductDetailsContainer9"), extendConfig({}, controller.args[2], "flxProductDetailsContainer9"));
        flxProductDetailsContainer9.setDefaultUnit(kony.flex.DP);
        var lblAdditionalField2 = new kony.ui.Label(extendConfig({
            "id": "lblAdditionalField2",
            "isVisible": true,
            "left": "0px",
            "skin": "sknLbl485C75LatoRegular13Px",
            "text": "Additional Field 2",
            "top": "0px",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, controller.args[0], "lblAdditionalField2"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblAdditionalField2"), extendConfig({}, controller.args[2], "lblAdditionalField2"));
        var tbxAdditionalField2 = new kony.ui.TextBox2(extendConfig({
            "autoCapitalize": constants.TEXTBOX_AUTO_CAPITALIZE_NONE,
            "focusSkin": "defTextBoxFocus",
            "height": "40dp",
            "id": "tbxAdditionalField2",
            "isVisible": true,
            "keyBoardStyle": constants.TEXTBOX_KEY_BOARD_STYLE_DEFAULT,
            "left": "0dp",
            "secureTextEntry": false,
            "skin": "txtD7d9e0",
            "textInputMode": constants.TEXTBOX_INPUT_MODE_ANY,
            "top": "15px",
            "width": "100%",
            "zIndex": 1
        }, controller.args[0], "tbxAdditionalField2"), extendConfig({
            "containerHeightMode": constants.TEXTBOX_FONT_METRICS_DRIVEN_HEIGHT,
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [3, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "tbxAdditionalField2"), extendConfig({
            "autoCorrect": false,
            "placeholderSkin": "defTextBoxPlaceholder"
        }, controller.args[2], "tbxAdditionalField2"));
        flxProductDetailsContainer9.add(lblAdditionalField2, tbxAdditionalField2);
        flxProductDetails3.add(flxProductDetailsContainer8, flxProductDetailsContainer9);
        var flxProductDetails4 = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "clipBounds": true,
            "height": "100px",
            "id": "flxProductDetails4",
            "isVisible": true,
            "layoutType": kony.flex.FLOW_HORIZONTAL,
            "left": "0px",
            "isModalContainer": false,
            "skin": "slFbox",
            "top": "300px",
            "width": "100%",
            "zIndex": 1
        }, controller.args[0], "flxProductDetails4"), extendConfig({}, controller.args[1], "flxProductDetails4"), extendConfig({}, controller.args[2], "flxProductDetails4"));
        flxProductDetails4.setDefaultUnit(kony.flex.DP);
        var flxProductDetailsContainer10 = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "clipBounds": true,
            "height": "100%",
            "id": "flxProductDetailsContainer10",
            "isVisible": true,
            "layoutType": kony.flex.FLOW_VERTICAL,
            "left": "0px",
            "isModalContainer": false,
            "skin": "slFbox",
            "top": "0px",
            "width": "31%",
            "zIndex": 1
        }, controller.args[0], "flxProductDetailsContainer10"), extendConfig({}, controller.args[1], "flxProductDetailsContainer10"), extendConfig({}, controller.args[2], "flxProductDetailsContainer10"));
        flxProductDetailsContainer10.setDefaultUnit(kony.flex.DP);
        var lblAdditionalField3 = new kony.ui.Label(extendConfig({
            "id": "lblAdditionalField3",
            "isVisible": true,
            "left": "0px",
            "skin": "sknLbl485C75LatoRegular13Px",
            "text": "Additional Field 3",
            "top": "0px",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, controller.args[0], "lblAdditionalField3"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblAdditionalField3"), extendConfig({}, controller.args[2], "lblAdditionalField3"));
        var tbxAdditionalField3 = new kony.ui.TextBox2(extendConfig({
            "autoCapitalize": constants.TEXTBOX_AUTO_CAPITALIZE_NONE,
            "focusSkin": "defTextBoxFocus",
            "height": "40dp",
            "id": "tbxAdditionalField3",
            "isVisible": true,
            "keyBoardStyle": constants.TEXTBOX_KEY_BOARD_STYLE_DEFAULT,
            "left": "0dp",
            "secureTextEntry": false,
            "skin": "txtD7d9e0",
            "textInputMode": constants.TEXTBOX_INPUT_MODE_ANY,
            "top": "15px",
            "width": "100%",
            "zIndex": 1
        }, controller.args[0], "tbxAdditionalField3"), extendConfig({
            "containerHeightMode": constants.TEXTBOX_FONT_METRICS_DRIVEN_HEIGHT,
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [3, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "tbxAdditionalField3"), extendConfig({
            "autoCorrect": false,
            "placeholderSkin": "defTextBoxPlaceholder"
        }, controller.args[2], "tbxAdditionalField3"));
        flxProductDetailsContainer10.add(lblAdditionalField3, tbxAdditionalField3);
        var flxProductDetailsContainer11 = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "clipBounds": true,
            "height": "100%",
            "id": "flxProductDetailsContainer11",
            "isVisible": true,
            "layoutType": kony.flex.FLOW_VERTICAL,
            "left": "15px",
            "isModalContainer": false,
            "skin": "slFbox",
            "top": "0px",
            "width": "31%",
            "zIndex": 1
        }, controller.args[0], "flxProductDetailsContainer11"), extendConfig({}, controller.args[1], "flxProductDetailsContainer11"), extendConfig({}, controller.args[2], "flxProductDetailsContainer11"));
        flxProductDetailsContainer11.setDefaultUnit(kony.flex.DP);
        var lblAdditionalField4 = new kony.ui.Label(extendConfig({
            "id": "lblAdditionalField4",
            "isVisible": true,
            "left": "0px",
            "skin": "sknLbl485C75LatoRegular13Px",
            "text": "Additional Field 4",
            "top": "0px",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, controller.args[0], "lblAdditionalField4"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblAdditionalField4"), extendConfig({}, controller.args[2], "lblAdditionalField4"));
        var tbxAdditionalField4 = new kony.ui.TextBox2(extendConfig({
            "autoCapitalize": constants.TEXTBOX_AUTO_CAPITALIZE_NONE,
            "focusSkin": "defTextBoxFocus",
            "height": "40dp",
            "id": "tbxAdditionalField4",
            "isVisible": true,
            "keyBoardStyle": constants.TEXTBOX_KEY_BOARD_STYLE_DEFAULT,
            "left": "0dp",
            "secureTextEntry": false,
            "skin": "txtD7d9e0",
            "textInputMode": constants.TEXTBOX_INPUT_MODE_ANY,
            "top": "15px",
            "width": "100%",
            "zIndex": 1
        }, controller.args[0], "tbxAdditionalField4"), extendConfig({
            "containerHeightMode": constants.TEXTBOX_FONT_METRICS_DRIVEN_HEIGHT,
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [3, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "tbxAdditionalField4"), extendConfig({
            "autoCorrect": false,
            "placeholderSkin": "defTextBoxPlaceholder"
        }, controller.args[2], "tbxAdditionalField4"));
        flxProductDetailsContainer11.add(lblAdditionalField4, tbxAdditionalField4);
        var flxProductDetailsContainer12 = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "clipBounds": true,
            "height": "100%",
            "id": "flxProductDetailsContainer12",
            "isVisible": true,
            "layoutType": kony.flex.FLOW_VERTICAL,
            "left": "15px",
            "isModalContainer": false,
            "skin": "slFbox",
            "top": "0px",
            "width": "31%",
            "zIndex": 1
        }, controller.args[0], "flxProductDetailsContainer12"), extendConfig({}, controller.args[1], "flxProductDetailsContainer12"), extendConfig({}, controller.args[2], "flxProductDetailsContainer12"));
        flxProductDetailsContainer12.setDefaultUnit(kony.flex.DP);
        var lblAdditionalField5 = new kony.ui.Label(extendConfig({
            "id": "lblAdditionalField5",
            "isVisible": true,
            "left": "0px",
            "skin": "sknLbl485C75LatoRegular13Px",
            "text": "Additional Field 5",
            "top": "0px",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, controller.args[0], "lblAdditionalField5"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblAdditionalField5"), extendConfig({}, controller.args[2], "lblAdditionalField5"));
        var tbxAdditionalField5 = new kony.ui.TextBox2(extendConfig({
            "autoCapitalize": constants.TEXTBOX_AUTO_CAPITALIZE_NONE,
            "focusSkin": "defTextBoxFocus",
            "height": "40dp",
            "id": "tbxAdditionalField5",
            "isVisible": true,
            "keyBoardStyle": constants.TEXTBOX_KEY_BOARD_STYLE_DEFAULT,
            "left": "0dp",
            "secureTextEntry": false,
            "skin": "txtD7d9e0",
            "textInputMode": constants.TEXTBOX_INPUT_MODE_ANY,
            "top": "15px",
            "width": "100%",
            "zIndex": 1
        }, controller.args[0], "tbxAdditionalField5"), extendConfig({
            "containerHeightMode": constants.TEXTBOX_FONT_METRICS_DRIVEN_HEIGHT,
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [3, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "tbxAdditionalField5"), extendConfig({
            "autoCorrect": false,
            "placeholderSkin": "defTextBoxPlaceholder"
        }, controller.args[2], "tbxAdditionalField5"));
        flxProductDetailsContainer12.add(lblAdditionalField5, tbxAdditionalField5);
        flxProductDetails4.add(flxProductDetailsContainer10, flxProductDetailsContainer11, flxProductDetailsContainer12);
        var flxProductDetails5 = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "clipBounds": true,
            "height": "100px",
            "id": "flxProductDetails5",
            "isVisible": true,
            "layoutType": kony.flex.FLOW_HORIZONTAL,
            "left": "0px",
            "isModalContainer": false,
            "skin": "slFbox",
            "top": "400px",
            "width": "100%",
            "zIndex": 1
        }, controller.args[0], "flxProductDetails5"), extendConfig({}, controller.args[1], "flxProductDetails5"), extendConfig({}, controller.args[2], "flxProductDetails5"));
        flxProductDetails5.setDefaultUnit(kony.flex.DP);
        var flxProductDetailsContainer13 = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "clipBounds": true,
            "height": "100%",
            "id": "flxProductDetailsContainer13",
            "isVisible": true,
            "layoutType": kony.flex.FLOW_VERTICAL,
            "left": "0px",
            "isModalContainer": false,
            "skin": "slFbox",
            "top": "0px",
            "width": "31%",
            "zIndex": 1
        }, controller.args[0], "flxProductDetailsContainer13"), extendConfig({}, controller.args[1], "flxProductDetailsContainer13"), extendConfig({}, controller.args[2], "flxProductDetailsContainer13"));
        flxProductDetailsContainer13.setDefaultUnit(kony.flex.DP);
        var lblAdditionalField6 = new kony.ui.Label(extendConfig({
            "id": "lblAdditionalField6",
            "isVisible": true,
            "left": "0px",
            "skin": "sknLbl485C75LatoRegular13Px",
            "text": "Additional Field 6",
            "top": "0px",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, controller.args[0], "lblAdditionalField6"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblAdditionalField6"), extendConfig({}, controller.args[2], "lblAdditionalField6"));
        var tbxAdditionalField6 = new kony.ui.TextBox2(extendConfig({
            "autoCapitalize": constants.TEXTBOX_AUTO_CAPITALIZE_NONE,
            "focusSkin": "defTextBoxFocus",
            "height": "40dp",
            "id": "tbxAdditionalField6",
            "isVisible": true,
            "keyBoardStyle": constants.TEXTBOX_KEY_BOARD_STYLE_DEFAULT,
            "left": "0dp",
            "secureTextEntry": false,
            "skin": "txtD7d9e0",
            "textInputMode": constants.TEXTBOX_INPUT_MODE_ANY,
            "top": "15px",
            "width": "100%",
            "zIndex": 1
        }, controller.args[0], "tbxAdditionalField6"), extendConfig({
            "containerHeightMode": constants.TEXTBOX_FONT_METRICS_DRIVEN_HEIGHT,
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [3, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "tbxAdditionalField6"), extendConfig({
            "autoCorrect": false,
            "placeholderSkin": "defTextBoxPlaceholder"
        }, controller.args[2], "tbxAdditionalField6"));
        flxProductDetailsContainer13.add(lblAdditionalField6, tbxAdditionalField6);
        var flxProductDetailsContainer14 = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "clipBounds": true,
            "height": "100%",
            "id": "flxProductDetailsContainer14",
            "isVisible": true,
            "layoutType": kony.flex.FLOW_VERTICAL,
            "left": "15px",
            "isModalContainer": false,
            "skin": "slFbox",
            "top": "0px",
            "width": "31%",
            "zIndex": 1
        }, controller.args[0], "flxProductDetailsContainer14"), extendConfig({}, controller.args[1], "flxProductDetailsContainer14"), extendConfig({}, controller.args[2], "flxProductDetailsContainer14"));
        flxProductDetailsContainer14.setDefaultUnit(kony.flex.DP);
        var lblAdditionalField7 = new kony.ui.Label(extendConfig({
            "id": "lblAdditionalField7",
            "isVisible": true,
            "left": "0px",
            "skin": "sknLbl485C75LatoRegular13Px",
            "text": "Additional Field 7",
            "top": "0px",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, controller.args[0], "lblAdditionalField7"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblAdditionalField7"), extendConfig({}, controller.args[2], "lblAdditionalField7"));
        var tbxAdditionalField7 = new kony.ui.TextBox2(extendConfig({
            "autoCapitalize": constants.TEXTBOX_AUTO_CAPITALIZE_NONE,
            "focusSkin": "defTextBoxFocus",
            "height": "40dp",
            "id": "tbxAdditionalField7",
            "isVisible": true,
            "keyBoardStyle": constants.TEXTBOX_KEY_BOARD_STYLE_DEFAULT,
            "left": "0dp",
            "secureTextEntry": false,
            "skin": "txtD7d9e0",
            "textInputMode": constants.TEXTBOX_INPUT_MODE_ANY,
            "top": "15px",
            "width": "100%",
            "zIndex": 1
        }, controller.args[0], "tbxAdditionalField7"), extendConfig({
            "containerHeightMode": constants.TEXTBOX_FONT_METRICS_DRIVEN_HEIGHT,
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [3, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "tbxAdditionalField7"), extendConfig({
            "autoCorrect": false,
            "placeholderSkin": "defTextBoxPlaceholder"
        }, controller.args[2], "tbxAdditionalField7"));
        flxProductDetailsContainer14.add(lblAdditionalField7, tbxAdditionalField7);
        var flxProductDetailsContainer15 = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "clipBounds": true,
            "height": "100%",
            "id": "flxProductDetailsContainer15",
            "isVisible": true,
            "layoutType": kony.flex.FLOW_VERTICAL,
            "left": "15px",
            "isModalContainer": false,
            "skin": "slFbox",
            "top": "0px",
            "width": "31%",
            "zIndex": 1
        }, controller.args[0], "flxProductDetailsContainer15"), extendConfig({}, controller.args[1], "flxProductDetailsContainer15"), extendConfig({}, controller.args[2], "flxProductDetailsContainer15"));
        flxProductDetailsContainer15.setDefaultUnit(kony.flex.DP);
        var lblAdditionalField8 = new kony.ui.Label(extendConfig({
            "id": "lblAdditionalField8",
            "isVisible": true,
            "left": "0px",
            "skin": "sknLbl485C75LatoRegular13Px",
            "text": "Additional Field 8",
            "top": "0px",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, controller.args[0], "lblAdditionalField8"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblAdditionalField8"), extendConfig({}, controller.args[2], "lblAdditionalField8"));
        var tbxAdditionalField8 = new kony.ui.TextBox2(extendConfig({
            "autoCapitalize": constants.TEXTBOX_AUTO_CAPITALIZE_NONE,
            "focusSkin": "defTextBoxFocus",
            "height": "40dp",
            "id": "tbxAdditionalField8",
            "isVisible": true,
            "keyBoardStyle": constants.TEXTBOX_KEY_BOARD_STYLE_DEFAULT,
            "left": "0dp",
            "secureTextEntry": false,
            "skin": "txtD7d9e0",
            "textInputMode": constants.TEXTBOX_INPUT_MODE_ANY,
            "top": "15px",
            "width": "100%",
            "zIndex": 1
        }, controller.args[0], "tbxAdditionalField8"), extendConfig({
            "containerHeightMode": constants.TEXTBOX_FONT_METRICS_DRIVEN_HEIGHT,
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [3, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "tbxAdditionalField8"), extendConfig({
            "autoCorrect": false,
            "placeholderSkin": "defTextBoxPlaceholder"
        }, controller.args[2], "tbxAdditionalField8"));
        flxProductDetailsContainer15.add(lblAdditionalField8, tbxAdditionalField8);
        flxProductDetails5.add(flxProductDetailsContainer13, flxProductDetailsContainer14, flxProductDetailsContainer15);
        var flxProductDetails6 = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "clipBounds": true,
            "height": "100px",
            "id": "flxProductDetails6",
            "isVisible": true,
            "layoutType": kony.flex.FLOW_HORIZONTAL,
            "left": "0px",
            "isModalContainer": false,
            "skin": "slFbox",
            "top": "500px",
            "width": "100%",
            "zIndex": 1
        }, controller.args[0], "flxProductDetails6"), extendConfig({}, controller.args[1], "flxProductDetails6"), extendConfig({}, controller.args[2], "flxProductDetails6"));
        flxProductDetails6.setDefaultUnit(kony.flex.DP);
        var flxProductDetailsContainer16 = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "clipBounds": true,
            "height": "100%",
            "id": "flxProductDetailsContainer16",
            "isVisible": true,
            "layoutType": kony.flex.FLOW_VERTICAL,
            "left": "0px",
            "isModalContainer": false,
            "skin": "slFbox",
            "top": "0px",
            "width": "31%",
            "zIndex": 1
        }, controller.args[0], "flxProductDetailsContainer16"), extendConfig({}, controller.args[1], "flxProductDetailsContainer16"), extendConfig({}, controller.args[2], "flxProductDetailsContainer16"));
        flxProductDetailsContainer16.setDefaultUnit(kony.flex.DP);
        var lblAdditionalField9 = new kony.ui.Label(extendConfig({
            "id": "lblAdditionalField9",
            "isVisible": true,
            "left": "0px",
            "skin": "sknLbl485C75LatoRegular13Px",
            "text": "Additional Field 9",
            "top": "0px",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, controller.args[0], "lblAdditionalField9"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblAdditionalField9"), extendConfig({}, controller.args[2], "lblAdditionalField9"));
        var tbxAdditionalField9 = new kony.ui.TextBox2(extendConfig({
            "autoCapitalize": constants.TEXTBOX_AUTO_CAPITALIZE_NONE,
            "focusSkin": "defTextBoxFocus",
            "height": "40dp",
            "id": "tbxAdditionalField9",
            "isVisible": true,
            "keyBoardStyle": constants.TEXTBOX_KEY_BOARD_STYLE_DEFAULT,
            "left": "0dp",
            "secureTextEntry": false,
            "skin": "txtD7d9e0",
            "textInputMode": constants.TEXTBOX_INPUT_MODE_ANY,
            "top": "15px",
            "width": "100%",
            "zIndex": 1
        }, controller.args[0], "tbxAdditionalField9"), extendConfig({
            "containerHeightMode": constants.TEXTBOX_FONT_METRICS_DRIVEN_HEIGHT,
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [3, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "tbxAdditionalField9"), extendConfig({
            "autoCorrect": false,
            "placeholderSkin": "defTextBoxPlaceholder"
        }, controller.args[2], "tbxAdditionalField9"));
        flxProductDetailsContainer16.add(lblAdditionalField9, tbxAdditionalField9);
        var flxProductDetailsContainer17 = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "clipBounds": true,
            "height": "100%",
            "id": "flxProductDetailsContainer17",
            "isVisible": true,
            "layoutType": kony.flex.FLOW_VERTICAL,
            "left": "15px",
            "isModalContainer": false,
            "skin": "slFbox",
            "top": "0px",
            "width": "31%",
            "zIndex": 1
        }, controller.args[0], "flxProductDetailsContainer17"), extendConfig({}, controller.args[1], "flxProductDetailsContainer17"), extendConfig({}, controller.args[2], "flxProductDetailsContainer17"));
        flxProductDetailsContainer17.setDefaultUnit(kony.flex.DP);
        var lblAdditionalField10 = new kony.ui.Label(extendConfig({
            "id": "lblAdditionalField10",
            "isVisible": true,
            "left": "0px",
            "skin": "sknLbl485C75LatoRegular13Px",
            "text": "Additional Field 10",
            "top": "0px",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, controller.args[0], "lblAdditionalField10"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblAdditionalField10"), extendConfig({}, controller.args[2], "lblAdditionalField10"));
        var tbxAdditionalField10 = new kony.ui.TextBox2(extendConfig({
            "autoCapitalize": constants.TEXTBOX_AUTO_CAPITALIZE_NONE,
            "focusSkin": "defTextBoxFocus",
            "height": "40dp",
            "id": "tbxAdditionalField10",
            "isVisible": true,
            "keyBoardStyle": constants.TEXTBOX_KEY_BOARD_STYLE_DEFAULT,
            "left": "0dp",
            "secureTextEntry": false,
            "skin": "txtD7d9e0",
            "textInputMode": constants.TEXTBOX_INPUT_MODE_ANY,
            "top": "15px",
            "width": "100%",
            "zIndex": 1
        }, controller.args[0], "tbxAdditionalField10"), extendConfig({
            "containerHeightMode": constants.TEXTBOX_FONT_METRICS_DRIVEN_HEIGHT,
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [3, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "tbxAdditionalField10"), extendConfig({
            "autoCorrect": false,
            "placeholderSkin": "defTextBoxPlaceholder"
        }, controller.args[2], "tbxAdditionalField10"));
        flxProductDetailsContainer17.add(lblAdditionalField10, tbxAdditionalField10);
        var flxProductDetailsContainer18 = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "clipBounds": true,
            "height": "100%",
            "id": "flxProductDetailsContainer18",
            "isVisible": true,
            "layoutType": kony.flex.FLOW_VERTICAL,
            "left": "15px",
            "isModalContainer": false,
            "skin": "slFbox",
            "top": "0px",
            "width": "31%",
            "zIndex": 1
        }, controller.args[0], "flxProductDetailsContainer18"), extendConfig({}, controller.args[1], "flxProductDetailsContainer18"), extendConfig({}, controller.args[2], "flxProductDetailsContainer18"));
        flxProductDetailsContainer18.setDefaultUnit(kony.flex.DP);
        var lblAdditionalField11 = new kony.ui.Label(extendConfig({
            "id": "lblAdditionalField11",
            "isVisible": true,
            "left": "0px",
            "skin": "sknLbl485C75LatoRegular13Px",
            "text": "Additional Field 11",
            "top": "0px",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, controller.args[0], "lblAdditionalField11"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblAdditionalField11"), extendConfig({}, controller.args[2], "lblAdditionalField11"));
        var tbxAdditionalField11 = new kony.ui.TextBox2(extendConfig({
            "autoCapitalize": constants.TEXTBOX_AUTO_CAPITALIZE_NONE,
            "focusSkin": "defTextBoxFocus",
            "height": "40dp",
            "id": "tbxAdditionalField11",
            "isVisible": true,
            "keyBoardStyle": constants.TEXTBOX_KEY_BOARD_STYLE_DEFAULT,
            "left": "0dp",
            "secureTextEntry": false,
            "skin": "txtD7d9e0",
            "textInputMode": constants.TEXTBOX_INPUT_MODE_ANY,
            "top": "15px",
            "width": "100%",
            "zIndex": 1
        }, controller.args[0], "tbxAdditionalField11"), extendConfig({
            "containerHeightMode": constants.TEXTBOX_FONT_METRICS_DRIVEN_HEIGHT,
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [3, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "tbxAdditionalField11"), extendConfig({
            "autoCorrect": false,
            "placeholderSkin": "defTextBoxPlaceholder"
        }, controller.args[2], "tbxAdditionalField11"));
        flxProductDetailsContainer18.add(lblAdditionalField11, tbxAdditionalField11);
        flxProductDetails6.add(flxProductDetailsContainer16, flxProductDetailsContainer17, flxProductDetailsContainer18);
        var flxProductDetails7 = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "clipBounds": true,
            "height": "100px",
            "id": "flxProductDetails7",
            "isVisible": true,
            "layoutType": kony.flex.FLOW_HORIZONTAL,
            "left": "0px",
            "isModalContainer": false,
            "skin": "slFbox",
            "top": "600px",
            "width": "100%",
            "zIndex": 1
        }, controller.args[0], "flxProductDetails7"), extendConfig({}, controller.args[1], "flxProductDetails7"), extendConfig({}, controller.args[2], "flxProductDetails7"));
        flxProductDetails7.setDefaultUnit(kony.flex.DP);
        var flxProductDetailsContainer19 = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "clipBounds": true,
            "height": "100%",
            "id": "flxProductDetailsContainer19",
            "isVisible": true,
            "layoutType": kony.flex.FLOW_VERTICAL,
            "left": "0px",
            "isModalContainer": false,
            "skin": "slFbox",
            "top": "0px",
            "width": "31%",
            "zIndex": 1
        }, controller.args[0], "flxProductDetailsContainer19"), extendConfig({}, controller.args[1], "flxProductDetailsContainer19"), extendConfig({}, controller.args[2], "flxProductDetailsContainer19"));
        flxProductDetailsContainer19.setDefaultUnit(kony.flex.DP);
        var lblAdditionalField12 = new kony.ui.Label(extendConfig({
            "id": "lblAdditionalField12",
            "isVisible": true,
            "left": "0px",
            "skin": "sknLbl485C75LatoRegular13Px",
            "text": "Additional Field 12",
            "top": "0px",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, controller.args[0], "lblAdditionalField12"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblAdditionalField12"), extendConfig({}, controller.args[2], "lblAdditionalField12"));
        var tbxAdditionalField12 = new kony.ui.TextBox2(extendConfig({
            "autoCapitalize": constants.TEXTBOX_AUTO_CAPITALIZE_NONE,
            "focusSkin": "defTextBoxFocus",
            "height": "40dp",
            "id": "tbxAdditionalField12",
            "isVisible": true,
            "keyBoardStyle": constants.TEXTBOX_KEY_BOARD_STYLE_DEFAULT,
            "left": "0dp",
            "secureTextEntry": false,
            "skin": "txtD7d9e0",
            "textInputMode": constants.TEXTBOX_INPUT_MODE_ANY,
            "top": "15px",
            "width": "100%",
            "zIndex": 1
        }, controller.args[0], "tbxAdditionalField12"), extendConfig({
            "containerHeightMode": constants.TEXTBOX_FONT_METRICS_DRIVEN_HEIGHT,
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [3, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "tbxAdditionalField12"), extendConfig({
            "autoCorrect": false,
            "placeholderSkin": "defTextBoxPlaceholder"
        }, controller.args[2], "tbxAdditionalField12"));
        flxProductDetailsContainer19.add(lblAdditionalField12, tbxAdditionalField12);
        var flxProductDetailsContainer20 = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "clipBounds": true,
            "height": "100%",
            "id": "flxProductDetailsContainer20",
            "isVisible": true,
            "layoutType": kony.flex.FLOW_VERTICAL,
            "left": "15px",
            "isModalContainer": false,
            "skin": "slFbox",
            "top": "0px",
            "width": "31%",
            "zIndex": 1
        }, controller.args[0], "flxProductDetailsContainer20"), extendConfig({}, controller.args[1], "flxProductDetailsContainer20"), extendConfig({}, controller.args[2], "flxProductDetailsContainer20"));
        flxProductDetailsContainer20.setDefaultUnit(kony.flex.DP);
        var lblAdditionalField13 = new kony.ui.Label(extendConfig({
            "id": "lblAdditionalField13",
            "isVisible": true,
            "left": "0px",
            "skin": "sknLbl485C75LatoRegular13Px",
            "text": "Additional Field 13",
            "top": "0px",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, controller.args[0], "lblAdditionalField13"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblAdditionalField13"), extendConfig({}, controller.args[2], "lblAdditionalField13"));
        var tbxAdditionalField13 = new kony.ui.TextBox2(extendConfig({
            "autoCapitalize": constants.TEXTBOX_AUTO_CAPITALIZE_NONE,
            "focusSkin": "defTextBoxFocus",
            "height": "40dp",
            "id": "tbxAdditionalField13",
            "isVisible": true,
            "keyBoardStyle": constants.TEXTBOX_KEY_BOARD_STYLE_DEFAULT,
            "left": "0dp",
            "secureTextEntry": false,
            "skin": "txtD7d9e0",
            "textInputMode": constants.TEXTBOX_INPUT_MODE_ANY,
            "top": "15px",
            "width": "100%",
            "zIndex": 1
        }, controller.args[0], "tbxAdditionalField13"), extendConfig({
            "containerHeightMode": constants.TEXTBOX_FONT_METRICS_DRIVEN_HEIGHT,
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [3, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "tbxAdditionalField13"), extendConfig({
            "autoCorrect": false,
            "placeholderSkin": "defTextBoxPlaceholder"
        }, controller.args[2], "tbxAdditionalField13"));
        flxProductDetailsContainer20.add(lblAdditionalField13, tbxAdditionalField13);
        var flxProductDetailsContainer21 = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "clipBounds": true,
            "height": "100%",
            "id": "flxProductDetailsContainer21",
            "isVisible": true,
            "layoutType": kony.flex.FLOW_VERTICAL,
            "left": "15px",
            "isModalContainer": false,
            "skin": "slFbox",
            "top": "0px",
            "width": "31%",
            "zIndex": 1
        }, controller.args[0], "flxProductDetailsContainer21"), extendConfig({}, controller.args[1], "flxProductDetailsContainer21"), extendConfig({}, controller.args[2], "flxProductDetailsContainer21"));
        flxProductDetailsContainer21.setDefaultUnit(kony.flex.DP);
        var lblAdditionalField14 = new kony.ui.Label(extendConfig({
            "id": "lblAdditionalField14",
            "isVisible": true,
            "left": "0px",
            "skin": "sknLbl485C75LatoRegular13Px",
            "text": "Additional Field 14",
            "top": "0px",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, controller.args[0], "lblAdditionalField14"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblAdditionalField14"), extendConfig({}, controller.args[2], "lblAdditionalField14"));
        var tbxAdditionalField14 = new kony.ui.TextBox2(extendConfig({
            "autoCapitalize": constants.TEXTBOX_AUTO_CAPITALIZE_NONE,
            "focusSkin": "defTextBoxFocus",
            "height": "40dp",
            "id": "tbxAdditionalField14",
            "isVisible": true,
            "keyBoardStyle": constants.TEXTBOX_KEY_BOARD_STYLE_DEFAULT,
            "left": "0dp",
            "secureTextEntry": false,
            "skin": "txtD7d9e0",
            "textInputMode": constants.TEXTBOX_INPUT_MODE_ANY,
            "top": "15px",
            "width": "100%",
            "zIndex": 1
        }, controller.args[0], "tbxAdditionalField14"), extendConfig({
            "containerHeightMode": constants.TEXTBOX_FONT_METRICS_DRIVEN_HEIGHT,
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [3, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "tbxAdditionalField14"), extendConfig({
            "autoCorrect": false,
            "placeholderSkin": "defTextBoxPlaceholder"
        }, controller.args[2], "tbxAdditionalField14"));
        flxProductDetailsContainer21.add(lblAdditionalField14, tbxAdditionalField14);
        flxProductDetails7.add(flxProductDetailsContainer19, flxProductDetailsContainer20, flxProductDetailsContainer21);
        var flxProductDetails8 = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "clipBounds": true,
            "height": "100px",
            "id": "flxProductDetails8",
            "isVisible": true,
            "layoutType": kony.flex.FLOW_HORIZONTAL,
            "left": "0px",
            "isModalContainer": false,
            "skin": "slFbox",
            "top": "700px",
            "width": "100%",
            "zIndex": 1
        }, controller.args[0], "flxProductDetails8"), extendConfig({}, controller.args[1], "flxProductDetails8"), extendConfig({}, controller.args[2], "flxProductDetails8"));
        flxProductDetails8.setDefaultUnit(kony.flex.DP);
        var flxProductDetailsContainer22 = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "clipBounds": true,
            "height": "100%",
            "id": "flxProductDetailsContainer22",
            "isVisible": true,
            "layoutType": kony.flex.FLOW_VERTICAL,
            "left": "0px",
            "isModalContainer": false,
            "skin": "slFbox",
            "top": "0px",
            "width": "31%",
            "zIndex": 1
        }, controller.args[0], "flxProductDetailsContainer22"), extendConfig({}, controller.args[1], "flxProductDetailsContainer22"), extendConfig({}, controller.args[2], "flxProductDetailsContainer22"));
        flxProductDetailsContainer22.setDefaultUnit(kony.flex.DP);
        var lblAdditionalField15 = new kony.ui.Label(extendConfig({
            "id": "lblAdditionalField15",
            "isVisible": true,
            "left": "0px",
            "skin": "sknLbl485C75LatoRegular13Px",
            "text": "Additional Field 15",
            "top": "0px",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, controller.args[0], "lblAdditionalField15"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblAdditionalField15"), extendConfig({}, controller.args[2], "lblAdditionalField15"));
        var tbxAdditionalField15 = new kony.ui.TextBox2(extendConfig({
            "autoCapitalize": constants.TEXTBOX_AUTO_CAPITALIZE_NONE,
            "focusSkin": "defTextBoxFocus",
            "height": "40dp",
            "id": "tbxAdditionalField15",
            "isVisible": true,
            "keyBoardStyle": constants.TEXTBOX_KEY_BOARD_STYLE_DEFAULT,
            "left": "0dp",
            "secureTextEntry": false,
            "skin": "txtD7d9e0",
            "textInputMode": constants.TEXTBOX_INPUT_MODE_ANY,
            "top": "15px",
            "width": "100%",
            "zIndex": 1
        }, controller.args[0], "tbxAdditionalField15"), extendConfig({
            "containerHeightMode": constants.TEXTBOX_FONT_METRICS_DRIVEN_HEIGHT,
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [3, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "tbxAdditionalField15"), extendConfig({
            "autoCorrect": false,
            "placeholderSkin": "defTextBoxPlaceholder"
        }, controller.args[2], "tbxAdditionalField15"));
        flxProductDetailsContainer22.add(lblAdditionalField15, tbxAdditionalField15);
        flxProductDetails8.add(flxProductDetailsContainer22);
        flxProductDetails.add(flxProductDetails1, flxProductDetails2, flxProductDetailsContainer7, flxProductDetails3, flxProductDetails4, flxProductDetails5, flxProductDetails6, flxProductDetails7, flxProductDetails8);
        var flxProductDescription = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
            "clipBounds": true,
            "id": "flxProductDescription",
            "isVisible": false,
            "layoutType": kony.flex.FLOW_VERTICAL,
            "left": "20dp",
            "isModalContainer": false,
            "right": "20dp",
            "skin": "slFbox",
            "top": "20dp",
            "zIndex": 1
        }, controller.args[0], "flxProductDescription"), extendConfig({}, controller.args[1], "flxProductDescription"), extendConfig({}, controller.args[2], "flxProductDescription"));
        flxProductDescription.setDefaultUnit(kony.flex.DP);
        var flxDescriptionContainer = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
            "clipBounds": true,
            "id": "flxDescriptionContainer",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "left": "0px",
            "isModalContainer": false,
            "skin": "slFbox",
            "top": "0px",
            "width": "100%",
            "zIndex": 1
        }, controller.args[0], "flxDescriptionContainer"), extendConfig({}, controller.args[1], "flxDescriptionContainer"), extendConfig({}, controller.args[2], "flxDescriptionContainer"));
        flxDescriptionContainer.setDefaultUnit(kony.flex.DP);
        var flxDescriptionTop = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
            "clipBounds": true,
            "id": "flxDescriptionTop",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "left": "0dp",
            "isModalContainer": false,
            "skin": "slFbox",
            "top": "0dp",
            "width": "100%"
        }, controller.args[0], "flxDescriptionTop"), extendConfig({}, controller.args[1], "flxDescriptionTop"), extendConfig({}, controller.args[2], "flxDescriptionTop"));
        flxDescriptionTop.setDefaultUnit(kony.flex.DP);
        var lblDescription = new kony.ui.Label(extendConfig({
            "id": "lblDescription",
            "isVisible": true,
            "left": "0px",
            "skin": "sknLbl485C75LatoRegular13Px",
            "i18n_text": "kony.i18n.getLocalizedString(\"i18n.products.Description_Optional\")",
            "top": "0px",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, controller.args[0], "lblDescription"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblDescription"), extendConfig({}, controller.args[2], "lblDescription"));
        var lblDescTextCounter = new kony.ui.Label(extendConfig({
            "id": "lblDescTextCounter",
            "isVisible": true,
            "right": "0px",
            "skin": "sknlblLato696c7313px",
            "text": "1/150",
            "top": "0px",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, controller.args[0], "lblDescTextCounter"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblDescTextCounter"), extendConfig({}, controller.args[2], "lblDescTextCounter"));
        flxDescriptionTop.add(lblDescription, lblDescTextCounter);
        var txtAreaDescription = new kony.ui.TextArea2(extendConfig({
            "autoCapitalize": constants.TEXTAREA_AUTO_CAPITALIZE_NONE,
            "focusSkin": "defTextAreaFocus",
            "height": "72px",
            "id": "txtAreaDescription",
            "isVisible": true,
            "keyBoardStyle": constants.TEXTAREA_KEY_BOARD_STYLE_DEFAULT,
            "left": "0px",
            "maxTextLength": 150,
            "numberOfVisibleLines": 3,
            "i18n_placeholder": "kony.i18n.getLocalizedString(\"i18n.products.Description\")",
            "skin": "skntxtAread7d9e0",
            "textInputMode": constants.TEXTAREA_INPUT_MODE_ANY,
            "top": "25px",
            "width": "100%",
            "zIndex": 1
        }, controller.args[0], "txtAreaDescription"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_TOP_LEFT,
            "padding": [2, 2, 2, 2],
            "paddingInPixel": false
        }, controller.args[1], "txtAreaDescription"), extendConfig({
            "autoCorrect": false,
            "placeholderSkin": "defTextAreaPlaceholder"
        }, controller.args[2], "txtAreaDescription"));
        flxDescriptionContainer.add(flxDescriptionTop, txtAreaDescription);
        var flxDetailedDescriptionContainer = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
            "clipBounds": true,
            "id": "flxDetailedDescriptionContainer",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "left": "0px",
            "isModalContainer": false,
            "skin": "slFbox",
            "top": "30px",
            "width": "100%",
            "zIndex": 1
        }, controller.args[0], "flxDetailedDescriptionContainer"), extendConfig({}, controller.args[1], "flxDetailedDescriptionContainer"), extendConfig({}, controller.args[2], "flxDetailedDescriptionContainer"));
        flxDetailedDescriptionContainer.setDefaultUnit(kony.flex.DP);
        var flxDetailedDescriptionTop = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
            "clipBounds": true,
            "id": "flxDetailedDescriptionTop",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "left": "0dp",
            "isModalContainer": false,
            "skin": "slFbox",
            "top": "0dp",
            "width": "100%"
        }, controller.args[0], "flxDetailedDescriptionTop"), extendConfig({}, controller.args[1], "flxDetailedDescriptionTop"), extendConfig({}, controller.args[2], "flxDetailedDescriptionTop"));
        flxDetailedDescriptionTop.setDefaultUnit(kony.flex.DP);
        var lblDetailedDescription = new kony.ui.Label(extendConfig({
            "id": "lblDetailedDescription",
            "isVisible": true,
            "left": "0px",
            "skin": "sknLbl485C75LatoRegular13Px",
            "i18n_text": "kony.i18n.getLocalizedString(\"i18n.products.Detailed Description_Optional\")",
            "top": "0px",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, controller.args[0], "lblDetailedDescription"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblDetailedDescription"), extendConfig({}, controller.args[2], "lblDetailedDescription"));
        var lblDetailedDescTextCounter = new kony.ui.Label(extendConfig({
            "id": "lblDetailedDescTextCounter",
            "isVisible": true,
            "right": "0px",
            "skin": "sknlblLato696c7313px",
            "text": "1/250",
            "top": "0px",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, controller.args[0], "lblDetailedDescTextCounter"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblDetailedDescTextCounter"), extendConfig({}, controller.args[2], "lblDetailedDescTextCounter"));
        flxDetailedDescriptionTop.add(lblDetailedDescription, lblDetailedDescTextCounter);
        var txtAreaDetailedDescription = new kony.ui.TextArea2(extendConfig({
            "autoCapitalize": constants.TEXTAREA_AUTO_CAPITALIZE_NONE,
            "focusSkin": "defTextAreaFocus",
            "height": "148px",
            "id": "txtAreaDetailedDescription",
            "isVisible": true,
            "keyBoardStyle": constants.TEXTAREA_KEY_BOARD_STYLE_DEFAULT,
            "left": "0px",
            "maxTextLength": 250,
            "numberOfVisibleLines": 3,
            "i18n_placeholder": "kony.i18n.getLocalizedString(\"i18n.products.Detailed Description\")",
            "skin": "skntxtAread7d9e0",
            "textInputMode": constants.TEXTAREA_INPUT_MODE_ANY,
            "top": "25px",
            "width": "100%",
            "zIndex": 1
        }, controller.args[0], "txtAreaDetailedDescription"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_TOP_LEFT,
            "padding": [2, 2, 2, 2],
            "paddingInPixel": false
        }, controller.args[1], "txtAreaDetailedDescription"), extendConfig({
            "autoCorrect": false,
            "placeholderSkin": "defTextAreaPlaceholder"
        }, controller.args[2], "txtAreaDetailedDescription"));
        flxDetailedDescriptionContainer.add(flxDetailedDescriptionTop, txtAreaDetailedDescription);
        var flxNotesContainer = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
            "clipBounds": true,
            "id": "flxNotesContainer",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "left": "0px",
            "isModalContainer": false,
            "skin": "slFbox",
            "top": "30px",
            "width": "100%",
            "zIndex": 1
        }, controller.args[0], "flxNotesContainer"), extendConfig({}, controller.args[1], "flxNotesContainer"), extendConfig({}, controller.args[2], "flxNotesContainer"));
        flxNotesContainer.setDefaultUnit(kony.flex.DP);
        var flxNotesTopHeader = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
            "clipBounds": true,
            "id": "flxNotesTopHeader",
            "isVisible": true,
            "layoutType": kony.flex.FLOW_HORIZONTAL,
            "left": "0dp",
            "isModalContainer": false,
            "skin": "slFbox",
            "top": "0dp",
            "width": "100%"
        }, controller.args[0], "flxNotesTopHeader"), extendConfig({}, controller.args[1], "flxNotesTopHeader"), extendConfig({}, controller.args[2], "flxNotesTopHeader"));
        flxNotesTopHeader.setDefaultUnit(kony.flex.DP);
        var lblNotes = new kony.ui.Label(extendConfig({
            "id": "lblNotes",
            "isVisible": true,
            "left": "0px",
            "skin": "sknLbl485C75LatoRegular13Px",
            "i18n_text": "kony.i18n.getLocalizedString(\"i18n.products.Notes_Optional\")",
            "top": "0px",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, controller.args[0], "lblNotes"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblNotes"), extendConfig({}, controller.args[2], "lblNotes"));
        var lblFontIconNotesInfo = new kony.ui.Label(extendConfig({
            "height": "14px",
            "id": "lblFontIconNotesInfo",
            "isVisible": true,
            "left": "2px",
            "skin": "sknFontIcon485C7518px",
            "text": "",
            "top": "0px",
            "width": "18px",
            "zIndex": 1
        }, controller.args[0], "lblFontIconNotesInfo"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblFontIconNotesInfo"), extendConfig({
            "toolTip": "Use |+| symbol as line separator for Infinity Onboarding."
        }, controller.args[2], "lblFontIconNotesInfo"));
        flxNotesTopHeader.add(lblNotes, lblFontIconNotesInfo);
        var txtAreaNotes = new kony.ui.TextArea2(extendConfig({
            "autoCapitalize": constants.TEXTAREA_AUTO_CAPITALIZE_NONE,
            "focusSkin": "defTextAreaFocus",
            "height": "148px",
            "id": "txtAreaNotes",
            "isVisible": true,
            "keyBoardStyle": constants.TEXTAREA_KEY_BOARD_STYLE_DEFAULT,
            "left": "0px",
            "numberOfVisibleLines": 3,
            "i18n_placeholder": "kony.i18n.getLocalizedString(\"i18n.products.Notes\")",
            "skin": "skntxtAread7d9e0",
            "textInputMode": constants.TEXTAREA_INPUT_MODE_ANY,
            "top": "25px",
            "width": "100%",
            "zIndex": 1
        }, controller.args[0], "txtAreaNotes"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_TOP_LEFT,
            "padding": [2, 2, 2, 2],
            "paddingInPixel": false
        }, controller.args[1], "txtAreaNotes"), extendConfig({
            "autoCorrect": false,
            "placeholderSkin": "defTextAreaPlaceholder"
        }, controller.args[2], "txtAreaNotes"));
        flxNotesContainer.add(flxNotesTopHeader, txtAreaNotes);
        var flxDisclosureContainer = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
            "clipBounds": true,
            "id": "flxDisclosureContainer",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "left": "0px",
            "isModalContainer": false,
            "skin": "slFbox",
            "top": "30px",
            "width": "100%",
            "zIndex": 1
        }, controller.args[0], "flxDisclosureContainer"), extendConfig({}, controller.args[1], "flxDisclosureContainer"), extendConfig({}, controller.args[2], "flxDisclosureContainer"));
        flxDisclosureContainer.setDefaultUnit(kony.flex.DP);
        var lblDisclosure = new kony.ui.Label(extendConfig({
            "id": "lblDisclosure",
            "isVisible": true,
            "left": "0px",
            "skin": "sknLbl485C75LatoRegular13Px",
            "i18n_text": "kony.i18n.getLocalizedString(\"i18n.products.Disclosure_Optional\")",
            "top": "0px",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, controller.args[0], "lblDisclosure"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblDisclosure"), extendConfig({}, controller.args[2], "lblDisclosure"));
        var txtAreaDisclosure = new kony.ui.TextArea2(extendConfig({
            "autoCapitalize": constants.TEXTAREA_AUTO_CAPITALIZE_NONE,
            "focusSkin": "defTextAreaFocus",
            "height": "148px",
            "id": "txtAreaDisclosure",
            "isVisible": true,
            "keyBoardStyle": constants.TEXTAREA_KEY_BOARD_STYLE_DEFAULT,
            "left": "0px",
            "numberOfVisibleLines": 3,
            "i18n_placeholder": "kony.i18n.getLocalizedString(\"i18n.products.Disclosure\")",
            "skin": "skntxtAread7d9e0",
            "textInputMode": constants.TEXTAREA_INPUT_MODE_ANY,
            "top": "25px",
            "width": "100%",
            "zIndex": 1
        }, controller.args[0], "txtAreaDisclosure"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_TOP_LEFT,
            "padding": [2, 2, 2, 2],
            "paddingInPixel": false
        }, controller.args[1], "txtAreaDisclosure"), extendConfig({
            "autoCorrect": false,
            "placeholderSkin": "defTextAreaPlaceholder"
        }, controller.args[2], "txtAreaDisclosure"));
        flxDisclosureContainer.add(lblDisclosure, txtAreaDisclosure);
        var flxTermsAndConditionsContainer = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
            "bottom": "30px",
            "clipBounds": true,
            "id": "flxTermsAndConditionsContainer",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "left": "0px",
            "isModalContainer": false,
            "skin": "slFbox",
            "top": "30px",
            "width": "100%",
            "zIndex": 1
        }, controller.args[0], "flxTermsAndConditionsContainer"), extendConfig({}, controller.args[1], "flxTermsAndConditionsContainer"), extendConfig({}, controller.args[2], "flxTermsAndConditionsContainer"));
        flxTermsAndConditionsContainer.setDefaultUnit(kony.flex.DP);
        var lblTermsAndConditions = new kony.ui.Label(extendConfig({
            "id": "lblTermsAndConditions",
            "isVisible": true,
            "left": "0px",
            "skin": "sknLbl485C75LatoRegular13Px",
            "i18n_text": "kony.i18n.getLocalizedString(\"i18n.products.Terms and Conditions_Optional\")",
            "top": "0px",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, controller.args[0], "lblTermsAndConditions"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblTermsAndConditions"), extendConfig({}, controller.args[2], "lblTermsAndConditions"));
        var txtAreaTermsAndConditions = new kony.ui.TextArea2(extendConfig({
            "autoCapitalize": constants.TEXTAREA_AUTO_CAPITALIZE_NONE,
            "focusSkin": "defTextAreaFocus",
            "height": "148px",
            "id": "txtAreaTermsAndConditions",
            "isVisible": true,
            "keyBoardStyle": constants.TEXTAREA_KEY_BOARD_STYLE_DEFAULT,
            "left": "0px",
            "numberOfVisibleLines": 3,
            "i18n_placeholder": "kony.i18n.getLocalizedString(\"i18n.products.Terms and Conditions\")",
            "skin": "skntxtAread7d9e0",
            "textInputMode": constants.TEXTAREA_INPUT_MODE_ANY,
            "top": "25px",
            "width": "100%",
            "zIndex": 1
        }, controller.args[0], "txtAreaTermsAndConditions"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_TOP_LEFT,
            "padding": [2, 2, 2, 2],
            "paddingInPixel": false
        }, controller.args[1], "txtAreaTermsAndConditions"), extendConfig({
            "autoCorrect": false,
            "placeholderSkin": "defTextAreaPlaceholder"
        }, controller.args[2], "txtAreaTermsAndConditions"));
        flxTermsAndConditionsContainer.add(lblTermsAndConditions, txtAreaTermsAndConditions);
        flxProductDescription.add(flxDescriptionContainer, flxDetailedDescriptionContainer, flxNotesContainer, flxDisclosureContainer, flxTermsAndConditionsContainer);
        var flxProductFeatures = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
            "clipBounds": true,
            "id": "flxProductFeatures",
            "isVisible": false,
            "layoutType": kony.flex.FLOW_VERTICAL,
            "left": "10px",
            "isModalContainer": false,
            "right": "10px",
            "skin": "slFbox",
            "top": "20px",
            "zIndex": 1
        }, controller.args[0], "flxProductFeatures"), extendConfig({}, controller.args[1], "flxProductFeatures"), extendConfig({}, controller.args[2], "flxProductFeatures"));
        flxProductFeatures.setDefaultUnit(kony.flex.DP);
        var flxFeaturesHeader = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
            "clipBounds": true,
            "id": "flxFeaturesHeader",
            "isVisible": true,
            "layoutType": kony.flex.FLOW_VERTICAL,
            "left": "0px",
            "isModalContainer": false,
            "skin": "slFbox",
            "top": "0px",
            "width": "100%",
            "zIndex": 1
        }, controller.args[0], "flxFeaturesHeader"), extendConfig({}, controller.args[1], "flxFeaturesHeader"), extendConfig({}, controller.args[2], "flxFeaturesHeader"));
        flxFeaturesHeader.setDefaultUnit(kony.flex.DP);
        var flxFeaturesTop = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "clipBounds": true,
            "height": "22px",
            "id": "flxFeaturesTop",
            "isVisible": true,
            "layoutType": kony.flex.FLOW_HORIZONTAL,
            "left": "0px",
            "isModalContainer": false,
            "skin": "slFbox",
            "top": "0px",
            "width": "100%"
        }, controller.args[0], "flxFeaturesTop"), extendConfig({}, controller.args[1], "flxFeaturesTop"), extendConfig({}, controller.args[2], "flxFeaturesTop"));
        flxFeaturesTop.setDefaultUnit(kony.flex.DP);
        var flxFeaturesTopWrapper1 = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "clipBounds": true,
            "height": "100%",
            "id": "flxFeaturesTopWrapper1",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "left": "0px",
            "isModalContainer": false,
            "skin": "slFbox",
            "top": "0px",
            "width": "50%"
        }, controller.args[0], "flxFeaturesTopWrapper1"), extendConfig({}, controller.args[1], "flxFeaturesTopWrapper1"), extendConfig({}, controller.args[2], "flxFeaturesTopWrapper1"));
        flxFeaturesTopWrapper1.setDefaultUnit(kony.flex.DP);
        var lblFeaturesHeader = new kony.ui.Label(extendConfig({
            "id": "lblFeaturesHeader",
            "isVisible": true,
            "left": "10px",
            "skin": "sknLbl192B45LatoRegular14px",
            "text": "Features (0)",
            "top": "0px",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, controller.args[0], "lblFeaturesHeader"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblFeaturesHeader"), extendConfig({}, controller.args[2], "lblFeaturesHeader"));
        flxFeaturesTopWrapper1.add(lblFeaturesHeader);
        var flxFeaturesTopWrapper2 = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "clipBounds": true,
            "height": "100%",
            "id": "flxFeaturesTopWrapper2",
            "isVisible": true,
            "layoutType": kony.flex.FLOW_HORIZONTAL,
            "reverseLayoutDirection": true,
            "left": "0%",
            "isModalContainer": false,
            "skin": "slFbox",
            "top": "0%",
            "width": "50%"
        }, controller.args[0], "flxFeaturesTopWrapper2"), extendConfig({}, controller.args[1], "flxFeaturesTopWrapper2"), extendConfig({}, controller.args[2], "flxFeaturesTopWrapper2"));
        flxFeaturesTopWrapper2.setDefaultUnit(kony.flex.DP);
        var btnAddFeature1 = new kony.ui.Button(extendConfig({
            "height": "22px",
            "id": "btnAddFeature1",
            "isVisible": false,
            "right": "10px",
            "skin": "sknbtnf7f7faLatoReg13px485c75Rad20px",
            "i18n_text": "kony.i18n.getLocalizedString(\"i18n.products.Add_Feature\")",
            "top": "0px",
            "width": "95px",
            "zIndex": 1
        }, controller.args[0], "btnAddFeature1"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_CENTER,
            "displayText": true,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "btnAddFeature1"), extendConfig({}, controller.args[2], "btnAddFeature1"));
        flxFeaturesTopWrapper2.add(btnAddFeature1);
        flxFeaturesTop.add(flxFeaturesTopWrapper1, flxFeaturesTopWrapper2);
        var flxSeparator1 = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "clipBounds": true,
            "height": "1px",
            "id": "flxSeparator1",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "left": "0px",
            "isModalContainer": false,
            "skin": "sknFlxFFFFFFbrdre1e5ed",
            "top": "15px",
            "width": "100%",
            "zIndex": 1
        }, controller.args[0], "flxSeparator1"), extendConfig({}, controller.args[1], "flxSeparator1"), extendConfig({}, controller.args[2], "flxSeparator1"));
        flxSeparator1.setDefaultUnit(kony.flex.DP);
        flxSeparator1.add();
        var flxNoFeaturesError = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "clipBounds": true,
            "height": "40dp",
            "id": "flxNoFeaturesError",
            "isVisible": false,
            "layoutType": kony.flex.FLOW_HORIZONTAL,
            "left": "0",
            "isModalContainer": false,
            "skin": "sknBorderRed",
            "top": "10px",
            "width": "100%",
            "zIndex": 10
        }, controller.args[0], "flxNoFeaturesError"), extendConfig({}, controller.args[1], "flxNoFeaturesError"), extendConfig({}, controller.args[2], "flxNoFeaturesError"));
        flxNoFeaturesError.setDefaultUnit(kony.flex.DP);
        var flxNoFeaturesErrorIcon = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "clipBounds": true,
            "height": "100%",
            "id": "flxNoFeaturesErrorIcon",
            "isVisible": true,
            "layoutType": kony.flex.FLOW_HORIZONTAL,
            "left": "0dp",
            "isModalContainer": false,
            "skin": "sknRedFill",
            "top": "0dp",
            "width": "10%"
        }, controller.args[0], "flxNoFeaturesErrorIcon"), extendConfig({}, controller.args[1], "flxNoFeaturesErrorIcon"), extendConfig({}, controller.args[2], "flxNoFeaturesErrorIcon"));
        flxNoFeaturesErrorIcon.setDefaultUnit(kony.flex.DP);
        var lblNoFeaturesErrorIcon = new kony.ui.Label(extendConfig({
            "centerX": "50%",
            "centerY": "50%",
            "height": "20dp",
            "id": "lblNoFeaturesErrorIcon",
            "isVisible": true,
            "left": "0dp",
            "skin": "sknIcon20pxWhite",
            "text": "",
            "top": "0dp",
            "width": "20dp",
            "zIndex": 1
        }, controller.args[0], "lblNoFeaturesErrorIcon"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblNoFeaturesErrorIcon"), extendConfig({}, controller.args[2], "lblNoFeaturesErrorIcon"));
        flxNoFeaturesErrorIcon.add(lblNoFeaturesErrorIcon);
        var lblNoFeaturesErrorText = new kony.ui.Label(extendConfig({
            "centerY": "50%",
            "height": "15dp",
            "id": "lblNoFeaturesErrorText",
            "isVisible": true,
            "left": "20dp",
            "skin": "sknlblError",
            "text": "Add atleast 1 feature to procced further.",
            "top": "0dp",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, controller.args[0], "lblNoFeaturesErrorText"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblNoFeaturesErrorText"), extendConfig({}, controller.args[2], "lblNoFeaturesErrorText"));
        flxNoFeaturesError.add(flxNoFeaturesErrorIcon, lblNoFeaturesErrorText);
        flxFeaturesHeader.add(flxFeaturesTop, flxSeparator1, flxNoFeaturesError);
        var flxNoSelectedFeatures = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "bottom": "0px",
            "clipBounds": true,
            "height": "380px",
            "id": "flxNoSelectedFeatures",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "left": "0px",
            "isModalContainer": false,
            "skin": "slFbox",
            "top": "0px",
            "width": "100%",
            "zIndex": 1
        }, controller.args[0], "flxNoSelectedFeatures"), extendConfig({}, controller.args[1], "flxNoSelectedFeatures"), extendConfig({}, controller.args[2], "flxNoSelectedFeatures"));
        flxNoSelectedFeatures.setDefaultUnit(kony.flex.DP);
        var flxNoSelectedDisplayText = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "centerX": "50%",
            "centerY": "50%",
            "clipBounds": true,
            "height": "160px",
            "id": "flxNoSelectedDisplayText",
            "isVisible": true,
            "layoutType": kony.flex.FLOW_VERTICAL,
            "isModalContainer": false,
            "skin": "slFbox",
            "width": "200px"
        }, controller.args[0], "flxNoSelectedDisplayText"), extendConfig({}, controller.args[1], "flxNoSelectedDisplayText"), extendConfig({}, controller.args[2], "flxNoSelectedDisplayText"));
        flxNoSelectedDisplayText.setDefaultUnit(kony.flex.DP);
        var imgCreateNewFeature = new kony.ui.Image2(extendConfig({
            "centerX": "50%",
            "height": "73px",
            "id": "imgCreateNewFeature",
            "isVisible": true,
            "skin": "slImage",
            "src": "group.png",
            "top": "0px",
            "width": "67px",
            "zIndex": 1
        }, controller.args[0], "imgCreateNewFeature"), extendConfig({
            "imageScaleMode": constants.IMAGE_SCALE_MODE_FIT_TO_DIMENSIONS,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "imgCreateNewFeature"), extendConfig({}, controller.args[2], "imgCreateNewFeature"));
        var lblFeaturesCanBeAdded = new kony.ui.Label(extendConfig({
            "centerX": "50%",
            "id": "lblFeaturesCanBeAdded",
            "isVisible": true,
            "skin": "sknLbl485C75LatoRegular13Px",
            "i18n_text": "kony.i18n.getLocalizedString(\"i18n.products.Product_Features_can_be_added\")",
            "top": "20px",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, controller.args[0], "lblFeaturesCanBeAdded"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblFeaturesCanBeAdded"), extendConfig({}, controller.args[2], "lblFeaturesCanBeAdded"));
        var btnAddFeature2 = new kony.ui.Button(extendConfig({
            "centerX": "50%",
            "height": "22px",
            "id": "btnAddFeature2",
            "isVisible": true,
            "skin": "sknbtnf7f7faLatoReg13px485c75Rad20px",
            "i18n_text": "kony.i18n.getLocalizedString(\"i18n.products.Add_Feature\")",
            "top": "20px",
            "width": "100px",
            "zIndex": 1
        }, controller.args[0], "btnAddFeature2"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_CENTER,
            "displayText": true,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "btnAddFeature2"), extendConfig({}, controller.args[2], "btnAddFeature2"));
        flxNoSelectedDisplayText.add(imgCreateNewFeature, lblFeaturesCanBeAdded, btnAddFeature2);
        flxNoSelectedFeatures.add(flxNoSelectedDisplayText);
        var flxViewSelectedFeatures = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
            "bottom": "0px",
            "clipBounds": true,
            "id": "flxViewSelectedFeatures",
            "isVisible": false,
            "layoutType": kony.flex.FLOW_VERTICAL,
            "left": "0px",
            "isModalContainer": false,
            "skin": "slFbox",
            "top": "20px",
            "width": "100%",
            "zIndex": 1
        }, controller.args[0], "flxViewSelectedFeatures"), extendConfig({}, controller.args[1], "flxViewSelectedFeatures"), extendConfig({}, controller.args[2], "flxViewSelectedFeatures"));
        flxViewSelectedFeatures.setDefaultUnit(kony.flex.DP);
        var flxProductFeaturesSegHeader = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "clipBounds": true,
            "height": "35px",
            "id": "flxProductFeaturesSegHeader",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "left": "0px",
            "isModalContainer": false,
            "skin": "slFbox",
            "top": "0px",
            "width": "100%",
            "zIndex": 1
        }, controller.args[0], "flxProductFeaturesSegHeader"), extendConfig({}, controller.args[1], "flxProductFeaturesSegHeader"), extendConfig({}, controller.args[2], "flxProductFeaturesSegHeader"));
        flxProductFeaturesSegHeader.setDefaultUnit(kony.flex.DP);
        var flxFeatureName = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
            "clipBounds": true,
            "id": "flxFeatureName",
            "isVisible": true,
            "layoutType": kony.flex.FLOW_HORIZONTAL,
            "left": "10px",
            "isModalContainer": false,
            "skin": "slFbox",
            "top": "0px",
            "width": "150px",
            "zIndex": 1
        }, controller.args[0], "flxFeatureName"), extendConfig({}, controller.args[1], "flxFeatureName"), extendConfig({}, controller.args[2], "flxFeatureName"));
        flxFeatureName.setDefaultUnit(kony.flex.DP);
        var lblFeatureName = new kony.ui.Label(extendConfig({
            "id": "lblFeatureName",
            "isVisible": true,
            "left": "0px",
            "skin": "sknlblLato696c7312px",
            "i18n_text": "kony.i18n.getLocalizedString(\"i18n.products.Feature_Name_CAPS\")",
            "top": "0px",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, controller.args[0], "lblFeatureName"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblFeatureName"), extendConfig({}, controller.args[2], "lblFeatureName"));
        var fontIconSortFeatureName = new kony.ui.Label(extendConfig({
            "id": "fontIconSortFeatureName",
            "isVisible": true,
            "left": "10px",
            "skin": "sknIcon15px",
            "text": "",
            "top": "0px",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, controller.args[0], "fontIconSortFeatureName"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "fontIconSortFeatureName"), extendConfig({}, controller.args[2], "fontIconSortFeatureName"));
        flxFeatureName.add(lblFeatureName, fontIconSortFeatureName);
        var flxFeatureGroup = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
            "clipBounds": true,
            "id": "flxFeatureGroup",
            "isVisible": true,
            "layoutType": kony.flex.FLOW_HORIZONTAL,
            "left": "33%",
            "isModalContainer": false,
            "skin": "slFbox",
            "top": "0px",
            "width": "150px",
            "zIndex": 1
        }, controller.args[0], "flxFeatureGroup"), extendConfig({}, controller.args[1], "flxFeatureGroup"), extendConfig({}, controller.args[2], "flxFeatureGroup"));
        flxFeatureGroup.setDefaultUnit(kony.flex.DP);
        var lblFeatureGroup = new kony.ui.Label(extendConfig({
            "id": "lblFeatureGroup",
            "isVisible": true,
            "left": "0px",
            "skin": "sknlblLato696c7312px",
            "i18n_text": "kony.i18n.getLocalizedString(\"i18n.products.Feature_Group_CAPS\")",
            "top": "0px",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, controller.args[0], "lblFeatureGroup"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblFeatureGroup"), extendConfig({}, controller.args[2], "lblFeatureGroup"));
        var fontIconSortFeatureGroup = new kony.ui.Label(extendConfig({
            "id": "fontIconSortFeatureGroup",
            "isVisible": true,
            "left": "10px",
            "skin": "sknIcon15px",
            "text": "",
            "top": "0px",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, controller.args[0], "fontIconSortFeatureGroup"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "fontIconSortFeatureGroup"), extendConfig({}, controller.args[2], "fontIconSortFeatureGroup"));
        flxFeatureGroup.add(lblFeatureGroup, fontIconSortFeatureGroup);
        var flxType = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
            "clipBounds": true,
            "id": "flxType",
            "isVisible": true,
            "layoutType": kony.flex.FLOW_HORIZONTAL,
            "left": "65%",
            "isModalContainer": false,
            "skin": "slFbox",
            "top": "0px",
            "width": "150px",
            "zIndex": 1
        }, controller.args[0], "flxType"), extendConfig({}, controller.args[1], "flxType"), extendConfig({}, controller.args[2], "flxType"));
        flxType.setDefaultUnit(kony.flex.DP);
        var lblType = new kony.ui.Label(extendConfig({
            "id": "lblType",
            "isVisible": true,
            "left": "0px",
            "skin": "sknlblLato696c7312px",
            "i18n_text": "kony.i18n.getLocalizedString(\"i18n.products.Feature_Type_CAPS\")",
            "top": "0px",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, controller.args[0], "lblType"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblType"), extendConfig({}, controller.args[2], "lblType"));
        var fontIconSortType = new kony.ui.Label(extendConfig({
            "id": "fontIconSortType",
            "isVisible": true,
            "left": "10px",
            "skin": "sknIcon15px",
            "text": "",
            "top": "0px",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, controller.args[0], "fontIconSortType"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "fontIconSortType"), extendConfig({}, controller.args[2], "fontIconSortType"));
        flxType.add(lblType, fontIconSortType);
        var flxSeparator2 = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "bottom": "0px",
            "clipBounds": true,
            "height": "1px",
            "id": "flxSeparator2",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "left": "0px",
            "isModalContainer": false,
            "skin": "sknFlx1pxBorder696C73",
            "width": "100%",
            "zIndex": 1
        }, controller.args[0], "flxSeparator2"), extendConfig({}, controller.args[1], "flxSeparator2"), extendConfig({}, controller.args[2], "flxSeparator2"));
        flxSeparator2.setDefaultUnit(kony.flex.DP);
        flxSeparator2.add();
        flxProductFeaturesSegHeader.add(flxFeatureName, flxFeatureGroup, flxType, flxSeparator2);
        var segAddedFeatures = new kony.ui.SegmentedUI2(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
            "data": [{
                "lblIconOptions": "",
                "lblSegFeatureGroup": "Label",
                "lblSegFeatureName": "Label",
                "lblSegType": "Label"
            }, {
                "lblIconOptions": "",
                "lblSegFeatureGroup": "Label",
                "lblSegFeatureName": "Label",
                "lblSegType": "Label"
            }],
            "groupCells": false,
            "id": "segAddedFeatures",
            "isVisible": true,
            "left": "0px",
            "needPageIndicator": true,
            "pageOffDotImage": "pageoffdot.png",
            "pageOnDotImage": "pageondot.png",
            "retainSelection": false,
            "rowFocusSkin": "seg2Focus",
            "rowSkin": "seg2Normal",
            "rowTemplate": "flxProductAddedFeatures",
            "sectionHeaderSkin": "sliPhoneSegmentHeader",
            "selectionBehavior": constants.SEGUI_DEFAULT_BEHAVIOR,
            "separatorColor": "aaaaaa00",
            "separatorRequired": true,
            "separatorThickness": 1,
            "showScrollbars": false,
            "top": "1px",
            "viewType": constants.SEGUI_VIEW_TYPE_TABLEVIEW,
            "widgetDataMap": {
                "flxOptions": "flxOptions",
                "flxProductAddedFeatures": "flxProductAddedFeatures",
                "lblIconOptions": "lblIconOptions",
                "lblSegFeatureGroup": "lblSegFeatureGroup",
                "lblSegFeatureName": "lblSegFeatureName",
                "lblSegType": "lblSegType"
            },
            "width": "100%",
            "zIndex": 1
        }, controller.args[0], "segAddedFeatures"), extendConfig({
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "segAddedFeatures"), extendConfig({}, controller.args[2], "segAddedFeatures"));
        flxViewSelectedFeatures.add(flxProductFeaturesSegHeader, segAddedFeatures);
        flxProductFeatures.add(flxFeaturesHeader, flxNoSelectedFeatures, flxViewSelectedFeatures);
        var flxImageDetails = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "bottom": "0px",
            "clipBounds": true,
            "id": "flxImageDetails",
            "isVisible": false,
            "layoutType": kony.flex.FREE_FORM,
            "left": "20px",
            "isModalContainer": false,
            "right": "20px",
            "skin": "slFbox",
            "top": "20px",
            "zIndex": 1
        }, controller.args[0], "flxImageDetails"), extendConfig({}, controller.args[1], "flxImageDetails"), extendConfig({}, controller.args[2], "flxImageDetails"));
        flxImageDetails.setDefaultUnit(kony.flex.DP);
        var flxImgDetailsScrollContainer = new kony.ui.FlexScrollContainer(extendConfig({
            "allowHorizontalBounce": false,
            "allowVerticalBounce": true,
            "bottom": "0px",
            "bounces": true,
            "clipBounds": true,
            "enableScrolling": true,
            "horizontalScrollIndicator": true,
            "id": "flxImgDetailsScrollContainer",
            "isVisible": true,
            "layoutType": kony.flex.FLOW_VERTICAL,
            "left": "0px",
            "pagingEnabled": false,
            "scrollDirection": kony.flex.SCROLL_VERTICAL,
            "skin": "slFSbox",
            "top": "0px",
            "verticalScrollIndicator": true,
            "width": "100%"
        }, controller.args[0], "flxImgDetailsScrollContainer"), extendConfig({}, controller.args[1], "flxImgDetailsScrollContainer"), extendConfig({}, controller.args[2], "flxImgDetailsScrollContainer"));
        flxImgDetailsScrollContainer.setDefaultUnit(kony.flex.DP);
        var flxNoImageDetailsError = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "bottom": "20px",
            "clipBounds": true,
            "height": "40dp",
            "id": "flxNoImageDetailsError",
            "isVisible": false,
            "layoutType": kony.flex.FLOW_HORIZONTAL,
            "left": "0",
            "isModalContainer": false,
            "skin": "sknBorderRed",
            "top": "0px",
            "width": "100%",
            "zIndex": 10
        }, controller.args[0], "flxNoImageDetailsError"), extendConfig({}, controller.args[1], "flxNoImageDetailsError"), extendConfig({}, controller.args[2], "flxNoImageDetailsError"));
        flxNoImageDetailsError.setDefaultUnit(kony.flex.DP);
        var flxNoImageDetailsErrorIcon = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "clipBounds": true,
            "height": "100%",
            "id": "flxNoImageDetailsErrorIcon",
            "isVisible": true,
            "layoutType": kony.flex.FLOW_HORIZONTAL,
            "left": "0dp",
            "isModalContainer": false,
            "skin": "sknRedFill",
            "top": "0dp",
            "width": "10%"
        }, controller.args[0], "flxNoImageDetailsErrorIcon"), extendConfig({}, controller.args[1], "flxNoImageDetailsErrorIcon"), extendConfig({}, controller.args[2], "flxNoImageDetailsErrorIcon"));
        flxNoImageDetailsErrorIcon.setDefaultUnit(kony.flex.DP);
        var lblNoImageDetailsErrorIcon = new kony.ui.Label(extendConfig({
            "centerX": "50%",
            "centerY": "50%",
            "height": "20dp",
            "id": "lblNoImageDetailsErrorIcon",
            "isVisible": true,
            "left": "0dp",
            "skin": "sknIcon20pxWhite",
            "text": "",
            "top": "0dp",
            "width": "20dp",
            "zIndex": 1
        }, controller.args[0], "lblNoImageDetailsErrorIcon"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblNoImageDetailsErrorIcon"), extendConfig({}, controller.args[2], "lblNoImageDetailsErrorIcon"));
        flxNoImageDetailsErrorIcon.add(lblNoImageDetailsErrorIcon);
        var lblNoImageDetailsErrorText = new kony.ui.Label(extendConfig({
            "centerY": "50%",
            "height": "15dp",
            "id": "lblNoImageDetailsErrorText",
            "isVisible": true,
            "left": "20dp",
            "skin": "sknlblError",
            "text": "Add atleast 1 field of image details to procced further.",
            "top": "0dp",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, controller.args[0], "lblNoImageDetailsErrorText"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblNoImageDetailsErrorText"), extendConfig({}, controller.args[2], "lblNoImageDetailsErrorText"));
        flxNoImageDetailsError.add(flxNoImageDetailsErrorIcon, lblNoImageDetailsErrorText);
        var flxSegImgDetailsWrapper = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
            "clipBounds": true,
            "id": "flxSegImgDetailsWrapper",
            "isVisible": true,
            "layoutType": kony.flex.FLOW_VERTICAL,
            "left": "0dp",
            "isModalContainer": false,
            "skin": "slFbox",
            "top": "0dp",
            "width": "98%"
        }, controller.args[0], "flxSegImgDetailsWrapper"), extendConfig({}, controller.args[1], "flxSegImgDetailsWrapper"), extendConfig({}, controller.args[2], "flxSegImgDetailsWrapper"));
        flxSegImgDetailsWrapper.setDefaultUnit(kony.flex.DP);
        var segImageDetails = new kony.ui.SegmentedUI2(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
            "data": [{
                "lblFontIconInformation": "",
                "lblIconDelete": "",
                "lblImgType": "Image Type",
                "lblImgURL": "Image URL",
                "lstBoxImageType": {
                    "masterData": [
                        ["lb1", "Placeholder One"],
                        ["lb2", "Placeholder Two"],
                        ["lb3", "Placeholder Three"]
                    ],
                    "selectedKey": null,
                    "selectedKeys": null
                },
                "tbxImageURL": ""
            }, {
                "lblFontIconInformation": "",
                "lblIconDelete": "",
                "lblImgType": "Image Type",
                "lblImgURL": "Image URL",
                "lstBoxImageType": {
                    "masterData": [
                        ["lb1", "Placeholder One"],
                        ["lb2", "Placeholder Two"],
                        ["lb3", "Placeholder Three"]
                    ],
                    "selectedKey": null,
                    "selectedKeys": null
                },
                "tbxImageURL": ""
            }],
            "groupCells": false,
            "id": "segImageDetails",
            "isVisible": true,
            "left": "0px",
            "needPageIndicator": true,
            "pageOffDotImage": "pageoffdot.png",
            "pageOnDotImage": "pageondot.png",
            "retainSelection": false,
            "rowFocusSkin": "seg2Focus",
            "rowSkin": "seg2Normal",
            "rowTemplate": "flxSegImageDetails",
            "sectionHeaderSkin": "sliPhoneSegmentHeader",
            "selectionBehavior": constants.SEGUI_DEFAULT_BEHAVIOR,
            "separatorColor": "aaaaaa00",
            "separatorRequired": false,
            "separatorThickness": 1,
            "showScrollbars": false,
            "top": "0px",
            "viewType": constants.SEGUI_VIEW_TYPE_TABLEVIEW,
            "widgetDataMap": {
                "flxDeleteIcon": "flxDeleteIcon",
                "flxImageType": "flxImageType",
                "flxImageURL": "flxImageURL",
                "flxImageURLTop": "flxImageURLTop",
                "flxSegImageDetails": "flxSegImageDetails",
                "lblFontIconInformation": "lblFontIconInformation",
                "lblIconDelete": "lblIconDelete",
                "lblImgType": "lblImgType",
                "lblImgURL": "lblImgURL",
                "lstBoxImageType": "lstBoxImageType",
                "tbxImageURL": "tbxImageURL"
            },
            "width": "100%",
            "zIndex": 1
        }, controller.args[0], "segImageDetails"), extendConfig({
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "segImageDetails"), extendConfig({}, controller.args[2], "segImageDetails"));
        flxSegImgDetailsWrapper.add(segImageDetails);
        var btnAddImage = new kony.ui.Button(extendConfig({
            "bottom": "55px",
            "height": "20px",
            "id": "btnAddImage",
            "isVisible": true,
            "left": "0px",
            "skin": "sknBtnLato117eb013Px",
            "i18n_text": "kony.i18n.getLocalizedString(\"i18n.products.Plus_Add_Image\")",
            "top": "10px",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, controller.args[0], "btnAddImage"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "displayText": true,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "btnAddImage"), extendConfig({}, controller.args[2], "btnAddImage"));
        flxImgDetailsScrollContainer.add(flxNoImageDetailsError, flxSegImgDetailsWrapper, btnAddImage);
        var ToolTip = new com.adminConsole.Customers.ToolTip(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "height": "45px",
            "id": "ToolTip",
            "isVisible": false,
            "layoutType": kony.flex.FREE_FORM,
            "left": "297px",
            "masterType": constants.MASTER_TYPE_DEFAULT,
            "isModalContainer": false,
            "skin": "slFbox",
            "top": "16px",
            "width": "290px",
            "overrides": {
                "ToolTip": {
                    "height": "45px",
                    "isVisible": false,
                    "left": "297px",
                    "minWidth": "viz.val_cleared",
                    "top": "16px",
                    "width": "290px"
                },
                "flxToolTipMessage": {
                    "height": "25px",
                    "width": kony.flex.USE_PREFFERED_SIZE
                },
                "lblDownArrow": {
                    "centerX": "viz.val_cleared",
                    "isVisible": false,
                    "left": "10px",
                    "top": "33dp"
                },
                "lblNoConcentToolTip": {
                    "bottom": "viz.val_cleared",
                    "centerX": "viz.val_cleared",
                    "centerY": "50%",
                    "height": kony.flex.USE_PREFFERED_SIZE,
                    "right": "viz.val_cleared",
                    "text": "Large Banner Image size should be 1743 x 1790 Px",
                    "top": "viz.val_cleared",
                    "width": kony.flex.USE_PREFFERED_SIZE
                },
                "lblarrow": {
                    "centerX": "viz.val_cleared",
                    "height": "10px",
                    "left": "10px"
                }
            }
        }, controller.args[0], "ToolTip"), extendConfig({
            "overrides": {}
        }, controller.args[1], "ToolTip"), extendConfig({
            "overrides": {}
        }, controller.args[2], "ToolTip"));
        flxImageDetails.add(flxImgDetailsScrollContainer, ToolTip);
        var flxAdditionalAttributes = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "bottom": "0px",
            "clipBounds": true,
            "id": "flxAdditionalAttributes",
            "isVisible": false,
            "layoutType": kony.flex.FREE_FORM,
            "left": "20px",
            "isModalContainer": false,
            "right": "20px",
            "skin": "slFbox",
            "top": "20px",
            "zIndex": 1
        }, controller.args[0], "flxAdditionalAttributes"), extendConfig({}, controller.args[1], "flxAdditionalAttributes"), extendConfig({}, controller.args[2], "flxAdditionalAttributes"));
        flxAdditionalAttributes.setDefaultUnit(kony.flex.DP);
        var flxAdditionalAttributesScrollContainer = new kony.ui.FlexScrollContainer(extendConfig({
            "allowHorizontalBounce": false,
            "allowVerticalBounce": true,
            "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
            "bottom": "0px",
            "bounces": true,
            "clipBounds": true,
            "enableScrolling": true,
            "horizontalScrollIndicator": true,
            "id": "flxAdditionalAttributesScrollContainer",
            "isVisible": true,
            "layoutType": kony.flex.FLOW_VERTICAL,
            "left": "0px",
            "pagingEnabled": false,
            "scrollDirection": kony.flex.SCROLL_VERTICAL,
            "skin": "slFSbox",
            "top": "0px",
            "verticalScrollIndicator": true,
            "width": "100%",
            "zIndex": 1
        }, controller.args[0], "flxAdditionalAttributesScrollContainer"), extendConfig({}, controller.args[1], "flxAdditionalAttributesScrollContainer"), extendConfig({}, controller.args[2], "flxAdditionalAttributesScrollContainer"));
        flxAdditionalAttributesScrollContainer.setDefaultUnit(kony.flex.DP);
        var flxAdditionalAttributesHeader = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "clipBounds": true,
            "height": "16px",
            "id": "flxAdditionalAttributesHeader",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "left": "0px",
            "isModalContainer": false,
            "skin": "slFbox",
            "top": "0px",
            "width": "100%",
            "zIndex": 1
        }, controller.args[0], "flxAdditionalAttributesHeader"), extendConfig({}, controller.args[1], "flxAdditionalAttributesHeader"), extendConfig({}, controller.args[2], "flxAdditionalAttributesHeader"));
        flxAdditionalAttributesHeader.setDefaultUnit(kony.flex.DP);
        var lblAttribute = new kony.ui.Label(extendConfig({
            "id": "lblAttribute",
            "isVisible": true,
            "left": "0px",
            "skin": "sknLbl485C75LatoRegular13Px",
            "i18n_text": "kony.i18n.getLocalizedString(\"i18n.products.Attribute\")",
            "top": "0px",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, controller.args[0], "lblAttribute"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblAttribute"), extendConfig({}, controller.args[2], "lblAttribute"));
        var lblAttributeValue = new kony.ui.Label(extendConfig({
            "id": "lblAttributeValue",
            "isVisible": true,
            "left": "28%",
            "skin": "sknLbl485C75LatoRegular13Px",
            "i18n_text": "kony.i18n.getLocalizedString(\"i18n.products.Value\")",
            "top": "0px",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, controller.args[0], "lblAttributeValue"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblAttributeValue"), extendConfig({}, controller.args[2], "lblAttributeValue"));
        flxAdditionalAttributesHeader.add(lblAttribute, lblAttributeValue);
        var segAddAttribute = new kony.ui.SegmentedUI2(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
            "data": [{
                "lblIconDelete": "",
                "tbxAttribute": "",
                "tbxAttributeValue": ""
            }, {
                "lblIconDelete": "",
                "tbxAttribute": "",
                "tbxAttributeValue": ""
            }],
            "groupCells": false,
            "id": "segAddAttribute",
            "isVisible": true,
            "left": "0px",
            "needPageIndicator": true,
            "pageOffDotImage": "pageoffdot.png",
            "pageOnDotImage": "pageondot.png",
            "retainSelection": false,
            "rowFocusSkin": "seg2Focus",
            "rowSkin": "seg2Normal",
            "rowTemplate": "flxSegAddAttribute",
            "sectionHeaderSkin": "sliPhoneSegmentHeader",
            "selectionBehavior": constants.SEGUI_DEFAULT_BEHAVIOR,
            "separatorColor": "aaaaaa00",
            "separatorRequired": false,
            "separatorThickness": 1,
            "showScrollbars": false,
            "top": "0px",
            "viewType": constants.SEGUI_VIEW_TYPE_TABLEVIEW,
            "widgetDataMap": {
                "flxAttribute": "flxAttribute",
                "flxAttributeValue": "flxAttributeValue",
                "flxDeleteIcon": "flxDeleteIcon",
                "flxSegAddAttribute": "flxSegAddAttribute",
                "lblIconDelete": "lblIconDelete",
                "tbxAttribute": "tbxAttribute",
                "tbxAttributeValue": "tbxAttributeValue"
            },
            "width": "98%",
            "zIndex": 1
        }, controller.args[0], "segAddAttribute"), extendConfig({
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "segAddAttribute"), extendConfig({}, controller.args[2], "segAddAttribute"));
        var btnAddAttribute = new kony.ui.Button(extendConfig({
            "bottom": "55px",
            "height": "20px",
            "id": "btnAddAttribute",
            "isVisible": true,
            "left": "0px",
            "skin": "sknBtnLato117eb013Px",
            "i18n_text": "kony.i18n.getLocalizedString(\"i18n.products.Plus_Add_Attribute\")",
            "top": "15px",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, controller.args[0], "btnAddAttribute"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "displayText": true,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "btnAddAttribute"), extendConfig({}, controller.args[2], "btnAddAttribute"));
        flxAdditionalAttributesScrollContainer.add(flxAdditionalAttributesHeader, segAddAttribute, btnAddAttribute);
        flxAdditionalAttributes.add(flxAdditionalAttributesScrollContainer);
        var flxContextualMenu = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
            "clipBounds": true,
            "id": "flxContextualMenu",
            "isVisible": false,
            "layoutType": kony.flex.FLOW_VERTICAL,
            "left": "851px",
            "isModalContainer": false,
            "skin": "slFbox",
            "top": "195dp",
            "width": "98px",
            "zIndex": 1
        }, controller.args[0], "flxContextualMenu"), extendConfig({}, controller.args[1], "flxContextualMenu"), extendConfig({}, controller.args[2], "flxContextualMenu"));
        flxContextualMenu.setDefaultUnit(kony.flex.DP);
        var flxArrowImage = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "clipBounds": true,
            "height": "12dp",
            "id": "flxArrowImage",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "left": "0dp",
            "isModalContainer": false,
            "skin": "slFbox",
            "top": "0dp",
            "width": "100%",
            "zIndex": 2
        }, controller.args[0], "flxArrowImage"), extendConfig({}, controller.args[1], "flxArrowImage"), extendConfig({}, controller.args[2], "flxArrowImage"));
        flxArrowImage.setDefaultUnit(kony.flex.DP);
        var imgUpArrow = new kony.ui.Image2(extendConfig({
            "height": "12dp",
            "id": "imgUpArrow",
            "isVisible": true,
            "right": "15dp",
            "skin": "slImage",
            "src": "uparrow_2x.png",
            "top": "0dp",
            "width": "15dp",
            "zIndex": 1
        }, controller.args[0], "imgUpArrow"), extendConfig({
            "imageScaleMode": constants.IMAGE_SCALE_MODE_FIT_TO_DIMENSIONS,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "imgUpArrow"), extendConfig({}, controller.args[2], "imgUpArrow"));
        flxArrowImage.add(imgUpArrow);
        var flxSelectOptionsInner = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
            "clipBounds": true,
            "id": "flxSelectOptionsInner",
            "isVisible": true,
            "layoutType": kony.flex.FLOW_VERTICAL,
            "left": "0dp",
            "isModalContainer": false,
            "skin": "sknflxffffffop100dbdbe6Radius3px",
            "top": "-1dp",
            "width": "100%"
        }, controller.args[0], "flxSelectOptionsInner"), extendConfig({}, controller.args[1], "flxSelectOptionsInner"), extendConfig({}, controller.args[2], "flxSelectOptionsInner"));
        flxSelectOptionsInner.setDefaultUnit(kony.flex.DP);
        var flxOption1 = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "clipBounds": true,
            "height": "35dp",
            "id": "flxOption1",
            "isVisible": true,
            "layoutType": kony.flex.FLOW_HORIZONTAL,
            "left": "0dp",
            "isModalContainer": false,
            "skin": "CopyslFbox0e960df276fa744",
            "top": "0dp",
            "width": "100%",
            "zIndex": 1
        }, controller.args[0], "flxOption1"), extendConfig({}, controller.args[1], "flxOption1"), extendConfig({
            "hoverSkin": "sknContextualMenuEntryHover"
        }, controller.args[2], "flxOption1"));
        flxOption1.setDefaultUnit(kony.flex.DP);
        var fontIconOption1 = new kony.ui.Label(extendConfig({
            "centerY": "50%",
            "id": "fontIconOption1",
            "isVisible": true,
            "left": "15px",
            "skin": "sknFontIconOptionMenuRow",
            "i18n_text": "kony.i18n.getLocalizedString(\"i18n.userwidgetmodel.lblIconOption1\")",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, controller.args[0], "fontIconOption1"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "fontIconOption1"), extendConfig({}, controller.args[2], "fontIconOption1"));
        var lblOption1Menu = new kony.ui.Label(extendConfig({
            "centerY": "50%",
            "id": "lblOption1Menu",
            "isVisible": true,
            "left": "10px",
            "skin": "sknlblLatoBold35475f14px",
            "i18n_text": "kony.i18n.getLocalizedString(\"i18n.permission.Edit\")",
            "top": "4dp",
            "width": "40px",
            "zIndex": 1
        }, controller.args[0], "lblOption1Menu"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblOption1Menu"), extendConfig({}, controller.args[2], "lblOption1Menu"));
        flxOption1.add(fontIconOption1, lblOption1Menu);
        var flxOption2 = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "clipBounds": true,
            "height": "35dp",
            "id": "flxOption2",
            "isVisible": true,
            "layoutType": kony.flex.FLOW_HORIZONTAL,
            "left": "0dp",
            "isModalContainer": false,
            "skin": "CopyslFbox0e55915c24f2a49",
            "width": "100%",
            "zIndex": 1
        }, controller.args[0], "flxOption2"), extendConfig({}, controller.args[1], "flxOption2"), extendConfig({
            "hoverSkin": "sknContextualMenuEntryHover"
        }, controller.args[2], "flxOption2"));
        flxOption2.setDefaultUnit(kony.flex.DP);
        var fontIconOption2 = new kony.ui.Label(extendConfig({
            "centerY": "50%",
            "id": "fontIconOption2",
            "isVisible": true,
            "left": "15px",
            "skin": "sknFontIconOptionMenuRow",
            "i18n_text": "kony.i18n.getLocalizedString(\"i18n.userwidgetmodel.lblIconOption2\")",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, controller.args[0], "fontIconOption2"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "fontIconOption2"), extendConfig({}, controller.args[2], "fontIconOption2"));
        var lblOption2Menu = new kony.ui.Label(extendConfig({
            "centerY": "50%",
            "id": "lblOption2Menu",
            "isVisible": true,
            "left": "10px",
            "skin": "sknlblLatoBold35475f14px",
            "i18n_text": "kony.i18n.getLocalizedString(\"i18n.SecurityQuestions.Delete\")",
            "top": "4dp",
            "width": "45px",
            "zIndex": 1
        }, controller.args[0], "lblOption2Menu"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblOption2Menu"), extendConfig({}, controller.args[2], "lblOption2Menu"));
        flxOption2.add(fontIconOption2, lblOption2Menu);
        flxSelectOptionsInner.add(flxOption1, flxOption2);
        var flxDownArrowImage = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "clipBounds": true,
            "height": "12px",
            "id": "flxDownArrowImage",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "left": "0px",
            "isModalContainer": false,
            "skin": "slFbox",
            "top": "-1px",
            "width": "100%",
            "zIndex": 2
        }, controller.args[0], "flxDownArrowImage"), extendConfig({}, controller.args[1], "flxDownArrowImage"), extendConfig({}, controller.args[2], "flxDownArrowImage"));
        flxDownArrowImage.setDefaultUnit(kony.flex.DP);
        var imgDownArrow = new kony.ui.Image2(extendConfig({
            "height": "12dp",
            "id": "imgDownArrow",
            "isVisible": true,
            "right": "15dp",
            "skin": "slImage",
            "src": "downarrow_2x.png",
            "top": "0dp",
            "width": "15dp",
            "zIndex": 1
        }, controller.args[0], "imgDownArrow"), extendConfig({
            "imageScaleMode": constants.IMAGE_SCALE_MODE_FIT_TO_DIMENSIONS,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "imgDownArrow"), extendConfig({}, controller.args[2], "imgDownArrow"));
        flxDownArrowImage.add(imgDownArrow);
        flxContextualMenu.add(flxArrowImage, flxSelectOptionsInner, flxDownArrowImage);
        flxAddProductMain.add(flxProductDetails, flxProductDescription, flxProductFeatures, flxImageDetails, flxAdditionalAttributes, flxContextualMenu);
        var flxNavigationBarButtons = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "bottom": "0px",
            "clipBounds": true,
            "height": "80px",
            "id": "flxNavigationBarButtons",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "left": "0%",
            "isModalContainer": false,
            "right": "0px",
            "skin": "sknFlxBgFFFFFFBorE4E6ECsz1pxR1px",
            "zIndex": 1
        }, controller.args[0], "flxNavigationBarButtons"), extendConfig({}, controller.args[1], "flxNavigationBarButtons"), extendConfig({}, controller.args[2], "flxNavigationBarButtons"));
        flxNavigationBarButtons.setDefaultUnit(kony.flex.DP);
        var btnCancelAddProduct = new kony.ui.Button(extendConfig({
            "centerY": "50%",
            "focusSkin": "sknbtnf7f7faLatoRegular4f555dBorder1px8b96a5",
            "height": "40dp",
            "id": "btnCancelAddProduct",
            "isVisible": true,
            "left": "20px",
            "skin": "sknbtnf7f7faLatoRegular4f555dBorder1px8b96a5",
            "i18n_text": "kony.i18n.getLocalizedString(\"i18n.products.Cancel_CAPS\")",
            "top": "0%",
            "width": "100px",
            "zIndex": 2
        }, controller.args[0], "btnCancelAddProduct"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_CENTER,
            "displayText": true,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "btnCancelAddProduct"), extendConfig({
            "hoverSkin": "sknbtnffffffLatoRegular4f555dBorder1px485c75"
        }, controller.args[2], "btnCancelAddProduct"));
        var flxRightButtons = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "clipBounds": true,
            "height": "100%",
            "id": "flxRightButtons",
            "isVisible": true,
            "layoutType": kony.flex.FLOW_HORIZONTAL,
            "reverseLayoutDirection": true,
            "isModalContainer": false,
            "right": "20px",
            "skin": "slFbox",
            "top": "0px",
            "width": "300px",
            "zIndex": 1
        }, controller.args[0], "flxRightButtons"), extendConfig({}, controller.args[1], "flxRightButtons"), extendConfig({}, controller.args[2], "flxRightButtons"));
        flxRightButtons.setDefaultUnit(kony.flex.DP);
        var btnAddProduct = new kony.ui.Button(extendConfig({
            "centerY": "50%",
            "focusSkin": "sknBtn003E75LatoRegular13pxFFFFFFRad20px",
            "height": "40dp",
            "id": "btnAddProduct",
            "isVisible": true,
            "left": "20px",
            "right": "0px",
            "skin": "sknBtn003E75LatoRegular13pxFFFFFFRad20px",
            "i18n_text": "kony.i18n.getLocalizedString(\"i18n.products.Add_Product_CAPS\")",
            "top": "0%",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, controller.args[0], "btnAddProduct"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_CENTER,
            "displayText": true,
            "padding": [8, 0, 8, 0],
            "paddingInPixel": false
        }, controller.args[1], "btnAddProduct"), extendConfig({}, controller.args[2], "btnAddProduct"));
        var btnNext = new kony.ui.Button(extendConfig({
            "centerY": "50%",
            "focusSkin": "sknBtn4A77A0LatoRegular13pxFFFFFFRad20px",
            "height": "40dp",
            "id": "btnNext",
            "isVisible": true,
            "minWidth": "100px",
            "right": "0px",
            "skin": "sknBtn4A77A0LatoRegular13pxFFFFFFRad20px",
            "i18n_text": "kony.i18n.getLocalizedString(\"i18n.products.Next_CAPS\")",
            "top": "0%",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, controller.args[0], "btnNext"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_CENTER,
            "displayText": true,
            "padding": [8, 0, 8, 0],
            "paddingInPixel": false
        }, controller.args[1], "btnNext"), extendConfig({}, controller.args[2], "btnNext"));
        flxRightButtons.add(btnAddProduct, btnNext);
        flxNavigationBarButtons.add(btnCancelAddProduct, flxRightButtons);
        flxProductContainer.add(flxAddProductMain, flxNavigationBarButtons);
        flxAddProductMainContainer.add(flxVerticalTabs, flxProductContainer);
        var flxAddProductPopups = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "centerX": "50%",
            "clipBounds": true,
            "height": "100%",
            "id": "flxAddProductPopups",
            "isVisible": false,
            "layoutType": kony.flex.FREE_FORM,
            "isModalContainer": false,
            "skin": "skn222b35",
            "top": "0px",
            "width": "100%",
            "zIndex": 100
        }, controller.args[0], "flxAddProductPopups"), extendConfig({}, controller.args[1], "flxAddProductPopups"), extendConfig({}, controller.args[2], "flxAddProductPopups"));
        flxAddProductPopups.setDefaultUnit(kony.flex.DP);
        var flxAddFeaturePopup = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "bottom": "0px",
            "centerY": "50%",
            "clipBounds": true,
            "height": "90%",
            "id": "flxAddFeaturePopup",
            "isVisible": false,
            "layoutType": kony.flex.FREE_FORM,
            "left": "10%",
            "isModalContainer": false,
            "right": "10%",
            "skin": "sknBackgroundFFFFFF",
            "zIndex": 10
        }, controller.args[0], "flxAddFeaturePopup"), extendConfig({}, controller.args[1], "flxAddFeaturePopup"), extendConfig({}, controller.args[2], "flxAddFeaturePopup"));
        flxAddFeaturePopup.setDefaultUnit(kony.flex.DP);
        var flxAddFeatureHeader = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "clipBounds": true,
            "height": "10px",
            "id": "flxAddFeatureHeader",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "left": "0px",
            "isModalContainer": false,
            "skin": "sknFlxBg4A77A0",
            "top": "0px",
            "width": "100%",
            "zIndex": 1
        }, controller.args[0], "flxAddFeatureHeader"), extendConfig({}, controller.args[1], "flxAddFeatureHeader"), extendConfig({}, controller.args[2], "flxAddFeatureHeader"));
        flxAddFeatureHeader.setDefaultUnit(kony.flex.DP);
        flxAddFeatureHeader.add();
        var flxClose = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "clipBounds": true,
            "height": "20px",
            "id": "flxClose",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "isModalContainer": false,
            "right": "20px",
            "skin": "sknFlxffffffCursorPointer",
            "top": "25px",
            "width": "20px",
            "zIndex": 10
        }, controller.args[0], "flxClose"), extendConfig({}, controller.args[1], "flxClose"), extendConfig({}, controller.args[2], "flxClose"));
        flxClose.setDefaultUnit(kony.flex.DP);
        var lblCloseIcon = new kony.ui.Label(extendConfig({
            "height": "100%",
            "id": "lblCloseIcon",
            "isVisible": true,
            "left": "0px",
            "skin": "lblIconGrey",
            "text": "",
            "top": "0dp",
            "width": "100%",
            "zIndex": 1
        }, controller.args[0], "lblCloseIcon"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_CENTER,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblCloseIcon"), extendConfig({}, controller.args[2], "lblCloseIcon"));
        flxClose.add(lblCloseIcon);
        var flxAddFeatureScrollMainContainer = new kony.ui.FlexScrollContainer(extendConfig({
            "allowHorizontalBounce": false,
            "allowVerticalBounce": true,
            "bottom": "80px",
            "bounces": true,
            "clipBounds": true,
            "enableScrolling": true,
            "horizontalScrollIndicator": true,
            "id": "flxAddFeatureScrollMainContainer",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "left": "0px",
            "pagingEnabled": false,
            "right": "0px",
            "scrollDirection": kony.flex.SCROLL_VERTICAL,
            "skin": "slFSbox",
            "top": "50px",
            "verticalScrollIndicator": true
        }, controller.args[0], "flxAddFeatureScrollMainContainer"), extendConfig({}, controller.args[1], "flxAddFeatureScrollMainContainer"), extendConfig({}, controller.args[2], "flxAddFeatureScrollMainContainer"));
        flxAddFeatureScrollMainContainer.setDefaultUnit(kony.flex.DP);
        var flxAddFeatureMainContainer = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
            "clipBounds": true,
            "id": "flxAddFeatureMainContainer",
            "isVisible": true,
            "layoutType": kony.flex.FLOW_VERTICAL,
            "left": "20px",
            "isModalContainer": false,
            "right": "20px",
            "skin": "slFbox",
            "top": "0px",
            "zIndex": 1
        }, controller.args[0], "flxAddFeatureMainContainer"), extendConfig({}, controller.args[1], "flxAddFeatureMainContainer"), extendConfig({}, controller.args[2], "flxAddFeatureMainContainer"));
        flxAddFeatureMainContainer.setDefaultUnit(kony.flex.DP);
        var flxAddFeatureDetails = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "clipBounds": true,
            "height": "40px",
            "id": "flxAddFeatureDetails",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "left": "0px",
            "isModalContainer": false,
            "right": "0px",
            "skin": "slFbox",
            "top": "0px",
            "zIndex": 1
        }, controller.args[0], "flxAddFeatureDetails"), extendConfig({}, controller.args[1], "flxAddFeatureDetails"), extendConfig({}, controller.args[2], "flxAddFeatureDetails"));
        flxAddFeatureDetails.setDefaultUnit(kony.flex.DP);
        var lblAddFeatureHeading = new kony.ui.Label(extendConfig({
            "id": "lblAddFeatureHeading",
            "isVisible": true,
            "left": "0px",
            "skin": "sknLatoRegular192B4516px",
            "i18n_text": "kony.i18n.getLocalizedString(\"i18n.products.Add_Feature\")",
            "top": "0px",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, controller.args[0], "lblAddFeatureHeading"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblAddFeatureHeading"), extendConfig({}, controller.args[2], "lblAddFeatureHeading"));
        var flxSeparator3 = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "bottom": "0px",
            "clipBounds": true,
            "height": "1px",
            "id": "flxSeparator3",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "left": "0px",
            "isModalContainer": false,
            "right": "0px",
            "skin": "sknFlxD7D9E0Separator",
            "zIndex": 1
        }, controller.args[0], "flxSeparator3"), extendConfig({}, controller.args[1], "flxSeparator3"), extendConfig({}, controller.args[2], "flxSeparator3"));
        flxSeparator3.setDefaultUnit(kony.flex.DP);
        flxSeparator3.add();
        flxAddFeatureDetails.add(lblAddFeatureHeading, flxSeparator3);
        var flxAddFeature1 = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
            "clipBounds": true,
            "id": "flxAddFeature1",
            "isVisible": true,
            "layoutType": kony.flex.FLOW_HORIZONTAL,
            "left": "0px",
            "isModalContainer": false,
            "skin": "slFbox",
            "top": "20px",
            "width": "100%",
            "zIndex": 1
        }, controller.args[0], "flxAddFeature1"), extendConfig({}, controller.args[1], "flxAddFeature1"), extendConfig({}, controller.args[2], "flxAddFeature1"));
        flxAddFeature1.setDefaultUnit(kony.flex.DP);
        var flxFeatureTypeContainer = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
            "clipBounds": true,
            "id": "flxFeatureTypeContainer",
            "isVisible": true,
            "layoutType": kony.flex.FLOW_VERTICAL,
            "left": "0px",
            "isModalContainer": false,
            "skin": "slFbox",
            "top": "0px",
            "width": "32%",
            "zIndex": 1
        }, controller.args[0], "flxFeatureTypeContainer"), extendConfig({}, controller.args[1], "flxFeatureTypeContainer"), extendConfig({}, controller.args[2], "flxFeatureTypeContainer"));
        flxFeatureTypeContainer.setDefaultUnit(kony.flex.DP);
        var lblAddFeatureType = new kony.ui.Label(extendConfig({
            "id": "lblAddFeatureType",
            "isVisible": true,
            "left": "0px",
            "skin": "sknLbl485C75LatoRegular13Px",
            "i18n_text": "kony.i18n.getLocalizedString(\"i18n.products.Feature_Type\")",
            "top": "0px",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, controller.args[0], "lblAddFeatureType"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblAddFeatureType"), extendConfig({}, controller.args[2], "lblAddFeatureType"));
        var lstBoxAddFeatureType = new kony.ui.ListBox(extendConfig({
            "focusSkin": "defListBoxFocus",
            "height": "40dp",
            "id": "lstBoxAddFeatureType",
            "isVisible": true,
            "left": "0px",
            "masterData": [
                ["lb1", "Placeholder One"],
                ["lb2", "Placeholder Two"],
                ["lb3", "Placeholder Three"]
            ],
            "skin": "sknLbxborderd7d9e03pxradius",
            "top": "15px",
            "width": "100%",
            "zIndex": 1
        }, controller.args[0], "lstBoxAddFeatureType"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_CENTER,
            "padding": [3, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lstBoxAddFeatureType"), extendConfig({
            "multiSelect": false
        }, controller.args[2], "lstBoxAddFeatureType"));
        var flxErrorMessageFeature1 = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
            "clipBounds": true,
            "id": "flxErrorMessageFeature1",
            "isVisible": true,
            "layoutType": kony.flex.FLOW_HORIZONTAL,
            "left": "0dp",
            "isModalContainer": false,
            "skin": "slFbox",
            "top": "10dp",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, controller.args[0], "flxErrorMessageFeature1"), extendConfig({}, controller.args[1], "flxErrorMessageFeature1"), extendConfig({}, controller.args[2], "flxErrorMessageFeature1"));
        flxErrorMessageFeature1.setDefaultUnit(kony.flex.DP);
        var lblErrorIconFeature1 = new kony.ui.Label(extendConfig({
            "height": "15dp",
            "id": "lblErrorIconFeature1",
            "isVisible": true,
            "left": "0dp",
            "skin": "sknErrorIcon",
            "text": "",
            "top": "0dp",
            "width": "15dp",
            "zIndex": 1
        }, controller.args[0], "lblErrorIconFeature1"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblErrorIconFeature1"), extendConfig({}, controller.args[2], "lblErrorIconFeature1"));
        var lblErrorTextFeature1 = new kony.ui.Label(extendConfig({
            "height": "15dp",
            "id": "lblErrorTextFeature1",
            "isVisible": true,
            "left": "0dp",
            "skin": "sknlblError",
            "i18n_text": "kony.i18n.getLocalizedString(\"i18n.products.Select_Feature_Type\")",
            "top": "0dp",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, controller.args[0], "lblErrorTextFeature1"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblErrorTextFeature1"), extendConfig({}, controller.args[2], "lblErrorTextFeature1"));
        flxErrorMessageFeature1.add(lblErrorIconFeature1, lblErrorTextFeature1);
        flxFeatureTypeContainer.add(lblAddFeatureType, lstBoxAddFeatureType, flxErrorMessageFeature1);
        var flxFeatureNameContainer = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
            "clipBounds": true,
            "id": "flxFeatureNameContainer",
            "isVisible": true,
            "layoutType": kony.flex.FLOW_VERTICAL,
            "left": "15px",
            "isModalContainer": false,
            "skin": "slFbox",
            "top": "0px",
            "width": "32%",
            "zIndex": 1
        }, controller.args[0], "flxFeatureNameContainer"), extendConfig({}, controller.args[1], "flxFeatureNameContainer"), extendConfig({}, controller.args[2], "flxFeatureNameContainer"));
        flxFeatureNameContainer.setDefaultUnit(kony.flex.DP);
        var lblAddFeatureName = new kony.ui.Label(extendConfig({
            "id": "lblAddFeatureName",
            "isVisible": true,
            "left": "0px",
            "skin": "sknLbl485C75LatoRegular13Px",
            "i18n_text": "kony.i18n.getLocalizedString(\"i18n.products.Feature_Name\")",
            "top": "0px",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, controller.args[0], "lblAddFeatureName"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblAddFeatureName"), extendConfig({}, controller.args[2], "lblAddFeatureName"));
        var tbxAddFeatureName = new kony.ui.TextBox2(extendConfig({
            "autoCapitalize": constants.TEXTBOX_AUTO_CAPITALIZE_NONE,
            "focusSkin": "defTextBoxFocus",
            "height": "40dp",
            "id": "tbxAddFeatureName",
            "isVisible": true,
            "keyBoardStyle": constants.TEXTBOX_KEY_BOARD_STYLE_DEFAULT,
            "left": "0dp",
            "i18n_placeholder": "kony.i18n.getLocalizedString(\"i18n.products.Feature_Name\")",
            "secureTextEntry": false,
            "skin": "txtD7d9e0",
            "textInputMode": constants.TEXTBOX_INPUT_MODE_ANY,
            "top": "15px",
            "width": "100%",
            "zIndex": 1
        }, controller.args[0], "tbxAddFeatureName"), extendConfig({
            "containerHeightMode": constants.TEXTBOX_FONT_METRICS_DRIVEN_HEIGHT,
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [3, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "tbxAddFeatureName"), extendConfig({
            "autoCorrect": false,
            "placeholderSkin": "defTextBoxPlaceholder"
        }, controller.args[2], "tbxAddFeatureName"));
        var flxErrorMessageFeature2 = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
            "clipBounds": true,
            "id": "flxErrorMessageFeature2",
            "isVisible": true,
            "layoutType": kony.flex.FLOW_HORIZONTAL,
            "left": "0dp",
            "isModalContainer": false,
            "skin": "slFbox",
            "top": "10dp",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, controller.args[0], "flxErrorMessageFeature2"), extendConfig({}, controller.args[1], "flxErrorMessageFeature2"), extendConfig({}, controller.args[2], "flxErrorMessageFeature2"));
        flxErrorMessageFeature2.setDefaultUnit(kony.flex.DP);
        var lblErrorIconFeature2 = new kony.ui.Label(extendConfig({
            "height": "15dp",
            "id": "lblErrorIconFeature2",
            "isVisible": true,
            "left": "0dp",
            "skin": "sknErrorIcon",
            "text": "",
            "top": "0dp",
            "width": "15dp",
            "zIndex": 1
        }, controller.args[0], "lblErrorIconFeature2"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblErrorIconFeature2"), extendConfig({}, controller.args[2], "lblErrorIconFeature2"));
        var lblErrorTextFeature2 = new kony.ui.Label(extendConfig({
            "height": "15dp",
            "id": "lblErrorTextFeature2",
            "isVisible": true,
            "left": "0dp",
            "skin": "sknlblError",
            "i18n_text": "kony.i18n.getLocalizedString(\"i18n.products.Enter_Feature_Name\")",
            "top": "0dp",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, controller.args[0], "lblErrorTextFeature2"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblErrorTextFeature2"), extendConfig({}, controller.args[2], "lblErrorTextFeature2"));
        flxErrorMessageFeature2.add(lblErrorIconFeature2, lblErrorTextFeature2);
        flxFeatureNameContainer.add(lblAddFeatureName, tbxAddFeatureName, flxErrorMessageFeature2);
        var flxFeatureGroupContainer = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
            "clipBounds": true,
            "id": "flxFeatureGroupContainer",
            "isVisible": true,
            "layoutType": kony.flex.FLOW_VERTICAL,
            "left": "15px",
            "isModalContainer": false,
            "skin": "slFbox",
            "top": "0px",
            "width": "32%",
            "zIndex": 1
        }, controller.args[0], "flxFeatureGroupContainer"), extendConfig({}, controller.args[1], "flxFeatureGroupContainer"), extendConfig({}, controller.args[2], "flxFeatureGroupContainer"));
        flxFeatureGroupContainer.setDefaultUnit(kony.flex.DP);
        var lblAddFeatureGroup = new kony.ui.Label(extendConfig({
            "id": "lblAddFeatureGroup",
            "isVisible": true,
            "left": "0px",
            "skin": "sknLbl485C75LatoRegular13Px",
            "i18n_text": "kony.i18n.getLocalizedString(\"i18n.products.Feature_Group\")",
            "top": "0px",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, controller.args[0], "lblAddFeatureGroup"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblAddFeatureGroup"), extendConfig({}, controller.args[2], "lblAddFeatureGroup"));
        var tbxAddFeatureGroup = new kony.ui.TextBox2(extendConfig({
            "autoCapitalize": constants.TEXTBOX_AUTO_CAPITALIZE_NONE,
            "focusSkin": "defTextBoxFocus",
            "height": "40dp",
            "id": "tbxAddFeatureGroup",
            "isVisible": true,
            "keyBoardStyle": constants.TEXTBOX_KEY_BOARD_STYLE_DEFAULT,
            "left": "0dp",
            "i18n_placeholder": "kony.i18n.getLocalizedString(\"i18n.products.Feature_Group\")",
            "secureTextEntry": false,
            "skin": "txtD7d9e0",
            "textInputMode": constants.TEXTBOX_INPUT_MODE_ANY,
            "top": "15px",
            "width": "100%",
            "zIndex": 1
        }, controller.args[0], "tbxAddFeatureGroup"), extendConfig({
            "containerHeightMode": constants.TEXTBOX_FONT_METRICS_DRIVEN_HEIGHT,
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [3, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "tbxAddFeatureGroup"), extendConfig({
            "autoCorrect": false,
            "placeholderSkin": "defTextBoxPlaceholder"
        }, controller.args[2], "tbxAddFeatureGroup"));
        var flxErrorMessageFeature3 = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
            "clipBounds": true,
            "id": "flxErrorMessageFeature3",
            "isVisible": true,
            "layoutType": kony.flex.FLOW_HORIZONTAL,
            "left": "0dp",
            "isModalContainer": false,
            "skin": "slFbox",
            "top": "10dp",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, controller.args[0], "flxErrorMessageFeature3"), extendConfig({}, controller.args[1], "flxErrorMessageFeature3"), extendConfig({}, controller.args[2], "flxErrorMessageFeature3"));
        flxErrorMessageFeature3.setDefaultUnit(kony.flex.DP);
        var lblErrorIconFeature3 = new kony.ui.Label(extendConfig({
            "height": "15dp",
            "id": "lblErrorIconFeature3",
            "isVisible": true,
            "left": "0dp",
            "skin": "sknErrorIcon",
            "text": "",
            "top": "0dp",
            "width": "15dp",
            "zIndex": 1
        }, controller.args[0], "lblErrorIconFeature3"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblErrorIconFeature3"), extendConfig({}, controller.args[2], "lblErrorIconFeature3"));
        var lblErrorTextFeature3 = new kony.ui.Label(extendConfig({
            "height": "15dp",
            "id": "lblErrorTextFeature3",
            "isVisible": true,
            "left": "0dp",
            "skin": "sknlblError",
            "i18n_text": "kony.i18n.getLocalizedString(\"i18n.products.Enter_Feature_Group\")",
            "top": "0dp",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, controller.args[0], "lblErrorTextFeature3"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblErrorTextFeature3"), extendConfig({}, controller.args[2], "lblErrorTextFeature3"));
        flxErrorMessageFeature3.add(lblErrorIconFeature3, lblErrorTextFeature3);
        flxFeatureGroupContainer.add(lblAddFeatureGroup, tbxAddFeatureGroup, flxErrorMessageFeature3);
        flxAddFeature1.add(flxFeatureTypeContainer, flxFeatureNameContainer, flxFeatureGroupContainer);
        var flxAddFeature2 = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
            "clipBounds": true,
            "id": "flxAddFeature2",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "left": "0px",
            "isModalContainer": false,
            "skin": "slFbox",
            "top": "20px",
            "width": "100%",
            "zIndex": 1
        }, controller.args[0], "flxAddFeature2"), extendConfig({}, controller.args[1], "flxAddFeature2"), extendConfig({}, controller.args[2], "flxAddFeature2"));
        flxAddFeature2.setDefaultUnit(kony.flex.DP);
        var lblAddFeatureDescription = new kony.ui.Label(extendConfig({
            "id": "lblAddFeatureDescription",
            "isVisible": true,
            "left": "0px",
            "skin": "sknLbl485C75LatoRegular13Px",
            "i18n_text": "kony.i18n.getLocalizedString(\"i18n.products.Description_Optional\")",
            "top": "0px",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, controller.args[0], "lblAddFeatureDescription"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblAddFeatureDescription"), extendConfig({}, controller.args[2], "lblAddFeatureDescription"));
        var lblFeatureDescTextCounter = new kony.ui.Label(extendConfig({
            "id": "lblFeatureDescTextCounter",
            "isVisible": true,
            "right": "0px",
            "skin": "sknlblLato696c7313px",
            "text": "1/150",
            "top": "0px",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, controller.args[0], "lblFeatureDescTextCounter"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblFeatureDescTextCounter"), extendConfig({}, controller.args[2], "lblFeatureDescTextCounter"));
        var txtAreaFeatureDescription = new kony.ui.TextArea2(extendConfig({
            "autoCapitalize": constants.TEXTAREA_AUTO_CAPITALIZE_NONE,
            "focusSkin": "defTextAreaFocus",
            "height": "72px",
            "id": "txtAreaFeatureDescription",
            "isVisible": true,
            "keyBoardStyle": constants.TEXTAREA_KEY_BOARD_STYLE_DEFAULT,
            "left": "0px",
            "maxTextLength": 150,
            "numberOfVisibleLines": 3,
            "i18n_placeholder": "kony.i18n.getLocalizedString(\"i18n.products.Description\")",
            "skin": "skntxtAread7d9e0",
            "textInputMode": constants.TEXTAREA_INPUT_MODE_ANY,
            "top": "35px",
            "width": "100%",
            "zIndex": 1
        }, controller.args[0], "txtAreaFeatureDescription"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_TOP_LEFT,
            "padding": [2, 2, 2, 2],
            "paddingInPixel": false
        }, controller.args[1], "txtAreaFeatureDescription"), extendConfig({
            "autoCorrect": false,
            "placeholderSkin": "defTextAreaPlaceholder"
        }, controller.args[2], "txtAreaFeatureDescription"));
        flxAddFeature2.add(lblAddFeatureDescription, lblFeatureDescTextCounter, txtAreaFeatureDescription);
        var flxAddFeature3 = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "clipBounds": true,
            "height": "100px",
            "id": "flxAddFeature3",
            "isVisible": true,
            "layoutType": kony.flex.FLOW_HORIZONTAL,
            "left": "0px",
            "isModalContainer": false,
            "skin": "slFbox",
            "top": "20px",
            "width": "100%",
            "zIndex": 1
        }, controller.args[0], "flxAddFeature3"), extendConfig({}, controller.args[1], "flxAddFeature3"), extendConfig({}, controller.args[2], "flxAddFeature3"));
        flxAddFeature3.setDefaultUnit(kony.flex.DP);
        var flxFeatureSequenceNumContainer = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
            "clipBounds": true,
            "id": "flxFeatureSequenceNumContainer",
            "isVisible": true,
            "layoutType": kony.flex.FLOW_VERTICAL,
            "left": "0dp",
            "isModalContainer": false,
            "skin": "slFbox",
            "top": "0dp",
            "width": "32%"
        }, controller.args[0], "flxFeatureSequenceNumContainer"), extendConfig({}, controller.args[1], "flxFeatureSequenceNumContainer"), extendConfig({}, controller.args[2], "flxFeatureSequenceNumContainer"));
        flxFeatureSequenceNumContainer.setDefaultUnit(kony.flex.DP);
        var lblAddFeatureSequenceNumber = new kony.ui.Label(extendConfig({
            "id": "lblAddFeatureSequenceNumber",
            "isVisible": true,
            "left": "0px",
            "skin": "sknLbl485C75LatoRegular13Px",
            "text": "Sequence Number",
            "top": "0px",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, controller.args[0], "lblAddFeatureSequenceNumber"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblAddFeatureSequenceNumber"), extendConfig({}, controller.args[2], "lblAddFeatureSequenceNumber"));
        var tbxAddFeatureSequenceNumber = new kony.ui.TextBox2(extendConfig({
            "autoCapitalize": constants.TEXTBOX_AUTO_CAPITALIZE_NONE,
            "focusSkin": "defTextBoxFocus",
            "height": "40dp",
            "id": "tbxAddFeatureSequenceNumber",
            "isVisible": true,
            "keyBoardStyle": constants.TEXTBOX_KEY_BOARD_STYLE_DEFAULT,
            "left": "0dp",
            "i18n_placeholder": "kony.i18n.getLocalizedString(\"i18n.products.Sequence_Number\")",
            "secureTextEntry": false,
            "skin": "txtD7d9e0",
            "textInputMode": constants.TEXTBOX_INPUT_MODE_ANY,
            "top": "15px",
            "width": "100%",
            "zIndex": 1
        }, controller.args[0], "tbxAddFeatureSequenceNumber"), extendConfig({
            "containerHeightMode": constants.TEXTBOX_FONT_METRICS_DRIVEN_HEIGHT,
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [3, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "tbxAddFeatureSequenceNumber"), extendConfig({
            "autoCorrect": false,
            "placeholderSkin": "defTextBoxPlaceholder"
        }, controller.args[2], "tbxAddFeatureSequenceNumber"));
        var flxErrorMessageFeature5 = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
            "clipBounds": true,
            "id": "flxErrorMessageFeature5",
            "isVisible": true,
            "layoutType": kony.flex.FLOW_HORIZONTAL,
            "left": "0dp",
            "isModalContainer": false,
            "skin": "slFbox",
            "top": "10dp",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, controller.args[0], "flxErrorMessageFeature5"), extendConfig({}, controller.args[1], "flxErrorMessageFeature5"), extendConfig({}, controller.args[2], "flxErrorMessageFeature5"));
        flxErrorMessageFeature5.setDefaultUnit(kony.flex.DP);
        var lblErrorIconFeature5 = new kony.ui.Label(extendConfig({
            "height": "15dp",
            "id": "lblErrorIconFeature5",
            "isVisible": true,
            "left": "0dp",
            "skin": "sknErrorIcon",
            "text": "",
            "top": "0dp",
            "width": "15dp",
            "zIndex": 1
        }, controller.args[0], "lblErrorIconFeature5"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblErrorIconFeature5"), extendConfig({}, controller.args[2], "lblErrorIconFeature5"));
        var lblErrorTextFeature5 = new kony.ui.Label(extendConfig({
            "height": "15dp",
            "id": "lblErrorTextFeature5",
            "isVisible": true,
            "left": "0dp",
            "skin": "sknlblError",
            "text": "Sequence Number cannot be empty or 0.",
            "top": "0dp",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, controller.args[0], "lblErrorTextFeature5"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblErrorTextFeature5"), extendConfig({}, controller.args[2], "lblErrorTextFeature5"));
        flxErrorMessageFeature5.add(lblErrorIconFeature5, lblErrorTextFeature5);
        flxFeatureSequenceNumContainer.add(lblAddFeatureSequenceNumber, tbxAddFeatureSequenceNumber, flxErrorMessageFeature5);
        var flxFeatureSequenceMandatory = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "clipBounds": true,
            "height": "100%",
            "id": "flxFeatureSequenceMandatory",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "left": "15px",
            "isModalContainer": false,
            "skin": "slFbox",
            "top": "0px",
            "width": "20%",
            "zIndex": 1
        }, controller.args[0], "flxFeatureSequenceMandatory"), extendConfig({}, controller.args[1], "flxFeatureSequenceMandatory"), extendConfig({}, controller.args[2], "flxFeatureSequenceMandatory"));
        flxFeatureSequenceMandatory.setDefaultUnit(kony.flex.DP);
        var fontIconSmsChannelSelectOption = new kony.ui.Label(extendConfig({
            "centerY": "50%",
            "height": "20px",
            "id": "fontIconSmsChannelSelectOption",
            "isVisible": true,
            "left": "0dp",
            "skin": "sknFontIconCheckBoxSelected",
            "text": "",
            "top": "0dp",
            "width": "20px",
            "zIndex": 1
        }, controller.args[0], "fontIconSmsChannelSelectOption"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_CENTER,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "fontIconSmsChannelSelectOption"), extendConfig({}, controller.args[2], "fontIconSmsChannelSelectOption"));
        var lblMandatorySequenceNumber = new kony.ui.Label(extendConfig({
            "centerY": "50%",
            "height": "20px",
            "id": "lblMandatorySequenceNumber",
            "isVisible": true,
            "left": "25px",
            "skin": "sknLbl485C75LatoRegular13Px",
            "i18n_text": "kony.i18n.getLocalizedString(\"i18n.products.Mandatory\")",
            "top": "0px",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, controller.args[0], "lblMandatorySequenceNumber"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_CENTER,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblMandatorySequenceNumber"), extendConfig({}, controller.args[2], "lblMandatorySequenceNumber"));
        flxFeatureSequenceMandatory.add(fontIconSmsChannelSelectOption, lblMandatorySequenceNumber);
        flxAddFeature3.add(flxFeatureSequenceNumContainer, flxFeatureSequenceMandatory);
        var flxOptionDetails = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "clipBounds": true,
            "height": "50px",
            "id": "flxOptionDetails",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "left": "0px",
            "isModalContainer": false,
            "skin": "slFbox",
            "top": "0px",
            "width": "100%",
            "zIndex": 1
        }, controller.args[0], "flxOptionDetails"), extendConfig({}, controller.args[1], "flxOptionDetails"), extendConfig({}, controller.args[2], "flxOptionDetails"));
        flxOptionDetails.setDefaultUnit(kony.flex.DP);
        var lblOptionDetails = new kony.ui.Label(extendConfig({
            "id": "lblOptionDetails",
            "isVisible": true,
            "left": "0px",
            "skin": "sknLatoRegular192B4516px",
            "i18n_text": "kony.i18n.getLocalizedString(\"i18n.products.Option_Details\")",
            "top": "20px",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, controller.args[0], "lblOptionDetails"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblOptionDetails"), extendConfig({}, controller.args[2], "lblOptionDetails"));
        var flxSeparator4 = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "bottom": "0px",
            "clipBounds": true,
            "height": "1px",
            "id": "flxSeparator4",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "left": "0px",
            "isModalContainer": false,
            "right": "0px",
            "skin": "sknFlxD7D9E0Separator",
            "zIndex": 1
        }, controller.args[0], "flxSeparator4"), extendConfig({}, controller.args[1], "flxSeparator4"), extendConfig({}, controller.args[2], "flxSeparator4"));
        flxSeparator4.setDefaultUnit(kony.flex.DP);
        flxSeparator4.add();
        flxOptionDetails.add(lblOptionDetails, flxSeparator4);
        var flxFeatureOption1 = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
            "clipBounds": true,
            "id": "flxFeatureOption1",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "left": "0px",
            "isModalContainer": false,
            "skin": "slFbox",
            "top": "20px",
            "width": "100%",
            "zIndex": 1
        }, controller.args[0], "flxFeatureOption1"), extendConfig({}, controller.args[1], "flxFeatureOption1"), extendConfig({}, controller.args[2], "flxFeatureOption1"));
        flxFeatureOption1.setDefaultUnit(kony.flex.DP);
        var flxOptionDisplayType = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
            "clipBounds": true,
            "id": "flxOptionDisplayType",
            "isVisible": true,
            "layoutType": kony.flex.FLOW_VERTICAL,
            "left": "0px",
            "isModalContainer": false,
            "skin": "slFbox",
            "top": "0px",
            "width": "32%",
            "zIndex": 1
        }, controller.args[0], "flxOptionDisplayType"), extendConfig({}, controller.args[1], "flxOptionDisplayType"), extendConfig({}, controller.args[2], "flxOptionDisplayType"));
        flxOptionDisplayType.setDefaultUnit(kony.flex.DP);
        var lblOptionDisplayType = new kony.ui.Label(extendConfig({
            "id": "lblOptionDisplayType",
            "isVisible": true,
            "left": "0px",
            "skin": "sknLbl485C75LatoRegular13Px",
            "i18n_text": "kony.i18n.getLocalizedString(\"i18n.products.Option_Display_Type\")",
            "top": "0px",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, controller.args[0], "lblOptionDisplayType"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblOptionDisplayType"), extendConfig({}, controller.args[2], "lblOptionDisplayType"));
        var lstBoxOptionDisplayType = new kony.ui.ListBox(extendConfig({
            "focusSkin": "defListBoxFocus",
            "height": "40dp",
            "id": "lstBoxOptionDisplayType",
            "isVisible": true,
            "left": "0px",
            "masterData": [
                ["lb1", "Placeholder One"],
                ["lb2", "Placeholder Two"],
                ["lb3", "Placeholder Three"]
            ],
            "skin": "sknLbxborderd7d9e03pxradius",
            "top": "15px",
            "width": "100%",
            "zIndex": 1
        }, controller.args[0], "lstBoxOptionDisplayType"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [3, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lstBoxOptionDisplayType"), extendConfig({
            "multiSelect": false
        }, controller.args[2], "lstBoxOptionDisplayType"));
        var flxErrorMessageFeature4 = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
            "clipBounds": true,
            "id": "flxErrorMessageFeature4",
            "isVisible": true,
            "layoutType": kony.flex.FLOW_HORIZONTAL,
            "left": "0dp",
            "isModalContainer": false,
            "skin": "slFbox",
            "top": "10dp",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, controller.args[0], "flxErrorMessageFeature4"), extendConfig({}, controller.args[1], "flxErrorMessageFeature4"), extendConfig({}, controller.args[2], "flxErrorMessageFeature4"));
        flxErrorMessageFeature4.setDefaultUnit(kony.flex.DP);
        var lblErrorIconFeature4 = new kony.ui.Label(extendConfig({
            "height": "15dp",
            "id": "lblErrorIconFeature4",
            "isVisible": true,
            "left": "0dp",
            "skin": "sknErrorIcon",
            "text": "",
            "top": "0dp",
            "width": "15dp",
            "zIndex": 1
        }, controller.args[0], "lblErrorIconFeature4"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblErrorIconFeature4"), extendConfig({}, controller.args[2], "lblErrorIconFeature4"));
        var lblErrorTextFeature4 = new kony.ui.Label(extendConfig({
            "height": "15dp",
            "id": "lblErrorTextFeature4",
            "isVisible": true,
            "left": "0dp",
            "skin": "sknlblError",
            "text": "Please select Option Display Type",
            "top": "0dp",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, controller.args[0], "lblErrorTextFeature4"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblErrorTextFeature4"), extendConfig({}, controller.args[2], "lblErrorTextFeature4"));
        flxErrorMessageFeature4.add(lblErrorIconFeature4, lblErrorTextFeature4);
        flxOptionDisplayType.add(lblOptionDisplayType, lstBoxOptionDisplayType, flxErrorMessageFeature4);
        flxFeatureOption1.add(flxOptionDisplayType);
        var flxOptionContainer = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
            "clipBounds": true,
            "id": "flxOptionContainer",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "left": "0px",
            "isModalContainer": false,
            "skin": "sknflxf5f6f8Border1px4pxRadius",
            "top": "20dp",
            "width": "100%",
            "zIndex": 1
        }, controller.args[0], "flxOptionContainer"), extendConfig({}, controller.args[1], "flxOptionContainer"), extendConfig({}, controller.args[2], "flxOptionContainer"));
        flxOptionContainer.setDefaultUnit(kony.flex.DP);
        var flxOptionValueInner = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
            "bottom": "15px",
            "clipBounds": true,
            "id": "flxOptionValueInner",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "left": "15px",
            "isModalContainer": false,
            "right": "15px",
            "skin": "slFbox",
            "top": "15px"
        }, controller.args[0], "flxOptionValueInner"), extendConfig({}, controller.args[1], "flxOptionValueInner"), extendConfig({}, controller.args[2], "flxOptionValueInner"));
        flxOptionValueInner.setDefaultUnit(kony.flex.DP);
        var lblOptionValue = new kony.ui.Label(extendConfig({
            "id": "lblOptionValue",
            "isVisible": true,
            "left": "0px",
            "skin": "sknLbl485C75LatoRegular13Px",
            "i18n_text": "kony.i18n.getLocalizedString(\"i18n.products.Value\")",
            "top": "0px",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, controller.args[0], "lblOptionValue"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblOptionValue"), extendConfig({}, controller.args[2], "lblOptionValue"));
        var lblOptionDescription = new kony.ui.Label(extendConfig({
            "id": "lblOptionDescription",
            "isVisible": true,
            "left": "30%",
            "skin": "sknLbl485C75LatoRegular13Px",
            "i18n_text": "kony.i18n.getLocalizedString(\"i18n.products.Description_Optional\")",
            "top": "0px",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, controller.args[0], "lblOptionDescription"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblOptionDescription"), extendConfig({}, controller.args[2], "lblOptionDescription"));
        var segFeatureOptionValues = new kony.ui.SegmentedUI2(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
            "data": [{
                "fontIconSelectOptionDefaultValue": "",
                "lblDefaultValue": "Mandatory",
                "lblFeatureDescTextCounter": "1/150",
                "lblIconDelete": "",
                "tbxValue": "",
                "txtAreaFeatureDescription": ""
            }, {
                "fontIconSelectOptionDefaultValue": "",
                "lblDefaultValue": "Mandatory",
                "lblFeatureDescTextCounter": "1/150",
                "lblIconDelete": "",
                "tbxValue": "",
                "txtAreaFeatureDescription": ""
            }],
            "groupCells": false,
            "id": "segFeatureOptionValues",
            "isVisible": true,
            "left": "0px",
            "needPageIndicator": true,
            "pageOffDotImage": "pageoffdot.png",
            "pageOnDotImage": "pageondot.png",
            "retainSelection": false,
            "rowFocusSkin": "seg2Focus",
            "rowSkin": "sknSegRowTransparent",
            "rowTemplate": "flxSegProductFeatureOption",
            "sectionHeaderSkin": "sliPhoneSegmentHeader",
            "selectionBehavior": constants.SEGUI_DEFAULT_BEHAVIOR,
            "separatorColor": "aaaaaa00",
            "separatorRequired": false,
            "separatorThickness": 1,
            "showScrollbars": false,
            "top": "15px",
            "viewType": constants.SEGUI_VIEW_TYPE_TABLEVIEW,
            "widgetDataMap": {
                "flxDeleteIcon": "flxDeleteIcon",
                "flxDescriptionContainer": "flxDescriptionContainer",
                "flxSegProductFeatureOption": "flxSegProductFeatureOption",
                "flxValueContainer": "flxValueContainer",
                "fontIconSelectOptionDefaultValue": "fontIconSelectOptionDefaultValue",
                "lblDefaultValue": "lblDefaultValue",
                "lblFeatureDescTextCounter": "lblFeatureDescTextCounter",
                "lblIconDelete": "lblIconDelete",
                "tbxValue": "tbxValue",
                "txtAreaFeatureDescription": "txtAreaFeatureDescription"
            },
            "width": "100%",
            "zIndex": 1
        }, controller.args[0], "segFeatureOptionValues"), extendConfig({
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "segFeatureOptionValues"), extendConfig({}, controller.args[2], "segFeatureOptionValues"));
        flxOptionValueInner.add(lblOptionValue, lblOptionDescription, segFeatureOptionValues);
        flxOptionContainer.add(flxOptionValueInner);
        var btnAddValue = new kony.ui.Button(extendConfig({
            "bottom": "55px",
            "id": "btnAddValue",
            "isVisible": true,
            "left": "0px",
            "skin": "sknBtnLato117eb013Px",
            "i18n_text": "kony.i18n.getLocalizedString(\"i18n.products.Plus_Add_Value\")",
            "top": "15px",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, controller.args[0], "btnAddValue"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "displayText": true,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "btnAddValue"), extendConfig({}, controller.args[2], "btnAddValue"));
        flxAddFeatureMainContainer.add(flxAddFeatureDetails, flxAddFeature1, flxAddFeature2, flxAddFeature3, flxOptionDetails, flxFeatureOption1, flxOptionContainer, btnAddValue);
        flxAddFeatureScrollMainContainer.add(flxAddFeatureMainContainer);
        var flxAddFeatureBottomBar = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "bottom": "0px",
            "clipBounds": true,
            "height": "80px",
            "id": "flxAddFeatureBottomBar",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "left": "0px",
            "isModalContainer": false,
            "skin": "sknflxBgE4E6EC",
            "width": "100%",
            "zIndex": 1
        }, controller.args[0], "flxAddFeatureBottomBar"), extendConfig({}, controller.args[1], "flxAddFeatureBottomBar"), extendConfig({}, controller.args[2], "flxAddFeatureBottomBar"));
        flxAddFeatureBottomBar.setDefaultUnit(kony.flex.DP);
        var btnCancel = new kony.ui.Button(extendConfig({
            "centerY": "50%",
            "focusSkin": "sknbtnf7f7faLatoRegular4f555dBorder1px8b96a5",
            "height": "40dp",
            "id": "btnCancel",
            "isVisible": true,
            "left": "20px",
            "skin": "sknbtnf7f7faLatoRegular4f555dBorder1px8b96a5",
            "i18n_text": "kony.i18n.getLocalizedString(\"i18n.products.Cancel_CAPS\")",
            "top": "0%",
            "width": "100px",
            "zIndex": 2
        }, controller.args[0], "btnCancel"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_CENTER,
            "displayText": true,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "btnCancel"), extendConfig({
            "hoverSkin": "sknbtnffffffLatoRegular4f555dBorder1px485c75"
        }, controller.args[2], "btnCancel"));
        var btnSaveAndClose = new kony.ui.Button(extendConfig({
            "centerY": "50%",
            "focusSkin": "sknBtn003E75LatoRegular13pxFFFFFFRad20px",
            "height": "40dp",
            "id": "btnSaveAndClose",
            "isVisible": true,
            "right": "20px",
            "skin": "sknBtn003E75LatoRegular13pxFFFFFFRad20px",
            "i18n_text": "kony.i18n.getLocalizedString(\"i18n.products.Save_and_Close_CAPS\")",
            "top": "0%",
            "width": "145px",
            "zIndex": 1
        }, controller.args[0], "btnSaveAndClose"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_CENTER,
            "displayText": true,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "btnSaveAndClose"), extendConfig({}, controller.args[2], "btnSaveAndClose"));
        flxAddFeatureBottomBar.add(btnCancel, btnSaveAndClose);
        flxAddFeaturePopup.add(flxAddFeatureHeader, flxClose, flxAddFeatureScrollMainContainer, flxAddFeatureBottomBar);
        var flxViewFeatureDetailsPopup = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "centerY": "50%",
            "clipBounds": true,
            "height": "85%",
            "id": "flxViewFeatureDetailsPopup",
            "isVisible": false,
            "layoutType": kony.flex.FREE_FORM,
            "left": "15%",
            "isModalContainer": false,
            "right": "15%",
            "skin": "sknBackgroundFFFFFF",
            "zIndex": 10
        }, controller.args[0], "flxViewFeatureDetailsPopup"), extendConfig({}, controller.args[1], "flxViewFeatureDetailsPopup"), extendConfig({}, controller.args[2], "flxViewFeatureDetailsPopup"));
        flxViewFeatureDetailsPopup.setDefaultUnit(kony.flex.DP);
        var flxViewFeatureTopDetails = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
            "clipBounds": true,
            "id": "flxViewFeatureTopDetails",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "left": "0px",
            "isModalContainer": false,
            "skin": "slFbox",
            "top": "0px",
            "width": "100%"
        }, controller.args[0], "flxViewFeatureTopDetails"), extendConfig({}, controller.args[1], "flxViewFeatureTopDetails"), extendConfig({}, controller.args[2], "flxViewFeatureTopDetails"));
        flxViewFeatureTopDetails.setDefaultUnit(kony.flex.DP);
        var flxViewFeatureHeader = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "clipBounds": true,
            "height": "10px",
            "id": "flxViewFeatureHeader",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "left": "0px",
            "isModalContainer": false,
            "skin": "sknFlxBg4A77A0",
            "top": "0px",
            "width": "100%",
            "zIndex": 1
        }, controller.args[0], "flxViewFeatureHeader"), extendConfig({}, controller.args[1], "flxViewFeatureHeader"), extendConfig({}, controller.args[2], "flxViewFeatureHeader"));
        flxViewFeatureHeader.setDefaultUnit(kony.flex.DP);
        flxViewFeatureHeader.add();
        var flxClose2 = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "clipBounds": true,
            "height": "20px",
            "id": "flxClose2",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "isModalContainer": false,
            "right": "20px",
            "skin": "sknFlxffffffCursorPointer",
            "top": "20px",
            "width": "20px",
            "zIndex": 10
        }, controller.args[0], "flxClose2"), extendConfig({}, controller.args[1], "flxClose2"), extendConfig({}, controller.args[2], "flxClose2"));
        flxClose2.setDefaultUnit(kony.flex.DP);
        var lblCloseIcon2 = new kony.ui.Label(extendConfig({
            "height": "100%",
            "id": "lblCloseIcon2",
            "isVisible": true,
            "left": "0px",
            "skin": "lblIconGrey",
            "text": "",
            "top": "0dp",
            "width": "100%",
            "zIndex": 1
        }, controller.args[0], "lblCloseIcon2"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_CENTER,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblCloseIcon2"), extendConfig({}, controller.args[2], "lblCloseIcon2"));
        flxClose2.add(lblCloseIcon2);
        var flxViewFeatureTop = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "clipBounds": true,
            "height": "40px",
            "id": "flxViewFeatureTop",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "left": "20px",
            "isModalContainer": false,
            "right": "20px",
            "skin": "slFbox",
            "top": "50px",
            "zIndex": 1
        }, controller.args[0], "flxViewFeatureTop"), extendConfig({}, controller.args[1], "flxViewFeatureTop"), extendConfig({}, controller.args[2], "flxViewFeatureTop"));
        flxViewFeatureTop.setDefaultUnit(kony.flex.DP);
        var lblViewFeatureName = new kony.ui.Label(extendConfig({
            "id": "lblViewFeatureName",
            "isVisible": true,
            "left": "0px",
            "skin": "sknLatoRegular192B4516px",
            "text": "Feature-1",
            "top": "0px",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, controller.args[0], "lblViewFeatureName"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblViewFeatureName"), extendConfig({}, controller.args[2], "lblViewFeatureName"));
        var btnEditFeature = new kony.ui.Button(extendConfig({
            "height": "22px",
            "id": "btnEditFeature",
            "isVisible": true,
            "right": "1%",
            "skin": "sknbtnf7f7faLatoReg13px485c75Rad20px",
            "i18n_text": "kony.i18n.getLocalizedString(\"i18n.products.Edit\")",
            "top": "0%",
            "width": "60px",
            "zIndex": 1
        }, controller.args[0], "btnEditFeature"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_CENTER,
            "displayText": true,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "btnEditFeature"), extendConfig({}, controller.args[2], "btnEditFeature"));
        var flxViewFeatureSeparator1 = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "bottom": "0px",
            "clipBounds": true,
            "height": "1px",
            "id": "flxViewFeatureSeparator1",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "left": "0px",
            "isModalContainer": false,
            "right": "0px",
            "skin": "sknFlxD7D9E0Separator",
            "zIndex": 1
        }, controller.args[0], "flxViewFeatureSeparator1"), extendConfig({}, controller.args[1], "flxViewFeatureSeparator1"), extendConfig({}, controller.args[2], "flxViewFeatureSeparator1"));
        flxViewFeatureSeparator1.setDefaultUnit(kony.flex.DP);
        flxViewFeatureSeparator1.add();
        flxViewFeatureTop.add(lblViewFeatureName, btnEditFeature, flxViewFeatureSeparator1);
        flxViewFeatureTopDetails.add(flxViewFeatureHeader, flxClose2, flxViewFeatureTop);
        var flxViewFeatureScrollMain = new kony.ui.FlexScrollContainer(extendConfig({
            "allowHorizontalBounce": false,
            "allowVerticalBounce": true,
            "bottom": "30px",
            "bounces": true,
            "clipBounds": true,
            "enableScrolling": true,
            "horizontalScrollIndicator": true,
            "id": "flxViewFeatureScrollMain",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "left": "0px",
            "pagingEnabled": false,
            "right": "0px",
            "scrollDirection": kony.flex.SCROLL_VERTICAL,
            "skin": "slFSbox",
            "top": "112px",
            "verticalScrollIndicator": true,
            "zIndex": 1
        }, controller.args[0], "flxViewFeatureScrollMain"), extendConfig({}, controller.args[1], "flxViewFeatureScrollMain"), extendConfig({}, controller.args[2], "flxViewFeatureScrollMain"));
        flxViewFeatureScrollMain.setDefaultUnit(kony.flex.DP);
        var flxViewFeatureMain = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
            "clipBounds": true,
            "id": "flxViewFeatureMain",
            "isVisible": true,
            "layoutType": kony.flex.FLOW_VERTICAL,
            "left": "20px",
            "isModalContainer": false,
            "right": "20px",
            "skin": "slFbox",
            "top": "0px",
            "zIndex": 1
        }, controller.args[0], "flxViewFeatureMain"), extendConfig({}, controller.args[1], "flxViewFeatureMain"), extendConfig({}, controller.args[2], "flxViewFeatureMain"));
        flxViewFeatureMain.setDefaultUnit(kony.flex.DP);
        var flxViewFeatureRow1 = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
            "clipBounds": true,
            "id": "flxViewFeatureRow1",
            "isVisible": true,
            "layoutType": kony.flex.FLOW_HORIZONTAL,
            "left": "0px",
            "isModalContainer": false,
            "right": "0px",
            "skin": "slFbox",
            "top": "0px",
            "width": "100%",
            "zIndex": 1
        }, controller.args[0], "flxViewFeatureRow1"), extendConfig({}, controller.args[1], "flxViewFeatureRow1"), extendConfig({}, controller.args[2], "flxViewFeatureRow1"));
        flxViewFeatureRow1.setDefaultUnit(kony.flex.DP);
        var flxViewFeatureGroup = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
            "clipBounds": true,
            "id": "flxViewFeatureGroup",
            "isVisible": true,
            "layoutType": kony.flex.FLOW_VERTICAL,
            "left": "0px",
            "isModalContainer": false,
            "skin": "slFbox",
            "top": "0px",
            "width": "35%",
            "zIndex": 1
        }, controller.args[0], "flxViewFeatureGroup"), extendConfig({}, controller.args[1], "flxViewFeatureGroup"), extendConfig({}, controller.args[2], "flxViewFeatureGroup"));
        flxViewFeatureGroup.setDefaultUnit(kony.flex.DP);
        var lblFeatureGroupHeading = new kony.ui.Label(extendConfig({
            "id": "lblFeatureGroupHeading",
            "isVisible": true,
            "left": "0px",
            "skin": "sknlblLato696c7311px",
            "i18n_text": "kony.i18n.getLocalizedString(\"i18n.products.Feature_Group_CAPS\")",
            "top": "0px",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, controller.args[0], "lblFeatureGroupHeading"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblFeatureGroupHeading"), extendConfig({}, controller.args[2], "lblFeatureGroupHeading"));
        var lblFeatureGroupVal = new kony.ui.Label(extendConfig({
            "id": "lblFeatureGroupVal",
            "isVisible": true,
            "left": "0px",
            "skin": "sknLbl485C75LatoRegular13Px",
            "text": "Bundles",
            "top": "10px",
            "width": "100%",
            "zIndex": 1
        }, controller.args[0], "lblFeatureGroupVal"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblFeatureGroupVal"), extendConfig({}, controller.args[2], "lblFeatureGroupVal"));
        flxViewFeatureGroup.add(lblFeatureGroupHeading, lblFeatureGroupVal);
        var flxViewFeatureSeqNum = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "clipBounds": true,
            "height": "40px",
            "id": "flxViewFeatureSeqNum",
            "isVisible": true,
            "layoutType": kony.flex.FLOW_VERTICAL,
            "left": "15px",
            "isModalContainer": false,
            "skin": "slFbox",
            "top": "0px",
            "width": "31%",
            "zIndex": 1
        }, controller.args[0], "flxViewFeatureSeqNum"), extendConfig({}, controller.args[1], "flxViewFeatureSeqNum"), extendConfig({}, controller.args[2], "flxViewFeatureSeqNum"));
        flxViewFeatureSeqNum.setDefaultUnit(kony.flex.DP);
        var lblSequenceNumHeading = new kony.ui.Label(extendConfig({
            "id": "lblSequenceNumHeading",
            "isVisible": true,
            "left": "0px",
            "skin": "sknlblLato696c7311px",
            "i18n_text": "kony.i18n.getLocalizedString(\"i18n.products.Sequence_Number_CAPS\")",
            "top": "0px",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, controller.args[0], "lblSequenceNumHeading"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblSequenceNumHeading"), extendConfig({}, controller.args[2], "lblSequenceNumHeading"));
        var lblSeqNumVal = new kony.ui.Label(extendConfig({
            "id": "lblSeqNumVal",
            "isVisible": true,
            "left": "0px",
            "skin": "sknLbl485C75LatoRegular13Px",
            "text": "06",
            "top": "10px",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, controller.args[0], "lblSeqNumVal"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblSeqNumVal"), extendConfig({}, controller.args[2], "lblSeqNumVal"));
        flxViewFeatureSeqNum.add(lblSequenceNumHeading, lblSeqNumVal);
        var flxViewFeatureIsMandatory = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "clipBounds": true,
            "height": "40px",
            "id": "flxViewFeatureIsMandatory",
            "isVisible": true,
            "layoutType": kony.flex.FLOW_VERTICAL,
            "left": "15px",
            "isModalContainer": false,
            "skin": "slFbox",
            "top": "0px",
            "width": "31%",
            "zIndex": 1
        }, controller.args[0], "flxViewFeatureIsMandatory"), extendConfig({}, controller.args[1], "flxViewFeatureIsMandatory"), extendConfig({}, controller.args[2], "flxViewFeatureIsMandatory"));
        flxViewFeatureIsMandatory.setDefaultUnit(kony.flex.DP);
        var lblIsMandatoryHeading = new kony.ui.Label(extendConfig({
            "id": "lblIsMandatoryHeading",
            "isVisible": true,
            "left": "0px",
            "skin": "sknlblLato696c7311px",
            "i18n_text": "kony.i18n.getLocalizedString(\"i18n.products.Mandatory_CAPS\")",
            "top": "0px",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, controller.args[0], "lblIsMandatoryHeading"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblIsMandatoryHeading"), extendConfig({}, controller.args[2], "lblIsMandatoryHeading"));
        var lblIsMandatoryVal = new kony.ui.Label(extendConfig({
            "id": "lblIsMandatoryVal",
            "isVisible": true,
            "left": "0px",
            "skin": "sknLbl485C75LatoRegular13Px",
            "text": "True",
            "top": "10px",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, controller.args[0], "lblIsMandatoryVal"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblIsMandatoryVal"), extendConfig({}, controller.args[2], "lblIsMandatoryVal"));
        flxViewFeatureIsMandatory.add(lblIsMandatoryHeading, lblIsMandatoryVal);
        flxViewFeatureRow1.add(flxViewFeatureGroup, flxViewFeatureSeqNum, flxViewFeatureIsMandatory);
        var flxViewFeatureRow2 = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
            "clipBounds": true,
            "id": "flxViewFeatureRow2",
            "isVisible": true,
            "layoutType": kony.flex.FLOW_HORIZONTAL,
            "left": "0px",
            "isModalContainer": false,
            "right": "0px",
            "skin": "slFbox",
            "top": "30dp",
            "width": "100%",
            "zIndex": 1
        }, controller.args[0], "flxViewFeatureRow2"), extendConfig({}, controller.args[1], "flxViewFeatureRow2"), extendConfig({}, controller.args[2], "flxViewFeatureRow2"));
        flxViewFeatureRow2.setDefaultUnit(kony.flex.DP);
        var flxViewFeatureDefaultValue = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
            "clipBounds": true,
            "id": "flxViewFeatureDefaultValue",
            "isVisible": true,
            "layoutType": kony.flex.FLOW_VERTICAL,
            "left": "0px",
            "isModalContainer": false,
            "skin": "slFbox",
            "top": "0px",
            "width": "35%",
            "zIndex": 1
        }, controller.args[0], "flxViewFeatureDefaultValue"), extendConfig({}, controller.args[1], "flxViewFeatureDefaultValue"), extendConfig({}, controller.args[2], "flxViewFeatureDefaultValue"));
        flxViewFeatureDefaultValue.setDefaultUnit(kony.flex.DP);
        var lblDefaultValueHeading = new kony.ui.Label(extendConfig({
            "id": "lblDefaultValueHeading",
            "isVisible": true,
            "left": "0px",
            "skin": "sknlblLato696c7311px",
            "i18n_text": "kony.i18n.getLocalizedString(\"i18n.products.Default_Value_CAPS\")",
            "top": "0px",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, controller.args[0], "lblDefaultValueHeading"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblDefaultValueHeading"), extendConfig({}, controller.args[2], "lblDefaultValueHeading"));
        var lblViewDefaultValue = new kony.ui.Label(extendConfig({
            "id": "lblViewDefaultValue",
            "isVisible": true,
            "left": "0px",
            "right": "0px",
            "skin": "sknLbl485C75LatoRegular13Px",
            "text": "Opt-In",
            "top": "10px",
            "width": "100%",
            "zIndex": 1
        }, controller.args[0], "lblViewDefaultValue"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblViewDefaultValue"), extendConfig({}, controller.args[2], "lblViewDefaultValue"));
        flxViewFeatureDefaultValue.add(lblDefaultValueHeading, lblViewDefaultValue);
        var flxViewFeatureDescription = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
            "clipBounds": true,
            "id": "flxViewFeatureDescription",
            "isVisible": true,
            "layoutType": kony.flex.FLOW_VERTICAL,
            "left": "15px",
            "isModalContainer": false,
            "skin": "slFbox",
            "top": "0px",
            "width": "64%",
            "zIndex": 1
        }, controller.args[0], "flxViewFeatureDescription"), extendConfig({}, controller.args[1], "flxViewFeatureDescription"), extendConfig({}, controller.args[2], "flxViewFeatureDescription"));
        flxViewFeatureDescription.setDefaultUnit(kony.flex.DP);
        var lblViewDescriptionHeading = new kony.ui.Label(extendConfig({
            "id": "lblViewDescriptionHeading",
            "isVisible": true,
            "left": "0px",
            "skin": "sknlblLato696c7311px",
            "i18n_text": "kony.i18n.getLocalizedString(\"i18n.products.Description_CAPS\")",
            "top": "0px",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, controller.args[0], "lblViewDescriptionHeading"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblViewDescriptionHeading"), extendConfig({}, controller.args[2], "lblViewDescriptionHeading"));
        var lblViewDescriptionVal = new kony.ui.Label(extendConfig({
            "id": "lblViewDescriptionVal",
            "isVisible": true,
            "left": "0px",
            "right": "0px",
            "skin": "sknLbl485C75LatoRegular13Px",
            "text": "To avoid the inconvenience of declined transactions on your checking account by Overdraft Protection",
            "top": "10px",
            "width": "400px",
            "zIndex": 1
        }, controller.args[0], "lblViewDescriptionVal"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblViewDescriptionVal"), extendConfig({}, controller.args[2], "lblViewDescriptionVal"));
        flxViewFeatureDescription.add(lblViewDescriptionHeading, lblViewDescriptionVal);
        flxViewFeatureRow2.add(flxViewFeatureDefaultValue, flxViewFeatureDescription);
        var flxViewFeatureSeperator2 = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "clipBounds": true,
            "height": "1px",
            "id": "flxViewFeatureSeperator2",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "left": "0px",
            "isModalContainer": false,
            "skin": "sknFlxD7D9E0Separator",
            "top": "20px",
            "width": "100%",
            "zIndex": 1
        }, controller.args[0], "flxViewFeatureSeperator2"), extendConfig({}, controller.args[1], "flxViewFeatureSeperator2"), extendConfig({}, controller.args[2], "flxViewFeatureSeperator2"));
        flxViewFeatureSeperator2.setDefaultUnit(kony.flex.DP);
        flxViewFeatureSeperator2.add();
        var lblViewFeatureOptions = new kony.ui.Label(extendConfig({
            "id": "lblViewFeatureOptions",
            "isVisible": true,
            "left": "0px",
            "skin": "sknLatoRegular192B4516px",
            "i18n_text": "kony.i18n.getLocalizedString(\"i18n.products.Option_Details\")",
            "top": "20px",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, controller.args[0], "lblViewFeatureOptions"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblViewFeatureOptions"), extendConfig({}, controller.args[2], "lblViewFeatureOptions"));
        var flxViewFeatureRow3 = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
            "clipBounds": true,
            "id": "flxViewFeatureRow3",
            "isVisible": true,
            "layoutType": kony.flex.FLOW_HORIZONTAL,
            "left": "0px",
            "isModalContainer": false,
            "right": "0px",
            "skin": "slFbox",
            "top": "20px",
            "width": "100%",
            "zIndex": 1
        }, controller.args[0], "flxViewFeatureRow3"), extendConfig({}, controller.args[1], "flxViewFeatureRow3"), extendConfig({}, controller.args[2], "flxViewFeatureRow3"));
        flxViewFeatureRow3.setDefaultUnit(kony.flex.DP);
        var flxViewOptionDisplayType = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "clipBounds": true,
            "height": "40px",
            "id": "flxViewOptionDisplayType",
            "isVisible": true,
            "layoutType": kony.flex.FLOW_VERTICAL,
            "left": "0px",
            "isModalContainer": false,
            "skin": "slFbox",
            "top": "0px",
            "width": "31%",
            "zIndex": 1
        }, controller.args[0], "flxViewOptionDisplayType"), extendConfig({}, controller.args[1], "flxViewOptionDisplayType"), extendConfig({}, controller.args[2], "flxViewOptionDisplayType"));
        flxViewOptionDisplayType.setDefaultUnit(kony.flex.DP);
        var lblOptionDisplayTypeHeading = new kony.ui.Label(extendConfig({
            "id": "lblOptionDisplayTypeHeading",
            "isVisible": true,
            "left": "0px",
            "skin": "sknlblLato696c7311px",
            "i18n_text": "kony.i18n.getLocalizedString(\"i18n.products.Option_Display_Type_CAPS\")",
            "top": "0px",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, controller.args[0], "lblOptionDisplayTypeHeading"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblOptionDisplayTypeHeading"), extendConfig({}, controller.args[2], "lblOptionDisplayTypeHeading"));
        var lblOptionDisplayTypeValue = new kony.ui.Label(extendConfig({
            "id": "lblOptionDisplayTypeValue",
            "isVisible": true,
            "left": "0px",
            "skin": "sknLbl485C75LatoRegular13Px",
            "text": "Single Selection",
            "top": "10px",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, controller.args[0], "lblOptionDisplayTypeValue"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblOptionDisplayTypeValue"), extendConfig({}, controller.args[2], "lblOptionDisplayTypeValue"));
        flxViewOptionDisplayType.add(lblOptionDisplayTypeHeading, lblOptionDisplayTypeValue);
        flxViewFeatureRow3.add(flxViewOptionDisplayType);
        var flxViewFeatureRow4 = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
            "clipBounds": true,
            "id": "flxViewFeatureRow4",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "left": "0px",
            "isModalContainer": false,
            "skin": "sknFlxBdrD5D9DDRadius3px",
            "top": "24px",
            "width": "100%",
            "zIndex": 1
        }, controller.args[0], "flxViewFeatureRow4"), extendConfig({}, controller.args[1], "flxViewFeatureRow4"), extendConfig({}, controller.args[2], "flxViewFeatureRow4"));
        flxViewFeatureRow4.setDefaultUnit(kony.flex.DP);
        var flxViewFeatureSegHeading = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "clipBounds": true,
            "height": "44px",
            "id": "flxViewFeatureSegHeading",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "left": "0px",
            "isModalContainer": false,
            "skin": "slFbox",
            "top": "0px",
            "width": "100%",
            "zIndex": 1
        }, controller.args[0], "flxViewFeatureSegHeading"), extendConfig({}, controller.args[1], "flxViewFeatureSegHeading"), extendConfig({}, controller.args[2], "flxViewFeatureSegHeading"));
        flxViewFeatureSegHeading.setDefaultUnit(kony.flex.DP);
        var lblViewFeatureValHeading = new kony.ui.Label(extendConfig({
            "id": "lblViewFeatureValHeading",
            "isVisible": true,
            "left": "12px",
            "skin": "sknlblLato696c7311px",
            "i18n_text": "kony.i18n.getLocalizedString(\"i18n.products.Value_CAPS\")",
            "top": "21px",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, controller.args[0], "lblViewFeatureValHeading"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblViewFeatureValHeading"), extendConfig({}, controller.args[2], "lblViewFeatureValHeading"));
        var lblViewFeatureDescHeading = new kony.ui.Label(extendConfig({
            "id": "lblViewFeatureDescHeading",
            "isVisible": true,
            "left": "192px",
            "skin": "sknlblLato696c7311px",
            "i18n_text": "kony.i18n.getLocalizedString(\"i18n.products.Description_CAPS\")",
            "top": "21px",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, controller.args[0], "lblViewFeatureDescHeading"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblViewFeatureDescHeading"), extendConfig({}, controller.args[2], "lblViewFeatureDescHeading"));
        var flxViewFeatureSeparator3 = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "clipBounds": true,
            "height": "1px",
            "id": "flxViewFeatureSeparator3",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "left": "0px",
            "isModalContainer": false,
            "right": "0px",
            "skin": "sknFlxD7D9E0Separator",
            "top": "43px",
            "zIndex": 1
        }, controller.args[0], "flxViewFeatureSeparator3"), extendConfig({}, controller.args[1], "flxViewFeatureSeparator3"), extendConfig({}, controller.args[2], "flxViewFeatureSeparator3"));
        flxViewFeatureSeparator3.setDefaultUnit(kony.flex.DP);
        flxViewFeatureSeparator3.add();
        flxViewFeatureSegHeading.add(lblViewFeatureValHeading, lblViewFeatureDescHeading, flxViewFeatureSeparator3);
        var segViewFeature = new kony.ui.SegmentedUI2(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
            "data": [{
                "lblOptionDescription": "I want the Bank to authorize and pay overdrafts on my transactions",
                "lblOptionValue": "Opt-in"
            }, {
                "lblOptionDescription": "I want the Bank to authorize and pay overdrafts on my transactions",
                "lblOptionValue": "Opt-in"
            }],
            "groupCells": false,
            "id": "segViewFeature",
            "isVisible": true,
            "left": "0px",
            "needPageIndicator": true,
            "pageOffDotImage": "pageoffdot.png",
            "pageOnDotImage": "pageondot.png",
            "retainSelection": false,
            "right": "0px",
            "rowFocusSkin": "seg2Focus",
            "rowSkin": "seg2Normal",
            "rowTemplate": "flxSegViewFeatureOptions",
            "sectionHeaderSkin": "sliPhoneSegmentHeader",
            "selectionBehavior": constants.SEGUI_DEFAULT_BEHAVIOR,
            "separatorColor": "d5d9dd00",
            "separatorRequired": true,
            "separatorThickness": 1,
            "showScrollbars": false,
            "top": "44px",
            "viewType": constants.SEGUI_VIEW_TYPE_TABLEVIEW,
            "widgetDataMap": {
                "flxSegViewFeatureOptions": "flxSegViewFeatureOptions",
                "lblOptionDescription": "lblOptionDescription",
                "lblOptionValue": "lblOptionValue"
            },
            "zIndex": 1
        }, controller.args[0], "segViewFeature"), extendConfig({
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "segViewFeature"), extendConfig({}, controller.args[2], "segViewFeature"));
        flxViewFeatureRow4.add(flxViewFeatureSegHeading, segViewFeature);
        flxViewFeatureMain.add(flxViewFeatureRow1, flxViewFeatureRow2, flxViewFeatureSeperator2, lblViewFeatureOptions, flxViewFeatureRow3, flxViewFeatureRow4);
        flxViewFeatureScrollMain.add(flxViewFeatureMain);
        flxViewFeatureDetailsPopup.add(flxViewFeatureTopDetails, flxViewFeatureScrollMain);
        flxAddProductPopups.add(flxAddFeaturePopup, flxViewFeatureDetailsPopup);
        var popUp = new com.adminConsole.common.popUp(extendConfig({
            "centerX": "50%",
            "height": "100%",
            "id": "popUp",
            "isVisible": false,
            "layoutType": kony.flex.FREE_FORM,
            "masterType": constants.MASTER_TYPE_DEFAULT,
            "isModalContainer": false,
            "skin": "sknflxBg000000Op50",
            "top": "0px",
            "width": "100%",
            "zIndex": 1,
            "overrides": {
                "lblPopUpMainMessage": {
                    "text": "Delete Question?"
                },
                "popUp": {
                    "centerX": "50%",
                    "isVisible": false,
                    "left": "viz.val_cleared",
                    "top": "0px"
                }
            }
        }, controller.args[0], "popUp"), extendConfig({
            "overrides": {}
        }, controller.args[1], "popUp"), extendConfig({
            "overrides": {}
        }, controller.args[2], "popUp"));
        addProduct.add(flxAddProductMainContainer, flxAddProductPopups, popUp);
        return addProduct;
    }
})