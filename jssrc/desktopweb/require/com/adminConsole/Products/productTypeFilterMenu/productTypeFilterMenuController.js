define("com/adminConsole/Products/productTypeFilterMenu/userproductTypeFilterMenuController", function() {
    return {};
});
define("com/adminConsole/Products/productTypeFilterMenu/productTypeFilterMenuControllerActions", {
    /*
        This is an auto generated file and any modifications to it may result in corruption of the action sequence.
    */
});
define("com/adminConsole/Products/productTypeFilterMenu/productTypeFilterMenuController", ["com/adminConsole/Products/productTypeFilterMenu/userproductTypeFilterMenuController", "com/adminConsole/Products/productTypeFilterMenu/productTypeFilterMenuControllerActions"], function() {
    var controller = require("com/adminConsole/Products/productTypeFilterMenu/userproductTypeFilterMenuController");
    var actions = require("com/adminConsole/Products/productTypeFilterMenu/productTypeFilterMenuControllerActions");
    for (var key in actions) {
        controller[key] = actions[key];
    }
    return controller;
});
