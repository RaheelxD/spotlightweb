define(function() {
    return function(controller) {
        var productDetailsViewNew = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
            "clipBounds": true,
            "isMaster": true,
            "id": "productDetailsViewNew",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "left": "0dp",
            "isModalContainer": false,
            "skin": "sknflxffffffop100Bordercbcdd1Radius3px",
            "top": "0dp",
            "width": "100%"
        }, controller.args[0], "productDetailsViewNew"), extendConfig({}, controller.args[1], "productDetailsViewNew"), extendConfig({}, controller.args[2], "productDetailsViewNew"));
        productDetailsViewNew.setDefaultUnit(kony.flex.DP);
        var flxProductHeader = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "clipBounds": true,
            "height": "39px",
            "id": "flxProductHeader",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "left": "20px",
            "isModalContainer": false,
            "right": "20px",
            "skin": "slFbox",
            "top": "20px",
            "zIndex": 1
        }, controller.args[0], "flxProductHeader"), extendConfig({}, controller.args[1], "flxProductHeader"), extendConfig({}, controller.args[2], "flxProductHeader"));
        flxProductHeader.setDefaultUnit(kony.flex.DP);
        var lblProductName = new kony.ui.Label(extendConfig({
            "id": "lblProductName",
            "isVisible": true,
            "left": "0px",
            "skin": "sknLbl192b45LatoReg16px",
            "text": "30% Discount on mortgage loans",
            "top": "0px",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, controller.args[0], "lblProductName"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblProductName"), extendConfig({}, controller.args[2], "lblProductName"));
        var flxSeparatorHeader = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "bottom": "1px",
            "clipBounds": true,
            "height": "1px",
            "id": "flxSeparatorHeader",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "left": "0px",
            "isModalContainer": false,
            "skin": "sknflxe1e5edop100",
            "width": "100%",
            "zIndex": 1
        }, controller.args[0], "flxSeparatorHeader"), extendConfig({}, controller.args[1], "flxSeparatorHeader"), extendConfig({}, controller.args[2], "flxSeparatorHeader"));
        flxSeparatorHeader.setDefaultUnit(kony.flex.DP);
        flxSeparatorHeader.add();
        var flxStatus = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "clipBounds": true,
            "height": "16px",
            "id": "flxStatus",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "isModalContainer": false,
            "right": "15px",
            "skin": "slFbox",
            "top": "2px",
            "width": "7%",
            "zIndex": 1
        }, controller.args[0], "flxStatus"), extendConfig({}, controller.args[1], "flxStatus"), extendConfig({}, controller.args[2], "flxStatus"));
        flxStatus.setDefaultUnit(kony.flex.DP);
        var fontIconActive = new kony.ui.Label(extendConfig({
            "centerY": "50%",
            "height": "12px",
            "id": "fontIconActive",
            "isVisible": true,
            "left": "0px",
            "skin": "sknFontIconActivate",
            "text": "",
            "top": "0px",
            "width": "12px",
            "zIndex": 1
        }, controller.args[0], "fontIconActive"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "fontIconActive"), extendConfig({}, controller.args[2], "fontIconActive"));
        var lblProductStatus = new kony.ui.Label(extendConfig({
            "id": "lblProductStatus",
            "isVisible": true,
            "left": "17px",
            "skin": "sknLbl485C75LatoRegular13Px",
            "text": "Active",
            "top": "0px",
            "width": "70%",
            "zIndex": 1
        }, controller.args[0], "lblProductStatus"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblProductStatus"), extendConfig({}, controller.args[2], "lblProductStatus"));
        flxStatus.add(fontIconActive, lblProductStatus);
        flxProductHeader.add(lblProductName, flxSeparatorHeader, flxStatus);
        var flxProductDetails = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
            "bottom": "20px",
            "clipBounds": true,
            "id": "flxProductDetails",
            "isVisible": true,
            "layoutType": kony.flex.FLOW_VERTICAL,
            "left": "20px",
            "isModalContainer": false,
            "right": "20px",
            "skin": "slFbox",
            "top": "80px",
            "zIndex": 1
        }, controller.args[0], "flxProductDetails"), extendConfig({}, controller.args[1], "flxProductDetails"), extendConfig({}, controller.args[2], "flxProductDetails"));
        flxProductDetails.setDefaultUnit(kony.flex.DP);
        var flxProductDetails1 = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "clipBounds": true,
            "height": "39px",
            "id": "flxProductDetails1",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "left": "0px",
            "isModalContainer": false,
            "skin": "slFbox",
            "top": "0px",
            "width": "100%"
        }, controller.args[0], "flxProductDetails1"), extendConfig({}, controller.args[1], "flxProductDetails1"), extendConfig({}, controller.args[2], "flxProductDetails1"));
        flxProductDetails1.setDefaultUnit(kony.flex.DP);
        var flxProductName = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "clipBounds": true,
            "height": "100%",
            "id": "flxProductName",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "left": "0px",
            "isModalContainer": false,
            "skin": "slFbox",
            "top": "0px",
            "width": "33%",
            "zIndex": 1
        }, controller.args[0], "flxProductName"), extendConfig({}, controller.args[1], "flxProductName"), extendConfig({}, controller.args[2], "flxProductName"));
        flxProductName.setDefaultUnit(kony.flex.DP);
        var lblProductNameTag = new kony.ui.Label(extendConfig({
            "id": "lblProductNameTag",
            "isVisible": true,
            "left": "0px",
            "skin": "sknlblLato696c7311px",
            "text": "PRODUCT NAME",
            "top": "0px",
            "width": "90%",
            "zIndex": 1
        }, controller.args[0], "lblProductNameTag"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblProductNameTag"), extendConfig({}, controller.args[2], "lblProductNameTag"));
        var lblProductNameValue = new kony.ui.Label(extendConfig({
            "bottom": "0px",
            "id": "lblProductNameValue",
            "isVisible": true,
            "left": "0px",
            "skin": "sknlbl485C75LatoSemiBold13px",
            "text": "30% Discount on mortgage loans",
            "width": "90%",
            "zIndex": 1
        }, controller.args[0], "lblProductNameValue"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblProductNameValue"), extendConfig({}, controller.args[2], "lblProductNameValue"));
        flxProductName.add(lblProductNameTag, lblProductNameValue);
        var flxProductGroup = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "clipBounds": true,
            "height": "100%",
            "id": "flxProductGroup",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "left": "33%",
            "isModalContainer": false,
            "skin": "slFbox",
            "top": "0px",
            "width": "33%",
            "zIndex": 1
        }, controller.args[0], "flxProductGroup"), extendConfig({}, controller.args[1], "flxProductGroup"), extendConfig({}, controller.args[2], "flxProductGroup"));
        flxProductGroup.setDefaultUnit(kony.flex.DP);
        var lblProductGroupTag = new kony.ui.Label(extendConfig({
            "id": "lblProductGroupTag",
            "isVisible": true,
            "left": "0px",
            "skin": "sknlblLato696c7311px",
            "text": "PRODUCT GROUP",
            "top": "0px",
            "width": "90%",
            "zIndex": 1
        }, controller.args[0], "lblProductGroupTag"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblProductGroupTag"), extendConfig({}, controller.args[2], "lblProductGroupTag"));
        var lblProductGroup = new kony.ui.Label(extendConfig({
            "bottom": "0px",
            "id": "lblProductGroup",
            "isVisible": true,
            "left": "0px",
            "skin": "sknlbl485C75LatoSemiBold13px",
            "text": "Mortgage",
            "width": "90%",
            "zIndex": 1
        }, controller.args[0], "lblProductGroup"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblProductGroup"), extendConfig({}, controller.args[2], "lblProductGroup"));
        flxProductGroup.add(lblProductGroupTag, lblProductGroup);
        var flxProductLine = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "clipBounds": true,
            "height": "100%",
            "id": "flxProductLine",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "left": "66%",
            "isModalContainer": false,
            "skin": "slFbox",
            "top": "0px",
            "width": "33%",
            "zIndex": 1
        }, controller.args[0], "flxProductLine"), extendConfig({}, controller.args[1], "flxProductLine"), extendConfig({}, controller.args[2], "flxProductLine"));
        flxProductLine.setDefaultUnit(kony.flex.DP);
        var lblProductLineTag = new kony.ui.Label(extendConfig({
            "id": "lblProductLineTag",
            "isVisible": true,
            "left": "0px",
            "skin": "sknlblLato696c7311px",
            "text": "PRODUCT LINE",
            "top": "0px",
            "width": "90%",
            "zIndex": 1
        }, controller.args[0], "lblProductLineTag"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblProductLineTag"), extendConfig({}, controller.args[2], "lblProductLineTag"));
        var lblProductLine = new kony.ui.Label(extendConfig({
            "bottom": "0px",
            "id": "lblProductLine",
            "isVisible": true,
            "left": "0px",
            "skin": "sknlbl485C75LatoSemiBold13px",
            "text": "Lending",
            "width": "90%",
            "zIndex": 1
        }, controller.args[0], "lblProductLine"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblProductLine"), extendConfig({}, controller.args[2], "lblProductLine"));
        flxProductLine.add(lblProductLineTag, lblProductLine);
        flxProductDetails1.add(flxProductName, flxProductGroup, flxProductLine);
        var flxProductDetails2 = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "clipBounds": true,
            "height": "39px",
            "id": "flxProductDetails2",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "left": "0px",
            "isModalContainer": false,
            "skin": "slFbox",
            "top": "25px",
            "width": "100%"
        }, controller.args[0], "flxProductDetails2"), extendConfig({}, controller.args[1], "flxProductDetails2"), extendConfig({}, controller.args[2], "flxProductDetails2"));
        flxProductDetails2.setDefaultUnit(kony.flex.DP);
        var flxProductID = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "clipBounds": true,
            "height": "100%",
            "id": "flxProductID",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "left": "0px",
            "isModalContainer": false,
            "skin": "slFbox",
            "top": "0px",
            "width": "33%",
            "zIndex": 1
        }, controller.args[0], "flxProductID"), extendConfig({}, controller.args[1], "flxProductID"), extendConfig({}, controller.args[2], "flxProductID"));
        flxProductID.setDefaultUnit(kony.flex.DP);
        var lblProductIDTag = new kony.ui.Label(extendConfig({
            "id": "lblProductIDTag",
            "isVisible": true,
            "left": "0px",
            "skin": "sknlblLato696c7311px",
            "text": "PRODUCT ID",
            "top": "0px",
            "width": "90%",
            "zIndex": 1
        }, controller.args[0], "lblProductIDTag"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblProductIDTag"), extendConfig({}, controller.args[2], "lblProductIDTag"));
        var lblProductID = new kony.ui.Label(extendConfig({
            "bottom": "0px",
            "id": "lblProductID",
            "isVisible": true,
            "left": "0px",
            "skin": "sknlbl485C75LatoSemiBold13px",
            "text": "Vehicle Loan",
            "width": "90%",
            "zIndex": 1
        }, controller.args[0], "lblProductID"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblProductID"), extendConfig({}, controller.args[2], "lblProductID"));
        flxProductID.add(lblProductIDTag, lblProductID);
        var flxCurrencyID = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "clipBounds": true,
            "height": "100%",
            "id": "flxCurrencyID",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "left": "33%",
            "isModalContainer": false,
            "skin": "slFbox",
            "top": "0px",
            "width": "33%",
            "zIndex": 1
        }, controller.args[0], "flxCurrencyID"), extendConfig({}, controller.args[1], "flxCurrencyID"), extendConfig({}, controller.args[2], "flxCurrencyID"));
        flxCurrencyID.setDefaultUnit(kony.flex.DP);
        var lblCurrencyIDTag = new kony.ui.Label(extendConfig({
            "id": "lblCurrencyIDTag",
            "isVisible": true,
            "left": "0px",
            "skin": "sknlblLato696c7311px",
            "text": "CURRENCY ID",
            "top": "0px",
            "width": "90%",
            "zIndex": 1
        }, controller.args[0], "lblCurrencyIDTag"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblCurrencyIDTag"), extendConfig({}, controller.args[2], "lblCurrencyIDTag"));
        var lblCurrencyID = new kony.ui.Label(extendConfig({
            "bottom": "0px",
            "id": "lblCurrencyID",
            "isVisible": true,
            "left": "0px",
            "skin": "sknlbl485C75LatoSemiBold13px",
            "text": "USD",
            "width": "90%",
            "zIndex": 1
        }, controller.args[0], "lblCurrencyID"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblCurrencyID"), extendConfig({}, controller.args[2], "lblCurrencyID"));
        flxCurrencyID.add(lblCurrencyIDTag, lblCurrencyID);
        var flxCreditInterest = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "clipBounds": true,
            "height": "100%",
            "id": "flxCreditInterest",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "left": "66%",
            "isModalContainer": false,
            "skin": "slFbox",
            "top": "0px",
            "width": "33%",
            "zIndex": 1
        }, controller.args[0], "flxCreditInterest"), extendConfig({}, controller.args[1], "flxCreditInterest"), extendConfig({}, controller.args[2], "flxCreditInterest"));
        flxCreditInterest.setDefaultUnit(kony.flex.DP);
        var lblCreditInterestTag = new kony.ui.Label(extendConfig({
            "id": "lblCreditInterestTag",
            "isVisible": true,
            "left": "0px",
            "skin": "sknlblLato696c7311px",
            "text": "CREDIT INTEREST",
            "top": "0px",
            "width": "90%",
            "zIndex": 1
        }, controller.args[0], "lblCreditInterestTag"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblCreditInterestTag"), extendConfig({}, controller.args[2], "lblCreditInterestTag"));
        var lblCreditInterest = new kony.ui.Label(extendConfig({
            "bottom": "0px",
            "id": "lblCreditInterest",
            "isVisible": true,
            "left": "0px",
            "skin": "sknlbl485C75LatoSemiBold13px",
            "text": "18%",
            "width": "90%",
            "zIndex": 1
        }, controller.args[0], "lblCreditInterest"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblCreditInterest"), extendConfig({}, controller.args[2], "lblCreditInterest"));
        flxCreditInterest.add(lblCreditInterestTag, lblCreditInterest);
        flxProductDetails2.add(flxProductID, flxCurrencyID, flxCreditInterest);
        var flxProductDetails3 = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "clipBounds": true,
            "height": "39px",
            "id": "flxProductDetails3",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "left": "0px",
            "isModalContainer": false,
            "skin": "slFbox",
            "top": "25px",
            "width": "100%"
        }, controller.args[0], "flxProductDetails3"), extendConfig({}, controller.args[1], "flxProductDetails3"), extendConfig({}, controller.args[2], "flxProductDetails3"));
        flxProductDetails3.setDefaultUnit(kony.flex.DP);
        var flxFees = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "clipBounds": true,
            "height": "100%",
            "id": "flxFees",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "left": "0px",
            "isModalContainer": false,
            "skin": "slFbox",
            "top": "0px",
            "width": "33%",
            "zIndex": 1
        }, controller.args[0], "flxFees"), extendConfig({}, controller.args[1], "flxFees"), extendConfig({}, controller.args[2], "flxFees"));
        flxFees.setDefaultUnit(kony.flex.DP);
        var lblFeesTag = new kony.ui.Label(extendConfig({
            "id": "lblFeesTag",
            "isVisible": true,
            "left": "0px",
            "skin": "sknlblLato696c7311px",
            "text": "FEES",
            "top": "0px",
            "width": "90%",
            "zIndex": 1
        }, controller.args[0], "lblFeesTag"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblFeesTag"), extendConfig({}, controller.args[2], "lblFeesTag"));
        var lblFees = new kony.ui.Label(extendConfig({
            "bottom": "0px",
            "id": "lblFees",
            "isVisible": true,
            "left": "0px",
            "skin": "sknlbl485C75LatoSemiBold13px",
            "text": "$ 12",
            "width": "90%",
            "zIndex": 1
        }, controller.args[0], "lblFees"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblFees"), extendConfig({}, controller.args[2], "lblFees"));
        flxFees.add(lblFeesTag, lblFees);
        var flxTerm = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "clipBounds": true,
            "height": "100%",
            "id": "flxTerm",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "left": "33%",
            "isModalContainer": false,
            "skin": "slFbox",
            "top": "0px",
            "width": "33%",
            "zIndex": 1
        }, controller.args[0], "flxTerm"), extendConfig({}, controller.args[1], "flxTerm"), extendConfig({}, controller.args[2], "flxTerm"));
        flxTerm.setDefaultUnit(kony.flex.DP);
        var lblTermTag = new kony.ui.Label(extendConfig({
            "id": "lblTermTag",
            "isVisible": true,
            "left": "0px",
            "skin": "sknlblLato696c7311px",
            "text": "TERM",
            "top": "0px",
            "width": "90%",
            "zIndex": 1
        }, controller.args[0], "lblTermTag"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblTermTag"), extendConfig({}, controller.args[2], "lblTermTag"));
        var lblTerm = new kony.ui.Label(extendConfig({
            "bottom": "0px",
            "id": "lblTerm",
            "isVisible": true,
            "left": "0px",
            "skin": "sknlbl485C75LatoSemiBold13px",
            "text": "12 Years",
            "width": "90%",
            "zIndex": 1
        }, controller.args[0], "lblTerm"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblTerm"), extendConfig({}, controller.args[2], "lblTerm"));
        flxTerm.add(lblTermTag, lblTerm);
        var flxType = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "clipBounds": true,
            "height": "100%",
            "id": "flxType",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "left": "66%",
            "isModalContainer": false,
            "skin": "slFbox",
            "top": "0px",
            "width": "33%",
            "zIndex": 1
        }, controller.args[0], "flxType"), extendConfig({}, controller.args[1], "flxType"), extendConfig({}, controller.args[2], "flxType"));
        flxType.setDefaultUnit(kony.flex.DP);
        var lblTypeTag = new kony.ui.Label(extendConfig({
            "id": "lblTypeTag",
            "isVisible": true,
            "left": "0px",
            "skin": "sknlblLato696c7311px",
            "text": "PRODUCT TYPE",
            "top": "0px",
            "width": "90%",
            "zIndex": 1
        }, controller.args[0], "lblTypeTag"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblTypeTag"), extendConfig({}, controller.args[2], "lblTypeTag"));
        var lblType = new kony.ui.Label(extendConfig({
            "bottom": "0px",
            "id": "lblType",
            "isVisible": true,
            "left": "0px",
            "skin": "sknlbl485C75LatoSemiBold13px",
            "text": "Internal Product",
            "width": "90%",
            "zIndex": 1
        }, controller.args[0], "lblType"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblType"), extendConfig({}, controller.args[2], "lblType"));
        flxType.add(lblTypeTag, lblType);
        flxProductDetails3.add(flxFees, flxTerm, flxType);
        var flxProductDetails4 = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "clipBounds": true,
            "height": "39px",
            "id": "flxProductDetails4",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "left": "0px",
            "isModalContainer": false,
            "skin": "slFbox",
            "top": "25px",
            "width": "100%"
        }, controller.args[0], "flxProductDetails4"), extendConfig({}, controller.args[1], "flxProductDetails4"), extendConfig({}, controller.args[2], "flxProductDetails4"));
        flxProductDetails4.setDefaultUnit(kony.flex.DP);
        var flxMinimumAmount = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "clipBounds": true,
            "height": "100%",
            "id": "flxMinimumAmount",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "left": "0px",
            "isModalContainer": false,
            "skin": "slFbox",
            "top": "0px",
            "width": "33%",
            "zIndex": 1
        }, controller.args[0], "flxMinimumAmount"), extendConfig({}, controller.args[1], "flxMinimumAmount"), extendConfig({}, controller.args[2], "flxMinimumAmount"));
        flxMinimumAmount.setDefaultUnit(kony.flex.DP);
        var lblMinimumAmountTag = new kony.ui.Label(extendConfig({
            "id": "lblMinimumAmountTag",
            "isVisible": true,
            "left": "0px",
            "skin": "sknlblLato696c7311px",
            "text": "MINIMUM AMOUNT",
            "top": "0px",
            "width": "90%",
            "zIndex": 1
        }, controller.args[0], "lblMinimumAmountTag"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblMinimumAmountTag"), extendConfig({}, controller.args[2], "lblMinimumAmountTag"));
        var lblMinimumAmount = new kony.ui.Label(extendConfig({
            "bottom": "0px",
            "id": "lblMinimumAmount",
            "isVisible": true,
            "left": "0px",
            "skin": "sknlbl485C75LatoSemiBold13px",
            "text": "$ 0",
            "width": "90%",
            "zIndex": 1
        }, controller.args[0], "lblMinimumAmount"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblMinimumAmount"), extendConfig({}, controller.args[2], "lblMinimumAmount"));
        flxMinimumAmount.add(lblMinimumAmountTag, lblMinimumAmount);
        var flxMaximumAmount = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "clipBounds": true,
            "height": "100%",
            "id": "flxMaximumAmount",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "left": "33%",
            "isModalContainer": false,
            "skin": "slFbox",
            "top": "0px",
            "width": "33%",
            "zIndex": 1
        }, controller.args[0], "flxMaximumAmount"), extendConfig({}, controller.args[1], "flxMaximumAmount"), extendConfig({}, controller.args[2], "flxMaximumAmount"));
        flxMaximumAmount.setDefaultUnit(kony.flex.DP);
        var lblMaximumAmountTag = new kony.ui.Label(extendConfig({
            "id": "lblMaximumAmountTag",
            "isVisible": true,
            "left": "0px",
            "skin": "sknlblLato696c7311px",
            "text": "MAXIMUM AMOUNT",
            "top": "0px",
            "width": "90%",
            "zIndex": 1
        }, controller.args[0], "lblMaximumAmountTag"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblMaximumAmountTag"), extendConfig({}, controller.args[2], "lblMaximumAmountTag"));
        var lblMaximumAmount = new kony.ui.Label(extendConfig({
            "bottom": "0px",
            "id": "lblMaximumAmount",
            "isVisible": true,
            "left": "0px",
            "skin": "sknlbl485C75LatoSemiBold13px",
            "text": "$ 12000",
            "width": "90%",
            "zIndex": 1
        }, controller.args[0], "lblMaximumAmount"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblMaximumAmount"), extendConfig({}, controller.args[2], "lblMaximumAmount"));
        flxMaximumAmount.add(lblMaximumAmountTag, lblMaximumAmount);
        var flxOverdraftAmount = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "clipBounds": true,
            "height": "100%",
            "id": "flxOverdraftAmount",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "left": "66%",
            "isModalContainer": false,
            "skin": "slFbox",
            "top": "0px",
            "width": "33%",
            "zIndex": 1
        }, controller.args[0], "flxOverdraftAmount"), extendConfig({}, controller.args[1], "flxOverdraftAmount"), extendConfig({}, controller.args[2], "flxOverdraftAmount"));
        flxOverdraftAmount.setDefaultUnit(kony.flex.DP);
        var lblOverdraftAmountTag = new kony.ui.Label(extendConfig({
            "id": "lblOverdraftAmountTag",
            "isVisible": true,
            "left": "0px",
            "skin": "sknlblLato696c7311px",
            "text": "OVERDRAFT AMOUNT",
            "top": "0px",
            "width": "90%",
            "zIndex": 1
        }, controller.args[0], "lblOverdraftAmountTag"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblOverdraftAmountTag"), extendConfig({}, controller.args[2], "lblOverdraftAmountTag"));
        var lblOverdraftAmount = new kony.ui.Label(extendConfig({
            "bottom": "0px",
            "id": "lblOverdraftAmount",
            "isVisible": true,
            "left": "0px",
            "skin": "sknlbl485C75LatoSemiBold13px",
            "text": "$ 12000",
            "width": "90%",
            "zIndex": 1
        }, controller.args[0], "lblOverdraftAmount"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblOverdraftAmount"), extendConfig({}, controller.args[2], "lblOverdraftAmount"));
        flxOverdraftAmount.add(lblOverdraftAmountTag, lblOverdraftAmount);
        flxProductDetails4.add(flxMinimumAmount, flxMaximumAmount, flxOverdraftAmount);
        var flxProductDetails5 = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "clipBounds": true,
            "height": "39px",
            "id": "flxProductDetails5",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "left": "0px",
            "isModalContainer": false,
            "skin": "slFbox",
            "top": "25px",
            "width": "100%"
        }, controller.args[0], "flxProductDetails5"), extendConfig({}, controller.args[1], "flxProductDetails5"), extendConfig({}, controller.args[2], "flxProductDetails5"));
        flxProductDetails5.setDefaultUnit(kony.flex.DP);
        var flxNoticePeriod = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "clipBounds": true,
            "height": "100%",
            "id": "flxNoticePeriod",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "left": "0px",
            "isModalContainer": false,
            "skin": "slFbox",
            "top": "0px",
            "width": "33%",
            "zIndex": 1
        }, controller.args[0], "flxNoticePeriod"), extendConfig({}, controller.args[1], "flxNoticePeriod"), extendConfig({}, controller.args[2], "flxNoticePeriod"));
        flxNoticePeriod.setDefaultUnit(kony.flex.DP);
        var lblNoticePeriodTag = new kony.ui.Label(extendConfig({
            "id": "lblNoticePeriodTag",
            "isVisible": true,
            "left": "0px",
            "skin": "sknlblLato696c7311px",
            "text": "NOTICE PERIOD",
            "top": "0px",
            "width": "90%",
            "zIndex": 1
        }, controller.args[0], "lblNoticePeriodTag"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblNoticePeriodTag"), extendConfig({}, controller.args[2], "lblNoticePeriodTag"));
        var lblNoticePeriod = new kony.ui.Label(extendConfig({
            "bottom": "0px",
            "id": "lblNoticePeriod",
            "isVisible": true,
            "left": "0px",
            "skin": "sknlbl485C75LatoSemiBold13px",
            "text": "2 months",
            "width": "90%",
            "zIndex": 1
        }, controller.args[0], "lblNoticePeriod"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblNoticePeriod"), extendConfig({}, controller.args[2], "lblNoticePeriod"));
        flxNoticePeriod.add(lblNoticePeriodTag, lblNoticePeriod);
        var flxPurpose = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "clipBounds": true,
            "height": "100%",
            "id": "flxPurpose",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "left": "33%",
            "isModalContainer": false,
            "skin": "slFbox",
            "top": "0px",
            "width": "33%",
            "zIndex": 1
        }, controller.args[0], "flxPurpose"), extendConfig({}, controller.args[1], "flxPurpose"), extendConfig({}, controller.args[2], "flxPurpose"));
        flxPurpose.setDefaultUnit(kony.flex.DP);
        var lblPurposeTag = new kony.ui.Label(extendConfig({
            "id": "lblPurposeTag",
            "isVisible": true,
            "left": "0px",
            "skin": "sknlblLato696c7311px",
            "text": "PURPOSE",
            "top": "0px",
            "width": "90%",
            "zIndex": 1
        }, controller.args[0], "lblPurposeTag"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblPurposeTag"), extendConfig({}, controller.args[2], "lblPurposeTag"));
        var lblPurpose = new kony.ui.Label(extendConfig({
            "bottom": "0px",
            "id": "lblPurpose",
            "isVisible": true,
            "left": "0px",
            "skin": "sknlbl485C75LatoSemiBold13px",
            "text": "Customer Lending",
            "width": "90%",
            "zIndex": 1
        }, controller.args[0], "lblPurpose"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblPurpose"), extendConfig({}, controller.args[2], "lblPurpose"));
        flxPurpose.add(lblPurposeTag, lblPurpose);
        flxProductDetails5.add(flxNoticePeriod, flxPurpose);
        var flxProductDetails6 = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
            "clipBounds": true,
            "id": "flxProductDetails6",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "left": "0px",
            "isModalContainer": false,
            "skin": "slFbox",
            "top": "25px",
            "width": "100%"
        }, controller.args[0], "flxProductDetails6"), extendConfig({}, controller.args[1], "flxProductDetails6"), extendConfig({}, controller.args[2], "flxProductDetails6"));
        flxProductDetails6.setDefaultUnit(kony.flex.DP);
        var flxDescription = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
            "clipBounds": true,
            "id": "flxDescription",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "left": "0px",
            "isModalContainer": false,
            "skin": "slFbox",
            "top": "0px",
            "width": "100%",
            "zIndex": 1
        }, controller.args[0], "flxDescription"), extendConfig({}, controller.args[1], "flxDescription"), extendConfig({}, controller.args[2], "flxDescription"));
        flxDescription.setDefaultUnit(kony.flex.DP);
        var lblDescriptionTag = new kony.ui.Label(extendConfig({
            "id": "lblDescriptionTag",
            "isVisible": true,
            "left": "0px",
            "skin": "sknlblLato696c7311px",
            "text": "DESCRIPTION",
            "top": "0px",
            "width": "10%",
            "zIndex": 1
        }, controller.args[0], "lblDescriptionTag"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblDescriptionTag"), extendConfig({}, controller.args[2], "lblDescriptionTag"));
        var lblDescription = new kony.ui.Label(extendConfig({
            "bottom": "2px",
            "id": "lblDescription",
            "isVisible": true,
            "left": "0px",
            "skin": "sknlbl485C75LatoSemiBold13px",
            "text": "Our small business and commercial deposit accounts make banking simple and our products can grow with you as your business grows.",
            "top": "23px",
            "width": "100%",
            "zIndex": 1
        }, controller.args[0], "lblDescription"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblDescription"), extendConfig({}, controller.args[2], "lblDescription"));
        flxDescription.add(lblDescriptionTag, lblDescription);
        flxProductDetails6.add(flxDescription);
        flxProductDetails.add(flxProductDetails1, flxProductDetails2, flxProductDetails3, flxProductDetails4, flxProductDetails5, flxProductDetails6);
        productDetailsViewNew.add(flxProductHeader, flxProductDetails);
        return productDetailsViewNew;
    }
})