define("com/adminConsole/Products/productDetailsViewNew/userproductDetailsViewNewController", function() {
    return {};
});
define("com/adminConsole/Products/productDetailsViewNew/productDetailsViewNewControllerActions", {
    /*
          This is an auto generated file and any modifications to it may result in corruption of the action sequence.
        */
});
define("com/adminConsole/Products/productDetailsViewNew/productDetailsViewNewController", ["com/adminConsole/Products/productDetailsViewNew/userproductDetailsViewNewController", "com/adminConsole/Products/productDetailsViewNew/productDetailsViewNewControllerActions"], function() {
    var controller = require("com/adminConsole/Products/productDetailsViewNew/userproductDetailsViewNewController");
    var actions = require("com/adminConsole/Products/productDetailsViewNew/productDetailsViewNewControllerActions");
    for (var key in actions) {
        controller[key] = actions[key];
    }
    return controller;
});
