define("com/adminConsole/locations/mainHeader/usermainHeaderController", function() {
    var VisibleState = function(initialState) {
        this.isVisible = initialState;
        this.setVisiblity = function(show) {
            this.isVisible = show;
        };
    };
    var controller = {
        imgLogout_onTouchStart: function() {
            var authModule = kony.mvc.MDAApplication.getSharedInstance().getModuleManager().getModule("AuthModule");
            authModule.presentationController.doLogout();
        },
        frmCSR_btnCreateNewMessage: new VisibleState(true),
        frmCSR_btnCreateMessageTemplate: new VisibleState(true),
        frmCSR_setBtnDropDownText: function(text) {
            if (text === kony.i18n.getLocalizedString("i18n.frmCSRController.CREATE_NEW_MESSAGE")) {
                this.view.btnDropdownList.text = text;
                this.view.btnDropdownList.isVisible = controller.frmCSR_btnCreateNewMessage.isVisible;
            } else if (text === kony.i18n.getLocalizedString("i18n.frmCSRController.CREATE_NEW_TEMPLATE")) {
                this.view.btnDropdownList.text = text;
                this.view.btnDropdownList.isVisible = controller.frmCSR_btnCreateMessageTemplate.isVisible;
            }
        }
    };
    return controller;
});
define("com/adminConsole/locations/mainHeader/mainHeaderControllerActions", {
    /*
        This is an auto generated file and any modifications to it may result in corruption of the action sequence.
    */
});
define("com/adminConsole/locations/mainHeader/mainHeaderController", ["com/adminConsole/locations/mainHeader/usermainHeaderController", "com/adminConsole/locations/mainHeader/mainHeaderControllerActions"], function() {
    var controller = require("com/adminConsole/locations/mainHeader/usermainHeaderController");
    var actions = require("com/adminConsole/locations/mainHeader/mainHeaderControllerActions");
    for (var key in actions) {
        controller[key] = actions[key];
    }
    return controller;
});
