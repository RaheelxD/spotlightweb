define("com/adminConsole/staticContent/noStaticData/usernoStaticDataController", function() {
    return {
        setCompFlowActions: function() {},
    };
});
define("com/adminConsole/staticContent/noStaticData/noStaticDataControllerActions", {
    /*
        This is an auto generated file and any modifications to it may result in corruption of the action sequence.
    */
    AS_FlexContainer_j1a6b556b4e842839c809061c5560949: function AS_FlexContainer_j1a6b556b4e842839c809061c5560949(eventobject) {
        var self = this;
    }
});
define("com/adminConsole/staticContent/noStaticData/noStaticDataController", ["com/adminConsole/staticContent/noStaticData/usernoStaticDataController", "com/adminConsole/staticContent/noStaticData/noStaticDataControllerActions"], function() {
    var controller = require("com/adminConsole/staticContent/noStaticData/usernoStaticDataController");
    var actions = require("com/adminConsole/staticContent/noStaticData/noStaticDataControllerActions");
    for (var key in actions) {
        controller[key] = actions[key];
    }
    return controller;
});
