define(function() {
    return function(controller) {
        var noStaticData = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "clipBounds": true,
            "isMaster": true,
            "height": "340px",
            "id": "noStaticData",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "left": "35px",
            "isModalContainer": false,
            "onClick": controller.AS_FlexContainer_j1a6b556b4e842839c809061c5560949,
            "right": "35px",
            "skin": "slFbox",
            "top": "0dp",
            "zIndex": 1
        }, controller.args[0], "noStaticData"), extendConfig({}, controller.args[1], "noStaticData"), extendConfig({}, controller.args[2], "noStaticData"));
        noStaticData.setDefaultUnit(kony.flex.DP);
        var flxNoStaticDataWrapper = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "bottom": "0px",
            "clipBounds": true,
            "height": "100%",
            "id": "flxNoStaticDataWrapper",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "left": "0px",
            "isModalContainer": false,
            "right": "0px",
            "skin": "sknflxffffffBorderd6dbe7Radius4px",
            "top": "0px",
            "width": "100%",
            "zIndex": 1
        }, controller.args[0], "flxNoStaticDataWrapper"), extendConfig({}, controller.args[1], "flxNoStaticDataWrapper"), extendConfig({}, controller.args[2], "flxNoStaticDataWrapper"));
        flxNoStaticDataWrapper.setDefaultUnit(kony.flex.DP);
        var lblNoStaticContentCreated = new kony.ui.Label(extendConfig({
            "centerX": "50%",
            "id": "lblNoStaticContentCreated",
            "isVisible": true,
            "skin": "sknlblLatoBold0a76813b6d67e48Static",
            "i18n_text": "kony.i18n.getLocalizedString(\"i18n.TermsAndConditions.noTnCMessage\")",
            "top": "100px",
            "width": "100%",
            "zIndex": 1
        }, controller.args[0], "lblNoStaticContentCreated"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_CENTER,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblNoStaticContentCreated"), extendConfig({}, controller.args[2], "lblNoStaticContentCreated"));
        var lblNoStaticContentMsg = new kony.ui.Label(extendConfig({
            "centerX": "50%",
            "height": "20px",
            "id": "lblNoStaticContentMsg",
            "isVisible": true,
            "skin": "sknlbllatoRegular0d4f9ffd20d2843Static",
            "i18n_text": "kony.i18n.getLocalizedString(\"i18n.TermsAndConditions.addTnCMessage\")",
            "top": "139px",
            "width": "100%",
            "zIndex": 1
        }, controller.args[0], "lblNoStaticContentMsg"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_CENTER,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblNoStaticContentMsg"), extendConfig({}, controller.args[2], "lblNoStaticContentMsg"));
        var btnAddStaticContent = new kony.ui.Button(extendConfig({
            "centerX": "50%",
            "focusSkin": "sknBtn003E75LatoRegular13pxFFFFFFRad20px",
            "height": "40px",
            "id": "btnAddStaticContent",
            "isVisible": true,
            "left": 0,
            "skin": "sknBtn003E75LatoRegular13pxFFFFFFRad20px",
            "i18n_text": "kony.i18n.getLocalizedString(\"i18n.TermsAndConditions.addBTN\")",
            "top": "177px",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, controller.args[0], "btnAddStaticContent"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_CENTER,
            "displayText": true,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "btnAddStaticContent"), extendConfig({
            "hoverSkin": "sknBtn005198LatoRegular13pxFFFFFFRad20px"
        }, controller.args[2], "btnAddStaticContent"));
        flxNoStaticDataWrapper.add(lblNoStaticContentCreated, lblNoStaticContentMsg, btnAddStaticContent);
        noStaticData.add(flxNoStaticDataWrapper);
        return noStaticData;
    }
})