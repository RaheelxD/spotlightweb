define(function() {
    return function(controller) {
        var staticData = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "clipBounds": false,
            "isMaster": true,
            "height": "500px",
            "id": "staticData",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "left": "35px",
            "isModalContainer": false,
            "preShow": function(eventobject) {
                controller.AS_FlexContainer_gd36cabed06d463cb80493b6dfe08033(eventobject);
            },
            "right": "35px",
            "skin": "sknFlxFFFFFFbrdre1e5ed",
            "top": "0dp",
            "zIndex": 1
        }, controller.args[0], "staticData"), extendConfig({}, controller.args[1], "staticData"), extendConfig({}, controller.args[2], "staticData"));
        staticData.setDefaultUnit(kony.flex.DP);
        var flxHeader = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "clipBounds": true,
            "height": "68px",
            "id": "flxHeader",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "left": "0",
            "isModalContainer": false,
            "skin": "sknflxffffffop100",
            "top": "0",
            "width": "100%"
        }, controller.args[0], "flxHeader"), extendConfig({}, controller.args[1], "flxHeader"), extendConfig({}, controller.args[2], "flxHeader"));
        flxHeader.setDefaultUnit(kony.flex.DP);
        var flxStaticContantHeader = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "clipBounds": true,
            "height": "100%",
            "id": "flxStaticContantHeader",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "left": "0px",
            "isModalContainer": false,
            "skin": "slFbox",
            "top": "0dp",
            "width": "100%",
            "zIndex": 1
        }, controller.args[0], "flxStaticContantHeader"), extendConfig({}, controller.args[1], "flxStaticContantHeader"), extendConfig({}, controller.args[2], "flxStaticContantHeader"));
        flxStaticContantHeader.setDefaultUnit(kony.flex.DP);
        var lblStaticContentHeader = new kony.ui.Label(extendConfig({
            "centerY": "50%",
            "id": "lblStaticContentHeader",
            "isVisible": true,
            "left": 20,
            "onTouchEnd": controller.AS_Label_b388eebaef3c4fe594f561b7cfbd6c59,
            "skin": "sknlblLatoBold35475f14px",
            "i18n_text": "kony.i18n.getLocalizedString(\"i18n.PrivacyPolicy.konyMessage\")",
            "width": "40%",
            "zIndex": 1
        }, controller.args[0], "lblStaticContentHeader"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblStaticContentHeader"), extendConfig({}, controller.args[2], "lblStaticContentHeader"));
        var flxStatus = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "centerY": "50%",
            "clipBounds": true,
            "height": "100%",
            "id": "flxStatus",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "isModalContainer": false,
            "right": "100px",
            "skin": "slFbox",
            "width": "57px",
            "zIndex": 1
        }, controller.args[0], "flxStatus"), extendConfig({}, controller.args[1], "flxStatus"), extendConfig({}, controller.args[2], "flxStatus"));
        flxStatus.setDefaultUnit(kony.flex.DP);
        var lblstaticContentStatus = new kony.ui.Label(extendConfig({
            "centerY": "50%",
            "id": "lblstaticContentStatus",
            "isVisible": true,
            "left": "15px",
            "skin": "sknlblLato5bc06cBold14px",
            "i18n_text": "kony.i18n.getLocalizedString(\"i18n.secureimage.Active\")",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, controller.args[0], "lblstaticContentStatus"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblstaticContentStatus"), extendConfig({}, controller.args[2], "lblstaticContentStatus"));
        var fontIconImgStaticContentStatus = new kony.ui.Label(extendConfig({
            "centerY": "50%",
            "id": "fontIconImgStaticContentStatus",
            "isVisible": true,
            "left": "0dp",
            "skin": "sknFontIconActivate",
            "i18n_text": "kony.i18n.getLocalizedString(\"i18n.userwidgetmodel.lblFontIconStatus1\")",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, controller.args[0], "fontIconImgStaticContentStatus"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "fontIconImgStaticContentStatus"), extendConfig({}, controller.args[2], "fontIconImgStaticContentStatus"));
        flxStatus.add(lblstaticContentStatus, fontIconImgStaticContentStatus);
        var flxOptions = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "centerY": "50.00%",
            "clipBounds": true,
            "height": "24px",
            "id": "flxOptions",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "isModalContainer": false,
            "right": "35px",
            "skin": "sknFlxBorffffff1pxRound",
            "top": "0dp",
            "width": "24px",
            "zIndex": 10
        }, controller.args[0], "flxOptions"), extendConfig({}, controller.args[1], "flxOptions"), extendConfig({
            "hoverSkin": "sknflxffffffop100Border424242Radius100px"
        }, controller.args[2], "flxOptions"));
        flxOptions.setDefaultUnit(kony.flex.DP);
        var fontIconImgOptions = new kony.ui.Label(extendConfig({
            "centerX": "50%",
            "centerY": "50%",
            "id": "fontIconImgOptions",
            "isVisible": true,
            "skin": "sknFontIconOptionMenu",
            "i18n_text": "kony.i18n.getLocalizedString(\"i18n.userwidgetmodel.fontIconImgOptions\")",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, controller.args[0], "fontIconImgOptions"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "fontIconImgOptions"), extendConfig({}, controller.args[2], "fontIconImgOptions"));
        flxOptions.add(fontIconImgOptions);
        var flxHeaderSeperator = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "bottom": "1dp",
            "clipBounds": true,
            "height": "1dp",
            "id": "flxHeaderSeperator",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "left": "20dp",
            "isModalContainer": false,
            "right": "20dp",
            "skin": "sknflx115678",
            "zIndex": 1
        }, controller.args[0], "flxHeaderSeperator"), extendConfig({}, controller.args[1], "flxHeaderSeperator"), extendConfig({}, controller.args[2], "flxHeaderSeperator"));
        flxHeaderSeperator.setDefaultUnit(kony.flex.DP);
        flxHeaderSeperator.add();
        flxStaticContantHeader.add(lblStaticContentHeader, flxStatus, flxOptions, flxHeaderSeperator);
        flxHeader.add(flxStaticContantHeader);
        var flxSelectOptions = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
            "clipBounds": false,
            "id": "flxSelectOptions",
            "isVisible": true,
            "layoutType": kony.flex.FLOW_VERTICAL,
            "isModalContainer": false,
            "right": "22px",
            "skin": "slFbox",
            "top": "46px",
            "width": "160px",
            "zIndex": 111
        }, controller.args[0], "flxSelectOptions"), extendConfig({}, controller.args[1], "flxSelectOptions"), extendConfig({}, controller.args[2], "flxSelectOptions"));
        flxSelectOptions.setDefaultUnit(kony.flex.DP);
        var flxArrowImage = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "clipBounds": true,
            "height": "12dp",
            "id": "flxArrowImage",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "left": "0dp",
            "isModalContainer": false,
            "skin": "slFbox",
            "top": "0dp",
            "width": "100%",
            "zIndex": 2
        }, controller.args[0], "flxArrowImage"), extendConfig({}, controller.args[1], "flxArrowImage"), extendConfig({}, controller.args[2], "flxArrowImage"));
        flxArrowImage.setDefaultUnit(kony.flex.DP);
        var imgUpArrow = new kony.ui.Image2(extendConfig({
            "height": "12dp",
            "id": "imgUpArrow",
            "isVisible": true,
            "right": "15dp",
            "skin": "slImage",
            "src": "uparrow_2x.png",
            "top": "0dp",
            "width": "15dp",
            "zIndex": 1
        }, controller.args[0], "imgUpArrow"), extendConfig({
            "imageScaleMode": constants.IMAGE_SCALE_MODE_FIT_TO_DIMENSIONS,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "imgUpArrow"), extendConfig({}, controller.args[2], "imgUpArrow"));
        flxArrowImage.add(imgUpArrow);
        var flxSelectOptionsInner = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
            "clipBounds": true,
            "id": "flxSelectOptionsInner",
            "isVisible": true,
            "layoutType": kony.flex.FLOW_VERTICAL,
            "left": "0dp",
            "isModalContainer": false,
            "skin": "sknflxffffffop100dbdbe6Radius3px",
            "top": "-1dp",
            "width": "100%"
        }, controller.args[0], "flxSelectOptionsInner"), extendConfig({}, controller.args[1], "flxSelectOptionsInner"), extendConfig({}, controller.args[2], "flxSelectOptionsInner"));
        flxSelectOptionsInner.setDefaultUnit(kony.flex.DP);
        var flxEditOption = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "clipBounds": true,
            "height": "35dp",
            "id": "flxEditOption",
            "isVisible": true,
            "layoutType": kony.flex.FLOW_HORIZONTAL,
            "left": "0dp",
            "isModalContainer": false,
            "skin": "CopyslFbox0e960df276fa744",
            "top": "0dp",
            "width": "100%",
            "zIndex": 1
        }, controller.args[0], "flxEditOption"), extendConfig({}, controller.args[1], "flxEditOption"), extendConfig({
            "hoverSkin": "sknContextualMenuEntryHover"
        }, controller.args[2], "flxEditOption"));
        flxEditOption.setDefaultUnit(kony.flex.DP);
        var fontIconOption2 = new kony.ui.Label(extendConfig({
            "centerY": "50%",
            "id": "fontIconOption2",
            "isVisible": true,
            "left": "15dp",
            "skin": "sknFontIconOptionMenuRow",
            "i18n_text": "kony.i18n.getLocalizedString(\"i18n.userwidgetmodel.lblIconOption1\")",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, controller.args[0], "fontIconOption2"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "fontIconOption2"), extendConfig({}, controller.args[2], "fontIconOption2"));
        var lblOption2 = new kony.ui.Label(extendConfig({
            "centerY": "50%",
            "id": "lblOption2",
            "isVisible": true,
            "left": "15dp",
            "skin": "sknlblLatoBold35475f14px",
            "i18n_text": "kony.i18n.getLocalizedString(\"i18n.permission.Edit\")",
            "top": "4dp",
            "width": "40px",
            "zIndex": 1
        }, controller.args[0], "lblOption2"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblOption2"), extendConfig({}, controller.args[2], "lblOption2"));
        flxEditOption.add(fontIconOption2, lblOption2);
        var flxDeactivateOption = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "clipBounds": true,
            "height": "35dp",
            "id": "flxDeactivateOption",
            "isVisible": true,
            "layoutType": kony.flex.FLOW_HORIZONTAL,
            "left": "0dp",
            "isModalContainer": false,
            "skin": "CopyslFbox0e8cab7435c9f44",
            "top": "0dp",
            "width": "100%",
            "zIndex": 1
        }, controller.args[0], "flxDeactivateOption"), extendConfig({}, controller.args[1], "flxDeactivateOption"), extendConfig({
            "hoverSkin": "sknContextualMenuEntryHover"
        }, controller.args[2], "flxDeactivateOption"));
        flxDeactivateOption.setDefaultUnit(kony.flex.DP);
        var fontIconOption4 = new kony.ui.Label(extendConfig({
            "centerY": "50%",
            "id": "fontIconOption4",
            "isVisible": true,
            "left": "15px",
            "skin": "sknFontIconOptionMenuRow",
            "i18n_text": "kony.i18n.getLocalizedString(\"i18n.userwidgetmodel.lblIconOption4\")",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, controller.args[0], "fontIconOption4"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "fontIconOption4"), extendConfig({}, controller.args[2], "fontIconOption4"));
        var lblOption4 = new kony.ui.Label(extendConfig({
            "centerY": "50%",
            "id": "lblOption4",
            "isVisible": true,
            "left": "15dp",
            "skin": "sknlblLatoBold35475f14px",
            "i18n_text": "kony.i18n.getLocalizedString(\"i18n.permission.Deactivate\")",
            "top": "4dp",
            "width": "70px",
            "zIndex": 1
        }, controller.args[0], "lblOption4"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblOption4"), extendConfig({}, controller.args[2], "lblOption4"));
        flxDeactivateOption.add(fontIconOption4, lblOption4);
        var flxDeleteOption = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "clipBounds": true,
            "height": "35dp",
            "id": "flxDeleteOption",
            "isVisible": true,
            "layoutType": kony.flex.FLOW_HORIZONTAL,
            "left": "0dp",
            "isModalContainer": false,
            "skin": "CopyslFbox0e55915c24f2a49",
            "top": "0dp",
            "width": "100%",
            "zIndex": 1
        }, controller.args[0], "flxDeleteOption"), extendConfig({}, controller.args[1], "flxDeleteOption"), extendConfig({
            "hoverSkin": "sknContextualMenuEntryHover"
        }, controller.args[2], "flxDeleteOption"));
        flxDeleteOption.setDefaultUnit(kony.flex.DP);
        var fontIconOption3 = new kony.ui.Label(extendConfig({
            "centerY": "50%",
            "id": "fontIconOption3",
            "isVisible": true,
            "left": "15px",
            "skin": "sknFontIconOptionMenuRow",
            "i18n_text": "kony.i18n.getLocalizedString(\"i18n.userwidgetmodel.lblIconOption2\")",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, controller.args[0], "fontIconOption3"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "fontIconOption3"), extendConfig({}, controller.args[2], "fontIconOption3"));
        var lblOption3 = new kony.ui.Label(extendConfig({
            "centerY": "50%",
            "id": "lblOption3",
            "isVisible": true,
            "left": "15dp",
            "skin": "sknlblLatoBold35475f14px",
            "i18n_text": "kony.i18n.getLocalizedString(\"i18n.SecurityQuestions.Delete\")",
            "top": "4dp",
            "width": "60px",
            "zIndex": 1
        }, controller.args[0], "lblOption3"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblOption3"), extendConfig({}, controller.args[2], "lblOption3"));
        flxDeleteOption.add(fontIconOption3, lblOption3);
        flxSelectOptionsInner.add(flxEditOption, flxDeactivateOption, flxDeleteOption);
        flxSelectOptions.add(flxArrowImage, flxSelectOptionsInner);
        var flxStaticContantData = new kony.ui.FlexScrollContainer(extendConfig({
            "allowHorizontalBounce": false,
            "allowVerticalBounce": true,
            "bottom": "20px",
            "bounces": true,
            "clipBounds": false,
            "enableScrolling": true,
            "horizontalScrollIndicator": true,
            "id": "flxStaticContantData",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "left": "20px",
            "pagingEnabled": false,
            "right": "0px",
            "scrollDirection": kony.flex.SCROLL_VERTICAL,
            "skin": "slFSbox",
            "top": "83px",
            "verticalScrollIndicator": true,
            "zIndex": 100
        }, controller.args[0], "flxStaticContantData"), extendConfig({}, controller.args[1], "flxStaticContantData"), extendConfig({}, controller.args[2], "flxStaticContantData"));
        flxStaticContantData.setDefaultUnit(kony.flex.DP);
        var rtxViewer = new kony.ui.Browser(extendConfig({
            "bottom": "0px",
            "detectTelNumber": true,
            "enableZoom": false,
            "id": "rtxViewer",
            "isVisible": true,
            "left": "0px",
            "setAsContent": false,
            "requestURLConfig": {
                "URL": "richtextViewer.html",
                "requestMethod": constants.BROWSER_REQUEST_METHOD_GET
            },
            "right": "35px",
            "top": "0px",
            "zIndex": 1
        }, controller.args[0], "rtxViewer"), extendConfig({}, controller.args[1], "rtxViewer"), extendConfig({}, controller.args[2], "rtxViewer"));
        flxStaticContantData.add(rtxViewer);
        staticData.add(flxHeader, flxSelectOptions, flxStaticContantData);
        return staticData;
    }
})