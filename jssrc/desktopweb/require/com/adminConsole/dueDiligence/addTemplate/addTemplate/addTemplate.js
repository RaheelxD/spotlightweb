define(function() {
    return function(controller) {
        var addTemplate = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
            "clipBounds": true,
            "isMaster": true,
            "id": "addTemplate",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "left": "0dp",
            "isModalContainer": false,
            "skin": "sknFlxFFFFFFKA",
            "top": "0dp",
            "width": "100%"
        }, controller.args[0], "addTemplate"), extendConfig({}, controller.args[1], "addTemplate"), extendConfig({}, controller.args[2], "addTemplate"));
        addTemplate.setDefaultUnit(kony.flex.DP);
        var flxDetails1 = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "clipBounds": true,
            "height": "308px",
            "id": "flxDetails1",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "left": "0%",
            "isModalContainer": false,
            "skin": "slFbox",
            "top": "0dp",
            "width": "100%",
            "zIndex": 1
        }, controller.args[0], "flxDetails1"), extendConfig({}, controller.args[1], "flxDetails1"), extendConfig({}, controller.args[2], "flxDetails1"));
        flxDetails1.setDefaultUnit(kony.flex.DP);
        var lblHeader = new kony.ui.Label(extendConfig({
            "id": "lblHeader",
            "isVisible": true,
            "left": "20dp",
            "skin": "sknLbl192B45LatoRegular14px",
            "text": "Citizenship Details",
            "textStyle": {},
            "top": "0dp",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, controller.args[0], "lblHeader"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblHeader"), extendConfig({}, controller.args[2], "lblHeader"));
        var flxAddDetails = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "clipBounds": true,
            "height": "258px",
            "id": "flxAddDetails",
            "isVisible": true,
            "layoutType": kony.flex.FLOW_VERTICAL,
            "left": "3%",
            "isModalContainer": false,
            "right": "20px",
            "skin": "sknFlxBordere1e5eedCustomRadius5px",
            "top": "30px",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, controller.args[0], "flxAddDetails"), extendConfig({}, controller.args[1], "flxAddDetails"), extendConfig({}, controller.args[2], "flxAddDetails"));
        flxAddDetails.setDefaultUnit(kony.flex.DP);
        var imgAddDetails = new kony.ui.Image2(extendConfig({
            "centerX": "50%",
            "centerY": "35%",
            "height": "72px",
            "id": "imgAddDetails",
            "isVisible": true,
            "left": "22dp",
            "skin": "slImage",
            "src": "customerrole.png",
            "width": "144px",
            "zIndex": 1
        }, controller.args[0], "imgAddDetails"), extendConfig({
            "imageScaleMode": constants.IMAGE_SCALE_MODE_MAINTAIN_ASPECT_RATIO,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "imgAddDetails"), extendConfig({}, controller.args[2], "imgAddDetails"));
        var lblAddDetails = new kony.ui.Label(extendConfig({
            "centerX": "50%",
            "id": "lblAddDetails",
            "isVisible": true,
            "left": "71dp",
            "skin": "sknLbl192B45LatoSemiBold13px",
            "text": "To add citizenship details click the ADD button",
            "top": "30dp",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, controller.args[0], "lblAddDetails"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblAddDetails"), extendConfig({}, controller.args[2], "lblAddDetails"));
        var btnAddDetails = new kony.ui.Button(extendConfig({
            "centerX": "50%",
            "focusSkin": "sknBtnLatoRegulara5abc413pxKA",
            "height": "30dp",
            "id": "btnAddDetails",
            "isVisible": true,
            "left": "5dp",
            "skin": "sknbtnf7f7faLatoRegular4f555dBorder1px8b96a5",
            "text": "ADD",
            "top": "20dp",
            "width": "90dp",
            "zIndex": 1
        }, controller.args[0], "btnAddDetails"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_CENTER,
            "displayText": true,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "btnAddDetails"), extendConfig({
            "hoverSkin": "sknbtnffffffLatoRegular4f555dBorder1px485c75"
        }, controller.args[2], "btnAddDetails"));
        flxAddDetails.add(imgAddDetails, lblAddDetails, btnAddDetails);
        flxDetails1.add(lblHeader, flxAddDetails);
        addTemplate.add(flxDetails1);
        return addTemplate;
    }
})