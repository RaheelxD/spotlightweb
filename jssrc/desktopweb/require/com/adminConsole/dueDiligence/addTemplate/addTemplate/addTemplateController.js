define("com/adminConsole/dueDiligence/addTemplate/addTemplate/useraddTemplateController", function() {
    return {};
});
define("com/adminConsole/dueDiligence/addTemplate/addTemplate/addTemplateControllerActions", {
    /*
          This is an auto generated file and any modifications to it may result in corruption of the action sequence.
        */
});
define("com/adminConsole/dueDiligence/addTemplate/addTemplate/addTemplateController", ["com/adminConsole/dueDiligence/addTemplate/addTemplate/useraddTemplateController", "com/adminConsole/dueDiligence/addTemplate/addTemplate/addTemplateControllerActions"], function() {
    var controller = require("com/adminConsole/dueDiligence/addTemplate/addTemplate/useraddTemplateController");
    var actions = require("com/adminConsole/dueDiligence/addTemplate/addTemplate/addTemplateControllerActions");
    for (var key in actions) {
        controller[key] = actions[key];
    }
    return controller;
});
