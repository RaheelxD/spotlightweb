define(function() {
    return function(controller) {
        var verticalTabs = new kony.ui.FlexContainer(extendConfig({
            "clipBounds": true,
            "isMaster": true,
            "height": "100%",
            "id": "verticalTabs",
            "isVisible": true,
            "layoutType": kony.flex.FLOW_VERTICAL,
            "left": "0dp",
            "isModalContainer": false,
            "skin": "sknflxf9f9f9NoBorder",
            "top": "0dp",
            "width": "100%"
        }, controller.args[0], "verticalTabs"), extendConfig({}, controller.args[1], "verticalTabs"), extendConfig({}, controller.args[2], "verticalTabs"));
        verticalTabs.setDefaultUnit(kony.flex.DP);
        var flxOption1 = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
            "clipBounds": true,
            "id": "flxOption1",
            "isVisible": true,
            "layoutType": kony.flex.FLOW_VERTICAL,
            "left": "0dp",
            "isModalContainer": false,
            "skin": "slFbox",
            "top": "0dp",
            "width": "100%",
            "zIndex": 1
        }, controller.args[0], "flxOption1"), extendConfig({}, controller.args[1], "flxOption1"), extendConfig({}, controller.args[2], "flxOption1"));
        flxOption1.setDefaultUnit(kony.flex.DP);
        var flxContentContainer1 = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
            "clipBounds": true,
            "id": "flxContentContainer1",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "left": "0dp",
            "isModalContainer": false,
            "skin": "slFbox",
            "top": "20px",
            "width": "100%"
        }, controller.args[0], "flxContentContainer1"), extendConfig({}, controller.args[1], "flxContentContainer1"), extendConfig({}, controller.args[2], "flxContentContainer1"));
        flxContentContainer1.setDefaultUnit(kony.flex.DP);
        var lblCompleted1 = new kony.ui.Label(extendConfig({
            "id": "lblCompleted1",
            "isVisible": false,
            "left": "15dp",
            "skin": "sknIcon18pxGray",
            "text": "",
            "top": "0dp",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, controller.args[0], "lblCompleted1"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblCompleted1"), extendConfig({}, controller.args[2], "lblCompleted1"));
        var btnOption1 = new kony.ui.Button(extendConfig({
            "bottom": "0dp",
            "id": "btnOption1",
            "isVisible": true,
            "left": "20dp",
            "skin": "sknBtnLatoBold485c7512PxNoBorder",
            "text": "CITIZENSHIP &TAX",
            "top": "0dp",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, controller.args[0], "btnOption1"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_TOP_LEFT,
            "displayText": true,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "btnOption1"), extendConfig({}, controller.args[2], "btnOption1"));
        var flxImgArrow1 = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "bottom": "0dp",
            "clipBounds": true,
            "height": "15px",
            "id": "flxImgArrow1",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "isModalContainer": false,
            "right": "20px",
            "skin": "slFbox",
            "top": "0dp",
            "width": "15px",
            "zIndex": 1
        }, controller.args[0], "flxImgArrow1"), extendConfig({}, controller.args[1], "flxImgArrow1"), extendConfig({}, controller.args[2], "flxImgArrow1"));
        flxImgArrow1.setDefaultUnit(kony.flex.DP);
        var imgSelected1 = new kony.ui.Image2(extendConfig({
            "centerY": "50%",
            "height": "100%",
            "id": "imgSelected1",
            "isVisible": false,
            "right": 0,
            "skin": "slImage",
            "src": "right_arrow2x.png",
            "width": "100%",
            "zIndex": 1
        }, controller.args[0], "imgSelected1"), extendConfig({
            "imageScaleMode": constants.IMAGE_SCALE_MODE_MAINTAIN_ASPECT_RATIO,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "imgSelected1"), extendConfig({}, controller.args[2], "imgSelected1"));
        var lblSelected1 = new kony.ui.Label(extendConfig({
            "centerX": "50%",
            "centerY": "50%",
            "height": "100%",
            "id": "lblSelected1",
            "isVisible": true,
            "skin": "sknicon15pxBlack",
            "i18n_text": "kony.i18n.getLocalizedString(\"i18n.userwidgetmodel.fontIconSelected1\")",
            "width": "100%",
            "zIndex": 1
        }, controller.args[0], "lblSelected1"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblSelected1"), extendConfig({}, controller.args[2], "lblSelected1"));
        flxImgArrow1.add(imgSelected1, lblSelected1);
        flxContentContainer1.add(lblCompleted1, btnOption1, flxImgArrow1);
        var flxSeparatorContainer1 = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "clipBounds": true,
            "height": "1px",
            "id": "flxSeparatorContainer1",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "left": "0dp",
            "isModalContainer": false,
            "skin": "slFbox",
            "top": "15px",
            "width": "100%"
        }, controller.args[0], "flxSeparatorContainer1"), extendConfig({}, controller.args[1], "flxSeparatorContainer1"), extendConfig({}, controller.args[2], "flxSeparatorContainer1"));
        flxSeparatorContainer1.setDefaultUnit(kony.flex.DP);
        var flxAddSeparator1 = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "bottom": 0,
            "clipBounds": true,
            "height": "1px",
            "id": "flxAddSeparator1",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "left": "20px",
            "isModalContainer": false,
            "right": "20px",
            "skin": "sknflxe1e5edop100",
            "top": 0,
            "zIndex": 1
        }, controller.args[0], "flxAddSeparator1"), extendConfig({}, controller.args[1], "flxAddSeparator1"), extendConfig({}, controller.args[2], "flxAddSeparator1"));
        flxAddSeparator1.setDefaultUnit(kony.flex.DP);
        flxAddSeparator1.add();
        flxSeparatorContainer1.add(flxAddSeparator1);
        flxOption1.add(flxContentContainer1, flxSeparatorContainer1);
        var flxOption4 = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
            "clipBounds": true,
            "id": "flxOption4",
            "isVisible": true,
            "layoutType": kony.flex.FLOW_VERTICAL,
            "left": "0dp",
            "isModalContainer": false,
            "skin": "slFbox",
            "top": "0dp",
            "width": "100%",
            "zIndex": 1
        }, controller.args[0], "flxOption4"), extendConfig({}, controller.args[1], "flxOption4"), extendConfig({}, controller.args[2], "flxOption4"));
        flxOption4.setDefaultUnit(kony.flex.DP);
        var flxContentContainer4 = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
            "clipBounds": true,
            "id": "flxContentContainer4",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "left": "0dp",
            "isModalContainer": false,
            "skin": "slFbox",
            "top": "15dp",
            "width": "100%"
        }, controller.args[0], "flxContentContainer4"), extendConfig({}, controller.args[1], "flxContentContainer4"), extendConfig({}, controller.args[2], "flxContentContainer4"));
        flxContentContainer4.setDefaultUnit(kony.flex.DP);
        var lblCompleted4 = new kony.ui.Label(extendConfig({
            "id": "lblCompleted4",
            "isVisible": false,
            "left": "15dp",
            "skin": "sknIcon18pxGray",
            "text": "",
            "top": "0dp",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, controller.args[0], "lblCompleted4"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblCompleted4"), extendConfig({}, controller.args[2], "lblCompleted4"));
        var btnOption4 = new kony.ui.Button(extendConfig({
            "bottom": "0dp",
            "id": "btnOption4",
            "isVisible": true,
            "left": "20dp",
            "skin": "sknBtn737678LatoReg12pxNoBgBorder",
            "text": "EMPLOYMENT",
            "top": "0dp",
            "width": "150dp",
            "zIndex": 1
        }, controller.args[0], "btnOption4"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_TOP_LEFT,
            "displayText": true,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "btnOption4"), extendConfig({}, controller.args[2], "btnOption4"));
        var flxImgArrow4 = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "bottom": "0dp",
            "clipBounds": true,
            "height": "15px",
            "id": "flxImgArrow4",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "isModalContainer": false,
            "right": "20px",
            "skin": "slFbox",
            "top": "0dp",
            "width": "15px",
            "zIndex": 1
        }, controller.args[0], "flxImgArrow4"), extendConfig({}, controller.args[1], "flxImgArrow4"), extendConfig({}, controller.args[2], "flxImgArrow4"));
        flxImgArrow4.setDefaultUnit(kony.flex.DP);
        var imgSelected4 = new kony.ui.Image2(extendConfig({
            "height": "100%",
            "id": "imgSelected4",
            "isVisible": false,
            "right": 0,
            "skin": "slImage",
            "src": "right_arrow2x.png",
            "top": "0dp",
            "width": "100%",
            "zIndex": 1
        }, controller.args[0], "imgSelected4"), extendConfig({
            "imageScaleMode": constants.IMAGE_SCALE_MODE_MAINTAIN_ASPECT_RATIO,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "imgSelected4"), extendConfig({}, controller.args[2], "imgSelected4"));
        var lblSelected4 = new kony.ui.Label(extendConfig({
            "centerX": "50%",
            "centerY": "50%",
            "height": "100%",
            "id": "lblSelected4",
            "isVisible": false,
            "left": "5dp",
            "skin": "sknicon15pxBlack",
            "i18n_text": "kony.i18n.getLocalizedString(\"i18n.userwidgetmodel.fontIconSelected1\")",
            "top": "6dp",
            "width": "100%",
            "zIndex": 1
        }, controller.args[0], "lblSelected4"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblSelected4"), extendConfig({}, controller.args[2], "lblSelected4"));
        flxImgArrow4.add(imgSelected4, lblSelected4);
        flxContentContainer4.add(lblCompleted4, btnOption4, flxImgArrow4);
        var lblOptional4 = new kony.ui.Label(extendConfig({
            "id": "lblOptional4",
            "isVisible": false,
            "left": "20px",
            "skin": "slLabel0d4f692dab05249",
            "i18n_text": "kony.i18n.getLocalizedString(\"i18n.permission.Optional\")",
            "top": "10px",
            "width": "93px",
            "zIndex": 1
        }, controller.args[0], "lblOptional4"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblOptional4"), extendConfig({}, controller.args[2], "lblOptional4"));
        var flxSeparatorContainer4 = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "clipBounds": true,
            "height": "1dp",
            "id": "flxSeparatorContainer4",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "left": "0dp",
            "isModalContainer": false,
            "skin": "slFbox",
            "top": "15dp",
            "width": "100%"
        }, controller.args[0], "flxSeparatorContainer4"), extendConfig({}, controller.args[1], "flxSeparatorContainer4"), extendConfig({}, controller.args[2], "flxSeparatorContainer4"));
        flxSeparatorContainer4.setDefaultUnit(kony.flex.DP);
        var flxAddSeparator4 = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "bottom": 0,
            "clipBounds": true,
            "height": "1px",
            "id": "flxAddSeparator4",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "left": "20px",
            "isModalContainer": false,
            "right": "20px",
            "skin": "sknflxe1e5edop100",
            "zIndex": 1
        }, controller.args[0], "flxAddSeparator4"), extendConfig({}, controller.args[1], "flxAddSeparator4"), extendConfig({}, controller.args[2], "flxAddSeparator4"));
        flxAddSeparator4.setDefaultUnit(kony.flex.DP);
        flxAddSeparator4.add();
        flxSeparatorContainer4.add(flxAddSeparator4);
        flxOption4.add(flxContentContainer4, lblOptional4, flxSeparatorContainer4);
        var flxOption3 = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
            "clipBounds": true,
            "id": "flxOption3",
            "isVisible": true,
            "layoutType": kony.flex.FLOW_VERTICAL,
            "left": "0dp",
            "isModalContainer": false,
            "skin": "slFbox",
            "top": "0dp",
            "width": "100%",
            "zIndex": 1
        }, controller.args[0], "flxOption3"), extendConfig({}, controller.args[1], "flxOption3"), extendConfig({}, controller.args[2], "flxOption3"));
        flxOption3.setDefaultUnit(kony.flex.DP);
        var flxContentContainer3 = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
            "clipBounds": true,
            "id": "flxContentContainer3",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "left": "0dp",
            "isModalContainer": false,
            "skin": "slFbox",
            "top": "15dp",
            "width": "100%"
        }, controller.args[0], "flxContentContainer3"), extendConfig({}, controller.args[1], "flxContentContainer3"), extendConfig({}, controller.args[2], "flxContentContainer3"));
        flxContentContainer3.setDefaultUnit(kony.flex.DP);
        var lblCompleted3 = new kony.ui.Label(extendConfig({
            "id": "lblCompleted3",
            "isVisible": false,
            "left": "15dp",
            "skin": "sknIcon18pxGray",
            "text": "",
            "top": "0dp",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, controller.args[0], "lblCompleted3"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblCompleted3"), extendConfig({}, controller.args[2], "lblCompleted3"));
        var btnOption3 = new kony.ui.Button(extendConfig({
            "bottom": "0dp",
            "id": "btnOption3",
            "isVisible": true,
            "left": "20dp",
            "skin": "sknBtn737678LatoReg12pxNoBgBorder",
            "text": "ACCOUNT USAGE",
            "top": "0dp",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, controller.args[0], "btnOption3"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_TOP_LEFT,
            "displayText": true,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "btnOption3"), extendConfig({}, controller.args[2], "btnOption3"));
        var flxImgArrow3 = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "bottom": "0dp",
            "clipBounds": true,
            "height": "15px",
            "id": "flxImgArrow3",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "isModalContainer": false,
            "right": "20px",
            "skin": "slFbox",
            "top": "0dp",
            "width": "15px",
            "zIndex": 1
        }, controller.args[0], "flxImgArrow3"), extendConfig({}, controller.args[1], "flxImgArrow3"), extendConfig({}, controller.args[2], "flxImgArrow3"));
        flxImgArrow3.setDefaultUnit(kony.flex.DP);
        var imgSelected3 = new kony.ui.Image2(extendConfig({
            "height": "100%",
            "id": "imgSelected3",
            "isVisible": false,
            "right": 0,
            "skin": "slImage",
            "src": "right_arrow2x.png",
            "top": "0dp",
            "width": "100%",
            "zIndex": 1
        }, controller.args[0], "imgSelected3"), extendConfig({
            "imageScaleMode": constants.IMAGE_SCALE_MODE_MAINTAIN_ASPECT_RATIO,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "imgSelected3"), extendConfig({}, controller.args[2], "imgSelected3"));
        var lblSelected3 = new kony.ui.Label(extendConfig({
            "centerX": "50%",
            "centerY": "50%",
            "height": "100%",
            "id": "lblSelected3",
            "isVisible": false,
            "left": "5dp",
            "skin": "sknicon15pxBlack",
            "i18n_text": "kony.i18n.getLocalizedString(\"i18n.userwidgetmodel.fontIconSelected1\")",
            "top": "6dp",
            "width": "100%",
            "zIndex": 1
        }, controller.args[0], "lblSelected3"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblSelected3"), extendConfig({}, controller.args[2], "lblSelected3"));
        flxImgArrow3.add(imgSelected3, lblSelected3);
        flxContentContainer3.add(lblCompleted3, btnOption3, flxImgArrow3);
        var lblOptional3 = new kony.ui.Label(extendConfig({
            "id": "lblOptional3",
            "isVisible": false,
            "left": "43px",
            "skin": "slLabel0d4f692dab05249",
            "i18n_text": "kony.i18n.getLocalizedString(\"i18n.permission.Optional\")",
            "top": "10px",
            "width": "93px",
            "zIndex": 1
        }, controller.args[0], "lblOptional3"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblOptional3"), extendConfig({}, controller.args[2], "lblOptional3"));
        var flxSubheader3 = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
            "clipBounds": true,
            "id": "flxSubheader3",
            "isVisible": true,
            "layoutType": kony.flex.FLOW_VERTICAL,
            "left": "0dp",
            "isModalContainer": false,
            "skin": "slFbox",
            "top": "0dp",
            "width": "100%"
        }, controller.args[0], "flxSubheader3"), extendConfig({}, controller.args[1], "flxSubheader3"), extendConfig({}, controller.args[2], "flxSubheader3"));
        flxSubheader3.setDefaultUnit(kony.flex.DP);
        var flxSubOption31 = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
            "clipBounds": true,
            "id": "flxSubOption31",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "left": "0dp",
            "isModalContainer": false,
            "skin": "slFbox",
            "top": "15px",
            "width": "100%"
        }, controller.args[0], "flxSubOption31"), extendConfig({}, controller.args[1], "flxSubOption31"), extendConfig({}, controller.args[2], "flxSubOption31"));
        flxSubOption31.setDefaultUnit(kony.flex.DP);
        var btnSubOption31 = new kony.ui.Button(extendConfig({
            "bottom": "0dp",
            "id": "btnSubOption31",
            "isVisible": true,
            "left": "35dp",
            "skin": "sknBtn737678LatoReg12pxNoBgBorder",
            "text": "RECEIVE PAYMENTS",
            "top": "0dp",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, controller.args[0], "btnSubOption31"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_TOP_LEFT,
            "displayText": true,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "btnSubOption31"), extendConfig({}, controller.args[2], "btnSubOption31"));
        var flxImgSubArrow31 = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "bottom": "0dp",
            "clipBounds": true,
            "height": "15px",
            "id": "flxImgSubArrow31",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "isModalContainer": false,
            "right": "20px",
            "skin": "slFbox",
            "top": "0px",
            "width": "15px",
            "zIndex": 1
        }, controller.args[0], "flxImgSubArrow31"), extendConfig({}, controller.args[1], "flxImgSubArrow31"), extendConfig({}, controller.args[2], "flxImgSubArrow31"));
        flxImgSubArrow31.setDefaultUnit(kony.flex.DP);
        var lblSelectedSub31 = new kony.ui.Label(extendConfig({
            "centerX": "50%",
            "centerY": "50%",
            "height": "100%",
            "id": "lblSelectedSub31",
            "isVisible": false,
            "skin": "sknicon15pxBlack",
            "i18n_text": "kony.i18n.getLocalizedString(\"i18n.userwidgetmodel.fontIconSelected1\")",
            "width": "100%",
            "zIndex": 1
        }, controller.args[0], "lblSelectedSub31"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblSelectedSub31"), extendConfig({}, controller.args[2], "lblSelectedSub31"));
        flxImgSubArrow31.add(lblSelectedSub31);
        flxSubOption31.add(btnSubOption31, flxImgSubArrow31);
        var flxSubOption32 = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
            "clipBounds": true,
            "id": "flxSubOption32",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "left": "0dp",
            "isModalContainer": false,
            "skin": "slFbox",
            "top": "15px",
            "width": "100%"
        }, controller.args[0], "flxSubOption32"), extendConfig({}, controller.args[1], "flxSubOption32"), extendConfig({}, controller.args[2], "flxSubOption32"));
        flxSubOption32.setDefaultUnit(kony.flex.DP);
        var btnSubOption32 = new kony.ui.Button(extendConfig({
            "bottom": "0dp",
            "id": "btnSubOption32",
            "isVisible": true,
            "left": "35dp",
            "skin": "sknBtn737678LatoReg12pxNoBgBorder",
            "text": "SEND PAYMENTS",
            "top": "0dp",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 2
        }, controller.args[0], "btnSubOption32"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_TOP_LEFT,
            "displayText": true,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "btnSubOption32"), extendConfig({}, controller.args[2], "btnSubOption32"));
        var flxImgSubArrow32 = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "bottom": "0dp",
            "clipBounds": true,
            "height": "15px",
            "id": "flxImgSubArrow32",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "isModalContainer": false,
            "right": "20px",
            "skin": "slFbox",
            "top": "0px",
            "width": "15px",
            "zIndex": 1
        }, controller.args[0], "flxImgSubArrow32"), extendConfig({}, controller.args[1], "flxImgSubArrow32"), extendConfig({}, controller.args[2], "flxImgSubArrow32"));
        flxImgSubArrow32.setDefaultUnit(kony.flex.DP);
        var lblSelectedSub32 = new kony.ui.Label(extendConfig({
            "centerX": "50%",
            "centerY": "50%",
            "height": "100%",
            "id": "lblSelectedSub32",
            "isVisible": false,
            "skin": "sknicon15pxBlack",
            "i18n_text": "kony.i18n.getLocalizedString(\"i18n.userwidgetmodel.fontIconSelected1\")",
            "width": "100%",
            "zIndex": 1
        }, controller.args[0], "lblSelectedSub32"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblSelectedSub32"), extendConfig({}, controller.args[2], "lblSelectedSub32"));
        flxImgSubArrow32.add(lblSelectedSub32);
        flxSubOption32.add(btnSubOption32, flxImgSubArrow32);
        flxSubheader3.add(flxSubOption31, flxSubOption32);
        var flxSeparatorContainer3 = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "clipBounds": true,
            "height": "1dp",
            "id": "flxSeparatorContainer3",
            "isVisible": false,
            "layoutType": kony.flex.FREE_FORM,
            "left": "0dp",
            "isModalContainer": false,
            "skin": "slFbox",
            "top": "15dp",
            "width": "100%"
        }, controller.args[0], "flxSeparatorContainer3"), extendConfig({}, controller.args[1], "flxSeparatorContainer3"), extendConfig({}, controller.args[2], "flxSeparatorContainer3"));
        flxSeparatorContainer3.setDefaultUnit(kony.flex.DP);
        var flxAddSeparator3 = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "bottom": 0,
            "clipBounds": true,
            "height": "1px",
            "id": "flxAddSeparator3",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "left": "43px",
            "isModalContainer": false,
            "right": "20px",
            "skin": "sknflxe1e5edop100",
            "zIndex": 1
        }, controller.args[0], "flxAddSeparator3"), extendConfig({}, controller.args[1], "flxAddSeparator3"), extendConfig({}, controller.args[2], "flxAddSeparator3"));
        flxAddSeparator3.setDefaultUnit(kony.flex.DP);
        flxAddSeparator3.add();
        flxSeparatorContainer3.add(flxAddSeparator3);
        flxOption3.add(flxContentContainer3, lblOptional3, flxSubheader3, flxSeparatorContainer3);
        var flxOption2 = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
            "clipBounds": true,
            "id": "flxOption2",
            "isVisible": false,
            "layoutType": kony.flex.FLOW_VERTICAL,
            "left": "0dp",
            "isModalContainer": false,
            "skin": "slFbox",
            "top": "0dp",
            "width": "100%",
            "zIndex": 1
        }, controller.args[0], "flxOption2"), extendConfig({}, controller.args[1], "flxOption2"), extendConfig({}, controller.args[2], "flxOption2"));
        flxOption2.setDefaultUnit(kony.flex.DP);
        var flxContentContainer2 = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
            "clipBounds": true,
            "id": "flxContentContainer2",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "left": "0dp",
            "isModalContainer": false,
            "skin": "slFbox",
            "top": "15px",
            "width": "100%"
        }, controller.args[0], "flxContentContainer2"), extendConfig({}, controller.args[1], "flxContentContainer2"), extendConfig({}, controller.args[2], "flxContentContainer2"));
        flxContentContainer2.setDefaultUnit(kony.flex.DP);
        var lblCompleted2 = new kony.ui.Label(extendConfig({
            "id": "lblCompleted2",
            "isVisible": true,
            "left": "15dp",
            "skin": "sknIcon18pxGray",
            "text": "",
            "top": "0dp",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, controller.args[0], "lblCompleted2"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblCompleted2"), extendConfig({}, controller.args[2], "lblCompleted2"));
        var btnOption2 = new kony.ui.Button(extendConfig({
            "bottom": "0dp",
            "id": "btnOption2",
            "isVisible": true,
            "left": "43dp",
            "skin": "Btn737678font12pxKA",
            "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmAdManagement.configureInAppChannelsCaps\")",
            "top": "0dp",
            "width": "150dp",
            "zIndex": 1
        }, controller.args[0], "btnOption2"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_TOP_LEFT,
            "displayText": true,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "btnOption2"), extendConfig({}, controller.args[2], "btnOption2"));
        var flxImgArrow2 = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "bottom": "0dp",
            "clipBounds": true,
            "height": "15px",
            "id": "flxImgArrow2",
            "isVisible": false,
            "layoutType": kony.flex.FREE_FORM,
            "isModalContainer": false,
            "right": "20px",
            "skin": "slFbox",
            "top": "0px",
            "width": "15px",
            "zIndex": 1
        }, controller.args[0], "flxImgArrow2"), extendConfig({}, controller.args[1], "flxImgArrow2"), extendConfig({}, controller.args[2], "flxImgArrow2"));
        flxImgArrow2.setDefaultUnit(kony.flex.DP);
        var imgSelected2 = new kony.ui.Image2(extendConfig({
            "centerY": "50%",
            "height": "100%",
            "id": "imgSelected2",
            "isVisible": false,
            "right": 0,
            "skin": "slImage",
            "src": "right_arrow2x.png",
            "width": "100%",
            "zIndex": 1
        }, controller.args[0], "imgSelected2"), extendConfig({
            "imageScaleMode": constants.IMAGE_SCALE_MODE_MAINTAIN_ASPECT_RATIO,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "imgSelected2"), extendConfig({}, controller.args[2], "imgSelected2"));
        var lblSelected2 = new kony.ui.Label(extendConfig({
            "centerX": "50%",
            "centerY": "50%",
            "height": "100%",
            "id": "lblSelected2",
            "isVisible": true,
            "skin": "sknicon15pxBlack",
            "i18n_text": "kony.i18n.getLocalizedString(\"i18n.userwidgetmodel.fontIconSelected1\")",
            "width": "100%",
            "zIndex": 1
        }, controller.args[0], "lblSelected2"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblSelected2"), extendConfig({}, controller.args[2], "lblSelected2"));
        flxImgArrow2.add(imgSelected2, lblSelected2);
        flxContentContainer2.add(lblCompleted2, btnOption2, flxImgArrow2);
        var lblOptional2 = new kony.ui.Label(extendConfig({
            "id": "lblOptional2",
            "isVisible": false,
            "left": "43px",
            "skin": "slLabel0d4f692dab05249",
            "i18n_text": "kony.i18n.getLocalizedString(\"i18n.permission.Optional\")",
            "top": "10px",
            "width": "93px",
            "zIndex": 1
        }, controller.args[0], "lblOptional2"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblOptional2"), extendConfig({}, controller.args[2], "lblOptional2"));
        var flxSubheader = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
            "clipBounds": true,
            "id": "flxSubheader",
            "isVisible": true,
            "layoutType": kony.flex.FLOW_VERTICAL,
            "left": "0dp",
            "isModalContainer": false,
            "skin": "slFbox",
            "top": "0dp",
            "width": "100%"
        }, controller.args[0], "flxSubheader"), extendConfig({}, controller.args[1], "flxSubheader"), extendConfig({}, controller.args[2], "flxSubheader"));
        flxSubheader.setDefaultUnit(kony.flex.DP);
        var flxSubOption1 = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
            "clipBounds": true,
            "id": "flxSubOption1",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "left": "0dp",
            "isModalContainer": false,
            "skin": "slFbox",
            "top": "15px",
            "width": "100%"
        }, controller.args[0], "flxSubOption1"), extendConfig({}, controller.args[1], "flxSubOption1"), extendConfig({}, controller.args[2], "flxSubOption1"));
        flxSubOption1.setDefaultUnit(kony.flex.DP);
        var btnSubOption1 = new kony.ui.Button(extendConfig({
            "bottom": "0dp",
            "id": "btnSubOption1",
            "isVisible": true,
            "left": "58dp",
            "skin": "Btn737678font12pxKA",
            "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmAdManagement.PreLoginScreen\")",
            "top": "0dp",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, controller.args[0], "btnSubOption1"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_TOP_LEFT,
            "displayText": true,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "btnSubOption1"), extendConfig({}, controller.args[2], "btnSubOption1"));
        var flxImgSubArrow = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "bottom": "0dp",
            "clipBounds": true,
            "height": "15px",
            "id": "flxImgSubArrow",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "isModalContainer": false,
            "right": "20px",
            "skin": "slFbox",
            "top": "0px",
            "width": "15px",
            "zIndex": 1
        }, controller.args[0], "flxImgSubArrow"), extendConfig({}, controller.args[1], "flxImgSubArrow"), extendConfig({}, controller.args[2], "flxImgSubArrow"));
        flxImgSubArrow.setDefaultUnit(kony.flex.DP);
        var lblSelectedSub1 = new kony.ui.Label(extendConfig({
            "centerX": "50%",
            "centerY": "50%",
            "height": "100%",
            "id": "lblSelectedSub1",
            "isVisible": false,
            "skin": "sknicon15pxBlack",
            "i18n_text": "kony.i18n.getLocalizedString(\"i18n.userwidgetmodel.fontIconSelected1\")",
            "width": "100%",
            "zIndex": 1
        }, controller.args[0], "lblSelectedSub1"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblSelectedSub1"), extendConfig({}, controller.args[2], "lblSelectedSub1"));
        flxImgSubArrow.add(lblSelectedSub1);
        flxSubOption1.add(btnSubOption1, flxImgSubArrow);
        var flxSubOption2 = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
            "clipBounds": true,
            "id": "flxSubOption2",
            "isVisible": false,
            "layoutType": kony.flex.FREE_FORM,
            "left": "0dp",
            "isModalContainer": false,
            "skin": "slFbox",
            "top": "15px",
            "width": "100%"
        }, controller.args[0], "flxSubOption2"), extendConfig({}, controller.args[1], "flxSubOption2"), extendConfig({}, controller.args[2], "flxSubOption2"));
        flxSubOption2.setDefaultUnit(kony.flex.DP);
        var btnSubOption2 = new kony.ui.Button(extendConfig({
            "bottom": "0dp",
            "id": "btnSubOption2",
            "isVisible": true,
            "left": "58dp",
            "skin": "Btn737678font12pxKA",
            "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmAdManagement.PostLoginScreen\")",
            "top": "0dp",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, controller.args[0], "btnSubOption2"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_TOP_LEFT,
            "displayText": true,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "btnSubOption2"), extendConfig({}, controller.args[2], "btnSubOption2"));
        var flxImgSubArrow2 = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "bottom": "0dp",
            "clipBounds": true,
            "height": "15px",
            "id": "flxImgSubArrow2",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "isModalContainer": false,
            "right": "20px",
            "skin": "slFbox",
            "top": "0px",
            "width": "15px",
            "zIndex": 1
        }, controller.args[0], "flxImgSubArrow2"), extendConfig({}, controller.args[1], "flxImgSubArrow2"), extendConfig({}, controller.args[2], "flxImgSubArrow2"));
        flxImgSubArrow2.setDefaultUnit(kony.flex.DP);
        var lblSelectedSub2 = new kony.ui.Label(extendConfig({
            "centerX": "50%",
            "centerY": "50%",
            "height": "100%",
            "id": "lblSelectedSub2",
            "isVisible": false,
            "skin": "sknicon15pxBlack",
            "i18n_text": "kony.i18n.getLocalizedString(\"i18n.userwidgetmodel.fontIconSelected1\")",
            "width": "100%",
            "zIndex": 1
        }, controller.args[0], "lblSelectedSub2"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblSelectedSub2"), extendConfig({}, controller.args[2], "lblSelectedSub2"));
        flxImgSubArrow2.add(lblSelectedSub2);
        flxSubOption2.add(btnSubOption2, flxImgSubArrow2);
        var flxSubOption3 = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
            "clipBounds": true,
            "id": "flxSubOption3",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "left": "0dp",
            "isModalContainer": false,
            "skin": "slFbox",
            "top": "15px",
            "width": "100%"
        }, controller.args[0], "flxSubOption3"), extendConfig({}, controller.args[1], "flxSubOption3"), extendConfig({}, controller.args[2], "flxSubOption3"));
        flxSubOption3.setDefaultUnit(kony.flex.DP);
        var btnSubOption3 = new kony.ui.Button(extendConfig({
            "bottom": "0dp",
            "id": "btnSubOption3",
            "isVisible": true,
            "left": "58dp",
            "skin": "Btn737678font12pxKA",
            "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmAdManagement.Dashboard\")",
            "top": "0dp",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, controller.args[0], "btnSubOption3"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_TOP_LEFT,
            "displayText": true,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "btnSubOption3"), extendConfig({}, controller.args[2], "btnSubOption3"));
        var flxImgSubArrow3 = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "bottom": "0dp",
            "clipBounds": true,
            "height": "15px",
            "id": "flxImgSubArrow3",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "isModalContainer": false,
            "right": "20px",
            "skin": "slFbox",
            "top": "0px",
            "width": "15px",
            "zIndex": 1
        }, controller.args[0], "flxImgSubArrow3"), extendConfig({}, controller.args[1], "flxImgSubArrow3"), extendConfig({}, controller.args[2], "flxImgSubArrow3"));
        flxImgSubArrow3.setDefaultUnit(kony.flex.DP);
        var lblSelectedSub3 = new kony.ui.Label(extendConfig({
            "centerX": "50%",
            "centerY": "50%",
            "height": "100%",
            "id": "lblSelectedSub3",
            "isVisible": false,
            "skin": "sknicon15pxBlack",
            "i18n_text": "kony.i18n.getLocalizedString(\"i18n.userwidgetmodel.fontIconSelected1\")",
            "width": "100%",
            "zIndex": 1
        }, controller.args[0], "lblSelectedSub3"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblSelectedSub3"), extendConfig({}, controller.args[2], "lblSelectedSub3"));
        flxImgSubArrow3.add(lblSelectedSub3);
        flxSubOption3.add(btnSubOption3, flxImgSubArrow3);
        var flxSubOption4 = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
            "clipBounds": true,
            "id": "flxSubOption4",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "left": "0dp",
            "isModalContainer": false,
            "skin": "slFbox",
            "top": "15px",
            "width": "100%"
        }, controller.args[0], "flxSubOption4"), extendConfig({}, controller.args[1], "flxSubOption4"), extendConfig({}, controller.args[2], "flxSubOption4"));
        flxSubOption4.setDefaultUnit(kony.flex.DP);
        var btnSubOption4 = new kony.ui.Button(extendConfig({
            "bottom": "0dp",
            "id": "btnSubOption4",
            "isVisible": true,
            "left": "58dp",
            "skin": "Btn737678font12pxKA",
            "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmAdManagement.ApplyforNewAcc\")",
            "top": "0dp",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, controller.args[0], "btnSubOption4"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_TOP_LEFT,
            "displayText": true,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "btnSubOption4"), extendConfig({}, controller.args[2], "btnSubOption4"));
        var flxImgSubArrow4 = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "bottom": "0dp",
            "clipBounds": true,
            "height": "15px",
            "id": "flxImgSubArrow4",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "isModalContainer": false,
            "right": "20px",
            "skin": "slFbox",
            "top": "0px",
            "width": "15px",
            "zIndex": 1
        }, controller.args[0], "flxImgSubArrow4"), extendConfig({}, controller.args[1], "flxImgSubArrow4"), extendConfig({}, controller.args[2], "flxImgSubArrow4"));
        flxImgSubArrow4.setDefaultUnit(kony.flex.DP);
        var lblSelectedSub4 = new kony.ui.Label(extendConfig({
            "centerX": "50%",
            "centerY": "50%",
            "height": "100%",
            "id": "lblSelectedSub4",
            "isVisible": false,
            "skin": "sknicon15pxBlack",
            "i18n_text": "kony.i18n.getLocalizedString(\"i18n.userwidgetmodel.fontIconSelected1\")",
            "width": "100%",
            "zIndex": 1
        }, controller.args[0], "lblSelectedSub4"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblSelectedSub4"), extendConfig({}, controller.args[2], "lblSelectedSub4"));
        flxImgSubArrow4.add(lblSelectedSub4);
        flxSubOption4.add(btnSubOption4, flxImgSubArrow4);
        var flxSeperator = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "clipBounds": true,
            "height": "1px",
            "id": "flxSeperator",
            "isVisible": false,
            "layoutType": kony.flex.FREE_FORM,
            "left": "0dp",
            "isModalContainer": false,
            "skin": "slFbox",
            "top": "15px",
            "width": "100%"
        }, controller.args[0], "flxSeperator"), extendConfig({}, controller.args[1], "flxSeperator"), extendConfig({}, controller.args[2], "flxSeperator"));
        flxSeperator.setDefaultUnit(kony.flex.DP);
        var flxSeperatorSubheader = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "bottom": "0dp",
            "clipBounds": true,
            "height": "1px",
            "id": "flxSeperatorSubheader",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "left": "20px",
            "isModalContainer": false,
            "right": "20px",
            "skin": "sknflxe1e5edop100",
            "top": "0dp",
            "zIndex": 1
        }, controller.args[0], "flxSeperatorSubheader"), extendConfig({}, controller.args[1], "flxSeperatorSubheader"), extendConfig({}, controller.args[2], "flxSeperatorSubheader"));
        flxSeperatorSubheader.setDefaultUnit(kony.flex.DP);
        flxSeperatorSubheader.add();
        flxSeperator.add(flxSeperatorSubheader);
        flxSubheader.add(flxSubOption1, flxSubOption2, flxSubOption3, flxSubOption4, flxSeperator);
        var flxSeparatorContainer2 = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "clipBounds": true,
            "height": "1px",
            "id": "flxSeparatorContainer2",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "left": "0dp",
            "isModalContainer": false,
            "skin": "slFbox",
            "top": "15px",
            "width": "100%"
        }, controller.args[0], "flxSeparatorContainer2"), extendConfig({}, controller.args[1], "flxSeparatorContainer2"), extendConfig({}, controller.args[2], "flxSeparatorContainer2"));
        flxSeparatorContainer2.setDefaultUnit(kony.flex.DP);
        var flxAddSeparator2 = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "bottom": "0dp",
            "clipBounds": true,
            "height": "1px",
            "id": "flxAddSeparator2",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "left": "43px",
            "isModalContainer": false,
            "right": "20px",
            "skin": "sknflxe1e5edop100",
            "top": "0dp",
            "zIndex": 1
        }, controller.args[0], "flxAddSeparator2"), extendConfig({}, controller.args[1], "flxAddSeparator2"), extendConfig({}, controller.args[2], "flxAddSeparator2"));
        flxAddSeparator2.setDefaultUnit(kony.flex.DP);
        flxAddSeparator2.add();
        flxSeparatorContainer2.add(flxAddSeparator2);
        flxOption2.add(flxContentContainer2, lblOptional2, flxSubheader, flxSeparatorContainer2);
        var flxOption5 = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
            "clipBounds": true,
            "id": "flxOption5",
            "isVisible": false,
            "layoutType": kony.flex.FLOW_VERTICAL,
            "left": "0dp",
            "isModalContainer": false,
            "skin": "slFbox",
            "top": "0dp",
            "width": "100%",
            "zIndex": 1
        }, controller.args[0], "flxOption5"), extendConfig({}, controller.args[1], "flxOption5"), extendConfig({}, controller.args[2], "flxOption5"));
        flxOption5.setDefaultUnit(kony.flex.DP);
        var flxContentContainer5 = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
            "clipBounds": true,
            "id": "flxContentContainer5",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "left": "0dp",
            "isModalContainer": false,
            "skin": "slFbox",
            "top": "15dp",
            "width": "100%"
        }, controller.args[0], "flxContentContainer5"), extendConfig({}, controller.args[1], "flxContentContainer5"), extendConfig({}, controller.args[2], "flxContentContainer5"));
        flxContentContainer5.setDefaultUnit(kony.flex.DP);
        var lblCompleted5 = new kony.ui.Label(extendConfig({
            "id": "lblCompleted5",
            "isVisible": true,
            "left": "15dp",
            "skin": "sknIcon18pxGray",
            "text": "",
            "top": "0dp",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, controller.args[0], "lblCompleted5"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblCompleted5"), extendConfig({}, controller.args[2], "lblCompleted5"));
        var btnOption5 = new kony.ui.Button(extendConfig({
            "bottom": "0dp",
            "id": "btnOption5",
            "isVisible": true,
            "left": "43dp",
            "skin": "Btn737678font12pxKA",
            "text": "CONFIGURE IN APP POPUPS",
            "top": "0dp",
            "width": "150dp",
            "zIndex": 1
        }, controller.args[0], "btnOption5"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_TOP_LEFT,
            "displayText": true,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "btnOption5"), extendConfig({}, controller.args[2], "btnOption5"));
        var flxImgArrow5 = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "bottom": "0dp",
            "clipBounds": true,
            "height": "15px",
            "id": "flxImgArrow5",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "isModalContainer": false,
            "right": "20px",
            "skin": "slFbox",
            "top": "0dp",
            "width": "15px",
            "zIndex": 1
        }, controller.args[0], "flxImgArrow5"), extendConfig({}, controller.args[1], "flxImgArrow5"), extendConfig({}, controller.args[2], "flxImgArrow5"));
        flxImgArrow5.setDefaultUnit(kony.flex.DP);
        var imgSelected5 = new kony.ui.Image2(extendConfig({
            "height": "100%",
            "id": "imgSelected5",
            "isVisible": false,
            "right": 0,
            "skin": "slImage",
            "src": "right_arrow2x.png",
            "top": "0dp",
            "width": "100%",
            "zIndex": 1
        }, controller.args[0], "imgSelected5"), extendConfig({
            "imageScaleMode": constants.IMAGE_SCALE_MODE_MAINTAIN_ASPECT_RATIO,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "imgSelected5"), extendConfig({}, controller.args[2], "imgSelected5"));
        var lblSelected5 = new kony.ui.Label(extendConfig({
            "centerX": "50%",
            "centerY": "50%",
            "height": "100%",
            "id": "lblSelected5",
            "isVisible": false,
            "left": "5dp",
            "skin": "sknicon15pxBlack",
            "i18n_text": "kony.i18n.getLocalizedString(\"i18n.userwidgetmodel.fontIconSelected1\")",
            "top": "6dp",
            "width": "100%",
            "zIndex": 1
        }, controller.args[0], "lblSelected5"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblSelected5"), extendConfig({}, controller.args[2], "lblSelected5"));
        flxImgArrow5.add(imgSelected5, lblSelected5);
        flxContentContainer5.add(lblCompleted5, btnOption5, flxImgArrow5);
        var lblOptional5 = new kony.ui.Label(extendConfig({
            "id": "lblOptional5",
            "isVisible": false,
            "left": "20px",
            "skin": "slLabel0d4f692dab05249",
            "i18n_text": "kony.i18n.getLocalizedString(\"i18n.permission.Optional\")",
            "top": "10px",
            "width": "93px",
            "zIndex": 1
        }, controller.args[0], "lblOptional5"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblOptional5"), extendConfig({}, controller.args[2], "lblOptional5"));
        var flxSeparatorContainer5 = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "clipBounds": true,
            "height": "1dp",
            "id": "flxSeparatorContainer5",
            "isVisible": false,
            "layoutType": kony.flex.FREE_FORM,
            "left": "0dp",
            "isModalContainer": false,
            "skin": "slFbox",
            "top": "15dp",
            "width": "100%"
        }, controller.args[0], "flxSeparatorContainer5"), extendConfig({}, controller.args[1], "flxSeparatorContainer5"), extendConfig({}, controller.args[2], "flxSeparatorContainer5"));
        flxSeparatorContainer5.setDefaultUnit(kony.flex.DP);
        var flxAddSeparator5 = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "bottom": 0,
            "clipBounds": true,
            "height": "1px",
            "id": "flxAddSeparator5",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "left": "43px",
            "isModalContainer": false,
            "right": "20px",
            "skin": "sknflxe1e5edop100",
            "zIndex": 1
        }, controller.args[0], "flxAddSeparator5"), extendConfig({}, controller.args[1], "flxAddSeparator5"), extendConfig({}, controller.args[2], "flxAddSeparator5"));
        flxAddSeparator5.setDefaultUnit(kony.flex.DP);
        flxAddSeparator5.add();
        flxSeparatorContainer5.add(flxAddSeparator5);
        flxOption5.add(flxContentContainer5, lblOptional5, flxSeparatorContainer5);
        var flxBottomPadding = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "bottom": "0dp",
            "clipBounds": true,
            "height": "20px",
            "id": "flxBottomPadding",
            "isVisible": true,
            "layoutType": kony.flex.FLOW_VERTICAL,
            "left": "0dp",
            "isModalContainer": false,
            "skin": "slFbox",
            "width": "100%",
            "zIndex": 1
        }, controller.args[0], "flxBottomPadding"), extendConfig({}, controller.args[1], "flxBottomPadding"), extendConfig({}, controller.args[2], "flxBottomPadding"));
        flxBottomPadding.setDefaultUnit(kony.flex.DP);
        flxBottomPadding.add();
        verticalTabs.add(flxOption1, flxOption4, flxOption3, flxOption2, flxOption5, flxBottomPadding);
        return verticalTabs;
    }
})