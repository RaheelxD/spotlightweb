define(function() {
    return function(controller) {
        var customListbox = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
            "clipBounds": true,
            "isMaster": true,
            "id": "customListbox",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "left": "0dp",
            "isModalContainer": false,
            "skin": "slFbox",
            "top": "0dp",
            "width": "100%"
        }, controller.args[0], "customListbox"), extendConfig({}, controller.args[1], "customListbox"), extendConfig({}, controller.args[2], "customListbox"));
        customListbox.setDefaultUnit(kony.flex.DP);
        var flxSelectedText = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "clipBounds": true,
            "height": "40dp",
            "id": "flxSelectedText",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "left": "0dp",
            "isModalContainer": false,
            "skin": "sknflxffffffop100Bordercbcdd1Radius3px",
            "top": "0dp",
            "width": "100%",
            "zIndex": 1
        }, controller.args[0], "flxSelectedText"), extendConfig({}, controller.args[1], "flxSelectedText"), extendConfig({
            "hoverSkin": "sknflxffffffBorderE1E5EERadius3pxPointer"
        }, controller.args[2], "flxSelectedText"));
        flxSelectedText.setDefaultUnit(kony.flex.DP);
        var lblSelectedValue = new kony.ui.Label(extendConfig({
            "centerY": "50%",
            "id": "lblSelectedValue",
            "isVisible": true,
            "left": "14dp",
            "skin": "sknLbl485C75LatoRegular13Px",
            "text": "Label",
            "top": "3dp",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, controller.args[0], "lblSelectedValue"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblSelectedValue"), extendConfig({}, controller.args[2], "lblSelectedValue"));
        var flxDropdown = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "centerY": "50%",
            "clipBounds": true,
            "height": "30dp",
            "id": "flxDropdown",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "isModalContainer": false,
            "right": "0dp",
            "skin": "slFbox",
            "top": "0dp",
            "width": "30dp"
        }, controller.args[0], "flxDropdown"), extendConfig({}, controller.args[1], "flxDropdown"), extendConfig({}, controller.args[2], "flxDropdown"));
        flxDropdown.setDefaultUnit(kony.flex.DP);
        var lblIconDropdown = new kony.ui.Label(extendConfig({
            "centerY": "50%",
            "id": "lblIconDropdown",
            "isVisible": true,
            "right": "10dp",
            "skin": "sknFontIconLimits13Px",
            "text": "",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, controller.args[0], "lblIconDropdown"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblIconDropdown"), extendConfig({}, controller.args[2], "lblIconDropdown"));
        flxDropdown.add(lblIconDropdown);
        flxSelectedText.add(lblSelectedValue, flxDropdown);
        var flxSegmentList = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "clipBounds": false,
            "height": "200dp",
            "id": "flxSegmentList",
            "isVisible": false,
            "layoutType": kony.flex.FLOW_VERTICAL,
            "left": "0dp",
            "isModalContainer": false,
            "skin": "sknflxFFFFFFBr1pxe1e5edR3px3sided",
            "top": "30dp",
            "width": "100%",
            "zIndex": 1
        }, controller.args[0], "flxSegmentList"), extendConfig({}, controller.args[1], "flxSegmentList"), extendConfig({}, controller.args[2], "flxSegmentList"));
        flxSegmentList.setDefaultUnit(kony.flex.DP);
        var flxSearch = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "clipBounds": true,
            "focusSkin": "sknflxffffffop100",
            "height": "40dp",
            "id": "flxSearch",
            "isVisible": true,
            "layoutType": kony.flex.FLOW_HORIZONTAL,
            "left": "0",
            "isModalContainer": false,
            "skin": "sknflxffffffop100",
            "top": "0",
            "width": "100%"
        }, controller.args[0], "flxSearch"), extendConfig({}, controller.args[1], "flxSearch"), extendConfig({
            "hoverSkin": "sknFlxffffffOp100Cursor"
        }, controller.args[2], "flxSearch"));
        flxSearch.setDefaultUnit(kony.flex.DP);
        var flxCheckBox = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "centerY": "50%",
            "clipBounds": true,
            "height": "15px",
            "id": "flxCheckBox",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "left": "15px",
            "isModalContainer": false,
            "skin": "slFbox",
            "top": "9dp",
            "width": "12px",
            "zIndex": 2
        }, controller.args[0], "flxCheckBox"), extendConfig({}, controller.args[1], "flxCheckBox"), extendConfig({}, controller.args[2], "flxCheckBox"));
        flxCheckBox.setDefaultUnit(kony.flex.DP);
        var imgSearchIcon = new kony.ui.Image2(extendConfig({
            "centerY": "50%",
            "height": "15px",
            "id": "imgSearchIcon",
            "isVisible": true,
            "left": "0px",
            "skin": "CopyslImage2",
            "src": "search_1x.png",
            "width": "100%",
            "zIndex": 1
        }, controller.args[0], "imgSearchIcon"), extendConfig({
            "imageScaleMode": constants.IMAGE_SCALE_MODE_FIT_TO_DIMENSIONS,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "imgSearchIcon"), extendConfig({}, controller.args[2], "imgSearchIcon"));
        flxCheckBox.add(imgSearchIcon);
        var lblDescription = new kony.ui.Label(extendConfig({
            "bottom": "6dp",
            "centerY": "50%",
            "id": "lblDescription",
            "isVisible": false,
            "left": "10px",
            "skin": "sknlblLatoBold35475f14px",
            "text": "Search",
            "top": "6dp",
            "width": "80%",
            "zIndex": 1
        }, controller.args[0], "lblDescription"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblDescription"), extendConfig({}, controller.args[2], "lblDescription"));
        var tbxSearchCountry = new kony.ui.TextBox2(extendConfig({
            "autoCapitalize": constants.TEXTBOX_AUTO_CAPITALIZE_NONE,
            "focusSkin": "skntbxffffffNoBorderlato35475f14px",
            "height": "100%",
            "id": "tbxSearchCountry",
            "isVisible": true,
            "keyBoardStyle": constants.TEXTBOX_KEY_BOARD_STYLE_DEFAULT,
            "left": "5dp",
            "placeholder": "Search",
            "secureTextEntry": false,
            "skin": "skntbxffffffNoBorderlato35475f14px",
            "textInputMode": constants.TEXTBOX_INPUT_MODE_ANY,
            "top": "0dp",
            "width": "85%",
            "zIndex": 1
        }, controller.args[0], "tbxSearchCountry"), extendConfig({
            "containerHeightMode": constants.TEXTBOX_FONT_METRICS_DRIVEN_HEIGHT,
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [3, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "tbxSearchCountry"), extendConfig({
            "autoCorrect": false,
            "placeholderSkin": "defTextBoxPlaceholder"
        }, controller.args[2], "tbxSearchCountry"));
        flxSearch.add(flxCheckBox, lblDescription, tbxSearchCountry);
        var segList = new kony.ui.SegmentedUI2(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "bottom": "10px",
            "data": [{
                "imgCheckBox": "checkboxnormal.png",
                "lblDescription": "KL098234 - Westheimer"
            }, {
                "imgCheckBox": "checkboxnormal.png",
                "lblDescription": "KL098234 - Westheimer"
            }, {
                "imgCheckBox": "checkboxnormal.png",
                "lblDescription": "KL098234 - Westheimer"
            }, {
                "imgCheckBox": "checkboxnormal.png",
                "lblDescription": "KL098234 - Westheimer"
            }],
            "groupCells": false,
            "height": "150dp",
            "id": "segList",
            "isVisible": true,
            "left": "0dp",
            "needPageIndicator": true,
            "pageOffDotImage": "pageoffdot.png",
            "pageOnDotImage": "pageondot.png",
            "retainSelection": false,
            "rowFocusSkin": "seg2Focus",
            "rowSkin": "seg2Normal",
            "rowTemplate": "flxSearchDropDown",
            "sectionHeaderSkin": "sliPhoneSegmentHeader",
            "selectionBehavior": constants.SEGUI_MULTI_SELECT_BEHAVIOR,
            "selectionBehaviorConfig": {
                "imageIdentifier": "imgCheckBox",
                "selectedStateImage": "checkboxselected.png",
                "unselectedStateImage": "checkboxnormal.png"
            },
            "separatorColor": "aaaaaa00",
            "separatorRequired": false,
            "separatorThickness": 1,
            "showScrollbars": false,
            "top": "-2dp",
            "viewType": constants.SEGUI_VIEW_TYPE_TABLEVIEW,
            "widgetDataMap": {
                "flxCheckBox": "flxCheckBox",
                "flxSearchDropDown": "flxSearchDropDown",
                "imgCheckBox": "imgCheckBox",
                "lblDescription": "lblDescription"
            },
            "width": "95%",
            "zIndex": 1
        }, controller.args[0], "segList"), extendConfig({
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "segList"), extendConfig({}, controller.args[2], "segList"));
        flxSegmentList.add(flxSearch, segList);
        var flxListboxError = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
            "clipBounds": true,
            "id": "flxListboxError",
            "isVisible": false,
            "layoutType": kony.flex.FLOW_HORIZONTAL,
            "left": "0dp",
            "isModalContainer": false,
            "skin": "slFbox",
            "top": "45dp",
            "width": "100%",
            "zIndex": 1
        }, controller.args[0], "flxListboxError"), extendConfig({}, controller.args[1], "flxListboxError"), extendConfig({}, controller.args[2], "flxListboxError"));
        flxListboxError.setDefaultUnit(kony.flex.DP);
        var lblErrorIcon = new kony.ui.Label(extendConfig({
            "id": "lblErrorIcon",
            "isVisible": true,
            "left": "0dp",
            "skin": "sknErrorIcon",
            "text": "",
            "top": "0dp",
            "width": "15dp",
            "zIndex": 1
        }, controller.args[0], "lblErrorIcon"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblErrorIcon"), extendConfig({}, controller.args[2], "lblErrorIcon"));
        var lblErrorMsg = new kony.ui.Label(extendConfig({
            "id": "lblErrorMsg",
            "isVisible": true,
            "left": "0dp",
            "skin": "sknlblError",
            "text": "Invalid data",
            "top": "0dp",
            "width": "170dp",
            "zIndex": 1
        }, controller.args[0], "lblErrorMsg"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblErrorMsg"), extendConfig({}, controller.args[2], "lblErrorMsg"));
        flxListboxError.add(lblErrorIcon, lblErrorMsg);
        customListbox.add(flxSelectedText, flxSegmentList, flxListboxError);
        return customListbox;
    }
})