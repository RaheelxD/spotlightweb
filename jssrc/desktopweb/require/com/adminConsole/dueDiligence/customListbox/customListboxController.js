define("com/adminConsole/dueDiligence/customListbox/usercustomListboxController", function() {
    return {};
});
define("com/adminConsole/dueDiligence/customListbox/customListboxControllerActions", {
    /*
          This is an auto generated file and any modifications to it may result in corruption of the action sequence.
        */
});
define("com/adminConsole/dueDiligence/customListbox/customListboxController", ["com/adminConsole/dueDiligence/customListbox/usercustomListboxController", "com/adminConsole/dueDiligence/customListbox/customListboxControllerActions"], function() {
    var controller = require("com/adminConsole/dueDiligence/customListbox/usercustomListboxController");
    var actions = require("com/adminConsole/dueDiligence/customListbox/customListboxControllerActions");
    for (var key in actions) {
        controller[key] = actions[key];
    }
    return controller;
});
