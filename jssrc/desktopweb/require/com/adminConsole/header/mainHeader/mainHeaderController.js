define("com/adminConsole/header/mainHeader/usermainHeaderController", function() {
    var VisibleState = function(initialState) {
        this.isVisible = initialState;
        this.setVisiblity = function(show) {
            this.isVisible = show;
        };
    };
    var controller = {
        imgLogout_onTouchStart: function() {
            var authModule = kony.mvc.MDAApplication.getSharedInstance().getModuleManager().getModule("AuthModule");
            authModule.presentationController.doLogout();
        },
        frmCSR_btnCreateNewMessage: new VisibleState(true),
        frmCSR_btnCreateMessageTemplate: new VisibleState(true),
        frmCSR_setBtnDropDownText: function(text) {
            if (text === kony.i18n.getLocalizedString("i18n.frmCSRController.CREATE_NEW_MESSAGE")) {
                this.view.btnDropdownList.text = text;
                this.view.btnDropdownList.isVisible = controller.frmCSR_btnCreateNewMessage.isVisible;
            } else if (text === kony.i18n.getLocalizedString("i18n.frmCSRController.CREATE_NEW_TEMPLATE")) {
                this.view.btnDropdownList.text = text;
                this.view.btnDropdownList.isVisible = controller.frmCSR_btnCreateMessageTemplate.isVisible;
            }
        }
    };
    return controller;
});
define("com/adminConsole/header/mainHeader/mainHeaderControllerActions", {
    /*
        This is an auto generated file and any modifications to it may result in corruption of the action sequence.
    */
    AS_Image_j15f5719709148459da2ca8b6b9f27da: function AS_Image_j15f5719709148459da2ca8b6b9f27da(eventobject, x, y) {
        var self = this;
        this.imgLogout_onTouchStart();
    }
});
define("com/adminConsole/header/mainHeader/mainHeaderController", ["com/adminConsole/header/mainHeader/usermainHeaderController", "com/adminConsole/header/mainHeader/mainHeaderControllerActions"], function() {
    var controller = require("com/adminConsole/header/mainHeader/usermainHeaderController");
    var actions = require("com/adminConsole/header/mainHeader/mainHeaderControllerActions");
    for (var key in actions) {
        controller[key] = actions[key];
    }
    return controller;
});
