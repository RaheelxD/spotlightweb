define(function() {
    return function(controller) {
        var subHeader = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "clipBounds": true,
            "isMaster": true,
            "height": "48dp",
            "id": "subHeader",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "left": "0dp",
            "isModalContainer": false,
            "preShow": function(eventobject) {
                controller.AS_FlexContainer_f11d59456811475ba309998f08be5a1b(eventobject);
            },
            "skin": "CopyslFbox2",
            "top": "0dp",
            "width": "100%"
        }, controller.args[0], "subHeader"), extendConfig({}, controller.args[1], "subHeader"), extendConfig({}, controller.args[2], "subHeader"));
        subHeader.setDefaultUnit(kony.flex.DP);
        var flxSubHeader = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "clipBounds": true,
            "height": "100%",
            "id": "flxSubHeader",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "left": "0dp",
            "isModalContainer": false,
            "skin": "slFbox0b7aaa2e3bfa340",
            "top": "0dp",
            "width": "100%",
            "zIndex": 1
        }, controller.args[0], "flxSubHeader"), extendConfig({}, controller.args[1], "flxSubHeader"), extendConfig({}, controller.args[2], "flxSubHeader"));
        flxSubHeader.setDefaultUnit(kony.flex.DP);
        var flxMenu = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "clipBounds": true,
            "height": "100%",
            "id": "flxMenu",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "left": "34dp",
            "isModalContainer": false,
            "skin": "CopyslFbox2",
            "top": "0dp",
            "width": "190px",
            "zIndex": 1
        }, controller.args[0], "flxMenu"), extendConfig({}, controller.args[1], "flxMenu"), extendConfig({}, controller.args[2], "flxMenu"));
        flxMenu.setDefaultUnit(kony.flex.DP);
        var lblRecords = new kony.ui.Label(extendConfig({
            "centerY": "50%",
            "id": "lblRecords",
            "isVisible": true,
            "left": "0dp",
            "skin": "slLabel0fb44c6072cea44",
            "i18n_text": "kony.i18n.getLocalizedString(\"i18n.permission.RECORDSPERPAGE\")",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, controller.args[0], "lblRecords"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblRecords"), extendConfig({}, controller.args[2], "lblRecords"));
        var lbxPageNumbers = new kony.ui.ListBox(extendConfig({
            "centerY": "50%",
            "height": "20dp",
            "id": "lbxPageNumbers",
            "isVisible": true,
            "left": "125dp",
            "masterData": [
                ["lb1", "10"],
                ["lb2", "50"],
                ["lb3", "100"]
            ],
            "selectedKey": "lb1",
            "skin": "slListBox0f57f820558d440",
            "width": "50dp",
            "zIndex": 1
        }, controller.args[0], "lbxPageNumbers"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lbxPageNumbers"), extendConfig({
            "hoverSkin": "lstboxcursor",
            "multiSelect": false
        }, controller.args[2], "lbxPageNumbers"));
        flxMenu.add(lblRecords, lbxPageNumbers);
        var flxSearch = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "clipBounds": true,
            "height": "100%",
            "id": "flxSearch",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "isModalContainer": false,
            "right": 32,
            "skin": "CopyslFbox2",
            "top": "0px",
            "width": "350px",
            "zIndex": 1
        }, controller.args[0], "flxSearch"), extendConfig({}, controller.args[1], "flxSearch"), extendConfig({}, controller.args[2], "flxSearch"));
        flxSearch.setDefaultUnit(kony.flex.DP);
        var flxSearchContainer = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "clipBounds": true,
            "height": "35px",
            "id": "flxSearchContainer",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "left": "0dp",
            "isModalContainer": false,
            "skin": "sknflxd5d9ddop100",
            "top": "8dp",
            "width": "350px",
            "zIndex": 1
        }, controller.args[0], "flxSearchContainer"), extendConfig({}, controller.args[1], "flxSearchContainer"), extendConfig({}, controller.args[2], "flxSearchContainer"));
        flxSearchContainer.setDefaultUnit(kony.flex.DP);
        var fontIconSearchImg = new kony.ui.Label(extendConfig({
            "centerY": "50%",
            "id": "fontIconSearchImg",
            "isVisible": true,
            "left": "12px",
            "skin": "sknFontIconSearchImg20px",
            "i18n_text": "kony.i18n.getLocalizedString(\"i18n.userwidgetmodel.lblSearchIcon\")",
            "top": "0dp",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, controller.args[0], "fontIconSearchImg"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "fontIconSearchImg"), extendConfig({}, controller.args[2], "fontIconSearchImg"));
        var tbxSearchBox = new kony.ui.TextBox2(extendConfig({
            "autoCapitalize": constants.TEXTBOX_AUTO_CAPITALIZE_NONE,
            "centerY": "50%",
            "height": "30px",
            "id": "tbxSearchBox",
            "isVisible": true,
            "keyBoardStyle": constants.TEXTBOX_KEY_BOARD_STYLE_DEFAULT,
            "left": "30dp",
            "onTouchStart": controller.AS_TextField_cfab06859f0f43eca99495891ee290d5,
            "placeholder": "Search",
            "right": "35px",
            "secureTextEntry": false,
            "skin": "skntbxffffffNoBorderlato35475f14px",
            "textInputMode": constants.TEXTBOX_INPUT_MODE_ANY,
            "zIndex": 1
        }, controller.args[0], "tbxSearchBox"), extendConfig({
            "containerHeightMode": constants.TEXTBOX_FONT_METRICS_DRIVEN_HEIGHT,
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [2, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "tbxSearchBox"), extendConfig({
            "autoCorrect": false
        }, controller.args[2], "tbxSearchBox"));
        var flxClearSearchImage = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "clipBounds": true,
            "height": "100%",
            "id": "flxClearSearchImage",
            "isVisible": false,
            "layoutType": kony.flex.FREE_FORM,
            "isModalContainer": false,
            "right": "0px",
            "skin": "sknCursor",
            "top": "0dp",
            "width": "35px",
            "zIndex": 1
        }, controller.args[0], "flxClearSearchImage"), extendConfig({}, controller.args[1], "flxClearSearchImage"), extendConfig({
            "hoverSkin": "sknCursor"
        }, controller.args[2], "flxClearSearchImage"));
        flxClearSearchImage.setDefaultUnit(kony.flex.DP);
        var fontIconCross = new kony.ui.Label(extendConfig({
            "centerY": "50%",
            "id": "fontIconCross",
            "isVisible": true,
            "left": "5px",
            "skin": "sknFontIconSearchCross16px",
            "i18n_text": "kony.i18n.getLocalizedString(\"i18n.userwidgetmodel.lblClearSearch\")",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, controller.args[0], "fontIconCross"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "fontIconCross"), extendConfig({}, controller.args[2], "fontIconCross"));
        flxClearSearchImage.add(fontIconCross);
        flxSearchContainer.add(fontIconSearchImg, tbxSearchBox, flxClearSearchImage);
        flxSearch.add(flxSearchContainer);
        flxSubHeader.add(flxMenu, flxSearch);
        subHeader.add(flxSubHeader);
        return subHeader;
    }
})