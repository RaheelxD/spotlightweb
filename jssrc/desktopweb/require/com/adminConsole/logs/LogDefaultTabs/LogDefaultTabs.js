define(function() {
    return function(controller) {
        var LogDefaultTabs = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "clipBounds": true,
            "isMaster": true,
            "height": "100%",
            "id": "LogDefaultTabs",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "left": "0dp",
            "isModalContainer": false,
            "skin": "sknflxffffffop100dbdbe6Radius3px",
            "top": "0dp",
            "width": "100%"
        }, controller.args[0], "LogDefaultTabs"), extendConfig({}, controller.args[1], "LogDefaultTabs"), extendConfig({
            "hoverSkin": "tabsHoverSkin"
        }, controller.args[2], "LogDefaultTabs"));
        LogDefaultTabs.setDefaultUnit(kony.flex.DP);
        var imgLog = new kony.ui.Image2(extendConfig({
            "height": "60px",
            "id": "imgLog",
            "isVisible": false,
            "left": "30px",
            "skin": "slImage",
            "src": "imagedrag.png",
            "top": "30px",
            "width": "60px",
            "zIndex": 1
        }, controller.args[0], "imgLog"), extendConfig({
            "imageScaleMode": constants.IMAGE_SCALE_MODE_MAINTAIN_ASPECT_RATIO,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "imgLog"), extendConfig({}, controller.args[2], "imgLog"));
        var lblLog = new kony.ui.Label(extendConfig({
            "height": "55dp",
            "id": "lblLog",
            "isVisible": true,
            "left": "40dp",
            "skin": "slLabel",
            "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmAlertsManagement.lblAlertTypeDescriptionCount\")",
            "top": "30dp",
            "width": "65dp",
            "zIndex": 1
        }, controller.args[0], "lblLog"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblLog"), extendConfig({}, controller.args[2], "lblLog"));
        var lblLogHeading = new kony.ui.Label(extendConfig({
            "height": "20px",
            "id": "lblLogHeading",
            "isVisible": true,
            "left": "105px",
            "skin": "sknlblLatoRegular484b5213px",
            "i18n_text": "kony.i18n.getLocalizedString(\"i18n.userwidgetmodel.lblName\")",
            "top": "30px",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, controller.args[0], "lblLogHeading"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblLogHeading"), extendConfig({}, controller.args[2], "lblLogHeading"));
        var lblLogDesc = new kony.ui.Label(extendConfig({
            "bottom": "30px",
            "id": "lblLogDesc",
            "isVisible": true,
            "left": "105px",
            "right": "30px",
            "skin": "sknLogsDescription",
            "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmLogsController.TempText\")",
            "top": "60px",
            "zIndex": 1
        }, controller.args[0], "lblLogDesc"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_TOP_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblLogDesc"), extendConfig({}, controller.args[2], "lblLogDesc"));
        LogDefaultTabs.add(imgLog, lblLog, lblLogHeading, lblLogDesc);
        return LogDefaultTabs;
    }
})