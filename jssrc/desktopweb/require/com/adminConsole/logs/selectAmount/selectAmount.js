define(function() {
    return function(controller) {
        var selectAmount = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
            "clipBounds": true,
            "isMaster": true,
            "id": "selectAmount",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "left": "0dp",
            "isModalContainer": false,
            "skin": "slFbox",
            "top": "0dp",
            "width": "160dp"
        }, controller.args[0], "selectAmount"), extendConfig({}, controller.args[1], "selectAmount"), extendConfig({}, controller.args[2], "selectAmount"));
        selectAmount.setDefaultUnit(kony.flex.DP);
        var flxAmount = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
            "clipBounds": true,
            "id": "flxAmount",
            "isVisible": true,
            "layoutType": kony.flex.FLOW_VERTICAL,
            "left": "0px",
            "isModalContainer": false,
            "skin": "slFbox",
            "top": "0dp",
            "width": "160dp",
            "zIndex": 1
        }, controller.args[0], "flxAmount"), extendConfig({}, controller.args[1], "flxAmount"), extendConfig({}, controller.args[2], "flxAmount"));
        flxAmount.setDefaultUnit(kony.flex.DP);
        var flxAmountList = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
            "clipBounds": true,
            "id": "flxAmountList",
            "isVisible": true,
            "layoutType": kony.flex.FLOW_VERTICAL,
            "left": "0dp",
            "isModalContainer": false,
            "skin": "sknflxffffffop0a",
            "top": "0dp",
            "width": "100%",
            "zIndex": 1
        }, controller.args[0], "flxAmountList"), extendConfig({}, controller.args[1], "flxAmountList"), extendConfig({}, controller.args[2], "flxAmountList"));
        flxAmountList.setDefaultUnit(kony.flex.DP);
        var flxList1 = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
            "clipBounds": true,
            "id": "flxList1",
            "isVisible": true,
            "layoutType": kony.flex.FLOW_VERTICAL,
            "left": "0dp",
            "isModalContainer": false,
            "skin": "slFbox",
            "top": "0dp",
            "width": "100%"
        }, controller.args[0], "flxList1"), extendConfig({}, controller.args[1], "flxList1"), extendConfig({
            "hoverSkin": "sknCursor"
        }, controller.args[2], "flxList1"));
        flxList1.setDefaultUnit(kony.flex.DP);
        var lbll0To100 = new kony.ui.Label(extendConfig({
            "id": "lbll0To100",
            "isVisible": true,
            "left": "10dp",
            "skin": "sknlblLatoRegular484b5213px",
            "i18n_text": "kony.i18n.getLocalizedString(\"i18n.userwidgetmodel.lbll0To100\")",
            "top": "10dp",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, controller.args[0], "lbll0To100"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lbll0To100"), extendConfig({}, controller.args[2], "lbll0To100"));
        flxList1.add(lbll0To100);
        var flxList2 = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
            "clipBounds": true,
            "id": "flxList2",
            "isVisible": true,
            "layoutType": kony.flex.FLOW_VERTICAL,
            "left": "0dp",
            "isModalContainer": false,
            "skin": "slFbox",
            "top": "0dp",
            "width": "100%"
        }, controller.args[0], "flxList2"), extendConfig({}, controller.args[1], "flxList2"), extendConfig({
            "hoverSkin": "sknCursor"
        }, controller.args[2], "flxList2"));
        flxList2.setDefaultUnit(kony.flex.DP);
        var lbl100To500 = new kony.ui.Label(extendConfig({
            "id": "lbl100To500",
            "isVisible": true,
            "left": "10dp",
            "skin": "sknlblLatoRegular484b5213px",
            "i18n_text": "kony.i18n.getLocalizedString(\"i18n.userwidgetmodel.lbl100To500\")",
            "top": "10dp",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, controller.args[0], "lbl100To500"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lbl100To500"), extendConfig({}, controller.args[2], "lbl100To500"));
        flxList2.add(lbl100To500);
        var flxList3 = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
            "clipBounds": true,
            "id": "flxList3",
            "isVisible": true,
            "layoutType": kony.flex.FLOW_VERTICAL,
            "left": "0dp",
            "isModalContainer": false,
            "skin": "slFbox",
            "top": "0dp",
            "width": "100%"
        }, controller.args[0], "flxList3"), extendConfig({}, controller.args[1], "flxList3"), extendConfig({
            "hoverSkin": "sknCursor"
        }, controller.args[2], "flxList3"));
        flxList3.setDefaultUnit(kony.flex.DP);
        var lbl500To1000 = new kony.ui.Label(extendConfig({
            "id": "lbl500To1000",
            "isVisible": true,
            "left": "10dp",
            "skin": "sknlblLatoRegular484b5213px",
            "i18n_text": "kony.i18n.getLocalizedString(\"i18n.userwidgetmodel.lbl500To1000\")",
            "top": "10dp",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, controller.args[0], "lbl500To1000"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lbl500To1000"), extendConfig({}, controller.args[2], "lbl500To1000"));
        flxList3.add(lbl500To1000);
        var flxList4 = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
            "clipBounds": true,
            "id": "flxList4",
            "isVisible": true,
            "layoutType": kony.flex.FLOW_VERTICAL,
            "left": "0dp",
            "isModalContainer": false,
            "skin": "slFbox",
            "top": "0dp",
            "width": "100%"
        }, controller.args[0], "flxList4"), extendConfig({}, controller.args[1], "flxList4"), extendConfig({
            "hoverSkin": "sknCursor"
        }, controller.args[2], "flxList4"));
        flxList4.setDefaultUnit(kony.flex.DP);
        var lbl1000To5000 = new kony.ui.Label(extendConfig({
            "id": "lbl1000To5000",
            "isVisible": true,
            "left": "10dp",
            "skin": "sknlblLatoRegular484b5213px",
            "i18n_text": "kony.i18n.getLocalizedString(\"i18n.userwidgetmodel.lbl1000To5000\")",
            "top": "10dp",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, controller.args[0], "lbl1000To5000"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lbl1000To5000"), extendConfig({}, controller.args[2], "lbl1000To5000"));
        flxList4.add(lbl1000To5000);
        var flxList5 = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
            "clipBounds": true,
            "id": "flxList5",
            "isVisible": true,
            "layoutType": kony.flex.FLOW_VERTICAL,
            "left": "0dp",
            "isModalContainer": false,
            "skin": "slFbox",
            "top": "0dp",
            "width": "100%"
        }, controller.args[0], "flxList5"), extendConfig({}, controller.args[1], "flxList5"), extendConfig({
            "hoverSkin": "sknCursor"
        }, controller.args[2], "flxList5"));
        flxList5.setDefaultUnit(kony.flex.DP);
        var lbl5000To10000 = new kony.ui.Label(extendConfig({
            "bottom": "10dp",
            "id": "lbl5000To10000",
            "isVisible": true,
            "left": "10dp",
            "skin": "sknlblLatoRegular484b5213px",
            "i18n_text": "kony.i18n.getLocalizedString(\"i18n.userwidgetmodel.lbl5000To10000\")",
            "top": "10dp",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, controller.args[0], "lbl5000To10000"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lbl5000To10000"), extendConfig({}, controller.args[2], "lbl5000To10000"));
        flxList5.add(lbl5000To10000);
        var lblSeperator = new kony.ui.Label(extendConfig({
            "bottom": 0,
            "height": "1px",
            "id": "lblSeperator",
            "isVisible": true,
            "left": "0px",
            "right": "0dp",
            "skin": "sknSeparator",
            "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmAlertsManagement.lblAlertTypeDescriptionCount\")",
            "width": "100%",
            "zIndex": 1
        }, controller.args[0], "lblSeperator"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblSeperator"), extendConfig({}, controller.args[2], "lblSeperator"));
        flxAmountList.add(flxList1, flxList2, flxList3, flxList4, flxList5, lblSeperator);
        var flxAmountpicker = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
            "clipBounds": true,
            "id": "flxAmountpicker",
            "isVisible": true,
            "layoutType": kony.flex.FLOW_VERTICAL,
            "left": "0dp",
            "isModalContainer": false,
            "skin": "sknfbfcfc",
            "top": "0dp",
            "width": "100%",
            "zIndex": 1
        }, controller.args[0], "flxAmountpicker"), extendConfig({}, controller.args[1], "flxAmountpicker"), extendConfig({}, controller.args[2], "flxAmountpicker"));
        flxAmountpicker.setDefaultUnit(kony.flex.DP);
        var lblCustomRange = new kony.ui.Label(extendConfig({
            "id": "lblCustomRange",
            "isVisible": true,
            "left": "10dp",
            "skin": "sknlblLatoBold35475f14px",
            "i18n_text": "kony.i18n.getLocalizedString(\"i18n.userwidgetmodel.lblCustomRange\")",
            "top": "10dp",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, controller.args[0], "lblCustomRange"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblCustomRange"), extendConfig({}, controller.args[2], "lblCustomRange"));
        var lblFrom = new kony.ui.Label(extendConfig({
            "id": "lblFrom",
            "isVisible": true,
            "left": "10dp",
            "skin": "sknlblLatoRegular484b5213px",
            "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmLocations.lblFrom\")",
            "top": "10dp",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, controller.args[0], "lblFrom"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblFrom"), extendConfig({}, controller.args[2], "lblFrom"));
        var txtbxFromAmount = new kony.ui.TextBox2(extendConfig({
            "autoCapitalize": constants.TEXTBOX_AUTO_CAPITALIZE_NONE,
            "focusSkin": "skntxtbxDetailsFocus14px",
            "height": "40dp",
            "id": "txtbxFromAmount",
            "isVisible": true,
            "keyBoardStyle": constants.TEXTBOX_KEY_BOARD_STYLE_DEFAULT,
            "left": "10px",
            "placeholder": "Enter Amount",
            "right": 0,
            "secureTextEntry": false,
            "skin": "skntxtbxDetails0bbf1235271384a",
            "textInputMode": constants.TEXTBOX_INPUT_MODE_ANY,
            "top": "10dp",
            "width": "140px",
            "zIndex": 1
        }, controller.args[0], "txtbxFromAmount"), extendConfig({
            "containerHeightMode": constants.TEXTBOX_FONT_METRICS_DRIVEN_HEIGHT,
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [3, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "txtbxFromAmount"), extendConfig({
            "autoCorrect": false,
            "placeholderSkin": "defTextBoxPlaceholder"
        }, controller.args[2], "txtbxFromAmount"));
        var lblTo = new kony.ui.Label(extendConfig({
            "id": "lblTo",
            "isVisible": true,
            "left": "10dp",
            "skin": "sknlblLatoRegular484b5213px",
            "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmLocations.lblTo\")",
            "top": "10dp",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, controller.args[0], "lblTo"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblTo"), extendConfig({}, controller.args[2], "lblTo"));
        var txtbxToAmount = new kony.ui.TextBox2(extendConfig({
            "autoCapitalize": constants.TEXTBOX_AUTO_CAPITALIZE_NONE,
            "bottom": "10px",
            "centerX": "50%",
            "focusSkin": "skntxtbxDetailsFocus14px",
            "height": "40dp",
            "id": "txtbxToAmount",
            "isVisible": true,
            "keyBoardStyle": constants.TEXTBOX_KEY_BOARD_STYLE_DEFAULT,
            "left": "10px",
            "placeholder": "Enter Amount",
            "secureTextEntry": false,
            "skin": "skntxtbxDetails0bbf1235271384a",
            "textInputMode": constants.TEXTBOX_INPUT_MODE_ANY,
            "top": "10dp",
            "width": "140dp",
            "zIndex": 1
        }, controller.args[0], "txtbxToAmount"), extendConfig({
            "containerHeightMode": constants.TEXTBOX_FONT_METRICS_DRIVEN_HEIGHT,
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [3, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "txtbxToAmount"), extendConfig({
            "autoCorrect": false,
            "placeholderSkin": "defTextBoxPlaceholder"
        }, controller.args[2], "txtbxToAmount"));
        var flxSelect = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "bottom": "15px",
            "clipBounds": true,
            "height": "22dp",
            "id": "flxSelect",
            "isVisible": true,
            "layoutType": kony.flex.FLOW_VERTICAL,
            "isModalContainer": false,
            "right": "10px",
            "skin": "sknFlxSelectButton",
            "top": "0dp",
            "width": "50dp",
            "zIndex": 10
        }, controller.args[0], "flxSelect"), extendConfig({}, controller.args[1], "flxSelect"), extendConfig({
            "hoverSkin": "sknDisableCursor"
        }, controller.args[2], "flxSelect"));
        flxSelect.setDefaultUnit(kony.flex.DP);
        var lblApply = new kony.ui.Label(extendConfig({
            "height": "22dp",
            "id": "lblApply",
            "isVisible": true,
            "left": "0dp",
            "skin": "CopyslLabel0j89d35d1ac424f",
            "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmLogsController.Select\")",
            "top": "0dp",
            "width": "50dp",
            "zIndex": 10
        }, controller.args[0], "lblApply"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_CENTER,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblApply"), extendConfig({}, controller.args[2], "lblApply"));
        flxSelect.add(lblApply);
        flxAmountpicker.add(lblCustomRange, lblFrom, txtbxFromAmount, lblTo, txtbxToAmount, flxSelect);
        flxAmount.add(flxAmountList, flxAmountpicker);
        selectAmount.add(flxAmount);
        return selectAmount;
    }
})