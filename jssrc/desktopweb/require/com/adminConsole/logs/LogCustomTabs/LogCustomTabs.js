define(function() {
    return function(controller) {
        var LogCustomTabs = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
            "clipBounds": true,
            "isMaster": true,
            "id": "LogCustomTabs",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "left": "0dp",
            "isModalContainer": false,
            "skin": "sknflxffffffop100dbdbe6Radius3px",
            "top": "0dp",
            "width": "290px"
        }, controller.args[0], "LogCustomTabs"), extendConfig({}, controller.args[1], "LogCustomTabs"), extendConfig({
            "hoverSkin": "tabsHoverSkin"
        }, controller.args[2], "LogCustomTabs"));
        LogCustomTabs.setDefaultUnit(kony.flex.DP);
        var imgLog = new kony.ui.Image2(extendConfig({
            "height": "30px",
            "id": "imgLog",
            "isVisible": false,
            "left": "20px",
            "skin": "slImage",
            "src": "log_2x.png",
            "top": "51px",
            "width": "30px",
            "zIndex": 1
        }, controller.args[0], "imgLog"), extendConfig({
            "imageScaleMode": constants.IMAGE_SCALE_MODE_MAINTAIN_ASPECT_RATIO,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "imgLog"), extendConfig({}, controller.args[2], "imgLog"));
        var lblLog = new kony.ui.Label(extendConfig({
            "id": "lblLog",
            "isVisible": true,
            "left": "18dp",
            "skin": "sknIcon40px",
            "i18n_text": "kony.i18n.getLocalizedString(\"i18n.userwidgetmodel.lblLog\")",
            "top": "47dp",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, controller.args[0], "lblLog"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblLog"), extendConfig({}, controller.args[2], "lblLog"));
        var lblLogHeading = new kony.ui.Label(extendConfig({
            "height": "12px",
            "id": "lblLogHeading",
            "isVisible": true,
            "left": "21px",
            "skin": "CopysknlblLato0b3ac52ba271c4a",
            "text": "Transactional",
            "top": "15px",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, controller.args[0], "lblLogHeading"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblLogHeading"), extendConfig({}, controller.args[2], "lblLogHeading"));
        var lblDate = new kony.ui.Label(extendConfig({
            "id": "lblDate",
            "isVisible": true,
            "right": "55px",
            "skin": "CopysknlblLato0ebd17ba5374d4c",
            "i18n_text": "kony.i18n.getLocalizedString(\"i18n.userwidgetmodel.lblDate\")",
            "top": "15px",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, controller.args[0], "lblDate"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblDate"), extendConfig({}, controller.args[2], "lblDate"));
        var lblName = new kony.ui.Label(extendConfig({
            "height": "19px",
            "id": "lblName",
            "isVisible": true,
            "left": "62px",
            "skin": "CopysknlblLato0c696f51a15df4c",
            "i18n_text": "kony.i18n.getLocalizedString(\"i18n.userwidgetmodel.lblName\")",
            "top": "51px",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, controller.args[0], "lblName"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblName"), extendConfig({}, controller.args[2], "lblName"));
        var lblLogDesc = new kony.ui.Label(extendConfig({
            "id": "lblLogDesc",
            "isVisible": true,
            "left": "62px",
            "right": "35px",
            "skin": "sknLogsDescription",
            "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmLogsController.TempText\")",
            "top": "82px",
            "zIndex": 1
        }, controller.args[0], "lblLogDesc"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_TOP_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblLogDesc"), extendConfig({}, controller.args[2], "lblLogDesc"));
        var flxImgCorner = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "clipBounds": true,
            "height": "12px",
            "id": "flxImgCorner",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "isModalContainer": false,
            "right": "20px",
            "skin": "CopyslFbox0efc73ba91cad4b",
            "top": "17px",
            "width": "12px"
        }, controller.args[0], "flxImgCorner"), extendConfig({}, controller.args[1], "flxImgCorner"), extendConfig({
            "hoverSkin": "tabsHoverSkin"
        }, controller.args[2], "flxImgCorner"));
        flxImgCorner.setDefaultUnit(kony.flex.DP);
        var imgCorner = new kony.ui.Image2(extendConfig({
            "height": "100%",
            "id": "imgCorner",
            "isVisible": false,
            "left": "0dp",
            "skin": "slImage",
            "src": "delete_2x.png",
            "top": "0dp",
            "width": "100%",
            "zIndex": 1
        }, controller.args[0], "imgCorner"), extendConfig({
            "imageScaleMode": constants.IMAGE_SCALE_MODE_MAINTAIN_ASPECT_RATIO,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "imgCorner"), extendConfig({}, controller.args[2], "imgCorner"));
        var lblCorner = new kony.ui.Label(extendConfig({
            "centerY": "50%",
            "id": "lblCorner",
            "isVisible": true,
            "left": "0dp",
            "skin": "sknIcon12pxBlack",
            "i18n_text": "kony.i18n.getLocalizedString(\"i18n.userwidgetmodel.lblIconOption2\")",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, controller.args[0], "lblCorner"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblCorner"), extendConfig({}, controller.args[2], "lblCorner"));
        flxImgCorner.add(imgCorner, lblCorner);
        var flxSeperator = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "clipBounds": true,
            "height": "10px",
            "id": "flxSeperator",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "left": "85%",
            "isModalContainer": false,
            "right": "42px",
            "skin": "sknflxd6dbe7",
            "top": "17px",
            "width": "1px",
            "zIndex": 1
        }, controller.args[0], "flxSeperator"), extendConfig({}, controller.args[1], "flxSeperator"), extendConfig({
            "hoverSkin": "dummyskin"
        }, controller.args[2], "flxSeperator"));
        flxSeperator.setDefaultUnit(kony.flex.DP);
        flxSeperator.add();
        LogCustomTabs.add(imgLog, lblLog, lblLogHeading, lblDate, lblName, lblLogDesc, flxImgCorner, flxSeperator);
        return LogCustomTabs;
    }
})