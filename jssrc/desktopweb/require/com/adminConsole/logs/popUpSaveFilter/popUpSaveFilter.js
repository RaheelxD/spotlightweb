define(function() {
    return function(controller) {
        var popUpSaveFilter = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "clipBounds": true,
            "isMaster": true,
            "height": "100%",
            "id": "popUpSaveFilter",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "left": "0dp",
            "isModalContainer": false,
            "preShow": function(eventobject) {
                controller.AS_FlexContainer_f861d67e4d0e4f109d85004bd1deb82d(eventobject);
            },
            "skin": "sknflxBg000000Op50",
            "top": "0dp",
            "width": "100%"
        }, controller.args[0], "popUpSaveFilter"), extendConfig({}, controller.args[1], "popUpSaveFilter"), extendConfig({}, controller.args[2], "popUpSaveFilter"));
        popUpSaveFilter.setDefaultUnit(kony.flex.DP);
        var flxPopUp = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
            "centerX": "50%",
            "clipBounds": false,
            "id": "flxPopUp",
            "isVisible": true,
            "layoutType": kony.flex.FLOW_VERTICAL,
            "isModalContainer": false,
            "skin": "slFbox0j9f841cc563e4e",
            "top": "250px",
            "width": "500px",
            "zIndex": 10
        }, controller.args[0], "flxPopUp"), extendConfig({}, controller.args[1], "flxPopUp"), extendConfig({}, controller.args[2], "flxPopUp"));
        flxPopUp.setDefaultUnit(kony.flex.DP);
        var flxPopUpTopColor = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "clipBounds": true,
            "height": "10px",
            "id": "flxPopUpTopColor",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "left": "0dp",
            "isModalContainer": false,
            "skin": "sknflx11abeb",
            "top": "0dp",
            "width": "100%",
            "zIndex": 1
        }, controller.args[0], "flxPopUpTopColor"), extendConfig({}, controller.args[1], "flxPopUpTopColor"), extendConfig({}, controller.args[2], "flxPopUpTopColor"));
        flxPopUpTopColor.setDefaultUnit(kony.flex.DP);
        flxPopUpTopColor.add();
        var flxPopupHeader = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
            "clipBounds": true,
            "id": "flxPopupHeader",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "left": "0dp",
            "isModalContainer": false,
            "skin": "slFbox0d9c3974835234d",
            "top": "0px",
            "width": "100%",
            "zIndex": 1
        }, controller.args[0], "flxPopupHeader"), extendConfig({}, controller.args[1], "flxPopupHeader"), extendConfig({}, controller.args[2], "flxPopupHeader"));
        flxPopupHeader.setDefaultUnit(kony.flex.DP);
        var lblPopUpMainMessage = new kony.ui.Label(extendConfig({
            "id": "lblPopUpMainMessage",
            "isVisible": true,
            "left": "20px",
            "skin": "sknlblLatoBold35475f23px",
            "i18n_text": "kony.i18n.getLocalizedString(\"i18n.userwidgetmodel.lblPopUpMainMessage\")",
            "top": "20px",
            "width": "80%",
            "zIndex": 1
        }, controller.args[0], "lblPopUpMainMessage"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblPopUpMainMessage"), extendConfig({}, controller.args[2], "lblPopUpMainMessage"));
        var flxPopUpClose = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "clipBounds": true,
            "height": "25px",
            "id": "flxPopUpClose",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "isModalContainer": false,
            "right": 15,
            "skin": "CopyslFbox0efc73ba91cad4b",
            "top": "15dp",
            "width": "25px",
            "zIndex": 20
        }, controller.args[0], "flxPopUpClose"), extendConfig({}, controller.args[1], "flxPopUpClose"), extendConfig({}, controller.args[2], "flxPopUpClose"));
        flxPopUpClose.setDefaultUnit(kony.flex.DP);
        var lblPopUpClose = new kony.ui.Label(extendConfig({
            "centerY": "50%",
            "id": "lblPopUpClose",
            "isVisible": true,
            "left": "5dp",
            "skin": "sknIcon18pxGray",
            "i18n_text": "kony.i18n.getLocalizedString(\"i18n.userwidgetmodel.lblClearSearch\")",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, controller.args[0], "lblPopUpClose"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblPopUpClose"), extendConfig({}, controller.args[2], "lblPopUpClose"));
        flxPopUpClose.add(lblPopUpClose);
        var flxRow2 = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "clipBounds": true,
            "height": "210dp",
            "id": "flxRow2",
            "isVisible": true,
            "layoutType": kony.flex.FLOW_VERTICAL,
            "left": "20dp",
            "isModalContainer": false,
            "right": "20px",
            "skin": "slFbox",
            "top": "70px",
            "zIndex": 2
        }, controller.args[0], "flxRow2"), extendConfig({}, controller.args[1], "flxRow2"), extendConfig({}, controller.args[2], "flxRow2"));
        flxRow2.setDefaultUnit(kony.flex.DP);
        var lblName = new kony.ui.Label(extendConfig({
            "id": "lblName",
            "isVisible": true,
            "left": "0px",
            "skin": "sknlblLatoRegular484b5213px",
            "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmAlertsManagement.lblEditAlertMainName\")",
            "top": "0px",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 3
        }, controller.args[0], "lblName"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblName"), extendConfig({}, controller.args[2], "lblName"));
        var txtfldName = new kony.ui.TextBox2(extendConfig({
            "autoCapitalize": constants.TEXTBOX_AUTO_CAPITALIZE_NONE,
            "focusSkin": "skntxtbxDetailsFocus14px",
            "height": "40dp",
            "id": "txtfldName",
            "isVisible": true,
            "keyBoardStyle": constants.TEXTBOX_KEY_BOARD_STYLE_DEFAULT,
            "left": "0dp",
            "maxTextLength": 60,
            "secureTextEntry": false,
            "skin": "skntxtbxDetails0bbf1235271384a",
            "textInputMode": constants.TEXTBOX_INPUT_MODE_ANY,
            "top": "8px",
            "width": "100%",
            "zIndex": 2
        }, controller.args[0], "txtfldName"), extendConfig({
            "containerHeightMode": constants.TEXTBOX_FONT_METRICS_DRIVEN_HEIGHT,
            "contentAlignment": constants.CONTENT_ALIGN_TOP_LEFT,
            "padding": [1, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "txtfldName"), extendConfig({
            "autoCorrect": false
        }, controller.args[2], "txtfldName"));
        var lblNoNameError = new kony.ui.Label(extendConfig({
            "height": "15dp",
            "id": "lblNoNameError",
            "isVisible": true,
            "left": "0dp",
            "skin": "sknErrorTransparent",
            "i18n_text": "kony.i18n.getLocalizedString(\"i18n.userwidgetmodel.lblNoNameError\")",
            "top": "0dp",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 3
        }, controller.args[0], "lblNoNameError"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblNoNameError"), extendConfig({}, controller.args[2], "lblNoNameError"));
        var flxDscription = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
            "clipBounds": true,
            "id": "flxDscription",
            "isVisible": true,
            "layoutType": kony.flex.FLOW_HORIZONTAL,
            "left": "0dp",
            "isModalContainer": false,
            "skin": "slFbox",
            "top": "-10dp",
            "width": kony.flex.USE_PREFFERED_SIZE
        }, controller.args[0], "flxDscription"), extendConfig({}, controller.args[1], "flxDscription"), extendConfig({}, controller.args[2], "flxDscription"));
        flxDscription.setDefaultUnit(kony.flex.DP);
        var lblDescription = new kony.ui.Label(extendConfig({
            "id": "lblDescription",
            "isVisible": true,
            "left": "0px",
            "skin": "sknlblLatoRegular484b5213px",
            "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmAlertsManagement.lblEditAlertDescription\")",
            "top": "18px",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 3
        }, controller.args[0], "lblDescription"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblDescription"), extendConfig({}, controller.args[2], "lblDescription"));
        var lblOptional = new kony.ui.Label(extendConfig({
            "id": "lblOptional",
            "isVisible": true,
            "left": "10px",
            "skin": "CopysknlblLato0d00f9bdc30cf4b",
            "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmPermissions.lblOptional\")",
            "top": "18px",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 3
        }, controller.args[0], "lblOptional"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblOptional"), extendConfig({}, controller.args[2], "lblOptional"));
        flxDscription.add(lblDescription, lblOptional);
        var txtareaDescription = new kony.ui.TextArea2(extendConfig({
            "autoCapitalize": constants.TEXTAREA_AUTO_CAPITALIZE_NONE,
            "focusSkin": "skntxtdescriptionHover",
            "height": "100dp",
            "id": "txtareaDescription",
            "isVisible": true,
            "keyBoardStyle": constants.TEXTAREA_KEY_BOARD_STYLE_DEFAULT,
            "left": "0dp",
            "maxTextLength": 70,
            "numberOfVisibleLines": 3,
            "skin": "skntxtAreaLato35475f14Px",
            "textInputMode": constants.TEXTAREA_INPUT_MODE_ANY,
            "top": "8dp",
            "width": "100%",
            "zIndex": 1
        }, controller.args[0], "txtareaDescription"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_TOP_LEFT,
            "padding": [1, 1, 1, 1],
            "paddingInPixel": false
        }, controller.args[1], "txtareaDescription"), extendConfig({
            "autoCorrect": false
        }, controller.args[2], "txtareaDescription"));
        flxRow2.add(lblName, txtfldName, lblNoNameError, flxDscription, txtareaDescription);
        var lbldescriptionSize = new kony.ui.Label(extendConfig({
            "id": "lbldescriptionSize",
            "isVisible": false,
            "right": "19px",
            "skin": "sknllbl485c75Lato13px",
            "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmUsers.lblEmailIdCount\")",
            "top": 152,
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 5
        }, controller.args[0], "lbldescriptionSize"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lbldescriptionSize"), extendConfig({}, controller.args[2], "lbldescriptionSize"));
        var lblNameSize = new kony.ui.Label(extendConfig({
            "id": "lblNameSize",
            "isVisible": false,
            "right": "20px",
            "skin": "sknllbl485c75Lato13px",
            "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmServiceManagement.lblServiceDisplayNameCount\")",
            "top": 72,
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 5
        }, controller.args[0], "lblNameSize"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, controller.args[1], "lblNameSize"), extendConfig({}, controller.args[2], "lblNameSize"));
        flxPopupHeader.add(lblPopUpMainMessage, flxPopUpClose, flxRow2, lbldescriptionSize, lblNameSize);
        var flxPopUpButtons = new kony.ui.FlexContainer(extendConfig({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "clipBounds": true,
            "height": "80px",
            "id": "flxPopUpButtons",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "left": "0%",
            "isModalContainer": false,
            "skin": "slFbox0d9c3974835234d",
            "top": "0px",
            "width": "100%",
            "zIndex": 1
        }, controller.args[0], "flxPopUpButtons"), extendConfig({}, controller.args[1], "flxPopUpButtons"), extendConfig({}, controller.args[2], "flxPopUpButtons"));
        flxPopUpButtons.setDefaultUnit(kony.flex.DP);
        var btnPopUpCancel = new kony.ui.Button(extendConfig({
            "centerY": "50%",
            "focusSkin": "sknBtnLatoRegulara5abc413pxKA",
            "height": "40px",
            "id": "btnPopUpCancel",
            "isVisible": true,
            "left": "20dp",
            "skin": "sknBtnLatoRegulara5abc413pxKA",
            "i18n_text": "kony.i18n.getLocalizedString(\"i18n.permission.CANCEL\")",
            "top": "0%",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 20
        }, controller.args[0], "btnPopUpCancel"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_CENTER,
            "displayText": true,
            "padding": [3, 0, 3, 0],
            "paddingInPixel": false
        }, controller.args[1], "btnPopUpCancel"), extendConfig({
            "hoverSkin": "sknbtnffffffLatoBolda5abc413Pxd7d8e01pxBorderRadius20px"
        }, controller.args[2], "btnPopUpCancel"));
        var btnPopUpDelete = new kony.ui.Button(extendConfig({
            "centerY": "50%",
            "focusSkin": "sknBtn003E75LatoRegular13pxFFFFFFRad20px",
            "height": "40px",
            "id": "btnPopUpDelete",
            "isVisible": true,
            "minWidth": "100px",
            "right": "20px",
            "skin": "sknBtn003E75LatoRegular13pxFFFFFFRad20px",
            "i18n_text": "kony.i18n.getLocalizedString(\"i18n.permission.SAVE\")",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 20
        }, controller.args[0], "btnPopUpDelete"), extendConfig({
            "contentAlignment": constants.CONTENT_ALIGN_CENTER,
            "displayText": true,
            "padding": [3, 0, 3, 0],
            "paddingInPixel": false
        }, controller.args[1], "btnPopUpDelete"), extendConfig({
            "hoverSkin": "sknBtn005198LatoRegular13pxFFFFFFRad20px"
        }, controller.args[2], "btnPopUpDelete"));
        flxPopUpButtons.add(btnPopUpCancel, btnPopUpDelete);
        flxPopUp.add(flxPopUpTopColor, flxPopupHeader, flxPopUpButtons);
        popUpSaveFilter.add(flxPopUp);
        return popUpSaveFilter;
    }
})