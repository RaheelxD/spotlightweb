define("segVariableReference", function() {
    return function(controller) {
        var segVariableReference = new kony.ui.FlexContainer({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "clipBounds": true,
            "height": "20px",
            "id": "segVariableReference",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "left": "0dp",
            "isModalContainer": false,
            "skin": "sknAlertsVariableReference",
            "top": "0dp",
            "width": "100%"
        }, {}, {});
        segVariableReference.setDefaultUnit(kony.flex.DP);
        var lbVariableReference = new kony.ui.Label({
            "centerY": "50%",
            "height": "20px",
            "id": "lbVariableReference",
            "isVisible": true,
            "left": "0px",
            "skin": "sknlblLatoBold35475f14px",
            "text": "Variable reference",
            "top": "5px",
            "width": "90%",
            "zIndex": 1
        }, {
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, {});
        segVariableReference.add(lbVariableReference);
        return segVariableReference;
    }
})