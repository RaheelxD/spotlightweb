define("flxSegViewFeatureOptions", function() {
    return function(controller) {
        var flxSegViewFeatureOptions = new kony.ui.FlexContainer({
            "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
            "clipBounds": true,
            "id": "flxSegViewFeatureOptions",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "left": "0dp",
            "isModalContainer": false,
            "skin": "slFbox",
            "top": "0dp",
            "width": "100%"
        }, {}, {});
        flxSegViewFeatureOptions.setDefaultUnit(kony.flex.DP);
        var lblOptionValue = new kony.ui.Label({
            "bottom": "15px",
            "id": "lblOptionValue",
            "isVisible": true,
            "left": "12px",
            "skin": "sknLbl485C75LatoRegular13Px",
            "text": "Opt-in",
            "top": "15px",
            "width": "160px",
            "zIndex": 1
        }, {
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, {});
        var lblOptionDescription = new kony.ui.Label({
            "bottom": "15px",
            "id": "lblOptionDescription",
            "isVisible": true,
            "left": "192px",
            "right": "12px",
            "skin": "sknLbl485C75LatoRegular13Px",
            "text": "I want the Bank to authorize and pay overdrafts on my transactions",
            "top": "15px",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, {
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, {});
        flxSegViewFeatureOptions.add(lblOptionValue, lblOptionDescription);
        return flxSegViewFeatureOptions;
    }
})