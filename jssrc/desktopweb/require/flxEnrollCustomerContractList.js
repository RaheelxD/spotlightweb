define("flxEnrollCustomerContractList", function() {
    return function(controller) {
        var flxEnrollCustomerContractList = new kony.ui.FlexContainer({
            "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
            "clipBounds": true,
            "id": "flxEnrollCustomerContractList",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "left": "0dp",
            "isModalContainer": false,
            "skin": "sknFlxFFFFFF100O",
            "top": "0dp",
            "width": "100%"
        }, {}, {});
        flxEnrollCustomerContractList.setDefaultUnit(kony.flex.DP);
        var flxCheckbox = new kony.ui.FlexContainer({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "clipBounds": true,
            "height": "15dp",
            "id": "flxCheckbox",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "left": "15dp",
            "isModalContainer": false,
            "skin": "slFbox",
            "top": "16dp",
            "width": "15dp"
        }, {}, {});
        flxCheckbox.setDefaultUnit(kony.flex.DP);
        var imgCheckbox = new kony.ui.Image2({
            "height": "100%",
            "id": "imgCheckbox",
            "isVisible": true,
            "left": "0",
            "skin": "slImage",
            "src": "checkboxnormal.png",
            "top": "0",
            "width": "100%"
        }, {
            "imageScaleMode": constants.IMAGE_SCALE_MODE_MAINTAIN_ASPECT_RATIO,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, {});
        flxCheckbox.add(imgCheckbox);
        var flxSectionCheckbox = new kony.ui.FlexContainer({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "clipBounds": true,
            "height": "15dp",
            "id": "flxSectionCheckbox",
            "isVisible": false,
            "layoutType": kony.flex.FREE_FORM,
            "left": "15dp",
            "isModalContainer": false,
            "skin": "slFbox",
            "top": "16dp",
            "width": "15dp"
        }, {}, {});
        flxSectionCheckbox.setDefaultUnit(kony.flex.DP);
        var imgSectionCheckbox = new kony.ui.Image2({
            "height": "100%",
            "id": "imgSectionCheckbox",
            "isVisible": true,
            "left": "0",
            "skin": "slImage",
            "src": "checkboxnormal.png",
            "top": "0",
            "width": "100%"
        }, {
            "imageScaleMode": constants.IMAGE_SCALE_MODE_MAINTAIN_ASPECT_RATIO,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, {});
        flxSectionCheckbox.add(imgSectionCheckbox);
        var lblContractName = new kony.ui.Label({
            "bottom": "15dp",
            "id": "lblContractName",
            "isVisible": true,
            "left": "40dp",
            "skin": "sknLbl485C75LatoRegular13Px",
            "text": "Label",
            "top": "15dp",
            "width": "30%"
        }, {
            "contentAlignment": constants.CONTENT_ALIGN_TOP_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, {});
        var lblContractId = new kony.ui.Label({
            "bottom": "15dp",
            "id": "lblContractId",
            "isVisible": true,
            "left": "35%",
            "skin": "sknLbl485C75LatoRegular13Px",
            "text": "Label",
            "top": "15dp",
            "width": "28%"
        }, {
            "contentAlignment": constants.CONTENT_ALIGN_TOP_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, {});
        var lblContractAddress = new kony.ui.Label({
            "bottom": "15dp",
            "id": "lblContractAddress",
            "isVisible": true,
            "left": "63%",
            "right": "20dp",
            "skin": "sknLbl485C75LatoRegular13Px",
            "text": "Label",
            "top": "15dp",
            "width": kony.flex.USE_PREFFERED_SIZE
        }, {
            "contentAlignment": constants.CONTENT_ALIGN_TOP_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, {});
        var lblSeperator = new kony.ui.Label({
            "bottom": "0dp",
            "height": "1dp",
            "id": "lblSeperator",
            "isVisible": true,
            "left": "0dp",
            "skin": "sknlblSeperatorD7D9E0",
            "text": "Label",
            "width": "100%"
        }, {
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, {});
        flxEnrollCustomerContractList.add(flxCheckbox, flxSectionCheckbox, lblContractName, lblContractId, lblContractAddress, lblSeperator);
        return flxEnrollCustomerContractList;
    }
})