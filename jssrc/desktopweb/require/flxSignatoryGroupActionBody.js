define("flxSignatoryGroupActionBody", function() {
    return function(controller) {
        var flxSignatoryGroupActionBody = new kony.ui.FlexContainer({
            "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
            "clipBounds": true,
            "id": "flxSignatoryGroupActionBody",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "left": "0dp",
            "isModalContainer": false,
            "skin": "sknFlxFFFFFF1000",
            "top": "0dp",
            "width": "100%"
        }, {}, {});
        flxSignatoryGroupActionBody.setDefaultUnit(kony.flex.DP);
        var flxViewAccounts = new kony.ui.FlexContainer({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "clipBounds": true,
            "height": "40px",
            "id": "flxViewAccounts",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "left": "10px",
            "isModalContainer": false,
            "right": "10px",
            "skin": "slFbox",
            "top": "0px"
        }, {}, {});
        flxViewAccounts.setDefaultUnit(kony.flex.DP);
        var lblAccountNameHeader = new kony.ui.Label({
            "centerY": "50%",
            "id": "lblAccountNameHeader",
            "isVisible": true,
            "left": "30px",
            "skin": "sknlblLato696c7311px",
            "text": "ACCOUNT NAME",
            "top": 0,
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, {
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, {});
        var lblAccountNumberHeader = new kony.ui.Label({
            "centerY": "50%",
            "id": "lblAccountNumberHeader",
            "isVisible": true,
            "left": "30%",
            "skin": "sknlblLato696c7311px",
            "text": "ACCOUNT NUMBER",
            "top": 0,
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, {
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, {});
        var lblAccountTypeHeader = new kony.ui.Label({
            "centerY": "50%",
            "id": "lblAccountTypeHeader",
            "isVisible": true,
            "left": "60%",
            "skin": "sknlblLato696c7311px",
            "text": "ACCOUNT TYPE",
            "top": 0,
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, {
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, {});
        flxViewAccounts.add(lblAccountNameHeader, lblAccountNumberHeader, lblAccountTypeHeader);
        flxSignatoryGroupActionBody.add(flxViewAccounts);
        return flxSignatoryGroupActionBody;
    }
})