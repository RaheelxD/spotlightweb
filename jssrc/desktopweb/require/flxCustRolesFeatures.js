define("flxCustRolesFeatures", function() {
    return function(controller) {
        var flxCustRolesFeatures = new kony.ui.FlexContainer({
            "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
            "clipBounds": true,
            "id": "flxCustRolesFeatures",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "left": "0dp",
            "isModalContainer": false,
            "skin": "slFbox",
            "top": "0dp",
            "width": "100%"
        }, {}, {});
        flxCustRolesFeatures.setDefaultUnit(kony.flex.DP);
        var flxFeatureNameContainer = new kony.ui.FlexContainer({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "clipBounds": true,
            "height": "60dp",
            "id": "flxFeatureNameContainer",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "left": "12dp",
            "isModalContainer": false,
            "right": "29px",
            "skin": "sknflxffffffBorderE1E5EERadius3px",
            "top": "12dp",
            "zIndex": 3
        }, {}, {
            "hoverSkin": "sknflxffffffBorderE1E5EERadius3pxPointer"
        });
        flxFeatureNameContainer.setDefaultUnit(kony.flex.DP);
        var flxFeatureCheckbox = new kony.ui.FlexContainer({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "clipBounds": true,
            "height": "20dp",
            "id": "flxFeatureCheckbox",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "left": "8dp",
            "isModalContainer": false,
            "skin": "slFbox",
            "top": "7dp",
            "width": "20dp",
            "zIndex": 1
        }, {}, {});
        flxFeatureCheckbox.setDefaultUnit(kony.flex.DP);
        var imgFeatureCheckbox = new kony.ui.Image2({
            "centerX": "50%",
            "centerY": "50%",
            "height": "15dp",
            "id": "imgFeatureCheckbox",
            "isVisible": true,
            "skin": "slImage",
            "src": "checkboxnormal.png",
            "width": "15dp",
            "zIndex": 1
        }, {
            "imageScaleMode": constants.IMAGE_SCALE_MODE_MAINTAIN_ASPECT_RATIO,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, {});
        flxFeatureCheckbox.add(imgFeatureCheckbox);
        var lblFeatureName = new kony.ui.Label({
            "id": "lblFeatureName",
            "isVisible": true,
            "left": "38dp",
            "skin": "sknLbl192B45LatoRegular14px",
            "text": "Bill Payments",
            "top": "8dp",
            "width": "75%",
            "zIndex": 1
        }, {
            "contentAlignment": constants.CONTENT_ALIGN_TOP_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, {});
        var flxSelectedArrow = new kony.ui.FlexContainer({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "centerY": "50%",
            "clipBounds": true,
            "height": "20dp",
            "id": "flxSelectedArrow",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "isModalContainer": false,
            "right": "10dp",
            "skin": "slFbox",
            "top": "9dp",
            "width": "20dp",
            "zIndex": 1
        }, {}, {});
        flxSelectedArrow.setDefaultUnit(kony.flex.DP);
        var lblIconSelectedArrow = new kony.ui.Label({
            "centerX": "50%",
            "centerY": "50%",
            "height": "15dp",
            "id": "lblIconSelectedArrow",
            "isVisible": true,
            "skin": "sknicon15pxBlack",
            "text": "",
            "width": "15dp",
            "zIndex": 1
        }, {
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, {});
        flxSelectedArrow.add(lblIconSelectedArrow);
        var flxFeatureStatus = new kony.ui.FlexContainer({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "clipBounds": true,
            "height": "20dp",
            "id": "flxFeatureStatus",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "left": "38dp",
            "isModalContainer": false,
            "skin": "slFbox",
            "top": "30dp",
            "width": "100dp"
        }, {}, {});
        flxFeatureStatus.setDefaultUnit(kony.flex.DP);
        var lblIconStatus = new kony.ui.Label({
            "centerY": "52%",
            "height": "12dp",
            "id": "lblIconStatus",
            "isVisible": true,
            "left": "0dp",
            "skin": "sknFontIconActivate",
            "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmAlertsManagement.lblViewAlertStatusKey\")",
            "width": "12dp",
            "zIndex": 1
        }, {
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, {});
        var lblFeatureStatusValue = new kony.ui.Label({
            "centerY": "50%",
            "id": "lblFeatureStatusValue",
            "isVisible": true,
            "left": 15,
            "skin": "sknLbl485C75LatoRegular13Px",
            "text": "Active",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, {
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, {});
        flxFeatureStatus.add(lblIconStatus, lblFeatureStatusValue);
        flxFeatureNameContainer.add(flxFeatureCheckbox, lblFeatureName, flxSelectedArrow, flxFeatureStatus);
        flxCustRolesFeatures.add(flxFeatureNameContainer);
        return flxCustRolesFeatures;
    }
})