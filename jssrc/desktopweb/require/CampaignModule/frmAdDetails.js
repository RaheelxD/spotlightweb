define("CampaignModule/frmAdDetails", function() {
    return function(controller) {
        function addWidgetsfrmAdDetails() {
            this.setDefaultUnit(kony.flex.DP);
            var flxLeftPannel = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "100%",
                "id": "flxLeftPannel",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "masterType": constants.MASTER_TYPE_USERWIDGET,
                "isModalContainer": false,
                "skin": "sknLeftPannel0bb79e4bcca8e45",
                "top": "0dp",
                "width": "305dp",
                "zIndex": 5
            }, {}, {});
            flxLeftPannel.setDefaultUnit(kony.flex.DP);
            var leftMenuNew = new com.adminConsole.navigation.leftMenuNew({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "height": "100%",
                "id": "leftMenuNew",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "masterType": constants.MASTER_TYPE_DEFAULT,
                "isModalContainer": false,
                "skin": "sknFlxBg003E75Op100",
                "top": "0dp",
                "width": "100%",
                "overrides": {
                    "brandLogo": {
                        "src": "infinity_dbx_c360_logo_white_2x.png"
                    },
                    "flxScrollMenu": {
                        "bottom": "40dp",
                        "left": "0dp",
                        "top": "80dp"
                    },
                    "imgBottomLogo": {
                        "src": "logo_white_2x.png"
                    },
                    "leftMenuNew": {
                        "bottom": "viz.val_cleared",
                        "centerX": "viz.val_cleared",
                        "centerY": "viz.val_cleared",
                        "height": "100%",
                        "left": "0dp",
                        "maxHeight": "viz.val_cleared",
                        "maxWidth": "viz.val_cleared",
                        "minHeight": "viz.val_cleared",
                        "minWidth": "viz.val_cleared",
                        "right": "viz.val_cleared",
                        "top": "0dp",
                        "width": "100%"
                    }
                }
            }, {
                "overrides": {}
            }, {
                "overrides": {}
            });
            flxLeftPannel.add(leftMenuNew);
            var flxRightPannel = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "100%",
                "id": "flxRightPannel",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "305px",
                "isModalContainer": false,
                "right": "0dp",
                "skin": "slFbox0b7aaa2e3bfa340",
                "zIndex": 5
            }, {}, {});
            flxRightPannel.setDefaultUnit(kony.flex.DP);
            var flxMainHeader = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "106dp",
                "id": "flxMainHeader",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0dp",
                "width": "100%",
                "zIndex": 1
            }, {}, {});
            flxMainHeader.setDefaultUnit(kony.flex.DP);
            var mainHeader = new com.adminConsole.header.mainHeader({
                "height": "106px",
                "id": "mainHeader",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "masterType": constants.MASTER_TYPE_DEFAULT,
                "isModalContainer": false,
                "skin": "CopyslFbox1",
                "width": "100%",
                "overrides": {
                    "btnAddNewOption": {
                        "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmCSRController.TEMPLATES\")",
                        "right": "212px"
                    },
                    "btnDropdownList": {
                        "text": "CREATE NEW MESSAGE"
                    },
                    "flxButtons": {
                        "isVisible": false
                    },
                    "flxHeaderSeperator": {
                        "isVisible": false
                    },
                    "flxHeaderUserOptions": {
                        "isVisible": false
                    },
                    "flxMainHeader": {
                        "left": undefined,
                        "top": undefined
                    },
                    "imgLogout": {
                        "right": "0px",
                        "src": "img_logout.png"
                    },
                    "lblHeading": {
                        "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmAdManagement.AdCampaigns\")"
                    },
                    "lblUserName": {
                        "right": "25px",
                        "text": "Preetish",
                        "top": "viz.val_cleared"
                    },
                    "mainHeader": {
                        "left": undefined,
                        "top": undefined
                    }
                }
            }, {
                "overrides": {}
            }, {
                "overrides": {}
            });
            flxMainHeader.add(mainHeader);
            var flxHeaderDropdown = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                "clipBounds": true,
                "id": "flxHeaderDropdown",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "isModalContainer": false,
                "right": "35dp",
                "skin": "slFbox",
                "top": "20dp",
                "width": "400dp",
                "zIndex": 5
            }, {}, {});
            flxHeaderDropdown.setDefaultUnit(kony.flex.DP);
            var dropdownMainHeader = new com.adminConsole.common.dropdownMainHeader({
                "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                "id": "dropdownMainHeader",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "masterType": constants.MASTER_TYPE_DEFAULT,
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0dp",
                "width": "100%",
                "overrides": {
                    "flxDropdown": {
                        "isVisible": false
                    },
                    "imgUpArrow": {
                        "src": "uparrow_2x.png"
                    }
                }
            }, {
                "overrides": {}
            }, {
                "overrides": {}
            });
            flxHeaderDropdown.add(dropdownMainHeader);
            var flxBreadCrumbs = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "30px",
                "id": "flxBreadCrumbs",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "masterType": constants.MASTER_TYPE_USERWIDGET,
                "isModalContainer": false,
                "skin": "sknflxf5f6f8Op100",
                "top": "95px",
                "width": "100%",
                "zIndex": 1
            }, {}, {});
            flxBreadCrumbs.setDefaultUnit(kony.flex.DP);
            var breadcrumbs = new com.adminConsole.common.breadcrumbs({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "height": "20px",
                "id": "breadcrumbs",
                "isVisible": true,
                "layoutType": kony.flex.FLOW_HORIZONTAL,
                "left": "0dp",
                "masterType": constants.MASTER_TYPE_DEFAULT,
                "isModalContainer": false,
                "skin": "sknflxf5f6f8Op100",
                "top": "1px",
                "width": "100%",
                "overrides": {
                    "breadcrumbs": {
                        "bottom": "viz.val_cleared",
                        "top": "1px",
                        "width": "100%"
                    },
                    "btnBackToMain": {
                        "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmAdManagement.AdCampaignListCAPS\")"
                    },
                    "imgBreadcrumbsDown": {
                        "src": "img_down_arrow.png"
                    },
                    "imgBreadcrumbsRight": {
                        "left": "10dp",
                        "right": 10,
                        "src": "img_breadcrumb_arrow.png"
                    },
                    "imgBreadcrumbsRight2": {
                        "left": "10dp",
                        "right": 10,
                        "src": "img_breadcrumb_arrow.png"
                    },
                    "imgBreadcrumbsRight3": {
                        "src": "img_breadcrumb_arrow.png"
                    }
                }
            }, {
                "overrides": {}
            }, {
                "overrides": {}
            });
            var flxHeaderSeperator = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "bottom": "1px",
                "clipBounds": true,
                "height": "1px",
                "id": "flxHeaderSeperator",
                "isVisible": false,
                "layoutType": kony.flex.FREE_FORM,
                "left": "35dp",
                "isModalContainer": false,
                "right": "35dp",
                "skin": "sknflx115678",
                "zIndex": 1
            }, {}, {});
            flxHeaderSeperator.setDefaultUnit(kony.flex.DP);
            flxHeaderSeperator.add();
            flxBreadCrumbs.add(breadcrumbs, flxHeaderSeperator);
            var flxContainer = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "83%",
                "id": "flxContainer",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "35px",
                "isModalContainer": false,
                "right": "35px",
                "skin": "slFbox",
                "top": "130px",
                "zIndex": 1
            }, {}, {});
            flxContainer.setDefaultUnit(kony.flex.DP);
            var flxOptions = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "100%",
                "id": "flxOptions",
                "isVisible": true,
                "layoutType": kony.flex.FLOW_VERTICAL,
                "left": "0px",
                "isModalContainer": false,
                "skin": "sknFlxBorE1E5ED1pxKA",
                "top": "0%",
                "width": "230dp",
                "zIndex": 1
            }, {}, {});
            flxOptions.setDefaultUnit(kony.flex.DP);
            var verticalTabs1 = new com.adminConsole.adManagement.verticalTabs({
                "height": "100%",
                "id": "verticalTabs1",
                "isVisible": true,
                "layoutType": kony.flex.FLOW_VERTICAL,
                "left": "0dp",
                "masterType": constants.MASTER_TYPE_DEFAULT,
                "isModalContainer": false,
                "skin": "sknflxf9f9f9NoBorder",
                "top": "0dp",
                "width": "100%",
                "overrides": {
                    "btnSubOption31": {
                        "i18n_text": "kony.i18n.getLocalizedString(\"i18n.leftmenu.segments\")"
                    },
                    "btnSubOption32": {
                        "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmAdManagement.events\")"
                    },
                    "flxImgSubArrow": {
                        "isVisible": true
                    },
                    "flxOption2": {
                        "isVisible": true
                    },
                    "imgSelected1": {
                        "src": "right_arrow2x.png"
                    },
                    "imgSelected2": {
                        "src": "right_arrow2x.png"
                    },
                    "imgSelected3": {
                        "src": "right_arrow2x.png"
                    },
                    "lblSelectedSub1": {
                        "isVisible": false
                    },
                    "verticalTabs": {
                        "left": "0dp",
                        "top": "0dp"
                    }
                }
            }, {
                "overrides": {}
            }, {
                "overrides": {}
            });
            flxOptions.add(verticalTabs1);
            var flxDetails = new kony.ui.FlexContainer({
                "clipBounds": true,
                "height": "100%",
                "id": "flxDetails",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "230dp",
                "isModalContainer": false,
                "right": "0dp",
                "skin": "slFbox",
                "top": "0dp",
                "zIndex": 1
            }, {}, {});
            flxDetails.setDefaultUnit(kony.flex.DP);
            var flxContent = new kony.ui.FlexScrollContainer({
                "allowHorizontalBounce": false,
                "allowVerticalBounce": true,
                "bounces": true,
                "clipBounds": true,
                "enableScrolling": true,
                "height": "400dp",
                "horizontalScrollIndicator": true,
                "id": "flxContent",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "pagingEnabled": false,
                "scrollDirection": kony.flex.SCROLL_VERTICAL,
                "skin": "sknflxScrle4e6ec1pxfff",
                "top": "0dp",
                "verticalScrollIndicator": true,
                "width": "100%",
                "zIndex": 10
            }, {}, {});
            flxContent.setDefaultUnit(kony.flex.DP);
            var flxBasicDetails = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                "clipBounds": true,
                "id": "flxBasicDetails",
                "isVisible": true,
                "layoutType": kony.flex.FLOW_VERTICAL,
                "left": "20dp",
                "isModalContainer": false,
                "right": 20,
                "skin": "slFbox",
                "top": "20dp",
                "width": kony.flex.USE_PREFFERED_SIZE
            }, {}, {});
            flxBasicDetails.setDefaultUnit(kony.flex.DP);
            var flxAdName = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                "clipBounds": true,
                "id": "flxAdName",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "20dp",
                "width": "100%"
            }, {}, {});
            flxAdName.setDefaultUnit(kony.flex.DP);
            var lblAdName = new kony.ui.Label({
                "id": "lblAdName",
                "isVisible": true,
                "left": "0dp",
                "skin": "sknLblLato485c7513px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmAdManagement.AdName\")",
                "top": "0dp",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var lblAdNameSize = new kony.ui.Label({
                "id": "lblAdNameSize",
                "isVisible": false,
                "right": "0dp",
                "skin": "sknLblLato485c7513px",
                "text": "0/50",
                "top": "0dp",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxAdName.add(lblAdName, lblAdNameSize);
            var txtAdName = new kony.ui.TextBox2({
                "autoCapitalize": constants.TEXTBOX_AUTO_CAPITALIZE_NONE,
                "focusSkin": "skntxtbxDetailsFocus14px",
                "height": "40dp",
                "id": "txtAdName",
                "isVisible": true,
                "keyBoardStyle": constants.TEXTBOX_KEY_BOARD_STYLE_DEFAULT,
                "left": "0",
                "maxTextLength": 50,
                "i18n_placeholder": "kony.i18n.getLocalizedString(\"i18n.frmAdManagement.AdName\")",
                "secureTextEntry": false,
                "skin": "txtD7d9e0",
                "textInputMode": constants.TEXTBOX_INPUT_MODE_ANY,
                "top": "15dp",
                "width": "100%",
                "zIndex": 1
            }, {
                "containerHeightMode": constants.TEXTBOX_FONT_METRICS_DRIVEN_HEIGHT,
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [2, 0, 0, 0],
                "paddingInPixel": false
            }, {
                "autoCorrect": false,
                "placeholderSkin": "sknTextGreyb2bdcd"
            });
            var AdNameError = new com.adminConsole.loans.applications.errorMsg({
                "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                "id": "AdNameError",
                "isVisible": false,
                "layoutType": kony.flex.FLOW_HORIZONTAL,
                "left": "0dp",
                "masterType": constants.MASTER_TYPE_DEFAULT,
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "10dp",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "overrides": {
                    "errorMsg": {
                        "isVisible": false
                    },
                    "lblErrorText": {
                        "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmAdManagement.ErrorAdName\")"
                    }
                }
            }, {
                "overrides": {}
            }, {
                "overrides": {}
            });
            var flxDescription = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                "clipBounds": true,
                "id": "flxDescription",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "30dp",
                "width": "100%"
            }, {}, {});
            flxDescription.setDefaultUnit(kony.flex.DP);
            var lblDescription = new kony.ui.Label({
                "id": "lblDescription",
                "isVisible": true,
                "left": "0dp",
                "skin": "sknLblLato485c7513px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmAdManagement.Description\")",
                "top": "0dp",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var lblDescriptionSize = new kony.ui.Label({
                "id": "lblDescriptionSize",
                "isVisible": false,
                "right": "0dp",
                "skin": "sknLblLato485c7513px",
                "text": "0/150",
                "top": "0dp",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxDescription.add(lblDescription, lblDescriptionSize);
            var txtDescription = new kony.ui.TextArea2({
                "autoCapitalize": constants.TEXTAREA_AUTO_CAPITALIZE_NONE,
                "focusSkin": "sknFocus",
                "height": "70dp",
                "id": "txtDescription",
                "isVisible": true,
                "keyBoardStyle": constants.TEXTAREA_KEY_BOARD_STYLE_DEFAULT,
                "left": "0dp",
                "maxTextLength": 150,
                "numberOfVisibleLines": 3,
                "i18n_placeholder": "kony.i18n.getLocalizedString(\"i18n.frmAdManagement.Description\")",
                "skin": "skntxtAread7d9e0",
                "textInputMode": constants.TEXTAREA_INPUT_MODE_ANY,
                "top": "15dp",
                "width": "100%",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_TOP_LEFT,
                "padding": [2, 2, 2, 2],
                "paddingInPixel": false
            }, {
                "autoCorrect": false,
                "placeholderSkin": "defTextAreaFocus"
            });
            var AdDescriptionError = new com.adminConsole.loans.applications.errorMsg({
                "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                "id": "AdDescriptionError",
                "isVisible": false,
                "layoutType": kony.flex.FLOW_HORIZONTAL,
                "left": "0dp",
                "masterType": constants.MASTER_TYPE_DEFAULT,
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "10dp",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "overrides": {
                    "errorMsg": {
                        "isVisible": false
                    },
                    "lblErrorText": {
                        "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmAdManagement.ErrorAdDescription\")"
                    }
                }
            }, {
                "overrides": {}
            }, {
                "overrides": {}
            });
            var flxAdType = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "75dp",
                "id": "flxAdType",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "30dp",
                "width": "100%",
                "zIndex": 10
            }, {}, {});
            flxAdType.setDefaultUnit(kony.flex.DP);
            var lblAdTypeHeader = new kony.ui.Label({
                "id": "lblAdTypeHeader",
                "isVisible": true,
                "left": "0dp",
                "skin": "sknLbl192B45LatoRegular14px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmAdManagement.adType\")",
                "top": "0dp",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var flxAdTypeSelect = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                "clipBounds": true,
                "id": "flxAdTypeSelect",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "30dp",
                "width": "100%",
                "zIndex": 1
            }, {}, {});
            flxAdTypeSelect.setDefaultUnit(kony.flex.DP);
            var customRadioButtonGroup = new com.adminConsole.common.customRadioButtonGroup({
                "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                "id": "customRadioButtonGroup",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "masterType": constants.MASTER_TYPE_DEFAULT,
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0dp",
                "width": "100%",
                "overrides": {
                    "flxRadioButton1": {
                        "right": "viz.val_cleared",
                        "width": "110dp"
                    },
                    "flxRadioButton2": {
                        "left": "0dp",
                        "width": "130dp"
                    },
                    "flxRadioButton3": {
                        "left": "0dp"
                    },
                    "imgRadioButton1": {
                        "src": "radionormal_1x.png"
                    },
                    "imgRadioButton2": {
                        "src": "radionormal_1x.png"
                    },
                    "imgRadioButton3": {
                        "src": "radionormal_1x.png"
                    },
                    "lblRadioButtonValue1": {
                        "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmAdManagement.marketing\")",
                        "right": "viz.val_cleared",
                        "width": kony.flex.USE_PREFFERED_SIZE
                    },
                    "lblRadioButtonValue2": {
                        "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmAdManagement.informational\")",
                        "right": "viz.val_cleared",
                        "width": kony.flex.USE_PREFFERED_SIZE
                    },
                    "lblRadioButtonValue3": {
                        "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmAdManagement.regulatory\")",
                        "right": "viz.val_cleared",
                        "width": kony.flex.USE_PREFFERED_SIZE
                    }
                }
            }, {
                "overrides": {}
            }, {
                "overrides": {}
            });
            flxAdTypeSelect.add(customRadioButtonGroup);
            var AdTypeError = new com.adminConsole.loans.applications.errorMsg({
                "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                "id": "AdTypeError",
                "isVisible": false,
                "layoutType": kony.flex.FLOW_HORIZONTAL,
                "left": "0dp",
                "masterType": constants.MASTER_TYPE_DEFAULT,
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "60dp",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "overrides": {
                    "errorMsg": {
                        "bottom": "viz.val_cleared",
                        "isVisible": false,
                        "left": "0dp",
                        "top": "60dp"
                    },
                    "lblErrorText": {
                        "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmAdManagement.errorMsg_SelectAdType\")"
                    }
                }
            }, {
                "overrides": {}
            }, {
                "overrides": {}
            });
            flxAdType.add(lblAdTypeHeader, flxAdTypeSelect, AdTypeError);
            var flxProduct = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                "bottom": "20dp",
                "clipBounds": true,
                "id": "flxProduct",
                "isVisible": false,
                "layoutType": kony.flex.FLOW_HORIZONTAL,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "10dp",
                "width": "100%",
                "zIndex": 1
            }, {}, {});
            flxProduct.setDefaultUnit(kony.flex.DP);
            var flxProductLIne = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                "bottom": "30dp",
                "clipBounds": true,
                "id": "flxProductLIne",
                "isVisible": false,
                "layoutType": kony.flex.FLOW_VERTICAL,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0dp",
                "width": "32%",
                "zIndex": 1
            }, {}, {});
            flxProductLIne.setDefaultUnit(kony.flex.DP);
            var lblProductLineHeader = new kony.ui.Label({
                "id": "lblProductLineHeader",
                "isVisible": true,
                "left": "0dp",
                "skin": "sknLbl485C75LatoRegular13Px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmAdManagement.productLine\")",
                "top": "0dp",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var lbxProductLine = new kony.ui.ListBox({
                "focusSkin": "defListBoxFocus",
                "height": "40dp",
                "id": "lbxProductLine",
                "isVisible": true,
                "left": "0dp",
                "masterData": [
                    ["lb1", "Placeholder One"],
                    ["lb2", "Placeholder Two"],
                    ["lb3", "Placeholder Three"]
                ],
                "skin": "sknlbxError",
                "top": "15dp",
                "width": "100%",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [3, 0, 0, 0],
                "paddingInPixel": false
            }, {
                "multiSelect": false
            });
            var productLineError = new com.adminConsole.loans.applications.errorMsg({
                "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                "id": "productLineError",
                "isVisible": false,
                "layoutType": kony.flex.FLOW_HORIZONTAL,
                "left": "0dp",
                "masterType": constants.MASTER_TYPE_DEFAULT,
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "10dp",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "overrides": {
                    "errorMsg": {
                        "bottom": "viz.val_cleared",
                        "isVisible": false,
                        "left": "0dp",
                        "top": "10dp"
                    },
                    "lblErrorText": {
                        "text": "Product line must be selected."
                    }
                }
            }, {
                "overrides": {}
            }, {
                "overrides": {}
            });
            flxProductLIne.add(lblProductLineHeader, lbxProductLine, productLineError);
            var flxProductGroup = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                "clipBounds": true,
                "id": "flxProductGroup",
                "isVisible": true,
                "layoutType": kony.flex.FLOW_VERTICAL,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0dp",
                "width": "32%",
                "zIndex": 1
            }, {}, {});
            flxProductGroup.setDefaultUnit(kony.flex.DP);
            var lblProductGroupHeader = new kony.ui.Label({
                "id": "lblProductGroupHeader",
                "isVisible": true,
                "left": "0dp",
                "skin": "sknLbl485C75LatoRegular13Px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmAdManagement.productGroup\")",
                "top": "0dp",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var lbxProductGroup = new kony.ui.ListBox({
                "focusSkin": "defListBoxFocus",
                "height": "40dp",
                "id": "lbxProductGroup",
                "isVisible": true,
                "left": "0dp",
                "masterData": [
                    ["lb1", "Placeholder One"],
                    ["lb2", "Placeholder Two"],
                    ["lb3", "Placeholder Three"]
                ],
                "skin": "sknlbxBgffffffBorderc1c9ceRadius3Px",
                "top": "15dp",
                "width": "100%",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [3, 0, 0, 0],
                "paddingInPixel": false
            }, {
                "multiSelect": false
            });
            var productGroupError = new com.adminConsole.loans.applications.errorMsg({
                "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                "id": "productGroupError",
                "isVisible": false,
                "layoutType": kony.flex.FLOW_HORIZONTAL,
                "left": "0dp",
                "masterType": constants.MASTER_TYPE_DEFAULT,
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "10dp",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "overrides": {
                    "errorMsg": {
                        "bottom": "viz.val_cleared",
                        "isVisible": false,
                        "left": "0dp",
                        "top": "10dp"
                    },
                    "lblErrorText": {
                        "text": "Product group must be selected."
                    }
                }
            }, {
                "overrides": {}
            }, {
                "overrides": {}
            });
            flxProductGroup.add(lblProductGroupHeader, lbxProductGroup, productGroupError);
            var flxProductName = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                "clipBounds": true,
                "id": "flxProductName",
                "isVisible": true,
                "layoutType": kony.flex.FLOW_VERTICAL,
                "left": "15dp",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0dp",
                "width": "32%",
                "zIndex": 1
            }, {}, {});
            flxProductName.setDefaultUnit(kony.flex.DP);
            var lblProductName = new kony.ui.Label({
                "id": "lblProductName",
                "isVisible": true,
                "left": "0dp",
                "skin": "sknLbl485C75LatoRegular13Px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmAdManagement.productName\")",
                "top": "0dp",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var lbxProductName = new kony.ui.ListBox({
                "focusSkin": "defListBoxFocus",
                "height": "40dp",
                "id": "lbxProductName",
                "isVisible": true,
                "left": "0dp",
                "masterData": [
                    ["lb1", "Placeholder One"],
                    ["lb2", "Placeholder Two"],
                    ["lb3", "Placeholder Three"]
                ],
                "skin": "sknLstBxBre1e5edBgf5f6f813pxDisableHover",
                "top": "15dp",
                "width": "100%",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [3, 0, 0, 0],
                "paddingInPixel": false
            }, {
                "multiSelect": false
            });
            var productNameError = new com.adminConsole.loans.applications.errorMsg({
                "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                "id": "productNameError",
                "isVisible": false,
                "layoutType": kony.flex.FLOW_HORIZONTAL,
                "left": "0dp",
                "masterType": constants.MASTER_TYPE_DEFAULT,
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "10dp",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "overrides": {
                    "errorMsg": {
                        "bottom": "viz.val_cleared",
                        "isVisible": false,
                        "left": "0dp",
                        "top": "10dp"
                    },
                    "lblErrorText": {
                        "isVisible": true,
                        "text": "Product name must be selected."
                    }
                }
            }, {
                "overrides": {}
            }, {
                "overrides": {}
            });
            flxProductName.add(lblProductName, lbxProductName, productNameError);
            flxProduct.add(flxProductLIne, flxProductGroup, flxProductName);
            var flxChannels = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "75dp",
                "id": "flxChannels",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "10dp",
                "width": "100%",
                "zIndex": 1
            }, {}, {});
            flxChannels.setDefaultUnit(kony.flex.DP);
            var lblChannels = new kony.ui.Label({
                "id": "lblChannels",
                "isVisible": true,
                "left": "0dp",
                "skin": "sknLbl192B45LatoRegular14px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmAdManagement.channels\")",
                "top": "0dp",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var flxChannelsOptions = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                "clipBounds": true,
                "id": "flxChannelsOptions",
                "isVisible": true,
                "layoutType": kony.flex.FLOW_HORIZONTAL,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "30dp",
                "width": "100%"
            }, {}, {});
            flxChannelsOptions.setDefaultUnit(kony.flex.DP);
            var flxSmsChannelOption = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                "clipBounds": true,
                "id": "flxSmsChannelOption",
                "isVisible": true,
                "layoutType": kony.flex.FLOW_HORIZONTAL,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0dp",
                "width": "62dp",
                "zIndex": 1
            }, {}, {});
            flxSmsChannelOption.setDefaultUnit(kony.flex.DP);
            var fontIconSmsChannelSelectOption = new kony.ui.Label({
                "id": "fontIconSmsChannelSelectOption",
                "isVisible": true,
                "left": "0dp",
                "skin": "sknFontIconCheckBoxSelected",
                "text": "",
                "top": "0dp",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var lblSmsChannel = new kony.ui.Label({
                "id": "lblSmsChannel",
                "isVisible": true,
                "left": "7dp",
                "skin": "sknLbl485C75LatoRegular13Px",
                "text": "SMS",
                "top": "5dp",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var fontIconSmsChannelInfo = new kony.ui.Label({
                "id": "fontIconSmsChannelInfo",
                "isVisible": true,
                "left": "4dp",
                "skin": "sknfontIcon33333316px",
                "text": "",
                "top": "5dp",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxSmsChannelOption.add(fontIconSmsChannelSelectOption, lblSmsChannel, fontIconSmsChannelInfo);
            var flxEmailChannelOption = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                "clipBounds": true,
                "id": "flxEmailChannelOption",
                "isVisible": true,
                "layoutType": kony.flex.FLOW_HORIZONTAL,
                "left": "3%",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0dp",
                "width": "67dp",
                "zIndex": 1
            }, {}, {});
            flxEmailChannelOption.setDefaultUnit(kony.flex.DP);
            var fontIconEmailChannelSelectOption = new kony.ui.Label({
                "id": "fontIconEmailChannelSelectOption",
                "isVisible": true,
                "left": "0dp",
                "skin": "sknFontIconCheckBoxSelected",
                "text": "",
                "top": "0dp",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var lblEmailChannel = new kony.ui.Label({
                "id": "lblEmailChannel",
                "isVisible": true,
                "left": "7dp",
                "skin": "sknLbl485C75LatoRegular13Px",
                "text": "Email",
                "top": "5dp",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var fontIconEmailChannelInfo = new kony.ui.Label({
                "id": "fontIconEmailChannelInfo",
                "isVisible": true,
                "left": "5dp",
                "skin": "sknfontIcon33333316px",
                "text": "",
                "top": "5dp",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxEmailChannelOption.add(fontIconEmailChannelSelectOption, lblEmailChannel, fontIconEmailChannelInfo);
            var flxPushNotificationChannelOption = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                "clipBounds": true,
                "id": "flxPushNotificationChannelOption",
                "isVisible": true,
                "layoutType": kony.flex.FLOW_HORIZONTAL,
                "left": "3%",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0dp",
                "width": "140dp",
                "zIndex": 1
            }, {}, {});
            flxPushNotificationChannelOption.setDefaultUnit(kony.flex.DP);
            var fontIconPushNotificationChannelSelectOption = new kony.ui.Label({
                "id": "fontIconPushNotificationChannelSelectOption",
                "isVisible": true,
                "left": "0dp",
                "skin": "sknFontIconCheckBoxSelected",
                "text": "",
                "top": "0dp",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var lblPushNotificationChannel = new kony.ui.Label({
                "id": "lblPushNotificationChannel",
                "isVisible": true,
                "left": "7dp",
                "skin": "sknLbl485C75LatoRegular13Px",
                "text": "Push Notifications",
                "top": "5dp",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var fontIconPushNotificationChannelInfo = new kony.ui.Label({
                "id": "fontIconPushNotificationChannelInfo",
                "isVisible": true,
                "left": "5dp",
                "skin": "sknfontIcon33333316px",
                "text": "",
                "top": "5dp",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxPushNotificationChannelOption.add(fontIconPushNotificationChannelSelectOption, lblPushNotificationChannel, fontIconPushNotificationChannelInfo);
            var flxInAppChannel = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                "clipBounds": true,
                "id": "flxInAppChannel",
                "isVisible": true,
                "layoutType": kony.flex.FLOW_HORIZONTAL,
                "left": "3%",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0dp",
                "width": "100dp",
                "zIndex": 1
            }, {}, {});
            flxInAppChannel.setDefaultUnit(kony.flex.DP);
            var fontIconInAppChannelSelectOption = new kony.ui.Label({
                "id": "fontIconInAppChannelSelectOption",
                "isVisible": true,
                "left": "0dp",
                "skin": "sknFontIconCheckBoxUnselected",
                "text": "",
                "top": "0dp",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var lblInAppChannel = new kony.ui.Label({
                "id": "lblInAppChannel",
                "isVisible": true,
                "left": "7dp",
                "skin": "sknLbl485C75LatoRegular13Px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmAdManagement.inApp\")",
                "top": "5dp",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var fontIconInAppChannelInfo = new kony.ui.Label({
                "id": "fontIconInAppChannelInfo",
                "isVisible": true,
                "left": "5dp",
                "skin": "sknfontIcon33333316px",
                "text": "",
                "top": "5dp",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxInAppChannel.add(fontIconInAppChannelSelectOption, lblInAppChannel, fontIconInAppChannelInfo);
            var flxOfflineChannel = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                "clipBounds": true,
                "id": "flxOfflineChannel",
                "isVisible": false,
                "layoutType": kony.flex.FLOW_HORIZONTAL,
                "left": "3%",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0dp",
                "width": "100dp",
                "zIndex": 1
            }, {}, {});
            flxOfflineChannel.setDefaultUnit(kony.flex.DP);
            var fontIconOfflineChannelSelectOption = new kony.ui.Label({
                "id": "fontIconOfflineChannelSelectOption",
                "isVisible": true,
                "left": "0dp",
                "skin": "sknFontIconCheckBoxSelected",
                "text": "",
                "top": "0dp",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var lblOfflineChannel = new kony.ui.Label({
                "id": "lblOfflineChannel",
                "isVisible": true,
                "left": "7dp",
                "skin": "sknLbl485C75LatoRegular13Px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmAdManagement.offline\")",
                "top": "5dp",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var fontIconOfflineChannelInfo = new kony.ui.Label({
                "id": "fontIconOfflineChannelInfo",
                "isVisible": true,
                "left": "5dp",
                "skin": "sknfontIcon33333316px",
                "text": "",
                "top": "5dp",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxOfflineChannel.add(fontIconOfflineChannelSelectOption, lblOfflineChannel, fontIconOfflineChannelInfo);
            var flxInAppPopupChannel = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                "clipBounds": true,
                "id": "flxInAppPopupChannel",
                "isVisible": true,
                "layoutType": kony.flex.FLOW_HORIZONTAL,
                "left": "3%",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0dp",
                "width": "138dp",
                "zIndex": 1
            }, {}, {});
            flxInAppPopupChannel.setDefaultUnit(kony.flex.DP);
            var fontIconInAppPopupChannelSelectOption = new kony.ui.Label({
                "id": "fontIconInAppPopupChannelSelectOption",
                "isVisible": true,
                "left": "0dp",
                "skin": "sknFontIconCheckBoxSelected",
                "text": "",
                "top": "0dp",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var lblInAppPopupChannel = new kony.ui.Label({
                "id": "lblInAppPopupChannel",
                "isVisible": true,
                "left": "7dp",
                "skin": "sknLbl485C75LatoRegular13Px",
                "text": "In App Popup",
                "top": "5dp",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var fontIconInAppPopupChannelInfo = new kony.ui.Label({
                "id": "fontIconInAppPopupChannelInfo",
                "isVisible": true,
                "left": "5dp",
                "skin": "sknfontIcon33333316px",
                "text": "",
                "top": "5dp",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxInAppPopupChannel.add(fontIconInAppPopupChannelSelectOption, lblInAppPopupChannel, fontIconInAppPopupChannelInfo);
            flxChannelsOptions.add(flxSmsChannelOption, flxEmailChannelOption, flxPushNotificationChannelOption, flxInAppChannel, flxOfflineChannel, flxInAppPopupChannel);
            var ChannelsError = new com.adminConsole.loans.applications.errorMsg({
                "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                "id": "ChannelsError",
                "isVisible": false,
                "layoutType": kony.flex.FLOW_HORIZONTAL,
                "left": "0dp",
                "masterType": constants.MASTER_TYPE_DEFAULT,
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "60dp",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "overrides": {
                    "errorMsg": {
                        "bottom": "viz.val_cleared",
                        "isVisible": false,
                        "left": "0dp",
                        "top": "60dp"
                    },
                    "lblErrorText": {
                        "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmAdManagement.errorMsg_SelectChannel\")"
                    }
                }
            }, {
                "overrides": {}
            }, {
                "overrides": {}
            });
            flxChannels.add(lblChannels, flxChannelsOptions, ChannelsError);
            var flxPriorityContainer = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                "clipBounds": true,
                "id": "flxPriorityContainer",
                "isVisible": true,
                "layoutType": kony.flex.FLOW_VERTICAL,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0dp",
                "width": "100%"
            }, {}, {});
            flxPriorityContainer.setDefaultUnit(kony.flex.DP);
            var lblPriority = new kony.ui.Label({
                "id": "lblPriority",
                "isVisible": true,
                "left": "0dp",
                "skin": "sknLblLato485c7513px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmAdManagement.Priority\")",
                "top": "10dp",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var lblLine = new kony.ui.Label({
                "bottom": "0dp",
                "height": "1dp",
                "id": "lblLine",
                "isVisible": true,
                "left": "0dp",
                "right": "0dp",
                "skin": "sknlblSeperatorD7D9E0",
                "text": "l",
                "top": "15dp",
                "width": "100%",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var lblPriorityInfoText = new kony.ui.Label({
                "id": "lblPriorityInfoText",
                "isVisible": true,
                "left": "0dp",
                "skin": "sknlblLato11px696C73",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmAdManagement.PriorityInfoText\")",
                "top": "15dp",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var flxPriority = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "40dp",
                "id": "flxPriority",
                "isVisible": true,
                "layoutType": kony.flex.FLOW_HORIZONTAL,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "15dp",
                "width": "230dp",
                "zIndex": 1
            }, {}, {});
            flxPriority.setDefaultUnit(kony.flex.DP);
            var txtPriority = new kony.ui.TextBox2({
                "autoCapitalize": constants.TEXTBOX_AUTO_CAPITALIZE_NONE,
                "focusSkin": "skntxtbxDetailsFocus14px",
                "height": "100%",
                "id": "txtPriority",
                "isVisible": true,
                "keyBoardStyle": constants.TEXTBOX_KEY_BOARD_STYLE_PHONE_PAD,
                "left": "0",
                "i18n_placeholder": "kony.i18n.getLocalizedString(\"i18n.frmAdManagement.Priority\")",
                "secureTextEntry": false,
                "skin": "txtD7d9e0",
                "textInputMode": constants.TEXTBOX_INPUT_MODE_NUMERIC,
                "top": "0",
                "width": "140dp",
                "zIndex": 1
            }, {
                "containerHeightMode": constants.TEXTBOX_FONT_METRICS_DRIVEN_HEIGHT,
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [5, 0, 0, 0],
                "paddingInPixel": false
            }, {
                "autoCorrect": false,
                "placeholderSkin": "defTextBoxPlaceholder"
            });
            var btnPriorityList = new kony.ui.Button({
                "height": "100%",
                "id": "btnPriorityList",
                "isVisible": true,
                "left": "0dp",
                "skin": "sknBtnd7d9e0",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmAdManagement.ViewList\")",
                "top": "0dp",
                "width": "80dp",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_CENTER,
                "displayText": true,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {
                "hoverSkin": "sknBtnd7d9e0"
            });
            flxPriority.add(txtPriority, btnPriorityList);
            flxPriorityContainer.add(lblPriority, lblLine, lblPriorityInfoText, flxPriority);
            var PriorityError = new com.adminConsole.loans.applications.errorMsg({
                "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                "id": "PriorityError",
                "isVisible": false,
                "layoutType": kony.flex.FLOW_HORIZONTAL,
                "left": "0dp",
                "masterType": constants.MASTER_TYPE_DEFAULT,
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "10dp",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "overrides": {
                    "errorMsg": {
                        "isVisible": false
                    },
                    "lblErrorText": {
                        "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmAdManagement.ErrorPriority\")"
                    }
                }
            }, {
                "overrides": {}
            }, {
                "overrides": {}
            });
            var lblDateScheduler = new kony.ui.Label({
                "id": "lblDateScheduler",
                "isVisible": true,
                "left": "0dp",
                "skin": "sknLblLato485c7513px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmAdManagement.DateScheduleforAd\")",
                "top": "30dp",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var lblLine1 = new kony.ui.Label({
                "bottom": "0dp",
                "height": "1dp",
                "id": "lblLine1",
                "isVisible": true,
                "left": "0dp",
                "right": "0dp",
                "skin": "sknlblSeperatorD7D9E0",
                "text": "l",
                "top": "15dp",
                "width": "100%",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var flxDateContainer = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                "bottom": "25dp",
                "clipBounds": true,
                "id": "flxDateContainer",
                "isVisible": true,
                "layoutType": kony.flex.FLOW_VERTICAL,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0dp",
                "width": "100%"
            }, {}, {});
            flxDateContainer.setDefaultUnit(kony.flex.DP);
            var lblStartDate = new kony.ui.Label({
                "id": "lblStartDate",
                "isVisible": true,
                "left": "0dp",
                "skin": "sknLblLato485c7513px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmAdManagement.StartDate\")",
                "top": "15dp",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var flxStartDate = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "40dp",
                "id": "flxStartDate",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "sknFlxCalendar",
                "top": "15dp",
                "width": "225dp"
            }, {}, {});
            flxStartDate.setDefaultUnit(kony.flex.DP);
            var customStartDate = new kony.ui.CustomWidget({
                "id": "customStartDate",
                "isVisible": true,
                "left": "0px",
                "right": "0px",
                "bottom": "0px",
                "top": "5px",
                "width": "100%",
                "height": "40px",
                "zIndex": 10,
                "clipBounds": true
            }, {
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {
                "widgetName": "dateRangePicker",
                "drops": "up",
                "event": null,
                "rangeType": "",
                "resetData": null,
                "type": "single",
                "value": ""
            });
            flxStartDate.add(customStartDate);
            var lblStartDateTip = new kony.ui.Label({
                "bottom": "0dp",
                "id": "lblStartDateTip",
                "isVisible": true,
                "left": "0dp",
                "right": "20dp",
                "skin": "skn11px9CA9BA",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmAdManagement.ErrorStartDateLessThan90DaysTip\")",
                "top": "10dp",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var StartDateError = new com.adminConsole.loans.applications.errorMsg({
                "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                "id": "StartDateError",
                "isVisible": false,
                "layoutType": kony.flex.FLOW_HORIZONTAL,
                "left": "0dp",
                "masterType": constants.MASTER_TYPE_DEFAULT,
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "10dp",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "overrides": {
                    "errorMsg": {
                        "isVisible": false
                    },
                    "lblErrorText": {
                        "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmAdManagement.ErrorEndDate\")"
                    }
                }
            }, {
                "overrides": {}
            }, {
                "overrides": {}
            });
            var lblEndDate = new kony.ui.Label({
                "id": "lblEndDate",
                "isVisible": true,
                "left": "0dp",
                "skin": "sknLblLato485c7513px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmAdManagement.EndDate\")",
                "top": "15dp",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var flxEndDate = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "40dp",
                "id": "flxEndDate",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "sknFlxCalendar",
                "top": "15dp",
                "width": "225dp"
            }, {}, {});
            flxEndDate.setDefaultUnit(kony.flex.DP);
            var customEndDate = new kony.ui.CustomWidget({
                "id": "customEndDate",
                "isVisible": true,
                "left": "0px",
                "right": "0px",
                "bottom": "0px",
                "top": "5px",
                "width": "100%",
                "height": "40px",
                "zIndex": 10,
                "clipBounds": true
            }, {
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {
                "widgetName": "dateRangePicker",
                "drops": "up",
                "event": null,
                "rangeType": "",
                "resetData": null,
                "type": "single",
                "value": ""
            });
            flxEndDate.add(customEndDate);
            var lblEndDateTip = new kony.ui.Label({
                "bottom": "0dp",
                "id": "lblEndDateTip",
                "isVisible": true,
                "left": "0dp",
                "right": "20dp",
                "skin": "skn11px9CA9BA",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmAdManagement.ErrorEndDateLessThan90DaysTip\")",
                "top": "11dp",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var EndDateError = new com.adminConsole.loans.applications.errorMsg({
                "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                "id": "EndDateError",
                "isVisible": false,
                "layoutType": kony.flex.FLOW_HORIZONTAL,
                "left": "0dp",
                "masterType": constants.MASTER_TYPE_DEFAULT,
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "10dp",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "overrides": {
                    "errorMsg": {
                        "isVisible": false
                    },
                    "lblErrorText": {
                        "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmAdManagement.ErrorEndDate\")"
                    }
                }
            }, {
                "overrides": {}
            }, {
                "overrides": {}
            });
            flxDateContainer.add(lblStartDate, flxStartDate, lblStartDateTip, StartDateError, lblEndDate, flxEndDate, lblEndDateTip, EndDateError);
            var rtxDefaultDateInfo = new kony.ui.RichText({
                "bottom": "25dp",
                "id": "rtxDefaultDateInfo",
                "isVisible": false,
                "left": "0",
                "linkSkin": "defRichTextLink",
                "skin": "sknrtxLatoRegular485c7512px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmAdManagement.DefaultCampaignDatemsg\")",
                "top": "15dp",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 10
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxBasicDetails.add(flxAdName, txtAdName, AdNameError, flxDescription, txtDescription, AdDescriptionError, flxAdType, flxProduct, flxChannels, flxPriorityContainer, PriorityError, lblDateScheduler, lblLine1, flxDateContainer, rtxDefaultDateInfo);
            var flxAdSpecifications = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                "clipBounds": true,
                "id": "flxAdSpecifications",
                "isVisible": false,
                "layoutType": kony.flex.FLOW_VERTICAL,
                "left": "20dp",
                "isModalContainer": false,
                "right": "20dp",
                "skin": "slFbox",
                "top": "0dp"
            }, {}, {});
            flxAdSpecifications.setDefaultUnit(kony.flex.DP);
            var flxPreLogin = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                "clipBounds": true,
                "id": "flxPreLogin",
                "isVisible": false,
                "layoutType": kony.flex.FLOW_VERTICAL,
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0dp",
                "width": kony.flex.USE_PREFFERED_SIZE
            }, {}, {});
            flxPreLogin.setDefaultUnit(kony.flex.DP);
            flxPreLogin.add();
            var flxPostLogin = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                "clipBounds": true,
                "id": "flxPostLogin",
                "isVisible": false,
                "layoutType": kony.flex.FLOW_VERTICAL,
                "left": "0dp",
                "isModalContainer": false,
                "right": "0dp",
                "skin": "slFbox",
                "top": "0dp",
                "width": kony.flex.USE_PREFFERED_SIZE
            }, {}, {});
            flxPostLogin.setDefaultUnit(kony.flex.DP);
            flxPostLogin.add();
            var flxDashboard = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                "clipBounds": true,
                "id": "flxDashboard",
                "isVisible": false,
                "layoutType": kony.flex.FLOW_VERTICAL,
                "left": "0dp",
                "isModalContainer": false,
                "right": "0dp",
                "skin": "slFbox",
                "top": "0dp",
                "width": kony.flex.USE_PREFFERED_SIZE
            }, {}, {});
            flxDashboard.setDefaultUnit(kony.flex.DP);
            flxDashboard.add();
            var flxNewAccount = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                "clipBounds": true,
                "id": "flxNewAccount",
                "isVisible": false,
                "layoutType": kony.flex.FLOW_VERTICAL,
                "left": "0dp",
                "isModalContainer": false,
                "right": "0dp",
                "skin": "slFbox",
                "top": "0dp",
                "width": kony.flex.USE_PREFFERED_SIZE
            }, {}, {});
            flxNewAccount.setDefaultUnit(kony.flex.DP);
            flxNewAccount.add();
            flxAdSpecifications.add(flxPreLogin, flxPostLogin, flxDashboard, flxNewAccount);
            var flxTargetCustomers = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                "clipBounds": true,
                "id": "flxTargetCustomers",
                "isVisible": false,
                "layoutType": kony.flex.FREE_FORM,
                "left": "20dp",
                "isModalContainer": false,
                "right": "20dp",
                "skin": "slFbox",
                "top": "0dp",
                "width": kony.flex.USE_PREFFERED_SIZE
            }, {}, {});
            flxTargetCustomers.setDefaultUnit(kony.flex.DP);
            var flxCustomer = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                "clipBounds": true,
                "id": "flxCustomer",
                "isVisible": false,
                "layoutType": kony.flex.FLOW_HORIZONTAL,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0dp",
                "width": kony.flex.USE_PREFFERED_SIZE
            }, {}, {});
            flxCustomer.setDefaultUnit(kony.flex.DP);
            var flxAvailableOptions = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                "clipBounds": true,
                "id": "flxAvailableOptions",
                "isVisible": true,
                "layoutType": kony.flex.FLOW_VERTICAL,
                "left": "0px",
                "isModalContainer": false,
                "top": "0dp",
                "width": "49%",
                "zIndex": 1
            }, {}, {});
            flxAvailableOptions.setDefaultUnit(kony.flex.DP);
            var flxAvailableOptionsHeader = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "15dp",
                "id": "flxAvailableOptionsHeader",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "15dp",
                "width": "100%"
            }, {}, {});
            flxAvailableOptionsHeader.setDefaultUnit(kony.flex.DP);
            var lblAvailableOptionsHeading = new kony.ui.Label({
                "id": "lblAvailableOptionsHeading",
                "isVisible": true,
                "left": "0px",
                "skin": "sknLblLato485c7513px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmAdManagement.AvailableCustomerRoles\")",
                "top": 0,
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var btnAddAll = new kony.ui.Button({
                "id": "btnAddAll",
                "isVisible": true,
                "right": "0dp",
                "skin": "sknbtn117eb011px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmAdManagement.AddAllCaps\")",
                "top": 0,
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "displayText": true,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxAvailableOptionsHeader.add(lblAvailableOptionsHeading, btnAddAll);
            var flxAvailableOptionsContent = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                "clipBounds": true,
                "id": "flxAvailableOptionsContent",
                "isVisible": true,
                "layoutType": kony.flex.FLOW_VERTICAL,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "sknflxBgf7f9faBorderd7d9e0",
                "top": "15dp",
                "width": "100%"
            }, {}, {});
            flxAvailableOptionsContent.setDefaultUnit(kony.flex.DP);
            var flxSearch = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "65px",
                "id": "flxSearch",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": 0,
                "isModalContainer": false,
                "right": 0,
                "skin": "sknflxf5f6f8Op100",
                "top": "0px",
                "zIndex": 1
            }, {}, {});
            flxSearch.setDefaultUnit(kony.flex.DP);
            var flxSearchContainer = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "centerY": "50%",
                "clipBounds": true,
                "focusSkin": "slFbox0ebc847fa67a243Search",
                "height": "40px",
                "id": "flxSearchContainer",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "10dp",
                "isModalContainer": false,
                "right": 10,
                "skin": "sknflxd5d9ddop100",
                "zIndex": 1
            }, {}, {});
            flxSearchContainer.setDefaultUnit(kony.flex.DP);
            var tbxSearchBox = new kony.ui.TextBox2({
                "autoCapitalize": constants.TEXTBOX_AUTO_CAPITALIZE_NONE,
                "centerY": "50%",
                "height": "100%",
                "id": "tbxSearchBox",
                "isVisible": true,
                "keyBoardStyle": constants.TEXTBOX_KEY_BOARD_STYLE_DEFAULT,
                "left": "35dp",
                "i18n_placeholder": "kony.i18n.getLocalizedString(\"i18n.permission.search\")",
                "right": "35dp",
                "secureTextEntry": false,
                "skin": "skntbxffffffNoBorderlato35475f14px",
                "textInputMode": constants.TEXTBOX_INPUT_MODE_ANY,
                "zIndex": 1
            }, {
                "containerHeightMode": constants.TEXTBOX_FONT_METRICS_DRIVEN_HEIGHT,
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [3, 0, 0, 0],
                "paddingInPixel": false
            }, {
                "autoCorrect": false
            });
            var flxClearSearch = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "centerY": "47%",
                "clipBounds": true,
                "height": "35px",
                "id": "flxClearSearch",
                "isVisible": false,
                "layoutType": kony.flex.FREE_FORM,
                "isModalContainer": false,
                "right": "20px",
                "skin": "slFbox",
                "width": "35px",
                "zIndex": 1
            }, {}, {
                "hoverSkin": "sknCursor"
            });
            flxClearSearch.setDefaultUnit(kony.flex.DP);
            var fontIconImgCLose = new kony.ui.Label({
                "centerY": "50.00%",
                "id": "fontIconImgCLose",
                "isVisible": true,
                "left": "17dp",
                "skin": "sknFontIconSearchCross16px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmAlertsManagement.lblAlertPreviewClose\")",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 2
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxClearSearch.add(fontIconImgCLose);
            var fontIconImgSearchIcon = new kony.ui.Label({
                "centerY": "50%",
                "id": "fontIconImgSearchIcon",
                "isVisible": true,
                "left": "17dp",
                "skin": "sknFontIconSearchImg20px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmGroups.fontIconSearchimg\")",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 2
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxSearchContainer.add(tbxSearchBox, flxClearSearch, fontIconImgSearchIcon);
            flxSearch.add(flxSearchContainer);
            var flxAddOptionsSegment = new kony.ui.FlexScrollContainer({
                "allowHorizontalBounce": false,
                "allowVerticalBounce": true,
                "bottom": 0,
                "bounces": true,
                "clipBounds": true,
                "enableScrolling": true,
                "horizontalScrollIndicator": true,
                "id": "flxAddOptionsSegment",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "pagingEnabled": false,
                "scrollDirection": kony.flex.SCROLL_VERTICAL,
                "top": "0dp",
                "verticalScrollIndicator": true,
                "width": "100%",
                "zIndex": 1
            }, {}, {});
            flxAddOptionsSegment.setDefaultUnit(kony.flex.DP);
            var segAddOptions = new kony.ui.SegmentedUI2({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "bottom": 0,
                "data": [{
                    "btnAdd": "kony.i18n.getLocalizedString(\"i18n.DragBox.ADD\")",
                    "lblDesc": "USERNAME",
                    "lblName": "John Doe"
                }, {
                    "btnAdd": "kony.i18n.getLocalizedString(\"i18n.DragBox.ADD\")",
                    "lblDesc": "USERNAME",
                    "lblName": "John Doe"
                }, {
                    "btnAdd": "kony.i18n.getLocalizedString(\"i18n.DragBox.ADD\")",
                    "lblDesc": "USERNAME",
                    "lblName": "John Doe"
                }, {
                    "btnAdd": "kony.i18n.getLocalizedString(\"i18n.DragBox.ADD\")",
                    "lblDesc": "USERNAME",
                    "lblName": "John Doe"
                }, {
                    "btnAdd": "kony.i18n.getLocalizedString(\"i18n.DragBox.ADD\")",
                    "lblDesc": "USERNAME",
                    "lblName": "John Doe"
                }, {
                    "btnAdd": "kony.i18n.getLocalizedString(\"i18n.DragBox.ADD\")",
                    "lblDesc": "USERNAME",
                    "lblName": "John Doe"
                }, {
                    "btnAdd": "kony.i18n.getLocalizedString(\"i18n.DragBox.ADD\")",
                    "lblDesc": "USERNAME",
                    "lblName": "John Doe"
                }, {
                    "btnAdd": "kony.i18n.getLocalizedString(\"i18n.DragBox.ADD\")",
                    "lblDesc": "USERNAME",
                    "lblName": "John Doe"
                }, {
                    "btnAdd": "kony.i18n.getLocalizedString(\"i18n.DragBox.ADD\")",
                    "lblDesc": "USERNAME",
                    "lblName": "John Doe"
                }],
                "groupCells": false,
                "height": "100%",
                "id": "segAddOptions",
                "isVisible": true,
                "left": "0dp",
                "needPageIndicator": true,
                "pageOffDotImage": "pageoffdot.png",
                "pageOnDotImage": "pageondot.png",
                "retainSelection": false,
                "rowFocusSkin": "seg2Focus",
                "rowSkin": "sknsegffffffOp100Bordere1e5ed",
                "rowTemplate": "flxCampaignAddCustomer",
                "sectionHeaderSkin": "sliPhoneSegmentHeader",
                "selectionBehavior": constants.SEGUI_DEFAULT_BEHAVIOR,
                "separatorRequired": false,
                "showScrollbars": true,
                "top": "0dp",
                "viewType": constants.SEGUI_VIEW_TYPE_TABLEVIEW,
                "widgetDataMap": {
                    "btnAdd": "btnAdd",
                    "flxAddUsersWrapper": "flxAddUsersWrapper",
                    "flxCampaignAddCustomer": "flxCampaignAddCustomer",
                    "flxDetails": "flxDetails",
                    "lblDesc": "lblDesc",
                    "lblName": "lblName"
                },
                "width": "100%",
                "zIndex": 1
            }, {
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var lblNoCustomersAvailable = new kony.ui.Label({
                "centerX": "50%",
                "id": "lblNoCustomersAvailable",
                "isVisible": false,
                "skin": "sknLblLato485c7513px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmAdManagement.NoCustomersAvailable\")",
                "top": "20dp",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxAddOptionsSegment.add(segAddOptions, lblNoCustomersAvailable);
            flxAvailableOptionsContent.add(flxSearch, flxAddOptionsSegment);
            flxAvailableOptions.add(flxAvailableOptionsHeader, flxAvailableOptionsContent);
            var flxSelectedOptions = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                "clipBounds": true,
                "id": "flxSelectedOptions",
                "isVisible": true,
                "layoutType": kony.flex.FLOW_VERTICAL,
                "left": "2%",
                "isModalContainer": false,
                "right": 0,
                "skin": "slFbox0c11dce22bfe447",
                "top": "0",
                "width": "49%",
                "zIndex": 1
            }, {}, {});
            flxSelectedOptions.setDefaultUnit(kony.flex.DP);
            var flxSelectedOptionsHeader = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "15dp",
                "id": "flxSelectedOptionsHeader",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "15dp",
                "width": "100%"
            }, {}, {});
            flxSelectedOptionsHeader.setDefaultUnit(kony.flex.DP);
            var lblSelectedOption = new kony.ui.Label({
                "id": "lblSelectedOption",
                "isVisible": true,
                "left": "0px",
                "skin": "sknLblLato485c7513px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmAdManagement.SelectedCustomerRoles\")",
                "top": 0,
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var btnRemoveAll = new kony.ui.Button({
                "id": "btnRemoveAll",
                "isVisible": true,
                "right": 0,
                "skin": "sknbtn117eb011px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmAdManagement.ResetCAPS\")",
                "top": 0,
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "displayText": true,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxSelectedOptionsHeader.add(lblSelectedOption, btnRemoveAll);
            var flxSegSelectedOptions = new kony.ui.FlexScrollContainer({
                "allowHorizontalBounce": false,
                "allowVerticalBounce": true,
                "bottom": 0,
                "bounces": true,
                "clipBounds": true,
                "enableScrolling": true,
                "horizontalScrollIndicator": true,
                "id": "flxSegSelectedOptions",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "pagingEnabled": false,
                "scrollDirection": kony.flex.SCROLL_VERTICAL,
                "skin": "sknf9f9f9d7d9e03px",
                "top": "15dp",
                "verticalScrollIndicator": true,
                "width": "100%",
                "zIndex": 1
            }, {}, {});
            flxSegSelectedOptions.setDefaultUnit(kony.flex.DP);
            var segSelectedOptions = new kony.ui.SegmentedUI2({
                "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                "data": [{
                    "fontIconClose": "",
                    "lblOption": "John Doe"
                }, {
                    "fontIconClose": "",
                    "lblOption": "John Doe"
                }],
                "groupCells": false,
                "id": "segSelectedOptions",
                "isVisible": true,
                "left": "0px",
                "needPageIndicator": true,
                "pageOffDotImage": "pageoffdot.png",
                "pageOnDotImage": "pageondot.png",
                "retainSelection": false,
                "rowSkin": "sknsegf9f9f9Op100",
                "rowTemplate": "flxCampaignOptionsSelected",
                "sectionHeaderSkin": "sliPhoneSegmentHeader0h3448bdd9d8941",
                "selectionBehavior": constants.SEGUI_DEFAULT_BEHAVIOR,
                "separatorColor": "64646400",
                "separatorRequired": true,
                "separatorThickness": 0,
                "showScrollbars": true,
                "top": "0px",
                "viewType": constants.SEGUI_VIEW_TYPE_TABLEVIEW,
                "widgetDataMap": {
                    "flxAddOptionWrapper": "flxAddOptionWrapper",
                    "flxCampaignOptionsSelected": "flxCampaignOptionsSelected",
                    "flxClose": "flxClose",
                    "fontIconClose": "fontIconClose",
                    "lblOption": "lblOption"
                },
                "width": "100%",
                "zIndex": 1
            }, {
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var lblNoSelectedCustomers = new kony.ui.Label({
                "centerX": "50%",
                "id": "lblNoSelectedCustomers",
                "isVisible": false,
                "skin": "sknLblLato485c7513px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmAdManagement.ClickToAddCustomer\")",
                "top": "20dp",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxSegSelectedOptions.add(segSelectedOptions, lblNoSelectedCustomers);
            flxSelectedOptions.add(flxSelectedOptionsHeader, flxSegSelectedOptions);
            var rtxdefaultCampaignCustomerMsg = new kony.ui.RichText({
                "id": "rtxdefaultCampaignCustomerMsg",
                "isVisible": false,
                "left": "0",
                "linkSkin": "defRichTextLink",
                "skin": "sknrtxLatoRegular485c7512px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmAdManagement.DefaultCampaignCustomersRolesMsg\")",
                "top": "20dp",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 10
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxCustomer.add(flxAvailableOptions, flxSelectedOptions, rtxdefaultCampaignCustomerMsg);
            var flxRoles = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                "clipBounds": true,
                "id": "flxRoles",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0dp",
                "width": kony.flex.USE_PREFFERED_SIZE
            }, {}, {});
            flxRoles.setDefaultUnit(kony.flex.DP);
            var flxNoRoleError = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "40dp",
                "id": "flxNoRoleError",
                "isVisible": false,
                "layoutType": kony.flex.FLOW_HORIZONTAL,
                "left": "0",
                "isModalContainer": false,
                "skin": "sknBorderRed",
                "top": "20dp",
                "width": "100%",
                "zIndex": 10
            }, {}, {});
            flxNoRoleError.setDefaultUnit(kony.flex.DP);
            var flxErrorIcon = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "100%",
                "id": "flxErrorIcon",
                "isVisible": true,
                "layoutType": kony.flex.FLOW_HORIZONTAL,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "sknRedFill",
                "top": "0dp",
                "width": "10%"
            }, {}, {});
            flxErrorIcon.setDefaultUnit(kony.flex.DP);
            var lblErrorIcon = new kony.ui.Label({
                "centerX": "50%",
                "centerY": "50%",
                "height": "20dp",
                "id": "lblErrorIcon",
                "isVisible": true,
                "left": "0dp",
                "skin": "sknIcon20pxWhite",
                "text": "",
                "top": "0dp",
                "width": "20dp",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxErrorIcon.add(lblErrorIcon);
            var lblErrorText = new kony.ui.Label({
                "centerY": "50%",
                "height": "15dp",
                "id": "lblErrorText",
                "isVisible": true,
                "left": "20dp",
                "skin": "sknlblError",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmAdManagement.ErrorRole\")",
                "top": "0dp",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxNoRoleError.add(flxErrorIcon, lblErrorText);
            var flxNoRoles = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                "clipBounds": true,
                "id": "flxNoRoles",
                "isVisible": true,
                "layoutType": kony.flex.FLOW_VERTICAL,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0dp",
                "width": "100%"
            }, {}, {});
            flxNoRoles.setDefaultUnit(kony.flex.DP);
            var imgCustRoles = new kony.ui.Image2({
                "centerX": "50%",
                "height": "70dp",
                "id": "imgCustRoles",
                "isVisible": true,
                "skin": "slImage",
                "src": "customerrole.png",
                "top": "150dp",
                "width": "150dp",
                "zIndex": 10
            }, {
                "imageScaleMode": constants.IMAGE_SCALE_MODE_MAINTAIN_ASPECT_RATIO,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var lblNoRoles = new kony.ui.Label({
                "centerX": "50%",
                "id": "lblNoRoles",
                "isVisible": true,
                "skin": "sknLbl485C75LatoRegular13Px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmAdManagement.NoCustomerSegmentsYet\")",
                "top": "15dp",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 10
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var flxRoleButtons = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                "centerX": "50%",
                "clipBounds": true,
                "id": "flxRoleButtons",
                "isVisible": true,
                "layoutType": kony.flex.FLOW_HORIZONTAL,
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "50dp",
                "width": "400dp",
                "zIndex": 10
            }, {}, {});
            flxRoleButtons.setDefaultUnit(kony.flex.DP);
            var btnExisting = new kony.ui.Button({
                "centerX": "50%",
                "focusSkin": "sknbtnffffffLatoRegular4f555dBorder1px485c75",
                "height": "30dp",
                "id": "btnExisting",
                "isVisible": true,
                "left": "0dp",
                "skin": "sknbtnf7f7faLatoRegular4f555dBorder1px8b96a5",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmAdManagement.AddSegment_s\")",
                "width": "130dp",
                "zIndex": 10
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_CENTER,
                "displayText": true,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {
                "hoverSkin": "sknbtnffffffLatoRegular4f555dBorder1px485c75"
            });
            var btnNewRole = new kony.ui.Button({
                "focusSkin": "sknbtnffffffLatoRegular4f555dBorder1px485c75",
                "height": "30dp",
                "id": "btnNewRole",
                "isVisible": false,
                "left": "15dp",
                "skin": "sknbtnf7f7faLatoRegular4f555dBorder1px8b96a5",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmAdManagement.CreateNewProfile\")",
                "top": "0",
                "width": "190dp",
                "zIndex": 10
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_CENTER,
                "displayText": true,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {
                "hoverSkin": "sknbtnffffffLatoRegular4f555dBorder1px485c75"
            });
            flxRoleButtons.add(btnExisting, btnNewRole);
            flxNoRoles.add(imgCustRoles, lblNoRoles, flxRoleButtons);
            var flxAddRole = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                "bottom": "20dp",
                "clipBounds": true,
                "id": "flxAddRole",
                "isVisible": false,
                "layoutType": kony.flex.FLOW_VERTICAL,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0dp",
                "width": "100%",
                "zIndex": 10
            }, {}, {});
            flxAddRole.setDefaultUnit(kony.flex.DP);
            var flxCreateRole = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                "clipBounds": true,
                "id": "flxCreateRole",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "20dp",
                "width": "100%",
                "zIndex": 1
            }, {}, {});
            flxCreateRole.setDefaultUnit(kony.flex.DP);
            var lblCreateRole = new kony.ui.Label({
                "id": "lblCreateRole",
                "isVisible": true,
                "left": "0dp",
                "skin": "sknLblLato485c7513px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmAdManagement.CreateNewProfile\")",
                "top": "0dp",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var backToRoles = new com.adminConsole.customerMang.backToPageHeader({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "height": "20px",
                "id": "backToRoles",
                "isVisible": true,
                "layoutType": kony.flex.FLOW_HORIZONTAL,
                "masterType": constants.MASTER_TYPE_DEFAULT,
                "isModalContainer": false,
                "right": "0dp",
                "skin": "slFbox",
                "top": "0dp",
                "width": "60dp",
                "overrides": {
                    "backToPageHeader": {
                        "left": "viz.val_cleared",
                        "right": "0dp",
                        "width": "60dp"
                    },
                    "btnBack": {
                        "centerY": "50%",
                        "top": "viz.val_cleared"
                    },
                    "flxBack": {
                        "centerY": "viz.val_cleared",
                        "left": "viz.val_cleared",
                        "top": "0dp"
                    }
                }
            }, {
                "overrides": {}
            }, {
                "overrides": {}
            });
            flxCreateRole.add(lblCreateRole, backToRoles);
            var lblRoleSeperator = new kony.ui.Label({
                "bottom": "0dp",
                "height": "1dp",
                "id": "lblRoleSeperator",
                "isVisible": true,
                "left": "0dp",
                "right": "0dp",
                "skin": "sknlblSeperatorD7D9E0",
                "text": "l",
                "top": "15dp",
                "width": "100%",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var flxRoleName = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                "clipBounds": true,
                "id": "flxRoleName",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "20dp",
                "width": "100%"
            }, {}, {});
            flxRoleName.setDefaultUnit(kony.flex.DP);
            var lblRoleName = new kony.ui.Label({
                "id": "lblRoleName",
                "isVisible": true,
                "left": "0dp",
                "skin": "sknLblLato485c7513px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmAdManagement.ProfileName\")",
                "top": "0dp",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var lblRoleNameSize = new kony.ui.Label({
                "id": "lblRoleNameSize",
                "isVisible": false,
                "right": "0dp",
                "skin": "sknLblLato485c7513px",
                "text": "0/50",
                "top": "0dp",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxRoleName.add(lblRoleName, lblRoleNameSize);
            var txtRoleName = new kony.ui.TextBox2({
                "autoCapitalize": constants.TEXTBOX_AUTO_CAPITALIZE_NONE,
                "focusSkin": "skntxtbxDetailsFocus14px",
                "height": "40dp",
                "id": "txtRoleName",
                "isVisible": true,
                "keyBoardStyle": constants.TEXTBOX_KEY_BOARD_STYLE_DEFAULT,
                "left": "0",
                "maxTextLength": 50,
                "i18n_placeholder": "kony.i18n.getLocalizedString(\"i18n.frmAdManagement.ProfileName\")",
                "secureTextEntry": false,
                "skin": "txtD7d9e0",
                "textInputMode": constants.TEXTBOX_INPUT_MODE_ANY,
                "top": "15dp",
                "width": "100%",
                "zIndex": 1
            }, {
                "containerHeightMode": constants.TEXTBOX_FONT_METRICS_DRIVEN_HEIGHT,
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [2, 0, 0, 0],
                "paddingInPixel": false
            }, {
                "autoCorrect": false,
                "placeholderSkin": "sknTextGreyb2bdcd"
            });
            var RoleNameError = new com.adminConsole.loans.applications.errorMsg({
                "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                "id": "RoleNameError",
                "isVisible": false,
                "layoutType": kony.flex.FLOW_HORIZONTAL,
                "left": "0dp",
                "masterType": constants.MASTER_TYPE_DEFAULT,
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "10dp",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "overrides": {
                    "errorMsg": {
                        "isVisible": false
                    },
                    "lblErrorText": {
                        "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmAdManagement.ErrorAdName\")"
                    }
                }
            }, {
                "overrides": {}
            }, {
                "overrides": {}
            });
            var flxRoleDescription = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                "clipBounds": true,
                "id": "flxRoleDescription",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "30dp",
                "width": "100%"
            }, {}, {});
            flxRoleDescription.setDefaultUnit(kony.flex.DP);
            var lblRoleDescription = new kony.ui.Label({
                "id": "lblRoleDescription",
                "isVisible": true,
                "left": "0dp",
                "skin": "sknLblLato485c7513px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmAdManagement.Description\")",
                "top": "0dp",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var lblRoleDescSize = new kony.ui.Label({
                "id": "lblRoleDescSize",
                "isVisible": false,
                "right": "0dp",
                "skin": "sknLblLato485c7513px",
                "text": "0/150",
                "top": "0dp",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxRoleDescription.add(lblRoleDescription, lblRoleDescSize);
            var txtRoleDescription = new kony.ui.TextArea2({
                "autoCapitalize": constants.TEXTAREA_AUTO_CAPITALIZE_NONE,
                "focusSkin": "sknFocus",
                "height": "70dp",
                "id": "txtRoleDescription",
                "isVisible": true,
                "keyBoardStyle": constants.TEXTAREA_KEY_BOARD_STYLE_DEFAULT,
                "left": "0dp",
                "maxTextLength": 300,
                "numberOfVisibleLines": 3,
                "i18n_placeholder": "kony.i18n.getLocalizedString(\"i18n.frmAdManagement.Description\")",
                "skin": "skntxtAread7d9e0",
                "textInputMode": constants.TEXTAREA_INPUT_MODE_ANY,
                "top": "15dp",
                "width": "100%",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_TOP_LEFT,
                "padding": [2, 2, 2, 2],
                "paddingInPixel": false
            }, {
                "autoCorrect": false,
                "placeholderSkin": "defTextAreaFocus"
            });
            var RoleDescError = new com.adminConsole.loans.applications.errorMsg({
                "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                "id": "RoleDescError",
                "isVisible": false,
                "layoutType": kony.flex.FLOW_HORIZONTAL,
                "left": "0dp",
                "masterType": constants.MASTER_TYPE_DEFAULT,
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "10dp",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "overrides": {
                    "errorMsg": {
                        "isVisible": false
                    },
                    "lblErrorText": {
                        "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmAdManagement.ErrorAdDescription\")"
                    }
                }
            }, {
                "overrides": {}
            }, {
                "overrides": {}
            });
            var flxAttrSection = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "30dp",
                "id": "flxAttrSection",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "30dp",
                "width": "100%"
            }, {}, {});
            flxAttrSection.setDefaultUnit(kony.flex.DP);
            var lblAttributes = new kony.ui.Label({
                "centerY": "50%",
                "id": "lblAttributes",
                "isVisible": true,
                "left": "0dp",
                "skin": "sknLblLato485c7513px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmAdManagement.Attributes\")",
                "top": "0dp",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var btnAddMoreAttrs = new kony.ui.Button({
                "focusSkin": "sknbtnffffffLatoRegular4f555dBorder1px485c75",
                "height": "22dp",
                "id": "btnAddMoreAttrs",
                "isVisible": true,
                "right": 0,
                "skin": "sknbtnf7f7faLatoRegular4f555dBorder1px8b96a5",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.ConfigurationBundles.edit\")",
                "top": "0",
                "width": "60dp",
                "zIndex": 10
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_CENTER,
                "displayText": true,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {
                "hoverSkin": "sknbtnffffffLatoRegular4f555dBorder1px485c75"
            });
            flxAttrSection.add(lblAttributes, btnAddMoreAttrs);
            var flxAttributesContainer = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                "clipBounds": true,
                "id": "flxAttributesContainer",
                "isVisible": true,
                "layoutType": kony.flex.FLOW_VERTICAL,
                "left": "0",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "15dp",
                "width": "100%",
                "zIndex": 1
            }, {}, {});
            flxAttributesContainer.setDefaultUnit(kony.flex.DP);
            var flxNoAttributes = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "200dp",
                "id": "flxNoAttributes",
                "isVisible": true,
                "layoutType": kony.flex.FLOW_VERTICAL,
                "isModalContainer": false,
                "skin": "sknflxf8f9fabkgdddee0border3pxradius",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {}, {});
            flxNoAttributes.setDefaultUnit(kony.flex.DP);
            var lblNoAttributes = new kony.ui.Label({
                "centerX": "50%",
                "id": "lblNoAttributes",
                "isVisible": true,
                "skin": "sknLbl485C75LatoRegular13Px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmAdManagement.NoAttributes\")",
                "top": "80dp",
                "width": "175dp",
                "zIndex": 10
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var btnAddAttibutes = new kony.ui.Button({
                "centerX": "50%",
                "focusSkin": "sknbtnffffffLatoRegular4f555dBorder1px485c75",
                "height": "30dp",
                "id": "btnAddAttibutes",
                "isVisible": true,
                "skin": "sknbtnf7f7faLatoRegular4f555dBorder1px8b96a5",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmAdManagement.AddDatasetsAndAttributes\")",
                "top": "15dp",
                "width": "190dp",
                "zIndex": 10
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_CENTER,
                "displayText": true,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {
                "hoverSkin": "sknbtnffffffLatoRegular4f555dBorder1px485c75"
            });
            flxNoAttributes.add(lblNoAttributes, btnAddAttibutes);
            var flxAttrContainer = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                "clipBounds": true,
                "id": "flxAttrContainer",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0dp",
                "width": "100%"
            }, {}, {});
            flxAttrContainer.setDefaultUnit(kony.flex.DP);
            var flxAddAttributes = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                "clipBounds": true,
                "id": "flxAddAttributes",
                "isVisible": false,
                "layoutType": kony.flex.FLOW_VERTICAL,
                "left": "0",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {}, {});
            flxAddAttributes.setDefaultUnit(kony.flex.DP);
            var flxAttrHeaderRow = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "50dp",
                "id": "flxAttrHeaderRow",
                "isVisible": true,
                "layoutType": kony.flex.FLOW_HORIZONTAL,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0",
                "width": "100%"
            }, {}, {});
            flxAttrHeaderRow.setDefaultUnit(kony.flex.DP);
            var flxDatasetsHeader = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "100%",
                "id": "flxDatasetsHeader",
                "isVisible": true,
                "layoutType": kony.flex.FLOW_HORIZONTAL,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0dp",
                "width": "24%"
            }, {}, {});
            flxDatasetsHeader.setDefaultUnit(kony.flex.DP);
            var lblDatasetHeader = new kony.ui.Label({
                "id": "lblDatasetHeader",
                "isVisible": true,
                "left": "0",
                "skin": "sknLbl73757C11px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmAdManagement.DatasetCAPS\")",
                "top": "21dp",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var lblFilterDataset = new kony.ui.Label({
                "centerY": "50%",
                "id": "lblFilterDataset",
                "isVisible": true,
                "left": "7dp",
                "skin": "sknIcon15px",
                "text": "",
                "top": "21dp",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {
                "hoverSkin": "sknlblCursorFont"
            });
            flxDatasetsHeader.add(lblDatasetHeader, lblFilterDataset);
            var flxAttrHeader = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "100%",
                "id": "flxAttrHeader",
                "isVisible": true,
                "layoutType": kony.flex.FLOW_HORIZONTAL,
                "left": "1%",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0dp",
                "width": "24%"
            }, {}, {});
            flxAttrHeader.setDefaultUnit(kony.flex.DP);
            var lblAttributeHeader = new kony.ui.Label({
                "id": "lblAttributeHeader",
                "isVisible": true,
                "left": "0dp",
                "skin": "sknLbl73757C11px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmAdManagement.AttributeCAPS\")",
                "top": "21dp",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var lblSortAttribute = new kony.ui.Label({
                "id": "lblSortAttribute",
                "isVisible": true,
                "left": "7dp",
                "skin": "sknIcon15px",
                "text": "",
                "top": "21dp",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {
                "hoverSkin": "sknlblCursorFont"
            });
            flxAttrHeader.add(lblAttributeHeader, lblSortAttribute);
            var lblAttributeCriteriaHeader = new kony.ui.Label({
                "id": "lblAttributeCriteriaHeader",
                "isVisible": true,
                "left": "1%",
                "skin": "sknLbl73757C11px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmAdManagement.CriteriaCAPS\")",
                "top": "21dp",
                "width": "24%",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var lblAttributeValueHeader = new kony.ui.Label({
                "id": "lblAttributeValueHeader",
                "isVisible": true,
                "left": "1%",
                "skin": "sknLbl73757C11px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmAdManagement.ValueCAPS\")",
                "top": "21dp",
                "width": "24%",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxAttrHeaderRow.add(flxDatasetsHeader, flxAttrHeader, lblAttributeCriteriaHeader, lblAttributeValueHeader);
            var segSelectedAttributes = new kony.ui.SegmentedUI2({
                "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                "data": [{
                    "lblAttribute": "Attribute",
                    "lblCriteria": "Criteria",
                    "lblDataset": "DATASET",
                    "lblValue": "Value"
                }, {
                    "lblAttribute": "Attribute",
                    "lblCriteria": "Criteria",
                    "lblDataset": "DATASET",
                    "lblValue": "Value"
                }, {
                    "lblAttribute": "Attribute",
                    "lblCriteria": "Criteria",
                    "lblDataset": "DATASET",
                    "lblValue": "Value"
                }, {
                    "lblAttribute": "Attribute",
                    "lblCriteria": "Criteria",
                    "lblDataset": "DATASET",
                    "lblValue": "Value"
                }, {
                    "lblAttribute": "Attribute",
                    "lblCriteria": "Criteria",
                    "lblDataset": "DATASET",
                    "lblValue": "Value"
                }, {
                    "lblAttribute": "Attribute",
                    "lblCriteria": "Criteria",
                    "lblDataset": "DATASET",
                    "lblValue": "Value"
                }, {
                    "lblAttribute": "Attribute",
                    "lblCriteria": "Criteria",
                    "lblDataset": "DATASET",
                    "lblValue": "Value"
                }, {
                    "lblAttribute": "Attribute",
                    "lblCriteria": "Criteria",
                    "lblDataset": "DATASET",
                    "lblValue": "Value"
                }, {
                    "lblAttribute": "Attribute",
                    "lblCriteria": "Criteria",
                    "lblDataset": "DATASET",
                    "lblValue": "Value"
                }, {
                    "lblAttribute": "Attribute",
                    "lblCriteria": "Criteria",
                    "lblDataset": "DATASET",
                    "lblValue": "Value"
                }],
                "groupCells": false,
                "id": "segSelectedAttributes",
                "isVisible": true,
                "left": "0dp",
                "needPageIndicator": true,
                "pageOffDotImage": "pageoffdot.png",
                "pageOnDotImage": "pageondot.png",
                "retainSelection": false,
                "rowFocusSkin": "seg2Focus",
                "rowSkin": "seg2Normal",
                "rowTemplate": "flxAttributeRow",
                "sectionHeaderSkin": "sliPhoneSegmentHeader",
                "selectionBehavior": constants.SEGUI_DEFAULT_BEHAVIOR,
                "separatorColor": "d7d9e000",
                "separatorRequired": true,
                "separatorThickness": 1,
                "showScrollbars": false,
                "top": "0dp",
                "viewType": constants.SEGUI_VIEW_TYPE_TABLEVIEW,
                "widgetDataMap": {
                    "flxAttributeRow": "flxAttributeRow",
                    "flxRow": "flxRow",
                    "lblAttribute": "lblAttribute",
                    "lblCriteria": "lblCriteria",
                    "lblDataset": "lblDataset",
                    "lblValue": "lblValue"
                },
                "width": "100%",
                "zIndex": 1
            }, {
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxAddAttributes.add(flxAttrHeaderRow, segSelectedAttributes);
            var datasetFilter = new com.adminConsole.common.statusFilterMenu({
                "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                "id": "datasetFilter",
                "isVisible": false,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "masterType": constants.MASTER_TYPE_DEFAULT,
                "maxWidth": "50%",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "40dp",
                "width": "30%",
                "zIndex": 5,
                "overrides": {
                    "statusFilterMenu": {
                        "isVisible": false,
                        "maxWidth": "50%",
                        "top": "40dp",
                        "width": "30%"
                    }
                }
            }, {
                "overrides": {}
            }, {
                "overrides": {}
            });
            flxAttrContainer.add(flxAddAttributes, datasetFilter);
            var NoAttributesError = new com.adminConsole.loans.applications.errorMsg({
                "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                "id": "NoAttributesError",
                "isVisible": false,
                "layoutType": kony.flex.FLOW_HORIZONTAL,
                "left": "0dp",
                "masterType": constants.MASTER_TYPE_DEFAULT,
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "10dp",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "overrides": {
                    "errorMsg": {
                        "isVisible": false
                    },
                    "lblErrorText": {
                        "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmAdManagement.NoAttributes\")"
                    }
                }
            }, {
                "overrides": {}
            }, {
                "overrides": {}
            });
            flxAttributesContainer.add(flxNoAttributes, flxAttrContainer, NoAttributesError);
            flxAddRole.add(flxCreateRole, lblRoleSeperator, flxRoleName, txtRoleName, RoleNameError, flxRoleDescription, txtRoleDescription, RoleDescError, flxAttrSection, flxAttributesContainer);
            var flxViewDetails = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                "clipBounds": true,
                "id": "flxViewDetails",
                "isVisible": false,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0px",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0px",
                "width": "100%",
                "zIndex": 10
            }, {}, {});
            flxViewDetails.setDefaultUnit(kony.flex.DP);
            var flxSelectedCustomerRoles = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "30dp",
                "id": "flxSelectedCustomerRoles",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0px",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "20px",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {}, {});
            flxSelectedCustomerRoles.setDefaultUnit(kony.flex.DP);
            var lblSelectedCustomerRoles = new kony.ui.Label({
                "id": "lblSelectedCustomerRoles",
                "isVisible": true,
                "left": "0px",
                "skin": "sknLbl192b45LatoReg16px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmAdManagement.selectedCustomerSegment_s\")",
                "top": "0px",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var flxProfileButtons = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "22dp",
                "id": "flxProfileButtons",
                "isVisible": true,
                "layoutType": kony.flex.FLOW_HORIZONTAL,
                "reverseLayoutDirection": true,
                "isModalContainer": false,
                "right": 0,
                "skin": "slFbox",
                "top": "0dp",
                "width": "400dp",
                "zIndex": 10
            }, {}, {});
            flxProfileButtons.setDefaultUnit(kony.flex.DP);
            var btnExistingProfiles = new kony.ui.Button({
                "focusSkin": "sknbtnffffffLatoRegular4f555dBorder1px485c75",
                "height": "100%",
                "id": "btnExistingProfiles",
                "isVisible": true,
                "left": "0dp",
                "skin": "sknbtnf7f7faLatoRegular4f555dBorder1px8b96a5",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmAdManagement.AddSegment_s\")",
                "top": "0",
                "width": "140dp",
                "zIndex": 10
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_CENTER,
                "displayText": true,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {
                "hoverSkin": "sknbtnffffffLatoRegular4f555dBorder1px485c75"
            });
            var btnNewProfile = new kony.ui.Button({
                "focusSkin": "sknbtnffffffLatoRegular4f555dBorder1px485c75",
                "height": "100%",
                "id": "btnNewProfile",
                "isVisible": false,
                "left": "15dp",
                "skin": "sknbtnf7f7faLatoRegular4f555dBorder1px8b96a5",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmAdManagement.CreateNewProfile\")",
                "top": "0",
                "width": "190dp",
                "zIndex": 10
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_CENTER,
                "displayText": true,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {
                "hoverSkin": "sknbtnffffffLatoRegular4f555dBorder1px485c75"
            });
            flxProfileButtons.add(btnExistingProfiles, btnNewProfile);
            flxSelectedCustomerRoles.add(lblSelectedCustomerRoles, flxProfileButtons);
            var flxTargetCustSegHeaderData = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "centerX": "50%",
                "clipBounds": true,
                "height": "50px",
                "id": "flxTargetCustSegHeaderData",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0px",
                "isModalContainer": false,
                "right": "0px",
                "skin": "slFbox",
                "top": "51px",
                "zIndex": 1
            }, {}, {});
            flxTargetCustSegHeaderData.setDefaultUnit(kony.flex.DP);
            var flxRoleNameHeader = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                "centerY": "50%",
                "clipBounds": true,
                "id": "flxRoleNameHeader",
                "isVisible": true,
                "layoutType": kony.flex.FLOW_HORIZONTAL,
                "left": "15px",
                "isModalContainer": false,
                "skin": "hoverhandSkin",
                "width": "125dp"
            }, {}, {});
            flxRoleNameHeader.setDefaultUnit(kony.flex.DP);
            var lblRoleNameHeader = new kony.ui.Label({
                "height": "16px",
                "id": "lblRoleNameHeader",
                "isVisible": true,
                "left": "0px",
                "skin": "sknLbl485C75LatoRegular13Px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.ProfileManagement.segmentNameCaps\")",
                "top": "0px",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var fontIconRoleName = new kony.ui.Label({
                "id": "fontIconRoleName",
                "isVisible": true,
                "left": "5px",
                "skin": "sknIcon15px",
                "text": "",
                "top": "0px",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxRoleNameHeader.add(lblRoleNameHeader, fontIconRoleName);
            var flxDescriptionHeader = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                "centerY": "50%",
                "clipBounds": true,
                "id": "flxDescriptionHeader",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "23%",
                "isModalContainer": false,
                "skin": "slFbox",
                "width": "90dp"
            }, {}, {});
            flxDescriptionHeader.setDefaultUnit(kony.flex.DP);
            var lblDescHeader = new kony.ui.Label({
                "height": "16px",
                "id": "lblDescHeader",
                "isVisible": true,
                "left": "0px",
                "skin": "sknLbl485C75LatoRegular13Px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmAdManagement.DescriptionCAPS\")",
                "top": "0px",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxDescriptionHeader.add(lblDescHeader);
            var flxNoOfUsersHeader = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                "centerY": "50%",
                "clipBounds": true,
                "id": "flxNoOfUsersHeader",
                "isVisible": true,
                "layoutType": kony.flex.FLOW_HORIZONTAL,
                "left": "60%",
                "isModalContainer": false,
                "skin": "hoverhandSkin",
                "width": "140dp"
            }, {}, {});
            flxNoOfUsersHeader.setDefaultUnit(kony.flex.DP);
            var lblNoOfUsersHeader = new kony.ui.Label({
                "height": "16px",
                "id": "lblNoOfUsersHeader",
                "isVisible": true,
                "left": "0px",
                "skin": "sknLbl485C75LatoRegular13Px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmAdManagement.UsersAvaialbleCAPS\")",
                "top": "0px",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var fontIconNoOfUsers = new kony.ui.Label({
                "id": "fontIconNoOfUsers",
                "isVisible": true,
                "left": "5px",
                "skin": "sknIcon15px",
                "text": "",
                "top": "0px",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxNoOfUsersHeader.add(lblNoOfUsersHeader, fontIconNoOfUsers);
            var flxAttributesHeader = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                "centerY": "50%",
                "clipBounds": true,
                "id": "flxAttributesHeader",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "82%",
                "isModalContainer": false,
                "skin": "slFbox",
                "width": "13%",
                "zIndex": 1
            }, {}, {});
            flxAttributesHeader.setDefaultUnit(kony.flex.DP);
            var lblAttributesHeader = new kony.ui.Label({
                "id": "lblAttributesHeader",
                "isVisible": true,
                "left": "0px",
                "skin": "sknLbl485C75LatoRegular13Px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmAdManagement.AttributesCAPS\")",
                "top": "0px",
                "width": "100%",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxAttributesHeader.add(lblAttributesHeader);
            flxTargetCustSegHeaderData.add(flxRoleNameHeader, flxDescriptionHeader, flxNoOfUsersHeader, flxAttributesHeader);
            var flxTargetCustSegData = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                "centerX": "50%",
                "clipBounds": true,
                "id": "flxTargetCustSegData",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0px",
                "isModalContainer": false,
                "right": "0px",
                "skin": "slFbox",
                "top": "101px",
                "zIndex": 1
            }, {}, {});
            flxTargetCustSegData.setDefaultUnit(kony.flex.DP);
            var lblViewAttrSep2 = new kony.ui.Label({
                "bottom": "0px",
                "height": "1px",
                "id": "lblViewAttrSep2",
                "isVisible": true,
                "left": "0px",
                "skin": "sknLblTableHeaderLine",
                "text": "-",
                "top": "0dp",
                "width": "100%",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var segTargetCustomerRoles = new kony.ui.SegmentedUI2({
                "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                "data": [{
                    "lblAttributes": "View",
                    "lblDeleteIcon": "",
                    "lblRoleDescription": "All customers list with DBX platinum  card holders, Includes new addition of 2018 -2019.",
                    "lblRoleName": "Platinum DBX Customers",
                    "lblSeparator": "Label",
                    "lblUserCount": "10283"
                }, {
                    "lblAttributes": "View",
                    "lblDeleteIcon": "",
                    "lblRoleDescription": "All customers list with DBX platinum  card holders, Includes new addition of 2018 -2019.",
                    "lblRoleName": "Platinum DBX Customers",
                    "lblSeparator": "Label",
                    "lblUserCount": "10283"
                }, {
                    "lblAttributes": "View",
                    "lblDeleteIcon": "",
                    "lblRoleDescription": "All customers list with DBX platinum  card holders, Includes new addition of 2018 -2019.",
                    "lblRoleName": "Platinum DBX Customers",
                    "lblSeparator": "Label",
                    "lblUserCount": "10283"
                }, {
                    "lblAttributes": "View",
                    "lblDeleteIcon": "",
                    "lblRoleDescription": "All customers list with DBX platinum  card holders, Includes new addition of 2018 -2019.",
                    "lblRoleName": "Platinum DBX Customers",
                    "lblSeparator": "Label",
                    "lblUserCount": "10283"
                }, {
                    "lblAttributes": "View",
                    "lblDeleteIcon": "",
                    "lblRoleDescription": "All customers list with DBX platinum  card holders, Includes new addition of 2018 -2019.",
                    "lblRoleName": "Platinum DBX Customers",
                    "lblSeparator": "Label",
                    "lblUserCount": "10283"
                }, {
                    "lblAttributes": "View",
                    "lblDeleteIcon": "",
                    "lblRoleDescription": "All customers list with DBX platinum  card holders, Includes new addition of 2018 -2019.",
                    "lblRoleName": "Platinum DBX Customers",
                    "lblSeparator": "Label",
                    "lblUserCount": "10283"
                }, {
                    "lblAttributes": "View",
                    "lblDeleteIcon": "",
                    "lblRoleDescription": "All customers list with DBX platinum  card holders, Includes new addition of 2018 -2019.",
                    "lblRoleName": "Platinum DBX Customers",
                    "lblSeparator": "Label",
                    "lblUserCount": "10283"
                }, {
                    "lblAttributes": "View",
                    "lblDeleteIcon": "",
                    "lblRoleDescription": "All customers list with DBX platinum  card holders, Includes new addition of 2018 -2019.",
                    "lblRoleName": "Platinum DBX Customers",
                    "lblSeparator": "Label",
                    "lblUserCount": "10283"
                }, {
                    "lblAttributes": "View",
                    "lblDeleteIcon": "",
                    "lblRoleDescription": "All customers list with DBX platinum  card holders, Includes new addition of 2018 -2019.",
                    "lblRoleName": "Platinum DBX Customers",
                    "lblSeparator": "Label",
                    "lblUserCount": "10283"
                }, {
                    "lblAttributes": "View",
                    "lblDeleteIcon": "",
                    "lblRoleDescription": "All customers list with DBX platinum  card holders, Includes new addition of 2018 -2019.",
                    "lblRoleName": "Platinum DBX Customers",
                    "lblSeparator": "Label",
                    "lblUserCount": "10283"
                }],
                "groupCells": false,
                "id": "segTargetCustomerRoles",
                "isVisible": true,
                "left": "0px",
                "needPageIndicator": true,
                "pageOffDotImage": "pageoffdot.png",
                "pageOnDotImage": "pageondot.png",
                "retainSelection": false,
                "rowFocusSkin": "seg2Focus",
                "rowSkin": "seg2Normal",
                "rowTemplate": "flxTargetCustRoles",
                "sectionHeaderSkin": "sliPhoneSegmentHeader",
                "selectionBehavior": constants.SEGUI_DEFAULT_BEHAVIOR,
                "separatorRequired": false,
                "showScrollbars": false,
                "top": "1dp",
                "viewType": constants.SEGUI_VIEW_TYPE_TABLEVIEW,
                "widgetDataMap": {
                    "flxAttributesContainer": "flxAttributesContainer",
                    "flxIconDelete": "flxIconDelete",
                    "flxTargetCustRoles": "flxTargetCustRoles",
                    "lblAttributes": "lblAttributes",
                    "lblDeleteIcon": "lblDeleteIcon",
                    "lblRoleDescription": "lblRoleDescription",
                    "lblRoleName": "lblRoleName",
                    "lblSeparator": "lblSeparator",
                    "lblUserCount": "lblUserCount"
                },
                "width": "100%",
                "zIndex": 1
            }, {
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxTargetCustSegData.add(lblViewAttrSep2, segTargetCustomerRoles);
            flxViewDetails.add(flxSelectedCustomerRoles, flxTargetCustSegHeaderData, flxTargetCustSegData);
            flxRoles.add(flxNoRoleError, flxNoRoles, flxAddRole, flxViewDetails);
            flxTargetCustomers.add(flxCustomer, flxRoles);
            var flxViewEvents = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                "clipBounds": true,
                "id": "flxViewEvents",
                "isVisible": false,
                "layoutType": kony.flex.FREE_FORM,
                "left": "20dp",
                "isModalContainer": false,
                "right": 20,
                "skin": "slFbox",
                "top": "0dp"
            }, {}, {});
            flxViewEvents.setDefaultUnit(kony.flex.DP);
            var flxViewEventsInner = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                "clipBounds": true,
                "id": "flxViewEventsInner",
                "isVisible": true,
                "layoutType": kony.flex.FLOW_VERTICAL,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0dp",
                "width": "100%"
            }, {}, {});
            flxViewEventsInner.setDefaultUnit(kony.flex.DP);
            var flxNoEventsError = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "40dp",
                "id": "flxNoEventsError",
                "isVisible": false,
                "layoutType": kony.flex.FLOW_HORIZONTAL,
                "left": "0",
                "isModalContainer": false,
                "skin": "sknBorderRed",
                "top": "20dp",
                "width": "100%",
                "zIndex": 10
            }, {}, {});
            flxNoEventsError.setDefaultUnit(kony.flex.DP);
            var flxNoEventsErrorIcon = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "100%",
                "id": "flxNoEventsErrorIcon",
                "isVisible": true,
                "layoutType": kony.flex.FLOW_HORIZONTAL,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "sknRedFill",
                "top": "0dp",
                "width": "10%"
            }, {}, {});
            flxNoEventsErrorIcon.setDefaultUnit(kony.flex.DP);
            var lblNoEventsErrorIcon = new kony.ui.Label({
                "centerX": "50%",
                "centerY": "50%",
                "height": "20dp",
                "id": "lblNoEventsErrorIcon",
                "isVisible": true,
                "left": "0dp",
                "skin": "sknIcon20pxWhite",
                "text": "",
                "top": "0dp",
                "width": "20dp",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxNoEventsErrorIcon.add(lblNoEventsErrorIcon);
            var lblNoEventsErrorText = new kony.ui.Label({
                "centerY": "50%",
                "height": "15dp",
                "id": "lblNoEventsErrorText",
                "isVisible": true,
                "left": "20dp",
                "skin": "sknlblError",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmAdManagement.errorMsg_OneEventRequired\")",
                "top": "0dp",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxNoEventsError.add(flxNoEventsErrorIcon, lblNoEventsErrorText);
            var viewPopupEvents = new com.adminConsole.adManagement.viewPopupEvents({
                "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                "id": "viewPopupEvents",
                "isVisible": true,
                "layoutType": kony.flex.FLOW_VERTICAL,
                "left": "0dp",
                "masterType": constants.MASTER_TYPE_DEFAULT,
                "isModalContainer": false,
                "skin": "sknFlxbgFFFFFFbdrD7D9E0rd4px",
                "top": "20dp",
                "width": "100%",
                "overrides": {
                    "lblScreenNameHeader": {
                        "text": "SCREEN NAME"
                    },
                    "lblSeparator2": {
                        "top": "0dp"
                    },
                    "viewPopupEvents": {
                        "top": "20dp"
                    }
                }
            }, {
                "overrides": {}
            }, {
                "overrides": {}
            });
            var viewInternalEvents = new com.adminConsole.adManagement.viewInternalExternalEvents({
                "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                "id": "viewInternalEvents",
                "isVisible": true,
                "layoutType": kony.flex.FLOW_VERTICAL,
                "left": "0dp",
                "masterType": constants.MASTER_TYPE_DEFAULT,
                "isModalContainer": false,
                "skin": "sknFlxbgFFFFFFbdrD7D9E0rd4px",
                "top": "10dp",
                "width": "100%",
                "overrides": {
                    "segEvents": {
                        "data": [{
                            "lblDelete": "kony.i18n.getLocalizedString(\"i18n.frmFAQ.fonticonDelete\")",
                            "lblEvent": "Events Name",
                            "lblEventCode": "Events Name",
                            "lblEventSource": "Events Name",
                            "lblSeparator": "Label"
                        }, {
                            "lblDelete": "",
                            "lblEvent": "Events Name",
                            "lblEventCode": "Events Name",
                            "lblEventSource": "Events Name",
                            "lblSeparator": "Label"
                        }, {
                            "lblDelete": "",
                            "lblEvent": "Events Name",
                            "lblEventCode": "Events Name",
                            "lblEventSource": "Events Name",
                            "lblSeparator": "Label"
                        }]
                    },
                    "viewInternalExternalEvents": {
                        "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                        "isVisible": true,
                        "left": "0dp",
                        "right": "viz.val_cleared",
                        "top": "10dp",
                        "width": "100%"
                    }
                }
            }, {
                "overrides": {}
            }, {
                "overrides": {}
            });
            var viewExternalEvents = new com.adminConsole.adManagement.viewInternalExternalEvents({
                "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                "id": "viewExternalEvents",
                "isVisible": true,
                "layoutType": kony.flex.FLOW_VERTICAL,
                "left": "0dp",
                "masterType": constants.MASTER_TYPE_DEFAULT,
                "isModalContainer": false,
                "skin": "sknFlxbgFFFFFFbdrD7D9E0rd4px",
                "top": "10dp",
                "width": "100%",
                "overrides": {
                    "lblEventsHeader": {
                        "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmAdManagement.externalEvents\")"
                    },
                    "segEvents": {
                        "data": [{
                            "lblDelete": "kony.i18n.getLocalizedString(\"i18n.frmFAQ.fonticonDelete\")",
                            "lblEvent": "Events Name",
                            "lblEventCode": "Events Name",
                            "lblEventSource": "Events Name",
                            "lblSeparator": "Label"
                        }, {
                            "lblDelete": "",
                            "lblEvent": "Events Name",
                            "lblEventCode": "Events Name",
                            "lblEventSource": "Events Name",
                            "lblSeparator": "Label"
                        }, {
                            "lblDelete": "",
                            "lblEvent": "Events Name",
                            "lblEventCode": "Events Name",
                            "lblEventSource": "Events Name",
                            "lblSeparator": "Label"
                        }]
                    },
                    "viewInternalExternalEvents": {
                        "top": "10dp"
                    }
                }
            }, {
                "overrides": {}
            }, {
                "overrides": {}
            });
            flxViewEventsInner.add(flxNoEventsError, viewPopupEvents, viewInternalEvents, viewExternalEvents);
            flxViewEvents.add(flxViewEventsInner);
            var flxOfflineChannels = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                "clipBounds": true,
                "id": "flxOfflineChannels",
                "isVisible": false,
                "layoutType": kony.flex.FLOW_VERTICAL,
                "left": "0dp",
                "isModalContainer": false,
                "right": "0dp",
                "skin": "slFbox",
                "top": "0dp",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {}, {});
            flxOfflineChannels.setDefaultUnit(kony.flex.DP);
            var flxSMSChannelParent = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                "clipBounds": true,
                "id": "flxSMSChannelParent",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "20dp",
                "width": "100%"
            }, {}, {});
            flxSMSChannelParent.setDefaultUnit(kony.flex.DP);
            var flxSMSChannel = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                "clipBounds": true,
                "id": "flxSMSChannel",
                "isVisible": true,
                "layoutType": kony.flex.FLOW_VERTICAL,
                "left": "20dp",
                "isModalContainer": false,
                "right": "25dp",
                "skin": "sknFlxbgFFFFFFbdrD7D9E0rd4px",
                "top": "0px",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {}, {});
            flxSMSChannel.setDefaultUnit(kony.flex.DP);
            var flxSMSHeaderDrop = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "40px",
                "id": "flxSMSHeaderDrop",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0px",
                "isModalContainer": false,
                "skin": "sknflxf5f6f8Op100",
                "top": "0px",
                "width": "100%",
                "zIndex": 1
            }, {}, {});
            flxSMSHeaderDrop.setDefaultUnit(kony.flex.DP);
            var lblDropDownIcon1 = new kony.ui.Label({
                "centerY": "50%",
                "height": "16px",
                "id": "lblDropDownIcon1",
                "isVisible": true,
                "left": "12px",
                "skin": "sknLblIconMoon16px192b45",
                "text": "",
                "top": "0px",
                "width": "16px",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var lblSMSHeader = new kony.ui.Label({
                "centerY": "50%",
                "id": "lblSMSHeader",
                "isVisible": true,
                "left": "40px",
                "skin": "sknLatoRegular192B4516px",
                "text": "SMS",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxSMSHeaderDrop.add(lblDropDownIcon1, lblSMSHeader);
            var flxSMSDetails = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "bottom": "15px",
                "clipBounds": true,
                "height": "160px",
                "id": "flxSMSDetails",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0px",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "20px",
                "width": "100%",
                "zIndex": 1
            }, {}, {});
            flxSMSDetails.setDefaultUnit(kony.flex.DP);
            var flxSMSContent = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "20px",
                "id": "flxSMSContent",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0px",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0px",
                "width": "100%",
                "zIndex": 1
            }, {}, {});
            flxSMSContent.setDefaultUnit(kony.flex.DP);
            var lblSMSContent = new kony.ui.Label({
                "height": "16px",
                "id": "lblSMSContent",
                "isVisible": true,
                "left": "15px",
                "skin": "LblLatoRegular485c7516px",
                "text": "SMS Content",
                "top": "0px",
                "width": "100px",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var lblSMSCount = new kony.ui.Label({
                "height": "14px",
                "id": "lblSMSCount",
                "isVisible": true,
                "right": "15px",
                "skin": "sknlblLatoReg485c7513px",
                "top": "0dp",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxSMSContent.add(lblSMSContent, lblSMSCount);
            var txtAreaSMSContent = new kony.ui.TextArea2({
                "autoCapitalize": constants.TEXTAREA_AUTO_CAPITALIZE_NONE,
                "focusSkin": "defTextAreaFocus",
                "height": "128px",
                "id": "txtAreaSMSContent",
                "isVisible": true,
                "keyBoardStyle": constants.TEXTAREA_KEY_BOARD_STYLE_DEFAULT,
                "left": "15px",
                "maxTextLength": 200,
                "numberOfVisibleLines": 3,
                "placeholder": "SMS Content",
                "right": "15dp",
                "skin": "skntxtAread7d9e0",
                "textInputMode": constants.TEXTAREA_INPUT_MODE_ANY,
                "top": "32px",
                "zIndex": 2
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_TOP_LEFT,
                "padding": [2, 2, 2, 2],
                "paddingInPixel": false
            }, {
                "autoCorrect": false,
                "placeholderSkin": "defTextAreaPlaceholder"
            });
            flxSMSDetails.add(flxSMSContent, txtAreaSMSContent);
            var SMSError = new com.adminConsole.loans.applications.errorMsg({
                "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                "bottom": "20px",
                "id": "SMSError",
                "isVisible": false,
                "layoutType": kony.flex.FLOW_HORIZONTAL,
                "left": "15dp",
                "masterType": constants.MASTER_TYPE_DEFAULT,
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0px",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "overrides": {
                    "errorMsg": {
                        "bottom": "20px",
                        "isVisible": false,
                        "left": "15dp",
                        "top": "0px"
                    },
                    "lblErrorText": {
                        "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmAdManagement.SMSContentCannotbeEmpty\")"
                    }
                }
            }, {
                "overrides": {}
            }, {
                "overrides": {}
            });
            flxSMSChannel.add(flxSMSHeaderDrop, flxSMSDetails, SMSError);
            flxSMSChannelParent.add(flxSMSChannel);
            var flxEmailChannelParent = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                "clipBounds": true,
                "id": "flxEmailChannelParent",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "10dp",
                "width": "100%"
            }, {}, {});
            flxEmailChannelParent.setDefaultUnit(kony.flex.DP);
            var flxEmailChannel = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                "clipBounds": true,
                "id": "flxEmailChannel",
                "isVisible": true,
                "layoutType": kony.flex.FLOW_VERTICAL,
                "left": "20dp",
                "isModalContainer": false,
                "right": "25dp",
                "skin": "sknFlxbgFFFFFFbdrD7D9E0rd4px",
                "top": "0px",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {}, {});
            flxEmailChannel.setDefaultUnit(kony.flex.DP);
            var flxEmailHeaderDrop = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "40px",
                "id": "flxEmailHeaderDrop",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0px",
                "isModalContainer": false,
                "skin": "sknflxf5f6f8Op100",
                "top": "0px",
                "width": "100%",
                "zIndex": 1
            }, {}, {});
            flxEmailHeaderDrop.setDefaultUnit(kony.flex.DP);
            var lblDropDownIcon2 = new kony.ui.Label({
                "centerY": "50%",
                "height": "16px",
                "id": "lblDropDownIcon2",
                "isVisible": true,
                "left": "12px",
                "skin": "sknLblIconMoon16px192b45",
                "text": "",
                "width": "16px",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var lblEmailHeader = new kony.ui.Label({
                "centerY": "50%",
                "id": "lblEmailHeader",
                "isVisible": true,
                "left": "40px",
                "skin": "sknLatoRegular192B4516px",
                "text": "Email",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var btnEmailPreview = new kony.ui.Button({
                "centerY": "50%",
                "focusSkin": "sknbtnf7f7faLatoRegular12Px485c75Border1px485C75Radius20px",
                "height": "22px",
                "id": "btnEmailPreview",
                "isVisible": true,
                "right": "15px",
                "skin": "sknbtnf7f7faLatoRegular12Px485c75Border1px485C75Radius20px",
                "text": "Preview",
                "width": "64px",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_CENTER,
                "displayText": true,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {
                "hoverSkin": "sknbtnffffffLatoRegular12Px485c75Border1px485C75Radius20px"
            });
            flxEmailHeaderDrop.add(lblDropDownIcon2, lblEmailHeader, btnEmailPreview);
            var flxEmailDetails = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                "clipBounds": true,
                "id": "flxEmailDetails",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0px",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "20px",
                "width": "100%",
                "zIndex": 1
            }, {}, {});
            flxEmailDetails.setDefaultUnit(kony.flex.DP);
            var flxEmailContent = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "20px",
                "id": "flxEmailContent",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0px",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0px",
                "width": "100%",
                "zIndex": 1
            }, {}, {});
            flxEmailContent.setDefaultUnit(kony.flex.DP);
            var lblEmailContent = new kony.ui.Label({
                "height": "16px",
                "id": "lblEmailContent",
                "isVisible": true,
                "left": "15px",
                "skin": "LblLatoRegular485c7516px",
                "text": "Email Content",
                "top": "0px",
                "width": "100px",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxEmailContent.add(lblEmailContent);
            var flxEmailSubject = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "40px",
                "id": "flxEmailSubject",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "15px",
                "isModalContainer": false,
                "right": "15px",
                "skin": "sknFlxbgFFFFFFbdrD7D9E0rd3px",
                "top": "34px",
                "zIndex": 1
            }, {}, {});
            flxEmailSubject.setDefaultUnit(kony.flex.DP);
            var lblSubjectTag = new kony.ui.Label({
                "centerY": "50%",
                "id": "lblSubjectTag",
                "isVisible": true,
                "left": "17px",
                "skin": "sknLbl485C75LatoRegular13Px",
                "text": "Subject: ",
                "width": "47px",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var txtFieldSubject = new kony.ui.TextBox2({
                "autoCapitalize": constants.TEXTBOX_AUTO_CAPITALIZE_NONE,
                "centerY": "50%",
                "height": "100%",
                "id": "txtFieldSubject",
                "isVisible": true,
                "keyBoardStyle": constants.TEXTBOX_KEY_BOARD_STYLE_DEFAULT,
                "left": "75px",
                "right": "0px",
                "secureTextEntry": false,
                "skin": "sknTbx485c75LatoReg13pxNoBor",
                "textInputMode": constants.TEXTBOX_INPUT_MODE_ANY,
                "zIndex": 1
            }, {
                "containerHeightMode": constants.TEXTBOX_FONT_METRICS_DRIVEN_HEIGHT,
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {
                "autoCorrect": false,
                "placeholderSkin": "defTextBoxPlaceholder"
            });
            flxEmailSubject.add(lblSubjectTag, txtFieldSubject);
            var EmailSubjectError = new com.adminConsole.loans.applications.errorMsg({
                "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                "id": "EmailSubjectError",
                "isVisible": false,
                "layoutType": kony.flex.FLOW_HORIZONTAL,
                "left": "15px",
                "masterType": constants.MASTER_TYPE_DEFAULT,
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "74px",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "overrides": {
                    "errorMsg": {
                        "isVisible": false,
                        "left": "15px",
                        "top": "74px"
                    },
                    "lblErrorText": {
                        "text": "Email Subject cannot be empty."
                    }
                }
            }, {
                "overrides": {}
            }, {
                "overrides": {}
            });
            var flxRichTextParent = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "bottom": "15px",
                "clipBounds": true,
                "height": "200px",
                "id": "flxRichTextParent",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "15px",
                "isModalContainer": false,
                "right": "15px",
                "skin": "sknFlxbgFFFFFFbdrD7D9E0rd3px",
                "top": "94px"
            }, {}, {});
            flxRichTextParent.setDefaultUnit(kony.flex.DP);
            var rtxEmailValue = new kony.ui.Browser({
                "bottom": "1px",
                "detectTelNumber": true,
                "enableZoom": false,
                "id": "rtxEmailValue",
                "isVisible": true,
                "left": "1px",
                "setAsContent": false,
                "requestURLConfig": {
                    "URL": "richtext.html",
                    "requestMethod": constants.BROWSER_REQUEST_METHOD_GET
                },
                "right": "1px",
                "top": "1px",
                "zIndex": 10
            }, {}, {});
            flxRichTextParent.add(rtxEmailValue);
            flxEmailDetails.add(flxEmailContent, flxEmailSubject, EmailSubjectError, flxRichTextParent);
            var EmailError = new com.adminConsole.loans.applications.errorMsg({
                "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                "bottom": "20px",
                "id": "EmailError",
                "isVisible": false,
                "layoutType": kony.flex.FLOW_HORIZONTAL,
                "left": "15dp",
                "masterType": constants.MASTER_TYPE_DEFAULT,
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0px",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "overrides": {
                    "errorMsg": {
                        "bottom": "20px",
                        "isVisible": false,
                        "left": "15dp",
                        "top": "0px"
                    },
                    "lblErrorText": {
                        "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmAdManagement.EmailContentCannotbeEmpty\")"
                    }
                }
            }, {
                "overrides": {}
            }, {
                "overrides": {}
            });
            flxEmailChannel.add(flxEmailHeaderDrop, flxEmailDetails, EmailError);
            flxEmailChannelParent.add(flxEmailChannel);
            var flxPushNotificationsParent = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                "bottom": "30dp",
                "clipBounds": true,
                "id": "flxPushNotificationsParent",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "10dp",
                "width": "100%"
            }, {}, {});
            flxPushNotificationsParent.setDefaultUnit(kony.flex.DP);
            var flxPushNotificationsChannel = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                "clipBounds": true,
                "id": "flxPushNotificationsChannel",
                "isVisible": true,
                "layoutType": kony.flex.FLOW_VERTICAL,
                "left": "20dp",
                "isModalContainer": false,
                "right": "25dp",
                "skin": "sknFlxbgFFFFFFbdrD7D9E0rd4px",
                "top": "0px",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {}, {});
            flxPushNotificationsChannel.setDefaultUnit(kony.flex.DP);
            var flxPushNotificationsDrop = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "40px",
                "id": "flxPushNotificationsDrop",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0px",
                "isModalContainer": false,
                "skin": "sknflxf5f6f8Op100",
                "top": "0px",
                "width": "100%",
                "zIndex": 1
            }, {}, {});
            flxPushNotificationsDrop.setDefaultUnit(kony.flex.DP);
            var lblDropDownIcon3 = new kony.ui.Label({
                "centerY": "50%",
                "height": "16px",
                "id": "lblDropDownIcon3",
                "isVisible": true,
                "left": "12px",
                "skin": "sknLblIconMoon16px192b45",
                "text": "",
                "width": "16px",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var lblPushNotificationsHeader = new kony.ui.Label({
                "centerY": "50%",
                "id": "lblPushNotificationsHeader",
                "isVisible": true,
                "left": "40px",
                "skin": "sknLatoRegular192B4516px",
                "text": "Push Notifications",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxPushNotificationsDrop.add(lblDropDownIcon3, lblPushNotificationsHeader);
            var flxPushNotificationsDetails = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                "bottom": "15px",
                "clipBounds": true,
                "id": "flxPushNotificationsDetails",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0px",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "20px",
                "width": "100%",
                "zIndex": 1
            }, {}, {});
            flxPushNotificationsDetails.setDefaultUnit(kony.flex.DP);
            var flxPushNotificationsContent = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "20px",
                "id": "flxPushNotificationsContent",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0px",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0px",
                "width": "100%",
                "zIndex": 1
            }, {}, {});
            flxPushNotificationsContent.setDefaultUnit(kony.flex.DP);
            var lblPushNotificationsContent = new kony.ui.Label({
                "height": "16px",
                "id": "lblPushNotificationsContent",
                "isVisible": true,
                "left": "15px",
                "skin": "LblLatoRegular485c7516px",
                "text": "Push Notifications",
                "top": "0px",
                "width": "150px",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var lblPushNotificationsCount = new kony.ui.Label({
                "height": "14px",
                "id": "lblPushNotificationsCount",
                "isVisible": false,
                "right": "15px",
                "skin": "sknlblLatoReg485c7513px",
                "top": "0dp",
                "width": "40px",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxPushNotificationsContent.add(lblPushNotificationsContent, lblPushNotificationsCount);
            var txtAreaPushNotificationsValue = new kony.ui.TextArea2({
                "autoCapitalize": constants.TEXTAREA_AUTO_CAPITALIZE_NONE,
                "focusSkin": "sknFocus",
                "height": "200px",
                "id": "txtAreaPushNotificationsValue",
                "isVisible": true,
                "keyBoardStyle": constants.TEXTAREA_KEY_BOARD_STYLE_DEFAULT,
                "left": "15px",
                "maxTextLength": 200,
                "numberOfVisibleLines": 3,
                "placeholder": "Push Notifications",
                "right": "15px",
                "skin": "skntxtAread7d9e0",
                "textInputMode": constants.TEXTAREA_INPUT_MODE_ANY,
                "top": "32px",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_TOP_LEFT,
                "padding": [2, 2, 2, 2],
                "paddingInPixel": false
            }, {
                "autoCorrect": false,
                "placeholderSkin": "defTextAreaPlaceholder"
            });
            flxPushNotificationsDetails.add(flxPushNotificationsContent, txtAreaPushNotificationsValue);
            var PushNotificationsError = new com.adminConsole.loans.applications.errorMsg({
                "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                "bottom": "20px",
                "id": "PushNotificationsError",
                "isVisible": false,
                "layoutType": kony.flex.FLOW_HORIZONTAL,
                "left": "15dp",
                "masterType": constants.MASTER_TYPE_DEFAULT,
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0px",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "overrides": {
                    "errorMsg": {
                        "bottom": "20px",
                        "isVisible": false,
                        "left": "15dp",
                        "top": "0px"
                    },
                    "lblErrorText": {
                        "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmAdManagement.PushNotificationsContentCannotbeEmpty\")"
                    }
                }
            }, {
                "overrides": {}
            }, {
                "overrides": {}
            });
            flxPushNotificationsChannel.add(flxPushNotificationsDrop, flxPushNotificationsDetails, PushNotificationsError);
            flxPushNotificationsParent.add(flxPushNotificationsChannel);
            flxOfflineChannels.add(flxSMSChannelParent, flxEmailChannelParent, flxPushNotificationsParent);
            var flxPopupDetails = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                "clipBounds": true,
                "id": "flxPopupDetails",
                "isVisible": false,
                "layoutType": kony.flex.FLOW_VERTICAL,
                "left": "20dp",
                "isModalContainer": false,
                "right": "20dp",
                "skin": "slFbox",
                "top": "20dp",
                "zIndex": 1
            }, {}, {});
            flxPopupDetails.setDefaultUnit(kony.flex.DP);
            var flxNoPopupBannerError = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "bottom": "20dp",
                "clipBounds": true,
                "height": "40dp",
                "id": "flxNoPopupBannerError",
                "isVisible": false,
                "layoutType": kony.flex.FLOW_HORIZONTAL,
                "left": "0",
                "isModalContainer": false,
                "skin": "sknBorderRed",
                "top": "0dp",
                "width": "100%",
                "zIndex": 10
            }, {}, {});
            flxNoPopupBannerError.setDefaultUnit(kony.flex.DP);
            var lxNoPopupBannerErrorIcon = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "100%",
                "id": "lxNoPopupBannerErrorIcon",
                "isVisible": true,
                "layoutType": kony.flex.FLOW_HORIZONTAL,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "sknRedFill",
                "top": "0dp",
                "width": "10%"
            }, {}, {});
            lxNoPopupBannerErrorIcon.setDefaultUnit(kony.flex.DP);
            var blNoPopupBannerErrorIcon = new kony.ui.Label({
                "centerX": "50%",
                "centerY": "50%",
                "height": "20dp",
                "id": "blNoPopupBannerErrorIcon",
                "isVisible": true,
                "left": "0dp",
                "skin": "sknIcon20pxWhite",
                "text": "",
                "top": "0dp",
                "width": "20dp",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            lxNoPopupBannerErrorIcon.add(blNoPopupBannerErrorIcon);
            var lblNoPopupBannerErrorText = new kony.ui.Label({
                "centerY": "50%",
                "height": "15dp",
                "id": "lblNoPopupBannerErrorText",
                "isVisible": true,
                "left": "20dp",
                "skin": "sknlblError",
                "text": "Add atleast 1 popup banner details required to procced further",
                "top": "0dp",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxNoPopupBannerError.add(lxNoPopupBannerErrorIcon, lblNoPopupBannerErrorText);
            var WebPopupDetails = new com.adminConsole.adManagement.popupDetails({
                "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                "id": "WebPopupDetails",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "masterType": constants.MASTER_TYPE_DEFAULT,
                "isModalContainer": false,
                "skin": "sknFlxbgFFFFFFbdrD7D9E0rd4px",
                "top": "0dp",
                "width": "100%",
                "overrides": {
                    "flxCopyBannerDetails": {
                        "isVisible": false
                    }
                }
            }, {
                "overrides": {}
            }, {
                "overrides": {}
            });
            var MobilePopupDetails = new com.adminConsole.adManagement.popupDetails({
                "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                "id": "MobilePopupDetails",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "masterType": constants.MASTER_TYPE_DEFAULT,
                "isModalContainer": false,
                "skin": "sknFlxbgFFFFFFbdrD7D9E0rd4px",
                "top": "20dp",
                "width": "100%",
                "overrides": {
                    "flxPopupTypeHeader": {
                        "width": "180dp"
                    },
                    "lblNoImageConfig": {
                        "text": "No Mobile Popup Ad Banner configuration "
                    },
                    "lblPopupTypeHeader": {
                        "text": "Mobile Popup Ad Banner"
                    },
                    "popupDetails": {
                        "bottom": "10dp",
                        "top": "20dp"
                    }
                }
            }, {
                "overrides": {}
            }, {
                "overrides": {}
            });
            flxPopupDetails.add(flxNoPopupBannerError, WebPopupDetails, MobilePopupDetails);
            var campaignDetailsToolTip = new com.adminConsole.Customers.ToolTip({
                "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                "id": "campaignDetailsToolTip",
                "isVisible": false,
                "layoutType": kony.flex.FREE_FORM,
                "left": "94dp",
                "masterType": constants.MASTER_TYPE_DEFAULT,
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "490dp",
                "width": "181dp",
                "overrides": {
                    "ToolTip": {
                        "isVisible": false,
                        "left": "94dp",
                        "top": "490dp",
                        "width": "181dp"
                    },
                    "flxToolTipMessage": {
                        "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                        "centerX": "viz.val_cleared",
                        "width": kony.flex.USE_PREFFERED_SIZE
                    },
                    "lblNoConcentToolTip": {
                        "bottom": "5px",
                        "height": kony.flex.USE_PREFFERED_SIZE,
                        "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmAdManagement.smsEmailPushNotification\")",
                        "left": "10px",
                        "right": "10px",
                        "top": "3px",
                        "width": kony.flex.USE_PREFFERED_SIZE
                    },
                    "lblarrow": {
                        "centerX": "viz.val_cleared",
                        "left": "10px"
                    }
                }
            }, {
                "overrides": {}
            }, {
                "overrides": {}
            });
            flxContent.add(flxBasicDetails, flxAdSpecifications, flxTargetCustomers, flxViewEvents, flxOfflineChannels, flxPopupDetails, campaignDetailsToolTip);
            var flxButtons = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "bottom": 0,
                "clipBounds": true,
                "height": "80dp",
                "id": "flxButtons",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "flxe4e6ec1px",
                "width": "100%",
                "zIndex": 11
            }, {}, {});
            flxButtons.setDefaultUnit(kony.flex.DP);
            var commonButtons = new com.adminConsole.common.commonButtons({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "height": "80px",
                "id": "commonButtons",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "masterType": constants.MASTER_TYPE_DEFAULT,
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0dp",
                "width": "100%",
                "overrides": {
                    "btnCancel": {
                        "isVisible": true,
                        "left": "20dp"
                    },
                    "btnNext": {
                        "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmAdManagement.CretaeProfileCAPS\")",
                        "isVisible": false,
                        "left": "viz.val_cleared",
                        "right": "0dp"
                    },
                    "btnSave": {
                        "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmAdManagement.CreateCampaignCAPS\")",
                        "isVisible": true,
                        "right": "0dp",
                        "width": "150px"
                    },
                    "flxRightButtons": {
                        "right": "20px",
                        "width": "250px"
                    }
                }
            }, {
                "overrides": {}
            }, {
                "overrides": {}
            });
            flxButtons.add(commonButtons);
            flxDetails.add(flxContent, flxButtons);
            flxContainer.add(flxOptions, flxDetails);
            var flxLoading = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "100%",
                "id": "flxLoading",
                "isVisible": false,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "sknLoadingBlur",
                "top": "0dp",
                "width": "100%",
                "zIndex": 200
            }, {}, {});
            flxLoading.setDefaultUnit(kony.flex.DP);
            var flxImageContainer = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "centerX": "50%",
                "centerY": "50%",
                "clipBounds": true,
                "height": "75px",
                "id": "flxImageContainer",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "isModalContainer": false,
                "skin": "slFbox",
                "width": "75px",
                "zIndex": 1
            }, {}, {});
            flxImageContainer.setDefaultUnit(kony.flex.DP);
            var imgLoading = new kony.ui.Image2({
                "centerX": "50%",
                "centerY": "50%",
                "height": "50px",
                "id": "imgLoading",
                "isVisible": true,
                "skin": "slImage",
                "src": "loadingscreenimage.gif",
                "width": "50px",
                "zIndex": 1
            }, {
                "imageScaleMode": constants.IMAGE_SCALE_MODE_MAINTAIN_ASPECT_RATIO,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxImageContainer.add(imgLoading);
            flxLoading.add(flxImageContainer);
            flxRightPannel.add(flxMainHeader, flxHeaderDropdown, flxBreadCrumbs, flxContainer, flxLoading);
            var flxConsentPopup = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "100%",
                "id": "flxConsentPopup",
                "isVisible": false,
                "layoutType": kony.flex.FREE_FORM,
                "masterType": constants.MASTER_TYPE_USERWIDGET,
                "isModalContainer": false,
                "skin": "skn222b35",
                "width": "100%",
                "zIndex": 10
            }, {}, {});
            flxConsentPopup.setDefaultUnit(kony.flex.DP);
            var flxCampaignList = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "centerX": "50%",
                "clipBounds": false,
                "height": "450dp",
                "id": "flxCampaignList",
                "isVisible": false,
                "layoutType": kony.flex.FLOW_VERTICAL,
                "isModalContainer": false,
                "skin": "flxNoShadowNoBorder",
                "top": "150px",
                "width": "560dp",
                "zIndex": 10
            }, {}, {});
            flxCampaignList.setDefaultUnit(kony.flex.DP);
            var flxPopUpTopColor = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "10px",
                "id": "flxPopUpTopColor",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "sknFlxInfoToastBg357C9ENoRadius",
                "top": "0dp",
                "width": "100%",
                "zIndex": 1
            }, {}, {});
            flxPopUpTopColor.setDefaultUnit(kony.flex.DP);
            flxPopUpTopColor.add();
            var flxPopupHeader = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "490dp",
                "id": "flxPopupHeader",
                "isVisible": true,
                "layoutType": kony.flex.FLOW_VERTICAL,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "slFbox0d9c3974835234d",
                "top": "0px",
                "width": "100%",
                "zIndex": 1
            }, {}, {});
            flxPopupHeader.setDefaultUnit(kony.flex.DP);
            var flxPopUpClose = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "25px",
                "id": "flxPopUpClose",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "isModalContainer": false,
                "right": 20,
                "skin": "slFbox",
                "top": "10dp",
                "width": "25px",
                "zIndex": 20
            }, {}, {
                "hoverSkin": "sknCursor"
            });
            flxPopUpClose.setDefaultUnit(kony.flex.DP);
            var lblPopupClose = new kony.ui.Label({
                "centerX": "50%",
                "centerY": "50%",
                "height": "20dp",
                "id": "lblPopupClose",
                "isVisible": true,
                "left": "0dp",
                "skin": "sknIcon18pxGray",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.userwidgetmodel.lblClearSearch\")",
                "width": "20dp",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxPopUpClose.add(lblPopupClose);
            var lblPopUpMainMessage = new kony.ui.Label({
                "height": "20dp",
                "id": "lblPopUpMainMessage",
                "isVisible": true,
                "left": "20px",
                "skin": "sknLbl192b45LatoReg16px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmAdManagement.PriorityList\")",
                "top": "0px",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var flxPriorityList = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                "clipBounds": true,
                "id": "flxPriorityList",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "20dp",
                "width": kony.flex.USE_PREFFERED_SIZE
            }, {}, {});
            flxPriorityList.setDefaultUnit(kony.flex.DP);
            var flxTable = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                "clipBounds": true,
                "id": "flxTable",
                "isVisible": true,
                "layoutType": kony.flex.FLOW_VERTICAL,
                "left": "20dp",
                "isModalContainer": false,
                "right": "40dp",
                "skin": "slFbox",
                "top": "0dp",
                "width": kony.flex.USE_PREFFERED_SIZE
            }, {}, {});
            flxTable.setDefaultUnit(kony.flex.DP);
            var flxHeader = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "15dp",
                "id": "flxHeader",
                "isVisible": true,
                "layoutType": kony.flex.FLOW_HORIZONTAL,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0dp",
                "width": "100%"
            }, {}, {});
            flxHeader.setDefaultUnit(kony.flex.DP);
            var flxCampaignName = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                "clipBounds": true,
                "id": "flxCampaignName",
                "isVisible": true,
                "layoutType": kony.flex.FLOW_HORIZONTAL,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0dp",
                "width": "60%"
            }, {}, {});
            flxCampaignName.setDefaultUnit(kony.flex.DP);
            var lblCampaignHeader = new kony.ui.Label({
                "id": "lblCampaignHeader",
                "isVisible": true,
                "left": "35dp",
                "skin": "sknlblLato696c7312px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmAdManagement.CampaignNameCAPS\")",
                "top": "0px",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {
                "hoverSkin": "sknlblLatoBold00000012px"
            });
            var lblCampaignSort = new kony.ui.Label({
                "id": "lblCampaignSort",
                "isVisible": true,
                "left": "5px",
                "skin": "sknLblIIcoMoon485c7514px",
                "text": "",
                "top": "0px",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxCampaignName.add(lblCampaignHeader, lblCampaignSort);
            var flxPriorityHeader = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                "clipBounds": true,
                "id": "flxPriorityHeader",
                "isVisible": true,
                "layoutType": kony.flex.FLOW_HORIZONTAL,
                "left": "20dp",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0dp",
                "width": "36%"
            }, {}, {});
            flxPriorityHeader.setDefaultUnit(kony.flex.DP);
            var lblPriorityHeader = new kony.ui.Label({
                "id": "lblPriorityHeader",
                "isVisible": true,
                "left": "0px",
                "skin": "sknlblLato696c7312px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmAdManagement.PriorityCAPS\")",
                "top": "0px",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {
                "hoverSkin": "sknlblLatoBold00000012px"
            });
            var lblPrioritySort = new kony.ui.Label({
                "id": "lblPrioritySort",
                "isVisible": true,
                "left": "5px",
                "skin": "sknLblIIcoMoon485c7514px",
                "text": "",
                "top": "0px",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxPriorityHeader.add(lblPriorityHeader, lblPrioritySort);
            flxHeader.add(flxCampaignName, flxPriorityHeader);
            var lblSeperator = new kony.ui.Label({
                "bottom": "0dp",
                "height": "1dp",
                "id": "lblSeperator",
                "isVisible": true,
                "left": "0dp",
                "right": "0dp",
                "skin": "sknlblSeperatorD7D9E0",
                "text": "l",
                "top": "15dp",
                "width": "100%",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var flxSegmentScrol = new kony.ui.FlexScrollContainer({
                "allowHorizontalBounce": false,
                "allowVerticalBounce": true,
                "bounces": true,
                "clipBounds": true,
                "enableScrolling": true,
                "height": "320dp",
                "horizontalScrollIndicator": true,
                "id": "flxSegmentScrol",
                "isVisible": true,
                "layoutType": kony.flex.FLOW_VERTICAL,
                "left": "0dp",
                "pagingEnabled": false,
                "scrollDirection": kony.flex.SCROLL_VERTICAL,
                "skin": "slFSbox",
                "top": "0dp",
                "verticalScrollIndicator": true,
                "width": "100%"
            }, {}, {});
            flxSegmentScrol.setDefaultUnit(kony.flex.DP);
            var segCampaigns = new kony.ui.SegmentedUI2({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "data": [{
                    "lblCampaign": "kony.i18n.getLocalizedString(\"i18n.frmAdManagement.CampaignNameCAPS\")",
                    "lblPriority": "kony.i18n.getLocalizedString(\"i18n.frmAdManagement.PriorityCAPS\")"
                }, {
                    "lblCampaign": "kony.i18n.getLocalizedString(\"i18n.frmAdManagement.CampaignNameCAPS\")",
                    "lblPriority": "kony.i18n.getLocalizedString(\"i18n.frmAdManagement.PriorityCAPS\")"
                }, {
                    "lblCampaign": "kony.i18n.getLocalizedString(\"i18n.frmAdManagement.CampaignNameCAPS\")",
                    "lblPriority": "kony.i18n.getLocalizedString(\"i18n.frmAdManagement.PriorityCAPS\")"
                }, {
                    "lblCampaign": "kony.i18n.getLocalizedString(\"i18n.frmAdManagement.CampaignNameCAPS\")",
                    "lblPriority": "kony.i18n.getLocalizedString(\"i18n.frmAdManagement.PriorityCAPS\")"
                }, {
                    "lblCampaign": "kony.i18n.getLocalizedString(\"i18n.frmAdManagement.CampaignNameCAPS\")",
                    "lblPriority": "kony.i18n.getLocalizedString(\"i18n.frmAdManagement.PriorityCAPS\")"
                }, {
                    "lblCampaign": "kony.i18n.getLocalizedString(\"i18n.frmAdManagement.CampaignNameCAPS\")",
                    "lblPriority": "kony.i18n.getLocalizedString(\"i18n.frmAdManagement.PriorityCAPS\")"
                }, {
                    "lblCampaign": "kony.i18n.getLocalizedString(\"i18n.frmAdManagement.CampaignNameCAPS\")",
                    "lblPriority": "kony.i18n.getLocalizedString(\"i18n.frmAdManagement.PriorityCAPS\")"
                }, {
                    "lblCampaign": "kony.i18n.getLocalizedString(\"i18n.frmAdManagement.CampaignNameCAPS\")",
                    "lblPriority": "kony.i18n.getLocalizedString(\"i18n.frmAdManagement.PriorityCAPS\")"
                }, {
                    "lblCampaign": "kony.i18n.getLocalizedString(\"i18n.frmAdManagement.CampaignNameCAPS\")",
                    "lblPriority": "kony.i18n.getLocalizedString(\"i18n.frmAdManagement.PriorityCAPS\")"
                }, {
                    "lblCampaign": "kony.i18n.getLocalizedString(\"i18n.frmAdManagement.CampaignNameCAPS\")",
                    "lblPriority": "kony.i18n.getLocalizedString(\"i18n.frmAdManagement.PriorityCAPS\")"
                }],
                "groupCells": false,
                "height": "100%",
                "id": "segCampaigns",
                "isVisible": true,
                "left": "0dp",
                "needPageIndicator": true,
                "pageOffDotImage": "pageoffdot.png",
                "pageOnDotImage": "pageondot.png",
                "retainSelection": false,
                "rowFocusSkin": "seg2Focus",
                "rowSkin": "seg2Normal",
                "rowTemplate": "flxCampaignPriorities",
                "sectionHeaderSkin": "sliPhoneSegmentHeader",
                "selectionBehavior": constants.SEGUI_DEFAULT_BEHAVIOR,
                "separatorColor": "aaaaaa00",
                "separatorRequired": true,
                "separatorThickness": 1,
                "showScrollbars": false,
                "top": "0",
                "viewType": constants.SEGUI_VIEW_TYPE_TABLEVIEW,
                "widgetDataMap": {
                    "flxCampaignPriorities": "flxCampaignPriorities",
                    "lblCampaign": "lblCampaign",
                    "lblPriority": "lblPriority"
                },
                "width": "100%",
                "zIndex": 20
            }, {
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxSegmentScrol.add(segCampaigns);
            flxTable.add(flxHeader, lblSeperator, flxSegmentScrol);
            flxPriorityList.add(flxTable);
            flxPopupHeader.add(flxPopUpClose, lblPopUpMainMessage, flxPriorityList);
            flxCampaignList.add(flxPopUpTopColor, flxPopupHeader);
            var popUp = new com.adminConsole.common.popUp({
                "height": "100%",
                "id": "popUp",
                "isVisible": false,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "masterType": constants.MASTER_TYPE_DEFAULT,
                "isModalContainer": false,
                "skin": "sknflxBg000000Op50",
                "top": "0dp",
                "width": "100%",
                "zIndex": 1,
                "overrides": {
                    "btnPopUpCancel": {
                        "i18n_text": "kony.i18n.getLocalizedString(\"i18n.PopUp.NoLeaveAsIS\")",
                        "left": "viz.val_cleared",
                        "minWidth": "100px",
                        "right": "20px",
                        "width": kony.flex.USE_PREFFERED_SIZE
                    },
                    "btnPopUpDelete": {
                        "i18n_text": "kony.i18n.getLocalizedString(\"i18n.PopUp.YesProceed\")"
                    },
                    "popUp": {
                        "isVisible": false
                    }
                }
            }, {
                "overrides": {}
            }, {
                "overrides": {}
            });
            var warningpopup = new com.adminConsole.common.popUp({
                "height": "100%",
                "id": "warningpopup",
                "isVisible": false,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "masterType": constants.MASTER_TYPE_DEFAULT,
                "isModalContainer": false,
                "skin": "sknflxBg000000Op50",
                "top": "0dp",
                "width": "100%",
                "zIndex": 1,
                "overrides": {
                    "btnPopUpCancel": {
                        "i18n_text": "kony.i18n.getLocalizedString(\"i18n.PopUp.NoLeaveAsIS\")",
                        "left": "viz.val_cleared",
                        "minWidth": "100px",
                        "right": "20px",
                        "top": "0%",
                        "width": kony.flex.USE_PREFFERED_SIZE
                    },
                    "btnPopUpDelete": {
                        "i18n_text": "kony.i18n.getLocalizedString(\"i18n.PopUp.YesProceed\")"
                    },
                    "flxPopUp": {
                        "width": "680px"
                    },
                    "lblPopUpMainMessage": {
                        "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmAdManagement.ReduceAdPlaceholders\")"
                    },
                    "popUp": {
                        "isVisible": false,
                        "left": "0dp",
                        "top": "0dp",
                        "width": "100%"
                    },
                    "rtxPopUpDisclaimer": {
                        "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmAdManagement.ReduceAdPlaceholdersMsg\")",
                        "width": "640dp"
                    }
                }
            }, {
                "overrides": {}
            }, {
                "overrides": {}
            });
            var flxAddAttributesPopUp = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "centerX": "50%",
                "centerY": "50%",
                "clipBounds": false,
                "height": "80%",
                "id": "flxAddAttributesPopUp",
                "isVisible": false,
                "layoutType": kony.flex.FREE_FORM,
                "isModalContainer": false,
                "skin": "flxNoShadowNoBorder",
                "width": "986dp",
                "zIndex": 10
            }, {}, {});
            flxAddAttributesPopUp.setDefaultUnit(kony.flex.DP);
            var flxAttributesPopUpTopColor = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "10px",
                "id": "flxAttributesPopUpTopColor",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "sknFlxInfoToastBg357C9ENoRadius",
                "top": "0dp",
                "width": "100%",
                "zIndex": 1
            }, {}, {});
            flxAttributesPopUpTopColor.setDefaultUnit(kony.flex.DP);
            flxAttributesPopUpTopColor.add();
            var flxAttributesPopUpClose = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "15px",
                "id": "flxAttributesPopUpClose",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "isModalContainer": false,
                "right": 20,
                "skin": "slFbox",
                "top": "30dp",
                "width": "15px",
                "zIndex": 1
            }, {}, {
                "hoverSkin": "sknCursor"
            });
            flxAttributesPopUpClose.setDefaultUnit(kony.flex.DP);
            var lblAttributesPopupClose = new kony.ui.Label({
                "height": "15dp",
                "id": "lblAttributesPopupClose",
                "isVisible": true,
                "left": "0dp",
                "skin": "sknfontIconListbox15px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.userwidgetmodel.lblClearSearch\")",
                "top": "0dp",
                "width": "15dp",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxAttributesPopUpClose.add(lblAttributesPopupClose);
            var lblAttributesPopUpMainMessage = new kony.ui.Label({
                "height": "19dp",
                "id": "lblAttributesPopUpMainMessage",
                "isVisible": true,
                "left": "20px",
                "skin": "sknLbl192b45LatoReg16px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmAdManagement.AddDatasetsAndAttributes\")",
                "top": "37px",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var flxAddAttributesContainer = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "bottom": "80dp",
                "clipBounds": true,
                "id": "flxAddAttributesContainer",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "77dp",
                "width": "100%",
                "zIndex": 1
            }, {}, {});
            flxAddAttributesContainer.setDefaultUnit(kony.flex.DP);
            var flxDatasetContainer = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "100%",
                "id": "flxDatasetContainer",
                "isVisible": true,
                "layoutType": kony.flex.FLOW_VERTICAL,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "flxe4e6ec1px",
                "top": "0dp",
                "width": 232,
                "zIndex": 1
            }, {}, {});
            flxDatasetContainer.setDefaultUnit(kony.flex.DP);
            var flxDatasetHeader = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "50dp",
                "id": "flxDatasetHeader",
                "isVisible": true,
                "layoutType": kony.flex.FLOW_HORIZONTAL,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0dp",
                "width": "100%"
            }, {}, {});
            flxDatasetHeader.setDefaultUnit(kony.flex.DP);
            var lblSelectDataset = new kony.ui.Label({
                "id": "lblSelectDataset",
                "isVisible": true,
                "left": "20dp",
                "skin": "sknLbl192B45LatoRegular14px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmAdManagement.selectDataSet\")",
                "top": "20dp",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var flxDatasetCount = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "20dp",
                "id": "flxDatasetCount",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "10dp",
                "isModalContainer": false,
                "skin": "sknFlx006CCA",
                "top": "20dp",
                "width": "40dp",
                "zIndex": 1
            }, {}, {});
            flxDatasetCount.setDefaultUnit(kony.flex.DP);
            var lblDatasetCount = new kony.ui.Label({
                "centerX": "50%",
                "centerY": "40%",
                "id": "lblDatasetCount",
                "isVisible": true,
                "skin": "sknLblffffff13px",
                "text": "00",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxDatasetCount.add(lblDatasetCount);
            var flxReset = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                "clipBounds": true,
                "id": "flxReset",
                "isVisible": true,
                "layoutType": kony.flex.FLOW_HORIZONTAL,
                "left": "20dp",
                "isModalContainer": false,
                "skin": "hoverhandSkin",
                "top": "20dp",
                "width": "35dp"
            }, {}, {});
            flxReset.setDefaultUnit(kony.flex.DP);
            var lblReset = new kony.ui.Label({
                "id": "lblReset",
                "isVisible": true,
                "left": "0dp",
                "skin": "sknLblLatoReg117eb013px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.Common.button.Reset\")",
                "top": "0dp",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxReset.add(lblReset);
            flxDatasetHeader.add(lblSelectDataset, flxDatasetCount, flxReset);
            var flxDatasets = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                "bottom": "0px",
                "clipBounds": true,
                "id": "flxDatasets",
                "isVisible": true,
                "layoutType": kony.flex.FLOW_VERTICAL,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "sknflxBgF9F9F9",
                "top": "0dp",
                "width": "100%",
                "zIndex": 1
            }, {}, {});
            flxDatasets.setDefaultUnit(kony.flex.DP);
            var flxDatasetSearchContainer = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "60dp",
                "id": "flxDatasetSearchContainer",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "sknflxBgF9F9F9Border1pxE4E6EC",
                "top": "0dp",
                "width": "100%",
                "zIndex": 1
            }, {}, {});
            flxDatasetSearchContainer.setDefaultUnit(kony.flex.DP);
            var flxDatasetSearch = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "centerY": "50%",
                "clipBounds": true,
                "height": "40px",
                "id": "flxDatasetSearch",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "15dp",
                "isModalContainer": false,
                "skin": "sknflxd5d9ddop100",
                "width": "200px",
                "zIndex": 1
            }, {}, {});
            flxDatasetSearch.setDefaultUnit(kony.flex.DP);
            var fontIconSearchImg = new kony.ui.Label({
                "centerY": "50%",
                "id": "fontIconSearchImg",
                "isVisible": true,
                "left": "12px",
                "skin": "sknFontIconSearchImg20px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.userwidgetmodel.lblSearchIcon\")",
                "top": "0dp",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var tbxDatasetSearchBox = new kony.ui.TextBox2({
                "autoCapitalize": constants.TEXTBOX_AUTO_CAPITALIZE_NONE,
                "centerY": "50%",
                "height": "30px",
                "id": "tbxDatasetSearchBox",
                "isVisible": true,
                "keyBoardStyle": constants.TEXTBOX_KEY_BOARD_STYLE_DEFAULT,
                "left": "35dp",
                "onTouchStart": controller.AS_TextField_cfab06859f0f43eca99495891ee290d5,
                "placeholder": "Search",
                "right": "35px",
                "secureTextEntry": false,
                "skin": "skntbxffffffNoBorderlato35475f14px",
                "textInputMode": constants.TEXTBOX_INPUT_MODE_ANY,
                "zIndex": 1
            }, {
                "containerHeightMode": constants.TEXTBOX_FONT_METRICS_DRIVEN_HEIGHT,
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [2, 0, 0, 0],
                "paddingInPixel": false
            }, {
                "autoCorrect": false
            });
            var flxDatasetClearSearch = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "100%",
                "id": "flxDatasetClearSearch",
                "isVisible": false,
                "layoutType": kony.flex.FREE_FORM,
                "isModalContainer": false,
                "right": "0px",
                "skin": "sknCursor",
                "top": "0dp",
                "width": "35px",
                "zIndex": 1
            }, {}, {
                "hoverSkin": "sknCursor"
            });
            flxDatasetClearSearch.setDefaultUnit(kony.flex.DP);
            var fontIconCross = new kony.ui.Label({
                "centerY": "50%",
                "id": "fontIconCross",
                "isVisible": true,
                "left": "5px",
                "skin": "sknFontIconSearchCross16px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.userwidgetmodel.lblClearSearch\")",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxDatasetClearSearch.add(fontIconCross);
            flxDatasetSearch.add(fontIconSearchImg, tbxDatasetSearchBox, flxDatasetClearSearch);
            flxDatasetSearchContainer.add(flxDatasetSearch);
            var flxDatasetList = new kony.ui.FlexScrollContainer({
                "allowHorizontalBounce": false,
                "allowVerticalBounce": true,
                "bounces": true,
                "clipBounds": true,
                "enableScrolling": true,
                "height": "240dp",
                "horizontalScrollIndicator": true,
                "id": "flxDatasetList",
                "isVisible": true,
                "layoutType": kony.flex.FLOW_VERTICAL,
                "left": "0dp",
                "pagingEnabled": false,
                "scrollDirection": kony.flex.SCROLL_VERTICAL,
                "skin": "slFSbox",
                "top": "0dp",
                "verticalScrollIndicator": true,
                "width": "100%"
            }, {}, {});
            flxDatasetList.setDefaultUnit(kony.flex.DP);
            flxDatasetList.add();
            var flxNoDatasetFound = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "bottom": "0dp",
                "clipBounds": true,
                "id": "flxNoDatasetFound",
                "isVisible": false,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": 0,
                "width": "100%",
                "zIndex": 1
            }, {}, {});
            flxNoDatasetFound.setDefaultUnit(kony.flex.DP);
            var lblNoDatasetFound = new kony.ui.Label({
                "centerX": "50%",
                "centerY": "50%",
                "id": "lblNoDatasetFound",
                "isVisible": true,
                "skin": "sknLblLato84939e13px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.Messages.SuggestionsNoResultsFound\")",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxNoDatasetFound.add(lblNoDatasetFound);
            flxDatasets.add(flxDatasetSearchContainer, flxDatasetList, flxNoDatasetFound);
            flxDatasetContainer.add(flxDatasetHeader, flxDatasets);
            var flxAddAttributesDetailsContainer = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "100%",
                "id": "flxAddAttributesDetailsContainer",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "231dp",
                "isModalContainer": false,
                "right": "0dp",
                "skin": "flxe4e6ec1px",
                "top": "0dp",
                "zIndex": 1
            }, {}, {});
            flxAddAttributesDetailsContainer.setDefaultUnit(kony.flex.DP);
            var flxAddAttributesDetailsMain = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "100%",
                "id": "flxAddAttributesDetailsMain",
                "isVisible": true,
                "layoutType": kony.flex.FLOW_VERTICAL,
                "left": "20dp",
                "isModalContainer": false,
                "right": "20dp",
                "skin": "slFbox",
                "top": "0dp",
                "width": kony.flex.USE_PREFFERED_SIZE
            }, {}, {});
            flxAddAttributesDetailsMain.setDefaultUnit(kony.flex.DP);
            flxAddAttributesDetailsMain.add();
            var flxAddAttributesMsgContainer = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "centerX": "50%",
                "centerY": "50%",
                "clipBounds": true,
                "height": "98dp",
                "id": "flxAddAttributesMsgContainer",
                "isVisible": false,
                "layoutType": kony.flex.FREE_FORM,
                "isModalContainer": false,
                "skin": "slFbox",
                "width": "360dp",
                "zIndex": 1
            }, {}, {});
            flxAddAttributesMsgContainer.setDefaultUnit(kony.flex.DP);
            var imgSelectCheckbox = new kony.ui.Image2({
                "centerX": "50%",
                "height": "67dp",
                "id": "imgSelectCheckbox",
                "isVisible": true,
                "skin": "slImage",
                "src": "selectacheckbox.png",
                "top": "0dp",
                "width": "83dp",
                "zIndex": 1
            }, {
                "imageScaleMode": constants.IMAGE_SCALE_MODE_MAINTAIN_ASPECT_RATIO,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var lblAddAttributesMsg = new kony.ui.Label({
                "bottom": "0dp",
                "id": "lblAddAttributesMsg",
                "isVisible": true,
                "left": "0dp",
                "skin": "sknlbl485C75LatoSemiBold13px",
                "text": "Select the checkbox on the left hand side to add the attributes.",
                "width": "100%",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxAddAttributesMsgContainer.add(imgSelectCheckbox, lblAddAttributesMsg);
            flxAddAttributesDetailsContainer.add(flxAddAttributesDetailsMain, flxAddAttributesMsgContainer);
            flxAddAttributesContainer.add(flxDatasetContainer, flxAddAttributesDetailsContainer);
            var flxAttributesPopUpButtons = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "bottom": "0dp",
                "clipBounds": true,
                "height": "80px",
                "id": "flxAttributesPopUpButtons",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "sknflxBgE4E6EC",
                "width": "100%",
                "zIndex": 1
            }, {}, {});
            flxAttributesPopUpButtons.setDefaultUnit(kony.flex.DP);
            var btnAttributesCancel = new kony.ui.Button({
                "centerY": "50%",
                "height": "40px",
                "id": "btnAttributesCancel",
                "isVisible": true,
                "right": "130px",
                "skin": "sknbtnf7f7faLatoRegular4f555dBorder1px8b96a5",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.permission.CANCEL\")",
                "top": "0%",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 20
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_CENTER,
                "displayText": true,
                "padding": [3, 0, 3, 0],
                "paddingInPixel": false
            }, {
                "hoverSkin": "sknbtnffffffLatoRegular4f555dBorder1px485c75"
            });
            var btnAttributesSave = new kony.ui.Button({
                "centerY": "50%",
                "focusSkin": "sknBtn003E75LatoRegular13pxFFFFFFRad20px",
                "height": "40px",
                "id": "btnAttributesSave",
                "isVisible": true,
                "minWidth": "100px",
                "right": "20px",
                "skin": "sknBtn003E75LatoRegular13pxFFFFFFRad20px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.permission.SAVE\")",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 20
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_CENTER,
                "displayText": true,
                "padding": [3, 0, 3, 0],
                "paddingInPixel": false
            }, {
                "hoverSkin": "sknBtn005198LatoRegular13pxFFFFFFRad20px"
            });
            flxAttributesPopUpButtons.add(btnAttributesCancel, btnAttributesSave);
            flxAddAttributesPopUp.add(flxAttributesPopUpTopColor, flxAttributesPopUpClose, lblAttributesPopUpMainMessage, flxAddAttributesContainer, flxAttributesPopUpButtons);
            var flxAddExistingRolesPopUp = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "centerX": "50%",
                "centerY": "50%",
                "clipBounds": false,
                "height": "80%",
                "id": "flxAddExistingRolesPopUp",
                "isVisible": false,
                "layoutType": kony.flex.FREE_FORM,
                "isModalContainer": false,
                "skin": "flxNoShadowNoBorder",
                "width": "986dp",
                "zIndex": 10
            }, {}, {});
            flxAddExistingRolesPopUp.setDefaultUnit(kony.flex.DP);
            var flxAddExistingRolesPopUpTopColor = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "10px",
                "id": "flxAddExistingRolesPopUpTopColor",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "sknFlxInfoToastBg357C9ENoRadius",
                "top": "0dp",
                "width": "100%",
                "zIndex": 1
            }, {}, {});
            flxAddExistingRolesPopUpTopColor.setDefaultUnit(kony.flex.DP);
            flxAddExistingRolesPopUpTopColor.add();
            var flxAddExistingRolesPopUpClose = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "15px",
                "id": "flxAddExistingRolesPopUpClose",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "isModalContainer": false,
                "right": 20,
                "skin": "slFbox",
                "top": "30dp",
                "width": "15px",
                "zIndex": 1
            }, {}, {
                "hoverSkin": "sknCursor"
            });
            flxAddExistingRolesPopUpClose.setDefaultUnit(kony.flex.DP);
            var lblAddExistingRolesPopupClose = new kony.ui.Label({
                "height": "15dp",
                "id": "lblAddExistingRolesPopupClose",
                "isVisible": true,
                "left": "0dp",
                "skin": "sknfontIconListbox15px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.userwidgetmodel.lblClearSearch\")",
                "top": "0dp",
                "width": "15dp",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxAddExistingRolesPopUpClose.add(lblAddExistingRolesPopupClose);
            var lblExistingRolesPopUpMainMessage = new kony.ui.Label({
                "height": "19dp",
                "id": "lblExistingRolesPopUpMainMessage",
                "isVisible": true,
                "left": "20px",
                "skin": "sknLbl192b45LatoReg16px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmAdManagement.targetSegmentsSelection\")",
                "top": "37px",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var flxSelectRoleErrorMsg = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                "clipBounds": true,
                "id": "flxSelectRoleErrorMsg",
                "isVisible": false,
                "layoutType": kony.flex.FREE_FORM,
                "left": "185dp",
                "minHeight": "10px",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "40px",
                "width": "280px"
            }, {}, {});
            flxSelectRoleErrorMsg.setDefaultUnit(kony.flex.DP);
            var lblSelectRoleErrorMsg = new kony.ui.Label({
                "id": "lblSelectRoleErrorMsg",
                "isVisible": true,
                "left": "15px",
                "skin": "sknlblError",
                "text": "Atleast 1 role needed to be selected to add a role.",
                "top": "0px",
                "width": "265px",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var lblSelectRoleErrorIcon = new kony.ui.Label({
                "height": "15dp",
                "id": "lblSelectRoleErrorIcon",
                "isVisible": true,
                "left": "0dp",
                "skin": "sknFontIconError",
                "text": "",
                "top": "0dp",
                "width": "15dp",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxSelectRoleErrorMsg.add(lblSelectRoleErrorMsg, lblSelectRoleErrorIcon);
            var flxAddExistingRoleContainer = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "bottom": "80dp",
                "clipBounds": true,
                "id": "flxAddExistingRoleContainer",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "77dp",
                "width": "100%",
                "zIndex": 1
            }, {}, {});
            flxAddExistingRoleContainer.setDefaultUnit(kony.flex.DP);
            var flxSelectRoleContainer = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "100%",
                "id": "flxSelectRoleContainer",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "flxe4e6ec1px",
                "top": "0dp",
                "width": 232,
                "zIndex": 1
            }, {}, {});
            flxSelectRoleContainer.setDefaultUnit(kony.flex.DP);
            var flxProfileHeader = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "50dp",
                "id": "flxProfileHeader",
                "isVisible": true,
                "layoutType": kony.flex.FLOW_HORIZONTAL,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0dp",
                "width": "100%"
            }, {}, {});
            flxProfileHeader.setDefaultUnit(kony.flex.DP);
            var lblSelectProfile = new kony.ui.Label({
                "id": "lblSelectProfile",
                "isVisible": true,
                "left": "20dp",
                "skin": "sknLbl192B45LatoRegular14px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmAdManagement.selectSegments Select\")",
                "top": "20dp",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var flxProfileCount = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "20dp",
                "id": "flxProfileCount",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "10dp",
                "isModalContainer": false,
                "skin": "sknFlx006CCA",
                "top": "20dp",
                "width": "40dp",
                "zIndex": 1
            }, {}, {});
            flxProfileCount.setDefaultUnit(kony.flex.DP);
            var lblProfileCount = new kony.ui.Label({
                "centerX": "50%",
                "centerY": "40%",
                "id": "lblProfileCount",
                "isVisible": true,
                "skin": "sknLblffffff13px",
                "text": "00",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxProfileCount.add(lblProfileCount);
            var flxResetProfiles = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                "clipBounds": true,
                "id": "flxResetProfiles",
                "isVisible": true,
                "layoutType": kony.flex.FLOW_HORIZONTAL,
                "left": "20dp",
                "isModalContainer": false,
                "skin": "hoverhandSkin",
                "top": "20dp",
                "width": "35dp"
            }, {}, {});
            flxResetProfiles.setDefaultUnit(kony.flex.DP);
            var lblResetProfiles = new kony.ui.Label({
                "id": "lblResetProfiles",
                "isVisible": true,
                "left": "0dp",
                "skin": "sknLblLatoReg117eb013px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.Common.button.Reset\")",
                "top": "0dp",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxResetProfiles.add(lblResetProfiles);
            flxProfileHeader.add(lblSelectProfile, flxProfileCount, flxResetProfiles);
            var flxExistingRolesListSearchContainer = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "bottom": "0px",
                "clipBounds": true,
                "id": "flxExistingRolesListSearchContainer",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "sknflxBgF9F9F9",
                "top": "57dp",
                "width": "100%",
                "zIndex": 1
            }, {}, {});
            flxExistingRolesListSearchContainer.setDefaultUnit(kony.flex.DP);
            var flxExistingRolesSearchContainer = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "60dp",
                "id": "flxExistingRolesSearchContainer",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "sknflxBgF9F9F9Border1pxE4E6EC",
                "top": "0dp",
                "width": "100%",
                "zIndex": 1
            }, {}, {});
            flxExistingRolesSearchContainer.setDefaultUnit(kony.flex.DP);
            var flxExistingRolesSearch = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "centerY": "50%",
                "clipBounds": true,
                "height": "40dp",
                "id": "flxExistingRolesSearch",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "10dp",
                "isModalContainer": false,
                "right": "10dp",
                "skin": "sknflxd5d9ddop100",
                "zIndex": 1
            }, {}, {});
            flxExistingRolesSearch.setDefaultUnit(kony.flex.DP);
            var fontIconExistingRolesSearchImg = new kony.ui.Label({
                "centerY": "50%",
                "id": "fontIconExistingRolesSearchImg",
                "isVisible": true,
                "left": "12px",
                "skin": "sknFontIconSearchImg20px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.userwidgetmodel.lblSearchIcon\")",
                "top": "0dp",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var tbxExistingRolesSearchBox = new kony.ui.TextBox2({
                "autoCapitalize": constants.TEXTBOX_AUTO_CAPITALIZE_NONE,
                "centerY": "50%",
                "height": "30px",
                "id": "tbxExistingRolesSearchBox",
                "isVisible": true,
                "keyBoardStyle": constants.TEXTBOX_KEY_BOARD_STYLE_DEFAULT,
                "left": "35dp",
                "onTouchStart": controller.AS_TextField_cfab06859f0f43eca99495891ee290d5,
                "placeholder": "Search",
                "right": "35px",
                "secureTextEntry": false,
                "skin": "skntbxffffffNoBorderlato35475f14px",
                "textInputMode": constants.TEXTBOX_INPUT_MODE_ANY,
                "zIndex": 1
            }, {
                "containerHeightMode": constants.TEXTBOX_FONT_METRICS_DRIVEN_HEIGHT,
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [2, 0, 0, 0],
                "paddingInPixel": false
            }, {
                "autoCorrect": false
            });
            var flxExistingRolesClearSearch = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "100%",
                "id": "flxExistingRolesClearSearch",
                "isVisible": false,
                "layoutType": kony.flex.FREE_FORM,
                "isModalContainer": false,
                "right": "0px",
                "skin": "sknCursor",
                "top": "0dp",
                "width": "35px",
                "zIndex": 1
            }, {}, {
                "hoverSkin": "sknCursor"
            });
            flxExistingRolesClearSearch.setDefaultUnit(kony.flex.DP);
            var fontIconExistingRolesCross = new kony.ui.Label({
                "centerY": "50%",
                "id": "fontIconExistingRolesCross",
                "isVisible": true,
                "left": "5px",
                "skin": "sknFontIconSearchCross16px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.userwidgetmodel.lblClearSearch\")",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxExistingRolesClearSearch.add(fontIconExistingRolesCross);
            flxExistingRolesSearch.add(fontIconExistingRolesSearchImg, tbxExistingRolesSearchBox, flxExistingRolesClearSearch);
            flxExistingRolesSearchContainer.add(flxExistingRolesSearch);
            var flxNoExistingRolesFound = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "bottom": "0dp",
                "clipBounds": true,
                "id": "flxNoExistingRolesFound",
                "isVisible": false,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": 60,
                "width": "100%",
                "zIndex": 1
            }, {}, {});
            flxNoExistingRolesFound.setDefaultUnit(kony.flex.DP);
            var lblNoExistingRolesFound = new kony.ui.Label({
                "centerX": "50%",
                "centerY": "50%",
                "id": "lblNoExistingRolesFound",
                "isVisible": true,
                "skin": "sknLblLato84939e13px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.Messages.SuggestionsNoResultsFound\")",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxNoExistingRolesFound.add(lblNoExistingRolesFound);
            var flxProfilesData = new kony.ui.FlexScrollContainer({
                "allowHorizontalBounce": false,
                "allowVerticalBounce": true,
                "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                "bounces": true,
                "clipBounds": true,
                "enableScrolling": true,
                "horizontalScrollIndicator": true,
                "id": "flxProfilesData",
                "isVisible": true,
                "layoutType": kony.flex.FLOW_VERTICAL,
                "left": "0dp",
                "pagingEnabled": false,
                "scrollDirection": kony.flex.SCROLL_VERTICAL,
                "skin": "slFSbox",
                "top": "60dp",
                "verticalScrollIndicator": true,
                "width": "100%"
            }, {}, {});
            flxProfilesData.setDefaultUnit(kony.flex.DP);
            flxProfilesData.add();
            var segExistingRoles = new kony.ui.SegmentedUI2({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "bottom": "0dp",
                "groupCells": false,
                "id": "segExistingRoles",
                "isVisible": false,
                "left": "0dp",
                "needPageIndicator": true,
                "pageOffDotImage": "pageoffdot.png",
                "pageOnDotImage": "pageondot.png",
                "retainSelection": false,
                "rowSkin": "seg2Focus",
                "sectionHeaderSkin": "sliPhoneSegmentHeader",
                "selectionBehavior": constants.SEGUI_DEFAULT_BEHAVIOR,
                "separatorRequired": false,
                "showScrollbars": true,
                "top": "60dp",
                "viewType": constants.SEGUI_VIEW_TYPE_TABLEVIEW,
                "widgetDataMap": {},
                "width": "100%",
                "zIndex": 1
            }, {
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxExistingRolesListSearchContainer.add(flxExistingRolesSearchContainer, flxNoExistingRolesFound, flxProfilesData, segExistingRoles);
            flxSelectRoleContainer.add(flxProfileHeader, flxExistingRolesListSearchContainer);
            var flxExistingRoleDetailsContainer = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "100%",
                "id": "flxExistingRoleDetailsContainer",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "231dp",
                "isModalContainer": false,
                "right": "0dp",
                "skin": "flxe4e6ec1px",
                "top": "0dp",
                "zIndex": 1
            }, {}, {});
            flxExistingRoleDetailsContainer.setDefaultUnit(kony.flex.DP);
            var flxExistingRoleDetailsInner = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "100%",
                "id": "flxExistingRoleDetailsInner",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0dp",
                "width": "100%"
            }, {}, {});
            flxExistingRoleDetailsInner.setDefaultUnit(kony.flex.DP);
            var lblSelectedRoleName = new kony.ui.Label({
                "id": "lblSelectedRoleName",
                "isVisible": true,
                "left": "20dp",
                "skin": "sknLbl192B45LatoRegular14px",
                "text": "Role Name",
                "top": "20dp",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var flxERAttributeListDetails = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "bottom": "0px",
                "clipBounds": true,
                "id": "flxERAttributeListDetails",
                "isVisible": true,
                "layoutType": kony.flex.FLOW_VERTICAL,
                "left": "0px",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "55px",
                "width": "100%",
                "zIndex": 1
            }, {}, {});
            flxERAttributeListDetails.setDefaultUnit(kony.flex.DP);
            var flxERAttributeDescriptionUsers = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                "clipBounds": true,
                "id": "flxERAttributeDescriptionUsers",
                "isVisible": true,
                "layoutType": kony.flex.FLOW_HORIZONTAL,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0dp",
                "width": "100%"
            }, {}, {});
            flxERAttributeDescriptionUsers.setDefaultUnit(kony.flex.DP);
            var flxERAttributeListDescription = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                "clipBounds": true,
                "id": "flxERAttributeListDescription",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0px",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0px",
                "width": "60%",
                "zIndex": 1
            }, {}, {});
            flxERAttributeListDescription.setDefaultUnit(kony.flex.DP);
            var lblERAttributeDescriptionTag = new kony.ui.Label({
                "height": "13px",
                "id": "lblERAttributeDescriptionTag",
                "isVisible": true,
                "left": "20px",
                "skin": "sknlblLato11px696C73",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmAdManagement.DescriptionCAPS\")",
                "top": "0px",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var lblERAttributeDescription = new kony.ui.Label({
                "id": "lblERAttributeDescription",
                "isVisible": true,
                "left": "20px",
                "right": "20px",
                "skin": "sknlbl485C75LatoSemiBold13px",
                "text": "These Micro Business users have the highest level of access to all the functions.",
                "top": "28px",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxERAttributeListDescription.add(lblERAttributeDescriptionTag, lblERAttributeDescription);
            var flxERAttributeListUserAvailable = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                "clipBounds": true,
                "id": "flxERAttributeListUserAvailable",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": 0,
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0px",
                "width": "40%",
                "zIndex": 1
            }, {}, {});
            flxERAttributeListUserAvailable.setDefaultUnit(kony.flex.DP);
            var lblERAttributeUserAvailableTag = new kony.ui.Label({
                "height": "13px",
                "id": "lblERAttributeUserAvailableTag",
                "isVisible": true,
                "left": "20px",
                "skin": "sknlblLato11px696C73",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.ProfileManagement.usersAvailableCaps\")",
                "top": "0px",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var lblERAttributeUserAvailable = new kony.ui.Label({
                "id": "lblERAttributeUserAvailable",
                "isVisible": true,
                "left": "20px",
                "right": "20px",
                "skin": "sknlbl485C75LatoSemiBold13px",
                "text": "127973",
                "top": "28px",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxERAttributeListUserAvailable.add(lblERAttributeUserAvailableTag, lblERAttributeUserAvailable);
            flxERAttributeDescriptionUsers.add(flxERAttributeListDescription, flxERAttributeListUserAvailable);
            var lblSelectedRoleHeaderSeperator = new kony.ui.Label({
                "height": "1dp",
                "id": "lblSelectedRoleHeaderSeperator",
                "isVisible": true,
                "left": "20dp",
                "right": "20dp",
                "skin": "HeaderSeparator",
                "text": "-",
                "top": "30dp",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var flxERAttributesListContainer = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "bottom": "20dp",
                "clipBounds": true,
                "height": "78%",
                "id": "flxERAttributesListContainer",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "20px",
                "isModalContainer": false,
                "right": "20dp",
                "skin": "slFbox",
                "top": "0px",
                "width": "97%",
                "zIndex": 1
            }, {}, {});
            flxERAttributesListContainer.setDefaultUnit(kony.flex.DP);
            var flxERAttributeListHeaders = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "50dp",
                "id": "flxERAttributeListHeaders",
                "isVisible": true,
                "layoutType": kony.flex.FLOW_HORIZONTAL,
                "left": "0dp",
                "isModalContainer": false,
                "right": "20dp",
                "skin": "slFbox",
                "top": "0",
                "width": kony.flex.USE_PREFFERED_SIZE
            }, {}, {});
            flxERAttributeListHeaders.setDefaultUnit(kony.flex.DP);
            var flxERDatasetsHeader = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "100%",
                "id": "flxERDatasetsHeader",
                "isVisible": true,
                "layoutType": kony.flex.FLOW_HORIZONTAL,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0dp",
                "width": "24%"
            }, {}, {});
            flxERDatasetsHeader.setDefaultUnit(kony.flex.DP);
            var lblERDatasetHeader = new kony.ui.Label({
                "id": "lblERDatasetHeader",
                "isVisible": true,
                "left": "0",
                "skin": "sknLbl73757C11px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmAdManagement.DatasetCAPS\")",
                "top": "21dp",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var lblERFilterDataset = new kony.ui.Label({
                "centerY": "50%",
                "id": "lblERFilterDataset",
                "isVisible": true,
                "left": "7dp",
                "skin": "sknIcon15px",
                "text": "",
                "top": "21dp",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {
                "hoverSkin": "sknlblCursorFont"
            });
            flxERDatasetsHeader.add(lblERDatasetHeader, lblERFilterDataset);
            var flxERAttrHeader = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "100%",
                "id": "flxERAttrHeader",
                "isVisible": true,
                "layoutType": kony.flex.FLOW_HORIZONTAL,
                "left": "1%",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0dp",
                "width": "24%"
            }, {}, {});
            flxERAttrHeader.setDefaultUnit(kony.flex.DP);
            var lblERAttributeHeader = new kony.ui.Label({
                "id": "lblERAttributeHeader",
                "isVisible": true,
                "left": "0dp",
                "skin": "sknLbl73757C11px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmAdManagement.AttributeCAPS\")",
                "top": "21dp",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var lblERSortAttribute = new kony.ui.Label({
                "id": "lblERSortAttribute",
                "isVisible": true,
                "left": "7dp",
                "skin": "sknIcon15px",
                "text": "",
                "top": "21dp",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {
                "hoverSkin": "sknlblCursorFont"
            });
            flxERAttrHeader.add(lblERAttributeHeader, lblERSortAttribute);
            var lblERAttributeCriteriaHeader = new kony.ui.Label({
                "id": "lblERAttributeCriteriaHeader",
                "isVisible": true,
                "left": "1%",
                "skin": "sknLbl73757C11px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmAdManagement.CriteriaCAPS\")",
                "top": "21dp",
                "width": "24%",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var lblERAttributeValueHeader = new kony.ui.Label({
                "id": "lblERAttributeValueHeader",
                "isVisible": true,
                "left": "1%",
                "skin": "sknLbl73757C11px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmAdManagement.ValueCAPS\")",
                "top": "21dp",
                "width": "24%",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxERAttributeListHeaders.add(flxERDatasetsHeader, flxERAttrHeader, lblERAttributeCriteriaHeader, lblERAttributeValueHeader);
            var lblViewAttrSep3 = new kony.ui.Label({
                "bottom": "0px",
                "height": "1px",
                "id": "lblViewAttrSep3",
                "isVisible": true,
                "left": "0dp",
                "right": "20dp",
                "skin": "sknLblTableHeaderLine",
                "text": "-",
                "top": "50dp",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var segERAttributeList = new kony.ui.SegmentedUI2({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "bottom": "0px",
                "data": [{
                    "lblAttribute": "Attribute",
                    "lblCriteria": "Criteria",
                    "lblDataset": "DATASET",
                    "lblValue": "Value"
                }],
                "groupCells": false,
                "id": "segERAttributeList",
                "isVisible": true,
                "left": "0px",
                "needPageIndicator": true,
                "pageOffDotImage": "pageoffdot.png",
                "pageOnDotImage": "pageondot.png",
                "retainSelection": false,
                "right": "20px",
                "rowFocusSkin": "seg2Focus",
                "rowSkin": "seg2Normal",
                "rowTemplate": "flxAttributeRow",
                "sectionHeaderSkin": "sliPhoneSegmentHeader",
                "selectionBehavior": constants.SEGUI_DEFAULT_BEHAVIOR,
                "separatorRequired": false,
                "showScrollbars": true,
                "top": "52px",
                "viewType": constants.SEGUI_VIEW_TYPE_TABLEVIEW,
                "widgetDataMap": {
                    "flxAttributeRow": "flxAttributeRow",
                    "flxRow": "flxRow",
                    "lblAttribute": "lblAttribute",
                    "lblCriteria": "lblCriteria",
                    "lblDataset": "lblDataset",
                    "lblValue": "lblValue"
                },
                "zIndex": 1
            }, {
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var flxNoExistingAttributesFound = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "bottom": "0dp",
                "clipBounds": true,
                "id": "flxNoExistingAttributesFound",
                "isVisible": false,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "55dp",
                "width": "100%",
                "zIndex": 1
            }, {}, {});
            flxNoExistingAttributesFound.setDefaultUnit(kony.flex.DP);
            var lblNoExistingAttributesFound = new kony.ui.Label({
                "centerX": "50%",
                "centerY": "50%",
                "id": "lblNoExistingAttributesFound",
                "isVisible": true,
                "skin": "sknLblLato84939e13px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.Messages.SuggestionsNoResultsFound\")",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxNoExistingAttributesFound.add(lblNoExistingAttributesFound);
            var statusFilterMenu = new com.adminConsole.common.statusFilterMenu({
                "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                "id": "statusFilterMenu",
                "isVisible": false,
                "layoutType": kony.flex.FREE_FORM,
                "left": "36dp",
                "masterType": constants.MASTER_TYPE_DEFAULT,
                "maxWidth": "50%",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "40dp",
                "width": "30%",
                "zIndex": 5,
                "overrides": {
                    "imgUpArrow": {
                        "left": "20dp",
                        "right": "viz.val_cleared",
                        "src": "uparrow_2x.png"
                    },
                    "statusFilterMenu": {
                        "isVisible": false,
                        "left": "36dp",
                        "maxWidth": "50%",
                        "minWidth": "viz.val_cleared",
                        "top": "40dp",
                        "width": "30%"
                    }
                }
            }, {
                "overrides": {}
            }, {
                "overrides": {}
            });
            flxERAttributesListContainer.add(flxERAttributeListHeaders, lblViewAttrSep3, segERAttributeList, flxNoExistingAttributesFound, statusFilterMenu);
            flxERAttributeListDetails.add(flxERAttributeDescriptionUsers, lblSelectedRoleHeaderSeperator, flxERAttributesListContainer);
            flxExistingRoleDetailsInner.add(lblSelectedRoleName, flxERAttributeListDetails);
            var flxAddExistingRoleMsgContainer = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "centerX": "50%",
                "centerY": "50%",
                "clipBounds": true,
                "height": "98dp",
                "id": "flxAddExistingRoleMsgContainer",
                "isVisible": false,
                "layoutType": kony.flex.FREE_FORM,
                "isModalContainer": false,
                "skin": "slFbox",
                "width": "420dp",
                "zIndex": 1
            }, {}, {});
            flxAddExistingRoleMsgContainer.setDefaultUnit(kony.flex.DP);
            var imgExistingRoleSelectCheckbox = new kony.ui.Image2({
                "centerX": "50%",
                "height": "67dp",
                "id": "imgExistingRoleSelectCheckbox",
                "isVisible": true,
                "skin": "slImage",
                "src": "selectacheckbox.png",
                "top": "0dp",
                "width": "83dp",
                "zIndex": 1
            }, {
                "imageScaleMode": constants.IMAGE_SCALE_MODE_MAINTAIN_ASPECT_RATIO,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var lblAddExistingRoleMsg = new kony.ui.Label({
                "bottom": "0dp",
                "id": "lblAddExistingRoleMsg",
                "isVisible": true,
                "left": "10dp",
                "skin": "sknlbl485C75LatoSemiBold13px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmAdManagement.selectNoSegmentMsg\")",
                "width": "100%",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxAddExistingRoleMsgContainer.add(imgExistingRoleSelectCheckbox, lblAddExistingRoleMsg);
            flxExistingRoleDetailsContainer.add(flxExistingRoleDetailsInner, flxAddExistingRoleMsgContainer);
            flxAddExistingRoleContainer.add(flxSelectRoleContainer, flxExistingRoleDetailsContainer);
            var flxExistingRolePopUpButtons = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "bottom": "0dp",
                "clipBounds": true,
                "height": "80px",
                "id": "flxExistingRolePopUpButtons",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "sknflxBgE4E6EC",
                "width": "100%",
                "zIndex": 1
            }, {}, {});
            flxExistingRolePopUpButtons.setDefaultUnit(kony.flex.DP);
            var btnExistingRoleCancel = new kony.ui.Button({
                "centerY": "50%",
                "height": "40px",
                "id": "btnExistingRoleCancel",
                "isVisible": true,
                "right": "130px",
                "skin": "sknbtnf7f7faLatoRegular4f555dBorder1px8b96a5",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.permission.CANCEL\")",
                "top": "0%",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 20
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_CENTER,
                "displayText": true,
                "padding": [3, 0, 3, 0],
                "paddingInPixel": false
            }, {
                "hoverSkin": "sknbtnffffffLatoRegular4f555dBorder1px485c75"
            });
            var btnExistingRoleAdd = new kony.ui.Button({
                "centerY": "50%",
                "focusSkin": "sknBtn003E75LatoRegular13pxFFFFFFRad20px",
                "height": "40px",
                "id": "btnExistingRoleAdd",
                "isVisible": true,
                "minWidth": "100px",
                "right": "20px",
                "skin": "sknBtn003E75LatoRegular13pxFFFFFFRad20px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.ConfigurationBundles.buttonAdd\")",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 20
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_CENTER,
                "displayText": true,
                "padding": [3, 0, 3, 0],
                "paddingInPixel": false
            }, {
                "hoverSkin": "sknBtn005198LatoRegular13pxFFFFFFRad20px"
            });
            flxExistingRolePopUpButtons.add(btnExistingRoleCancel, btnExistingRoleAdd);
            flxAddExistingRolesPopUp.add(flxAddExistingRolesPopUpTopColor, flxAddExistingRolesPopUpClose, lblExistingRolesPopUpMainMessage, flxSelectRoleErrorMsg, flxAddExistingRoleContainer, flxExistingRolePopUpButtons);
            var flxAddEventsPopUp = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "centerX": "50%",
                "centerY": "50%",
                "clipBounds": false,
                "height": "80%",
                "id": "flxAddEventsPopUp",
                "isVisible": false,
                "layoutType": kony.flex.FREE_FORM,
                "isModalContainer": false,
                "skin": "flxNoShadowNoBorder",
                "width": "986dp",
                "zIndex": 10
            }, {}, {});
            flxAddEventsPopUp.setDefaultUnit(kony.flex.DP);
            var flxEventsPopUpTopColor = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "10px",
                "id": "flxEventsPopUpTopColor",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "sknFlxInfoToastBg357C9ENoRadius",
                "top": "0dp",
                "width": "100%",
                "zIndex": 1
            }, {}, {});
            flxEventsPopUpTopColor.setDefaultUnit(kony.flex.DP);
            flxEventsPopUpTopColor.add();
            var flxEventsPopUpClose = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "15px",
                "id": "flxEventsPopUpClose",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "isModalContainer": false,
                "right": 20,
                "skin": "slFbox",
                "top": "30dp",
                "width": "15px",
                "zIndex": 1
            }, {}, {
                "hoverSkin": "sknCursor"
            });
            flxEventsPopUpClose.setDefaultUnit(kony.flex.DP);
            var lblEventsPopUpClose = new kony.ui.Label({
                "height": "15dp",
                "id": "lblEventsPopUpClose",
                "isVisible": true,
                "left": "0dp",
                "skin": "sknfontIconListbox15px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.userwidgetmodel.lblClearSearch\")",
                "top": "0dp",
                "width": "15dp",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxEventsPopUpClose.add(lblEventsPopUpClose);
            var lblEventsPopUpHeader = new kony.ui.Label({
                "height": "19dp",
                "id": "lblEventsPopUpHeader",
                "isVisible": true,
                "left": "20px",
                "skin": "sknLbl192b45LatoReg16px",
                "text": "Select Events",
                "top": "40px",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var lblSelectDatasetEvents = new kony.ui.Label({
                "id": "lblSelectDatasetEvents",
                "isVisible": true,
                "left": "20dp",
                "skin": "sknLbl192B45LatoRegular14px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmAdManagement.selectedEvents\")",
                "top": "80dp",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var flxDatasetCountEventsSelected = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "20dp",
                "id": "flxDatasetCountEventsSelected",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "125dp",
                "isModalContainer": false,
                "skin": "sknFlx006CCA",
                "top": "80dp",
                "width": "40dp",
                "zIndex": 1
            }, {}, {});
            flxDatasetCountEventsSelected.setDefaultUnit(kony.flex.DP);
            var lblDatasetCountEventsSelected = new kony.ui.Label({
                "centerX": "50%",
                "centerY": "40%",
                "id": "lblDatasetCountEventsSelected",
                "isVisible": true,
                "skin": "sknLblffffff13px",
                "text": "00",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxDatasetCountEventsSelected.add(lblDatasetCountEventsSelected);
            var flxResetEventsSelection = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                "clipBounds": true,
                "id": "flxResetEventsSelection",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "180dp",
                "isModalContainer": false,
                "skin": "hoverhandSkin",
                "top": "82dp",
                "width": "35dp"
            }, {}, {});
            flxResetEventsSelection.setDefaultUnit(kony.flex.DP);
            var lblResetEventsSelection = new kony.ui.Label({
                "id": "lblResetEventsSelection",
                "isVisible": true,
                "left": "0dp",
                "skin": "sknLblLatoReg117eb013px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.Common.button.Reset\")",
                "top": "0dp",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxResetEventsSelection.add(lblResetEventsSelection);
            var lblSeparatorEventsTopHeader = new kony.ui.Label({
                "height": "1px",
                "id": "lblSeparatorEventsTopHeader",
                "isVisible": true,
                "left": "20dp",
                "right": "20dp",
                "skin": "sknLblBgE4E6ECSeperator",
                "text": "Label",
                "top": "119dp",
                "zIndex": 2
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_TOP_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var flxSelectEventsHeaders = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "55dp",
                "id": "flxSelectEventsHeaders",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "20dp",
                "isModalContainer": false,
                "right": 20,
                "skin": "slFbox",
                "top": "120dp",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {}, {});
            flxSelectEventsHeaders.setDefaultUnit(kony.flex.DP);
            var lblEventNameHeader = new kony.ui.Label({
                "centerY": "50%",
                "id": "lblEventNameHeader",
                "isVisible": true,
                "left": "10dp",
                "skin": "sknLbl485C75LatoRegular13Px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmAdManagement.eventName\")",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var lblEventTypeSourceHeader = new kony.ui.Label({
                "centerY": "50%",
                "id": "lblEventTypeSourceHeader",
                "isVisible": true,
                "left": "39%",
                "skin": "sknLbl485C75LatoRegular13Px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmAdManagement.type\")",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var flxEventIdentifierCodeHeader = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                "centerY": "50%",
                "clipBounds": true,
                "id": "flxEventIdentifierCodeHeader",
                "isVisible": true,
                "layoutType": kony.flex.FLOW_HORIZONTAL,
                "left": "80%",
                "isModalContainer": false,
                "skin": "slFbox",
                "width": "70dp"
            }, {}, {});
            flxEventIdentifierCodeHeader.setDefaultUnit(kony.flex.DP);
            var lblEventIdentifierCodeHeader = new kony.ui.Label({
                "id": "lblEventIdentifierCodeHeader",
                "isVisible": true,
                "left": "0dp",
                "skin": "sknLbl485C75LatoRegular13Px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmAdManagement.identifier\")",
                "top": "0dp",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var fontIconSortEventCode = new kony.ui.Label({
                "id": "fontIconSortEventCode",
                "isVisible": false,
                "left": "5dp",
                "skin": "sknIcon15px",
                "text": "",
                "top": "0dp",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {
                "hoverSkin": "sknlblCursorFont"
            });
            flxEventIdentifierCodeHeader.add(lblEventIdentifierCodeHeader, fontIconSortEventCode);
            var flxEventUpdatedDateHeader = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                "centerY": "50%",
                "clipBounds": true,
                "id": "flxEventUpdatedDateHeader",
                "isVisible": false,
                "layoutType": kony.flex.FLOW_HORIZONTAL,
                "left": "80%",
                "isModalContainer": false,
                "skin": "hoverhandSkin",
                "width": "100dp"
            }, {}, {});
            flxEventUpdatedDateHeader.setDefaultUnit(kony.flex.DP);
            var lblEventUpdatedDateHeader = new kony.ui.Label({
                "id": "lblEventUpdatedDateHeader",
                "isVisible": true,
                "left": "0dp",
                "skin": "sknLbl485C75LatoRegular13Px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmAdManagement.lastUpdated\")",
                "top": "0dp",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var fontIconSortEventUpdatedDate = new kony.ui.Label({
                "id": "fontIconSortEventUpdatedDate",
                "isVisible": false,
                "left": "5dp",
                "skin": "sknIcon15px",
                "text": "",
                "top": "0dp",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {
                "hoverSkin": "sknlblCursorFont"
            });
            flxEventUpdatedDateHeader.add(lblEventUpdatedDateHeader, fontIconSortEventUpdatedDate);
            var lblSeparatorEventHeader = new kony.ui.Label({
                "bottom": "0dp",
                "height": "1px",
                "id": "lblSeparatorEventHeader",
                "isVisible": true,
                "left": "0dp",
                "right": "0dp",
                "skin": "sknlblSeperatorD7D9E0",
                "text": "Label",
                "zIndex": 2
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_TOP_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxSelectEventsHeaders.add(lblEventNameHeader, lblEventTypeSourceHeader, flxEventIdentifierCodeHeader, flxEventUpdatedDateHeader, lblSeparatorEventHeader);
            var segSelectEvents = new kony.ui.SegmentedUI2({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "bottom": "81dp",
                "data": [{
                    "fontIconSelectOption": "",
                    "lblDate": "Date",
                    "lblIdentifier": "Identifier",
                    "lblName": "Name",
                    "lblSeparator": "Label",
                    "lblType": "Type"
                }, {
                    "fontIconSelectOption": "",
                    "lblDate": "Date",
                    "lblIdentifier": "Identifier",
                    "lblName": "Name",
                    "lblSeparator": "Label",
                    "lblType": "Type"
                }, {
                    "fontIconSelectOption": "",
                    "lblDate": "Date",
                    "lblIdentifier": "Identifier",
                    "lblName": "Name",
                    "lblSeparator": "Label",
                    "lblType": "Type"
                }],
                "groupCells": false,
                "id": "segSelectEvents",
                "isVisible": true,
                "left": "20dp",
                "needPageIndicator": true,
                "pageOffDotImage": "pageoffdot.png",
                "pageOnDotImage": "pageondot.png",
                "retainSelection": false,
                "right": "20dp",
                "rowFocusSkin": "seg2Focus",
                "rowSkin": "sknseg3pxRadius",
                "rowTemplate": "flxEventsDataSet",
                "sectionHeaderSkin": "sliPhoneSegmentHeader",
                "selectionBehavior": constants.SEGUI_DEFAULT_BEHAVIOR,
                "separatorRequired": false,
                "showScrollbars": false,
                "top": "176dp",
                "viewType": constants.SEGUI_VIEW_TYPE_TABLEVIEW,
                "widgetDataMap": {
                    "flxEventsDataSet": "flxEventsDataSet",
                    "flxSelectOption": "flxSelectOption",
                    "fontIconSelectOption": "fontIconSelectOption",
                    "lblDate": "lblDate",
                    "lblIdentifier": "lblIdentifier",
                    "lblName": "lblName",
                    "lblSeparator": "lblSeparator",
                    "lblType": "lblType"
                },
                "zIndex": 1
            }, {
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var flxNoResultFoundEvents = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                "clipBounds": true,
                "id": "flxNoResultFoundEvents",
                "isVisible": false,
                "layoutType": kony.flex.FREE_FORM,
                "left": "20dp",
                "isModalContainer": false,
                "right": "20dp",
                "skin": "slFbox",
                "top": "176dp",
                "zIndex": 1
            }, {}, {});
            flxNoResultFoundEvents.setDefaultUnit(kony.flex.DP);
            var rtxSearchMesg = new kony.ui.RichText({
                "bottom": "50px",
                "centerX": "52%",
                "id": "rtxSearchMesg",
                "isVisible": true,
                "left": "0",
                "skin": "sknRtxLato84939e12Px",
                "text": "No Results found",
                "top": "90px",
                "width": "80%",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_CENTER,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxNoResultFoundEvents.add(rtxSearchMesg);
            var flxAddEventsPopUpButtons = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "bottom": "0dp",
                "clipBounds": true,
                "height": "80px",
                "id": "flxAddEventsPopUpButtons",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "sknflxBgE4E6EC",
                "width": "100%",
                "zIndex": 1
            }, {}, {});
            flxAddEventsPopUpButtons.setDefaultUnit(kony.flex.DP);
            var btnEventsCancel = new kony.ui.Button({
                "centerY": "50%",
                "height": "40px",
                "id": "btnEventsCancel",
                "isVisible": true,
                "right": "130px",
                "skin": "sknbtnf7f7faLatoRegular4f555dBorder1px8b96a5",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.permission.CANCEL\")",
                "top": "0%",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 20
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_CENTER,
                "displayText": true,
                "padding": [3, 0, 3, 0],
                "paddingInPixel": false
            }, {
                "hoverSkin": "sknbtnffffffLatoRegular4f555dBorder1px485c75"
            });
            var btnEventsAdd = new kony.ui.Button({
                "centerY": "50%",
                "focusSkin": "sknBtn003E75LatoRegular13pxFFFFFFRad20px",
                "height": "40px",
                "id": "btnEventsAdd",
                "isVisible": true,
                "minWidth": "100px",
                "right": "20px",
                "skin": "sknBtn003E75LatoRegular13pxFFFFFFRad20px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.DragBox.ADD\")",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 20
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_CENTER,
                "displayText": true,
                "padding": [3, 0, 3, 0],
                "paddingInPixel": false
            }, {
                "hoverSkin": "sknBtn005198LatoRegular13pxFFFFFFRad20px"
            });
            flxAddEventsPopUpButtons.add(btnEventsCancel, btnEventsAdd);
            flxAddEventsPopUp.add(flxEventsPopUpTopColor, flxEventsPopUpClose, lblEventsPopUpHeader, lblSelectDatasetEvents, flxDatasetCountEventsSelected, flxResetEventsSelection, lblSeparatorEventsTopHeader, flxSelectEventsHeaders, segSelectEvents, flxNoResultFoundEvents, flxAddEventsPopUpButtons);
            var flxAddPopupEventsPopup = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "centerX": "50%",
                "centerY": "50%",
                "clipBounds": false,
                "height": "80%",
                "id": "flxAddPopupEventsPopup",
                "isVisible": false,
                "layoutType": kony.flex.FREE_FORM,
                "isModalContainer": false,
                "skin": "flxNoShadowNoBorder",
                "width": "986dp",
                "zIndex": 10
            }, {}, {});
            flxAddPopupEventsPopup.setDefaultUnit(kony.flex.DP);
            var flxPopupEventsTopColor = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "10px",
                "id": "flxPopupEventsTopColor",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "sknFlxInfoToastBg357C9ENoRadius",
                "top": "0dp",
                "width": "100%",
                "zIndex": 1
            }, {}, {});
            flxPopupEventsTopColor.setDefaultUnit(kony.flex.DP);
            flxPopupEventsTopColor.add();
            var flxPopupEventsPopUpClose = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "15px",
                "id": "flxPopupEventsPopUpClose",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "isModalContainer": false,
                "right": 20,
                "skin": "slFbox",
                "top": "30dp",
                "width": "15px",
                "zIndex": 1
            }, {}, {
                "hoverSkin": "sknCursor"
            });
            flxPopupEventsPopUpClose.setDefaultUnit(kony.flex.DP);
            var lblPopupEventsPopUpClose = new kony.ui.Label({
                "height": "15dp",
                "id": "lblPopupEventsPopUpClose",
                "isVisible": true,
                "left": "0dp",
                "skin": "sknfontIconListbox15px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.userwidgetmodel.lblClearSearch\")",
                "top": "0dp",
                "width": "15dp",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxPopupEventsPopUpClose.add(lblPopupEventsPopUpClose);
            var lblPopupEventsTopHeader = new kony.ui.Label({
                "height": "19dp",
                "id": "lblPopupEventsTopHeader",
                "isVisible": true,
                "left": "20px",
                "skin": "sknLbl192b45LatoReg16px",
                "text": "Define Popup Events",
                "top": "40px",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var lblTabSeperator = new kony.ui.Label({
                "height": "1px",
                "id": "lblTabSeperator",
                "isVisible": true,
                "left": "20dp",
                "right": "20dp",
                "skin": "sknlblSeperatorD7D9E0",
                "text": ".",
                "top": "133dp",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var flxPopEventsTabsContainer = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                "clipBounds": true,
                "id": "flxPopEventsTabsContainer",
                "isVisible": true,
                "layoutType": kony.flex.FLOW_HORIZONTAL,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "94dp",
                "width": "100%",
                "zIndex": 1
            }, {}, {});
            flxPopEventsTabsContainer.setDefaultUnit(kony.flex.DP);
            var btnTab1 = new kony.ui.Button({
                "bottom": "0dp",
                "height": "40dp",
                "id": "btnTab1",
                "isVisible": true,
                "left": "20dp",
                "skin": "sknBtnBgffffffBrD7D9E0Rd3px485B75Bold12px",
                "text": "WEB POPUP AD BANNER",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_CENTER,
                "displayText": true,
                "padding": [3, 0, 3, 0],
                "paddingInPixel": false
            }, {});
            var btnTab2 = new kony.ui.Button({
                "bottom": "0dp",
                "height": "40dp",
                "id": "btnTab2",
                "isVisible": true,
                "left": "10dp",
                "skin": "sknBtnBgD2D7E2Rou3pxLato485B7512px",
                "text": "MOBILE POPUP AD BANNER",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_CENTER,
                "displayText": true,
                "padding": [3, 0, 3, 0],
                "paddingInPixel": false
            }, {});
            var btnTab3 = new kony.ui.Button({
                "bottom": "0dp",
                "focusSkin": "defBtnFocus",
                "height": "40dp",
                "id": "btnTab3",
                "isVisible": false,
                "left": "10dp",
                "skin": "sknBtnBgD2D7E2Rou3pxLato485B7512px",
                "text": "TAB",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_CENTER,
                "displayText": true,
                "padding": [3, 0, 3, 0],
                "paddingInPixel": false
            }, {});
            flxPopEventsTabsContainer.add(btnTab1, btnTab2, btnTab3);
            var flxAddPopupEventsPopUpButtons = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "bottom": "0dp",
                "clipBounds": true,
                "height": "80px",
                "id": "flxAddPopupEventsPopUpButtons",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "sknflxBgE4E6EC",
                "width": "100%",
                "zIndex": 1
            }, {}, {});
            flxAddPopupEventsPopUpButtons.setDefaultUnit(kony.flex.DP);
            var btnPopupEventsCancel = new kony.ui.Button({
                "centerY": "50%",
                "height": "40px",
                "id": "btnPopupEventsCancel",
                "isVisible": true,
                "right": "130px",
                "skin": "sknbtnf7f7faLatoRegular4f555dBorder1px8b96a5",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.permission.CANCEL\")",
                "top": "0%",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 20
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_CENTER,
                "displayText": true,
                "padding": [3, 0, 3, 0],
                "paddingInPixel": false
            }, {
                "hoverSkin": "sknbtnffffffLatoRegular4f555dBorder1px485c75"
            });
            var btnPopupEventsAdd = new kony.ui.Button({
                "centerY": "50%",
                "focusSkin": "sknBtn003E75LatoRegular13pxFFFFFFRad20px",
                "height": "40px",
                "id": "btnPopupEventsAdd",
                "isVisible": true,
                "minWidth": "100px",
                "right": "20px",
                "skin": "sknBtn003E75LatoRegular13pxFFFFFFRad20px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.DragBox.ADD\")",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 20
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_CENTER,
                "displayText": true,
                "padding": [3, 0, 3, 0],
                "paddingInPixel": false
            }, {
                "hoverSkin": "sknBtn005198LatoRegular13pxFFFFFFRad20px"
            });
            flxAddPopupEventsPopUpButtons.add(btnPopupEventsCancel, btnPopupEventsAdd);
            var flxPopupScreenWEB = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "bottom": "81dp",
                "clipBounds": true,
                "id": "flxPopupScreenWEB",
                "isVisible": false,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "150dp",
                "width": "100%"
            }, {}, {});
            flxPopupScreenWEB.setDefaultUnit(kony.flex.DP);
            var flxNoResultFoundPopupEventsWEB = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                "clipBounds": true,
                "id": "flxNoResultFoundPopupEventsWEB",
                "isVisible": false,
                "layoutType": kony.flex.FREE_FORM,
                "left": "20dp",
                "isModalContainer": false,
                "right": "20dp",
                "skin": "slFbox",
                "top": "116dp",
                "zIndex": 1
            }, {}, {});
            flxNoResultFoundPopupEventsWEB.setDefaultUnit(kony.flex.DP);
            var rtxSearchMesgPopupEventsWEB = new kony.ui.RichText({
                "bottom": "50px",
                "centerX": "52%",
                "id": "rtxSearchMesgPopupEventsWEB",
                "isVisible": true,
                "left": "0",
                "skin": "sknRtxLato84939e12Px",
                "text": "No Results found",
                "top": "90px",
                "width": "80%",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_CENTER,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxNoResultFoundPopupEventsWEB.add(rtxSearchMesgPopupEventsWEB);
            var segSelectPopupEventsWEB = new kony.ui.SegmentedUI2({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "bottom": "0dp",
                "data": [{
                    "fontIconSelectOption": "",
                    "lblDate": "Date",
                    "lblIdentifier": "Identifier",
                    "lblName": "Name",
                    "lblSeparator": "Label",
                    "lblType": "Type"
                }, {
                    "fontIconSelectOption": "",
                    "lblDate": "Date",
                    "lblIdentifier": "Identifier",
                    "lblName": "Name",
                    "lblSeparator": "Label",
                    "lblType": "Type"
                }, {
                    "fontIconSelectOption": "",
                    "lblDate": "Date",
                    "lblIdentifier": "Identifier",
                    "lblName": "Name",
                    "lblSeparator": "Label",
                    "lblType": "Type"
                }],
                "groupCells": false,
                "id": "segSelectPopupEventsWEB",
                "isVisible": true,
                "left": "20dp",
                "needPageIndicator": true,
                "pageOffDotImage": "pageoffdot.png",
                "pageOnDotImage": "pageondot.png",
                "retainSelection": false,
                "right": "20dp",
                "rowFocusSkin": "seg2Focus",
                "rowSkin": "sknseg3pxRadius",
                "rowTemplate": "flxEventsDataSet",
                "sectionHeaderSkin": "sliPhoneSegmentHeader",
                "selectionBehavior": constants.SEGUI_DEFAULT_BEHAVIOR,
                "separatorRequired": false,
                "showScrollbars": false,
                "top": "116dp",
                "viewType": constants.SEGUI_VIEW_TYPE_TABLEVIEW,
                "widgetDataMap": {
                    "flxEventsDataSet": "flxEventsDataSet",
                    "flxSelectOption": "flxSelectOption",
                    "fontIconSelectOption": "fontIconSelectOption",
                    "lblDate": "lblDate",
                    "lblIdentifier": "lblIdentifier",
                    "lblName": "lblName",
                    "lblSeparator": "lblSeparator",
                    "lblType": "lblType"
                },
                "zIndex": 1
            }, {
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var flxSelectPopupEventsHeadersWEB = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "55dp",
                "id": "flxSelectPopupEventsHeadersWEB",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "20dp",
                "isModalContainer": false,
                "right": 20,
                "skin": "slFbox",
                "top": "60dp",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {}, {});
            flxSelectPopupEventsHeadersWEB.setDefaultUnit(kony.flex.DP);
            var flxPopupScreensHeaderWEB = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                "centerY": "50%",
                "clipBounds": true,
                "id": "flxPopupScreensHeaderWEB",
                "isVisible": true,
                "layoutType": kony.flex.FLOW_HORIZONTAL,
                "left": "10dp",
                "isModalContainer": false,
                "skin": "slFbox",
                "width": "115dp"
            }, {}, {});
            flxPopupScreensHeaderWEB.setDefaultUnit(kony.flex.DP);
            var lblPopupScreensWEB = new kony.ui.Label({
                "id": "lblPopupScreensWEB",
                "isVisible": true,
                "left": "0dp",
                "skin": "sknLbl485C75LatoRegular13Px",
                "text": "SCREEN NAME",
                "top": "0dp",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var fontIconSortPopupScreensWEB = new kony.ui.Label({
                "id": "fontIconSortPopupScreensWEB",
                "isVisible": true,
                "left": "5dp",
                "skin": "sknIcon15px",
                "text": "",
                "top": "0dp",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {
                "hoverSkin": "sknlblCursorFont"
            });
            flxPopupScreensHeaderWEB.add(lblPopupScreensWEB, fontIconSortPopupScreensWEB);
            var lblSeparatorPopupEventHeaderWEB = new kony.ui.Label({
                "bottom": "0dp",
                "height": "1px",
                "id": "lblSeparatorPopupEventHeaderWEB",
                "isVisible": true,
                "left": "0dp",
                "right": "0dp",
                "skin": "sknlblSeperatorD7D9E0",
                "text": "Label",
                "zIndex": 2
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_TOP_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxSelectPopupEventsHeadersWEB.add(flxPopupScreensHeaderWEB, lblSeparatorPopupEventHeaderWEB);
            var lblSeparatorPopupEventsTopHeaderWEB = new kony.ui.Label({
                "height": "1px",
                "id": "lblSeparatorPopupEventsTopHeaderWEB",
                "isVisible": true,
                "left": "20dp",
                "right": "20dp",
                "skin": "sknLblBgE4E6ECSeperator",
                "text": "Label",
                "top": "60dp",
                "zIndex": 2
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_TOP_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var flxDatasetSearchPopupEventsWEB = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "45px",
                "id": "flxDatasetSearchPopupEventsWEB",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "isModalContainer": false,
                "right": "20dp",
                "skin": "sknflxd5d9ddop100",
                "top": "0dp",
                "width": "500px",
                "zIndex": 1
            }, {}, {});
            flxDatasetSearchPopupEventsWEB.setDefaultUnit(kony.flex.DP);
            var fontIconSearchImgPopupEventsWEB = new kony.ui.Label({
                "centerY": "50%",
                "id": "fontIconSearchImgPopupEventsWEB",
                "isVisible": true,
                "left": "12px",
                "skin": "sknFontIconSearchImg20px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.userwidgetmodel.lblSearchIcon\")",
                "top": "0dp",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var tbxDatasetSearchBoxPopupEventsWEB = new kony.ui.TextBox2({
                "autoCapitalize": constants.TEXTBOX_AUTO_CAPITALIZE_NONE,
                "centerY": "50%",
                "height": "30px",
                "id": "tbxDatasetSearchBoxPopupEventsWEB",
                "isVisible": true,
                "keyBoardStyle": constants.TEXTBOX_KEY_BOARD_STYLE_DEFAULT,
                "left": "35dp",
                "onTouchStart": controller.AS_TextField_cfab06859f0f43eca99495891ee290d5,
                "placeholder": "Search Web Screen Names",
                "right": "35px",
                "secureTextEntry": false,
                "skin": "skntbxffffffNoBorderlato35475f14px",
                "textInputMode": constants.TEXTBOX_INPUT_MODE_ANY,
                "zIndex": 1
            }, {
                "containerHeightMode": constants.TEXTBOX_FONT_METRICS_DRIVEN_HEIGHT,
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [2, 0, 0, 0],
                "paddingInPixel": false
            }, {
                "autoCorrect": false
            });
            var flxPopupEventsClearSearchWEB = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "100%",
                "id": "flxPopupEventsClearSearchWEB",
                "isVisible": false,
                "layoutType": kony.flex.FREE_FORM,
                "isModalContainer": false,
                "right": "0px",
                "skin": "sknCursor",
                "top": "0dp",
                "width": "35px",
                "zIndex": 1
            }, {}, {
                "hoverSkin": "sknCursor"
            });
            flxPopupEventsClearSearchWEB.setDefaultUnit(kony.flex.DP);
            var fontIconCrossPopupEventsWEB = new kony.ui.Label({
                "centerY": "50%",
                "id": "fontIconCrossPopupEventsWEB",
                "isVisible": true,
                "left": "5px",
                "skin": "sknFontIconSearchCross16px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.userwidgetmodel.lblClearSearch\")",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxPopupEventsClearSearchWEB.add(fontIconCrossPopupEventsWEB);
            flxDatasetSearchPopupEventsWEB.add(fontIconSearchImgPopupEventsWEB, tbxDatasetSearchBoxPopupEventsWEB, flxPopupEventsClearSearchWEB);
            var flxResetPopupEventsSelectionWEB = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                "clipBounds": true,
                "id": "flxResetPopupEventsSelectionWEB",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "200dp",
                "isModalContainer": false,
                "skin": "hoverhandSkin",
                "top": "12dp",
                "width": "35dp"
            }, {}, {});
            flxResetPopupEventsSelectionWEB.setDefaultUnit(kony.flex.DP);
            var lblResetPopupEventsSelectionWEB = new kony.ui.Label({
                "id": "lblResetPopupEventsSelectionWEB",
                "isVisible": true,
                "left": "0dp",
                "skin": "sknLblLatoReg117eb013px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.Common.button.Reset\")",
                "top": "0dp",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxResetPopupEventsSelectionWEB.add(lblResetPopupEventsSelectionWEB);
            var flxDatasetCountPopupEventsWEB = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "20dp",
                "id": "flxDatasetCountPopupEventsWEB",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "145dp",
                "isModalContainer": false,
                "skin": "sknFlx006CCA",
                "top": "10dp",
                "width": "35dp",
                "zIndex": 1
            }, {}, {});
            flxDatasetCountPopupEventsWEB.setDefaultUnit(kony.flex.DP);
            var lblDatasetCountPopupEventsWEB = new kony.ui.Label({
                "centerX": "50%",
                "centerY": "40%",
                "id": "lblDatasetCountPopupEventsWEB",
                "isVisible": true,
                "skin": "sknLblffffff13px",
                "text": "00",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxDatasetCountPopupEventsWEB.add(lblDatasetCountPopupEventsWEB);
            var lblSelectDatasetPopupEventsWEB = new kony.ui.Label({
                "id": "lblSelectDatasetPopupEventsWEB",
                "isVisible": true,
                "left": "20dp",
                "skin": "sknLbl192B45LatoRegular14px",
                "text": "Selected Screen(s)",
                "top": "10dp",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxPopupScreenWEB.add(flxNoResultFoundPopupEventsWEB, segSelectPopupEventsWEB, flxSelectPopupEventsHeadersWEB, lblSeparatorPopupEventsTopHeaderWEB, flxDatasetSearchPopupEventsWEB, flxResetPopupEventsSelectionWEB, flxDatasetCountPopupEventsWEB, lblSelectDatasetPopupEventsWEB);
            var flxPopupScreenMOB = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "bottom": "81dp",
                "clipBounds": true,
                "id": "flxPopupScreenMOB",
                "isVisible": false,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "150dp",
                "width": "100%"
            }, {}, {});
            flxPopupScreenMOB.setDefaultUnit(kony.flex.DP);
            var flxNoResultFoundPopupEventsMOB = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                "clipBounds": true,
                "id": "flxNoResultFoundPopupEventsMOB",
                "isVisible": false,
                "layoutType": kony.flex.FREE_FORM,
                "left": "20dp",
                "isModalContainer": false,
                "right": "20dp",
                "skin": "slFbox",
                "top": "116dp",
                "zIndex": 1
            }, {}, {});
            flxNoResultFoundPopupEventsMOB.setDefaultUnit(kony.flex.DP);
            var rtxSearchMesgPopupEventsMOB = new kony.ui.RichText({
                "bottom": "50px",
                "centerX": "52%",
                "id": "rtxSearchMesgPopupEventsMOB",
                "isVisible": true,
                "left": "0",
                "skin": "sknRtxLato84939e12Px",
                "text": "No Results found",
                "top": "90px",
                "width": "80%",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_CENTER,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxNoResultFoundPopupEventsMOB.add(rtxSearchMesgPopupEventsMOB);
            var segSelectPopupEventsMOB = new kony.ui.SegmentedUI2({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "bottom": "0dp",
                "data": [{
                    "fontIconSelectOption": "",
                    "lblDate": "Date",
                    "lblIdentifier": "Identifier",
                    "lblName": "Name",
                    "lblSeparator": "Label",
                    "lblType": "Type"
                }, {
                    "fontIconSelectOption": "",
                    "lblDate": "Date",
                    "lblIdentifier": "Identifier",
                    "lblName": "Name",
                    "lblSeparator": "Label",
                    "lblType": "Type"
                }, {
                    "fontIconSelectOption": "",
                    "lblDate": "Date",
                    "lblIdentifier": "Identifier",
                    "lblName": "Name",
                    "lblSeparator": "Label",
                    "lblType": "Type"
                }],
                "groupCells": false,
                "id": "segSelectPopupEventsMOB",
                "isVisible": true,
                "left": "20dp",
                "needPageIndicator": true,
                "pageOffDotImage": "pageoffdot.png",
                "pageOnDotImage": "pageondot.png",
                "retainSelection": false,
                "right": "20dp",
                "rowFocusSkin": "seg2Focus",
                "rowSkin": "sknseg3pxRadius",
                "rowTemplate": "flxEventsDataSet",
                "sectionHeaderSkin": "sliPhoneSegmentHeader",
                "selectionBehavior": constants.SEGUI_DEFAULT_BEHAVIOR,
                "separatorRequired": false,
                "showScrollbars": false,
                "top": "116dp",
                "viewType": constants.SEGUI_VIEW_TYPE_TABLEVIEW,
                "widgetDataMap": {
                    "flxEventsDataSet": "flxEventsDataSet",
                    "flxSelectOption": "flxSelectOption",
                    "fontIconSelectOption": "fontIconSelectOption",
                    "lblDate": "lblDate",
                    "lblIdentifier": "lblIdentifier",
                    "lblName": "lblName",
                    "lblSeparator": "lblSeparator",
                    "lblType": "lblType"
                },
                "zIndex": 1
            }, {
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var flxSelectPopupEventsHeadersMOB = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "55dp",
                "id": "flxSelectPopupEventsHeadersMOB",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "20dp",
                "isModalContainer": false,
                "right": 20,
                "skin": "slFbox",
                "top": "60dp",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {}, {});
            flxSelectPopupEventsHeadersMOB.setDefaultUnit(kony.flex.DP);
            var flxPopupScreensHeaderMOB = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                "centerY": "50%",
                "clipBounds": true,
                "id": "flxPopupScreensHeaderMOB",
                "isVisible": true,
                "layoutType": kony.flex.FLOW_HORIZONTAL,
                "left": "10dp",
                "isModalContainer": false,
                "skin": "slFbox",
                "width": "115dp"
            }, {}, {});
            flxPopupScreensHeaderMOB.setDefaultUnit(kony.flex.DP);
            var lblPopupScreensMOB = new kony.ui.Label({
                "id": "lblPopupScreensMOB",
                "isVisible": true,
                "left": "0dp",
                "skin": "sknLbl485C75LatoRegular13Px",
                "text": "SCREEN NAME",
                "top": "0dp",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var fontIconSortPopupScreensMOB = new kony.ui.Label({
                "id": "fontIconSortPopupScreensMOB",
                "isVisible": true,
                "left": "5dp",
                "skin": "sknIcon15px",
                "text": "",
                "top": "0dp",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {
                "hoverSkin": "sknlblCursorFont"
            });
            flxPopupScreensHeaderMOB.add(lblPopupScreensMOB, fontIconSortPopupScreensMOB);
            var lblSeparatorPopupEventHeaderMOB = new kony.ui.Label({
                "bottom": "0dp",
                "height": "1px",
                "id": "lblSeparatorPopupEventHeaderMOB",
                "isVisible": true,
                "left": "0dp",
                "right": "0dp",
                "skin": "sknlblSeperatorD7D9E0",
                "text": "Label",
                "zIndex": 2
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_TOP_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxSelectPopupEventsHeadersMOB.add(flxPopupScreensHeaderMOB, lblSeparatorPopupEventHeaderMOB);
            var lblSeparatorPopupEventsTopHeaderMOB = new kony.ui.Label({
                "height": "1px",
                "id": "lblSeparatorPopupEventsTopHeaderMOB",
                "isVisible": true,
                "left": "20dp",
                "right": "20dp",
                "skin": "sknLblBgE4E6ECSeperator",
                "text": "Label",
                "top": "60dp",
                "zIndex": 2
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_TOP_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var flxDatasetSearchPopupEventsMOB = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "45px",
                "id": "flxDatasetSearchPopupEventsMOB",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "isModalContainer": false,
                "right": "20dp",
                "skin": "sknflxd5d9ddop100",
                "top": "0dp",
                "width": "500px",
                "zIndex": 1
            }, {}, {});
            flxDatasetSearchPopupEventsMOB.setDefaultUnit(kony.flex.DP);
            var fontIconSearchImgPopupEventsMOB = new kony.ui.Label({
                "centerY": "50%",
                "id": "fontIconSearchImgPopupEventsMOB",
                "isVisible": true,
                "left": "12px",
                "skin": "sknFontIconSearchImg20px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.userwidgetmodel.lblSearchIcon\")",
                "top": "0dp",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var tbxDatasetSearchBoxPopupEventsMOB = new kony.ui.TextBox2({
                "autoCapitalize": constants.TEXTBOX_AUTO_CAPITALIZE_NONE,
                "centerY": "50%",
                "height": "30px",
                "id": "tbxDatasetSearchBoxPopupEventsMOB",
                "isVisible": true,
                "keyBoardStyle": constants.TEXTBOX_KEY_BOARD_STYLE_DEFAULT,
                "left": "35dp",
                "onTouchStart": controller.AS_TextField_cfab06859f0f43eca99495891ee290d5,
                "placeholder": "Search Web Screen Names",
                "right": "35px",
                "secureTextEntry": false,
                "skin": "skntbxffffffNoBorderlato35475f14px",
                "textInputMode": constants.TEXTBOX_INPUT_MODE_ANY,
                "zIndex": 1
            }, {
                "containerHeightMode": constants.TEXTBOX_FONT_METRICS_DRIVEN_HEIGHT,
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [2, 0, 0, 0],
                "paddingInPixel": false
            }, {
                "autoCorrect": false
            });
            var flxPopupEventsClearSearchMOB = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "100%",
                "id": "flxPopupEventsClearSearchMOB",
                "isVisible": false,
                "layoutType": kony.flex.FREE_FORM,
                "isModalContainer": false,
                "right": "0px",
                "skin": "sknCursor",
                "top": "0dp",
                "width": "35px",
                "zIndex": 1
            }, {}, {
                "hoverSkin": "sknCursor"
            });
            flxPopupEventsClearSearchMOB.setDefaultUnit(kony.flex.DP);
            var fontIconCrossPopupEventsMOB = new kony.ui.Label({
                "centerY": "50%",
                "id": "fontIconCrossPopupEventsMOB",
                "isVisible": true,
                "left": "5px",
                "skin": "sknFontIconSearchCross16px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.userwidgetmodel.lblClearSearch\")",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxPopupEventsClearSearchMOB.add(fontIconCrossPopupEventsMOB);
            flxDatasetSearchPopupEventsMOB.add(fontIconSearchImgPopupEventsMOB, tbxDatasetSearchBoxPopupEventsMOB, flxPopupEventsClearSearchMOB);
            var flxResetPopupEventsSelectionMOB = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                "clipBounds": true,
                "id": "flxResetPopupEventsSelectionMOB",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "200dp",
                "isModalContainer": false,
                "skin": "hoverhandSkin",
                "top": "12dp",
                "width": "35dp"
            }, {}, {});
            flxResetPopupEventsSelectionMOB.setDefaultUnit(kony.flex.DP);
            var lblResetPopupEventsSelectionMOB = new kony.ui.Label({
                "id": "lblResetPopupEventsSelectionMOB",
                "isVisible": true,
                "left": "0dp",
                "skin": "sknLblLatoReg117eb013px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.Common.button.Reset\")",
                "top": "0dp",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxResetPopupEventsSelectionMOB.add(lblResetPopupEventsSelectionMOB);
            var flxDatasetCountPopupEventsMOB = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "20dp",
                "id": "flxDatasetCountPopupEventsMOB",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "145dp",
                "isModalContainer": false,
                "skin": "sknFlx006CCA",
                "top": "10dp",
                "width": "35dp",
                "zIndex": 1
            }, {}, {});
            flxDatasetCountPopupEventsMOB.setDefaultUnit(kony.flex.DP);
            var lblDatasetCountPopupEventsMOB = new kony.ui.Label({
                "centerX": "50%",
                "centerY": "40%",
                "id": "lblDatasetCountPopupEventsMOB",
                "isVisible": true,
                "skin": "sknLblffffff13px",
                "text": "00",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxDatasetCountPopupEventsMOB.add(lblDatasetCountPopupEventsMOB);
            var lblSelectDatasetPopupEventsMOB = new kony.ui.Label({
                "id": "lblSelectDatasetPopupEventsMOB",
                "isVisible": true,
                "left": "20dp",
                "skin": "sknLbl192B45LatoRegular14px",
                "text": "Selected Screen(s)",
                "top": "10dp",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxPopupScreenMOB.add(flxNoResultFoundPopupEventsMOB, segSelectPopupEventsMOB, flxSelectPopupEventsHeadersMOB, lblSeparatorPopupEventsTopHeaderMOB, flxDatasetSearchPopupEventsMOB, flxResetPopupEventsSelectionMOB, flxDatasetCountPopupEventsMOB, lblSelectDatasetPopupEventsMOB);
            flxAddPopupEventsPopup.add(flxPopupEventsTopColor, flxPopupEventsPopUpClose, lblPopupEventsTopHeader, lblTabSeperator, flxPopEventsTabsContainer, flxAddPopupEventsPopUpButtons, flxPopupScreenWEB, flxPopupScreenMOB);
            var flxPopUpScreenPreview = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "bottom": "5.25%",
                "clipBounds": true,
                "id": "flxPopUpScreenPreview",
                "isVisible": false,
                "layoutType": kony.flex.FREE_FORM,
                "left": "7.32%",
                "isModalContainer": false,
                "right": "7.32%",
                "skin": "sknBackgroundFFFFFF",
                "top": "5.25%"
            }, {}, {});
            flxPopUpScreenPreview.setDefaultUnit(kony.flex.DP);
            var flxPopupSreenTopBar = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "10px",
                "id": "flxPopupSreenTopBar",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0px",
                "isModalContainer": false,
                "skin": "sknFlxInfoToastBg357C9ENoRadius",
                "top": "0px",
                "width": "100%",
                "zIndex": 1
            }, {}, {});
            flxPopupSreenTopBar.setDefaultUnit(kony.flex.DP);
            flxPopupSreenTopBar.add();
            var flxPopupScreenTopDetails = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "133px",
                "id": "flxPopupScreenTopDetails",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0px",
                "isModalContainer": false,
                "right": "0px",
                "skin": "slFbox",
                "top": "10px",
                "width": "100%",
                "zIndex": 1
            }, {}, {});
            flxPopupScreenTopDetails.setDefaultUnit(kony.flex.DP);
            var lblPreviewModePopupScreen = new kony.ui.Label({
                "id": "lblPreviewModePopupScreen",
                "isVisible": true,
                "left": "20px",
                "skin": "sknLbl485c75LatoBold13px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmAdManagement.previewMode\")",
                "top": "40px",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var flxClosePopupScreen = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "20px",
                "id": "flxClosePopupScreen",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "isModalContainer": false,
                "right": "20px",
                "skin": "sknFlxffffffCursorPointer",
                "top": "10px",
                "width": "20px",
                "zIndex": 1
            }, {}, {});
            flxClosePopupScreen.setDefaultUnit(kony.flex.DP);
            var lblClosePopupScreen = new kony.ui.Label({
                "height": "100%",
                "id": "lblClosePopupScreen",
                "isVisible": true,
                "left": "0px",
                "skin": "lblIconGrey",
                "text": "",
                "top": "0dp",
                "width": "100%",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_CENTER,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxClosePopupScreen.add(lblClosePopupScreen);
            var lblChannelsHeader = new kony.ui.Label({
                "id": "lblChannelsHeader",
                "isVisible": true,
                "left": "370px",
                "skin": "sknlblLato11px696C73",
                "text": "CHANNELS",
                "top": "45px",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var flxPopupScreenInfo = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "41px",
                "id": "flxPopupScreenInfo",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "20px",
                "isModalContainer": false,
                "right": "20px",
                "skin": "slFbox",
                "top": "75px",
                "zIndex": 1
            }, {}, {});
            flxPopupScreenInfo.setDefaultUnit(kony.flex.DP);
            var flxDisplayPageName = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "37px",
                "id": "flxDisplayPageName",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0px",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0px",
                "width": "300px"
            }, {}, {});
            flxDisplayPageName.setDefaultUnit(kony.flex.DP);
            var fontIconDisplayPageName = new kony.ui.Label({
                "bottom": "2px",
                "height": "32px",
                "id": "fontIconDisplayPageName",
                "isVisible": true,
                "left": "0px",
                "skin": "lblIconMoonb5b5b532px",
                "text": "",
                "top": "2px",
                "width": "32px",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var lblDisplayPageNameHeader = new kony.ui.Label({
                "id": "lblDisplayPageNameHeader",
                "isVisible": true,
                "left": "49px",
                "skin": "sknlblLato11px696C73",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmAdManagement.DisplayPageNameCAPS\")",
                "top": "0px",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var lblDisplayPageNam = new kony.ui.Label({
                "bottom": "0px",
                "id": "lblDisplayPageNam",
                "isVisible": true,
                "left": "49px",
                "skin": "sknLbl16pxLato192B45",
                "text": "Pop-up screen",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxDisplayPageName.add(fontIconDisplayPageName, lblDisplayPageNameHeader, lblDisplayPageNam);
            var flxVerticalInfoSeparatorPopupScreen = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "centerY": "50%",
                "clipBounds": true,
                "height": "33px",
                "id": "flxVerticalInfoSeparatorPopupScreen",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "301px",
                "isModalContainer": false,
                "skin": "sknflxD5D9DD",
                "width": "1px",
                "zIndex": 1
            }, {}, {});
            flxVerticalInfoSeparatorPopupScreen.setDefaultUnit(kony.flex.DP);
            flxVerticalInfoSeparatorPopupScreen.add();
            var flxChannelsIcons = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "100%",
                "id": "flxChannelsIcons",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "302px",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0px",
                "width": "100px"
            }, {}, {});
            flxChannelsIcons.setDefaultUnit(kony.flex.DP);
            var flxGeneralPreview = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "40dp",
                "id": "flxGeneralPreview",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "50dp",
                "isModalContainer": false,
                "skin": "sknflxf3f6f8Radius3PX",
                "top": "0dp",
                "width": "40dp"
            }, {}, {});
            flxGeneralPreview.setDefaultUnit(kony.flex.DP);
            var fontIconGeneralPreview = new kony.ui.Label({
                "centerX": "51%",
                "centerY": "50%",
                "id": "fontIconGeneralPreview",
                "isVisible": true,
                "skin": "lblIconMoonb5b5b532px",
                "text": "",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxGeneralPreview.add(fontIconGeneralPreview);
            flxChannelsIcons.add(flxGeneralPreview);
            flxPopupScreenInfo.add(flxDisplayPageName, flxVerticalInfoSeparatorPopupScreen, flxChannelsIcons);
            var flxPopupScreenTopSeparator = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "1px",
                "id": "flxPopupScreenTopSeparator",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "20px",
                "isModalContainer": false,
                "right": "20px",
                "skin": "sknFlxD7D9E0Separator",
                "top": "130px",
                "zIndex": 1
            }, {}, {});
            flxPopupScreenTopSeparator.setDefaultUnit(kony.flex.DP);
            flxPopupScreenTopSeparator.add();
            flxPopupScreenTopDetails.add(lblPreviewModePopupScreen, flxClosePopupScreen, lblChannelsHeader, flxPopupScreenInfo, flxPopupScreenTopSeparator);
            var flxPopupScreenMain = new kony.ui.FlexScrollContainer({
                "allowHorizontalBounce": false,
                "allowVerticalBounce": true,
                "bottom": "20dp",
                "bounces": true,
                "clipBounds": true,
                "enableScrolling": true,
                "horizontalScrollIndicator": true,
                "id": "flxPopupScreenMain",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "pagingEnabled": false,
                "scrollDirection": kony.flex.SCROLL_VERTICAL,
                "skin": "slFSbox",
                "top": "150dp",
                "verticalScrollIndicator": true,
                "width": "100%"
            }, {}, {});
            flxPopupScreenMain.setDefaultUnit(kony.flex.DP);
            var flxPopScreenMobile = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                "centerX": "50%",
                "clipBounds": true,
                "id": "flxPopScreenMobile",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "sknFlxBorAndRadius4px",
                "top": "20dp",
                "width": "300dp"
            }, {}, {});
            flxPopScreenMobile.setDefaultUnit(kony.flex.DP);
            var campaignPopupMobile = new com.adminConsole.adManagement.campaignPopupMobile({
                "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                "id": "campaignPopupMobile",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "masterType": constants.MASTER_TYPE_DEFAULT,
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0dp",
                "width": "300px",
                "overrides": {
                    "campaignPopupMobile": {
                        "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                        "centerX": "viz.val_cleared",
                        "top": "0dp",
                        "width": "300px"
                    },
                    "imgPopup": {
                        "src": "campaign_bonus.png"
                    }
                }
            }, {
                "overrides": {}
            }, {
                "overrides": {}
            });
            flxPopScreenMobile.add(campaignPopupMobile);
            var flxPopScreenWeb = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                "centerX": "50%",
                "clipBounds": true,
                "id": "flxPopScreenWeb",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "sknFlxBorAndRadius4px",
                "top": "20dp",
                "width": "560dp"
            }, {}, {});
            flxPopScreenWeb.setDefaultUnit(kony.flex.DP);
            var campaignPopupWeb = new com.adminConsole.adManagement.campaignPopupWeb({
                "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                "id": "campaignPopupWeb",
                "isVisible": true,
                "layoutType": kony.flex.FLOW_VERTICAL,
                "left": "0dp",
                "masterType": constants.MASTER_TYPE_DEFAULT,
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "5dp",
                "width": "560dp",
                "zIndex": 1001,
                "overrides": {}
            }, {
                "overrides": {}
            }, {
                "overrides": {}
            });
            flxPopScreenWeb.add(campaignPopupWeb);
            flxPopupScreenMain.add(flxPopScreenMobile, flxPopScreenWeb);
            flxPopUpScreenPreview.add(flxPopupSreenTopBar, flxPopupScreenTopDetails, flxPopupScreenMain);
            flxConsentPopup.add(flxCampaignList, popUp, warningpopup, flxAddAttributesPopUp, flxAddExistingRolesPopUp, flxAddEventsPopUp, flxAddPopupEventsPopup, flxPopUpScreenPreview);
            var flxToastMessage = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "bottom": "0%",
                "clipBounds": true,
                "height": "8%",
                "id": "flxToastMessage",
                "isVisible": false,
                "layoutType": kony.flex.FREE_FORM,
                "left": "305dp",
                "isModalContainer": false,
                "right": "0dp",
                "skin": "slFbox",
                "zIndex": 20
            }, {}, {});
            flxToastMessage.setDefaultUnit(kony.flex.DP);
            var toastMessage = new com.adminConsole.common.toastMessage({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "height": "70px",
                "id": "toastMessage",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "masterType": constants.MASTER_TYPE_DEFAULT,
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0dp",
                "width": "100%",
                "overrides": {}
            }, {
                "overrides": {}
            }, {
                "overrides": {}
            });
            flxToastMessage.add(toastMessage);
            var flxViewAdPreview = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "centerX": "50%",
                "centerY": "50%",
                "clipBounds": true,
                "height": "100%",
                "id": "flxViewAdPreview",
                "isVisible": false,
                "layoutType": kony.flex.FREE_FORM,
                "isModalContainer": false,
                "skin": "sknFlxBg000000op40",
                "width": "100%",
                "zIndex": 5
            }, {}, {});
            flxViewAdPreview.setDefaultUnit(kony.flex.DP);
            var flxDisplayImage = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "bottom": "5.25%",
                "clipBounds": true,
                "id": "flxDisplayImage",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "7.32%",
                "isModalContainer": false,
                "right": "7.32%",
                "skin": "sknBackgroundFFFFFF",
                "top": "5.25%"
            }, {}, {});
            flxDisplayImage.setDefaultUnit(kony.flex.DP);
            var flxTopBar = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "10px",
                "id": "flxTopBar",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0px",
                "isModalContainer": false,
                "skin": "sknFlxInfoToastBg357C9ENoRadius",
                "top": "0px",
                "width": "100%",
                "zIndex": 1
            }, {}, {});
            flxTopBar.setDefaultUnit(kony.flex.DP);
            flxTopBar.add();
            var flxChannelInfo = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "186px",
                "id": "flxChannelInfo",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0px",
                "isModalContainer": false,
                "right": "0px",
                "skin": "slFbox",
                "top": "10px",
                "width": "100%",
                "zIndex": 1
            }, {}, {});
            flxChannelInfo.setDefaultUnit(kony.flex.DP);
            var flxPreviewMode = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "bottom": "73px",
                "clipBounds": true,
                "height": "50px",
                "id": "flxPreviewMode",
                "isVisible": true,
                "layoutType": kony.flex.FLOW_HORIZONTAL,
                "left": "18px",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0px",
                "width": "101px",
                "zIndex": 1
            }, {}, {});
            flxPreviewMode.setDefaultUnit(kony.flex.DP);
            var lblPreviewMode = new kony.ui.Label({
                "id": "lblPreviewMode",
                "isVisible": true,
                "left": "0px",
                "skin": "sknLbl485c75LatoBold13px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmAdManagement.previewMode\")",
                "top": "24px",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxPreviewMode.add(lblPreviewMode);
            var flxScreenInfo = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "37px",
                "id": "flxScreenInfo",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "20px",
                "isModalContainer": false,
                "right": "20px",
                "skin": "slFbox",
                "top": "68px",
                "zIndex": 1
            }, {}, {});
            flxScreenInfo.setDefaultUnit(kony.flex.DP);
            var flxScreenName = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "100%",
                "id": "flxScreenName",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0px",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0px",
                "width": "350px"
            }, {}, {});
            flxScreenName.setDefaultUnit(kony.flex.DP);
            var lblMoonIconScreen = new kony.ui.Label({
                "bottom": "2px",
                "height": "32px",
                "id": "lblMoonIconScreen",
                "isVisible": true,
                "left": "0px",
                "skin": "lblIconMoonb5b5b532px",
                "text": "",
                "top": "2px",
                "width": "32px",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var lblScreenNameHeader = new kony.ui.Label({
                "id": "lblScreenNameHeader",
                "isVisible": true,
                "left": "49px",
                "skin": "sknlblLato11px696C73",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmAdManagement.DisplayPageNameCAPS\")",
                "top": "0px",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var lblScreenName = new kony.ui.Label({
                "bottom": "0px",
                "id": "lblScreenName",
                "isVisible": true,
                "left": "49px",
                "skin": "sknLbl16pxLato192B45",
                "text": "Post Login Full Screen Interstitial Ad",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxScreenName.add(lblMoonIconScreen, lblScreenNameHeader, lblScreenName);
            var flxChannelName = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "100%",
                "id": "flxChannelName",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "350px",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0px",
                "width": "300px"
            }, {}, {});
            flxChannelName.setDefaultUnit(kony.flex.DP);
            var flxVerticalSeparator = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "centerY": "50%",
                "clipBounds": true,
                "height": "33px",
                "id": "flxVerticalSeparator",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0px",
                "isModalContainer": false,
                "skin": "sknflxD5D9DD",
                "width": "1px",
                "zIndex": 1
            }, {}, {});
            flxVerticalSeparator.setDefaultUnit(kony.flex.DP);
            flxVerticalSeparator.add();
            var lblMoonIconChannel = new kony.ui.Label({
                "bottom": "2px",
                "height": "32px",
                "id": "lblMoonIconChannel",
                "isVisible": true,
                "left": "61px",
                "skin": "lblIconMoonb5b5b532px",
                "text": "",
                "top": "2px",
                "width": "32px",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var lblChannelNameHeader = new kony.ui.Label({
                "id": "lblChannelNameHeader",
                "isVisible": true,
                "left": "110px",
                "skin": "sknlblLato11px696C73",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmAdManagement.ChannelCAPS\")",
                "top": "0px",
                "width": "53px",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var lblChannelName = new kony.ui.Label({
                "bottom": "0px",
                "id": "lblChannelName",
                "isVisible": true,
                "left": "110px",
                "skin": "sknLbl16pxLato192B45",
                "text": "Native Mobile App",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxChannelName.add(flxVerticalSeparator, lblMoonIconChannel, lblChannelNameHeader, lblChannelName);
            flxScreenInfo.add(flxScreenName, flxChannelName);
            var flxClose = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "20px",
                "id": "flxClose",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "isModalContainer": false,
                "right": "20px",
                "skin": "sknFlxffffffCursorPointer",
                "top": "10px",
                "width": "20px",
                "zIndex": 1
            }, {}, {});
            flxClose.setDefaultUnit(kony.flex.DP);
            var lblClose = new kony.ui.Label({
                "height": "100%",
                "id": "lblClose",
                "isVisible": true,
                "left": "0px",
                "skin": "lblIconGrey",
                "text": "",
                "top": "0dp",
                "width": "100%",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_CENTER,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxClose.add(lblClose);
            var flxAdSeparator = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "bottom": "0px",
                "clipBounds": true,
                "height": "1px",
                "id": "flxAdSeparator",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "20px",
                "isModalContainer": false,
                "right": "20px",
                "skin": "sknFlxD7D9E0Separator",
                "top": "116px",
                "zIndex": 1
            }, {}, {});
            flxAdSeparator.setDefaultUnit(kony.flex.DP);
            flxAdSeparator.add();
            var flxMessage = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "40px",
                "id": "flxMessage",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "20px",
                "isModalContainer": false,
                "right": "20px",
                "skin": "sknFlx1FB44DBorder2px",
                "top": "137px",
                "zIndex": 1
            }, {}, {});
            flxMessage.setDefaultUnit(kony.flex.DP);
            var flxMessageColor = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "40px",
                "id": "flxMessageColor",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "-2px",
                "isModalContainer": false,
                "skin": "sknFlx1FB44DBackground",
                "top": "-2px",
                "width": "40px",
                "zIndex": 1
            }, {}, {});
            flxMessageColor.setDefaultUnit(kony.flex.DP);
            var lblStatus = new kony.ui.Label({
                "centerX": "50%",
                "centerY": "50%",
                "height": "19px",
                "id": "lblStatus",
                "isVisible": true,
                "skin": "sknFontIcontoastSuccess",
                "text": "",
                "width": "19px",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxMessageColor.add(lblStatus);
            var lblMessage = new kony.ui.Label({
                "height": "16px",
                "id": "lblMessage",
                "isVisible": true,
                "left": "53px",
                "skin": "sknLblLato485c7513px",
                "text": "Uploaded image matches the container",
                "top": "9px",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxMessage.add(flxMessageColor, lblMessage);
            flxChannelInfo.add(flxPreviewMode, flxScreenInfo, flxClose, flxAdSeparator, flxMessage);
            var flxShowImageParent = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "bottom": "0px",
                "clipBounds": true,
                "id": "flxShowImageParent",
                "isVisible": true,
                "layoutType": kony.flex.FLOW_VERTICAL,
                "left": "0px",
                "isModalContainer": false,
                "right": "0px",
                "skin": "slFbox",
                "top": "200px",
                "width": "100%"
            }, {}, {});
            flxShowImageParent.setDefaultUnit(kony.flex.DP);
            var flxScrollImageDisplay = new kony.ui.FlexScrollContainer({
                "allowHorizontalBounce": false,
                "allowVerticalBounce": true,
                "bottom": "0px",
                "bounces": true,
                "clipBounds": true,
                "enableScrolling": true,
                "height": "100%",
                "horizontalScrollIndicator": true,
                "id": "flxScrollImageDisplay",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0px",
                "pagingEnabled": false,
                "right": "10px",
                "scrollDirection": kony.flex.SCROLL_BOTH,
                "skin": "slFSbox",
                "top": "0px",
                "verticalScrollIndicator": true,
                "width": "100%",
                "zIndex": 1
            }, {}, {});
            flxScrollImageDisplay.setDefaultUnit(kony.flex.DP);
            var flxViewImageFlowVertical = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                "clipBounds": true,
                "id": "flxViewImageFlowVertical",
                "isVisible": true,
                "layoutType": kony.flex.FLOW_VERTICAL,
                "left": "0px",
                "isModalContainer": false,
                "right": "15px",
                "skin": "slFbox",
                "top": "0px"
            }, {}, {});
            flxViewImageFlowVertical.setDefaultUnit(kony.flex.DP);
            var flxUploadedImageParent = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                "clipBounds": true,
                "id": "flxUploadedImageParent",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0px",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0px",
                "width": "100%"
            }, {}, {});
            flxUploadedImageParent.setDefaultUnit(kony.flex.DP);
            var flxBorder = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "bottom": "20px",
                "clipBounds": true,
                "id": "flxBorder",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "20px",
                "isModalContainer": false,
                "right": "20px",
                "skin": "sknFlxBorder6px000000",
                "top": "40px",
                "zIndex": 2
            }, {}, {});
            flxBorder.setDefaultUnit(kony.flex.DP);
            flxBorder.add();
            var flxImage = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "bottom": "20px",
                "clipBounds": true,
                "id": "flxImage",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "20px",
                "isModalContainer": false,
                "right": "20px",
                "skin": "slFbox",
                "top": "10px",
                "zIndex": 1
            }, {}, {});
            flxImage.setDefaultUnit(kony.flex.DP);
            var flxContainerResolution = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                "clipBounds": true,
                "id": "flxContainerResolution",
                "isVisible": true,
                "layoutType": kony.flex.FLOW_VERTICAL,
                "left": "0px",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0px",
                "width": kony.flex.USE_PREFFERED_SIZE
            }, {}, {});
            flxContainerResolution.setDefaultUnit(kony.flex.DP);
            var flxResolution = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "30px",
                "id": "flxResolution",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0px",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0px",
                "width": "100%",
                "zIndex": 2
            }, {}, {});
            flxResolution.setDefaultUnit(kony.flex.DP);
            var flxResolutionDisplay = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "100%",
                "id": "flxResolutionDisplay",
                "isVisible": true,
                "layoutType": kony.flex.FLOW_HORIZONTAL,
                "left": "0px",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0px",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {}, {});
            flxResolutionDisplay.setDefaultUnit(kony.flex.DP);
            var lblTargetResolution = new kony.ui.Label({
                "centerY": "50%",
                "id": "lblTargetResolution",
                "isVisible": true,
                "left": "0px",
                "skin": "sknLbl696c73LatoBold11px",
                "text": "Target Container Resolution : 1125*318 | ",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var lblUploadedResolution = new kony.ui.Label({
                "centerY": "50%",
                "id": "lblUploadedResolution",
                "isVisible": true,
                "left": "2px",
                "skin": "sknlblLato11px696C73",
                "text": "Uploaded image resolution : 1024*318",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxResolutionDisplay.add(lblTargetResolution, lblUploadedResolution);
            flxResolution.add(flxResolutionDisplay);
            var richTextAdImage = new kony.ui.RichText({
                "id": "richTextAdImage",
                "isVisible": true,
                "left": "0px",
                "linkSkin": "defRichTextLink",
                "skin": "slRichText",
                "top": "0px",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxContainerResolution.add(flxResolution, richTextAdImage);
            flxImage.add(flxContainerResolution);
            flxUploadedImageParent.add(flxBorder, flxImage);
            var flxAdjustedImageParent = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                "clipBounds": true,
                "id": "flxAdjustedImageParent",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0px",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0px",
                "width": "100%",
                "zIndex": 1
            }, {}, {});
            flxAdjustedImageParent.setDefaultUnit(kony.flex.DP);
            var flxBorder2 = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "bottom": "20px",
                "clipBounds": true,
                "id": "flxBorder2",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "20px",
                "isModalContainer": false,
                "right": "20px",
                "skin": "sknFlxBorder6px000000",
                "top": "40px",
                "zIndex": 2
            }, {}, {});
            flxBorder2.setDefaultUnit(kony.flex.DP);
            flxBorder2.add();
            var flxImage2 = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "bottom": "20px",
                "clipBounds": true,
                "id": "flxImage2",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "20px",
                "isModalContainer": false,
                "right": "20px",
                "skin": "slFbox",
                "top": "10px",
                "zIndex": 1
            }, {}, {});
            flxImage2.setDefaultUnit(kony.flex.DP);
            var flxContainerResolution2 = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                "clipBounds": true,
                "id": "flxContainerResolution2",
                "isVisible": true,
                "layoutType": kony.flex.FLOW_VERTICAL,
                "left": "0px",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0px",
                "width": kony.flex.USE_PREFFERED_SIZE
            }, {}, {});
            flxContainerResolution2.setDefaultUnit(kony.flex.DP);
            var flxResolution2 = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "30px",
                "id": "flxResolution2",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0px",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0px",
                "width": "100%",
                "zIndex": 2
            }, {}, {});
            flxResolution2.setDefaultUnit(kony.flex.DP);
            var flxResolutionDisplay2 = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "100%",
                "id": "flxResolutionDisplay2",
                "isVisible": true,
                "layoutType": kony.flex.FLOW_HORIZONTAL,
                "left": "0px",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0px",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {}, {});
            flxResolutionDisplay2.setDefaultUnit(kony.flex.DP);
            var lblAdjustedToContainer = new kony.ui.Label({
                "centerY": "50%",
                "id": "lblAdjustedToContainer",
                "isVisible": true,
                "left": "0px",
                "skin": "sknlblLato11px696C73",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmAdManagement.ImageAdjustedToFitTheContainer\")",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxResolutionDisplay2.add(lblAdjustedToContainer);
            flxResolution2.add(flxResolutionDisplay2);
            var richTextAdImage2 = new kony.ui.RichText({
                "id": "richTextAdImage2",
                "isVisible": true,
                "left": "0px",
                "linkSkin": "defRichTextLink",
                "skin": "slRichText",
                "top": "0px",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxContainerResolution2.add(flxResolution2, richTextAdImage2);
            flxImage2.add(flxContainerResolution2);
            flxAdjustedImageParent.add(flxBorder2, flxImage2);
            flxViewImageFlowVertical.add(flxUploadedImageParent, flxAdjustedImageParent);
            flxScrollImageDisplay.add(flxViewImageFlowVertical);
            flxShowImageParent.add(flxScrollImageDisplay);
            flxDisplayImage.add(flxTopBar, flxChannelInfo, flxShowImageParent);
            flxViewAdPreview.add(flxDisplayImage);
            var flxViewAttributeList = new kony.ui.FlexContainer({
                "centerX": "50%",
                "centerY": "50%",
                "clipBounds": true,
                "height": "100%",
                "id": "flxViewAttributeList",
                "isVisible": false,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "skn222b35",
                "top": "0dp",
                "width": "100%",
                "zIndex": 20
            }, {}, {});
            flxViewAttributeList.setDefaultUnit(kony.flex.DP);
            var flxViewAttributeListWindow = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "centerX": "50%",
                "centerY": "50%",
                "clipBounds": true,
                "height": "80%",
                "id": "flxViewAttributeListWindow",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "isModalContainer": false,
                "skin": "sknBackgroundFFFFFF",
                "width": "60%",
                "zIndex": 200
            }, {}, {});
            flxViewAttributeListWindow.setDefaultUnit(kony.flex.DP);
            var flxAttributeListTopDetails = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "78px",
                "id": "flxAttributeListTopDetails",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0px",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0px",
                "width": "100%"
            }, {}, {});
            flxAttributeListTopDetails.setDefaultUnit(kony.flex.DP);
            var flxTopScreenBar = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "10px",
                "id": "flxTopScreenBar",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0%",
                "isModalContainer": false,
                "skin": "sknFlxInfoToastBg357C9ENoRadius",
                "top": "0px",
                "width": "100%",
                "zIndex": 1
            }, {}, {});
            flxTopScreenBar.setDefaultUnit(kony.flex.DP);
            flxTopScreenBar.add();
            var flxCustomerRoleName = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "19px",
                "id": "flxCustomerRoleName",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "22px",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "37px",
                "width": "90%",
                "zIndex": 1
            }, {}, {});
            flxCustomerRoleName.setDefaultUnit(kony.flex.DP);
            var lblCustomerRoleName = new kony.ui.Label({
                "id": "lblCustomerRoleName",
                "isVisible": true,
                "left": "0px",
                "skin": "sknLbl16pxLato192B45",
                "text": "Platinum DBX Users Attribute(s) List",
                "top": "0px",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxCustomerRoleName.add(lblCustomerRoleName);
            var flxCloseAttributeList = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "20px",
                "id": "flxCloseAttributeList",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "isModalContainer": false,
                "right": "16px",
                "skin": "sknFlxffffffCursorPointer",
                "top": "30px",
                "width": "20px",
                "zIndex": 1
            }, {}, {});
            flxCloseAttributeList.setDefaultUnit(kony.flex.DP);
            var lblAttributeClose = new kony.ui.Label({
                "height": "100%",
                "id": "lblAttributeClose",
                "isVisible": true,
                "left": "0px",
                "skin": "lblIconGrey",
                "text": "",
                "top": "0dp",
                "width": "100%",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_CENTER,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxCloseAttributeList.add(lblAttributeClose);
            flxAttributeListTopDetails.add(flxTopScreenBar, flxCustomerRoleName, flxCloseAttributeList);
            var flxAttributeListBottomDetails = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "bottom": "0px",
                "clipBounds": true,
                "id": "flxAttributeListBottomDetails",
                "isVisible": true,
                "layoutType": kony.flex.FLOW_VERTICAL,
                "left": "0px",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "98px",
                "width": "100%",
                "zIndex": 1
            }, {}, {});
            flxAttributeListBottomDetails.setDefaultUnit(kony.flex.DP);
            var flxAttributeDescriptionUsers = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                "clipBounds": true,
                "id": "flxAttributeDescriptionUsers",
                "isVisible": true,
                "layoutType": kony.flex.FLOW_HORIZONTAL,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0dp",
                "width": "100%"
            }, {}, {});
            flxAttributeDescriptionUsers.setDefaultUnit(kony.flex.DP);
            var flxAttributeListDescription = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                "clipBounds": true,
                "id": "flxAttributeListDescription",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0px",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0px",
                "width": "60%",
                "zIndex": 1
            }, {}, {});
            flxAttributeListDescription.setDefaultUnit(kony.flex.DP);
            var lblAttributeDescriptionTag = new kony.ui.Label({
                "height": "13px",
                "id": "lblAttributeDescriptionTag",
                "isVisible": true,
                "left": "20px",
                "skin": "sknlblLato11px696C73",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmAdManagement.DescriptionCAPS\")",
                "top": "0px",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var lblAttributeDescription = new kony.ui.Label({
                "id": "lblAttributeDescription",
                "isVisible": true,
                "left": "20px",
                "right": 20,
                "skin": "sknlbl485C75LatoSemiBold13px",
                "text": "These Micro Business users have the highest level of access to all the functions.",
                "top": "28px",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxAttributeListDescription.add(lblAttributeDescriptionTag, lblAttributeDescription);
            var flxAttributeListUserAvailable = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                "clipBounds": true,
                "id": "flxAttributeListUserAvailable",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0px",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0px",
                "width": "40%",
                "zIndex": 1
            }, {}, {});
            flxAttributeListUserAvailable.setDefaultUnit(kony.flex.DP);
            var lblAttributeUserAvailableTag = new kony.ui.Label({
                "height": "13px",
                "id": "lblAttributeUserAvailableTag",
                "isVisible": true,
                "left": "20px",
                "skin": "sknlblLato11px696C73",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmAdManagement.UsersAvaialbleCAPS\")",
                "top": "0px",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var lblAttributeUserAvailable = new kony.ui.Label({
                "id": "lblAttributeUserAvailable",
                "isVisible": true,
                "left": "20px",
                "right": 20,
                "skin": "sknlbl485C75LatoSemiBold13px",
                "text": "58752",
                "top": "28px",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxAttributeListUserAvailable.add(lblAttributeUserAvailableTag, lblAttributeUserAvailable);
            flxAttributeDescriptionUsers.add(flxAttributeListDescription, flxAttributeListUserAvailable);
            var flxAttributeListSeparator = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "1px",
                "id": "flxAttributeListSeparator",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "19px",
                "isModalContainer": false,
                "right": "31px",
                "skin": "flxe4e6ec1px",
                "top": "30px",
                "width": "95%",
                "zIndex": 1
            }, {}, {});
            flxAttributeListSeparator.setDefaultUnit(kony.flex.DP);
            flxAttributeListSeparator.add();
            var flxAttributesList1 = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "bottom": "0px",
                "clipBounds": true,
                "height": "80%",
                "id": "flxAttributesList1",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "22px",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0px",
                "width": "97.40%",
                "zIndex": 1
            }, {}, {});
            flxAttributesList1.setDefaultUnit(kony.flex.DP);
            var flxAttrHeaderRow1 = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "50dp",
                "id": "flxAttrHeaderRow1",
                "isVisible": true,
                "layoutType": kony.flex.FLOW_HORIZONTAL,
                "left": 0,
                "isModalContainer": false,
                "right": "20dp",
                "skin": "slFbox",
                "top": "0",
                "width": kony.flex.USE_PREFFERED_SIZE
            }, {}, {});
            flxAttrHeaderRow1.setDefaultUnit(kony.flex.DP);
            var flxDatasetsHeader1 = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "100%",
                "id": "flxDatasetsHeader1",
                "isVisible": true,
                "layoutType": kony.flex.FLOW_HORIZONTAL,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0dp",
                "width": "24%"
            }, {}, {});
            flxDatasetsHeader1.setDefaultUnit(kony.flex.DP);
            var lblDatasetHeader1 = new kony.ui.Label({
                "centerY": "50%",
                "id": "lblDatasetHeader1",
                "isVisible": true,
                "left": "0",
                "skin": "sknlblLato11px696C73",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmAdManagement.DatasetCAPS\")",
                "top": "21dp",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var lblFilterDataset1 = new kony.ui.Label({
                "centerY": "50%",
                "id": "lblFilterDataset1",
                "isVisible": true,
                "left": "7dp",
                "skin": "sknIcon15px",
                "text": "",
                "top": "21dp",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {
                "hoverSkin": "sknlblCursorFont"
            });
            flxDatasetsHeader1.add(lblDatasetHeader1, lblFilterDataset1);
            var flxAttrHeader1 = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "100%",
                "id": "flxAttrHeader1",
                "isVisible": true,
                "layoutType": kony.flex.FLOW_HORIZONTAL,
                "left": "1%",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0dp",
                "width": "24%"
            }, {}, {});
            flxAttrHeader1.setDefaultUnit(kony.flex.DP);
            var lblAttributeHeader1 = new kony.ui.Label({
                "centerY": "50%",
                "id": "lblAttributeHeader1",
                "isVisible": true,
                "left": "0dp",
                "skin": "sknlblLato11px696C73",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmAdManagement.AttributeCAPS\")",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var lblSortAttribute1 = new kony.ui.Label({
                "centerY": "50%",
                "id": "lblSortAttribute1",
                "isVisible": true,
                "left": "7dp",
                "skin": "sknIcon15px",
                "text": "",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {
                "hoverSkin": "sknlblCursorFont"
            });
            flxAttrHeader1.add(lblAttributeHeader1, lblSortAttribute1);
            var lblAttributeCriteriaHeader1 = new kony.ui.Label({
                "centerY": "50%",
                "id": "lblAttributeCriteriaHeader1",
                "isVisible": true,
                "left": "1%",
                "skin": "sknlblLato11px696C73",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmAdManagement.CriteriaCAPS\")",
                "width": "24%",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var lblAttributeValueHeader1 = new kony.ui.Label({
                "centerY": "50%",
                "id": "lblAttributeValueHeader1",
                "isVisible": true,
                "left": "1%",
                "skin": "sknlblLato11px696C73",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmAdManagement.ValueCAPS\")",
                "top": "21dp",
                "width": "24%",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxAttrHeaderRow1.add(flxDatasetsHeader1, flxAttrHeader1, lblAttributeCriteriaHeader1, lblAttributeValueHeader1);
            var lblViewAttrSep1 = new kony.ui.Label({
                "bottom": "0px",
                "height": "1px",
                "id": "lblViewAttrSep1",
                "isVisible": true,
                "right": "20dp",
                "skin": "sknLblTableHeaderLine",
                "text": "-",
                "top": "50dp",
                "width": "100%",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var flxSegAttributeList = new kony.ui.FlexContainer({
                "bottom": "0px",
                "clipBounds": true,
                "id": "flxSegAttributeList",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0px",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "51px",
                "width": "99%",
                "zIndex": 1
            }, {}, {});
            flxSegAttributeList.setDefaultUnit(kony.flex.DP);
            var segAttributeList = new kony.ui.SegmentedUI2({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "bottom": "0px",
                "data": [{
                    "lblAttribute": "Attribute",
                    "lblCriteria": "Criteria",
                    "lblDataset": "DATASET",
                    "lblValue": "Value"
                }, {
                    "lblAttribute": "Attribute",
                    "lblCriteria": "Criteria",
                    "lblDataset": "DATASET",
                    "lblValue": "Value"
                }, {
                    "lblAttribute": "Attribute",
                    "lblCriteria": "Criteria",
                    "lblDataset": "DATASET",
                    "lblValue": "Value"
                }, {
                    "lblAttribute": "Attribute",
                    "lblCriteria": "Criteria",
                    "lblDataset": "DATASET",
                    "lblValue": "Value"
                }, {
                    "lblAttribute": "Attribute",
                    "lblCriteria": "Criteria",
                    "lblDataset": "DATASET",
                    "lblValue": "Value"
                }, {
                    "lblAttribute": "Attribute",
                    "lblCriteria": "Criteria",
                    "lblDataset": "DATASET",
                    "lblValue": "Value"
                }, {
                    "lblAttribute": "Attribute",
                    "lblCriteria": "Criteria",
                    "lblDataset": "DATASET",
                    "lblValue": "Value"
                }, {
                    "lblAttribute": "Attribute",
                    "lblCriteria": "Criteria",
                    "lblDataset": "DATASET",
                    "lblValue": "Value"
                }, {
                    "lblAttribute": "Attribute",
                    "lblCriteria": "Criteria",
                    "lblDataset": "DATASET",
                    "lblValue": "Value"
                }, {
                    "lblAttribute": "Attribute",
                    "lblCriteria": "Criteria",
                    "lblDataset": "DATASET",
                    "lblValue": "Value"
                }],
                "groupCells": false,
                "id": "segAttributeList",
                "isVisible": true,
                "left": "0px",
                "needPageIndicator": true,
                "pageOffDotImage": "pageoffdot.png",
                "pageOnDotImage": "pageondot.png",
                "retainSelection": false,
                "right": "6px",
                "rowFocusSkin": "seg2Focus",
                "rowSkin": "seg2Normal",
                "rowTemplate": "flxAttributeRow",
                "sectionHeaderSkin": "sliPhoneSegmentHeader",
                "selectionBehavior": constants.SEGUI_DEFAULT_BEHAVIOR,
                "separatorColor": "aaaaaa00",
                "separatorRequired": true,
                "separatorThickness": 1,
                "showScrollbars": false,
                "top": "0px",
                "viewType": constants.SEGUI_VIEW_TYPE_TABLEVIEW,
                "widgetDataMap": {
                    "flxAttributeRow": "flxAttributeRow",
                    "flxRow": "flxRow",
                    "lblAttribute": "lblAttribute",
                    "lblCriteria": "lblCriteria",
                    "lblDataset": "lblDataset",
                    "lblValue": "lblValue"
                },
                "zIndex": 1
            }, {
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxSegAttributeList.add(segAttributeList);
            var viewAttributesFilter = new com.adminConsole.common.statusFilterMenu({
                "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                "id": "viewAttributesFilter",
                "isVisible": false,
                "layoutType": kony.flex.FREE_FORM,
                "left": "43dp",
                "masterType": constants.MASTER_TYPE_DEFAULT,
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "35dp",
                "width": "22%",
                "zIndex": 5,
                "overrides": {
                    "flxArrowImage": {
                        "width": "100%"
                    },
                    "imgUpArrow": {
                        "left": "15dp",
                        "right": "viz.val_cleared",
                        "src": "uparrow_2x.png"
                    },
                    "statusFilterMenu": {
                        "isVisible": false,
                        "left": "43dp",
                        "maxWidth": "viz.val_cleared",
                        "minWidth": "viz.val_cleared",
                        "top": "35dp",
                        "width": "22%"
                    }
                }
            }, {
                "overrides": {}
            }, {
                "overrides": {}
            });
            flxAttributesList1.add(flxAttrHeaderRow1, lblViewAttrSep1, flxSegAttributeList, viewAttributesFilter);
            flxAttributeListBottomDetails.add(flxAttributeDescriptionUsers, flxAttributeListSeparator, flxAttributesList1);
            flxViewAttributeListWindow.add(flxAttributeListTopDetails, flxAttributeListBottomDetails);
            flxViewAttributeList.add(flxViewAttributeListWindow);
            var flxEmailPreview = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "centerX": "50%",
                "centerY": "50%",
                "clipBounds": true,
                "height": "100%",
                "id": "flxEmailPreview",
                "isVisible": false,
                "layoutType": kony.flex.FREE_FORM,
                "isModalContainer": false,
                "skin": "sknFlxBg000000op40",
                "width": "100%",
                "zIndex": 5
            }, {}, {});
            flxEmailPreview.setDefaultUnit(kony.flex.DP);
            var flxDisplayEmailPreview = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "bottom": "5.25%",
                "clipBounds": true,
                "id": "flxDisplayEmailPreview",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "7.32%",
                "isModalContainer": false,
                "right": "7.32%",
                "skin": "sknBackgroundFFFFFF",
                "top": "5.25%"
            }, {}, {});
            flxDisplayEmailPreview.setDefaultUnit(kony.flex.DP);
            var flxEmailPreviewTopBar = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "10px",
                "id": "flxEmailPreviewTopBar",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0px",
                "isModalContainer": false,
                "skin": "sknFlxInfoToastBg357C9ENoRadius",
                "top": "0px",
                "width": "100%",
                "zIndex": 1
            }, {}, {});
            flxEmailPreviewTopBar.setDefaultUnit(kony.flex.DP);
            flxEmailPreviewTopBar.add();
            var flxEmailPreviewHeader = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "70px",
                "id": "flxEmailPreviewHeader",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0px",
                "isModalContainer": false,
                "right": "0px",
                "skin": "slFbox",
                "top": "10px",
                "width": "100%",
                "zIndex": 1
            }, {}, {});
            flxEmailPreviewHeader.setDefaultUnit(kony.flex.DP);
            var flxEmailPreviewMode = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "bottom": "73px",
                "clipBounds": true,
                "height": "50px",
                "id": "flxEmailPreviewMode",
                "isVisible": true,
                "layoutType": kony.flex.FLOW_HORIZONTAL,
                "left": "18px",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0px",
                "width": "150px",
                "zIndex": 1
            }, {}, {});
            flxEmailPreviewMode.setDefaultUnit(kony.flex.DP);
            var lblEmailPreviewMode = new kony.ui.Label({
                "id": "lblEmailPreviewMode",
                "isVisible": true,
                "left": "0px",
                "skin": "sknLbl485c75LatoBold13px",
                "text": "EMAIL PREVIEW MODE",
                "top": "24px",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxEmailPreviewMode.add(lblEmailPreviewMode);
            var flxEmailPreviewClose = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "20px",
                "id": "flxEmailPreviewClose",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "isModalContainer": false,
                "right": "20px",
                "skin": "sknFlxffffffCursorPointer",
                "top": "10px",
                "width": "20px",
                "zIndex": 1
            }, {}, {});
            flxEmailPreviewClose.setDefaultUnit(kony.flex.DP);
            var lblEmailPreviewClose = new kony.ui.Label({
                "height": "100%",
                "id": "lblEmailPreviewClose",
                "isVisible": true,
                "left": "0px",
                "skin": "lblIconGrey",
                "text": "",
                "top": "0dp",
                "width": "100%",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_CENTER,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxEmailPreviewClose.add(lblEmailPreviewClose);
            var flxEmailPreviewSeparator = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "bottom": "0px",
                "clipBounds": true,
                "height": "1px",
                "id": "flxEmailPreviewSeparator",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "20px",
                "isModalContainer": false,
                "right": "20px",
                "skin": "sknFlxD7D9E0Separator",
                "top": "55px",
                "zIndex": 1
            }, {}, {});
            flxEmailPreviewSeparator.setDefaultUnit(kony.flex.DP);
            flxEmailPreviewSeparator.add();
            flxEmailPreviewHeader.add(flxEmailPreviewMode, flxEmailPreviewClose, flxEmailPreviewSeparator);
            var flxEmailPreviewContent = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "bottom": "0px",
                "clipBounds": true,
                "id": "flxEmailPreviewContent",
                "isVisible": true,
                "layoutType": kony.flex.FLOW_VERTICAL,
                "left": "0px",
                "isModalContainer": false,
                "right": "0px",
                "skin": "slFbox",
                "top": "90px",
                "width": "100%"
            }, {}, {});
            flxEmailPreviewContent.setDefaultUnit(kony.flex.DP);
            var flxEmailPreviewScroll = new kony.ui.FlexScrollContainer({
                "allowHorizontalBounce": false,
                "allowVerticalBounce": true,
                "bottom": "0px",
                "bounces": true,
                "clipBounds": true,
                "enableScrolling": true,
                "height": "100%",
                "horizontalScrollIndicator": true,
                "id": "flxEmailPreviewScroll",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0px",
                "pagingEnabled": false,
                "right": "10px",
                "scrollDirection": kony.flex.SCROLL_BOTH,
                "skin": "slFSbox",
                "top": "0px",
                "verticalScrollIndicator": true,
                "width": "100%",
                "zIndex": 1
            }, {}, {});
            flxEmailPreviewScroll.setDefaultUnit(kony.flex.DP);
            var flxEmailPreviewContentFlowVertical = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                "clipBounds": true,
                "id": "flxEmailPreviewContentFlowVertical",
                "isVisible": true,
                "layoutType": kony.flex.FLOW_VERTICAL,
                "left": "18px",
                "isModalContainer": false,
                "right": "18px",
                "skin": "slFbox",
                "top": "0px"
            }, {}, {});
            flxEmailPreviewContentFlowVertical.setDefaultUnit(kony.flex.DP);
            var flxEmailPreviewSubject = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                "clipBounds": true,
                "id": "flxEmailPreviewSubject",
                "isVisible": true,
                "layoutType": kony.flex.FLOW_VERTICAL,
                "left": "0px",
                "isModalContainer": false,
                "right": "0px",
                "skin": "slFbox",
                "top": "0px",
                "width": "100%",
                "zIndex": 1
            }, {}, {});
            flxEmailPreviewSubject.setDefaultUnit(kony.flex.DP);
            var lblSubjectHeader = new kony.ui.Label({
                "id": "lblSubjectHeader",
                "isVisible": true,
                "left": "0px",
                "skin": "sknLatoRegular192B4516px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmAlertsManagement.SubjectColon\")",
                "top": "0px",
                "width": "100%",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var lblSubject = new kony.ui.Label({
                "id": "lblSubject",
                "isVisible": true,
                "left": "0",
                "skin": "sknLblLato485c7513px",
                "text": "Platinum Customers",
                "top": "15px",
                "width": "100%",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxEmailPreviewSubject.add(lblSubjectHeader, lblSubject);
            var flxEmailPreviewDescription = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "bottom": "0px",
                "clipBounds": true,
                "height": "1000px",
                "id": "flxEmailPreviewDescription",
                "isVisible": true,
                "layoutType": kony.flex.FLOW_VERTICAL,
                "left": "0px",
                "isModalContainer": false,
                "right": "0px",
                "skin": "slFbox",
                "top": "20px",
                "width": "100%",
                "zIndex": 1
            }, {}, {});
            flxEmailPreviewDescription.setDefaultUnit(kony.flex.DP);
            var lblEmailDescHeader = new kony.ui.Label({
                "id": "lblEmailDescHeader",
                "isVisible": true,
                "left": "0",
                "skin": "sknLatoRegular192B4516px",
                "text": "Email Description: ",
                "top": "0px",
                "width": "100%",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var rtxEmailDescPreview = new kony.ui.RichText({
                "height": "100%",
                "id": "rtxEmailDescPreview",
                "isVisible": true,
                "left": "0px",
                "linkSkin": "defRichTextLink",
                "skin": "slRichText",
                "top": "15px",
                "width": "100%",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxEmailPreviewDescription.add(lblEmailDescHeader, rtxEmailDescPreview);
            flxEmailPreviewContentFlowVertical.add(flxEmailPreviewSubject, flxEmailPreviewDescription);
            flxEmailPreviewScroll.add(flxEmailPreviewContentFlowVertical);
            flxEmailPreviewContent.add(flxEmailPreviewScroll);
            flxDisplayEmailPreview.add(flxEmailPreviewTopBar, flxEmailPreviewHeader, flxEmailPreviewContent);
            flxEmailPreview.add(flxDisplayEmailPreview);
            this.add(flxLeftPannel, flxRightPannel, flxConsentPopup, flxToastMessage, flxViewAdPreview, flxViewAttributeList, flxEmailPreview);
        };
        return [{
            "addWidgets": addWidgetsfrmAdDetails,
            "enabledForIdleTimeout": true,
            "id": "frmAdDetails",
            "layoutType": kony.flex.FREE_FORM,
            "needAppMenu": false,
            "postShow": controller.AS_Form_bf5188f919a741e0bb5187da73d26d40,
            "preShow": function(eventobject) {
                controller.AS_Form_e9f8359825e84b548cde686afc1299a2(eventobject);
            },
            "skin": "slForm"
        }, {
            "displayOrientation": constants.FORM_DISPLAY_ORIENTATION_PORTRAIT,
            "layoutType": kony.flex.FREE_FORM,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, {
            "onDeviceBack": controller.AS_Form_b0638d9f664d41828d64a669d1ca619c,
            "retainScrollPosition": false
        }]
    }
});