define("CampaignModule/frmAdManagement", function() {
    return function(controller) {
        function addWidgetsfrmAdManagement() {
            this.setDefaultUnit(kony.flex.DP);
            var flxLeftPannel = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "100%",
                "id": "flxLeftPannel",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "masterType": constants.MASTER_TYPE_USERWIDGET,
                "isModalContainer": false,
                "skin": "sknLeftPannel0bb79e4bcca8e45",
                "top": "0dp",
                "width": "305dp",
                "zIndex": 5
            }, {}, {});
            flxLeftPannel.setDefaultUnit(kony.flex.DP);
            var leftMenuNew = new com.adminConsole.navigation.leftMenuNew({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "height": "100%",
                "id": "leftMenuNew",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "masterType": constants.MASTER_TYPE_DEFAULT,
                "isModalContainer": false,
                "skin": "sknFlxBg003E75Op100",
                "top": "0dp",
                "width": "100%",
                "overrides": {
                    "brandLogo": {
                        "src": "infinity_dbx_c360_logo_white_2x.png"
                    },
                    "flxScrollMenu": {
                        "bottom": "40dp",
                        "left": "0dp",
                        "top": "80dp"
                    },
                    "imgBottomLogo": {
                        "src": "logo_white_2x.png"
                    },
                    "leftMenuNew": {
                        "bottom": "viz.val_cleared",
                        "centerX": "viz.val_cleared",
                        "centerY": "viz.val_cleared",
                        "height": "100%",
                        "left": "0dp",
                        "maxHeight": "viz.val_cleared",
                        "maxWidth": "viz.val_cleared",
                        "minHeight": "viz.val_cleared",
                        "minWidth": "viz.val_cleared",
                        "right": "viz.val_cleared",
                        "top": "0dp",
                        "width": "100%"
                    }
                }
            }, {
                "overrides": {}
            }, {
                "overrides": {}
            });
            flxLeftPannel.add(leftMenuNew);
            var flxRightPannel = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "100%",
                "id": "flxRightPannel",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "305px",
                "isModalContainer": false,
                "right": "0dp",
                "skin": "slFbox0b7aaa2e3bfa340",
                "zIndex": 5
            }, {}, {});
            flxRightPannel.setDefaultUnit(kony.flex.DP);
            var flxHeaderDropdown = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                "clipBounds": true,
                "id": "flxHeaderDropdown",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "isModalContainer": false,
                "right": "35dp",
                "skin": "slFbox",
                "top": "20dp",
                "width": "400dp",
                "zIndex": 5
            }, {}, {});
            flxHeaderDropdown.setDefaultUnit(kony.flex.DP);
            var dropdownMainHeader = new com.adminConsole.common.dropdownMainHeader({
                "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                "id": "dropdownMainHeader",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "masterType": constants.MASTER_TYPE_DEFAULT,
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0dp",
                "width": "100%",
                "overrides": {
                    "flxDropdown": {
                        "isVisible": false
                    },
                    "imgUpArrow": {
                        "src": "uparrow_2x.png"
                    }
                }
            }, {
                "overrides": {}
            }, {
                "overrides": {}
            });
            flxHeaderDropdown.add(dropdownMainHeader);
            var flxMainHeader = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "106dp",
                "id": "flxMainHeader",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0dp",
                "width": "100%",
                "zIndex": 1
            }, {}, {});
            flxMainHeader.setDefaultUnit(kony.flex.DP);
            var mainHeader = new com.adminConsole.header.mainHeader({
                "height": "106px",
                "id": "mainHeader",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "masterType": constants.MASTER_TYPE_DEFAULT,
                "isModalContainer": false,
                "skin": "CopyslFbox1",
                "width": "100%",
                "overrides": {
                    "btnAddNewOption": {
                        "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmAdManagement.DefaultCampaignCAPS\")",
                        "isVisible": false,
                        "right": "212px"
                    },
                    "btnDropdownList": {
                        "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmAdManagement.CreateAdCampaignCAPS\")"
                    },
                    "flxButtons": {
                        "right": "100dp",
                        "top": "55px",
                        "width": "420dp"
                    },
                    "flxMainHeader": {
                        "left": undefined,
                        "top": undefined
                    },
                    "imgLogout": {
                        "right": "0px",
                        "src": "img_logout.png"
                    },
                    "lblHeading": {
                        "centerY": "viz.val_cleared",
                        "height": kony.flex.USE_PREFFERED_SIZE,
                        "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmAdManagement.AdCampaigns\")",
                        "top": "55px",
                        "width": kony.flex.USE_PREFFERED_SIZE
                    },
                    "lblUserName": {
                        "right": "25px",
                        "text": "Preetish",
                        "top": "viz.val_cleared"
                    },
                    "mainHeader": {
                        "left": undefined,
                        "top": undefined
                    }
                }
            }, {
                "overrides": {}
            }, {
                "overrides": {}
            });
            var flxSettings = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "30dp",
                "id": "flxSettings",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "isModalContainer": false,
                "right": "50dp",
                "skin": "sknflxBorder485c75Radius20PxPointer",
                "top": "55dp",
                "width": "30dp",
                "zIndex": 2
            }, {}, {});
            flxSettings.setDefaultUnit(kony.flex.DP);
            var lblIconSettings = new kony.ui.Label({
                "centerX": "47%",
                "centerY": "48%",
                "height": "20dp",
                "id": "lblIconSettings",
                "isVisible": true,
                "skin": "sknLblIcomoon20px485c75",
                "text": "",
                "width": "20dp",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxSettings.add(lblIconSettings);
            flxMainHeader.add(mainHeader, flxSettings);
            var flxBreadCrumbs = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "30px",
                "id": "flxBreadCrumbs",
                "isVisible": false,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "masterType": constants.MASTER_TYPE_USERWIDGET,
                "isModalContainer": false,
                "skin": "sknflxf5f6f8Op100",
                "top": "95px",
                "width": "100%",
                "zIndex": 1
            }, {}, {});
            flxBreadCrumbs.setDefaultUnit(kony.flex.DP);
            var breadcrumbs = new com.adminConsole.common.breadcrumbs({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "height": "20px",
                "id": "breadcrumbs",
                "isVisible": true,
                "layoutType": kony.flex.FLOW_HORIZONTAL,
                "left": "0dp",
                "masterType": constants.MASTER_TYPE_DEFAULT,
                "isModalContainer": false,
                "skin": "sknflxf5f6f8Op100",
                "top": "1px",
                "width": "100%",
                "overrides": {
                    "breadcrumbs": {
                        "bottom": "viz.val_cleared",
                        "top": "1px",
                        "width": "100%"
                    },
                    "imgBreadcrumbsDown": {
                        "src": "img_down_arrow.png"
                    },
                    "imgBreadcrumbsRight": {
                        "left": "10dp",
                        "right": 10,
                        "src": "img_breadcrumb_arrow.png"
                    },
                    "imgBreadcrumbsRight2": {
                        "left": "10dp",
                        "right": 10,
                        "src": "img_breadcrumb_arrow.png"
                    },
                    "imgBreadcrumbsRight3": {
                        "src": "img_breadcrumb_arrow.png"
                    }
                }
            }, {
                "overrides": {}
            }, {
                "overrides": {}
            });
            var flxHeaderSeperator = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "bottom": "1px",
                "clipBounds": true,
                "height": "1px",
                "id": "flxHeaderSeperator",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "35dp",
                "isModalContainer": false,
                "right": "35dp",
                "skin": "sknflx115678",
                "zIndex": 1
            }, {}, {});
            flxHeaderSeperator.setDefaultUnit(kony.flex.DP);
            flxHeaderSeperator.add();
            flxBreadCrumbs.add(breadcrumbs, flxHeaderSeperator);
            var flxListing = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                "bottom": "0dp",
                "clipBounds": true,
                "id": "flxListing",
                "isVisible": false,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "isModalContainer": false,
                "right": "0dp",
                "skin": "slFbox",
                "top": "106dp",
                "zIndex": 1
            }, {}, {});
            flxListing.setDefaultUnit(kony.flex.DP);
            var flxMainSubHeader = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "63dp",
                "id": "flxMainSubHeader",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0dp",
                "width": "100%",
                "zIndex": 100
            }, {}, {});
            flxMainSubHeader.setDefaultUnit(kony.flex.DP);
            var subHeader = new com.adminConsole.header.subHeader({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "centerY": "50%",
                "height": "42dp",
                "id": "subHeader",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "masterType": constants.MASTER_TYPE_DEFAULT,
                "isModalContainer": false,
                "skin": "CopyslFbox2",
                "top": "0dp",
                "width": "100%",
                "overrides": {
                    "flxMenu": {
                        "isVisible": false
                    },
                    "flxSearch": {
                        "width": "48.10%"
                    },
                    "flxSearchContainer": {
                        "centerY": "50%",
                        "height": "83.30%",
                        "width": "100%"
                    },
                    "flxSubHeader": {
                        "centerY": "viz.val_cleared"
                    },
                    "subHeader": {
                        "centerY": "50%",
                        "height": "42dp",
                        "maxHeight": "viz.val_cleared"
                    },
                    "tbxSearchBox": {
                        "i18n_placeholder": "kony.i18n.getLocalizedString(\"i18n.frmAdManagement.SearchCampaignName\")"
                    }
                }
            }, {
                "overrides": {}
            }, {
                "overrides": {}
            });
            flxMainSubHeader.add(subHeader);
            var flxListingMainContainer = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "bottom": "0dp",
                "clipBounds": true,
                "id": "flxListingMainContainer",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "35dp",
                "isModalContainer": false,
                "right": "35dp",
                "skin": "sknflxffffffBorderd6dbe7Radius4px",
                "top": "63dp",
                "zIndex": 100
            }, {}, {});
            flxListingMainContainer.setDefaultUnit(kony.flex.DP);
            var flxCampaignsInfo = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "60dp",
                "id": "flxCampaignsInfo",
                "isVisible": false,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0dp",
                "width": "100%",
                "zIndex": 11
            }, {}, {});
            flxCampaignsInfo.setDefaultUnit(kony.flex.DP);
            var lblCampaignsInfo = new kony.ui.Label({
                "centerY": "50%",
                "id": "lblCampaignsInfo",
                "isVisible": true,
                "left": "2%",
                "skin": "sknLbl485C75LatoRegular12Px",
                "text": "Label",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var lblCampaignsInfoSeparator = new kony.ui.Label({
                "bottom": "0dp",
                "height": "1dp",
                "id": "lblCampaignsInfoSeparator",
                "isVisible": true,
                "left": "15dp",
                "right": "15dp",
                "skin": "sknlblSeperator",
                "text": "Label",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxCampaignsInfo.add(lblCampaignsInfo, lblCampaignsInfoSeparator);
            var flxCampaignsHeader = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "60dp",
                "id": "flxCampaignsHeader",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "20px",
                "isModalContainer": false,
                "right": "20px",
                "skin": "sknflxffffffop100",
                "top": "0dp",
                "zIndex": 11
            }, {}, {});
            flxCampaignsHeader.setDefaultUnit(kony.flex.DP);
            var flxCampaignNameHeader = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "100%",
                "id": "flxCampaignNameHeader",
                "isVisible": true,
                "layoutType": kony.flex.FLOW_HORIZONTAL,
                "left": "6.50%",
                "isModalContainer": false,
                "onClick": controller.AS_FlexContainer_a43b80cc872f4831807a918bd48f0ac3,
                "skin": "sknCursor",
                "top": "0dp",
                "width": "126dp",
                "zIndex": 1
            }, {}, {});
            flxCampaignNameHeader.setDefaultUnit(kony.flex.DP);
            var lblCampaignNameHeader = new kony.ui.Label({
                "centerY": "50%",
                "id": "lblCampaignNameHeader",
                "isVisible": true,
                "left": "0dp",
                "skin": "sknlblLato696c7312px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmAdManagement.campaignNameHeader\")",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {
                "hoverSkin": "sknlblLatoBold00000012px"
            });
            var fontIconSortCampaignName = new kony.ui.Label({
                "centerY": "50%",
                "id": "fontIconSortCampaignName",
                "isVisible": true,
                "left": "5dp",
                "skin": "sknIcon15px",
                "text": "",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {
                "hoverSkin": "sknlblCursorFont"
            });
            flxCampaignNameHeader.add(lblCampaignNameHeader, fontIconSortCampaignName);
            var flxCampaignPriorityHeader = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "100%",
                "id": "flxCampaignPriorityHeader",
                "isVisible": true,
                "layoutType": kony.flex.FLOW_HORIZONTAL,
                "left": "31%",
                "isModalContainer": false,
                "onClick": controller.AS_FlexContainer_f184888da9f042978e3b0332711e0319,
                "skin": "sknCursor",
                "top": "0dp",
                "width": "76dp",
                "zIndex": 1
            }, {}, {});
            flxCampaignPriorityHeader.setDefaultUnit(kony.flex.DP);
            var lblCampaignPriorityHeader = new kony.ui.Label({
                "centerY": "50%",
                "id": "lblCampaignPriorityHeader",
                "isVisible": true,
                "left": "0px",
                "skin": "sknlblLato696c7312px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmAdManagement.PriorityCAPS\")",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {
                "hoverSkin": "sknlblLatoBold00000012px"
            });
            var fontIconSortCampaignPriority = new kony.ui.Label({
                "centerY": "50%",
                "id": "fontIconSortCampaignPriority",
                "isVisible": true,
                "left": "5dp",
                "skin": "sknIcon15px",
                "text": "",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {
                "hoverSkin": "sknlblCursorFont"
            });
            flxCampaignPriorityHeader.add(lblCampaignPriorityHeader, fontIconSortCampaignPriority);
            var flxCampaignStartDateTimeHeader = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "100%",
                "id": "flxCampaignStartDateTimeHeader",
                "isVisible": true,
                "layoutType": kony.flex.FLOW_HORIZONTAL,
                "left": "43.80%",
                "isModalContainer": false,
                "onClick": controller.AS_FlexContainer_if8688b9f1b14c0796ccfa73761ffbf7,
                "skin": "sknCursor",
                "top": "0dp",
                "width": "130dp",
                "zIndex": 1
            }, {}, {});
            flxCampaignStartDateTimeHeader.setDefaultUnit(kony.flex.DP);
            var lblCampaignStartDateTimeHeader = new kony.ui.Label({
                "centerY": "50%",
                "id": "lblCampaignStartDateTimeHeader",
                "isVisible": true,
                "left": "0px",
                "skin": "sknlblLato696c7312px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmTrackApplication.StartDateCAPS\")",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {
                "hoverSkin": "sknlblLatoBold00000012px"
            });
            var fontIconSortCampaignStartDateTime = new kony.ui.Label({
                "centerY": "50%",
                "id": "fontIconSortCampaignStartDateTime",
                "isVisible": true,
                "left": "5dp",
                "skin": "sknIcon15px",
                "text": "",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {
                "hoverSkin": "sknlblCursorFont"
            });
            flxCampaignStartDateTimeHeader.add(lblCampaignStartDateTimeHeader, fontIconSortCampaignStartDateTime);
            var flxCampaignEndDateTimeHeader = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "100%",
                "id": "flxCampaignEndDateTimeHeader",
                "isVisible": true,
                "layoutType": kony.flex.FLOW_HORIZONTAL,
                "left": "64.20%",
                "isModalContainer": false,
                "onClick": controller.AS_FlexContainer_e8c18fc5a3a34ece96d2cc198ff5d25a,
                "skin": "sknCursor",
                "top": "0dp",
                "width": "120dp",
                "zIndex": 1
            }, {}, {});
            flxCampaignEndDateTimeHeader.setDefaultUnit(kony.flex.DP);
            var lblCampaignEndDateTimeHeader = new kony.ui.Label({
                "centerY": "50%",
                "id": "lblCampaignEndDateTimeHeader",
                "isVisible": true,
                "left": "0px",
                "skin": "sknlblLato696c7312px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmAdManagement.EndDateCAPS\")",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {
                "hoverSkin": "sknlblLatoBold00000012px"
            });
            var fontIconSortCampaignEndDateTime = new kony.ui.Label({
                "centerY": "50%",
                "id": "fontIconSortCampaignEndDateTime",
                "isVisible": true,
                "left": "5dp",
                "skin": "sknIcon15px",
                "text": "",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {
                "hoverSkin": "sknlblCursorFont"
            });
            flxCampaignEndDateTimeHeader.add(lblCampaignEndDateTimeHeader, fontIconSortCampaignEndDateTime);
            var flxCampaignStatusHeader = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "100%",
                "id": "flxCampaignStatusHeader",
                "isVisible": true,
                "layoutType": kony.flex.FLOW_HORIZONTAL,
                "left": "83%",
                "isModalContainer": false,
                "onClick": controller.AS_FlexContainer_ia2738f1fdda45cf914f716c251ff157,
                "skin": "sknCursor",
                "top": "0dp",
                "width": "74dp",
                "zIndex": 1
            }, {}, {});
            flxCampaignStatusHeader.setDefaultUnit(kony.flex.DP);
            var lblCampaignStatusHeader = new kony.ui.Label({
                "centerY": "50%",
                "id": "lblCampaignStatusHeader",
                "isVisible": true,
                "left": "0px",
                "skin": "sknlblLato696c7312px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.roles.STATUS\")",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {
                "hoverSkin": "sknlblLatoBold00000012px"
            });
            var fontIconFilterCampaignStatus = new kony.ui.Label({
                "centerY": "50%",
                "id": "fontIconFilterCampaignStatus",
                "isVisible": true,
                "left": "5dp",
                "skin": "sknIcon15px",
                "text": "",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {
                "hoverSkin": "sknlblCursorFont"
            });
            flxCampaignStatusHeader.add(lblCampaignStatusHeader, fontIconFilterCampaignStatus);
            var lblCampaignsHeaderSeparator = new kony.ui.Label({
                "bottom": 0,
                "height": "1px",
                "id": "lblCampaignsHeaderSeparator",
                "isVisible": true,
                "left": "0px",
                "right": "0px",
                "skin": "sknLblTableHeaderLine",
                "text": ".",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxCampaignsHeader.add(flxCampaignNameHeader, flxCampaignPriorityHeader, flxCampaignStartDateTimeHeader, flxCampaignEndDateTimeHeader, flxCampaignStatusHeader, lblCampaignsHeaderSeparator);
            var flxSegCampaigns = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "bottom": "0dp",
                "clipBounds": true,
                "id": "flxSegCampaigns",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "60dp",
                "width": "100%"
            }, {}, {});
            flxSegCampaigns.setDefaultUnit(kony.flex.DP);
            var segCampaigns = new kony.ui.SegmentedUI2({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "bottom": "0dp",
                "data": [{
                    "fontIconStatusImg": "",
                    "fonticonArrow": "",
                    "lblCampaignsName": "",
                    "lblDescription": "",
                    "lblDescriptionHeader": "",
                    "lblEndDateTime": "",
                    "lblIconOptions": "",
                    "lblPriority": "",
                    "lblSeparator": "",
                    "lblStartDateTime": "",
                    "lblStatus": ""
                }],
                "groupCells": false,
                "id": "segCampaigns",
                "isVisible": true,
                "left": "20px",
                "needPageIndicator": true,
                "pageOffDotImage": "pageoffdot.png",
                "pageOnDotImage": "pageondot.png",
                "retainSelection": false,
                "right": "20px",
                "rowFocusSkin": "seg2Focus",
                "rowSkin": "sknseg3pxRadius",
                "rowTemplate": "flxCampaigns",
                "sectionHeaderSkin": "sliPhoneSegmentHeader",
                "selectionBehavior": constants.SEGUI_DEFAULT_BEHAVIOR,
                "separatorRequired": false,
                "showScrollbars": true,
                "top": "0dp",
                "viewType": constants.SEGUI_VIEW_TYPE_TABLEVIEW,
                "widgetDataMap": {
                    "flxCampaignCollapse": "flxCampaignCollapse",
                    "flxCampaignExpand": "flxCampaignExpand",
                    "flxCampaigns": "flxCampaigns",
                    "flxDropdown": "flxDropdown",
                    "flxOptions": "flxOptions",
                    "flxStatus": "flxStatus",
                    "fontIconStatusImg": "fontIconStatusImg",
                    "fonticonArrow": "fonticonArrow",
                    "lblCampaignsName": "lblCampaignsName",
                    "lblDescription": "lblDescription",
                    "lblDescriptionHeader": "lblDescriptionHeader",
                    "lblEndDateTime": "lblEndDateTime",
                    "lblIconOptions": "lblIconOptions",
                    "lblPriority": "lblPriority",
                    "lblSeperator": "lblSeperator",
                    "lblStartDateTime": "lblStartDateTime",
                    "lblStatus": "lblStatus"
                },
                "zIndex": 1
            }, {
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxSegCampaigns.add(segCampaigns);
            var flxNoResultFound = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                "clipBounds": true,
                "id": "flxNoResultFound",
                "isVisible": false,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "60dp",
                "width": "100%",
                "zIndex": 1
            }, {}, {});
            flxNoResultFound.setDefaultUnit(kony.flex.DP);
            var rtxSearchMesg = new kony.ui.RichText({
                "bottom": "50px",
                "centerX": "52%",
                "id": "rtxSearchMesg",
                "isVisible": true,
                "left": "0",
                "skin": "sknRtxLato84939e12Px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmCSR.richtextNoResult\")",
                "top": "90px",
                "width": "80%",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_CENTER,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxNoResultFound.add(rtxSearchMesg);
            var flxCampaignStatusFilter = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                "clipBounds": true,
                "id": "flxCampaignStatusFilter",
                "isVisible": false,
                "layoutType": kony.flex.FREE_FORM,
                "isModalContainer": false,
                "right": "20dp",
                "skin": "slFbox",
                "top": "30dp",
                "width": "130dp",
                "zIndex": 11
            }, {}, {});
            flxCampaignStatusFilter.setDefaultUnit(kony.flex.DP);
            var statusFilterMenu = new com.adminConsole.common.statusFilterMenu({
                "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                "id": "statusFilterMenu",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "masterType": constants.MASTER_TYPE_DEFAULT,
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "5dp",
                "width": "100%",
                "zIndex": 5,
                "overrides": {
                    "imgUpArrow": {
                        "src": "uparrow_2x.png"
                    }
                }
            }, {
                "overrides": {}
            }, {
                "overrides": {}
            });
            flxCampaignStatusFilter.add(statusFilterMenu);
            flxListingMainContainer.add(flxCampaignsInfo, flxCampaignsHeader, flxSegCampaigns, flxNoResultFound, flxCampaignStatusFilter);
            flxListing.add(flxMainSubHeader, flxListingMainContainer);
            var flxViewDetails = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "bottom": "0px",
                "clipBounds": true,
                "id": "flxViewDetails",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0px",
                "isModalContainer": false,
                "right": "0px",
                "skin": "slFbox",
                "top": "126px",
                "width": "100%",
                "zIndex": 1
            }, {}, {});
            flxViewDetails.setDefaultUnit(kony.flex.DP);
            var flxScrollViewDetails = new kony.ui.FlexScrollContainer({
                "allowHorizontalBounce": false,
                "allowVerticalBounce": true,
                "bottom": "0px",
                "bounces": true,
                "clipBounds": true,
                "enableScrolling": true,
                "height": "100%",
                "horizontalScrollIndicator": true,
                "id": "flxScrollViewDetails",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "35px",
                "pagingEnabled": false,
                "right": "10px",
                "scrollDirection": kony.flex.SCROLL_VERTICAL,
                "skin": "sknscrollflxf5f6f8Op100",
                "top": "0%",
                "verticalScrollIndicator": true,
                "zIndex": 1
            }, {}, {});
            flxScrollViewDetails.setDefaultUnit(kony.flex.DP);
            var flxDetailsFlowVertical = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                "clipBounds": true,
                "id": "flxDetailsFlowVertical",
                "isVisible": true,
                "layoutType": kony.flex.FLOW_VERTICAL,
                "left": "0px",
                "isModalContainer": false,
                "right": "15px",
                "skin": "slFbox",
                "top": "0px",
                "zIndex": 1
            }, {}, {});
            flxDetailsFlowVertical.setDefaultUnit(kony.flex.DP);
            var flxTopDetails = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                "clipBounds": true,
                "id": "flxTopDetails",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0%",
                "isModalContainer": false,
                "right": "0%",
                "skin": "sknFlxbgFFFFFFbdrD7D9E0rd3px",
                "top": "0px",
                "width": "99%",
                "zIndex": 1
            }, {}, {});
            flxTopDetails.setDefaultUnit(kony.flex.DP);
            var flxCampaignDetailsTop = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "50px",
                "id": "flxCampaignDetailsTop",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0px",
                "isModalContainer": false,
                "right": "0px",
                "skin": "slFbox",
                "top": "0px",
                "width": "100%",
                "zIndex": 1
            }, {}, {});
            flxCampaignDetailsTop.setDefaultUnit(kony.flex.DP);
            var lblCampaignNameSelected = new kony.ui.Label({
                "height": "16px",
                "id": "lblCampaignNameSelected",
                "isVisible": true,
                "left": "20px",
                "skin": "sknLblLato485c7513px",
                "text": "SapphireCreditCard_Aug19",
                "top": "20px",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var flxCampaignStatus = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "centerY": "50%",
                "clipBounds": true,
                "height": "25px",
                "id": "flxCampaignStatus",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "isModalContainer": false,
                "right": "25px",
                "skin": "slFbox",
                "top": "12dp",
                "width": "20%",
                "zIndex": 1
            }, {}, {});
            flxCampaignStatus.setDefaultUnit(kony.flex.DP);
            var flxOptions = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "centerY": "50%",
                "clipBounds": true,
                "height": "100%",
                "id": "flxOptions",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "isModalContainer": false,
                "right": "0px",
                "skin": "sknFlxBorffffff1pxRound",
                "top": "0px",
                "width": "25px",
                "zIndex": 1
            }, {}, {
                "hoverSkin": "sknflxffffffop100Border424242Radius100px"
            });
            flxOptions.setDefaultUnit(kony.flex.DP);
            var lblIconOptions = new kony.ui.Label({
                "centerX": "50%",
                "centerY": "50%",
                "id": "lblIconOptions",
                "isVisible": true,
                "skin": "sknFontIconOptionMenu",
                "text": "",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxOptions.add(lblIconOptions);
            var flxOptionsDefault = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "centerY": "50%",
                "clipBounds": true,
                "height": "100%",
                "id": "flxOptionsDefault",
                "isVisible": false,
                "layoutType": kony.flex.FREE_FORM,
                "isModalContainer": false,
                "right": "0px",
                "skin": "sknFlxBorffffff1pxRound",
                "top": "0px",
                "width": "25px",
                "zIndex": 1
            }, {}, {
                "hoverSkin": "sknflxffffffop100Border424242Radius100px"
            });
            flxOptionsDefault.setDefaultUnit(kony.flex.DP);
            var lblIconOptionsDefault = new kony.ui.Label({
                "centerX": "50%",
                "centerY": "50%",
                "id": "lblIconOptionsDefault",
                "isVisible": true,
                "skin": "sknFontIconOptionMenu",
                "text": "",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxOptionsDefault.add(lblIconOptionsDefault);
            var flxStatus = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "centerY": "50%",
                "clipBounds": true,
                "height": "100%",
                "id": "flxStatus",
                "isVisible": true,
                "layoutType": kony.flex.FLOW_HORIZONTAL,
                "reverseLayoutDirection": true,
                "isModalContainer": false,
                "right": "38px",
                "skin": "slFbox",
                "width": "100%",
                "zIndex": 1
            }, {}, {});
            flxStatus.setDefaultUnit(kony.flex.DP);
            var lblCampaignStatus = new kony.ui.Label({
                "centerY": "50%",
                "height": "16px",
                "id": "lblCampaignStatus",
                "isVisible": true,
                "right": "0px",
                "skin": "sknlblLatoReg485c7513px",
                "text": "Active",
                "top": "0px",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var lblFontIconStatusImg = new kony.ui.Label({
                "centerY": "50%",
                "id": "lblFontIconStatusImg",
                "isVisible": true,
                "right": "5px",
                "skin": "sknLblIIcoMoon485c7514px",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxStatus.add(lblCampaignStatus, lblFontIconStatusImg);
            flxCampaignStatus.add(flxOptions, flxOptionsDefault, flxStatus);
            flxCampaignDetailsTop.add(lblCampaignNameSelected, flxCampaignStatus);
            var flxSeparatorLine = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "1dp",
                "id": "flxSeparatorLine",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "20px",
                "isModalContainer": false,
                "right": "20px",
                "skin": "sknFlxD7D9E0Separator",
                "top": "51px",
                "zIndex": 1
            }, {}, {});
            flxSeparatorLine.setDefaultUnit(kony.flex.DP);
            flxSeparatorLine.add();
            var flxFirstLine = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "98px",
                "id": "flxFirstLine",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "20px",
                "isModalContainer": false,
                "right": "20px",
                "skin": "slFbox",
                "top": "71px",
                "zIndex": 1
            }, {}, {});
            flxFirstLine.setDefaultUnit(kony.flex.DP);
            var flxStartDate = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "39px",
                "id": "flxStartDate",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0%",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0px",
                "width": "33%",
                "zIndex": 1
            }, {}, {});
            flxStartDate.setDefaultUnit(kony.flex.DP);
            var lblStartDateTag = new kony.ui.Label({
                "height": "13px",
                "id": "lblStartDateTag",
                "isVisible": true,
                "left": "0px",
                "skin": "sknlblLato11px696C73",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmCustomerManagement.lblStartDate\")",
                "top": "0px",
                "width": "134px",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var lblStartDate = new kony.ui.Label({
                "bottom": "0px",
                "height": "16px",
                "id": "lblStartDate",
                "isVisible": true,
                "left": "0px",
                "skin": "sknlbl485C75LatoSemiBold13px",
                "text": "08/01/2019 12:00 AM",
                "width": "100%",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxStartDate.add(lblStartDateTag, lblStartDate);
            var flxEndDate = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "39px",
                "id": "flxEndDate",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "33%",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0px",
                "width": "33%",
                "zIndex": 1
            }, {}, {});
            flxEndDate.setDefaultUnit(kony.flex.DP);
            var lblEndDateTag = new kony.ui.Label({
                "height": "13px",
                "id": "lblEndDateTag",
                "isVisible": true,
                "left": "0px",
                "skin": "sknlblLato11px696C73",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmAdManagement.EndDateCAPS\")",
                "top": "0px",
                "width": "134px",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var lblEndDate = new kony.ui.Label({
                "bottom": "0px",
                "height": "16px",
                "id": "lblEndDate",
                "isVisible": true,
                "left": "0px",
                "skin": "sknlbl485C75LatoSemiBold13px",
                "text": "08/01/2019 12:00 AM",
                "width": "100%",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxEndDate.add(lblEndDateTag, lblEndDate);
            var flxPriority = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "39px",
                "id": "flxPriority",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "66%",
                "isModalContainer": false,
                "right": "0px",
                "skin": "slFbox",
                "top": "0px",
                "zIndex": 1
            }, {}, {});
            flxPriority.setDefaultUnit(kony.flex.DP);
            var lblPriorityTag = new kony.ui.Label({
                "height": "13px",
                "id": "lblPriorityTag",
                "isVisible": true,
                "left": "0px",
                "skin": "sknlblLato11px696C73",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmAdManagement.PriorityCAPS\")",
                "top": "0px",
                "width": "55px",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var lblPriority = new kony.ui.Label({
                "bottom": "0px",
                "height": "16px",
                "id": "lblPriority",
                "isVisible": true,
                "left": "0px",
                "skin": "sknlbl485C75LatoSemiBold13px",
                "text": "08",
                "width": "100%",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxPriority.add(lblPriorityTag, lblPriority);
            var flxAdType = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "39px",
                "id": "flxAdType",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0%",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "59px",
                "width": "33%",
                "zIndex": 1
            }, {}, {});
            flxAdType.setDefaultUnit(kony.flex.DP);
            var lblAdTypeTag = new kony.ui.Label({
                "height": "13px",
                "id": "lblAdTypeTag",
                "isVisible": true,
                "left": "0px",
                "skin": "sknlblLato11px696C73",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmAdManagement.adTypeCAPS\")",
                "top": "0px",
                "width": "134px",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var lblAdType = new kony.ui.Label({
                "bottom": "0px",
                "height": "16px",
                "id": "lblAdType",
                "isVisible": true,
                "left": "0px",
                "skin": "sknlbl485C75LatoSemiBold13px",
                "text": "Marketing",
                "width": "100%",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxAdType.add(lblAdTypeTag, lblAdType);
            var flxChannels = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "39px",
                "id": "flxChannels",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "33%",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "59px",
                "width": "33%",
                "zIndex": 1
            }, {}, {});
            flxChannels.setDefaultUnit(kony.flex.DP);
            var lblChannelsTag = new kony.ui.Label({
                "height": "13px",
                "id": "lblChannelsTag",
                "isVisible": true,
                "left": "0px",
                "skin": "sknlblLato11px696C73",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmAdManagement.channelsCAPS\")",
                "top": "0px",
                "width": "134px",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var lblChannels = new kony.ui.Label({
                "bottom": "0px",
                "height": "16px",
                "id": "lblChannels",
                "isVisible": true,
                "left": "0px",
                "skin": "sknlbl485C75LatoSemiBold13px",
                "text": "Offline, In App",
                "width": "100%",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxChannels.add(lblChannelsTag, lblChannels);
            flxFirstLine.add(flxStartDate, flxEndDate, flxPriority, flxAdType, flxChannels);
            var flxSecondLineGroup = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                "clipBounds": true,
                "id": "flxSecondLineGroup",
                "isVisible": true,
                "layoutType": kony.flex.FLOW_VERTICAL,
                "left": "20px",
                "isModalContainer": false,
                "right": "20px",
                "skin": "slFbox",
                "top": "189dp"
            }, {}, {});
            flxSecondLineGroup.setDefaultUnit(kony.flex.DP);
            var flxMarketingLine = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                "bottom": "20px",
                "clipBounds": true,
                "id": "flxMarketingLine",
                "isVisible": false,
                "layoutType": kony.flex.FREE_FORM,
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0px",
                "width": "100%",
                "zIndex": 1
            }, {}, {});
            flxMarketingLine.setDefaultUnit(kony.flex.DP);
            var flxProductGroup = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "39px",
                "id": "flxProductGroup",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0%",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0px",
                "width": "33%",
                "zIndex": 1
            }, {}, {});
            flxProductGroup.setDefaultUnit(kony.flex.DP);
            var lblProductGroupTag = new kony.ui.Label({
                "height": "13px",
                "id": "lblProductGroupTag",
                "isVisible": true,
                "left": "0px",
                "skin": "sknlblLato11px696C73",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmAdManagement.productGroupCAPS\")",
                "top": "0px",
                "width": "134px",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var lblProductGroup = new kony.ui.Label({
                "bottom": "0px",
                "height": "16px",
                "id": "lblProductGroup",
                "isVisible": true,
                "left": "0px",
                "skin": "sknlbl485C75LatoSemiBold13px",
                "text": "Loans",
                "width": "100%",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxProductGroup.add(lblProductGroupTag, lblProductGroup);
            var flxProductName = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "39px",
                "id": "flxProductName",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "33%",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0px",
                "width": "33%",
                "zIndex": 1
            }, {}, {});
            flxProductName.setDefaultUnit(kony.flex.DP);
            var lblProductNameTag = new kony.ui.Label({
                "height": "13px",
                "id": "lblProductNameTag",
                "isVisible": true,
                "left": "0px",
                "skin": "sknlblLato11px696C73",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmAdManagement.productNameCAPS\")",
                "top": "0px",
                "width": "134px",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var lblProductName = new kony.ui.Label({
                "bottom": "0px",
                "height": "16px",
                "id": "lblProductName",
                "isVisible": true,
                "left": "0px",
                "skin": "sknlbl485C75LatoSemiBold13px",
                "text": "Home Loans",
                "width": "100%",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxProductName.add(lblProductNameTag, lblProductName);
            flxMarketingLine.add(flxProductGroup, flxProductName);
            var flxSecondLine = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                "bottom": "20px",
                "clipBounds": true,
                "id": "flxSecondLine",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0px",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0px",
                "width": "100%",
                "zIndex": 1
            }, {}, {});
            flxSecondLine.setDefaultUnit(kony.flex.DP);
            var flxDescription = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                "clipBounds": true,
                "id": "flxDescription",
                "isVisible": true,
                "layoutType": kony.flex.FLOW_VERTICAL,
                "left": "0px",
                "isModalContainer": false,
                "right": "0px",
                "skin": "slFbox",
                "top": "0px",
                "zIndex": 1
            }, {}, {});
            flxDescription.setDefaultUnit(kony.flex.DP);
            var flxDescriptionTag = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                "clipBounds": true,
                "id": "flxDescriptionTag",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0px",
                "isModalContainer": false,
                "right": "0px",
                "skin": "sknFlxffffffCursorPointer",
                "top": "0px",
                "width": "91px",
                "zIndex": 1
            }, {}, {});
            flxDescriptionTag.setDefaultUnit(kony.flex.DP);
            var lblDescriptionTag = new kony.ui.Label({
                "height": "13px",
                "id": "lblDescriptionTag",
                "isVisible": true,
                "left": "0px",
                "skin": "sknlblLato11px696C73",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmAdManagement.DescriptionCAPS\")",
                "top": "0px",
                "width": "73px",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var lblIconDownArrow = new kony.ui.Label({
                "height": "13px",
                "id": "lblIconDownArrow",
                "isVisible": false,
                "left": "78px",
                "skin": "sknfontIconDescDownArrow12px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.userwidgetmodel.fontIconBreadcrumbsDown\")",
                "top": "0px",
                "width": "13px",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {
                "hoverSkin": "sknLblHoverCursor"
            });
            var lblIconRightArrow = new kony.ui.Label({
                "height": "13px",
                "id": "lblIconRightArrow",
                "isVisible": true,
                "left": "78px",
                "skin": "sknfontIconDescRightArrow12px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.userwidgetmodel.fontIconBreadcrumbsRight\")",
                "top": "0px",
                "width": "13px",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {
                "hoverSkin": "sknLblHoverCursor"
            });
            flxDescriptionTag.add(lblDescriptionTag, lblIconDownArrow, lblIconRightArrow);
            var flxDescriptionBody = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                "clipBounds": true,
                "id": "flxDescriptionBody",
                "isVisible": false,
                "layoutType": kony.flex.FLOW_VERTICAL,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "15px",
                "width": "100%"
            }, {}, {});
            flxDescriptionBody.setDefaultUnit(kony.flex.DP);
            var lblDescription = new kony.ui.Label({
                "id": "lblDescription",
                "isVisible": true,
                "left": "0px",
                "skin": "sknlbl485C75LatoSemiBold13px",
                "text": "Promotion for Sapphire Credit Card, targeted  at all customers who live in Austin, TX",
                "top": "0px",
                "width": "100%",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxDescriptionBody.add(lblDescription);
            flxDescription.add(flxDescriptionTag, flxDescriptionBody);
            flxSecondLine.add(flxDescription);
            flxSecondLineGroup.add(flxMarketingLine, flxSecondLine);
            var flxDefaultAdCampaign = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                "bottom": "20px",
                "clipBounds": true,
                "id": "flxDefaultAdCampaign",
                "isVisible": false,
                "layoutType": kony.flex.FLOW_VERTICAL,
                "left": "20px",
                "isModalContainer": false,
                "right": "20px",
                "skin": "slFbox",
                "top": "71px",
                "zIndex": 1
            }, {}, {});
            flxDefaultAdCampaign.setDefaultUnit(kony.flex.DP);
            var flxDefaultDescriptionTag = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                "clipBounds": true,
                "id": "flxDefaultDescriptionTag",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0px",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0px",
                "width": "73px"
            }, {}, {});
            flxDefaultDescriptionTag.setDefaultUnit(kony.flex.DP);
            var lblDefaultDescriptionTag = new kony.ui.Label({
                "id": "lblDefaultDescriptionTag",
                "isVisible": true,
                "left": "0px",
                "skin": "sknlblLato11px696C73",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmAdManagement.DescriptionCAPS\")",
                "top": "0px",
                "width": "73px",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxDefaultDescriptionTag.add(lblDefaultDescriptionTag);
            var flxDefaultDescription = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                "clipBounds": true,
                "id": "flxDefaultDescription",
                "isVisible": true,
                "layoutType": kony.flex.FLOW_VERTICAL,
                "left": "0px",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "28px",
                "width": "100%"
            }, {}, {});
            flxDefaultDescription.setDefaultUnit(kony.flex.DP);
            var lblDefaultDescription = new kony.ui.Label({
                "id": "lblDefaultDescription",
                "isVisible": true,
                "left": "0px",
                "skin": "sknlbl485C75LatoSemiBold13px",
                "text": "This campaign will be displayed to the end user when no other campaign is available or scheduled for him.",
                "top": "0px",
                "width": "100%",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxDefaultDescription.add(lblDefaultDescription);
            flxDefaultAdCampaign.add(flxDefaultDescriptionTag, flxDefaultDescription);
            flxTopDetails.add(flxCampaignDetailsTop, flxSeparatorLine, flxFirstLine, flxSecondLineGroup, flxDefaultAdCampaign);
            var flxBottomDetails = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                "clipBounds": true,
                "id": "flxBottomDetails",
                "isVisible": true,
                "layoutType": kony.flex.FLOW_VERTICAL,
                "left": "0px",
                "isModalContainer": false,
                "right": "15px",
                "skin": "sknFlxbgFFFFFFbdrD7D9E0rd3px",
                "top": "20px",
                "width": "99%",
                "zIndex": 1
            }, {}, {});
            flxBottomDetails.setDefaultUnit(kony.flex.DP);
            var flxViewTabs = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "50px",
                "id": "flxViewTabs",
                "isVisible": true,
                "layoutType": kony.flex.FLOW_HORIZONTAL,
                "left": "0px",
                "isModalContainer": false,
                "right": "0px",
                "skin": "slFbox",
                "top": "0px",
                "zIndex": 4
            }, {}, {});
            flxViewTabs.setDefaultUnit(kony.flex.DP);
            var flxTab2 = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "100%",
                "id": "flxTab2",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "20px",
                "isModalContainer": false,
                "skin": "sknFlxffffffCursorPointer",
                "top": "0px",
                "width": "160px",
                "zIndex": 1
            }, {}, {});
            flxTab2.setDefaultUnit(kony.flex.DP);
            var lblTargetCustomerRoles = new kony.ui.Label({
                "centerX": "50%",
                "centerY": "50%",
                "height": "100%",
                "id": "lblTargetCustomerRoles",
                "isVisible": true,
                "skin": "sknlblLatoRegular485c7512px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmAdManagement.TargetCustomerRolesCAPS\")",
                "width": "100%",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_CENTER,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxTab2.add(lblTargetCustomerRoles);
            var flxTab3 = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "100%",
                "id": "flxTab3",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "20px",
                "isModalContainer": false,
                "skin": "sknFlxffffffCursorPointer",
                "width": "125px",
                "zIndex": 1
            }, {}, {});
            flxTab3.setDefaultUnit(kony.flex.DP);
            var lblOffline = new kony.ui.Label({
                "centerX": "50%",
                "centerY": "50%",
                "height": "100%",
                "id": "lblOffline",
                "isVisible": true,
                "skin": "sknlblLatoRegular485c7512px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmAdManagement.OfflineChannelsCAPS\")",
                "width": "100%",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_CENTER,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxTab3.add(lblOffline);
            var flxTab1 = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "100%",
                "id": "flxTab1",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "20px",
                "isModalContainer": false,
                "skin": "sknFlxffffffCursorPointer",
                "top": "0px",
                "width": "120px",
                "zIndex": 1
            }, {}, {});
            flxTab1.setDefaultUnit(kony.flex.DP);
            var lblAdSpecifications = new kony.ui.Label({
                "centerX": "50%",
                "centerY": "50%",
                "height": "100%",
                "id": "lblAdSpecifications",
                "isVisible": true,
                "skin": "sknlblLatoBold485c7512px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmAdManagement.InAppChannelCAPS\")",
                "width": "100%",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_CENTER,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxTab1.add(lblAdSpecifications);
            var flxTab4 = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "100%",
                "id": "flxTab4",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "20px",
                "isModalContainer": false,
                "skin": "sknFlxffffffCursorPointer",
                "top": "0px",
                "width": "120px",
                "zIndex": 1
            }, {}, {});
            flxTab4.setDefaultUnit(kony.flex.DP);
            var lblPopupAd = new kony.ui.Label({
                "centerX": "50%",
                "centerY": "50%",
                "height": "100%",
                "id": "lblPopupAd",
                "isVisible": true,
                "skin": "sknlblLatoBold485c7512px",
                "text": "IN APP POPUP",
                "width": "100%",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_CENTER,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxTab4.add(lblPopupAd);
            flxViewTabs.add(flxTab2, flxTab3, flxTab1, flxTab4);
            var flxSeparator = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "1px",
                "id": "flxSeparator",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0px",
                "isModalContainer": false,
                "right": "0px",
                "skin": "sknflxSepratorE1E5ED",
                "top": "0px",
                "zIndex": 1
            }, {}, {});
            flxSeparator.setDefaultUnit(kony.flex.DP);
            flxSeparator.add();
            var flxChannelsDataFlowVertical = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                "clipBounds": true,
                "id": "flxChannelsDataFlowVertical",
                "isVisible": false,
                "layoutType": kony.flex.FLOW_VERTICAL,
                "left": "0px",
                "isModalContainer": false,
                "right": "0px",
                "skin": "slFbox",
                "top": "10px",
                "zIndex": 4
            }, {}, {});
            flxChannelsDataFlowVertical.setDefaultUnit(kony.flex.DP);
            flxChannelsDataFlowVertical.add();
            var flxTargetCustomerRolesData = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                "centerX": "50%",
                "clipBounds": true,
                "id": "flxTargetCustomerRolesData",
                "isVisible": true,
                "layoutType": kony.flex.FLOW_VERTICAL,
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0px",
                "width": "100%",
                "zIndex": 4
            }, {}, {});
            flxTargetCustomerRolesData.setDefaultUnit(kony.flex.DP);
            var flxTab2Wrapper = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "55dp",
                "id": "flxTab2Wrapper",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "sknf8f9fabordere1e5ed",
                "top": "0dp",
                "width": "100%",
                "zIndex": 10
            }, {}, {});
            flxTab2Wrapper.setDefaultUnit(kony.flex.DP);
            var tabs = new com.adminConsole.common.tabs({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "height": "100%",
                "id": "tabs",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "masterType": constants.MASTER_TYPE_DEFAULT,
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0dp",
                "width": "100%",
                "overrides": {
                    "btnTab1": {
                        "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmAdManagement.SegmentCAPS\")"
                    },
                    "btnTab2": {
                        "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmAdManagement.eventsCAPS\")"
                    }
                }
            }, {
                "overrides": {}
            }, {
                "overrides": {}
            });
            flxTab2Wrapper.add(tabs);
            var lblNoAd = new kony.ui.Label({
                "centerX": "50%",
                "height": "30dp",
                "id": "lblNoAd",
                "isVisible": false,
                "skin": "sknLblLato485c7513px",
                "text": "No Profiles Found",
                "top": "50dp",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var flxTargetDatasets = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                "clipBounds": true,
                "id": "flxTargetDatasets",
                "isVisible": true,
                "layoutType": kony.flex.FLOW_VERTICAL,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0dp",
                "width": "100%"
            }, {}, {});
            flxTargetDatasets.setDefaultUnit(kony.flex.DP);
            var flxSegmentHeader = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "50dp",
                "id": "flxSegmentHeader",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0dp",
                "width": "100%"
            }, {}, {});
            flxSegmentHeader.setDefaultUnit(kony.flex.DP);
            var flxRoleNameHeader = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                "clipBounds": true,
                "id": "flxRoleNameHeader",
                "isVisible": true,
                "layoutType": kony.flex.FLOW_HORIZONTAL,
                "left": "32px",
                "isModalContainer": false,
                "skin": "hoverhandSkin",
                "top": "21px",
                "width": "120dp"
            }, {}, {});
            flxRoleNameHeader.setDefaultUnit(kony.flex.DP);
            var lblRoleName = new kony.ui.Label({
                "height": "16px",
                "id": "lblRoleName",
                "isVisible": true,
                "left": "0px",
                "skin": "sknlblLato696c7311px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmAdManagement.SegmentNameCAPS\")",
                "top": "0px",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var fontIconRoleName = new kony.ui.Label({
                "id": "fontIconRoleName",
                "isVisible": true,
                "left": "5px",
                "skin": "sknIcon15px",
                "text": "",
                "top": "0px",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxRoleNameHeader.add(lblRoleName, fontIconRoleName);
            var flxDescriptionHeader = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                "clipBounds": true,
                "id": "flxDescriptionHeader",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "24%",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "21px",
                "width": "90dp"
            }, {}, {});
            flxDescriptionHeader.setDefaultUnit(kony.flex.DP);
            var lblDescriptionCustomerRoles = new kony.ui.Label({
                "height": "16px",
                "id": "lblDescriptionCustomerRoles",
                "isVisible": true,
                "left": "0px",
                "skin": "sknlblLato696c7311px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmAdManagement.DescriptionCAPS\")",
                "top": "0px",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxDescriptionHeader.add(lblDescriptionCustomerRoles);
            var flxUsersHeader = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                "clipBounds": true,
                "id": "flxUsersHeader",
                "isVisible": true,
                "layoutType": kony.flex.FLOW_HORIZONTAL,
                "left": "66%",
                "isModalContainer": false,
                "skin": "hoverhandSkin",
                "top": "21px",
                "width": "120px"
            }, {}, {});
            flxUsersHeader.setDefaultUnit(kony.flex.DP);
            var lblUsersHeader = new kony.ui.Label({
                "height": "16px",
                "id": "lblUsersHeader",
                "isVisible": true,
                "left": "0px",
                "skin": "sknlblLato696c7311px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmAdManagement.UsersAvaialbleCAPS\")",
                "top": "0px",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var fontIconSortUsers = new kony.ui.Label({
                "id": "fontIconSortUsers",
                "isVisible": true,
                "left": "5px",
                "skin": "sknIcon15px",
                "text": "",
                "top": "0px",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxUsersHeader.add(lblUsersHeader, fontIconSortUsers);
            var flxAttributesHeader = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                "clipBounds": true,
                "id": "flxAttributesHeader",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "84%",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "21px",
                "width": "80px",
                "zIndex": 1
            }, {}, {});
            flxAttributesHeader.setDefaultUnit(kony.flex.DP);
            var lblAttributes = new kony.ui.Label({
                "id": "lblAttributes",
                "isVisible": true,
                "left": "0px",
                "skin": "sknlblLato696c7311px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmAdManagement.AttributesCAPS\")",
                "top": "0px",
                "width": "100%",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxAttributesHeader.add(lblAttributes);
            flxSegmentHeader.add(flxRoleNameHeader, flxDescriptionHeader, flxUsersHeader, flxAttributesHeader);
            var flxSeperator = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "1dp",
                "id": "flxSeperator",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0dp",
                "width": "100%"
            }, {}, {});
            flxSeperator.setDefaultUnit(kony.flex.DP);
            var lblCustomerRoleSeparator = new kony.ui.Label({
                "bottom": "0px",
                "height": "1px",
                "id": "lblCustomerRoleSeparator",
                "isVisible": true,
                "left": "20dp",
                "right": "20dp",
                "skin": "sknLblTableHeaderLine",
                "text": "-",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxSeperator.add(lblCustomerRoleSeparator);
            var flxTargetCustSegData = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                "centerX": "50%",
                "clipBounds": true,
                "id": "flxTargetCustSegData",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "20px",
                "isModalContainer": false,
                "right": "20px",
                "skin": "slFbox",
                "top": "0px",
                "zIndex": 1
            }, {}, {});
            flxTargetCustSegData.setDefaultUnit(kony.flex.DP);
            var segTargetCustomerRoles = new kony.ui.SegmentedUI2({
                "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                "data": [{
                    "lblAttributes": "View",
                    "lblDeleteIcon": "",
                    "lblRoleDescription": "All customers list with DBX platinum  card holders, Includes new addition of 2018 -2019.",
                    "lblRoleName": "Platinum DBX Customers",
                    "lblSeparator": "Label",
                    "lblUserCount": "10283"
                }],
                "groupCells": false,
                "id": "segTargetCustomerRoles",
                "isVisible": true,
                "left": "0px",
                "needPageIndicator": true,
                "pageOffDotImage": "pageoffdot.png",
                "pageOnDotImage": "pageondot.png",
                "retainSelection": false,
                "rowFocusSkin": "seg2Focus",
                "rowSkin": "seg2Normal",
                "rowTemplate": "flxTargetCustRoles",
                "sectionHeaderSkin": "sliPhoneSegmentHeader",
                "selectionBehavior": constants.SEGUI_DEFAULT_BEHAVIOR,
                "separatorRequired": false,
                "showScrollbars": false,
                "top": "0px",
                "viewType": constants.SEGUI_VIEW_TYPE_TABLEVIEW,
                "widgetDataMap": {
                    "flxAttributesContainer": "flxAttributesContainer",
                    "flxIconDelete": "flxIconDelete",
                    "flxTargetCustRoles": "flxTargetCustRoles",
                    "lblAttributes": "lblAttributes",
                    "lblDeleteIcon": "lblDeleteIcon",
                    "lblRoleDescription": "lblRoleDescription",
                    "lblRoleName": "lblRoleName",
                    "lblSeparator": "lblSeparator",
                    "lblUserCount": "lblUserCount"
                },
                "width": "100%",
                "zIndex": 1
            }, {
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxTargetCustSegData.add(segTargetCustomerRoles);
            flxTargetDatasets.add(flxSegmentHeader, flxSeperator, flxTargetCustSegData);
            var flxEventsData = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                "clipBounds": true,
                "id": "flxEventsData",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0dp",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {}, {});
            flxEventsData.setDefaultUnit(kony.flex.DP);
            var flxViewEventsInner = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                "clipBounds": true,
                "id": "flxViewEventsInner",
                "isVisible": true,
                "layoutType": kony.flex.FLOW_VERTICAL,
                "left": "20px",
                "isModalContainer": false,
                "right": "20px",
                "skin": "slFbox",
                "top": "0dp"
            }, {}, {});
            flxViewEventsInner.setDefaultUnit(kony.flex.DP);
            var viewPopupEvents = new com.adminConsole.adManagement.viewPopupEvents({
                "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                "id": "viewPopupEvents",
                "isVisible": true,
                "layoutType": kony.flex.FLOW_VERTICAL,
                "left": "0dp",
                "masterType": constants.MASTER_TYPE_DEFAULT,
                "isModalContainer": false,
                "skin": "sknFlxbgFFFFFFbdrD7D9E0rd4px",
                "top": "20dp",
                "width": "100%",
                "overrides": {
                    "btnAddEvents": {
                        "isVisible": false
                    },
                    "flxEventsSegHeader": {
                        "isVisible": true
                    },
                    "viewPopupEvents": {
                        "bottom": "viz.val_cleared",
                        "isVisible": true,
                        "top": "20dp"
                    }
                }
            }, {
                "overrides": {}
            }, {
                "overrides": {}
            });
            var viewInternalEvents = new com.adminConsole.adManagement.viewInternalExternalEvents({
                "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                "id": "viewInternalEvents",
                "isVisible": true,
                "layoutType": kony.flex.FLOW_VERTICAL,
                "left": "0dp",
                "masterType": constants.MASTER_TYPE_DEFAULT,
                "isModalContainer": false,
                "skin": "sknFlxbgFFFFFFbdrD7D9E0rd4px",
                "top": "10dp",
                "width": "100%",
                "overrides": {
                    "btnAddEvents": {
                        "isVisible": false
                    },
                    "viewInternalExternalEvents": {
                        "top": "10dp"
                    }
                }
            }, {
                "overrides": {}
            }, {
                "overrides": {}
            });
            var viewExternalEvents = new com.adminConsole.adManagement.viewInternalExternalEvents({
                "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                "bottom": "20dp",
                "id": "viewExternalEvents",
                "isVisible": true,
                "layoutType": kony.flex.FLOW_VERTICAL,
                "left": "0dp",
                "masterType": constants.MASTER_TYPE_DEFAULT,
                "isModalContainer": false,
                "skin": "sknFlxbgFFFFFFbdrD7D9E0rd4px",
                "top": "10dp",
                "width": "100%",
                "overrides": {
                    "btnAddEvents": {
                        "isVisible": false
                    },
                    "lblEventsHeader": {
                        "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmAdManagement.externalEvents\")"
                    },
                    "viewInternalExternalEvents": {
                        "bottom": "20dp",
                        "top": "10dp"
                    }
                }
            }, {
                "overrides": {}
            }, {
                "overrides": {}
            });
            flxViewEventsInner.add(viewPopupEvents, viewInternalEvents, viewExternalEvents);
            var campaignDetailsToolTip = new com.adminConsole.Customers.ToolTip({
                "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                "id": "campaignDetailsToolTip",
                "isVisible": false,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "masterType": constants.MASTER_TYPE_DEFAULT,
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "50dp",
                "width": "220px",
                "overrides": {
                    "ToolTip": {
                        "isVisible": false,
                        "top": "50dp",
                        "width": "220px"
                    },
                    "flxToolTipMessage": {
                        "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                        "width": "100%"
                    },
                    "lblNoConcentToolTip": {
                        "bottom": "5px",
                        "height": kony.flex.USE_PREFFERED_SIZE,
                        "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmAdManagement.popupEventsInfo\")",
                        "left": "10px",
                        "top": "5px",
                        "width": "210px"
                    },
                    "lblarrow": {
                        "centerX": "viz.val_cleared",
                        "left": "10px"
                    }
                }
            }, {
                "overrides": {}
            }, {
                "overrides": {}
            });
            flxEventsData.add(flxViewEventsInner, campaignDetailsToolTip);
            flxTargetCustomerRolesData.add(flxTab2Wrapper, lblNoAd, flxTargetDatasets, flxEventsData);
            var flxInAppPopupSection = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                "centerX": "50%",
                "clipBounds": true,
                "id": "flxInAppPopupSection",
                "isVisible": true,
                "layoutType": kony.flex.FLOW_VERTICAL,
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "10px",
                "width": "100%",
                "zIndex": 4
            }, {}, {});
            flxInAppPopupSection.setDefaultUnit(kony.flex.DP);
            var flxPopUpTabWrapper = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "55dp",
                "id": "flxPopUpTabWrapper",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "sknf8f9fabordere1e5ed",
                "top": "0dp",
                "width": "100%",
                "zIndex": 10
            }, {}, {});
            flxPopUpTabWrapper.setDefaultUnit(kony.flex.DP);
            var popupChannelWrapper = new com.adminConsole.common.tabs({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "height": "100%",
                "id": "popupChannelWrapper",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "masterType": constants.MASTER_TYPE_DEFAULT,
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0dp",
                "width": "100%",
                "overrides": {
                    "btnTab1": {
                        "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmAdManagement.WebCAPS\")"
                    },
                    "btnTab2": {
                        "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmReportsManagement.lblMobile\")"
                    }
                }
            }, {
                "overrides": {}
            }, {
                "overrides": {}
            });
            flxPopUpTabWrapper.add(popupChannelWrapper);
            var flxInAppPopupContent = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                "clipBounds": true,
                "id": "flxInAppPopupContent",
                "isVisible": true,
                "layoutType": kony.flex.FLOW_VERTICAL,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0dp",
                "width": "100%"
            }, {}, {});
            flxInAppPopupContent.setDefaultUnit(kony.flex.DP);
            flxInAppPopupContent.add();
            flxInAppPopupSection.add(flxPopUpTabWrapper, flxInAppPopupContent);
            var flxOfflineChannelsData = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                "clipBounds": true,
                "id": "flxOfflineChannelsData",
                "isVisible": false,
                "layoutType": kony.flex.FLOW_VERTICAL,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "10dp",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 4
            }, {}, {});
            flxOfflineChannelsData.setDefaultUnit(kony.flex.DP);
            flxOfflineChannelsData.add();
            flxBottomDetails.add(flxViewTabs, flxSeparator, flxChannelsDataFlowVertical, flxTargetCustomerRolesData, flxInAppPopupSection, flxOfflineChannelsData);
            var flxBottomSpace = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "20px",
                "id": "flxBottomSpace",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0px",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0px",
                "width": "99%",
                "zIndex": 4
            }, {}, {});
            flxBottomSpace.setDefaultUnit(kony.flex.DP);
            flxBottomSpace.add();
            flxDetailsFlowVertical.add(flxTopDetails, flxBottomDetails, flxBottomSpace);
            flxScrollViewDetails.add(flxDetailsFlowVertical);
            flxViewDetails.add(flxScrollViewDetails);
            var flxLoading = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "100%",
                "id": "flxLoading",
                "isVisible": false,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "sknLoadingBlur",
                "top": "0dp",
                "width": "100%",
                "zIndex": 200
            }, {}, {});
            flxLoading.setDefaultUnit(kony.flex.DP);
            var flxImageContainer = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "centerX": "50%",
                "centerY": "50%",
                "clipBounds": true,
                "height": "75px",
                "id": "flxImageContainer",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "isModalContainer": false,
                "skin": "slFbox",
                "width": "75px",
                "zIndex": 1
            }, {}, {});
            flxImageContainer.setDefaultUnit(kony.flex.DP);
            var imgLoading = new kony.ui.Image2({
                "centerX": "50%",
                "centerY": "50%",
                "height": "50px",
                "id": "imgLoading",
                "isVisible": true,
                "skin": "slImage",
                "src": "loadingscreenimage.gif",
                "width": "50px",
                "zIndex": 1
            }, {
                "imageScaleMode": constants.IMAGE_SCALE_MODE_MAINTAIN_ASPECT_RATIO,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxImageContainer.add(imgLoading);
            flxLoading.add(flxImageContainer);
            var flxToastMessage = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "bottom": "0px",
                "centerX": "50%",
                "clipBounds": true,
                "height": "70px",
                "id": "flxToastMessage",
                "isVisible": false,
                "layoutType": kony.flex.FREE_FORM,
                "isModalContainer": false,
                "skin": "slFbox",
                "width": "1413px",
                "zIndex": 1
            }, {}, {});
            flxToastMessage.setDefaultUnit(kony.flex.DP);
            var flxToastContainer = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "centerX": "50%",
                "centerY": "50%",
                "clipBounds": true,
                "height": "45px",
                "id": "flxToastContainer",
                "isVisible": false,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0%",
                "isModalContainer": false,
                "skin": "sknflxSuccessToast1F844D",
                "top": "0%",
                "width": "60%",
                "zIndex": 1
            }, {}, {});
            flxToastContainer.setDefaultUnit(kony.flex.DP);
            var imgLeft = new kony.ui.Image2({
                "centerY": "50%",
                "height": "15dp",
                "id": "imgLeft",
                "isVisible": true,
                "left": "15px",
                "skin": "slImage",
                "src": "arrow2x.png",
                "top": "0dp",
                "width": "15dp",
                "zIndex": 1
            }, {
                "imageScaleMode": constants.IMAGE_SCALE_MODE_MAINTAIN_ASPECT_RATIO,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var lbltoastMessage = new kony.ui.Label({
                "centerY": "50%",
                "id": "lbltoastMessage",
                "isVisible": true,
                "left": "45px",
                "right": "45px",
                "skin": "lblfffffflatoregular14px",
                "text": "Label",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var imgRight = new kony.ui.Image2({
                "centerY": "50%",
                "height": "15px",
                "id": "imgRight",
                "isVisible": true,
                "right": "15px",
                "skin": "slImage",
                "src": "close_small2x.png",
                "top": "0dp",
                "width": "15px",
                "zIndex": 1
            }, {
                "imageScaleMode": constants.IMAGE_SCALE_MODE_MAINTAIN_ASPECT_RATIO,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxToastContainer.add(imgLeft, lbltoastMessage, imgRight);
            var toastMessage = new com.adminConsole.common.toastMessage({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "id": "toastMessage",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "masterType": constants.MASTER_TYPE_DEFAULT,
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0dp",
                "overrides": {
                    "lbltoastMessage": {
                        "text": "label"
                    },
                    "toastMessage": {
                        "isVisible": true
                    }
                }
            }, {
                "overrides": {}
            }, {
                "overrides": {}
            });
            flxToastMessage.add(flxToastContainer, toastMessage);
            var flxSelectOptions = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                "clipBounds": true,
                "id": "flxSelectOptions",
                "isVisible": false,
                "layoutType": kony.flex.FREE_FORM,
                "isModalContainer": false,
                "right": "50dp",
                "skin": "slFbox",
                "top": "325dp",
                "width": "165dp",
                "zIndex": 1
            }, {}, {});
            flxSelectOptions.setDefaultUnit(kony.flex.DP);
            var selectOptions = new com.adminConsole.adManagement.selectOptions({
                "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                "id": "selectOptions",
                "isVisible": true,
                "layoutType": kony.flex.FLOW_VERTICAL,
                "left": "0dp",
                "masterType": constants.MASTER_TYPE_DEFAULT,
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0dp",
                "width": "100%",
                "overrides": {
                    "imgDownArrow": {
                        "src": "downarrow_2x.png"
                    },
                    "imgUpArrow": {
                        "src": "uparrow_2x.png"
                    }
                }
            }, {
                "overrides": {}
            }, {
                "overrides": {}
            });
            flxSelectOptions.add(selectOptions);
            var settingsMenu = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                "clipBounds": true,
                "id": "settingsMenu",
                "isVisible": false,
                "layoutType": kony.flex.FREE_FORM,
                "isModalContainer": false,
                "right": "40dp",
                "skin": "slFbox",
                "top": "85dp",
                "width": "190dp",
                "zIndex": 1
            }, {}, {});
            settingsMenu.setDefaultUnit(kony.flex.DP);
            var settingsMenuOptions = new com.adminConsole.adManagement.selectOptions({
                "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                "id": "settingsMenuOptions",
                "isVisible": true,
                "layoutType": kony.flex.FLOW_VERTICAL,
                "left": "0dp",
                "masterType": constants.MASTER_TYPE_DEFAULT,
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0dp",
                "width": "100%",
                "overrides": {
                    "flxDownArrowImage": {
                        "isVisible": false
                    },
                    "flxOption2": {
                        "isVisible": false
                    },
                    "flxOption3": {
                        "isVisible": false
                    },
                    "flxOption4": {
                        "isVisible": false
                    },
                    "fontIconOption1": {
                        "text": ""
                    },
                    "fontIconOption2": {
                        "text": ""
                    },
                    "imgDownArrow": {
                        "src": "downarrow_2x.png"
                    },
                    "imgUpArrow": {
                        "src": "uparrow_2x.png"
                    },
                    "lblOption1": {
                        "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmAdManagement.DefaultAdCampaign\")",
                        "width": kony.flex.USE_PREFFERED_SIZE
                    },
                    "lblOption2": {
                        "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmAdManagement.Settings\")"
                    },
                    "lblSeperator": {
                        "isVisible": false
                    }
                }
            }, {
                "overrides": {}
            }, {
                "overrides": {}
            });
            settingsMenu.add(settingsMenuOptions);
            flxRightPannel.add(flxHeaderDropdown, flxMainHeader, flxBreadCrumbs, flxListing, flxViewDetails, flxLoading, flxToastMessage, flxSelectOptions, settingsMenu);
            var flxAdManagementPopUp = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "100%",
                "id": "flxAdManagementPopUp",
                "isVisible": false,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "masterType": constants.MASTER_TYPE_USERWIDGET,
                "isModalContainer": false,
                "skin": "flxPopupTrans0ba858b5d5d304e",
                "top": "0dp",
                "width": "100%",
                "zIndex": 10
            }, {}, {});
            flxAdManagementPopUp.setDefaultUnit(kony.flex.DP);
            var popUp = new com.adminConsole.common.popUp({
                "height": "100%",
                "id": "popUp",
                "isVisible": false,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "masterType": constants.MASTER_TYPE_DEFAULT,
                "isModalContainer": false,
                "skin": "sknflxBg000000Op50",
                "top": "0dp",
                "width": "100%",
                "zIndex": 10,
                "overrides": {
                    "btnPopUpCancel": {
                        "i18n_text": "kony.i18n.getLocalizedString(\"i18n.PopUp.NoLeaveAsIS\")",
                        "minWidth": "80px",
                        "right": "20px"
                    },
                    "btnPopUpDelete": {
                        "i18n_text": "kony.i18n.getLocalizedString(\"i18n.PopUp.YesDeactivate\")",
                        "minWidth": "85px"
                    },
                    "popUp": {
                        "isVisible": false,
                        "zIndex": 10
                    }
                }
            }, {
                "overrides": {}
            }, {
                "overrides": {}
            });
            var postLoginSettingspopup = new com.adminConsole.adManagement.postLoginSettingspopup({
                "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                "centerX": "50%",
                "id": "postLoginSettingspopup",
                "isVisible": false,
                "layoutType": kony.flex.FLOW_VERTICAL,
                "masterType": constants.MASTER_TYPE_DEFAULT,
                "isModalContainer": false,
                "skin": "slFbox0j9f841cc563e4e",
                "top": "250px",
                "width": "550px",
                "zIndex": 1,
                "overrides": {
                    "postLoginSettingspopup": {
                        "isVisible": false,
                        "zIndex": 1
                    }
                }
            }, {
                "overrides": {}
            }, {
                "overrides": {}
            });
            flxAdManagementPopUp.add(popUp, postLoginSettingspopup);
            var flxViewAdPreview = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "centerX": "50%",
                "centerY": "50%",
                "clipBounds": true,
                "height": "100%",
                "id": "flxViewAdPreview",
                "isVisible": false,
                "layoutType": kony.flex.FREE_FORM,
                "isModalContainer": false,
                "skin": "sknFlxBg000000op40",
                "width": "100%",
                "zIndex": 5
            }, {}, {});
            flxViewAdPreview.setDefaultUnit(kony.flex.DP);
            var flxDisplayImage = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "bottom": "5.25%",
                "clipBounds": true,
                "id": "flxDisplayImage",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "7.32%",
                "isModalContainer": false,
                "right": "7.32%",
                "skin": "sknBackgroundFFFFFF",
                "top": "5.25%"
            }, {}, {});
            flxDisplayImage.setDefaultUnit(kony.flex.DP);
            var flxTopBar = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "10px",
                "id": "flxTopBar",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0px",
                "isModalContainer": false,
                "skin": "sknFlxInfoToastBg357C9ENoRadius",
                "top": "0px",
                "width": "100%",
                "zIndex": 1
            }, {}, {});
            flxTopBar.setDefaultUnit(kony.flex.DP);
            flxTopBar.add();
            var flxChannelInfo = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "186px",
                "id": "flxChannelInfo",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0px",
                "isModalContainer": false,
                "right": "0px",
                "skin": "slFbox",
                "top": "10px",
                "width": "100%",
                "zIndex": 1
            }, {}, {});
            flxChannelInfo.setDefaultUnit(kony.flex.DP);
            var flxPreviewMode = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "bottom": "73px",
                "clipBounds": true,
                "height": "50px",
                "id": "flxPreviewMode",
                "isVisible": true,
                "layoutType": kony.flex.FLOW_HORIZONTAL,
                "left": "18px",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0px",
                "width": "101px",
                "zIndex": 1
            }, {}, {});
            flxPreviewMode.setDefaultUnit(kony.flex.DP);
            var lblPreviewMode = new kony.ui.Label({
                "id": "lblPreviewMode",
                "isVisible": true,
                "left": "0px",
                "skin": "sknLbl485c75LatoBold13px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmAdManagement.previewMode\")",
                "top": "24px",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxPreviewMode.add(lblPreviewMode);
            var flxScreenInfo = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "37px",
                "id": "flxScreenInfo",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "20px",
                "isModalContainer": false,
                "right": "20px",
                "skin": "slFbox",
                "top": "68px",
                "zIndex": 1
            }, {}, {});
            flxScreenInfo.setDefaultUnit(kony.flex.DP);
            var flxScreenName = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "100%",
                "id": "flxScreenName",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0px",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0px",
                "width": "350px"
            }, {}, {});
            flxScreenName.setDefaultUnit(kony.flex.DP);
            var lblMoonIconScreen = new kony.ui.Label({
                "bottom": "2px",
                "height": "32px",
                "id": "lblMoonIconScreen",
                "isVisible": true,
                "left": "0px",
                "skin": "lblIconMoonb5b5b532px",
                "text": "",
                "top": "2px",
                "width": "32px",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var lblScreenNameHeader = new kony.ui.Label({
                "id": "lblScreenNameHeader",
                "isVisible": true,
                "left": "49px",
                "skin": "sknlblLato11px696C73",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmAdManagement.DisplayPageNameCAPS\")",
                "top": "0px",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var lblScreenName = new kony.ui.Label({
                "bottom": "0px",
                "id": "lblScreenName",
                "isVisible": true,
                "left": "49px",
                "skin": "sknLbl16pxLato192B45",
                "text": "Post Login Full Screen Interstitial Ad",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxScreenName.add(lblMoonIconScreen, lblScreenNameHeader, lblScreenName);
            var flxChannelName = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "100%",
                "id": "flxChannelName",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "350px",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0px",
                "width": "300px"
            }, {}, {});
            flxChannelName.setDefaultUnit(kony.flex.DP);
            var flxVerticalSeparator = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "centerY": "50%",
                "clipBounds": true,
                "height": "33px",
                "id": "flxVerticalSeparator",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0px",
                "isModalContainer": false,
                "skin": "sknflxD5D9DD",
                "width": "1px",
                "zIndex": 1
            }, {}, {});
            flxVerticalSeparator.setDefaultUnit(kony.flex.DP);
            flxVerticalSeparator.add();
            var lblMoonIconChannel = new kony.ui.Label({
                "bottom": "2px",
                "height": "32px",
                "id": "lblMoonIconChannel",
                "isVisible": true,
                "left": "61px",
                "skin": "lblIconMoonb5b5b532px",
                "text": "",
                "top": "2px",
                "width": "32px",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var lblChannelNameHeader = new kony.ui.Label({
                "id": "lblChannelNameHeader",
                "isVisible": true,
                "left": "110px",
                "skin": "sknlblLato11px696C73",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmAdManagement.ChannelCAPS\")",
                "top": "0px",
                "width": "53px",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var lblChannelName = new kony.ui.Label({
                "bottom": "0px",
                "id": "lblChannelName",
                "isVisible": true,
                "left": "110px",
                "skin": "sknLbl16pxLato192B45",
                "text": "Native Mobile App",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxChannelName.add(flxVerticalSeparator, lblMoonIconChannel, lblChannelNameHeader, lblChannelName);
            flxScreenInfo.add(flxScreenName, flxChannelName);
            var flxClose = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "20px",
                "id": "flxClose",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "isModalContainer": false,
                "right": "20px",
                "skin": "sknFlxffffffCursorPointer",
                "top": "10px",
                "width": "20px",
                "zIndex": 1
            }, {}, {});
            flxClose.setDefaultUnit(kony.flex.DP);
            var lblClose = new kony.ui.Label({
                "height": "100%",
                "id": "lblClose",
                "isVisible": true,
                "left": "0px",
                "skin": "lblIconGrey",
                "text": "",
                "top": "0dp",
                "width": "100%",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_CENTER,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxClose.add(lblClose);
            var flxAdSeparator = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "bottom": "0px",
                "clipBounds": true,
                "height": "1px",
                "id": "flxAdSeparator",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "20px",
                "isModalContainer": false,
                "right": "20px",
                "skin": "sknFlxD7D9E0Separator",
                "top": "116px",
                "zIndex": 1
            }, {}, {});
            flxAdSeparator.setDefaultUnit(kony.flex.DP);
            flxAdSeparator.add();
            var flxMessage = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "40px",
                "id": "flxMessage",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "20px",
                "isModalContainer": false,
                "right": "20px",
                "skin": "sknFlx1FB44DBorder2px",
                "top": "137px",
                "zIndex": 1
            }, {}, {});
            flxMessage.setDefaultUnit(kony.flex.DP);
            var flxMessageColor = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "40px",
                "id": "flxMessageColor",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "-2px",
                "isModalContainer": false,
                "skin": "sknFlx1FB44DBackground",
                "top": "-2px",
                "width": "40px",
                "zIndex": 1
            }, {}, {});
            flxMessageColor.setDefaultUnit(kony.flex.DP);
            var lblStatus = new kony.ui.Label({
                "centerX": "50%",
                "centerY": "50%",
                "height": "19px",
                "id": "lblStatus",
                "isVisible": true,
                "skin": "sknFontIcontoastSuccess",
                "text": "",
                "width": "19px",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxMessageColor.add(lblStatus);
            var lblMessage = new kony.ui.Label({
                "height": "16px",
                "id": "lblMessage",
                "isVisible": true,
                "left": "53px",
                "skin": "sknLblLato485c7513px",
                "text": "Uploaded image matches the container",
                "top": "9px",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxMessage.add(flxMessageColor, lblMessage);
            flxChannelInfo.add(flxPreviewMode, flxScreenInfo, flxClose, flxAdSeparator, flxMessage);
            var flxShowImageParent = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "bottom": "0px",
                "clipBounds": true,
                "id": "flxShowImageParent",
                "isVisible": true,
                "layoutType": kony.flex.FLOW_VERTICAL,
                "left": "0px",
                "isModalContainer": false,
                "right": "0px",
                "skin": "slFbox",
                "top": "200px",
                "width": "100%"
            }, {}, {});
            flxShowImageParent.setDefaultUnit(kony.flex.DP);
            var flxScrollImageDisplay = new kony.ui.FlexScrollContainer({
                "allowHorizontalBounce": false,
                "allowVerticalBounce": true,
                "bottom": "0px",
                "bounces": true,
                "clipBounds": true,
                "enableScrolling": true,
                "height": "100%",
                "horizontalScrollIndicator": true,
                "id": "flxScrollImageDisplay",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0px",
                "pagingEnabled": false,
                "right": "10px",
                "scrollDirection": kony.flex.SCROLL_BOTH,
                "skin": "slFSbox",
                "top": "0px",
                "verticalScrollIndicator": true,
                "width": "100%",
                "zIndex": 1
            }, {}, {});
            flxScrollImageDisplay.setDefaultUnit(kony.flex.DP);
            var flxViewImageFlowVertical = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                "clipBounds": true,
                "id": "flxViewImageFlowVertical",
                "isVisible": true,
                "layoutType": kony.flex.FLOW_VERTICAL,
                "left": "0px",
                "isModalContainer": false,
                "right": "15px",
                "skin": "slFbox",
                "top": "0px"
            }, {}, {});
            flxViewImageFlowVertical.setDefaultUnit(kony.flex.DP);
            var flxUploadedImageParent = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                "clipBounds": true,
                "id": "flxUploadedImageParent",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0px",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0px",
                "width": "100%"
            }, {}, {});
            flxUploadedImageParent.setDefaultUnit(kony.flex.DP);
            var flxResolution = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "30px",
                "id": "flxResolution",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "20px",
                "isModalContainer": false,
                "right": "0px",
                "skin": "slFbox",
                "top": "0px",
                "zIndex": 2
            }, {}, {});
            flxResolution.setDefaultUnit(kony.flex.DP);
            var flxResolutionDisplay = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "100%",
                "id": "flxResolutionDisplay",
                "isVisible": true,
                "layoutType": kony.flex.FLOW_HORIZONTAL,
                "left": "0px",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0px",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {}, {});
            flxResolutionDisplay.setDefaultUnit(kony.flex.DP);
            var lblTargetResolution = new kony.ui.Label({
                "centerY": "50%",
                "id": "lblTargetResolution",
                "isVisible": true,
                "left": "0px",
                "skin": "sknLbl696c73LatoBold11px",
                "text": "Target Container Resolution : 1125*318 | ",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var lblUploadedResolution = new kony.ui.Label({
                "centerY": "50%",
                "id": "lblUploadedResolution",
                "isVisible": true,
                "left": "2px",
                "skin": "sknlblLato11px696C73",
                "text": "Uploaded image resolution : 1024*318",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxResolutionDisplay.add(lblTargetResolution, lblUploadedResolution);
            flxResolution.add(flxResolutionDisplay);
            var flxBorder = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "bottom": "20px",
                "clipBounds": true,
                "id": "flxBorder",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "20px",
                "isModalContainer": false,
                "right": "20px",
                "skin": "sknFlxBorder6px000000",
                "top": "30px",
                "zIndex": 2
            }, {}, {});
            flxBorder.setDefaultUnit(kony.flex.DP);
            flxBorder.add();
            var flxImage = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "bottom": "20px",
                "clipBounds": true,
                "id": "flxImage",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "20px",
                "isModalContainer": false,
                "right": "20px",
                "skin": "slFbox",
                "top": "10px",
                "zIndex": 1
            }, {}, {});
            flxImage.setDefaultUnit(kony.flex.DP);
            var flxContainerResolution = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                "clipBounds": true,
                "id": "flxContainerResolution",
                "isVisible": true,
                "layoutType": kony.flex.FLOW_VERTICAL,
                "left": "0px",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "20px",
                "width": "100%"
            }, {}, {});
            flxContainerResolution.setDefaultUnit(kony.flex.DP);
            var richTextAdImage = new kony.ui.RichText({
                "id": "richTextAdImage",
                "isVisible": true,
                "left": "0px",
                "linkSkin": "defRichTextLink",
                "skin": "slRichText",
                "top": "0px",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxContainerResolution.add(richTextAdImage);
            flxImage.add(flxContainerResolution);
            flxUploadedImageParent.add(flxResolution, flxBorder, flxImage);
            var flxAdjustedImageParent = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                "clipBounds": true,
                "id": "flxAdjustedImageParent",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0px",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0px",
                "width": "100%",
                "zIndex": 1
            }, {}, {});
            flxAdjustedImageParent.setDefaultUnit(kony.flex.DP);
            var flxBorder2 = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "bottom": "20px",
                "clipBounds": true,
                "id": "flxBorder2",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "20px",
                "isModalContainer": false,
                "right": "20px",
                "skin": "sknFlxBorder6px000000",
                "top": "40px",
                "zIndex": 2
            }, {}, {});
            flxBorder2.setDefaultUnit(kony.flex.DP);
            flxBorder2.add();
            var flxImage2 = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "bottom": "20px",
                "clipBounds": true,
                "id": "flxImage2",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "20px",
                "isModalContainer": false,
                "right": "20px",
                "skin": "slFbox",
                "top": "10px",
                "zIndex": 1
            }, {}, {});
            flxImage2.setDefaultUnit(kony.flex.DP);
            var flxContainerResolution2 = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                "clipBounds": true,
                "id": "flxContainerResolution2",
                "isVisible": true,
                "layoutType": kony.flex.FLOW_VERTICAL,
                "left": "0px",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0px",
                "width": kony.flex.USE_PREFFERED_SIZE
            }, {}, {});
            flxContainerResolution2.setDefaultUnit(kony.flex.DP);
            var flxResolution2 = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "30px",
                "id": "flxResolution2",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0px",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0px",
                "width": "100%",
                "zIndex": 2
            }, {}, {});
            flxResolution2.setDefaultUnit(kony.flex.DP);
            var flxResolutionDisplay2 = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "100%",
                "id": "flxResolutionDisplay2",
                "isVisible": true,
                "layoutType": kony.flex.FLOW_HORIZONTAL,
                "left": "0px",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0px",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {}, {});
            flxResolutionDisplay2.setDefaultUnit(kony.flex.DP);
            var lblAdjustedToContainer = new kony.ui.Label({
                "centerY": "50%",
                "id": "lblAdjustedToContainer",
                "isVisible": true,
                "left": "0px",
                "skin": "sknlblLato11px696C73",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmAdManagement.ImageAdjustedToFitTheContainer\")",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxResolutionDisplay2.add(lblAdjustedToContainer);
            flxResolution2.add(flxResolutionDisplay2);
            var richTextAdImage2 = new kony.ui.RichText({
                "id": "richTextAdImage2",
                "isVisible": true,
                "left": "0px",
                "linkSkin": "defRichTextLink",
                "skin": "slRichText",
                "top": "0px",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxContainerResolution2.add(flxResolution2, richTextAdImage2);
            flxImage2.add(flxContainerResolution2);
            flxAdjustedImageParent.add(flxBorder2, flxImage2);
            flxViewImageFlowVertical.add(flxUploadedImageParent, flxAdjustedImageParent);
            flxScrollImageDisplay.add(flxViewImageFlowVertical);
            flxShowImageParent.add(flxScrollImageDisplay);
            flxDisplayImage.add(flxTopBar, flxChannelInfo, flxShowImageParent);
            flxViewAdPreview.add(flxDisplayImage);
            var flxViewAttributeList = new kony.ui.FlexContainer({
                "centerX": "50%",
                "centerY": "50%",
                "clipBounds": true,
                "height": "100%",
                "id": "flxViewAttributeList",
                "isVisible": false,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "skn222b35",
                "top": "0dp",
                "width": "100%",
                "zIndex": 20
            }, {}, {});
            flxViewAttributeList.setDefaultUnit(kony.flex.DP);
            var flxViewAttributeListWindow = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "centerX": "50%",
                "centerY": "50%",
                "clipBounds": true,
                "height": "80%",
                "id": "flxViewAttributeListWindow",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "isModalContainer": false,
                "skin": "sknBackgroundFFFFFF",
                "width": "60%",
                "zIndex": 200
            }, {}, {});
            flxViewAttributeListWindow.setDefaultUnit(kony.flex.DP);
            var flxAttributeListTopDetails = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "78px",
                "id": "flxAttributeListTopDetails",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0px",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0px",
                "width": "100%"
            }, {}, {});
            flxAttributeListTopDetails.setDefaultUnit(kony.flex.DP);
            var flxTopScreenBar = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "10px",
                "id": "flxTopScreenBar",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0%",
                "isModalContainer": false,
                "skin": "sknFlxInfoToastBg357C9ENoRadius",
                "top": "0px",
                "width": "100%",
                "zIndex": 1
            }, {}, {});
            flxTopScreenBar.setDefaultUnit(kony.flex.DP);
            flxTopScreenBar.add();
            var flxCustomerRoleName = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "19px",
                "id": "flxCustomerRoleName",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "22px",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "37px",
                "width": "90%",
                "zIndex": 1
            }, {}, {});
            flxCustomerRoleName.setDefaultUnit(kony.flex.DP);
            var lblCustomerRoleName = new kony.ui.Label({
                "id": "lblCustomerRoleName",
                "isVisible": true,
                "left": "0px",
                "skin": "sknLbl16pxLato192B45",
                "text": "Platinum DBX Users Attribute(s) List",
                "top": "0px",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxCustomerRoleName.add(lblCustomerRoleName);
            var flxCloseAttributeList = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "20px",
                "id": "flxCloseAttributeList",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "isModalContainer": false,
                "right": "16px",
                "skin": "sknFlxffffffCursorPointer",
                "top": "30px",
                "width": "20px",
                "zIndex": 1
            }, {}, {});
            flxCloseAttributeList.setDefaultUnit(kony.flex.DP);
            var lblAttributeClose = new kony.ui.Label({
                "height": "100%",
                "id": "lblAttributeClose",
                "isVisible": true,
                "left": "0px",
                "skin": "lblIconGrey",
                "text": "",
                "top": "0dp",
                "width": "100%",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_CENTER,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxCloseAttributeList.add(lblAttributeClose);
            flxAttributeListTopDetails.add(flxTopScreenBar, flxCustomerRoleName, flxCloseAttributeList);
            var flxAttributeListBottomDetails = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "bottom": "0px",
                "clipBounds": true,
                "id": "flxAttributeListBottomDetails",
                "isVisible": true,
                "layoutType": kony.flex.FLOW_VERTICAL,
                "left": "0px",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "98px",
                "width": "100%",
                "zIndex": 1
            }, {}, {});
            flxAttributeListBottomDetails.setDefaultUnit(kony.flex.DP);
            var flxAttributeDescriptionUsers = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                "clipBounds": true,
                "id": "flxAttributeDescriptionUsers",
                "isVisible": true,
                "layoutType": kony.flex.FLOW_HORIZONTAL,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0dp",
                "width": "100%"
            }, {}, {});
            flxAttributeDescriptionUsers.setDefaultUnit(kony.flex.DP);
            var flxAttributeListDescription = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                "clipBounds": true,
                "id": "flxAttributeListDescription",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0px",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0px",
                "width": "60%",
                "zIndex": 1
            }, {}, {});
            flxAttributeListDescription.setDefaultUnit(kony.flex.DP);
            var lblAttributeDescriptionTag = new kony.ui.Label({
                "height": "13px",
                "id": "lblAttributeDescriptionTag",
                "isVisible": true,
                "left": "20px",
                "skin": "sknlblLato11px696C73",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmAdManagement.DescriptionCAPS\")",
                "top": "0px",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var lblAttributeDescription = new kony.ui.Label({
                "id": "lblAttributeDescription",
                "isVisible": true,
                "left": "20px",
                "right": 20,
                "skin": "sknlbl485C75LatoSemiBold13px",
                "text": "These Micro Business users have the highest level of access to all the functions.",
                "top": "28px",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxAttributeListDescription.add(lblAttributeDescriptionTag, lblAttributeDescription);
            var flxAttributeListUserAvailable = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                "clipBounds": true,
                "id": "flxAttributeListUserAvailable",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0px",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0px",
                "width": "40%",
                "zIndex": 1
            }, {}, {});
            flxAttributeListUserAvailable.setDefaultUnit(kony.flex.DP);
            var lblAttributeUserAvailableTag = new kony.ui.Label({
                "height": "13px",
                "id": "lblAttributeUserAvailableTag",
                "isVisible": true,
                "left": "20px",
                "skin": "sknlblLato11px696C73",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmAdManagement.UsersAvaialbleCAPS\")",
                "top": "0px",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var lblAttributeUserAvailable = new kony.ui.Label({
                "id": "lblAttributeUserAvailable",
                "isVisible": true,
                "left": "20px",
                "right": 20,
                "skin": "sknlbl485C75LatoSemiBold13px",
                "text": "58752",
                "top": "28px",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxAttributeListUserAvailable.add(lblAttributeUserAvailableTag, lblAttributeUserAvailable);
            flxAttributeDescriptionUsers.add(flxAttributeListDescription, flxAttributeListUserAvailable);
            var flxAttributeListSeparator = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "1px",
                "id": "flxAttributeListSeparator",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "19px",
                "isModalContainer": false,
                "skin": "flxe4e6ec1px",
                "top": "30px",
                "width": "95%",
                "zIndex": 1
            }, {}, {});
            flxAttributeListSeparator.setDefaultUnit(kony.flex.DP);
            flxAttributeListSeparator.add();
            var flxAttributesList = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "bottom": "0px",
                "clipBounds": true,
                "height": "80%",
                "id": "flxAttributesList",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "27px",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {}, {});
            flxAttributesList.setDefaultUnit(kony.flex.DP);
            var flxAttrHeaderRow = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "53dp",
                "id": "flxAttrHeaderRow",
                "isVisible": true,
                "layoutType": kony.flex.FLOW_HORIZONTAL,
                "left": "20dp",
                "isModalContainer": false,
                "right": "20dp",
                "skin": "slFbox",
                "top": "0",
                "width": kony.flex.USE_PREFFERED_SIZE
            }, {}, {});
            flxAttrHeaderRow.setDefaultUnit(kony.flex.DP);
            var flxDatasetsHeader = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "100%",
                "id": "flxDatasetsHeader",
                "isVisible": true,
                "layoutType": kony.flex.FLOW_HORIZONTAL,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0dp",
                "width": "24%"
            }, {}, {});
            flxDatasetsHeader.setDefaultUnit(kony.flex.DP);
            var lblDatasetHeader = new kony.ui.Label({
                "id": "lblDatasetHeader",
                "isVisible": true,
                "left": "0",
                "skin": "sknlblLato11px696C73",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmAdManagement.DatasetCAPS\")",
                "top": "21dp",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var lblFilterDataset = new kony.ui.Label({
                "centerY": "50%",
                "id": "lblFilterDataset",
                "isVisible": true,
                "left": "7dp",
                "skin": "sknIcon15px",
                "text": "",
                "top": "21dp",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {
                "hoverSkin": "sknlblCursorFont"
            });
            flxDatasetsHeader.add(lblDatasetHeader, lblFilterDataset);
            var flxAttrHeader = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "100%",
                "id": "flxAttrHeader",
                "isVisible": true,
                "layoutType": kony.flex.FLOW_HORIZONTAL,
                "left": "1%",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0dp",
                "width": "24%"
            }, {}, {});
            flxAttrHeader.setDefaultUnit(kony.flex.DP);
            var lblAttributeHeader = new kony.ui.Label({
                "id": "lblAttributeHeader",
                "isVisible": true,
                "left": "0dp",
                "skin": "sknlblLato11px696C73",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmAdManagement.AttributeCAPS\")",
                "top": "21dp",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var lblSortAttribute = new kony.ui.Label({
                "id": "lblSortAttribute",
                "isVisible": true,
                "left": "7dp",
                "skin": "sknIcon15px",
                "text": "",
                "top": "21dp",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {
                "hoverSkin": "sknlblCursorFont"
            });
            flxAttrHeader.add(lblAttributeHeader, lblSortAttribute);
            var lblAttributeCriteriaHeader = new kony.ui.Label({
                "id": "lblAttributeCriteriaHeader",
                "isVisible": true,
                "left": "1%",
                "skin": "sknlblLato11px696C73",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmAdManagement.CriteriaCAPS\")",
                "top": "21dp",
                "width": "24%",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var lblAttributeValueHeader = new kony.ui.Label({
                "id": "lblAttributeValueHeader",
                "isVisible": true,
                "left": "1%",
                "skin": "sknlblLato11px696C73",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmAdManagement.ValueCAPS\")",
                "top": "21dp",
                "width": "24%",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxAttrHeaderRow.add(flxDatasetsHeader, flxAttrHeader, lblAttributeCriteriaHeader, lblAttributeValueHeader);
            var flxAttributeHeaderSeparator = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "bottom": "0px",
                "clipBounds": true,
                "height": "1px",
                "id": "flxAttributeHeaderSeparator",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "20dp",
                "isModalContainer": false,
                "right": "20dp",
                "skin": "sknFlx1pxBorder696C73",
                "top": 53,
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 2
            }, {}, {});
            flxAttributeHeaderSeparator.setDefaultUnit(kony.flex.DP);
            flxAttributeHeaderSeparator.add();
            var segAttributeList = new kony.ui.SegmentedUI2({
                "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                "bottom": "20dp",
                "data": [{
                    "lblAttribute": "Attribute",
                    "lblCriteria": "Criteria",
                    "lblDataset": "DATASET",
                    "lblValue": "Value"
                }, {
                    "lblAttribute": "Attribute",
                    "lblCriteria": "Criteria",
                    "lblDataset": "DATASET",
                    "lblValue": "Value"
                }, {
                    "lblAttribute": "Attribute",
                    "lblCriteria": "Criteria",
                    "lblDataset": "DATASET",
                    "lblValue": "Value"
                }, {
                    "lblAttribute": "Attribute",
                    "lblCriteria": "Criteria",
                    "lblDataset": "DATASET",
                    "lblValue": "Value"
                }, {
                    "lblAttribute": "Attribute",
                    "lblCriteria": "Criteria",
                    "lblDataset": "DATASET",
                    "lblValue": "Value"
                }, {
                    "lblAttribute": "Attribute",
                    "lblCriteria": "Criteria",
                    "lblDataset": "DATASET",
                    "lblValue": "Value"
                }, {
                    "lblAttribute": "Attribute",
                    "lblCriteria": "Criteria",
                    "lblDataset": "DATASET",
                    "lblValue": "Value"
                }, {
                    "lblAttribute": "Attribute",
                    "lblCriteria": "Criteria",
                    "lblDataset": "DATASET",
                    "lblValue": "Value"
                }, {
                    "lblAttribute": "Attribute",
                    "lblCriteria": "Criteria",
                    "lblDataset": "DATASET",
                    "lblValue": "Value"
                }, {
                    "lblAttribute": "Attribute",
                    "lblCriteria": "Criteria",
                    "lblDataset": "DATASET",
                    "lblValue": "Value"
                }],
                "groupCells": false,
                "id": "segAttributeList",
                "isVisible": true,
                "left": "20dp",
                "needPageIndicator": true,
                "pageOffDotImage": "pageoffdot.png",
                "pageOnDotImage": "pageondot.png",
                "retainSelection": false,
                "right": "20dp",
                "rowFocusSkin": "seg2Focus",
                "rowSkin": "seg2Normal",
                "rowTemplate": "flxAttributeRow",
                "sectionHeaderSkin": "sliPhoneSegmentHeader",
                "selectionBehavior": constants.SEGUI_DEFAULT_BEHAVIOR,
                "separatorColor": "d7d9e000",
                "separatorRequired": true,
                "separatorThickness": 1,
                "showScrollbars": false,
                "top": "54dp",
                "viewType": constants.SEGUI_VIEW_TYPE_TABLEVIEW,
                "widgetDataMap": {
                    "flxAttributeRow": "flxAttributeRow",
                    "flxRow": "flxRow",
                    "lblAttribute": "lblAttribute",
                    "lblCriteria": "lblCriteria",
                    "lblDataset": "lblDataset",
                    "lblValue": "lblValue"
                },
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var viewAttributesFilter = new com.adminConsole.common.statusFilterMenu({
                "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                "id": "viewAttributesFilter",
                "isVisible": false,
                "layoutType": kony.flex.FREE_FORM,
                "left": "20dp",
                "masterType": constants.MASTER_TYPE_DEFAULT,
                "maxWidth": "50%",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "38dp",
                "width": "20%",
                "zIndex": 5,
                "overrides": {
                    "flxArrowImage": {
                        "width": "60%"
                    },
                    "imgUpArrow": {
                        "src": "uparrow_2x.png"
                    },
                    "statusFilterMenu": {
                        "isVisible": false,
                        "left": "20dp",
                        "maxWidth": "50%",
                        "minWidth": "viz.val_cleared",
                        "top": "38dp",
                        "width": "20%"
                    }
                }
            }, {
                "overrides": {}
            }, {
                "overrides": {}
            });
            flxAttributesList.add(flxAttrHeaderRow, flxAttributeHeaderSeparator, segAttributeList, viewAttributesFilter);
            flxAttributeListBottomDetails.add(flxAttributeDescriptionUsers, flxAttributeListSeparator, flxAttributesList);
            flxViewAttributeListWindow.add(flxAttributeListTopDetails, flxAttributeListBottomDetails);
            flxViewAttributeList.add(flxViewAttributeListWindow);
            var flxEmailPreview = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "centerX": "50%",
                "centerY": "50%",
                "clipBounds": true,
                "height": "100%",
                "id": "flxEmailPreview",
                "isVisible": false,
                "layoutType": kony.flex.FREE_FORM,
                "isModalContainer": false,
                "skin": "sknFlxBg000000op40",
                "width": "100%",
                "zIndex": 5
            }, {}, {});
            flxEmailPreview.setDefaultUnit(kony.flex.DP);
            var flxDisplayEmailPreview = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "bottom": "5.25%",
                "clipBounds": true,
                "id": "flxDisplayEmailPreview",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "7.32%",
                "isModalContainer": false,
                "right": "7.32%",
                "skin": "sknBackgroundFFFFFF",
                "top": "5.25%"
            }, {}, {});
            flxDisplayEmailPreview.setDefaultUnit(kony.flex.DP);
            var flxEmailPreviewTopBar = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "10px",
                "id": "flxEmailPreviewTopBar",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0px",
                "isModalContainer": false,
                "skin": "sknFlxInfoToastBg357C9ENoRadius",
                "top": "0px",
                "width": "100%",
                "zIndex": 1
            }, {}, {});
            flxEmailPreviewTopBar.setDefaultUnit(kony.flex.DP);
            flxEmailPreviewTopBar.add();
            var flxEmailPreviewHeader = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "70px",
                "id": "flxEmailPreviewHeader",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0px",
                "isModalContainer": false,
                "right": "0px",
                "skin": "slFbox",
                "top": "10px",
                "width": "100%",
                "zIndex": 1
            }, {}, {});
            flxEmailPreviewHeader.setDefaultUnit(kony.flex.DP);
            var flxEmailPreviewMode = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "bottom": "73px",
                "clipBounds": true,
                "height": "50px",
                "id": "flxEmailPreviewMode",
                "isVisible": true,
                "layoutType": kony.flex.FLOW_HORIZONTAL,
                "left": "18px",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0px",
                "width": "150px",
                "zIndex": 1
            }, {}, {});
            flxEmailPreviewMode.setDefaultUnit(kony.flex.DP);
            var lblEmailPreviewMode = new kony.ui.Label({
                "id": "lblEmailPreviewMode",
                "isVisible": true,
                "left": "0px",
                "skin": "sknLbl485c75LatoBold13px",
                "text": "EMAIL PREVIEW MODE",
                "top": "24px",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxEmailPreviewMode.add(lblEmailPreviewMode);
            var flxEmailPreviewClose = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "20px",
                "id": "flxEmailPreviewClose",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "isModalContainer": false,
                "right": "20px",
                "skin": "sknFlxffffffCursorPointer",
                "top": "10px",
                "width": "20px",
                "zIndex": 1
            }, {}, {});
            flxEmailPreviewClose.setDefaultUnit(kony.flex.DP);
            var lblEmailPreviewClose = new kony.ui.Label({
                "height": "100%",
                "id": "lblEmailPreviewClose",
                "isVisible": true,
                "left": "0px",
                "skin": "lblIconGrey",
                "text": "",
                "top": "0dp",
                "width": "100%",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_CENTER,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxEmailPreviewClose.add(lblEmailPreviewClose);
            var flxEmailPreviewSeparator = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "bottom": "0px",
                "clipBounds": true,
                "height": "1px",
                "id": "flxEmailPreviewSeparator",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "20px",
                "isModalContainer": false,
                "right": "20px",
                "skin": "sknFlxD7D9E0Separator",
                "top": "55px",
                "zIndex": 1
            }, {}, {});
            flxEmailPreviewSeparator.setDefaultUnit(kony.flex.DP);
            flxEmailPreviewSeparator.add();
            flxEmailPreviewHeader.add(flxEmailPreviewMode, flxEmailPreviewClose, flxEmailPreviewSeparator);
            var flxEmailPreviewContent = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "bottom": "0px",
                "clipBounds": true,
                "id": "flxEmailPreviewContent",
                "isVisible": true,
                "layoutType": kony.flex.FLOW_VERTICAL,
                "left": "0px",
                "isModalContainer": false,
                "right": "0px",
                "skin": "slFbox",
                "top": "90px",
                "width": "100%"
            }, {}, {});
            flxEmailPreviewContent.setDefaultUnit(kony.flex.DP);
            var flxEmailPreviewScroll = new kony.ui.FlexScrollContainer({
                "allowHorizontalBounce": false,
                "allowVerticalBounce": true,
                "bottom": "0px",
                "bounces": true,
                "clipBounds": true,
                "enableScrolling": true,
                "height": "100%",
                "horizontalScrollIndicator": true,
                "id": "flxEmailPreviewScroll",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0px",
                "pagingEnabled": false,
                "right": "10px",
                "scrollDirection": kony.flex.SCROLL_BOTH,
                "skin": "slFSbox",
                "top": "0px",
                "verticalScrollIndicator": true,
                "width": "100%",
                "zIndex": 1
            }, {}, {});
            flxEmailPreviewScroll.setDefaultUnit(kony.flex.DP);
            var flxEmailPreviewContentFlowVertical = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                "clipBounds": true,
                "id": "flxEmailPreviewContentFlowVertical",
                "isVisible": true,
                "layoutType": kony.flex.FLOW_VERTICAL,
                "left": "18px",
                "isModalContainer": false,
                "right": "18px",
                "skin": "slFbox",
                "top": "0px"
            }, {}, {});
            flxEmailPreviewContentFlowVertical.setDefaultUnit(kony.flex.DP);
            var flxEmailPreviewSubject = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                "clipBounds": true,
                "id": "flxEmailPreviewSubject",
                "isVisible": true,
                "layoutType": kony.flex.FLOW_VERTICAL,
                "left": "0px",
                "isModalContainer": false,
                "right": "0px",
                "skin": "slFbox",
                "top": "0px",
                "width": "100%",
                "zIndex": 1
            }, {}, {});
            flxEmailPreviewSubject.setDefaultUnit(kony.flex.DP);
            var lblSubjectHeader1 = new kony.ui.Label({
                "id": "lblSubjectHeader1",
                "isVisible": true,
                "left": "0px",
                "skin": "sknLatoRegular192B4516px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmAlertsManagement.SubjectColon\")",
                "top": "0px",
                "width": "100%",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var lblSubject1 = new kony.ui.Label({
                "id": "lblSubject1",
                "isVisible": true,
                "left": "0",
                "skin": "sknLblLato485c7513px",
                "text": "Platinum Customers",
                "top": "15px",
                "width": "100%",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxEmailPreviewSubject.add(lblSubjectHeader1, lblSubject1);
            var flxEmailPreviewDescription = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "bottom": "0px",
                "clipBounds": true,
                "height": "1000px",
                "id": "flxEmailPreviewDescription",
                "isVisible": true,
                "layoutType": kony.flex.FLOW_VERTICAL,
                "left": "0px",
                "isModalContainer": false,
                "right": "0px",
                "skin": "slFbox",
                "top": "20px",
                "width": "100%",
                "zIndex": 1
            }, {}, {});
            flxEmailPreviewDescription.setDefaultUnit(kony.flex.DP);
            var lblEmailDescHeader1 = new kony.ui.Label({
                "id": "lblEmailDescHeader1",
                "isVisible": true,
                "left": "0",
                "skin": "sknLatoRegular192B4516px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmAdManagement.emailDescriptionWithColon\")",
                "top": "0px",
                "width": "100%",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var rtxEmailDescPreview = new kony.ui.RichText({
                "height": "100%",
                "id": "rtxEmailDescPreview",
                "isVisible": true,
                "left": "0px",
                "linkSkin": "defRichTextLink",
                "skin": "slRichText",
                "top": "15px",
                "width": "100%",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxEmailPreviewDescription.add(lblEmailDescHeader1, rtxEmailDescPreview);
            flxEmailPreviewContentFlowVertical.add(flxEmailPreviewSubject, flxEmailPreviewDescription);
            flxEmailPreviewScroll.add(flxEmailPreviewContentFlowVertical);
            flxEmailPreviewContent.add(flxEmailPreviewScroll);
            flxDisplayEmailPreview.add(flxEmailPreviewTopBar, flxEmailPreviewHeader, flxEmailPreviewContent);
            flxEmailPreview.add(flxDisplayEmailPreview);
            this.add(flxLeftPannel, flxRightPannel, flxAdManagementPopUp, flxViewAdPreview, flxViewAttributeList, flxEmailPreview);
        };
        return [{
            "addWidgets": addWidgetsfrmAdManagement,
            "enabledForIdleTimeout": true,
            "id": "frmAdManagement",
            "layoutType": kony.flex.FREE_FORM,
            "needAppMenu": false,
            "preShow": function(eventobject) {
                controller.AS_Form_i58bfca52b6d4776babece9162ffe51d(eventobject);
            },
            "skin": "slForm"
        }, {
            "displayOrientation": constants.FORM_DISPLAY_ORIENTATION_PORTRAIT,
            "layoutType": kony.flex.FREE_FORM,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, {
            "onDeviceBack": controller.AS_Form_h505faddf8bb46d78d9774512819783e,
            "retainScrollPosition": false
        }]
    }
});