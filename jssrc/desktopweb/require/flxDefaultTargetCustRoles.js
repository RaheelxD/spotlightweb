define("flxDefaultTargetCustRoles", function() {
    return function(controller) {
        var flxDefaultTargetCustRoles = new kony.ui.FlexContainer({
            "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
            "clipBounds": true,
            "id": "flxDefaultTargetCustRoles",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "left": "0dp",
            "isModalContainer": false,
            "skin": "sknBackgroundFFFFFF",
            "top": "0dp",
            "width": "100%"
        }, {}, {});
        flxDefaultTargetCustRoles.setDefaultUnit(kony.flex.DP);
        var lblRoleName = new kony.ui.Label({
            "height": "17px",
            "id": "lblRoleName",
            "isVisible": true,
            "left": "2%",
            "skin": "sknLbl192B45LatoRegular14px",
            "text": "Platinum DBX Customers",
            "top": "15px",
            "width": "20%",
            "zIndex": 1
        }, {
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, {});
        var lblRoleDescription = new kony.ui.Label({
            "bottom": "10px",
            "id": "lblRoleDescription",
            "isVisible": true,
            "left": "27%",
            "right": "2%",
            "skin": "sknLbl192B45LatoRegular14px",
            "text": "All customers list with DBX platinum  card holders, Includes new addition of 2018 -2019. All customers list with DBX platinum  card holders, Includes new addition of 2018 -2019.",
            "top": "15px",
            "zIndex": 1
        }, {
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, {});
        flxDefaultTargetCustRoles.add(lblRoleName, lblRoleDescription);
        return flxDefaultTargetCustRoles;
    }
})