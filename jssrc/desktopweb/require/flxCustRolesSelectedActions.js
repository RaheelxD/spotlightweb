define("flxCustRolesSelectedActions", function() {
    return function(controller) {
        var flxCustRolesSelectedActions = new kony.ui.FlexContainer({
            "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
            "clipBounds": true,
            "id": "flxCustRolesSelectedActions",
            "isVisible": true,
            "layoutType": kony.flex.FLOW_VERTICAL,
            "left": "0dp",
            "isModalContainer": false,
            "skin": "slFbox",
            "top": "0dp",
            "width": "100%"
        }, {}, {});
        flxCustRolesSelectedActions.setDefaultUnit(kony.flex.DP);
        var flxActionNameContainer = new kony.ui.FlexContainer({
            "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
            "clipBounds": true,
            "id": "flxActionNameContainer",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "left": "0dp",
            "isModalContainer": false,
            "right": "20px",
            "skin": "sknflxffffffBorderE1E5EERadius3px",
            "top": "12dp",
            "width": "100%",
            "zIndex": 3
        }, {}, {});
        flxActionNameContainer.setDefaultUnit(kony.flex.DP);
        var flxActionRowCont = new kony.ui.FlexContainer({
            "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
            "clipBounds": true,
            "id": "flxActionRowCont",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "left": "0dp",
            "isModalContainer": false,
            "skin": "slFbox",
            "top": "0dp",
            "width": "100%"
        }, {}, {});
        flxActionRowCont.setDefaultUnit(kony.flex.DP);
        var flxActionCheckbox = new kony.ui.FlexContainer({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "clipBounds": true,
            "height": "20dp",
            "id": "flxActionCheckbox",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "left": "10dp",
            "isModalContainer": false,
            "skin": "slFbox",
            "top": "12dp",
            "width": "20dp",
            "zIndex": 1
        }, {}, {
            "hoverSkin": "sknCursor"
        });
        flxActionCheckbox.setDefaultUnit(kony.flex.DP);
        var imgActionCheckbox = new kony.ui.Image2({
            "centerX": "50%",
            "centerY": "50%",
            "height": "15dp",
            "id": "imgActionCheckbox",
            "isVisible": true,
            "skin": "slImage",
            "src": "checkboxnormal.png",
            "width": "15dp",
            "zIndex": 1
        }, {
            "imageScaleMode": constants.IMAGE_SCALE_MODE_MAINTAIN_ASPECT_RATIO,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, {});
        flxActionCheckbox.add(imgActionCheckbox);
        var flxActionName = new kony.ui.FlexContainer({
            "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
            "bottom": "15dp",
            "clipBounds": true,
            "id": "flxActionName",
            "isVisible": true,
            "layoutType": kony.flex.FLOW_VERTICAL,
            "left": "40dp",
            "isModalContainer": false,
            "skin": "slFbox",
            "top": "13dp",
            "width": "80%"
        }, {}, {});
        flxActionName.setDefaultUnit(kony.flex.DP);
        var lblActionName = new kony.ui.Label({
            "id": "lblActionName",
            "isVisible": true,
            "left": "0dp",
            "skin": "sknLbl192B45LatoRegular14px",
            "text": "Label",
            "top": "0dp",
            "width": "100%",
            "zIndex": 1
        }, {
            "contentAlignment": constants.CONTENT_ALIGN_TOP_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, {});
        var lblMandatory = new kony.ui.Label({
            "id": "lblMandatory",
            "isVisible": false,
            "left": "0dp",
            "skin": "sknLblLato84939E12px",
            "text": "Mandatory Action",
            "top": "4dp",
            "width": "80%",
            "zIndex": 1
        }, {
            "contentAlignment": constants.CONTENT_ALIGN_TOP_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, {});
        flxActionName.add(lblActionName, lblMandatory);
        var flxActionArrow = new kony.ui.FlexContainer({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "clipBounds": true,
            "height": "20dp",
            "id": "flxActionArrow",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "isModalContainer": false,
            "right": "10dp",
            "skin": "slFbox",
            "top": "12dp",
            "width": "20dp",
            "zIndex": 1
        }, {}, {
            "hoverSkin": "sknCursor"
        });
        flxActionArrow.setDefaultUnit(kony.flex.DP);
        var lblIconActionArrow = new kony.ui.Label({
            "centerX": "50%",
            "centerY": "50%",
            "height": "15dp",
            "id": "lblIconActionArrow",
            "isVisible": true,
            "right": "0dp",
            "skin": "sknBackIcon006CCA",
            "text": "",
            "width": "15dp",
            "zIndex": 1
        }, {
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, {});
        flxActionArrow.add(lblIconActionArrow);
        flxActionRowCont.add(flxActionCheckbox, flxActionName, flxActionArrow);
        flxActionNameContainer.add(flxActionRowCont);
        var flxActionLimits = new kony.ui.FlexContainer({
            "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
            "clipBounds": true,
            "id": "flxActionLimits",
            "isVisible": false,
            "layoutType": kony.flex.FREE_FORM,
            "left": "0dp",
            "isModalContainer": false,
            "right": "20dp",
            "skin": "slFbox",
            "top": "-5dp",
            "width": "100%"
        }, {}, {});
        flxActionLimits.setDefaultUnit(kony.flex.DP);
        var flxActionLimitsGroup = new kony.ui.FlexContainer({
            "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
            "clipBounds": true,
            "id": "flxActionLimitsGroup",
            "isVisible": true,
            "layoutType": kony.flex.FLOW_VERTICAL,
            "left": "0dp",
            "isModalContainer": false,
            "right": "0dp",
            "skin": "sknflxBgF9F9F9BrE1E5EE1pxR3px",
            "top": "0dp",
            "zIndex": 1
        }, {}, {});
        flxActionLimitsGroup.setDefaultUnit(kony.flex.DP);
        var flxLimitsRow1 = new kony.ui.FlexContainer({
            "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
            "clipBounds": true,
            "id": "flxLimitsRow1",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "left": "0dp",
            "isModalContainer": false,
            "skin": "slFbox",
            "top": "20dp",
            "width": "100%",
            "zIndex": 1
        }, {}, {});
        flxLimitsRow1.setDefaultUnit(kony.flex.DP);
        var lblRowHeading1 = new kony.ui.Label({
            "id": "lblRowHeading1",
            "isVisible": true,
            "left": "15dp",
            "skin": "sknLbl485C75LatoRegular13Px",
            "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmGroupsController.Min_Transaction_Value\")",
            "top": "15dp",
            "width": "50%",
            "zIndex": 1
        }, {
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, {});
        var flxLimitValue1 = new kony.ui.FlexContainer({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "clipBounds": true,
            "height": "40dp",
            "id": "flxLimitValue1",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "isModalContainer": false,
            "right": "15dp",
            "skin": "sknFlxbgFFFFFFbdrD7D9E0rd3px",
            "top": "0dp",
            "width": "40%"
        }, {}, {});
        flxLimitValue1.setDefaultUnit(kony.flex.DP);
        var lblCurrencySymbol1 = new kony.ui.Label({
            "centerY": "50%",
            "id": "lblCurrencySymbol1",
            "isVisible": true,
            "left": "5dp",
            "skin": "sknFontIcon485C7518px",
            "text": "",
            "width": "20dp",
            "zIndex": 1
        }, {
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, {});
        var tbxLimitValue1 = new kony.ui.TextBox2({
            "autoCapitalize": constants.TEXTBOX_AUTO_CAPITALIZE_NONE,
            "focusSkin": "sknTbx485c75LatoReg13pxNoBor",
            "height": "40dp",
            "id": "tbxLimitValue1",
            "isVisible": true,
            "keyBoardStyle": constants.TEXTBOX_KEY_BOARD_STYLE_DEFAULT,
            "left": "25dp",
            "right": "0dp",
            "secureTextEntry": false,
            "skin": "sknTbxBgFFFFFFf485c7513pxNoBorder",
            "textInputMode": constants.TEXTBOX_INPUT_MODE_NUMERIC,
            "top": "0dp",
            "zIndex": 1
        }, {
            "containerHeightMode": constants.TEXTBOX_FONT_METRICS_DRIVEN_HEIGHT,
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [1, 0, 0, 0],
            "paddingInPixel": false
        }, {
            "autoCorrect": false,
            "placeholderSkin": "defTextBoxPlaceholder"
        });
        flxLimitValue1.add(lblCurrencySymbol1, tbxLimitValue1);
        var flxLimitError1 = new kony.ui.FlexContainer({
            "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
            "clipBounds": true,
            "id": "flxLimitError1",
            "isVisible": false,
            "layoutType": kony.flex.FREE_FORM,
            "left": "15dp",
            "isModalContainer": false,
            "skin": "slFbox",
            "top": "50dp",
            "width": "260dp",
            "zIndex": 1
        }, {}, {});
        flxLimitError1.setDefaultUnit(kony.flex.DP);
        var lblLimitErrorIcon1 = new kony.ui.Label({
            "height": "15dp",
            "id": "lblLimitErrorIcon1",
            "isVisible": true,
            "left": "0dp",
            "skin": "sknErrorIcon",
            "text": "",
            "top": "0dp",
            "width": "15dp",
            "zIndex": 1
        }, {
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, {});
        var lblLimitErrorMsg1 = new kony.ui.Label({
            "id": "lblLimitErrorMsg1",
            "isVisible": true,
            "left": "17dp",
            "skin": "sknlblError",
            "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmServiceManagement.lblNoServiceTypeError\")",
            "top": "0dp",
            "width": "80%",
            "zIndex": 1
        }, {
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, {});
        flxLimitError1.add(lblLimitErrorIcon1, lblLimitErrorMsg1);
        flxLimitsRow1.add(lblRowHeading1, flxLimitValue1, flxLimitError1);
        var flxLimitsRow2 = new kony.ui.FlexContainer({
            "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
            "clipBounds": true,
            "id": "flxLimitsRow2",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "left": "0dp",
            "isModalContainer": false,
            "skin": "slFbox",
            "top": "10dp",
            "width": "100%",
            "zIndex": 1
        }, {}, {});
        flxLimitsRow2.setDefaultUnit(kony.flex.DP);
        var lblRowHeading2 = new kony.ui.Label({
            "id": "lblRowHeading2",
            "isVisible": true,
            "left": "15dp",
            "skin": "sknLbl485C75LatoRegular13Px",
            "text": "Label",
            "top": "13dp",
            "width": "48%",
            "zIndex": 1
        }, {
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, {});
        var flxLimitValue2 = new kony.ui.FlexContainer({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "clipBounds": true,
            "height": "40dp",
            "id": "flxLimitValue2",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "isModalContainer": false,
            "right": "15dp",
            "skin": "sknFlxbgFFFFFFbdrD7D9E0rd3px",
            "top": "0dp",
            "width": "40%"
        }, {}, {});
        flxLimitValue2.setDefaultUnit(kony.flex.DP);
        var lblCurrencySymbol2 = new kony.ui.Label({
            "centerY": "50%",
            "id": "lblCurrencySymbol2",
            "isVisible": true,
            "left": "5dp",
            "skin": "sknFontIcon485C7518px",
            "text": "",
            "width": "20dp",
            "zIndex": 1
        }, {
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, {});
        var tbxLimitValue2 = new kony.ui.TextBox2({
            "autoCapitalize": constants.TEXTBOX_AUTO_CAPITALIZE_NONE,
            "focusSkin": "sknTbx485c75LatoReg13pxNoBor",
            "height": "40dp",
            "id": "tbxLimitValue2",
            "isVisible": true,
            "keyBoardStyle": constants.TEXTBOX_KEY_BOARD_STYLE_DEFAULT,
            "left": "25dp",
            "right": "0dp",
            "secureTextEntry": false,
            "skin": "sknTbxBgFFFFFFf485c7513pxNoBorder",
            "textInputMode": constants.TEXTBOX_INPUT_MODE_NUMERIC,
            "top": "0dp",
            "zIndex": 1
        }, {
            "containerHeightMode": constants.TEXTBOX_FONT_METRICS_DRIVEN_HEIGHT,
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [1, 0, 0, 0],
            "paddingInPixel": false
        }, {
            "autoCorrect": false,
            "placeholderSkin": "defTextBoxPlaceholder"
        });
        flxLimitValue2.add(lblCurrencySymbol2, tbxLimitValue2);
        var flxLimitError2 = new kony.ui.FlexContainer({
            "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
            "clipBounds": true,
            "id": "flxLimitError2",
            "isVisible": false,
            "layoutType": kony.flex.FREE_FORM,
            "left": "15dp",
            "isModalContainer": false,
            "skin": "slFbox",
            "top": "50dp",
            "width": "260dp",
            "zIndex": 1
        }, {}, {});
        flxLimitError2.setDefaultUnit(kony.flex.DP);
        var lblLimitErrorIcon2 = new kony.ui.Label({
            "height": "15dp",
            "id": "lblLimitErrorIcon2",
            "isVisible": true,
            "left": "0dp",
            "skin": "sknErrorIcon",
            "text": "",
            "top": "0dp",
            "width": "15dp",
            "zIndex": 1
        }, {
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, {});
        var lblLimitErrorMsg2 = new kony.ui.Label({
            "id": "lblLimitErrorMsg2",
            "isVisible": true,
            "left": "17dp",
            "skin": "sknlblError",
            "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmServiceManagement.lblNoServiceTypeError\")",
            "top": "0dp",
            "width": "80%",
            "zIndex": 1
        }, {
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, {});
        flxLimitError2.add(lblLimitErrorIcon2, lblLimitErrorMsg2);
        flxLimitsRow2.add(lblRowHeading2, flxLimitValue2, flxLimitError2);
        var flxLimitsRow3 = new kony.ui.FlexContainer({
            "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
            "bottom": "15dp",
            "clipBounds": true,
            "id": "flxLimitsRow3",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "left": "0dp",
            "isModalContainer": false,
            "skin": "slFbox",
            "top": "10dp",
            "width": "100%",
            "zIndex": 1
        }, {}, {});
        flxLimitsRow3.setDefaultUnit(kony.flex.DP);
        var lblRowHeading3 = new kony.ui.Label({
            "id": "lblRowHeading3",
            "isVisible": true,
            "left": "15dp",
            "skin": "sknLbl485C75LatoRegular13Px",
            "text": "Label",
            "top": "13dp",
            "width": "50%",
            "zIndex": 1
        }, {
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, {});
        var flxLimitValue3 = new kony.ui.FlexContainer({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "clipBounds": true,
            "height": "40dp",
            "id": "flxLimitValue3",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "isModalContainer": false,
            "right": "15dp",
            "skin": "sknFlxbgFFFFFFbdrD7D9E0rd3px",
            "top": "0dp",
            "width": "40%"
        }, {}, {});
        flxLimitValue3.setDefaultUnit(kony.flex.DP);
        var lblCurrencySymbol3 = new kony.ui.Label({
            "centerY": "50%",
            "id": "lblCurrencySymbol3",
            "isVisible": true,
            "left": "5dp",
            "skin": "sknFontIcon485C7518px",
            "text": "",
            "width": "20dp",
            "zIndex": 1
        }, {
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, {});
        var tbxLimitValue3 = new kony.ui.TextBox2({
            "autoCapitalize": constants.TEXTBOX_AUTO_CAPITALIZE_NONE,
            "focusSkin": "sknTbx485c75LatoReg13pxNoBor",
            "height": "40dp",
            "id": "tbxLimitValue3",
            "isVisible": true,
            "keyBoardStyle": constants.TEXTBOX_KEY_BOARD_STYLE_DEFAULT,
            "left": "25dp",
            "right": "0dp",
            "secureTextEntry": false,
            "skin": "sknTbxBgFFFFFFf485c7513pxNoBorder",
            "textInputMode": constants.TEXTBOX_INPUT_MODE_NUMERIC,
            "top": "0dp",
            "zIndex": 1
        }, {
            "containerHeightMode": constants.TEXTBOX_FONT_METRICS_DRIVEN_HEIGHT,
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [1, 0, 0, 0],
            "paddingInPixel": false
        }, {
            "autoCorrect": false,
            "placeholderSkin": "defTextBoxPlaceholder"
        });
        flxLimitValue3.add(lblCurrencySymbol3, tbxLimitValue3);
        var flxLimitError3 = new kony.ui.FlexContainer({
            "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
            "clipBounds": true,
            "id": "flxLimitError3",
            "isVisible": false,
            "layoutType": kony.flex.FREE_FORM,
            "left": "15dp",
            "isModalContainer": false,
            "skin": "slFbox",
            "top": "50dp",
            "width": "260dp",
            "zIndex": 1
        }, {}, {});
        flxLimitError3.setDefaultUnit(kony.flex.DP);
        var lblLimitErrorIcon3 = new kony.ui.Label({
            "height": "15dp",
            "id": "lblLimitErrorIcon3",
            "isVisible": true,
            "left": "0dp",
            "skin": "sknErrorIcon",
            "text": "",
            "top": "0dp",
            "width": "15dp",
            "zIndex": 1
        }, {
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, {});
        var lblLimitErrorMsg3 = new kony.ui.Label({
            "height": "15dp",
            "id": "lblLimitErrorMsg3",
            "isVisible": true,
            "left": "17dp",
            "skin": "sknlblError",
            "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmServiceManagement.lblNoServiceTypeError\")",
            "top": "0dp",
            "width": "80%",
            "zIndex": 1
        }, {
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, {});
        flxLimitError3.add(lblLimitErrorIcon3, lblLimitErrorMsg3);
        flxLimitsRow3.add(lblRowHeading3, flxLimitValue3, flxLimitError3);
        var flxLimitsRow4 = new kony.ui.FlexContainer({
            "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
            "bottom": "15dp",
            "clipBounds": true,
            "id": "flxLimitsRow4",
            "isVisible": false,
            "layoutType": kony.flex.FREE_FORM,
            "left": "0dp",
            "isModalContainer": false,
            "skin": "slFbox",
            "top": "10dp",
            "width": "100%",
            "zIndex": 1
        }, {}, {});
        flxLimitsRow4.setDefaultUnit(kony.flex.DP);
        var lblRowHeading4 = new kony.ui.Label({
            "id": "lblRowHeading4",
            "isVisible": true,
            "left": "15dp",
            "skin": "sknLbl485C75LatoRegular13Px",
            "text": "Label",
            "top": "15dp",
            "width": "50%",
            "zIndex": 1
        }, {
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, {});
        var flxLimitValue4 = new kony.ui.FlexContainer({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "clipBounds": true,
            "height": "40dp",
            "id": "flxLimitValue4",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "isModalContainer": false,
            "right": "15dp",
            "skin": "sknFlxbgFFFFFFbdrD7D9E0rd3px",
            "top": "0dp",
            "width": "40%"
        }, {}, {});
        flxLimitValue4.setDefaultUnit(kony.flex.DP);
        var lblCurrencySymbol4 = new kony.ui.Label({
            "centerY": "50%",
            "id": "lblCurrencySymbol4",
            "isVisible": true,
            "left": "10dp",
            "skin": "sknFontIcon485C7518px",
            "text": "",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, {
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, {});
        var tbxLimitValue4 = new kony.ui.TextBox2({
            "autoCapitalize": constants.TEXTBOX_AUTO_CAPITALIZE_NONE,
            "focusSkin": "sknTbx485c75LatoReg13pxNoBor",
            "height": "40dp",
            "id": "tbxLimitValue4",
            "isVisible": true,
            "keyBoardStyle": constants.TEXTBOX_KEY_BOARD_STYLE_DEFAULT,
            "left": "30dp",
            "right": "10dp",
            "secureTextEntry": false,
            "skin": "sknTbxBgFFFFFFf485c7513pxNoBorder",
            "textInputMode": constants.TEXTBOX_INPUT_MODE_NUMERIC,
            "top": "0dp",
            "zIndex": 1
        }, {
            "containerHeightMode": constants.TEXTBOX_FONT_METRICS_DRIVEN_HEIGHT,
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [3, 0, 0, 0],
            "paddingInPixel": false
        }, {
            "autoCorrect": false,
            "placeholderSkin": "defTextBoxPlaceholder"
        });
        flxLimitValue4.add(lblCurrencySymbol4, tbxLimitValue4);
        var flxLimitError4 = new kony.ui.FlexContainer({
            "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
            "clipBounds": true,
            "id": "flxLimitError4",
            "isVisible": false,
            "layoutType": kony.flex.FREE_FORM,
            "left": "15dp",
            "isModalContainer": false,
            "skin": "slFbox",
            "top": "50dp",
            "width": "260dp",
            "zIndex": 1
        }, {}, {});
        flxLimitError4.setDefaultUnit(kony.flex.DP);
        var lblLimitErrorIcon4 = new kony.ui.Label({
            "height": "15dp",
            "id": "lblLimitErrorIcon4",
            "isVisible": true,
            "left": "0dp",
            "skin": "sknErrorIcon",
            "text": "",
            "top": "0dp",
            "width": "15dp",
            "zIndex": 1
        }, {
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, {});
        var lblLimitErrorMsg4 = new kony.ui.Label({
            "height": "15dp",
            "id": "lblLimitErrorMsg4",
            "isVisible": true,
            "left": "15dp",
            "skin": "sknlblError",
            "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmServiceManagement.lblNoServiceTypeError\")",
            "top": "0dp",
            "width": "100%",
            "zIndex": 1
        }, {
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, {});
        flxLimitError4.add(lblLimitErrorIcon4, lblLimitErrorMsg4);
        flxLimitsRow4.add(lblRowHeading4, flxLimitValue4, flxLimitError4);
        flxActionLimitsGroup.add(flxLimitsRow1, flxLimitsRow2, flxLimitsRow3, flxLimitsRow4);
        var flxActionDisabled = new kony.ui.FlexContainer({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "clipBounds": true,
            "height": "180dp",
            "id": "flxActionDisabled",
            "isVisible": false,
            "layoutType": kony.flex.FREE_FORM,
            "left": "0dp",
            "minHeight": "180dp",
            "isModalContainer": false,
            "right": "0dp",
            "skin": "slFbox",
            "top": "0dp",
            "zIndex": 10
        }, {}, {});
        flxActionDisabled.setDefaultUnit(kony.flex.DP);
        var lblNoDesc = new kony.ui.Label({
            "id": "lblNoDesc",
            "isVisible": true,
            "left": "-10dp",
            "skin": "slLabel",
            "top": "48dp",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, {
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, {});
        flxActionDisabled.add(lblNoDesc);
        flxActionLimits.add(flxActionLimitsGroup, flxActionDisabled);
        flxCustRolesSelectedActions.add(flxActionNameContainer, flxActionLimits);
        return flxCustRolesSelectedActions;
    }
})