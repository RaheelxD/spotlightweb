define("segNotes", function() {
    return function(controller) {
        var segNotes = new kony.ui.FlexContainer({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "clipBounds": true,
            "height": "40dp",
            "id": "segNotes",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "isModalContainer": false,
            "skin": "sknbackGroundffffff100"
        }, {}, {});
        segNotes.setDefaultUnit(kony.flex.DP);
        var flxNotesHeader = new kony.ui.FlexContainer({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "clipBounds": true,
            "height": "38dp",
            "id": "flxNotesHeader",
            "isVisible": true,
            "layoutType": kony.flex.FLOW_HORIZONTAL,
            "left": "15px",
            "isModalContainer": false,
            "onClick": controller.AS_FlexContainer_f8223ea4059f490dafba3660d3ea53cf,
            "skin": "slFbox",
            "top": "0dp",
            "width": "100%",
            "zIndex": 1
        }, {}, {});
        flxNotesHeader.setDefaultUnit(kony.flex.DP);
        var flxArrow = new kony.ui.FlexContainer({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "clipBounds": true,
            "height": "40dp",
            "id": "flxArrow",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "left": "0px",
            "isModalContainer": false,
            "skin": "sknCursor",
            "top": "0dp",
            "width": "20dp",
            "zIndex": 2
        }, {}, {});
        flxArrow.setDefaultUnit(kony.flex.DP);
        var imgArrow = new kony.ui.Image2({
            "centerY": "50%",
            "height": "10dp",
            "id": "imgArrow",
            "isVisible": false,
            "left": "0dp",
            "right": "0dp",
            "skin": "slImage",
            "src": "img_down_arrow.png",
            "width": "10dp",
            "zIndex": 10
        }, {
            "imageScaleMode": constants.IMAGE_SCALE_MODE_MAINTAIN_ASPECT_RATIO,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, {});
        var fonticonArrow = new kony.ui.Label({
            "centerX": "50%",
            "centerY": "50%",
            "height": "12px",
            "id": "fonticonArrow",
            "isVisible": true,
            "skin": "sknfontIconDescDownArrow12px",
            "text": "",
            "width": "12px",
            "zIndex": 1
        }, {
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, {});
        flxArrow.add(imgArrow, fonticonArrow);
        var lblDate = new kony.ui.Label({
            "centerY": "50%",
            "id": "lblDate",
            "isVisible": true,
            "left": "5px",
            "skin": "sknlblLatoBold35475f14px",
            "text": "October 21, 2017",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, {
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, {});
        flxNotesHeader.add(flxArrow, lblDate);
        segNotes.add(flxNotesHeader);
        return segNotes;
    }
})