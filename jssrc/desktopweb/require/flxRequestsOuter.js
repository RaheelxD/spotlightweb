define("flxRequestsOuter", function() {
    return function(controller) {
        var flxRequestsOuter = new kony.ui.FlexContainer({
            "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
            "clipBounds": true,
            "id": "flxRequestsOuter",
            "isVisible": true,
            "layoutType": kony.flex.FLOW_VERTICAL,
            "left": "0dp",
            "isModalContainer": false,
            "skin": "sknflxffffffop100",
            "top": "0dp",
            "width": "100%"
        }, {}, {
            "hoverSkin": "sknfbfcfc"
        });
        flxRequestsOuter.setDefaultUnit(kony.flex.DP);
        var flxRequestData = new kony.ui.FlexContainer({
            "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
            "clipBounds": true,
            "id": "flxRequestData",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "left": "0dp",
            "isModalContainer": false,
            "skin": "slFbox",
            "top": "0dp",
            "width": "100%",
            "zIndex": 1
        }, {}, {});
        flxRequestData.setDefaultUnit(kony.flex.DP);
        var lblRequestId = new kony.ui.Label({
            "bottom": "15dp",
            "id": "lblRequestId",
            "isVisible": true,
            "left": "20dp",
            "skin": "sknlblLatoRegular484b5213px",
            "text": "1234567",
            "top": "15dp",
            "width": "12%",
            "zIndex": 1
        }, {
            "contentAlignment": constants.CONTENT_ALIGN_TOP_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, {});
        var lblDate = new kony.ui.Label({
            "bottom": "15dp",
            "id": "lblDate",
            "isVisible": true,
            "left": "17%",
            "skin": "sknlblLatoRegular484b5213px",
            "text": "05/01/18 12:34:00 AM",
            "top": "15dp",
            "width": "16%",
            "zIndex": 1
        }, {
            "contentAlignment": constants.CONTENT_ALIGN_TOP_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, {});
        var lblRequestType = new kony.ui.Label({
            "bottom": "15dp",
            "id": "lblRequestType",
            "isVisible": true,
            "left": "36.50%",
            "skin": "sknlblLatoRegular484b5213px",
            "text": "New Checkbook",
            "top": "15dp",
            "width": "14.50%",
            "zIndex": 1
        }, {
            "contentAlignment": constants.CONTENT_ALIGN_TOP_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, {});
        var lblCardAccount = new kony.ui.Label({
            "bottom": "15dp",
            "id": "lblCardAccount",
            "isVisible": true,
            "left": "53%",
            "skin": "sknlblLatoRegular484b5213px",
            "text": "abc Credit account **** **** **** 1234",
            "top": "15dp",
            "width": "30.00%",
            "zIndex": 1
        }, {
            "contentAlignment": constants.CONTENT_ALIGN_TOP_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, {});
        var lblChannel = new kony.ui.Label({
            "bottom": "15dp",
            "id": "lblChannel",
            "isVisible": false,
            "left": "75%",
            "skin": "sknlblLatoRegular484b5213px",
            "text": "Mobile",
            "top": "15dp",
            "width": "10%",
            "zIndex": 1
        }, {
            "contentAlignment": constants.CONTENT_ALIGN_TOP_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, {});
        var flxStatusCont = new kony.ui.FlexContainer({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "clipBounds": true,
            "height": "20dp",
            "id": "flxStatusCont",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "left": "85%",
            "isModalContainer": false,
            "skin": "slFbox",
            "top": "12dp",
            "width": "13%",
            "zIndex": 1
        }, {}, {});
        flxStatusCont.setDefaultUnit(kony.flex.DP);
        var lblIconStatus = new kony.ui.Label({
            "centerY": "50%",
            "id": "lblIconStatus",
            "isVisible": true,
            "left": "0dp",
            "skin": "sknIcon13pxGreen",
            "text": "",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, {
            "contentAlignment": constants.CONTENT_ALIGN_TOP_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, {});
        var lblStatus = new kony.ui.Label({
            "centerY": "50%",
            "id": "lblStatus",
            "isVisible": true,
            "left": "20dp",
            "skin": "sknlblLatoRegular484b5213px",
            "text": "In progress",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, {
            "contentAlignment": constants.CONTENT_ALIGN_TOP_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, {});
        flxStatusCont.add(lblIconStatus, lblStatus);
        flxRequestData.add(lblRequestId, lblDate, lblRequestType, lblCardAccount, lblChannel, flxStatusCont);
        var flxRowLine = new kony.ui.FlexContainer({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "clipBounds": true,
            "height": "1dp",
            "id": "flxRowLine",
            "isVisible": false,
            "layoutType": kony.flex.FREE_FORM,
            "left": "0dp",
            "isModalContainer": false,
            "skin": "sknSeparatorCsr",
            "top": "5dp",
            "width": "100%",
            "zIndex": 1
        }, {}, {});
        flxRowLine.setDefaultUnit(kony.flex.DP);
        flxRowLine.add();
        flxRequestsOuter.add(flxRequestData, flxRowLine);
        return flxRequestsOuter;
    }
})