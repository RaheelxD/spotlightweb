define("flxSearchCompanyMap", function() {
    return function(controller) {
        var flxSearchCompanyMap = new kony.ui.FlexContainer({
            "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
            "clipBounds": true,
            "id": "flxSearchCompanyMap",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "left": "0dp",
            "isModalContainer": false,
            "skin": "sknbackGroundffffff100",
            "top": "0dp",
            "width": "100%"
        }, {}, {
            "hoverSkin": "sknbackGroundffffffPointer"
        });
        flxSearchCompanyMap.setDefaultUnit(kony.flex.DP);
        var lblPinIcon = new kony.ui.Label({
            "height": "20dp",
            "id": "lblPinIcon",
            "isVisible": true,
            "left": "10dp",
            "skin": "sknIcomoon20px",
            "top": "10px",
            "width": "20dp",
            "zIndex": 1
        }, {
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, {});
        var lblAddress = new kony.ui.Label({
            "bottom": "10dp",
            "id": "lblAddress",
            "isVisible": true,
            "left": "40dp",
            "skin": "sknlbl485c7514px",
            "text": "Granville Street, Vancover, BC, Canada",
            "top": "10dp",
            "width": "80%",
            "zIndex": 1
        }, {
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, {});
        flxSearchCompanyMap.add(lblPinIcon, lblAddress);
        return flxSearchCompanyMap;
    }
})