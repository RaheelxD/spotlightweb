define("flxEnrollLimits", function() {
    return function(controller) {
        var flxEnrollLimits = new kony.ui.FlexContainer({
            "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
            "clipBounds": true,
            "id": "flxEnrollLimits",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "left": "0dp",
            "isModalContainer": false,
            "skin": "sknFlxFFFFFF100O",
            "top": "0dp",
            "width": "100%"
        }, {}, {});
        flxEnrollLimits.setDefaultUnit(kony.flex.DP);
        var lblLimitsHeading = new kony.ui.Label({
            "id": "lblLimitsHeading",
            "isVisible": true,
            "left": 15,
            "skin": "sknlblLatoBold485c7512px",
            "text": "Label",
            "top": 15,
            "width": kony.flex.USE_PREFFERED_SIZE
        }, {
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, {});
        var flxLimitsContainer = new kony.ui.FlexContainer({
            "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
            "clipBounds": true,
            "id": "flxLimitsContainer",
            "isVisible": true,
            "layoutType": kony.flex.FLOW_VERTICAL,
            "left": "0",
            "isModalContainer": false,
            "skin": "slFbox",
            "top": "45dp",
            "width": "100%"
        }, {}, {});
        flxLimitsContainer.setDefaultUnit(kony.flex.DP);
        var flxLimitsRow1 = new kony.ui.FlexContainer({
            "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
            "clipBounds": true,
            "id": "flxLimitsRow1",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "left": "0dp",
            "isModalContainer": false,
            "skin": "slFbox",
            "top": "0dp",
            "width": "100%",
            "zIndex": 1
        }, {}, {});
        flxLimitsRow1.setDefaultUnit(kony.flex.DP);
        var lblRowHeading1 = new kony.ui.Label({
            "id": "lblRowHeading1",
            "isVisible": true,
            "left": "15dp",
            "skin": "sknLbl485C75LatoRegular13Px",
            "i18n_text": "kony.i18n.getLocalizedString(\"i18n.konybb.perTransaction\")",
            "top": "0dp",
            "width": "130dp",
            "zIndex": 1
        }, {
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, {});
        var flxLimitValue1 = new kony.ui.FlexContainer({
            "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
            "clipBounds": true,
            "id": "flxLimitValue1",
            "isVisible": true,
            "layoutType": kony.flex.FLOW_HORIZONTAL,
            "left": "160dp",
            "isModalContainer": false,
            "skin": "slFbox",
            "top": "0dp",
            "width": "200dp"
        }, {}, {});
        flxLimitValue1.setDefaultUnit(kony.flex.DP);
        var lblCurrency1 = new kony.ui.Label({
            "id": "lblCurrency1",
            "isVisible": true,
            "left": "0dp",
            "skin": "sknIcon485c75Info16px",
            "text": "$",
            "top": "0dp",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, {
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, {});
        var lblValue1 = new kony.ui.Label({
            "id": "lblValue1",
            "isVisible": true,
            "left": "10dp",
            "skin": "sknLbl485C75LatoRegular13Px",
            "text": "2000",
            "top": "1dp",
            "width": "80%",
            "zIndex": 1
        }, {
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, {});
        flxLimitValue1.add(lblCurrency1, lblValue1);
        flxLimitsRow1.add(lblRowHeading1, flxLimitValue1);
        var flxLimitsRow2 = new kony.ui.FlexContainer({
            "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
            "clipBounds": true,
            "id": "flxLimitsRow2",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "left": "0dp",
            "isModalContainer": false,
            "skin": "slFbox",
            "top": "15dp",
            "width": "100%",
            "zIndex": 1
        }, {}, {});
        flxLimitsRow2.setDefaultUnit(kony.flex.DP);
        var lblRowHeading2 = new kony.ui.Label({
            "id": "lblRowHeading2",
            "isVisible": true,
            "left": "15dp",
            "skin": "sknLbl485C75LatoRegular13Px",
            "i18n_text": "kony.i18n.getLocalizedString(\"i18n.konybb.dailyTransaction\")",
            "top": "0dp",
            "width": "130dp",
            "zIndex": 1
        }, {
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, {});
        var flxLimitValue2 = new kony.ui.FlexContainer({
            "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
            "clipBounds": true,
            "id": "flxLimitValue2",
            "isVisible": true,
            "layoutType": kony.flex.FLOW_HORIZONTAL,
            "left": "160dp",
            "isModalContainer": false,
            "skin": "slFbox",
            "top": "0dp",
            "width": "200dp"
        }, {}, {});
        flxLimitValue2.setDefaultUnit(kony.flex.DP);
        var lblCurrency2 = new kony.ui.Label({
            "id": "lblCurrency2",
            "isVisible": true,
            "left": "0dp",
            "skin": "sknIcon485c75Info16px",
            "text": "$",
            "top": "0dp",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, {
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, {});
        var lblValue2 = new kony.ui.Label({
            "id": "lblValue2",
            "isVisible": true,
            "left": "10dp",
            "skin": "sknLbl485C75LatoRegular13Px",
            "text": "2000",
            "top": "1dp",
            "width": "80%",
            "zIndex": 1
        }, {
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, {});
        flxLimitValue2.add(lblCurrency2, lblValue2);
        flxLimitsRow2.add(lblRowHeading2, flxLimitValue2);
        var flxLimitsRow3 = new kony.ui.FlexContainer({
            "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
            "bottom": "15dp",
            "clipBounds": true,
            "id": "flxLimitsRow3",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "left": "0dp",
            "isModalContainer": false,
            "skin": "slFbox",
            "top": "15dp",
            "width": "100%",
            "zIndex": 1
        }, {}, {});
        flxLimitsRow3.setDefaultUnit(kony.flex.DP);
        var lblRowHeading3 = new kony.ui.Label({
            "id": "lblRowHeading3",
            "isVisible": true,
            "left": "15dp",
            "skin": "sknLbl485C75LatoRegular13Px",
            "i18n_text": "kony.i18n.getLocalizedString(\"i18n.konybb.weeklyTransaction\")",
            "top": "0dp",
            "width": "130dp",
            "zIndex": 1
        }, {
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, {});
        var flxLimitValue3 = new kony.ui.FlexContainer({
            "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
            "clipBounds": true,
            "id": "flxLimitValue3",
            "isVisible": true,
            "layoutType": kony.flex.FLOW_HORIZONTAL,
            "left": "160dp",
            "isModalContainer": false,
            "skin": "slFbox",
            "top": "0dp",
            "width": "200dp"
        }, {}, {});
        flxLimitValue3.setDefaultUnit(kony.flex.DP);
        var lblCurrency3 = new kony.ui.Label({
            "id": "lblCurrency3",
            "isVisible": true,
            "left": "0dp",
            "skin": "sknIcon485c75Info16px",
            "text": "$",
            "top": "0dp",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, {
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, {});
        var lblValue3 = new kony.ui.Label({
            "id": "lblValue3",
            "isVisible": true,
            "left": "10dp",
            "skin": "sknLbl485C75LatoRegular13Px",
            "text": "2000",
            "top": "1dp",
            "width": "80%",
            "zIndex": 1
        }, {
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, {});
        flxLimitValue3.add(lblCurrency3, lblValue3);
        flxLimitsRow3.add(lblRowHeading3, flxLimitValue3);
        flxLimitsContainer.add(flxLimitsRow1, flxLimitsRow2, flxLimitsRow3);
        var lblSeperator = new kony.ui.Label({
            "bottom": "0dp",
            "height": "1dp",
            "id": "lblSeperator",
            "isVisible": true,
            "left": "15dp",
            "right": "15dp",
            "skin": "sknlblSeperatorD7D9E0",
            "text": "Label"
        }, {
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, {});
        flxEnrollLimits.add(lblLimitsHeading, flxLimitsContainer, lblSeperator);
        return flxEnrollLimits;
    }
})