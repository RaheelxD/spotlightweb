define("flxServicesScheduleMaster", function() {
    return function(controller) {
        var flxServicesScheduleMaster = new kony.ui.FlexContainer({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "clipBounds": true,
            "height": "50dp",
            "id": "flxServicesScheduleMaster",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "isModalContainer": false,
            "skin": "CopyslFbox0d1045f833ae342"
        }, {}, {});
        flxServicesScheduleMaster.setDefaultUnit(kony.flex.DP);
        var flxServicesScheduleMasterContent = new kony.ui.FlexContainer({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "centerX": "50%",
            "centerY": "50%",
            "clipBounds": true,
            "height": "80%",
            "id": "flxServicesScheduleMasterContent",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "left": "0dp",
            "isModalContainer": false,
            "skin": "CopyslFbox0i009fcabe31541",
            "top": "0dp",
            "width": "100%",
            "zIndex": 1
        }, {}, {});
        flxServicesScheduleMasterContent.setDefaultUnit(kony.flex.DP);
        var imgCheckbox = new kony.ui.Image2({
            "height": "20dp",
            "id": "imgCheckbox",
            "isVisible": true,
            "left": "10dp",
            "skin": "slImage",
            "src": "radionormal_1x.png",
            "top": "10dp",
            "width": "20dp",
            "zIndex": 1
        }, {
            "imageScaleMode": constants.IMAGE_SCALE_MODE_MAINTAIN_ASPECT_RATIO,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, {});
        var lblMasterData = new kony.ui.Label({
            "centerY": "50%",
            "height": "20dp",
            "id": "lblMasterData",
            "isVisible": true,
            "left": "40dp",
            "skin": "CopyslLabel0j61bbe238e4540",
            "text": "Master Data",
            "top": "6dp",
            "width": "160dp",
            "zIndex": 1
        }, {
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, {});
        flxServicesScheduleMasterContent.add(imgCheckbox, lblMasterData);
        flxServicesScheduleMaster.add(flxServicesScheduleMasterContent);
        return flxServicesScheduleMaster;
    }
})