define("flxOptionValue", function() {
    return function(controller) {
        var flxOptionValue = new kony.ui.FlexContainer({
            "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
            "clipBounds": true,
            "id": "flxOptionValue",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "left": "0dp",
            "isModalContainer": false,
            "skin": "sknBackgroundFFFFFF",
            "top": "0dp",
            "width": "100%"
        }, {}, {});
        flxOptionValue.setDefaultUnit(kony.flex.DP);
        var flxValue = new kony.ui.FlexContainer({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "clipBounds": true,
            "height": "20px",
            "id": "flxValue",
            "isVisible": true,
            "layoutType": kony.flex.FLOW_HORIZONTAL,
            "left": "10px",
            "isModalContainer": false,
            "skin": "slFbox",
            "top": "12px",
            "width": kony.flex.USE_PREFFERED_SIZE
        }, {}, {});
        flxValue.setDefaultUnit(kony.flex.DP);
        var lblValue = new kony.ui.Label({
            "centerY": "50%",
            "id": "lblValue",
            "isVisible": true,
            "left": "0px",
            "skin": "sknLbl485C75LatoRegular13Px",
            "text": "Value",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, {
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, {});
        var lblDefault = new kony.ui.Label({
            "height": "20px",
            "id": "lblDefault",
            "isVisible": false,
            "left": "15px",
            "skin": "sknLblFontFFFFFF13pxBg1F844DRoundCorner",
            "text": "Default",
            "top": "0px",
            "width": "62px",
            "zIndex": 1
        }, {
            "contentAlignment": constants.CONTENT_ALIGN_CENTER,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, {});
        flxValue.add(lblValue, lblDefault);
        var lblDescription = new kony.ui.Label({
            "bottom": "15px",
            "id": "lblDescription",
            "isVisible": true,
            "left": "30%",
            "skin": "sknLbl485C75LatoRegular13Px",
            "text": "Description",
            "top": "15px",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, {
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, {});
        var lblSeparator = new kony.ui.Label({
            "bottom": "0px",
            "height": "1px",
            "id": "lblSeparator",
            "isVisible": true,
            "left": "0px",
            "right": "0px",
            "skin": "sknlblSeperatorD7D9E0",
            "text": "Label",
            "zIndex": 2
        }, {
            "contentAlignment": constants.CONTENT_ALIGN_TOP_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, {});
        flxOptionValue.add(flxValue, lblDescription, lblSeparator);
        return flxOptionValue;
    }
})