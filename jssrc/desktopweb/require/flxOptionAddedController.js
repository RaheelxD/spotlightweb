define("userflxOptionAddedController", {
    removeRow: function() {
        var index = kony.application.getCurrentForm().segSelectedOptions.selectedIndex;
        var rowIndex = index[0];
        kony.application.getCurrentForm().segSelectedOptions.removeAt(rowIndex);
    }
});
define("flxOptionAddedControllerActions", {
    /*
        This is an auto generated file and any modifications to it may result in corruption of the action sequence.
    */
    AS_FlexContainer_d01abe772a6a4c09b140ae1057a7a0f2: function AS_FlexContainer_d01abe772a6a4c09b140ae1057a7a0f2(eventobject, context) {
        var self = this;
        this.executeOnParent("unSelectedOption");
    }
});
define("flxOptionAddedController", ["userflxOptionAddedController", "flxOptionAddedControllerActions"], function() {
    var controller = require("userflxOptionAddedController");
    var controllerActions = ["flxOptionAddedControllerActions"];
    return kony.visualizer.mixinControllerActions(controller, controllerActions);
});
