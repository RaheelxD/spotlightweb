define("ProfileManagement/frmCreateProfile", function() {
    return function(controller) {
        function addWidgetsfrmCreateProfile() {
            this.setDefaultUnit(kony.flex.DP);
            var flxLeftPannel = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "100%",
                "id": "flxLeftPannel",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "masterType": constants.MASTER_TYPE_USERWIDGET,
                "isModalContainer": false,
                "skin": "sknLeftPannel0bb79e4bcca8e45",
                "top": "0dp",
                "width": "305dp",
                "zIndex": 5
            }, {}, {});
            flxLeftPannel.setDefaultUnit(kony.flex.DP);
            var leftMenuNew = new com.adminConsole.navigation.leftMenuNew({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "height": "100%",
                "id": "leftMenuNew",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "masterType": constants.MASTER_TYPE_DEFAULT,
                "isModalContainer": false,
                "skin": "sknFlxBg003E75Op100",
                "top": "0dp",
                "width": "100%",
                "overrides": {
                    "brandLogo": {
                        "src": "infinity_dbx_c360_logo_white_2x.png"
                    },
                    "flxScrollMenu": {
                        "bottom": "40dp",
                        "left": "0dp",
                        "top": "80dp"
                    },
                    "imgBottomLogo": {
                        "src": "logo_white_2x.png"
                    },
                    "leftMenuNew": {
                        "bottom": "viz.val_cleared",
                        "centerX": "viz.val_cleared",
                        "centerY": "viz.val_cleared",
                        "height": "100%",
                        "left": "0dp",
                        "maxHeight": "viz.val_cleared",
                        "maxWidth": "viz.val_cleared",
                        "minHeight": "viz.val_cleared",
                        "minWidth": "viz.val_cleared",
                        "right": "viz.val_cleared",
                        "top": "0dp",
                        "width": "100%"
                    }
                }
            }, {
                "overrides": {}
            }, {
                "overrides": {}
            });
            flxLeftPannel.add(leftMenuNew);
            var flxRightPannel = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "100%",
                "id": "flxRightPannel",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "305px",
                "isModalContainer": false,
                "right": "0dp",
                "skin": "slFbox0b7aaa2e3bfa340",
                "zIndex": 5
            }, {}, {});
            flxRightPannel.setDefaultUnit(kony.flex.DP);
            var flxHeaderDropdown = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                "clipBounds": true,
                "id": "flxHeaderDropdown",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "isModalContainer": false,
                "right": "35dp",
                "skin": "slFbox",
                "top": "20dp",
                "width": "400dp",
                "zIndex": 5
            }, {}, {});
            flxHeaderDropdown.setDefaultUnit(kony.flex.DP);
            var dropdownMainHeader = new com.adminConsole.common.dropdownMainHeader({
                "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                "id": "dropdownMainHeader",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "masterType": constants.MASTER_TYPE_DEFAULT,
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0dp",
                "width": "100%",
                "overrides": {
                    "flxDropdown": {
                        "isVisible": false
                    },
                    "imgUpArrow": {
                        "src": "uparrow_2x.png"
                    }
                }
            }, {
                "overrides": {}
            }, {
                "overrides": {}
            });
            flxHeaderDropdown.add(dropdownMainHeader);
            var flxMainHeader = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "106dp",
                "id": "flxMainHeader",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0dp",
                "width": "100%",
                "zIndex": 1
            }, {}, {});
            flxMainHeader.setDefaultUnit(kony.flex.DP);
            var mainHeader = new com.adminConsole.header.mainHeader({
                "height": "106px",
                "id": "mainHeader",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "masterType": constants.MASTER_TYPE_DEFAULT,
                "isModalContainer": false,
                "skin": "CopyslFbox1",
                "width": "100%",
                "overrides": {
                    "btnAddNewOption": {
                        "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmAdManagement.DefaultCampaignCAPS\")",
                        "isVisible": false,
                        "right": "212px"
                    },
                    "btnDropdownList": {
                        "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmAdManagement.CreateAdCampaignCAPS\")"
                    },
                    "flxButtons": {
                        "isVisible": false,
                        "right": "100dp",
                        "top": "55px",
                        "width": "420dp"
                    },
                    "flxMainHeader": {
                        "left": undefined,
                        "top": undefined
                    },
                    "imgLogout": {
                        "right": "0px",
                        "src": "img_logout.png"
                    },
                    "lblHeading": {
                        "centerY": "viz.val_cleared",
                        "height": kony.flex.USE_PREFFERED_SIZE,
                        "i18n_text": "kony.i18n.getLocalizedString(\"i18n.ProfileManagement.createSegment\")",
                        "top": "55px",
                        "width": kony.flex.USE_PREFFERED_SIZE
                    },
                    "lblUserName": {
                        "right": "25px",
                        "text": "Preetish",
                        "top": "viz.val_cleared"
                    },
                    "mainHeader": {
                        "left": undefined,
                        "top": undefined
                    }
                }
            }, {
                "overrides": {}
            }, {
                "overrides": {}
            });
            flxMainHeader.add(mainHeader);
            var flxBreadCrumbs = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "40px",
                "id": "flxBreadCrumbs",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "masterType": constants.MASTER_TYPE_USERWIDGET,
                "isModalContainer": false,
                "skin": "sknflxf5f6f8Op100",
                "top": "95px",
                "width": "100%",
                "zIndex": 1
            }, {}, {});
            flxBreadCrumbs.setDefaultUnit(kony.flex.DP);
            var breadcrumbs = new com.adminConsole.common.breadcrumbs({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "height": "20px",
                "id": "breadcrumbs",
                "isVisible": true,
                "layoutType": kony.flex.FLOW_HORIZONTAL,
                "left": "0dp",
                "masterType": constants.MASTER_TYPE_DEFAULT,
                "isModalContainer": false,
                "skin": "sknflxf5f6f8Op100",
                "top": "1px",
                "width": "100%",
                "overrides": {
                    "breadcrumbs": {
                        "bottom": "viz.val_cleared",
                        "top": "1px",
                        "width": "100%"
                    },
                    "btnBackToMain": {
                        "i18n_text": "kony.i18n.getLocalizedString(\"i18n.ProfileManagement.segmentsCaps\")"
                    },
                    "imgBreadcrumbsDown": {
                        "src": "img_down_arrow.png"
                    },
                    "imgBreadcrumbsRight": {
                        "left": "10dp",
                        "right": 10,
                        "src": "img_breadcrumb_arrow.png"
                    },
                    "imgBreadcrumbsRight2": {
                        "left": "10dp",
                        "right": 10,
                        "src": "img_breadcrumb_arrow.png"
                    },
                    "imgBreadcrumbsRight3": {
                        "src": "img_breadcrumb_arrow.png"
                    },
                    "lblCurrentScreen": {
                        "i18n_text": "kony.i18n.getLocalizedString(\"i18n.common.newCAPS\")"
                    }
                }
            }, {
                "overrides": {}
            }, {
                "overrides": {}
            });
            var flxHeaderSeperator = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "bottom": "1px",
                "clipBounds": true,
                "height": "1px",
                "id": "flxHeaderSeperator",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "35dp",
                "isModalContainer": false,
                "right": "35dp",
                "skin": "sknflx115678",
                "zIndex": 1
            }, {}, {});
            flxHeaderSeperator.setDefaultUnit(kony.flex.DP);
            flxHeaderSeperator.add();
            flxBreadCrumbs.add(breadcrumbs, flxHeaderSeperator);
            var flxCreateProfile = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "bottom": "35dp",
                "clipBounds": true,
                "id": "flxCreateProfile",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "35dp",
                "isModalContainer": false,
                "right": "35dp",
                "skin": "sknFlxBgFFFFFFBorE4E6ECsz1pxR3px",
                "top": "130dp"
            }, {}, {});
            flxCreateProfile.setDefaultUnit(kony.flex.DP);
            var flxAddProfile = new kony.ui.FlexScrollContainer({
                "allowHorizontalBounce": false,
                "allowVerticalBounce": true,
                "bottom": "80dp",
                "bounces": true,
                "clipBounds": true,
                "enableScrolling": true,
                "horizontalScrollIndicator": true,
                "id": "flxAddProfile",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "pagingEnabled": false,
                "right": "0dp",
                "scrollDirection": kony.flex.SCROLL_VERTICAL,
                "skin": "slFSbox",
                "top": "0dp",
                "verticalScrollIndicator": true,
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 10
            }, {}, {});
            flxAddProfile.setDefaultUnit(kony.flex.DP);
            var flxProfileName = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                "clipBounds": true,
                "id": "flxProfileName",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "20dp",
                "isModalContainer": false,
                "right": "20dp",
                "skin": "slFbox",
                "top": "20dp"
            }, {}, {});
            flxProfileName.setDefaultUnit(kony.flex.DP);
            var lblProfileName = new kony.ui.Label({
                "id": "lblProfileName",
                "isVisible": true,
                "left": "0dp",
                "skin": "sknLblLato485c7513px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.ProfileManagement.segmentName\")",
                "top": "0dp",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var lblProfileNameSize = new kony.ui.Label({
                "id": "lblProfileNameSize",
                "isVisible": false,
                "right": "0dp",
                "skin": "sknLblLato485c7513px",
                "text": "0/50",
                "top": "0dp",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxProfileName.add(lblProfileName, lblProfileNameSize);
            var txtProfileName = new kony.ui.TextBox2({
                "autoCapitalize": constants.TEXTBOX_AUTO_CAPITALIZE_NONE,
                "focusSkin": "skntxtbxDetailsFocus14px",
                "height": "40dp",
                "id": "txtProfileName",
                "isVisible": true,
                "keyBoardStyle": constants.TEXTBOX_KEY_BOARD_STYLE_DEFAULT,
                "left": "20dp",
                "maxTextLength": 50,
                "i18n_placeholder": "kony.i18n.getLocalizedString(\"i18n.ProfileManagement.segmentName\")",
                "right": "20dp",
                "secureTextEntry": false,
                "skin": "txtD7d9e0",
                "textInputMode": constants.TEXTBOX_INPUT_MODE_ANY,
                "top": "51dp",
                "zIndex": 1
            }, {
                "containerHeightMode": constants.TEXTBOX_FONT_METRICS_DRIVEN_HEIGHT,
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [2, 0, 0, 0],
                "paddingInPixel": false
            }, {
                "autoCorrect": false,
                "placeholderSkin": "sknTextGreyb2bdcd"
            });
            var ProfileNameError = new com.adminConsole.loans.applications.errorMsg({
                "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                "id": "ProfileNameError",
                "isVisible": false,
                "layoutType": kony.flex.FLOW_HORIZONTAL,
                "left": "20dp",
                "masterType": constants.MASTER_TYPE_DEFAULT,
                "isModalContainer": false,
                "right": "20dp",
                "skin": "slFbox",
                "top": "95dp",
                "overrides": {
                    "errorMsg": {
                        "isVisible": false,
                        "left": "20dp",
                        "right": "20dp",
                        "top": "95dp",
                        "width": "viz.val_cleared"
                    },
                    "lblErrorText": {
                        "i18n_text": "kony.i18n.getLocalizedString(\"i18n.ProfileManagement.errMsgFieldCantEmpty\")"
                    }
                }
            }, {
                "overrides": {}
            }, {
                "overrides": {}
            });
            var flxProfileDescription = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                "clipBounds": true,
                "id": "flxProfileDescription",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "20dp",
                "isModalContainer": false,
                "right": "20dp",
                "skin": "slFbox",
                "top": "124dp"
            }, {}, {});
            flxProfileDescription.setDefaultUnit(kony.flex.DP);
            var lblProfileDescription = new kony.ui.Label({
                "id": "lblProfileDescription",
                "isVisible": true,
                "left": "0dp",
                "skin": "sknLblLato485c7513px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.common.description\")",
                "top": "0dp",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var lblProfileDescSize = new kony.ui.Label({
                "id": "lblProfileDescSize",
                "isVisible": false,
                "right": "0dp",
                "skin": "sknLblLato485c7513px",
                "text": "0/150",
                "top": "0dp",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxProfileDescription.add(lblProfileDescription, lblProfileDescSize);
            var txtProfileDescription = new kony.ui.TextArea2({
                "autoCapitalize": constants.TEXTAREA_AUTO_CAPITALIZE_NONE,
                "focusSkin": "sknFocus",
                "height": "70dp",
                "id": "txtProfileDescription",
                "isVisible": true,
                "keyBoardStyle": constants.TEXTAREA_KEY_BOARD_STYLE_DEFAULT,
                "left": "20dp",
                "maxTextLength": 255,
                "numberOfVisibleLines": 3,
                "i18n_placeholder": "kony.i18n.getLocalizedString(\"i18n.common.description\")",
                "right": "20dp",
                "skin": "skntxtAread7d9e0",
                "textInputMode": constants.TEXTAREA_INPUT_MODE_ANY,
                "top": "152dp",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_TOP_LEFT,
                "padding": [2, 2, 2, 2],
                "paddingInPixel": false
            }, {
                "autoCorrect": false,
                "placeholderSkin": "defTextAreaFocus"
            });
            var ProfileDescError = new com.adminConsole.loans.applications.errorMsg({
                "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                "id": "ProfileDescError",
                "isVisible": false,
                "layoutType": kony.flex.FLOW_HORIZONTAL,
                "left": "20dp",
                "masterType": constants.MASTER_TYPE_DEFAULT,
                "isModalContainer": false,
                "right": "20dp",
                "skin": "slFbox",
                "top": "227dp",
                "overrides": {
                    "errorMsg": {
                        "isVisible": false,
                        "left": "20dp",
                        "right": "20dp",
                        "top": "227dp",
                        "width": "viz.val_cleared"
                    },
                    "lblErrorText": {
                        "i18n_text": "kony.i18n.getLocalizedString(\"i18n.ProfileManagement.errMsgFieldCantEmpty\")"
                    }
                }
            }, {
                "overrides": {}
            }, {
                "overrides": {}
            });
            var flxAttrSection = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "30dp",
                "id": "flxAttrSection",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "20dp",
                "isModalContainer": false,
                "right": "20dp",
                "skin": "slFbox",
                "top": "245dp"
            }, {}, {});
            flxAttrSection.setDefaultUnit(kony.flex.DP);
            var lblAttributes = new kony.ui.Label({
                "centerY": "50%",
                "id": "lblAttributes",
                "isVisible": true,
                "left": "0dp",
                "skin": "sknLblLato485c7513px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmAdManagement.Attributes\")",
                "top": "0dp",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var btnAddMoreAttrs = new kony.ui.Button({
                "focusSkin": "sknbtnffffffLatoRegular4f555dBorder1px485c75",
                "height": "22dp",
                "id": "btnAddMoreAttrs",
                "isVisible": false,
                "right": 0,
                "skin": "sknbtnf7f7faLatoRegular4f555dBorder1px8b96a5",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.ConfigurationBundles.edit\")",
                "top": "0",
                "width": "60dp",
                "zIndex": 10
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_CENTER,
                "displayText": true,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {
                "hoverSkin": "sknbtnffffffLatoRegular4f555dBorder1px485c75"
            });
            flxAttrSection.add(lblAttributes, btnAddMoreAttrs);
            var flxAttributesContainer = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                "clipBounds": true,
                "id": "flxAttributesContainer",
                "isVisible": true,
                "layoutType": kony.flex.FLOW_VERTICAL,
                "left": 20,
                "isModalContainer": false,
                "right": "20dp",
                "skin": "slFbox",
                "top": "284dp",
                "zIndex": 1
            }, {}, {});
            flxAttributesContainer.setDefaultUnit(kony.flex.DP);
            var flxNoAttributes = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "200dp",
                "id": "flxNoAttributes",
                "isVisible": true,
                "layoutType": kony.flex.FLOW_VERTICAL,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "sknflxf8f9fabkgdddee0border3pxradius",
                "top": "0dp",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {}, {});
            flxNoAttributes.setDefaultUnit(kony.flex.DP);
            var lblNoAttributes = new kony.ui.Label({
                "centerX": "50%",
                "id": "lblNoAttributes",
                "isVisible": true,
                "skin": "sknLbl485C75LatoRegular13Px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmAdManagement.NoAttributes\")",
                "top": "80dp",
                "width": "175dp",
                "zIndex": 10
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var btnAddAttibutes = new kony.ui.Button({
                "centerX": "50%",
                "focusSkin": "sknbtnffffffLatoRegular4f555dBorder1px485c75",
                "height": "30dp",
                "id": "btnAddAttibutes",
                "isVisible": true,
                "skin": "sknbtnf7f7faLatoRegular4f555dBorder1px8b96a5",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmAdManagement.AddDatasetsAndAttributes\")",
                "top": "15dp",
                "width": "190dp",
                "zIndex": 10
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_CENTER,
                "displayText": true,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {
                "hoverSkin": "sknbtnffffffLatoRegular4f555dBorder1px485c75"
            });
            flxNoAttributes.add(lblNoAttributes, btnAddAttibutes);
            var flxAttrContainer = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                "clipBounds": true,
                "id": "flxAttrContainer",
                "isVisible": false,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0dp",
                "width": "100%"
            }, {}, {});
            flxAttrContainer.setDefaultUnit(kony.flex.DP);
            var flxAddAttributes = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                "clipBounds": true,
                "id": "flxAddAttributes",
                "isVisible": true,
                "layoutType": kony.flex.FLOW_VERTICAL,
                "left": "0",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {}, {});
            flxAddAttributes.setDefaultUnit(kony.flex.DP);
            var flxAttrHeaderRow = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "50dp",
                "id": "flxAttrHeaderRow",
                "isVisible": true,
                "layoutType": kony.flex.FLOW_HORIZONTAL,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0",
                "width": "100%"
            }, {}, {});
            flxAttrHeaderRow.setDefaultUnit(kony.flex.DP);
            var flxDatasetsHeader = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "100%",
                "id": "flxDatasetsHeader",
                "isVisible": true,
                "layoutType": kony.flex.FLOW_HORIZONTAL,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0dp",
                "width": "24%"
            }, {}, {});
            flxDatasetsHeader.setDefaultUnit(kony.flex.DP);
            var lblDatasetHeader = new kony.ui.Label({
                "id": "lblDatasetHeader",
                "isVisible": true,
                "left": "0",
                "skin": "sknLbl73757C11px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmAdManagement.DatasetCAPS\")",
                "top": "21dp",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var lblFilterDataset = new kony.ui.Label({
                "centerY": "50%",
                "id": "lblFilterDataset",
                "isVisible": true,
                "left": "7dp",
                "skin": "sknIcon15px",
                "text": "",
                "top": "21dp",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {
                "hoverSkin": "sknlblCursorFont"
            });
            flxDatasetsHeader.add(lblDatasetHeader, lblFilterDataset);
            var flxAttrHeader = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "100%",
                "id": "flxAttrHeader",
                "isVisible": true,
                "layoutType": kony.flex.FLOW_HORIZONTAL,
                "left": "1%",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0dp",
                "width": "24%"
            }, {}, {});
            flxAttrHeader.setDefaultUnit(kony.flex.DP);
            var lblAttributeHeader = new kony.ui.Label({
                "id": "lblAttributeHeader",
                "isVisible": true,
                "left": "0dp",
                "skin": "sknLbl73757C11px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmAdManagement.AttributeCAPS\")",
                "top": "21dp",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var lblSortAttribute = new kony.ui.Label({
                "id": "lblSortAttribute",
                "isVisible": true,
                "left": "7dp",
                "skin": "sknIcon15px",
                "text": "",
                "top": "21dp",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {
                "hoverSkin": "sknlblCursorFont"
            });
            flxAttrHeader.add(lblAttributeHeader, lblSortAttribute);
            var lblAttributeCriteriaHeader = new kony.ui.Label({
                "id": "lblAttributeCriteriaHeader",
                "isVisible": true,
                "left": "1%",
                "skin": "sknLbl73757C11px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmAdManagement.CriteriaCAPS\")",
                "top": "21dp",
                "width": "24%",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var lblAttributeValueHeader = new kony.ui.Label({
                "id": "lblAttributeValueHeader",
                "isVisible": true,
                "left": "1%",
                "skin": "sknLbl73757C11px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmAdManagement.ValueCAPS\")",
                "top": "21dp",
                "width": "24%",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxAttrHeaderRow.add(flxDatasetsHeader, flxAttrHeader, lblAttributeCriteriaHeader, lblAttributeValueHeader);
            var lblDatasetHeaderSeparator = new kony.ui.Label({
                "height": "1px",
                "id": "lblDatasetHeaderSeparator",
                "isVisible": true,
                "left": "0dp",
                "skin": "sknLblTableHeaderLine",
                "text": ".",
                "top": "0dp",
                "width": "100%",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var segSelectedAttributes = new kony.ui.SegmentedUI2({
                "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                "data": [{
                    "lblAttribute": "Attribute",
                    "lblCriteria": "Criteria",
                    "lblDataset": "DATASET",
                    "lblValue": "Value"
                }, {
                    "lblAttribute": "Attribute",
                    "lblCriteria": "Criteria",
                    "lblDataset": "DATASET",
                    "lblValue": "Value"
                }, {
                    "lblAttribute": "Attribute",
                    "lblCriteria": "Criteria",
                    "lblDataset": "DATASET",
                    "lblValue": "Value"
                }, {
                    "lblAttribute": "Attribute",
                    "lblCriteria": "Criteria",
                    "lblDataset": "DATASET",
                    "lblValue": "Value"
                }, {
                    "lblAttribute": "Attribute",
                    "lblCriteria": "Criteria",
                    "lblDataset": "DATASET",
                    "lblValue": "Value"
                }, {
                    "lblAttribute": "Attribute",
                    "lblCriteria": "Criteria",
                    "lblDataset": "DATASET",
                    "lblValue": "Value"
                }, {
                    "lblAttribute": "Attribute",
                    "lblCriteria": "Criteria",
                    "lblDataset": "DATASET",
                    "lblValue": "Value"
                }, {
                    "lblAttribute": "Attribute",
                    "lblCriteria": "Criteria",
                    "lblDataset": "DATASET",
                    "lblValue": "Value"
                }, {
                    "lblAttribute": "Attribute",
                    "lblCriteria": "Criteria",
                    "lblDataset": "DATASET",
                    "lblValue": "Value"
                }, {
                    "lblAttribute": "Attribute",
                    "lblCriteria": "Criteria",
                    "lblDataset": "DATASET",
                    "lblValue": "Value"
                }],
                "groupCells": false,
                "id": "segSelectedAttributes",
                "isVisible": true,
                "left": "0dp",
                "needPageIndicator": true,
                "pageOffDotImage": "pageoffdot.png",
                "pageOnDotImage": "pageondot.png",
                "retainSelection": false,
                "rowFocusSkin": "seg2Focus",
                "rowSkin": "seg2Normal",
                "rowTemplate": "flxAttributeRow",
                "sectionHeaderSkin": "sliPhoneSegmentHeader",
                "selectionBehavior": constants.SEGUI_DEFAULT_BEHAVIOR,
                "separatorColor": "d7d9e000",
                "separatorRequired": true,
                "separatorThickness": 1,
                "showScrollbars": false,
                "top": "0dp",
                "viewType": constants.SEGUI_VIEW_TYPE_TABLEVIEW,
                "widgetDataMap": {
                    "flxAttributeRow": "flxAttributeRow",
                    "flxRow": "flxRow",
                    "lblAttribute": "lblAttribute",
                    "lblCriteria": "lblCriteria",
                    "lblDataset": "lblDataset",
                    "lblValue": "lblValue"
                },
                "width": "100%",
                "zIndex": 1
            }, {
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxAddAttributes.add(flxAttrHeaderRow, lblDatasetHeaderSeparator, segSelectedAttributes);
            var flxDatasetFilter = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                "clipBounds": true,
                "id": "flxDatasetFilter",
                "isVisible": false,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "maxWidth": "50%",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "35dp",
                "width": "25%"
            }, {}, {});
            flxDatasetFilter.setDefaultUnit(kony.flex.DP);
            var datasetFilter = new com.adminConsole.common.statusFilterMenu({
                "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                "id": "datasetFilter",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "masterType": constants.MASTER_TYPE_DEFAULT,
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0dp",
                "width": "100%",
                "zIndex": 5,
                "overrides": {
                    "imgUpArrow": {
                        "left": "57dp",
                        "right": "viz.val_cleared",
                        "src": "uparrow_2x.png"
                    },
                    "statusFilterMenu": {
                        "isVisible": true,
                        "maxWidth": "viz.val_cleared",
                        "top": "0dp",
                        "width": "100%"
                    }
                }
            }, {
                "overrides": {}
            }, {
                "overrides": {}
            });
            flxDatasetFilter.add(datasetFilter);
            flxAttrContainer.add(flxAddAttributes, flxDatasetFilter);
            var NoAttributesError = new com.adminConsole.loans.applications.errorMsg({
                "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                "id": "NoAttributesError",
                "isVisible": false,
                "layoutType": kony.flex.FLOW_HORIZONTAL,
                "left": "0dp",
                "masterType": constants.MASTER_TYPE_DEFAULT,
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "10dp",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "overrides": {
                    "errorMsg": {
                        "isVisible": false
                    },
                    "lblErrorText": {
                        "i18n_text": "kony.i18n.getLocalizedString(\"i18n.ProfileManagement.errMsgNoAttributesAdded\")"
                    }
                }
            }, {
                "overrides": {}
            }, {
                "overrides": {}
            });
            flxAttributesContainer.add(flxNoAttributes, flxAttrContainer, NoAttributesError);
            flxAddProfile.add(flxProfileName, txtProfileName, ProfileNameError, flxProfileDescription, txtProfileDescription, ProfileDescError, flxAttrSection, flxAttributesContainer);
            var flxButtons = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "bottom": 0,
                "clipBounds": true,
                "height": "80dp",
                "id": "flxButtons",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "flxe4e6ec1px",
                "width": "100%",
                "zIndex": 11
            }, {}, {});
            flxButtons.setDefaultUnit(kony.flex.DP);
            var commonButtons = new com.adminConsole.common.commonButtons({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "height": "80px",
                "id": "commonButtons",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "masterType": constants.MASTER_TYPE_DEFAULT,
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0dp",
                "width": "100%",
                "overrides": {
                    "btnCancel": {
                        "isVisible": true,
                        "left": "20dp"
                    },
                    "btnNext": {
                        "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmAdManagement.CretaeProfileCAPS\")",
                        "isVisible": false,
                        "left": "viz.val_cleared",
                        "right": "0dp"
                    },
                    "btnSave": {
                        "i18n_text": "kony.i18n.getLocalizedString(\"i18n.ProfileManagement.createSegmentCaps\")",
                        "isVisible": true,
                        "right": "0dp",
                        "width": "170px"
                    },
                    "commonButtons": {
                        "bottom": "viz.val_cleared",
                        "top": "0dp"
                    },
                    "flxRightButtons": {
                        "right": "20px",
                        "width": "250px"
                    }
                }
            }, {
                "overrides": {}
            }, {
                "overrides": {}
            });
            flxButtons.add(commonButtons);
            flxCreateProfile.add(flxAddProfile, flxButtons);
            var flxLoading = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "100%",
                "id": "flxLoading",
                "isVisible": false,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "sknLoadingBlur",
                "top": "0dp",
                "width": "100%",
                "zIndex": 200
            }, {}, {});
            flxLoading.setDefaultUnit(kony.flex.DP);
            var flxImageContainer = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "centerX": "50%",
                "centerY": "50%",
                "clipBounds": true,
                "height": "75px",
                "id": "flxImageContainer",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "isModalContainer": false,
                "skin": "slFbox",
                "width": "75px",
                "zIndex": 1
            }, {}, {});
            flxImageContainer.setDefaultUnit(kony.flex.DP);
            var imgLoading = new kony.ui.Image2({
                "centerX": "50%",
                "centerY": "50%",
                "height": "50px",
                "id": "imgLoading",
                "isVisible": true,
                "skin": "slImage",
                "src": "loadingscreenimage.gif",
                "width": "50px",
                "zIndex": 1
            }, {
                "imageScaleMode": constants.IMAGE_SCALE_MODE_MAINTAIN_ASPECT_RATIO,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxImageContainer.add(imgLoading);
            flxLoading.add(flxImageContainer);
            var flxToastMessage = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "bottom": "0px",
                "centerX": "50%",
                "clipBounds": true,
                "height": "70px",
                "id": "flxToastMessage",
                "isVisible": false,
                "layoutType": kony.flex.FREE_FORM,
                "isModalContainer": false,
                "skin": "slFbox",
                "width": "1413px",
                "zIndex": 1
            }, {}, {});
            flxToastMessage.setDefaultUnit(kony.flex.DP);
            var flxToastContainer = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "centerX": "50%",
                "centerY": "50%",
                "clipBounds": true,
                "height": "45px",
                "id": "flxToastContainer",
                "isVisible": false,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0%",
                "isModalContainer": false,
                "skin": "sknflxSuccessToast1F844D",
                "top": "0%",
                "width": "60%",
                "zIndex": 1
            }, {}, {});
            flxToastContainer.setDefaultUnit(kony.flex.DP);
            var imgLeft = new kony.ui.Image2({
                "centerY": "50%",
                "height": "15dp",
                "id": "imgLeft",
                "isVisible": true,
                "left": "15px",
                "skin": "slImage",
                "src": "arrow2x.png",
                "top": "0dp",
                "width": "15dp",
                "zIndex": 1
            }, {
                "imageScaleMode": constants.IMAGE_SCALE_MODE_MAINTAIN_ASPECT_RATIO,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var lbltoastMessage = new kony.ui.Label({
                "centerY": "50%",
                "id": "lbltoastMessage",
                "isVisible": true,
                "left": "45px",
                "right": "45px",
                "skin": "lblfffffflatoregular14px",
                "text": "Label",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var imgRight = new kony.ui.Image2({
                "centerY": "50%",
                "height": "15px",
                "id": "imgRight",
                "isVisible": true,
                "right": "15px",
                "skin": "slImage",
                "src": "close_small2x.png",
                "top": "0dp",
                "width": "15px",
                "zIndex": 1
            }, {
                "imageScaleMode": constants.IMAGE_SCALE_MODE_MAINTAIN_ASPECT_RATIO,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxToastContainer.add(imgLeft, lbltoastMessage, imgRight);
            var toastMessage = new com.adminConsole.common.toastMessage({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "id": "toastMessage",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "masterType": constants.MASTER_TYPE_DEFAULT,
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0dp",
                "overrides": {
                    "lbltoastMessage": {
                        "text": "label"
                    },
                    "toastMessage": {
                        "isVisible": true
                    }
                }
            }, {
                "overrides": {}
            }, {
                "overrides": {}
            });
            flxToastMessage.add(flxToastContainer, toastMessage);
            var flxSelectOptions = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                "clipBounds": true,
                "id": "flxSelectOptions",
                "isVisible": false,
                "layoutType": kony.flex.FREE_FORM,
                "isModalContainer": false,
                "right": "50dp",
                "skin": "slFbox",
                "top": "325dp",
                "width": "165dp",
                "zIndex": 1
            }, {}, {});
            flxSelectOptions.setDefaultUnit(kony.flex.DP);
            var selectOptions = new com.adminConsole.adManagement.selectOptions({
                "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                "id": "selectOptions",
                "isVisible": true,
                "layoutType": kony.flex.FLOW_VERTICAL,
                "left": "0dp",
                "masterType": constants.MASTER_TYPE_DEFAULT,
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0dp",
                "width": "100%",
                "overrides": {
                    "imgDownArrow": {
                        "src": "downarrow_2x.png"
                    },
                    "imgUpArrow": {
                        "src": "uparrow_2x.png"
                    }
                }
            }, {
                "overrides": {}
            }, {
                "overrides": {}
            });
            flxSelectOptions.add(selectOptions);
            flxRightPannel.add(flxHeaderDropdown, flxMainHeader, flxBreadCrumbs, flxCreateProfile, flxLoading, flxToastMessage, flxSelectOptions);
            var flxDatasetPopup = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "100%",
                "id": "flxDatasetPopup",
                "isVisible": false,
                "layoutType": kony.flex.FREE_FORM,
                "masterType": constants.MASTER_TYPE_USERWIDGET,
                "isModalContainer": false,
                "skin": "skn222b35",
                "width": "100%",
                "zIndex": 10
            }, {}, {});
            flxDatasetPopup.setDefaultUnit(kony.flex.DP);
            var flxAddAttributesPopUp = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "centerX": "50%",
                "centerY": "50%",
                "clipBounds": false,
                "height": "80%",
                "id": "flxAddAttributesPopUp",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "isModalContainer": false,
                "skin": "flxNoShadowNoBorder",
                "width": "986dp",
                "zIndex": 10
            }, {}, {});
            flxAddAttributesPopUp.setDefaultUnit(kony.flex.DP);
            var flxAttributesPopUpTopColor = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "10px",
                "id": "flxAttributesPopUpTopColor",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "sknFlxInfoToastBg357C9ENoRadius",
                "top": "0dp",
                "width": "100%",
                "zIndex": 1
            }, {}, {});
            flxAttributesPopUpTopColor.setDefaultUnit(kony.flex.DP);
            flxAttributesPopUpTopColor.add();
            var flxAttributesPopUpClose = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "15px",
                "id": "flxAttributesPopUpClose",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "isModalContainer": false,
                "right": 20,
                "skin": "slFbox",
                "top": "30dp",
                "width": "15px",
                "zIndex": 1
            }, {}, {
                "hoverSkin": "sknCursor"
            });
            flxAttributesPopUpClose.setDefaultUnit(kony.flex.DP);
            var lblAttributesPopupClose = new kony.ui.Label({
                "height": "15dp",
                "id": "lblAttributesPopupClose",
                "isVisible": true,
                "left": "0dp",
                "skin": "sknfontIconListbox15px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.userwidgetmodel.lblClearSearch\")",
                "top": "0dp",
                "width": "15dp",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxAttributesPopUpClose.add(lblAttributesPopupClose);
            var lblAttributesPopUpMainMessage = new kony.ui.Label({
                "height": "19dp",
                "id": "lblAttributesPopUpMainMessage",
                "isVisible": true,
                "left": "20px",
                "skin": "sknLbl192b45LatoReg16px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmAdManagement.AddDatasetsAndAttributes\")",
                "top": "37px",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var flxAddAttributesContainer = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "bottom": "80dp",
                "clipBounds": true,
                "id": "flxAddAttributesContainer",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "77dp",
                "width": "100%",
                "zIndex": 1
            }, {}, {});
            flxAddAttributesContainer.setDefaultUnit(kony.flex.DP);
            var flxDatasetContainer = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "100%",
                "id": "flxDatasetContainer",
                "isVisible": true,
                "layoutType": kony.flex.FLOW_VERTICAL,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "flxe4e6ec1px",
                "top": "0dp",
                "width": 232,
                "zIndex": 1
            }, {}, {});
            flxDatasetContainer.setDefaultUnit(kony.flex.DP);
            var flxDatasetHeader = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "50dp",
                "id": "flxDatasetHeader",
                "isVisible": true,
                "layoutType": kony.flex.FLOW_HORIZONTAL,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0dp",
                "width": "100%"
            }, {}, {});
            flxDatasetHeader.setDefaultUnit(kony.flex.DP);
            var lblSelectDataset = new kony.ui.Label({
                "id": "lblSelectDataset",
                "isVisible": true,
                "left": "20dp",
                "skin": "sknLbl192B45LatoRegular14px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmAdManagement.selectDataSet\")",
                "top": "20dp",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var flxDatasetCount = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "20dp",
                "id": "flxDatasetCount",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "10dp",
                "isModalContainer": false,
                "skin": "sknFlx006CCA",
                "top": "20dp",
                "width": "40dp",
                "zIndex": 1
            }, {}, {});
            flxDatasetCount.setDefaultUnit(kony.flex.DP);
            var lblDatasetCount = new kony.ui.Label({
                "centerX": "50%",
                "centerY": "40%",
                "id": "lblDatasetCount",
                "isVisible": true,
                "skin": "sknLblffffff13px",
                "text": "00",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxDatasetCount.add(lblDatasetCount);
            var flxReset = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                "clipBounds": true,
                "id": "flxReset",
                "isVisible": true,
                "layoutType": kony.flex.FLOW_HORIZONTAL,
                "left": "20dp",
                "isModalContainer": false,
                "skin": "hoverhandSkin",
                "top": "20dp",
                "width": "35dp"
            }, {}, {});
            flxReset.setDefaultUnit(kony.flex.DP);
            var lblReset = new kony.ui.Label({
                "id": "lblReset",
                "isVisible": true,
                "left": "0dp",
                "skin": "sknLblLatoReg117eb013px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.Common.button.Reset\")",
                "top": "0dp",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxReset.add(lblReset);
            flxDatasetHeader.add(lblSelectDataset, flxDatasetCount, flxReset);
            var flxDatasets = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                "bottom": "0px",
                "clipBounds": true,
                "id": "flxDatasets",
                "isVisible": true,
                "layoutType": kony.flex.FLOW_VERTICAL,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "sknflxBgF9F9F9",
                "top": "0dp",
                "width": "100%",
                "zIndex": 1
            }, {}, {});
            flxDatasets.setDefaultUnit(kony.flex.DP);
            var flxDatasetSearchContainer = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "60dp",
                "id": "flxDatasetSearchContainer",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "sknflxBgF9F9F9Border1pxE4E6EC",
                "top": "0dp",
                "width": "100%",
                "zIndex": 1
            }, {}, {});
            flxDatasetSearchContainer.setDefaultUnit(kony.flex.DP);
            var flxDatasetSearch = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "centerY": "50%",
                "clipBounds": true,
                "height": "40px",
                "id": "flxDatasetSearch",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "15dp",
                "isModalContainer": false,
                "skin": "sknflxd5d9ddop100",
                "width": "200px",
                "zIndex": 1
            }, {}, {});
            flxDatasetSearch.setDefaultUnit(kony.flex.DP);
            var fontIconSearchImg = new kony.ui.Label({
                "centerY": "50%",
                "id": "fontIconSearchImg",
                "isVisible": true,
                "left": "12px",
                "skin": "sknFontIconSearchImg20px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.userwidgetmodel.lblSearchIcon\")",
                "top": "0dp",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var tbxDatasetSearchBox = new kony.ui.TextBox2({
                "autoCapitalize": constants.TEXTBOX_AUTO_CAPITALIZE_NONE,
                "centerY": "50%",
                "height": "30px",
                "id": "tbxDatasetSearchBox",
                "isVisible": true,
                "keyBoardStyle": constants.TEXTBOX_KEY_BOARD_STYLE_DEFAULT,
                "left": "35dp",
                "onTouchStart": controller.AS_TextField_cfab06859f0f43eca99495891ee290d5,
                "placeholder": "Search",
                "right": "35px",
                "secureTextEntry": false,
                "skin": "skntbxffffffNoBorderlato35475f14px",
                "textInputMode": constants.TEXTBOX_INPUT_MODE_ANY,
                "zIndex": 1
            }, {
                "containerHeightMode": constants.TEXTBOX_FONT_METRICS_DRIVEN_HEIGHT,
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [2, 0, 0, 0],
                "paddingInPixel": false
            }, {
                "autoCorrect": false
            });
            var flxDatasetClearSearch = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "100%",
                "id": "flxDatasetClearSearch",
                "isVisible": false,
                "layoutType": kony.flex.FREE_FORM,
                "isModalContainer": false,
                "right": "0px",
                "skin": "sknCursor",
                "top": "0dp",
                "width": "35px",
                "zIndex": 1
            }, {}, {
                "hoverSkin": "sknCursor"
            });
            flxDatasetClearSearch.setDefaultUnit(kony.flex.DP);
            var fontIconCross = new kony.ui.Label({
                "centerY": "50%",
                "id": "fontIconCross",
                "isVisible": true,
                "left": "5px",
                "skin": "sknFontIconSearchCross16px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.userwidgetmodel.lblClearSearch\")",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxDatasetClearSearch.add(fontIconCross);
            flxDatasetSearch.add(fontIconSearchImg, tbxDatasetSearchBox, flxDatasetClearSearch);
            flxDatasetSearchContainer.add(flxDatasetSearch);
            var flxDatasetList = new kony.ui.FlexScrollContainer({
                "allowHorizontalBounce": false,
                "allowVerticalBounce": true,
                "bounces": true,
                "clipBounds": true,
                "enableScrolling": true,
                "height": "240dp",
                "horizontalScrollIndicator": true,
                "id": "flxDatasetList",
                "isVisible": true,
                "layoutType": kony.flex.FLOW_VERTICAL,
                "left": "0dp",
                "pagingEnabled": false,
                "scrollDirection": kony.flex.SCROLL_VERTICAL,
                "skin": "slFSbox",
                "top": "0dp",
                "verticalScrollIndicator": true,
                "width": "100%"
            }, {}, {});
            flxDatasetList.setDefaultUnit(kony.flex.DP);
            flxDatasetList.add();
            var flxNoDatasetFound = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "bottom": "0dp",
                "clipBounds": true,
                "id": "flxNoDatasetFound",
                "isVisible": false,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": 0,
                "width": "100%",
                "zIndex": 1
            }, {}, {});
            flxNoDatasetFound.setDefaultUnit(kony.flex.DP);
            var lblNoDatasetFound = new kony.ui.Label({
                "centerX": "50%",
                "centerY": "50%",
                "id": "lblNoDatasetFound",
                "isVisible": true,
                "skin": "sknLblLato84939e13px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.Messages.SuggestionsNoResultsFound\")",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxNoDatasetFound.add(lblNoDatasetFound);
            flxDatasets.add(flxDatasetSearchContainer, flxDatasetList, flxNoDatasetFound);
            flxDatasetContainer.add(flxDatasetHeader, flxDatasets);
            var flxAddAttributesDetailsContainer = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "100%",
                "id": "flxAddAttributesDetailsContainer",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "231dp",
                "isModalContainer": false,
                "right": "0dp",
                "skin": "flxe4e6ec1px",
                "top": "0dp",
                "zIndex": 1
            }, {}, {});
            flxAddAttributesDetailsContainer.setDefaultUnit(kony.flex.DP);
            var flxAddAttributesDetailsMain = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "100%",
                "id": "flxAddAttributesDetailsMain",
                "isVisible": true,
                "layoutType": kony.flex.FLOW_VERTICAL,
                "left": "20dp",
                "isModalContainer": false,
                "right": "20dp",
                "skin": "slFbox",
                "top": "0dp",
                "width": kony.flex.USE_PREFFERED_SIZE
            }, {}, {});
            flxAddAttributesDetailsMain.setDefaultUnit(kony.flex.DP);
            flxAddAttributesDetailsMain.add();
            var flxAddAttributesMsgContainer = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "centerX": "50%",
                "centerY": "50%",
                "clipBounds": true,
                "height": "98dp",
                "id": "flxAddAttributesMsgContainer",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "isModalContainer": false,
                "skin": "slFbox",
                "width": "360dp",
                "zIndex": 1
            }, {}, {});
            flxAddAttributesMsgContainer.setDefaultUnit(kony.flex.DP);
            var imgSelectCheckbox = new kony.ui.Image2({
                "centerX": "50%",
                "height": "67dp",
                "id": "imgSelectCheckbox",
                "isVisible": true,
                "skin": "slImage",
                "src": "selectacheckbox.png",
                "top": "0dp",
                "width": "83dp",
                "zIndex": 1
            }, {
                "imageScaleMode": constants.IMAGE_SCALE_MODE_MAINTAIN_ASPECT_RATIO,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var lblAddAttributesMsg = new kony.ui.Label({
                "bottom": "0dp",
                "id": "lblAddAttributesMsg",
                "isVisible": true,
                "left": "0dp",
                "skin": "sknlbl485C75LatoSemiBold13px",
                "text": "Select the checkbox on the left hand side to add the attributes.",
                "width": "100%",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxAddAttributesMsgContainer.add(imgSelectCheckbox, lblAddAttributesMsg);
            flxAddAttributesDetailsContainer.add(flxAddAttributesDetailsMain, flxAddAttributesMsgContainer);
            flxAddAttributesContainer.add(flxDatasetContainer, flxAddAttributesDetailsContainer);
            var flxAttributesPopUpButtons = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "bottom": "0dp",
                "clipBounds": true,
                "height": "80px",
                "id": "flxAttributesPopUpButtons",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "sknflxBgE4E6EC",
                "width": "100%",
                "zIndex": 1
            }, {}, {});
            flxAttributesPopUpButtons.setDefaultUnit(kony.flex.DP);
            var btnAttributesCancel = new kony.ui.Button({
                "centerY": "50%",
                "height": "40px",
                "id": "btnAttributesCancel",
                "isVisible": true,
                "right": "130px",
                "skin": "sknbtnf7f7faLatoRegular4f555dBorder1px8b96a5",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.permission.CANCEL\")",
                "top": "0%",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 20
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_CENTER,
                "displayText": true,
                "padding": [3, 0, 3, 0],
                "paddingInPixel": false
            }, {
                "hoverSkin": "sknbtnffffffLatoRegular4f555dBorder1px485c75"
            });
            var btnAttributesSave = new kony.ui.Button({
                "centerY": "50%",
                "focusSkin": "sknBtn003E75LatoRegular13pxFFFFFFRad20px",
                "height": "40px",
                "id": "btnAttributesSave",
                "isVisible": true,
                "minWidth": "100px",
                "right": "20px",
                "skin": "sknBtn003E75LatoRegular13pxFFFFFFRad20px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.permission.SAVE\")",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 20
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_CENTER,
                "displayText": true,
                "padding": [3, 0, 3, 0],
                "paddingInPixel": false
            }, {
                "hoverSkin": "sknBtn005198LatoRegular13pxFFFFFFRad20px"
            });
            flxAttributesPopUpButtons.add(btnAttributesCancel, btnAttributesSave);
            flxAddAttributesPopUp.add(flxAttributesPopUpTopColor, flxAttributesPopUpClose, lblAttributesPopUpMainMessage, flxAddAttributesContainer, flxAttributesPopUpButtons);
            flxDatasetPopup.add(flxAddAttributesPopUp);
            var flxWarningPopup = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "100%",
                "id": "flxWarningPopup",
                "isVisible": false,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "masterType": constants.MASTER_TYPE_USERWIDGET,
                "isModalContainer": false,
                "skin": "skn222b35",
                "top": "0dp",
                "width": "100%",
                "zIndex": 10
            }, {}, {});
            flxWarningPopup.setDefaultUnit(kony.flex.DP);
            var warningpopup = new com.adminConsole.common.popUp({
                "height": "100%",
                "id": "warningpopup",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "masterType": constants.MASTER_TYPE_DEFAULT,
                "isModalContainer": false,
                "skin": "sknflxBg000000Op50",
                "top": "0dp",
                "width": "100%",
                "zIndex": 1,
                "overrides": {
                    "btnPopUpCancel": {
                        "i18n_text": "kony.i18n.getLocalizedString(\"i18n.PopUp.NoLeaveAsIS\")",
                        "left": "viz.val_cleared",
                        "minWidth": "100px",
                        "right": "20px",
                        "top": "0%",
                        "width": kony.flex.USE_PREFFERED_SIZE
                    },
                    "btnPopUpDelete": {
                        "i18n_text": "kony.i18n.getLocalizedString(\"i18n.PopUp.YesProceed\")"
                    },
                    "flxPopUp": {
                        "width": "680px"
                    },
                    "lblPopUpMainMessage": {
                        "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmAdManagement.ReduceAdPlaceholders\")"
                    },
                    "popUp": {
                        "isVisible": true,
                        "left": "0dp",
                        "top": "0dp",
                        "width": "100%"
                    },
                    "rtxPopUpDisclaimer": {
                        "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmAdManagement.ReduceAdPlaceholdersMsg\")",
                        "width": "640dp"
                    }
                }
            }, {
                "overrides": {}
            }, {
                "overrides": {}
            });
            flxWarningPopup.add(warningpopup);
            this.add(flxLeftPannel, flxRightPannel, flxDatasetPopup, flxWarningPopup);
        };
        return [{
            "addWidgets": addWidgetsfrmCreateProfile,
            "enabledForIdleTimeout": true,
            "id": "frmCreateProfile",
            "layoutType": kony.flex.FREE_FORM,
            "needAppMenu": false,
            "preShow": function(eventobject) {
                controller.AS_Form_c413dcc6042a45bfaf9adda624840524(eventobject);
            },
            "skin": "slForm"
        }, {
            "displayOrientation": constants.FORM_DISPLAY_ORIENTATION_PORTRAIT,
            "layoutType": kony.flex.FREE_FORM,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, {
            "onDeviceBack": controller.AS_Form_cf13a2e2afa94d45a5c770e9d6162b95,
            "retainScrollPosition": false
        }]
    }
});