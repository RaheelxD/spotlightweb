define("ProfileManagement/frmProfiles", function() {
    return function(controller) {
        function addWidgetsfrmProfiles() {
            this.setDefaultUnit(kony.flex.DP);
            var flxLeftPannel = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "100%",
                "id": "flxLeftPannel",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "masterType": constants.MASTER_TYPE_USERWIDGET,
                "isModalContainer": false,
                "skin": "sknLeftPannel0bb79e4bcca8e45",
                "top": "0dp",
                "width": "305dp",
                "zIndex": 5
            }, {}, {});
            flxLeftPannel.setDefaultUnit(kony.flex.DP);
            var leftMenuNew = new com.adminConsole.navigation.leftMenuNew({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "height": "100%",
                "id": "leftMenuNew",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "masterType": constants.MASTER_TYPE_DEFAULT,
                "isModalContainer": false,
                "skin": "sknFlxBg003E75Op100",
                "top": "0dp",
                "width": "100%",
                "overrides": {
                    "brandLogo": {
                        "src": "infinity_dbx_c360_logo_white_2x.png"
                    },
                    "flxScrollMenu": {
                        "bottom": "40dp",
                        "left": "0dp",
                        "top": "80dp"
                    },
                    "imgBottomLogo": {
                        "src": "logo_white_2x.png"
                    },
                    "leftMenuNew": {
                        "bottom": "viz.val_cleared",
                        "centerX": "viz.val_cleared",
                        "centerY": "viz.val_cleared",
                        "height": "100%",
                        "left": "0dp",
                        "maxHeight": "viz.val_cleared",
                        "maxWidth": "viz.val_cleared",
                        "minHeight": "viz.val_cleared",
                        "minWidth": "viz.val_cleared",
                        "right": "viz.val_cleared",
                        "top": "0dp",
                        "width": "100%"
                    }
                }
            }, {
                "overrides": {}
            }, {
                "overrides": {}
            });
            flxLeftPannel.add(leftMenuNew);
            var flxRightPannel = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "100%",
                "id": "flxRightPannel",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "305px",
                "isModalContainer": false,
                "right": "0dp",
                "skin": "slFbox0b7aaa2e3bfa340",
                "zIndex": 5
            }, {}, {});
            flxRightPannel.setDefaultUnit(kony.flex.DP);
            var settingsMenu = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                "clipBounds": true,
                "id": "settingsMenu",
                "isVisible": false,
                "layoutType": kony.flex.FREE_FORM,
                "isModalContainer": false,
                "right": "40dp",
                "skin": "slFbox",
                "top": "85dp",
                "width": "190dp",
                "zIndex": 1
            }, {}, {});
            settingsMenu.setDefaultUnit(kony.flex.DP);
            var settingsMenuOptions = new com.adminConsole.adManagement.selectOptions({
                "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                "id": "settingsMenuOptions",
                "isVisible": true,
                "layoutType": kony.flex.FLOW_VERTICAL,
                "left": "0dp",
                "masterType": constants.MASTER_TYPE_DEFAULT,
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0dp",
                "width": "100%",
                "overrides": {
                    "flxDownArrowImage": {
                        "isVisible": false
                    },
                    "flxOption3": {
                        "isVisible": false
                    },
                    "flxOption4": {
                        "isVisible": false
                    },
                    "fontIconOption1": {
                        "text": ""
                    },
                    "fontIconOption2": {
                        "text": ""
                    },
                    "imgDownArrow": {
                        "src": "downarrow_2x.png"
                    },
                    "imgUpArrow": {
                        "src": "uparrow_2x.png"
                    },
                    "lblOption1": {
                        "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmAdManagement.DefaultAdCampaign\")",
                        "width": kony.flex.USE_PREFFERED_SIZE
                    },
                    "lblOption2": {
                        "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmAdManagement.Settings\")"
                    },
                    "lblSeperator": {
                        "isVisible": false
                    }
                }
            }, {
                "overrides": {}
            }, {
                "overrides": {}
            });
            settingsMenu.add(settingsMenuOptions);
            var flxHeaderDropdown = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                "clipBounds": true,
                "id": "flxHeaderDropdown",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "isModalContainer": false,
                "right": "35dp",
                "skin": "slFbox",
                "top": "20dp",
                "width": "400dp",
                "zIndex": 5
            }, {}, {});
            flxHeaderDropdown.setDefaultUnit(kony.flex.DP);
            var dropdownMainHeader = new com.adminConsole.common.dropdownMainHeader({
                "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                "id": "dropdownMainHeader",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "masterType": constants.MASTER_TYPE_DEFAULT,
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0dp",
                "width": "100%",
                "overrides": {
                    "flxDropdown": {
                        "isVisible": false
                    },
                    "imgUpArrow": {
                        "src": "uparrow_2x.png"
                    }
                }
            }, {
                "overrides": {}
            }, {
                "overrides": {}
            });
            flxHeaderDropdown.add(dropdownMainHeader);
            var flxMainHeader = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "106dp",
                "id": "flxMainHeader",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0dp",
                "width": "100%",
                "zIndex": 1
            }, {}, {});
            flxMainHeader.setDefaultUnit(kony.flex.DP);
            var mainHeader = new com.adminConsole.header.mainHeader({
                "height": "106px",
                "id": "mainHeader",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "masterType": constants.MASTER_TYPE_DEFAULT,
                "isModalContainer": false,
                "skin": "CopyslFbox1",
                "width": "100%",
                "overrides": {
                    "btnAddNewOption": {
                        "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmAdManagement.DefaultCampaignCAPS\")",
                        "isVisible": false,
                        "right": "212px"
                    },
                    "btnDropdownList": {
                        "i18n_text": "kony.i18n.getLocalizedString(\"i18n.ProfileManagement.createSegmentCaps\")"
                    },
                    "flxButtons": {
                        "right": "35dp",
                        "top": "55px",
                        "width": "420dp"
                    },
                    "flxMainHeader": {
                        "left": undefined,
                        "top": undefined
                    },
                    "imgLogout": {
                        "right": "0px",
                        "src": "img_logout.png"
                    },
                    "lblHeading": {
                        "centerY": "viz.val_cleared",
                        "height": kony.flex.USE_PREFFERED_SIZE,
                        "i18n_text": "kony.i18n.getLocalizedString(\"i18n.leftmenu.segments\")",
                        "top": "55px",
                        "width": kony.flex.USE_PREFFERED_SIZE
                    },
                    "lblUserName": {
                        "right": "25px",
                        "text": "Preetish",
                        "top": "viz.val_cleared"
                    },
                    "mainHeader": {
                        "left": undefined,
                        "top": undefined
                    }
                }
            }, {
                "overrides": {}
            }, {
                "overrides": {}
            });
            flxMainHeader.add(mainHeader);
            var flxBreadCrumbs = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "30px",
                "id": "flxBreadCrumbs",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "masterType": constants.MASTER_TYPE_USERWIDGET,
                "isModalContainer": false,
                "skin": "sknflxf5f6f8Op100",
                "top": "90px",
                "width": "100%",
                "zIndex": 1
            }, {}, {});
            flxBreadCrumbs.setDefaultUnit(kony.flex.DP);
            var breadcrumbs = new com.adminConsole.common.breadcrumbs({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "height": "20px",
                "id": "breadcrumbs",
                "isVisible": true,
                "layoutType": kony.flex.FLOW_HORIZONTAL,
                "left": "0dp",
                "masterType": constants.MASTER_TYPE_DEFAULT,
                "isModalContainer": false,
                "skin": "sknflxf5f6f8Op100",
                "top": "1px",
                "width": "100%",
                "overrides": {
                    "breadcrumbs": {
                        "bottom": "viz.val_cleared",
                        "top": "1px",
                        "width": "100%"
                    },
                    "btnBackToMain": {
                        "i18n_text": "kony.i18n.getLocalizedString(\"i18n.ProfileManagement.segmentsCaps\")"
                    },
                    "imgBreadcrumbsDown": {
                        "src": "img_down_arrow.png"
                    },
                    "imgBreadcrumbsRight": {
                        "left": "10dp",
                        "right": 10,
                        "src": "img_breadcrumb_arrow.png"
                    },
                    "imgBreadcrumbsRight2": {
                        "left": "10dp",
                        "right": 10,
                        "src": "img_breadcrumb_arrow.png"
                    },
                    "imgBreadcrumbsRight3": {
                        "src": "img_breadcrumb_arrow.png"
                    }
                }
            }, {
                "overrides": {}
            }, {
                "overrides": {}
            });
            flxBreadCrumbs.add(breadcrumbs);
            var flxListing = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                "bottom": "0dp",
                "clipBounds": true,
                "id": "flxListing",
                "isVisible": false,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "isModalContainer": false,
                "right": "0dp",
                "skin": "slFbox",
                "top": "106dp",
                "zIndex": 1
            }, {}, {});
            flxListing.setDefaultUnit(kony.flex.DP);
            var flxMainSubHeader = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "63dp",
                "id": "flxMainSubHeader",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0dp",
                "width": "100%",
                "zIndex": 100
            }, {}, {});
            flxMainSubHeader.setDefaultUnit(kony.flex.DP);
            var subHeader = new com.adminConsole.header.subHeader({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "centerY": "50%",
                "height": "42dp",
                "id": "subHeader",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "masterType": constants.MASTER_TYPE_DEFAULT,
                "isModalContainer": false,
                "skin": "CopyslFbox2",
                "top": "0dp",
                "width": "100%",
                "overrides": {
                    "flxMenu": {
                        "isVisible": false
                    },
                    "flxSearch": {
                        "width": "48.10%"
                    },
                    "flxSearchContainer": {
                        "centerY": "50%",
                        "height": "83.30%",
                        "width": "100%"
                    },
                    "flxSubHeader": {
                        "centerY": "viz.val_cleared"
                    },
                    "subHeader": {
                        "centerY": "50%",
                        "height": "42dp",
                        "maxHeight": "viz.val_cleared"
                    },
                    "tbxSearchBox": {
                        "i18n_placeholder": "kony.i18n.getLocalizedString(\"i18n.ProfileManagement.segmentSearchPlaceholder\")"
                    }
                }
            }, {
                "overrides": {}
            }, {
                "overrides": {}
            });
            flxMainSubHeader.add(subHeader);
            var flxListingMainContainer = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "bottom": "0dp",
                "clipBounds": true,
                "id": "flxListingMainContainer",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "35dp",
                "isModalContainer": false,
                "right": "35dp",
                "skin": "sknflxffffffBorderd6dbe7Radius4px",
                "top": "63dp",
                "zIndex": 100
            }, {}, {});
            flxListingMainContainer.setDefaultUnit(kony.flex.DP);
            var flxProfileHeaders = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "60dp",
                "id": "flxProfileHeaders",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "sknflxffffffop100",
                "top": "0dp",
                "width": "100%",
                "zIndex": 11
            }, {}, {});
            flxProfileHeaders.setDefaultUnit(kony.flex.DP);
            var flxProfilesNameHeader = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "100%",
                "id": "flxProfilesNameHeader",
                "isVisible": true,
                "layoutType": kony.flex.FLOW_HORIZONTAL,
                "left": "30dp",
                "isModalContainer": false,
                "onClick": controller.AS_FlexContainer_a43b80cc872f4831807a918bd48f0ac3,
                "skin": "sknCursor",
                "top": "0dp",
                "width": "120dp",
                "zIndex": 1
            }, {}, {});
            flxProfilesNameHeader.setDefaultUnit(kony.flex.DP);
            var lblProfilesNameHeader = new kony.ui.Label({
                "centerY": "50%",
                "id": "lblProfilesNameHeader",
                "isVisible": true,
                "left": "0dp",
                "skin": "sknlblLato696c7312px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.ProfileManagement.segmentNameCaps\")",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {
                "hoverSkin": "sknlblLatoBold00000012px"
            });
            var fontIconSortProfilesName = new kony.ui.Label({
                "centerY": "50%",
                "id": "fontIconSortProfilesName",
                "isVisible": true,
                "left": "5dp",
                "skin": "sknIcon15px",
                "text": "",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {
                "hoverSkin": "sknlblCursorFont"
            });
            flxProfilesNameHeader.add(lblProfilesNameHeader, fontIconSortProfilesName);
            var flxUsersHeader = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "100%",
                "id": "flxUsersHeader",
                "isVisible": true,
                "layoutType": kony.flex.FLOW_HORIZONTAL,
                "left": "44%",
                "isModalContainer": false,
                "onClick": controller.AS_FlexContainer_f184888da9f042978e3b0332711e0319,
                "skin": "sknCursor",
                "top": "0dp",
                "width": "132dp",
                "zIndex": 1
            }, {}, {});
            flxUsersHeader.setDefaultUnit(kony.flex.DP);
            var lblUsersHeader = new kony.ui.Label({
                "centerY": "50%",
                "id": "lblUsersHeader",
                "isVisible": true,
                "left": "0px",
                "skin": "sknlblLato696c7312px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.ProfileManagement.noOfUsersCaps\")",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {
                "hoverSkin": "sknlblLatoBold00000012px"
            });
            var fontIconSortUsers = new kony.ui.Label({
                "centerY": "50%",
                "id": "fontIconSortUsers",
                "isVisible": true,
                "left": "5dp",
                "skin": "sknIcon15px",
                "text": "",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {
                "hoverSkin": "sknlblCursorFont"
            });
            flxUsersHeader.add(lblUsersHeader, fontIconSortUsers);
            var flxCampaignsHeader = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "100%",
                "id": "flxCampaignsHeader",
                "isVisible": true,
                "layoutType": kony.flex.FLOW_HORIZONTAL,
                "left": "67%",
                "isModalContainer": false,
                "onClick": controller.AS_FlexContainer_e8c18fc5a3a34ece96d2cc198ff5d25a,
                "skin": "sknCursor",
                "top": "0dp",
                "width": "92dp",
                "zIndex": 1
            }, {}, {});
            flxCampaignsHeader.setDefaultUnit(kony.flex.DP);
            var lblCampaignsHeader = new kony.ui.Label({
                "centerY": "50%",
                "id": "lblCampaignsHeader",
                "isVisible": true,
                "left": "0px",
                "skin": "sknlblLato696c7312px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.ProfileManagement.campaignsCaps\")",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {
                "hoverSkin": "sknlblLatoBold00000012px"
            });
            var fontIconSortCampaigns = new kony.ui.Label({
                "centerY": "50%",
                "id": "fontIconSortCampaigns",
                "isVisible": true,
                "left": "5dp",
                "skin": "sknIcon15px",
                "text": "",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {
                "hoverSkin": "sknlblCursorFont"
            });
            flxCampaignsHeader.add(lblCampaignsHeader, fontIconSortCampaigns);
            var flxProfilesStatusHeader = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "100%",
                "id": "flxProfilesStatusHeader",
                "isVisible": true,
                "layoutType": kony.flex.FLOW_HORIZONTAL,
                "left": "84%",
                "isModalContainer": false,
                "onClick": controller.AS_FlexContainer_ia2738f1fdda45cf914f716c251ff157,
                "skin": "sknCursor",
                "top": "0dp",
                "width": "70dp",
                "zIndex": 1
            }, {}, {});
            flxProfilesStatusHeader.setDefaultUnit(kony.flex.DP);
            var lblProfilesStatusHeader = new kony.ui.Label({
                "centerY": "50%",
                "id": "lblProfilesStatusHeader",
                "isVisible": true,
                "left": "0px",
                "skin": "sknlblLato696c7312px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.roles.STATUS\")",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {
                "hoverSkin": "sknlblLatoBold00000012px"
            });
            var fontIconFilterProfilesStatus = new kony.ui.Label({
                "centerY": "50%",
                "id": "fontIconFilterProfilesStatus",
                "isVisible": true,
                "left": "5dp",
                "skin": "sknIcon15px",
                "text": "",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {
                "hoverSkin": "sknlblCursorFont"
            });
            flxProfilesStatusHeader.add(lblProfilesStatusHeader, fontIconFilterProfilesStatus);
            var lblProfilesHeaderSeparator = new kony.ui.Label({
                "bottom": 0,
                "height": "1px",
                "id": "lblProfilesHeaderSeparator",
                "isVisible": true,
                "left": "20dp",
                "right": "20dp",
                "skin": "sknLblTableHeaderLine",
                "text": ".",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxProfileHeaders.add(flxProfilesNameHeader, flxUsersHeader, flxCampaignsHeader, flxProfilesStatusHeader, lblProfilesHeaderSeparator);
            var flxSegProfiles = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "bottom": "0dp",
                "clipBounds": true,
                "id": "flxSegProfiles",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "60dp",
                "width": "100%"
            }, {}, {});
            flxSegProfiles.setDefaultUnit(kony.flex.DP);
            var segProfiles = new kony.ui.SegmentedUI2({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "bottom": "0dp",
                "data": [{
                    "fontIconStatus": "",
                    "lblCampaigns": "5",
                    "lblName": "Name",
                    "lblOptions": "",
                    "lblSeparator": "Label",
                    "lblStatus": "Active",
                    "lblUsers": "83"
                }],
                "groupCells": false,
                "id": "segProfiles",
                "isVisible": true,
                "left": "0dp",
                "needPageIndicator": true,
                "pageOffDotImage": "pageoffdot.png",
                "pageOnDotImage": "pageondot.png",
                "retainSelection": false,
                "rowFocusSkin": "seg2Focus",
                "rowSkin": "sknseg3pxRadius",
                "rowTemplate": "flxProfiles",
                "sectionHeaderSkin": "sliPhoneSegmentHeader",
                "selectionBehavior": constants.SEGUI_DEFAULT_BEHAVIOR,
                "separatorRequired": false,
                "showScrollbars": true,
                "top": "0dp",
                "viewType": constants.SEGUI_VIEW_TYPE_TABLEVIEW,
                "widgetDataMap": {
                    "flxOptions": "flxOptions",
                    "flxProfiles": "flxProfiles",
                    "flxStatus": "flxStatus",
                    "fontIconStatus": "fontIconStatus",
                    "lblCampaigns": "lblCampaigns",
                    "lblName": "lblName",
                    "lblOptions": "lblOptions",
                    "lblSeparator": "lblSeparator",
                    "lblStatus": "lblStatus",
                    "lblUsers": "lblUsers"
                },
                "width": "100%",
                "zIndex": 1
            }, {
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxSegProfiles.add(segProfiles);
            var flxNoResultFound = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                "clipBounds": true,
                "id": "flxNoResultFound",
                "isVisible": false,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "60dp",
                "width": "100%",
                "zIndex": 1
            }, {}, {});
            flxNoResultFound.setDefaultUnit(kony.flex.DP);
            var rtxSearchMesg = new kony.ui.RichText({
                "bottom": "50px",
                "centerX": "52%",
                "id": "rtxSearchMesg",
                "isVisible": true,
                "left": "0",
                "skin": "sknRtxLato84939e12Px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmCSR.richtextNoResult\")",
                "top": "90px",
                "width": "80%",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_CENTER,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxNoResultFound.add(rtxSearchMesg);
            var flxProfilesStatusFilter = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                "clipBounds": true,
                "id": "flxProfilesStatusFilter",
                "isVisible": false,
                "layoutType": kony.flex.FREE_FORM,
                "left": "79%",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "30dp",
                "width": "130dp",
                "zIndex": 11
            }, {}, {});
            flxProfilesStatusFilter.setDefaultUnit(kony.flex.DP);
            var statusFilterMenu = new com.adminConsole.common.statusFilterMenu({
                "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                "id": "statusFilterMenu",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "masterType": constants.MASTER_TYPE_DEFAULT,
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "5dp",
                "width": "100%",
                "zIndex": 5,
                "overrides": {
                    "imgUpArrow": {
                        "src": "uparrow_2x.png"
                    }
                }
            }, {
                "overrides": {}
            }, {
                "overrides": {}
            });
            flxProfilesStatusFilter.add(statusFilterMenu);
            flxListingMainContainer.add(flxProfileHeaders, flxSegProfiles, flxNoResultFound, flxProfilesStatusFilter);
            flxListing.add(flxMainSubHeader, flxListingMainContainer);
            var flxViewDetails = new kony.ui.FlexScrollContainer({
                "allowHorizontalBounce": false,
                "allowVerticalBounce": true,
                "bottom": "0dp",
                "bounces": true,
                "clipBounds": true,
                "enableScrolling": true,
                "horizontalScrollIndicator": true,
                "id": "flxViewDetails",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "pagingEnabled": false,
                "right": "0dp",
                "scrollDirection": kony.flex.SCROLL_VERTICAL,
                "skin": "sknscrollflxf5f6f8Op100",
                "top": "120dp",
                "verticalScrollIndicator": true,
                "zIndex": 1
            }, {}, {});
            flxViewDetails.setDefaultUnit(kony.flex.DP);
            var flxViewDetailsInner = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                "clipBounds": true,
                "id": "flxViewDetailsInner",
                "isVisible": true,
                "layoutType": kony.flex.FLOW_VERTICAL,
                "left": "35dp",
                "isModalContainer": false,
                "right": "35dp",
                "skin": "slFbox",
                "top": "0dp",
                "zIndex": 1
            }, {}, {});
            flxViewDetailsInner.setDefaultUnit(kony.flex.DP);
            var flxTopDetails = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                "clipBounds": true,
                "id": "flxTopDetails",
                "isVisible": true,
                "layoutType": kony.flex.FLOW_VERTICAL,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "sknFlxBgFFFFFFBorE4E6ECsz1pxR3px",
                "top": "0dp",
                "width": "100%",
                "zIndex": 1
            }, {}, {});
            flxTopDetails.setDefaultUnit(kony.flex.DP);
            var flxHeadRow = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "56dp",
                "id": "flxHeadRow",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0dp",
                "width": "100%"
            }, {}, {});
            flxHeadRow.setDefaultUnit(kony.flex.DP);
            var lblProfileNameSelected = new kony.ui.Label({
                "id": "lblProfileNameSelected",
                "isVisible": true,
                "left": "20dp",
                "skin": "sknLatoRegular192B4518px",
                "text": "SapphireCreditCard_Aug19",
                "top": "17dp",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var flxStatus = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "16dp",
                "id": "flxStatus",
                "isVisible": true,
                "layoutType": kony.flex.FLOW_HORIZONTAL,
                "reverseLayoutDirection": true,
                "isModalContainer": false,
                "right": "50dp",
                "skin": "slFbox",
                "top": "20dp",
                "width": "80dp",
                "zIndex": 1
            }, {}, {});
            flxStatus.setDefaultUnit(kony.flex.DP);
            var lblStatus = new kony.ui.Label({
                "centerY": "50%",
                "height": "16px",
                "id": "lblStatus",
                "isVisible": true,
                "right": "0px",
                "skin": "sknlblLatoReg485c7513px",
                "text": "Active",
                "top": "0px",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var fontIconStatus = new kony.ui.Label({
                "centerY": "50%",
                "id": "fontIconStatus",
                "isVisible": true,
                "right": "5px",
                "skin": "sknfontIconInactive",
                "text": "",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxStatus.add(lblStatus, fontIconStatus);
            var flxOptions = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "25dp",
                "id": "flxOptions",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "isModalContainer": false,
                "right": "20dp",
                "skin": "sknFlxBorffffff1pxRound",
                "top": "17dp",
                "width": "25dp",
                "zIndex": 1
            }, {}, {
                "hoverSkin": "sknflxffffffop100Border424242Radius100px"
            });
            flxOptions.setDefaultUnit(kony.flex.DP);
            var lblIconOptions = new kony.ui.Label({
                "centerX": "50%",
                "centerY": "50%",
                "id": "lblIconOptions",
                "isVisible": true,
                "skin": "sknFontIconOptionMenu",
                "text": "",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxOptions.add(lblIconOptions);
            var flxSeparatorLine = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "bottom": "0dp",
                "clipBounds": true,
                "height": "1dp",
                "id": "flxSeparatorLine",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "20dp",
                "isModalContainer": false,
                "right": "20dp",
                "skin": "sknFlxD7D9E0Separator",
                "zIndex": 1
            }, {}, {});
            flxSeparatorLine.setDefaultUnit(kony.flex.DP);
            flxSeparatorLine.add();
            flxHeadRow.add(lblProfileNameSelected, flxStatus, flxOptions, flxSeparatorLine);
            var flxFirstRow = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                "clipBounds": true,
                "id": "flxFirstRow",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0dp",
                "width": "100%"
            }, {}, {});
            flxFirstRow.setDefaultUnit(kony.flex.DP);
            var lblDescriptionHeader = new kony.ui.Label({
                "id": "lblDescriptionHeader",
                "isVisible": true,
                "left": "20dp",
                "skin": "sknlblLato11px696C73",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmAdManagement.DescriptionCAPS\")",
                "top": "18dp",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var lblDescription = new kony.ui.Label({
                "id": "lblDescription",
                "isVisible": true,
                "left": "20dp",
                "right": 20,
                "skin": "sknlbl485C75LatoSemiBold13px",
                "text": "Promotion for Sapphire Credit Card, targeted  at all customers who live in Austin, TX Promotion for Sapphire Credit Card, targeted  at all customers who live in Austin, TX Promotion for Sapphire Credit Card, targeted  at all customers who live in Austin, TX Promotion for Sapphire Credit Card, targeted  at all customers who live in Austin, TX Promotion for Sapphire Credit Card, targeted  at all customers who live in Austin, TX Promotion for Sapphire Credit Card, targeted  at all customers who live in Austin, TX",
                "top": "42dp",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxFirstRow.add(lblDescriptionHeader, lblDescription);
            var flxSecondRow = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "97dp",
                "id": "flxSecondRow",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0dp",
                "width": "100%"
            }, {}, {});
            flxSecondRow.setDefaultUnit(kony.flex.DP);
            var lblProfileStartDateHeader = new kony.ui.Label({
                "id": "lblProfileStartDateHeader",
                "isVisible": true,
                "left": "20dp",
                "skin": "sknlblLato11px696C73",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.ProfileManagement.segmentStartDateCaps\")",
                "top": "30dp",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var lblProfileStartDate = new kony.ui.Label({
                "id": "lblProfileStartDate",
                "isVisible": true,
                "left": "20dp",
                "skin": "sknlbl485C75LatoSemiBold13px",
                "text": "20/06/2020",
                "top": "55dp",
                "width": "100dp",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var lblProfileLastUsedHeader = new kony.ui.Label({
                "id": "lblProfileLastUsedHeader",
                "isVisible": true,
                "left": "39%",
                "skin": "sknlblLato11px696C73",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.ProfileManagement.lastUsedCaps\")",
                "top": "30dp",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var lblProfileLastUsed = new kony.ui.Label({
                "id": "lblProfileLastUsed",
                "isVisible": true,
                "left": "39%",
                "skin": "sknlbl485C75LatoSemiBold13px",
                "text": "N/A",
                "top": "55dp",
                "width": "100dp",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var flxUsersAvailable = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "20dp",
                "id": "flxUsersAvailable",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "75%",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "30dp",
                "width": "115dp"
            }, {}, {});
            flxUsersAvailable.setDefaultUnit(kony.flex.DP);
            var lblUsersAvailableHeader = new kony.ui.Label({
                "centerY": "50%",
                "id": "lblUsersAvailableHeader",
                "isVisible": true,
                "left": "0dp",
                "skin": "sknlblLato11px696C73",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.ProfileManagement.usersAvailableCaps\")",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var flxUsersRefresh = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "centerY": "50%",
                "clipBounds": true,
                "height": "18dp",
                "id": "flxUsersRefresh",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "isModalContainer": false,
                "right": "0dp",
                "skin": "hoverhandSkin",
                "width": "18dp"
            }, {}, {});
            flxUsersRefresh.setDefaultUnit(kony.flex.DP);
            var fontIconUsersRefresh = new kony.ui.Label({
                "centerY": "50%",
                "id": "fontIconUsersRefresh",
                "isVisible": true,
                "right": "0dp",
                "skin": "sknFontIcon485C7518px",
                "text": "",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxUsersRefresh.add(fontIconUsersRefresh);
            flxUsersAvailable.add(lblUsersAvailableHeader, flxUsersRefresh);
            var lblUsersAvailable = new kony.ui.Label({
                "id": "lblUsersAvailable",
                "isVisible": true,
                "left": "75%",
                "skin": "sknlbl485C75LatoSemiBold13px",
                "text": "23641",
                "top": "55dp",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxSecondRow.add(lblProfileStartDateHeader, lblProfileStartDate, lblProfileLastUsedHeader, lblProfileLastUsed, flxUsersAvailable, lblUsersAvailable);
            flxTopDetails.add(flxHeadRow, flxFirstRow, flxSecondRow);
            var flxBottomDetails = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                "clipBounds": true,
                "id": "flxBottomDetails",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0px",
                "isModalContainer": false,
                "skin": "sknFlxBgFFFFFFBorE4E6ECsz1pxR3px",
                "top": "20px",
                "width": "100%",
                "zIndex": 1
            }, {}, {});
            flxBottomDetails.setDefaultUnit(kony.flex.DP);
            var flxViewTabs = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "50px",
                "id": "flxViewTabs",
                "isVisible": true,
                "layoutType": kony.flex.FLOW_HORIZONTAL,
                "left": "0px",
                "isModalContainer": false,
                "right": "0px",
                "skin": "slFbox",
                "top": "0px",
                "zIndex": 4
            }, {}, {});
            flxViewTabs.setDefaultUnit(kony.flex.DP);
            var flxTab1 = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "100%",
                "id": "flxTab1",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "20dp",
                "isModalContainer": false,
                "skin": "sknFlxffffffCursorPointer",
                "top": "0dp",
                "width": "74dp",
                "zIndex": 1
            }, {}, {});
            flxTab1.setDefaultUnit(kony.flex.DP);
            var lblAttributesTab = new kony.ui.Label({
                "centerX": "50%",
                "centerY": "50%",
                "height": "100%",
                "id": "lblAttributesTab",
                "isVisible": true,
                "skin": "sknLblTabUtilActive",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmAdManagement.AttributesCAPS\")",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_CENTER,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxTab1.add(lblAttributesTab);
            var flxTab2 = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "100%",
                "id": "flxTab2",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "20dp",
                "isModalContainer": false,
                "skin": "sknFlxffffffCursorPointer",
                "top": "0dp",
                "width": "90dp",
                "zIndex": 1
            }, {}, {});
            flxTab2.setDefaultUnit(kony.flex.DP);
            var lblCampaignsTab = new kony.ui.Label({
                "centerY": "50%",
                "height": "100%",
                "id": "lblCampaignsTab",
                "isVisible": true,
                "left": "0dp",
                "skin": "sknlblLatoRegular485c7512px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.ProfileManagement.campaignsCaps\")",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_CENTER,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxTab2.add(lblCampaignsTab);
            flxViewTabs.add(flxTab1, flxTab2);
            var flxSeparator = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "1px",
                "id": "flxSeparator",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0px",
                "isModalContainer": false,
                "right": "0px",
                "skin": "sknflxSepratorE1E5ED",
                "top": "50px",
                "zIndex": 1
            }, {}, {});
            flxSeparator.setDefaultUnit(kony.flex.DP);
            flxSeparator.add();
            var flxAttributesData = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                "bottom": "0px",
                "clipBounds": true,
                "id": "flxAttributesData",
                "isVisible": false,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0px",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "51px",
                "width": "100%",
                "zIndex": 1
            }, {}, {});
            flxAttributesData.setDefaultUnit(kony.flex.DP);
            var flxAttributesHeaders = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "50dp",
                "id": "flxAttributesHeaders",
                "isVisible": true,
                "layoutType": kony.flex.FLOW_HORIZONTAL,
                "left": "30dp",
                "isModalContainer": false,
                "right": "0dp",
                "skin": "slFbox",
                "top": "0",
                "width": kony.flex.USE_PREFFERED_SIZE
            }, {}, {});
            flxAttributesHeaders.setDefaultUnit(kony.flex.DP);
            var flxAttributeDatasetsHeader = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "100%",
                "id": "flxAttributeDatasetsHeader",
                "isVisible": true,
                "layoutType": kony.flex.FLOW_HORIZONTAL,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0dp",
                "width": "24%"
            }, {}, {});
            flxAttributeDatasetsHeader.setDefaultUnit(kony.flex.DP);
            var lblAttributeDatasetHeader = new kony.ui.Label({
                "centerY": "50%",
                "id": "lblAttributeDatasetHeader",
                "isVisible": true,
                "left": "0",
                "skin": "sknlblLato696c7311px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmAdManagement.DatasetCAPS\")",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var lblFilterDataset = new kony.ui.Label({
                "centerY": "50%",
                "id": "lblFilterDataset",
                "isVisible": false,
                "left": "7dp",
                "skin": "sknIcon15px",
                "text": "",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {
                "hoverSkin": "sknlblCursorFont"
            });
            flxAttributeDatasetsHeader.add(lblAttributeDatasetHeader, lblFilterDataset);
            var flxAttributeNameHeader = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "100%",
                "id": "flxAttributeNameHeader",
                "isVisible": true,
                "layoutType": kony.flex.FLOW_HORIZONTAL,
                "left": "1%",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0dp",
                "width": "24%"
            }, {}, {});
            flxAttributeNameHeader.setDefaultUnit(kony.flex.DP);
            var lblAttributeNameHeader = new kony.ui.Label({
                "centerY": "50%",
                "id": "lblAttributeNameHeader",
                "isVisible": true,
                "left": "0dp",
                "skin": "sknlblLato696c7311px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmAdManagement.AttributeCAPS\")",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var lblSortAttributeName = new kony.ui.Label({
                "centerY": "50%",
                "id": "lblSortAttributeName",
                "isVisible": true,
                "left": "7dp",
                "skin": "sknIcon15px",
                "text": "",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {
                "hoverSkin": "sknlblCursorFont"
            });
            flxAttributeNameHeader.add(lblAttributeNameHeader, lblSortAttributeName);
            var lblAttributeCriteriaHeader = new kony.ui.Label({
                "centerY": "50%",
                "id": "lblAttributeCriteriaHeader",
                "isVisible": true,
                "left": "1%",
                "skin": "sknlblLato696c7311px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmAdManagement.CriteriaCAPS\")",
                "width": "24%",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var lblAttributeValueHeader = new kony.ui.Label({
                "centerY": "50%",
                "id": "lblAttributeValueHeader",
                "isVisible": true,
                "left": "1%",
                "skin": "sknlblLato696c7311px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmAdManagement.ValueCAPS\")",
                "width": "24%",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxAttributesHeaders.add(flxAttributeDatasetsHeader, flxAttributeNameHeader, lblAttributeCriteriaHeader, lblAttributeValueHeader);
            var lblSeparatorAttributesHeader = new kony.ui.Label({
                "bottom": "0px",
                "height": "1px",
                "id": "lblSeparatorAttributesHeader",
                "isVisible": true,
                "left": "20dp",
                "right": "20dp",
                "skin": "sknLblTableHeaderLine",
                "text": "-",
                "top": "50dp",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var segAttributes = new kony.ui.SegmentedUI2({
                "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                "data": [{
                    "lblAttribute": "Attribute",
                    "lblCriteria": "Criteria",
                    "lblDataset": "DATASET",
                    "lblValue": "Value"
                }],
                "groupCells": false,
                "id": "segAttributes",
                "isVisible": true,
                "left": "30px",
                "needPageIndicator": true,
                "pageOffDotImage": "pageoffdot.png",
                "pageOnDotImage": "pageondot.png",
                "retainSelection": false,
                "right": "0px",
                "rowFocusSkin": "seg2Focus",
                "rowSkin": "seg2Normal",
                "rowTemplate": "flxAttributeRow",
                "sectionHeaderSkin": "sliPhoneSegmentHeader",
                "selectionBehavior": constants.SEGUI_DEFAULT_BEHAVIOR,
                "separatorColor": "d7d9e000",
                "separatorRequired": true,
                "showScrollbars": false,
                "top": "52px",
                "viewType": constants.SEGUI_VIEW_TYPE_TABLEVIEW,
                "widgetDataMap": {
                    "flxAttributeRow": "flxAttributeRow",
                    "flxRow": "flxRow",
                    "lblAttribute": "lblAttribute",
                    "lblCriteria": "lblCriteria",
                    "lblDataset": "lblDataset",
                    "lblValue": "lblValue"
                },
                "zIndex": 1
            }, {
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var flxDatasetFilter = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                "clipBounds": true,
                "id": "flxDatasetFilter",
                "isVisible": false,
                "layoutType": kony.flex.FREE_FORM,
                "left": "20dp",
                "maxWidth": "150dp",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "40dp",
                "width": "20%"
            }, {}, {});
            flxDatasetFilter.setDefaultUnit(kony.flex.DP);
            var datasetFilter = new com.adminConsole.common.statusFilterMenu({
                "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                "id": "datasetFilter",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "masterType": constants.MASTER_TYPE_DEFAULT,
                "maxWidth": "150dp",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0dp",
                "width": "100%",
                "zIndex": 5,
                "overrides": {
                    "imgUpArrow": {
                        "left": "67dp",
                        "right": "viz.val_cleared",
                        "src": "uparrow_2x.png"
                    },
                    "segStatusFilterDropdown": {
                        "right": "10px"
                    },
                    "statusFilterMenu": {
                        "isVisible": true,
                        "left": "0dp",
                        "maxWidth": "150dp",
                        "minWidth": "viz.val_cleared",
                        "top": "0dp",
                        "width": "100%"
                    }
                }
            }, {
                "overrides": {}
            }, {
                "overrides": {}
            });
            flxDatasetFilter.add(datasetFilter);
            flxAttributesData.add(flxAttributesHeaders, lblSeparatorAttributesHeader, segAttributes, flxDatasetFilter);
            var flxCampaignsData = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                "clipBounds": true,
                "id": "flxCampaignsData",
                "isVisible": false,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "51dp",
                "width": "100%",
                "zIndex": 5
            }, {}, {});
            flxCampaignsData.setDefaultUnit(kony.flex.DP);
            var segCampaignsData = new kony.ui.SegmentedUI2({
                "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                "bottom": 20,
                "data": [{
                    "fontIconStatusImg": "",
                    "fonticonArrow": "",
                    "lblDescription": "Promotion for Sapphire Credit Card, targeted  at all customers who live in Austin, TX Promotion for Sapphire Credit Card, targeted  at all customers who live in Austin, TX Promotion for Sapphire Credit Card, targeted  at all customers who live in Austin, TX Promotion for Sapphire Credit Card, targeted  at all customers who live in Austin, TX Promotion for Sapphire Credit Card, targeted  at all customers who live in Austin, TX",
                    "lblDescriptionHeader": "kony.i18n.getLocalizedString(\"i18n.ConfigurationBundles.descriptionInCaps\")",
                    "lblEndDateTime": "08/31/2019 11:59 PM",
                    "lblEndDateTimeHeader": "kony.i18n.getLocalizedString(\"i18n.common.endDateCaps\")",
                    "lblName": "SapphireCreditCard_Aug19",
                    "lblStartDateTime": "08/01/2019 12:00 AM",
                    "lblStartDateTimeHeader": "kony.i18n.getLocalizedString(\"i18n.common.startDateCaps\")",
                    "lblStatus": "Active",
                    "lblUsersCount": "IN App : 2223 \n Offline : 896",
                    "lblUsersCountHeader": "kony.i18n.getLocalizedString(\"i18n.ProfileManagement.reachedUsersCountCaps\")"
                }],
                "groupCells": false,
                "id": "segCampaignsData",
                "isVisible": true,
                "left": "20px",
                "needPageIndicator": true,
                "pageOffDotImage": "pageoffdot.png",
                "pageOnDotImage": "pageondot.png",
                "retainSelection": false,
                "right": "20px",
                "rowFocusSkin": "seg2Focus",
                "rowSkin": "seg2Normal",
                "rowTemplate": "flxCampaignsDetails",
                "sectionHeaderSkin": "sliPhoneSegmentHeader",
                "selectionBehavior": constants.SEGUI_DEFAULT_BEHAVIOR,
                "separatorRequired": false,
                "showScrollbars": false,
                "top": "10px",
                "viewType": constants.SEGUI_VIEW_TYPE_TABLEVIEW,
                "widgetDataMap": {
                    "flxCampaignsDetails": "flxCampaignsDetails",
                    "flxCampaignsDetailsInner": "flxCampaignsDetailsInner",
                    "flxCollapse": "flxCollapse",
                    "flxDropdown": "flxDropdown",
                    "flxExpand": "flxExpand",
                    "flxStatus": "flxStatus",
                    "fontIconStatusImg": "fontIconStatusImg",
                    "fonticonArrow": "fonticonArrow",
                    "lblDescription": "lblDescription",
                    "lblDescriptionHeader": "lblDescriptionHeader",
                    "lblEndDateTime": "lblEndDateTime",
                    "lblEndDateTimeHeader": "lblEndDateTimeHeader",
                    "lblName": "lblName",
                    "lblStartDateTime": "lblStartDateTime",
                    "lblStartDateTimeHeader": "lblStartDateTimeHeader",
                    "lblStatus": "lblStatus",
                    "lblUsersCount": "lblUsersCount",
                    "lblUsersCountHeader": "lblUsersCountHeader"
                },
                "zIndex": 1
            }, {
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxCampaignsData.add(segCampaignsData);
            var flxNoResultFoundAttributes = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                "clipBounds": true,
                "id": "flxNoResultFoundAttributes",
                "isVisible": false,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "51dp",
                "width": "100%",
                "zIndex": 1
            }, {}, {});
            flxNoResultFoundAttributes.setDefaultUnit(kony.flex.DP);
            var rtxSearchMesgAttributes = new kony.ui.RichText({
                "bottom": "50px",
                "centerX": "52%",
                "id": "rtxSearchMesgAttributes",
                "isVisible": true,
                "left": "0",
                "skin": "sknRtxLato84939e12Px",
                "text": "No data found.",
                "top": "90px",
                "width": "80%",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_CENTER,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxNoResultFoundAttributes.add(rtxSearchMesgAttributes);
            flxBottomDetails.add(flxViewTabs, flxSeparator, flxAttributesData, flxCampaignsData, flxNoResultFoundAttributes);
            flxViewDetailsInner.add(flxTopDetails, flxBottomDetails);
            flxViewDetails.add(flxViewDetailsInner);
            var flxLoading = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "100%",
                "id": "flxLoading",
                "isVisible": false,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "sknLoadingBlur",
                "top": "0dp",
                "width": "100%",
                "zIndex": 200
            }, {}, {});
            flxLoading.setDefaultUnit(kony.flex.DP);
            var flxImageContainer = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "centerX": "50%",
                "centerY": "50%",
                "clipBounds": true,
                "height": "75px",
                "id": "flxImageContainer",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "isModalContainer": false,
                "skin": "slFbox",
                "width": "75px",
                "zIndex": 1
            }, {}, {});
            flxImageContainer.setDefaultUnit(kony.flex.DP);
            var imgLoading = new kony.ui.Image2({
                "centerX": "50%",
                "centerY": "50%",
                "height": "50px",
                "id": "imgLoading",
                "isVisible": true,
                "skin": "slImage",
                "src": "loadingscreenimage.gif",
                "width": "50px",
                "zIndex": 1
            }, {
                "imageScaleMode": constants.IMAGE_SCALE_MODE_MAINTAIN_ASPECT_RATIO,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxImageContainer.add(imgLoading);
            flxLoading.add(flxImageContainer);
            var flxToastMessage = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "bottom": "0px",
                "centerX": "50%",
                "clipBounds": true,
                "height": "70px",
                "id": "flxToastMessage",
                "isVisible": false,
                "layoutType": kony.flex.FREE_FORM,
                "isModalContainer": false,
                "skin": "slFbox",
                "width": "1413px",
                "zIndex": 1
            }, {}, {});
            flxToastMessage.setDefaultUnit(kony.flex.DP);
            var flxToastContainer = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "centerX": "50%",
                "centerY": "50%",
                "clipBounds": true,
                "height": "45px",
                "id": "flxToastContainer",
                "isVisible": false,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0%",
                "isModalContainer": false,
                "skin": "sknflxSuccessToast1F844D",
                "top": "0%",
                "width": "60%",
                "zIndex": 1
            }, {}, {});
            flxToastContainer.setDefaultUnit(kony.flex.DP);
            var imgLeft = new kony.ui.Image2({
                "centerY": "50%",
                "height": "15dp",
                "id": "imgLeft",
                "isVisible": true,
                "left": "15px",
                "skin": "slImage",
                "src": "arrow2x.png",
                "top": "0dp",
                "width": "15dp",
                "zIndex": 1
            }, {
                "imageScaleMode": constants.IMAGE_SCALE_MODE_MAINTAIN_ASPECT_RATIO,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var lbltoastMessage = new kony.ui.Label({
                "centerY": "50%",
                "id": "lbltoastMessage",
                "isVisible": true,
                "left": "45px",
                "right": "45px",
                "skin": "lblfffffflatoregular14px",
                "text": "Label",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var imgRight = new kony.ui.Image2({
                "centerY": "50%",
                "height": "15px",
                "id": "imgRight",
                "isVisible": true,
                "right": "15px",
                "skin": "slImage",
                "src": "close_small2x.png",
                "top": "0dp",
                "width": "15px",
                "zIndex": 1
            }, {
                "imageScaleMode": constants.IMAGE_SCALE_MODE_MAINTAIN_ASPECT_RATIO,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxToastContainer.add(imgLeft, lbltoastMessage, imgRight);
            var toastMessage = new com.adminConsole.common.toastMessage({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "id": "toastMessage",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "masterType": constants.MASTER_TYPE_DEFAULT,
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0dp",
                "overrides": {
                    "lbltoastMessage": {
                        "text": "label"
                    },
                    "toastMessage": {
                        "isVisible": true
                    }
                }
            }, {
                "overrides": {}
            }, {
                "overrides": {}
            });
            flxToastMessage.add(flxToastContainer, toastMessage);
            var flxSelectOptions = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                "clipBounds": true,
                "id": "flxSelectOptions",
                "isVisible": false,
                "layoutType": kony.flex.FREE_FORM,
                "isModalContainer": false,
                "right": "50dp",
                "skin": "slFbox",
                "top": "325dp",
                "width": "165dp",
                "zIndex": 1
            }, {}, {});
            flxSelectOptions.setDefaultUnit(kony.flex.DP);
            var selectOptions = new com.adminConsole.adManagement.selectOptions({
                "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                "id": "selectOptions",
                "isVisible": true,
                "layoutType": kony.flex.FLOW_VERTICAL,
                "left": "0dp",
                "masterType": constants.MASTER_TYPE_DEFAULT,
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0dp",
                "width": "100%",
                "overrides": {
                    "flxOption3": {
                        "isVisible": true
                    },
                    "fontIconOption2": {
                        "i18n_text": "kony.i18n.getLocalizedString(\"i18n.userwidgetmodel.lblIconOption4\")"
                    },
                    "fontIconOption3": {
                        "i18n_text": "kony.i18n.getLocalizedString(\"i18n.userwidgetmodel.lblIconOption2\")"
                    },
                    "imgDownArrow": {
                        "src": "downarrow_2x.png"
                    },
                    "imgUpArrow": {
                        "src": "uparrow_2x.png"
                    },
                    "lblOption2": {
                        "i18n_text": "kony.i18n.getLocalizedString(\"i18n.common.deactivate\")",
                        "width": "63px"
                    },
                    "lblOption3": {
                        "i18n_text": "kony.i18n.getLocalizedString(\"i18n.common.delete\")"
                    },
                    "lblOption4": {
                        "i18n_text": "kony.i18n.getLocalizedString(\"i18n.ProfileManagement.copySegment\")"
                    }
                }
            }, {
                "overrides": {}
            }, {
                "overrides": {}
            });
            flxSelectOptions.add(selectOptions);
            flxRightPannel.add(settingsMenu, flxHeaderDropdown, flxMainHeader, flxBreadCrumbs, flxListing, flxViewDetails, flxLoading, flxToastMessage, flxSelectOptions);
            var flxPopup = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "100%",
                "id": "flxPopup",
                "isVisible": false,
                "layoutType": kony.flex.FREE_FORM,
                "masterType": constants.MASTER_TYPE_USERWIDGET,
                "isModalContainer": false,
                "skin": "skn222b35",
                "width": "100%",
                "zIndex": 10
            }, {}, {});
            flxPopup.setDefaultUnit(kony.flex.DP);
            var warningpopup = new com.adminConsole.common.popUp({
                "height": "100%",
                "id": "warningpopup",
                "isVisible": false,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "masterType": constants.MASTER_TYPE_DEFAULT,
                "isModalContainer": false,
                "skin": "sknflxBg000000Op50",
                "top": "0dp",
                "width": "100%",
                "zIndex": 1,
                "overrides": {
                    "btnPopUpCancel": {
                        "i18n_text": "kony.i18n.getLocalizedString(\"i18n.PopUp.NoLeaveAsIS\")",
                        "left": "viz.val_cleared",
                        "minWidth": "100px",
                        "right": "20dp",
                        "top": "0%",
                        "width": kony.flex.USE_PREFFERED_SIZE
                    },
                    "btnPopUpDelete": {
                        "i18n_text": "kony.i18n.getLocalizedString(\"i18n.PopUp.YesProceed\")"
                    },
                    "flxPopUp": {
                        "width": "680px"
                    },
                    "lblPopUpMainMessage": {
                        "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmAdManagement.ReduceAdPlaceholders\")"
                    },
                    "popUp": {
                        "isVisible": false,
                        "left": "0dp",
                        "top": "0dp",
                        "width": "100%"
                    },
                    "rtxPopUpDisclaimer": {
                        "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmAdManagement.ReduceAdPlaceholdersMsg\")",
                        "width": "640dp"
                    }
                }
            }, {
                "overrides": {}
            }, {
                "overrides": {}
            });
            flxPopup.add(warningpopup);
            this.add(flxLeftPannel, flxRightPannel, flxPopup);
        };
        return [{
            "addWidgets": addWidgetsfrmProfiles,
            "enabledForIdleTimeout": true,
            "id": "frmProfiles",
            "layoutType": kony.flex.FREE_FORM,
            "needAppMenu": false,
            "preShow": function(eventobject) {
                controller.AS_Form_hf9939be929a42639285afc93e753fd7(eventobject);
            },
            "skin": "slForm"
        }, {
            "displayOrientation": constants.FORM_DISPLAY_ORIENTATION_PORTRAIT,
            "layoutType": kony.flex.FREE_FORM,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, {
            "onDeviceBack": controller.AS_Form_i1df3e5f08d84b7696891b7315cc3896,
            "retainScrollPosition": false
        }]
    }
});