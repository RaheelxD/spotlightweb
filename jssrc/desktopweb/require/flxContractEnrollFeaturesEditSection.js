define("flxContractEnrollFeaturesEditSection", function() {
    return function(controller) {
        var flxContractEnrollFeaturesEditSection = new kony.ui.FlexContainer({
            "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
            "clipBounds": true,
            "id": "flxContractEnrollFeaturesEditSection",
            "isVisible": true,
            "layoutType": kony.flex.FLOW_VERTICAL,
            "left": "0dp",
            "isModalContainer": false,
            "skin": "sknFlxFFFFFF100O",
            "top": "0dp",
            "width": "100%"
        }, {}, {});
        flxContractEnrollFeaturesEditSection.setDefaultUnit(kony.flex.DP);
        var lblTopSeperator = new kony.ui.Label({
            "height": "1dp",
            "id": "lblTopSeperator",
            "isVisible": true,
            "left": "0dp",
            "skin": "sknlblSeperatorD7D9E0",
            "text": "Label",
            "top": "20dp",
            "width": "100%"
        }, {
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, {});
        var flxRow1 = new kony.ui.FlexContainer({
            "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
            "clipBounds": true,
            "id": "flxRow1",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "left": "0dp",
            "isModalContainer": false,
            "skin": "slFbox",
            "top": "12dp",
            "width": "100%"
        }, {}, {});
        flxRow1.setDefaultUnit(kony.flex.DP);
        var flxLeft = new kony.ui.FlexContainer({
            "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
            "clipBounds": true,
            "id": "flxLeft",
            "isVisible": true,
            "layoutType": kony.flex.FLOW_HORIZONTAL,
            "left": "15dp",
            "isModalContainer": false,
            "right": "180dp",
            "skin": "slFbox",
            "top": "0dp"
        }, {}, {});
        flxLeft.setDefaultUnit(kony.flex.DP);
        var flxCheckbox = new kony.ui.FlexContainer({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "clipBounds": true,
            "height": "15dp",
            "id": "flxCheckbox",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "left": "0dp",
            "isModalContainer": false,
            "skin": "sknCursor",
            "top": "2dp",
            "width": "15dp"
        }, {}, {});
        flxCheckbox.setDefaultUnit(kony.flex.DP);
        var imgSectionCheckbox = new kony.ui.Image2({
            "height": "100%",
            "id": "imgSectionCheckbox",
            "isVisible": true,
            "left": "0",
            "skin": "slImage",
            "src": "checkboxnormal.png",
            "top": "0",
            "width": "100%"
        }, {
            "imageScaleMode": constants.IMAGE_SCALE_MODE_MAINTAIN_ASPECT_RATIO,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, {});
        flxCheckbox.add(imgSectionCheckbox);
        var flxToggleArrow = new kony.ui.FlexContainer({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "clipBounds": true,
            "height": "15dp",
            "id": "flxToggleArrow",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "left": "15dp",
            "isModalContainer": false,
            "skin": "sknCursor",
            "top": "2dp",
            "width": "15dp"
        }, {}, {});
        flxToggleArrow.setDefaultUnit(kony.flex.DP);
        var lblIconToggleArrow = new kony.ui.Label({
            "id": "lblIconToggleArrow",
            "isVisible": true,
            "left": "0",
            "skin": "sknIcon485C7513px",
            "text": "",
            "top": "0",
            "width": kony.flex.USE_PREFFERED_SIZE
        }, {
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, {});
        flxToggleArrow.add(lblIconToggleArrow);
        var lblFeatureName = new kony.ui.Label({
            "id": "lblFeatureName",
            "isVisible": true,
            "left": "10dp",
            "skin": "sknLbl192B45LatoRegular14px",
            "text": "Label",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, {
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, {});
        flxLeft.add(flxCheckbox, flxToggleArrow, lblFeatureName);
        var flxRight = new kony.ui.FlexContainer({
            "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
            "clipBounds": true,
            "id": "flxRight",
            "isVisible": true,
            "layoutType": kony.flex.FLOW_HORIZONTAL,
            "reverseLayoutDirection": false,
            "isModalContainer": false,
            "right": "20dp",
            "skin": "slFbox",
            "top": "0dp",
            "width": "85dp"
        }, {}, {});
        flxRight.setDefaultUnit(kony.flex.DP);
        var lblIconStatusTop = new kony.ui.Label({
            "height": "15px",
            "id": "lblIconStatusTop",
            "isVisible": true,
            "left": "0dp",
            "right": "5dp",
            "skin": "sknFontIconActivate",
            "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmAlertsManagement.lblViewAlertStatusKey\")",
            "top": "0px",
            "width": "13px",
            "zIndex": 1
        }, {
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, {});
        var lblStatusValue = new kony.ui.Label({
            "id": "lblStatusValue",
            "isVisible": true,
            "left": "0dp",
            "skin": "sknlblLato5bc06cBold14px",
            "i18n_text": "kony.i18n.getLocalizedString(\"i18n.secureimage.Active\")",
            "top": "0px",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, {
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, {});
        flxRight.add(lblIconStatusTop, lblStatusValue);
        var lblCustom = new kony.ui.Label({
            "id": "lblCustom",
            "isVisible": false,
            "right": "120dp",
            "skin": "sknLbl485c75LatoRegItalic13px",
            "text": "Custom",
            "top": "0dp",
            "width": "52dp"
        }, {
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, {});
        flxRow1.add(flxLeft, flxRight, lblCustom);
        var flxRow2 = new kony.ui.FlexContainer({
            "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
            "bottom": "10dp",
            "clipBounds": true,
            "id": "flxRow2",
            "isVisible": true,
            "layoutType": kony.flex.FLOW_HORIZONTAL,
            "left": "0dp",
            "isModalContainer": false,
            "skin": "slFbox",
            "top": "8dp",
            "width": "100%"
        }, {}, {});
        flxRow2.setDefaultUnit(kony.flex.DP);
        var lblAvailableActions = new kony.ui.Label({
            "id": "lblAvailableActions",
            "isVisible": true,
            "left": "70dp",
            "skin": "sknLbl485C75LatoRegular13Px",
            "text": "Available Actions:",
            "top": "0dp",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, {
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, {});
        var lblCountActions = new kony.ui.Label({
            "id": "lblCountActions",
            "isVisible": true,
            "left": "10dp",
            "skin": "sknLbl192B45LatoSemiBold13px",
            "text": "2",
            "top": "0dp",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, {
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, {});
        var lblTotalActions = new kony.ui.Label({
            "id": "lblTotalActions",
            "isVisible": true,
            "left": "3dp",
            "skin": "sknLbl485C75LatoRegular13Px",
            "text": "of 2",
            "top": "0dp",
            "width": "40dp",
            "zIndex": 1
        }, {
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, {});
        flxRow2.add(lblAvailableActions, lblCountActions, lblTotalActions);
        var lblBottomSeperator = new kony.ui.Label({
            "height": "1dp",
            "id": "lblBottomSeperator",
            "isVisible": true,
            "left": "0dp",
            "skin": "sknlblSeperatorD7D9E0",
            "text": "Label",
            "top": "0dp",
            "width": "100%"
        }, {
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, {});
        flxContractEnrollFeaturesEditSection.add(lblTopSeperator, flxRow1, flxRow2, lblBottomSeperator);
        return flxContractEnrollFeaturesEditSection;
    }
})