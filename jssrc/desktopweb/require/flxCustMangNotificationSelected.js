define("flxCustMangNotificationSelected", function() {
    return function(controller) {
        var flxCustMangNotificationSelected = new kony.ui.FlexContainer({
            "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
            "clipBounds": true,
            "id": "flxCustMangNotificationSelected",
            "isVisible": true,
            "layoutType": kony.flex.FLOW_VERTICAL,
            "isModalContainer": false,
            "onClick": controller.AS_FlexContainer_e553ee3b13fe4973aef70f31c7776c5d,
            "skin": "slFbox"
        }, {}, {
            "hoverSkin": "sknfbfcfc"
        });
        flxCustMangNotificationSelected.setDefaultUnit(kony.flex.DP);
        var flxCustMangRequestHeader = new kony.ui.FlexContainer({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "clipBounds": true,
            "height": "50dp",
            "id": "flxCustMangRequestHeader",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "left": "0dp",
            "isModalContainer": false,
            "skin": "slFbox",
            "top": "0dp",
            "width": "100%",
            "zIndex": 1
        }, {}, {});
        flxCustMangRequestHeader.setDefaultUnit(kony.flex.DP);
        var flxFirstColoum = new kony.ui.FlexContainer({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "clipBounds": true,
            "height": "50dp",
            "id": "flxFirstColoum",
            "isVisible": true,
            "layoutType": kony.flex.FLOW_HORIZONTAL,
            "left": "0dp",
            "isModalContainer": false,
            "skin": "slFbox",
            "top": "0dp",
            "width": "46%",
            "zIndex": 2
        }, {}, {});
        flxFirstColoum.setDefaultUnit(kony.flex.DP);
        var flxArrow = new kony.ui.FlexContainer({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "centerY": "50%",
            "clipBounds": true,
            "height": "25px",
            "id": "flxArrow",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "left": "35px",
            "isModalContainer": false,
            "onClick": controller.AS_FlexContainer_ec8edea2bd1f48aa8bbc24f8f9f22b05,
            "skin": "sknCursor",
            "width": "25px",
            "zIndex": 2
        }, {}, {});
        flxArrow.setDefaultUnit(kony.flex.DP);
        var ImgArrow = new kony.ui.Image2({
            "centerX": "50%",
            "centerY": "50%",
            "height": "15px",
            "id": "ImgArrow",
            "isVisible": false,
            "left": "0%",
            "skin": "slImage",
            "src": "img_desc_arrow.png",
            "width": "15px",
            "zIndex": 2
        }, {
            "imageScaleMode": constants.IMAGE_SCALE_MODE_MAINTAIN_ASPECT_RATIO,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, {});
        var fonticonArrow = new kony.ui.Label({
            "centerX": "50%",
            "centerY": "50%",
            "height": "12px",
            "id": "fonticonArrow",
            "isVisible": true,
            "skin": "sknfontIconDescDownArrow12px",
            "text": "",
            "width": "12px",
            "zIndex": 1
        }, {
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, {});
        flxArrow.add(ImgArrow, fonticonArrow);
        var lblName = new kony.ui.Label({
            "centerY": "50%",
            "id": "lblName",
            "isVisible": true,
            "left": "10px",
            "skin": "sknlblLatoBold35475f14px",
            "text": "Summer Discount",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, {
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, {});
        flxFirstColoum.add(flxArrow, lblName);
        var lblStartDate = new kony.ui.Label({
            "centerY": "50%",
            "id": "lblStartDate",
            "isVisible": true,
            "left": "46%",
            "skin": "sknlblLatoBold35475f14px",
            "text": "12/12/2017",
            "width": "15%",
            "zIndex": 1
        }, {
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, {});
        var lblExpDate = new kony.ui.Label({
            "centerY": "50%",
            "id": "lblExpDate",
            "isVisible": true,
            "left": "61%",
            "skin": "sknlblLatoBold35475f14px",
            "text": "01/21/2018",
            "width": "15%",
            "zIndex": 1
        }, {
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, {});
        var lblStatus = new kony.ui.Label({
            "centerY": "50%",
            "id": "lblStatus",
            "isVisible": true,
            "left": "76%",
            "right": "55px",
            "skin": "sknlblLatoBold35475f14px",
            "text": "Starting soon",
            "zIndex": 1
        }, {
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, {});
        flxCustMangRequestHeader.add(flxFirstColoum, lblStartDate, lblExpDate, lblStatus);
        var flxCustMangRequestDesc = new kony.ui.FlexContainer({
            "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
            "bottom": "19px",
            "clipBounds": true,
            "id": "flxCustMangRequestDesc",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "left": "0dp",
            "isModalContainer": false,
            "skin": "slFbox",
            "top": "0dp",
            "width": "100%",
            "zIndex": 1
        }, {}, {});
        flxCustMangRequestDesc.setDefaultUnit(kony.flex.DP);
        var lblDesc = new kony.ui.RichText({
            "id": "lblDesc",
            "isVisible": true,
            "left": "65px",
            "right": "55px",
            "skin": "sknrtxLato0df8337c414274d",
            "text": "<html><Body><p>Select multiple billers and pay all your dues in one click!<br></p></body></html>",
            "top": "0dp",
            "zIndex": 2
        }, {
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, {});
        flxCustMangRequestDesc.add(lblDesc);
        var flxSeperator = new kony.ui.FlexContainer({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "clipBounds": true,
            "height": "1px",
            "id": "flxSeperator",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "left": "0dp",
            "isModalContainer": false,
            "skin": "slFbox",
            "top": "0dp",
            "width": "100%",
            "zIndex": 2
        }, {}, {});
        flxSeperator.setDefaultUnit(kony.flex.DP);
        var lblSeperator = new kony.ui.Label({
            "height": "1px",
            "id": "lblSeperator",
            "isVisible": true,
            "left": "35px",
            "right": "35px",
            "skin": "sknlblSeperator",
            "text": ".",
            "top": "0",
            "zIndex": 1
        }, {
            "contentAlignment": constants.CONTENT_ALIGN_TOP_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, {});
        flxSeperator.add(lblSeperator);
        flxCustMangNotificationSelected.add(flxCustMangRequestHeader, flxCustMangRequestDesc, flxSeperator);
        return flxCustMangNotificationSelected;
    }
})