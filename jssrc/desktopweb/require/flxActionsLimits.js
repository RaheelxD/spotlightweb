define("flxActionsLimits", function() {
    return function(controller) {
        var flxActionsLimits = new kony.ui.FlexContainer({
            "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
            "clipBounds": true,
            "id": "flxActionsLimits",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "left": "0dp",
            "isModalContainer": false,
            "skin": "sknflxffffffop100",
            "top": "0dp",
            "width": "100%"
        }, {}, {});
        flxActionsLimits.setDefaultUnit(kony.flex.DP);
        var flxLimitsRow1 = new kony.ui.FlexContainer({
            "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
            "bottom": 0,
            "clipBounds": true,
            "id": "flxLimitsRow1",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "left": "0dp",
            "isModalContainer": false,
            "skin": "slFbox",
            "top": "0dp",
            "width": "100%",
            "zIndex": 1
        }, {}, {});
        flxLimitsRow1.setDefaultUnit(kony.flex.DP);
        var lblActionName = new kony.ui.Label({
            "id": "lblActionName",
            "isVisible": true,
            "left": "1%",
            "skin": "sknLbl485C75LatoRegular13Px",
            "text": "Create Bill Payment",
            "top": "22dp",
            "width": "30%",
            "zIndex": 1
        }, {
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, {});
        var flxLimitValue1 = new kony.ui.FlexContainer({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "bottom": "15dp",
            "clipBounds": true,
            "height": "40dp",
            "id": "flxLimitValue1",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "left": "32%",
            "isModalContainer": false,
            "right": "15dp",
            "skin": "sknFlxbgFFFFFFbdrD7D9E0rd3px",
            "top": "15dp",
            "width": "19%"
        }, {}, {});
        flxLimitValue1.setDefaultUnit(kony.flex.DP);
        var lblCurrencySymbol1 = new kony.ui.Label({
            "centerY": "52%",
            "id": "lblCurrencySymbol1",
            "isVisible": true,
            "left": "5dp",
            "skin": "sknFontIcon485C7518px",
            "text": "",
            "width": "20dp",
            "zIndex": 1
        }, {
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, {});
        var tbxLimitValue1 = new kony.ui.TextBox2({
            "autoCapitalize": constants.TEXTBOX_AUTO_CAPITALIZE_NONE,
            "focusSkin": "sknTbx485c75LatoReg13pxNoBor",
            "height": "40dp",
            "id": "tbxLimitValue1",
            "isVisible": true,
            "keyBoardStyle": constants.TEXTBOX_KEY_BOARD_STYLE_DEFAULT,
            "left": "25dp",
            "right": "0dp",
            "secureTextEntry": false,
            "skin": "sknTbxBgFFFFFFf485c7513pxNoBorder",
            "textInputMode": constants.TEXTBOX_INPUT_MODE_NUMERIC,
            "top": "0dp",
            "zIndex": 1
        }, {
            "containerHeightMode": constants.TEXTBOX_FONT_METRICS_DRIVEN_HEIGHT,
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [1, 0, 0, 0],
            "paddingInPixel": false
        }, {
            "autoCorrect": false,
            "placeholderSkin": "defTextBoxPlaceholder"
        });
        flxLimitValue1.add(lblCurrencySymbol1, tbxLimitValue1);
        var flxLimitValue2 = new kony.ui.FlexContainer({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "bottom": "15dp",
            "clipBounds": true,
            "height": "40dp",
            "id": "flxLimitValue2",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "left": "54%",
            "isModalContainer": false,
            "right": "15dp",
            "skin": "sknFlxbgFFFFFFbdrD7D9E0rd3px",
            "top": "15dp",
            "width": "19%",
            "zIndex": 1
        }, {}, {});
        flxLimitValue2.setDefaultUnit(kony.flex.DP);
        var lblCurrencySymbol2 = new kony.ui.Label({
            "centerY": "52%",
            "id": "lblCurrencySymbol2",
            "isVisible": true,
            "left": "5dp",
            "skin": "sknFontIcon485C7518px",
            "text": "",
            "width": "20dp",
            "zIndex": 1
        }, {
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, {});
        var tbxLimitValue2 = new kony.ui.TextBox2({
            "autoCapitalize": constants.TEXTBOX_AUTO_CAPITALIZE_NONE,
            "focusSkin": "sknTbx485c75LatoReg13pxNoBor",
            "height": "40dp",
            "id": "tbxLimitValue2",
            "isVisible": true,
            "keyBoardStyle": constants.TEXTBOX_KEY_BOARD_STYLE_DEFAULT,
            "left": "25dp",
            "right": "0dp",
            "secureTextEntry": false,
            "skin": "sknTbxBgFFFFFFf485c7513pxNoBorder",
            "textInputMode": constants.TEXTBOX_INPUT_MODE_NUMERIC,
            "top": "0dp",
            "zIndex": 1
        }, {
            "containerHeightMode": constants.TEXTBOX_FONT_METRICS_DRIVEN_HEIGHT,
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [1, 0, 0, 0],
            "paddingInPixel": false
        }, {
            "autoCorrect": false,
            "placeholderSkin": "defTextBoxPlaceholder"
        });
        flxLimitValue2.add(lblCurrencySymbol2, tbxLimitValue2);
        var flxLimitValue3 = new kony.ui.FlexContainer({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "bottom": "15dp",
            "clipBounds": true,
            "height": "40dp",
            "id": "flxLimitValue3",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "left": "76%",
            "isModalContainer": false,
            "right": "15dp",
            "skin": "sknFlxbgFFFFFFbdrD7D9E0rd3px",
            "top": "15dp",
            "width": "19%"
        }, {}, {});
        flxLimitValue3.setDefaultUnit(kony.flex.DP);
        var lblCurrencySymbol3 = new kony.ui.Label({
            "centerY": "52%",
            "id": "lblCurrencySymbol3",
            "isVisible": true,
            "left": "5dp",
            "skin": "sknFontIcon485C7518px",
            "text": "",
            "width": "20dp",
            "zIndex": 1
        }, {
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, {});
        var tbxLimitValue3 = new kony.ui.TextBox2({
            "autoCapitalize": constants.TEXTBOX_AUTO_CAPITALIZE_NONE,
            "focusSkin": "sknTbx485c75LatoReg13pxNoBor",
            "height": "40dp",
            "id": "tbxLimitValue3",
            "isVisible": true,
            "keyBoardStyle": constants.TEXTBOX_KEY_BOARD_STYLE_DEFAULT,
            "left": "25dp",
            "right": "0dp",
            "secureTextEntry": false,
            "skin": "sknTbxBgFFFFFFf485c7513pxNoBorder",
            "textInputMode": constants.TEXTBOX_INPUT_MODE_NUMERIC,
            "top": "0dp",
            "zIndex": 1
        }, {
            "containerHeightMode": constants.TEXTBOX_FONT_METRICS_DRIVEN_HEIGHT,
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [1, 0, 0, 0],
            "paddingInPixel": false
        }, {
            "autoCorrect": false,
            "placeholderSkin": "defTextBoxPlaceholder"
        });
        flxLimitValue3.add(lblCurrencySymbol3, tbxLimitValue3);
        var flxLimit1Range = new kony.ui.FlexContainer({
            "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
            "bottom": "5dp",
            "clipBounds": true,
            "id": "flxLimit1Range",
            "isVisible": false,
            "layoutType": kony.flex.FLOW_HORIZONTAL,
            "left": "31.019999999999996%",
            "isModalContainer": false,
            "skin": "slFbox",
            "top": "60dp",
            "width": "20%",
            "zIndex": 1
        }, {}, {});
        flxLimit1Range.setDefaultUnit(kony.flex.DP);
        var lblRange = new kony.ui.Label({
            "id": "lblRange",
            "isVisible": true,
            "left": "2dp",
            "skin": "sknlblLatoReg485c7512px",
            "i18n_text": "kony.i18n.getLocalizedString(\"i18n.actionLimits.range\")",
            "top": "0",
            "width": kony.flex.USE_PREFFERED_SIZE
        }, {
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, {});
        var flxAmountRange = new kony.ui.FlexContainer({
            "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
            "clipBounds": true,
            "id": "flxAmountRange",
            "isVisible": true,
            "layoutType": kony.flex.FLOW_HORIZONTAL,
            "left": "2dp",
            "isModalContainer": false,
            "skin": "slFbox",
            "top": "0",
            "width": "80%"
        }, {}, {});
        flxAmountRange.setDefaultUnit(kony.flex.DP);
        var lblCurrencySymbol4 = new kony.ui.Label({
            "id": "lblCurrencySymbol4",
            "isVisible": true,
            "left": 0,
            "skin": "sknfontIcon192b4b14px",
            "text": "",
            "top": "1dp",
            "width": kony.flex.USE_PREFFERED_SIZE
        }, {
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, {});
        var lblLowerLimit = new kony.ui.Label({
            "id": "lblLowerLimit",
            "isVisible": true,
            "left": "0dp",
            "skin": "sknlblLatoReg485c7512px",
            "text": "2000",
            "top": "0",
            "width": kony.flex.USE_PREFFERED_SIZE
        }, {
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, {});
        var lblRangeSymbol = new kony.ui.Label({
            "id": "lblRangeSymbol",
            "isVisible": true,
            "left": "2%",
            "skin": "sknlblLatoReg485c7514px",
            "text": "-",
            "top": "0",
            "width": kony.flex.USE_PREFFERED_SIZE
        }, {
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, {});
        var lblCurrencySymbol5 = new kony.ui.Label({
            "id": "lblCurrencySymbol5",
            "isVisible": true,
            "left": "1%",
            "skin": "sknfontIcon192b4b14px",
            "text": "",
            "top": "1dp",
            "width": kony.flex.USE_PREFFERED_SIZE
        }, {
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, {});
        var lblUpperLimit = new kony.ui.Label({
            "id": "lblUpperLimit",
            "isVisible": true,
            "left": "0dp",
            "skin": "sknlblLatoReg485c7512px",
            "text": "4000",
            "top": "0",
            "width": kony.flex.USE_PREFFERED_SIZE
        }, {
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, {});
        flxAmountRange.add(lblCurrencySymbol4, lblLowerLimit, lblRangeSymbol, lblCurrencySymbol5, lblUpperLimit);
        flxLimit1Range.add(lblRange, flxAmountRange);
        var lblSeparator = new kony.ui.Label({
            "height": "1px",
            "id": "lblSeparator",
            "isVisible": true,
            "left": "1%",
            "right": "1%",
            "skin": "sknLblSeparatore7e7e7",
            "text": ".",
            "textStyle": {},
            "top": "0dp",
            "zIndex": 1
        }, {
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, {});
        flxLimitsRow1.add(lblActionName, flxLimitValue1, flxLimitValue2, flxLimitValue3, flxLimit1Range, lblSeparator);
        flxActionsLimits.add(flxLimitsRow1);
        return flxActionsLimits;
    }
})