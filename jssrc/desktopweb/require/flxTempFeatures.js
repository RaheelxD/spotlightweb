define("flxTempFeatures", function() {
    return function(controller) {
        var flxTempFeatures = new kony.ui.FlexContainer({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "clipBounds": true,
            "height": "60dp",
            "id": "flxTempFeatures",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "left": "0dp",
            "isModalContainer": false,
            "skin": "slFbox",
            "top": "0dp",
            "width": "100%"
        }, {}, {});
        flxTempFeatures.setDefaultUnit(kony.flex.DP);
        var flxFeaturesOuter = new kony.ui.FlexContainer({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "clipBounds": true,
            "height": "100%",
            "id": "flxFeaturesOuter",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "left": "20dp",
            "isModalContainer": false,
            "right": "20dp",
            "skin": "sknflxffffffop100Bordercbcdd1Radius3px",
            "top": "0dp"
        }, {}, {});
        flxFeaturesOuter.setDefaultUnit(kony.flex.DP);
        var flxFeaturesContainer = new kony.ui.FlexContainer({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "clipBounds": true,
            "height": "100%",
            "id": "flxFeaturesContainer",
            "isVisible": true,
            "layoutType": kony.flex.FLOW_VERTICAL,
            "left": "10dp",
            "isModalContainer": false,
            "right": "10dp",
            "skin": "slFbox",
            "top": "0dp",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, {}, {});
        flxFeaturesContainer.setDefaultUnit(kony.flex.DP);
        var flxFeatureUpper = new kony.ui.FlexContainer({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "clipBounds": true,
            "height": "25dp",
            "id": "flxFeatureUpper",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "left": "0dp",
            "isModalContainer": false,
            "right": "0dp",
            "skin": "slFbox",
            "top": 5,
            "width": "100%",
            "zIndex": 2
        }, {}, {});
        flxFeatureUpper.setDefaultUnit(kony.flex.DP);
        var flxCheckBox = new kony.ui.FlexContainer({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "centerY": "50%",
            "clipBounds": true,
            "height": "12px",
            "id": "flxCheckBox",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "left": "0%",
            "isModalContainer": false,
            "onClick": controller.AS_FlexContainer_e9eac13faa9844c789818b70d5a2e077,
            "skin": "slFbox",
            "top": "15dp",
            "width": "12px",
            "zIndex": 2
        }, {}, {});
        flxCheckBox.setDefaultUnit(kony.flex.DP);
        var ImgCheckBox = new kony.ui.Image2({
            "centerY": "50%",
            "height": "100%",
            "id": "ImgCheckBox",
            "isVisible": true,
            "left": "0%",
            "skin": "slImage",
            "src": "checkboxselected.png",
            "width": "100%",
            "zIndex": 2
        }, {
            "imageScaleMode": constants.IMAGE_SCALE_MODE_MAINTAIN_ASPECT_RATIO,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, {});
        flxCheckBox.add(ImgCheckBox);
        var lblFeature = new kony.ui.Label({
            "centerY": "50%",
            "id": "lblFeature",
            "isVisible": true,
            "left": "20dp",
            "skin": "sknLatoRegular192B4516px",
            "text": "Transfer to account in other FIs",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 2
        }, {
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, {});
        var flxStatus = new kony.ui.FlexContainer({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "clipBounds": true,
            "height": "100%",
            "id": "flxStatus",
            "isVisible": true,
            "layoutType": kony.flex.FLOW_HORIZONTAL,
            "reverseLayoutDirection": true,
            "isModalContainer": false,
            "right": "15dp",
            "skin": "slFbox",
            "top": "0dp",
            "width": "100px",
            "zIndex": 1
        }, {}, {});
        flxStatus.setDefaultUnit(kony.flex.DP);
        var lblUsersStatus = new kony.ui.Label({
            "centerY": "50%",
            "id": "lblUsersStatus",
            "isVisible": true,
            "left": "5dp",
            "right": "0dp",
            "skin": "sknlblLato5bc06cBold14px",
            "text": "Active",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, {
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, {});
        var fontIconStatusImg = new kony.ui.Label({
            "centerY": "50%",
            "id": "fontIconStatusImg",
            "isVisible": true,
            "left": "0dp",
            "skin": "sknFontIconActivate",
            "text": "",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, {
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, {});
        flxStatus.add(lblUsersStatus, fontIconStatusImg);
        flxFeatureUpper.add(flxCheckBox, lblFeature, flxStatus);
        var flxFeaturesLower = new kony.ui.FlexContainer({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "bottom": "5dp",
            "clipBounds": true,
            "height": "25dp",
            "id": "flxFeaturesLower",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "left": "0dp",
            "isModalContainer": false,
            "right": "0dp",
            "skin": "slFbox",
            "width": "100%",
            "zIndex": 2
        }, {}, {});
        flxFeaturesLower.setDefaultUnit(kony.flex.DP);
        var flxViewDetails = new kony.ui.FlexContainer({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "clipBounds": true,
            "height": "100%",
            "id": "flxViewDetails",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "left": "20dp",
            "isModalContainer": false,
            "skin": "slFbox",
            "top": "0dp",
            "width": "80dp"
        }, {}, {});
        flxViewDetails.setDefaultUnit(kony.flex.DP);
        var lblViewFeatureDetails = new kony.ui.Label({
            "centerY": "50%",
            "id": "lblViewFeatureDetails",
            "isVisible": true,
            "left": "0dp",
            "skin": "sknLblLato13px117eb0",
            "text": "View Details",
            "top": "0dp",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 2
        }, {
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, {});
        flxViewDetails.add(lblViewFeatureDetails);
        flxFeaturesLower.add(flxViewDetails);
        flxFeaturesContainer.add(flxFeatureUpper, flxFeaturesLower);
        flxFeaturesOuter.add(flxFeaturesContainer);
        flxTempFeatures.add(flxFeaturesOuter);
        return flxTempFeatures;
    }
})