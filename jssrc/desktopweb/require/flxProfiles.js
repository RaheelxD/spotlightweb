define("flxProfiles", function() {
    return function(controller) {
        var flxProfiles = new kony.ui.FlexContainer({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "clipBounds": true,
            "focusSkin": "sknflxffffffop100",
            "height": "50dp",
            "id": "flxProfiles",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "isModalContainer": false,
            "skin": "sknflxffffffop100"
        }, {}, {
            "hoverSkin": "sknfbfcfc"
        });
        flxProfiles.setDefaultUnit(kony.flex.DP);
        var lblName = new kony.ui.Label({
            "id": "lblName",
            "isVisible": true,
            "left": "30px",
            "skin": "sknlblLatoReg485c7513px",
            "text": "Name",
            "top": "15dp",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, {
            "contentAlignment": constants.CONTENT_ALIGN_TOP_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, {});
        var lblUsers = new kony.ui.Label({
            "id": "lblUsers",
            "isVisible": true,
            "left": "44%",
            "skin": "sknlblLatoReg485c7513px",
            "text": "83",
            "top": "15dp",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, {
            "contentAlignment": constants.CONTENT_ALIGN_TOP_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, {});
        var lblCampaigns = new kony.ui.Label({
            "id": "lblCampaigns",
            "isVisible": true,
            "left": "67%",
            "skin": "sknlblLatoReg485c7513px",
            "text": "5",
            "top": "15dp",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, {
            "contentAlignment": constants.CONTENT_ALIGN_TOP_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, {});
        var flxStatus = new kony.ui.FlexContainer({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "clipBounds": true,
            "height": "20px",
            "id": "flxStatus",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "left": "84%",
            "isModalContainer": false,
            "skin": "slFbox",
            "top": "15dp",
            "width": "90dp",
            "zIndex": 1
        }, {}, {});
        flxStatus.setDefaultUnit(kony.flex.DP);
        var fontIconStatus = new kony.ui.Label({
            "centerY": "50%",
            "id": "fontIconStatus",
            "isVisible": true,
            "left": "0dp",
            "skin": "sknFontIconActivate",
            "text": "",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 2
        }, {
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, {});
        var lblStatus = new kony.ui.Label({
            "centerY": "50%",
            "id": "lblStatus",
            "isVisible": true,
            "left": "20px",
            "skin": "sknlblLatoReg485c7513px",
            "text": "Active",
            "width": "60dp",
            "zIndex": 1
        }, {
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, {});
        flxStatus.add(fontIconStatus, lblStatus);
        var flxOptions = new kony.ui.FlexContainer({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "bottom": "10dp",
            "clipBounds": true,
            "height": "25px",
            "id": "flxOptions",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "left": "95%",
            "isModalContainer": false,
            "onClick": controller.AS_FlexContainer_je8c6ac6ecd94b228012f17d270072d2,
            "right": "65dp",
            "skin": "sknFlxBorffffff1pxRound",
            "top": "15dp",
            "width": "25px",
            "zIndex": 10
        }, {}, {
            "hoverSkin": "sknflxffffffop100Border424242Radius100px"
        });
        flxOptions.setDefaultUnit(kony.flex.DP);
        var lblOptions = new kony.ui.Label({
            "centerX": "46%",
            "centerY": "47%",
            "id": "lblOptions",
            "isVisible": true,
            "skin": "sknFontIconOptionMenu",
            "text": "",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 10
        }, {
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, {});
        flxOptions.add(lblOptions);
        var lblSeparator = new kony.ui.Label({
            "bottom": "0px",
            "centerX": "50%",
            "height": "1dp",
            "id": "lblSeparator",
            "isVisible": true,
            "left": "20dp",
            "right": "20dp",
            "skin": "sknlblSeperatorD7D9E0",
            "text": "Label",
            "zIndex": 2
        }, {
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, {});
        flxProfiles.add(lblName, lblUsers, lblCampaigns, flxStatus, flxOptions, lblSeparator);
        return flxProfiles;
    }
})