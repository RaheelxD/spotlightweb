define("flxAddSecurityQuestion", function() {
    return function(controller) {
        var flxAddSecurityQuestion = new kony.ui.FlexContainer({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "clipBounds": true,
            "height": "95px",
            "id": "flxAddSecurityQuestion",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "isModalContainer": false,
            "skin": "sknflxffffffop100"
        }, {}, {});
        flxAddSecurityQuestion.setDefaultUnit(kony.flex.DP);
        var flxQuestions = new kony.ui.FlexContainer({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "clipBounds": true,
            "height": "100%",
            "id": "flxQuestions",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "left": "20px",
            "isModalContainer": false,
            "skin": "slFbox",
            "top": "0dp",
            "width": "73%"
        }, {}, {});
        flxQuestions.setDefaultUnit(kony.flex.DP);
        var lblCharCount = new kony.ui.Label({
            "id": "lblCharCount",
            "isVisible": false,
            "right": "0px",
            "skin": "sknllbl485c75Lato13px",
            "text": "0/150",
            "top": "5px",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 5
        }, {
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, {});
        var txtAddSecurityQuestion = new kony.ui.TextArea2({
            "autoCapitalize": constants.TEXTAREA_AUTO_CAPITALIZE_NONE,
            "focusSkin": "skntxtAreaLato9ca9ba12Px",
            "height": "65px",
            "id": "txtAddSecurityQuestion",
            "isVisible": true,
            "keyBoardStyle": constants.TEXTAREA_KEY_BOARD_STYLE_DEFAULT,
            "left": "0px",
            "maxTextLength": 150,
            "numberOfVisibleLines": 3,
            "i18n_placeholder": "kony.i18n.getLocalizedString(\"i18n.SecurityQuestions.AddSecurityQuestion\")",
            "skin": "skntxtAddSecurityQuestion",
            "textInputMode": constants.TEXTAREA_INPUT_MODE_ANY,
            "top": "25px",
            "width": "100%",
            "zIndex": 1
        }, {
            "contentAlignment": constants.CONTENT_ALIGN_TOP_LEFT,
            "padding": [1, 1, 1, 1],
            "paddingInPixel": false
        }, {
            "autoCorrect": false
        });
        flxQuestions.add(lblCharCount, txtAddSecurityQuestion);
        var flxQuestionStatus = new kony.ui.FlexContainer({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "clipBounds": true,
            "height": "100%",
            "id": "flxQuestionStatus",
            "isVisible": true,
            "layoutType": kony.flex.FLOW_HORIZONTAL,
            "isModalContainer": false,
            "right": "60px",
            "skin": "slFbox",
            "top": "0dp",
            "width": "100px",
            "zIndex": 1
        }, {}, {});
        flxQuestionStatus.setDefaultUnit(kony.flex.DP);
        var lblQuestionStatus = new kony.ui.Label({
            "centerY": "50%",
            "id": "lblQuestionStatus",
            "isVisible": true,
            "right": "0px",
            "skin": "sknlblLatoBold35475f14px",
            "i18n_text": "kony.i18n.getLocalizedString(\"i18n.permission.Active\")",
            "top": "20px",
            "width": "50px",
            "zIndex": 1
        }, {
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, {});
        var flxQuestionStatusSwitch = new kony.ui.FlexContainer({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "centerY": "50%",
            "clipBounds": true,
            "height": "20dp",
            "id": "flxQuestionStatusSwitch",
            "isVisible": false,
            "layoutType": kony.flex.FREE_FORM,
            "left": "0dp",
            "isModalContainer": false,
            "skin": "slFbox",
            "width": "36px",
            "zIndex": 1
        }, {}, {});
        flxQuestionStatusSwitch.setDefaultUnit(kony.flex.DP);
        flxQuestionStatusSwitch.add();
        var StatusswitchToggle = new kony.ui.Switch({
            "centerY": "50%",
            "height": "25dp",
            "id": "StatusswitchToggle",
            "isVisible": true,
            "leftSideText": "ON",
            "right": "0px",
            "rightSideText": "OFF",
            "selectedIndex": 0,
            "skin": "sknSwitchServiceManagement",
            "width": "38dp",
            "zIndex": 1
        }, {
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, {});
        flxQuestionStatus.add(lblQuestionStatus, flxQuestionStatusSwitch, StatusswitchToggle);
        var flxDelete = new kony.ui.FlexContainer({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "centerY": "50%",
            "clipBounds": true,
            "height": "40dp",
            "id": "flxDelete",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "isModalContainer": false,
            "right": "20px",
            "skin": "sknCursor",
            "width": "50px",
            "zIndex": 1
        }, {}, {});
        flxDelete.setDefaultUnit(kony.flex.DP);
        var fontIconDelete = new kony.ui.Label({
            "centerY": "50%",
            "id": "fontIconDelete",
            "isVisible": true,
            "right": "0px",
            "skin": "sknFontIconOptionMenuRow",
            "text": "",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, {
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, {});
        flxDelete.add(fontIconDelete);
        var lblAddSecurityQuestionSeperator = new kony.ui.Label({
            "bottom": 0,
            "height": "1px",
            "id": "lblAddSecurityQuestionSeperator",
            "isVisible": false,
            "left": "20px",
            "right": "20px",
            "skin": "sknlblSeperator",
            "text": ".",
            "zIndex": 1
        }, {
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, {});
        flxAddSecurityQuestion.add(flxQuestions, flxQuestionStatus, flxDelete, lblAddSecurityQuestionSeperator);
        return flxAddSecurityQuestion;
    }
})