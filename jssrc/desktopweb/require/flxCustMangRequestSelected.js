define("flxCustMangRequestSelected", function() {
    return function(controller) {
        var flxCustMangRequestSelected = new kony.ui.FlexContainer({
            "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
            "clipBounds": true,
            "id": "flxCustMangRequestSelected",
            "isVisible": true,
            "layoutType": kony.flex.FLOW_VERTICAL,
            "isModalContainer": false,
            "onClick": controller.AS_FlexContainer_e553ee3b13fe4973aef70f31c7776c5d,
            "skin": "slFbox"
        }, {}, {
            "hoverSkin": "sknfbfcfc"
        });
        flxCustMangRequestSelected.setDefaultUnit(kony.flex.DP);
        var flxCustMangRequestHeader = new kony.ui.FlexContainer({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "clipBounds": true,
            "height": "50dp",
            "id": "flxCustMangRequestHeader",
            "isVisible": true,
            "layoutType": kony.flex.FLOW_HORIZONTAL,
            "left": "0dp",
            "isModalContainer": false,
            "skin": "slFbox",
            "top": "0dp",
            "width": "100%",
            "zIndex": 1
        }, {}, {});
        flxCustMangRequestHeader.setDefaultUnit(kony.flex.DP);
        var flxFirstColoum = new kony.ui.FlexContainer({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "centerY": "50%",
            "clipBounds": true,
            "height": "25px",
            "id": "flxFirstColoum",
            "isVisible": true,
            "layoutType": kony.flex.FLOW_HORIZONTAL,
            "left": "0px",
            "isModalContainer": false,
            "onClick": controller.AS_FlexContainer_g1f37c06974d4c3998965f5648994b5e,
            "skin": "slFbox",
            "width": "27%",
            "zIndex": 2
        }, {}, {});
        flxFirstColoum.setDefaultUnit(kony.flex.DP);
        var flxArrow = new kony.ui.FlexContainer({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "centerY": "50%",
            "clipBounds": true,
            "height": "25px",
            "id": "flxArrow",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "left": "35px",
            "isModalContainer": false,
            "onClick": controller.AS_FlexContainer_gc03ddf62d7d4ce59a3bd25ec02dd201,
            "skin": "sknCursor",
            "width": "25px",
            "zIndex": 2
        }, {}, {});
        flxArrow.setDefaultUnit(kony.flex.DP);
        var ImgArrow = new kony.ui.Image2({
            "centerX": "50%",
            "centerY": "50%",
            "height": "15px",
            "id": "ImgArrow",
            "isVisible": false,
            "skin": "slImage",
            "src": "img_desc_arrow.png",
            "width": "15px",
            "zIndex": 2
        }, {
            "imageScaleMode": constants.IMAGE_SCALE_MODE_MAINTAIN_ASPECT_RATIO,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, {});
        var fonticonArrow = new kony.ui.Label({
            "centerX": "50%",
            "centerY": "50%",
            "height": "12px",
            "id": "fonticonArrow",
            "isVisible": true,
            "skin": "sknfontIconDescDownArrow12px",
            "text": "",
            "width": "12px",
            "zIndex": 1
        }, {
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, {});
        flxArrow.add(ImgArrow, fonticonArrow);
        var lblRequestNumber = new kony.ui.Label({
            "centerY": "50%",
            "id": "lblRequestNumber",
            "isVisible": true,
            "left": "10px",
            "skin": "sknlblLatoBold35475f14px",
            "text": "RI123456",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, {
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, {});
        flxFirstColoum.add(flxArrow, lblRequestNumber);
        var lblCategory = new kony.ui.Label({
            "centerY": "50%",
            "id": "lblCategory",
            "isVisible": true,
            "left": "0",
            "skin": "sknlblLatoBold35475f14px",
            "text": "Need KFH's list",
            "width": "23%",
            "zIndex": 1
        }, {
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, {});
        var lblPriority = new kony.ui.Label({
            "centerY": "50%",
            "id": "lblPriority",
            "isVisible": true,
            "left": "0",
            "skin": "sknlblLatoBold35475f14px",
            "text": "Low",
            "width": "18%",
            "zIndex": 1
        }, {
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, {});
        var lblStatus = new kony.ui.Label({
            "centerY": "50%",
            "id": "lblStatus",
            "isVisible": true,
            "left": "0",
            "skin": "sknlblLatoBold35475f14px",
            "text": "Open",
            "width": "16%",
            "zIndex": 1
        }, {
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, {});
        var lblDate = new kony.ui.Label({
            "centerY": "50%",
            "id": "lblDate",
            "isVisible": true,
            "skin": "sknlblLatoBold35475f14px",
            "text": "12.10.2017",
            "width": "16%",
            "zIndex": 1
        }, {
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, {});
        flxCustMangRequestHeader.add(flxFirstColoum, lblCategory, lblPriority, lblStatus, lblDate);
        var flxCustMangRequestDesc = new kony.ui.FlexContainer({
            "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
            "bottom": "19px",
            "clipBounds": true,
            "id": "flxCustMangRequestDesc",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "left": "0dp",
            "isModalContainer": false,
            "skin": "slFbox",
            "top": "0dp",
            "width": "100%",
            "zIndex": 1
        }, {}, {});
        flxCustMangRequestDesc.setDefaultUnit(kony.flex.DP);
        var rtxDescription = new kony.ui.RichText({
            "id": "rtxDescription",
            "isVisible": true,
            "left": "70dp",
            "right": "55px",
            "skin": "sknrtxLato485c7514px",
            "text": "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aenean euismod bibendum laoreet. Proin gravida dolor sit amet lacus accumsan et viverra justo commodo. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aenean euismod bibendum laoreet. Proin gravida dolor sit amet lacus accumsan et viverra justo commodo.",
            "top": "0dp",
            "zIndex": 1
        }, {
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, {});
        flxCustMangRequestDesc.add(rtxDescription);
        var flxSeperator = new kony.ui.FlexContainer({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "clipBounds": true,
            "height": "1px",
            "id": "flxSeperator",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "left": "0dp",
            "isModalContainer": false,
            "skin": "slFbox",
            "top": "0dp",
            "width": "100%",
            "zIndex": 1
        }, {}, {});
        flxSeperator.setDefaultUnit(kony.flex.DP);
        var lblSeperator = new kony.ui.Label({
            "height": "1px",
            "id": "lblSeperator",
            "isVisible": true,
            "left": "0px",
            "right": "0px",
            "skin": "sknlblSeperator",
            "text": ".",
            "top": "0px",
            "width": "100%",
            "zIndex": 1
        }, {
            "contentAlignment": constants.CONTENT_ALIGN_TOP_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, {});
        flxSeperator.add(lblSeperator);
        flxCustMangRequestSelected.add(flxCustMangRequestHeader, flxCustMangRequestDesc, flxSeperator);
        return flxCustMangRequestSelected;
    }
})