define("flxEventsView", function() {
    return function(controller) {
        var flxEventsView = new kony.ui.FlexContainer({
            "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
            "clipBounds": true,
            "id": "flxEventsView",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "left": "0dp",
            "isModalContainer": false,
            "skin": "sknBackgroundFFFFFF",
            "top": "0dp",
            "width": "100%"
        }, {}, {});
        flxEventsView.setDefaultUnit(kony.flex.DP);
        var lblEvent = new kony.ui.Label({
            "bottom": "15dp",
            "id": "lblEvent",
            "isVisible": true,
            "left": "10dp",
            "skin": "sknLbl485C75LatoRegular13Px",
            "text": "Events Name",
            "top": "15dp",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, {
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, {});
        var lblEventSource = new kony.ui.Label({
            "bottom": "15dp",
            "id": "lblEventSource",
            "isVisible": true,
            "left": "38%",
            "skin": "sknLbl485C75LatoRegular13Px",
            "text": "Events Source",
            "top": "15dp",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, {
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, {});
        var lblEventCode = new kony.ui.Label({
            "bottom": "15dp",
            "id": "lblEventCode",
            "isVisible": true,
            "left": "71%",
            "skin": "sknLbl485C75LatoRegular13Px",
            "text": "Events Code",
            "top": "15dp",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, {
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, {});
        var flxDelete = new kony.ui.FlexContainer({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "clipBounds": true,
            "height": "20dp",
            "id": "flxDelete",
            "isVisible": false,
            "layoutType": kony.flex.FREE_FORM,
            "isModalContainer": false,
            "onClick": controller.AS_FlexContainer_h43e332b32624c87a4361f632e581db3,
            "right": 12,
            "skin": "hoverhandSkin",
            "top": "15dp",
            "width": "20dp"
        }, {}, {});
        flxDelete.setDefaultUnit(kony.flex.DP);
        var lblDelete = new kony.ui.Label({
            "centerX": "50%",
            "centerY": "50%",
            "height": "20dp",
            "id": "lblDelete",
            "isVisible": true,
            "skin": "sknIcon20px",
            "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmFAQ.fonticonDelete\")",
            "width": "20dp",
            "zIndex": 1
        }, {
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, {});
        flxDelete.add(lblDelete);
        var lblSeparator = new kony.ui.Label({
            "bottom": "0dp",
            "height": "1px",
            "id": "lblSeparator",
            "isVisible": true,
            "left": "0dp",
            "right": "0dp",
            "skin": "sknlblSeperatorD7D9E0",
            "text": "Label",
            "zIndex": 2
        }, {
            "contentAlignment": constants.CONTENT_ALIGN_TOP_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, {});
        flxEventsView.add(lblEvent, lblEventSource, lblEventCode, flxDelete, lblSeparator);
        return flxEventsView;
    }
})