define("flxFeatureLimits", function() {
    return function(controller) {
        var flxFeatureLimits = new kony.ui.FlexContainer({
            "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
            "clipBounds": true,
            "id": "flxFeatureLimits",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "isModalContainer": false,
            "skin": "sknflxffffffop100"
        }, {}, {});
        flxFeatureLimits.setDefaultUnit(kony.flex.DP);
        var lblApprovalLimits = new kony.ui.Label({
            "bottom": "15dp",
            "id": "lblApprovalLimits",
            "isVisible": true,
            "left": "20px",
            "skin": "sknlbl485C75LatoSemiBold13px",
            "text": "Admin Role",
            "top": "15dp",
            "width": "28%",
            "zIndex": 1
        }, {
            "contentAlignment": constants.CONTENT_ALIGN_TOP_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, {});
        var lblApprovers = new kony.ui.Label({
            "bottom": "15dp",
            "id": "lblApprovers",
            "isVisible": true,
            "left": "32%",
            "skin": "sknlbl485C75LatoSemiBold13px",
            "text": "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do",
            "top": "15dp",
            "width": "35%",
            "zIndex": 1
        }, {
            "contentAlignment": constants.CONTENT_ALIGN_TOP_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, {});
        var lblApprovalRule = new kony.ui.Label({
            "bottom": "15dp",
            "id": "lblApprovalRule",
            "isVisible": true,
            "left": "70%",
            "right": "20dp",
            "skin": "sknlbl485C75LatoSemiBold13px",
            "text": "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do",
            "top": "15dp",
            "zIndex": 1
        }, {
            "contentAlignment": constants.CONTENT_ALIGN_TOP_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, {});
        var lblSeperator = new kony.ui.Label({
            "height": "1px",
            "id": "lblSeperator",
            "isVisible": false,
            "left": "0dp",
            "skin": "sknlblSeperator",
            "text": ".",
            "top": "0dp",
            "width": "100%",
            "zIndex": 1
        }, {
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, {});
        flxFeatureLimits.add(lblApprovalLimits, lblApprovers, lblApprovalRule, lblSeperator);
        return flxFeatureLimits;
    }
})