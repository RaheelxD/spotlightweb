define("flxCSRQueueList", function() {
    return function(controller) {
        var flxCSRQueueList = new kony.ui.FlexContainer({
            "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
            "clipBounds": true,
            "id": "flxCSRQueueList",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "left": "0",
            "isModalContainer": false,
            "skin": "sknbackGroundffffff100",
            "top": "0",
            "width": "100%"
        }, {}, {
            "hoverSkin": "sknfbfcfc"
        });
        flxCSRQueueList.setDefaultUnit(kony.flex.DP);
        var flxCheckboxMsg = new kony.ui.FlexContainer({
            "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
            "bottom": "20dp",
            "clipBounds": true,
            "id": "flxCheckboxMsg",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "left": "20px",
            "isModalContainer": false,
            "skin": "sknCursor",
            "top": "20dp",
            "width": "30px",
            "zIndex": 2
        }, {}, {});
        flxCheckboxMsg.setDefaultUnit(kony.flex.DP);
        var imgCheckBoxMsg = new kony.ui.Image2({
            "height": "12px",
            "id": "imgCheckBoxMsg",
            "isVisible": true,
            "left": "0px",
            "skin": "slImage",
            "src": "checkbox.png",
            "top": "0dp",
            "width": "12px",
            "zIndex": 2
        }, {
            "imageScaleMode": constants.IMAGE_SCALE_MODE_MAINTAIN_ASPECT_RATIO,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, {});
        flxCheckboxMsg.add(imgCheckBoxMsg);
        var flxSegMain = new kony.ui.FlexContainer({
            "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
            "clipBounds": true,
            "id": "flxSegMain",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "left": "50dp",
            "isModalContainer": false,
            "skin": "sknCursor",
            "top": "0dp",
            "width": "100%",
            "zIndex": 2
        }, {}, {
            "hoverSkin": "sknfbfcfc"
        });
        flxSegMain.setDefaultUnit(kony.flex.DP);
        var flxMsgSegMain = new kony.ui.FlexContainer({
            "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
            "clipBounds": true,
            "id": "flxMsgSegMain",
            "isVisible": true,
            "layoutType": kony.flex.FLOW_HORIZONTAL,
            "left": "0",
            "isModalContainer": false,
            "skin": "slFbox",
            "top": "0px",
            "width": "100%"
        }, {}, {
            "hoverSkin": "sknfbfcfc"
        });
        flxMsgSegMain.setDefaultUnit(kony.flex.DP);
        var flxReqId = new kony.ui.FlexContainer({
            "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
            "clipBounds": true,
            "id": "flxReqId",
            "isVisible": true,
            "layoutType": kony.flex.FLOW_HORIZONTAL,
            "left": "20px",
            "isModalContainer": false,
            "skin": "slFbox",
            "top": "18px",
            "width": "120dp"
        }, {}, {});
        flxReqId.setDefaultUnit(kony.flex.DP);
        var lblReply = new kony.ui.Label({
            "id": "lblReply",
            "isVisible": false,
            "left": "0px",
            "skin": "sknicon15pxBlack",
            "text": "",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, {
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, {});
        var lblRequestID = new kony.ui.Label({
            "id": "lblRequestID",
            "isVisible": true,
            "left": "2px",
            "skin": "sknlblLatoBold35475f14px",
            "text": " RI333456",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, {
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, {});
        flxReqId.add(lblReply, lblRequestID);
        var lblCustomerId = new kony.ui.Label({
            "id": "lblCustomerId",
            "isVisible": true,
            "left": "20dp",
            "skin": "sknlblLatoBold35475f14px",
            "text": "John Doe(9), ",
            "top": "18px",
            "width": "120dp",
            "zIndex": 1
        }, {
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, {});
        var flxUserName = new kony.ui.FlexContainer({
            "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
            "clipBounds": true,
            "id": "flxUserName",
            "isVisible": true,
            "layoutType": kony.flex.FLOW_HORIZONTAL,
            "left": "20dp",
            "isModalContainer": false,
            "onClick": controller.AS_FlexContainer_a3ab7b9f4fbb49bfb050ba644d5f88b4,
            "skin": "slFbox",
            "top": "18px",
            "width": "150dp",
            "zIndex": 2
        }, {}, {});
        flxUserName.setDefaultUnit(kony.flex.DP);
        var lblCustomerName = new kony.ui.Label({
            "id": "lblCustomerName",
            "isVisible": true,
            "left": "2px",
            "skin": "sknlblLatoBold35475f14px",
            "text": "John Doe(9), ",
            "top": "0px",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, {
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, {});
        var lblDraft = new kony.ui.Label({
            "id": "lblDraft",
            "isVisible": true,
            "left": "2px",
            "skin": "sknlblLatoRegularee6565",
            "text": "Draft",
            "top": "0px",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, {
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, {});
        flxUserName.add(lblCustomerName, lblDraft);
        var lblSubject = new kony.ui.Label({
            "bottom": "18px",
            "id": "lblSubject",
            "isVisible": true,
            "left": "20dp",
            "skin": "sknlblLatoBold35475f14px",
            "text": "Lorem ipsum dolor sitconsectetu.. (9).orem ipsum dolor sitconsectetu.. (orem ipsum dolor sitconsectetu.. (9).",
            "top": "18px",
            "width": "180dp",
            "zIndex": 1
        }, {
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, {});
        var lblCategory = new kony.ui.Label({
            "id": "lblCategory",
            "isVisible": true,
            "left": "20dp",
            "skin": "sknlblLatoBold35475f14px",
            "text": "General banking",
            "top": "18px",
            "width": "110dp",
            "zIndex": 1
        }, {
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, {});
        var lblDate = new kony.ui.Label({
            "id": "lblDate",
            "isVisible": true,
            "left": "20dp",
            "skin": "sknlblLatoBold35475f14px",
            "text": "12.10.2017",
            "top": "18px",
            "width": "80dp",
            "zIndex": 1
        }, {
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, {});
        var flxStatus = new kony.ui.FlexContainer({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "clipBounds": true,
            "height": "20px",
            "id": "flxStatus",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "left": "20dp",
            "isModalContainer": false,
            "skin": "slFbox",
            "top": "18px",
            "width": "100dp",
            "zIndex": 1
        }, {}, {});
        flxStatus.setDefaultUnit(kony.flex.DP);
        var lblStatus = new kony.ui.Label({
            "centerY": "50%",
            "id": "lblStatus",
            "isVisible": true,
            "left": "20px",
            "skin": "sknlblLatoBold35475f14px",
            "text": "Active",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, {
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, {});
        var lblIconStatus = new kony.ui.Label({
            "centerY": "60%",
            "height": "15px",
            "id": "lblIconStatus",
            "isVisible": true,
            "left": "0px",
            "skin": "sknIcon13pxDarkBlue",
            "text": "",
            "width": "15px",
            "zIndex": 1
        }, {
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, {});
        var imgStatus = new kony.ui.Image2({
            "centerY": "50%",
            "height": "10dp",
            "id": "imgStatus",
            "isVisible": false,
            "left": "0dp",
            "skin": "slImage",
            "src": "active_circle2x.png",
            "width": "10dp",
            "zIndex": 1
        }, {
            "imageScaleMode": constants.IMAGE_SCALE_MODE_MAINTAIN_ASPECT_RATIO,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, {});
        flxStatus.add(lblStatus, lblIconStatus, imgStatus);
        var flxOptions = new kony.ui.FlexContainer({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "clipBounds": true,
            "height": "24dp",
            "id": "flxOptions",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "left": "20dp",
            "isModalContainer": false,
            "onClick": controller.AS_FlexContainer_af32134a8bde450e84866edb7b803362,
            "skin": "slFbox",
            "top": "18px",
            "width": "24px",
            "zIndex": 2
        }, {}, {
            "hoverSkin": "sknflxffffffop100Border424242Radius100px"
        });
        flxOptions.setDefaultUnit(kony.flex.DP);
        var imgOptions = new kony.ui.Image2({
            "centerX": "50%",
            "centerY": "50%",
            "height": "12dp",
            "id": "imgOptions",
            "isVisible": false,
            "left": 14,
            "onTouchStart": controller.AS_Image_b3c11d2c62ec47afb72f96de3bff3add,
            "skin": "slImage",
            "src": "dots3x.png",
            "width": "6dp",
            "zIndex": 1
        }, {
            "imageScaleMode": constants.IMAGE_SCALE_MODE_MAINTAIN_ASPECT_RATIO,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, {});
        var lblIconOptions = new kony.ui.Label({
            "centerY": "50%",
            "height": "15dp",
            "id": "lblIconOptions",
            "isVisible": true,
            "left": "3dp",
            "skin": "sknIcon15px",
            "text": "",
            "width": "15dp",
            "zIndex": 1
        }, {
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, {});
        flxOptions.add(imgOptions, lblIconOptions);
        flxMsgSegMain.add(flxReqId, lblCustomerId, flxUserName, lblSubject, lblCategory, lblDate, flxStatus, flxOptions);
        flxSegMain.add(flxMsgSegMain);
        var lblSeparator = new kony.ui.Label({
            "bottom": "0px",
            "height": "1px",
            "id": "lblSeparator",
            "isVisible": true,
            "left": "0px",
            "right": "0dp",
            "skin": "sknlblSeperator",
            "text": "'",
            "zIndex": 2
        }, {
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, {});
        flxCSRQueueList.add(flxCheckboxMsg, flxSegMain, lblSeparator);
        return flxCSRQueueList;
    }
})