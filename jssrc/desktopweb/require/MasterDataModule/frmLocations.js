define("MasterDataModule/frmLocations", function() {
    return function(controller) {
        function addWidgetsfrmLocations() {
            this.setDefaultUnit(kony.flex.DP);
            var flxMain = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "100%",
                "id": "flxMain",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "slFbox0b7aaa2e3bfa340",
                "top": "0dp",
                "width": "100%",
                "zIndex": 1
            }, {}, {});
            flxMain.setDefaultUnit(kony.flex.DP);
            var flxLeftPannel = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "100%",
                "id": "flxLeftPannel",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "sknLeftPannel0bb79e4bcca8e45",
                "top": "0dp",
                "width": "305px",
                "zIndex": 5
            }, {}, {});
            flxLeftPannel.setDefaultUnit(kony.flex.DP);
            var leftMenuNew = new com.adminConsole.navigation.leftMenuNew({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "height": "100%",
                "id": "leftMenuNew",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "masterType": constants.MASTER_TYPE_DEFAULT,
                "isModalContainer": false,
                "skin": "sknFlxBg003E75Op100",
                "top": "0dp",
                "width": "100%",
                "overrides": {
                    "leftMenuNew": {
                        "bottom": "viz.val_cleared",
                        "centerX": "viz.val_cleared",
                        "centerY": "viz.val_cleared",
                        "height": "100%",
                        "left": "0dp",
                        "maxHeight": "viz.val_cleared",
                        "maxWidth": "viz.val_cleared",
                        "minHeight": "viz.val_cleared",
                        "minWidth": "viz.val_cleared",
                        "right": "viz.val_cleared",
                        "top": "0dp",
                        "width": "100%"
                    }
                }
            }, {
                "overrides": {}
            }, {
                "overrides": {}
            });
            flxLeftPannel.add(leftMenuNew);
            var flxRightPannel = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "100%",
                "id": "flxRightPannel",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "305px",
                "isModalContainer": false,
                "right": 0,
                "skin": "slFbox0b7aaa2e3bfa340",
                "top": "0%",
                "zIndex": 5
            }, {}, {});
            flxRightPannel.setDefaultUnit(kony.flex.DP);
            var flxMainHeader = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "106dp",
                "id": "flxMainHeader",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0dp",
                "width": "100%",
                "zIndex": 1
            }, {}, {});
            flxMainHeader.setDefaultUnit(kony.flex.DP);
            var mainHeader = new com.adminConsole.locations.mainHeader({
                "height": "106px",
                "id": "mainHeader",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": 0,
                "masterType": constants.MASTER_TYPE_DEFAULT,
                "isModalContainer": false,
                "skin": "CopyslFbox1",
                "top": 0,
                "width": "100%",
                "overrides": {
                    "btnAddNewOption": {
                        "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmLocations.btnImport\")"
                    },
                    "flxDownloadList": {
                        "isVisible": false
                    },
                    "imgLogout": {
                        "src": "img_logout.png"
                    }
                }
            }, {
                "overrides": {}
            }, {
                "overrides": {}
            });
            var btnAddCase = new kony.ui.Button({
                "focusSkin": "sknBtn003E75LatoRegular13pxFFFFFFRad20px",
                "height": "30dp",
                "id": "btnAddCase",
                "isVisible": true,
                "right": "280dp",
                "skin": "sknBtn003E75LatoRegular13pxFFFFFFRad20px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.DragBox.ADD\")",
                "top": "60px",
                "width": "70px",
                "zIndex": 2
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_CENTER,
                "displayText": true,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {
                "hoverSkin": "sknBtn005198LatoRegular13pxFFFFFFRad20px"
            });
            flxMainHeader.add(mainHeader, btnAddCase);
            var flxHeaderDropdown = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                "clipBounds": true,
                "id": "flxHeaderDropdown",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "isModalContainer": false,
                "right": "35dp",
                "skin": "slFbox",
                "top": "20dp",
                "width": "400dp",
                "zIndex": 5
            }, {}, {});
            flxHeaderDropdown.setDefaultUnit(kony.flex.DP);
            var dropdownMainHeader = new com.adminConsole.common.dropdownMainHeader({
                "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                "id": "dropdownMainHeader",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "masterType": constants.MASTER_TYPE_DEFAULT,
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0dp",
                "width": "100%",
                "overrides": {
                    "flxDropdown": {
                        "isVisible": false
                    },
                    "imgUpArrow": {
                        "src": "uparrow_2x.png"
                    }
                }
            }, {
                "overrides": {}
            }, {
                "overrides": {}
            });
            flxHeaderDropdown.add(dropdownMainHeader);
            var flxBreadCrumbs = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "20px",
                "id": "flxBreadCrumbs",
                "isVisible": false,
                "layoutType": kony.flex.FLOW_HORIZONTAL,
                "left": "0dp",
                "masterType": constants.MASTER_TYPE_USERWIDGET,
                "isModalContainer": false,
                "skin": "sknflxf5f6f8Op100",
                "top": "105px",
                "width": "100%",
                "zIndex": 2
            }, {}, {});
            flxBreadCrumbs.setDefaultUnit(kony.flex.DP);
            var breadcrumbs = new com.adminConsole.common.breadcrumbs({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "height": "20px",
                "id": "breadcrumbs",
                "isVisible": true,
                "layoutType": kony.flex.FLOW_HORIZONTAL,
                "left": "0dp",
                "masterType": constants.MASTER_TYPE_DEFAULT,
                "isModalContainer": false,
                "skin": "sknflxf5f6f8Op100",
                "top": "0dp",
                "width": "100%",
                "overrides": {}
            }, {
                "overrides": {}
            }, {
                "overrides": {}
            });
            flxBreadCrumbs.add(breadcrumbs);
            var flxImportDetails = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "340dp",
                "id": "flxImportDetails",
                "isVisible": false,
                "layoutType": kony.flex.FREE_FORM,
                "left": "35dp",
                "isModalContainer": false,
                "right": "35dp",
                "skin": "sknflxUploadImage0e65d5a93fe0f45",
                "top": "125px",
                "zIndex": 1
            }, {}, {});
            flxImportDetails.setDefaultUnit(kony.flex.DP);
            var flxImportMain = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "100%",
                "id": "flxImportMain",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0dp",
                "width": "100%",
                "zIndex": 1
            }, {}, {});
            flxImportMain.setDefaultUnit(kony.flex.DP);
            var lblNoRecordAdded = new kony.ui.Label({
                "centerX": "50%",
                "id": "lblNoRecordAdded",
                "isVisible": true,
                "skin": "sknLblLato",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmLocations.lblNoRecordAdded\")",
                "top": "100px",
                "width": "100%",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_CENTER,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var flxLabels = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "centerX": "50%",
                "clipBounds": true,
                "height": "20dp",
                "id": "flxLabels",
                "isVisible": true,
                "layoutType": kony.flex.FLOW_HORIZONTAL,
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "153px",
                "width": "691px",
                "zIndex": 1
            }, {}, {});
            flxLabels.setDefaultUnit(kony.flex.DP);
            var lbl1 = new kony.ui.Label({
                "id": "lbl1",
                "isVisible": true,
                "left": "0px",
                "skin": "sknlbllatoRegular0f715a58455b840",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmLocations.lbl1\")",
                "top": "0dp",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var btnClickHere = new kony.ui.Button({
                "focusSkin": "btnClick",
                "id": "btnClickHere",
                "isVisible": true,
                "left": "2px",
                "right": "2px",
                "skin": "btnClick",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmLocations.btnClickHere\")",
                "top": "0px",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_CENTER,
                "displayText": true,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var lbl2 = new kony.ui.Label({
                "id": "lbl2",
                "isVisible": true,
                "left": "0px",
                "skin": "sknlbllatoRegular0f715a58455b840",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmLocations.lbl2\")",
                "top": "0dp",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxLabels.add(lbl1, btnClickHere, lbl2);
            var flxButtons = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                "centerX": "50%",
                "clipBounds": true,
                "id": "flxButtons",
                "isVisible": true,
                "layoutType": kony.flex.FLOW_HORIZONTAL,
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "185px",
                "width": "240px",
                "zIndex": 1
            }, {}, {});
            flxButtons.setDefaultUnit(kony.flex.DP);
            var btnAdd = new kony.ui.Button({
                "focusSkin": "sknBtn003E75LatoRegular13pxFFFFFFRad20px",
                "height": "40dp",
                "id": "btnAdd",
                "isVisible": true,
                "left": "0px",
                "skin": "sknBtn003E75LatoRegular13pxFFFFFFRad20px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.DragBox.ADD\")",
                "top": "0dp",
                "width": "110dp",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_CENTER,
                "displayText": true,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {
                "hoverSkin": "sknBtn005198LatoRegular13pxFFFFFFRad20px"
            });
            var btnImport = new kony.ui.Button({
                "focusSkin": "sknBtn003E75LatoRegular13pxFFFFFFRad20px",
                "height": "40dp",
                "id": "btnImport",
                "isVisible": true,
                "left": "20px",
                "skin": "sknBtn003E75LatoRegular13pxFFFFFFRad20px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmLocations.btnImport\")",
                "top": "0dp",
                "width": "110dp",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_CENTER,
                "displayText": true,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {
                "hoverSkin": "sknBtn005198LatoRegular13pxFFFFFFRad20px"
            });
            flxButtons.add(btnAdd, btnImport);
            flxImportMain.add(lblNoRecordAdded, flxLabels, flxButtons);
            var flxUploadingIndiacator = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "100%",
                "id": "flxUploadingIndiacator",
                "isVisible": false,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0dp",
                "width": "100%",
                "zIndex": 5
            }, {}, {});
            flxUploadingIndiacator.setDefaultUnit(kony.flex.DP);
            var lblUploading = new kony.ui.Label({
                "centerX": "50%",
                "height": "15px",
                "id": "lblUploading",
                "isVisible": true,
                "skin": "sknlbllatoRegular0bc8f2143d6204e",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.secureimage.Uploading\")",
                "top": "118px",
                "width": "40%",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_CENTER,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var flxUploadingStatusBar = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "centerX": "50%",
                "clipBounds": true,
                "height": "10px",
                "id": "flxUploadingStatusBar",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "isModalContainer": false,
                "skin": "sknflx0c0989fa7ba8d45",
                "top": "145px",
                "width": "302px",
                "zIndex": 1
            }, {}, {});
            flxUploadingStatusBar.setDefaultUnit(kony.flex.DP);
            flxUploadingStatusBar.add();
            var btnCancel = new kony.ui.Button({
                "centerX": "50%",
                "focusSkin": "sknBtnLatoRegulara5abc413pxKA",
                "height": "40px",
                "id": "btnCancel",
                "isVisible": false,
                "skin": "sknBtnLatoRegulara5abc413pxKA",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.secureimage.CANCEL\")",
                "top": "205px",
                "width": "110px",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_CENTER,
                "displayText": true,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {
                "hoverSkin": "sknbtnffffffLatoBolda5abc413Pxd7d8e01pxBorderRadius20px"
            });
            flxUploadingIndiacator.add(lblUploading, flxUploadingStatusBar, btnCancel);
            flxImportDetails.add(flxImportMain, flxUploadingIndiacator);
            var flxScrollMainContent = new kony.ui.FlexScrollContainer({
                "allowHorizontalBounce": false,
                "allowVerticalBounce": true,
                "bounces": true,
                "clipBounds": false,
                "enableScrolling": true,
                "height": "780px",
                "horizontalScrollIndicator": true,
                "id": "flxScrollMainContent",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0px",
                "pagingEnabled": false,
                "right": "0px",
                "scrollDirection": kony.flex.SCROLL_VERTICAL,
                "skin": "sknscrollflxf5f6f8Op100",
                "top": "115px",
                "verticalScrollIndicator": true,
                "zIndex": 1
            }, {}, {});
            flxScrollMainContent.setDefaultUnit(kony.flex.DP);
            var flxListingPage = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "100%",
                "id": "flxListingPage",
                "isVisible": false,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0px",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0px",
                "width": "100%",
                "zIndex": 1
            }, {}, {});
            flxListingPage.setDefaultUnit(kony.flex.DP);
            var flxMainSubHeader = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "60dp",
                "id": "flxMainSubHeader",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "-10dp",
                "width": "100%",
                "zIndex": 100
            }, {}, {});
            flxMainSubHeader.setDefaultUnit(kony.flex.DP);
            var subHeader = new com.adminConsole.header.subHeader({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "height": "60dp",
                "id": "subHeader",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "masterType": constants.MASTER_TYPE_DEFAULT,
                "isModalContainer": false,
                "skin": "CopyslFbox2",
                "top": "0dp",
                "width": "100%",
                "overrides": {
                    "flxClearSearchImage": {
                        "isVisible": false
                    },
                    "flxMenu": {
                        "isVisible": false,
                        "left": "35dp"
                    },
                    "flxSearch": {
                        "right": "35px"
                    },
                    "flxSearchContainer": {
                        "centerY": "50%",
                        "right": "viz.val_cleared",
                        "top": "viz.val_cleared"
                    },
                    "subHeader": {
                        "height": "60dp"
                    }
                }
            }, {
                "overrides": {}
            }, {
                "overrides": {}
            });
            flxMainSubHeader.add(subHeader);
            var flxLocationsWrapper = new kony.ui.FlexContainer({
                "clipBounds": true,
                "height": "100%",
                "id": "flxLocationsWrapper",
                "isVisible": true,
                "layoutType": kony.flex.FLOW_VERTICAL,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "50dp",
                "width": "100%",
                "zIndex": 100
            }, {}, {});
            flxLocationsWrapper.setDefaultUnit(kony.flex.DP);
            var flxSegmentWrapper = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                "clipBounds": true,
                "id": "flxSegmentWrapper",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0dp",
                "width": "100%",
                "zIndex": 1
            }, {}, {});
            flxSegmentWrapper.setDefaultUnit(kony.flex.DP);
            var flxHeaderAndSegmentWrapper = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                "bottom": "35px",
                "clipBounds": false,
                "id": "flxHeaderAndSegmentWrapper",
                "isVisible": true,
                "layoutType": kony.flex.FLOW_VERTICAL,
                "left": "35px",
                "isModalContainer": false,
                "right": "35px",
                "skin": "sknflxffffffop100Bordercbcdd1Radius3px",
                "top": "0dp",
                "zIndex": 1
            }, {}, {});
            flxHeaderAndSegmentWrapper.setDefaultUnit(kony.flex.DP);
            var flxLocationsHeader = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "60px",
                "id": "flxLocationsHeader",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0px",
                "isModalContainer": false,
                "right": "0px",
                "skin": "slFbox",
                "top": "0"
            }, {}, {});
            flxLocationsHeader.setDefaultUnit(kony.flex.DP);
            var flxLocationsHeaderName = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "100%",
                "id": "flxLocationsHeaderName",
                "isVisible": true,
                "layoutType": kony.flex.FLOW_HORIZONTAL,
                "left": "30px",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0dp",
                "width": "60px",
                "zIndex": 1
            }, {}, {});
            flxLocationsHeaderName.setDefaultUnit(kony.flex.DP);
            var lblHeaderName = new kony.ui.Label({
                "centerY": "50%",
                "id": "lblHeaderName",
                "isVisible": true,
                "left": "0",
                "skin": "sknlblLato696c7312px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.permission.NAME\")",
                "top": 0,
                "width": "38px",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {
                "hoverSkin": "sknlblLato00000011px"
            });
            var lblSortName = new kony.ui.Label({
                "centerY": "50%",
                "id": "lblSortName",
                "isVisible": true,
                "left": "0dp",
                "skin": "sknIcon15px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmAlertsManagement.lblSortName\")",
                "width": "15px",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxLocationsHeaderName.add(lblHeaderName, lblSortName);
            var flxLocationsHeaderCode = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "100%",
                "id": "flxLocationsHeaderCode",
                "isVisible": true,
                "layoutType": kony.flex.FLOW_HORIZONTAL,
                "left": "21.98%",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0dp",
                "width": "60px",
                "zIndex": 1
            }, {}, {});
            flxLocationsHeaderCode.setDefaultUnit(kony.flex.DP);
            var lblLocationsHeaderCode = new kony.ui.Label({
                "centerY": "50%",
                "id": "lblLocationsHeaderCode",
                "isVisible": true,
                "left": "0px",
                "skin": "sknlblLato696c7312px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.Group.CODE\")",
                "top": 0,
                "width": "35px",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {
                "hoverSkin": "sknlblLato00000011px"
            });
            var lblSortCode = new kony.ui.Label({
                "centerY": "50%",
                "id": "lblSortCode",
                "isVisible": true,
                "left": "0dp",
                "skin": "sknIcon15px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmAlertsManagement.lblSortName\")",
                "width": "15px",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxLocationsHeaderCode.add(lblLocationsHeaderCode, lblSortCode);
            var flxLocationsHeaderDescription = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "100%",
                "id": "flxLocationsHeaderDescription",
                "isVisible": true,
                "layoutType": kony.flex.FLOW_HORIZONTAL,
                "left": "32.96%",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0dp",
                "width": "100px",
                "zIndex": 1
            }, {}, {});
            flxLocationsHeaderDescription.setDefaultUnit(kony.flex.DP);
            var lblLocationsHeaderDescription = new kony.ui.Label({
                "centerY": "50%",
                "id": "lblLocationsHeaderDescription",
                "isVisible": true,
                "left": "0px",
                "skin": "sknlblLato696c7312px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.View.DESCRIPTION\")",
                "top": 0,
                "width": "80px",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxLocationsHeaderDescription.add(lblLocationsHeaderDescription);
            var flxLocationsHeaderPhone = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "100%",
                "id": "flxLocationsHeaderPhone",
                "isVisible": true,
                "layoutType": kony.flex.FLOW_HORIZONTAL,
                "left": "61.87%",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0dp",
                "width": "65px",
                "zIndex": 1
            }, {}, {});
            flxLocationsHeaderPhone.setDefaultUnit(kony.flex.DP);
            var lblLocationsHeaderPhone = new kony.ui.Label({
                "centerY": "50%",
                "id": "lblLocationsHeaderPhone",
                "isVisible": true,
                "left": "0px",
                "skin": "sknlblLato696c7312px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmCustomerCare.lblLocationsHeaderPhone\")",
                "top": 0,
                "width": "44px",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {
                "hoverSkin": "sknlblLato00000011px"
            });
            var lblSortPhone = new kony.ui.Label({
                "centerY": "50%",
                "id": "lblSortPhone",
                "isVisible": true,
                "left": "0dp",
                "skin": "sknIcon15px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmAlertsManagement.lblSortName\")",
                "width": "15px",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxLocationsHeaderPhone.add(lblLocationsHeaderPhone, lblSortPhone);
            var flxLocationsHeaderType = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "100%",
                "id": "flxLocationsHeaderType",
                "isVisible": true,
                "layoutType": kony.flex.FLOW_HORIZONTAL,
                "left": "74.73%",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0dp",
                "width": "55px",
                "zIndex": 1
            }, {}, {});
            flxLocationsHeaderType.setDefaultUnit(kony.flex.DP);
            var lblLocationsHeaderType = new kony.ui.Label({
                "centerY": "50%",
                "id": "lblLocationsHeaderType",
                "isVisible": true,
                "left": "0px",
                "skin": "sknlblLato696c7312px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.Group.TYPE\")",
                "top": 0,
                "width": "30px",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {
                "hoverSkin": "sknlblLato00000011px"
            });
            var lblFilterType = new kony.ui.Label({
                "centerY": "50%",
                "id": "lblFilterType",
                "isVisible": true,
                "left": "0dp",
                "skin": "sknIcon15px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmAlertsManagement.lblIconChannels\")",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {
                "hoverSkin": "sknlblCursorFont"
            });
            flxLocationsHeaderType.add(lblLocationsHeaderType, lblFilterType);
            var flxStatus = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "100%",
                "id": "flxStatus",
                "isVisible": true,
                "layoutType": kony.flex.FLOW_HORIZONTAL,
                "left": "84.06%",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0dp",
                "width": "65dp",
                "zIndex": 1
            }, {}, {});
            flxStatus.setDefaultUnit(kony.flex.DP);
            var lblStatus = new kony.ui.Label({
                "centerY": "50%",
                "id": "lblStatus",
                "isVisible": true,
                "left": "0px",
                "skin": "sknlblLato696c7312px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.roles.STATUS\")",
                "top": 0,
                "width": "45px",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {
                "hoverSkin": "sknlblLato00000011px"
            });
            var lblSortStatus = new kony.ui.Label({
                "centerY": "50%",
                "id": "lblSortStatus",
                "isVisible": true,
                "left": "0dp",
                "skin": "sknIcon15px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmAlertsManagement.lblIconChannels\")",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {
                "hoverSkin": "sknlblCursorFont"
            });
            flxStatus.add(lblStatus, lblSortStatus);
            var lblUsersHeaderSeperator = new kony.ui.Label({
                "bottom": 0,
                "height": "1px",
                "id": "lblUsersHeaderSeperator",
                "isVisible": true,
                "left": "20px",
                "right": "20px",
                "skin": "sknLblTableHeaderLine",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmCSR.lblSeperator\")",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxLocationsHeader.add(flxLocationsHeaderName, flxLocationsHeaderCode, flxLocationsHeaderDescription, flxLocationsHeaderPhone, flxLocationsHeaderType, flxStatus, lblUsersHeaderSeperator);
            var flxLocationsSegment = new kony.ui.FlexScrollContainer({
                "allowHorizontalBounce": false,
                "allowVerticalBounce": true,
                "bounces": true,
                "clipBounds": true,
                "enableScrolling": true,
                "height": "300dp",
                "horizontalScrollIndicator": true,
                "id": "flxLocationsSegment",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "pagingEnabled": false,
                "scrollDirection": kony.flex.SCROLL_VERTICAL,
                "skin": "slFSbox",
                "top": "0dp",
                "verticalScrollIndicator": true,
                "width": "100%",
                "zIndex": 1
            }, {}, {});
            flxLocationsSegment.setDefaultUnit(kony.flex.DP);
            var listingSegmentClient = new com.adminConsole.common.listingSegmentClient({
                "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                "id": "listingSegmentClient",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "masterType": constants.MASTER_TYPE_DEFAULT,
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0dp",
                "width": "100%",
                "overrides": {
                    "contextualMenu.imgOption1": {
                        "src": "imagedrag.png"
                    },
                    "contextualMenu.imgOption2": {
                        "src": "edit2x.png"
                    },
                    "contextualMenu.imgOption3": {
                        "src": "imagedrag.png"
                    },
                    "contextualMenu.imgOption4": {
                        "src": "deactive_2x.png"
                    },
                    "flxContextualMenu": {
                        "right": "32px",
                        "width": "130px"
                    },
                    "flxPagination": {
                        "isVisible": false
                    },
                    "listingSegmentClient": {
                        "autogrowMode": kony.flex.AUTOGROW_HEIGHT
                    },
                    "segListing": {
                        "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                        "bottom": "viz.val_cleared",
                        "isVisible": false
                    }
                }
            }, {
                "overrides": {}
            }, {
                "overrides": {}
            });
            flxLocationsSegment.add(listingSegmentClient);
            flxHeaderAndSegmentWrapper.add(flxLocationsHeader, flxLocationsSegment);
            var flxTypeFilter = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                "clipBounds": true,
                "id": "flxTypeFilter",
                "isVisible": false,
                "layoutType": kony.flex.FREE_FORM,
                "isModalContainer": false,
                "right": "150dp",
                "skin": "slFbox",
                "top": "35px",
                "width": "95dp",
                "zIndex": 5
            }, {}, {});
            flxTypeFilter.setDefaultUnit(kony.flex.DP);
            var typeFilterMenu = new com.adminConsole.common.statusFilterMenu({
                "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                "id": "typeFilterMenu",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "masterType": constants.MASTER_TYPE_DEFAULT,
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "5dp",
                "width": "100%",
                "zIndex": 5,
                "overrides": {
                    "imgUpArrow": {
                        "src": "uparrow_2x.png"
                    },
                    "segStatusFilterDropdown": {
                        "bottom": "10dp",
                        "data": [{
                            "imgCheckBox": "checkbox.png",
                            "lblDescription": "Branch"
                        }, {
                            "imgCheckBox": "checkbox.png",
                            "lblDescription": "ATM"
                        }],
                        "left": "0dp",
                        "top": "10dp"
                    }
                }
            }, {
                "overrides": {}
            }, {
                "overrides": {}
            });
            flxTypeFilter.add(typeFilterMenu);
            var flxStatusFilter = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                "clipBounds": true,
                "id": "flxStatusFilter",
                "isVisible": false,
                "layoutType": kony.flex.FREE_FORM,
                "isModalContainer": false,
                "right": "80dp",
                "skin": "slFbox",
                "top": "35px",
                "width": "100dp",
                "zIndex": 10
            }, {}, {});
            flxStatusFilter.setDefaultUnit(kony.flex.DP);
            var statusFilterMenu = new com.adminConsole.common.statusFilterMenu({
                "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                "id": "statusFilterMenu",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "masterType": constants.MASTER_TYPE_DEFAULT,
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "5dp",
                "width": "100%",
                "zIndex": 5,
                "overrides": {
                    "imgUpArrow": {
                        "src": "uparrow_2x.png"
                    },
                    "segStatusFilterDropdown": {
                        "bottom": "10dp",
                        "data": [{
                            "imgCheckBox": "checkbox.png",
                            "lblDescription": "Active"
                        }, {
                            "imgCheckBox": "checkbox.png",
                            "lblDescription": "Inactive"
                        }],
                        "left": "0dp",
                        "top": "10dp"
                    }
                }
            }, {
                "overrides": {}
            }, {
                "overrides": {}
            });
            flxStatusFilter.add(statusFilterMenu);
            flxSegmentWrapper.add(flxHeaderAndSegmentWrapper, flxTypeFilter, flxStatusFilter);
            flxLocationsWrapper.add(flxSegmentWrapper);
            flxListingPage.add(flxMainSubHeader, flxLocationsWrapper);
            var flxViewLocation = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                "clipBounds": true,
                "id": "flxViewLocation",
                "isVisible": false,
                "layoutType": kony.flex.FLOW_VERTICAL,
                "left": "35dp",
                "isModalContainer": false,
                "right": 35,
                "skin": "slFbox",
                "top": "25px",
                "zIndex": 1
            }, {}, {});
            flxViewLocation.setDefaultUnit(kony.flex.DP);
            var flxViewDetails = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                "clipBounds": true,
                "id": "flxViewDetails",
                "isVisible": true,
                "layoutType": kony.flex.FLOW_VERTICAL,
                "left": "0px",
                "isModalContainer": false,
                "skin": "sknflxffffffBorderd6dbe7Radius4px",
                "top": "0px",
                "width": "100%",
                "zIndex": 1
            }, {}, {});
            flxViewDetails.setDefaultUnit(kony.flex.DP);
            var flxHeading = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "40dp",
                "id": "flxHeading",
                "isVisible": true,
                "layoutType": kony.flex.FLOW_HORIZONTAL,
                "left": "20dp",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "5dp",
                "width": "30%",
                "zIndex": 1
            }, {}, {});
            flxHeading.setDefaultUnit(kony.flex.DP);
            var lblSpecialBranch = new kony.ui.Label({
                "centerY": "50%",
                "id": "lblSpecialBranch",
                "isVisible": true,
                "left": "0dp",
                "skin": "sknlblLatoBold35475f14px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmLocations.lblSpecialBranch\")",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var lblViewKey = new kony.ui.Label({
                "centerY": "51%",
                "id": "lblViewKey",
                "isVisible": true,
                "left": "10dp",
                "skin": "sknIcon13pxGreen",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmAlertsManagement.lblViewAlertStatusKey\")",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var lblViewValue = new kony.ui.Label({
                "centerY": "50%",
                "id": "lblViewValue",
                "isVisible": true,
                "left": "5dp",
                "skin": "sknlblLato5bc06cBold14px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.secureimage.Active\")",
                "width": "60dp",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxHeading.add(lblSpecialBranch, lblViewKey, lblViewValue);
            var flxViewEditButton = new kony.ui.Button({
                "focusSkin": "sknbtnf7f7faLatoRegular12Px485c75Border1px485C75Radius20px",
                "height": "22px",
                "id": "flxViewEditButton",
                "isVisible": true,
                "right": "20px",
                "skin": "sknbtnf7f7faLatoRegular12Px485c75Border1px485C75Radius20px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.permission.Edit\")",
                "top": "-20px",
                "width": "50px",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_CENTER,
                "displayText": true,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {
                "hoverSkin": "sknbtnffffffLatoRegular12Px485c75Border1px485C75Radius20px"
            });
            var flxDetails1 = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "20dp",
                "id": "flxDetails1",
                "isVisible": true,
                "layoutType": kony.flex.FLOW_HORIZONTAL,
                "left": "20px",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "10dp",
                "width": "8%",
                "zIndex": 1
            }, {}, {
                "hoverSkin": "sknCursor"
            });
            flxDetails1.setDefaultUnit(kony.flex.DP);
            var lblSubTitle1 = new kony.ui.Label({
                "centerY": "50%",
                "id": "lblSubTitle1",
                "isVisible": true,
                "left": "5px",
                "skin": "sknlblSubHeader0b73b6af9628b4e",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmRolesController.DETAILS\")",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var flxImgArrow1 = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "centerY": "50%",
                "clipBounds": true,
                "height": "20dp",
                "id": "flxImgArrow1",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "slFbox",
                "width": "20px",
                "zIndex": 1
            }, {}, {
                "hoverSkin": "sknCursor"
            });
            flxImgArrow1.setDefaultUnit(kony.flex.DP);
            var lblToggleDetails = new kony.ui.Label({
                "centerX": "50%",
                "centerY": "50%",
                "height": "12dp",
                "id": "lblToggleDetails",
                "isVisible": true,
                "skin": "sknIcon13pxGray",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmAlertsManagement.lblIconDescription\")",
                "width": "12dp",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxImgArrow1.add(lblToggleDetails);
            flxDetails1.add(lblSubTitle1, flxImgArrow1);
            var flxDetailsData1 = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                "clipBounds": true,
                "id": "flxDetailsData1",
                "isVisible": true,
                "layoutType": kony.flex.FLOW_VERTICAL,
                "left": "0px",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "10px",
                "width": "100%",
                "zIndex": 1
            }, {}, {});
            flxDetailsData1.setDefaultUnit(kony.flex.DP);
            var detailsRow1 = new com.adminConsole.view.details1({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "height": "40px",
                "id": "detailsRow1",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "masterType": constants.MASTER_TYPE_DEFAULT,
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0dp",
                "width": "100%",
                "overrides": {
                    "details1": {
                        "bottom": "viz.val_cleared",
                        "centerX": "viz.val_cleared",
                        "centerY": "viz.val_cleared",
                        "height": "40px",
                        "left": "0dp",
                        "maxHeight": "viz.val_cleared",
                        "maxWidth": "viz.val_cleared",
                        "minHeight": "viz.val_cleared",
                        "minWidth": "viz.val_cleared",
                        "right": "viz.val_cleared",
                        "top": "0dp",
                        "width": "100%"
                    },
                    "lblIconData2": {
                        "isVisible": false
                    },
                    "lblIconData3": {
                        "isVisible": false
                    }
                }
            }, {
                "overrides": {}
            }, {
                "overrides": {}
            });
            var detailsRow2 = new com.adminConsole.view.details1({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "height": "40px",
                "id": "detailsRow2",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "masterType": constants.MASTER_TYPE_DEFAULT,
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "20px",
                "width": "100%",
                "overrides": {
                    "details1": {
                        "bottom": "viz.val_cleared",
                        "centerX": "viz.val_cleared",
                        "centerY": "viz.val_cleared",
                        "height": "40px",
                        "left": "0dp",
                        "maxHeight": "viz.val_cleared",
                        "maxWidth": "viz.val_cleared",
                        "minHeight": "viz.val_cleared",
                        "minWidth": "viz.val_cleared",
                        "right": "viz.val_cleared",
                        "top": "20px",
                        "width": "100%"
                    }
                }
            }, {
                "overrides": {}
            }, {
                "overrides": {}
            });
            flxDetailsData1.add(detailsRow1, detailsRow2);
            var flxContactDetails = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "20dp",
                "id": "flxContactDetails",
                "isVisible": true,
                "layoutType": kony.flex.FLOW_HORIZONTAL,
                "left": "20px",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "15px",
                "width": "14%",
                "zIndex": 1
            }, {}, {
                "hoverSkin": "sknCursor"
            });
            flxContactDetails.setDefaultUnit(kony.flex.DP);
            var lblContactDetails = new kony.ui.Label({
                "centerY": "50%",
                "id": "lblContactDetails",
                "isVisible": true,
                "left": "0dp",
                "skin": "sknlblSubHeader0b73b6af9628b4e",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmLocations.lblContactDetails\")",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var flxArrow2 = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "centerY": "50%",
                "clipBounds": true,
                "height": "20dp",
                "id": "flxArrow2",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "isModalContainer": false,
                "right": "5px",
                "skin": "slFbox",
                "width": "20px",
                "zIndex": 1
            }, {}, {
                "hoverSkin": "sknCursor"
            });
            flxArrow2.setDefaultUnit(kony.flex.DP);
            var lblToggleContact = new kony.ui.Label({
                "centerX": "50%",
                "centerY": "50%",
                "height": "12dp",
                "id": "lblToggleContact",
                "isVisible": true,
                "skin": "sknIcon13pxGray",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmAlertsManagement.lblIconDescription\")",
                "width": "12dp",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxArrow2.add(lblToggleContact);
            flxContactDetails.add(lblContactDetails, flxArrow2);
            var flxContactDetailsData = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                "clipBounds": true,
                "id": "flxContactDetailsData",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0px",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "10px",
                "width": "100%",
                "zIndex": 1
            }, {}, {});
            flxContactDetailsData.setDefaultUnit(kony.flex.DP);
            var detailsMultiLine = new com.adminConsole.view.detailsMultiLine({
                "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                "id": "detailsMultiLine",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "masterType": constants.MASTER_TYPE_DEFAULT,
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0dp",
                "width": "100%",
                "overrides": {
                    "lblHeading1": {
                        "i18n_text": "kony.i18n.getLocalizedString(\"i18n.View.ADDRESS\")"
                    },
                    "lblHeading2": {
                        "i18n_text": "kony.i18n.getLocalizedString(\"i18n.View.EMAILID\")"
                    },
                    "lblHeading3": {
                        "i18n_text": "kony.i18n.getLocalizedString(\"i18n.View.GEOLOCATION\")"
                    },
                    "rtxData1": {
                        "text": "The Business Centre<br>\n61 Wellfield Road<br>\nRoath Cardiff CF24 3DG"
                    },
                    "rtxData2": {
                        "text": "Loremipsum@konybank.com"
                    },
                    "rtxData3": {
                        "text": "Longitude: 12.12<br>\nLatitude : 61.34"
                    }
                }
            }, {
                "overrides": {}
            }, {
                "overrides": {}
            });
            flxContactDetailsData.add(detailsMultiLine);
            var flxOperationDetails = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "20dp",
                "id": "flxOperationDetails",
                "isVisible": true,
                "layoutType": kony.flex.FLOW_HORIZONTAL,
                "left": "20px",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "15px",
                "width": "15%",
                "zIndex": 1
            }, {}, {
                "hoverSkin": "sknCursor"
            });
            flxOperationDetails.setDefaultUnit(kony.flex.DP);
            var lblOperationDetails1 = new kony.ui.Label({
                "centerY": "50%",
                "id": "lblOperationDetails1",
                "isVisible": true,
                "left": "0dp",
                "skin": "sknlblSubHeader0b73b6af9628b4e",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmLocations.lblOperationDetails1\")",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var flximgArrow2 = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "centerY": "50%",
                "clipBounds": true,
                "height": "20dp",
                "id": "flximgArrow2",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "isModalContainer": false,
                "right": "5px",
                "skin": "slFbox",
                "width": "20px",
                "zIndex": 1
            }, {}, {
                "hoverSkin": "sknCursor"
            });
            flximgArrow2.setDefaultUnit(kony.flex.DP);
            var lblToggleIperationDetails = new kony.ui.Label({
                "centerX": "50%",
                "centerY": "50%",
                "height": "12dp",
                "id": "lblToggleIperationDetails",
                "isVisible": true,
                "skin": "sknIcon13pxGray",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmAlertsManagement.lblIconDescription\")",
                "width": "12dp",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flximgArrow2.add(lblToggleIperationDetails);
            flxOperationDetails.add(lblOperationDetails1, flximgArrow2);
            var flxOperationDetailsDataWrapper = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                "clipBounds": true,
                "id": "flxOperationDetailsDataWrapper",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0px",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "10px",
                "width": "100%",
                "zIndex": 1
            }, {}, {});
            flxOperationDetailsDataWrapper.setDefaultUnit(kony.flex.DP);
            var operationDetails = new com.adminConsole.view.details({
                "height": "40px",
                "id": "operationDetails",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "masterType": constants.MASTER_TYPE_DEFAULT,
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0dp",
                "width": "100%",
                "overrides": {
                    "imgData1": {
                        "src": "active_circle2x.png"
                    },
                    "imgData2": {
                        "src": "active_circle2x.png"
                    },
                    "imgData3": {
                        "src": "active_circle2x.png"
                    }
                }
            }, {
                "overrides": {}
            }, {
                "overrides": {}
            });
            flxOperationDetailsDataWrapper.add(operationDetails);
            var flxViewDescription = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "bottom": 10,
                "clipBounds": true,
                "height": "90px",
                "id": "flxViewDescription",
                "isVisible": true,
                "layoutType": kony.flex.FLOW_HORIZONTAL,
                "left": "0px",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "10px",
                "width": "100%",
                "zIndex": 1
            }, {}, {});
            flxViewDescription.setDefaultUnit(kony.flex.DP);
            var Description = new com.adminConsole.view.Description1({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "height": "220dp",
                "id": "Description",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "masterType": constants.MASTER_TYPE_DEFAULT,
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0dp",
                "width": "100%",
                "overrides": {
                    "Description1": {
                        "bottom": "viz.val_cleared",
                        "centerX": "viz.val_cleared",
                        "centerY": "viz.val_cleared",
                        "height": "220dp",
                        "left": "0dp",
                        "maxHeight": "viz.val_cleared",
                        "maxWidth": "viz.val_cleared",
                        "minHeight": "viz.val_cleared",
                        "minWidth": "viz.val_cleared",
                        "right": "viz.val_cleared",
                        "top": "0dp",
                        "width": "100%"
                    },
                    "imgToggleDescription": {
                        "src": "img_down_arrow.png"
                    }
                }
            }, {
                "overrides": {}
            }, {
                "overrides": {}
            });
            flxViewDescription.add(Description);
            flxViewDetails.add(flxHeading, flxViewEditButton, flxDetails1, flxDetailsData1, flxContactDetails, flxContactDetailsData, flxOperationDetails, flxOperationDetailsDataWrapper, flxViewDescription);
            var flxViewServices = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                "bottom": "35px",
                "clipBounds": true,
                "id": "flxViewServices",
                "isVisible": true,
                "layoutType": kony.flex.FLOW_VERTICAL,
                "left": "0px",
                "isModalContainer": false,
                "skin": "sknflxffffffBorderd6dbe7Radius4px",
                "top": "20px",
                "width": "100%",
                "zIndex": 1
            }, {}, {});
            flxViewServices.setDefaultUnit(kony.flex.DP);
            var flxViewTabs = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "65dp",
                "id": "flxViewTabs",
                "isVisible": true,
                "layoutType": kony.flex.FLOW_HORIZONTAL,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0dp",
                "width": "100%",
                "zIndex": 1
            }, {}, {});
            flxViewTabs.setDefaultUnit(kony.flex.DP);
            var lblServices = new kony.ui.Label({
                "centerY": "50%",
                "id": "lblServices",
                "isVisible": true,
                "left": "20dp",
                "skin": "CopyslLabel0b2a456ab2c7742",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmLocations.lblServices\")",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxViewTabs.add(lblServices);
            var flxViewSeperator = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "1px",
                "id": "flxViewSeperator",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "sknflxD5D9DD",
                "top": "0dp",
                "width": "100%",
                "zIndex": 1
            }, {}, {});
            flxViewSeperator.setDefaultUnit(kony.flex.DP);
            flxViewSeperator.add();
            var flxViewSegmentAndHeaders = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                "clipBounds": true,
                "id": "flxViewSegmentAndHeaders",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "isModalContainer": false,
                "right": 0,
                "skin": "slFbox",
                "top": "0dp",
                "zIndex": 1
            }, {}, {});
            flxViewSegmentAndHeaders.setDefaultUnit(kony.flex.DP);
            var flxServicesHeader = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "60px",
                "id": "flxServicesHeader",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": 20,
                "isModalContainer": false,
                "right": 20,
                "skin": "sknflxffffffop100",
                "top": "0"
            }, {}, {});
            flxServicesHeader.setDefaultUnit(kony.flex.DP);
            var flxViewServiceName = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "100%",
                "id": "flxViewServiceName",
                "isVisible": true,
                "layoutType": kony.flex.FLOW_HORIZONTAL,
                "left": "10px",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0dp",
                "width": "100px",
                "zIndex": 1
            }, {}, {});
            flxViewServiceName.setDefaultUnit(kony.flex.DP);
            var lblViewServiceName = new kony.ui.Label({
                "centerY": "50%",
                "id": "lblViewServiceName",
                "isVisible": true,
                "left": "0px",
                "skin": "sknlblLato5d6c7f12px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.permission.NAME\")",
                "width": "37px",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var flxSortLocationServices = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "centerY": "50%",
                "clipBounds": true,
                "height": "20dp",
                "id": "flxSortLocationServices",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "CopyslFbox0efc73ba91cad4b",
                "width": "20px",
                "zIndex": 1
            }, {}, {});
            flxSortLocationServices.setDefaultUnit(kony.flex.DP);
            var lblSortLocationServices = new kony.ui.Label({
                "centerY": "50%",
                "height": "15dp",
                "id": "lblSortLocationServices",
                "isVisible": true,
                "left": "5dp",
                "skin": "sknIcon15px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmAlertsManagement.lblSortName\")",
                "width": "15dp",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxSortLocationServices.add(lblSortLocationServices);
            flxViewServiceName.add(lblViewServiceName, flxSortLocationServices);
            var flxViewServiceDescription = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "100%",
                "id": "flxViewServiceDescription",
                "isVisible": true,
                "layoutType": kony.flex.FLOW_HORIZONTAL,
                "left": "26.27%",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0dp",
                "width": "100px",
                "zIndex": 1
            }, {}, {});
            flxViewServiceDescription.setDefaultUnit(kony.flex.DP);
            var lblViewServiceDescription = new kony.ui.Label({
                "centerY": "50%",
                "id": "lblViewServiceDescription",
                "isVisible": true,
                "left": "0px",
                "skin": "sknlblLato5d6c7f12px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.permission.DESCRIPTION\")",
                "top": 0,
                "width": "90px",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxViewServiceDescription.add(lblViewServiceDescription);
            var flxViewServiceSeperator = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "bottom": 0,
                "clipBounds": true,
                "height": "1px",
                "id": "flxViewServiceSeperator",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "sknTableHeaderLine",
                "width": "100%",
                "zIndex": 1
            }, {}, {});
            flxViewServiceSeperator.setDefaultUnit(kony.flex.DP);
            flxViewServiceSeperator.add();
            flxServicesHeader.add(flxViewServiceName, flxViewServiceDescription, flxViewServiceSeperator);
            var flxViewServiceSegment = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                "clipBounds": true,
                "id": "flxViewServiceSegment",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0px",
                "isModalContainer": false,
                "right": "0px",
                "skin": "slFbox",
                "top": "61dp",
                "zIndex": 1
            }, {}, {});
            flxViewServiceSegment.setDefaultUnit(kony.flex.DP);
            var segViewServiceSegment = new kony.ui.SegmentedUI2({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "data": [{
                    "lblDescription": "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do",
                    "lblFacilityName": "Admin Role",
                    "lblIconAction": "",
                    "lblSeperator": "."
                }, {
                    "lblDescription": "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do",
                    "lblFacilityName": "Admin Role",
                    "lblIconAction": "",
                    "lblSeperator": "."
                }, {
                    "lblDescription": "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do",
                    "lblFacilityName": "Admin Role",
                    "lblIconAction": "",
                    "lblSeperator": "."
                }, {
                    "lblDescription": "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do",
                    "lblFacilityName": "Admin Role",
                    "lblIconAction": "",
                    "lblSeperator": "."
                }, {
                    "lblDescription": "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do",
                    "lblFacilityName": "Admin Role",
                    "lblIconAction": "",
                    "lblSeperator": "."
                }, {
                    "lblDescription": "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do",
                    "lblFacilityName": "Admin Role",
                    "lblIconAction": "",
                    "lblSeperator": "."
                }, {
                    "lblDescription": "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do",
                    "lblFacilityName": "Admin Role",
                    "lblIconAction": "",
                    "lblSeperator": "."
                }, {
                    "lblDescription": "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do",
                    "lblFacilityName": "Admin Role",
                    "lblIconAction": "",
                    "lblSeperator": "."
                }, {
                    "lblDescription": "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do",
                    "lblFacilityName": "Admin Role",
                    "lblIconAction": "",
                    "lblSeperator": "."
                }],
                "groupCells": false,
                "height": "500px",
                "id": "segViewServiceSegment",
                "isVisible": true,
                "left": "0dp",
                "needPageIndicator": true,
                "pageOffDotImage": "pageoffdot.png",
                "pageOnDotImage": "pageondot.png",
                "retainSelection": false,
                "rowSkin": "seg2Normal",
                "rowTemplate": "flxLocationFacilites",
                "sectionHeaderSkin": "sliPhoneSegmentHeader",
                "selectionBehavior": constants.SEGUI_DEFAULT_BEHAVIOR,
                "separatorRequired": false,
                "showScrollbars": false,
                "top": "0dp",
                "viewType": constants.SEGUI_VIEW_TYPE_TABLEVIEW,
                "widgetDataMap": {
                    "flxActionIcon": "flxActionIcon",
                    "flxLocationFacilites": "flxLocationFacilites",
                    "lblDescription": "lblDescription",
                    "lblFacilityName": "lblFacilityName",
                    "lblIconAction": "lblIconAction",
                    "lblSeperator": "lblSeperator"
                },
                "width": "100%",
                "zIndex": 1
            }, {
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var rtxMsgServices = new kony.ui.RichText({
                "bottom": "200px",
                "centerX": "50%",
                "id": "rtxMsgServices",
                "isVisible": false,
                "skin": "sknRtxLato84939e12Px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmCustomerManagement.rtxMsgActivityHistory\")",
                "top": "200px",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxViewServiceSegment.add(segViewServiceSegment, rtxMsgServices);
            flxViewSegmentAndHeaders.add(flxServicesHeader, flxViewServiceSegment);
            flxViewServices.add(flxViewTabs, flxViewSeperator, flxViewSegmentAndHeaders);
            flxViewLocation.add(flxViewDetails, flxViewServices);
            var flxLocationDetailsAndServices = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "bottom": "35px",
                "clipBounds": true,
                "id": "flxLocationDetailsAndServices",
                "isVisible": false,
                "layoutType": kony.flex.FREE_FORM,
                "left": "35dp",
                "masterType": constants.MASTER_TYPE_USERWIDGET,
                "isModalContainer": false,
                "right": "35dp",
                "skin": "sknflxffffffOp100BorderE1E5Ed",
                "top": "25px",
                "zIndex": 1
            }, {}, {});
            flxLocationDetailsAndServices.setDefaultUnit(kony.flex.DP);
            var flxVerticalTabs = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "bottom": "0px",
                "clipBounds": true,
                "id": "flxVerticalTabs",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0dp",
                "width": "230px",
                "zIndex": 1
            }, {}, {});
            flxVerticalTabs.setDefaultUnit(kony.flex.DP);
            var verticalTabs = new com.adminConsole.common.verticalTabs1({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "height": "100%",
                "id": "verticalTabs",
                "isVisible": true,
                "layoutType": kony.flex.FLOW_VERTICAL,
                "left": "0dp",
                "masterType": constants.MASTER_TYPE_DEFAULT,
                "isModalContainer": false,
                "skin": "sknflxf9f9f9NoBorder",
                "top": "0dp",
                "width": "100%",
                "overrides": {
                    "imgSelected1": {
                        "src": "right_arrow2x.png"
                    },
                    "imgSelected2": {
                        "src": "right_arrow2x.png"
                    },
                    "imgSelected3": {
                        "src": "right_arrow2x.png"
                    },
                    "imgSelected4": {
                        "src": "right_arrow2x.png"
                    },
                    "lblOptional2": {
                        "top": "5dp"
                    },
                    "verticalTabs1": {
                        "bottom": "viz.val_cleared",
                        "centerX": "viz.val_cleared",
                        "centerY": "viz.val_cleared",
                        "height": "100%",
                        "left": "0dp",
                        "maxHeight": "viz.val_cleared",
                        "maxWidth": "viz.val_cleared",
                        "minHeight": "viz.val_cleared",
                        "minWidth": "viz.val_cleared",
                        "right": "viz.val_cleared",
                        "top": "0dp",
                        "width": "100%"
                    }
                }
            }, {
                "overrides": {}
            }, {
                "overrides": {}
            });
            flxVerticalTabs.add(verticalTabs);
            var flxLocationDetailsWrapper = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "bottom": "0px",
                "clipBounds": true,
                "id": "flxLocationDetailsWrapper",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "230dp",
                "isModalContainer": false,
                "right": "0px",
                "skin": "sknbackGroundffffff100",
                "top": "0dp",
                "zIndex": 1
            }, {}, {});
            flxLocationDetailsWrapper.setDefaultUnit(kony.flex.DP);
            var flxLocationDetailsData = new kony.ui.FlexScrollContainer({
                "allowHorizontalBounce": false,
                "allowVerticalBounce": true,
                "bottom": "81px",
                "bounces": true,
                "clipBounds": true,
                "enableScrolling": true,
                "horizontalScrollIndicator": true,
                "id": "flxLocationDetailsData",
                "isVisible": true,
                "layoutType": kony.flex.FLOW_VERTICAL,
                "left": "20px",
                "pagingEnabled": false,
                "right": "0px",
                "scrollDirection": kony.flex.SCROLL_VERTICAL,
                "skin": "slFSbox",
                "top": "0dp",
                "verticalScrollIndicator": true,
                "zIndex": 1
            }, {}, {});
            flxLocationDetailsData.setDefaultUnit(kony.flex.DP);
            var flxDetailsHeading = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "20dp",
                "id": "flxDetailsHeading",
                "isVisible": true,
                "layoutType": kony.flex.FLOW_HORIZONTAL,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "30dp",
                "width": "80dp",
                "zIndex": 1
            }, {}, {
                "hoverSkin": "sknCursor"
            });
            flxDetailsHeading.setDefaultUnit(kony.flex.DP);
            var lblSubTitle = new kony.ui.Label({
                "centerY": "50%",
                "id": "lblSubTitle",
                "isVisible": true,
                "left": "0dp",
                "skin": "sknLblLato84939e13px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmRolesController.DETAILS\")",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var flxImg = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "centerY": "50%",
                "clipBounds": true,
                "height": "20dp",
                "id": "flxImg",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "10dp",
                "isModalContainer": false,
                "skin": "slFbox",
                "width": "20px",
                "zIndex": 1
            }, {}, {});
            flxImg.setDefaultUnit(kony.flex.DP);
            var lblIconToggle = new kony.ui.Label({
                "centerY": "50%",
                "height": "12dp",
                "id": "lblIconToggle",
                "isVisible": true,
                "left": "0dp",
                "skin": "sknIcon13pxGray",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmAlertsManagement.lblIconDescription\")",
                "width": "14dp",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxImg.add(lblIconToggle);
            flxDetailsHeading.add(lblSubTitle, flxImg);
            var lblLineUser = new kony.ui.Label({
                "bottom": "0px",
                "height": "1px",
                "id": "lblLineUser",
                "isVisible": true,
                "left": "0dp",
                "skin": "sknLble1e5edOp100",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmAlertsManagement.lblAlertTypeDescriptionCount\")",
                "top": "5px",
                "width": "100%",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var flxDetailsData = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                "clipBounds": true,
                "id": "flxDetailsData",
                "isVisible": true,
                "layoutType": kony.flex.FLOW_VERTICAL,
                "left": "0dp",
                "masterType": constants.MASTER_TYPE_USERWIDGET,
                "isModalContainer": false,
                "skin": "slFbox0afd6fda7b2514b",
                "top": "10dp",
                "width": "100%",
                "zIndex": 1
            }, {}, {});
            flxDetailsData.setDefaultUnit(kony.flex.DP);
            var flxFullName = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "85dp",
                "id": "flxFullName",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "slFbox0afd6fda7b2514b",
                "top": "10dp",
                "width": "100%",
                "zIndex": 1
            }, {}, {});
            flxFullName.setDefaultUnit(kony.flex.DP);
            var lblFullName = new kony.ui.Label({
                "id": "lblFullName",
                "isVisible": true,
                "left": "0dp",
                "skin": "slLabel0d20174dce8ea42",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmLocations.lblFullName\")",
                "top": "0dp",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var lblFullNameCount = new kony.ui.Label({
                "id": "lblFullNameCount",
                "isVisible": false,
                "right": "71%",
                "skin": "slLabel0d20174dce8ea42",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmLocations.lblFullNameCount\")",
                "top": "0dp",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var txtbxCode = new kony.ui.TextBox2({
                "autoCapitalize": constants.TEXTBOX_AUTO_CAPITALIZE_NONE,
                "focusSkin": "skntxtbxDetailsFocus14px",
                "height": "40px",
                "id": "txtbxCode",
                "isVisible": true,
                "keyBoardStyle": constants.TEXTBOX_KEY_BOARD_STYLE_DEFAULT,
                "left": "0dp",
                "maxTextLength": 10,
                "placeholder": "Code",
                "secureTextEntry": false,
                "skin": "skntxtbxDetails0bbf1235271384a",
                "textInputMode": constants.TEXTBOX_INPUT_MODE_NUMERIC,
                "top": "25dp",
                "width": "29.20%",
                "zIndex": 1
            }, {
                "containerHeightMode": constants.TEXTBOX_FONT_METRICS_DRIVEN_HEIGHT,
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [1, 0, 0, 0],
                "paddingInPixel": false
            }, {
                "autoCorrect": false
            });
            var flxNoCodeError = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "15dp",
                "id": "flxNoCodeError",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "70dp",
                "width": "29.20%",
                "zIndex": 1
            }, {}, {});
            flxNoCodeError.setDefaultUnit(kony.flex.DP);
            var lblNoCodeErrorIcon = new kony.ui.Label({
                "height": "15dp",
                "id": "lblNoCodeErrorIcon",
                "isVisible": true,
                "left": "0dp",
                "skin": "sknErrorIcon",
                "text": "",
                "top": "0dp",
                "width": "15dp",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var lblNoCodeError = new kony.ui.Label({
                "height": "15dp",
                "id": "lblNoCodeError",
                "isVisible": true,
                "left": "15dp",
                "skin": "sknlblError",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmLocationsController.Enter_Code\")",
                "top": "0dp",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxNoCodeError.add(lblNoCodeErrorIcon, lblNoCodeError);
            var txtbxName = new kony.ui.TextBox2({
                "autoCapitalize": constants.TEXTBOX_AUTO_CAPITALIZE_NONE,
                "focusSkin": "skntxtbxDetailsFocus14px",
                "height": "40px",
                "id": "txtbxName",
                "isVisible": true,
                "keyBoardStyle": constants.TEXTBOX_KEY_BOARD_STYLE_DEFAULT,
                "left": "33.42%",
                "maxTextLength": 100,
                "placeholder": "Name",
                "secureTextEntry": false,
                "skin": "skntxtbxDetails0bbf1235271384a",
                "textInputMode": constants.TEXTBOX_INPUT_MODE_ANY,
                "top": "25dp",
                "width": "29.20%",
                "zIndex": 1
            }, {
                "containerHeightMode": constants.TEXTBOX_FONT_METRICS_DRIVEN_HEIGHT,
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [1, 0, 0, 0],
                "paddingInPixel": false
            }, {
                "autoCorrect": false
            });
            var flxNoNameError = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "15dp",
                "id": "flxNoNameError",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "33.40%",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "70dp",
                "width": "29.20%",
                "zIndex": 1
            }, {}, {});
            flxNoNameError.setDefaultUnit(kony.flex.DP);
            var lblNoNameErrorIcon = new kony.ui.Label({
                "height": "15dp",
                "id": "lblNoNameErrorIcon",
                "isVisible": true,
                "left": "0dp",
                "skin": "sknErrorIcon",
                "text": "",
                "top": "0dp",
                "width": "15dp",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var lblNoNameError = new kony.ui.Label({
                "height": "15dp",
                "id": "lblNoNameError",
                "isVisible": true,
                "left": "15dp",
                "skin": "sknlblError",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmLocationsController.Enter_Name\")",
                "top": "0dp",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxNoNameError.add(lblNoNameErrorIcon, lblNoNameError);
            var txtbxDisplayName = new kony.ui.TextBox2({
                "autoCapitalize": constants.TEXTBOX_AUTO_CAPITALIZE_NONE,
                "focusSkin": "skntxtbxDetailsFocus14px",
                "height": "40px",
                "id": "txtbxDisplayName",
                "isVisible": true,
                "keyBoardStyle": constants.TEXTBOX_KEY_BOARD_STYLE_DEFAULT,
                "left": "67.15%",
                "maxTextLength": 100,
                "placeholder": "Display Name",
                "secureTextEntry": false,
                "skin": "skntxtbxDetails0bbf1235271384a",
                "textInputMode": constants.TEXTBOX_INPUT_MODE_ANY,
                "top": "25dp",
                "width": "29.20%",
                "zIndex": 1
            }, {
                "containerHeightMode": constants.TEXTBOX_FONT_METRICS_DRIVEN_HEIGHT,
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [1, 0, 0, 0],
                "paddingInPixel": false
            }, {
                "autoCorrect": false
            });
            var flxNoDisplayNameError = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "15dp",
                "id": "flxNoDisplayNameError",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "67.10%",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "70dp",
                "width": "29.20%",
                "zIndex": 1
            }, {}, {});
            flxNoDisplayNameError.setDefaultUnit(kony.flex.DP);
            var lblNoDisplayNameErrorIcon = new kony.ui.Label({
                "height": "15dp",
                "id": "lblNoDisplayNameErrorIcon",
                "isVisible": true,
                "left": "0dp",
                "skin": "sknErrorIcon",
                "text": "",
                "top": "0dp",
                "width": "15dp",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var lblNoDisplayNameError = new kony.ui.Label({
                "height": "15dp",
                "id": "lblNoDisplayNameError",
                "isVisible": true,
                "left": "15dp",
                "skin": "sknlblError",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmLocationsController.Enter_Display_Name\")",
                "top": "0dp",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxNoDisplayNameError.add(lblNoDisplayNameErrorIcon, lblNoDisplayNameError);
            var imgMandatoryCode = new kony.ui.Image2({
                "height": "12px",
                "id": "imgMandatoryCode",
                "isVisible": false,
                "left": "35px",
                "skin": "slImage",
                "src": "mandatory_2x.png",
                "top": "3px",
                "width": "6px",
                "zIndex": 1
            }, {
                "imageScaleMode": constants.IMAGE_SCALE_MODE_MAINTAIN_ASPECT_RATIO,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var lblNameCountValue = new kony.ui.Label({
                "id": "lblNameCountValue",
                "isVisible": false,
                "right": "37.50%",
                "skin": "slLabel0d20174dce8ea42",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmLocations.lblCountDisplayName\")",
                "top": "0dp",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var flxNameIndicator = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "20dp",
                "id": "flxNameIndicator",
                "isVisible": true,
                "layoutType": kony.flex.FLOW_HORIZONTAL,
                "left": "33.42%",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0dp",
                "width": "30%",
                "zIndex": 1
            }, {}, {});
            flxNameIndicator.setDefaultUnit(kony.flex.DP);
            var lblName = new kony.ui.Label({
                "id": "lblName",
                "isVisible": true,
                "left": "0px",
                "skin": "slLabel0d20174dce8ea42",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmAlertsManagement.lblEditAlertMainName\")",
                "top": "0dp",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var imgMandatoryName = new kony.ui.Image2({
                "height": "12px",
                "id": "imgMandatoryName",
                "isVisible": false,
                "left": "8px",
                "skin": "slImage",
                "src": "mandatory_2x.png",
                "top": "3px",
                "width": "6px",
                "zIndex": 1
            }, {
                "imageScaleMode": constants.IMAGE_SCALE_MODE_MAINTAIN_ASPECT_RATIO,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxNameIndicator.add(lblName, imgMandatoryName);
            var lblCountDisplayName = new kony.ui.Label({
                "id": "lblCountDisplayName",
                "isVisible": false,
                "right": "3.50%",
                "skin": "slLabel0d20174dce8ea42",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmLocations.lblCountDisplayName\")",
                "top": "0dp",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var flxDisplayName = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "20px",
                "id": "flxDisplayName",
                "isVisible": true,
                "layoutType": kony.flex.FLOW_HORIZONTAL,
                "left": "67.15%",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0dp",
                "width": "30%",
                "zIndex": 1
            }, {}, {});
            flxDisplayName.setDefaultUnit(kony.flex.DP);
            var lblDisplayName = new kony.ui.Label({
                "id": "lblDisplayName",
                "isVisible": true,
                "left": "0px",
                "skin": "slLabel0d20174dce8ea42",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmLocations.lblDisplayName\")",
                "top": "0dp",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var imgMandatoryDisplayName = new kony.ui.Image2({
                "height": "12px",
                "id": "imgMandatoryDisplayName",
                "isVisible": false,
                "left": "8px",
                "skin": "slImage",
                "src": "mandatory_2x.png",
                "top": "3px",
                "width": "6px",
                "zIndex": 1
            }, {
                "imageScaleMode": constants.IMAGE_SCALE_MODE_MAINTAIN_ASPECT_RATIO,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxDisplayName.add(lblDisplayName, imgMandatoryDisplayName);
            flxFullName.add(lblFullName, lblFullNameCount, txtbxCode, flxNoCodeError, txtbxName, flxNoNameError, txtbxDisplayName, flxNoDisplayNameError, imgMandatoryCode, lblNameCountValue, flxNameIndicator, lblCountDisplayName, flxDisplayName);
            var flxEmailID = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "85dp",
                "id": "flxEmailID",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "slFbox0afd6fda7b2514b",
                "top": "10dp",
                "width": "100%",
                "zIndex": 1
            }, {}, {});
            flxEmailID.setDefaultUnit(kony.flex.DP);
            var lblEmailIDUser = new kony.ui.Label({
                "id": "lblEmailIDUser",
                "isVisible": true,
                "left": "0dp",
                "skin": "slLabel0d20174dce8ea42",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmCustomerCareController.Email_ID\")",
                "top": "3dp",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var txtbxEmailIDUser = new kony.ui.TextBox2({
                "autoCapitalize": constants.TEXTBOX_AUTO_CAPITALIZE_NONE,
                "focusSkin": "skntxtbxDetailsFocus14px",
                "height": "40px",
                "id": "txtbxEmailIDUser",
                "isVisible": true,
                "keyBoardStyle": constants.TEXTBOX_KEY_BOARD_STYLE_EMAIL,
                "left": "0dp",
                "maxTextLength": 70,
                "i18n_placeholder": "kony.i18n.getLocalizedString(\"i18n.Applications.EmailAddress\")",
                "secureTextEntry": false,
                "skin": "skntxtbxDetails0bbf1235271384a",
                "textInputMode": constants.TEXTBOX_INPUT_MODE_ANY,
                "top": "25dp",
                "width": "62.33%",
                "zIndex": 1
            }, {
                "containerHeightMode": constants.TEXTBOX_FONT_METRICS_DRIVEN_HEIGHT,
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [1, 0, 0, 0],
                "paddingInPixel": false
            }, {
                "autoCorrect": false
            });
            var flxNoEmailError = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "15dp",
                "id": "flxNoEmailError",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "70dp",
                "width": "62.30%",
                "zIndex": 1
            }, {}, {});
            flxNoEmailError.setDefaultUnit(kony.flex.DP);
            var lblNoEmailErrorIcon = new kony.ui.Label({
                "height": "15dp",
                "id": "lblNoEmailErrorIcon",
                "isVisible": true,
                "left": "0dp",
                "skin": "sknErrorIcon",
                "text": "",
                "top": "0dp",
                "width": "15dp",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var lblNoEmailError = new kony.ui.Label({
                "height": "15dp",
                "id": "lblNoEmailError",
                "isVisible": true,
                "left": "15dp",
                "skin": "sknlblError",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmUsersController.Enter_a_valid_Email-id\")",
                "top": "0dp",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxNoEmailError.add(lblNoEmailErrorIcon, lblNoEmailError);
            var flxType = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "40dp",
                "id": "flxType",
                "isVisible": true,
                "layoutType": kony.flex.FLOW_HORIZONTAL,
                "left": "67.15%",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "25dp",
                "width": "200dp",
                "zIndex": 1
            }, {}, {});
            flxType.setDefaultUnit(kony.flex.DP);
            var flxRadio1 = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "centerY": "50%",
                "clipBounds": true,
                "height": "20dp",
                "id": "flxRadio1",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "-2dp",
                "isModalContainer": false,
                "skin": "slFbox",
                "width": "20px",
                "zIndex": 1
            }, {}, {
                "hoverSkin": "sknCursor"
            });
            flxRadio1.setDefaultUnit(kony.flex.DP);
            var imgRadio1 = new kony.ui.Image2({
                "centerX": "50%",
                "centerY": "50%",
                "height": "15dp",
                "id": "imgRadio1",
                "isVisible": true,
                "skin": "slImage",
                "src": "radio_selected.png",
                "width": "15px",
                "zIndex": 1
            }, {
                "imageScaleMode": constants.IMAGE_SCALE_MODE_MAINTAIN_ASPECT_RATIO,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxRadio1.add(imgRadio1);
            var lblBank = new kony.ui.Label({
                "centerY": "50%",
                "id": "lblBank",
                "isVisible": true,
                "left": "5dp",
                "skin": "slLabel0d20174dce8ea42",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmLocations.lblBank\")",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var flxRadio2 = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "centerY": "50%",
                "clipBounds": true,
                "height": "20dp",
                "id": "flxRadio2",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "20px",
                "isModalContainer": false,
                "skin": "slFbox",
                "width": "20px",
                "zIndex": 1
            }, {}, {
                "hoverSkin": "sknCursor"
            });
            flxRadio2.setDefaultUnit(kony.flex.DP);
            var imgRadio2 = new kony.ui.Image2({
                "centerX": "50%",
                "centerY": "50%",
                "height": "15dp",
                "id": "imgRadio2",
                "isVisible": true,
                "skin": "slImage",
                "src": "radio_notselected.png",
                "width": "15dp",
                "zIndex": 1
            }, {
                "imageScaleMode": constants.IMAGE_SCALE_MODE_MAINTAIN_ASPECT_RATIO,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxRadio2.add(imgRadio2);
            var lblAtm = new kony.ui.Label({
                "centerY": "50%",
                "id": "lblAtm",
                "isVisible": true,
                "left": "5dp",
                "skin": "slLabel0d20174dce8ea42",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmLocationsController.ATM\")",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxType.add(flxRadio1, lblBank, flxRadio2, lblAtm);
            var flxTypeIndicator = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "20px",
                "id": "flxTypeIndicator",
                "isVisible": true,
                "layoutType": kony.flex.FLOW_HORIZONTAL,
                "left": "67.15%",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0dp",
                "width": "30%",
                "zIndex": 1
            }, {}, {});
            flxTypeIndicator.setDefaultUnit(kony.flex.DP);
            var lblType = new kony.ui.Label({
                "id": "lblType",
                "isVisible": true,
                "left": "0%",
                "skin": "slLabel0d20174dce8ea42",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmLocations.lblType\")",
                "top": "0dp",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var imgMandatoryType = new kony.ui.Image2({
                "height": "12px",
                "id": "imgMandatoryType",
                "isVisible": false,
                "left": "8px",
                "skin": "slImage",
                "src": "mandatory_2x.png",
                "top": "3px",
                "width": "6px",
                "zIndex": 1
            }, {
                "imageScaleMode": constants.IMAGE_SCALE_MODE_MAINTAIN_ASPECT_RATIO,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxTypeIndicator.add(lblType, imgMandatoryType);
            flxEmailID.add(lblEmailIDUser, txtbxEmailIDUser, flxNoEmailError, flxType, flxTypeIndicator);
            var flxRadioButtons = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "bottom": "5px",
                "clipBounds": true,
                "height": "40dp",
                "id": "flxRadioButtons",
                "isVisible": true,
                "layoutType": kony.flex.FLOW_HORIZONTAL,
                "left": "0px",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "5px",
                "width": "100%",
                "zIndex": 1
            }, {}, {});
            flxRadioButtons.setDefaultUnit(kony.flex.DP);
            var lblActiveBranch = new kony.ui.Label({
                "centerY": "50%",
                "id": "lblActiveBranch",
                "isVisible": true,
                "left": "0dp",
                "skin": "slLabel0d20174dce8ea42",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmLocations.lblActiveBranch\")",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var flxToggle = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "centerY": "50%",
                "clipBounds": true,
                "height": "20dp",
                "id": "flxToggle",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "10dp",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0dp",
                "width": "36px",
                "zIndex": 1
            }, {}, {});
            flxToggle.setDefaultUnit(kony.flex.DP);
            var switchActiveBranch = new kony.ui.Switch({
                "height": "20dp",
                "id": "switchActiveBranch",
                "isVisible": true,
                "left": "0dp",
                "leftSideText": "ON",
                "rightSideText": "OFF",
                "selectedIndex": 0,
                "skin": "sknSwitchServiceManagement",
                "top": "0dp",
                "width": "36dp",
                "zIndex": 1
            }, {
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxToggle.add(switchActiveBranch);
            var lblMainBranch = new kony.ui.Label({
                "centerY": "50%",
                "id": "lblMainBranch",
                "isVisible": true,
                "left": "35dp",
                "skin": "slLabel0d20174dce8ea42",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmLocations.lblMainBranch\")",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var flxToggle1 = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "centerY": "50%",
                "clipBounds": true,
                "height": "20dp",
                "id": "flxToggle1",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "10dp",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "3dp",
                "width": "36px",
                "zIndex": 1
            }, {}, {});
            flxToggle1.setDefaultUnit(kony.flex.DP);
            var switchMainBranch = new kony.ui.Switch({
                "height": "20dp",
                "id": "switchMainBranch",
                "isVisible": true,
                "left": "0dp",
                "leftSideText": "ON",
                "rightSideText": "OFF",
                "selectedIndex": 0,
                "skin": "sknSwitchServiceManagement",
                "top": "0dp",
                "width": "36dp",
                "zIndex": 1
            }, {
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxToggle1.add(switchMainBranch);
            flxRadioButtons.add(lblActiveBranch, flxToggle, lblMainBranch, flxToggle1);
            var flxTypeBasedOptions = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                "clipBounds": true,
                "id": "flxTypeBasedOptions",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "10dp",
                "width": "100%",
                "zIndex": 1
            }, {}, {});
            flxTypeBasedOptions.setDefaultUnit(kony.flex.DP);
            var flxBankBasedOptions = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "125dp",
                "id": "flxBankBasedOptions",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0dp",
                "width": "100%",
                "zIndex": 1
            }, {}, {});
            flxBankBasedOptions.setDefaultUnit(kony.flex.DP);
            var flxBankType = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "60dp",
                "id": "flxBankType",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0dp",
                "width": "100%",
                "zIndex": 1
            }, {}, {});
            flxBankType.setDefaultUnit(kony.flex.DP);
            var lblBankType = new kony.ui.Label({
                "height": "16dp",
                "id": "lblBankType",
                "isVisible": true,
                "left": "0dp",
                "skin": "slLabel0d20174dce8ea42",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmLocations.lblBankType\")",
                "top": "0dp",
                "width": "12%",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var flxBankTypeRadio = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "30dp",
                "id": "flxBankTypeRadio",
                "isVisible": true,
                "layoutType": kony.flex.FLOW_HORIZONTAL,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "20dp",
                "width": "200dp",
                "zIndex": 1
            }, {}, {});
            flxBankTypeRadio.setDefaultUnit(kony.flex.DP);
            var flxRadioPhysical = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "centerY": "50%",
                "clipBounds": true,
                "height": "20dp",
                "id": "flxRadioPhysical",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "-2dp",
                "isModalContainer": false,
                "skin": "slFbox",
                "width": "20px",
                "zIndex": 1
            }, {}, {
                "hoverSkin": "sknCursor"
            });
            flxRadioPhysical.setDefaultUnit(kony.flex.DP);
            var imgRadioPhysical = new kony.ui.Image2({
                "centerX": "50%",
                "centerY": "50%",
                "height": "15dp",
                "id": "imgRadioPhysical",
                "isVisible": true,
                "skin": "slImage",
                "src": "radio_selected.png",
                "width": "15px",
                "zIndex": 1
            }, {
                "imageScaleMode": constants.IMAGE_SCALE_MODE_MAINTAIN_ASPECT_RATIO,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxRadioPhysical.add(imgRadioPhysical);
            var lblPhysical = new kony.ui.Label({
                "centerY": "50%",
                "id": "lblPhysical",
                "isVisible": true,
                "left": "5dp",
                "skin": "slLabel0d20174dce8ea42",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmLocations.lblPhysical\")",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var flxRadioMobile = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "centerY": "50%",
                "clipBounds": true,
                "height": "20dp",
                "id": "flxRadioMobile",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "20px",
                "isModalContainer": false,
                "skin": "slFbox",
                "width": "20px",
                "zIndex": 1
            }, {}, {
                "hoverSkin": "sknCursor"
            });
            flxRadioMobile.setDefaultUnit(kony.flex.DP);
            var imgRadioMobile = new kony.ui.Image2({
                "centerX": "50%",
                "centerY": "50%",
                "height": "15dp",
                "id": "imgRadioMobile",
                "isVisible": true,
                "skin": "slImage",
                "src": "radio_notselected.png",
                "width": "15dp",
                "zIndex": 1
            }, {
                "imageScaleMode": constants.IMAGE_SCALE_MODE_MAINTAIN_ASPECT_RATIO,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxRadioMobile.add(imgRadioMobile);
            var lblMobile = new kony.ui.Label({
                "centerY": "50%",
                "id": "lblMobile",
                "isVisible": true,
                "left": "5dp",
                "skin": "slLabel0d20174dce8ea42",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmLocations.lblMobile\")",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxBankTypeRadio.add(flxRadioPhysical, lblPhysical, flxRadioMobile, lblMobile);
            flxBankType.add(lblBankType, flxBankTypeRadio);
            var flxCustomerSegment = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "60dp",
                "id": "flxCustomerSegment",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "65dp",
                "width": "100%",
                "zIndex": 1
            }, {}, {});
            flxCustomerSegment.setDefaultUnit(kony.flex.DP);
            var lblCustomerSegment = new kony.ui.Label({
                "height": "16dp",
                "id": "lblCustomerSegment",
                "isVisible": true,
                "left": "0dp",
                "skin": "slLabel0d20174dce8ea42",
                "text": "Customer Segment",
                "top": "0dp",
                "width": "18%",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var flxCustomerSegmentEntries = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "30dp",
                "id": "flxCustomerSegmentEntries",
                "isVisible": true,
                "layoutType": kony.flex.FLOW_HORIZONTAL,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "20dp",
                "width": "95%",
                "zIndex": 1
            }, {}, {});
            flxCustomerSegmentEntries.setDefaultUnit(kony.flex.DP);
            var flxCheckBox = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "100%",
                "id": "flxCheckBox",
                "isVisible": true,
                "layoutType": kony.flex.FLOW_HORIZONTAL,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0dp",
                "width": "100%",
                "zIndex": 1
            }, {}, {});
            flxCheckBox.setDefaultUnit(kony.flex.DP);
            var flxcbSmallBusinessUser = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "centerY": "50%",
                "clipBounds": true,
                "height": "15px",
                "id": "flxcbSmallBusinessUser",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "sknCursor",
                "width": "15px",
                "zIndex": 1
            }, {}, {});
            flxcbSmallBusinessUser.setDefaultUnit(kony.flex.DP);
            var imgcbSmallBusinessUser = new kony.ui.Image2({
                "centerX": "50%",
                "centerY": "50%",
                "height": "100%",
                "id": "imgcbSmallBusinessUser",
                "isVisible": true,
                "left": "0px",
                "skin": "slImage",
                "src": "checkboxnormal.png",
                "top": "0px",
                "width": "98%",
                "zIndex": 1
            }, {
                "imageScaleMode": constants.IMAGE_SCALE_MODE_MAINTAIN_ASPECT_RATIO,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxcbSmallBusinessUser.add(imgcbSmallBusinessUser);
            var lblSmallBusinessUser = new kony.ui.Label({
                "centerY": "50%",
                "id": "lblSmallBusinessUser",
                "isVisible": true,
                "left": "5dp",
                "skin": "slLabel0d20174dce8ea42",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmLocations.lblSmallBusinessUser\")",
                "top": "18dp",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var flxcbMicroBusinessUser = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "centerY": "50%",
                "clipBounds": true,
                "height": "15px",
                "id": "flxcbMicroBusinessUser",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "25px",
                "isModalContainer": false,
                "skin": "sknCursor",
                "width": "15px",
                "zIndex": 1
            }, {}, {});
            flxcbMicroBusinessUser.setDefaultUnit(kony.flex.DP);
            var imgcbMicroBusinessUser = new kony.ui.Image2({
                "centerX": "50%",
                "centerY": "50%",
                "height": "100%",
                "id": "imgcbMicroBusinessUser",
                "isVisible": true,
                "left": "0px",
                "skin": "slImage",
                "src": "checkboxnormal.png",
                "top": "0px",
                "width": "98%",
                "zIndex": 1
            }, {
                "imageScaleMode": constants.IMAGE_SCALE_MODE_MAINTAIN_ASPECT_RATIO,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxcbMicroBusinessUser.add(imgcbMicroBusinessUser);
            var lblMicroBusinessUser = new kony.ui.Label({
                "centerY": "50%",
                "id": "lblMicroBusinessUser",
                "isVisible": true,
                "left": "5dp",
                "skin": "slLabel0d20174dce8ea42",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmLocations.lblMicroBusinessUser\")",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var flxcbRetailCustomer = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "centerY": "50%",
                "clipBounds": true,
                "height": "15px",
                "id": "flxcbRetailCustomer",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "26px",
                "isModalContainer": false,
                "skin": "sknCursor",
                "width": "15px",
                "zIndex": 1
            }, {}, {});
            flxcbRetailCustomer.setDefaultUnit(kony.flex.DP);
            var imgcbRetailCustomer = new kony.ui.Image2({
                "centerX": "50%",
                "centerY": "50%",
                "height": "100%",
                "id": "imgcbRetailCustomer",
                "isVisible": true,
                "left": "0px",
                "skin": "slImage",
                "src": "checkboxnormal.png",
                "top": "0px",
                "width": "98%",
                "zIndex": 1
            }, {
                "imageScaleMode": constants.IMAGE_SCALE_MODE_MAINTAIN_ASPECT_RATIO,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxcbRetailCustomer.add(imgcbRetailCustomer);
            var lblRetailCustomer = new kony.ui.Label({
                "centerY": "50%",
                "id": "lblRetailCustomer",
                "isVisible": true,
                "left": "5dp",
                "skin": "slLabel0d20174dce8ea42",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmLocations.lblRetailCustomer\")",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxCheckBox.add(flxcbSmallBusinessUser, lblSmallBusinessUser, flxcbMicroBusinessUser, lblMicroBusinessUser, flxcbRetailCustomer, lblRetailCustomer);
            flxCustomerSegmentEntries.add(flxCheckBox);
            flxCustomerSegment.add(lblCustomerSegment, flxCustomerSegmentEntries);
            flxBankBasedOptions.add(flxBankType, flxCustomerSegment);
            var flxATMBasedOptions = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "60dp",
                "id": "flxATMBasedOptions",
                "isVisible": false,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0dp",
                "width": "100%",
                "zIndex": 1
            }, {}, {});
            flxATMBasedOptions.setDefaultUnit(kony.flex.DP);
            var flxSupportedCurrency = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "60dp",
                "id": "flxSupportedCurrency",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0dp",
                "width": "100%",
                "zIndex": 1
            }, {}, {});
            flxSupportedCurrency.setDefaultUnit(kony.flex.DP);
            var lblSupportedCurrency = new kony.ui.Label({
                "height": "16dp",
                "id": "lblSupportedCurrency",
                "isVisible": true,
                "left": "0dp",
                "skin": "slLabel0d20174dce8ea42",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmLocations.lblSupportedCurrency\")",
                "top": "0dp",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var flxSupportedCurrencyEntries = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "30dp",
                "id": "flxSupportedCurrencyEntries",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "20dp",
                "width": "95%",
                "zIndex": 1
            }, {}, {});
            flxSupportedCurrencyEntries.setDefaultUnit(kony.flex.DP);
            flxSupportedCurrencyEntries.add();
            flxSupportedCurrency.add(lblSupportedCurrency, flxSupportedCurrencyEntries);
            flxATMBasedOptions.add(flxSupportedCurrency);
            flxTypeBasedOptions.add(flxBankBasedOptions, flxATMBasedOptions);
            var flxDescription = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "125dp",
                "id": "flxDescription",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "10dp",
                "width": "100%",
                "zIndex": 1
            }, {}, {});
            flxDescription.setDefaultUnit(kony.flex.DP);
            var lblDescription = new kony.ui.Label({
                "id": "lblDescription",
                "isVisible": true,
                "left": "1dp",
                "skin": "slLabel0d20174dce8ea42",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmAlertsManagement.lblEditAlertDescription\")",
                "top": "3dp",
                "width": "12%",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var lblDescriptionCount = new kony.ui.Label({
                "id": "lblDescriptionCount",
                "isVisible": false,
                "right": "4%",
                "skin": "slLabel0d20174dce8ea42",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmGroups.lblGroupDescriptionCount\")",
                "top": "0dp",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var TxtAreaDescription = new kony.ui.TextArea2({
                "autoCapitalize": constants.TEXTAREA_AUTO_CAPITALIZE_NONE,
                "focusSkin": "skntxtAreaLato9ca9ba12Px",
                "height": "80dp",
                "id": "TxtAreaDescription",
                "isVisible": true,
                "keyBoardStyle": constants.TEXTAREA_KEY_BOARD_STYLE_DEFAULT,
                "left": "0dp",
                "maxTextLength": 200,
                "numberOfVisibleLines": 3,
                "placeholder": "Enter Description",
                "skin": "sknTextAreaDescription",
                "textInputMode": constants.TEXTAREA_INPUT_MODE_ANY,
                "top": "25dp",
                "width": "100%",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_TOP_LEFT,
                "padding": [1, 1, 0, 0],
                "paddingInPixel": false
            }, {
                "autoCorrect": false
            });
            var flxNoDescriptionError = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "15dp",
                "id": "flxNoDescriptionError",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "110dp",
                "width": "96.30%",
                "zIndex": 1
            }, {}, {});
            flxNoDescriptionError.setDefaultUnit(kony.flex.DP);
            var lblNoDescriptionErrorIcon = new kony.ui.Label({
                "height": "15dp",
                "id": "lblNoDescriptionErrorIcon",
                "isVisible": true,
                "left": "0dp",
                "skin": "sknErrorIcon",
                "text": "",
                "top": "0dp",
                "width": "15dp",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var lblNoDescriptionError = new kony.ui.Label({
                "height": "15dp",
                "id": "lblNoDescriptionError",
                "isVisible": true,
                "left": "15dp",
                "skin": "sknlblError",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmLocations.lblNoDescriptionError\")",
                "top": "0dp",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxNoDescriptionError.add(lblNoDescriptionErrorIcon, lblNoDescriptionError);
            flxDescription.add(lblDescription, lblDescriptionCount, TxtAreaDescription, flxNoDescriptionError);
            flxDetailsData.add(flxFullName, flxEmailID, flxRadioButtons, flxTypeBasedOptions, flxDescription);
            var flxAddressHeading = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "20dp",
                "id": "flxAddressHeading",
                "isVisible": true,
                "layoutType": kony.flex.FLOW_HORIZONTAL,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "20dp",
                "width": "85dp",
                "zIndex": 1
            }, {}, {
                "hoverSkin": "sknCursor"
            });
            flxAddressHeading.setDefaultUnit(kony.flex.DP);
            var lblAddressUser = new kony.ui.Label({
                "centerY": "50%",
                "id": "lblAddressUser",
                "isVisible": true,
                "left": "0dp",
                "skin": "sknLblLato84939e13px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.View.ADDRESS\")",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var flxImg1 = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "centerY": "50%",
                "clipBounds": true,
                "height": "20dp",
                "id": "flxImg1",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "10dp",
                "isModalContainer": false,
                "skin": "slFbox",
                "width": "20px",
                "zIndex": 1
            }, {}, {});
            flxImg1.setDefaultUnit(kony.flex.DP);
            var lblIconToggle1 = new kony.ui.Label({
                "centerY": "50%",
                "height": "12dp",
                "id": "lblIconToggle1",
                "isVisible": true,
                "left": "0dp",
                "skin": "sknIcon13pxGray",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmAlertsManagement.lblIconDescription\")",
                "width": "14dp",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxImg1.add(lblIconToggle1);
            flxAddressHeading.add(lblAddressUser, flxImg1);
            var lblLine2 = new kony.ui.Label({
                "bottom": "0px",
                "height": "1dp",
                "id": "lblLine2",
                "isVisible": true,
                "left": "0dp",
                "skin": "sknLble1e5edOp100",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmAlertsManagement.lblAlertTypeDescriptionCount\")",
                "top": "5px",
                "width": "100%",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var flxAddressData = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                "clipBounds": true,
                "id": "flxAddressData",
                "isVisible": true,
                "layoutType": kony.flex.FLOW_VERTICAL,
                "left": "0dp",
                "masterType": constants.MASTER_TYPE_USERWIDGET,
                "isModalContainer": false,
                "skin": "slFbox0afd6fda7b2514b",
                "top": "20dp",
                "width": "100%",
                "zIndex": 1
            }, {}, {});
            flxAddressData.setDefaultUnit(kony.flex.DP);
            var flxAddrLine1User = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "85dp",
                "id": "flxAddrLine1User",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "slFbox0afd6fda7b2514b",
                "top": "0dp",
                "width": "100%",
                "zIndex": 1
            }, {}, {});
            flxAddrLine1User.setDefaultUnit(kony.flex.DP);
            var lblStreetAddress = new kony.ui.Label({
                "id": "lblStreetAddress",
                "isVisible": true,
                "left": "0dp",
                "skin": "slLabel0d20174dce8ea42",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmLocations.lblStreetAddress\")",
                "top": "3dp",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var txtbxAddressLine1User = new kony.ui.TextBox2({
                "autoCapitalize": constants.TEXTBOX_AUTO_CAPITALIZE_NONE,
                "focusSkin": "skntxtbxDetailsFocus14px",
                "height": "40px",
                "id": "txtbxAddressLine1User",
                "isVisible": true,
                "keyBoardStyle": constants.TEXTBOX_KEY_BOARD_STYLE_DEFAULT,
                "left": "0dp",
                "maxTextLength": 80,
                "placeholder": "Address",
                "secureTextEntry": false,
                "skin": "skntxtbxDetails0bbf1235271384a",
                "textInputMode": constants.TEXTBOX_INPUT_MODE_ANY,
                "top": "25dp",
                "width": "100%",
                "zIndex": 1
            }, {
                "containerHeightMode": constants.TEXTBOX_FONT_METRICS_DRIVEN_HEIGHT,
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [1, 0, 0, 0],
                "paddingInPixel": false
            }, {
                "autoCorrect": false
            });
            var imgMandatoryStreetAddress = new kony.ui.Image2({
                "height": "12px",
                "id": "imgMandatoryStreetAddress",
                "isVisible": false,
                "left": "86px",
                "skin": "slImage",
                "src": "mandatory_2x.png",
                "top": "6px",
                "width": "6px",
                "zIndex": 1
            }, {
                "imageScaleMode": constants.IMAGE_SCALE_MODE_MAINTAIN_ASPECT_RATIO,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var flxNoAddressError = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "15dp",
                "id": "flxNoAddressError",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "70dp",
                "width": "96.30%",
                "zIndex": 1
            }, {}, {});
            flxNoAddressError.setDefaultUnit(kony.flex.DP);
            var lblNoAddressErrorIcon = new kony.ui.Label({
                "height": "15dp",
                "id": "lblNoAddressErrorIcon",
                "isVisible": true,
                "left": "0dp",
                "skin": "sknErrorIcon",
                "text": "",
                "top": "0dp",
                "width": "15dp",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var lblNoAddressError = new kony.ui.Label({
                "height": "15dp",
                "id": "lblNoAddressError",
                "isVisible": true,
                "left": "15dp",
                "skin": "sknlblError",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmLocationsController.Enter_Address\")",
                "top": "0dp",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxNoAddressError.add(lblNoAddressErrorIcon, lblNoAddressError);
            var lblLocationAddressCount = new kony.ui.Label({
                "id": "lblLocationAddressCount",
                "isVisible": false,
                "right": "4%",
                "skin": "slLabel0d20174dce8ea42",
                "text": "0/80",
                "top": "0dp",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxAddrLine1User.add(lblStreetAddress, txtbxAddressLine1User, imgMandatoryStreetAddress, flxNoAddressError, lblLocationAddressCount);
            var flxCityUser = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "85dp",
                "id": "flxCityUser",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "slFbox0afd6fda7b2514b",
                "top": "5px",
                "width": "100%",
                "zIndex": 1
            }, {}, {});
            flxCityUser.setDefaultUnit(kony.flex.DP);
            var flxCityUser1 = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "100%",
                "id": "flxCityUser1",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "67.15%",
                "isModalContainer": false,
                "skin": "slFbox0afd6fda7b2514b",
                "top": "0dp",
                "width": "29.20%",
                "zIndex": 1
            }, {}, {});
            flxCityUser1.setDefaultUnit(kony.flex.DP);
            var lblCityUser = new kony.ui.Label({
                "id": "lblCityUser",
                "isVisible": true,
                "left": "0dp",
                "skin": "slLabel0d20174dce8ea42",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmCreateNewUser.lblCityUser\")",
                "top": "0dp",
                "width": "90%",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var imgMandatoryCity = new kony.ui.Image2({
                "height": "12px",
                "id": "imgMandatoryCity",
                "isVisible": false,
                "left": "30px",
                "skin": "slImage",
                "src": "mandatory_2x.png",
                "top": "3px",
                "width": "6px",
                "zIndex": 1
            }, {
                "imageScaleMode": constants.IMAGE_SCALE_MODE_MAINTAIN_ASPECT_RATIO,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var lbxLocationCity = new kony.ui.ListBox({
                "focusSkin": "sknlbxBgffffffBorder11abebRadius3Px14px",
                "height": "40px",
                "id": "lbxLocationCity",
                "isVisible": false,
                "left": "0dp",
                "masterData": [
                    ["lb1", "Select"],
                    ["lb2", "India"],
                    ["lb3", "USA"]
                ],
                "selectedKey": "lb1",
                "skin": "sknlstbxNormal0f9abd8e88aa64a",
                "top": "25dp",
                "width": "100%",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [2, 0, 0, 0],
                "paddingInPixel": false
            }, {
                "hoverSkin": "lstboxcursor",
                "multiSelect": false
            });
            var txtbxLocationCity = new kony.ui.TextBox2({
                "autoCapitalize": constants.TEXTBOX_AUTO_CAPITALIZE_NONE,
                "focusSkin": "skntxtbxDetailsFocus14px",
                "height": "40px",
                "id": "txtbxLocationCity",
                "isVisible": true,
                "keyBoardStyle": constants.TEXTBOX_KEY_BOARD_STYLE_DEFAULT,
                "left": "0dp",
                "placeholder": "Enter City",
                "secureTextEntry": false,
                "skin": "skntxtbxDetails0bbf1235271384a",
                "textInputMode": constants.TEXTBOX_INPUT_MODE_ANY,
                "top": "25dp",
                "width": "100%",
                "zIndex": 1
            }, {
                "containerHeightMode": constants.TEXTBOX_FONT_METRICS_DRIVEN_HEIGHT,
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [3, 0, 0, 0],
                "paddingInPixel": false
            }, {
                "autoCorrect": false
            });
            var flxNoCityError = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "15dp",
                "id": "flxNoCityError",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "70dp",
                "width": "100%",
                "zIndex": 1
            }, {}, {});
            flxNoCityError.setDefaultUnit(kony.flex.DP);
            var lblNoCityErrorIcon = new kony.ui.Label({
                "height": "15dp",
                "id": "lblNoCityErrorIcon",
                "isVisible": true,
                "left": "0dp",
                "skin": "sknErrorIcon",
                "text": "",
                "top": "0dp",
                "width": "15dp",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var lblNoCityError = new kony.ui.Label({
                "height": "15dp",
                "id": "lblNoCityError",
                "isVisible": true,
                "left": "15dp",
                "skin": "sknlblError",
                "text": "Enter City Name",
                "top": "0dp",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxNoCityError.add(lblNoCityErrorIcon, lblNoCityError);
            flxCityUser1.add(lblCityUser, imgMandatoryCity, lbxLocationCity, txtbxLocationCity, flxNoCityError);
            var flxStateUser = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "100%",
                "id": "flxStateUser",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "33.42%",
                "isModalContainer": false,
                "skin": "slFbox0afd6fda7b2514b",
                "top": "0dp",
                "width": "29.20%",
                "zIndex": 1
            }, {}, {});
            flxStateUser.setDefaultUnit(kony.flex.DP);
            var lblStateUser = new kony.ui.Label({
                "id": "lblStateUser",
                "isVisible": true,
                "left": "0dp",
                "skin": "slLabel0d20174dce8ea42",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmCreateNewUser.lblStateUser\")",
                "top": "0dp",
                "width": "90%",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var lstbxStateUser = new kony.ui.ListBox({
                "focusSkin": "sknlbxBgffffffBorder11abebRadius3Px14px",
                "height": "40px",
                "id": "lstbxStateUser",
                "isVisible": true,
                "left": "0dp",
                "masterData": [
                    ["lb1", "Select"],
                    ["lb2", "Telengana"],
                    ["lb3", "Maharastra"]
                ],
                "right": 0,
                "selectedKey": "lb1",
                "skin": "sknlstbxNormal0f9abd8e88aa64a",
                "top": "25dp",
                "width": "100%",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [2, 0, 0, 0],
                "paddingInPixel": false
            }, {
                "hoverSkin": "lstboxcursor",
                "multiSelect": false
            });
            var imgMandatoryState = new kony.ui.Image2({
                "height": "12px",
                "id": "imgMandatoryState",
                "isVisible": false,
                "left": "36px",
                "skin": "slImage",
                "src": "mandatory_2x.png",
                "top": "3px",
                "width": "6px",
                "zIndex": 1
            }, {
                "imageScaleMode": constants.IMAGE_SCALE_MODE_MAINTAIN_ASPECT_RATIO,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var flxNoStateError = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "15dp",
                "id": "flxNoStateError",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "70dp",
                "width": "100%",
                "zIndex": 1
            }, {}, {});
            flxNoStateError.setDefaultUnit(kony.flex.DP);
            var lblNoStateErrorIcon = new kony.ui.Label({
                "height": "15dp",
                "id": "lblNoStateErrorIcon",
                "isVisible": true,
                "left": "0dp",
                "skin": "sknErrorIcon",
                "text": "",
                "top": "0dp",
                "width": "15dp",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var lblNoStateError = new kony.ui.Label({
                "height": "15dp",
                "id": "lblNoStateError",
                "isVisible": true,
                "left": "15dp",
                "skin": "sknlblError",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmLocations.lblNoStateError\")",
                "top": "0dp",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxNoStateError.add(lblNoStateErrorIcon, lblNoStateError);
            flxStateUser.add(lblStateUser, lstbxStateUser, imgMandatoryState, flxNoStateError);
            var flxCountryUser = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "100%",
                "id": "flxCountryUser",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0%",
                "isModalContainer": false,
                "skin": "slFbox0afd6fda7b2514b",
                "top": "0dp",
                "width": "29.20%",
                "zIndex": 1
            }, {}, {});
            flxCountryUser.setDefaultUnit(kony.flex.DP);
            var lblCountryUser = new kony.ui.Label({
                "id": "lblCountryUser",
                "isVisible": true,
                "left": "0dp",
                "skin": "slLabel0d20174dce8ea42",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmCreateNewUser.lblCountryUser\")",
                "top": "0dp",
                "width": "100%",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var lstbxCountryUser = new kony.ui.ListBox({
                "focusSkin": "sknlbxBgffffffBorder11abebRadius3Px14px",
                "height": "40px",
                "id": "lstbxCountryUser",
                "isVisible": true,
                "left": "0dp",
                "masterData": [
                    ["lb1", "Select"],
                    ["lb2", "India"],
                    ["lb3", "USA"]
                ],
                "selectedKey": "lb1",
                "skin": "sknlstbxNormal0f9abd8e88aa64a",
                "top": "25dp",
                "width": "100%",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [2, 0, 0, 0],
                "paddingInPixel": false
            }, {
                "hoverSkin": "lstboxcursor",
                "multiSelect": false
            });
            var imgMandatoryCountry = new kony.ui.Image2({
                "height": "12px",
                "id": "imgMandatoryCountry",
                "isVisible": false,
                "left": "51px",
                "skin": "slImage",
                "src": "mandatory_2x.png",
                "top": "3px",
                "width": "6px",
                "zIndex": 1
            }, {
                "imageScaleMode": constants.IMAGE_SCALE_MODE_MAINTAIN_ASPECT_RATIO,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var flxNoCountryError = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "15dp",
                "id": "flxNoCountryError",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "70dp",
                "width": "100%",
                "zIndex": 1
            }, {}, {});
            flxNoCountryError.setDefaultUnit(kony.flex.DP);
            var lblNoCountryErrorIcon = new kony.ui.Label({
                "height": "15dp",
                "id": "lblNoCountryErrorIcon",
                "isVisible": true,
                "left": "0dp",
                "skin": "sknErrorIcon",
                "text": "",
                "top": "0dp",
                "width": "15dp",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var lblNoCountryError = new kony.ui.Label({
                "height": "15dp",
                "id": "lblNoCountryError",
                "isVisible": true,
                "left": "15dp",
                "skin": "sknlblError",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmLocations.lblNoCountryError\")",
                "top": "0dp",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxNoCountryError.add(lblNoCountryErrorIcon, lblNoCountryError);
            flxCountryUser.add(lblCountryUser, lstbxCountryUser, imgMandatoryCountry, flxNoCountryError);
            flxCityUser.add(flxCityUser1, flxStateUser, flxCountryUser);
            var flxCountryDetailsUser = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "85dp",
                "id": "flxCountryDetailsUser",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "slFbox0afd6fda7b2514b",
                "top": "5px",
                "width": "100%",
                "zIndex": 1
            }, {}, {});
            flxCountryDetailsUser.setDefaultUnit(kony.flex.DP);
            var flxPhoneNumber = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "100%",
                "id": "flxPhoneNumber",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "33.42%",
                "isModalContainer": false,
                "skin": "slFbox0afd6fda7b2514b",
                "top": "0dp",
                "width": "350px",
                "zIndex": 1
            }, {}, {});
            flxPhoneNumber.setDefaultUnit(kony.flex.DP);
            var lblPhoneNumber = new kony.ui.Label({
                "id": "lblPhoneNumber",
                "isVisible": true,
                "left": "0dp",
                "skin": "slLabel0d20174dce8ea42",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmCustomerCareController.Phone_Number\")",
                "top": "0dp",
                "width": "90%",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var txtbxPhoneNumber = new kony.ui.TextBox2({
                "autoCapitalize": constants.TEXTBOX_AUTO_CAPITALIZE_NONE,
                "focusSkin": "skntxtbxDetailsFocus14px",
                "height": "40px",
                "id": "txtbxPhoneNumber",
                "isVisible": false,
                "keyBoardStyle": constants.TEXTBOX_KEY_BOARD_STYLE_DEFAULT,
                "left": "0dp",
                "maxTextLength": 15,
                "placeholder": "Phone Number",
                "secureTextEntry": false,
                "skin": "skntxtbxDetails0bbf1235271384a",
                "textInputMode": constants.TEXTBOX_INPUT_MODE_ANY,
                "top": "25dp",
                "width": "100%",
                "zIndex": 1
            }, {
                "containerHeightMode": constants.TEXTBOX_FONT_METRICS_DRIVEN_HEIGHT,
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [3, 0, 0, 0],
                "paddingInPixel": false
            }, {
                "autoCorrect": false
            });
            var flxNoPhnNumberError = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "15dp",
                "id": "flxNoPhnNumberError",
                "isVisible": false,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "70dp",
                "width": "100%",
                "zIndex": 1
            }, {}, {});
            flxNoPhnNumberError.setDefaultUnit(kony.flex.DP);
            var lblNoPhnNumberErrorIcon = new kony.ui.Label({
                "height": "15dp",
                "id": "lblNoPhnNumberErrorIcon",
                "isVisible": true,
                "left": "0dp",
                "skin": "sknErrorIcon",
                "text": "",
                "top": "0dp",
                "width": "15dp",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var lblNoPhnNumberError = new kony.ui.Label({
                "height": "15dp",
                "id": "lblNoPhnNumberError",
                "isVisible": true,
                "left": "15dp",
                "skin": "sknlblError",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmLocations.lblNoPhnNumberError\")",
                "top": "0dp",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxNoPhnNumberError.add(lblNoPhnNumberErrorIcon, lblNoPhnNumberError);
            var contactNumber = new com.adminConsole.common.contactNumber({
                "height": "100%",
                "id": "contactNumber",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "masterType": constants.MASTER_TYPE_DEFAULT,
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "25dp",
                "width": "100%",
                "overrides": {
                    "contactNumber": {
                        "top": "25dp"
                    },
                    "flxError": {
                        "isVisible": false
                    },
                    "txtContactNumber": {
                        "maxTextLength": 15
                    }
                }
            }, {
                "overrides": {}
            }, {
                "overrides": {}
            });
            flxPhoneNumber.add(lblPhoneNumber, txtbxPhoneNumber, flxNoPhnNumberError, contactNumber);
            var flxPostalCodeUser = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "100%",
                "id": "flxPostalCodeUser",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0%",
                "isModalContainer": false,
                "skin": "slFbox0afd6fda7b2514b",
                "top": "0dp",
                "width": "29.20%",
                "zIndex": 1
            }, {}, {});
            flxPostalCodeUser.setDefaultUnit(kony.flex.DP);
            var lblPostalCodeUser = new kony.ui.Label({
                "id": "lblPostalCodeUser",
                "isVisible": true,
                "left": "0dp",
                "skin": "slLabel0d20174dce8ea42",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmLocations.lblPostalCodeUser\")",
                "top": "0dp",
                "width": "90%",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var txtbxPostalCodeUser = new kony.ui.TextBox2({
                "autoCapitalize": constants.TEXTBOX_AUTO_CAPITALIZE_NONE,
                "focusSkin": "skntxtbxDetailsFocus14px",
                "height": "40px",
                "id": "txtbxPostalCodeUser",
                "isVisible": true,
                "keyBoardStyle": constants.TEXTBOX_KEY_BOARD_STYLE_DEFAULT,
                "left": "0dp",
                "i18n_placeholder": "kony.i18n.getLocalizedString(\"i18n.frmLocations.lblPostalCodeUser\")",
                "secureTextEntry": false,
                "skin": "skntxtbxDetails0bbf1235271384a",
                "textInputMode": constants.TEXTBOX_INPUT_MODE_ANY,
                "top": "25dp",
                "width": "100%",
                "zIndex": 1
            }, {
                "containerHeightMode": constants.TEXTBOX_FONT_METRICS_DRIVEN_HEIGHT,
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [3, 0, 0, 0],
                "paddingInPixel": false
            }, {
                "autoCorrect": false
            });
            var imgMandatoryZipCode = new kony.ui.Image2({
                "height": "12px",
                "id": "imgMandatoryZipCode",
                "isVisible": false,
                "left": "55px",
                "skin": "slImage",
                "src": "mandatory_2x.png",
                "top": "3px",
                "width": "6px",
                "zIndex": 1
            }, {
                "imageScaleMode": constants.IMAGE_SCALE_MODE_MAINTAIN_ASPECT_RATIO,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var flxNoZipCodeError = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "15dp",
                "id": "flxNoZipCodeError",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "70dp",
                "width": "100%",
                "zIndex": 1
            }, {}, {});
            flxNoZipCodeError.setDefaultUnit(kony.flex.DP);
            var lblNoZipCodeErrorIcon = new kony.ui.Label({
                "height": "15dp",
                "id": "lblNoZipCodeErrorIcon",
                "isVisible": true,
                "left": "0dp",
                "skin": "sknErrorIcon",
                "text": "",
                "top": "0dp",
                "width": "15dp",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var lblNoZipCodeError = new kony.ui.Label({
                "height": "15dp",
                "id": "lblNoZipCodeError",
                "isVisible": true,
                "left": "15dp",
                "skin": "sknlblError",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmLocations.lblNoZipCodeError\")",
                "top": "0dp",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxNoZipCodeError.add(lblNoZipCodeErrorIcon, lblNoZipCodeError);
            flxPostalCodeUser.add(lblPostalCodeUser, txtbxPostalCodeUser, imgMandatoryZipCode, flxNoZipCodeError);
            flxCountryDetailsUser.add(flxPhoneNumber, flxPostalCodeUser);
            var flxGeoLocation = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "110dp",
                "id": "flxGeoLocation",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "slFbox0afd6fda7b2514b",
                "top": "5px",
                "width": "100%",
                "zIndex": 1
            }, {}, {});
            flxGeoLocation.setDefaultUnit(kony.flex.DP);
            var lblGeoLocation = new kony.ui.Label({
                "id": "lblGeoLocation",
                "isVisible": true,
                "left": "0dp",
                "skin": "sknLblLato485c7513px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmLocations.lblGeoLocation\")",
                "top": "0dp",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var flxLatitude = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "100%",
                "id": "flxLatitude",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "25dp",
                "width": "29.20%",
                "zIndex": 1
            }, {}, {});
            flxLatitude.setDefaultUnit(kony.flex.DP);
            var lblLatitude = new kony.ui.Label({
                "id": "lblLatitude",
                "isVisible": true,
                "left": "0dp",
                "skin": "slLabel0d20174dce8ea42",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmLocations.lblLatitude\")",
                "top": "0dp",
                "width": "100%",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var txtbxLatitude = new kony.ui.TextBox2({
                "autoCapitalize": constants.TEXTBOX_AUTO_CAPITALIZE_NONE,
                "focusSkin": "skntxtbxDetailsFocus14px",
                "height": "40px",
                "id": "txtbxLatitude",
                "isVisible": true,
                "keyBoardStyle": constants.TEXTBOX_KEY_BOARD_STYLE_DEFAULT,
                "left": "0dp",
                "maxTextLength": 10,
                "placeholder": "Latitude",
                "secureTextEntry": false,
                "skin": "skntxtbxDetails0bbf1235271384a",
                "textInputMode": constants.TEXTBOX_INPUT_MODE_NUMERIC,
                "top": "25dp",
                "width": "100%",
                "zIndex": 1
            }, {
                "containerHeightMode": constants.TEXTBOX_FONT_METRICS_DRIVEN_HEIGHT,
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [3, 0, 0, 0],
                "paddingInPixel": false
            }, {
                "autoCorrect": false
            });
            var imgMandatoryLatitude = new kony.ui.Image2({
                "height": "12px",
                "id": "imgMandatoryLatitude",
                "isVisible": false,
                "left": "51px",
                "skin": "slImage",
                "src": "mandatory_2x.png",
                "top": "3px",
                "width": "6px",
                "zIndex": 1
            }, {
                "imageScaleMode": constants.IMAGE_SCALE_MODE_MAINTAIN_ASPECT_RATIO,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var flxNoLatitudeError = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "15dp",
                "id": "flxNoLatitudeError",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "70dp",
                "width": "100%",
                "zIndex": 1
            }, {}, {});
            flxNoLatitudeError.setDefaultUnit(kony.flex.DP);
            var lblNoLatitudeErrorIcon = new kony.ui.Label({
                "height": "15dp",
                "id": "lblNoLatitudeErrorIcon",
                "isVisible": true,
                "left": "0dp",
                "skin": "sknErrorIcon",
                "text": "",
                "top": "0dp",
                "width": "15dp",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var lblNoLatitudeError = new kony.ui.Label({
                "height": "15dp",
                "id": "lblNoLatitudeError",
                "isVisible": true,
                "left": "15dp",
                "skin": "sknlblError",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmLocations.lblNoLatitudeError\")",
                "top": "0dp",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxNoLatitudeError.add(lblNoLatitudeErrorIcon, lblNoLatitudeError);
            flxLatitude.add(lblLatitude, txtbxLatitude, imgMandatoryLatitude, flxNoLatitudeError);
            var flxLongtitude = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "100%",
                "id": "flxLongtitude",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "33.42%",
                "isModalContainer": false,
                "skin": "slFbox0afd6fda7b2514b",
                "top": "25dp",
                "width": "32.20%",
                "zIndex": 1
            }, {}, {});
            flxLongtitude.setDefaultUnit(kony.flex.DP);
            var lblLongtitude = new kony.ui.Label({
                "id": "lblLongtitude",
                "isVisible": true,
                "left": "0dp",
                "skin": "slLabel0d20174dce8ea42",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmLocations.lblLongtitude\")",
                "top": "0dp",
                "width": "90%",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var txtbxLongtitude = new kony.ui.TextBox2({
                "autoCapitalize": constants.TEXTBOX_AUTO_CAPITALIZE_NONE,
                "focusSkin": "skntxtbxDetailsFocus14px",
                "height": "40px",
                "id": "txtbxLongtitude",
                "isVisible": true,
                "keyBoardStyle": constants.TEXTBOX_KEY_BOARD_STYLE_DEFAULT,
                "left": "0dp",
                "maxTextLength": 10,
                "placeholder": "Longitude",
                "secureTextEntry": false,
                "skin": "skntxtbxDetails0bbf1235271384a",
                "textInputMode": constants.TEXTBOX_INPUT_MODE_NUMERIC,
                "top": "25dp",
                "width": "93.50%",
                "zIndex": 1
            }, {
                "containerHeightMode": constants.TEXTBOX_FONT_METRICS_DRIVEN_HEIGHT,
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [3, 0, 0, 0],
                "paddingInPixel": false
            }, {
                "autoCorrect": false
            });
            var imgMandatoryLongitude = new kony.ui.Image2({
                "height": "12px",
                "id": "imgMandatoryLongitude",
                "isVisible": false,
                "left": "60px",
                "skin": "slImage",
                "src": "mandatory_2x.png",
                "top": "3px",
                "width": "6px",
                "zIndex": 1
            }, {
                "imageScaleMode": constants.IMAGE_SCALE_MODE_MAINTAIN_ASPECT_RATIO,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var flxNoLongitudeError = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "15dp",
                "id": "flxNoLongitudeError",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "70dp",
                "width": "100%",
                "zIndex": 1
            }, {}, {});
            flxNoLongitudeError.setDefaultUnit(kony.flex.DP);
            var lblNoLongitudeErrorIcon = new kony.ui.Label({
                "height": "15dp",
                "id": "lblNoLongitudeErrorIcon",
                "isVisible": true,
                "left": "0dp",
                "skin": "sknErrorIcon",
                "text": "",
                "top": "0dp",
                "width": "15dp",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var lblNoLongitudeError = new kony.ui.Label({
                "height": "15dp",
                "id": "lblNoLongitudeError",
                "isVisible": true,
                "left": "15dp",
                "skin": "sknlblError",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmLocations.lblNoLongitudeError\")",
                "top": "0dp",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxNoLongitudeError.add(lblNoLongitudeErrorIcon, lblNoLongitudeError);
            flxLongtitude.add(lblLongtitude, txtbxLongtitude, imgMandatoryLongitude, flxNoLongitudeError);
            flxGeoLocation.add(lblGeoLocation, flxLatitude, flxLongtitude);
            flxAddressData.add(flxAddrLine1User, flxCityUser, flxCountryDetailsUser, flxGeoLocation);
            var flxOperationDetailsHeading = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "20dp",
                "id": "flxOperationDetailsHeading",
                "isVisible": true,
                "layoutType": kony.flex.FLOW_HORIZONTAL,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "20px",
                "width": "140dp",
                "zIndex": 1
            }, {}, {
                "hoverSkin": "sknCursor"
            });
            flxOperationDetailsHeading.setDefaultUnit(kony.flex.DP);
            var lblOperationDetails = new kony.ui.Label({
                "centerY": "50%",
                "id": "lblOperationDetails",
                "isVisible": true,
                "left": "0dp",
                "skin": "sknLblLato84939e13px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmLocations.lblOperationDetails\")",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var flxImg2 = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "centerY": "50%",
                "clipBounds": true,
                "height": "20dp",
                "id": "flxImg2",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "10dp",
                "isModalContainer": false,
                "skin": "slFbox",
                "width": "20px",
                "zIndex": 1
            }, {}, {});
            flxImg2.setDefaultUnit(kony.flex.DP);
            var lblIconToggle2 = new kony.ui.Label({
                "centerY": "50%",
                "height": "12dp",
                "id": "lblIconToggle2",
                "isVisible": true,
                "left": "0dp",
                "skin": "sknIcon13pxGray",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmAlertsManagement.lblIconDescription\")",
                "top": "8dp",
                "width": "14dp",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxImg2.add(lblIconToggle2);
            flxOperationDetailsHeading.add(lblOperationDetails, flxImg2);
            var lblLineUserWork = new kony.ui.Label({
                "bottom": "0px",
                "height": "1dp",
                "id": "lblLineUserWork",
                "isVisible": true,
                "left": "0dp",
                "skin": "sknLble1e5edOp100",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmAlertsManagement.lblAlertTypeDescriptionCount\")",
                "top": "5dp",
                "width": "100%",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var flxOperationDetailsData = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                "clipBounds": true,
                "id": "flxOperationDetailsData",
                "isVisible": true,
                "layoutType": kony.flex.FLOW_VERTICAL,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "20dp",
                "width": "100%",
                "zIndex": 1
            }, {}, {});
            flxOperationDetailsData.setDefaultUnit(kony.flex.DP);
            var flxOperationDetailsWeekdays = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                "clipBounds": true,
                "id": "flxOperationDetailsWeekdays",
                "isVisible": true,
                "layoutType": kony.flex.FLOW_VERTICAL,
                "left": "0dp",
                "masterType": constants.MASTER_TYPE_USERWIDGET,
                "isModalContainer": false,
                "skin": "slFbox0afd6fda7b2514b",
                "top": "0dp",
                "width": "100%",
                "zIndex": 1
            }, {}, {});
            flxOperationDetailsWeekdays.setDefaultUnit(kony.flex.DP);
            var flxWeekdaysHeader = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "20dp",
                "id": "flxWeekdaysHeader",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0dp",
                "width": "100%",
                "zIndex": 1
            }, {}, {});
            flxWeekdaysHeader.setDefaultUnit(kony.flex.DP);
            var lblWeekDays = new kony.ui.Label({
                "id": "lblWeekDays",
                "isVisible": true,
                "left": "0dp",
                "skin": "sknLblLato485c7513px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmLocations.lblWeekDays\")",
                "top": "0px",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxWeekdaysHeader.add(lblWeekDays);
            var flxWorkingHours = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "110dp",
                "id": "flxWorkingHours",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "slFbox0afd6fda7b2514b",
                "top": "5px",
                "width": "100%",
                "zIndex": 1
            }, {}, {});
            flxWorkingHours.setDefaultUnit(kony.flex.DP);
            var flxFrom = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "100%",
                "id": "flxFrom",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "slFbox0afd6fda7b2514b",
                "top": "20dp",
                "width": "212px",
                "zIndex": 1
            }, {}, {});
            flxFrom.setDefaultUnit(kony.flex.DP);
            var lblFrom = new kony.ui.Label({
                "id": "lblFrom",
                "isVisible": true,
                "left": "0dp",
                "skin": "slLabel0d20174dce8ea42",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmLocations.lblFrom\")",
                "top": "-1dp",
                "width": "100%",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var timePicker = new com.adminConsole.common.timePicker({
                "height": "100px",
                "id": "timePicker",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "masterType": constants.MASTER_TYPE_DEFAULT,
                "isModalContainer": false,
                "skin": "CopyslFbox0b6ab2ac21ea948",
                "top": "0dp",
                "width": "100%",
                "overrides": {}
            }, {
                "overrides": {}
            }, {
                "overrides": {}
            });
            flxFrom.add(lblFrom, timePicker);
            var flxTo = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "100%",
                "id": "flxTo",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "240dp",
                "isModalContainer": false,
                "skin": "slFbox0afd6fda7b2514b",
                "top": "20dp",
                "width": "212px",
                "zIndex": 1
            }, {}, {});
            flxTo.setDefaultUnit(kony.flex.DP);
            var lblTo = new kony.ui.Label({
                "id": "lblTo",
                "isVisible": true,
                "left": "0dp",
                "skin": "slLabel0d20174dce8ea42",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmLocations.lblTo\")",
                "top": "0dp",
                "width": "90%",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var timePicker1 = new com.adminConsole.common.timePicker({
                "height": "100px",
                "id": "timePicker1",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "masterType": constants.MASTER_TYPE_DEFAULT,
                "isModalContainer": false,
                "skin": "CopyslFbox0b6ab2ac21ea948",
                "top": "0dp",
                "width": "100%",
                "overrides": {}
            }, {
                "overrides": {}
            }, {
                "overrides": {}
            });
            flxTo.add(lblTo, timePicker1);
            var flxErrorTextWeekday = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "bottom": "5dp",
                "clipBounds": true,
                "height": "15dp",
                "id": "flxErrorTextWeekday",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "slFbox",
                "width": "200dp",
                "zIndex": 1
            }, {}, {});
            flxErrorTextWeekday.setDefaultUnit(kony.flex.DP);
            var lblErrorTextWeekday = new kony.ui.Label({
                "height": "15dp",
                "id": "lblErrorTextWeekday",
                "isVisible": true,
                "left": "15dp",
                "skin": "sknlblError",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmLocationsController.Select_Valid_Time\")",
                "top": "0dp",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var lblErrorTextWeekdayIcon = new kony.ui.Label({
                "height": "15dp",
                "id": "lblErrorTextWeekdayIcon",
                "isVisible": true,
                "left": "0dp",
                "skin": "sknErrorIcon",
                "text": "",
                "top": "0dp",
                "width": "15dp",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxErrorTextWeekday.add(lblErrorTextWeekday, lblErrorTextWeekdayIcon);
            flxWorkingHours.add(flxFrom, flxTo, flxErrorTextWeekday);
            flxOperationDetailsWeekdays.add(flxWeekdaysHeader, flxWorkingHours);
            var flxSeperatorOperationDetails = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "1px",
                "id": "flxSeperatorOperationDetails",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "20dp",
                "width": "96.50%",
                "zIndex": 1
            }, {}, {});
            flxSeperatorOperationDetails.setDefaultUnit(kony.flex.DP);
            var lblLine3 = new kony.ui.Label({
                "height": "1dp",
                "id": "lblLine3",
                "isVisible": true,
                "left": "0dp",
                "skin": "sknLble1e5edOp100",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmAlertsManagement.lblAlertTypeDescriptionCount\")",
                "top": "0px",
                "width": "100%",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxSeperatorOperationDetails.add(lblLine3);
            var flxOperationDetailsWeekends = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                "bottom": "10px",
                "clipBounds": true,
                "id": "flxOperationDetailsWeekends",
                "isVisible": true,
                "layoutType": kony.flex.FLOW_VERTICAL,
                "left": "0dp",
                "masterType": constants.MASTER_TYPE_USERWIDGET,
                "isModalContainer": false,
                "skin": "slFbox0afd6fda7b2514b",
                "top": "0dp",
                "width": "100%",
                "zIndex": 1
            }, {}, {});
            flxOperationDetailsWeekends.setDefaultUnit(kony.flex.DP);
            var flxWeekendsSwitch = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "30dp",
                "id": "flxWeekendsSwitch",
                "isVisible": true,
                "layoutType": kony.flex.FLOW_HORIZONTAL,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "20dp",
                "width": "100%",
                "zIndex": 1
            }, {}, {});
            flxWeekendsSwitch.setDefaultUnit(kony.flex.DP);
            var lblWeekendSchedule = new kony.ui.Label({
                "centerY": "50%",
                "id": "lblWeekendSchedule",
                "isVisible": true,
                "left": 0,
                "skin": "slLabel0d20174dce8ea42",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmLocations.lblWeekendSchedule\")",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var flxToggle2 = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "centerY": "50%",
                "clipBounds": true,
                "height": "20dp",
                "id": "flxToggle2",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "10dp",
                "isModalContainer": false,
                "skin": "slFbox",
                "width": "36px",
                "zIndex": 1
            }, {}, {});
            flxToggle2.setDefaultUnit(kony.flex.DP);
            var switchWeekendSchedule = new kony.ui.Switch({
                "height": "20dp",
                "id": "switchWeekendSchedule",
                "isVisible": true,
                "left": "0dp",
                "leftSideText": "ON",
                "rightSideText": "OFF",
                "selectedIndex": 1,
                "skin": "sknSwitchServiceManagement",
                "top": "0dp",
                "width": "36dp",
                "zIndex": 1
            }, {
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxToggle2.add(switchWeekendSchedule);
            flxWeekendsSwitch.add(lblWeekendSchedule, flxToggle2);
            var flxWorkingTime = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                "clipBounds": true,
                "id": "flxWorkingTime",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "slFbox0afd6fda7b2514b",
                "top": "0px",
                "width": "100%",
                "zIndex": 1
            }, {}, {});
            flxWorkingTime.setDefaultUnit(kony.flex.DP);
            var flxDays = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "40dp",
                "id": "flxDays",
                "isVisible": true,
                "layoutType": kony.flex.FLOW_HORIZONTAL,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "15dp",
                "width": "100%",
                "zIndex": 1
            }, {}, {});
            flxDays.setDefaultUnit(kony.flex.DP);
            var flxSaturdayCheckBox = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "centerY": "50%",
                "clipBounds": true,
                "height": "15px",
                "id": "flxSaturdayCheckBox",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "slFbox",
                "width": "15px",
                "zIndex": 1
            }, {}, {});
            flxSaturdayCheckBox.setDefaultUnit(kony.flex.DP);
            var imgSaturdayCheckBox = new kony.ui.Image2({
                "height": "15px",
                "id": "imgSaturdayCheckBox",
                "isVisible": true,
                "left": "0px",
                "skin": "slImage",
                "src": "checkbox.png",
                "top": "0px",
                "width": "15px",
                "zIndex": 1
            }, {
                "imageScaleMode": constants.IMAGE_SCALE_MODE_MAINTAIN_ASPECT_RATIO,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxSaturdayCheckBox.add(imgSaturdayCheckBox);
            var lblSaturday = new kony.ui.Label({
                "centerY": "50%",
                "id": "lblSaturday",
                "isVisible": true,
                "left": "5dp",
                "skin": "slLabel0d20174dce8ea42",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmLocations.lblSaturday\")",
                "top": "18dp",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var flxSundayCheckBox = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "centerY": "50%",
                "clipBounds": true,
                "height": "15px",
                "id": "flxSundayCheckBox",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "25px",
                "isModalContainer": false,
                "skin": "slFbox",
                "width": "15px",
                "zIndex": 1
            }, {}, {});
            flxSundayCheckBox.setDefaultUnit(kony.flex.DP);
            var imgSundayCheckBox = new kony.ui.Image2({
                "height": "15px",
                "id": "imgSundayCheckBox",
                "isVisible": true,
                "left": "0px",
                "skin": "slImage",
                "src": "checkbox.png",
                "top": "0px",
                "width": "15px",
                "zIndex": 1
            }, {
                "imageScaleMode": constants.IMAGE_SCALE_MODE_MAINTAIN_ASPECT_RATIO,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxSundayCheckBox.add(imgSundayCheckBox);
            var lblSunday = new kony.ui.Label({
                "centerY": "50%",
                "id": "lblSunday",
                "isVisible": true,
                "left": "5dp",
                "skin": "slLabel0d20174dce8ea42",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmLocations.lblSunday\")",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxDays.add(flxSaturdayCheckBox, lblSaturday, flxSundayCheckBox, lblSunday);
            var flxFromTime = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "100px",
                "id": "flxFromTime",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "slFbox0afd6fda7b2514b",
                "top": "70px",
                "width": "212dp",
                "zIndex": 1
            }, {}, {});
            flxFromTime.setDefaultUnit(kony.flex.DP);
            var lblFromWeekends = new kony.ui.Label({
                "id": "lblFromWeekends",
                "isVisible": true,
                "left": "0dp",
                "skin": "slLabel0d20174dce8ea42",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmLocations.lblFrom\")",
                "top": "0dp",
                "width": "100%",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var timePicker2 = new com.adminConsole.common.timePicker({
                "height": "100px",
                "id": "timePicker2",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "masterType": constants.MASTER_TYPE_DEFAULT,
                "isModalContainer": false,
                "skin": "CopyslFbox0b6ab2ac21ea948",
                "top": "0dp",
                "width": "100%",
                "overrides": {}
            }, {
                "overrides": {}
            }, {
                "overrides": {}
            });
            flxFromTime.add(lblFromWeekends, timePicker2);
            var flxToTime = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "100px",
                "id": "flxToTime",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "240dp",
                "isModalContainer": false,
                "skin": "slFbox0afd6fda7b2514b",
                "top": "70dp",
                "width": "212dp",
                "zIndex": 1
            }, {}, {});
            flxToTime.setDefaultUnit(kony.flex.DP);
            var lblToWeekends = new kony.ui.Label({
                "id": "lblToWeekends",
                "isVisible": true,
                "left": "0dp",
                "skin": "slLabel0d20174dce8ea42",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmLocations.lblTo\")",
                "top": "0dp",
                "width": "90%",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var timePicker3 = new com.adminConsole.common.timePicker({
                "height": "100px",
                "id": "timePicker3",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "masterType": constants.MASTER_TYPE_DEFAULT,
                "isModalContainer": false,
                "skin": "CopyslFbox0b6ab2ac21ea948",
                "top": "0dp",
                "width": "100%",
                "overrides": {}
            }, {
                "overrides": {}
            }, {
                "overrides": {}
            });
            flxToTime.add(lblToWeekends, timePicker3);
            var flxWeekendSchErrorMsg = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "15dp",
                "id": "flxWeekendSchErrorMsg",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "135dp",
                "width": "200dp",
                "zIndex": 1
            }, {}, {});
            flxWeekendSchErrorMsg.setDefaultUnit(kony.flex.DP);
            var lblWeekendSchErrorMsg = new kony.ui.Label({
                "height": "15dp",
                "id": "lblWeekendSchErrorMsg",
                "isVisible": true,
                "left": "15dp",
                "skin": "sknlblError",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmLocationsController.Select_a_Day\")",
                "top": "0dp",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var lblWeekendSchErrorMsgIcon = new kony.ui.Label({
                "height": "15dp",
                "id": "lblWeekendSchErrorMsgIcon",
                "isVisible": true,
                "left": "0dp",
                "skin": "sknErrorIcon",
                "text": "",
                "top": "0dp",
                "width": "15dp",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxWeekendSchErrorMsg.add(lblWeekendSchErrorMsg, lblWeekendSchErrorMsgIcon);
            flxWorkingTime.add(flxDays, flxFromTime, flxToTime, flxWeekendSchErrorMsg);
            flxOperationDetailsWeekends.add(flxWeekendsSwitch, flxWorkingTime);
            flxOperationDetailsData.add(flxOperationDetailsWeekdays, flxSeperatorOperationDetails, flxOperationDetailsWeekends);
            flxLocationDetailsData.add(flxDetailsHeading, lblLineUser, flxDetailsData, flxAddressHeading, lblLine2, flxAddressData, flxOperationDetailsHeading, lblLineUserWork, flxOperationDetailsData);
            var flxLocationDetailsSeperator = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "bottom": 81,
                "centerX": "50%",
                "clipBounds": true,
                "height": "1px",
                "id": "flxLocationDetailsSeperator",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0px",
                "isModalContainer": false,
                "right": "0px",
                "skin": "sknflxe1e5edop100",
                "zIndex": 2
            }, {}, {});
            flxLocationDetailsSeperator.setDefaultUnit(kony.flex.DP);
            flxLocationDetailsSeperator.add();
            var flxLocationDetailsButtons = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "bottom": 0,
                "clipBounds": true,
                "height": "80px",
                "id": "flxLocationDetailsButtons",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "slFbox",
                "width": "100%",
                "zIndex": 1
            }, {}, {});
            flxLocationDetailsButtons.setDefaultUnit(kony.flex.DP);
            var locationDetailsButtons = new com.adminConsole.common.commonButtons({
                "height": "80px",
                "id": "locationDetailsButtons",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "masterType": constants.MASTER_TYPE_DEFAULT,
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0dp",
                "width": "100%",
                "overrides": {
                    "btnCancel": {
                        "left": "10px"
                    },
                    "flxRightButtons": {
                        "right": "10px"
                    }
                }
            }, {
                "overrides": {}
            }, {
                "overrides": {}
            });
            flxLocationDetailsButtons.add(locationDetailsButtons);
            flxLocationDetailsWrapper.add(flxLocationDetailsData, flxLocationDetailsSeperator, flxLocationDetailsButtons);
            var flxAddAndRemoveServicesWrapper = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "bottom": "0px",
                "clipBounds": true,
                "id": "flxAddAndRemoveServicesWrapper",
                "isVisible": false,
                "layoutType": kony.flex.FREE_FORM,
                "left": "230px",
                "isModalContainer": false,
                "right": "0px",
                "skin": "sknbackGroundffffff100",
                "top": "0px",
                "zIndex": 1
            }, {}, {});
            flxAddAndRemoveServicesWrapper.setDefaultUnit(kony.flex.DP);
            var flxAddAndRemoveServices = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "bottom": "81px",
                "clipBounds": true,
                "id": "flxAddAndRemoveServices",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0dp",
                "width": "100%",
                "zIndex": 1
            }, {}, {});
            flxAddAndRemoveServices.setDefaultUnit(kony.flex.DP);
            var addAndRemoveOptions = new com.adminConsole.common.addAndRemoveOptions1({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "bottom": "0px",
                "id": "addAndRemoveOptions",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "masterType": constants.MASTER_TYPE_DEFAULT,
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0dp",
                "width": "100%",
                "overrides": {
                    "addAndRemoveOptions1": {
                        "bottom": "0px",
                        "centerX": "viz.val_cleared",
                        "centerY": "viz.val_cleared",
                        "height": "viz.val_cleared",
                        "left": "0dp",
                        "maxHeight": "viz.val_cleared",
                        "maxWidth": "viz.val_cleared",
                        "minHeight": "viz.val_cleared",
                        "minWidth": "viz.val_cleared",
                        "right": "viz.val_cleared",
                        "top": "0dp",
                        "width": "100%"
                    },
                    "btnRemoveAll": {
                        "i18n_text": "kony.i18n.getLocalizedString(\"i18n.Common.button.Reset\")"
                    },
                    "flxClearSearchImage": {
                        "isVisible": false
                    },
                    "imgLoadingAccountSearch": {
                        "src": "loadingscreenimage.gif"
                    }
                }
            }, {
                "overrides": {}
            }, {
                "overrides": {}
            });
            flxAddAndRemoveServices.add(addAndRemoveOptions);
            var flxAddAndRemoveSeperator = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "bottom": 81,
                "centerX": "50%",
                "clipBounds": true,
                "height": "1px",
                "id": "flxAddAndRemoveSeperator",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0px",
                "isModalContainer": false,
                "right": "0px",
                "skin": "sknflxe1e5edop100",
                "zIndex": 2
            }, {}, {});
            flxAddAndRemoveSeperator.setDefaultUnit(kony.flex.DP);
            flxAddAndRemoveSeperator.add();
            var flxAddAndRemoveServicesButtons = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "bottom": 0,
                "clipBounds": true,
                "height": "80px",
                "id": "flxAddAndRemoveServicesButtons",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "slFbox",
                "width": "100%",
                "zIndex": 1
            }, {}, {});
            flxAddAndRemoveServicesButtons.setDefaultUnit(kony.flex.DP);
            var AddAndRemoveServicesButtons = new com.adminConsole.common.commonButtons({
                "height": "80px",
                "id": "AddAndRemoveServicesButtons",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "masterType": constants.MASTER_TYPE_DEFAULT,
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0dp",
                "width": "100%",
                "overrides": {
                    "btnCancel": {
                        "left": "10px"
                    },
                    "flxRightButtons": {
                        "right": "10px"
                    }
                }
            }, {
                "overrides": {}
            }, {
                "overrides": {}
            });
            flxAddAndRemoveServicesButtons.add(AddAndRemoveServicesButtons);
            flxAddAndRemoveServicesWrapper.add(flxAddAndRemoveServices, flxAddAndRemoveSeperator, flxAddAndRemoveServicesButtons);
            flxLocationDetailsAndServices.add(flxVerticalTabs, flxLocationDetailsWrapper, flxAddAndRemoveServicesWrapper);
            flxScrollMainContent.add(flxListingPage, flxViewLocation, flxLocationDetailsAndServices);
            var flxToastMessage = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "bottom": "-70px",
                "clipBounds": true,
                "height": "70px",
                "id": "flxToastMessage",
                "isVisible": false,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0px",
                "isModalContainer": false,
                "right": "0px",
                "skin": "slFbox",
                "zIndex": 1
            }, {}, {});
            flxToastMessage.setDefaultUnit(kony.flex.DP);
            var toastMessage = new com.adminConsole.common.toastMessage({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "id": "toastMessage",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "masterType": constants.MASTER_TYPE_DEFAULT,
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0dp",
                "overrides": {
                    "lbltoastMessage": {
                        "text": "Branch deactivated successfully"
                    }
                }
            }, {
                "overrides": {}
            }, {
                "overrides": {}
            });
            flxToastMessage.add(toastMessage);
            var flxToastMessageWithLink = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "bottom": "-70px",
                "clipBounds": true,
                "height": "70px",
                "id": "flxToastMessageWithLink",
                "isVisible": false,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0px",
                "isModalContainer": false,
                "skin": "slFbox",
                "width": "100%",
                "zIndex": 1
            }, {}, {});
            flxToastMessageWithLink.setDefaultUnit(kony.flex.DP);
            var toastMessageWithLink = new com.adminConsole.common.toastMessageWithLink({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "height": "70px",
                "id": "toastMessageWithLink",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "masterType": constants.MASTER_TYPE_DEFAULT,
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0dp",
                "width": "100%",
                "overrides": {
                    "lblToastMessageLeft": {
                        "text": "20/100 branches uploaded to the system."
                    },
                    "lblToastMessageRight": {
                        "text": "to download the details."
                    }
                }
            }, {
                "overrides": {}
            }, {
                "overrides": {}
            });
            flxToastMessageWithLink.add(toastMessageWithLink);
            var flxAdvancedSearch = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                "clipBounds": true,
                "id": "flxAdvancedSearch",
                "isVisible": false,
                "layoutType": kony.flex.FREE_FORM,
                "left": "35px",
                "isModalContainer": false,
                "right": "35px",
                "skin": "CopyslFbox0c37f1533930c4d",
                "top": "0px",
                "zIndex": 1
            }, {}, {});
            flxAdvancedSearch.setDefaultUnit(kony.flex.DP);
            var flxSearch1 = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                "bottom": "35px",
                "clipBounds": true,
                "id": "flxSearch1",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "CopyslFbox0ddacfb6d79684f",
                "top": "0dp",
                "width": "100%",
                "zIndex": 1
            }, {}, {});
            flxSearch1.setDefaultUnit(kony.flex.DP);
            var flxSearchHeading = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                "clipBounds": true,
                "id": "flxSearchHeading",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0%",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0px",
                "width": "100%",
                "zIndex": 1
            }, {}, {});
            flxSearchHeading.setDefaultUnit(kony.flex.DP);
            var advancedSearch = new com.adminConsole.search.advancedSearch({
                "height": "85px",
                "id": "advancedSearch",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "masterType": constants.MASTER_TYPE_DEFAULT,
                "isModalContainer": false,
                "skin": "slFbox0hfd18814fd664dCM",
                "top": "0dp",
                "width": "100%",
                "overrides": {}
            }, {
                "overrides": {}
            }, {
                "overrides": {}
            });
            var flxSearchResult = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "60px",
                "id": "flxSearchResult",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "85dp",
                "width": "100%",
                "zIndex": 1
            }, {}, {});
            flxSearchResult.setDefaultUnit(kony.flex.DP);
            var flxCsr = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "25px",
                "id": "flxCsr",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "35dp",
                "isModalContainer": false,
                "skin": "CopyslFbox0ica8ce013fdc40",
                "top": "16dp",
                "width": "95px",
                "zIndex": 1
            }, {}, {});
            flxCsr.setDefaultUnit(kony.flex.DP);
            var lblCsrAssist = new kony.ui.Label({
                "centerY": "50%",
                "id": "lblCsrAssist",
                "isVisible": true,
                "left": "5dp",
                "skin": "CopyslLabel0d2920a8c52fc41",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmLogsController.CSR_Assist\")",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var imgCross = new kony.ui.Image2({
                "centerY": "50%",
                "height": "10dp",
                "id": "imgCross",
                "isVisible": true,
                "left": "75dp",
                "skin": "slImage",
                "src": "close_blue.png",
                "width": "10dp",
                "zIndex": 1
            }, {
                "imageScaleMode": constants.IMAGE_SCALE_MODE_MAINTAIN_ASPECT_RATIO,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxCsr.add(lblCsrAssist, imgCross);
            var flxAdmin = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "25px",
                "id": "flxAdmin",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "140dp",
                "isModalContainer": false,
                "skin": "CopyslFbox0ica8ce013fdc40",
                "top": "16dp",
                "width": "95px",
                "zIndex": 1
            }, {}, {});
            flxAdmin.setDefaultUnit(kony.flex.DP);
            var lblAdmin = new kony.ui.Label({
                "centerY": "50%",
                "id": "lblAdmin",
                "isVisible": true,
                "left": "25dp",
                "skin": "CopyslLabel0d2920a8c52fc41",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmLogsController.Admin\")",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var imgCross1 = new kony.ui.Image2({
                "centerY": "50%",
                "height": "10dp",
                "id": "imgCross1",
                "isVisible": true,
                "left": "75dp",
                "skin": "slImage",
                "src": "close_blue.png",
                "width": "10dp",
                "zIndex": 1
            }, {
                "imageScaleMode": constants.IMAGE_SCALE_MODE_MAINTAIN_ASPECT_RATIO,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxAdmin.add(lblAdmin, imgCross1);
            var flxSuperAdmin = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "25px",
                "id": "flxSuperAdmin",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "245dp",
                "isModalContainer": false,
                "skin": "CopyslFbox0ica8ce013fdc40",
                "top": "16dp",
                "width": "95px",
                "zIndex": 1
            }, {}, {});
            flxSuperAdmin.setDefaultUnit(kony.flex.DP);
            var lblSuperAdmin = new kony.ui.Label({
                "centerY": "50%",
                "id": "lblSuperAdmin",
                "isVisible": true,
                "left": "5dp",
                "skin": "CopyslLabel0d2920a8c52fc41",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmLogsController.Super_Admin\")",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var imgCross2 = new kony.ui.Image2({
                "centerY": "50%",
                "height": "10dp",
                "id": "imgCross2",
                "isVisible": true,
                "left": "75dp",
                "skin": "slImage",
                "src": "close_blue.png",
                "width": "10dp",
                "zIndex": 1
            }, {
                "imageScaleMode": constants.IMAGE_SCALE_MODE_MAINTAIN_ASPECT_RATIO,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxSuperAdmin.add(lblSuperAdmin, imgCross2);
            var flxSeparetor = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "centerY": "50%",
                "clipBounds": true,
                "height": "25px",
                "id": "flxSeparetor",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "356dp",
                "isModalContainer": false,
                "skin": "CopyslFbox0b600ae3ddf634f",
                "width": "1px",
                "zIndex": 1
            }, {}, {});
            flxSeparetor.setDefaultUnit(kony.flex.DP);
            flxSeparetor.add();
            var lblClearAll = new kony.ui.Label({
                "centerY": "50%",
                "id": "lblClearAll",
                "isVisible": true,
                "left": "372dp",
                "skin": "CopyslLabel0d6e029a5d8e647",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmCustomerCare.lblClearAll\")",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var flxUnderline2 = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "bottom": "0dp",
                "clipBounds": true,
                "height": "1px",
                "id": "flxUnderline2",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "CopyslFbox0g9535637f9db4c",
                "width": "100%",
                "zIndex": 1
            }, {}, {});
            flxUnderline2.setDefaultUnit(kony.flex.DP);
            flxUnderline2.add();
            flxSearchResult.add(flxCsr, flxAdmin, flxSuperAdmin, flxSeparetor, lblClearAll, flxUnderline2);
            flxSearchHeading.add(advancedSearch, flxSearchResult);
            var flxSegmentUsers1 = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                "clipBounds": false,
                "id": "flxSegmentUsers1",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0%",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "146px",
                "width": "100%",
                "zIndex": 1
            }, {}, {});
            flxSegmentUsers1.setDefaultUnit(kony.flex.DP);
            var segusers1 = new kony.ui.SegmentedUI2({
                "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                "data": [
                    [{
                            "imgSortEmail": "sorting3x.png",
                            "imgSortName": "sorting3x.png",
                            "imgSortPermissions": "sorting3x.png",
                            "imgSortRole": "sorting3x.png",
                            "imgSortStatus": "sorting3x.png",
                            "imgSortUsername": "sorting3x.png",
                            "lblUsersHeaderEmail": "EMAIL",
                            "lblUsersHeaderName": "FULL NAME",
                            "lblUsersHeaderPermissions": "PERMISSIONS",
                            "lblUsersHeaderRole": "ROLE",
                            "lblUsersHeaderSeperator": ".",
                            "lblUsersHeaderStatus": "STATUS",
                            "lblUsersHeaderUsername": "USERNAME"
                        },
                        [{
                            "imgOptions": "dots3x.png",
                            "imgUsersStatus": "active_circle2x.png",
                            "lblEmailId": "john.doe@kony.com",
                            "lblFullName": "John Doe",
                            "lblPermissions": "25",
                            "lblRole": "20",
                            "lblSeperator": ".",
                            "lblUsername": "john.doe",
                            "lblUsersStatus": "Active"
                        }, {
                            "imgOptions": "dots3x.png",
                            "imgUsersStatus": "active_circle2x.png",
                            "lblEmailId": "john.doe@kony.com",
                            "lblFullName": "John Doe",
                            "lblPermissions": "25",
                            "lblRole": "20",
                            "lblSeperator": ".",
                            "lblUsername": "john.doe",
                            "lblUsersStatus": "Active"
                        }, {
                            "imgOptions": "dots3x.png",
                            "imgUsersStatus": "active_circle2x.png",
                            "lblEmailId": "john.doe@kony.com",
                            "lblFullName": "John Doe",
                            "lblPermissions": "25",
                            "lblRole": "20",
                            "lblSeperator": ".",
                            "lblUsername": "john.doe",
                            "lblUsersStatus": "Active"
                        }, {
                            "imgOptions": "dots3x.png",
                            "imgUsersStatus": "active_circle2x.png",
                            "lblEmailId": "john.doe@kony.com",
                            "lblFullName": "John Doe",
                            "lblPermissions": "25",
                            "lblRole": "20",
                            "lblSeperator": ".",
                            "lblUsername": "john.doe",
                            "lblUsersStatus": "Active"
                        }, {
                            "imgOptions": "dots3x.png",
                            "imgUsersStatus": "active_circle2x.png",
                            "lblEmailId": "john.doe@kony.com",
                            "lblFullName": "John Doe",
                            "lblPermissions": "25",
                            "lblRole": "20",
                            "lblSeperator": ".",
                            "lblUsername": "john.doe",
                            "lblUsersStatus": "Active"
                        }]
                    ]
                ],
                "groupCells": false,
                "id": "segusers1",
                "isVisible": true,
                "left": "35dp",
                "needPageIndicator": true,
                "pageOffDotImage": "pageoffdot.png",
                "pageOnDotImage": "pageondot.png",
                "retainSelection": false,
                "right": 35,
                "rowFocusSkin": "seg2Focus",
                "rowSkin": "sknseg3pxRadius",
                "rowTemplate": "flxUsers",
                "sectionHeaderSkin": "sliPhoneSegmentHeader",
                "sectionHeaderTemplate": "flxUsersHeader",
                "selectionBehavior": constants.SEGUI_DEFAULT_BEHAVIOR,
                "separatorRequired": false,
                "showScrollbars": false,
                "top": "0dp",
                "viewType": constants.SEGUI_VIEW_TYPE_TABLEVIEW,
                "widgetDataMap": {
                    "flxOptions": "flxOptions",
                    "flxStatus": "flxStatus",
                    "flxUsers": "flxUsers",
                    "flxUsersHeader": "flxUsersHeader",
                    "flxUsersHeaderEmailId": "flxUsersHeaderEmailId",
                    "flxUsersHeaderFullName": "flxUsersHeaderFullName",
                    "flxUsersHeaderPermissions": "flxUsersHeaderPermissions",
                    "flxUsersHeaderRole": "flxUsersHeaderRole",
                    "flxUsersHeaderStatus": "flxUsersHeaderStatus",
                    "flxUsersHeaderUsername": "flxUsersHeaderUsername",
                    "imgOptions": "imgOptions",
                    "imgSortEmail": "imgSortEmail",
                    "imgSortName": "imgSortName",
                    "imgSortPermissions": "imgSortPermissions",
                    "imgSortRole": "imgSortRole",
                    "imgSortStatus": "imgSortStatus",
                    "imgSortUsername": "imgSortUsername",
                    "imgUsersStatus": "imgUsersStatus",
                    "lblEmailId": "lblEmailId",
                    "lblFullName": "lblFullName",
                    "lblPermissions": "lblPermissions",
                    "lblRole": "lblRole",
                    "lblSeperator": "lblSeperator",
                    "lblUsername": "lblUsername",
                    "lblUsersHeaderEmail": "lblUsersHeaderEmail",
                    "lblUsersHeaderName": "lblUsersHeaderName",
                    "lblUsersHeaderPermissions": "lblUsersHeaderPermissions",
                    "lblUsersHeaderRole": "lblUsersHeaderRole",
                    "lblUsersHeaderSeperator": "lblUsersHeaderSeperator",
                    "lblUsersHeaderStatus": "lblUsersHeaderStatus",
                    "lblUsersHeaderUsername": "lblUsersHeaderUsername",
                    "lblUsersStatus": "lblUsersStatus"
                },
                "zIndex": 10
            }, {
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var flexOptions = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                "clipBounds": false,
                "id": "flexOptions",
                "isVisible": false,
                "layoutType": kony.flex.FLOW_VERTICAL,
                "isModalContainer": false,
                "right": "2%",
                "skin": "sknflxffffffop100dbdbe6Radius3px",
                "top": "100px",
                "width": "18.39%",
                "zIndex": 100
            }, {}, {});
            flexOptions.setDefaultUnit(kony.flex.DP);
            var CopylblDescription0a252259b5c5b47 = new kony.ui.Label({
                "height": "40px",
                "id": "CopylblDescription0a252259b5c5b47",
                "isVisible": true,
                "left": "15px",
                "skin": "sknlblLato5d6c7f12px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.roles.ASSIGN\")",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 6, 0, 0],
                "paddingInPixel": false
            }, {});
            var CopylblRoles0f4ed925d516743 = new kony.ui.Label({
                "id": "CopylblRoles0f4ed925d516743",
                "isVisible": true,
                "left": "15dp",
                "skin": "sknlbllato00a3d412px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.permission.Users\")",
                "top": "0dp",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var CopylblPremissions0f8ff66e806ad46 = new kony.ui.Label({
                "bottom": 7,
                "id": "CopylblPremissions0f8ff66e806ad46",
                "isVisible": true,
                "left": "15dp",
                "skin": "sknlbllato00a3d412px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmCustomerCare.CopylblPremissions0f8ff66e806ad46\")",
                "top": "5dp",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var CopyflxSeperator0f47be42d7a5845 = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "centerX": "50%",
                "clipBounds": true,
                "height": "1px",
                "id": "CopyflxSeperator0f47be42d7a5845",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "isModalContainer": false,
                "skin": "sknflxd6dbe7",
                "top": "0px",
                "width": "100%",
                "zIndex": 1
            }, {}, {});
            CopyflxSeperator0f47be42d7a5845.setDefaultUnit(kony.flex.DP);
            CopyflxSeperator0f47be42d7a5845.add();
            var CopyflxOption0f482b0cd04b74b = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "35dp",
                "id": "CopyflxOption0f482b0cd04b74b",
                "isVisible": false,
                "layoutType": kony.flex.FLOW_HORIZONTAL,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0dp",
                "width": "100%",
                "zIndex": 1
            }, {}, {});
            CopyflxOption0f482b0cd04b74b.setDefaultUnit(kony.flex.DP);
            var CopyimgOption0cd1138e9eecd41 = new kony.ui.Image2({
                "centerY": "50%",
                "height": "15dp",
                "id": "CopyimgOption0cd1138e9eecd41",
                "isVisible": true,
                "left": "15dp",
                "skin": "slImage",
                "src": "imagedrag.png",
                "width": "15dp",
                "zIndex": 1
            }, {
                "imageScaleMode": constants.IMAGE_SCALE_MODE_MAINTAIN_ASPECT_RATIO,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var CopylblOption0h45b31f2ff7d41 = new kony.ui.Label({
                "centerY": "50%",
                "id": "CopylblOption0h45b31f2ff7d41",
                "isVisible": true,
                "left": "15dp",
                "skin": "sknlblLatoBold35475f14px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.permission.View\")",
                "top": "4dp",
                "width": "40px",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            CopyflxOption0f482b0cd04b74b.add(CopyimgOption0cd1138e9eecd41, CopylblOption0h45b31f2ff7d41);
            var CopyflxOption0bc20c8a8030749 = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "35dp",
                "id": "CopyflxOption0bc20c8a8030749",
                "isVisible": true,
                "layoutType": kony.flex.FLOW_HORIZONTAL,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0dp",
                "width": "100%",
                "zIndex": 1
            }, {}, {});
            CopyflxOption0bc20c8a8030749.setDefaultUnit(kony.flex.DP);
            var CopyimgOption0b58a89219f314f = new kony.ui.Image2({
                "centerY": "50%",
                "height": "15dp",
                "id": "CopyimgOption0b58a89219f314f",
                "isVisible": true,
                "left": "15dp",
                "skin": "slImage",
                "src": "edit2x.png",
                "width": "15dp",
                "zIndex": 1
            }, {
                "imageScaleMode": constants.IMAGE_SCALE_MODE_MAINTAIN_ASPECT_RATIO,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var CopylblOption0b3d5b94677c44e = new kony.ui.Label({
                "centerY": "50%",
                "id": "CopylblOption0b3d5b94677c44e",
                "isVisible": true,
                "left": "15dp",
                "skin": "sknlblLatoBold35475f14px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.permission.Edit\")",
                "top": "4dp",
                "width": "40px",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            CopyflxOption0bc20c8a8030749.add(CopyimgOption0b58a89219f314f, CopylblOption0b3d5b94677c44e);
            var CopyflxOption0a3abb187c4704d = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "35dp",
                "id": "CopyflxOption0a3abb187c4704d",
                "isVisible": false,
                "layoutType": kony.flex.FLOW_HORIZONTAL,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0dp",
                "width": "100%",
                "zIndex": 1
            }, {}, {});
            CopyflxOption0a3abb187c4704d.setDefaultUnit(kony.flex.DP);
            var CopyimgOption0g7346328c2894d = new kony.ui.Image2({
                "centerY": "50%",
                "height": "15dp",
                "id": "CopyimgOption0g7346328c2894d",
                "isVisible": true,
                "left": "15dp",
                "skin": "slImage",
                "src": "imagedrag.png",
                "width": "15dp",
                "zIndex": 1
            }, {
                "imageScaleMode": constants.IMAGE_SCALE_MODE_MAINTAIN_ASPECT_RATIO,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var CopylblOption0e4aa6e320a224b = new kony.ui.Label({
                "centerY": "50%",
                "id": "CopylblOption0e4aa6e320a224b",
                "isVisible": true,
                "left": "15dp",
                "skin": "sknlblLatoBold35475f14px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.permission.Suspend\")",
                "top": "4dp",
                "width": "60px",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            CopyflxOption0a3abb187c4704d.add(CopyimgOption0g7346328c2894d, CopylblOption0e4aa6e320a224b);
            var CopyflxOption0e8d2e0a8b51446 = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "35dp",
                "id": "CopyflxOption0e8d2e0a8b51446",
                "isVisible": true,
                "layoutType": kony.flex.FLOW_HORIZONTAL,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0dp",
                "width": "100%",
                "zIndex": 1
            }, {}, {});
            CopyflxOption0e8d2e0a8b51446.setDefaultUnit(kony.flex.DP);
            var CopyimgOption0j2dc3599ac654d = new kony.ui.Image2({
                "centerY": "50%",
                "height": "15dp",
                "id": "CopyimgOption0j2dc3599ac654d",
                "isVisible": true,
                "left": "15dp",
                "skin": "slImage",
                "src": "deactive_2x.png",
                "width": "15dp",
                "zIndex": 1
            }, {
                "imageScaleMode": constants.IMAGE_SCALE_MODE_MAINTAIN_ASPECT_RATIO,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var CopylblOption0d9aafa510e7f4c = new kony.ui.Label({
                "centerY": "50%",
                "id": "CopylblOption0d9aafa510e7f4c",
                "isVisible": true,
                "left": "15dp",
                "skin": "sknlblLatoBold35475f14px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.permission.Deactivate\")",
                "top": "4dp",
                "width": "70px",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            CopyflxOption0e8d2e0a8b51446.add(CopyimgOption0j2dc3599ac654d, CopylblOption0d9aafa510e7f4c);
            flexOptions.add(CopylblDescription0a252259b5c5b47, CopylblRoles0f4ed925d516743, CopylblPremissions0f8ff66e806ad46, CopyflxSeperator0f47be42d7a5845, CopyflxOption0f482b0cd04b74b, CopyflxOption0bc20c8a8030749, CopyflxOption0a3abb187c4704d, CopyflxOption0e8d2e0a8b51446);
            flxSegmentUsers1.add(segusers1, flexOptions);
            flxSearch1.add(flxSearchHeading, flxSegmentUsers1);
            flxAdvancedSearch.add(flxSearch1);
            var flxLoading = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "100%",
                "id": "flxLoading",
                "isVisible": false,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "sknLoadingBlur",
                "top": "0dp",
                "width": "100%",
                "zIndex": 10
            }, {}, {});
            flxLoading.setDefaultUnit(kony.flex.DP);
            var flxImageContainer = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "centerX": "50%",
                "centerY": "50%",
                "clipBounds": true,
                "height": "75px",
                "id": "flxImageContainer",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "isModalContainer": false,
                "skin": "slFbox",
                "width": "75px",
                "zIndex": 1
            }, {}, {});
            flxImageContainer.setDefaultUnit(kony.flex.DP);
            var imgLoading = new kony.ui.Image2({
                "centerX": "50%",
                "centerY": "50%",
                "height": "50px",
                "id": "imgLoading",
                "isVisible": true,
                "skin": "slImage",
                "src": "loadingscreenimage.gif",
                "width": "50px",
                "zIndex": 1
            }, {
                "imageScaleMode": constants.IMAGE_SCALE_MODE_MAINTAIN_ASPECT_RATIO,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxImageContainer.add(imgLoading);
            flxLoading.add(flxImageContainer);
            flxRightPannel.add(flxMainHeader, flxHeaderDropdown, flxBreadCrumbs, flxImportDetails, flxScrollMainContent, flxToastMessage, flxToastMessageWithLink, flxAdvancedSearch, flxLoading);
            flxMain.add(flxLeftPannel, flxRightPannel);
            var flxImportCancelPopup = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "100%",
                "id": "flxImportCancelPopup",
                "isVisible": false,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "masterType": constants.MASTER_TYPE_USERWIDGET,
                "isModalContainer": false,
                "skin": "flxPopupTrans0ba858b5d5d304e",
                "top": "0dp",
                "width": "100%",
                "zIndex": 10
            }, {}, {});
            flxImportCancelPopup.setDefaultUnit(kony.flex.DP);
            var popUpImportCancel = new com.adminConsole.common.popUp({
                "height": "100%",
                "id": "popUpImportCancel",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "masterType": constants.MASTER_TYPE_DEFAULT,
                "isModalContainer": false,
                "skin": "sknflxBg000000Op50",
                "top": "0dp",
                "width": "100%",
                "zIndex": 1,
                "overrides": {
                    "btnPopUpCancel": {
                        "right": "20px",
                        "text": "NO, LEAVE AS IS",
                        "width": kony.flex.USE_PREFFERED_SIZE
                    },
                    "btnPopUpDelete": {
                        "i18n_text": "kony.i18n.getLocalizedString(\"i18n.PopUp.YesDeactivate\")",
                        "width": kony.flex.USE_PREFFERED_SIZE
                    },
                    "flxPopupHeader": {
                        "layoutType": kony.flex.FREE_FORM
                    },
                    "lblPopUpMainMessage": {
                        "text": "Cancel file upload ?",
                        "top": "40px"
                    },
                    "popUp": {
                        "left": "0dp",
                        "top": "0dp"
                    },
                    "rtxPopUpDisclaimer": {
                        "bottom": "30dp",
                        "right": "viz.val_cleared",
                        "text": "Are you sure to cancel this Privacy Policy?<br><br>\n\nIf you cancel it, the branch list in the file does not get uploaded to the application.",
                        "top": "90px",
                        "width": "93%"
                    }
                }
            }, {
                "overrides": {}
            }, {
                "overrides": {}
            });
            flxImportCancelPopup.add(popUpImportCancel);
            var flxImportcsv = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "100%",
                "id": "flxImportcsv",
                "isVisible": false,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "masterType": constants.MASTER_TYPE_USERWIDGET,
                "isModalContainer": false,
                "skin": "flxPopupTrans0ba858b5d5d304e",
                "top": "0dp",
                "width": "100%",
                "zIndex": 10
            }, {}, {});
            flxImportcsv.setDefaultUnit(kony.flex.DP);
            var uploadfilePopup = new com.adminConsole.locations.uploadfilePopup({
                "height": "100%",
                "id": "uploadfilePopup",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "masterType": constants.MASTER_TYPE_DEFAULT,
                "isModalContainer": false,
                "skin": "sknflxBg000000Op50",
                "top": "0dp",
                "width": "100%",
                "zIndex": 1,
                "overrides": {
                    "flxPopUp": {
                        "top": "200px"
                    }
                }
            }, {
                "overrides": {}
            }, {
                "overrides": {}
            });
            flxImportcsv.add(uploadfilePopup);
            var flxStatusChangePopUp = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "100%",
                "id": "flxStatusChangePopUp",
                "isVisible": false,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "masterType": constants.MASTER_TYPE_USERWIDGET,
                "isModalContainer": false,
                "skin": "flxPopupTrans0ba858b5d5d304e",
                "top": "0dp",
                "width": "100%",
                "zIndex": 10
            }, {}, {});
            flxStatusChangePopUp.setDefaultUnit(kony.flex.DP);
            var popUpStatusChange = new com.adminConsole.common.popUp1({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "height": "100%",
                "id": "popUpStatusChange",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "masterType": constants.MASTER_TYPE_DEFAULT,
                "isModalContainer": false,
                "skin": "sknflxBg000000Op50",
                "top": "0dp",
                "width": "100%",
                "zIndex": 1,
                "overrides": {
                    "btnPopUpCancel": {
                        "i18n_text": "kony.i18n.getLocalizedString(\"i18n.PopUp.NoLeaveAsIS\")",
                        "minWidth": "81px",
                        "right": "20px"
                    },
                    "btnPopUpDelete": {
                        "i18n_text": "kony.i18n.getLocalizedString(\"i18n.PopUp.YesDeactivate\")",
                        "minWidth": "83px"
                    },
                    "popUp1": {
                        "bottom": "viz.val_cleared",
                        "centerX": "viz.val_cleared",
                        "centerY": "viz.val_cleared",
                        "height": "100%",
                        "left": "0dp",
                        "maxHeight": "viz.val_cleared",
                        "maxWidth": "viz.val_cleared",
                        "minHeight": "viz.val_cleared",
                        "minWidth": "viz.val_cleared",
                        "right": "viz.val_cleared",
                        "top": "0dp",
                        "width": "100%"
                    },
                    "rtxPopUpDisclaimer": {
                        "text": "Are you sure to deactivate the Branch?<br><br>If you deactivate it, the Branch does not appear for the customer.\n"
                    }
                }
            }, {
                "overrides": {}
            }, {
                "overrides": {}
            });
            flxStatusChangePopUp.add(popUpStatusChange);
            var flxSetOfflinePopUp = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "100%",
                "id": "flxSetOfflinePopUp",
                "isVisible": false,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "masterType": constants.MASTER_TYPE_USERWIDGET,
                "isModalContainer": false,
                "skin": "flxPopupTrans0ba858b5d5d304e",
                "top": "0dp",
                "width": "100%",
                "zIndex": 10
            }, {}, {});
            flxSetOfflinePopUp.setDefaultUnit(kony.flex.DP);
            var popUpSetOffline = new com.adminConsole.common.popUp1({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "height": "100%",
                "id": "popUpSetOffline",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "masterType": constants.MASTER_TYPE_DEFAULT,
                "isModalContainer": false,
                "skin": "sknflxBg000000Op50",
                "top": "0dp",
                "width": "100%",
                "zIndex": 1,
                "overrides": {
                    "btnPopUpCancel": {
                        "i18n_text": "kony.i18n.getLocalizedString(\"i18n.PopUp.NoLeaveAsIS\")",
                        "minWidth": "81px",
                        "right": "20px"
                    },
                    "btnPopUpDelete": {
                        "i18n_text": "kony.i18n.getLocalizedString(\"i18n.PopUp.YesDeactivate\")",
                        "minWidth": "83px"
                    },
                    "popUp1": {
                        "bottom": "viz.val_cleared",
                        "centerX": "viz.val_cleared",
                        "centerY": "viz.val_cleared",
                        "height": "100%",
                        "left": "0dp",
                        "maxHeight": "viz.val_cleared",
                        "maxWidth": "viz.val_cleared",
                        "minHeight": "viz.val_cleared",
                        "minWidth": "viz.val_cleared",
                        "right": "viz.val_cleared",
                        "top": "0dp",
                        "width": "100%"
                    },
                    "rtxPopUpDisclaimer": {
                        "text": "Do you want to set the branch to offline mode?<br><br>If you set it offline, the branch will not be operational for the customer."
                    }
                }
            }, {
                "overrides": {}
            }, {
                "overrides": {}
            });
            flxSetOfflinePopUp.add(popUpSetOffline);
            var flxEditCancelConfirmation = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "100%",
                "id": "flxEditCancelConfirmation",
                "isVisible": false,
                "layoutType": kony.flex.FREE_FORM,
                "left": "10dp",
                "masterType": constants.MASTER_TYPE_USERWIDGET,
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "10dp",
                "width": "100%",
                "zIndex": 5
            }, {}, {});
            flxEditCancelConfirmation.setDefaultUnit(kony.flex.DP);
            var popUpCancelEdits = new com.adminConsole.common.popUpCancelEdits({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "height": "100%",
                "id": "popUpCancelEdits",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "masterType": constants.MASTER_TYPE_DEFAULT,
                "isModalContainer": false,
                "skin": "sknflxBg000000Op50",
                "top": "0dp",
                "width": "100%",
                "zIndex": 1,
                "overrides": {
                    "btnPopUpCancel": {
                        "i18n_text": "kony.i18n.getLocalizedString(\"i8n.navigation.cancelButton\")"
                    },
                    "btnPopUpDelete": {
                        "i18n_text": "kony.i18n.getLocalizedString(\"i8n.navigation.okButton\")"
                    },
                    "lblPopUpMainMessage": {
                        "i18n_text": "kony.i18n.getLocalizedString(\"i8n.navigation.cancelChanges\")"
                    },
                    "popUpCancelEdits": {
                        "bottom": "viz.val_cleared",
                        "centerX": "viz.val_cleared",
                        "centerY": "viz.val_cleared",
                        "height": "100%",
                        "left": "0dp",
                        "maxHeight": "viz.val_cleared",
                        "maxWidth": "viz.val_cleared",
                        "minHeight": "viz.val_cleared",
                        "minWidth": "viz.val_cleared",
                        "right": "viz.val_cleared",
                        "top": "0dp",
                        "width": "100%"
                    },
                    "rtxPopUpDisclaimer": {
                        "i18n_text": "kony.i18n.getLocalizedString(\"i8n.navigation.cancelChangesBody\")"
                    }
                }
            }, {
                "overrides": {}
            }, {
                "overrides": {}
            });
            flxEditCancelConfirmation.add(popUpCancelEdits);
            this.add(flxMain, flxImportCancelPopup, flxImportcsv, flxStatusChangePopUp, flxSetOfflinePopUp, flxEditCancelConfirmation);
        };
        return [{
            "addWidgets": addWidgetsfrmLocations,
            "enabledForIdleTimeout": true,
            "id": "frmLocations",
            "layoutType": kony.flex.FREE_FORM,
            "needAppMenu": false,
            "preShow": function(eventobject) {
                controller.AS_Form_i7daec5864fd4074833c7a5a811d2bac(eventobject);
            },
            "skin": "slForm"
        }, {
            "displayOrientation": constants.FORM_DISPLAY_ORIENTATION_PORTRAIT,
            "layoutType": kony.flex.FREE_FORM,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, {
            "onDeviceBack": controller.AS_Form_cf96ed0ed0c34a3f9414e06002eb459b,
            "retainScrollPosition": false
        }]
    }
});