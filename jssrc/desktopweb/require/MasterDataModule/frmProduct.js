define("MasterDataModule/frmProduct", function() {
    return function(controller) {
        function addWidgetsfrmProduct() {
            this.setDefaultUnit(kony.flex.DP);
            var flxProduct = new kony.ui.FlexContainer({
                "clipBounds": true,
                "height": "100%",
                "id": "flxProduct",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "slFbox0b7aaa2e3bfa340",
                "top": "0dp",
                "width": "100%",
                "zIndex": 1
            }, {}, {});
            flxProduct.setDefaultUnit(kony.flex.DP);
            var flxLeftPannel = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "100%",
                "id": "flxLeftPannel",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "masterType": constants.MASTER_TYPE_USERWIDGET,
                "isModalContainer": false,
                "skin": "sknLeftPannel0bb79e4bcca8e45",
                "top": "0dp",
                "width": "305px",
                "zIndex": 5
            }, {}, {});
            flxLeftPannel.setDefaultUnit(kony.flex.DP);
            var leftMenuNew = new com.adminConsole.navigation.leftMenuNew({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "height": "100%",
                "id": "leftMenuNew",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "masterType": constants.MASTER_TYPE_DEFAULT,
                "isModalContainer": false,
                "skin": "sknFlxBg003E75Op100",
                "top": "0dp",
                "width": "100%",
                "overrides": {
                    "brandLogo": {
                        "src": "infinity_dbx_c360_logo_white_2x.png"
                    },
                    "imgBottomLogo": {
                        "src": "logo_white_2x.png"
                    },
                    "leftMenuNew": {
                        "bottom": "viz.val_cleared",
                        "centerX": "viz.val_cleared",
                        "centerY": "viz.val_cleared",
                        "height": "100%",
                        "left": "0dp",
                        "maxHeight": "viz.val_cleared",
                        "maxWidth": "viz.val_cleared",
                        "minHeight": "viz.val_cleared",
                        "minWidth": "viz.val_cleared",
                        "right": "viz.val_cleared",
                        "top": "0dp",
                        "width": "100%"
                    }
                }
            }, {
                "overrides": {}
            }, {
                "overrides": {}
            });
            flxLeftPannel.add(leftMenuNew);
            var flxRightPanel = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "100%",
                "id": "flxRightPanel",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "305px",
                "masterType": constants.MASTER_TYPE_USERWIDGET,
                "isModalContainer": false,
                "right": "0px",
                "skin": "slFbox0b7aaa2e3bfa340",
                "top": "0.00%",
                "zIndex": 1
            }, {}, {});
            flxRightPanel.setDefaultUnit(kony.flex.DP);
            var flxMainHeader = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "106dp",
                "id": "flxMainHeader",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0dp",
                "width": "100%",
                "zIndex": 1
            }, {}, {});
            flxMainHeader.setDefaultUnit(kony.flex.DP);
            var mainHeader = new com.adminConsole.header.mainHeader({
                "height": "106px",
                "id": "mainHeader",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0",
                "masterType": constants.MASTER_TYPE_DEFAULT,
                "isModalContainer": false,
                "skin": "CopyslFbox1",
                "top": "0",
                "width": "100%",
                "overrides": {
                    "btnAddNewOption": {
                        "text": "IMPORT"
                    },
                    "btnDropdownList": {
                        "i18n_text": "kony.i18n.getLocalizedString(\"i18n.mainHeader.DOWNLOADLIST\")"
                    },
                    "flxButtons": {
                        "centerY": "viz.val_cleared",
                        "isVisible": true,
                        "right": "100dp"
                    },
                    "imgLogout": {
                        "right": "0px",
                        "src": "img_logout.png"
                    },
                    "lblHeading": {
                        "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmProductController.Banking_Product_Management\")"
                    },
                    "lblUserName": {
                        "right": "25px",
                        "text": "Preetish",
                        "top": "viz.val_cleared"
                    },
                    "mainHeader": {
                        "left": "0",
                        "top": "0"
                    }
                }
            }, {
                "overrides": {}
            }, {
                "overrides": {}
            });
            var flxSettings = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "30dp",
                "id": "flxSettings",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "isModalContainer": false,
                "right": "50dp",
                "skin": "sknflxBorder485c75Radius20PxPointer",
                "top": "60dp",
                "width": "30dp",
                "zIndex": 2
            }, {}, {});
            flxSettings.setDefaultUnit(kony.flex.DP);
            var lblIconSettings = new kony.ui.Label({
                "centerX": "50%",
                "centerY": "50%",
                "height": "20dp",
                "id": "lblIconSettings",
                "isVisible": true,
                "skin": "sknLblIcomoon20px485c75",
                "text": "",
                "width": "20dp",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxSettings.add(lblIconSettings);
            flxMainHeader.add(mainHeader, flxSettings);
            var flxHeaderDropdown = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                "clipBounds": true,
                "id": "flxHeaderDropdown",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "isModalContainer": false,
                "right": "35dp",
                "skin": "slFbox",
                "top": "20dp",
                "width": "400dp",
                "zIndex": 8
            }, {}, {});
            flxHeaderDropdown.setDefaultUnit(kony.flex.DP);
            var dropdownMainHeader = new com.adminConsole.common.dropdownMainHeader({
                "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                "id": "dropdownMainHeader",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "masterType": constants.MASTER_TYPE_DEFAULT,
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0dp",
                "width": "100%",
                "overrides": {
                    "flxDropdown": {
                        "isVisible": false
                    },
                    "imgUpArrow": {
                        "src": "uparrow_2x.png"
                    }
                }
            }, {
                "overrides": {}
            }, {
                "overrides": {}
            });
            flxHeaderDropdown.add(dropdownMainHeader);
            var flxBreadCrumb = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "20px",
                "id": "flxBreadCrumb",
                "isVisible": false,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0px",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "107px",
                "width": "100%",
                "zIndex": 4
            }, {}, {});
            flxBreadCrumb.setDefaultUnit(kony.flex.DP);
            var breadcrumbs = new com.adminConsole.common.breadcrumbs({
                "bottom": "0px",
                "height": "20px",
                "id": "breadcrumbs",
                "isVisible": true,
                "layoutType": kony.flex.FLOW_HORIZONTAL,
                "left": "0px",
                "masterType": constants.MASTER_TYPE_DEFAULT,
                "isModalContainer": false,
                "right": "0px",
                "skin": "sknflxf5f6f8Op100",
                "top": "0px",
                "zIndex": 100,
                "overrides": {
                    "breadcrumbs": {
                        "bottom": "0px",
                        "left": "0px",
                        "right": "0px",
                        "top": "0px",
                        "width": "viz.val_cleared",
                        "zIndex": 100
                    },
                    "btnBackToMain": {
                        "centerY": "50%",
                        "left": "35px"
                    },
                    "imgBreadcrumbsDown": {
                        "src": "img_down_arrow.png"
                    },
                    "imgBreadcrumbsRight": {
                        "src": "img_breadcrumb_arrow.png"
                    },
                    "imgBreadcrumbsRight2": {
                        "src": "img_breadcrumb_arrow.png"
                    },
                    "imgBreadcrumbsRight3": {
                        "src": "img_breadcrumb_arrow.png"
                    },
                    "lblCurrentScreen": {
                        "width": kony.flex.USE_PREFFERED_SIZE
                    }
                }
            }, {
                "overrides": {}
            }, {
                "overrides": {}
            });
            flxBreadCrumb.add(breadcrumbs);
            flxRightPanel.add(flxMainHeader, flxHeaderDropdown, flxBreadCrumb);
            var flxLoading = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "100%",
                "id": "flxLoading",
                "isVisible": false,
                "layoutType": kony.flex.FREE_FORM,
                "left": "305dp",
                "isModalContainer": false,
                "right": "0dp",
                "skin": "sknLoadingBlur",
                "top": "0dp",
                "zIndex": 15
            }, {}, {});
            flxLoading.setDefaultUnit(kony.flex.DP);
            var flxImageContainer = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "centerX": "50%",
                "centerY": "50%",
                "clipBounds": true,
                "height": "75px",
                "id": "flxImageContainer",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "isModalContainer": false,
                "skin": "slFbox",
                "width": "75px",
                "zIndex": 1
            }, {}, {});
            flxImageContainer.setDefaultUnit(kony.flex.DP);
            var imgLoading = new kony.ui.Image2({
                "centerX": "50%",
                "centerY": "50%",
                "height": "50px",
                "id": "imgLoading",
                "isVisible": true,
                "skin": "slImage",
                "src": "loadingscreenimage.gif",
                "width": "50px",
                "zIndex": 1
            }, {
                "imageScaleMode": constants.IMAGE_SCALE_MODE_MAINTAIN_ASPECT_RATIO,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxImageContainer.add(imgLoading);
            flxLoading.add(flxImageContainer);
            var flxToastMessage = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "bottom": "0px",
                "clipBounds": true,
                "height": "70px",
                "id": "flxToastMessage",
                "isVisible": false,
                "layoutType": kony.flex.FREE_FORM,
                "left": "305px",
                "isModalContainer": false,
                "right": "0px",
                "skin": "slFbox",
                "zIndex": 12
            }, {}, {});
            flxToastMessage.setDefaultUnit(kony.flex.DP);
            var toastMessage = new com.adminConsole.common.toastMessage({
                "height": "70px",
                "id": "toastMessage",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "masterType": constants.MASTER_TYPE_DEFAULT,
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0dp",
                "width": "100%",
                "overrides": {}
            }, {
                "overrides": {}
            }, {
                "overrides": {}
            });
            flxToastMessage.add(toastMessage);
            var settingsMenu = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                "clipBounds": true,
                "id": "settingsMenu",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "isModalContainer": false,
                "right": "42px",
                "skin": "slFbox",
                "top": "90px",
                "width": "280px",
                "zIndex": 10
            }, {}, {});
            settingsMenu.setDefaultUnit(kony.flex.DP);
            var settingsMenuOptions = new com.adminConsole.adManagement.selectOptions({
                "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                "id": "settingsMenuOptions",
                "isVisible": true,
                "layoutType": kony.flex.FLOW_VERTICAL,
                "left": "0dp",
                "masterType": constants.MASTER_TYPE_DEFAULT,
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0dp",
                "width": "100%",
                "overrides": {
                    "flxDownArrowImage": {
                        "isVisible": false
                    },
                    "flxOption2": {
                        "isVisible": false
                    },
                    "flxOption3": {
                        "isVisible": false
                    },
                    "flxOption4": {
                        "isVisible": false
                    },
                    "fontIconOption1": {
                        "isVisible": false,
                        "text": ""
                    },
                    "fontIconOption2": {
                        "text": ""
                    },
                    "imgDownArrow": {
                        "src": "downarrow_2x.png"
                    },
                    "imgUpArrow": {
                        "src": "uparrow_2x.png"
                    },
                    "lblOption1": {
                        "text": "Manage Product Lines, Groups and Features",
                        "width": kony.flex.USE_PREFFERED_SIZE
                    },
                    "lblOption2": {
                        "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmAdManagement.Settings\")"
                    },
                    "lblSeperator": {
                        "isVisible": false
                    }
                }
            }, {
                "overrides": {}
            }, {
                "overrides": {}
            });
            settingsMenu.add(settingsMenuOptions);
            var flxViewProducts = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "bottom": "0px",
                "clipBounds": true,
                "id": "flxViewProducts",
                "isVisible": false,
                "layoutType": kony.flex.FREE_FORM,
                "left": "340px",
                "isModalContainer": false,
                "right": "35px",
                "skin": "slFbox",
                "top": "106px",
                "zIndex": 5
            }, {}, {});
            flxViewProducts.setDefaultUnit(kony.flex.DP);
            var viewProducts = new com.adminConsole.productManagement.viewProducts({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "height": "100%",
                "id": "viewProducts",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "masterType": constants.MASTER_TYPE_USERWIDGET,
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0dp",
                "width": "100%",
                "overrides": {
                    "viewProducts": {
                        "right": "viz.val_cleared",
                        "bottom": "viz.val_cleared",
                        "minWidth": "viz.val_cleared",
                        "minHeight": "viz.val_cleared",
                        "maxWidth": "viz.val_cleared",
                        "maxHeight": "viz.val_cleared",
                        "centerX": "viz.val_cleared",
                        "centerY": "viz.val_cleared"
                    }
                }
            }, {
                "overrides": {}
            }, {
                "overrides": {}
            });
            viewProducts.objSeviceName = "BankProductManagment";
            viewProducts.objSeviceName1 = "BankProductManagment";
            viewProducts.objSeviceName2 = "BankProductManagment";
            viewProducts.Label1 = "";
            viewProducts.SubHeaderSkin = "slFbox0b7aaa2e3bfa340";
            viewProducts.ProductListHeaderSkin = "sknlblLato696c7312px";
            viewProducts.LblProductSegmentSkin = "sknlblLatoBold35475f14px";
            viewProducts.OptionMenuSkin = "sknflxffffffop100dbdbe6Radius3px";
            viewProducts.FilterMenuSkin = "sknflxffffffop100dbdbe6Radius3px";
            viewProducts.TopDetailsBgSkin = "sknFlxBgFFFFFFBorE4E6ECsz1pxR3px";
            viewProducts.BottomDetailsBgSkin = "sknFlxBgFFFFFFBorE4E6ECsz1pxR3px";
            viewProducts.PopupOuterShadowBgSkin = "skn222b35";
            viewProducts.ProductListBgSkin = "sknflxffffffop100Bordercbcdd1Radius3px";
            viewProducts.operationName = "GetProducts";
            viewProducts.operationName1 = "UpdateProduct";
            viewProducts.objName2 = "product";
            viewProducts.Label2 = "";
            viewProducts.SearchContainerSkin = "sknflxd5d9ddop100";
            viewProducts.LblProductGroupTxt = "PRODUCT GROUP";
            viewProducts.IconStatusActiveSkin = "sknFontIconActivate";
            viewProducts.IconOptionMenuSkin = "sknFontIconOptionMenuRow";
            viewProducts.FilterMenuSearchBoxSkin = "skntbxffffffNoBorderlato35475f14px";
            viewProducts.ProductNameSkin = "sknLatoRegular192B4518px";
            viewProducts.TabSelectedSkin = "sknLblTabUtilActive";
            viewProducts.PopupBgSkin = "flxNoShadowNoBorder";
            viewProducts.objName = "product";
            viewProducts.objName1 = "product";
            viewProducts.operationName2 = "DeleteProduct";
            viewProducts.Label3 = "";
            viewProducts.SearchIconSkin = "sknFontIconSearchImg20px";
            viewProducts.IconProductGroup = "{ \"text\" : \"\\ue916\"}";
            viewProducts.IconStatusDeactiveSkin = "sknfontIconInactive";
            viewProducts.IconEdit = "{ \"text\" : \"\\ue91e\"}";
            viewProducts.FilterMenuSearchIconSkin = "sknFontIconSearchImg20px";
            viewProducts.ProductDetailHeaderSkin = "sknlblLato11px696C73";
            viewProducts.TabUnselectedSkin = "sknlblLatoRegular485c7512px";
            viewProducts.PopupTopBarSkin = "sknFlxInfoToastBg357C9ENoRadius";
            viewProducts.Label4 = "";
            viewProducts.SearchIconText = "{ \"text\" : \"\\ue923\"}";
            viewProducts.LblProductLineTxt = "PRODUCT LINE";
            viewProducts.IconStatusActiveDeactive = "{ \"text\" : \"\\ue921\"}";
            viewProducts.IconDelete = "{ \"text\" : \"\\ue91b\"}";
            viewProducts.FilterMenuCrossIconSkin = "sknFontIconSearchCross16px";
            viewProducts.ProductDetailSkin = "sknlbl485C75LatoSemiBold13px";
            viewProducts.Tab1Text = "PRODUCT FEATURES";
            viewProducts.PopupTopHeaderSeparatorSkin = "sknlblSeperatorD7D9E0";
            viewProducts.Label5 = "";
            viewProducts.CrossIconSkin = "sknFontIconSearchCross16px";
            viewProducts.IconProductLine = "{ \"text\" : \"\\ue916\"}";
            viewProducts.IconOptionSkin = "sknFontIconOptionMenu";
            viewProducts.IconActivate = "{ \"text\" : \"\\ue931\"}";
            viewProducts.FilterMenuSearchSeparatorSkin = "sknLble1e5edOp100";
            viewProducts.LblProductLineHeader = "PRODUCT LINE";
            viewProducts.Tab2Text = "IMAGE DETAILS";
            viewProducts.PopupCloseIconSkin = "sknfontIconListbox15px";
            viewProducts.Label6 = "";
            viewProducts.CrossIconText = "{ \"text\" : \"\\ue929\"}";
            viewProducts.LblProductStatusTxt = "STATUS";
            viewProducts.IconOption = "{ \"text\" : \"\\ue91f\"}";
            viewProducts.IconDeactivate = "{ \"text\" : \"\\ue91c\"}";
            viewProducts.FilterMenuSegLblSkin = "sknlblLatoBold35475f14px";
            viewProducts.LblProductGroupHeader = "PRODUCT GROUP";
            viewProducts.Tab3Text = "ADDITIONAL ATTRIBUTES";
            viewProducts.LblPopupReadMoreHeaderSkin = "sknLbl192b45LatoReg16px";
            viewProducts.Label7 = "";
            viewProducts.SearchTextBoxSkin = "skntbxffffffNoBorderlato35475f14px";
            viewProducts.IconProductStatus = "{ \"text\" : \"\\ue916\"}";
            viewProducts.SegRowSeparatorSkin = "sknlblSeperator";
            viewProducts.IconCopy = "{ \"text\" : \"\\ue907\"}";
            viewProducts.FilterMenuNoResultLblSkin = "sknLbl485C75LatoRegular13Px";
            viewProducts.LblProductRefHeader = "PRODUCT REFERENCE";
            viewProducts.SeparatorBottomDetailsSkin = "sknlblSeperator";
            viewProducts.LblAgreementTermsSkin = "sknLatoRegular192B4518px";
            viewProducts.Label8 = "";
            viewProducts.SearchTextBoxPlaceholder = "Search by Product Name, Group and Line";
            viewProducts.HeaderSeparatorSkin = "sknLblTableHeaderLine";
            viewProducts.SegNoResultLblSkin = "sknLbl485C75LatoRegular13Px";
            viewProducts.LblPurposeHeader = "PURPOSE";
            viewProducts.LblNoResultSkin = "sknLbl485C75LatoRegular13Px";
            viewProducts.LblAgreementTermsText = "AGREEMENT TO TERMS";
            viewProducts.Label9 = "";
            viewProducts.LblAvailableFromDateHeader = "AVAILABLE FROM DATE";
            viewProducts.NofeaturesText = "No features found.";
            viewProducts.LblReadMoreDetailsSkin = "sknLatoRegular192B4518px";
            viewProducts.LblProductNameTxt = "PRODUCT NAME";
            viewProducts.Label10 = "";
            viewProducts.LblAvailableToDateHeader = "AVAILABLE TO DATE";
            viewProducts.ImgHeaderSkin = "sknlblLato696c7311px";
            viewProducts.PopupButtonConatinerBgSkin = "sknflxBgE4E6EC";
            viewProducts.Label11 = "";
            viewProducts.ProductDescriptionHeaderSkin = "sknLbl192b45LatoReg16px";
            viewProducts.LblImgTypeHeader = "IMAGE TYPE";
            viewProducts.PopupButtonCloseSkin = "sknBtn003E75LatoRegular13pxFFFFFFRad20px";
            viewProducts.Label12 = "";
            viewProducts.LblProductDescriptionHeader = "Product Description";
            viewProducts.LblImgUrlHeader = "IMAGE URL";
            viewProducts.PopupButtonCloseHoverSkin = "sknBtn005198LatoRegular13pxFFFFFFRad20px";
            viewProducts.Label13 = "";
            viewProducts.IconDropdownSkin = "sknIcon6E7178Sz15px";
            viewProducts.SegImgLabelSkin = "sknLbl485C75LatoRegular13Px";
            viewProducts.PopupWarningTopBarSkin = "sknflxebb54cOp100";
            viewProducts.Label14 = "";
            viewProducts.IconDropdownCollapse = "{ \"text\" : \"\\ue922\"}";
            viewProducts.NoImagesText = "No images found.";
            viewProducts.PopupAlertTopBarSkin = "sknFlxBge61919";
            viewProducts.Label15 = "";
            viewProducts.IconDropdownExpand = "{ \"text\" : \"\\ue915\"}";
            viewProducts.AttributeHeaderSkin = "sknlblLato696c7311px";
            viewProducts.PopupMainHeaderSkin = "sknlblLatoBold35475f23px";
            viewProducts.LblDescriptionHeader = "DESCRIPTION";
            viewProducts.LblAttribute1Header = "ATTRIBUTE1";
            viewProducts.PopupDisclaimerSkin = "sknrtxLato0df8337c414274d";
            viewProducts.LblDetailedDescHeader = "DETAILED DESCRIPTION";
            viewProducts.LblAttribute2Header = "ATTRIBUTE2";
            viewProducts.PopupButtonNoSkin = "sknbtnf7f7faLatoRegular4f555dBorder1px8b96a5";
            viewProducts.LblNotesHeader = "NOTES";
            viewProducts.SegAttributeLabelSkin = "sknLbl485C75LatoRegular13Px";
            viewProducts.PopupButtonNoHoverSkin = "sknbtnffffffLatoRegular4f555dBorder1px485c75";
            viewProducts.LblDisclosureHeader = "DISCLOSURE";
            viewProducts.NoAttributesText = "No additional attributes found.";
            viewProducts.PopupButtonYesSkin = "sknBtn003E75LatoRegular13pxFFFFFFRad20px";
            viewProducts.LblTermsAndConditionsHeader = "TERMS AND CONDITIONS";
            viewProducts.PopupButtonYesHoverSkin = "sknBtn005198LatoRegular13pxFFFFFFRad20px";
            viewProducts.ReadMoreSkin = "sknLblLatoReg117eb013px";
            viewProducts.LblReadMore = "Read More";
            viewProducts.formUpdateUI = controller.AS_UWI_ae74786889a146a1a624b378a2439e37;
            flxViewProducts.add(viewProducts);
            var flxManageProducts = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "bottom": "0px",
                "clipBounds": true,
                "id": "flxManageProducts",
                "isVisible": false,
                "layoutType": kony.flex.FREE_FORM,
                "left": "340px",
                "isModalContainer": false,
                "right": "35px",
                "skin": "slFbox",
                "top": "150px",
                "zIndex": 5
            }, {}, {});
            flxManageProducts.setDefaultUnit(kony.flex.DP);
            var manageProducts = new com.adminConsole.productManagement.manageProducts({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "height": "100%",
                "id": "manageProducts",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "masterType": constants.MASTER_TYPE_USERWIDGET,
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0dp",
                "width": "100%",
                "overrides": {
                    "manageProducts": {
                        "right": "viz.val_cleared",
                        "bottom": "viz.val_cleared",
                        "minWidth": "viz.val_cleared",
                        "minHeight": "viz.val_cleared",
                        "maxWidth": "viz.val_cleared",
                        "maxHeight": "viz.val_cleared",
                        "centerX": "viz.val_cleared",
                        "centerY": "viz.val_cleared"
                    }
                }
            }, {
                "overrides": {}
            }, {
                "overrides": {}
            });
            manageProducts.objSeviceName = "BankProductManagment";
            manageProducts.objSeviceName1 = "BankProductManagment";
            manageProducts.objSeviceName2 = "BankProductManagment";
            manageProducts.objSeviceName3 = "BankProductManagment";
            manageProducts.objSeviceName4 = "BankProductManagment";
            manageProducts.objSeviceName5 = "BankProductManagment";
            manageProducts.objSeviceName6 = "BankProductManagment";
            manageProducts.objSeviceName7 = "BankProductManagment";
            manageProducts.objSeviceName8 = "BankProductManagment";
            manageProducts.ProductTabSkinOnSelect = "sknBorderTop006CCA3px";
            manageProducts.ProductLineTopHeaderTxt = "Product Lines";
            manageProducts.AddLineProductBtnTxt = "Add Product Line";
            manageProducts.flxSearchBoxSkin = "sknflxd5d9ddop100";
            manageProducts.LblSegmentHeaderSkin = "sknlblLato696c7311px";
            manageProducts.PopupDetailsEditBtnTxt = "i18n.common.Edit";
            manageProducts.PopupAddProductLineTopHeaderTxt = "Add Product Line";
            manageProducts.PopupAddProductGrpTopHeaderTxt = "Add Product Group";
            manageProducts.PopupAddFeatureTopHeaderTxt = "Add Product Features";
            manageProducts.MainPageBgSkin = "sknFlxBgFFFFFFBorE4E6ECsz1pxR3px";
            manageProducts.objName = "product";
            manageProducts.objName1 = "product";
            manageProducts.objName2 = "product";
            manageProducts.operationName3 = "GetProductGroups";
            manageProducts.objName4 = "product";
            manageProducts.objName5 = "product";
            manageProducts.objName6 = "product";
            manageProducts.objName7 = "product";
            manageProducts.objName8 = "product";
            manageProducts.ProductTabSkinUnSelect = "sknBorderE4E6EC1px";
            manageProducts.ProductGroupTopHeaderTxt = "Product Groups";
            manageProducts.AddGroupProductBtnTxt = "Add Product Group";
            manageProducts.SeachTxtBoxSkin = "skntbxffffffNoBorderlato35475f14px";
            manageProducts.IconSortSkin = "sknIcon15px";
            manageProducts.PopupContinerTranslucentBgSkin = "skn222b35";
            manageProducts.PopupEditProductLineTopHeaderTxt = "Edit Product Line";
            manageProducts.PopupEditProductGrpTopHeaderTxt = "Edit Product Group";
            manageProducts.PopupEditFeatureTopHeaderTxt = "Edit Product Features";
            manageProducts.operationName = "GetProductLines";
            manageProducts.operationName1 = "CreateProductLine";
            manageProducts.operationName2 = "UpdateProductLine";
            manageProducts.objName3 = "product";
            manageProducts.operationName4 = "CreateProductGroup";
            manageProducts.operationName5 = "UpdateProductGroup";
            manageProducts.operationName6 = "GetFeatures";
            manageProducts.operationName7 = "CreateFeature";
            manageProducts.operationName8 = "UpdateFeature";
            manageProducts.ProductTabLblNameSkin = "skn485C75LatoRegular11Px";
            manageProducts.ProductFeatureTopHeaderTxt = "Product Features";
            manageProducts.AddFeatureProductBtnTxt = "Add Product Feature";
            manageProducts.IconSearchTxt = "{ \"text\" : \"\" }";
            manageProducts.IconSortTxt = "{ \"text\" : \"\" }";
            manageProducts.PopupMainBgSkin = "flxNoShadowNoBorder";
            manageProducts.PopupAddProductLineBtnWidth = "180px";
            manageProducts.PopupLblAssociatedProductLineNameTxt = "Associated Product Line";
            manageProducts.PopupLblFeatureNameTxt = "Feature Name";
            manageProducts.PopupLblProductLineNameTxt = "Product Line Name";
            manageProducts.ProductTabLblCountSkin = "sknLbl485c75LatoBold13px";
            manageProducts.ProductTopHeaderSkin = "sknLatoRegular192B4516px";
            manageProducts.AddProductBtnSkin = "sknbtnf7f7faLatoReg13px485c75Rad20px";
            manageProducts.IconSearchClearTxt = "{ \"text\" : \"\" }";
            manageProducts.LblSegmentHeader1Txt = "PRODUCT LINE";
            manageProducts.PopupTopBarSkin = "sknFlxInfoToastBg357C9ENoRadius";
            manageProducts.PopupLblProductGrpNameTxt = "Product Group Name";
            manageProducts.PopupLblFeatureGrpTxt = "Feature Group";
            manageProducts.PopupLblProductLineRefTxt = "Product Line Reference";
            manageProducts.ProductLineTabLblNameTxt = "Product Lines";
            manageProducts.AddLineProductBtnWidth = "141px";
            manageProducts.IconSearchSkin = "sknfontIconSearch";
            manageProducts.LblSegmentHeader2Txt = "PRODUCT REFERENCE";
            manageProducts.PopupCloseIconSkin = "sknfontIconListbox15px";
            manageProducts.PopupLblProductGrpRefTxt = "Product Group Reference";
            manageProducts.PopupLblFeatureTypeTxt = "Feature Type";
            manageProducts.PopupTextBoxProductLineNamePlaceholderTxt = "Product Line Name";
            manageProducts.ProductGroupTabLblNameTxt = "Product Groups";
            manageProducts.AddGroupProductBtnWidth = "151px";
            manageProducts.SeachTxtBoxPlaceholder1Txt = "Search by product line";
            manageProducts.LblSegmentHeader3Txt = "PRODUCT GROUP";
            manageProducts.PopupCloseIconTxt = "{ \"text\" : \"\" }";
            manageProducts.PopupLblProductGrpDescTxt = "i18n.common.description";
            manageProducts.PopupLblFeatureDescTxt = "i18n.common.description";
            manageProducts.PopupTextBoxProductLineRefPlaceholderTxt = "Product Line Reference";
            manageProducts.ProductFeatureTabLblNameTxt = "Product Features";
            manageProducts.AddFeatureProductBtnWidth = "165px";
            manageProducts.SeachTxtBoxPlaceholder2Txt = "Search by product group";
            manageProducts.LblSegmentHeader4Txt = "GROUP REFERENCE";
            manageProducts.PopupCancelBtnSkin = "sknbtnf7f7faLatoRegular4f555dBorder1px8b96a5";
            manageProducts.PopupLblProductGrpDetailedDescTxt = "Detailed Description ";
            manageProducts.PopupTextBoxFeatureNamePlaceholderTxt = "Feature Name";
            manageProducts.PopupErrMsgProductLineNameTxt = "Please enter product line name";
            manageProducts.SeachTxtBoxPlaceholder3Txt = "Search by product feature";
            manageProducts.LblSegmentHeader5Txt = "PRODUCT LINE";
            manageProducts.PopupCancelBtnWidth = "110px";
            manageProducts.PopupLblImgTypeTxt = "Image Type";
            manageProducts.PopupTextBoxFeatureGrpPlaceholderTxt = "Feature Group";
            manageProducts.PopupErrMsgProductLineRefTxt = "Please enter product line reference";
            manageProducts.LblSegmentHeader6Txt = "FEATURE NAME";
            manageProducts.PopupCancelBtnTxt = "i18n.permission.CANCEL";
            manageProducts.PopupLblImgUrlTxt = "Image URL";
            manageProducts.PopupTextBoxFeatureTypePlaceholderTxt = "Feature Type";
            manageProducts.flxSearchBoxOnFocusSkin = "slFbox0ebc847fa67a243Search";
            manageProducts.PopupAddProductLineBtnTxt = "ADD PRODUCT LINE";
            manageProducts.LblSegmentHeader7Txt = "FEATURE GROUP";
            manageProducts.PopupSaveBtnSkin = "sknBtn003E75LatoRegular13pxFFFFFFRad20px";
            manageProducts.PopupTextBoxProductGrpNamePlaceholderTxt = "Product Group Name";
            manageProducts.PopupTextBoxFeatureDescPlaceholderTxt = "i18n.common.description";
            manageProducts.PopupEditProductLineBtnTxt = "UPDATE";
            manageProducts.LblSegmentHeader8Txt = "FEATURE TYPE";
            manageProducts.PopupTopHeaderSkin = "sknLbl192b45LatoReg16px";
            manageProducts.PopupListBoxProductLinePlaceholderTxt = "Select Product Line";
            manageProducts.PopupErrMsgFeatureNameTxt = "Enter Feature Name";
            manageProducts.LblSegmentSkin = "sknLbl485C75LatoRegular13Px";
            manageProducts.PopupTextFieldCountSkin = "sknLblLato485c7513px";
            manageProducts.PopupEditProductLineBtnWidth = "110px";
            manageProducts.PopupTextBoxProductGrpRefPlaceholderTxt = "Product Group Reference";
            manageProducts.PopupErrMsgFeatureGrpTxt = "Enter Feature Group ";
            manageProducts.HeaderSeparatorSkin = "sknLblSeparator696C73";
            manageProducts.PopupFieldHeaderSkin = "sknLblLato485c7513px";
            manageProducts.PopupTextBoxProductGrpDescPlaceholderTxt = "i18n.common.description";
            manageProducts.PopupErrMsgFeatureTypeTxt = "Enter Feature Type";
            manageProducts.SegmentSeparatorSkin = "sknlblSeperatorD7D9E0";
            manageProducts.PopupTextBoxSkin = "txtD7d9e0";
            manageProducts.PopupTextBoxProductGrpDetailedDescPlaceholderTxt = "Detailed Description ";
            manageProducts.PopupErrMsgFeatureDescTxt = "i18n.ProfileManagement.errMsgFieldCantEmpty";
            manageProducts.SegmentEditIconSkin = "sknIcon20px";
            manageProducts.PopupTextAreaSkin = "skntxtAread7d9e0";
            manageProducts.PopupListBoxImgTypePlaceholderTxt = "Select Image Type";
            manageProducts.PopupAddFeatureBtnTxt = "ADD FEATURE";
            manageProducts.SegmentEditIconTxt = "{ \"text\" : \"\\ue91e\"}";
            manageProducts.PopupListBoxSkin = "sknLbxborderd7d9e03pxradius";
            manageProducts.PopupTextBoxImgUrlPlaceholderTxt = "Image URL";
            manageProducts.PopupEditFeatureBtnTxt = "UPDATE & SAVE";
            manageProducts.PopupTextBoxErrSkin = "skinredbg";
            manageProducts.PopupLblAddImgFieldTxt = "+ Add Image";
            manageProducts.PopupAddFeatureBtnWidth = "150px";
            manageProducts.PopupTextAreaErrSkin = "sknTxtError";
            manageProducts.PopupLblAddImgFieldSkin = "sknLblLatoReg117eb013px";
            manageProducts.PopupEditFeatureBtnWidth = "160px";
            manageProducts.PopupListBoxErrSkin = "sknlbxeb30173px";
            manageProducts.PopupErrMsgProductGrpNameTxt = "Enter Product Group Name";
            manageProducts.ErrMsgTextSkin = "sknlblError";
            manageProducts.PopupErrMsgAssociatedProductLineNameTxt = "Select Associated Product Line";
            manageProducts.ErrMsgIconSkin = "sknErrorIcon";
            manageProducts.PopupErrMsgProductGrpRefTxt = "Enter Product Group Reference";
            manageProducts.ErrMsgIconTxt = "{ \"text\" : \"\"}";
            manageProducts.PopupErrMsgProductGrpDescTxt = "i18n.ProfileManagement.errMsgFieldCantEmpty";
            manageProducts.PopupViewTopHeaderSkin = "sknLbl192b45LatoReg16px";
            manageProducts.PopupErrMsgProductGrpDetailedDescTxt = "i18n.ProfileManagement.errMsgFieldCantEmpty";
            manageProducts.PopupViewTopHeaderSeparatorSkin = "sknlblSeperatorD7D9E0";
            manageProducts.PopupErrMsgImgTypeTxt = "i18n.ProfileManagement.errMsgFieldCantEmpty";
            manageProducts.PopupViewDetailsHeaderSkin = "sknlblLato696c7311px";
            manageProducts.PopupErrMsgImgUrlTxt = "i18n.ProfileManagement.errMsgFieldCantEmpty";
            manageProducts.PopupViewDetailsSkin = "sknLblLato485c7513px";
            manageProducts.AddImgFieldDeleteIconTxt = "{ \"text\" : \"\" }";
            manageProducts.PopupDetailsEditBtnSkin = "sknbtnf7f7faLatoReg13px485c75Rad20px";
            manageProducts.AddImgFieldDeleteIconSkin = "sknIcon20px";
            manageProducts.PopupAddProductGrpBtnTxt = "ADD PRODUCT GROUP";
            manageProducts.PopupCancelBtnSkinHover = "sknbtnffffffLatoRegular4f555dBorder1px485c75";
            manageProducts.PopupEditProductGrpBtnTxt = "UPDATE & SAVE";
            manageProducts.PopupSaveBtnSkinHover = "sknBtn005198LatoRegular13pxFFFFFFRad20px";
            manageProducts.PopupAddProductGrpBtnWidth = "210px";
            manageProducts.PopupTextBoxDisabledSkin = "txtD7d9e0disabledf3f3f3";
            manageProducts.PopupEditProductGrpBtnWidth = "160px";
            manageProducts.PopupListBoxDisabledSkin = "sknLbxborderd7d9e03pxradiusF3F3F3Disabled";
            manageProducts.ImgDetailsViewBgSkin = "sknFlxBgFFFFFFBorE4E6ECsz1pxR3px";
            manageProducts.ImageTypeDetailsJsonData = "{\"data\":[{\"imageType\":\"Banner\",\"height\":1366,\"width\":556,\"lstBoxText\":\"Banner (1366 X 556 px)\"},{\"imageType\":\"Icon\",\"height\":40,\"width\":40,\"lstBoxText\":\"Icon (40 X 40 px)\"},{\"imageType\":\"Banner_Large\",\"height\":2732,\"width\":1112,\"lstBoxText\":\"Large Banner (2732 X 1112 px)\"}]}";
            manageProducts.formUpdateUI = controller.AS_UWI_ja28acb6354346188ed99194d3c7643b;
            flxManageProducts.add(manageProducts);
            var flxAddProducts = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "bottom": "0px",
                "clipBounds": true,
                "id": "flxAddProducts",
                "isVisible": false,
                "layoutType": kony.flex.FREE_FORM,
                "left": "340px",
                "isModalContainer": false,
                "right": "35px",
                "skin": "slFbox",
                "top": "150px",
                "zIndex": 6
            }, {}, {});
            flxAddProducts.setDefaultUnit(kony.flex.DP);
            var addProduct = new com.adminConsole.Products.addProduct({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "height": "100%",
                "id": "addProduct",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "masterType": constants.MASTER_TYPE_USERWIDGET,
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0dp",
                "width": "100%",
                "overrides": {
                    "addProduct": {
                        "right": "viz.val_cleared",
                        "bottom": "viz.val_cleared",
                        "minWidth": "viz.val_cleared",
                        "minHeight": "viz.val_cleared",
                        "maxWidth": "viz.val_cleared",
                        "maxHeight": "viz.val_cleared",
                        "centerX": "viz.val_cleared",
                        "centerY": "viz.val_cleared"
                    }
                }
            }, {
                "overrides": {}
            }, {
                "overrides": {}
            });
            addProduct.objectServiceName1 = "BankProductManagment";
            addProduct.objectServiceName2 = "BankProductManagment";
            addProduct.objectServiceName3 = "BankProductManagment";
            addProduct.objectServiceName4 = "BankProductManagment";
            addProduct.label1Txt = "";
            addProduct.productLineLblTxt = "i18n.products.Product_Line";
            addProduct.productPurposes = "{\"values\":[\"Onboarding\",\"Campaigns\"]}";
            addProduct.objectServiceName5 = "BankProductManagment";
            addProduct.descLblTxt = "i18n.products.Description_Optional";
            addProduct.featureHeadingLblTxt = "i18n.products.Features";
            addProduct.featureAddPopupHeadingLblTxt = "i18n.products.Add_Feature";
            addProduct.featureViewEditBtnTxt = "i18n.products.Edit";
            addProduct.imageTypeLblTxt = "i18n.products.Image_Type";
            addProduct.attributeKeyLblTxt = "i18n.products.Attribute";
            addProduct.option1LblTxt = "i18n.products.Product_Details_CAPS";
            addProduct.leftVBarBgSkin = "sknflxBgF9F9F9Border1pxE4E6EC";
            addProduct.commonLabelSkin = "sknLbl485C75LatoRegular13Px";
            addProduct.prodPurposeSkin = "sknflxffffffop100Bordercbcdd1Radius3px";
            addProduct.screenHeaderLblSkin = "sknLbl192B45LatoRegular14px";
            addProduct.popupCmnBgSkin = "skn222b35";
            addProduct.optionDetailsSegBgSkin = "sknflxf5f6f8Border1px4pxRadius";
            addProduct.lblKeySkin = "sknlblLato696c7311px";
            addProduct.addAttrTextBoxSkin = "sknTbxBgFFFFFFBrD7D9E01pxR3px";
            addProduct.deleteIcon = "{\"text\":\"\\ue91b\"}";
            addProduct.objectName1 = "product";
            addProduct.objectName2 = "product";
            addProduct.objectName3 = "product";
            addProduct.objectName4 = "product";
            addProduct.placeholder1Txt = "";
            addProduct.productLineDropdownPlaceholderTxt = "i18n.products.Select_Product_Line";
            addProduct.optionDisplayType = "{\"values\":[{\"displayValue\":\"Single Selection\",\"backendValue\":\"SingleSelection\"},{\"displayValue\":\"Check Box\",\"backendValue\":\"CheckBox\"}]}";
            addProduct.objectName5 = "product";
            addProduct.descPlaceholderTxt = "i18n.products.Description";
            addProduct.addFeatureBtnTxt = "i18n.products.Add_Feature";
            addProduct.featureTypeLblTxt = "i18n.products.Feature_Type";
            addProduct.featureViewProductGroupLblTxt = "i18n.products.Feature_Group_CAPS";
            addProduct.imageTypeDropdownPlaceholderTxt = "i18n.products.Select_Image_Type";
            addProduct.attributeKeyPlaceholderTxt = "i18n.products.Attribute";
            addProduct.subOption1LblTxt = "i18n.products.Details";
            addProduct.leftVBarBtnUnselectedSkin = "sknLbl485C75LatoRegular12Px";
            addProduct.tbxNormalSkin = "txtD7d9e0";
            addProduct.prodPurposeDropdownIConSkin = "sknFontIconLimits13Px";
            addProduct.addFeatureBtnSkin = "sknbtnf7f7faLatoRegular12Px485c75Border1px485C75Radius20px";
            addProduct.popupCmnTopBarSkin = "sknFlxBg4A77A0";
            addProduct.bottomBarPopupBgSkin = "sknflxBgE4E6EC";
            addProduct.editIcon = "{\"text\":\"\\ue91e\"}";
            addProduct.operationName1 = "GetProductLines";
            addProduct.operationName2 = "GetProductGroups";
            addProduct.operationName3 = "GetFeatures";
            addProduct.operationName4 = "CreateProduct";
            addProduct.label2Txt = "";
            addProduct.productLineErrorMsgTxt = "i18n.products.Please_Select_Product_Line";
            addProduct.imageTypes = "{\"values\":[{\"imageTypeDisplay\":\"Banner\", \"imageTypeBackend\":\"Banner\", \"height\":\"556\", \"width\":\"1366\"}, {\"imageTypeDisplay\":\"Icon\", \"imageTypeBackend\":\"Icon\", \"height\":\"40\", \"width\":\"40\"}, {\"imageTypeDisplay\":\"Large Banner\", \"imageTypeBackend\":\"Banner_Large\", \"height\":\"1112\", \"width\":\"2732\"}]}";
            addProduct.operationName5 = "UpdateProduct";
            addProduct.detailedDescLblTxt = "i18n.products.Detailed Description_Optional";
            addProduct.noFeatureDescTxt = "i18n.products.Product_Features_can_be_added";
            addProduct.featureTypeDropdownPlaceholderTxt = "i18n.products.Feature_Type";
            addProduct.featureViewSequenceNumberLblTxt = "i18n.products.Sequence_Number_CAPS";
            addProduct.imageURLLblTxt = "i18n.products.Image_URL";
            addProduct.attributeValueLblTxt = "i18n.products.Value";
            addProduct.subOption2LblTxt = "i18n.products.Description";
            addProduct.leftVBarBtnSelectedSkin = "sknlblLatoBold485c7512px";
            addProduct.tbxErrorSkin = "skinredbg";
            addProduct.dateDisabledFlexSkin = "sknFlxbgF3F3F3bdrd7d9e0";
            addProduct.noFeatureScreenDescSkin = "sknLbl485C75LatoRegular13Px";
            addProduct.popupCmnHeadingLblSkin = "sknLatoRegular192B4516px";
            addProduct.selCheckboxIcon = "{\"text\":\"\\ue965\"}";
            addProduct.placeholder2Txt = "";
            addProduct.productGroupLblTxt = "i18n.products.Product_Group";
            addProduct.maxAddAttrAllowed = "10";
            addProduct.detailedDescPlaceholderTxt = "i18n.products.Detailed Description";
            addProduct.featureNameLblSegTxt = "i18n.products.Feature_Name";
            addProduct.featureTypeErrorMsgTxt = "i18n.products.Select_Feature_Type";
            addProduct.featureViewMandatoryLblTxt = "i18n.products.Mandatory_CAPS";
            addProduct.imageURLPlaceholderTxt = "i18n.products.Image_URL";
            addProduct.attributeValuePlaceholderTxt = "i18n.products.Value";
            addProduct.option2LblTxt = "i18n.products.Product_Features_CAPS";
            addProduct.leftVBarBtnHoverSkin = "sknlblLatoBoldCursorHover485c7512px";
            addProduct.tbxDisabledSkin = "txtD7d9e0disabledf3f3f3";
            addProduct.dateDisabledIconLblSkin = "sknLblIcomoon20px696c75";
            addProduct.segHeaderLblTxtSkin = "sknlblLato696c7312px";
            addProduct.unselCheckboxIcon = "{\"text\":\"\\ue966\"}";
            addProduct.label3Txt = "";
            addProduct.productGroupDropdownPlaceholderTxt = "i18n.products.Select_Product_Group";
            addProduct.notesLblTxt = "i18n.products.Notes_Optional";
            addProduct.featureGroupLblSegTxt = "i18n.products.Feature_Group";
            addProduct.featureNameLblTxt = "i18n.products.Feature_Name";
            addProduct.featureViewDefaultValueLblTxt = "i18n.products.Default_Value_CAPS";
            addProduct.addImageBtnTxt = "i18n.products.Plus_Add_Image";
            addProduct.addAttributeBtnTxt = "i18n.products.Plus_Add_Attribute";
            addProduct.option3LblTxt = "i18n.products.Image_Details_CAPS";
            addProduct.leftVBarOptionalLblSkin = "sknLblLato84939E12px";
            addProduct.tbxAreaNormalSkin = "skntxtAread7d9e0";
            addProduct.dateDisabledLblTxtSkin = "sknlblLato696c7313px";
            addProduct.tooltipInfoIcon = "{\"text\":\"\\ue94d\"}";
            addProduct.placeholder3Txt = "";
            addProduct.productGroupErrorMsgTxt = "i18n.products.Please_Select_Product_Group";
            addProduct.notesPlaceholderTxt = "i18n.products.Notes";
            addProduct.featureTypeLblSegTxt = "i18n.products.Feature_Type";
            addProduct.featureNamePlaceholderTxt = "i18n.products.Feature_Name";
            addProduct.featureViewDescValueLblTxt = "i18n.products.Description_CAPS";
            addProduct.option4LblTxt = "i18n.products.Additional_Attributes_CAPS";
            addProduct.leftVBarSeparatorSkin = "sknflxD5D9DD";
            addProduct.tbxAreaErrorSkin = "sknTxtError";
            addProduct.errorIcon = "{\"text\":\"\\ue94c\"}";
            addProduct.label4Txt = "";
            addProduct.productNameLblTxt = "i18n.products.Product_Name";
            addProduct.disclosureLblTxt = "i18n.products.Disclosure_Optional";
            addProduct.featureNameErrorMsgTxt = "i18n.products.Enter_Feature_Name";
            addProduct.featureViewOptionDetailsLblTxt = "i18n.products.Option_Details";
            addProduct.optionalFieldTxt = "i18n.products.Optional";
            addProduct.leftVBarCollapseIconSkin = "sknIcon485C7513px";
            addProduct.lstboxNormalSkin = "sknLbxborderd7d9e03pxradius";
            addProduct.sortUpDownIcon = "{\"text\":\"\\ue92b\"}";
            addProduct.placeholder4Txt = "";
            addProduct.productNamePlaceholderTxt = "i18n.products.Product_Name";
            addProduct.disclosurePlaceholderTxt = "i18n.products.Disclosure";
            addProduct.featureGroupLblTxt = "i18n.products.Feature_Group";
            addProduct.featureViewOptionDisplayTypeLblTxt = "i18n.products.Option_Display_Type_CAPS";
            addProduct.cancelBtnTxt = "i18n.products.Cancel_CAPS";
            addProduct.leftVBarRightArrowIconSkin = "sknIcon485C7513px";
            addProduct.lstboxErrorSkin = "redListBxSkin";
            addProduct.downArrowIcon = "{\"text\":\"\\ue920\"}";
            addProduct.label5Txt = "";
            addProduct.productNameErrorMsgTxt = "i18n.products.Product_Name_cannot_be_empty";
            addProduct.tandCLblTxt = "i18n.products.Terms and Conditions_Optional";
            addProduct.featureGroupPlaceholderTxt = "i18n.products.Feature_Group";
            addProduct.featureViewValueLblTxt = "i18n.products.Value_CAPS";
            addProduct.nextBtnTxt = "i18n.products.Next_CAPS";
            addProduct.bottomBarBgSkin = "sknFlxBgFFFFFFBorE4E6ECsz1pxR1px";
            addProduct.lstboxDisabledSkin = "sknLbxBGf3f3f3Radius3pxDisabled";
            addProduct.upArrowIcon = "{\"text\":\"\\ue92a\"}";
            addProduct.placeholder5Txt = "";
            addProduct.productRefLblTxt = "i18n.products.Product_Reference";
            addProduct.tandCPlaceholderTxt = "i18n.products.Terms and Conditions";
            addProduct.featureGroupErrorMsgTxt = "i18n.products.Enter_Feature_Group";
            addProduct.featureViewDescriptionLblTxt = "i18n.products.Description_CAPS";
            addProduct.addProductBtnTxt = "i18n.products.Add_Product_CAPS";
            addProduct.bottomBarCancelBtnSkin = "sknbtnf7f7faLatoRegular4f555dBorder1px8b96a5";
            addProduct.calendarNormalSkin = "sknFlxCalendar";
            addProduct.segMoreDetailsIcon = "{\"text\":\"\\ue91f\"}";
            addProduct.label6Txt = "";
            addProduct.productRefPlaceholderTxt = "i18n.products.Product_Reference";
            addProduct.featureDescLblTxt = "i18n.products.Description_Optional";
            addProduct.updateProductBtnTxt = "i18n.products.Update_CAPS";
            addProduct.bottomBarCancelBtnHoverSkin = "sknbtnffffffLatoRegular4f555dBorder1px485c75";
            addProduct.calendarErrorSkin = "sknFlxCalendarError";
            addProduct.menuRightArrowIcon = "{\"text\":\"\\ue918\"}";
            addProduct.notesToolTipTxt = "i18n.products.Notes_Tooltip";
            addProduct.placeholder6Txt = "";
            addProduct.productRefErrorMsgTxt = "i18n.products.Product_Reference_cannot_be_empty";
            addProduct.featureDescPlaceholderTxt = "i18n.products.Description";
            addProduct.bottomBarBtnLightSkin = "sknBtn4A77A0LatoRegular13pxFFFFFFRad20px";
            addProduct.commonRightBackgroundSkin = "sknFlxBgFFFFFFBore4e6ec3Px";
            addProduct.label7Txt = "";
            addProduct.startDateLblTxt = "i18n.products.Available_From_Date";
            addProduct.sequenceNumLblTxt = "i18n.products.Sequence_Number";
            addProduct.bottomBarBtnDarkSkin = "sknBtn003E75LatoRegular13pxFFFFFFRad20px";
            addProduct.errorMsgIconSkin = "sknErrorIcon";
            addProduct.placeholder7Txt = "";
            addProduct.startDateErrorMsgTxt = "i18n.products.Please_select_a_valid_date";
            addProduct.sequenceNumPlaceholderTxt = "i18n.products.Sequence_Number";
            addProduct.errorMsgTextSkin = "sknlblError";
            addProduct.label8Txt = "";
            addProduct.endDateLblTxt = "i18n.products.Available_To_Date";
            addProduct.mandatoryLblTxt = "i18n.products.Mandatory";
            addProduct.txtCounterLabelSkin = "sknlblLato696c7313px";
            addProduct.placeholder8Txt = "";
            addProduct.endDateErrorMsgTxt = "i18n.products.Please_select_a_valid_date";
            addProduct.optionDetailsSubHeadingLblTxt = "i18n.products.Option_Details";
            addProduct.segmentOptionsIconSkin = "sknFontIconOptionMenu";
            addProduct.label9Txt = "";
            addProduct.purposeLblTxt = "i18n.products.Purpose";
            addProduct.optionDisplayTypeLblTxt = "i18n.products.Option_Display_Type";
            addProduct.popupCloseCrossBtnSkin = "lblIconGrey";
            addProduct.placeholder9Txt = "";
            addProduct.purposeDropdownPlaceholderTxt = "i18n.products.Product_Purpose";
            addProduct.optionDisplayPlaceholderTxt = "i18n.products.Option_Display_Type";
            addProduct.AddSegmentRowBtnSkin = "sknBtnLato117eb013Px";
            addProduct.label10Txt = "";
            addProduct.purposeErrorMsgTxt = "i18n.products.Purpose_cannot_be_empty";
            addProduct.valueLblTxt = "i18n.products.Value";
            addProduct.sortconsSkin = "sknIcon15px";
            addProduct.deleteIconSkin = "sknIcon22px192b45";
            addProduct.placeholder10Txt = "";
            addProduct.optionDescLblTxt = "i18n.products.Description_Optional";
            addProduct.label11Txt = "";
            addProduct.optionDefaultValueLblTxt = "i18n.products.Default_Value";
            addProduct.checkboxSelIconSkin = "sknFontIconCheckBoxSelected";
            addProduct.placeholder11Txt = "";
            addProduct.optionValuePlaceholderTxt = "i18n.products.Value";
            addProduct.checkboxUnselIconSkin = "sknFontIconCheckBoxUnselected";
            addProduct.label12Txt = "";
            addProduct.optionDescPlaceholderTxt = "i18n.products.Description";
            addProduct.placeholder12Txt = "";
            addProduct.addValueBtnTxt = "i18n.products.Plus_Add_Value";
            addProduct.label13Txt = "";
            addProduct.featureCancelBtnTxt = "i18n.products.Cancel_CAPS";
            addProduct.placeholder13Txt = "";
            addProduct.featureSaveBtnTxt = "i18n.products.Save_and_Close_CAPS";
            addProduct.label14Txt = "";
            addProduct.featureUpdateBtnTxt = "i18n.products.Update_and_Close_CAPS";
            addProduct.placeholder14Txt = "";
            addProduct.sequenceNumErrorMsgTxt = "i18n.products.Sequence_Number_cannot_be_zero_or_empty";
            addProduct.label15Txt = "";
            addProduct.optionDisplayTypeErrorMsgTxt = "i18n.products.Select_Option_Display_Type";
            addProduct.placeholder15Txt = "";
            addProduct.optionDefaultValueErrorMsgTxt = "i18n.products.Default_Value_cannot_be_empty";
            addProduct.formUpdateUI = controller.AS_UWI_g753f0e2b31943238f8695f9d6f2d69d;
            addProduct.alignPopups = controller.AS_UWI_id8641937d8445d9a4fc2fedbebcade0;
            addProduct.closeAddProductComponent = controller.AS_UWI_h9c52059d9c64c4784053923c8a0cb46;
            flxAddProducts.add(addProduct);
            flxProduct.add(flxLeftPannel, flxRightPanel, flxLoading, flxToastMessage, settingsMenu, flxViewProducts, flxManageProducts, flxAddProducts);
            this.add(flxProduct);
        };
        return [{
            "addWidgets": addWidgetsfrmProduct,
            "enabledForIdleTimeout": true,
            "id": "frmProduct",
            "layoutType": kony.flex.FREE_FORM,
            "needAppMenu": false,
            "preShow": function(eventobject) {
                controller.AS_Form_d9031c26bdde47fe93cabcf46ab4e718(eventobject);
            },
            "skin": "slForm"
        }, {
            "displayOrientation": constants.FORM_DISPLAY_ORIENTATION_PORTRAIT,
            "layoutType": kony.flex.FREE_FORM,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, {
            "onDeviceBack": controller.AS_Form_i7e627b88d1d4832ad4472348ecd0e6c,
            "retainScrollPosition": false
        }]
    }
});