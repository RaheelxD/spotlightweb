define("MasterDataModule/frmCustomerCare", function() {
    return function(controller) {
        function addWidgetsfrmCustomerCare() {
            this.setDefaultUnit(kony.flex.DP);
            var flxMain = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "100%",
                "id": "flxMain",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "slFbox0b7aaa2e3bfa340",
                "top": "0dp",
                "width": "100%",
                "zIndex": 1
            }, {}, {});
            flxMain.setDefaultUnit(kony.flex.DP);
            var flxLeftPannel = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "100%",
                "id": "flxLeftPannel",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "sknLeftPannel0bb79e4bcca8e45",
                "top": "0dp",
                "width": "305px",
                "zIndex": 5
            }, {}, {});
            flxLeftPannel.setDefaultUnit(kony.flex.DP);
            var leftMenuNew = new com.adminConsole.navigation.leftMenuNew({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "height": "100%",
                "id": "leftMenuNew",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "masterType": constants.MASTER_TYPE_DEFAULT,
                "isModalContainer": false,
                "skin": "sknFlxBg003E75Op100",
                "top": "0dp",
                "width": "100%",
                "overrides": {
                    "leftMenuNew": {
                        "bottom": "viz.val_cleared",
                        "centerX": "viz.val_cleared",
                        "centerY": "viz.val_cleared",
                        "height": "100%",
                        "left": "0dp",
                        "maxHeight": "viz.val_cleared",
                        "maxWidth": "viz.val_cleared",
                        "minHeight": "viz.val_cleared",
                        "minWidth": "viz.val_cleared",
                        "right": "viz.val_cleared",
                        "top": "0dp",
                        "width": "100%"
                    }
                }
            }, {
                "overrides": {}
            }, {
                "overrides": {}
            });
            flxLeftPannel.add(leftMenuNew);
            var flxRightPannel = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "100%",
                "id": "flxRightPannel",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "305px",
                "isModalContainer": false,
                "right": 0,
                "skin": "slFbox0b7aaa2e3bfa340",
                "top": "0%",
                "zIndex": 5
            }, {}, {});
            flxRightPannel.setDefaultUnit(kony.flex.DP);
            var flxMainHeader = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "106dp",
                "id": "flxMainHeader",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0dp",
                "width": "100%",
                "zIndex": 1
            }, {}, {});
            flxMainHeader.setDefaultUnit(kony.flex.DP);
            var mainHeader = new com.adminConsole.header.mainHeader({
                "height": "106px",
                "id": "mainHeader",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0",
                "masterType": constants.MASTER_TYPE_DEFAULT,
                "isModalContainer": false,
                "skin": "CopyslFbox1",
                "top": "0",
                "width": "100%",
                "overrides": {
                    "btnAddNewOption": {
                        "right": "0dp",
                        "text": "ADD",
                        "width": kony.flex.USE_PREFFERED_SIZE
                    },
                    "flxHeaderUserOptions": {
                        "right": "35dp",
                        "width": "30%"
                    },
                    "imgLogout": {
                        "left": "viz.val_cleared",
                        "right": "0px",
                        "width": "18dp"
                    },
                    "lblHeading": {
                        "i18n_text": "kony.i18n.getLocalizedString(\"i18n.userwidgetmodel.lblManageCustomerCareInformation\")"
                    },
                    "lblUserName": {
                        "text": "Preetish"
                    },
                    "mainHeader": {
                        "left": "0",
                        "top": "0"
                    }
                }
            }, {
                "overrides": {}
            }, {
                "overrides": {}
            });
            flxMainHeader.add(mainHeader);
            var flxHeaderDropdown = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                "clipBounds": true,
                "id": "flxHeaderDropdown",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "isModalContainer": false,
                "right": "35dp",
                "skin": "slFbox",
                "top": "20dp",
                "width": "400dp",
                "zIndex": 5
            }, {}, {});
            flxHeaderDropdown.setDefaultUnit(kony.flex.DP);
            var dropdownMainHeader = new com.adminConsole.common.dropdownMainHeader({
                "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                "id": "dropdownMainHeader",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "masterType": constants.MASTER_TYPE_DEFAULT,
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0dp",
                "width": "100%",
                "overrides": {
                    "flxDropdown": {
                        "isVisible": false
                    },
                    "imgUpArrow": {
                        "src": "uparrow_2x.png"
                    }
                }
            }, {
                "overrides": {}
            }, {
                "overrides": {}
            });
            flxHeaderDropdown.add(dropdownMainHeader);
            var flxBreadCrumbs = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "30px",
                "id": "flxBreadCrumbs",
                "isVisible": false,
                "layoutType": kony.flex.FLOW_HORIZONTAL,
                "left": "0dp",
                "masterType": constants.MASTER_TYPE_USERWIDGET,
                "isModalContainer": false,
                "skin": "sknflxf5f6f8Op100",
                "top": "95px",
                "width": "100%",
                "zIndex": 1
            }, {}, {});
            flxBreadCrumbs.setDefaultUnit(kony.flex.DP);
            var breadcrumbs = new com.adminConsole.common.breadcrumbs({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "id": "breadcrumbs",
                "isVisible": true,
                "layoutType": kony.flex.FLOW_HORIZONTAL,
                "masterType": constants.MASTER_TYPE_DEFAULT,
                "isModalContainer": false,
                "skin": "sknflxf5f6f8Op100",
                "top": "0dp",
                "overrides": {
                    "btnBackToMain": {
                        "text": "MANAGE CUSTOMER CARE INFORMATION"
                    },
                    "lblCurrentScreen": {
                        "text": "ADD "
                    }
                }
            }, {
                "overrides": {}
            }, {
                "overrides": {}
            });
            flxBreadCrumbs.add(breadcrumbs);
            var flxImportDetails = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "340dp",
                "id": "flxImportDetails",
                "isVisible": false,
                "layoutType": kony.flex.FREE_FORM,
                "left": "35dp",
                "isModalContainer": false,
                "right": "35dp",
                "skin": "slFbox",
                "top": "125px",
                "zIndex": 1
            }, {}, {});
            flxImportDetails.setDefaultUnit(kony.flex.DP);
            var noStaticData = new com.adminConsole.staticContent.noStaticData({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "bottom": "0dp",
                "id": "noStaticData",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0px",
                "masterType": constants.MASTER_TYPE_DEFAULT,
                "isModalContainer": false,
                "right": "0dp",
                "skin": "slFbox",
                "top": "0dp",
                "zIndex": 1,
                "overrides": {
                    "btnAddStaticContent": {
                        "text": "ADD",
                        "width": "110px"
                    },
                    "lblNoStaticContentCreated": {
                        "text": "No Customer Care Information added yet."
                    },
                    "lblNoStaticContentMsg": {
                        "text": "Click on \"Add\" to enter the Customer Care Information"
                    },
                    "noStaticData": {
                        "bottom": "0dp",
                        "height": "viz.val_cleared",
                        "left": "0px",
                        "right": "0dp",
                        "width": "viz.val_cleared"
                    }
                }
            }, {
                "overrides": {}
            }, {
                "overrides": {}
            });
            flxImportDetails.add(noStaticData);
            var flxScrollMainContent = new kony.ui.FlexScrollContainer({
                "allowHorizontalBounce": false,
                "allowVerticalBounce": true,
                "bounces": true,
                "clipBounds": true,
                "enableScrolling": true,
                "height": "100%",
                "horizontalScrollIndicator": true,
                "id": "flxScrollMainContent",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0px",
                "pagingEnabled": false,
                "right": "0px",
                "scrollDirection": kony.flex.SCROLL_VERTICAL,
                "skin": "sknscrollflxf5f6f8Op100",
                "top": "115px",
                "verticalScrollIndicator": true,
                "zIndex": 1
            }, {}, {});
            flxScrollMainContent.setDefaultUnit(kony.flex.DP);
            var flxAdvancedSearch = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                "clipBounds": true,
                "id": "flxAdvancedSearch",
                "isVisible": false,
                "layoutType": kony.flex.FREE_FORM,
                "left": "35px",
                "isModalContainer": false,
                "right": "35px",
                "skin": "CopyslFbox0c37f1533930c4d",
                "top": "0px",
                "zIndex": 1
            }, {}, {});
            flxAdvancedSearch.setDefaultUnit(kony.flex.DP);
            var flxSearch1 = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                "bottom": "35px",
                "clipBounds": true,
                "id": "flxSearch1",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "CopyslFbox0ddacfb6d79684f",
                "top": "0dp",
                "width": "100%",
                "zIndex": 1
            }, {}, {});
            flxSearch1.setDefaultUnit(kony.flex.DP);
            var flxSearchHeading = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                "clipBounds": true,
                "id": "flxSearchHeading",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0%",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0px",
                "width": "100%",
                "zIndex": 1
            }, {}, {});
            flxSearchHeading.setDefaultUnit(kony.flex.DP);
            var advancedSearch = new com.adminConsole.search.advancedSearch({
                "height": "85px",
                "id": "advancedSearch",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "masterType": constants.MASTER_TYPE_DEFAULT,
                "isModalContainer": false,
                "skin": "slFbox0hfd18814fd664dCM",
                "top": "0dp",
                "width": "100%",
                "overrides": {}
            }, {
                "overrides": {}
            }, {
                "overrides": {}
            });
            var flxSearchResult = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "60px",
                "id": "flxSearchResult",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "85dp",
                "width": "100%",
                "zIndex": 1
            }, {}, {});
            flxSearchResult.setDefaultUnit(kony.flex.DP);
            var flxCsr = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "25px",
                "id": "flxCsr",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "35dp",
                "isModalContainer": false,
                "skin": "CopyslFbox0ica8ce013fdc40",
                "top": "16dp",
                "width": "95px",
                "zIndex": 1
            }, {}, {});
            flxCsr.setDefaultUnit(kony.flex.DP);
            var lblCsrAssist = new kony.ui.Label({
                "centerY": "50%",
                "id": "lblCsrAssist",
                "isVisible": true,
                "left": "5dp",
                "skin": "CopyslLabel0d2920a8c52fc41",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmLogsController.CSR_Assist\")",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var imgCross = new kony.ui.Image2({
                "centerY": "50%",
                "height": "10dp",
                "id": "imgCross",
                "isVisible": true,
                "left": "75dp",
                "skin": "slImage",
                "src": "close_blue.png",
                "width": "10dp",
                "zIndex": 1
            }, {
                "imageScaleMode": constants.IMAGE_SCALE_MODE_MAINTAIN_ASPECT_RATIO,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxCsr.add(lblCsrAssist, imgCross);
            var flxAdmin = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "25px",
                "id": "flxAdmin",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "140dp",
                "isModalContainer": false,
                "skin": "CopyslFbox0ica8ce013fdc40",
                "top": "16dp",
                "width": "95px",
                "zIndex": 1
            }, {}, {});
            flxAdmin.setDefaultUnit(kony.flex.DP);
            var lblAdmin = new kony.ui.Label({
                "centerY": "50%",
                "id": "lblAdmin",
                "isVisible": true,
                "left": "25dp",
                "skin": "CopyslLabel0d2920a8c52fc41",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmLogsController.Admin\")",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var imgCross1 = new kony.ui.Image2({
                "centerY": "50%",
                "height": "10dp",
                "id": "imgCross1",
                "isVisible": true,
                "left": "75dp",
                "skin": "slImage",
                "src": "close_blue.png",
                "width": "10dp",
                "zIndex": 1
            }, {
                "imageScaleMode": constants.IMAGE_SCALE_MODE_MAINTAIN_ASPECT_RATIO,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxAdmin.add(lblAdmin, imgCross1);
            var flxSuperAdmin = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "25px",
                "id": "flxSuperAdmin",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "245dp",
                "isModalContainer": false,
                "skin": "CopyslFbox0ica8ce013fdc40",
                "top": "16dp",
                "width": "95px",
                "zIndex": 1
            }, {}, {});
            flxSuperAdmin.setDefaultUnit(kony.flex.DP);
            var lblSuperAdmin = new kony.ui.Label({
                "centerY": "50%",
                "id": "lblSuperAdmin",
                "isVisible": true,
                "left": "5dp",
                "skin": "CopyslLabel0d2920a8c52fc41",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmLogsController.Super_Admin\")",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var imgCross2 = new kony.ui.Image2({
                "centerY": "50%",
                "height": "10dp",
                "id": "imgCross2",
                "isVisible": true,
                "left": "75dp",
                "skin": "slImage",
                "src": "close_blue.png",
                "width": "10dp",
                "zIndex": 1
            }, {
                "imageScaleMode": constants.IMAGE_SCALE_MODE_MAINTAIN_ASPECT_RATIO,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxSuperAdmin.add(lblSuperAdmin, imgCross2);
            var flxSeparetor = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "centerY": "50%",
                "clipBounds": true,
                "height": "25px",
                "id": "flxSeparetor",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "356dp",
                "isModalContainer": false,
                "skin": "CopyslFbox0b600ae3ddf634f",
                "width": "1px",
                "zIndex": 1
            }, {}, {});
            flxSeparetor.setDefaultUnit(kony.flex.DP);
            flxSeparetor.add();
            var lblClearAll = new kony.ui.Label({
                "centerY": "50%",
                "id": "lblClearAll",
                "isVisible": true,
                "left": "372dp",
                "skin": "CopyslLabel0d6e029a5d8e647",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmCustomerCare.lblClearAll\")",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var flxUnderline2 = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "bottom": "0dp",
                "clipBounds": true,
                "height": "1px",
                "id": "flxUnderline2",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "CopyslFbox0g9535637f9db4c",
                "width": "100%",
                "zIndex": 1
            }, {}, {});
            flxUnderline2.setDefaultUnit(kony.flex.DP);
            flxUnderline2.add();
            flxSearchResult.add(flxCsr, flxAdmin, flxSuperAdmin, flxSeparetor, lblClearAll, flxUnderline2);
            flxSearchHeading.add(advancedSearch, flxSearchResult);
            var flxSegmentUsers1 = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                "clipBounds": false,
                "id": "flxSegmentUsers1",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0%",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "146px",
                "width": "100%",
                "zIndex": 1
            }, {}, {});
            flxSegmentUsers1.setDefaultUnit(kony.flex.DP);
            var segusers1 = new kony.ui.SegmentedUI2({
                "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                "data": [
                    [{
                            "imgSortEmail": "sorting3x.png",
                            "imgSortName": "sorting3x.png",
                            "imgSortPermissions": "sorting3x.png",
                            "imgSortRole": "sorting3x.png",
                            "imgSortStatus": "sorting3x.png",
                            "imgSortUsername": "sorting3x.png",
                            "lblUsersHeaderEmail": "EMAIL",
                            "lblUsersHeaderName": "FULL NAME",
                            "lblUsersHeaderPermissions": "PERMISSIONS",
                            "lblUsersHeaderRole": "ROLE",
                            "lblUsersHeaderSeperator": ".",
                            "lblUsersHeaderStatus": "STATUS",
                            "lblUsersHeaderUsername": "USERNAME"
                        },
                        [{
                            "imgOptions": "dots3x.png",
                            "imgUsersStatus": "active_circle2x.png",
                            "lblEmailId": "john.doe@kony.com",
                            "lblFullName": "John Doe",
                            "lblPermissions": "25",
                            "lblRole": "20",
                            "lblSeperator": ".",
                            "lblUsername": "john.doe",
                            "lblUsersStatus": "Active"
                        }, {
                            "imgOptions": "dots3x.png",
                            "imgUsersStatus": "active_circle2x.png",
                            "lblEmailId": "john.doe@kony.com",
                            "lblFullName": "John Doe",
                            "lblPermissions": "25",
                            "lblRole": "20",
                            "lblSeperator": ".",
                            "lblUsername": "john.doe",
                            "lblUsersStatus": "Active"
                        }, {
                            "imgOptions": "dots3x.png",
                            "imgUsersStatus": "active_circle2x.png",
                            "lblEmailId": "john.doe@kony.com",
                            "lblFullName": "John Doe",
                            "lblPermissions": "25",
                            "lblRole": "20",
                            "lblSeperator": ".",
                            "lblUsername": "john.doe",
                            "lblUsersStatus": "Active"
                        }, {
                            "imgOptions": "dots3x.png",
                            "imgUsersStatus": "active_circle2x.png",
                            "lblEmailId": "john.doe@kony.com",
                            "lblFullName": "John Doe",
                            "lblPermissions": "25",
                            "lblRole": "20",
                            "lblSeperator": ".",
                            "lblUsername": "john.doe",
                            "lblUsersStatus": "Active"
                        }, {
                            "imgOptions": "dots3x.png",
                            "imgUsersStatus": "active_circle2x.png",
                            "lblEmailId": "john.doe@kony.com",
                            "lblFullName": "John Doe",
                            "lblPermissions": "25",
                            "lblRole": "20",
                            "lblSeperator": ".",
                            "lblUsername": "john.doe",
                            "lblUsersStatus": "Active"
                        }]
                    ]
                ],
                "groupCells": false,
                "id": "segusers1",
                "isVisible": true,
                "left": "35dp",
                "needPageIndicator": true,
                "pageOffDotImage": "pageoffdot.png",
                "pageOnDotImage": "pageondot.png",
                "retainSelection": false,
                "right": 35,
                "rowFocusSkin": "seg2Focus",
                "rowSkin": "sknseg3pxRadius",
                "rowTemplate": "flxUsers",
                "sectionHeaderSkin": "sliPhoneSegmentHeader",
                "sectionHeaderTemplate": "flxUsersHeader",
                "selectionBehavior": constants.SEGUI_DEFAULT_BEHAVIOR,
                "separatorRequired": false,
                "showScrollbars": false,
                "top": "0dp",
                "viewType": constants.SEGUI_VIEW_TYPE_TABLEVIEW,
                "widgetDataMap": {
                    "flxOptions": "flxOptions",
                    "flxStatus": "flxStatus",
                    "flxUsers": "flxUsers",
                    "flxUsersHeader": "flxUsersHeader",
                    "flxUsersHeaderEmailId": "flxUsersHeaderEmailId",
                    "flxUsersHeaderFullName": "flxUsersHeaderFullName",
                    "flxUsersHeaderPermissions": "flxUsersHeaderPermissions",
                    "flxUsersHeaderRole": "flxUsersHeaderRole",
                    "flxUsersHeaderStatus": "flxUsersHeaderStatus",
                    "flxUsersHeaderUsername": "flxUsersHeaderUsername",
                    "imgOptions": "imgOptions",
                    "imgSortEmail": "imgSortEmail",
                    "imgSortName": "imgSortName",
                    "imgSortPermissions": "imgSortPermissions",
                    "imgSortRole": "imgSortRole",
                    "imgSortStatus": "imgSortStatus",
                    "imgSortUsername": "imgSortUsername",
                    "imgUsersStatus": "imgUsersStatus",
                    "lblEmailId": "lblEmailId",
                    "lblFullName": "lblFullName",
                    "lblPermissions": "lblPermissions",
                    "lblRole": "lblRole",
                    "lblSeperator": "lblSeperator",
                    "lblUsername": "lblUsername",
                    "lblUsersHeaderEmail": "lblUsersHeaderEmail",
                    "lblUsersHeaderName": "lblUsersHeaderName",
                    "lblUsersHeaderPermissions": "lblUsersHeaderPermissions",
                    "lblUsersHeaderRole": "lblUsersHeaderRole",
                    "lblUsersHeaderSeperator": "lblUsersHeaderSeperator",
                    "lblUsersHeaderStatus": "lblUsersHeaderStatus",
                    "lblUsersHeaderUsername": "lblUsersHeaderUsername",
                    "lblUsersStatus": "lblUsersStatus"
                },
                "zIndex": 10
            }, {
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var flexOptions = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                "clipBounds": false,
                "id": "flexOptions",
                "isVisible": false,
                "layoutType": kony.flex.FLOW_VERTICAL,
                "isModalContainer": false,
                "right": "2%",
                "skin": "sknflxffffffop100dbdbe6Radius3px",
                "top": "100px",
                "width": "18.39%",
                "zIndex": 100
            }, {}, {});
            flexOptions.setDefaultUnit(kony.flex.DP);
            var CopylblDescription0a252259b5c5b47 = new kony.ui.Label({
                "height": "40px",
                "id": "CopylblDescription0a252259b5c5b47",
                "isVisible": true,
                "left": "15px",
                "skin": "sknlblLato5d6c7f12px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.roles.ASSIGN\")",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 6, 0, 0],
                "paddingInPixel": false
            }, {});
            var CopylblRoles0f4ed925d516743 = new kony.ui.Label({
                "id": "CopylblRoles0f4ed925d516743",
                "isVisible": true,
                "left": "15dp",
                "skin": "sknlbllato00a3d412px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.permission.Users\")",
                "top": "0dp",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var CopylblPremissions0f8ff66e806ad46 = new kony.ui.Label({
                "bottom": 7,
                "id": "CopylblPremissions0f8ff66e806ad46",
                "isVisible": true,
                "left": "15dp",
                "skin": "sknlbllato00a3d412px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmCustomerCare.CopylblPremissions0f8ff66e806ad46\")",
                "top": "5dp",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var CopyflxSeperator0f47be42d7a5845 = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "centerX": "50%",
                "clipBounds": true,
                "height": "1px",
                "id": "CopyflxSeperator0f47be42d7a5845",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "isModalContainer": false,
                "skin": "sknflxd6dbe7",
                "top": "0px",
                "width": "100%",
                "zIndex": 1
            }, {}, {});
            CopyflxSeperator0f47be42d7a5845.setDefaultUnit(kony.flex.DP);
            CopyflxSeperator0f47be42d7a5845.add();
            var CopyflxOption0f482b0cd04b74b = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "35dp",
                "id": "CopyflxOption0f482b0cd04b74b",
                "isVisible": false,
                "layoutType": kony.flex.FLOW_HORIZONTAL,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0dp",
                "width": "100%",
                "zIndex": 1
            }, {}, {});
            CopyflxOption0f482b0cd04b74b.setDefaultUnit(kony.flex.DP);
            var CopyimgOption0cd1138e9eecd41 = new kony.ui.Image2({
                "centerY": "50%",
                "height": "15dp",
                "id": "CopyimgOption0cd1138e9eecd41",
                "isVisible": true,
                "left": "15dp",
                "skin": "slImage",
                "src": "imagedrag.png",
                "width": "15dp",
                "zIndex": 1
            }, {
                "imageScaleMode": constants.IMAGE_SCALE_MODE_MAINTAIN_ASPECT_RATIO,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var CopylblOption0h45b31f2ff7d41 = new kony.ui.Label({
                "centerY": "50%",
                "id": "CopylblOption0h45b31f2ff7d41",
                "isVisible": true,
                "left": "15dp",
                "skin": "sknlblLatoBold35475f14px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.permission.View\")",
                "top": "4dp",
                "width": "40px",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            CopyflxOption0f482b0cd04b74b.add(CopyimgOption0cd1138e9eecd41, CopylblOption0h45b31f2ff7d41);
            var CopyflxOption0bc20c8a8030749 = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "35dp",
                "id": "CopyflxOption0bc20c8a8030749",
                "isVisible": true,
                "layoutType": kony.flex.FLOW_HORIZONTAL,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0dp",
                "width": "100%",
                "zIndex": 1
            }, {}, {});
            CopyflxOption0bc20c8a8030749.setDefaultUnit(kony.flex.DP);
            var CopyimgOption0b58a89219f314f = new kony.ui.Image2({
                "centerY": "50%",
                "height": "15dp",
                "id": "CopyimgOption0b58a89219f314f",
                "isVisible": true,
                "left": "15dp",
                "skin": "slImage",
                "src": "edit2x.png",
                "width": "15dp",
                "zIndex": 1
            }, {
                "imageScaleMode": constants.IMAGE_SCALE_MODE_MAINTAIN_ASPECT_RATIO,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var CopylblOption0b3d5b94677c44e = new kony.ui.Label({
                "centerY": "50%",
                "id": "CopylblOption0b3d5b94677c44e",
                "isVisible": true,
                "left": "15dp",
                "skin": "sknlblLatoBold35475f14px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.permission.Edit\")",
                "top": "4dp",
                "width": "40px",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            CopyflxOption0bc20c8a8030749.add(CopyimgOption0b58a89219f314f, CopylblOption0b3d5b94677c44e);
            var CopyflxOption0a3abb187c4704d = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "35dp",
                "id": "CopyflxOption0a3abb187c4704d",
                "isVisible": false,
                "layoutType": kony.flex.FLOW_HORIZONTAL,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0dp",
                "width": "100%",
                "zIndex": 1
            }, {}, {});
            CopyflxOption0a3abb187c4704d.setDefaultUnit(kony.flex.DP);
            var CopyimgOption0g7346328c2894d = new kony.ui.Image2({
                "centerY": "50%",
                "height": "15dp",
                "id": "CopyimgOption0g7346328c2894d",
                "isVisible": true,
                "left": "15dp",
                "skin": "slImage",
                "src": "imagedrag.png",
                "width": "15dp",
                "zIndex": 1
            }, {
                "imageScaleMode": constants.IMAGE_SCALE_MODE_MAINTAIN_ASPECT_RATIO,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var CopylblOption0e4aa6e320a224b = new kony.ui.Label({
                "centerY": "50%",
                "id": "CopylblOption0e4aa6e320a224b",
                "isVisible": true,
                "left": "15dp",
                "skin": "sknlblLatoBold35475f14px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.permission.Suspend\")",
                "top": "4dp",
                "width": "60px",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            CopyflxOption0a3abb187c4704d.add(CopyimgOption0g7346328c2894d, CopylblOption0e4aa6e320a224b);
            var CopyflxOption0e8d2e0a8b51446 = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "35dp",
                "id": "CopyflxOption0e8d2e0a8b51446",
                "isVisible": true,
                "layoutType": kony.flex.FLOW_HORIZONTAL,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0dp",
                "width": "100%",
                "zIndex": 1
            }, {}, {});
            CopyflxOption0e8d2e0a8b51446.setDefaultUnit(kony.flex.DP);
            var CopyimgOption0j2dc3599ac654d = new kony.ui.Image2({
                "centerY": "50%",
                "height": "15dp",
                "id": "CopyimgOption0j2dc3599ac654d",
                "isVisible": true,
                "left": "15dp",
                "skin": "slImage",
                "src": "deactive_2x.png",
                "width": "15dp",
                "zIndex": 1
            }, {
                "imageScaleMode": constants.IMAGE_SCALE_MODE_MAINTAIN_ASPECT_RATIO,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var CopylblOption0d9aafa510e7f4c = new kony.ui.Label({
                "centerY": "50%",
                "id": "CopylblOption0d9aafa510e7f4c",
                "isVisible": true,
                "left": "15dp",
                "skin": "sknlblLatoBold35475f14px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.permission.Deactivate\")",
                "top": "4dp",
                "width": "70px",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            CopyflxOption0e8d2e0a8b51446.add(CopyimgOption0j2dc3599ac654d, CopylblOption0d9aafa510e7f4c);
            flexOptions.add(CopylblDescription0a252259b5c5b47, CopylblRoles0f4ed925d516743, CopylblPremissions0f8ff66e806ad46, CopyflxSeperator0f47be42d7a5845, CopyflxOption0f482b0cd04b74b, CopyflxOption0bc20c8a8030749, CopyflxOption0a3abb187c4704d, CopyflxOption0e8d2e0a8b51446);
            flxSegmentUsers1.add(segusers1, flexOptions);
            flxSearch1.add(flxSearchHeading, flxSegmentUsers1);
            flxAdvancedSearch.add(flxSearch1);
            var flxListingPage = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "100%",
                "id": "flxListingPage",
                "isVisible": false,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0px",
                "isModalContainer": false,
                "right": "0px",
                "skin": "slFbox",
                "top": "0px",
                "width": "100%",
                "zIndex": 1
            }, {}, {});
            flxListingPage.setDefaultUnit(kony.flex.DP);
            var flxMainSubHeader = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "60dp",
                "id": "flxMainSubHeader",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "-5dp",
                "width": "100%",
                "zIndex": 100
            }, {}, {});
            flxMainSubHeader.setDefaultUnit(kony.flex.DP);
            var subHeader = new com.adminConsole.header.subHeader({
                "height": "48dp",
                "id": "subHeader",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "masterType": constants.MASTER_TYPE_DEFAULT,
                "isModalContainer": false,
                "skin": "CopyslFbox2",
                "top": "0dp",
                "width": "100%",
                "overrides": {
                    "tbxSearchBox": {
                        "placeholder": "Search by Service Name"
                    }
                }
            }, {
                "overrides": {}
            }, {
                "overrides": {}
            });
            flxMainSubHeader.add(subHeader);
            var flxCustomerCareList = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                "clipBounds": true,
                "id": "flxCustomerCareList",
                "isVisible": true,
                "layoutType": kony.flex.FLOW_VERTICAL,
                "left": "35px",
                "isModalContainer": false,
                "right": "35px",
                "skin": "sknflxffffffBorderd6dbe7Radius4px",
                "top": "50dp",
                "zIndex": 1
            }, {}, {});
            flxCustomerCareList.setDefaultUnit(kony.flex.DP);
            var flxCustomerCareHeader = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "60px",
                "id": "flxCustomerCareHeader",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0px",
                "isModalContainer": false,
                "right": "0px",
                "skin": "slFbox",
                "top": "0"
            }, {}, {});
            flxCustomerCareHeader.setDefaultUnit(kony.flex.DP);
            var flxLocationsHeaderName = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "100%",
                "id": "flxLocationsHeaderName",
                "isVisible": true,
                "layoutType": kony.flex.FLOW_HORIZONTAL,
                "left": "30px",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0dp",
                "width": "60px",
                "zIndex": 1
            }, {}, {});
            flxLocationsHeaderName.setDefaultUnit(kony.flex.DP);
            var lblHeaderName = new kony.ui.Label({
                "centerY": "50%",
                "id": "lblHeaderName",
                "isVisible": true,
                "left": "0",
                "skin": "sknlblLato696c7312px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.permission.NAME\")",
                "top": 0,
                "width": "38px",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {
                "hoverSkin": "sknlblLato00000011px"
            });
            var lblFonticonName = new kony.ui.Label({
                "centerY": "50%",
                "id": "lblFonticonName",
                "isVisible": true,
                "left": "7dp",
                "skin": "sknIcon12px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmAlertsManagement.lblSortName\")",
                "top": "0dp",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {
                "hoverSkin": "sknlblcursor"
            });
            var imgSortName = new kony.ui.Image2({
                "centerY": "50%",
                "height": "13dp",
                "id": "imgSortName",
                "isVisible": false,
                "left": "3px",
                "skin": "slImage",
                "src": "sorting3x.png",
                "top": "0dp",
                "width": "13dp",
                "zIndex": 1
            }, {
                "imageScaleMode": constants.IMAGE_SCALE_MODE_MAINTAIN_ASPECT_RATIO,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxLocationsHeaderName.add(lblHeaderName, lblFonticonName, imgSortName);
            var flxLocationsHeaderEmailPhone = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "100%",
                "id": "flxLocationsHeaderEmailPhone",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "39.56%",
                "isModalContainer": false,
                "right": "18%",
                "skin": "slFbox",
                "top": "0dp"
            }, {}, {});
            flxLocationsHeaderEmailPhone.setDefaultUnit(kony.flex.DP);
            var flxLocationsHeaderEmail = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "100%",
                "id": "flxLocationsHeaderEmail",
                "isVisible": true,
                "layoutType": kony.flex.FLOW_HORIZONTAL,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0dp",
                "width": "100px",
                "zIndex": 1
            }, {}, {});
            flxLocationsHeaderEmail.setDefaultUnit(kony.flex.DP);
            var lblLocationsHeaderEmail = new kony.ui.Label({
                "centerY": "50%",
                "id": "lblLocationsHeaderEmail",
                "isVisible": true,
                "left": "0px",
                "skin": "sknlblLato696c7312px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.View.EMAILID\")",
                "top": 0,
                "width": "60px",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var imgSortEmail = new kony.ui.Image2({
                "centerY": "50%",
                "height": "13dp",
                "id": "imgSortEmail",
                "isVisible": true,
                "left": "3px",
                "skin": "slImage",
                "src": "sorting3x.png",
                "top": "0dp",
                "width": "13dp",
                "zIndex": 1
            }, {
                "imageScaleMode": constants.IMAGE_SCALE_MODE_MAINTAIN_ASPECT_RATIO,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxLocationsHeaderEmail.add(lblLocationsHeaderEmail, imgSortEmail);
            var flxLocationsHeaderPhone = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "100%",
                "id": "flxLocationsHeaderPhone",
                "isVisible": true,
                "layoutType": kony.flex.FLOW_HORIZONTAL,
                "left": "60%",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0dp",
                "width": "65px",
                "zIndex": 1
            }, {}, {});
            flxLocationsHeaderPhone.setDefaultUnit(kony.flex.DP);
            var lblLocationsHeaderPhone = new kony.ui.Label({
                "centerY": "50%",
                "id": "lblLocationsHeaderPhone",
                "isVisible": true,
                "left": "0px",
                "skin": "sknlblLato696c7312px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmCustomerCare.lblLocationsHeaderPhone\")",
                "top": 0,
                "width": "42px",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var imgSortPhone = new kony.ui.Image2({
                "centerY": "50%",
                "height": "13dp",
                "id": "imgSortPhone",
                "isVisible": true,
                "left": "3dp",
                "skin": "slImage",
                "src": "sorting3x.png",
                "top": "0dp",
                "width": "13dp",
                "zIndex": 1
            }, {
                "imageScaleMode": constants.IMAGE_SCALE_MODE_MAINTAIN_ASPECT_RATIO,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxLocationsHeaderPhone.add(lblLocationsHeaderPhone, imgSortPhone);
            flxLocationsHeaderEmailPhone.add(flxLocationsHeaderEmail, flxLocationsHeaderPhone);
            var flxStatus = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "100%",
                "id": "flxStatus",
                "isVisible": true,
                "layoutType": kony.flex.FLOW_HORIZONTAL,
                "left": "84%",
                "isModalContainer": false,
                "skin": "sknCursor",
                "top": "0dp",
                "width": "65px",
                "zIndex": 1
            }, {}, {});
            flxStatus.setDefaultUnit(kony.flex.DP);
            var lblStatus = new kony.ui.Label({
                "centerY": "50%",
                "id": "lblStatus",
                "isVisible": true,
                "left": "0px",
                "skin": "sknlblLato696c7312px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.roles.STATUS\")",
                "top": 0,
                "width": "45px",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {
                "hoverSkin": "sknlblLato00000011px"
            });
            var imgSortStatus = new kony.ui.Image2({
                "centerY": "50%",
                "height": "13dp",
                "id": "imgSortStatus",
                "isVisible": false,
                "left": "3dp",
                "skin": "slImage",
                "src": "u195.png",
                "top": "0dp",
                "width": "13dp",
                "zIndex": 1
            }, {
                "imageScaleMode": constants.IMAGE_SCALE_MODE_MAINTAIN_ASPECT_RATIO,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var fontIconFilterStatus = new kony.ui.Label({
                "centerY": "50%",
                "id": "fontIconFilterStatus",
                "isVisible": true,
                "left": "3dp",
                "skin": "sknIcon15px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmAlertsManagement.lblIconChannels\")",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {
                "hoverSkin": "sknSortIconHover15px"
            });
            flxStatus.add(lblStatus, imgSortStatus, fontIconFilterStatus);
            var lblUsersHeaderSeperator = new kony.ui.Label({
                "bottom": 0,
                "height": "1px",
                "id": "lblUsersHeaderSeperator",
                "isVisible": true,
                "left": "20px",
                "right": "33px",
                "skin": "sknLblTableHeaderLine",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmCSR.lblSeperator\")",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxCustomerCareHeader.add(flxLocationsHeaderName, flxLocationsHeaderEmailPhone, flxStatus, lblUsersHeaderSeperator);
            var flxLocationsSegment = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                "clipBounds": true,
                "id": "flxLocationsSegment",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0dp",
                "width": "100%",
                "zIndex": 1
            }, {}, {});
            flxLocationsSegment.setDefaultUnit(kony.flex.DP);
            var listingSegmentClient = new com.adminConsole.common.listingSegmentClient({
                "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                "id": "listingSegmentClient",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "masterType": constants.MASTER_TYPE_DEFAULT,
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0dp",
                "width": "100%",
                "overrides": {
                    "contextualMenu.imgOption1": {
                        "src": "imagedrag.png"
                    },
                    "contextualMenu.imgOption2": {
                        "src": "edit2x.png"
                    },
                    "contextualMenu.imgOption3": {
                        "src": "imagedrag.png"
                    },
                    "contextualMenu.imgOption4": {
                        "src": "deactive_2x.png"
                    },
                    "flxContextualMenu": {
                        "isVisible": false,
                        "right": "32px"
                    },
                    "flxListingSegmentWrapper": {
                        "layoutType": kony.flex.FREE_FORM
                    },
                    "flxPagination": {
                        "isVisible": false
                    },
                    "listingSegmentClient": {
                        "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                        "bottom": "viz.val_cleared",
                        "centerX": "viz.val_cleared",
                        "centerY": "viz.val_cleared",
                        "left": "0dp",
                        "maxHeight": "viz.val_cleared",
                        "maxWidth": "viz.val_cleared",
                        "minHeight": "viz.val_cleared",
                        "minWidth": "viz.val_cleared",
                        "right": "viz.val_cleared",
                        "top": "0dp",
                        "width": "100%"
                    },
                    "segListing": {
                        "bottom": "viz.val_cleared",
                        "height": "300dp"
                    }
                }
            }, {
                "overrides": {}
            }, {
                "overrides": {}
            });
            flxLocationsSegment.add(listingSegmentClient);
            flxCustomerCareList.add(flxCustomerCareHeader, flxLocationsSegment);
            var flxStatusFilter = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                "clipBounds": true,
                "id": "flxStatusFilter",
                "isVisible": false,
                "layoutType": kony.flex.FREE_FORM,
                "isModalContainer": false,
                "right": "111px",
                "skin": "slFbox",
                "top": "85px",
                "width": "150px",
                "zIndex": 15
            }, {}, {});
            flxStatusFilter.setDefaultUnit(kony.flex.DP);
            var statusFilterMenu = new com.adminConsole.common.statusFilterMenu({
                "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                "id": "statusFilterMenu",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "masterType": constants.MASTER_TYPE_DEFAULT,
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "5dp",
                "width": "100%",
                "zIndex": 15,
                "overrides": {
                    "statusFilterMenu": {
                        "zIndex": 15
                    }
                }
            }, {
                "overrides": {}
            }, {
                "overrides": {}
            });
            flxStatusFilter.add(statusFilterMenu);
            flxListingPage.add(flxMainSubHeader, flxCustomerCareList, flxStatusFilter);
            var flxCustomercareInformation = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "bottom": "35px",
                "clipBounds": true,
                "id": "flxCustomercareInformation",
                "isVisible": false,
                "layoutType": kony.flex.FREE_FORM,
                "left": "35dp",
                "masterType": constants.MASTER_TYPE_USERWIDGET,
                "isModalContainer": false,
                "right": "35dp",
                "skin": "sknflxffffffOp100BorderE1E5Ed",
                "top": "15px",
                "zIndex": 1
            }, {}, {});
            flxCustomercareInformation.setDefaultUnit(kony.flex.DP);
            var flxInformationWrapper1 = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "bottom": "81dp",
                "clipBounds": true,
                "id": "flxInformationWrapper1",
                "isVisible": false,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "isModalContainer": false,
                "right": "0dp",
                "skin": "slFbox",
                "top": "0dp",
                "zIndex": 1
            }, {}, {});
            flxInformationWrapper1.setDefaultUnit(kony.flex.DP);
            flxInformationWrapper1.add();
            var flxInformationButtons = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "bottom": "0dp",
                "clipBounds": true,
                "height": "80px",
                "id": "flxInformationButtons",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "20dp",
                "isModalContainer": false,
                "right": "20dp",
                "skin": "slFbox",
                "zIndex": 1
            }, {}, {});
            flxInformationButtons.setDefaultUnit(kony.flex.DP);
            var informationButtons = new com.adminConsole.common.commonButtons({
                "height": "80px",
                "id": "informationButtons",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "masterType": constants.MASTER_TYPE_DEFAULT,
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0dp",
                "width": "100%",
                "overrides": {
                    "btnNext": {
                        "text": "SAVE & ADD NEW",
                        "width": "160px"
                    },
                    "flxRightButtons": {
                        "width": "280px"
                    }
                }
            }, {
                "overrides": {}
            }, {
                "overrides": {}
            });
            flxInformationButtons.add(informationButtons);
            var flxInformationWrapper = new kony.ui.FlexScrollContainer({
                "allowHorizontalBounce": false,
                "allowVerticalBounce": true,
                "bottom": "81px",
                "bounces": true,
                "clipBounds": true,
                "enableScrolling": true,
                "horizontalScrollIndicator": true,
                "id": "flxInformationWrapper",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "pagingEnabled": false,
                "right": "0dp",
                "scrollDirection": kony.flex.SCROLL_VERTICAL,
                "skin": "slFSbox",
                "top": "0dp",
                "verticalScrollIndicator": true,
                "zIndex": 1
            }, {}, {});
            flxInformationWrapper.setDefaultUnit(kony.flex.DP);
            var lblServiceName = new kony.ui.Label({
                "id": "lblServiceName",
                "isVisible": true,
                "left": "20dp",
                "right": "0dp",
                "skin": "sknlblLatoRegular484b5213px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmCustomerCare.lblServiceName\")",
                "top": "45dp",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var lblCharCount = new kony.ui.Label({
                "id": "lblCharCount",
                "isVisible": false,
                "right": "40px",
                "skin": "sknllbl485c75Lato13px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmCustomerCare.lblCharCount\")",
                "top": "45px",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 5
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var txtbxServiceName = new kony.ui.TextBox2({
                "autoCapitalize": constants.TEXTBOX_AUTO_CAPITALIZE_NONE,
                "focusSkin": "skntxtbxDetailsFocus14px",
                "height": "40dp",
                "id": "txtbxServiceName",
                "isVisible": true,
                "keyBoardStyle": constants.TEXTBOX_KEY_BOARD_STYLE_DEFAULT,
                "left": "20dp",
                "placeholder": "Add service for which you want to create customer care information",
                "right": "20dp",
                "secureTextEntry": false,
                "skin": "skntxtbxDetails0bbf1235271384a",
                "textInputMode": constants.TEXTBOX_INPUT_MODE_ANY,
                "top": "70dp",
                "zIndex": 1
            }, {
                "containerHeightMode": constants.TEXTBOX_FONT_METRICS_DRIVEN_HEIGHT,
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [1, 0, 0, 0],
                "paddingInPixel": false
            }, {
                "autoCorrect": false
            });
            var flxNoServiceNameError = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "15dp",
                "id": "flxNoServiceNameError",
                "isVisible": false,
                "layoutType": kony.flex.FREE_FORM,
                "left": "40dp",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "115dp",
                "width": "270dp",
                "zIndex": 5
            }, {}, {});
            flxNoServiceNameError.setDefaultUnit(kony.flex.DP);
            var lblNoServiceNameError = new kony.ui.Label({
                "id": "lblNoServiceNameError",
                "isVisible": true,
                "left": "15dp",
                "skin": "sknlblError",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmCustomerCareController.Service_Name_cannot_be_empty\")",
                "top": "0dp",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var lblNoServiceChannelErrorIcon = new kony.ui.Label({
                "height": "15dp",
                "id": "lblNoServiceChannelErrorIcon",
                "isVisible": true,
                "left": "0dp",
                "skin": "sknErrorIcon",
                "text": "",
                "top": "0dp",
                "width": "15dp",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxNoServiceNameError.add(lblNoServiceNameError, lblNoServiceChannelErrorIcon);
            var lblActive = new kony.ui.Label({
                "id": "lblActive",
                "isVisible": true,
                "left": "20dp",
                "skin": "sknlblLatoRegular484b5213px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.secureimage.Active\")",
                "top": "135dp",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var SwitchActive = new kony.ui.Switch({
                "height": "25px",
                "id": "SwitchActive",
                "isVisible": true,
                "left": "85dp",
                "leftSideText": "ON",
                "rightSideText": "OFF",
                "selectedIndex": 0,
                "skin": "sknSwitchServiceManagement",
                "top": "135px",
                "width": "38px",
                "zIndex": 1
            }, {
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var lblDetails = new kony.ui.Label({
                "id": "lblDetails",
                "isVisible": true,
                "left": "20dp",
                "skin": "sknlblSubHeader0b73b6af9628b4e",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmCustomerCare.lblDetails\")",
                "top": "176dp",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var flxError = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                "clipBounds": true,
                "id": "flxError",
                "isVisible": true,
                "layoutType": kony.flex.FLOW_HORIZONTAL,
                "left": "450dp",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "176dp",
                "width": "30%",
                "zIndex": 1
            }, {}, {});
            flxError.setDefaultUnit(kony.flex.DP);
            var imgError = new kony.ui.Image2({
                "height": "15dp",
                "id": "imgError",
                "isVisible": true,
                "left": "5dp",
                "skin": "slImage",
                "src": "error_1x.png",
                "top": "0dp",
                "width": "15dp",
                "zIndex": 1
            }, {
                "imageScaleMode": constants.IMAGE_SCALE_MODE_MAINTAIN_ASPECT_RATIO,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var lblErrorMsg = new kony.ui.Label({
                "id": "lblErrorMsg",
                "isVisible": true,
                "left": "5dp",
                "right": "0dp",
                "skin": "sknlblError",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmCustomerCareController.Enter_Atleast_One\")",
                "top": "0dp",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxError.add(imgError, lblErrorMsg);
            var lblMandatoryText = new kony.ui.Label({
                "id": "lblMandatoryText",
                "isVisible": true,
                "left": "115dp",
                "skin": "slLabelCaps12px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmCustomerCare.lblMandatoryText\")",
                "top": "176dp",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var FlxContactDetails1 = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                "clipBounds": true,
                "id": "FlxContactDetails1",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "isModalContainer": false,
                "right": "0px",
                "skin": "slFbox",
                "top": "200dp",
                "zIndex": 1
            }, {}, {});
            FlxContactDetails1.setDefaultUnit(kony.flex.DP);
            var flxCustomerInfoHeader = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "60dp",
                "id": "flxCustomerInfoHeader",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "isModalContainer": false,
                "right": "0px",
                "skin": "slFbox",
                "top": "0dp",
                "zIndex": 1
            }, {}, {});
            flxCustomerInfoHeader.setDefaultUnit(kony.flex.DP);
            var lblPhoneNumber = new kony.ui.Label({
                "centerY": "50%",
                "id": "lblPhoneNumber",
                "isVisible": true,
                "left": "20dp",
                "skin": "slLabelCaps12px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmCustomerCare.lblPhoneNumber\")",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var lblNumberStatus = new kony.ui.Label({
                "centerY": "50%",
                "id": "lblNumberStatus",
                "isVisible": true,
                "left": "38.15%",
                "skin": "slLabelCaps12px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.roles.STATUS\")",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var lblEmailId = new kony.ui.Label({
                "centerY": "50%",
                "id": "lblEmailId",
                "isVisible": true,
                "left": "49.56%",
                "skin": "slLabelCaps12px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.View.EMAILID\")",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var lblEmailidStatus = new kony.ui.Label({
                "centerY": "50%",
                "id": "lblEmailidStatus",
                "isVisible": true,
                "left": "83.88%",
                "skin": "slLabelCaps12px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.roles.STATUS\")",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var lblCustomerSeparator = new kony.ui.Label({
                "bottom": "0px",
                "height": "1px",
                "id": "lblCustomerSeparator",
                "isVisible": true,
                "left": "20px",
                "right": "20px",
                "skin": "sknlblSeperator",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmAlertsManagement.lblAlertTypeDescriptionCount\")",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxCustomerInfoHeader.add(lblPhoneNumber, lblNumberStatus, lblEmailId, lblEmailidStatus, lblCustomerSeparator);
            var flxCustomerData = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                "clipBounds": true,
                "id": "flxCustomerData",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "isModalContainer": false,
                "right": "0dp",
                "skin": "slFbox",
                "top": "60dp",
                "zIndex": 1
            }, {}, {});
            flxCustomerData.setDefaultUnit(kony.flex.DP);
            var addNewRow = new com.adminConsole.common.addNewRow({
                "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                "id": "addNewRow",
                "isVisible": true,
                "layoutType": kony.flex.FLOW_VERTICAL,
                "left": "0dp",
                "masterType": constants.MASTER_TYPE_DEFAULT,
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0dp",
                "width": "100%",
                "overrides": {
                    "addNewRow": {
                        "autogrowMode": kony.flex.AUTOGROW_HEIGHT
                    },
                    "btnAddMore": {
                        "left": "40dp",
                        "width": kony.flex.USE_PREFFERED_SIZE
                    },
                    "segData": {
                        "data": [{
                            "SwitchActive": {
                                "selectedIndex": 0
                            },
                            "SwitchActiveStatus": {
                                "selectedIndex": 0
                            },
                            "fontIconDelete": "",
                            "imgDelete": "delete_2x.png",
                            "lblActive": "Active",
                            "lblActiveStatus": "Active",
                            "lblDash": "-",
                            "lblErrorIcon": "",
                            "lblErrorText": "kony.i18n.getLocalizedString(\"i18n.frmLocations.lblNoPhnNumberError\")",
                            "lblNoEmailIDError": "Please enter a valid Email ID",
                            "lblNoPhoneNumberError": "Please enter a valid phone number",
                            "lblSeparator": ".",
                            "txtISDCode": "",
                            "txtbxEmailid": {
                                "placeholder": "",
                                "text": ""
                            },
                            "txtbxPhoneNumber": ""
                        }]
                    }
                }
            }, {
                "overrides": {}
            }, {
                "overrides": {}
            });
            flxCustomerData.add(addNewRow);
            FlxContactDetails1.add(flxCustomerInfoHeader, flxCustomerData);
            var btnAddMore = new kony.ui.Button({
                "height": "22px",
                "id": "btnAddMore",
                "isVisible": true,
                "right": "30px",
                "skin": "sknbtnf7f7faLatoRegular12Px485c75Border1px485C75Radius20px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmCustomerCare.btnAddMore\")",
                "top": "176px",
                "width": "80px",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_CENTER,
                "displayText": true,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {
                "hoverSkin": "sknbtnffffffLatoRegular12Px485c75Border1px485C75Radius20px"
            });
            flxInformationWrapper.add(lblServiceName, lblCharCount, txtbxServiceName, flxNoServiceNameError, lblActive, SwitchActive, lblDetails, flxError, lblMandatoryText, FlxContactDetails1, btnAddMore);
            flxCustomercareInformation.add(flxInformationWrapper1, flxInformationButtons, flxInformationWrapper);
            flxScrollMainContent.add(flxAdvancedSearch, flxListingPage, flxCustomercareInformation);
            var flxToastMessage = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "bottom": "0%",
                "clipBounds": true,
                "height": "80px",
                "id": "flxToastMessage",
                "isVisible": false,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0px",
                "isModalContainer": false,
                "right": "0px",
                "skin": "slFbox",
                "zIndex": 1
            }, {}, {});
            flxToastMessage.setDefaultUnit(kony.flex.DP);
            var toastMessage = new com.adminConsole.common.toastMessage({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "id": "toastMessage",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "masterType": constants.MASTER_TYPE_DEFAULT,
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0dp",
                "overrides": {
                    "lbltoastMessage": {
                        "text": "Branch deactivated successfully"
                    }
                }
            }, {
                "overrides": {}
            }, {
                "overrides": {}
            });
            flxToastMessage.add(toastMessage);
            var flxAlertToastMessage = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "bottom": "0%",
                "clipBounds": true,
                "height": "80px",
                "id": "flxAlertToastMessage",
                "isVisible": false,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0px",
                "isModalContainer": false,
                "skin": "slFbox",
                "width": "100%",
                "zIndex": 1
            }, {}, {});
            flxAlertToastMessage.setDefaultUnit(kony.flex.DP);
            var AlertToastMessage = new com.adminConsole.common.toastMessage({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "id": "AlertToastMessage",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "masterType": constants.MASTER_TYPE_DEFAULT,
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0dp",
                "overrides": {
                    "lbltoastMessage": {
                        "text": "20/100 branches uploaded to the system."
                    }
                }
            }, {
                "overrides": {}
            }, {
                "overrides": {}
            });
            flxAlertToastMessage.add(AlertToastMessage);
            var flxLoading = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "100%",
                "id": "flxLoading",
                "isVisible": false,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "sknLoadingBlur",
                "top": "0dp",
                "width": "100%",
                "zIndex": 10
            }, {}, {});
            flxLoading.setDefaultUnit(kony.flex.DP);
            var flxImageContainer = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "centerX": "50%",
                "centerY": "50%",
                "clipBounds": true,
                "height": "75px",
                "id": "flxImageContainer",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "isModalContainer": false,
                "skin": "slFbox",
                "width": "75px",
                "zIndex": 1
            }, {}, {});
            flxImageContainer.setDefaultUnit(kony.flex.DP);
            var imgLoading = new kony.ui.Image2({
                "centerX": "50%",
                "centerY": "50%",
                "height": "50px",
                "id": "imgLoading",
                "isVisible": true,
                "skin": "slImage",
                "src": "loadingscreenimage.gif",
                "width": "50px",
                "zIndex": 1
            }, {
                "imageScaleMode": constants.IMAGE_SCALE_MODE_MAINTAIN_ASPECT_RATIO,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxImageContainer.add(imgLoading);
            flxLoading.add(flxImageContainer);
            flxRightPannel.add(flxMainHeader, flxHeaderDropdown, flxBreadCrumbs, flxImportDetails, flxScrollMainContent, flxToastMessage, flxAlertToastMessage, flxLoading);
            flxMain.add(flxLeftPannel, flxRightPannel);
            var flxImportDeletePopup = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "100%",
                "id": "flxImportDeletePopup",
                "isVisible": false,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "masterType": constants.MASTER_TYPE_USERWIDGET,
                "isModalContainer": false,
                "skin": "flxPopupTrans0ba858b5d5d304e",
                "top": "0dp",
                "width": "100%",
                "zIndex": 10
            }, {}, {});
            flxImportDeletePopup.setDefaultUnit(kony.flex.DP);
            var popUp = new com.adminConsole.common.popUp({
                "height": "100%",
                "id": "popUp",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "masterType": constants.MASTER_TYPE_DEFAULT,
                "isModalContainer": false,
                "skin": "sknflxBg000000Op50",
                "top": "0dp",
                "width": "100%",
                "zIndex": 1,
                "overrides": {
                    "btnPopUpCancel": {
                        "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmAlertsManagement.NO_LEAVE_IT_AS_IT_IS\")",
                        "minWidth": "80dp",
                        "right": "20px",
                        "width": kony.flex.USE_PREFFERED_SIZE
                    },
                    "btnPopUpDelete": {
                        "i18n_text": "kony.i18n.getLocalizedString(\"i18n.PopUp.YesDeactivate\")",
                        "width": kony.flex.USE_PREFFERED_SIZE
                    },
                    "lblPopUpMainMessage": {
                        "text": "Delete Customer Care Information"
                    },
                    "rtxPopUpDisclaimer": {
                        "text": "Are you sure to delete the Customer Care Information?<br><br>\n\nIf you delete it, the customer can not see the Customer Care Information."
                    }
                }
            }, {
                "overrides": {}
            }, {
                "overrides": {}
            });
            flxImportDeletePopup.add(popUp);
            var flxDeactivePopup = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "100%",
                "id": "flxDeactivePopup",
                "isVisible": false,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "flxPopupTrans0ba858b5d5d304e",
                "top": "0dp",
                "width": "100%",
                "zIndex": 10
            }, {}, {});
            flxDeactivePopup.setDefaultUnit(kony.flex.DP);
            var popUp1 = new com.adminConsole.common.popUp({
                "height": "100%",
                "id": "popUp1",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "masterType": constants.MASTER_TYPE_DEFAULT,
                "isModalContainer": false,
                "skin": "sknflxBg000000Op50",
                "top": "0dp",
                "width": "100%",
                "zIndex": 1,
                "overrides": {
                    "btnPopUpCancel": {
                        "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmAlertsManagement.NO_LEAVE_IT_AS_IT_IS\")",
                        "minWidth": "80dp",
                        "right": "20dp",
                        "width": kony.flex.USE_PREFFERED_SIZE
                    },
                    "btnPopUpDelete": {
                        "i18n_text": "kony.i18n.getLocalizedString(\"i18n.PopUp.YesDeactivate\")",
                        "width": kony.flex.USE_PREFFERED_SIZE
                    },
                    "lblPopUpMainMessage": {
                        "text": "Deactivate Customer Care Information?"
                    },
                    "rtxPopUpDisclaimer": {
                        "text": "Are you sure to delete the Customer Care Information?<br><br>\n\nIf you delete it, the customer can not see the Customer Care Information."
                    }
                }
            }, {
                "overrides": {}
            }, {
                "overrides": {}
            });
            flxDeactivePopup.add(popUp1);
            var flxEditCancelConfirmation = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "100%",
                "id": "flxEditCancelConfirmation",
                "isVisible": false,
                "layoutType": kony.flex.FREE_FORM,
                "left": "10dp",
                "masterType": constants.MASTER_TYPE_USERWIDGET,
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "10dp",
                "width": "100%",
                "zIndex": 5
            }, {}, {});
            flxEditCancelConfirmation.setDefaultUnit(kony.flex.DP);
            var popUpCancelEdits = new com.adminConsole.common.popUpCancelEdits({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "height": "100%",
                "id": "popUpCancelEdits",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "masterType": constants.MASTER_TYPE_DEFAULT,
                "isModalContainer": false,
                "skin": "sknflxBg000000Op50",
                "top": "0dp",
                "width": "100%",
                "zIndex": 1,
                "overrides": {
                    "btnPopUpCancel": {
                        "i18n_text": "kony.i18n.getLocalizedString(\"i8n.navigation.cancelButton\")"
                    },
                    "btnPopUpDelete": {
                        "i18n_text": "kony.i18n.getLocalizedString(\"i8n.navigation.okButton\")"
                    },
                    "lblPopUpMainMessage": {
                        "i18n_text": "kony.i18n.getLocalizedString(\"i8n.navigation.cancelChanges\")"
                    },
                    "popUpCancelEdits": {
                        "bottom": "viz.val_cleared",
                        "centerX": "viz.val_cleared",
                        "centerY": "viz.val_cleared",
                        "height": "100%",
                        "left": "0dp",
                        "maxHeight": "viz.val_cleared",
                        "maxWidth": "viz.val_cleared",
                        "minHeight": "viz.val_cleared",
                        "minWidth": "viz.val_cleared",
                        "right": "viz.val_cleared",
                        "top": "0dp",
                        "width": "100%"
                    },
                    "rtxPopUpDisclaimer": {
                        "i18n_text": "kony.i18n.getLocalizedString(\"i8n.navigation.cancelChangesBody\")"
                    }
                }
            }, {
                "overrides": {}
            }, {
                "overrides": {}
            });
            flxEditCancelConfirmation.add(popUpCancelEdits);
            this.add(flxMain, flxImportDeletePopup, flxDeactivePopup, flxEditCancelConfirmation);
        };
        return [{
            "addWidgets": addWidgetsfrmCustomerCare,
            "enabledForIdleTimeout": true,
            "id": "frmCustomerCare",
            "layoutType": kony.flex.FREE_FORM,
            "needAppMenu": false,
            "preShow": function(eventobject) {
                controller.AS_Form_f397b6c2e73a4e59937b1146470104e1(eventobject);
            },
            "skin": "slForm"
        }, {
            "displayOrientation": constants.FORM_DISPLAY_ORIENTATION_PORTRAIT,
            "layoutType": kony.flex.FREE_FORM,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, {
            "onDeviceBack": controller.AS_Form_jaef654522ba47cf9f0673fdce7b4b65,
            "retainScrollPosition": false
        }]
    }
});