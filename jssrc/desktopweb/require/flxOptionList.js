define("flxOptionList", function() {
    return function(controller) {
        var flxOptionList = new kony.ui.FlexContainer({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "clipBounds": true,
            "height": "40dp",
            "id": "flxOptionList",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "left": "0dp",
            "isModalContainer": false,
            "skin": "slFbox",
            "top": "0dp",
            "width": "100%"
        }, {}, {});
        flxOptionList.setDefaultUnit(kony.flex.DP);
        var lblOptionValue = new kony.ui.Label({
            "centerX": "15%",
            "centerY": "50%",
            "id": "lblOptionValue",
            "isVisible": true,
            "left": "0",
            "skin": "sknLbl485C75Font12pxKA",
            "text": "Label",
            "top": "0",
            "width": kony.flex.USE_PREFFERED_SIZE
        }, {
            "contentAlignment": constants.CONTENT_ALIGN_CENTER,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, {});
        flxOptionList.add(lblOptionValue);
        return flxOptionList;
    }
})