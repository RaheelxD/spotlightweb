define("flxCustomerCardsList", function() {
    return function(controller) {
        var flxCustomerCardsList = new kony.ui.FlexContainer({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "clipBounds": true,
            "focusSkin": "sknflxffffffop100",
            "height": "50px",
            "id": "flxCustomerCardsList",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "isModalContainer": false,
            "skin": "sknflxffffffop100"
        }, {}, {
            "hoverSkin": "sknfbfcfc"
        });
        flxCustomerCardsList.setDefaultUnit(kony.flex.DP);
        var flxCustomerCards = new kony.ui.FlexContainer({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "clipBounds": true,
            "height": "100%",
            "id": "flxCustomerCards",
            "isVisible": true,
            "layoutType": kony.flex.FLOW_HORIZONTAL,
            "left": "0dp",
            "isModalContainer": false,
            "skin": "slFbox",
            "top": "0dp",
            "width": "100%"
        }, {}, {
            "hoverSkin": "sknCursor"
        });
        flxCustomerCards.setDefaultUnit(kony.flex.DP);
        var flxCardType = new kony.ui.FlexContainer({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "centerY": "50%",
            "clipBounds": true,
            "height": "20px",
            "id": "flxCardType",
            "isVisible": true,
            "layoutType": kony.flex.FLOW_HORIZONTAL,
            "left": "35px",
            "isModalContainer": false,
            "onClick": controller.AS_FlexContainer_ddda6a6b213845f396ee45cea95d2f72,
            "skin": "slFbox",
            "width": "11%",
            "zIndex": 2
        }, {}, {});
        flxCardType.setDefaultUnit(kony.flex.DP);
        var ImgCardType = new kony.ui.Image2({
            "centerY": "50%",
            "height": "20px",
            "id": "ImgCardType",
            "isVisible": true,
            "left": "0px",
            "skin": "slImage",
            "src": "visa.png",
            "width": "25px",
            "zIndex": 2
        }, {
            "imageScaleMode": constants.IMAGE_SCALE_MODE_MAINTAIN_ASPECT_RATIO,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, {});
        var lblCardType = new kony.ui.Label({
            "centerY": "50%",
            "id": "lblCardType",
            "isVisible": true,
            "left": "3px",
            "skin": "sknLblLato485c7513px",
            "text": "Credit Card",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, {
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, {});
        flxCardType.add(ImgCardType, lblCardType);
        var lblCardName = new kony.ui.Label({
            "centerY": "50%",
            "id": "lblCardName",
            "isVisible": true,
            "left": "2%",
            "skin": "sknLblLato485c7513px",
            "text": "Gold Plus Savving credit Card",
            "width": "13%",
            "zIndex": 1
        }, {
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, {});
        var lblCardNumber = new kony.ui.Label({
            "centerY": "50%",
            "id": "lblCardNumber",
            "isVisible": true,
            "left": "2%",
            "skin": "sknLblLato485c7513px",
            "text": "1234 **** **** ****",
            "width": "16%",
            "zIndex": 1
        }, {
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, {});
        var lblCardHolder = new kony.ui.Label({
            "centerY": "50%",
            "id": "lblCardHolder",
            "isVisible": true,
            "left": "2%",
            "skin": "sknLblLato485c7513px",
            "text": "John Peter Samual Junior Doe",
            "width": "15%",
            "zIndex": 1
        }, {
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, {});
        var lblRequestAndNotification = new kony.ui.Label({
            "centerY": "50%",
            "id": "lblRequestAndNotification",
            "isVisible": true,
            "left": "2%",
            "skin": "sknLblLato485c7513px",
            "text": "2 request and 5 notifications",
            "width": "17%",
            "zIndex": 1
        }, {
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, {});
        var flxStatus = new kony.ui.FlexContainer({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "centerY": "50%",
            "clipBounds": true,
            "height": "20px",
            "id": "flxStatus",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "left": "2%",
            "isModalContainer": false,
            "skin": "slFbox",
            "width": "12%",
            "zIndex": 1
        }, {}, {});
        flxStatus.setDefaultUnit(kony.flex.DP);
        var lblFontIconServiceStatus = new kony.ui.Label({
            "centerY": "50%",
            "height": "15dp",
            "id": "lblFontIconServiceStatus",
            "isVisible": false,
            "left": "0px",
            "skin": "sknFontIconOptionMenu",
            "text": "",
            "width": "15px",
            "zIndex": 1
        }, {
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, {});
        var lblServiceStatus = new kony.ui.Label({
            "centerY": "50%",
            "id": "lblServiceStatus",
            "isVisible": true,
            "left": "20px",
            "skin": "sknLblLato485c7513px",
            "text": "Active",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, {
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, {});
        flxStatus.add(lblFontIconServiceStatus, lblServiceStatus);
        var flxOptions = new kony.ui.FlexContainer({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "centerY": "50%",
            "clipBounds": true,
            "height": "24dp",
            "id": "flxOptions",
            "isVisible": false,
            "layoutType": kony.flex.FREE_FORM,
            "left": "1%",
            "isModalContainer": false,
            "right": "35px",
            "skin": "slFbox",
            "top": "10dp",
            "width": "24px",
            "zIndex": 2
        }, {}, {
            "hoverSkin": "sknflxffffffop100Border424242Radius100px"
        });
        flxOptions.setDefaultUnit(kony.flex.DP);
        var lblIconOptions = new kony.ui.Label({
            "centerX": "50%",
            "centerY": "50%",
            "height": "15px",
            "id": "lblIconOptions",
            "isVisible": true,
            "left": "14dp",
            "skin": "sknFontIconOptionMenu",
            "text": "",
            "width": "15px",
            "zIndex": 1
        }, {
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, {});
        flxOptions.add(lblIconOptions);
        flxCustomerCards.add(flxCardType, lblCardName, lblCardNumber, lblCardHolder, lblRequestAndNotification, flxStatus, flxOptions);
        var lblSeparator = new kony.ui.Label({
            "bottom": "0px",
            "height": "1px",
            "id": "lblSeparator",
            "isVisible": true,
            "left": "0px",
            "right": "0px",
            "skin": "sknlblSeperator",
            "text": "'",
            "zIndex": 2
        }, {
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, {});
        flxCustomerCardsList.add(flxCustomerCards, lblSeparator);
        return flxCustomerCardsList;
    }
})