define("flxSegAddAttribute", function() {
    return function(controller) {
        var flxSegAddAttribute = new kony.ui.FlexContainer({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "clipBounds": true,
            "height": "60px",
            "id": "flxSegAddAttribute",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "left": "0dp",
            "isModalContainer": false,
            "skin": "slFbox",
            "top": "0dp",
            "width": "100%"
        }, {}, {});
        flxSegAddAttribute.setDefaultUnit(kony.flex.DP);
        var flxAttribute = new kony.ui.FlexContainer({
            "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
            "clipBounds": true,
            "id": "flxAttribute",
            "isVisible": true,
            "layoutType": kony.flex.FLOW_VERTICAL,
            "left": "0%",
            "isModalContainer": false,
            "skin": "slFbox",
            "top": "0%",
            "width": "25%"
        }, {}, {});
        flxAttribute.setDefaultUnit(kony.flex.DP);
        var tbxAttribute = new kony.ui.TextBox2({
            "autoCapitalize": constants.TEXTBOX_AUTO_CAPITALIZE_NONE,
            "focusSkin": "defTextBoxFocus",
            "height": "40px",
            "id": "tbxAttribute",
            "isVisible": true,
            "keyBoardStyle": constants.TEXTBOX_KEY_BOARD_STYLE_DEFAULT,
            "left": "0px",
            "i18n_placeholder": "kony.i18n.getLocalizedString(\"i18n.products.Attribute\")",
            "secureTextEntry": false,
            "skin": "sknTbxBgFFFFFFBrD7D9E01pxR3px",
            "textInputMode": constants.TEXTBOX_INPUT_MODE_ANY,
            "top": "15px",
            "width": "100%",
            "zIndex": 1
        }, {
            "containerHeightMode": constants.TEXTBOX_FONT_METRICS_DRIVEN_HEIGHT,
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [3, 0, 0, 0],
            "paddingInPixel": false
        }, {
            "autoCorrect": false,
            "placeholderSkin": "defTextBoxPlaceholder"
        });
        flxAttribute.add(tbxAttribute);
        var flxAttributeValue = new kony.ui.FlexContainer({
            "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
            "clipBounds": true,
            "id": "flxAttributeValue",
            "isVisible": true,
            "layoutType": kony.flex.FLOW_VERTICAL,
            "left": "28%",
            "isModalContainer": false,
            "skin": "slFbox",
            "top": "0%",
            "width": "25%"
        }, {}, {});
        flxAttributeValue.setDefaultUnit(kony.flex.DP);
        var tbxAttributeValue = new kony.ui.TextBox2({
            "autoCapitalize": constants.TEXTBOX_AUTO_CAPITALIZE_NONE,
            "focusSkin": "defTextBoxFocus",
            "height": "40px",
            "id": "tbxAttributeValue",
            "isVisible": true,
            "keyBoardStyle": constants.TEXTBOX_KEY_BOARD_STYLE_DEFAULT,
            "left": "0px",
            "placeholder": "Value",
            "secureTextEntry": false,
            "skin": "sknTbxBgFFFFFFBrD7D9E01pxR3px",
            "textInputMode": constants.TEXTBOX_INPUT_MODE_ANY,
            "top": "15px",
            "width": "100%",
            "zIndex": 1
        }, {
            "containerHeightMode": constants.TEXTBOX_FONT_METRICS_DRIVEN_HEIGHT,
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [3, 0, 0, 0],
            "paddingInPixel": false
        }, {
            "autoCorrect": false,
            "placeholderSkin": "defTextBoxPlaceholder"
        });
        flxAttributeValue.add(tbxAttributeValue);
        var flxDeleteIcon = new kony.ui.FlexContainer({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "centerY": "55%",
            "clipBounds": true,
            "height": "22px",
            "id": "flxDeleteIcon",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "left": "56%",
            "isModalContainer": false,
            "skin": "slFbox",
            "top": "0px",
            "width": "22px",
            "zIndex": 1
        }, {}, {});
        flxDeleteIcon.setDefaultUnit(kony.flex.DP);
        var lblIconDelete = new kony.ui.Label({
            "id": "lblIconDelete",
            "isVisible": true,
            "left": "0px",
            "skin": "sknIcon22px192b45",
            "text": "",
            "top": "0px",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, {
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, {});
        flxDeleteIcon.add(lblIconDelete);
        flxSegAddAttribute.add(flxAttribute, flxAttributeValue, flxDeleteIcon);
        return flxSegAddAttribute;
    }
})