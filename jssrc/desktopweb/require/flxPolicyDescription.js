define("flxPolicyDescription", function() {
    return function(controller) {
        var flxPolicyDescription = new kony.ui.FlexContainer({
            "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
            "clipBounds": true,
            "id": "flxPolicyDescription",
            "isVisible": true,
            "layoutType": kony.flex.FLOW_VERTICAL,
            "left": "0dp",
            "isModalContainer": false,
            "skin": "sknflxffffffop100",
            "top": "0dp",
            "width": "100%"
        }, {}, {});
        flxPolicyDescription.setDefaultUnit(kony.flex.DP);
        var flxLanguage = new kony.ui.FlexContainer({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "clipBounds": true,
            "height": "18px",
            "id": "flxLanguage",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "left": "0dp",
            "isModalContainer": false,
            "skin": "slFbox",
            "top": "0dp",
            "width": "100%"
        }, {}, {});
        flxLanguage.setDefaultUnit(kony.flex.DP);
        var lblLanguage = new kony.ui.Label({
            "id": "lblLanguage",
            "isVisible": true,
            "left": "21dp",
            "skin": "sknlbl585f66latoRegular12px",
            "text": "ENGLISH (US)",
            "top": "0dp",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, {
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, {});
        var flxEdit = new kony.ui.FlexContainer({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "clipBounds": true,
            "height": "100%",
            "id": "flxEdit",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "isModalContainer": false,
            "onClick": controller.AS_FlexContainer_b035d7dcf52546248a11d56e5b8ee6f6,
            "right": "74dp",
            "skin": "hoverhandSkin",
            "top": "0dp",
            "width": "20dp"
        }, {}, {});
        flxEdit.setDefaultUnit(kony.flex.DP);
        var lblEdit = new kony.ui.Label({
            "centerX": "50%",
            "centerY": "50%",
            "height": "20px",
            "id": "lblEdit",
            "isVisible": true,
            "right": "72px",
            "skin": "sknIcon20px",
            "i18n_text": "kony.i18n.getLocalizedString(\"i18n.userwidgetmodel.lblIconOption1\")",
            "top": "0px",
            "width": "20px",
            "zIndex": 1
        }, {
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, {});
        flxEdit.add(lblEdit);
        var flxDelete = new kony.ui.FlexContainer({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "clipBounds": true,
            "height": "100%",
            "id": "flxDelete",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "isModalContainer": false,
            "onClick": controller.AS_FlexContainer_h43e332b32624c87a4361f632e581db3,
            "right": 42,
            "skin": "hoverhandSkin",
            "top": "0dp",
            "width": "20dp"
        }, {}, {});
        flxDelete.setDefaultUnit(kony.flex.DP);
        var lblDelete = new kony.ui.Label({
            "centerX": "50%",
            "centerY": "50%",
            "height": "20dp",
            "id": "lblDelete",
            "isVisible": true,
            "right": "49dp",
            "skin": "sknIcon20px",
            "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmFAQ.fonticonDelete\")",
            "top": "0dp",
            "width": "20dp",
            "zIndex": 1
        }, {
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, {});
        flxDelete.add(lblDelete);
        flxLanguage.add(lblLanguage, flxEdit, flxDelete);
        var lblSeperator = new kony.ui.Label({
            "bottom": "1dp",
            "height": "1px",
            "id": "lblSeperator",
            "isVisible": true,
            "left": "21dp",
            "right": "36px",
            "skin": "HeaderSeparator",
            "top": "7px",
            "width": "95%",
            "zIndex": 1
        }, {
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, {});
        var lblLanguageDescription = new kony.ui.RichText({
            "bottom": "20dp",
            "id": "lblLanguageDescription",
            "isVisible": true,
            "left": "21dp",
            "linkSkin": "defRichTextLink",
            "skin": "sknrtxLato485c7514px",
            "text": "Length of username must be 8 characters long but should not be more than 64 characters. Username must contain a symbol (.,-,_,@,!,#,$).",
            "top": "12dp",
            "width": "763dp",
            "zIndex": 1
        }, {
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [2, 0, 0, 0],
            "paddingInPixel": false
        }, {});
        flxPolicyDescription.add(flxLanguage, lblSeperator, lblLanguageDescription);
        return flxPolicyDescription;
    }
})