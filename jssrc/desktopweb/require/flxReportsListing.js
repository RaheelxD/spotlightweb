define("flxReportsListing", function() {
    return function(controller) {
        var flxReportsListing = new kony.ui.FlexContainer({
            "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
            "clipBounds": true,
            "id": "flxReportsListing",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "left": "0dp",
            "isModalContainer": false,
            "skin": "slFbox",
            "top": "0dp",
            "width": "100%"
        }, {}, {});
        flxReportsListing.setDefaultUnit(kony.flex.DP);
        var flxReportsContent = new kony.ui.FlexContainer({
            "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
            "clipBounds": true,
            "id": "flxReportsContent",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "left": "20dp",
            "isModalContainer": false,
            "right": "20dp",
            "skin": "slFbox",
            "top": "0dp"
        }, {}, {
            "hoverSkin": "sknfbfcfc"
        });
        flxReportsContent.setDefaultUnit(kony.flex.DP);
        var flxContent = new kony.ui.FlexContainer({
            "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
            "bottom": "15dp",
            "clipBounds": true,
            "id": "flxContent",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "left": "10dp",
            "isModalContainer": false,
            "right": "0dp",
            "skin": "slFbox",
            "top": "0"
        }, {}, {});
        flxContent.setDefaultUnit(kony.flex.DP);
        var lblReportName = new kony.ui.Label({
            "id": "lblReportName",
            "isVisible": true,
            "left": "0px",
            "skin": "sknlblLatoReg485c7513px",
            "text": "Platinum Customers",
            "top": "15px",
            "width": "22%",
            "zIndex": 1
        }, {
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, {});
        var lblDescription = new kony.ui.Label({
            "id": "lblDescription",
            "isVisible": true,
            "left": "25%",
            "skin": "sknlblLatoReg485c7513px",
            "text": "2019 Credit Report",
            "top": "15px",
            "width": "32%",
            "zIndex": 1
        }, {
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, {});
        var lblDataSource = new kony.ui.Label({
            "height": "15px",
            "id": "lblDataSource",
            "isVisible": true,
            "left": "60%",
            "skin": "sknlblLatoReg485c7513px",
            "text": "Fabric",
            "top": "15px",
            "width": "29%",
            "zIndex": 1
        }, {
            "contentAlignment": constants.CONTENT_ALIGN_TOP_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, {});
        var lblCreatedDate = new kony.ui.Label({
            "id": "lblCreatedDate",
            "isVisible": true,
            "left": "80%",
            "skin": "sknlblLatoReg485c7513px",
            "text": "12/12/2019",
            "top": "15px",
            "width": "14%",
            "zIndex": 1
        }, {
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, {});
        var flxOptions = new kony.ui.FlexContainer({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "clipBounds": true,
            "height": "25dp",
            "id": "flxOptions",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "isModalContainer": false,
            "onClick": controller.AS_FlexContainer_be0a45917cb844dd8b666cfe156fe497,
            "right": "10px",
            "skin": "sknFlxPointer",
            "top": "12px",
            "width": "25px",
            "zIndex": 2
        }, {}, {
            "hoverSkin": "sknFlxPointer"
        });
        flxOptions.setDefaultUnit(kony.flex.DP);
        var fontIconOptions = new kony.ui.Label({
            "centerX": "50%",
            "centerY": "49%",
            "id": "fontIconOptions",
            "isVisible": true,
            "skin": "sknIcon20px",
            "i18n_text": "kony.i18n.getLocalizedString(\"i18n.userwidgetmodel.lblIconOption2\")",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, {
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, {
            "hoverSkin": "sknIcon20pxWhiteHover"
        });
        flxOptions.add(fontIconOptions);
        flxContent.add(lblReportName, lblDescription, lblDataSource, lblCreatedDate, flxOptions);
        var lblSeparator = new kony.ui.Label({
            "bottom": "0dp",
            "height": "1px",
            "id": "lblSeparator",
            "isVisible": true,
            "left": "0dp",
            "right": "0dp",
            "skin": "sknlblSeperator",
            "text": "Label",
            "zIndex": 2
        }, {
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, {});
        flxReportsContent.add(flxContent, lblSeparator);
        flxReportsListing.add(flxReportsContent);
        return flxReportsListing;
    }
})