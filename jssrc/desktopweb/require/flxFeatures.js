define("flxFeatures", function() {
    return function(controller) {
        var flxFeatures = new kony.ui.FlexContainer({
            "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
            "clipBounds": true,
            "id": "flxFeatures",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "isModalContainer": false,
            "skin": "sknflxffffffop100"
        }, {}, {
            "hoverSkin": "sknfbfcfc"
        });
        flxFeatures.setDefaultUnit(kony.flex.DP);
        var flxDropdown = new kony.ui.FlexContainer({
            "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
            "bottom": "10dp",
            "clipBounds": true,
            "id": "flxDropdown",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "left": "1%",
            "isModalContainer": false,
            "onClick": controller.AS_FlexContainer_b1057bf841aa44f4aa0f68532b65c2ed,
            "right": "10px",
            "skin": "sknCursor",
            "top": "15dp",
            "width": "15px",
            "zIndex": 2
        }, {}, {});
        flxDropdown.setDefaultUnit(kony.flex.DP);
        var fonticonArrow = new kony.ui.Label({
            "height": "12px",
            "id": "fonticonArrow",
            "isVisible": true,
            "left": "0dp",
            "skin": "sknfontIconDescRightArrow14px",
            "text": "",
            "top": "0dp",
            "width": "12px",
            "zIndex": 1
        }, {
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, {});
        flxDropdown.add(fonticonArrow);
        var lblFeatureName = new kony.ui.Label({
            "bottom": "15px",
            "id": "lblFeatureName",
            "isVisible": true,
            "left": "4%",
            "skin": "sknLbl485C75LatoRegular13Px",
            "text": "ACH Collections",
            "top": "15px",
            "width": "22%",
            "zIndex": 100
        }, {
            "contentAlignment": constants.CONTENT_ALIGN_TOP_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, {});
        var lblFeatureCode = new kony.ui.Label({
            "id": "lblFeatureCode",
            "isVisible": true,
            "left": "28%",
            "skin": "sknLbl485C75LatoRegular13Px",
            "text": "ACH_Collection_1",
            "top": "15px",
            "width": "22%",
            "zIndex": 1
        }, {
            "contentAlignment": constants.CONTENT_ALIGN_TOP_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, {});
        var lblFeatureType = new kony.ui.Label({
            "bottom": "15dp",
            "id": "lblFeatureType",
            "isVisible": true,
            "left": "55%",
            "skin": "sknLbl485C75LatoRegular13Px",
            "text": "Retail Banking, Micro Business Banking, Small Business Banking",
            "top": "15px",
            "width": "25%",
            "zIndex": 1
        }, {
            "contentAlignment": constants.CONTENT_ALIGN_TOP_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, {});
        var flxStatus = new kony.ui.FlexContainer({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "clipBounds": true,
            "height": "50px",
            "id": "flxStatus",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "left": "82%",
            "isModalContainer": false,
            "skin": "slFbox",
            "top": "0dp",
            "width": "10%",
            "zIndex": 1
        }, {}, {});
        flxStatus.setDefaultUnit(kony.flex.DP);
        var lblFeatureStatus = new kony.ui.Label({
            "centerY": "48%",
            "id": "lblFeatureStatus",
            "isVisible": true,
            "left": "16px",
            "skin": "sknLbl485C75LatoRegular13Px",
            "text": "Active",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, {
            "contentAlignment": constants.CONTENT_ALIGN_TOP_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, {});
        var fontIconStatusImg = new kony.ui.Label({
            "centerY": "48%",
            "id": "fontIconStatusImg",
            "isVisible": true,
            "left": "0dp",
            "skin": "sknFontIconActivate",
            "text": "",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, {
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, {});
        flxStatus.add(lblFeatureStatus, fontIconStatusImg);
        var lblSeperator = new kony.ui.Label({
            "bottom": 0,
            "height": "1px",
            "id": "lblSeperator",
            "isVisible": true,
            "left": "0dp",
            "right": "0dp",
            "skin": "sknlblSeperator",
            "text": ".",
            "zIndex": 1
        }, {
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, {});
        var flxOptions = new kony.ui.FlexContainer({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "clipBounds": true,
            "height": "25dp",
            "id": "flxOptions",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "isModalContainer": false,
            "onClick": controller.AS_FlexContainer_a58f522d2cfc419c9a99c2cf77a878ac,
            "right": "15px",
            "skin": "slFbox",
            "top": "11dp",
            "width": "25px",
            "zIndex": 1
        }, {}, {
            "hoverSkin": "sknflxffffffop100Border424242Radius100px"
        });
        flxOptions.setDefaultUnit(kony.flex.DP);
        var lblIconImgOptions = new kony.ui.Label({
            "centerX": "50%",
            "centerY": "50%",
            "id": "lblIconImgOptions",
            "isVisible": true,
            "skin": "sknFontIconOptionMenu",
            "text": "",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, {
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, {});
        flxOptions.add(lblIconImgOptions);
        var flxFeatureDescriptionContent = new kony.ui.FlexContainer({
            "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
            "bottom": "15dp",
            "clipBounds": true,
            "id": "flxFeatureDescriptionContent",
            "isVisible": true,
            "layoutType": kony.flex.FLOW_HORIZONTAL,
            "left": "0dp",
            "isModalContainer": false,
            "skin": "slFbox",
            "top": "45dp",
            "width": "100%",
            "zIndex": 1
        }, {}, {});
        flxFeatureDescriptionContent.setDefaultUnit(kony.flex.DP);
        var flxFeatureDesc = new kony.ui.FlexContainer({
            "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
            "clipBounds": true,
            "id": "flxFeatureDesc",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "left": "4%",
            "isModalContainer": false,
            "skin": "slFbox",
            "top": "0dp",
            "width": "91%",
            "zIndex": 20
        }, {}, {});
        flxFeatureDesc.setDefaultUnit(kony.flex.DP);
        var lblDescriptionHeader = new kony.ui.Label({
            "height": "15px",
            "id": "lblDescriptionHeader",
            "isVisible": true,
            "left": "0px",
            "skin": "sknlblLato696c7311px",
            "text": "ACTIONS(11)",
            "top": "0px",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, {
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, {});
        var lblDescriptionValue = new kony.ui.Label({
            "id": "lblDescriptionValue",
            "isVisible": true,
            "left": "0px",
            "right": "35px",
            "skin": "sknLblLato485c7513px",
            "text": "Monetary(8),  Non-monetory(3)",
            "top": "25px",
            "zIndex": 1
        }, {
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, {});
        flxFeatureDesc.add(lblDescriptionHeader, lblDescriptionValue);
        flxFeatureDescriptionContent.add(flxFeatureDesc);
        flxFeatures.add(flxDropdown, lblFeatureName, lblFeatureCode, lblFeatureType, flxStatus, lblSeperator, flxOptions, flxFeatureDescriptionContent);
        return flxFeatures;
    }
})