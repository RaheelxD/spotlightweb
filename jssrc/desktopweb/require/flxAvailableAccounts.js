define("flxAvailableAccounts", function() {
    return function(controller) {
        var flxAvailableAccounts = new kony.ui.FlexContainer({
            "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
            "clipBounds": true,
            "id": "flxAvailableAccounts",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "left": "0dp",
            "isModalContainer": false,
            "skin": "slFbox",
            "top": "0dp",
            "width": "100%"
        }, {}, {});
        flxAvailableAccounts.setDefaultUnit(kony.flex.DP);
        var flxAcccountsContainer = new kony.ui.FlexContainer({
            "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
            "clipBounds": true,
            "id": "flxAcccountsContainer",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "left": "10dp",
            "isModalContainer": false,
            "right": "20dp",
            "skin": "sknflxffffffOp100Border5e90cbRadius3Px",
            "top": "10dp"
        }, {}, {});
        flxAcccountsContainer.setDefaultUnit(kony.flex.DP);
        var flxAccountNumber = new kony.ui.FlexContainer({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "clipBounds": true,
            "height": "45dp",
            "id": "flxAccountNumber",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "left": "0dp",
            "isModalContainer": false,
            "skin": "slFbox",
            "top": "0dp",
            "width": "100%",
            "zIndex": 1
        }, {}, {});
        flxAccountNumber.setDefaultUnit(kony.flex.DP);
        var lblAccountText = new kony.ui.Label({
            "centerY": "50%",
            "id": "lblAccountText",
            "isVisible": true,
            "left": "15dp",
            "skin": "sknLbl485c75LatoBold13px",
            "text": "Accounts",
            "top": "10dp",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, {
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, {});
        var lblAccountNum = new kony.ui.Label({
            "centerY": "50%",
            "id": "lblAccountNum",
            "isVisible": true,
            "left": "90dp",
            "skin": "sknLbl485c75LatoBold13px",
            "text": "1234567885646343",
            "top": "9dp",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, {
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, {});
        var btnAddAccount = new kony.ui.Button({
            "id": "btnAddAccount",
            "isVisible": true,
            "right": 20,
            "skin": "sknBtnLatoRegular11abeb12px",
            "text": "Add",
            "top": "12dp",
            "width": "35dp",
            "zIndex": 1
        }, {
            "contentAlignment": constants.CONTENT_ALIGN_CENTER,
            "displayText": true,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, {
            "hoverSkin": "sknBtnLatoRegular1682b212px"
        });
        var lblLine = new kony.ui.Label({
            "bottom": "0dp",
            "height": "1dp",
            "id": "lblLine",
            "isVisible": true,
            "left": "0dp",
            "right": "0dp",
            "skin": "sknlblSeperator",
            "text": "l",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, {
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, {});
        flxAccountNumber.add(lblAccountText, lblAccountNum, btnAddAccount, lblLine);
        var flxAccountDetails = new kony.ui.FlexContainer({
            "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
            "clipBounds": true,
            "id": "flxAccountDetails",
            "isVisible": true,
            "layoutType": kony.flex.FLOW_HORIZONTAL,
            "left": "0dp",
            "isModalContainer": false,
            "skin": "slFbox",
            "top": "45dp",
            "width": "100%",
            "zIndex": 1
        }, {}, {});
        flxAccountDetails.setDefaultUnit(kony.flex.DP);
        var flxAccField1 = new kony.ui.FlexContainer({
            "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
            "bottom": "10dp",
            "clipBounds": true,
            "id": "flxAccField1",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "left": "0dp",
            "isModalContainer": false,
            "skin": "slFbox",
            "top": "0dp",
            "width": "45%",
            "zIndex": 1
        }, {}, {});
        flxAccField1.setDefaultUnit(kony.flex.DP);
        var lblAccFieldHeader1 = new kony.ui.Label({
            "id": "lblAccFieldHeader1",
            "isVisible": true,
            "left": "15dp",
            "skin": "sknLbl485C75LatoRegular13Px",
            "i18n_text": "kony.i18n.getLocalizedString(\"i18n.Group.TYPE\")",
            "top": "10dp",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, {
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, {});
        var lblAccFieldValue1 = new kony.ui.Label({
            "id": "lblAccFieldValue1",
            "isVisible": true,
            "left": "15dp",
            "right": "15dp",
            "skin": "sknLbl485C75LatoRegular13Px",
            "text": "Savings",
            "top": "30dp",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, {
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, {});
        flxAccField1.add(lblAccFieldHeader1, lblAccFieldValue1);
        var flxAccField2 = new kony.ui.FlexContainer({
            "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
            "bottom": "10dp",
            "clipBounds": true,
            "id": "flxAccField2",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "left": "0dp",
            "isModalContainer": false,
            "skin": "slFbox",
            "top": "0dp",
            "width": "48%",
            "zIndex": 1
        }, {}, {});
        flxAccField2.setDefaultUnit(kony.flex.DP);
        var lblAccFieldHeader2 = new kony.ui.Label({
            "id": "lblAccFieldHeader2",
            "isVisible": true,
            "left": "15dp",
            "skin": "sknLbl485C75LatoRegular13Px",
            "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmCompanies.MEMBERSHIP_ID\")",
            "top": "10dp",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, {
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, {});
        var lblAccFieldValue2 = new kony.ui.Label({
            "id": "lblAccFieldValue2",
            "isVisible": true,
            "left": "15dp",
            "right": "15dp",
            "skin": "sknLbl485C75LatoRegular13Px",
            "text": "32752747643846834683483744",
            "top": "30dp",
            "zIndex": 1
        }, {
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, {});
        flxAccField2.add(lblAccFieldHeader2, lblAccFieldValue2);
        var flxAccField3 = new kony.ui.FlexContainer({
            "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
            "bottom": "10dp",
            "clipBounds": true,
            "id": "flxAccField3",
            "isVisible": false,
            "layoutType": kony.flex.FREE_FORM,
            "left": "0dp",
            "isModalContainer": false,
            "skin": "slFbox",
            "top": "0dp",
            "width": "32%",
            "zIndex": 1
        }, {}, {});
        flxAccField3.setDefaultUnit(kony.flex.DP);
        var lblAccFieldHeader3 = new kony.ui.Label({
            "id": "lblAccFieldHeader3",
            "isVisible": true,
            "left": "15dp",
            "skin": "sknLbl485C75LatoRegular13Px",
            "text": "TIN",
            "top": "10dp",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, {
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, {});
        var lblAccFieldValue3 = new kony.ui.Label({
            "id": "lblAccFieldValue3",
            "isVisible": true,
            "left": "15dp",
            "right": "15dp",
            "skin": "sknLbl485C75LatoRegular13Px",
            "text": "FES5DSHDBJNK",
            "top": "30dp",
            "zIndex": 1
        }, {
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, {});
        flxAccField3.add(lblAccFieldHeader3, lblAccFieldValue3);
        flxAccountDetails.add(flxAccField1, flxAccField2, flxAccField3);
        flxAcccountsContainer.add(flxAccountNumber, flxAccountDetails);
        flxAvailableAccounts.add(flxAcccountsContainer);
        return flxAvailableAccounts;
    }
})