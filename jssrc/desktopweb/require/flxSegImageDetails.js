define("flxSegImageDetails", function() {
    return function(controller) {
        var flxSegImageDetails = new kony.ui.FlexContainer({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "clipBounds": true,
            "height": "95px",
            "id": "flxSegImageDetails",
            "isVisible": true,
            "layoutType": kony.flex.FLOW_HORIZONTAL,
            "left": "0dp",
            "isModalContainer": false,
            "skin": "slFbox",
            "top": "0dp",
            "width": "100%"
        }, {}, {});
        flxSegImageDetails.setDefaultUnit(kony.flex.DP);
        var flxImageType = new kony.ui.FlexContainer({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "clipBounds": true,
            "height": "100%",
            "id": "flxImageType",
            "isVisible": true,
            "layoutType": kony.flex.FLOW_VERTICAL,
            "left": "0px",
            "isModalContainer": false,
            "skin": "slFbox",
            "top": "0px",
            "width": "25%"
        }, {}, {});
        flxImageType.setDefaultUnit(kony.flex.DP);
        var lblImgType = new kony.ui.Label({
            "id": "lblImgType",
            "isVisible": true,
            "left": "0px",
            "skin": "sknLbl485C75LatoRegular13Px",
            "i18n_text": "kony.i18n.getLocalizedString(\"i18n.products.Image_Type\")",
            "top": "0px",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, {
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, {});
        var lstBoxImageType = new kony.ui.ListBox({
            "focusSkin": "defListBoxFocus",
            "height": "40dp",
            "id": "lstBoxImageType",
            "isVisible": true,
            "left": "0px",
            "masterData": [
                ["SELECT", "Image Type"]
            ],
            "selectedKey": "SELECT",
            "skin": "sknLbxborderd7d9e03pxradius",
            "top": "15px",
            "width": "100%",
            "zIndex": 1
        }, {
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [3, 0, 0, 0],
            "paddingInPixel": false
        }, {
            "multiSelect": false
        });
        flxImageType.add(lblImgType, lstBoxImageType);
        var flxImageURL = new kony.ui.FlexContainer({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "clipBounds": true,
            "height": "100%",
            "id": "flxImageURL",
            "isVisible": true,
            "layoutType": kony.flex.FLOW_VERTICAL,
            "left": "2%",
            "isModalContainer": false,
            "skin": "slFbox",
            "top": "0px",
            "width": "65%",
            "zIndex": 1
        }, {}, {});
        flxImageURL.setDefaultUnit(kony.flex.DP);
        var flxImageURLTop = new kony.ui.FlexContainer({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "clipBounds": true,
            "height": "15px",
            "id": "flxImageURLTop",
            "isVisible": true,
            "layoutType": kony.flex.FLOW_HORIZONTAL,
            "left": "0dp",
            "isModalContainer": false,
            "skin": "slFbox",
            "top": "0dp",
            "width": "100%"
        }, {}, {});
        flxImageURLTop.setDefaultUnit(kony.flex.DP);
        var lblImgURL = new kony.ui.Label({
            "id": "lblImgURL",
            "isVisible": true,
            "left": "0px",
            "skin": "sknLbl485C75LatoRegular13Px",
            "i18n_text": "kony.i18n.getLocalizedString(\"i18n.products.Image_URL\")",
            "top": "0px",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, {
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, {});
        var lblFontIconInformation = new kony.ui.Label({
            "height": "14px",
            "id": "lblFontIconInformation",
            "isVisible": true,
            "left": "2px",
            "skin": "sknFontIcon485C7518px",
            "text": "",
            "top": "0px",
            "width": "18px",
            "zIndex": 1
        }, {
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, {});
        flxImageURLTop.add(lblImgURL, lblFontIconInformation);
        var tbxImageURL = new kony.ui.TextBox2({
            "autoCapitalize": constants.TEXTBOX_AUTO_CAPITALIZE_NONE,
            "focusSkin": "defTextBoxFocus",
            "height": "40dp",
            "id": "tbxImageURL",
            "isVisible": true,
            "keyBoardStyle": constants.TEXTBOX_KEY_BOARD_STYLE_DEFAULT,
            "left": "0px",
            "i18n_placeholder": "kony.i18n.getLocalizedString(\"i18n.products.Image_URL\")",
            "secureTextEntry": false,
            "skin": "sknTbxBgFFFFFFBrD7D9E01pxR3px",
            "textInputMode": constants.TEXTBOX_INPUT_MODE_ANY,
            "top": "15px",
            "width": "100%",
            "zIndex": 1
        }, {
            "containerHeightMode": constants.TEXTBOX_FONT_METRICS_DRIVEN_HEIGHT,
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [3, 0, 0, 0],
            "paddingInPixel": false
        }, {
            "autoCorrect": false,
            "placeholderSkin": "defTextBoxPlaceholder"
        });
        var flxErrorMsgURL = new kony.ui.FlexContainer({
            "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
            "clipBounds": true,
            "id": "flxErrorMsgURL",
            "isVisible": false,
            "layoutType": kony.flex.FLOW_HORIZONTAL,
            "left": "0dp",
            "isModalContainer": false,
            "skin": "slFbox",
            "top": "10dp",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, {}, {});
        flxErrorMsgURL.setDefaultUnit(kony.flex.DP);
        var lblErrorIcon = new kony.ui.Label({
            "height": "15dp",
            "id": "lblErrorIcon",
            "isVisible": true,
            "left": "0dp",
            "skin": "sknErrorIcon",
            "text": "",
            "top": "0dp",
            "width": "15dp",
            "zIndex": 1
        }, {
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, {});
        var lblErrorText = new kony.ui.Label({
            "height": "15dp",
            "id": "lblErrorText",
            "isVisible": true,
            "left": "0dp",
            "skin": "sknlblError",
            "text": "Image URL not valid.",
            "top": "0dp",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, {
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, {});
        flxErrorMsgURL.add(lblErrorIcon, lblErrorText);
        flxImageURL.add(flxImageURLTop, tbxImageURL, flxErrorMsgURL);
        var flxDeleteIcon = new kony.ui.FlexContainer({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "centerY": "50%",
            "clipBounds": true,
            "height": "22px",
            "id": "flxDeleteIcon",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "left": "2%",
            "isModalContainer": false,
            "skin": "slFbox",
            "top": "0px",
            "width": "22px",
            "zIndex": 1
        }, {}, {});
        flxDeleteIcon.setDefaultUnit(kony.flex.DP);
        var lblIconDelete = new kony.ui.Label({
            "id": "lblIconDelete",
            "isVisible": true,
            "left": "0px",
            "skin": "sknIcon22px192b45",
            "text": "",
            "top": "0px",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, {
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, {});
        flxDeleteIcon.add(lblIconDelete);
        flxSegImageDetails.add(flxImageType, flxImageURL, flxDeleteIcon);
        return flxSegImageDetails;
    }
})