define("flxSegProductFeatureOption", function() {
    return function(controller) {
        var flxSegProductFeatureOption = new kony.ui.FlexContainer({
            "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
            "clipBounds": true,
            "id": "flxSegProductFeatureOption",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "left": "0dp",
            "isModalContainer": false,
            "skin": "slFbox",
            "top": "0dp",
            "width": "100%"
        }, {}, {});
        flxSegProductFeatureOption.setDefaultUnit(kony.flex.DP);
        var flxValueContainer = new kony.ui.FlexContainer({
            "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
            "clipBounds": true,
            "id": "flxValueContainer",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "left": "0dp",
            "isModalContainer": false,
            "skin": "slFbox",
            "top": "5dp",
            "width": "28%",
            "zIndex": 1
        }, {}, {});
        flxValueContainer.setDefaultUnit(kony.flex.DP);
        var fontIconSelectOptionDefaultValue = new kony.ui.Label({
            "height": "20px",
            "id": "fontIconSelectOptionDefaultValue",
            "isVisible": true,
            "left": "0dp",
            "skin": "sknFontIconCheckBoxSelected",
            "text": "",
            "top": "31px",
            "width": "20px",
            "zIndex": 1
        }, {
            "contentAlignment": constants.CONTENT_ALIGN_CENTER,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, {});
        var lblDefaultValue = new kony.ui.Label({
            "height": "20px",
            "id": "lblDefaultValue",
            "isVisible": true,
            "left": "25px",
            "skin": "sknLbl485C75LatoRegular13Px",
            "i18n_text": "kony.i18n.getLocalizedString(\"i18n.products.Default_Value\")",
            "top": "31px",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, {
            "contentAlignment": constants.CONTENT_ALIGN_CENTER,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, {});
        var tbxValue = new kony.ui.TextBox2({
            "autoCapitalize": constants.TEXTBOX_AUTO_CAPITALIZE_NONE,
            "focusSkin": "defTextBoxFocus",
            "height": "40px",
            "id": "tbxValue",
            "isVisible": true,
            "keyBoardStyle": constants.TEXTBOX_KEY_BOARD_STYLE_DEFAULT,
            "left": "0dp",
            "maxTextLength": 50,
            "i18n_placeholder": "kony.i18n.getLocalizedString(\"i18n.products.Value\")",
            "secureTextEntry": false,
            "skin": "txtD7d9e0",
            "textInputMode": constants.TEXTBOX_INPUT_MODE_ANY,
            "top": "60px",
            "width": "100%",
            "zIndex": 1
        }, {
            "containerHeightMode": constants.TEXTBOX_FONT_METRICS_DRIVEN_HEIGHT,
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [3, 0, 0, 0],
            "paddingInPixel": false
        }, {
            "autoCorrect": false,
            "placeholderSkin": "defTextBoxPlaceholder"
        });
        var flxErrorMessageFeature6 = new kony.ui.FlexContainer({
            "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
            "clipBounds": true,
            "id": "flxErrorMessageFeature6",
            "isVisible": true,
            "layoutType": kony.flex.FLOW_HORIZONTAL,
            "left": "0dp",
            "isModalContainer": false,
            "skin": "slFbox",
            "top": "110dp",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, {}, {});
        flxErrorMessageFeature6.setDefaultUnit(kony.flex.DP);
        var lblErrorIconFeature6 = new kony.ui.Label({
            "height": "15dp",
            "id": "lblErrorIconFeature6",
            "isVisible": true,
            "left": "0dp",
            "skin": "sknErrorIcon",
            "text": "",
            "top": "0dp",
            "width": "15dp",
            "zIndex": 1
        }, {
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, {});
        var lblErrorTextFeature6 = new kony.ui.Label({
            "height": "15dp",
            "id": "lblErrorTextFeature6",
            "isVisible": true,
            "left": "0dp",
            "skin": "sknlblError",
            "i18n_text": "kony.i18n.getLocalizedString(\"i18n.products.Default_Value_cannot_be_empty\")",
            "top": "0dp",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, {
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, {});
        flxErrorMessageFeature6.add(lblErrorIconFeature6, lblErrorTextFeature6);
        flxValueContainer.add(fontIconSelectOptionDefaultValue, lblDefaultValue, tbxValue, flxErrorMessageFeature6);
        var flxDescriptionContainer = new kony.ui.FlexContainer({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "clipBounds": true,
            "height": "100px",
            "id": "flxDescriptionContainer",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "left": "30%",
            "isModalContainer": false,
            "skin": "slFbox",
            "top": "5px",
            "width": "65%",
            "zIndex": 1
        }, {}, {});
        flxDescriptionContainer.setDefaultUnit(kony.flex.DP);
        var lblFeatureDescTextCounter = new kony.ui.Label({
            "id": "lblFeatureDescTextCounter",
            "isVisible": true,
            "right": "0px",
            "skin": "sknlblLato696c7313px",
            "text": "1/100",
            "top": "0px",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, {
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, {});
        var txtAreaFeatureDescription = new kony.ui.TextArea2({
            "autoCapitalize": constants.TEXTAREA_AUTO_CAPITALIZE_NONE,
            "bottom": "0px",
            "focusSkin": "defTextAreaFocus",
            "height": "72px",
            "id": "txtAreaFeatureDescription",
            "isVisible": true,
            "keyBoardStyle": constants.TEXTAREA_KEY_BOARD_STYLE_DEFAULT,
            "left": "0px",
            "maxTextLength": 100,
            "numberOfVisibleLines": 3,
            "i18n_placeholder": "kony.i18n.getLocalizedString(\"i18n.products.Description\")",
            "skin": "skntxtAread7d9e0",
            "textInputMode": constants.TEXTAREA_INPUT_MODE_ANY,
            "width": "100%",
            "zIndex": 1
        }, {
            "contentAlignment": constants.CONTENT_ALIGN_TOP_LEFT,
            "padding": [2, 2, 2, 2],
            "paddingInPixel": false
        }, {
            "autoCorrect": false,
            "placeholderSkin": "defTextAreaPlaceholder"
        });
        flxDescriptionContainer.add(lblFeatureDescTextCounter, txtAreaFeatureDescription);
        var flxDeleteIcon = new kony.ui.FlexContainer({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "centerY": "65px",
            "clipBounds": true,
            "height": "22px",
            "id": "flxDeleteIcon",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "left": "97%",
            "isModalContainer": false,
            "skin": "slFbox",
            "width": "22px",
            "zIndex": 1
        }, {}, {});
        flxDeleteIcon.setDefaultUnit(kony.flex.DP);
        var lblIconDelete = new kony.ui.Label({
            "id": "lblIconDelete",
            "isVisible": true,
            "left": "0px",
            "skin": "sknIcon22px192b45",
            "text": "",
            "top": "0px",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, {
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, {});
        flxDeleteIcon.add(lblIconDelete);
        flxSegProductFeatureOption.add(flxValueContainer, flxDescriptionContainer, flxDeleteIcon);
        return flxSegProductFeatureOption;
    }
})