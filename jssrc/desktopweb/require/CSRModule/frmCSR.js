define("CSRModule/frmCSR", function() {
    return function(controller) {
        function addWidgetsfrmCSR() {
            this.setDefaultUnit(kony.flex.DP);
            var flxMain = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "900dp",
                "id": "flxMain",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "slFbox0b7aaa2e3bfa340",
                "top": "0dp",
                "width": "100%",
                "zIndex": 1
            }, {}, {});
            flxMain.setDefaultUnit(kony.flex.DP);
            var draggableArea = new kony.ui.CustomWidget({
                "id": "draggableArea",
                "isVisible": true,
                "left": "0px",
                "top": "-20px",
                "width": "100%",
                "height": "100%",
                "zIndex": 5,
                "clipBounds": true
            }, {
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {
                "widgetName": "draggableChart"
            });
            var flxLeftPannel = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "100%",
                "id": "flxLeftPannel",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "masterType": constants.MASTER_TYPE_USERWIDGET,
                "isModalContainer": false,
                "skin": "sknLeftPannel0bb79e4bcca8e45",
                "top": "0dp",
                "width": "305px",
                "zIndex": 5
            }, {}, {});
            flxLeftPannel.setDefaultUnit(kony.flex.DP);
            var leftMenuNew = new com.adminConsole.navigation.leftMenuNew({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "height": "100%",
                "id": "leftMenuNew",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "masterType": constants.MASTER_TYPE_DEFAULT,
                "isModalContainer": false,
                "skin": "sknFlxBg003E75Op100",
                "top": "0dp",
                "width": "100%",
                "overrides": {
                    "leftMenuNew": {
                        "bottom": "viz.val_cleared",
                        "centerX": "viz.val_cleared",
                        "centerY": "viz.val_cleared",
                        "height": "100%",
                        "left": "0dp",
                        "maxHeight": "viz.val_cleared",
                        "maxWidth": "viz.val_cleared",
                        "minHeight": "viz.val_cleared",
                        "minWidth": "viz.val_cleared",
                        "right": "viz.val_cleared",
                        "top": "0dp",
                        "width": "100%"
                    }
                }
            }, {
                "overrides": {}
            }, {
                "overrides": {}
            });
            flxLeftPannel.add(leftMenuNew);
            var flxRightPannel = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "100%",
                "id": "flxRightPannel",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "305px",
                "isModalContainer": false,
                "right": "0dp",
                "skin": "slFbox0b7aaa2e3bfa340",
                "zIndex": 5
            }, {}, {});
            flxRightPannel.setDefaultUnit(kony.flex.DP);
            var flxMainHeader = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "106dp",
                "id": "flxMainHeader",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0dp",
                "width": "100%",
                "zIndex": 1
            }, {}, {});
            flxMainHeader.setDefaultUnit(kony.flex.DP);
            var mainHeader = new com.adminConsole.header.mainHeader({
                "height": "106px",
                "id": "mainHeader",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "masterType": constants.MASTER_TYPE_DEFAULT,
                "isModalContainer": false,
                "skin": "CopyslFbox1",
                "width": "100%",
                "overrides": {
                    "btnAddNewOption": {
                        "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmCSRController.TEMPLATES\")",
                        "right": "212px"
                    },
                    "btnDropdownList": {
                        "text": "CREATE NEW MESSAGE"
                    },
                    "flxMainHeader": {
                        "left": undefined,
                        "top": undefined
                    },
                    "imgLogout": {
                        "right": "0px",
                        "src": "img_logout.png"
                    },
                    "lblHeading": {
                        "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmCSRController.New_Messages\")"
                    },
                    "lblUserName": {
                        "right": "25px",
                        "text": "Preetish",
                        "top": "viz.val_cleared"
                    },
                    "mainHeader": {
                        "left": undefined,
                        "top": undefined
                    }
                }
            }, {
                "overrides": {}
            }, {
                "overrides": {}
            });
            flxMainHeader.add(mainHeader);
            var flxHeaderDropdown = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                "clipBounds": true,
                "id": "flxHeaderDropdown",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "isModalContainer": false,
                "right": "35dp",
                "skin": "slFbox",
                "top": "20dp",
                "width": "400dp",
                "zIndex": 5
            }, {}, {});
            flxHeaderDropdown.setDefaultUnit(kony.flex.DP);
            var dropdownMainHeader = new com.adminConsole.common.dropdownMainHeader({
                "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                "id": "dropdownMainHeader",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "masterType": constants.MASTER_TYPE_DEFAULT,
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0dp",
                "width": "100%",
                "overrides": {
                    "flxDropdown": {
                        "isVisible": false
                    },
                    "imgUpArrow": {
                        "src": "uparrow_2x.png"
                    }
                }
            }, {
                "overrides": {}
            }, {
                "overrides": {}
            });
            flxHeaderDropdown.add(dropdownMainHeader);
            var flxBreadCrumbs = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "30px",
                "id": "flxBreadCrumbs",
                "isVisible": false,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "masterType": constants.MASTER_TYPE_USERWIDGET,
                "isModalContainer": false,
                "skin": "sknflxf5f6f8Op100",
                "top": "95px",
                "width": "100%",
                "zIndex": 1
            }, {}, {});
            flxBreadCrumbs.setDefaultUnit(kony.flex.DP);
            var breadcrumbs = new com.adminConsole.common.breadcrumbs({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "height": "20px",
                "id": "breadcrumbs",
                "isVisible": true,
                "layoutType": kony.flex.FLOW_HORIZONTAL,
                "left": "0dp",
                "masterType": constants.MASTER_TYPE_DEFAULT,
                "isModalContainer": false,
                "skin": "sknflxf5f6f8Op100",
                "top": "1px",
                "width": "100%",
                "overrides": {
                    "breadcrumbs": {
                        "bottom": "viz.val_cleared",
                        "top": "1px",
                        "width": "100%"
                    },
                    "imgBreadcrumbsDown": {
                        "src": "img_down_arrow.png"
                    },
                    "imgBreadcrumbsRight": {
                        "left": "10dp",
                        "right": 10,
                        "src": "img_breadcrumb_arrow.png"
                    },
                    "imgBreadcrumbsRight2": {
                        "left": "10dp",
                        "right": 10,
                        "src": "img_breadcrumb_arrow.png"
                    },
                    "imgBreadcrumbsRight3": {
                        "src": "img_breadcrumb_arrow.png"
                    }
                }
            }, {
                "overrides": {}
            }, {
                "overrides": {}
            });
            var flxHeaderSeperator = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "bottom": "1px",
                "clipBounds": true,
                "height": "1px",
                "id": "flxHeaderSeperator",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "35dp",
                "isModalContainer": false,
                "right": "35dp",
                "skin": "sknflx115678",
                "zIndex": 1
            }, {}, {});
            flxHeaderSeperator.setDefaultUnit(kony.flex.DP);
            flxHeaderSeperator.add();
            flxBreadCrumbs.add(breadcrumbs, flxHeaderSeperator);
            var flxTemplates = new kony.ui.FlexContainer({
                "clipBounds": true,
                "height": "80%",
                "id": "flxTemplates",
                "isVisible": false,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0px",
                "isModalContainer": false,
                "right": "0px",
                "skin": "slFbox",
                "top": "146px",
                "zIndex": 1
            }, {}, {});
            flxTemplates.setDefaultUnit(kony.flex.DP);
            var flxNoTemplate = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "bottom": "0px",
                "clipBounds": true,
                "height": "340px",
                "id": "flxNoTemplate",
                "isVisible": false,
                "layoutType": kony.flex.FREE_FORM,
                "left": "35px",
                "isModalContainer": false,
                "right": "35px",
                "skin": "slFbox",
                "top": "15px",
                "zIndex": 1
            }, {}, {});
            flxNoTemplate.setDefaultUnit(kony.flex.DP);
            var noTemplateData = new com.adminConsole.staticContent.noStaticData({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "height": "340px",
                "id": "noTemplateData",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0px",
                "masterType": constants.MASTER_TYPE_DEFAULT,
                "isModalContainer": false,
                "right": "0px",
                "skin": "slFbox",
                "top": "0dp",
                "width": "100%",
                "zIndex": 1,
                "overrides": {
                    "btnAddStaticContent": {
                        "maxWidth": undefined,
                        "text": "ADD TEMPLATE",
                        "width": undefined
                    },
                    "lblNoStaticContentCreated": {
                        "text": "No message template has been created yet."
                    },
                    "lblNoStaticContentMsg": {
                        "text": "Click on \"Add Template\" to continue"
                    },
                    "noStaticData": {
                        "centerX": undefined,
                        "left": "0px",
                        "right": "0px",
                        "width": "100%"
                    }
                }
            }, {
                "overrides": {}
            }, {
                "overrides": {}
            });
            flxNoTemplate.add(noTemplateData);
            var flxTemplateView = new kony.ui.FlexScrollContainer({
                "allowHorizontalBounce": false,
                "allowVerticalBounce": true,
                "bounces": true,
                "clipBounds": false,
                "enableScrolling": true,
                "height": "100%",
                "horizontalScrollIndicator": true,
                "id": "flxTemplateView",
                "isVisible": false,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0px",
                "pagingEnabled": false,
                "right": "0px",
                "scrollDirection": kony.flex.SCROLL_VERTICAL,
                "skin": "slFSbox",
                "top": "-8px",
                "verticalScrollIndicator": true,
                "zIndex": 1
            }, {}, {});
            flxTemplateView.setDefaultUnit(kony.flex.DP);
            var flxMainSubHeader = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "63dp",
                "id": "flxMainSubHeader",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0px",
                "isModalContainer": false,
                "right": "0px",
                "skin": "slFbox",
                "top": "0dp",
                "zIndex": 101
            }, {}, {});
            flxMainSubHeader.setDefaultUnit(kony.flex.DP);
            var subHeader = new com.adminConsole.header.subHeader({
                "height": "48px",
                "id": "subHeader",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0px",
                "masterType": constants.MASTER_TYPE_DEFAULT,
                "isModalContainer": false,
                "right": "0px",
                "skin": "CopyslFbox2",
                "top": "0px",
                "width": "100%",
                "overrides": {
                    "flxMenu": {
                        "isVisible": false,
                        "left": "35px"
                    },
                    "flxSearch": {
                        "left": "viz.val_cleared",
                        "right": 32,
                        "top": "0px"
                    },
                    "flxSubHeader": {
                        "left": "0px"
                    },
                    "lblRecords": {
                        "left": "0px"
                    },
                    "subHeader": {
                        "height": "48px",
                        "left": "0px",
                        "right": "0px",
                        "top": "0px",
                        "width": "100%"
                    }
                }
            }, {
                "overrides": {}
            }, {
                "overrides": {}
            });
            subHeader.tbxSearchBox.onTouchStart = controller.AS_TextField_d44f6a097be240fd8d18f891d00da7e9;
            flxMainSubHeader.add(subHeader);
            var flxTemplate = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "85%",
                "id": "flxTemplate",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0px",
                "isModalContainer": false,
                "right": "0px",
                "skin": "slFbox",
                "top": "51px",
                "zIndex": 100
            }, {}, {});
            flxTemplate.setDefaultUnit(kony.flex.DP);
            var flxLocationsSegment = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "220dp",
                "id": "flxLocationsSegment",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "35px",
                "isModalContainer": false,
                "right": "35px",
                "skin": "sknflxffffffoptemplateop3px",
                "top": "0dp",
                "zIndex": 1
            }, {}, {});
            flxLocationsSegment.setDefaultUnit(kony.flex.DP);
            var flxGroupsAdvSearchHeader = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "61px",
                "id": "flxGroupsAdvSearchHeader",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "20px",
                "isModalContainer": false,
                "right": "20px",
                "skin": "slFbox",
                "top": "0"
            }, {}, {});
            flxGroupsAdvSearchHeader.setDefaultUnit(kony.flex.DP);
            var flxGroupSegHeader = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "centerX": "50%",
                "clipBounds": true,
                "height": "60px",
                "id": "flxGroupSegHeader",
                "isVisible": true,
                "layoutType": kony.flex.FLOW_HORIZONTAL,
                "isModalContainer": false,
                "right": "20px",
                "skin": "sknflxffffffop100",
                "top": "0px",
                "width": "100%"
            }, {}, {});
            flxGroupSegHeader.setDefaultUnit(kony.flex.DP);
            var flxHederName = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "100%",
                "id": "flxHederName",
                "isVisible": true,
                "layoutType": kony.flex.FLOW_HORIZONTAL,
                "left": "20px",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0dp",
                "width": "50%",
                "zIndex": 1
            }, {}, {});
            flxHederName.setDefaultUnit(kony.flex.DP);
            var lblUsersHeaderName = new kony.ui.Label({
                "centerY": "50%",
                "id": "lblUsersHeaderName",
                "isVisible": true,
                "left": "0px",
                "skin": "sknlblLato5d6c7f12px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.permission.NAME\")",
                "top": 0,
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {
                "hoverSkin": "sknlblLato00000011px"
            });
            var fontIconUsersHeaderName = new kony.ui.Label({
                "centerY": "50%",
                "height": "15px",
                "id": "fontIconUsersHeaderName",
                "isVisible": true,
                "left": "5px",
                "skin": "sknIcon15px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmAlertsManagement.lblSortName\")",
                "top": 0,
                "width": "15px",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxHederName.add(lblUsersHeaderName, fontIconUsersHeaderName);
            var flxCreated = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "100%",
                "id": "flxCreated",
                "isVisible": true,
                "layoutType": kony.flex.FLOW_HORIZONTAL,
                "left": "0px",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0dp",
                "width": "20%",
                "zIndex": 1
            }, {}, {});
            flxCreated.setDefaultUnit(kony.flex.DP);
            var lblCreated = new kony.ui.Label({
                "centerY": "50%",
                "id": "lblCreated",
                "isVisible": true,
                "left": "0px",
                "skin": "sknlblLato5d6c7f12px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmCSR.lblCreated\")",
                "top": 0,
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {
                "hoverSkin": "sknlblLato00000011px"
            });
            var fontIconUsername = new kony.ui.Label({
                "centerY": "50%",
                "height": "15px",
                "id": "fontIconUsername",
                "isVisible": true,
                "left": "5px",
                "skin": "sknIcon15px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmAlertsManagement.lblSortName\")",
                "top": 0,
                "width": "15px",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxCreated.add(lblCreated, fontIconUsername);
            var flxLastUpdate = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "100%",
                "id": "flxLastUpdate",
                "isVisible": true,
                "layoutType": kony.flex.FLOW_HORIZONTAL,
                "left": "0px",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0dp",
                "width": "19%",
                "zIndex": 1
            }, {}, {});
            flxLastUpdate.setDefaultUnit(kony.flex.DP);
            var lblLastUpdated = new kony.ui.Label({
                "centerY": "50%",
                "id": "lblLastUpdated",
                "isVisible": true,
                "left": "0px",
                "skin": "sknlblLato5d6c7f12px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmCSR.lblLastUpdated\")",
                "top": 0,
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {
                "hoverSkin": "sknlblLato00000011px"
            });
            var fontIconLastUpdated = new kony.ui.Label({
                "centerY": "50%",
                "height": "15px",
                "id": "fontIconLastUpdated",
                "isVisible": true,
                "left": "5px",
                "skin": "sknIcon15px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmAlertsManagement.lblSortName\")",
                "top": 0,
                "width": "15px",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxLastUpdate.add(lblLastUpdated, fontIconLastUpdated);
            flxGroupSegHeader.add(flxHederName, flxCreated, flxLastUpdate);
            var lblSeperator = new kony.ui.Label({
                "bottom": "0px",
                "height": "1px",
                "id": "lblSeperator",
                "isVisible": true,
                "left": "0px",
                "right": "0px",
                "skin": "sknLblTableHeaderLine",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmCSR.lblSeperator\")",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxGroupsAdvSearchHeader.add(flxGroupSegHeader, lblSeperator);
            var listingSegmentClient = new com.adminConsole.common.listingSegmentClient({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "bottom": "10px",
                "id": "listingSegmentClient",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "20px",
                "masterType": constants.MASTER_TYPE_DEFAULT,
                "isModalContainer": false,
                "right": "20px",
                "skin": "slFbox",
                "top": "61px",
                "overrides": {
                    "contextualMenu.imgDownArrow": {
                        "src": "downarrow_2x.png"
                    },
                    "contextualMenu.imgOption1": {
                        "src": "imagedrag.png"
                    },
                    "contextualMenu.imgOption2": {
                        "src": "edit2x.png"
                    },
                    "contextualMenu.imgOption3": {
                        "src": "imagedrag.png"
                    },
                    "contextualMenu.imgOption4": {
                        "src": "deactive_2x.png"
                    },
                    "contextualMenu.imgUpArrow": {
                        "src": "uparrow_2x.png"
                    },
                    "flxListingSegmentWrapper": {
                        "bottom": "10px",
                        "height": "viz.val_cleared",
                        "right": "viz.val_cleared"
                    },
                    "flxPagination": {
                        "isVisible": false
                    },
                    "listingSegmentClient": {
                        "bottom": "10px",
                        "centerX": "viz.val_cleared",
                        "centerY": "viz.val_cleared",
                        "height": "viz.val_cleared",
                        "left": "20px",
                        "maxHeight": "viz.val_cleared",
                        "maxWidth": "viz.val_cleared",
                        "minHeight": "viz.val_cleared",
                        "minWidth": "viz.val_cleared",
                        "right": "20px",
                        "top": "61px",
                        "width": "viz.val_cleared"
                    },
                    "segListing": {
                        "bottom": "10px",
                        "height": "100%",
                        "left": "0px",
                        "top": "0px"
                    }
                }
            }, {
                "overrides": {}
            }, {
                "overrides": {}
            });
            flxLocationsSegment.add(flxGroupsAdvSearchHeader, listingSegmentClient);
            flxTemplate.add(flxLocationsSegment);
            flxTemplateView.add(flxMainSubHeader, flxTemplate);
            var flxAddTemplates = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "100%",
                "id": "flxAddTemplates",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0px",
                "isModalContainer": false,
                "right": "0dp",
                "skin": "slFbox",
                "top": "20px",
                "zIndex": 1
            }, {}, {});
            flxAddTemplates.setDefaultUnit(kony.flex.DP);
            var flxCreateTempplate = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                "bottom": "25dp",
                "clipBounds": true,
                "id": "flxCreateTempplate",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "35px",
                "isModalContainer": false,
                "right": "35px",
                "skin": "sknflxffffffoptemplateop3px",
                "top": "0px",
                "zIndex": 1
            }, {}, {});
            flxCreateTempplate.setDefaultUnit(kony.flex.DP);
            var flxTemplateAddInner = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                "clipBounds": true,
                "id": "flxTemplateAddInner",
                "isVisible": true,
                "layoutType": kony.flex.FLOW_VERTICAL,
                "left": "20px",
                "isModalContainer": false,
                "right": "20px",
                "skin": "CopyslFbox",
                "top": "20px",
                "zIndex": 1
            }, {}, {});
            flxTemplateAddInner.setDefaultUnit(kony.flex.DP);
            var flxTemplatesAddSpace = new kony.ui.FlexScrollContainer({
                "allowHorizontalBounce": false,
                "allowVerticalBounce": true,
                "bounces": true,
                "clipBounds": true,
                "enableScrolling": true,
                "height": "500dp",
                "horizontalScrollIndicator": true,
                "id": "flxTemplatesAddSpace",
                "isVisible": true,
                "layoutType": kony.flex.FLOW_VERTICAL,
                "left": "0px",
                "pagingEnabled": false,
                "scrollDirection": kony.flex.SCROLL_VERTICAL,
                "skin": "slFSbox",
                "top": "0px",
                "verticalScrollIndicator": true,
                "width": "100%",
                "zIndex": 1
            }, {}, {});
            flxTemplatesAddSpace.setDefaultUnit(kony.flex.DP);
            var lblMessageHeader = new kony.ui.Label({
                "id": "lblMessageHeader",
                "isVisible": true,
                "left": "0px",
                "right": "35px",
                "skin": "sknlblLato5d6c7f12px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmCSR.lblMessageHeader\")",
                "top": "20px",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var flxname = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "30px",
                "id": "flxname",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "isModalContainer": false,
                "right": "20dp",
                "skin": "slFbox",
                "top": "5px",
                "width": "96%"
            }, {}, {});
            flxname.setDefaultUnit(kony.flex.DP);
            var lblCharCount = new kony.ui.Label({
                "centerY": "50%",
                "id": "lblCharCount",
                "isVisible": false,
                "right": "0px",
                "skin": "sknLatoRegular485c7514px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmCSR.lblCharCount\")",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var lblTemplateName = new kony.ui.Label({
                "centerY": "49%",
                "id": "lblTemplateName",
                "isVisible": true,
                "left": "-1px",
                "skin": "sknLatoRegular485c7514px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmCSR.lblTemplateName\")",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxname.add(lblCharCount, lblTemplateName);
            var txtfldAddressLine1 = new kony.ui.TextBox2({
                "autoCapitalize": constants.TEXTBOX_AUTO_CAPITALIZE_NONE,
                "focusSkin": "skntxtbxDetailsFocus14px",
                "height": "40dp",
                "id": "txtfldAddressLine1",
                "isVisible": true,
                "keyBoardStyle": constants.TEXTBOX_KEY_BOARD_STYLE_DEFAULT,
                "left": "0dp",
                "maxTextLength": 80,
                "placeholder": "Template name",
                "right": "35px",
                "secureTextEntry": false,
                "skin": "skntxtbxDetails0bbf1235271384a",
                "textInputMode": constants.TEXTBOX_INPUT_MODE_ANY,
                "top": "0px",
                "width": "96%",
                "zIndex": 2
            }, {
                "containerHeightMode": constants.TEXTBOX_FONT_METRICS_DRIVEN_HEIGHT,
                "contentAlignment": constants.CONTENT_ALIGN_TOP_LEFT,
                "padding": [1, 0, 0, 0],
                "paddingInPixel": false
            }, {
                "autoCorrect": false
            });
            var flxNoTemplateNameError = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "20px",
                "id": "flxNoTemplateNameError",
                "isVisible": false,
                "layoutType": kony.flex.FLOW_HORIZONTAL,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "5px",
                "width": "100%"
            }, {}, {});
            flxNoTemplateNameError.setDefaultUnit(kony.flex.DP);
            var lblNoTemplateNameErrorIcon = new kony.ui.Label({
                "height": "15dp",
                "id": "lblNoTemplateNameErrorIcon",
                "isVisible": true,
                "left": "0dp",
                "skin": "sknErrorIcon",
                "text": "",
                "top": "0dp",
                "width": "15dp",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var lblNoTemplateNameError = new kony.ui.Label({
                "height": "15px",
                "id": "lblNoTemplateNameError",
                "isVisible": true,
                "left": "0px",
                "skin": "sknlblError",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmCSR.lblNoTemplateNameError\")",
                "top": "0px",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 2
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxNoTemplateNameError.add(lblNoTemplateNameErrorIcon, lblNoTemplateNameError);
            var lblTemplateBody = new kony.ui.Label({
                "height": "15px",
                "id": "lblTemplateBody",
                "isVisible": true,
                "left": "0px",
                "skin": "sknLatoRegular485c7514px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmCSR.lblTemplateBody\")",
                "top": "15px",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var flxScrollAddTemplate = new kony.ui.FlexScrollContainer({
                "allowHorizontalBounce": false,
                "allowVerticalBounce": true,
                "bounces": true,
                "clipBounds": true,
                "enableScrolling": true,
                "height": "305px",
                "horizontalScrollIndicator": true,
                "id": "flxScrollAddTemplate",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0px",
                "pagingEnabled": false,
                "right": "40px",
                "scrollDirection": kony.flex.SCROLL_VERTICAL,
                "skin": "CopyslFSbox0dc3a490e7a1546",
                "top": "10px",
                "verticalScrollIndicator": true,
                "width": "96%",
                "zIndex": 1
            }, {}, {});
            flxScrollAddTemplate.setDefaultUnit(kony.flex.DP);
            var rtxTemplate = new kony.ui.Browser({
                "detectTelNumber": true,
                "enableZoom": false,
                "height": "100%",
                "id": "rtxTemplate",
                "isVisible": true,
                "left": "0px",
                "setAsContent": false,
                "requestURLConfig": {
                    "URL": "richtext.html",
                    "requestMethod": constants.BROWSER_REQUEST_METHOD_GET
                },
                "right": "0px",
                "top": "0px",
                "zIndex": 1
            }, {}, {});
            flxScrollAddTemplate.add(rtxTemplate);
            var flxNoTemplateDescError = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "20px",
                "id": "flxNoTemplateDescError",
                "isVisible": false,
                "layoutType": kony.flex.FLOW_HORIZONTAL,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "5px",
                "width": "100%"
            }, {}, {});
            flxNoTemplateDescError.setDefaultUnit(kony.flex.DP);
            var lblNoTemplateDescErrorIcon = new kony.ui.Label({
                "height": "15dp",
                "id": "lblNoTemplateDescErrorIcon",
                "isVisible": true,
                "left": "0dp",
                "skin": "sknErrorIcon",
                "text": "",
                "top": "0dp",
                "width": "15dp",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var lblNoTemplateDescription = new kony.ui.Label({
                "height": "15px",
                "id": "lblNoTemplateDescription",
                "isVisible": true,
                "left": "0px",
                "skin": "sknlblError",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmCSR.lblNoTemplateDescription\")",
                "top": "1px",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxNoTemplateDescError.add(lblNoTemplateDescErrorIcon, lblNoTemplateDescription);
            var flxNotes = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "35px",
                "id": "flxNotes",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": 0,
                "isModalContainer": false,
                "right": "0px",
                "skin": "slFbox",
                "top": "10px",
                "width": "96%"
            }, {}, {});
            flxNotes.setDefaultUnit(kony.flex.DP);
            var lblAdditionalNote = new kony.ui.Label({
                "centerY": "50%",
                "id": "lblAdditionalNote",
                "isVisible": true,
                "left": "0px",
                "skin": "sknLatoRegular485c7514px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmCSR.lblAdditionalNote\")",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var lblCharCount2 = new kony.ui.Label({
                "centerY": "50%",
                "height": "15px",
                "id": "lblCharCount2",
                "isVisible": true,
                "right": "0px",
                "skin": "sknLatoRegular485c7514px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmCSR.lblCharCount2\")",
                "width": "70px",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_RIGHT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxNotes.add(lblAdditionalNote, lblCharCount2);
            var flxStaticContantData = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "140px",
                "id": "flxStaticContantData",
                "isVisible": true,
                "layoutType": kony.flex.FLOW_VERTICAL,
                "left": "0px",
                "isModalContainer": false,
                "right": "35px",
                "skin": "sknflxffffffop0d0b87507b0274eSe",
                "top": "0px",
                "width": "96%",
                "zIndex": 1
            }, {}, {});
            flxStaticContantData.setDefaultUnit(kony.flex.DP);
            var richTextData = new kony.ui.RichText({
                "height": "110px",
                "id": "richTextData",
                "isVisible": false,
                "left": "0px",
                "right": "0px",
                "skin": "sknrtxLato35475f14px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmCSR.richTextData\")",
                "top": "0px",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_TOP_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {
                "linkFocusSkin": "sknrtxLato35475f14px"
            });
            var txtbxTextData = new kony.ui.TextBox2({
                "autoCapitalize": constants.TEXTBOX_AUTO_CAPITALIZE_NONE,
                "focusSkin": "skntxtbxDetailsFocus14px",
                "height": "110px",
                "id": "txtbxTextData",
                "isVisible": false,
                "keyBoardStyle": constants.TEXTBOX_KEY_BOARD_STYLE_DEFAULT,
                "left": "0px",
                "right": "0px",
                "secureTextEntry": false,
                "skin": "skntxtbxDetails0bbf1235271384a",
                "textInputMode": constants.TEXTBOX_INPUT_MODE_ANY,
                "top": "0px",
                "zIndex": 2
            }, {
                "containerHeightMode": constants.TEXTBOX_FONT_METRICS_DRIVEN_HEIGHT,
                "contentAlignment": constants.CONTENT_ALIGN_TOP_LEFT,
                "padding": [1, 0, 0, 0],
                "paddingInPixel": false
            }, {
                "autoCorrect": false
            });
            var txtAreaData = new kony.ui.TextArea2({
                "autoCapitalize": constants.TEXTAREA_AUTO_CAPITALIZE_NONE,
                "focusSkin": "skntxtdescriptionHover",
                "height": "110px",
                "id": "txtAreaData",
                "isVisible": true,
                "keyBoardStyle": constants.TEXTAREA_KEY_BOARD_STYLE_DEFAULT,
                "left": "0px",
                "maxTextLength": 500,
                "numberOfVisibleLines": 3,
                "placeholder": "Add Note",
                "right": "0px",
                "skin": "skntxtAreaLato35475f14Px",
                "textInputMode": constants.TEXTAREA_INPUT_MODE_ANY,
                "top": "0px",
                "width": "100%",
                "zIndex": 2
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_TOP_LEFT,
                "padding": [2, 2, 2, 2],
                "paddingInPixel": false
            }, {
                "autoCorrect": false
            });
            flxStaticContantData.add(richTextData, txtbxTextData, txtAreaData);
            flxTemplatesAddSpace.add(lblMessageHeader, flxname, txtfldAddressLine1, flxNoTemplateNameError, lblTemplateBody, flxScrollAddTemplate, flxNoTemplateDescError, flxNotes, flxStaticContantData);
            var flxSeperator = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "bottom": "1px",
                "clipBounds": true,
                "height": "1px",
                "id": "flxSeperator",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "isModalContainer": false,
                "right": "35dp",
                "skin": "sknflx115678",
                "top": "40dp",
                "width": "100%",
                "zIndex": 1
            }, {}, {});
            flxSeperator.setDefaultUnit(kony.flex.DP);
            flxSeperator.add();
            var flxScrollAddCommonBtn = new kony.ui.FlexContainer({
                "clipBounds": true,
                "height": "80px",
                "id": "flxScrollAddCommonBtn",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0px",
                "isModalContainer": false,
                "right": "35px",
                "skin": "slFbox",
                "top": "20px",
                "zIndex": 1
            }, {}, {});
            flxScrollAddCommonBtn.setDefaultUnit(kony.flex.DP);
            var commonButtons = new com.adminConsole.common.commonButtons({
                "height": "80px",
                "id": "commonButtons",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0px",
                "masterType": constants.MASTER_TYPE_DEFAULT,
                "isModalContainer": false,
                "right": "0px",
                "skin": "slFbox",
                "top": "0",
                "width": "100%",
                "overrides": {
                    "btnCancel": {
                        "top": undefined
                    },
                    "btnNext": {
                        "isVisible": false
                    },
                    "btnSave": {
                        "right": "0px"
                    },
                    "commonButtons": {
                        "bottom": undefined,
                        "height": "80px",
                        "left": "0px",
                        "right": "0px",
                        "top": "0",
                        "width": "100%"
                    },
                    "flxRightButtons": {
                        "left": undefined,
                        "right": "0px"
                    }
                }
            }, {
                "overrides": {}
            }, {
                "overrides": {}
            });
            flxScrollAddCommonBtn.add(commonButtons);
            flxTemplateAddInner.add(flxTemplatesAddSpace, flxSeperator, flxScrollAddCommonBtn);
            flxCreateTempplate.add(flxTemplateAddInner);
            flxAddTemplates.add(flxCreateTempplate);
            var flxTemplatesDetails = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                "clipBounds": true,
                "id": "flxTemplatesDetails",
                "isVisible": false,
                "layoutType": kony.flex.FREE_FORM,
                "left": "35px",
                "isModalContainer": false,
                "right": "35px",
                "skin": "sknflxffffffoptemplateop3px",
                "top": "20px",
                "zIndex": 1
            }, {}, {});
            flxTemplatesDetails.setDefaultUnit(kony.flex.DP);
            var TemplateMessage = new com.adminConsole.CSR.TemplateMessage({
                "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                "id": "TemplateMessage",
                "isVisible": true,
                "layoutType": kony.flex.FLOW_VERTICAL,
                "masterType": constants.MASTER_TYPE_DEFAULT,
                "isModalContainer": false,
                "right": "0px",
                "skin": "sknflxffffffop100",
                "width": "100%",
                "overrides": {
                    "TemplateMessage": {
                        "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                        "right": "0px",
                        "width": "100%"
                    },
                    "flxAddressDetails": {
                        "left": "0dp"
                    },
                    "flxButtons": {
                        "right": "20px"
                    },
                    "flxTemplateBody": {
                        "right": "0px",
                        "top": "viz.val_cleared",
                        "width": "100%"
                    },
                    "lblAdditionalNoteHeader": {
                        "left": "20px"
                    },
                    "lblAdditionalNoteValue": {
                        "left": "20px"
                    },
                    "lblTemplateBodyHeader": {
                        "left": "20px"
                    },
                    "rtxAddressAdditional1": {
                        "left": "20px",
                        "right": "20px"
                    }
                }
            }, {
                "overrides": {}
            }, {
                "overrides": {}
            });
            flxTemplatesDetails.add(TemplateMessage);
            flxTemplates.add(flxNoTemplate, flxTemplateView, flxAddTemplates, flxTemplatesDetails);
            var flxScrollMainContent = new kony.ui.FlexScrollContainer({
                "allowHorizontalBounce": false,
                "allowVerticalBounce": true,
                "bounces": true,
                "clipBounds": false,
                "enableScrolling": true,
                "height": "738px",
                "horizontalScrollIndicator": true,
                "id": "flxScrollMainContent",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "pagingEnabled": false,
                "scrollDirection": kony.flex.SCROLL_VERTICAL,
                "skin": "sknscrollflxf5f6f8Op100",
                "top": "125px",
                "verticalScrollIndicator": true,
                "zIndex": 1
            }, {}, {});
            flxScrollMainContent.setDefaultUnit(kony.flex.DP);
            var flxLocationsWrapper = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "62px",
                "id": "flxLocationsWrapper",
                "isVisible": true,
                "layoutType": kony.flex.FLOW_VERTICAL,
                "left": "35dp",
                "isModalContainer": false,
                "right": "35dp",
                "skin": "slFbox",
                "top": "20dp",
                "zIndex": 100
            }, {}, {});
            flxLocationsWrapper.setDefaultUnit(kony.flex.DP);
            var detailHeader = new com.adminConsole.CSR.detailHeader({
                "height": "62px",
                "id": "detailHeader",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "masterType": constants.MASTER_TYPE_DEFAULT,
                "isModalContainer": false,
                "skin": "slFbox",
                "width": "625px",
                "overrides": {
                    "flxDetials2": {
                        "width": "100px"
                    },
                    "lblStatus2": {
                        "left": "0px"
                    }
                }
            }, {
                "overrides": {}
            }, {
                "overrides": {}
            });
            flxLocationsWrapper.add(detailHeader);
            var flxNewMessagePage = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                "clipBounds": true,
                "id": "flxNewMessagePage",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "35px",
                "isModalContainer": false,
                "right": "35px",
                "skin": "csrBackgroundSkinffffff1pxBore5e8ef",
                "top": "100px",
                "zIndex": 1
            }, {}, {});
            flxNewMessagePage.setDefaultUnit(kony.flex.DP);
            var MessageFilter = new com.adminConsole.CSR.MessageFilter({
                "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                "id": "MessageFilter",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "masterType": constants.MASTER_TYPE_DEFAULT,
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0dp",
                "width": "100%",
                "zIndex": 10,
                "overrides": {
                    "MessageFilter": {
                        "isVisible": true
                    },
                    "SegSearchResult": {
                        "data": [{
                            "imgCheckBoxMsg": "checkbox.png",
                            "lblAssignto": "Edward Shwartz",
                            "lblCategory": "General banking",
                            "lblCustomerId": "John Doe(9), ",
                            "lblCustomerName": "John Doe(9), ",
                            "lblDate": "12.10.2017",
                            "lblDraft": "Draft",
                            "lblIconOptions": "",
                            "lblReply": "",
                            "lblRequestID": " RI333456",
                            "lblSeparator": "'",
                            "lblSubject": "Lorem ipsum dolor sitconsectetu.. (9).orem ipsum dolor sitconsectetu.. (orem ipsum dolor sitconsectetu.. (9)."
                        }, {
                            "imgCheckBoxMsg": "checkbox.png",
                            "lblAssignto": "Edward Shwartz",
                            "lblCategory": "General banking",
                            "lblCustomerId": "John Doe(9), ",
                            "lblCustomerName": "John Doe(9), ",
                            "lblDate": "12.10.2017",
                            "lblDraft": "Draft",
                            "lblIconOptions": "",
                            "lblReply": "",
                            "lblRequestID": " RI333456",
                            "lblSeparator": "'",
                            "lblSubject": "Lorem ipsum dolor sitconsectetu.. (9).orem ipsum dolor sitconsectetu.. (orem ipsum dolor sitconsectetu.. (9)."
                        }, {
                            "imgCheckBoxMsg": "checkbox.png",
                            "lblAssignto": "Edward Shwartz",
                            "lblCategory": "General banking",
                            "lblCustomerId": "John Doe(9), ",
                            "lblCustomerName": "John Doe(9), ",
                            "lblDate": "12.10.2017",
                            "lblDraft": "Draft",
                            "lblIconOptions": "",
                            "lblReply": "",
                            "lblRequestID": " RI333456",
                            "lblSeparator": "'",
                            "lblSubject": "Lorem ipsum dolor sitconsectetu.. (9).orem ipsum dolor sitconsectetu.. (orem ipsum dolor sitconsectetu.. (9)."
                        }]
                    },
                    "contextualMenu": {
                        "autogrowMode": kony.flex.AUTOGROW_HEIGHT
                    },
                    "contextualMenu.imgDownArrow": {
                        "src": "downarrow_2x.png"
                    },
                    "contextualMenu.imgOption1": {
                        "src": "imagedrag.png"
                    },
                    "contextualMenu.imgOption2": {
                        "src": "edit2x.png"
                    },
                    "contextualMenu.imgOption3": {
                        "src": "imagedrag.png"
                    },
                    "contextualMenu.imgOption4": {
                        "src": "deactive_2x.png"
                    },
                    "contextualMenu.imgUpArrow": {
                        "src": "uparrow_2x.png"
                    },
                    "flxClearSearchImage": {
                        "zIndex": 10
                    },
                    "flxRepliedOnDropdown": {
                        "isVisible": false
                    },
                    "flxSearchDownImg": {
                        "isVisible": false
                    },
                    "imgAssignedTo": {
                        "src": "dropdownarrow.png"
                    },
                    "imgCalender": {
                        "src": "calicon.png"
                    },
                    "imgCategory": {
                        "src": "dropdownarrow.png"
                    },
                    "imgClearSearch": {
                        "src": "close_blue.png"
                    },
                    "imgCustSortUsername": {
                        "src": "dropdownarrow.png"
                    },
                    "imgDate": {
                        "src": "dropdownarrow.png"
                    },
                    "imgRequestIdSortName": {
                        "src": "dropdownarrow.png"
                    },
                    "imgSearchIcon": {
                        "src": "search_1x.png"
                    },
                    "imgSubject": {
                        "src": "dropdownarrow.png"
                    },
                    "tbxSearchBox": {
                        "placeholder": "Search by Request ID, Username, Customer ID"
                    },
                    "txtfldAssignTo": {
                        "i18n_placeholder": "kony.i18n.getLocalizedString(\"i18n.frmCSRController.searchCSRname\")"
                    },
                    "txtfldRepliedby": {
                        "i18n_placeholder": "kony.i18n.getLocalizedString(\"i18n.frmCSRController.searchCSRname\")"
                    }
                }
            }, {
                "overrides": {}
            }, {
                "overrides": {}
            });
            var datePicker = new kony.ui.CustomWidget({
                "id": "datePicker",
                "isVisible": true,
                "left": "22px",
                "right": "3px",
                "bottom": "1px",
                "top": "100px",
                "width": "180px",
                "height": "30px",
                "zIndex": 10,
                "clipBounds": true
            }, {
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {
                "widgetName": "dateRangePicker",
                "event": null,
                "rangeType": "",
                "resetData": null,
                "type": "list",
                "value": ""
            });
            var flxCloseCal1 = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "15px",
                "id": "flxCloseCal1",
                "isVisible": false,
                "layoutType": kony.flex.FREE_FORM,
                "left": "185px",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "105px",
                "width": "15px",
                "zIndex": 11
            }, {}, {
                "hoverSkin": "sknCursor"
            });
            flxCloseCal1.setDefaultUnit(kony.flex.DP);
            var lblCloseCal1 = new kony.ui.Label({
                "centerY": "50%",
                "id": "lblCloseCal1",
                "isVisible": true,
                "right": "0dp",
                "skin": "sknfontIconDescDownArrow12px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmAlertsManagement.lblAlertPreviewClose\")",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxCloseCal1.add(lblCloseCal1);
            flxNewMessagePage.add(MessageFilter, datePicker, flxCloseCal1);
            var flxMyQueuePage = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                "clipBounds": true,
                "id": "flxMyQueuePage",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "35px",
                "isModalContainer": false,
                "right": "35px",
                "skin": "csrBackgroundSkinffffff1pxBore5e8ef",
                "top": "100px",
                "zIndex": 1
            }, {}, {});
            flxMyQueuePage.setDefaultUnit(kony.flex.DP);
            var myQueueMessageFilter = new com.adminConsole.CSR.MessageFilter({
                "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                "id": "myQueueMessageFilter",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "masterType": constants.MASTER_TYPE_DEFAULT,
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0dp",
                "width": "100%",
                "zIndex": 10,
                "overrides": {
                    "ListBoxStatus": {
                        "masterData": [
                            ["Select Status", "Select Status"],
                            ["SID_OPEN", "New"],
                            ["SID_INPROGRESS", "InProgress"]
                        ]
                    },
                    "SegSearchResult": {
                        "data": [{
                            "imgCheckBoxMsg": "checkbox.png",
                            "lblAssignto": "Edward Shwartz",
                            "lblCategory": "General banking",
                            "lblCustomerId": "John Doe(9), ",
                            "lblCustomerName": "John Doe(9), ",
                            "lblDate": "12.10.2017",
                            "lblDraft": "Draft",
                            "lblIconOptions": "",
                            "lblReply": "",
                            "lblRequestID": " RI333456",
                            "lblSeparator": "'",
                            "lblSubject": "Lorem ipsum dolor sitconsectetu.. (9).orem ipsum dolor sitconsectetu.. (orem ipsum dolor sitconsectetu.. (9)."
                        }, {
                            "imgCheckBoxMsg": "checkbox.png",
                            "lblAssignto": "Edward Shwartz",
                            "lblCategory": "General banking",
                            "lblCustomerId": "John Doe(9), ",
                            "lblCustomerName": "John Doe(9), ",
                            "lblDate": "12.10.2017",
                            "lblDraft": "Draft",
                            "lblIconOptions": "",
                            "lblReply": "",
                            "lblRequestID": " RI333456",
                            "lblSeparator": "'",
                            "lblSubject": "Lorem ipsum dolor sitconsectetu.. (9).orem ipsum dolor sitconsectetu.. (orem ipsum dolor sitconsectetu.. (9)."
                        }, {
                            "imgCheckBoxMsg": "checkbox.png",
                            "lblAssignto": "Edward Shwartz",
                            "lblCategory": "General banking",
                            "lblCustomerId": "John Doe(9), ",
                            "lblCustomerName": "John Doe(9), ",
                            "lblDate": "12.10.2017",
                            "lblDraft": "Draft",
                            "lblIconOptions": "",
                            "lblReply": "",
                            "lblRequestID": " RI333456",
                            "lblSeparator": "'",
                            "lblSubject": "Lorem ipsum dolor sitconsectetu.. (9).orem ipsum dolor sitconsectetu.. (orem ipsum dolor sitconsectetu.. (9)."
                        }]
                    },
                    "contextualMenu": {
                        "autogrowMode": kony.flex.AUTOGROW_HEIGHT
                    },
                    "contextualMenu.imgDownArrow": {
                        "src": "downarrow_2x.png"
                    },
                    "contextualMenu.imgOption1": {
                        "src": "imagedrag.png"
                    },
                    "contextualMenu.imgOption2": {
                        "src": "edit2x.png"
                    },
                    "contextualMenu.imgOption3": {
                        "src": "imagedrag.png"
                    },
                    "contextualMenu.imgOption4": {
                        "src": "deactive_2x.png"
                    },
                    "contextualMenu.imgUpArrow": {
                        "src": "uparrow_2x.png"
                    },
                    "flxClearSearchImage": {
                        "zIndex": 10
                    },
                    "flxColumn3": {
                        "isVisible": false
                    },
                    "flxColumn4": {
                        "isVisible": false
                    },
                    "flxColumn5": {
                        "isVisible": true
                    },
                    "flxSearchDownImg": {
                        "isVisible": false
                    },
                    "imgAssignedTo": {
                        "src": "dropdownarrow.png"
                    },
                    "imgCalender": {
                        "src": "calicon.png"
                    },
                    "imgCategory": {
                        "src": "dropdownarrow.png"
                    },
                    "imgClearSearch": {
                        "src": "close_blue.png"
                    },
                    "imgCustSortUsername": {
                        "src": "dropdownarrow.png"
                    },
                    "imgDate": {
                        "src": "dropdownarrow.png"
                    },
                    "imgRequestIdSortName": {
                        "src": "dropdownarrow.png"
                    },
                    "imgSearchIcon": {
                        "src": "search_1x.png"
                    },
                    "imgSubject": {
                        "src": "dropdownarrow.png"
                    },
                    "tbxSearchBox": {
                        "placeholder": "Search by Request ID, Username, Customer ID"
                    }
                }
            }, {
                "overrides": {}
            }, {
                "overrides": {}
            });
            var datePicker1 = new kony.ui.CustomWidget({
                "id": "datePicker1",
                "isVisible": true,
                "left": "22px",
                "right": "3px",
                "bottom": "1px",
                "top": "100px",
                "width": "180dp",
                "height": "30px",
                "zIndex": 10,
                "clipBounds": true
            }, {
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {
                "widgetName": "dateRangePicker",
                "event": null,
                "rangeType": "",
                "resetData": null,
                "type": "list",
                "value": ""
            });
            var flxCloseCal2 = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "15px",
                "id": "flxCloseCal2",
                "isVisible": false,
                "layoutType": kony.flex.FREE_FORM,
                "left": "185px",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "105px",
                "width": "15px",
                "zIndex": 11
            }, {}, {
                "hoverSkin": "sknCursor"
            });
            flxCloseCal2.setDefaultUnit(kony.flex.DP);
            var lblCloseCal2 = new kony.ui.Label({
                "centerY": "50%",
                "id": "lblCloseCal2",
                "isVisible": true,
                "right": "0dp",
                "skin": "sknfontIconDescDownArrow12px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmAlertsManagement.lblAlertPreviewClose\")",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxCloseCal2.add(lblCloseCal2);
            flxMyQueuePage.add(myQueueMessageFilter, datePicker1, flxCloseCal2);
            var flxMessageDetailsPage = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                "clipBounds": true,
                "id": "flxMessageDetailsPage",
                "isVisible": false,
                "layoutType": kony.flex.FREE_FORM,
                "left": "35px",
                "isModalContainer": false,
                "right": "35px",
                "skin": "sknheaderCSR",
                "top": "100px",
                "zIndex": 1
            }, {}, {});
            flxMessageDetailsPage.setDefaultUnit(kony.flex.DP);
            var editMessages = new com.adminConsole.csrEdit.editMessages({
                "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                "id": "editMessages",
                "isVisible": true,
                "layoutType": kony.flex.FLOW_VERTICAL,
                "left": "0dp",
                "masterType": constants.MASTER_TYPE_DEFAULT,
                "isModalContainer": false,
                "right": "0dp",
                "skin": "sknflxffffffop0a",
                "top": "0dp",
                "overrides": {
                    "backToPageHeader.flxBack": {
                        "left": "20px"
                    },
                    "btnAssign": {
                        "right": "105px"
                    },
                    "btnReply": {
                        "right": 20
                    },
                    "btnStatus": {
                        "right": "20px"
                    },
                    "editMessages": {
                        "left": "0dp",
                        "right": "0dp",
                        "top": "0dp"
                    },
                    "imgPermissionStatus": {
                        "src": "active_circle2x.png"
                    },
                    "lblAssign": {
                        "right": "210px"
                    },
                    "lblHeading": {
                        "left": "35dp"
                    },
                    "lblIconPermissionStatus": {
                        "left": "20px"
                    },
                    "segMesgs": {
                        "left": "20px",
                        "right": "20px"
                    }
                }
            }, {
                "overrides": {}
            }, {
                "overrides": {}
            });
            flxMessageDetailsPage.add(editMessages);
            flxScrollMainContent.add(flxLocationsWrapper, flxNewMessagePage, flxMyQueuePage, flxMessageDetailsPage);
            flxRightPannel.add(flxMainHeader, flxHeaderDropdown, flxBreadCrumbs, flxTemplates, flxScrollMainContent);
            var flxLoading = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "100%",
                "id": "flxLoading",
                "isVisible": false,
                "layoutType": kony.flex.FREE_FORM,
                "left": "305dp",
                "isModalContainer": false,
                "right": "0dp",
                "skin": "sknLoadingBlur",
                "top": "0dp",
                "zIndex": 90
            }, {}, {});
            flxLoading.setDefaultUnit(kony.flex.DP);
            var flxImageContainer = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "centerX": "50%",
                "centerY": "50%",
                "clipBounds": true,
                "height": "75px",
                "id": "flxImageContainer",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "isModalContainer": false,
                "skin": "slFbox",
                "width": "75px",
                "zIndex": 1
            }, {}, {});
            flxImageContainer.setDefaultUnit(kony.flex.DP);
            var imgLoading = new kony.ui.Image2({
                "centerX": "50%",
                "centerY": "50%",
                "height": "50px",
                "id": "imgLoading",
                "isVisible": true,
                "skin": "slImage",
                "src": "loadingscreenimage.gif",
                "width": "50px",
                "zIndex": 1
            }, {
                "imageScaleMode": constants.IMAGE_SCALE_MODE_MAINTAIN_ASPECT_RATIO,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxImageContainer.add(imgLoading);
            flxLoading.add(flxImageContainer);
            var flxNewMessage = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "bottom": "0px",
                "clipBounds": true,
                "height": "81.47%",
                "id": "flxNewMessage",
                "isVisible": false,
                "layoutType": kony.flex.FREE_FORM,
                "isModalContainer": false,
                "right": "0px",
                "skin": "sknheaderCSRDrag",
                "width": "44.56%",
                "zIndex": 100
            }, {}, {});
            flxNewMessage.setDefaultUnit(kony.flex.DP);
            var Message = new com.adminConsole.templateMessage.Message({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "height": "100%",
                "id": "Message",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "masterType": constants.MASTER_TYPE_DEFAULT,
                "isModalContainer": false,
                "right": "0px",
                "skin": "sknflxffffffop0j3debc02cb1248resize",
                "top": "0dp",
                "width": "100%",
                "overrides": {
                    "flxFilter": {
                        "height": "240px",
                        "isVisible": true,
                        "maxHeight": "viz.val_cleared",
                        "maxWidth": "viz.val_cleared",
                        "scrollDirection": 2,
                        "width": "240px"
                    },
                    "flxStatusUpdate": {
                        "isVisible": true
                    },
                    "imgCheckBoxMsg": {
                        "src": "checkbox.png"
                    },
                    "lblSubject": {
                        "left": "15px"
                    },
                    "lblTo": {
                        "left": "15px"
                    },
                    "lstbocCategory": {
                        "left": "15px"
                    },
                    "lstboxMail": {
                        "width": "290px"
                    },
                    "richTxtTopSuggestions": {
                        "height": "25px"
                    },
                    "richtextNoResult": {
                        "height": kony.flex.USE_PREFFERED_SIZE
                    },
                    "segFilterDropdown": {
                        "bottom": "viz.val_cleared",
                        "height": "205px",
                        "isVisible": true,
                        "left": "0px",
                        "right": "0px",
                        "top": "0px"
                    },
                    "txtTo": {
                        "left": "40px"
                    }
                }
            }, {
                "overrides": {}
            }, {
                "overrides": {}
            });
            flxNewMessage.add(Message);
            var flxToastMessage = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "bottom": "-70px",
                "clipBounds": true,
                "height": "70px",
                "id": "flxToastMessage",
                "isVisible": false,
                "layoutType": kony.flex.FREE_FORM,
                "left": "305px",
                "isModalContainer": false,
                "right": "0px",
                "skin": "slFbox",
                "zIndex": 110
            }, {}, {});
            flxToastMessage.setDefaultUnit(kony.flex.DP);
            var toastMessage = new com.adminConsole.common.toastMessage({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "height": "70px",
                "id": "toastMessage",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "masterType": constants.MASTER_TYPE_DEFAULT,
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0dp",
                "width": "100%",
                "overrides": {
                    "lbltoastMessage": {
                        "text": "Branch deactivated successfully"
                    }
                }
            }, {
                "overrides": {}
            }, {
                "overrides": {}
            });
            flxToastMessage.add(toastMessage);
            var flxNewMessageHeader = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "bottom": "0px",
                "clipBounds": true,
                "height": "100%",
                "id": "flxNewMessageHeader",
                "isVisible": false,
                "layoutType": kony.flex.FREE_FORM,
                "isModalContainer": false,
                "right": "0px",
                "skin": "slFbox",
                "width": "44.50%",
                "zIndex": 120
            }, {}, {});
            flxNewMessageHeader.setDefaultUnit(kony.flex.DP);
            var flxMessageHeader = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "bottom": "0px",
                "clipBounds": true,
                "height": "40dp",
                "id": "flxMessageHeader",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0px",
                "isModalContainer": false,
                "right": "0px",
                "skin": "sknflx2f4b6cborderDrag",
                "width": "100%",
                "zIndex": 10
            }, {}, {});
            flxMessageHeader.setDefaultUnit(kony.flex.DP);
            var lblMsg = new kony.ui.Label({
                "centerY": "50%",
                "id": "lblMsg",
                "isVisible": true,
                "left": "20px",
                "skin": "slLabel0ea43a2a5545d46",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmCSR.lblMsg\")",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var flxButtons = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "centerY": "50%",
                "clipBounds": true,
                "height": "100%",
                "id": "flxButtons",
                "isVisible": true,
                "layoutType": kony.flex.FLOW_HORIZONTAL,
                "isModalContainer": false,
                "right": "0%",
                "skin": "slFbox",
                "width": "80px",
                "zIndex": 1
            }, {}, {});
            flxButtons.setDefaultUnit(kony.flex.DP);
            var flxDraggable = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "centerY": "50%",
                "clipBounds": true,
                "height": "20px",
                "id": "flxDraggable",
                "isVisible": false,
                "layoutType": kony.flex.FREE_FORM,
                "left": "15px",
                "isModalContainer": false,
                "skin": "sknCursorDragabble",
                "width": "20px",
                "zIndex": 1
            }, {}, {});
            flxDraggable.setDefaultUnit(kony.flex.DP);
            var imgDraggable = new kony.ui.Image2({
                "centerX": "50%",
                "centerY": "50%",
                "height": "18px",
                "id": "imgDraggable",
                "isVisible": true,
                "skin": "slImage",
                "src": "move_2x.png",
                "width": "18px",
                "zIndex": 1
            }, {
                "imageScaleMode": constants.IMAGE_SCALE_MODE_MAINTAIN_ASPECT_RATIO,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxDraggable.add(imgDraggable);
            var flxMaximize = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "centerY": "50%",
                "clipBounds": true,
                "height": "20px",
                "id": "flxMaximize",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "15px",
                "isModalContainer": false,
                "skin": "sknCursor",
                "width": "20px",
                "zIndex": 1
            }, {}, {});
            flxMaximize.setDefaultUnit(kony.flex.DP);
            var lblMaximize = new kony.ui.Label({
                "centerY": "50%",
                "height": "20dp",
                "id": "lblMaximize",
                "isVisible": true,
                "left": "0dp",
                "skin": "sknIcon18pxWhiteNormal",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.userwidgetmodel.lblMaximize\")",
                "width": "20dp",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxMaximize.add(lblMaximize);
            var flxClose = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "centerY": "50%",
                "clipBounds": true,
                "height": "20px",
                "id": "flxClose",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "15px",
                "isModalContainer": false,
                "skin": "sknCursor",
                "width": "20px",
                "zIndex": 1
            }, {}, {});
            flxClose.setDefaultUnit(kony.flex.DP);
            var lblClose = new kony.ui.Label({
                "centerY": "50%",
                "height": "20dp",
                "id": "lblClose",
                "isVisible": true,
                "left": "0dp",
                "skin": "sknIcon18pxWhiteNormal",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.userwidgetmodel.lblClearSearch\")",
                "width": "20dp",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxClose.add(lblClose);
            flxButtons.add(flxDraggable, flxMaximize, flxClose);
            flxMessageHeader.add(lblMsg, flxButtons);
            flxNewMessageHeader.add(flxMessageHeader);
            flxMain.add(draggableArea, flxLeftPannel, flxRightPannel, flxLoading, flxNewMessage, flxToastMessage, flxNewMessageHeader);
            var flxImportDeletePopup = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "100%",
                "id": "flxImportDeletePopup",
                "isVisible": false,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "masterType": constants.MASTER_TYPE_USERWIDGET,
                "isModalContainer": false,
                "skin": "flxPopupTrans0ba858b5d5d304e",
                "top": "0dp",
                "width": "100%",
                "zIndex": 10
            }, {}, {});
            flxImportDeletePopup.setDefaultUnit(kony.flex.DP);
            var popUp = new com.adminConsole.common.popUp1({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "height": "100%",
                "id": "popUp",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "masterType": constants.MASTER_TYPE_DEFAULT,
                "isModalContainer": false,
                "skin": "sknflxBg000000Op50",
                "top": "0dp",
                "width": "100%",
                "zIndex": 1,
                "overrides": {
                    "btnPopUpCancel": {
                        "i18n_text": "kony.i18n.getLocalizedString(\"i18n.PopUp.NoLeaveAsIS\")",
                        "right": "20px",
                        "width": kony.flex.USE_PREFFERED_SIZE
                    },
                    "btnPopUpDelete": {
                        "i18n_text": "kony.i18n.getLocalizedString(\"i18n.PopUp.YesDeactivate\")",
                        "minWidth": "viz.val_cleared"
                    },
                    "popUp1": {
                        "bottom": "viz.val_cleared",
                        "centerX": "viz.val_cleared",
                        "centerY": "viz.val_cleared",
                        "height": "100%",
                        "left": "0dp",
                        "maxHeight": "viz.val_cleared",
                        "maxWidth": "viz.val_cleared",
                        "minHeight": "viz.val_cleared",
                        "minWidth": "viz.val_cleared",
                        "right": "viz.val_cleared",
                        "top": "0dp",
                        "width": "100%"
                    }
                }
            }, {
                "overrides": {}
            }, {
                "overrides": {}
            });
            flxImportDeletePopup.add(popUp);
            var flxAssignPopup = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "100%",
                "id": "flxAssignPopup",
                "isVisible": false,
                "layoutType": kony.flex.FREE_FORM,
                "masterType": constants.MASTER_TYPE_USERWIDGET,
                "isModalContainer": false,
                "skin": "flxPopupTrans0ba858b5d5d304e",
                "width": "100%",
                "zIndex": 10
            }, {}, {});
            flxAssignPopup.setDefaultUnit(kony.flex.DP);
            var flxPopUp = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "centerX": "50%",
                "clipBounds": false,
                "height": "250px",
                "id": "flxPopUp",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "isModalContainer": false,
                "skin": "slFbox0j9f841cc563e4e",
                "top": "250px",
                "width": "600px",
                "zIndex": 10
            }, {}, {});
            flxPopUp.setDefaultUnit(kony.flex.DP);
            var flxPopUpTopColor = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "10px",
                "id": "flxPopUpTopColor",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "sknflx11abebop100",
                "top": "0dp",
                "width": "100%",
                "zIndex": 1
            }, {}, {});
            flxPopUpTopColor.setDefaultUnit(kony.flex.DP);
            flxPopUpTopColor.add();
            var flxPopupHeader = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                "clipBounds": true,
                "id": "flxPopupHeader",
                "isVisible": true,
                "layoutType": kony.flex.FLOW_VERTICAL,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "slFbox0d9c3974835234d",
                "top": "10px",
                "width": "100%",
                "zIndex": 1
            }, {}, {});
            flxPopupHeader.setDefaultUnit(kony.flex.DP);
            var flxPopUpClose = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "25px",
                "id": "flxPopUpClose",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "isModalContainer": false,
                "right": 15,
                "skin": "sknCursor",
                "top": "15dp",
                "width": "25px",
                "zIndex": 20
            }, {}, {});
            flxPopUpClose.setDefaultUnit(kony.flex.DP);
            var imgPopUpClose = new kony.ui.Image2({
                "centerX": "50%",
                "centerY": "50%",
                "height": "15dp",
                "id": "imgPopUpClose",
                "isVisible": true,
                "skin": "slImage",
                "src": "close_blue.png",
                "width": "15dp",
                "zIndex": 1
            }, {
                "imageScaleMode": constants.IMAGE_SCALE_MODE_MAINTAIN_ASPECT_RATIO,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxPopUpClose.add(imgPopUpClose);
            var lblPopUpMainMessage = new kony.ui.Label({
                "id": "lblPopUpMainMessage",
                "isVisible": true,
                "left": "20px",
                "skin": "sknlblLatoBold485c7516px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmCSR.lblPopUpMainMessage\")",
                "top": "0px",
                "width": "80%",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var flxAssign = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "bottom": "0px",
                "clipBounds": true,
                "height": "40px",
                "id": "flxAssign",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "20px",
                "isModalContainer": false,
                "right": "20px",
                "skin": "sknflxffffffop0pxe0e44c8047c5ef48",
                "top": "20px",
                "width": "560px",
                "zIndex": 20
            }, {}, {});
            flxAssign.setDefaultUnit(kony.flex.DP);
            var txtbxAssign = new kony.ui.TextBox2({
                "autoCapitalize": constants.TEXTBOX_AUTO_CAPITALIZE_NONE,
                "centerY": "50%",
                "height": 40,
                "id": "txtbxAssign",
                "isVisible": true,
                "keyBoardStyle": constants.TEXTBOX_KEY_BOARD_STYLE_DEFAULT,
                "left": "0dp",
                "secureTextEntry": false,
                "text": "Assign to me",
                "textInputMode": constants.TEXTBOX_INPUT_MODE_ANY,
                "width": "90%",
                "zIndex": 20
            }, {
                "containerHeightMode": constants.TEXTBOX_FONT_METRICS_DRIVEN_HEIGHT,
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [3, 0, 0, 0],
                "paddingInPixel": false
            }, {
                "autoCorrect": false
            });
            var flxAssignClose = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "centerY": "50%",
                "clipBounds": true,
                "height": "25px",
                "id": "flxAssignClose",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "isModalContainer": false,
                "right": 15,
                "skin": "sknCursor",
                "width": "25px",
                "zIndex": 20
            }, {}, {});
            flxAssignClose.setDefaultUnit(kony.flex.DP);
            var imgAssignClose = new kony.ui.Image2({
                "centerX": "50%",
                "centerY": "50%",
                "height": "15dp",
                "id": "imgAssignClose",
                "isVisible": true,
                "skin": "slImage",
                "src": "close_blue.png",
                "width": "15dp",
                "zIndex": 1
            }, {
                "imageScaleMode": constants.IMAGE_SCALE_MODE_MAINTAIN_ASPECT_RATIO,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxAssignClose.add(imgAssignClose);
            flxAssign.add(txtbxAssign, flxAssignClose);
            var lblNoAssignToError = new kony.ui.Label({
                "height": "15px",
                "id": "lblNoAssignToError",
                "isVisible": true,
                "left": "25px",
                "skin": "sknLabelRed",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmCSR.lblNoAssignToError\")",
                "top": "5px",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxPopupHeader.add(flxPopUpClose, lblPopUpMainMessage, flxAssign, lblNoAssignToError);
            var flxPopUpButtons = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "bottom": "0px",
                "clipBounds": true,
                "height": "80px",
                "id": "flxPopUpButtons",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0%",
                "isModalContainer": false,
                "skin": "slFbox0dc900c4572aa40",
                "width": "100%",
                "zIndex": 1
            }, {}, {});
            flxPopUpButtons.setDefaultUnit(kony.flex.DP);
            var btnPopUpCancel = new kony.ui.Button({
                "centerY": "50%",
                "focusSkin": "sknBtnLatoRegulara5abc413pxKA",
                "height": "40px",
                "id": "btnPopUpCancel",
                "isVisible": true,
                "left": "20dp",
                "skin": "sknBtnLatoRegulara5abc413pxKA",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmCustomers.CANCEL\")",
                "top": "0%",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 20
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_CENTER,
                "displayText": true,
                "padding": [3, 0, 3, 0],
                "paddingInPixel": false
            }, {
                "hoverSkin": "sknbtnffffffLatoBolda5abc413Pxd7d8e01pxBorderRadius20px"
            });
            var btnPopUpDelete = new kony.ui.Button({
                "centerY": "50%",
                "focusSkin": "sknBtn003E75LatoRegular13pxFFFFFFRad20px",
                "height": "40px",
                "id": "btnPopUpDelete",
                "isVisible": true,
                "minWidth": "100px",
                "right": "20px",
                "skin": "sknBtn003E75LatoRegular13pxFFFFFFRad20px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmCSR.btnPopUpDelete\")",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 20
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_CENTER,
                "displayText": true,
                "padding": [3, 0, 3, 0],
                "paddingInPixel": false
            }, {
                "hoverSkin": "sknBtn005198LatoRegular13pxFFFFFFRad20px"
            });
            flxPopUpButtons.add(btnPopUpCancel, btnPopUpDelete);
            var flxAssignDropdown = new kony.ui.FlexScrollContainer({
                "allowHorizontalBounce": false,
                "allowVerticalBounce": true,
                "bounces": true,
                "clipBounds": true,
                "enableScrolling": true,
                "height": "200px",
                "horizontalScrollIndicator": true,
                "id": "flxAssignDropdown",
                "isVisible": false,
                "layoutType": kony.flex.FLOW_VERTICAL,
                "left": "20px",
                "pagingEnabled": false,
                "right": "20px",
                "scrollDirection": kony.flex.SCROLL_VERTICAL,
                "skin": "sknscrflxffffffop0d0b87507b0274eSe",
                "top": "130px",
                "verticalScrollIndicator": true,
                "width": "560px",
                "zIndex": 20
            }, {}, {});
            flxAssignDropdown.setDefaultUnit(kony.flex.DP);
            var segUsers = new kony.ui.SegmentedUI2({
                "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                "centerX": "50%",
                "data": [{
                    "lblViewFullName": "Edward"
                }, {
                    "lblViewFullName": "Edward"
                }, {
                    "lblViewFullName": "Edward"
                }, {
                    "lblViewFullName": "Edward"
                }, {
                    "lblViewFullName": "Edward"
                }],
                "groupCells": false,
                "id": "segUsers",
                "isVisible": true,
                "needPageIndicator": true,
                "pageOffDotImage": "pageoffdot.png",
                "pageOnDotImage": "pageondot.png",
                "retainSelection": false,
                "rowFocusSkin": "seg2Focus",
                "rowSkin": "seg2Normal",
                "rowTemplate": "flxAssignUsers",
                "sectionHeaderSkin": "sliPhoneSegmentHeader",
                "selectionBehavior": constants.SEGUI_DEFAULT_BEHAVIOR,
                "separatorRequired": false,
                "showScrollbars": false,
                "top": "10px",
                "viewType": constants.SEGUI_VIEW_TYPE_TABLEVIEW,
                "widgetDataMap": {
                    "flxAssignUsers": "flxAssignUsers",
                    "lblViewFullName": "lblViewFullName"
                },
                "width": "100%",
                "zIndex": 20
            }, {
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var richtextNoResult = new kony.ui.RichText({
                "centerX": "50%",
                "centerY": "50%",
                "id": "richtextNoResult",
                "isVisible": false,
                "left": "0dp",
                "skin": "sknrichtext12pxlatoregular",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmCSR.richtextNoResult\")",
                "top": "0dp",
                "width": "100%",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_CENTER,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxAssignDropdown.add(segUsers, richtextNoResult);
            flxPopUp.add(flxPopUpTopColor, flxPopupHeader, flxPopUpButtons, flxAssignDropdown);
            flxAssignPopup.add(flxPopUp);
            var flxEditCancelConfirmation = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "100%",
                "id": "flxEditCancelConfirmation",
                "isVisible": false,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "masterType": constants.MASTER_TYPE_USERWIDGET,
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0dp",
                "width": "100%",
                "zIndex": 5
            }, {}, {});
            flxEditCancelConfirmation.setDefaultUnit(kony.flex.DP);
            var popUpCancelEdits = new com.adminConsole.common.popUpCancelEdits({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "height": "100%",
                "id": "popUpCancelEdits",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "masterType": constants.MASTER_TYPE_DEFAULT,
                "isModalContainer": false,
                "skin": "sknflxBg000000Op50",
                "top": "0dp",
                "width": "100%",
                "zIndex": 1,
                "overrides": {
                    "btnPopUpCancel": {
                        "i18n_text": "kony.i18n.getLocalizedString(\"i8n.navigation.cancelButton\")",
                        "right": "95px"
                    },
                    "btnPopUpDelete": {
                        "i18n_text": "kony.i18n.getLocalizedString(\"i8n.navigation.okButton\")",
                        "minWidth": "viz.val_cleared"
                    },
                    "imgPopUpClose": {
                        "src": "close_blue.png"
                    },
                    "lblPopUpMainMessage": {
                        "i18n_text": "kony.i18n.getLocalizedString(\"i8n.navigation.cancelChanges\")"
                    },
                    "popUpCancelEdits": {
                        "bottom": "viz.val_cleared",
                        "centerX": "viz.val_cleared",
                        "centerY": "viz.val_cleared",
                        "height": "100%",
                        "left": "0dp",
                        "maxHeight": "viz.val_cleared",
                        "maxWidth": "viz.val_cleared",
                        "minHeight": "viz.val_cleared",
                        "minWidth": "viz.val_cleared",
                        "right": "viz.val_cleared",
                        "top": "0dp",
                        "width": "100%"
                    },
                    "rtxPopUpDisclaimer": {
                        "i18n_text": "kony.i18n.getLocalizedString(\"i8n.navigation.cancelChangesBody\")"
                    }
                }
            }, {
                "overrides": {}
            }, {
                "overrides": {}
            });
            flxEditCancelConfirmation.add(popUpCancelEdits);
            this.add(flxMain, flxImportDeletePopup, flxAssignPopup, flxEditCancelConfirmation);
        };
        return [{
            "addWidgets": addWidgetsfrmCSR,
            "enabledForIdleTimeout": true,
            "id": "frmCSR",
            "layoutType": kony.flex.FREE_FORM,
            "needAppMenu": false,
            "postShow": controller.AS_Form_g01a21653be34ed48432393df48931ab,
            "preShow": function(eventobject) {
                controller.AS_Form_e78d6699e06940a982c48b8a1a432aac(eventobject);
            },
            "skin": "slForm"
        }, {
            "displayOrientation": constants.FORM_DISPLAY_ORIENTATION_PORTRAIT,
            "layoutType": kony.flex.FREE_FORM,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, {
            "onDeviceBack": controller.AS_Form_dae365d09d7a40db9fae3e596347b489,
            "retainScrollPosition": false
        }]
    }
});