define("flxSegSignatoryGroupHeader", function() {
    return function(controller) {
        var flxSegSignatoryGroupHeader = new kony.ui.FlexContainer({
            "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
            "clipBounds": true,
            "id": "flxSegSignatoryGroupHeader",
            "isVisible": true,
            "layoutType": kony.flex.FLOW_VERTICAL,
            "left": "0dp",
            "isModalContainer": false,
            "skin": "sknFlxFFFFFF100O",
            "top": "0dp",
            "width": "100%"
        }, {}, {});
        flxSegSignatoryGroupHeader.setDefaultUnit(kony.flex.DP);
        var flxHeaderContainer = new kony.ui.FlexContainer({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "clipBounds": true,
            "height": "52dp",
            "id": "flxHeaderContainer",
            "isVisible": true,
            "layoutType": kony.flex.FLOW_HORIZONTAL,
            "left": "0dp",
            "isModalContainer": false,
            "skin": "slFbox",
            "top": "0dp",
            "width": "100%"
        }, {}, {});
        flxHeaderContainer.setDefaultUnit(kony.flex.DP);
        var flxGroupName = new kony.ui.FlexContainer({
            "clipBounds": true,
            "height": "100%",
            "id": "flxGroupName",
            "isVisible": true,
            "layoutType": kony.flex.FLOW_HORIZONTAL,
            "left": "0dp",
            "isModalContainer": false,
            "skin": "slFbox",
            "top": "0px",
            "width": "22%"
        }, {}, {});
        flxGroupName.setDefaultUnit(kony.flex.DP);
        var lblGroupName = new kony.ui.Label({
            "id": "lblGroupName",
            "isVisible": true,
            "left": "36px",
            "skin": "sknlblLato696c7311px",
            "text": "GROUP NAME",
            "top": "17dp",
            "width": kony.flex.USE_PREFFERED_SIZE
        }, {
            "contentAlignment": constants.CONTENT_ALIGN_TOP_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, {});
        var lblIconGroupNameSort = new kony.ui.Label({
            "id": "lblIconGroupNameSort",
            "isVisible": true,
            "left": "7dp",
            "skin": "sknIcon485C7513px",
            "text": "",
            "top": "17dp",
            "width": kony.flex.USE_PREFFERED_SIZE
        }, {
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, {
            "hoverSkin": "sknIcon485C7513pxHover"
        });
        flxGroupName.add(lblGroupName, lblIconGroupNameSort);
        var flxGroupDescription = new kony.ui.FlexContainer({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "clipBounds": true,
            "height": "100%",
            "id": "flxGroupDescription",
            "isVisible": true,
            "layoutType": kony.flex.FLOW_HORIZONTAL,
            "isModalContainer": false,
            "skin": "slFbox",
            "width": "45%"
        }, {}, {});
        flxGroupDescription.setDefaultUnit(kony.flex.DP);
        var lblGroupDescription = new kony.ui.Label({
            "id": "lblGroupDescription",
            "isVisible": true,
            "left": "0%",
            "skin": "sknlblLato696c7311px",
            "text": "GROUP DESCRIPTION",
            "top": "17dp",
            "width": "90%"
        }, {
            "contentAlignment": constants.CONTENT_ALIGN_TOP_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, {});
        flxGroupDescription.add(lblGroupDescription);
        var flxNumberOfUsers = new kony.ui.FlexContainer({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "clipBounds": true,
            "height": "100%",
            "id": "flxNumberOfUsers",
            "isVisible": true,
            "layoutType": kony.flex.FLOW_HORIZONTAL,
            "isModalContainer": false,
            "right": 0,
            "skin": "slFbox",
            "width": "15%"
        }, {}, {});
        flxNumberOfUsers.setDefaultUnit(kony.flex.DP);
        var lblNumberOfUsers = new kony.ui.Label({
            "id": "lblNumberOfUsers",
            "isVisible": true,
            "left": "0%",
            "skin": "sknlblLato696c7311px",
            "text": "NUMBER OF USERS",
            "top": "17dp",
            "width": kony.flex.USE_PREFFERED_SIZE
        }, {
            "contentAlignment": constants.CONTENT_ALIGN_TOP_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, {});
        var lblIconSortNumberOfUsers = new kony.ui.Label({
            "id": "lblIconSortNumberOfUsers",
            "isVisible": true,
            "left": "5dp",
            "skin": "sknIcon485C7513px",
            "text": "",
            "top": "17dp",
            "width": kony.flex.USE_PREFFERED_SIZE
        }, {
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, {
            "hoverSkin": "sknIcon485C7513pxHover"
        });
        flxNumberOfUsers.add(lblNumberOfUsers, lblIconSortNumberOfUsers);
        var lblSeperator = new kony.ui.Label({
            "bottom": "0dp",
            "height": "1dp",
            "id": "lblSeperator",
            "isVisible": true,
            "left": "0dp",
            "skin": "sknLblSeparator696C73",
            "text": "Label",
            "width": "100%"
        }, {
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, {});
        flxHeaderContainer.add(flxGroupName, flxGroupDescription, flxNumberOfUsers, lblSeperator);
        flxSegSignatoryGroupHeader.add(flxHeaderContainer);
        return flxSegSignatoryGroupHeader;
    }
})