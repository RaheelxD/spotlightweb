define("flxRelatedCustomerList", function() {
    return function(controller) {
        var flxRelatedCustomerList = new kony.ui.FlexContainer({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "clipBounds": true,
            "height": "106dp",
            "id": "flxRelatedCustomerList",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "left": "0dp",
            "isModalContainer": false,
            "skin": "slFbox",
            "top": "0dp",
            "width": "100%"
        }, {}, {});
        flxRelatedCustomerList.setDefaultUnit(kony.flex.DP);
        var flxRelatedCustomerRow = new kony.ui.FlexContainer({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "clipBounds": true,
            "height": "96dp",
            "id": "flxRelatedCustomerRow",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "left": "20dp",
            "isModalContainer": false,
            "right": "20dp",
            "skin": "sknFlxbgFFFFFFbdrD7D9E0rd4px",
            "top": "10dp"
        }, {}, {});
        flxRelatedCustomerRow.setDefaultUnit(kony.flex.DP);
        var flxLeftDetailsCont = new kony.ui.FlexContainer({
            "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
            "clipBounds": true,
            "id": "flxLeftDetailsCont",
            "isVisible": true,
            "layoutType": kony.flex.FLOW_HORIZONTAL,
            "left": "5dp",
            "isModalContainer": false,
            "skin": "slFbox",
            "top": "15dp",
            "width": "85%"
        }, {}, {});
        flxLeftDetailsCont.setDefaultUnit(kony.flex.DP);
        var flxCheckbox = new kony.ui.FlexContainer({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "clipBounds": true,
            "height": "15dp",
            "id": "flxCheckbox",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "left": "10dp",
            "isModalContainer": false,
            "skin": "sknCursor",
            "top": "1dp",
            "width": "15dp"
        }, {}, {});
        flxCheckbox.setDefaultUnit(kony.flex.DP);
        var imgCheckbox = new kony.ui.Image2({
            "height": "100%",
            "id": "imgCheckbox",
            "isVisible": true,
            "left": "0",
            "skin": "slImage",
            "src": "checkboxnormal.png",
            "top": "0",
            "width": "100%"
        }, {
            "imageScaleMode": constants.IMAGE_SCALE_MODE_MAINTAIN_ASPECT_RATIO,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, {});
        flxCheckbox.add(imgCheckbox);
        var flxContractDetails = new kony.ui.FlexContainer({
            "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
            "clipBounds": true,
            "id": "flxContractDetails",
            "isVisible": true,
            "layoutType": kony.flex.FLOW_VERTICAL,
            "left": "10dp",
            "isModalContainer": false,
            "skin": "slFbox",
            "top": "0dp",
            "width": "100%"
        }, {}, {});
        flxContractDetails.setDefaultUnit(kony.flex.DP);
        var flxContractName = new kony.ui.FlexContainer({
            "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
            "clipBounds": true,
            "id": "flxContractName",
            "isVisible": true,
            "layoutType": kony.flex.FLOW_HORIZONTAL,
            "left": "0dp",
            "isModalContainer": false,
            "skin": "slFbox",
            "top": "0dp",
            "width": "100%"
        }, {}, {});
        flxContractName.setDefaultUnit(kony.flex.DP);
        var lblContractName = new kony.ui.Label({
            "id": "lblContractName",
            "isVisible": true,
            "left": "0dp",
            "skin": "sknLbl117EB0LatoReg14px",
            "text": "Label",
            "top": "0dp",
            "width": kony.flex.USE_PREFFERED_SIZE
        }, {
            "contentAlignment": constants.CONTENT_ALIGN_TOP_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, {
            "hoverSkin": "sknLbl117EB0LatoReg14pxHov"
        });
        var lblContractId = new kony.ui.Label({
            "id": "lblContractId",
            "isVisible": false,
            "left": "10dp",
            "skin": "sknLatoSemibold485c7313px",
            "text": "Label",
            "top": "1dp",
            "width": kony.flex.USE_PREFFERED_SIZE
        }, {
            "contentAlignment": constants.CONTENT_ALIGN_TOP_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, {});
        flxContractName.add(lblContractName, lblContractId);
        var flxDetails = new kony.ui.FlexContainer({
            "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
            "clipBounds": true,
            "id": "flxDetails",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "left": "0dp",
            "isModalContainer": false,
            "skin": "slFbox",
            "top": "10dp",
            "width": "100%"
        }, {}, {});
        flxDetails.setDefaultUnit(kony.flex.DP);
        var flxColumn1 = new kony.ui.FlexContainer({
            "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
            "clipBounds": true,
            "id": "flxColumn1",
            "isVisible": true,
            "layoutType": kony.flex.FLOW_VERTICAL,
            "left": "0px",
            "isModalContainer": false,
            "skin": "slFbox",
            "top": "0dp",
            "width": "27%",
            "zIndex": 1
        }, {}, {});
        flxColumn1.setDefaultUnit(kony.flex.DP);
        var lblHeading1 = new kony.ui.Label({
            "id": "lblHeading1",
            "isVisible": true,
            "left": "0px",
            "skin": "sknlblLato696c7311px",
            "i18n_text": "kony.i18n.getLocalizedString(\"i18n.userwidgetmodel.lblHeading1\")",
            "top": "0px",
            "width": "100%",
            "zIndex": 1
        }, {
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, {});
        var lblData1 = new kony.ui.Label({
            "id": "lblData1",
            "isVisible": true,
            "left": "0px",
            "skin": "sknlbl485C75LatoSemiBold13px",
            "i18n_text": "kony.i18n.getLocalizedString(\"i18n.userwidgetmodel.lblData1\")",
            "top": "8px",
            "width": "100%",
            "zIndex": 1
        }, {
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, {});
        flxColumn1.add(lblHeading1, lblData1);
        var flxColumn2 = new kony.ui.FlexContainer({
            "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
            "clipBounds": true,
            "id": "flxColumn2",
            "isVisible": true,
            "layoutType": kony.flex.FLOW_VERTICAL,
            "left": "30%",
            "isModalContainer": false,
            "skin": "slFbox",
            "top": "0dp",
            "width": "25%",
            "zIndex": 1
        }, {}, {});
        flxColumn2.setDefaultUnit(kony.flex.DP);
        var lblHeading2 = new kony.ui.Label({
            "id": "lblHeading2",
            "isVisible": true,
            "left": "0px",
            "skin": "sknlblLato696c7311px",
            "i18n_text": "kony.i18n.getLocalizedString(\"i18n.userwidgetmodel.lblHeading1\")",
            "top": "0px",
            "width": "100%",
            "zIndex": 1
        }, {
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, {});
        var lblData2 = new kony.ui.Label({
            "id": "lblData2",
            "isVisible": true,
            "left": "0px",
            "skin": "sknlbl485C75LatoSemiBold13px",
            "i18n_text": "kony.i18n.getLocalizedString(\"i18n.userwidgetmodel.lblData1\")",
            "top": "8px",
            "width": "100%",
            "zIndex": 1
        }, {
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, {});
        flxColumn2.add(lblHeading2, lblData2);
        var flxColumn3 = new kony.ui.FlexContainer({
            "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
            "clipBounds": true,
            "id": "flxColumn3",
            "isVisible": false,
            "layoutType": kony.flex.FLOW_VERTICAL,
            "left": "57%",
            "isModalContainer": false,
            "right": "30px",
            "skin": "slFbox",
            "top": "0dp",
            "zIndex": 1
        }, {}, {});
        flxColumn3.setDefaultUnit(kony.flex.DP);
        var lblHeading3 = new kony.ui.Label({
            "id": "lblHeading3",
            "isVisible": true,
            "left": "0px",
            "skin": "sknlblLato696c7311px",
            "i18n_text": "kony.i18n.getLocalizedString(\"i18n.userwidgetmodel.lblHeading1\")",
            "top": "0px",
            "width": "100%",
            "zIndex": 1
        }, {
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, {});
        var lblData3 = new kony.ui.Label({
            "id": "lblData3",
            "isVisible": true,
            "left": "0px",
            "skin": "sknlbl485C75LatoSemiBold13px",
            "i18n_text": "kony.i18n.getLocalizedString(\"i18n.userwidgetmodel.lblData1\")",
            "top": "8px",
            "width": "97%",
            "zIndex": 1
        }, {
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, {});
        flxColumn3.add(lblHeading3, lblData3);
        flxDetails.add(flxColumn1, flxColumn2, flxColumn3);
        flxContractDetails.add(flxContractName, flxDetails);
        flxLeftDetailsCont.add(flxCheckbox, flxContractDetails);
        var flxRightDetailsCont = new kony.ui.FlexContainer({
            "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
            "clipBounds": true,
            "id": "flxRightDetailsCont",
            "isVisible": true,
            "layoutType": kony.flex.FLOW_HORIZONTAL,
            "reverseLayoutDirection": true,
            "isModalContainer": false,
            "right": "15dp",
            "skin": "slFbox",
            "top": "15dp",
            "width": "50%"
        }, {}, {});
        flxRightDetailsCont.setDefaultUnit(kony.flex.DP);
        var btnRelation = new kony.ui.Button({
            "height": "20dp",
            "id": "btnRelation",
            "isVisible": true,
            "right": "0dp",
            "skin": "sknBtn006CCARad16pxLatoReg13px",
            "text": "Partnership",
            "top": "0dp",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, {
            "contentAlignment": constants.CONTENT_ALIGN_CENTER,
            "displayText": true,
            "padding": [2, 0, 2, 0],
            "paddingInPixel": false
        }, {});
        var flxContractRelation = new kony.ui.FlexContainer({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "clipBounds": true,
            "height": "20dp",
            "id": "flxContractRelation",
            "isVisible": false,
            "layoutType": kony.flex.FREE_FORM,
            "reverseLayoutDirection": false,
            "isModalContainer": false,
            "right": "0dp",
            "skin": "sknflxffffffop100Border006ccaRadius16px",
            "top": "0",
            "width": "80dp"
        }, {}, {});
        flxContractRelation.setDefaultUnit(kony.flex.DP);
        var lblContractRelation = new kony.ui.Label({
            "id": "lblContractRelation",
            "isVisible": true,
            "left": "5dp",
            "skin": "sknLbl006CCA13pxKA",
            "text": "Label",
            "top": "0",
            "width": kony.flex.USE_PREFFERED_SIZE
        }, {
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, {});
        flxContractRelation.add(lblContractRelation);
        var lblContractInfo = new kony.ui.Label({
            "id": "lblContractInfo",
            "isVisible": true,
            "right": "20dp",
            "skin": "sknLbl485c75LatoRegItalic13px",
            "text": "Label",
            "top": "0dp",
            "width": kony.flex.USE_PREFFERED_SIZE
        }, {
            "contentAlignment": constants.CONTENT_ALIGN_TOP_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, {});
        flxRightDetailsCont.add(btnRelation, flxContractRelation, lblContractInfo);
        var flxRightActionCont = new kony.ui.FlexContainer({
            "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
            "clipBounds": true,
            "id": "flxRightActionCont",
            "isVisible": true,
            "layoutType": kony.flex.FLOW_HORIZONTAL,
            "reverseLayoutDirection": true,
            "isModalContainer": false,
            "right": "0dp",
            "skin": "slFbox",
            "top": "10dp",
            "width": "40%",
            "zIndex": 2
        }, {}, {});
        flxRightActionCont.setDefaultUnit(kony.flex.DP);
        var flxRemoveContract = new kony.ui.FlexContainer({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "clipBounds": true,
            "height": "20dp",
            "id": "flxRemoveContract",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "reverseLayoutDirection": false,
            "isModalContainer": false,
            "right": "10dp",
            "skin": "slFbox",
            "top": "5dp",
            "width": "20dp"
        }, {}, {});
        flxRemoveContract.setDefaultUnit(kony.flex.DP);
        var lblIconRemove = new kony.ui.Label({
            "centerY": "50%",
            "id": "lblIconRemove",
            "isVisible": true,
            "right": "0dp",
            "skin": "sknIcon00000018px",
            "text": "",
            "top": "0",
            "width": kony.flex.USE_PREFFERED_SIZE
        }, {
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, {});
        flxRemoveContract.add(lblIconRemove);
        var lblLine = new kony.ui.Label({
            "height": "30dp",
            "id": "lblLine",
            "isVisible": true,
            "right": "10dp",
            "skin": "sknlblSeperatorD7D9E0",
            "top": "0dp",
            "width": "1dp"
        }, {
            "contentAlignment": constants.CONTENT_ALIGN_TOP_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, {});
        var lblPrimaryText = new kony.ui.Label({
            "id": "lblPrimaryText",
            "isVisible": true,
            "right": "10dp",
            "skin": "sknLbl485C75LatoRegular13Px",
            "text": "Label",
            "top": "5dp",
            "width": kony.flex.USE_PREFFERED_SIZE
        }, {
            "contentAlignment": constants.CONTENT_ALIGN_TOP_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, {});
        var flxPrimaryBtn = new kony.ui.FlexContainer({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "clipBounds": true,
            "height": "15dp",
            "id": "flxPrimaryBtn",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "isModalContainer": false,
            "right": "10dp",
            "skin": "slFbox",
            "top": "5dp",
            "width": "15dp"
        }, {}, {});
        flxPrimaryBtn.setDefaultUnit(kony.flex.DP);
        var imgRadioButton = new kony.ui.Image2({
            "height": "100%",
            "id": "imgRadioButton",
            "isVisible": true,
            "left": "0",
            "skin": "slImage",
            "src": "radio_selected.png",
            "top": "0",
            "width": "100%"
        }, {
            "imageScaleMode": constants.IMAGE_SCALE_MODE_MAINTAIN_ASPECT_RATIO,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, {});
        flxPrimaryBtn.add(imgRadioButton);
        flxRightActionCont.add(flxRemoveContract, lblLine, lblPrimaryText, flxPrimaryBtn);
        flxRelatedCustomerRow.add(flxLeftDetailsCont, flxRightDetailsCont, flxRightActionCont);
        flxRelatedCustomerList.add(flxRelatedCustomerRow);
        return flxRelatedCustomerList;
    }
})