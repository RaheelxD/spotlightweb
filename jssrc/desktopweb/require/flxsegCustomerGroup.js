define("flxsegCustomerGroup", function() {
    return function(controller) {
        var flxsegCustomerGroup = new kony.ui.FlexContainer({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "clipBounds": true,
            "height": "56px",
            "id": "flxsegCustomerGroup",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "isModalContainer": false,
            "skin": "sknflxffffffop100"
        }, {}, {});
        flxsegCustomerGroup.setDefaultUnit(kony.flex.DP);
        var flxFullName = new kony.ui.FlexContainer({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "clipBounds": true,
            "height": "50%",
            "id": "flxFullName",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "left": "0dp",
            "isModalContainer": false,
            "skin": "slFbox",
            "top": "0dp",
            "width": "100%"
        }, {}, {});
        flxFullName.setDefaultUnit(kony.flex.DP);
        var lblViewFullName = new kony.ui.Label({
            "centerY": "65%",
            "id": "lblViewFullName",
            "isVisible": true,
            "left": "20dp",
            "skin": "sknLblLatoBold485c7513px",
            "text": "Edward",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, {
            "contentAlignment": constants.CONTENT_ALIGN_CENTER,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, {});
        flxFullName.add(lblViewFullName);
        var flxUserName = new kony.ui.FlexContainer({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "clipBounds": true,
            "height": "50%",
            "id": "flxUserName",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "left": "0dp",
            "isModalContainer": false,
            "skin": "slFbox",
            "top": "50%",
            "width": "100%"
        }, {}, {});
        flxUserName.setDefaultUnit(kony.flex.DP);
        var lblUserName = new kony.ui.Label({
            "centerY": "30%",
            "id": "lblUserName",
            "isVisible": true,
            "left": "20dp",
            "skin": "sknlblLato5d6c7f12px",
            "text": "UserID:",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, {
            "contentAlignment": constants.CONTENT_ALIGN_CENTER,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, {});
        flxUserName.add(lblUserName);
        var lblSeperator = new kony.ui.Label({
            "bottom": "0px",
            "height": "1px",
            "id": "lblSeperator",
            "isVisible": true,
            "left": "0px",
            "skin": "sknlblSeperator",
            "text": "'",
            "width": "100%",
            "zIndex": 1
        }, {
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, {});
        flxsegCustomerGroup.add(flxFullName, flxUserName, lblSeperator);
        return flxsegCustomerGroup;
    }
})