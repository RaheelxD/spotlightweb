define("flxAdminConsoleLogList", function() {
    return function(controller) {
        var flxAdminConsoleLogList = new kony.ui.FlexContainer({
            "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
            "clipBounds": true,
            "id": "flxAdminConsoleLogList",
            "isVisible": true,
            "layoutType": kony.flex.FLOW_VERTICAL,
            "isModalContainer": false,
            "skin": "slFbox"
        }, {}, {
            "hoverSkin": "sknfbfcfc"
        });
        flxAdminConsoleLogList.setDefaultUnit(kony.flex.DP);
        var flxAdminConsoleLogListSelected = new kony.ui.FlexContainer({
            "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
            "clipBounds": true,
            "id": "flxAdminConsoleLogListSelected",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "left": "0dp",
            "isModalContainer": false,
            "skin": "slFbox",
            "top": "0dp",
            "width": "100%"
        }, {}, {});
        flxAdminConsoleLogListSelected.setDefaultUnit(kony.flex.DP);
        var flxAdminConsoleLogWrapper = new kony.ui.FlexContainer({
            "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
            "bottom": "17dp",
            "clipBounds": true,
            "id": "flxAdminConsoleLogWrapper",
            "isVisible": true,
            "layoutType": kony.flex.FLOW_HORIZONTAL,
            "left": "35px",
            "isModalContainer": false,
            "right": "35px",
            "skin": "slFbox",
            "top": "0",
            "width": "1200dp"
        }, {}, {});
        flxAdminConsoleLogWrapper.setDefaultUnit(kony.flex.DP);
        var lblEvent = new kony.ui.Label({
            "id": "lblEvent",
            "isVisible": true,
            "left": "20px",
            "skin": "sknlblLatoRegular484b5213px",
            "text": "EVENT",
            "top": "17px",
            "width": "7%",
            "zIndex": 1
        }, {
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, {});
        var lblUserName = new kony.ui.Label({
            "id": "lblUserName",
            "isVisible": true,
            "left": "0px",
            "skin": "sknlblLatoRegular484b5213px",
            "text": "USERNAME",
            "top": "17px",
            "width": "8%",
            "zIndex": 1
        }, {
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, {});
        var lblUserRole = new kony.ui.Label({
            "id": "lblUserRole",
            "isVisible": true,
            "left": "18px",
            "skin": "sknlblLatoRegular484b5213px",
            "text": "USER ROLE",
            "top": "17px",
            "width": "9%",
            "zIndex": 1
        }, {
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, {});
        var lblModuleName = new kony.ui.Label({
            "id": "lblModuleName",
            "isVisible": true,
            "left": "1px",
            "skin": "sknlblLatoRegular484b5213px",
            "text": "MODULE NAME",
            "top": "17px",
            "width": "12%",
            "zIndex": 1
        }, {
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, {});
        var lblDateAndTime = new kony.ui.Label({
            "id": "lblDateAndTime",
            "isVisible": true,
            "right": "2%",
            "skin": "sknlblLatoRegular484b5213px",
            "text": "DATE & TIME",
            "top": "17px",
            "width": "13%",
            "zIndex": 1
        }, {
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, {});
        var flxStatus = new kony.ui.FlexContainer({
            "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
            "clipBounds": true,
            "id": "flxStatus",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "isModalContainer": false,
            "skin": "slFbox",
            "top": "17px",
            "width": "9%",
            "zIndex": 1
        }, {}, {});
        flxStatus.setDefaultUnit(kony.flex.DP);
        var imgStatus = new kony.ui.Image2({
            "height": "8px",
            "id": "imgStatus",
            "isVisible": false,
            "left": "0dp",
            "skin": "slImage",
            "src": "active_circle2x.png",
            "top": "0dp",
            "width": "8px",
            "zIndex": 1
        }, {
            "imageScaleMode": constants.IMAGE_SCALE_MODE_MAINTAIN_ASPECT_RATIO,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, {});
        var lblStatus = new kony.ui.Label({
            "id": "lblStatus",
            "isVisible": true,
            "left": "18px",
            "skin": "sknlblLatoRegular484b5213px",
            "text": "Active",
            "top": "0px",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, {
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, {});
        var lblIconStatus = new kony.ui.Label({
            "height": "15dp",
            "id": "lblIconStatus",
            "isVisible": true,
            "left": "0px",
            "skin": "sknIcon13pxGreen",
            "text": "",
            "top": "1dp",
            "width": "15dp",
            "zIndex": 1
        }, {
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, {});
        flxStatus.add(imgStatus, lblStatus, lblIconStatus);
        var lblJSONData = new kony.ui.Label({
            "id": "lblJSONData",
            "isVisible": true,
            "left": "14px",
            "skin": "sknLblLato13px117eb0",
            "text": "View",
            "top": "17px",
            "width": "15%",
            "zIndex": 1
        }, {
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, {});
        flxAdminConsoleLogWrapper.add(lblEvent, lblUserName, lblUserRole, lblModuleName, lblDateAndTime, flxStatus, lblJSONData);
        var flxDropdown = new kony.ui.FlexContainer({
            "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
            "clipBounds": true,
            "id": "flxDropdown",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "left": "30px",
            "masterType": constants.MASTER_TYPE_USERWIDGET,
            "isModalContainer": false,
            "right": "10px",
            "top": "20dp",
            "width": "30px",
            "zIndex": 2
        }, {}, {
            "hoverSkin": "sknCursor"
        });
        flxDropdown.setDefaultUnit(kony.flex.DP);
        var ImgArrow = new kony.ui.Image2({
            "height": "12px",
            "id": "ImgArrow",
            "isVisible": false,
            "left": "0dp",
            "src": "img_desc_arrow_1.png",
            "top": "0dp",
            "width": "12px",
            "zIndex": 2
        }, {
            "imageScaleMode": constants.IMAGE_SCALE_MODE_MAINTAIN_ASPECT_RATIO,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, {});
        var fonticonArrow = new kony.ui.Label({
            "height": "12px",
            "id": "fonticonArrow",
            "isVisible": true,
            "left": "0dp",
            "text": "",
            "top": "0dp",
            "width": "12px",
            "zIndex": 1
        }, {
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, {});
        flxDropdown.add(ImgArrow, fonticonArrow);
        flxAdminConsoleLogListSelected.add(flxAdminConsoleLogWrapper, flxDropdown);
        var flxGroupDesc = new kony.ui.FlexContainer({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "bottom": "20dp",
            "clipBounds": true,
            "height": "50dp",
            "id": "flxGroupDesc",
            "isVisible": true,
            "layoutType": kony.flex.FLOW_VERTICAL,
            "reverseLayoutDirection": false,
            "left": "20dp",
            "masterType": constants.MASTER_TYPE_USERWIDGET,
            "isModalContainer": false,
            "top": "-2px",
            "width": "91%",
            "zIndex": 20
        }, {}, {});
        flxGroupDesc.setDefaultUnit(kony.flex.DP);
        var lblDescriptionHeader = new kony.ui.Label({
            "id": "lblDescriptionHeader",
            "isVisible": true,
            "left": "35px",
            "skin": "sknlblLato696c7312px",
            "i18n_text": "kony.i18n.getLocalizedString(\"i18n.View.DESCRIPTION\")",
            "top": 10,
            "width": kony.flex.USE_PREFFERED_SIZE
        }, {
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, {});
        var lblDescription = new kony.ui.Label({
            "id": "lblDescription",
            "isVisible": true,
            "left": "35px",
            "skin": "sknlblLatoRegular484b5213px",
            "text": "DESCRITION",
            "top": 10,
            "width": kony.flex.USE_PREFFERED_SIZE
        }, {
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, {});
        flxGroupDesc.add(lblDescriptionHeader, lblDescription);
        var lblSeperator = new kony.ui.Label({
            "bottom": "0px",
            "height": "1px",
            "id": "lblSeperator",
            "isVisible": true,
            "left": "35px",
            "right": "45px",
            "skin": "sknlblSeperator",
            "text": ".",
            "top": "-5dp",
            "width": "95%",
            "zIndex": 1
        }, {
            "contentAlignment": constants.CONTENT_ALIGN_TOP_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, {});
        flxAdminConsoleLogList.add(flxAdminConsoleLogListSelected, flxGroupDesc, lblSeperator);
        return flxAdminConsoleLogList;
    }
})