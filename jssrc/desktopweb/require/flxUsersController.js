define("userflxUsersController", {
    //Type your controller code here 
});
define("flxUsersControllerActions", {
    /*
        This is an auto generated file and any modifications to it may result in corruption of the action sequence.
    */
    AS_FlexContainer_c805bcc4d3ad47418d4b8b9e07492755: function AS_FlexContainer_c805bcc4d3ad47418d4b8b9e07492755(eventobject, context) {
        var self = this;
        this.executeOnParent("onClickOptions");
    }
});
define("flxUsersController", ["userflxUsersController", "flxUsersControllerActions"], function() {
    var controller = require("userflxUsersController");
    var controllerActions = ["flxUsersControllerActions"];
    return kony.visualizer.mixinControllerActions(controller, controllerActions);
});
