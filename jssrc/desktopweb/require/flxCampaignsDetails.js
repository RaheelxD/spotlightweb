define("flxCampaignsDetails", function() {
    return function(controller) {
        var flxCampaignsDetails = new kony.ui.FlexContainer({
            "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
            "clipBounds": true,
            "id": "flxCampaignsDetails",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "isModalContainer": false,
            "skin": "sknflxffffffop100"
        }, {}, {});
        flxCampaignsDetails.setDefaultUnit(kony.flex.DP);
        var flxCampaignsDetailsInner = new kony.ui.FlexContainer({
            "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
            "clipBounds": true,
            "id": "flxCampaignsDetailsInner",
            "isVisible": true,
            "layoutType": kony.flex.FLOW_VERTICAL,
            "left": "0dp",
            "isModalContainer": false,
            "skin": "sknFlxbgFFFFFFbdrD7D9E0rd3px",
            "top": "10dp",
            "width": "100%"
        }, {}, {});
        flxCampaignsDetailsInner.setDefaultUnit(kony.flex.DP);
        var flxCollapse = new kony.ui.FlexContainer({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "clipBounds": true,
            "height": "50dp",
            "id": "flxCollapse",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "left": "0dp",
            "isModalContainer": false,
            "skin": "sknflxf5f6f8Op100",
            "top": "0dp",
            "width": "100%"
        }, {}, {});
        flxCollapse.setDefaultUnit(kony.flex.DP);
        var flxDropdown = new kony.ui.FlexContainer({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "centerY": "50%",
            "clipBounds": true,
            "height": "14dp",
            "id": "flxDropdown",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "left": "15dp",
            "isModalContainer": false,
            "onClick": controller.AS_FlexContainer_aeb8168ea3624740bf06d583316c5596,
            "skin": "sknCursor",
            "width": "14dp",
            "zIndex": 2
        }, {}, {});
        flxDropdown.setDefaultUnit(kony.flex.DP);
        var fonticonArrow = new kony.ui.Label({
            "centerX": "50%",
            "centerY": "50%",
            "height": "14dp",
            "id": "fonticonArrow",
            "isVisible": true,
            "skin": "sknfontIconDescRightArrow14px",
            "text": "",
            "width": "14dp",
            "zIndex": 1
        }, {
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, {});
        flxDropdown.add(fonticonArrow);
        var lblName = new kony.ui.Label({
            "centerY": "50%",
            "id": "lblName",
            "isVisible": true,
            "left": "38dp",
            "skin": "sknLbl192B45LatoRegular14px",
            "text": "SapphireCreditCard_Aug19",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, {
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, {});
        var flxStatus = new kony.ui.FlexContainer({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "clipBounds": true,
            "height": "100%",
            "id": "flxStatus",
            "isVisible": true,
            "layoutType": kony.flex.FLOW_HORIZONTAL,
            "reverseLayoutDirection": true,
            "isModalContainer": false,
            "right": "28dp",
            "skin": "slFbox",
            "top": 0,
            "width": "90dp",
            "zIndex": 1
        }, {}, {});
        flxStatus.setDefaultUnit(kony.flex.DP);
        var lblStatus = new kony.ui.Label({
            "centerY": "50%",
            "id": "lblStatus",
            "isVisible": true,
            "left": "5dp",
            "skin": "sknlblLatoRegular484b5213px",
            "text": "Active",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, {
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, {});
        var fontIconStatusImg = new kony.ui.Label({
            "centerY": "50%",
            "id": "fontIconStatusImg",
            "isVisible": true,
            "left": "0dp",
            "skin": "sknFontIconActivate",
            "text": "",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, {
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, {});
        flxStatus.add(lblStatus, fontIconStatusImg);
        flxCollapse.add(flxDropdown, lblName, flxStatus);
        var flxExpand = new kony.ui.FlexContainer({
            "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
            "clipBounds": true,
            "id": "flxExpand",
            "isVisible": false,
            "layoutType": kony.flex.FREE_FORM,
            "left": "0dp",
            "isModalContainer": false,
            "skin": "slFbox",
            "top": "0dp",
            "width": "100%",
            "zIndex": 2
        }, {}, {});
        flxExpand.setDefaultUnit(kony.flex.DP);
        var lblStartDateTimeHeader = new kony.ui.Label({
            "id": "lblStartDateTimeHeader",
            "isVisible": true,
            "left": "38dp",
            "skin": "sknlblLato696c7311px",
            "i18n_text": "kony.i18n.getLocalizedString(\"i18n.common.startDateCaps\")",
            "top": "18dp",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, {
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, {});
        var lblStartDateTime = new kony.ui.Label({
            "id": "lblStartDateTime",
            "isVisible": true,
            "left": "38dp",
            "skin": "sknlbl485C75LatoSemiBold13px",
            "text": "08/01/2019 12:00 AM",
            "top": "38dp",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, {
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, {});
        var lblEndDateTimeHeader = new kony.ui.Label({
            "id": "lblEndDateTimeHeader",
            "isVisible": true,
            "left": "40%",
            "skin": "sknlblLato696c7311px",
            "i18n_text": "kony.i18n.getLocalizedString(\"i18n.common.endDateCaps\")",
            "top": "18dp",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, {
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, {});
        var lblEndDateTime = new kony.ui.Label({
            "id": "lblEndDateTime",
            "isVisible": true,
            "left": "40%",
            "skin": "sknlbl485C75LatoSemiBold13px",
            "text": "08/31/2019 11:59 PM",
            "top": "38dp",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, {
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, {});
        var lblUsersCountHeader = new kony.ui.Label({
            "id": "lblUsersCountHeader",
            "isVisible": true,
            "left": "75%",
            "skin": "sknlblLato696c7311px",
            "i18n_text": "kony.i18n.getLocalizedString(\"i18n.ProfileManagement.reachedUsersCountCaps\")",
            "top": "18dp",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, {
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, {});
        var lblUsersCount = new kony.ui.Label({
            "id": "lblUsersCount",
            "isVisible": true,
            "left": "75%",
            "skin": "sknlbl485C75LatoSemiBold13px",
            "text": "IN App : 2223 \n Offline : 896",
            "top": "38dp",
            "width": "130dp",
            "zIndex": 1
        }, {
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, {});
        var lblDescriptionHeader = new kony.ui.Label({
            "id": "lblDescriptionHeader",
            "isVisible": true,
            "left": "38dp",
            "skin": "sknLbl78818A11px",
            "i18n_text": "kony.i18n.getLocalizedString(\"i18n.ConfigurationBundles.descriptionInCaps\")",
            "top": "75dp",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, {
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, {});
        var lblDescription = new kony.ui.Label({
            "bottom": "28dp",
            "id": "lblDescription",
            "isVisible": true,
            "left": "38dp",
            "right": "28dp",
            "skin": "sknlblLatoRegular484b5213px",
            "text": "Promotion for Sapphire Credit Card, targeted  at all customers who live in Austin, TX Promotion for Sapphire Credit Card, targeted  at all customers who live in Austin, TX Promotion for Sapphire Credit Card, targeted  at all customers who live in Austin, TX Promotion for Sapphire Credit Card, targeted  at all customers who live in Austin, TX Promotion for Sapphire Credit Card, targeted  at all customers who live in Austin, TX",
            "top": "100dp",
            "zIndex": 1
        }, {
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, {});
        flxExpand.add(lblStartDateTimeHeader, lblStartDateTime, lblEndDateTimeHeader, lblEndDateTime, lblUsersCountHeader, lblUsersCount, lblDescriptionHeader, lblDescription);
        flxCampaignsDetailsInner.add(flxCollapse, flxExpand);
        flxCampaignsDetails.add(flxCampaignsDetailsInner);
        return flxCampaignsDetails;
    }
})