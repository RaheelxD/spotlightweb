define("PolicyModule/frmPolicies", function() {
    return function(controller) {
        function addWidgetsfrmPolicies() {
            this.setDefaultUnit(kony.flex.DP);
            var flxMain = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "900dp",
                "id": "flxMain",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "slFbox0b7aaa2e3bfa340",
                "top": "0dp",
                "width": "100%",
                "zIndex": 1
            }, {}, {});
            flxMain.setDefaultUnit(kony.flex.DP);
            var flxLeftPanel = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "100%",
                "id": "flxLeftPanel",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0dp",
                "width": "306px",
                "zIndex": 1
            }, {}, {});
            flxLeftPanel.setDefaultUnit(kony.flex.DP);
            var leftMenuNew = new com.adminConsole.navigation.leftMenuNew({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "height": "100%",
                "id": "leftMenuNew",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "masterType": constants.MASTER_TYPE_DEFAULT,
                "isModalContainer": false,
                "skin": "sknFlxBg003E75Op100",
                "top": "0dp",
                "width": "100%",
                "overrides": {
                    "brandLogo": {
                        "src": "infinity_dbx_c360_logo_white_2x.png"
                    },
                    "imgBottomLogo": {
                        "src": "logo_white_2x.png"
                    },
                    "leftMenuNew": {
                        "bottom": "viz.val_cleared",
                        "centerX": "viz.val_cleared",
                        "centerY": "viz.val_cleared",
                        "height": "100%",
                        "left": "0dp",
                        "maxHeight": "viz.val_cleared",
                        "maxWidth": "viz.val_cleared",
                        "minHeight": "viz.val_cleared",
                        "minWidth": "viz.val_cleared",
                        "right": "viz.val_cleared",
                        "top": "0dp",
                        "width": "100%"
                    },
                    "segMenu": {
                        "left": "0dp",
                        "top": "0dp"
                    }
                }
            }, {
                "overrides": {}
            }, {
                "overrides": {}
            });
            flxLeftPanel.add(leftMenuNew);
            var flxRightPanel = new kony.ui.FlexContainer({
                "clipBounds": true,
                "height": "100%",
                "id": "flxRightPanel",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "305px",
                "isModalContainer": false,
                "right": 0,
                "skin": "CopyslFbox0a3ff363ad54a4d",
                "top": "0dp",
                "zIndex": 1
            }, {}, {});
            flxRightPanel.setDefaultUnit(kony.flex.DP);
            var flxMainHeader = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "108dp",
                "id": "flxMainHeader",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0dp",
                "width": "100%",
                "zIndex": 1
            }, {}, {});
            flxMainHeader.setDefaultUnit(kony.flex.DP);
            var mainHeader = new com.adminConsole.header.mainHeader({
                "height": "106px",
                "id": "mainHeader",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0",
                "masterType": constants.MASTER_TYPE_DEFAULT,
                "isModalContainer": false,
                "skin": "CopyslFbox1",
                "top": "0",
                "width": "100%",
                "overrides": {
                    "flxButtons": {
                        "isVisible": false
                    },
                    "flxHeaderSeperator": {
                        "isVisible": false
                    },
                    "imgLogout": {
                        "src": "img_logout.png"
                    },
                    "lblHeading": {
                        "i18n_text": "kony.i18n.getLocalizedString(\"i18n.leftmenu.credentialManagement\")",
                        "left": "20dp"
                    },
                    "mainHeader": {
                        "left": "0",
                        "top": "0"
                    }
                }
            }, {
                "overrides": {}
            }, {
                "overrides": {}
            });
            flxMainHeader.add(mainHeader);
            var flxHeaderDropdown = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                "clipBounds": true,
                "id": "flxHeaderDropdown",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "isModalContainer": false,
                "right": "35dp",
                "skin": "slFbox",
                "top": "20dp",
                "width": "400dp",
                "zIndex": 5
            }, {}, {});
            flxHeaderDropdown.setDefaultUnit(kony.flex.DP);
            var dropdownMainHeader = new com.adminConsole.common.dropdownMainHeader({
                "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                "id": "dropdownMainHeader",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "masterType": constants.MASTER_TYPE_DEFAULT,
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0dp",
                "width": "100%",
                "overrides": {
                    "flxDropdown": {
                        "isVisible": false
                    },
                    "imgUpArrow": {
                        "src": "uparrow_2x.png"
                    }
                }
            }, {
                "overrides": {}
            }, {
                "overrides": {}
            });
            flxHeaderDropdown.add(dropdownMainHeader);
            var flxMainSubHeader = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "63px",
                "id": "flxMainSubHeader",
                "isVisible": false,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "106dp",
                "width": "100%",
                "zIndex": 1
            }, {}, {});
            flxMainSubHeader.setDefaultUnit(kony.flex.DP);
            var subHeader = new com.adminConsole.header.subHeader({
                "height": "48dp",
                "id": "subHeader",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "masterType": constants.MASTER_TYPE_DEFAULT,
                "isModalContainer": false,
                "skin": "CopyslFbox2",
                "top": "0dp",
                "width": "100%",
                "overrides": {
                    "flxMenu": {
                        "isVisible": false
                    },
                    "flxSearch": {
                        "centerY": "viz.val_cleared"
                    },
                    "flxSearchContainer": {
                        "top": "12dp"
                    },
                    "flxSubHeader": {
                        "centerY": "viz.val_cleared",
                        "isVisible": false
                    },
                    "subHeader": {
                        "centerY": "viz.val_cleared"
                    }
                }
            }, {
                "overrides": {}
            }, {
                "overrides": {}
            });
            flxMainSubHeader.add(subHeader);
            var flxScrollMainContent = new kony.ui.FlexContainer({
                "bottom": "10px",
                "clipBounds": true,
                "id": "flxScrollMainContent",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "isModalContainer": false,
                "right": "0dp",
                "skin": "slFbox",
                "top": "110dp",
                "width": "100%",
                "zIndex": 1
            }, {}, {});
            flxScrollMainContent.setDefaultUnit(kony.flex.DP);
            var flxPolicies = new kony.ui.FlexScrollContainer({
                "allowHorizontalBounce": false,
                "allowVerticalBounce": true,
                "bounces": true,
                "clipBounds": true,
                "enableScrolling": true,
                "height": "100%",
                "horizontalScrollIndicator": true,
                "id": "flxPolicies",
                "isVisible": false,
                "layoutType": kony.flex.FLOW_HORIZONTAL,
                "left": "0dp",
                "pagingEnabled": false,
                "right": "20dp",
                "scrollDirection": kony.flex.SCROLL_VERTICAL,
                "skin": "slFSbox",
                "top": "0dp",
                "verticalScrollIndicator": true,
                "width": "100%"
            }, {}, {});
            flxPolicies.setDefaultUnit(kony.flex.DP);
            var flxPolicyForCustomers = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                "clipBounds": true,
                "id": "flxPolicyForCustomers",
                "isVisible": true,
                "layoutType": kony.flex.FLOW_VERTICAL,
                "left": "21px",
                "isModalContainer": false,
                "skin": "CopyslFbox0j190b3ed67d04b",
                "top": "0dp",
                "width": "96%",
                "zIndex": 2
            }, {}, {});
            flxPolicyForCustomers.setDefaultUnit(kony.flex.DP);
            var customersPolicies = new com.customer360.common.policies({
                "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                "id": "customersPolicies",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "masterType": constants.MASTER_TYPE_DEFAULT,
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0dp",
                "width": "100%",
                "overrides": {
                    "flxCustomerHeader": {
                        "height": "82dp",
                        "left": "0dp",
                        "right": "viz.val_cleared",
                        "top": "0dp",
                        "width": "100%"
                    },
                    "flxPasswordPoliciesDataWrapper": {
                        "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                        "left": "20dp",
                        "right": "viz.val_cleared",
                        "top": "21dp",
                        "width": "97.50%"
                    },
                    "flxPasswordPoliciesWrapper": {
                        "width": "100%"
                    },
                    "flxPoliciesDataWrapper": {
                        "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                        "width": "97.50%"
                    },
                    "flxPoliciesWrapper": {
                        "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                        "bottom": "20dp",
                        "width": "100%"
                    },
                    "flxUsernamePolicies": {
                        "autogrowMode": kony.flex.AUTOGROW_HEIGHT
                    },
                    "flxUsernamePoliciesName": {
                        "width": "100%"
                    },
                    "lblCustomerHeader": {
                        "bottom": "31dp",
                        "i18n_text": "kony.i18n.getLocalizedString(\"i18n.policies.customerPolicies\")",
                        "left": "37dp",
                        "top": "32dp"
                    },
                    "lblCustomerIcon": {
                        "centerX": "viz.val_cleared",
                        "centerY": "viz.val_cleared",
                        "height": kony.flex.USE_PREFFERED_SIZE,
                        "isVisible": true,
                        "right": "52dp",
                        "text": "",
                        "top": "32dp",
                        "width": "70dp"
                    },
                    "lblEditIconPassword": {
                        "right": "15px"
                    },
                    "lblEditIconUsername": {
                        "right": "15dp"
                    },
                    "lblPasswordPolicies": {
                        "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmPolicies.passwordPolicies\")"
                    },
                    "lblPasswordPoliciesValue": {
                        "width": "91.30%"
                    },
                    "lblUsernamePoliciesValue": {
                        "width": "91.30%"
                    },
                    "policies": {
                        "bottom": "viz.val_cleared",
                        "width": "100%"
                    }
                }
            }, {
                "overrides": {}
            }, {
                "overrides": {}
            });
            flxPolicyForCustomers.add(customersPolicies);
            var flxPolicyForInternalUsers = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "439px",
                "id": "flxPolicyForInternalUsers",
                "isVisible": false,
                "layoutType": kony.flex.FLOW_VERTICAL,
                "left": "20dp",
                "isModalContainer": false,
                "right": "20px",
                "skin": "CopyslFbox0j190b3ed67d04b",
                "top": "0dp",
                "width": "460dp",
                "zIndex": 2
            }, {}, {});
            flxPolicyForInternalUsers.setDefaultUnit(kony.flex.DP);
            var internalUserPolicies = new com.customer360.common.policies({
                "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                "id": "internalUserPolicies",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "masterType": constants.MASTER_TYPE_DEFAULT,
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0dp",
                "width": "100%",
                "overrides": {
                    "flxCustomerHeader": {
                        "left": "20dp",
                        "right": "20dp",
                        "width": "91%"
                    },
                    "flxPasswordPoliciesDataWrapper": {
                        "height": "135dp",
                        "left": "37dp",
                        "top": "32dp",
                        "width": "91%"
                    },
                    "flxPoliciesDataWrapper": {
                        "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                        "width": "91%"
                    },
                    "lblCustomerHeader": {
                        "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmPolicies.forInternalUsers\")"
                    },
                    "lblCustomerIcon": {
                        "isVisible": true,
                        "text": ""
                    },
                    "lblEditIconPassword": {
                        "right": "15px"
                    },
                    "lblEditIconUsername": {
                        "right": "15px"
                    },
                    "lblPasswordPolicies": {
                        "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmPolicies.passwordPolicies\")"
                    },
                    "policies": {
                        "width": "100%"
                    }
                }
            }, {
                "overrides": {}
            }, {
                "overrides": {}
            });
            flxPolicyForInternalUsers.add(internalUserPolicies);
            flxPolicies.add(flxPolicyForCustomers, flxPolicyForInternalUsers);
            var flxPoliciesParentRulesView = new kony.ui.FlexContainer({
                "bottom": "20dp",
                "clipBounds": true,
                "id": "flxPoliciesParentRulesView",
                "isVisible": false,
                "layoutType": kony.flex.FLOW_VERTICAL,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "30dp",
                "width": "100%"
            }, {}, {});
            flxPoliciesParentRulesView.setDefaultUnit(kony.flex.DP);
            var flxPoliciesRulesView = new kony.ui.FlexScrollContainer({
                "allowHorizontalBounce": false,
                "allowVerticalBounce": true,
                "bounces": true,
                "clipBounds": true,
                "enableScrolling": true,
                "height": "100%",
                "horizontalScrollIndicator": true,
                "id": "flxPoliciesRulesView",
                "isVisible": true,
                "layoutType": kony.flex.FLOW_VERTICAL,
                "left": "0dp",
                "pagingEnabled": false,
                "scrollDirection": kony.flex.SCROLL_VERTICAL,
                "skin": "slFSbox",
                "top": "0dp",
                "verticalScrollIndicator": true,
                "width": "100%",
                "zIndex": 1
            }, {}, {});
            flxPoliciesRulesView.setDefaultUnit(kony.flex.DP);
            var policiesRulesView = new com.customer360.common.policiesRulesView({
                "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                "id": "policiesRulesView",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "20dp",
                "masterType": constants.MASTER_TYPE_DEFAULT,
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0dp",
                "width": "96%",
                "overrides": {
                    "flxLengthOfPasswordValues": {
                        "height": "60dp"
                    },
                    "lblSeperator": {
                        "right": "viz.val_cleared",
                        "width": "96%"
                    },
                    "policiesRulesView": {
                        "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                        "left": "20dp",
                        "width": "96%"
                    }
                }
            }, {
                "overrides": {}
            }, {
                "overrides": {}
            });
            var flxPolicyDescription = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                "clipBounds": true,
                "id": "flxPolicyDescription",
                "isVisible": true,
                "layoutType": kony.flex.FLOW_VERTICAL,
                "left": "21dp",
                "isModalContainer": false,
                "skin": "CopysknflxffffffOp0e4fae09f31bc4e",
                "top": "20dp",
                "width": "96%",
                "zIndex": 1
            }, {}, {});
            flxPolicyDescription.setDefaultUnit(kony.flex.DP);
            var flxAddNewButton = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                "clipBounds": true,
                "id": "flxAddNewButton",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0dp",
                "width": "100%"
            }, {}, {});
            flxAddNewButton.setDefaultUnit(kony.flex.DP);
            var btnAddNew = new kony.ui.Button({
                "height": "22dp",
                "id": "btnAddNew",
                "isVisible": true,
                "right": "25dp",
                "skin": "sknbtnf7f7faLatoRegular12Px485c75Border1px485C75Radius20px",
                "text": "Add New",
                "top": "22dp",
                "width": "82dp",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_CENTER,
                "displayText": true,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {
                "hoverSkin": "sknbtnffffffLatoRegular12Px485c75Border1px485C75Radius20px"
            });
            var lblPolicyDescriptionName = new kony.ui.Label({
                "id": "lblPolicyDescriptionName",
                "isVisible": true,
                "left": "21dp",
                "skin": "sknLbl192b45LatoBold12px",
                "text": "USERNAME POLICY DESCRIPTION FOR CUSTOMERS",
                "top": "22dp",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxAddNewButton.add(btnAddNew, lblPolicyDescriptionName);
            var flxToastMsg = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                "clipBounds": true,
                "id": "flxToastMsg",
                "isVisible": false,
                "layoutType": kony.flex.FLOW_VERTICAL,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0dp",
                "width": "100%"
            }, {}, {});
            flxToastMsg.setDefaultUnit(kony.flex.DP);
            var toastMessageWithWarning = new com.adminConsole.common.toastMessageWithWarning({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "height": "70px",
                "id": "toastMessageWithWarning",
                "isVisible": false,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "masterType": constants.MASTER_TYPE_DEFAULT,
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "5dp",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "overrides": {
                    "flxToastContainer": {
                        "centerX": "viz.val_cleared",
                        "centerY": "50%",
                        "left": "20dp",
                        "right": "20dp",
                        "width": kony.flex.USE_PREFFERED_SIZE
                    },
                    "lblToastMessageLeft": {
                        "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmPolicies.Please_update_statements\")",
                        "left": "2px"
                    },
                    "lblToastMessageRight": {
                        "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmPolicies.usernamePoliciesForCustomers\")",
                        "left": "5px"
                    },
                    "toastMessageWithWarning": {
                        "bottom": "viz.val_cleared",
                        "isVisible": false,
                        "left": "0dp",
                        "top": "5dp",
                        "width": kony.flex.USE_PREFFERED_SIZE
                    }
                }
            }, {
                "overrides": {}
            }, {
                "overrides": {}
            });
            flxToastMsg.add(toastMessageWithWarning);
            var flxAddNewPolicyDescription = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                "clipBounds": true,
                "id": "flxAddNewPolicyDescription",
                "isVisible": true,
                "layoutType": kony.flex.FLOW_VERTICAL,
                "left": "21dp",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "17dp",
                "width": "100%",
                "zIndex": 1
            }, {}, {});
            flxAddNewPolicyDescription.setDefaultUnit(kony.flex.DP);
            var flxAddPolicyHeadings = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "35dp",
                "id": "flxAddPolicyHeadings",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0dp",
                "width": "96%"
            }, {}, {});
            flxAddPolicyHeadings.setDefaultUnit(kony.flex.DP);
            var lblAddPolicyDescName = new kony.ui.Label({
                "id": "lblAddPolicyDescName",
                "isVisible": true,
                "left": "0dp",
                "skin": "sknLblLato485c7513px",
                "text": "Policy Description",
                "top": "0dp",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var lblVariableReference = new kony.ui.Label({
                "id": "lblVariableReference",
                "isVisible": false,
                "left": "145dp",
                "skin": "sknLbl006CCA13pxKA",
                "text": "Variable Reference",
                "top": "0dp",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var lstDropDown = new kony.ui.ListBox({
                "id": "lstDropDown",
                "isVisible": true,
                "masterData": [
                    ["lbl1", "Select a language"],
                    ["lb2", "Placeholder Two"],
                    ["lb3", "Placeholder Three"]
                ],
                "right": "20dp",
                "selectedKey": "lbl1",
                "skin": "slListBox",
                "top": "0dp",
                "width": "200dp",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [3, 0, 0, 0],
                "paddingInPixel": false
            }, {
                "multiSelect": false
            });
            flxAddPolicyHeadings.add(lblAddPolicyDescName, lblVariableReference, lstDropDown);
            var flxRichText = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "140px",
                "id": "flxRichText",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0px",
                "isModalContainer": false,
                "right": "0px",
                "skin": "CopyslFbox0d934b969a90b40",
                "top": "5px",
                "width": "96%",
                "zIndex": 1
            }, {}, {});
            flxRichText.setDefaultUnit(kony.flex.DP);
            var rtxTnC = new kony.ui.Browser({
                "centerX": "50%",
                "detectTelNumber": true,
                "enableZoom": false,
                "height": "100%",
                "id": "rtxTnC",
                "isVisible": true,
                "left": "0px",
                "setAsContent": false,
                "requestURLConfig": {
                    "URL": "richtext.html",
                    "requestMethod": constants.BROWSER_REQUEST_METHOD_GET
                },
                "right": "0px",
                "top": "0px",
                "zIndex": 1
            }, {}, {});
            flxRichText.add(rtxTnC);
            var flxErrorLanguage = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "bottom": "0px",
                "clipBounds": true,
                "height": "20px",
                "id": "flxErrorLanguage",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0px",
                "isModalContainer": false,
                "right": "0px",
                "skin": "slFbox",
                "top": "5px",
                "width": "100%",
                "zIndex": 1
            }, {}, {});
            flxErrorLanguage.setDefaultUnit(kony.flex.DP);
            var lblErrorLang = new kony.ui.Label({
                "centerY": "50%",
                "height": "15px",
                "id": "lblErrorLang",
                "isVisible": true,
                "left": 0,
                "skin": "sknFontIconError",
                "text": "",
                "width": "15px",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var lblErrorLangMsg = new kony.ui.Label({
                "centerY": "50%",
                "id": "lblErrorLangMsg",
                "isVisible": true,
                "left": "15px",
                "skin": "sknLabelRed",
                "text": "Please Select a language",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxErrorLanguage.add(lblErrorLang, lblErrorLangMsg);
            var flxCommonButtons = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                "clipBounds": true,
                "id": "flxCommonButtons",
                "isVisible": true,
                "layoutType": kony.flex.FLOW_HORIZONTAL,
                "isModalContainer": false,
                "right": "45dp",
                "skin": "slFbox",
                "top": "20dp",
                "width": "220dp"
            }, {}, {});
            flxCommonButtons.setDefaultUnit(kony.flex.DP);
            var btnCancel = new kony.ui.Button({
                "focusSkin": "sknBtnLatoRegulara5abc413pxKA",
                "height": "40dp",
                "id": "btnCancel",
                "isVisible": true,
                "right": "10px",
                "skin": "sknBtnLatoRegulara5abc413pxKA",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.permission.CANCEL\")",
                "top": "0dp",
                "width": "100px",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_CENTER,
                "displayText": true,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {
                "hoverSkin": "sknbtnffffffLatoBolda5abc413Pxd7d8e01pxBorderRadius20px"
            });
            var btnSave = new kony.ui.Button({
                "focusSkin": "sknBtn003E75LatoRegular13pxFFFFFFRad20px",
                "height": "40dp",
                "id": "btnSave",
                "isVisible": true,
                "right": "114px",
                "skin": "sknBtn003E75LatoRegular13pxFFFFFFRad20px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.policies.ADD\")",
                "top": "0dp",
                "width": "100px",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_CENTER,
                "displayText": true,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {
                "hoverSkin": "sknBtn005198LatoRegular13pxFFFFFFRad20px"
            });
            flxCommonButtons.add(btnCancel, btnSave);
            flxAddNewPolicyDescription.add(flxAddPolicyHeadings, flxRichText, flxErrorLanguage, flxCommonButtons);
            var segPolicyDescriptions = new kony.ui.SegmentedUI2({
                "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                "bottom": "10dp",
                "data": [{
                    "lblDelete": "",
                    "lblEdit": "",
                    "lblLanguage": "",
                    "lblLanguageDescription": "RichText",
                    "lblSeperator": ""
                }],
                "groupCells": false,
                "id": "segPolicyDescriptions",
                "isVisible": true,
                "left": "21dp",
                "needPageIndicator": true,
                "pageOffDotImage": "pageoffdot.png",
                "pageOnDotImage": "pageondot.png",
                "retainSelection": false,
                "rowFocusSkin": "seg2Focus",
                "rowSkin": "seg2Normal",
                "rowTemplate": "flxPolicyDescription",
                "sectionHeaderSkin": "sliPhoneSegmentHeader",
                "selectionBehavior": constants.SEGUI_DEFAULT_BEHAVIOR,
                "separatorRequired": false,
                "separatorThickness": 0,
                "showScrollbars": false,
                "top": "40dp",
                "viewType": constants.SEGUI_VIEW_TYPE_TABLEVIEW,
                "widgetDataMap": {
                    "flxDelete": "flxDelete",
                    "flxEdit": "flxEdit",
                    "flxLanguage": "flxLanguage",
                    "flxPolicyDescription": "flxPolicyDescription",
                    "lblDelete": "lblDelete",
                    "lblEdit": "lblEdit",
                    "lblLanguage": "lblLanguage",
                    "lblLanguageDescription": "lblLanguageDescription",
                    "lblSeperator": "lblSeperator"
                },
                "width": "96%",
                "zIndex": 1
            }, {
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxPolicyDescription.add(flxAddNewButton, flxToastMsg, flxAddNewPolicyDescription, segPolicyDescriptions);
            flxPoliciesRulesView.add(policiesRulesView, flxPolicyDescription);
            flxPoliciesParentRulesView.add(flxPoliciesRulesView);
            var flxPoliciesEdit = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "bottom": "10dp",
                "clipBounds": true,
                "id": "flxPoliciesEdit",
                "isVisible": false,
                "layoutType": kony.flex.FLOW_VERTICAL,
                "left": "20dp",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "30dp",
                "width": "96%",
                "zIndex": 1
            }, {}, {});
            flxPoliciesEdit.setDefaultUnit(kony.flex.DP);
            var flxPoliciesWrapper = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                "clipBounds": true,
                "id": "flxPoliciesWrapper",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "isModalContainer": false,
                "right": "0dp",
                "skin": "sknflxffffffOp100Border5e90cbRadius3Px",
                "top": "0dp",
                "width": "100%",
                "zIndex": 1
            }, {}, {});
            flxPoliciesWrapper.setDefaultUnit(kony.flex.DP);
            var flxPoliciesDataWrapper = new kony.ui.FlexScrollContainer({
                "allowHorizontalBounce": false,
                "allowVerticalBounce": true,
                "bounces": true,
                "clipBounds": true,
                "enableScrolling": true,
                "height": "350dp",
                "horizontalScrollIndicator": true,
                "id": "flxPoliciesDataWrapper",
                "isVisible": true,
                "layoutType": kony.flex.FLOW_VERTICAL,
                "left": "20dp",
                "pagingEnabled": false,
                "right": "20dp",
                "scrollDirection": kony.flex.SCROLL_VERTICAL,
                "skin": "slFSbox",
                "top": "23dp",
                "verticalScrollIndicator": true,
                "width": "97.50%",
                "zIndex": 1
            }, {}, {});
            flxPoliciesDataWrapper.setDefaultUnit(kony.flex.DP);
            var lblPoliciesRuleName = new kony.ui.Label({
                "id": "lblPoliciesRuleName",
                "isVisible": true,
                "left": "0px",
                "skin": "LblLatoRegular485c7516px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmPolicies.passwordPoliciesForCustomers\")",
                "top": "0dp",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var lblSeperator = new kony.ui.Label({
                "bottom": "1dp",
                "height": "1px",
                "id": "lblSeperator",
                "isVisible": true,
                "left": "0dp",
                "right": "0px",
                "skin": "HeaderSeparator",
                "top": "22px",
                "width": "100%",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var lblLengthOfPassword = new kony.ui.Label({
                "id": "lblLengthOfPassword",
                "isVisible": true,
                "left": "0px",
                "skin": "sknLbl192b45LatoBold12px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmPolicies.lengthOfPassword\")",
                "top": "18dp",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var flxLengthOfPasswordValues = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "100dp",
                "id": "flxLengthOfPasswordValues",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "20dp",
                "width": "100%",
                "zIndex": 1
            }, {}, {});
            flxLengthOfPasswordValues.setDefaultUnit(kony.flex.DP);
            var flxCharacters = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "100%",
                "id": "flxCharacters",
                "isVisible": true,
                "layoutType": kony.flex.FLOW_HORIZONTAL,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0dp",
                "width": "100%"
            }, {}, {});
            flxCharacters.setDefaultUnit(kony.flex.DP);
            var flxMinCharacters = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "100%",
                "id": "flxMinCharacters",
                "isVisible": true,
                "layoutType": kony.flex.FLOW_VERTICAL,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0dp",
                "width": "210dp"
            }, {}, {});
            flxMinCharacters.setDefaultUnit(kony.flex.DP);
            var lblMinNumberChars = new kony.ui.Label({
                "id": "lblMinNumberChars",
                "isVisible": true,
                "left": "0dp",
                "skin": "sknLbl485C75LatoRegular13Px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmPolicies.minNumOfCharacters\")",
                "top": "0dp",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var MinCharacters = new com.customer360.common.CharactersplusMinus({
                "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                "id": "MinCharacters",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "masterType": constants.MASTER_TYPE_DEFAULT,
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "14dp",
                "width": "181dp",
                "overrides": {
                    "CharactersplusMinus": {
                        "top": "14dp"
                    },
                    "flxMinus": {
                        "bottom": "0dp",
                        "height": "40dp",
                        "right": "0dp",
                        "top": "0dp",
                        "width": "54dp"
                    },
                    "flxMinusDisable": {
                        "bottom": 2,
                        "height": "38dp",
                        "right": "1dp",
                        "top": "1dp",
                        "width": "53dp"
                    },
                    "flxPlus": {
                        "bottom": "0dp",
                        "height": "40dp",
                        "left": "0dp",
                        "top": "0dp",
                        "width": "54dp"
                    },
                    "flxPlusDisable": {
                        "bottom": "2dp",
                        "height": "38dp",
                        "left": "1dp",
                        "top": "1dp",
                        "width": "53dp"
                    },
                    "flxSeperator1": {
                        "width": "1dp"
                    },
                    "flxSeperator2": {
                        "width": "1dp"
                    },
                    "lblMinus": {
                        "height": "36dp",
                        "width": "50dp"
                    },
                    "lblPlus": {
                        "height": "36dp",
                        "right": "viz.val_cleared",
                        "text": "-",
                        "width": "50dp"
                    },
                    "tbxcharcterSize": {
                        "text": "8"
                    }
                }
            }, {
                "overrides": {}
            }, {
                "overrides": {}
            });
            var flxErrorMessage = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "bottom": "80px",
                "clipBounds": true,
                "height": "20px",
                "id": "flxErrorMessage",
                "isVisible": false,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0px",
                "isModalContainer": false,
                "right": "0px",
                "skin": "slFbox",
                "top": "5px",
                "width": "100%",
                "zIndex": 1
            }, {}, {});
            flxErrorMessage.setDefaultUnit(kony.flex.DP);
            var lblIconOptions = new kony.ui.Label({
                "centerY": "50%",
                "height": "15px",
                "id": "lblIconOptions",
                "isVisible": true,
                "left": 0,
                "skin": "sknFontIconError",
                "text": "",
                "width": "15px",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var lblErrormsg = new kony.ui.Label({
                "centerY": "50%",
                "id": "lblErrormsg",
                "isVisible": true,
                "left": "15px",
                "skin": "sknLabelRed",
                "text": "Range is between 8 and 64",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxErrorMessage.add(lblIconOptions, lblErrormsg);
            flxMinCharacters.add(lblMinNumberChars, MinCharacters, flxErrorMessage);
            var flxMaxCharacters = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "100%",
                "id": "flxMaxCharacters",
                "isVisible": true,
                "layoutType": kony.flex.FLOW_VERTICAL,
                "left": "72dp",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0dp",
                "width": "210dp"
            }, {}, {});
            flxMaxCharacters.setDefaultUnit(kony.flex.DP);
            var lblMaxNumberChars = new kony.ui.Label({
                "id": "lblMaxNumberChars",
                "isVisible": true,
                "left": "0dp",
                "skin": "sknLbl485C75LatoRegular13Px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmPolicies.maxNumOfCharacters\")",
                "top": "0dp",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var MaxCharacters = new com.customer360.common.CharactersplusMinus({
                "height": "40dp",
                "id": "MaxCharacters",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "masterType": constants.MASTER_TYPE_DEFAULT,
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "14dp",
                "width": "181dp",
                "overrides": {
                    "CharactersplusMinus": {
                        "height": "40dp",
                        "top": "14dp",
                        "width": "181dp"
                    },
                    "flxMinus": {
                        "bottom": "0dp",
                        "height": "40dp",
                        "isVisible": true,
                        "right": "0dp",
                        "top": "0dp",
                        "width": "54dp"
                    },
                    "flxMinusDisable": {
                        "bottom": 2,
                        "height": "38dp",
                        "right": "1dp",
                        "top": "1dp",
                        "width": "53dp"
                    },
                    "flxPlus": {
                        "bottom": "0dp",
                        "height": "40dp",
                        "left": "0dp",
                        "top": "0dp",
                        "width": "54dp"
                    },
                    "flxPlusDisable": {
                        "bottom": "2dp",
                        "height": "38dp",
                        "left": "1dp",
                        "top": "1dp",
                        "width": "53dp"
                    },
                    "flxSeperator1": {
                        "width": "1dp"
                    },
                    "flxSeperator2": {
                        "width": "1dp"
                    },
                    "lblMinus": {
                        "centerY": "50%",
                        "height": "36dp",
                        "width": "50dp"
                    },
                    "lblPlus": {
                        "height": "36dp",
                        "width": "50dp"
                    },
                    "tbxcharcterSize": {
                        "height": "40dp",
                        "text": "64"
                    }
                }
            }, {
                "overrides": {}
            }, {
                "overrides": {}
            });
            flxMaxCharacters.add(lblMaxNumberChars, MaxCharacters);
            var flxSpecialCharacters = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "100%",
                "id": "flxSpecialCharacters",
                "isVisible": true,
                "layoutType": kony.flex.FLOW_VERTICAL,
                "left": "72dp",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0dp",
                "width": "210dp"
            }, {}, {});
            flxSpecialCharacters.setDefaultUnit(kony.flex.DP);
            var lblSpecialChars = new kony.ui.Label({
                "id": "lblSpecialChars",
                "isVisible": true,
                "left": "0dp",
                "right": "0dp",
                "skin": "sknLbl485C75LatoRegular13Px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmPolicies.specialCharactersSmall\")",
                "top": "0dp",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var flxSpecialCharsRadioButtons = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "40px",
                "id": "flxSpecialCharsRadioButtons",
                "isVisible": true,
                "layoutType": kony.flex.FLOW_HORIZONTAL,
                "left": "0px",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "14px",
                "width": "210px",
                "zIndex": 1
            }, {}, {});
            flxSpecialCharsRadioButtons.setDefaultUnit(kony.flex.DP);
            var flxYes = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "centerY": "50%",
                "clipBounds": true,
                "height": "20dp",
                "id": "flxYes",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "-2dp",
                "isModalContainer": false,
                "skin": "slFbox",
                "width": "20px",
                "zIndex": 1
            }, {}, {
                "hoverSkin": "sknCursor"
            });
            flxYes.setDefaultUnit(kony.flex.DP);
            var imgYes = new kony.ui.Image2({
                "centerX": "50%",
                "centerY": "50%",
                "height": "15dp",
                "id": "imgYes",
                "isVisible": true,
                "skin": "slImage",
                "src": "radio_selected.png",
                "width": "15px",
                "zIndex": 1
            }, {
                "imageScaleMode": constants.IMAGE_SCALE_MODE_MAINTAIN_ASPECT_RATIO,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxYes.add(imgYes);
            var lblYes = new kony.ui.Label({
                "centerY": "50%",
                "id": "lblYes",
                "isVisible": true,
                "left": "5dp",
                "skin": "slLabel0d20174dce8ea42",
                "text": "YES",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var flxNo = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "centerY": "50%",
                "clipBounds": true,
                "height": "20dp",
                "id": "flxNo",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "20px",
                "isModalContainer": false,
                "skin": "slFbox",
                "width": "20px",
                "zIndex": 1
            }, {}, {
                "hoverSkin": "sknCursor"
            });
            flxNo.setDefaultUnit(kony.flex.DP);
            var imgNo = new kony.ui.Image2({
                "centerX": "50%",
                "centerY": "50%",
                "height": "15dp",
                "id": "imgNo",
                "isVisible": true,
                "skin": "slImage",
                "src": "radio_notselected.png",
                "width": "15dp",
                "zIndex": 1
            }, {
                "imageScaleMode": constants.IMAGE_SCALE_MODE_MAINTAIN_ASPECT_RATIO,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxNo.add(imgNo);
            var lblNo = new kony.ui.Label({
                "centerY": "50%",
                "id": "lblNo",
                "isVisible": true,
                "left": "5dp",
                "skin": "slLabel0d20174dce8ea42",
                "text": "NO",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxSpecialCharsRadioButtons.add(flxYes, lblYes, flxNo, lblNo);
            flxSpecialCharacters.add(lblSpecialChars, flxSpecialCharsRadioButtons);
            flxCharacters.add(flxMinCharacters, flxMaxCharacters, flxSpecialCharacters);
            flxLengthOfPasswordValues.add(flxCharacters);
            var flxPasswordAdditionalRules = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "bottom": "20dp",
                "clipBounds": true,
                "height": "250dp",
                "id": "flxPasswordAdditionalRules",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0dp",
                "width": "100%",
                "zIndex": 1
            }, {}, {});
            flxPasswordAdditionalRules.setDefaultUnit(kony.flex.DP);
            var flxPasswordRules = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "100%",
                "id": "flxPasswordRules",
                "isVisible": true,
                "layoutType": kony.flex.FLOW_VERTICAL,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0dp",
                "width": "100%"
            }, {}, {});
            flxPasswordRules.setDefaultUnit(kony.flex.DP);
            var flxPasswordMustContain = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "70dp",
                "id": "flxPasswordMustContain",
                "isVisible": true,
                "layoutType": kony.flex.FLOW_VERTICAL,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0dp",
                "width": "100%"
            }, {}, {});
            flxPasswordMustContain.setDefaultUnit(kony.flex.DP);
            var lblPasswordMustContain = new kony.ui.Label({
                "id": "lblPasswordMustContain",
                "isVisible": true,
                "left": "0dp",
                "skin": "sknLbl192b45LatoBold12px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmPolicies.passwordMustContain\")",
                "top": "0dp",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var flxNoValueChecked = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "25dp",
                "id": "flxNoValueChecked",
                "isVisible": true,
                "layoutType": kony.flex.FLOW_HORIZONTAL,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0dp",
                "width": "100%"
            }, {}, {});
            flxNoValueChecked.setDefaultUnit(kony.flex.DP);
            var lblNoValueErrorIcon = new kony.ui.Label({
                "height": "15dp",
                "id": "lblNoValueErrorIcon",
                "isVisible": false,
                "left": "0dp",
                "right": "5dp",
                "skin": "sknErrorIcon",
                "text": "",
                "top": "8dp",
                "width": "15dp",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var lblSelectedOptions = new kony.ui.Label({
                "id": "lblSelectedOptions",
                "isVisible": true,
                "left": "0dp",
                "skin": "sknlblLatoRegularB2BDCB",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmPolicies.alLeastOneOptionMustBeSelected\")",
                "top": "10dp",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxNoValueChecked.add(lblNoValueErrorIcon, lblSelectedOptions);
            var flxcheckRules = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "100%",
                "id": "flxcheckRules",
                "isVisible": true,
                "layoutType": kony.flex.FLOW_HORIZONTAL,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0dp",
                "width": "100%"
            }, {}, {});
            flxcheckRules.setDefaultUnit(kony.flex.DP);
            var flxUppercaseChars = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "30px",
                "id": "flxUppercaseChars",
                "isVisible": true,
                "layoutType": kony.flex.FLOW_HORIZONTAL,
                "left": "0px",
                "isModalContainer": false,
                "skin": "sknCursor",
                "top": "3dp",
                "width": "120dp",
                "zIndex": 2
            }, {}, {
                "hoverSkin": "sknCursor"
            });
            flxUppercaseChars.setDefaultUnit(kony.flex.DP);
            var imgUppercaseCharacters = new kony.ui.Image2({
                "centerY": "50%",
                "height": "12px",
                "id": "imgUppercaseCharacters",
                "isVisible": true,
                "skin": "slImage",
                "src": "checkbox.png",
                "width": "12px",
                "zIndex": 2
            }, {
                "imageScaleMode": constants.IMAGE_SCALE_MODE_MAINTAIN_ASPECT_RATIO,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var lblUppercaseCharacters = new kony.ui.Label({
                "centerY": "50%",
                "id": "lblUppercaseCharacters",
                "isVisible": true,
                "left": "5dp",
                "skin": "sknLbl485C75LatoRegular13Px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmPolicies.uppercaseLetters\")",
                "top": "0dp",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 2
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxUppercaseChars.add(imgUppercaseCharacters, lblUppercaseCharacters);
            var flxLowercaseChars = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "30px",
                "id": "flxLowercaseChars",
                "isVisible": true,
                "layoutType": kony.flex.FLOW_HORIZONTAL,
                "left": "30px",
                "isModalContainer": false,
                "skin": "sknCursor",
                "top": "3dp",
                "width": "120dp",
                "zIndex": 2
            }, {}, {
                "hoverSkin": "sknCursor"
            });
            flxLowercaseChars.setDefaultUnit(kony.flex.DP);
            var imgLowercaseCharacters = new kony.ui.Image2({
                "centerY": "50%",
                "height": "12px",
                "id": "imgLowercaseCharacters",
                "isVisible": true,
                "left": "0",
                "skin": "slImage",
                "src": "checkbox.png",
                "width": "12px",
                "zIndex": 2
            }, {
                "imageScaleMode": constants.IMAGE_SCALE_MODE_MAINTAIN_ASPECT_RATIO,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var lblLowercaseCharacters = new kony.ui.Label({
                "centerY": "50%",
                "id": "lblLowercaseCharacters",
                "isVisible": true,
                "left": "5dp",
                "skin": "sknLbl485C75LatoRegular13Px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmPolicies.lowercaseLetters\")",
                "top": "0dp",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 2
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxLowercaseChars.add(imgLowercaseCharacters, lblLowercaseCharacters);
            var flxNumbers = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "30px",
                "id": "flxNumbers",
                "isVisible": true,
                "layoutType": kony.flex.FLOW_HORIZONTAL,
                "left": "30px",
                "isModalContainer": false,
                "skin": "sknCursor",
                "top": "3dp",
                "width": "70dp",
                "zIndex": 2
            }, {}, {
                "hoverSkin": "sknCursor"
            });
            flxNumbers.setDefaultUnit(kony.flex.DP);
            var imgNumbers = new kony.ui.Image2({
                "centerY": "50%",
                "height": "12px",
                "id": "imgNumbers",
                "isVisible": true,
                "left": "0",
                "skin": "slImage",
                "src": "checkbox.png",
                "width": "12px",
                "zIndex": 2
            }, {
                "imageScaleMode": constants.IMAGE_SCALE_MODE_MAINTAIN_ASPECT_RATIO,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var lblNumbers = new kony.ui.Label({
                "centerY": "50%",
                "id": "lblNumbers",
                "isVisible": true,
                "left": "5dp",
                "skin": "sknLbl485C75LatoRegular13Px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmPolicies.Numbers\")",
                "top": "0dp",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 2
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxNumbers.add(imgNumbers, lblNumbers);
            var flxCheckSpecialChars = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "30px",
                "id": "flxCheckSpecialChars",
                "isVisible": true,
                "layoutType": kony.flex.FLOW_HORIZONTAL,
                "left": "30px",
                "isModalContainer": false,
                "skin": "sknCursor",
                "top": "3dp",
                "width": "200dp",
                "zIndex": 2
            }, {}, {
                "hoverSkin": "sknCursor"
            });
            flxCheckSpecialChars.setDefaultUnit(kony.flex.DP);
            var imgSpecialChars = new kony.ui.Image2({
                "centerY": "50%",
                "height": "12px",
                "id": "imgSpecialChars",
                "isVisible": true,
                "left": "0",
                "skin": "slImage",
                "src": "checkbox.png",
                "width": "12px",
                "zIndex": 2
            }, {
                "imageScaleMode": constants.IMAGE_SCALE_MODE_MAINTAIN_ASPECT_RATIO,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var lblsplchars = new kony.ui.Label({
                "centerY": "50%",
                "id": "lblsplchars",
                "isVisible": true,
                "left": "5dp",
                "skin": "sknLbl485C75LatoRegular13Px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmPolicies.specialCharactersSmall\")",
                "top": "0dp",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 2
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxCheckSpecialChars.add(imgSpecialChars, lblsplchars);
            flxcheckRules.add(flxUppercaseChars, flxLowercaseChars, flxNumbers, flxCheckSpecialChars);
            flxPasswordMustContain.add(lblPasswordMustContain, flxNoValueChecked, flxcheckRules);
            var flxCharacterRepetition = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "155dp",
                "id": "flxCharacterRepetition",
                "isVisible": true,
                "layoutType": kony.flex.FLOW_VERTICAL,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "20dp",
                "width": "100%",
                "zIndex": 1
            }, {}, {});
            flxCharacterRepetition.setDefaultUnit(kony.flex.DP);
            var lblCharacterRepetition = new kony.ui.Label({
                "id": "lblCharacterRepetition",
                "isVisible": true,
                "left": "0dp",
                "skin": "sknLbl192b45LatoBold12px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmPolicies.characterRepetition\")",
                "top": "0dp",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var flxegrepetition = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "30dp",
                "id": "flxegrepetition",
                "isVisible": true,
                "layoutType": kony.flex.FLOW_HORIZONTAL,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "7dp",
                "width": "100%"
            }, {}, {});
            flxegrepetition.setDefaultUnit(kony.flex.DP);
            var lblMaxConsecutiveRepetition = new kony.ui.Label({
                "id": "lblMaxConsecutiveRepetition",
                "isVisible": true,
                "left": "0dp",
                "skin": "sknLblLato485c7513px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmPolicies.Maxnumberofconsecutiverepetitionsmall\")",
                "top": "7dp",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var lblMaxConsecutiveRepetitionExample = new kony.ui.Label({
                "id": "lblMaxConsecutiveRepetitionExample",
                "isVisible": true,
                "left": "13dp",
                "skin": "sknlblLatoRegularB2BDCB",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmPolicies.egaaaaor2222\")",
                "top": "7dp",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxegrepetition.add(lblMaxConsecutiveRepetition, lblMaxConsecutiveRepetitionExample);
            var flxNoRestriction = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "20dp",
                "id": "flxNoRestriction",
                "isVisible": true,
                "layoutType": kony.flex.FLOW_HORIZONTAL,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "6dp",
                "width": "100%"
            }, {}, {});
            flxNoRestriction.setDefaultUnit(kony.flex.DP);
            var imgNoRepetition = new kony.ui.Image2({
                "centerY": "50%",
                "height": "12px",
                "id": "imgNoRepetition",
                "isVisible": true,
                "left": "0",
                "skin": "slImage",
                "src": "checkbox.png",
                "width": "12px",
                "zIndex": 2
            }, {
                "imageScaleMode": constants.IMAGE_SCALE_MODE_MAINTAIN_ASPECT_RATIO,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var lblNoRestriction = new kony.ui.Label({
                "centerY": "50%",
                "id": "lblNoRestriction",
                "isVisible": true,
                "left": "7dp",
                "skin": "sknLblLato485c7513px",
                "text": "No restriction",
                "top": "0dp",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxNoRestriction.add(imgNoRepetition, lblNoRestriction);
            var maxConsecutiveChars = new com.customer360.common.CharactersplusMinus({
                "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                "id": "maxConsecutiveChars",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "masterType": constants.MASTER_TYPE_DEFAULT,
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "15dp",
                "width": "181dp",
                "overrides": {
                    "CharactersplusMinus": {
                        "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                        "bottom": "viz.val_cleared",
                        "isVisible": true,
                        "top": "15dp",
                        "width": "181dp"
                    },
                    "flxPlus": {
                        "bottom": "2dp",
                        "height": "36dp",
                        "left": "2dp",
                        "top": "2dp",
                        "width": "51dp"
                    },
                    "tbxcharcterSize": {
                        "bottom": "viz.val_cleared",
                        "height": "40dp",
                        "text": "4",
                        "width": "181dp"
                    }
                }
            }, {
                "overrides": {}
            }, {
                "overrides": {}
            });
            var flxErrorConsecutive = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "bottom": "80px",
                "clipBounds": true,
                "height": "20px",
                "id": "flxErrorConsecutive",
                "isVisible": false,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0px",
                "isModalContainer": false,
                "right": "0px",
                "skin": "slFbox",
                "top": "5px",
                "width": "100%",
                "zIndex": 1
            }, {}, {});
            flxErrorConsecutive.setDefaultUnit(kony.flex.DP);
            var lblError = new kony.ui.Label({
                "centerY": "50%",
                "height": "15px",
                "id": "lblError",
                "isVisible": true,
                "left": 0,
                "skin": "sknFontIconError",
                "text": "",
                "width": "15px",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var lblImgError = new kony.ui.Label({
                "centerY": "50%",
                "id": "lblImgError",
                "isVisible": true,
                "left": "15px",
                "skin": "sknLabelRed",
                "text": "Range is between 0 and 9",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxErrorConsecutive.add(lblError, lblImgError);
            flxCharacterRepetition.add(lblCharacterRepetition, flxegrepetition, flxNoRestriction, maxConsecutiveChars, flxErrorConsecutive);
            flxPasswordRules.add(flxPasswordMustContain, flxCharacterRepetition);
            flxPasswordAdditionalRules.add(flxPasswordRules);
            flxPoliciesDataWrapper.add(lblPoliciesRuleName, lblSeperator, lblLengthOfPassword, flxLengthOfPasswordValues, flxPasswordAdditionalRules);
            var lblButtonSeperator = new kony.ui.Label({
                "height": "1dp",
                "id": "lblButtonSeperator",
                "isVisible": true,
                "left": "0dp",
                "right": "0px",
                "skin": "HeaderSeparator",
                "top": "375px",
                "width": "100%",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var commonButtons = new com.adminConsole.common.commonButtons({
                "height": "80dp",
                "id": "commonButtons",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "masterType": constants.MASTER_TYPE_DEFAULT,
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "376dp",
                "width": "100%",
                "overrides": {
                    "btnCancel": {
                        "left": "21px"
                    },
                    "btnNext": {
                        "isVisible": false
                    },
                    "btnSave": {
                        "i18n_text": "kony.i18n.getLocalizedString(\"i18n.ConfigurationBundles.buttonUpdate\")"
                    },
                    "commonButtons": {
                        "bottom": "viz.val_cleared",
                        "height": "80dp",
                        "left": "0dp",
                        "maxHeight": "viz.val_cleared",
                        "top": "376dp",
                        "width": "100%"
                    },
                    "flxRightButtons": {
                        "right": "21px"
                    }
                }
            }, {
                "overrides": {}
            }, {
                "overrides": {}
            });
            flxPoliciesWrapper.add(flxPoliciesDataWrapper, lblButtonSeperator, commonButtons);
            flxPoliciesEdit.add(flxPoliciesWrapper);
            flxScrollMainContent.add(flxPolicies, flxPoliciesParentRulesView, flxPoliciesEdit);
            var flxBreadcrumb = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "20px",
                "id": "flxBreadcrumb",
                "isVisible": false,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "104px",
                "width": "100%",
                "zIndex": 3
            }, {}, {});
            flxBreadcrumb.setDefaultUnit(kony.flex.DP);
            var breadcrumbs = new com.adminConsole.common.breadcrumbs({
                "height": "20px",
                "id": "breadcrumbs",
                "isVisible": true,
                "layoutType": kony.flex.FLOW_HORIZONTAL,
                "left": "0dp",
                "masterType": constants.MASTER_TYPE_DEFAULT,
                "isModalContainer": false,
                "skin": "sknflxf5f6f8Op100",
                "top": "0dp",
                "width": "100%",
                "overrides": {
                    "btnBackToMain": {
                        "i18n_text": "kony.i18n.getLocalizedString(\"i18n.leftmenu.CredentialManagementCaps\")",
                        "left": "20px"
                    },
                    "btnPreviousPage": {
                        "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmPolicies.policiesandstatements\")",
                        "isVisible": false
                    },
                    "fontIconBreadcrumbsRight": {
                        "isVisible": false,
                        "left": "12dp"
                    },
                    "fontIconBreadcrumbsRight2": {
                        "isVisible": true
                    },
                    "imgBreadcrumbsDown": {
                        "src": "img_down_arrow.png"
                    },
                    "imgBreadcrumbsRight": {
                        "src": "img_breadcrumb_arrow.png"
                    },
                    "imgBreadcrumbsRight2": {
                        "src": "img_breadcrumb_arrow.png"
                    },
                    "imgBreadcrumbsRight3": {
                        "src": "img_breadcrumb_arrow.png"
                    },
                    "lblCurrentScreen": {
                        "text": "CONFIGURATION"
                    }
                }
            }, {
                "overrides": {}
            }, {
                "overrides": {}
            });
            flxBreadcrumb.add(breadcrumbs);
            var flxLoading = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "100%",
                "id": "flxLoading",
                "isVisible": false,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "sknLoadingBlur",
                "top": "0dp",
                "width": "100%",
                "zIndex": 10
            }, {}, {});
            flxLoading.setDefaultUnit(kony.flex.DP);
            var flxImageContainer = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "centerX": "50%",
                "centerY": "50%",
                "clipBounds": true,
                "height": "75px",
                "id": "flxImageContainer",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "isModalContainer": false,
                "skin": "slFbox",
                "width": "75px",
                "zIndex": 1
            }, {}, {});
            flxImageContainer.setDefaultUnit(kony.flex.DP);
            var imgLoading = new kony.ui.Image2({
                "centerX": "50%",
                "centerY": "50%",
                "height": "50px",
                "id": "imgLoading",
                "isVisible": true,
                "skin": "slImage",
                "src": "loadingscreenimage.gif",
                "width": "50px",
                "zIndex": 1
            }, {
                "imageScaleMode": constants.IMAGE_SCALE_MODE_MAINTAIN_ASPECT_RATIO,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxImageContainer.add(imgLoading);
            flxLoading.add(flxImageContainer);
            flxRightPanel.add(flxMainHeader, flxHeaderDropdown, flxMainSubHeader, flxScrollMainContent, flxBreadcrumb, flxLoading);
            flxMain.add(flxLeftPanel, flxRightPanel);
            var flxToastMessage = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "bottom": "0%",
                "clipBounds": true,
                "height": "8%",
                "id": "flxToastMessage",
                "isVisible": false,
                "layoutType": kony.flex.FREE_FORM,
                "left": "305px",
                "isModalContainer": false,
                "skin": "slFbox",
                "width": "78%",
                "zIndex": 1
            }, {}, {});
            flxToastMessage.setDefaultUnit(kony.flex.DP);
            var flxToastContainer = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "centerX": "50%",
                "centerY": "50%",
                "clipBounds": true,
                "height": "70%",
                "id": "flxToastContainer",
                "isVisible": false,
                "layoutType": kony.flex.FLOW_HORIZONTAL,
                "left": "0%",
                "isModalContainer": false,
                "skin": "sknflxSuccessToast1F844D",
                "top": "0%",
                "width": "60%",
                "zIndex": 1
            }, {}, {});
            flxToastContainer.setDefaultUnit(kony.flex.DP);
            var imgLeft = new kony.ui.Image2({
                "centerY": "50%",
                "height": "10dp",
                "id": "imgLeft",
                "isVisible": true,
                "left": "2%",
                "skin": "slImage",
                "src": "arrow2x.png",
                "top": "0dp",
                "width": "15dp",
                "zIndex": 1
            }, {
                "imageScaleMode": constants.IMAGE_SCALE_MODE_MAINTAIN_ASPECT_RATIO,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var lbltoastMessage = new kony.ui.Label({
                "centerY": "50%",
                "id": "lbltoastMessage",
                "isVisible": true,
                "left": "3%",
                "skin": "lblfffffflatoregular14px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.permission.SuccessfullyDeactivated\")",
                "top": "18%",
                "width": "250px",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var imgRight = new kony.ui.Image2({
                "centerY": "50%",
                "height": "10dp",
                "id": "imgRight",
                "isVisible": true,
                "left": "50%",
                "skin": "slImage",
                "src": "close_small2x.png",
                "top": "0dp",
                "width": "10dp",
                "zIndex": 1
            }, {
                "imageScaleMode": constants.IMAGE_SCALE_MODE_MAINTAIN_ASPECT_RATIO,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxToastContainer.add(imgLeft, lbltoastMessage, imgRight);
            var toastMessage = new com.adminConsole.common.toastMessage({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "id": "toastMessage",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "masterType": constants.MASTER_TYPE_DEFAULT,
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0dp",
                "overrides": {}
            }, {
                "overrides": {}
            }, {
                "overrides": {}
            });
            flxToastMessage.add(flxToastContainer, toastMessage);
            this.add(flxMain, flxToastMessage);
        };
        return [{
            "addWidgets": addWidgetsfrmPolicies,
            "enabledForIdleTimeout": true,
            "id": "frmPolicies",
            "layoutType": kony.flex.FREE_FORM,
            "needAppMenu": false,
            "preShow": function(eventobject) {
                controller.AS_Form_ecab3926c81549e2a81f51c89c0e6017(eventobject);
            },
            "skin": "slForm"
        }, {
            "displayOrientation": constants.FORM_DISPLAY_ORIENTATION_PORTRAIT,
            "layoutType": kony.flex.FREE_FORM,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, {
            "onDeviceBack": controller.AS_Form_b9037ebbdeb34d98b93eacef4438dc4a,
            "retainScrollPosition": false
        }]
    }
});