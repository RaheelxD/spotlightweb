define("flxSelectReport", function() {
    return function(controller) {
        var flxSelectReport = new kony.ui.FlexContainer({
            "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
            "clipBounds": true,
            "id": "flxSelectReport",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "left": "0dp",
            "isModalContainer": false,
            "skin": "slFbox",
            "top": "0dp",
            "width": "100%"
        }, {}, {});
        flxSelectReport.setDefaultUnit(kony.flex.DP);
        var flxContent = new kony.ui.FlexContainer({
            "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
            "bottom": "0dp",
            "clipBounds": false,
            "id": "flxContent",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "left": "20dp",
            "isModalContainer": false,
            "right": "20dp",
            "skin": "slFbox",
            "top": "0"
        }, {}, {
            "hoverSkin": "sknCursor"
        });
        flxContent.setDefaultUnit(kony.flex.DP);
        var flxRow = new kony.ui.FlexContainer({
            "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
            "bottom": "15dp",
            "clipBounds": false,
            "id": "flxRow",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "left": "10dp",
            "isModalContainer": false,
            "right": "0dp",
            "skin": "slFbox",
            "top": "0dp"
        }, {}, {});
        flxRow.setDefaultUnit(kony.flex.DP);
        var flxReportName = new kony.ui.FlexContainer({
            "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
            "clipBounds": false,
            "id": "flxReportName",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "left": "0dp",
            "minHeight": "35dp",
            "isModalContainer": false,
            "skin": "slFbox",
            "top": "0dp",
            "width": "49%"
        }, {}, {});
        flxReportName.setDefaultUnit(kony.flex.DP);
        var fontIconSelect = new kony.ui.Label({
            "centerX": "50%",
            "height": "24dp",
            "id": "fontIconSelect",
            "isVisible": true,
            "left": "-3dp",
            "skin": "sknfontIcon004F93size24px",
            "text": "",
            "top": "11dp",
            "width": kony.flex.USE_PREFFERED_SIZE
        }, {
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, {});
        var lblReportName = new kony.ui.Label({
            "id": "lblReportName",
            "isVisible": true,
            "left": "30px",
            "right": "20dp",
            "skin": "sknlblLatoReg485c7513px",
            "text": "Platinum Customers",
            "top": "15px",
            "zIndex": 1
        }, {
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, {});
        flxReportName.add(fontIconSelect, lblReportName);
        var lblDescription = new kony.ui.Label({
            "id": "lblDescription",
            "isVisible": true,
            "left": "50%",
            "skin": "sknlblLatoReg485c7513px",
            "text": "2019 Credit Report",
            "top": "15px",
            "width": "49%",
            "zIndex": 1
        }, {
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, {});
        var lblFiltersAvailable = new kony.ui.Label({
            "height": "15px",
            "id": "lblFiltersAvailable",
            "isVisible": false,
            "left": "60%",
            "skin": "sknlblLatoReg485c7513px",
            "text": "Yes",
            "top": "15px",
            "width": "29%",
            "zIndex": 1
        }, {
            "contentAlignment": constants.CONTENT_ALIGN_TOP_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, {});
        flxRow.add(flxReportName, lblDescription, lblFiltersAvailable);
        flxContent.add(flxRow);
        var lblSeparator = new kony.ui.Label({
            "bottom": "0dp",
            "height": "1px",
            "id": "lblSeparator",
            "isVisible": true,
            "left": "20dp",
            "right": "20dp",
            "skin": "sknlblSeperator",
            "text": "Label",
            "zIndex": 2
        }, {
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, {});
        flxSelectReport.add(flxContent, lblSeparator);
        return flxSelectReport;
    }
})