define("flxCustomerCentricDetails", function() {
    return function(controller) {
        var flxCustomerCentricDetails = new kony.ui.FlexContainer({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "clipBounds": true,
            "height": "50px",
            "id": "flxCustomerCentricDetails",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "isModalContainer": false,
            "onClick": controller.AS_FlexContainer_e553ee3b13fe4973aef70f31c7776c5d,
            "skin": "sknCursor"
        }, {}, {
            "hoverSkin": "sknfbfcfc"
        });
        flxCustomerCentricDetails.setDefaultUnit(kony.flex.DP);
        var flxContent = new kony.ui.FlexContainer({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "clipBounds": true,
            "height": "100%",
            "id": "flxContent",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "left": "10dp",
            "isModalContainer": false,
            "right": "20dp",
            "skin": "slFbox",
            "top": "0dp"
        }, {}, {});
        flxContent.setDefaultUnit(kony.flex.DP);
        var flxRadio = new kony.ui.FlexContainer({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "centerY": "50%",
            "clipBounds": true,
            "height": "20dp",
            "id": "flxRadio",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "left": "0dp",
            "isModalContainer": false,
            "skin": "slFbox",
            "width": "20dp",
            "zIndex": 1
        }, {}, {
            "hoverSkin": "sknFlxPointer"
        });
        flxRadio.setDefaultUnit(kony.flex.DP);
        var imgRadio = new kony.ui.Image2({
            "centerX": "50%",
            "centerY": "50%",
            "height": "15dp",
            "id": "imgRadio",
            "isVisible": true,
            "skin": "slImage",
            "src": "radio_notselected.png",
            "width": "15dp",
            "zIndex": 1
        }, {
            "imageScaleMode": constants.IMAGE_SCALE_MODE_MAINTAIN_ASPECT_RATIO,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, {});
        flxRadio.add(imgRadio);
        var flxDetails = new kony.ui.FlexContainer({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "clipBounds": true,
            "height": "50px",
            "id": "flxDetails",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "left": "30px",
            "isModalContainer": false,
            "onClick": controller.AS_FlexContainer_e553ee3b13fe4973aef70f31c7776c5d,
            "right": "30px",
            "skin": "slFbox",
            "top": "0"
        }, {}, {
            "hoverSkin": "sknfbfcfc"
        });
        flxDetails.setDefaultUnit(kony.flex.DP);
        var lblName = new kony.ui.Label({
            "centerY": "50%",
            "id": "lblName",
            "isVisible": true,
            "left": "0dp",
            "skin": "sknllbl485c75Lato13px",
            "text": "John",
            "width": "20%",
            "zIndex": 1
        }, {
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, {});
        var lblSSN = new kony.ui.Label({
            "centerY": "50%",
            "id": "lblSSN",
            "isVisible": true,
            "left": "21%",
            "skin": "sknllbl485c75Lato13px",
            "text": "123-223-990",
            "width": "15%",
            "zIndex": 1
        }, {
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, {});
        var lblDob = new kony.ui.Label({
            "centerY": "50%",
            "id": "lblDob",
            "isVisible": true,
            "left": "37%",
            "skin": "sknllbl485c75Lato13px",
            "text": "11-03-1991",
            "width": "15%",
            "zIndex": 1
        }, {
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, {});
        var lblContactNumber = new kony.ui.Label({
            "centerY": "50%",
            "id": "lblContactNumber",
            "isVisible": true,
            "left": "53%",
            "skin": "sknllbl485c75Lato13px",
            "text": "+1 555-666-8791",
            "width": "25%",
            "zIndex": 1
        }, {
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, {});
        var lblEmail = new kony.ui.Label({
            "centerY": "50%",
            "id": "lblEmail",
            "isVisible": true,
            "left": "79%",
            "skin": "sknllbl485c75Lato13px",
            "text": "james.ron@gmail.com",
            "width": "23%",
            "zIndex": 1
        }, {
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, {});
        flxDetails.add(lblName, lblSSN, lblDob, lblContactNumber, lblEmail);
        var flxUnlink = new kony.ui.FlexContainer({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "clipBounds": true,
            "height": "100%",
            "id": "flxUnlink",
            "isVisible": false,
            "layoutType": kony.flex.FREE_FORM,
            "isModalContainer": false,
            "right": "10dp",
            "skin": "slFbox",
            "top": "0dp",
            "width": "20dp",
            "zIndex": 1
        }, {}, {});
        flxUnlink.setDefaultUnit(kony.flex.DP);
        var flblUnlink = new kony.ui.Label({
            "centerY": "50%",
            "height": "20dp",
            "id": "flblUnlink",
            "isVisible": true,
            "right": "0dp",
            "skin": "sknIcomoon20px",
            "text": "",
            "top": "20dp",
            "width": "20dp",
            "zIndex": 1
        }, {
            "contentAlignment": constants.CONTENT_ALIGN_CENTER,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, {});
        flxUnlink.add(flblUnlink);
        flxContent.add(flxRadio, flxDetails, flxUnlink);
        var lblSeperator = new kony.ui.Label({
            "height": "1px",
            "id": "lblSeperator",
            "isVisible": true,
            "left": "0px",
            "right": "20dp",
            "skin": "sknLblD5D9DD1000",
            "text": ".",
            "top": "49px",
            "zIndex": 1
        }, {
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, {});
        flxCustomerCentricDetails.add(flxContent, lblSeperator);
        return flxCustomerCentricDetails;
    }
})