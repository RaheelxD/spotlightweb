define("flxCustomerInformation", function() {
    return function(controller) {
        var flxCustomerInformation = new kony.ui.FlexContainer({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "clipBounds": true,
            "height": "70dp",
            "id": "flxCustomerInformation",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "isModalContainer": false,
            "skin": "slFbox"
        }, {}, {});
        flxCustomerInformation.setDefaultUnit(kony.flex.DP);
        var flxContactNumberWrapper = new kony.ui.FlexContainer({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "centerY": "50%",
            "clipBounds": true,
            "height": "40dp",
            "id": "flxContactNumberWrapper",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "left": "40dp",
            "isModalContainer": false,
            "skin": "slFbox",
            "top": "0dp",
            "width": "300dp"
        }, {}, {});
        flxContactNumberWrapper.setDefaultUnit(kony.flex.DP);
        var txtISDCode = new kony.ui.TextBox2({
            "autoCapitalize": constants.TEXTBOX_AUTO_CAPITALIZE_NONE,
            "focusSkin": "defTextBoxFocus",
            "height": "40dp",
            "id": "txtISDCode",
            "isVisible": true,
            "keyBoardStyle": constants.TEXTBOX_KEY_BOARD_STYLE_DEFAULT,
            "left": "0dp",
            "placeholder": "ISD Code",
            "secureTextEntry": false,
            "skin": "skntxtbxDetails0bbf1235271384a",
            "textInputMode": constants.TEXTBOX_INPUT_MODE_ANY,
            "top": "0dp",
            "width": "70dp",
            "zIndex": 1
        }, {
            "containerHeightMode": constants.TEXTBOX_FONT_METRICS_DRIVEN_HEIGHT,
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [3, 0, 0, 0],
            "paddingInPixel": false
        }, {
            "autoCorrect": false,
            "placeholderSkin": "defTextBoxPlaceholder"
        });
        var txtbxPhoneNumber = new kony.ui.TextBox2({
            "autoCapitalize": constants.TEXTBOX_AUTO_CAPITALIZE_NONE,
            "focusSkin": "defTextBoxFocus",
            "height": "40dp",
            "id": "txtbxPhoneNumber",
            "isVisible": true,
            "keyBoardStyle": constants.TEXTBOX_KEY_BOARD_STYLE_DEFAULT,
            "left": "100dp",
            "placeholder": "Contact Number",
            "secureTextEntry": false,
            "skin": "skntxtbxDetails0bbf1235271384a",
            "textInputMode": constants.TEXTBOX_INPUT_MODE_ANY,
            "top": "0dp",
            "width": "200dp",
            "zIndex": 1
        }, {
            "containerHeightMode": constants.TEXTBOX_FONT_METRICS_DRIVEN_HEIGHT,
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [3, 0, 0, 0],
            "paddingInPixel": false
        }, {
            "autoCorrect": false,
            "placeholderSkin": "defTextBoxPlaceholder"
        });
        var lblDash = new kony.ui.Label({
            "height": "40dp",
            "id": "lblDash",
            "isVisible": true,
            "left": "80dp",
            "skin": "slLabel",
            "text": "-",
            "top": "0dp",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, {
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, {});
        var flxError = new kony.ui.FlexContainer({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "clipBounds": true,
            "height": "15dp",
            "id": "flxError",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "left": "0dp",
            "isModalContainer": false,
            "skin": "slFbox",
            "top": "45dp",
            "width": "100%",
            "zIndex": 1
        }, {}, {});
        flxError.setDefaultUnit(kony.flex.DP);
        var lblErrorIcon = new kony.ui.Label({
            "height": "15dp",
            "id": "lblErrorIcon",
            "isVisible": true,
            "left": "0dp",
            "skin": "sknErrorIcon",
            "text": "",
            "top": "0dp",
            "width": "15dp",
            "zIndex": 1
        }, {
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, {});
        var lblErrorText = new kony.ui.Label({
            "height": "15dp",
            "id": "lblErrorText",
            "isVisible": true,
            "left": "15dp",
            "skin": "sknlblError",
            "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmLocations.lblNoPhnNumberError\")",
            "top": "0dp",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, {
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, {});
        flxError.add(lblErrorIcon, lblErrorText);
        flxContactNumberWrapper.add(txtISDCode, txtbxPhoneNumber, lblDash, flxError);
        var lblNoPhoneNumberError = new kony.ui.Label({
            "id": "lblNoPhoneNumberError",
            "isVisible": false,
            "left": "40dp",
            "skin": "sknlblError",
            "text": "Please enter a valid phone number",
            "top": "55dp",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, {
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, {});
        var lblActive = new kony.ui.Label({
            "centerY": "50%",
            "id": "lblActive",
            "isVisible": true,
            "left": "38.15%",
            "skin": "sknlblLatoRegular484b5213px",
            "text": "Active",
            "top": "13dp",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, {
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, {});
        var SwitchActive = new kony.ui.Switch({
            "centerY": "50%",
            "height": "25dp",
            "id": "SwitchActive",
            "isVisible": true,
            "left": "42.76%",
            "leftSideText": "ON",
            "rightSideText": "OFF",
            "selectedIndex": 0,
            "skin": "sknSwitchServiceManagement",
            "width": "38dp",
            "zIndex": 1
        }, {
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, {});
        var txtbxEmailid = new kony.ui.TextBox2({
            "autoCapitalize": constants.TEXTBOX_AUTO_CAPITALIZE_NONE,
            "centerY": "50%",
            "focusSkin": "skntxtbxDetailsFocus14px",
            "height": "40dp",
            "id": "txtbxEmailid",
            "isVisible": true,
            "keyBoardStyle": constants.TEXTBOX_KEY_BOARD_STYLE_DEFAULT,
            "left": "49.56%",
            "maxTextLength": 70,
            "placeholder": "Email ID",
            "secureTextEntry": false,
            "skin": "skntxtbxDetails0bbf1235271384a",
            "textInputMode": constants.TEXTBOX_INPUT_MODE_ANY,
            "width": "300dp",
            "zIndex": 1
        }, {
            "containerHeightMode": constants.TEXTBOX_FONT_METRICS_DRIVEN_HEIGHT,
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [1, 0, 0, 0],
            "paddingInPixel": false
        }, {
            "autoCorrect": false
        });
        var lblNoEmailIDError = new kony.ui.Label({
            "id": "lblNoEmailIDError",
            "isVisible": false,
            "left": "49.50%",
            "skin": "sknlblError",
            "text": "Please enter a valid Email ID",
            "top": "55dp",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, {
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, {});
        var lblActiveStatus = new kony.ui.Label({
            "centerY": "50%",
            "id": "lblActiveStatus",
            "isVisible": true,
            "left": "83.88%",
            "skin": "sknlblLatoRegular484b5213px",
            "text": "Active",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, {
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, {});
        var SwitchActiveStatus = new kony.ui.Switch({
            "centerY": "50%",
            "height": "25dp",
            "id": "SwitchActiveStatus",
            "isVisible": true,
            "left": "88.48%",
            "leftSideText": "ON",
            "rightSideText": "OFF",
            "selectedIndex": 0,
            "skin": "sknSwitchServiceManagement",
            "width": "38dp",
            "zIndex": 1
        }, {
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, {});
        var flxDelete = new kony.ui.FlexContainer({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "centerY": "50%",
            "clipBounds": true,
            "height": "40dp",
            "id": "flxDelete",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "left": "93.54%",
            "isModalContainer": false,
            "skin": "sknCursor",
            "width": "50px",
            "zIndex": 1
        }, {}, {});
        flxDelete.setDefaultUnit(kony.flex.DP);
        var imgDelete = new kony.ui.Image2({
            "centerX": "50%",
            "centerY": "50%",
            "height": "15dp",
            "id": "imgDelete",
            "isVisible": false,
            "skin": "slImage",
            "src": "delete_2x.png",
            "width": "15dp",
            "zIndex": 1
        }, {
            "imageScaleMode": constants.IMAGE_SCALE_MODE_MAINTAIN_ASPECT_RATIO,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, {});
        var fontIconDelete = new kony.ui.Label({
            "centerX": "50%",
            "centerY": "50%",
            "id": "fontIconDelete",
            "isVisible": true,
            "skin": "sknFontIconOptionMenuRow",
            "text": "",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, {
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, {});
        flxDelete.add(imgDelete, fontIconDelete);
        var lblSeparator = new kony.ui.Label({
            "bottom": "0dp",
            "height": "1px",
            "id": "lblSeparator",
            "isVisible": true,
            "left": "35dp",
            "right": "35dp",
            "skin": "sknlblSeperator",
            "text": ".",
            "zIndex": 1
        }, {
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, {});
        flxCustomerInformation.add(flxContactNumberWrapper, lblNoPhoneNumberError, lblActive, SwitchActive, txtbxEmailid, lblNoEmailIDError, lblActiveStatus, SwitchActiveStatus, flxDelete, lblSeparator);
        return flxCustomerInformation;
    }
})