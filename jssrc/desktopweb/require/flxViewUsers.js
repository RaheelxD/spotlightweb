define("flxViewUsers", function() {
    return function(controller) {
        var flxViewUsers = new kony.ui.FlexContainer({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "clipBounds": true,
            "height": "50px",
            "id": "flxViewUsers",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "isModalContainer": false,
            "skin": "sknflxffffffop100"
        }, {}, {
            "hoverSkin": "sknfbfcfc"
        });
        flxViewUsers.setDefaultUnit(kony.flex.DP);
        var lblViewFullName = new kony.ui.Label({
            "centerY": "50%",
            "id": "lblViewFullName",
            "isVisible": true,
            "left": "10px",
            "skin": "sknlblLatoBold35475f14px",
            "text": "John Doe",
            "width": "14%",
            "zIndex": 1
        }, {
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, {});
        var lblViewUsername = new kony.ui.Label({
            "centerY": "50%",
            "id": "lblViewUsername",
            "isVisible": true,
            "left": "18.93%",
            "skin": "sknlblLatoBold35475f14px",
            "text": "john.doe",
            "width": "17%",
            "zIndex": 1
        }, {
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, {});
        var lblViewEmailId = new kony.ui.Label({
            "centerY": "50%",
            "id": "lblViewEmailId",
            "isVisible": true,
            "left": "36.69%",
            "skin": "sknlblLatoBold35475f14px",
            "text": "john.doe@kony.com",
            "width": "22%",
            "zIndex": 1
        }, {
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, {});
        var lblViewUpdatedBy = new kony.ui.Label({
            "centerY": "50%",
            "id": "lblViewUpdatedBy",
            "isVisible": true,
            "left": "60.35%",
            "skin": "sknlblLatoBold35475f14px",
            "text": "Chester Dean",
            "width": "16%",
            "zIndex": 1
        }, {
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, {});
        var lblViewUpdatedDate = new kony.ui.Label({
            "centerY": "50%",
            "id": "lblViewUpdatedDate",
            "isVisible": true,
            "left": "77.28%",
            "right": "35dp",
            "skin": "sknlblLatoBold35475f14px",
            "text": "17/08/2017",
            "zIndex": 1
        }, {
            "contentAlignment": constants.CONTENT_ALIGN_TOP_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, {});
        var lblViewUpdatedTime = new kony.ui.Label({
            "centerY": "50%",
            "id": "lblViewUpdatedTime",
            "isVisible": true,
            "left": "86%",
            "skin": "sknlblLatoBold35475f14px",
            "text": "10:30 AM PST",
            "width": "12%",
            "zIndex": 1
        }, {
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, {});
        var lblViewSeperator = new kony.ui.Label({
            "height": "1px",
            "id": "lblViewSeperator",
            "isVisible": true,
            "left": "0dp",
            "skin": "sknlblSeperator",
            "text": ".",
            "top": "0dp",
            "width": "100%",
            "zIndex": 1
        }, {
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, {});
        flxViewUsers.add(lblViewFullName, lblViewUsername, lblViewEmailId, lblViewUpdatedBy, lblViewUpdatedDate, lblViewUpdatedTime, lblViewSeperator);
        return flxViewUsers;
    }
})