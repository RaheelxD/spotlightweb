define("flxContractsAccountHeaderView", function() {
    return function(controller) {
        var flxContractsAccountHeaderView = new kony.ui.FlexContainer({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "clipBounds": true,
            "height": "50dp",
            "id": "flxContractsAccountHeaderView",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "left": "0dp",
            "isModalContainer": false,
            "skin": "sknBackgroundFFFFFF",
            "top": "0dp",
            "width": "100%"
        }, {}, {});
        flxContractsAccountHeaderView.setDefaultUnit(kony.flex.DP);
        var flxHeader = new kony.ui.FlexContainer({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "clipBounds": true,
            "height": "50px",
            "id": "flxHeader",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "left": "20dp",
            "isModalContainer": false,
            "right": "20dp",
            "skin": "slFbox",
            "top": "0dp"
        }, {}, {});
        flxHeader.setDefaultUnit(kony.flex.DP);
        var flxAccountNumberHeader = new kony.ui.FlexContainer({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "centerY": "50%",
            "clipBounds": true,
            "height": "100%",
            "id": "flxAccountNumberHeader",
            "isVisible": true,
            "layoutType": kony.flex.FLOW_HORIZONTAL,
            "left": "0px",
            "isModalContainer": false,
            "onClick": controller.AS_FlexContainer_a5084e29a733477596892b84c89d16f9,
            "skin": "slFbox",
            "top": 0,
            "width": "21%",
            "zIndex": 1
        }, {}, {});
        flxAccountNumberHeader.setDefaultUnit(kony.flex.DP);
        var lblAccountNumberHeader = new kony.ui.Label({
            "centerY": "50%",
            "id": "lblAccountNumberHeader",
            "isVisible": true,
            "left": "0px",
            "skin": "sknlblLato696c7311px",
            "text": "ACCOUNT NUMBER",
            "top": 0,
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, {
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, {});
        var fontIconAccNumberSort = new kony.ui.Label({
            "centerY": "50%",
            "id": "fontIconAccNumberSort",
            "isVisible": true,
            "left": "10dp",
            "skin": "sknIcon13px",
            "text": "",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, {
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, {});
        flxAccountNumberHeader.add(lblAccountNumberHeader, fontIconAccNumberSort);
        var flxAccountTypeHeader = new kony.ui.FlexContainer({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "centerY": "50%",
            "clipBounds": true,
            "height": "100%",
            "id": "flxAccountTypeHeader",
            "isVisible": true,
            "layoutType": kony.flex.FLOW_HORIZONTAL,
            "left": "21%",
            "isModalContainer": false,
            "skin": "slFbox",
            "top": 0,
            "width": "20%",
            "zIndex": 1
        }, {}, {});
        flxAccountTypeHeader.setDefaultUnit(kony.flex.DP);
        var lblAccountTypeHeader = new kony.ui.Label({
            "centerY": "50%",
            "id": "lblAccountTypeHeader",
            "isVisible": true,
            "left": "0px",
            "skin": "sknlblLato696c7311px",
            "text": "ACCOUNT TYPE",
            "top": 0,
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, {
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, {});
        var fontIconAccTypeFilter = new kony.ui.Label({
            "centerY": "50%",
            "id": "fontIconAccTypeFilter",
            "isVisible": true,
            "left": "5dp",
            "skin": "sknIcon13px",
            "text": "",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, {
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, {});
        flxAccountTypeHeader.add(lblAccountTypeHeader, fontIconAccTypeFilter);
        var flxAccountNameHeader = new kony.ui.FlexContainer({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "centerY": "50%",
            "clipBounds": true,
            "height": "100%",
            "id": "flxAccountNameHeader",
            "isVisible": true,
            "layoutType": kony.flex.FLOW_HORIZONTAL,
            "left": "41%",
            "isModalContainer": false,
            "onClick": controller.AS_FlexContainer_a5084e29a733477596892b84c89d16f9,
            "skin": "slFbox",
            "top": 0,
            "width": "22%",
            "zIndex": 1
        }, {}, {});
        flxAccountNameHeader.setDefaultUnit(kony.flex.DP);
        var lblAccountNameHeader = new kony.ui.Label({
            "centerY": "50%",
            "id": "lblAccountNameHeader",
            "isVisible": true,
            "left": "0%",
            "skin": "sknlblLato696c7311px",
            "text": "ACCOUNT NAME",
            "top": 0,
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, {
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, {});
        var fontIconAccNameSort = new kony.ui.Label({
            "centerY": "50%",
            "id": "fontIconAccNameSort",
            "isVisible": true,
            "left": "10dp",
            "skin": "sknIcon13px",
            "text": "",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, {
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, {});
        flxAccountNameHeader.add(lblAccountNameHeader, fontIconAccNameSort);
        var flxAccHolderNameHeader = new kony.ui.FlexContainer({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "centerY": "50%",
            "clipBounds": true,
            "height": "100%",
            "id": "flxAccHolderNameHeader",
            "isVisible": true,
            "layoutType": kony.flex.FLOW_HORIZONTAL,
            "left": "63%",
            "isModalContainer": false,
            "onClick": controller.AS_FlexContainer_fbe01558a0a64e0ca4625eeff7f71077,
            "skin": "slFbox",
            "top": 0,
            "width": "22%",
            "zIndex": 1
        }, {}, {});
        flxAccHolderNameHeader.setDefaultUnit(kony.flex.DP);
        var lblAccHolderNameHeader = new kony.ui.Label({
            "centerY": "50%",
            "id": "lblAccHolderNameHeader",
            "isVisible": true,
            "left": "0%",
            "skin": "sknlblLato696c7311px",
            "text": "ACCOUNT HOLDER NAME",
            "top": 0,
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, {
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, {});
        var fontIconAccHoldNameSort = new kony.ui.Label({
            "centerY": "50%",
            "id": "fontIconAccHoldNameSort",
            "isVisible": true,
            "left": "5dp",
            "skin": "sknIcon13px",
            "text": "",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, {
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, {});
        flxAccHolderNameHeader.add(lblAccHolderNameHeader, fontIconAccHoldNameSort);
        var flxHeaderAccountStatus = new kony.ui.FlexContainer({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "centerY": "50%",
            "clipBounds": true,
            "height": "100%",
            "id": "flxHeaderAccountStatus",
            "isVisible": true,
            "layoutType": kony.flex.FLOW_HORIZONTAL,
            "left": "85%",
            "isModalContainer": false,
            "onClick": controller.AS_FlexContainer_c62ddb6717304b7199d5a79964e58736,
            "skin": "sknFlxPointer",
            "top": "0dp",
            "width": "15%",
            "zIndex": 1
        }, {}, {});
        flxHeaderAccountStatus.setDefaultUnit(kony.flex.DP);
        var lblAccountStatusHeader = new kony.ui.Label({
            "centerY": "50%",
            "id": "lblAccountStatusHeader",
            "isVisible": true,
            "left": "0px",
            "skin": "sknlblLato696c7311px",
            "i18n_text": "kony.i18n.getLocalizedString(\"i18n.Group.SERVICE\")",
            "top": 0,
            "width": "55px",
            "zIndex": 1
        }, {
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, {});
        var fontIconFilterStatus = new kony.ui.Label({
            "centerY": "50%",
            "id": "fontIconFilterStatus",
            "isVisible": true,
            "left": "5dp",
            "skin": "sknIcon13px",
            "text": "",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, {
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, {});
        flxHeaderAccountStatus.add(lblAccountStatusHeader, fontIconFilterStatus);
        flxHeader.add(flxAccountNumberHeader, flxAccountTypeHeader, flxAccountNameHeader, flxAccHolderNameHeader, flxHeaderAccountStatus);
        var lblHeaderSeperator = new kony.ui.Label({
            "bottom": 0,
            "height": "1px",
            "id": "lblHeaderSeperator",
            "isVisible": true,
            "left": "0dp",
            "right": "0dp",
            "skin": "sknLblTableHeaderLine",
            "text": "-",
            "zIndex": 1
        }, {
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, {});
        flxContractsAccountHeaderView.add(flxHeader, lblHeaderSeperator);
        return flxContractsAccountHeaderView;
    }
})