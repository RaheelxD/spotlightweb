define("flxContractsLimitsBodyCreate", function() {
    return function(controller) {
        var flxContractsLimitsBodyCreate = new kony.ui.FlexContainer({
            "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
            "clipBounds": true,
            "id": "flxContractsLimitsBodyCreate",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "left": "0dp",
            "isModalContainer": false,
            "skin": "sknFlxFFFFFF1000",
            "top": "0dp",
            "width": "100%"
        }, {}, {});
        flxContractsLimitsBodyCreate.setDefaultUnit(kony.flex.DP);
        var flxViewLimits = new kony.ui.FlexContainer({
            "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
            "clipBounds": true,
            "id": "flxViewLimits",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "left": "10px",
            "minHeight": 70,
            "isModalContainer": false,
            "right": "10px",
            "skin": "slFbox",
            "top": "0px"
        }, {}, {});
        flxViewLimits.setDefaultUnit(kony.flex.DP);
        var flxLimitActionName = new kony.ui.FlexContainer({
            "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
            "clipBounds": true,
            "id": "flxLimitActionName",
            "isVisible": true,
            "layoutType": kony.flex.FLOW_HORIZONTAL,
            "left": "0dp",
            "isModalContainer": false,
            "skin": "slFbox",
            "top": "15dp",
            "width": "22%"
        }, {}, {});
        flxLimitActionName.setDefaultUnit(kony.flex.DP);
        var lblAction = new kony.ui.Label({
            "bottom": "15px",
            "id": "lblAction",
            "isVisible": true,
            "left": "30dp",
            "skin": "sknlblLato696c7312px",
            "text": "Create Bill Payment",
            "top": 10,
            "width": "60%",
            "zIndex": 1
        }, {
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, {
            "hoverSkin": "sknlblLato00000011px"
        });
        var flxRangeIcon = new kony.ui.FlexContainer({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "clipBounds": true,
            "height": "15dp",
            "id": "flxRangeIcon",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "left": "7dp",
            "isModalContainer": false,
            "skin": "sknCursor",
            "top": "10dp",
            "width": "15dp"
        }, {}, {});
        flxRangeIcon.setDefaultUnit(kony.flex.DP);
        var lblIconRangeInfo = new kony.ui.Label({
            "centerX": "50%",
            "centerY": "50%",
            "height": "15dp",
            "id": "lblIconRangeInfo",
            "isVisible": true,
            "skin": "sknFontIcon485C7518px",
            "text": "",
            "width": "18dp"
        }, {
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, {});
        flxRangeIcon.add(lblIconRangeInfo);
        flxLimitActionName.add(lblAction, flxRangeIcon);
        var flxPerLimitTextBox = new kony.ui.FlexContainer({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "clipBounds": true,
            "height": "40dp",
            "id": "flxPerLimitTextBox",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "left": "25%",
            "isModalContainer": false,
            "skin": "sknFlxbgFFFFFFbdrD7D9E0rd3px",
            "top": "15px",
            "width": "22%"
        }, {}, {});
        flxPerLimitTextBox.setDefaultUnit(kony.flex.DP);
        var tbxPerValue = new kony.ui.TextBox2({
            "autoCapitalize": constants.TEXTBOX_AUTO_CAPITALIZE_NONE,
            "centerY": "50%",
            "height": "38dp",
            "id": "tbxPerValue",
            "isVisible": true,
            "keyBoardStyle": constants.TEXTBOX_KEY_BOARD_STYLE_DEFAULT,
            "left": "30px",
            "right": "0dp",
            "secureTextEntry": false,
            "skin": "sknTbxBgFFFFFFf485c7513pxNoBorder",
            "textInputMode": constants.TEXTBOX_INPUT_MODE_NUMERIC,
            "zIndex": 1
        }, {
            "containerHeightMode": constants.TEXTBOX_FONT_METRICS_DRIVEN_HEIGHT,
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [2, 0, 0, 0],
            "paddingInPixel": false
        }, {
            "autoCorrect": false,
            "placeholderSkin": "defTextBoxPlaceholder"
        });
        var lblCurrencySymbol1 = new kony.ui.Label({
            "centerY": "50%",
            "id": "lblCurrencySymbol1",
            "isVisible": true,
            "left": "5dp",
            "skin": "sknFontIcon425D7724px",
            "text": "",
            "width": "20dp",
            "zIndex": 1
        }, {
            "contentAlignment": constants.CONTENT_ALIGN_CENTER,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, {});
        flxPerLimitTextBox.add(tbxPerValue, lblCurrencySymbol1);
        var flxLimitError1 = new kony.ui.FlexContainer({
            "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
            "bottom": "10dp",
            "clipBounds": true,
            "id": "flxLimitError1",
            "isVisible": false,
            "layoutType": kony.flex.FLOW_HORIZONTAL,
            "left": "25%",
            "isModalContainer": false,
            "skin": "slFbox",
            "top": "60dp",
            "width": "22%",
            "zIndex": 1
        }, {}, {});
        flxLimitError1.setDefaultUnit(kony.flex.DP);
        var lblLimitErrorIcon1 = new kony.ui.Label({
            "height": "15dp",
            "id": "lblLimitErrorIcon1",
            "isVisible": true,
            "left": "0dp",
            "skin": "sknErrorIcon",
            "text": "",
            "top": "0dp",
            "width": "15dp",
            "zIndex": 1
        }, {
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, {});
        var lblLimitError1 = new kony.ui.Label({
            "id": "lblLimitError1",
            "isVisible": true,
            "left": "0dp",
            "skin": "sknlblError",
            "text": "Name cannot be empty ",
            "top": "0dp",
            "width": "94%",
            "zIndex": 1
        }, {
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, {});
        flxLimitError1.add(lblLimitErrorIcon1, lblLimitError1);
        var flxLimitError2 = new kony.ui.FlexContainer({
            "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
            "bottom": "10dp",
            "clipBounds": true,
            "id": "flxLimitError2",
            "isVisible": false,
            "layoutType": kony.flex.FLOW_HORIZONTAL,
            "left": "50%",
            "isModalContainer": false,
            "skin": "slFbox",
            "top": "60dp",
            "width": "22%",
            "zIndex": 1
        }, {}, {});
        flxLimitError2.setDefaultUnit(kony.flex.DP);
        var lblLimitErrorIcon2 = new kony.ui.Label({
            "height": "15dp",
            "id": "lblLimitErrorIcon2",
            "isVisible": true,
            "left": "0dp",
            "skin": "sknErrorIcon",
            "text": "",
            "top": "0dp",
            "width": "15dp",
            "zIndex": 1
        }, {
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, {});
        var lblLimitError2 = new kony.ui.Label({
            "id": "lblLimitError2",
            "isVisible": true,
            "left": "0dp",
            "skin": "sknlblError",
            "text": "Name cannot be empty ",
            "top": "0dp",
            "width": "94%",
            "zIndex": 1
        }, {
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, {});
        flxLimitError2.add(lblLimitErrorIcon2, lblLimitError2);
        var flxLimitError3 = new kony.ui.FlexContainer({
            "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
            "bottom": "10dp",
            "clipBounds": true,
            "id": "flxLimitError3",
            "isVisible": false,
            "layoutType": kony.flex.FLOW_HORIZONTAL,
            "left": "75%",
            "isModalContainer": false,
            "skin": "slFbox",
            "top": "60dp",
            "width": "22%",
            "zIndex": 1
        }, {}, {});
        flxLimitError3.setDefaultUnit(kony.flex.DP);
        var lblLimitErrorIcon3 = new kony.ui.Label({
            "height": "15dp",
            "id": "lblLimitErrorIcon3",
            "isVisible": true,
            "left": "0dp",
            "skin": "sknErrorIcon",
            "text": "",
            "top": "0dp",
            "width": "15dp",
            "zIndex": 1
        }, {
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, {});
        var lblLimitError3 = new kony.ui.Label({
            "id": "lblLimitError3",
            "isVisible": true,
            "left": "0dp",
            "skin": "sknlblError",
            "text": "Name cannot be empty ",
            "top": "0dp",
            "width": "94%",
            "zIndex": 1
        }, {
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, {});
        flxLimitError3.add(lblLimitErrorIcon3, lblLimitError3);
        var flxDailyLimitTextBox = new kony.ui.FlexContainer({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "clipBounds": true,
            "height": "40dp",
            "id": "flxDailyLimitTextBox",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "left": "50%",
            "isModalContainer": false,
            "skin": "sknFlxbgFFFFFFbdrD7D9E0rd3px",
            "top": "15px",
            "width": "22%"
        }, {}, {});
        flxDailyLimitTextBox.setDefaultUnit(kony.flex.DP);
        var tbxDailyValue = new kony.ui.TextBox2({
            "autoCapitalize": constants.TEXTBOX_AUTO_CAPITALIZE_NONE,
            "centerY": "50%",
            "height": "38dp",
            "id": "tbxDailyValue",
            "isVisible": true,
            "keyBoardStyle": constants.TEXTBOX_KEY_BOARD_STYLE_DEFAULT,
            "left": "30px",
            "right": "0dp",
            "secureTextEntry": false,
            "skin": "sknTbxBgFFFFFFf485c7513pxNoBorder",
            "textInputMode": constants.TEXTBOX_INPUT_MODE_NUMERIC,
            "zIndex": 1
        }, {
            "containerHeightMode": constants.TEXTBOX_FONT_METRICS_DRIVEN_HEIGHT,
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [2, 0, 0, 0],
            "paddingInPixel": false
        }, {
            "autoCorrect": false,
            "placeholderSkin": "defTextBoxPlaceholder"
        });
        var lblCurrencySymbol2 = new kony.ui.Label({
            "centerY": "50%",
            "id": "lblCurrencySymbol2",
            "isVisible": true,
            "left": "5dp",
            "skin": "sknFontIcon425D7724px",
            "text": "",
            "width": "20dp",
            "zIndex": 1
        }, {
            "contentAlignment": constants.CONTENT_ALIGN_CENTER,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, {});
        flxDailyLimitTextBox.add(tbxDailyValue, lblCurrencySymbol2);
        var flxWeeklyLimitTextBox = new kony.ui.FlexContainer({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "clipBounds": true,
            "height": "40dp",
            "id": "flxWeeklyLimitTextBox",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "left": "75%",
            "isModalContainer": false,
            "skin": "sknFlxbgFFFFFFbdrD7D9E0rd3px",
            "top": "15px",
            "width": "22%"
        }, {}, {});
        flxWeeklyLimitTextBox.setDefaultUnit(kony.flex.DP);
        var tbxWeeklyValue = new kony.ui.TextBox2({
            "autoCapitalize": constants.TEXTBOX_AUTO_CAPITALIZE_NONE,
            "centerY": "50%",
            "height": "38dp",
            "id": "tbxWeeklyValue",
            "isVisible": true,
            "keyBoardStyle": constants.TEXTBOX_KEY_BOARD_STYLE_DEFAULT,
            "left": "30px",
            "right": "0dp",
            "secureTextEntry": false,
            "skin": "sknTbxBgFFFFFFf485c7513pxNoBorder",
            "textInputMode": constants.TEXTBOX_INPUT_MODE_NUMERIC,
            "zIndex": 1
        }, {
            "containerHeightMode": constants.TEXTBOX_FONT_METRICS_DRIVEN_HEIGHT,
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [2, 0, 0, 0],
            "paddingInPixel": false
        }, {
            "autoCorrect": false,
            "placeholderSkin": "defTextBoxPlaceholder"
        });
        var lblCurrencySymbol3 = new kony.ui.Label({
            "centerY": "50%",
            "id": "lblCurrencySymbol3",
            "isVisible": true,
            "left": "5dp",
            "skin": "sknFontIcon425D7724px",
            "text": "",
            "width": "20dp",
            "zIndex": 1
        }, {
            "contentAlignment": constants.CONTENT_ALIGN_CENTER,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, {});
        flxWeeklyLimitTextBox.add(tbxWeeklyValue, lblCurrencySymbol3);
        var lblLimitsSeperator = new kony.ui.Label({
            "bottom": 0,
            "height": "1px",
            "id": "lblLimitsSeperator",
            "isVisible": true,
            "left": "10px",
            "right": "10px",
            "skin": "sknLblSeparatore7e7e7",
            "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmCSR.lblSeperator\")",
            "zIndex": 1
        }, {
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, {});
        flxViewLimits.add(flxLimitActionName, flxPerLimitTextBox, flxLimitError1, flxLimitError2, flxLimitError3, flxDailyLimitTextBox, flxWeeklyLimitTextBox, lblLimitsSeperator);
        flxContractsLimitsBodyCreate.add(flxViewLimits);
        return flxContractsLimitsBodyCreate;
    }
})