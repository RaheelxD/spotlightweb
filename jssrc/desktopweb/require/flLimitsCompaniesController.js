define("userflLimitsCompaniesController", {
    //Type your controller code here 
});
define("flLimitsCompaniesControllerActions", {
    /*
      This is an auto generated file and any modifications to it may result in corruption of the action sequence.
    */
    /** onClick defined for flxCheckBox **/
    AS_FlexContainer_c4b6e1cec6894fb6951c9ca4a51f7ac8: function AS_FlexContainer_c4b6e1cec6894fb6951c9ca4a51f7ac8(eventobject, context) {
        var self = this;
        this.executeOnParent("showSelectedRowServices");
    }
});
define("flLimitsCompaniesController", ["userflLimitsCompaniesController", "flLimitsCompaniesControllerActions"], function() {
    var controller = require("userflLimitsCompaniesController");
    var controllerActions = ["flLimitsCompaniesControllerActions"];
    return kony.visualizer.mixinControllerActions(controller, controllerActions);
});
