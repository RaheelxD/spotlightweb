define("flxActionStatus", function() {
    return function(controller) {
        var flxActionStatus = new kony.ui.FlexContainer({
            "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
            "clipBounds": true,
            "id": "flxActionStatus",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "left": "0dp",
            "isModalContainer": false,
            "skin": "sknflxffffffop100",
            "top": "0dp",
            "width": "100%"
        }, {}, {});
        flxActionStatus.setDefaultUnit(kony.flex.DP);
        var lblActionName = new kony.ui.Label({
            "bottom": "6dp",
            "id": "lblActionName",
            "isVisible": true,
            "left": "30px",
            "skin": "sknLbl485C75LatoRegular13Px",
            "text": "Admin Role",
            "top": "6dp",
            "width": "24%",
            "zIndex": 1
        }, {
            "contentAlignment": constants.CONTENT_ALIGN_TOP_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, {});
        var lblDescription = new kony.ui.Label({
            "bottom": "6dp",
            "id": "lblDescription",
            "isVisible": true,
            "left": "32%",
            "skin": "sknLbl485C75LatoRegular13Px",
            "text": "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do",
            "top": "6dp",
            "width": "57%",
            "zIndex": 1
        }, {
            "contentAlignment": constants.CONTENT_ALIGN_TOP_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, {});
        var flxStatus = new kony.ui.FlexContainer({
            "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
            "bottom": "5dp",
            "clipBounds": true,
            "id": "flxStatus",
            "isVisible": true,
            "layoutType": kony.flex.FLOW_HORIZONTAL,
            "reverseLayoutDirection": false,
            "isModalContainer": false,
            "right": 0,
            "skin": "slFbox",
            "top": "5dp",
            "width": "10%"
        }, {}, {});
        flxStatus.setDefaultUnit(kony.flex.DP);
        var statusIcon = new kony.ui.Label({
            "id": "statusIcon",
            "isVisible": true,
            "left": "0dp",
            "skin": "sknFontIconActivate",
            "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmAlertsManagement.lblViewAlertStatusKey\")",
            "top": 2,
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, {
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, {});
        var statusValue = new kony.ui.Label({
            "id": "statusValue",
            "isVisible": true,
            "left": "5dp",
            "skin": "sknlblLato5bc06cBold14px",
            "i18n_text": "kony.i18n.getLocalizedString(\"i18n.secureimage.Active\")",
            "top": "0px",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, {
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, {});
        flxStatus.add(statusIcon, statusValue);
        var lblSeperator = new kony.ui.Label({
            "height": "1px",
            "id": "lblSeperator",
            "isVisible": false,
            "left": "0dp",
            "skin": "sknlblSeperator",
            "text": ".",
            "top": "0dp",
            "width": "100%",
            "zIndex": 1
        }, {
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, {});
        flxActionStatus.add(lblActionName, lblDescription, flxStatus, lblSeperator);
        return flxActionStatus;
    }
})