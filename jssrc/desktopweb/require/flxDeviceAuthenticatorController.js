define("userflxDeviceAuthenticatorController", {
    //Type your controller code here 
});
define("flxDeviceAuthenticatorControllerActions", {
    /*
        This is an auto generated file and any modifications to it may result in corruption of the action sequence.
    */
});
define("flxDeviceAuthenticatorController", ["userflxDeviceAuthenticatorController", "flxDeviceAuthenticatorControllerActions"], function() {
    var controller = require("userflxDeviceAuthenticatorController");
    var controllerActions = ["flxDeviceAuthenticatorControllerActions"];
    return kony.visualizer.mixinControllerActions(controller, controllerActions);
});
