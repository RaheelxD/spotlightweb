define("flxUsersDropDown", function() {
    return function(controller) {
        var flxUsersDropDown = new kony.ui.FlexContainer({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "clipBounds": true,
            "height": "30px",
            "id": "flxUsersDropDown",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "isModalContainer": false,
            "skin": "sknflxffffffop100"
        }, {}, {
            "hoverSkin": "sknCursor"
        });
        flxUsersDropDown.setDefaultUnit(kony.flex.DP);
        var flxName = new kony.ui.FlexContainer({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "clipBounds": true,
            "height": "100%",
            "id": "flxName",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "left": "10dp",
            "isModalContainer": false,
            "right": "10dp",
            "skin": "slFbox",
            "top": "0dp"
        }, {}, {
            "hoverSkin": "sknFlxHoverEAF3FB"
        });
        flxName.setDefaultUnit(kony.flex.DP);
        var lblViewFullName = new kony.ui.Label({
            "centerY": "50%",
            "id": "lblViewFullName",
            "isVisible": true,
            "left": "10dp",
            "skin": "sknlblLatoRegular484b5213px",
            "text": "Edward",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, {
            "contentAlignment": constants.CONTENT_ALIGN_CENTER,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, {});
        flxName.add(lblViewFullName);
        flxUsersDropDown.add(flxName);
        return flxUsersDropDown;
    }
})