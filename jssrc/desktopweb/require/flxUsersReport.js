define("flxUsersReport", function() {
    return function(controller) {
        var flxUsersReport = new kony.ui.FlexContainer({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "clipBounds": true,
            "height": "50px",
            "id": "flxUsersReport",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "left": "0dp",
            "isModalContainer": false,
            "skin": "slFbox",
            "top": "0dp",
            "width": "100%"
        }, {}, {
            "hoverSkin": "sknfbfcfc"
        });
        flxUsersReport.setDefaultUnit(kony.flex.DP);
        var flxUsersReportWrapper = new kony.ui.FlexContainer({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "clipBounds": true,
            "height": "50px",
            "id": "flxUsersReportWrapper",
            "isVisible": true,
            "layoutType": kony.flex.FLOW_HORIZONTAL,
            "left": "35px",
            "isModalContainer": false,
            "onClick": controller.AS_FlexContainer_e553ee3b13fe4973aef70f31c7776c5d,
            "right": "35px",
            "skin": "slFbox",
            "top": "0"
        }, {}, {
            "hoverSkin": "sknHoverWithoutHandCursor"
        });
        flxUsersReportWrapper.setDefaultUnit(kony.flex.DP);
        var flxFirstColumn = new kony.ui.FlexContainer({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "clipBounds": true,
            "height": "50px",
            "id": "flxFirstColumn",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "left": "0dp",
            "isModalContainer": false,
            "skin": "slFbox",
            "top": "0dp",
            "width": "50%",
            "zIndex": 10
        }, {}, {});
        flxFirstColumn.setDefaultUnit(kony.flex.DP);
        var lblDescription = new kony.ui.Label({
            "centerY": "49%",
            "id": "lblDescription",
            "isVisible": true,
            "left": "20px",
            "right": 0,
            "skin": "sknlblLatoRegular484b5213px",
            "text": "User enrolled",
            "zIndex": 1
        }, {
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, {});
        flxFirstColumn.add(lblDescription);
        var lblMobile = new kony.ui.Label({
            "centerY": "50%",
            "id": "lblMobile",
            "isVisible": true,
            "left": "0",
            "skin": "sknlblLatoRegular484b5213px",
            "text": "2000",
            "width": "17%",
            "zIndex": 1
        }, {
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, {});
        var lblOnline = new kony.ui.Label({
            "centerY": "50%",
            "id": "lblOnline",
            "isVisible": true,
            "left": "0",
            "skin": "sknlblLatoRegular484b5213px",
            "text": "3000",
            "width": "16%",
            "zIndex": 1
        }, {
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, {});
        var flxlastColoumn = new kony.ui.FlexContainer({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "centerY": "50%",
            "clipBounds": true,
            "height": "25px",
            "id": "flxlastColoumn",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "isModalContainer": false,
            "onClick": controller.AS_FlexContainer_b841079c89634eada06d07f145523502,
            "skin": "slFbox",
            "width": "17%",
            "zIndex": 10
        }, {}, {});
        flxlastColoumn.setDefaultUnit(kony.flex.DP);
        var flxOptions = new kony.ui.FlexContainer({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "centerY": "50%",
            "clipBounds": true,
            "height": "20px",
            "id": "flxOptions",
            "isVisible": false,
            "layoutType": kony.flex.FREE_FORM,
            "isModalContainer": false,
            "onClick": controller.AS_FlexContainer_b841079c89634eada06d07f145523502,
            "right": "0px",
            "skin": "slFbox",
            "width": "20px",
            "zIndex": 10
        }, {}, {
            "hoverSkin": "sknflxffffffop100Border424242Radius100px"
        });
        flxOptions.setDefaultUnit(kony.flex.DP);
        var imgOptions = new kony.ui.Image2({
            "centerX": "50%",
            "centerY": "50%",
            "height": "12dp",
            "id": "imgOptions",
            "isVisible": true,
            "left": 0,
            "onTouchStart": controller.AS_Image_b35eefc2658245709c5fad34602a8750,
            "skin": "slImage",
            "src": "dots3x.png",
            "width": "6dp",
            "zIndex": 1
        }, {
            "imageScaleMode": constants.IMAGE_SCALE_MODE_MAINTAIN_ASPECT_RATIO,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, {});
        flxOptions.add(imgOptions);
        var lblTotal = new kony.ui.Label({
            "centerY": "50%",
            "id": "lblTotal",
            "isVisible": true,
            "left": "0",
            "right": "20px",
            "skin": "sknlblLatoRegular484b5213px",
            "text": "5000",
            "zIndex": 1
        }, {
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, {});
        flxlastColoumn.add(flxOptions, lblTotal);
        flxUsersReportWrapper.add(flxFirstColumn, lblMobile, lblOnline, flxlastColoumn);
        var lblSeperator = new kony.ui.Label({
            "bottom": "0px",
            "height": "1px",
            "id": "lblSeperator",
            "isVisible": true,
            "left": "35px",
            "right": "35px",
            "skin": "sknlblSeperator",
            "text": ".",
            "zIndex": 1
        }, {
            "contentAlignment": constants.CONTENT_ALIGN_TOP_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, {});
        flxUsersReport.add(flxUsersReportWrapper, lblSeperator);
        return flxUsersReport;
    }
})