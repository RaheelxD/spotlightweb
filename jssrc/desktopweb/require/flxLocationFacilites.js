define("flxLocationFacilites", function() {
    return function(controller) {
        var flxLocationFacilites = new kony.ui.FlexContainer({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "clipBounds": true,
            "height": "50px",
            "id": "flxLocationFacilites",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "isModalContainer": false,
            "skin": "sknflxffffffop100"
        }, {}, {});
        flxLocationFacilites.setDefaultUnit(kony.flex.DP);
        var lblFacilityName = new kony.ui.Label({
            "centerY": "50%",
            "id": "lblFacilityName",
            "isVisible": true,
            "left": "30px",
            "skin": "sknlblLatoBold35475f14px",
            "text": "Admin Role",
            "width": "22%",
            "zIndex": 1
        }, {
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, {});
        var lblDescription = new kony.ui.Label({
            "centerY": "50%",
            "id": "lblDescription",
            "isVisible": true,
            "left": "26.27%",
            "skin": "sknlblLatoBold35475f14px",
            "text": "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do",
            "width": "66%",
            "zIndex": 1
        }, {
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, {});
        var lblSeperator = new kony.ui.Label({
            "height": "1px",
            "id": "lblSeperator",
            "isVisible": true,
            "left": "20dp",
            "right": "20dp",
            "skin": "sknlblSeperator",
            "text": ".",
            "top": "0dp",
            "zIndex": 1
        }, {
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, {});
        var flxActionIcon = new kony.ui.FlexContainer({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "centerY": "50%",
            "clipBounds": true,
            "height": "40dp",
            "id": "flxActionIcon",
            "isVisible": false,
            "layoutType": kony.flex.FREE_FORM,
            "isModalContainer": false,
            "right": "30dp",
            "skin": "slFbox",
            "width": "40dp",
            "zIndex": 1
        }, {}, {
            "hoverSkin": "sknCursor"
        });
        flxActionIcon.setDefaultUnit(kony.flex.DP);
        var lblIconAction = new kony.ui.Label({
            "centerX": "50%",
            "centerY": "50%",
            "id": "lblIconAction",
            "isVisible": true,
            "skin": "sknIcon20px",
            "text": "",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, {
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, {});
        flxActionIcon.add(lblIconAction);
        flxLocationFacilites.add(lblFacilityName, lblDescription, lblSeperator, flxActionIcon);
        return flxLocationFacilites;
    }
})