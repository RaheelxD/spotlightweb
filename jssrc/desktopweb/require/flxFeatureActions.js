define("flxFeatureActions", function() {
    return function(controller) {
        var flxFeatureActions = new kony.ui.FlexContainer({
            "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
            "clipBounds": true,
            "id": "flxFeatureActions",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "isModalContainer": false,
            "skin": "sknflxffffffop100"
        }, {}, {});
        flxFeatureActions.setDefaultUnit(kony.flex.DP);
        var lblActionName = new kony.ui.Label({
            "bottom": "13dp",
            "id": "lblActionName",
            "isVisible": true,
            "left": "30px",
            "skin": "sknLbl485C75LatoRegular13Px",
            "text": "Admin Role",
            "top": "12dp",
            "width": "24%",
            "zIndex": 1
        }, {
            "contentAlignment": constants.CONTENT_ALIGN_TOP_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, {});
        var lblDescription = new kony.ui.Label({
            "bottom": "13dp",
            "id": "lblDescription",
            "isVisible": true,
            "left": "32%",
            "skin": "sknLbl485C75LatoRegular13Px",
            "text": "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do",
            "top": "12dp",
            "width": "57%",
            "zIndex": 1
        }, {
            "contentAlignment": constants.CONTENT_ALIGN_TOP_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, {});
        var lblView = new kony.ui.Label({
            "bottom": "13dp",
            "id": "lblView",
            "isVisible": true,
            "right": "20dp",
            "skin": "sknLblLatoReg117eb013px",
            "i18n_text": "kony.i18n.getLocalizedString(\"i18n.permission.View\")",
            "top": "12dp",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, {
            "contentAlignment": constants.CONTENT_ALIGN_TOP_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, {
            "hoverSkin": "sknLbl117eb013pxHov"
        });
        var lblSeperator = new kony.ui.Label({
            "height": "1px",
            "id": "lblSeperator",
            "isVisible": false,
            "left": "0dp",
            "skin": "sknlblSeperator",
            "text": ".",
            "top": "0dp",
            "width": "100%",
            "zIndex": 1
        }, {
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, {});
        flxFeatureActions.add(lblActionName, lblDescription, lblView, lblSeperator);
        return flxFeatureActions;
    }
})