define("flxDepositApplicationsSubmitted", function() {
    return function(controller) {
        var flxDepositApplicationsSubmitted = new kony.ui.FlexContainer({
            "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
            "clipBounds": true,
            "id": "flxDepositApplicationsSubmitted",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "left": "0dp",
            "isModalContainer": false,
            "skin": "slFbox",
            "top": "0dp",
            "width": "100%"
        }, {}, {});
        flxDepositApplicationsSubmitted.setDefaultUnit(kony.flex.DP);
        var flxSubmittedApplications = new kony.ui.FlexContainer({
            "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
            "bottom": "15dp",
            "clipBounds": true,
            "id": "flxSubmittedApplications",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "left": "0dp",
            "isModalContainer": false,
            "skin": "slFbox",
            "top": "0dp",
            "width": "100%"
        }, {}, {});
        flxSubmittedApplications.setDefaultUnit(kony.flex.DP);
        var lblDepositApplIdSubmit = new kony.ui.Label({
            "id": "lblDepositApplIdSubmit",
            "isVisible": true,
            "left": "35px",
            "skin": "sknlblLatoBold35475f14px",
            "text": "4354545465656767",
            "top": "15dp",
            "width": "17%",
            "zIndex": 1
        }, {
            "contentAlignment": constants.CONTENT_ALIGN_TOP_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, {});
        var lblDepositApplProductSubmit = new kony.ui.Label({
            "id": "lblDepositApplProductSubmit",
            "isVisible": true,
            "left": "22%",
            "skin": "sknlblLatoBold35475f14px",
            "text": "Club Saving Account",
            "top": "15dp",
            "width": "20%",
            "zIndex": 1
        }, {
            "contentAlignment": constants.CONTENT_ALIGN_TOP_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, {});
        var lblDepositApplCreatedStarted = new kony.ui.Label({
            "id": "lblDepositApplCreatedStarted",
            "isVisible": true,
            "left": "43%",
            "skin": "sknlblLatoBold35475f14px",
            "text": "25-05-2019",
            "top": "15dp",
            "width": "17%",
            "zIndex": 1
        }, {
            "contentAlignment": constants.CONTENT_ALIGN_TOP_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, {});
        var lblDepositApplSubmitOn = new kony.ui.Label({
            "id": "lblDepositApplSubmitOn",
            "isVisible": true,
            "left": "61%",
            "skin": "sknlblLatoBold35475f14px",
            "text": "29-05-2019",
            "top": "15dp",
            "width": "17%",
            "zIndex": 1
        }, {
            "contentAlignment": constants.CONTENT_ALIGN_TOP_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, {});
        var lblDepositApplStatusSubmit = new kony.ui.Label({
            "id": "lblDepositApplStatusSubmit",
            "isVisible": true,
            "left": "80%",
            "skin": "sknlblLatoBold35475f14px",
            "text": "Approval Pending",
            "top": "15dp",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, {
            "contentAlignment": constants.CONTENT_ALIGN_TOP_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, {});
        var flxTrackApplication = new kony.ui.FlexContainer({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "clipBounds": true,
            "height": "20dp",
            "id": "flxTrackApplication",
            "isVisible": false,
            "layoutType": kony.flex.FREE_FORM,
            "isModalContainer": false,
            "right": "35dp",
            "skin": "hoverhandSkin",
            "top": "10dp",
            "width": "20dp"
        }, {}, {});
        flxTrackApplication.setDefaultUnit(kony.flex.DP);
        var lblIconTrackApplication = new kony.ui.Label({
            "id": "lblIconTrackApplication",
            "isVisible": true,
            "left": "0dp",
            "skin": "sknIcon20px",
            "text": "",
            "top": "0dp",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, {
            "contentAlignment": constants.CONTENT_ALIGN_TOP_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, {});
        flxTrackApplication.add(lblIconTrackApplication);
        var flxNavtoSummary = new kony.ui.FlexContainer({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "clipBounds": true,
            "height": "20dp",
            "id": "flxNavtoSummary",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "isModalContainer": false,
            "right": "35dp",
            "skin": "hoverhandSkin",
            "top": "10dp",
            "width": "20dp",
            "zIndex": 1
        }, {}, {});
        flxNavtoSummary.setDefaultUnit(kony.flex.DP);
        var lblNavtoSummary = new kony.ui.Label({
            "height": "20px",
            "id": "lblNavtoSummary",
            "isVisible": true,
            "left": "0dp",
            "skin": "sknIcon20px",
            "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmAdManagement.ResumeIcon\")",
            "top": "0px",
            "width": "20px",
            "zIndex": 1
        }, {
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, {});
        flxNavtoSummary.add(lblNavtoSummary);
        flxSubmittedApplications.add(lblDepositApplIdSubmit, lblDepositApplProductSubmit, lblDepositApplCreatedStarted, lblDepositApplSubmitOn, lblDepositApplStatusSubmit, flxTrackApplication, flxNavtoSummary);
        var lblStartedSeperator = new kony.ui.Label({
            "bottom": "0dp",
            "height": "1dp",
            "id": "lblStartedSeperator",
            "isVisible": true,
            "left": "20dp",
            "right": 20,
            "skin": "sknlblSeperator",
            "text": "Label",
            "zIndex": 1
        }, {
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, {});
        flxDepositApplicationsSubmitted.add(flxSubmittedApplications, lblStartedSeperator);
        return flxDepositApplicationsSubmitted;
    }
})