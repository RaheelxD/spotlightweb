define("flxCustMangProductInfo", function() {
    return function(controller) {
        var flxCustMangProductInfo = new kony.ui.FlexContainer({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "clipBounds": true,
            "height": "50px",
            "id": "flxCustMangProductInfo",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "isModalContainer": false,
            "onClick": controller.AS_FlexContainer_e553ee3b13fe4973aef70f31c7776c5d,
            "skin": "sknCursor"
        }, {}, {
            "hoverSkin": "sknfbfcfc"
        });
        flxCustMangProductInfo.setDefaultUnit(kony.flex.DP);
        var flxCustMangProduct = new kony.ui.FlexContainer({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "clipBounds": true,
            "height": "50px",
            "id": "flxCustMangProduct",
            "isVisible": true,
            "layoutType": kony.flex.FLOW_HORIZONTAL,
            "left": "0px",
            "isModalContainer": false,
            "onClick": controller.AS_FlexContainer_e553ee3b13fe4973aef70f31c7776c5d,
            "right": "0px",
            "skin": "slFbox",
            "top": "0"
        }, {}, {
            "hoverSkin": "sknfbfcfc"
        });
        flxCustMangProduct.setDefaultUnit(kony.flex.DP);
        var flxProductType = new kony.ui.FlexContainer({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "clipBounds": true,
            "height": "50px",
            "id": "flxProductType",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "left": "10dp",
            "isModalContainer": false,
            "skin": "slFbox",
            "top": "0dp",
            "width": "23%",
            "zIndex": 1
        }, {}, {});
        flxProductType.setDefaultUnit(kony.flex.DP);
        var lblProductType = new kony.ui.Label({
            "centerY": "50%",
            "id": "lblProductType",
            "isVisible": true,
            "left": "0px",
            "right": "0px",
            "skin": "sknLblLato485c7513px",
            "text": "Savings",
            "zIndex": 1
        }, {
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, {});
        flxProductType.add(lblProductType);
        var lblProductName = new kony.ui.Label({
            "centerY": "50%",
            "id": "lblProductName",
            "isVisible": true,
            "skin": "sknLblLato485c7513px",
            "text": "Gold saving",
            "width": "24.50%",
            "zIndex": 1
        }, {
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, {});
        var lblAccountNumber = new kony.ui.Label({
            "centerY": "50%",
            "id": "lblAccountNumber",
            "isVisible": true,
            "skin": "sknLblLato485c7513px",
            "text": "1234567890123456",
            "width": "21%",
            "zIndex": 1
        }, {
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, {});
        var flxAccountOwner = new kony.ui.FlexContainer({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "clipBounds": true,
            "height": "100%",
            "id": "flxAccountOwner",
            "isVisible": true,
            "layoutType": kony.flex.FLOW_HORIZONTAL,
            "left": "0dp",
            "isModalContainer": false,
            "skin": "slFbox",
            "top": "0dp",
            "width": "19.50%"
        }, {}, {});
        flxAccountOwner.setDefaultUnit(kony.flex.DP);
        var btnAccountOwner = new kony.ui.Button({
            "centerY": "50%",
            "id": "btnAccountOwner",
            "isVisible": true,
            "left": "0",
            "skin": "sknBtnLato117eb013Px",
            "text": "John",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, {
            "contentAlignment": constants.CONTENT_ALIGN_CENTER,
            "displayText": true,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, {});
        var lblAccountOwner = new kony.ui.Label({
            "centerY": "50%",
            "id": "lblAccountOwner",
            "isVisible": true,
            "skin": "sknLblLato485c7513px",
            "text": "John",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, {
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, {});
        flxAccountOwner.add(btnAccountOwner, lblAccountOwner);
        var flxStatus = new kony.ui.FlexContainer({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "clipBounds": true,
            "height": "50px",
            "id": "flxStatus",
            "isVisible": true,
            "layoutType": kony.flex.FLOW_HORIZONTAL,
            "isModalContainer": false,
            "skin": "slFbox",
            "top": "0dp",
            "width": "8%",
            "zIndex": 1
        }, {}, {});
        flxStatus.setDefaultUnit(kony.flex.DP);
        var fontIconStatus = new kony.ui.Label({
            "centerY": "50%",
            "id": "fontIconStatus",
            "isVisible": true,
            "left": "0px",
            "skin": "sknFontIconActivate",
            "text": "",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, {
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, {});
        var lblStatus = new kony.ui.Label({
            "centerY": "50%",
            "id": "lblStatus",
            "isVisible": true,
            "left": "5px",
            "skin": "sknLblLato485c7513px",
            "text": "Active",
            "top": "0dp",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, {
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, {});
        flxStatus.add(fontIconStatus, lblStatus);
        var flxUnlink = new kony.ui.FlexContainer({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "clipBounds": true,
            "height": "100%",
            "id": "flxUnlink",
            "isVisible": false,
            "layoutType": kony.flex.FREE_FORM,
            "left": "0",
            "isModalContainer": false,
            "skin": "slFbox",
            "top": "0dp",
            "width": "2%",
            "zIndex": 1
        }, {}, {});
        flxUnlink.setDefaultUnit(kony.flex.DP);
        var flblUnlink = new kony.ui.Label({
            "centerY": "50%",
            "height": "20dp",
            "id": "flblUnlink",
            "isVisible": true,
            "right": "0dp",
            "skin": "sknIcomoon20px",
            "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmFAQ.fonticonEdit\")",
            "top": "20dp",
            "width": "20dp",
            "zIndex": 1
        }, {
            "contentAlignment": constants.CONTENT_ALIGN_CENTER,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, {});
        flxUnlink.add(flblUnlink);
        flxCustMangProduct.add(flxProductType, lblProductName, lblAccountNumber, flxAccountOwner, flxStatus, flxUnlink);
        var lblSeperator = new kony.ui.Label({
            "height": "1px",
            "id": "lblSeperator",
            "isVisible": true,
            "left": "0px",
            "right": "0px",
            "skin": "sknlblSeperator",
            "text": ".",
            "top": "49px",
            "zIndex": 1
        }, {
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, {});
        flxCustMangProductInfo.add(flxCustMangProduct, lblSeperator);
        return flxCustMangProductInfo;
    }
})