define("flxCustMangContracts", function() {
    return function(controller) {
        var flxCustMangContracts = new kony.ui.FlexContainer({
            "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
            "clipBounds": true,
            "id": "flxCustMangContracts",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "left": "0dp",
            "isModalContainer": false,
            "skin": "sknFlxFFFFFF100O",
            "top": "0dp",
            "width": "100%"
        }, {}, {});
        flxCustMangContracts.setDefaultUnit(kony.flex.DP);
        var flxContractCustomers = new kony.ui.FlexContainer({
            "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
            "clipBounds": true,
            "id": "flxContractCustomers",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "left": "30dp",
            "isModalContainer": false,
            "right": "0px",
            "skin": "slFbox",
            "top": "0dp",
            "zIndex": 1
        }, {}, {});
        flxContractCustomers.setDefaultUnit(kony.flex.DP);
        var lblCustName = new kony.ui.Label({
            "id": "lblCustName",
            "isVisible": true,
            "left": "10px",
            "skin": "sknLbl485C75LatoRegular13Px",
            "text": "1234567890",
            "top": "15px",
            "width": "18%",
            "zIndex": 1
        }, {
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, {});
        var lblCustId = new kony.ui.Label({
            "bottom": "15px",
            "id": "lblCustId",
            "isVisible": true,
            "left": "22%",
            "skin": "sknLbl485C75LatoRegular13Px",
            "text": "Savings",
            "top": "15px",
            "width": "16%",
            "zIndex": 1
        }, {
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, {});
        var lblTaxId = new kony.ui.Label({
            "id": "lblTaxId",
            "isVisible": true,
            "left": "40%",
            "skin": "sknLbl485C75LatoRegular13Px",
            "text": "Reward savings",
            "top": "15px",
            "width": "15%",
            "zIndex": 1
        }, {
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, {});
        var lblCustAddress = new kony.ui.Label({
            "bottom": "15px",
            "id": "lblCustAddress",
            "isVisible": true,
            "left": "57%",
            "skin": "sknLbl485C75LatoRegular13Px",
            "text": "Kony India Limited",
            "top": "15px",
            "width": "26%",
            "zIndex": 1
        }, {
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, {});
        var lblCustRole = new kony.ui.Label({
            "bottom": "15dp",
            "id": "lblCustRole",
            "isVisible": true,
            "left": "85%",
            "right": "20dp",
            "skin": "sknLblLato13px117eb0Cursor",
            "text": "Active",
            "top": "15dp",
            "zIndex": 1
        }, {
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, {});
        flxContractCustomers.add(lblCustName, lblCustId, lblTaxId, lblCustAddress, lblCustRole);
        var lblSeperator = new kony.ui.Label({
            "bottom": "0dp",
            "height": "1px",
            "id": "lblSeperator",
            "isVisible": true,
            "left": "0dp",
            "right": "0dp",
            "skin": "sknlblSeperator",
            "text": ".",
            "zIndex": 1
        }, {
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, {});
        flxCustMangContracts.add(flxContractCustomers, lblSeperator);
        return flxCustMangContracts;
    }
})