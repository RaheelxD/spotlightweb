define("flxCompanySelectedAccountsRow", function() {
    return function(controller) {
        var flxCompanySelectedAccountsRow = new kony.ui.FlexContainer({
            "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
            "clipBounds": true,
            "id": "flxCompanySelectedAccountsRow",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "left": "0dp",
            "isModalContainer": false,
            "skin": "sknFlxFFFFFF100O",
            "top": "0dp",
            "width": "100%"
        }, {}, {});
        flxCompanySelectedAccountsRow.setDefaultUnit(kony.flex.DP);
        var flxAccountRowContainer = new kony.ui.FlexContainer({
            "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
            "clipBounds": true,
            "id": "flxAccountRowContainer",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "left": 10,
            "isModalContainer": false,
            "right": "20dp",
            "skin": "sknFlxBgFFFFFFbrD7D9E0r1pxLeftRight",
            "top": "0"
        }, {}, {});
        flxAccountRowContainer.setDefaultUnit(kony.flex.DP);
        var flxAccountRowRecord = new kony.ui.FlexContainer({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "bottom": "10dp",
            "clipBounds": true,
            "height": "45px",
            "id": "flxAccountRowRecord",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "left": "10dp",
            "isModalContainer": false,
            "right": "10dp",
            "skin": "sknFlxbgFFFFFFbdrD7D9E0rd3px",
            "top": "0dp",
            "zIndex": 1
        }, {}, {
            "hoverSkin": "sknFlxSegRowHover11abeb"
        });
        flxAccountRowRecord.setDefaultUnit(kony.flex.DP);
        var lblRecordName = new kony.ui.Label({
            "centerY": "50%",
            "id": "lblRecordName",
            "isVisible": true,
            "left": "15px",
            "right": "45px",
            "skin": "sknlbl485c7514px",
            "text": "John Doe",
            "zIndex": 1
        }, {
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, {});
        var flxClose = new kony.ui.FlexContainer({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "centerY": "50%",
            "clipBounds": true,
            "height": "30dp",
            "id": "flxClose",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "isModalContainer": false,
            "right": "5px",
            "skin": "slFbox",
            "width": "30px",
            "zIndex": 1
        }, {}, {
            "hoverSkin": "sknCursor"
        });
        flxClose.setDefaultUnit(kony.flex.DP);
        var fontIconClose = new kony.ui.Label({
            "centerX": "50%",
            "centerY": "50%",
            "id": "fontIconClose",
            "isVisible": true,
            "skin": "sknFontIconCross14px",
            "text": "",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, {
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, {});
        flxClose.add(fontIconClose);
        flxAccountRowRecord.add(lblRecordName, flxClose);
        flxAccountRowContainer.add(flxAccountRowRecord);
        flxCompanySelectedAccountsRow.add(flxAccountRowContainer);
        return flxCompanySelectedAccountsRow;
    }
})