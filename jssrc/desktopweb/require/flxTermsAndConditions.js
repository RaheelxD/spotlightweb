define("flxTermsAndConditions", function() {
    return function(controller) {
        var flxTermsAndConditions = new kony.ui.FlexContainer({
            "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
            "clipBounds": true,
            "id": "flxTermsAndConditions",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "left": "0dp",
            "isModalContainer": false,
            "skin": "slFbox",
            "top": "0dp",
            "width": "100%"
        }, {}, {});
        flxTermsAndConditions.setDefaultUnit(kony.flex.DP);
        var flxTermsAndConditionsWrapper = new kony.ui.FlexContainer({
            "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
            "clipBounds": true,
            "focusSkin": "sknflxffffffop100",
            "id": "flxTermsAndConditionsWrapper",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "left": "20dp",
            "isModalContainer": false,
            "right": "20dp",
            "skin": "sknflxffffffop100",
            "top": "0"
        }, {}, {
            "hoverSkin": "sknfbfcfc"
        });
        flxTermsAndConditionsWrapper.setDefaultUnit(kony.flex.DP);
        var lblTitle = new kony.ui.Label({
            "bottom": "15dp",
            "id": "lblTitle",
            "isVisible": true,
            "left": "10px",
            "skin": "sknlblLatoBold35475f14px",
            "text": " DBX footer terms & conditions",
            "top": "15dp",
            "width": "190px",
            "zIndex": 1
        }, {
            "contentAlignment": constants.CONTENT_ALIGN_TOP_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, {});
        var lblCode = new kony.ui.Label({
            "bottom": "15dp",
            "id": "lblCode",
            "isVisible": true,
            "left": "34%",
            "skin": "sknlblLatoBold35475f14px",
            "text": "Loans_Mortgage_ApplicantAgreement",
            "top": "15dp",
            "width": "25%",
            "zIndex": 1
        }, {
            "contentAlignment": constants.CONTENT_ALIGN_TOP_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, {});
        var lblApplicableApps = new kony.ui.Label({
            "bottom": "15dp",
            "id": "lblApplicableApps",
            "isVisible": true,
            "left": "65%",
            "skin": "sknlblLatoBold35475f14px",
            "text": "Retail Banking, Business Banking, Onboarding",
            "top": "15dp",
            "width": "20%",
            "zIndex": 1
        }, {
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, {});
        var fonticonActive = new kony.ui.Label({
            "id": "fonticonActive",
            "isVisible": false,
            "right": "70dp",
            "skin": "sknIcon20px",
            "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmFAQ.fonticonEdit\")",
            "top": "13dp",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 2
        }, {
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, {});
        flxTermsAndConditionsWrapper.add(lblTitle, lblCode, lblApplicableApps, fonticonActive);
        var lblSeparator1 = new kony.ui.Label({
            "bottom": "0dp",
            "height": "1px",
            "id": "lblSeparator1",
            "isVisible": true,
            "left": "20dp",
            "right": "20dp",
            "skin": "sknlblSeperator",
            "text": "-",
            "zIndex": 2
        }, {
            "contentAlignment": constants.CONTENT_ALIGN_TOP_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, {});
        flxTermsAndConditions.add(flxTermsAndConditionsWrapper, lblSeparator1);
        return flxTermsAndConditions;
    }
})