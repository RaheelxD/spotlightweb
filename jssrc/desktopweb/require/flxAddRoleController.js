define("userflxAddRoleController", {
    addRole: function() {
        var index = kony.application.getCurrentForm().segAddOptions.selectedIndex;
        var rowIndex = index[0];
        var data = kony.application.getCurrentForm().segAddOptions.data;
        var lblOptionText = data[rowIndex].lblRoleName;
        var toAdd = {
            "imgClose": "close_blue.png",
            "lblOption": "" + lblOptionText
        };
        var data2 = kony.application.getCurrentForm().segSelectedOptions.data;
        data2.push(toAdd);
        kony.application.getCurrentForm().segSelectedOptions.setData(data2);
        kony.application.getCurrentForm().forceLayout();
        _kony.mvc.GetController(kony.application.getCurrentForm().id, true).addToUser();
    },
});
define("flxAddRoleControllerActions", {
    /*
        This is an auto generated file and any modifications to it may result in corruption of the action sequence.
    */
    AS_Button_fa163d0f68044aaaa7f73ec7e3863105: function AS_Button_fa163d0f68044aaaa7f73ec7e3863105(eventobject, context) {
        var self = this;
        this.addRole();
        //this.executeOnParent("addRoleRowInOptionsSelected");
    }
});
define("flxAddRoleController", ["userflxAddRoleController", "flxAddRoleControllerActions"], function() {
    var controller = require("userflxAddRoleController");
    var controllerActions = ["flxAddRoleControllerActions"];
    return kony.visualizer.mixinControllerActions(controller, controllerActions);
});
