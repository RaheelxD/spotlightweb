define("flxSubAlerts", function() {
    return function(controller) {
        var flxSubAlerts = new kony.ui.FlexContainer({
            "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
            "clipBounds": true,
            "id": "flxSubAlerts",
            "isVisible": true,
            "layoutType": kony.flex.FLOW_VERTICAL,
            "left": "0dp",
            "isModalContainer": false,
            "skin": "sknflxffffffop100",
            "top": "0dp",
            "width": "100%"
        }, {}, {
            "hoverSkin": "sknfbfcfc"
        });
        flxSubAlerts.setDefaultUnit(kony.flex.DP);
        var flxRow1 = new kony.ui.FlexContainer({
            "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
            "clipBounds": true,
            "id": "flxRow1",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "left": "0dp",
            "isModalContainer": false,
            "skin": "slFbox",
            "top": "0dp",
            "width": "100%"
        }, {}, {});
        flxRow1.setDefaultUnit(kony.flex.DP);
        var flxOptions = new kony.ui.FlexContainer({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "clipBounds": true,
            "height": "25dp",
            "id": "flxOptions",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "isModalContainer": false,
            "onClick": controller.AS_FlexContainer_gb05fbb7e77e41eda2711661bab68811,
            "right": "35px",
            "skin": "slFbox",
            "top": "11dp",
            "width": "25px",
            "zIndex": 1
        }, {}, {
            "hoverSkin": "sknflxffffffop100Border424242Radius100px"
        });
        flxOptions.setDefaultUnit(kony.flex.DP);
        var lblIconImgOptions = new kony.ui.Label({
            "centerX": "50%",
            "centerY": "50%",
            "id": "lblIconImgOptions",
            "isVisible": true,
            "skin": "sknFontIconOptionMenu",
            "text": "",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, {
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, {});
        flxOptions.add(lblIconImgOptions);
        var lblDisplayName = new kony.ui.Label({
            "bottom": "0dp",
            "id": "lblDisplayName",
            "isVisible": true,
            "left": "25dp",
            "skin": "sknlblLatoBold35475f14px",
            "text": "Admin Role",
            "top": "15px",
            "width": "17%",
            "zIndex": 100
        }, {
            "contentAlignment": constants.CONTENT_ALIGN_TOP_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, {});
        var flxStatus = new kony.ui.FlexContainer({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "bottom": "0dp",
            "clipBounds": true,
            "height": "15px",
            "id": "flxStatus",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "left": "82%",
            "isModalContainer": false,
            "skin": "slFbox",
            "top": "15dp",
            "width": "100px",
            "zIndex": 1
        }, {}, {});
        flxStatus.setDefaultUnit(kony.flex.DP);
        var lblAlertStatus = new kony.ui.Label({
            "centerY": "50%",
            "id": "lblAlertStatus",
            "isVisible": true,
            "left": "16px",
            "skin": "sknlblLato5bc06cBold14px",
            "text": "Active",
            "width": "70px",
            "zIndex": 1
        }, {
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, {});
        var fontIconStatusImg = new kony.ui.Label({
            "centerY": "50%",
            "id": "fontIconStatusImg",
            "isVisible": true,
            "left": "0dp",
            "skin": "sknFontIconActivate",
            "text": "",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, {
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, {});
        flxStatus.add(lblAlertStatus, fontIconStatusImg);
        var lblAlertType = new kony.ui.Label({
            "bottom": "0dp",
            "height": "20px",
            "id": "lblAlertType",
            "isVisible": true,
            "left": "50%",
            "skin": "sknLblIcomoon20px485c75",
            "text": "",
            "top": "15px",
            "width": "20px",
            "zIndex": 1
        }, {
            "contentAlignment": constants.CONTENT_ALIGN_TOP_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, {});
        var lblCode = new kony.ui.Label({
            "bottom": "0dp",
            "id": "lblCode",
            "isVisible": true,
            "left": "20%",
            "skin": "sknlblLatoBold35475f14px",
            "text": "AL001AL001",
            "top": "15px",
            "width": "20%",
            "zIndex": 2
        }, {
            "contentAlignment": constants.CONTENT_ALIGN_TOP_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, {});
        var lblFreqEnabled = new kony.ui.Label({
            "bottom": "0dp",
            "id": "lblFreqEnabled",
            "isVisible": true,
            "left": "65%",
            "skin": "sknlblLatoBold35475f14px",
            "text": "Yes",
            "top": "15px",
            "width": "10%",
            "zIndex": 1
        }, {
            "contentAlignment": constants.CONTENT_ALIGN_TOP_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, {});
        var flxToggle = new kony.ui.FlexContainer({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "clipBounds": true,
            "height": "13dp",
            "id": "flxToggle",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "left": "0dp",
            "isModalContainer": false,
            "right": "0px",
            "skin": "sknCursor",
            "top": "15dp",
            "width": "15dp",
            "zIndex": 1
        }, {}, {
            "hoverSkin": "sknCursor"
        });
        flxToggle.setDefaultUnit(kony.flex.DP);
        var lblToggle = new kony.ui.Label({
            "centerX": "50%",
            "centerY": "50%",
            "height": "100%",
            "id": "lblToggle",
            "isVisible": true,
            "skin": "sknIcon12pxBlack",
            "i18n_text": "kony.i18n.getLocalizedString(\"i18n.userwidgetmodel.fontIconBreadcrumbsDown\")",
            "width": "100%",
            "zIndex": 1
        }, {
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, {});
        flxToggle.add(lblToggle);
        flxRow1.add(flxOptions, lblDisplayName, flxStatus, lblAlertType, lblCode, lblFreqEnabled, flxToggle);
        var flxTagsGroup = new kony.ui.FlexContainer({
            "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
            "clipBounds": true,
            "id": "flxTagsGroup",
            "isVisible": true,
            "layoutType": kony.flex.FLOW_HORIZONTAL,
            "left": "0dp",
            "isModalContainer": false,
            "skin": "slFbox",
            "top": "0dp",
            "width": "100%"
        }, {}, {});
        flxTagsGroup.setDefaultUnit(kony.flex.DP);
        var btnExt = new kony.ui.Button({
            "bottom": "0dp",
            "id": "btnExt",
            "isVisible": true,
            "left": "25dp",
            "skin": "sknbtnffffffLatoRegular12PxA94240",
            "text": "Button",
            "width": kony.flex.USE_PREFFERED_SIZE
        }, {
            "contentAlignment": constants.CONTENT_ALIGN_CENTER,
            "displayText": true,
            "padding": [1, 0, 1, 0],
            "paddingInPixel": false
        }, {});
        var btnAutoSubscribed = new kony.ui.Button({
            "bottom": "0dp",
            "id": "btnAutoSubscribed",
            "isVisible": true,
            "left": "5dp",
            "skin": "sknbtnffffffLatoRegularLightGreen",
            "text": "Auto Subscribed",
            "width": kony.flex.USE_PREFFERED_SIZE
        }, {
            "contentAlignment": constants.CONTENT_ALIGN_CENTER,
            "displayText": true,
            "padding": [1, 0, 1, 0],
            "paddingInPixel": false
        }, {});
        flxTagsGroup.add(btnExt, btnAutoSubscribed);
        var flxDesc = new kony.ui.FlexContainer({
            "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
            "clipBounds": true,
            "id": "flxDesc",
            "isVisible": true,
            "layoutType": kony.flex.FLOW_VERTICAL,
            "left": "0dp",
            "isModalContainer": false,
            "skin": "slFbox",
            "top": "0dp",
            "width": "100%"
        }, {}, {});
        flxDesc.setDefaultUnit(kony.flex.DP);
        var lblDescriptionTxt = new kony.ui.Label({
            "id": "lblDescriptionTxt",
            "isVisible": true,
            "left": "30px",
            "skin": "sknLbl485C75LatoRegular13Px",
            "text": "DESCRIPTION",
            "top": "10dp",
            "width": "36%",
            "zIndex": 1
        }, {
            "contentAlignment": constants.CONTENT_ALIGN_TOP_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, {});
        var lblDescription = new kony.ui.Label({
            "bottom": "5dp",
            "id": "lblDescription",
            "isVisible": true,
            "left": "30px",
            "skin": "sknLbl485C75LatoRegular13Px",
            "text": "Des",
            "top": "5dp",
            "width": "36%",
            "zIndex": 1
        }, {
            "contentAlignment": constants.CONTENT_ALIGN_TOP_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, {});
        flxDesc.add(lblDescriptionTxt, lblDescription);
        var lblSeperator = new kony.ui.Label({
            "bottom": 0,
            "height": "1px",
            "id": "lblSeperator",
            "isVisible": true,
            "left": "0dp",
            "skin": "sknlblSeperator",
            "text": ".",
            "width": "100%",
            "zIndex": 1
        }, {
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, {});
        flxSubAlerts.add(flxRow1, flxTagsGroup, flxDesc, lblSeperator);
        return flxSubAlerts;
    }
})