define("flxAddPermissions", function() {
    return function(controller) {
        var flxAddPermissions = new kony.ui.FlexContainer({
            "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
            "clipBounds": true,
            "id": "flxAddPermissions",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "isModalContainer": false,
            "skin": "slFbox"
        }, {}, {});
        flxAddPermissions.setDefaultUnit(kony.flex.DP);
        var flxAddWrapper = new kony.ui.FlexContainer({
            "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
            "clipBounds": true,
            "id": "flxAddWrapper",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "left": "10dp",
            "isModalContainer": false,
            "right": 20,
            "skin": "sknflxffffffOp100Border5e90cbRadius3Px",
            "top": "10dp",
            "zIndex": 1
        }, {}, {
            "hoverSkin": "blocksHover"
        });
        flxAddWrapper.setDefaultUnit(kony.flex.DP);
        var lblPermissionsName = new kony.ui.Label({
            "id": "lblPermissionsName",
            "isVisible": true,
            "left": "20px",
            "right": "55px",
            "skin": "sknlblLatoBold35475f14px",
            "text": "View Permission",
            "top": "15px",
            "zIndex": 1
        }, {
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, {});
        var btnAdd = new kony.ui.Button({
            "height": "20px",
            "id": "btnAdd",
            "isVisible": true,
            "onClick": controller.AS_Button_c900685a8a2046e9b7341cf39934bdec,
            "right": 20,
            "skin": "sknBtnLatoRegular11abeb12px",
            "i18n_text": "kony.i18n.getLocalizedString(\"i18n.DragBox.ADD\")",
            "top": "15dp",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, {
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_RIGHT,
            "displayText": true,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, {
            "hoverSkin": "sknBtnLatoRegular1682b212px"
        });
        var rtxPermissionDescription = new kony.ui.Label({
            "bottom": 15,
            "id": "rtxPermissionDescription",
            "isVisible": true,
            "left": "20px",
            "right": "40px",
            "skin": "sknlblLatoBold35475f14px",
            "text": "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.",
            "top": "40px",
            "zIndex": 1
        }, {
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, {});
        flxAddWrapper.add(lblPermissionsName, btnAdd, rtxPermissionDescription);
        flxAddPermissions.add(flxAddWrapper);
        return flxAddPermissions;
    }
})