define("flxAlertCategory", function() {
    return function(controller) {
        var flxAlertCategory = new kony.ui.FlexContainer({
            "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
            "clipBounds": true,
            "id": "flxAlertCategory",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "left": "0dp",
            "isModalContainer": false,
            "skin": "sknflxf9f9f9NoBorder",
            "top": "0dp",
            "width": "100%"
        }, {}, {
            "hoverSkin": "sknFlxBGF9F9F9Cursor"
        });
        flxAlertCategory.setDefaultUnit(kony.flex.DP);
        var lblAlertCategory = new kony.ui.Label({
            "bottom": "10px",
            "id": "lblAlertCategory",
            "isVisible": true,
            "left": "43px",
            "skin": "sknLblTabUtilRest",
            "text": "Label",
            "top": "7px",
            "width": "80%",
            "zIndex": 1
        }, {
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, {});
        var lblArrow = new kony.ui.Label({
            "bottom": "10px",
            "id": "lblArrow",
            "isVisible": true,
            "right": "0dp",
            "skin": "sknIcon485C7513px",
            "text": "",
            "top": "7px",
            "width": "30dp",
            "zIndex": 1
        }, {
            "contentAlignment": constants.CONTENT_ALIGN_TOP_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, {});
        flxAlertCategory.add(lblAlertCategory, lblArrow);
        return flxAlertCategory;
    }
})