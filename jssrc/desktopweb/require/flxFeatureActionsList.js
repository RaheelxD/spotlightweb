define("flxFeatureActionsList", function() {
    return function(controller) {
        var flxFeatureActionsList = new kony.ui.FlexContainer({
            "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
            "clipBounds": true,
            "id": "flxFeatureActionsList",
            "isVisible": true,
            "layoutType": kony.flex.FLOW_VERTICAL,
            "left": "0dp",
            "isModalContainer": false,
            "skin": "sknBackgroundFFFFFF",
            "top": "0dp",
            "width": "100%"
        }, {}, {
            "hoverSkin": "sknfbfcfc"
        });
        flxFeatureActionsList.setDefaultUnit(kony.flex.DP);
        var flxActionDetails = new kony.ui.FlexContainer({
            "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
            "clipBounds": true,
            "id": "flxActionDetails",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "left": "0dp",
            "isModalContainer": false,
            "skin": "slFbox",
            "top": "0dp",
            "width": "100%"
        }, {}, {});
        flxActionDetails.setDefaultUnit(kony.flex.DP);
        var flxArrow = new kony.ui.FlexContainer({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "clipBounds": true,
            "height": "25dp",
            "id": "flxArrow",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "left": "10dp",
            "isModalContainer": false,
            "skin": "slFbox",
            "top": "12dp",
            "width": "25dp",
            "zIndex": 1
        }, {}, {
            "hoverSkin": "sknCursor"
        });
        flxArrow.setDefaultUnit(kony.flex.DP);
        var lblArrow = new kony.ui.Label({
            "centerY": "50%",
            "id": "lblArrow",
            "isVisible": true,
            "left": "0dp",
            "skin": "sknIcon485C7513px",
            "text": "",
            "width": "20dp",
            "zIndex": 1
        }, {
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, {});
        flxArrow.add(lblArrow);
        var lblActionName = new kony.ui.Label({
            "id": "lblActionName",
            "isVisible": true,
            "left": "35dp",
            "skin": "sknLbl485C75LatoRegular13Px",
            "text": "Label",
            "top": "15dp",
            "width": "15%",
            "zIndex": 1
        }, {
            "contentAlignment": constants.CONTENT_ALIGN_TOP_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, {});
        var lblActionCode = new kony.ui.Label({
            "id": "lblActionCode",
            "isVisible": true,
            "left": "20%",
            "skin": "sknLbl485C75LatoRegular13Px",
            "text": "ACTION_CODE",
            "top": "15dp",
            "width": "17%",
            "zIndex": 1
        }, {
            "contentAlignment": constants.CONTENT_ALIGN_TOP_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, {});
        var lblActionType = new kony.ui.Label({
            "id": "lblActionType",
            "isVisible": true,
            "left": "39%",
            "skin": "sknLbl485C75LatoRegular13Px",
            "text": "Retail Banking",
            "top": "15dp",
            "width": "14%",
            "zIndex": 1
        }, {
            "contentAlignment": constants.CONTENT_ALIGN_TOP_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, {});
        var lblActionCategory = new kony.ui.Label({
            "id": "lblActionCategory",
            "isVisible": true,
            "left": "55%",
            "skin": "sknLbl485C75LatoRegular13Px",
            "text": "Non-monetaory",
            "top": "15dp",
            "width": "15%",
            "zIndex": 1
        }, {
            "contentAlignment": constants.CONTENT_ALIGN_TOP_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, {});
        var lblActionLimitGroup = new kony.ui.Label({
            "id": "lblActionLimitGroup",
            "isVisible": true,
            "left": "75%",
            "skin": "sknLbl485C75LatoRegular13Px",
            "text": "Label",
            "top": "15dp",
            "width": "10%",
            "zIndex": 1
        }, {
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, {});
        var flxStatus = new kony.ui.FlexContainer({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "clipBounds": true,
            "height": "50px",
            "id": "flxStatus",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "left": "89%",
            "isModalContainer": false,
            "skin": "slFbox",
            "top": "0dp",
            "width": "7%",
            "zIndex": 1
        }, {}, {});
        flxStatus.setDefaultUnit(kony.flex.DP);
        var lblActionStatus = new kony.ui.Label({
            "centerY": "48%",
            "id": "lblActionStatus",
            "isVisible": true,
            "left": "16px",
            "skin": "sknLbl485C75LatoRegular13Px",
            "text": "Active",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, {
            "contentAlignment": constants.CONTENT_ALIGN_TOP_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, {});
        var fontIconStatusImg = new kony.ui.Label({
            "centerY": "48%",
            "id": "fontIconStatusImg",
            "isVisible": true,
            "left": "0dp",
            "skin": "sknFontIconActivate",
            "text": "",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, {
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, {});
        flxStatus.add(lblActionStatus, fontIconStatusImg);
        var flxOptions = new kony.ui.FlexContainer({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "clipBounds": true,
            "height": "25dp",
            "id": "flxOptions",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "isModalContainer": false,
            "onClick": controller.AS_FlexContainer_a58f522d2cfc419c9a99c2cf77a878ac,
            "right": "10px",
            "skin": "slFbox",
            "top": "11dp",
            "width": "25px",
            "zIndex": 1
        }, {}, {
            "hoverSkin": "sknflxffffffop100Border424242Radius100px"
        });
        flxOptions.setDefaultUnit(kony.flex.DP);
        var lblIconImgOptions = new kony.ui.Label({
            "centerX": "50%",
            "centerY": "50%",
            "id": "lblIconImgOptions",
            "isVisible": true,
            "skin": "sknFontIconOptionMenu",
            "text": "",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, {
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, {});
        flxOptions.add(lblIconImgOptions);
        flxActionDetails.add(flxArrow, lblActionName, lblActionCode, lblActionType, lblActionCategory, lblActionLimitGroup, flxStatus, flxOptions);
        var flxActionDescription = new kony.ui.FlexContainer({
            "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
            "clipBounds": true,
            "id": "flxActionDescription",
            "isVisible": false,
            "layoutType": kony.flex.FREE_FORM,
            "left": "35dp",
            "isModalContainer": false,
            "skin": "slFbox",
            "top": "10dp",
            "width": "90%",
            "zIndex": 1
        }, {}, {});
        flxActionDescription.setDefaultUnit(kony.flex.DP);
        var lblDescriptionHeader = new kony.ui.Label({
            "id": "lblDescriptionHeader",
            "isVisible": true,
            "left": "0dp",
            "skin": "sknLbl78818A11px",
            "text": "DESCRIPTION",
            "top": "0dp",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, {
            "contentAlignment": constants.CONTENT_ALIGN_TOP_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, {});
        var lblDescriptionValue = new kony.ui.Label({
            "id": "lblDescriptionValue",
            "isVisible": true,
            "left": "0dp",
            "skin": "sknLbl485C75LatoRegular13Px",
            "text": "Label",
            "top": "20dp",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, {
            "contentAlignment": constants.CONTENT_ALIGN_TOP_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, {});
        flxActionDescription.add(lblDescriptionHeader, lblDescriptionValue);
        var lblSeperatorLine = new kony.ui.Label({
            "height": "1dp",
            "id": "lblSeperatorLine",
            "isVisible": true,
            "left": "0dp",
            "skin": "sknlblSeperatorBgD5D9DD",
            "text": "Label",
            "top": "15dp",
            "width": "100%",
            "zIndex": 1
        }, {
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, {});
        flxFeatureActionsList.add(flxActionDetails, flxActionDescription, lblSeperatorLine);
        return flxFeatureActionsList;
    }
})