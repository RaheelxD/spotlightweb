define("flxProductAddedFeatures", function() {
    return function(controller) {
        var flxProductAddedFeatures = new kony.ui.FlexContainer({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "clipBounds": true,
            "height": "45px",
            "id": "flxProductAddedFeatures",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "left": "0dp",
            "isModalContainer": false,
            "skin": "sknflxffffffop100",
            "top": "0dp",
            "width": "100%"
        }, {}, {});
        flxProductAddedFeatures.setDefaultUnit(kony.flex.DP);
        var lblSegFeatureName = new kony.ui.Label({
            "centerY": "50%",
            "id": "lblSegFeatureName",
            "isVisible": true,
            "left": "10px",
            "skin": "sknLbl485C75LatoRegular13Px",
            "text": "Feature 1",
            "width": "28%",
            "zIndex": 1
        }, {
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, {});
        var lblSegFeatureGroup = new kony.ui.Label({
            "centerY": "50%",
            "id": "lblSegFeatureGroup",
            "isVisible": true,
            "left": "33%",
            "skin": "sknLbl485C75LatoRegular13Px",
            "text": "KonySelect23",
            "width": "28%",
            "zIndex": 1
        }, {
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, {});
        var lblSegType = new kony.ui.Label({
            "centerY": "50%",
            "id": "lblSegType",
            "isVisible": true,
            "left": "65%",
            "skin": "sknLbl485C75LatoRegular13Px",
            "text": "Overdraft Protection",
            "width": "28%",
            "zIndex": 1
        }, {
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, {});
        var flxOptions = new kony.ui.FlexContainer({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "centerY": "50%",
            "clipBounds": true,
            "focusSkin": "sknflxffffffop100Border424242Radius100px",
            "height": "25px",
            "id": "flxOptions",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "isModalContainer": false,
            "right": "10px",
            "skin": "sknFlxBorffffff1pxRound",
            "width": "25px",
            "zIndex": 2
        }, {}, {
            "hoverSkin": "sknflxffffffop100Border424242Radius100px"
        });
        flxOptions.setDefaultUnit(kony.flex.DP);
        var lblIconOptions = new kony.ui.Label({
            "centerX": "50%",
            "centerY": "49%",
            "id": "lblIconOptions",
            "isVisible": true,
            "skin": "sknFontIconOptionMenu",
            "text": "",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, {
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, {});
        flxOptions.add(lblIconOptions);
        flxProductAddedFeatures.add(lblSegFeatureName, lblSegFeatureGroup, lblSegType, flxOptions);
        return flxProductAddedFeatures;
    }
})