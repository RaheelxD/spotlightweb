define("flxLimitGroup", function() {
    return function(controller) {
        var flxLimitGroup = new kony.ui.FlexContainer({
            "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
            "clipBounds": true,
            "id": "flxLimitGroup",
            "isVisible": true,
            "layoutType": kony.flex.FLOW_VERTICAL,
            "left": "0dp",
            "isModalContainer": false,
            "skin": "sknBackgroundFFFFFF",
            "top": "0dp",
            "width": "100%"
        }, {}, {});
        flxLimitGroup.setDefaultUnit(kony.flex.DP);
        var flxLimitGroupDetails = new kony.ui.FlexContainer({
            "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
            "clipBounds": true,
            "id": "flxLimitGroupDetails",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "left": "0dp",
            "isModalContainer": false,
            "skin": "slFbox",
            "top": "0dp",
            "width": "100%"
        }, {}, {});
        flxLimitGroupDetails.setDefaultUnit(kony.flex.DP);
        var flxArrow = new kony.ui.FlexContainer({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "clipBounds": true,
            "height": "25dp",
            "id": "flxArrow",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "left": "10dp",
            "isModalContainer": false,
            "skin": "slFbox",
            "top": "12dp",
            "width": "25dp",
            "zIndex": 1
        }, {}, {
            "hoverSkin": "sknCursor"
        });
        flxArrow.setDefaultUnit(kony.flex.DP);
        var lblArrow = new kony.ui.Label({
            "centerY": "50%",
            "id": "lblArrow",
            "isVisible": true,
            "left": "0dp",
            "skin": "sknIcon485C7513px",
            "text": "",
            "width": "20dp",
            "zIndex": 1
        }, {
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, {});
        flxArrow.add(lblArrow);
        var lblLimitGroupName = new kony.ui.Label({
            "id": "lblLimitGroupName",
            "isVisible": true,
            "left": "5%",
            "skin": "sknLbl485C75LatoRegular13Px",
            "text": "Label",
            "top": "15dp",
            "width": "15%",
            "zIndex": 1
        }, {
            "contentAlignment": constants.CONTENT_ALIGN_TOP_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, {});
        var lblLimitGroupCode = new kony.ui.Label({
            "id": "lblLimitGroupCode",
            "isVisible": true,
            "left": "40%",
            "skin": "sknLbl485C75LatoRegular13Px",
            "text": "LIMIT_GROUP_CODE",
            "top": "15dp",
            "width": "17%",
            "zIndex": 1
        }, {
            "contentAlignment": constants.CONTENT_ALIGN_TOP_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, {});
        var flxOptions = new kony.ui.FlexContainer({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "clipBounds": true,
            "height": "25dp",
            "id": "flxOptions",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "isModalContainer": false,
            "onClick": controller.AS_FlexContainer_a58f522d2cfc419c9a99c2cf77a878ac,
            "right": "10px",
            "skin": "slFbox",
            "top": "11dp",
            "width": "25px",
            "zIndex": 1
        }, {}, {
            "hoverSkin": "sknflxffffffop100Border424242Radius100px"
        });
        flxOptions.setDefaultUnit(kony.flex.DP);
        var lblIconImgOptions = new kony.ui.Label({
            "centerX": "50%",
            "centerY": "50%",
            "id": "lblIconImgOptions",
            "isVisible": true,
            "skin": "sknFontIconOptionMenu",
            "i18n_text": "kony.i18n.getLocalizedString(\"i18n.userwidgetmodel.lblIconOption1\")",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, {
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, {});
        flxOptions.add(lblIconImgOptions);
        flxLimitGroupDetails.add(flxArrow, lblLimitGroupName, lblLimitGroupCode, flxOptions);
        var flxDescription = new kony.ui.FlexContainer({
            "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
            "clipBounds": true,
            "id": "flxDescription",
            "isVisible": false,
            "layoutType": kony.flex.FREE_FORM,
            "left": "5%",
            "isModalContainer": false,
            "skin": "slFbox",
            "top": "10dp",
            "width": "90%",
            "zIndex": 1
        }, {}, {});
        flxDescription.setDefaultUnit(kony.flex.DP);
        var lblDescriptionHeader = new kony.ui.Label({
            "id": "lblDescriptionHeader",
            "isVisible": true,
            "left": "0dp",
            "skin": "sknLbl78818A11px",
            "text": "DESCRIPTION",
            "top": "0dp",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, {
            "contentAlignment": constants.CONTENT_ALIGN_TOP_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, {});
        var lblDescriptionValue = new kony.ui.Label({
            "id": "lblDescriptionValue",
            "isVisible": true,
            "left": "0dp",
            "skin": "sknLbl485C75LatoRegular13Px",
            "text": "Label",
            "top": "20dp",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, {
            "contentAlignment": constants.CONTENT_ALIGN_TOP_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, {});
        flxDescription.add(lblDescriptionHeader, lblDescriptionValue);
        var lblSeperatorLine = new kony.ui.Label({
            "height": "1dp",
            "id": "lblSeperatorLine",
            "isVisible": true,
            "left": "0dp",
            "skin": "sknlblSeperatorBgD5D9DD",
            "text": "Label",
            "top": "15dp",
            "width": "100%",
            "zIndex": 1
        }, {
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, {});
        flxLimitGroup.add(flxLimitGroupDetails, flxDescription, lblSeperatorLine);
        return flxLimitGroup;
    }
})