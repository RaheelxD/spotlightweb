define("flxOutage", function() {
    return function(controller) {
        var flxOutage = new kony.ui.FlexContainer({
            "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
            "clipBounds": true,
            "focusSkin": "sknflxffffffop100",
            "id": "flxOutage",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "isModalContainer": false,
            "skin": "sknFlxffffffCursorPointer"
        }, {}, {
            "hoverSkin": "sknfbfcfc"
        });
        flxOutage.setDefaultUnit(kony.flex.DP);
        var flxOutageLeft = new kony.ui.FlexContainer({
            "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
            "clipBounds": true,
            "id": "flxOutageLeft",
            "isVisible": true,
            "layoutType": kony.flex.FLOW_HORIZONTAL,
            "left": "0dp",
            "isModalContainer": false,
            "skin": "slFbox",
            "top": "0dp",
            "width": "28%",
            "zIndex": 2
        }, {}, {});
        flxOutageLeft.setDefaultUnit(kony.flex.DP);
        var flxOutageCheckbox = new kony.ui.FlexContainer({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "clipBounds": true,
            "height": "30px",
            "id": "flxOutageCheckbox",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "left": "35px",
            "isModalContainer": false,
            "onClick": controller.AS_FlexContainer_b067d1d36a774c238383da14ae6d4be9,
            "skin": "slFbox",
            "top": "20px",
            "width": "30px",
            "zIndex": 2
        }, {}, {
            "hoverSkin": "sknCursor"
        });
        flxOutageCheckbox.setDefaultUnit(kony.flex.DP);
        var imgCheckBox = new kony.ui.Image2({
            "centerX": "50%",
            "height": "12px",
            "id": "imgCheckBox",
            "isVisible": true,
            "skin": "slImage",
            "src": "checkbox.png",
            "top": 0,
            "width": "12px",
            "zIndex": 2
        }, {
            "imageScaleMode": constants.IMAGE_SCALE_MODE_MAINTAIN_ASPECT_RATIO,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, {});
        flxOutageCheckbox.add(imgCheckBox);
        var lblOutageServiceName = new kony.ui.Label({
            "bottom": "10px",
            "id": "lblOutageServiceName",
            "isVisible": true,
            "left": "55px",
            "skin": "sknlblLatoBold35475f14px",
            "text": "Jompay",
            "top": "20px",
            "width": "45%",
            "zIndex": 1
        }, {
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, {});
        flxOutageLeft.add(flxOutageCheckbox, lblOutageServiceName);
        var lblDescription = new kony.ui.Label({
            "bottom": "20px",
            "id": "lblDescription",
            "isVisible": true,
            "left": "30.20%",
            "skin": "sknlblLatoBold35475f14px",
            "i18n_text": "kony.i18n.getLocalizedString(\"i18n.OutageMessage.MessageDescription\")",
            "top": "20px",
            "width": "40%",
            "zIndex": 1
        }, {
            "contentAlignment": constants.CONTENT_ALIGN_TOP_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, {});
        var flxStatus = new kony.ui.FlexContainer({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "clipBounds": true,
            "height": "20px",
            "id": "flxStatus",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "isModalContainer": false,
            "right": "100px",
            "skin": "slFbox",
            "top": "20px",
            "width": "92px",
            "zIndex": 1
        }, {}, {});
        flxStatus.setDefaultUnit(kony.flex.DP);
        var lblIconStatus = new kony.ui.Label({
            "centerY": "50%",
            "id": "lblIconStatus",
            "isVisible": true,
            "left": "0dp",
            "skin": "sknFontIconActivate",
            "text": "",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, {
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, {});
        var lblServiceStatus = new kony.ui.Label({
            "centerY": "50%",
            "id": "lblServiceStatus",
            "isVisible": true,
            "left": "20px",
            "skin": "sknlblLato5bc06cBold14px",
            "text": "Active",
            "width": "45px",
            "zIndex": 1
        }, {
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, {});
        flxStatus.add(lblIconStatus, lblServiceStatus);
        var flxOptions = new kony.ui.FlexContainer({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "clipBounds": true,
            "height": "25px",
            "id": "flxOptions",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "isModalContainer": false,
            "onClick": controller.AS_FlexContainer_ba9fe5ae8e9f4d3091e928faa2da885e,
            "right": "35dp",
            "skin": "slFbox",
            "top": "20px",
            "width": "25px",
            "zIndex": 2
        }, {}, {
            "hoverSkin": "sknflxffffffop100Border424242Radius100px"
        });
        flxOptions.setDefaultUnit(kony.flex.DP);
        var lblOptionsIcon = new kony.ui.Label({
            "centerX": "50%",
            "centerY": "50%",
            "height": "12dp",
            "id": "lblOptionsIcon",
            "isVisible": true,
            "left": "14dp",
            "skin": "sknFontIconOptionMenu",
            "text": "",
            "width": "6dp",
            "zIndex": 10
        }, {
            "contentAlignment": constants.CONTENT_ALIGN_CENTER,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, {});
        flxOptions.add(lblOptionsIcon);
        var lblSeparator = new kony.ui.Label({
            "bottom": "0px",
            "centerX": "50%",
            "height": "1dp",
            "id": "lblSeparator",
            "isVisible": true,
            "left": "0dp",
            "skin": "sknlblSeperator",
            "text": "Label",
            "width": "96%",
            "zIndex": 2
        }, {
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, {});
        flxOutage.add(flxOutageLeft, lblDescription, flxStatus, flxOptions, lblSeparator);
        return flxOutage;
    }
})