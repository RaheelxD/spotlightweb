define("flxEventsDataSet", function() {
    return function(controller) {
        var flxEventsDataSet = new kony.ui.FlexContainer({
            "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
            "clipBounds": true,
            "id": "flxEventsDataSet",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "left": "0dp",
            "isModalContainer": false,
            "skin": "sknBackgroundFFFFFF",
            "top": "0dp",
            "width": "100%"
        }, {}, {});
        flxEventsDataSet.setDefaultUnit(kony.flex.DP);
        var flxSelectOption = new kony.ui.FlexContainer({
            "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
            "clipBounds": true,
            "id": "flxSelectOption",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "left": "10dp",
            "isModalContainer": false,
            "skin": "hoverhandSkin",
            "top": "12dp",
            "width": "25dp"
        }, {}, {});
        flxSelectOption.setDefaultUnit(kony.flex.DP);
        var fontIconSelectOption = new kony.ui.Label({
            "id": "fontIconSelectOption",
            "isVisible": true,
            "left": "0dp",
            "skin": "sknFontIconCheckBoxSelected",
            "text": "",
            "top": "0dp",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, {
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, {});
        flxSelectOption.add(fontIconSelectOption);
        var lblName = new kony.ui.Label({
            "bottom": "15dp",
            "id": "lblName",
            "isVisible": true,
            "left": "42dp",
            "skin": "sknLbl485C75LatoRegular13Px",
            "text": "Name",
            "top": "15dp",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, {
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, {});
        var lblType = new kony.ui.Label({
            "bottom": "15dp",
            "id": "lblType",
            "isVisible": true,
            "left": "39%",
            "skin": "sknLbl485C75LatoRegular13Px",
            "text": "Type",
            "top": "15dp",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, {
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, {});
        var lblIdentifier = new kony.ui.Label({
            "bottom": "15dp",
            "id": "lblIdentifier",
            "isVisible": true,
            "left": "80%",
            "skin": "sknLbl485C75LatoRegular13Px",
            "text": "Identifier",
            "top": "15dp",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, {
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, {});
        var lblDate = new kony.ui.Label({
            "bottom": "15dp",
            "id": "lblDate",
            "isVisible": false,
            "left": "80%",
            "skin": "sknLbl485C75LatoRegular13Px",
            "text": "Date",
            "top": "15dp",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, {
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, {});
        var lblSeparator = new kony.ui.Label({
            "bottom": "0dp",
            "height": "1px",
            "id": "lblSeparator",
            "isVisible": true,
            "left": "0dp",
            "right": "0dp",
            "skin": "sknlblSeperatorD7D9E0",
            "text": "Label",
            "zIndex": 2
        }, {
            "contentAlignment": constants.CONTENT_ALIGN_TOP_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, {});
        flxEventsDataSet.add(flxSelectOption, lblName, lblType, lblIdentifier, lblDate, lblSeparator);
        return flxEventsDataSet;
    }
})