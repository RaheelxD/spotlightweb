define("flxServices2", function() {
    return function(controller) {
        var flxServices2 = new kony.ui.FlexContainer({
            "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
            "clipBounds": true,
            "id": "flxServices2",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "isModalContainer": false,
            "skin": "sknflxffffffop100"
        }, {}, {
            "hoverSkin": "sknfbfcfc"
        });
        flxServices2.setDefaultUnit(kony.flex.DP);
        var lblServicesName = new kony.ui.Label({
            "bottom": "15dp",
            "id": "lblServicesName",
            "isVisible": true,
            "left": "30dp",
            "skin": "sknlblLatoBold35475f14px",
            "text": "Name",
            "top": "15dp",
            "width": "130dp",
            "zIndex": 1
        }, {
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, {});
        var lblServicesCode = new kony.ui.Label({
            "bottom": "15dp",
            "id": "lblServicesCode",
            "isVisible": true,
            "left": "21.90%",
            "skin": "sknlblLatoBold35475f14px",
            "text": "Code",
            "top": "15dp",
            "width": "100dp",
            "zIndex": 1
        }, {
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, {});
        var lblServicesType = new kony.ui.Label({
            "bottom": "15dp",
            "id": "lblServicesType",
            "isVisible": true,
            "left": "35%",
            "skin": "sknlblLatoBold35475f14px",
            "text": "Type",
            "top": "15dp",
            "width": "90dp",
            "zIndex": 1
        }, {
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, {});
        var lblServicesCategory = new kony.ui.Label({
            "bottom": "15dp",
            "id": "lblServicesCategory",
            "isVisible": true,
            "left": "47%",
            "skin": "sknlblLatoBold35475f14px",
            "text": "Category",
            "top": "15dp",
            "width": "130dp",
            "zIndex": 1
        }, {
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, {});
        var lblServicesSupportedChannels = new kony.ui.Label({
            "bottom": "15dp",
            "id": "lblServicesSupportedChannels",
            "isVisible": true,
            "left": "61.70%",
            "skin": "sknlblLatoBold35475f14px",
            "text": "Supported Channels",
            "top": "15dp",
            "width": "180dp",
            "zIndex": 1
        }, {
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, {});
        var flxStatus = new kony.ui.FlexContainer({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "clipBounds": true,
            "height": "20px",
            "id": "flxStatus",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "left": "84%",
            "isModalContainer": false,
            "skin": "slFbox",
            "top": "13dp",
            "width": "8%",
            "zIndex": 1
        }, {}, {});
        flxStatus.setDefaultUnit(kony.flex.DP);
        var lblServicesStatus = new kony.ui.Label({
            "centerY": "50%",
            "id": "lblServicesStatus",
            "isVisible": true,
            "left": "16px",
            "skin": "sknlblLato5bc06cBold14px",
            "text": "Active",
            "width": "45px",
            "zIndex": 1
        }, {
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, {});
        var lblIconStatus = new kony.ui.Label({
            "centerY": "51%",
            "height": "15dp",
            "id": "lblIconStatus",
            "isVisible": true,
            "left": "0dp",
            "skin": "sknIcon13pxGreen",
            "text": "",
            "width": "15dp",
            "zIndex": 1
        }, {
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, {});
        flxStatus.add(lblServicesStatus, lblIconStatus);
        var flxOptions = new kony.ui.FlexContainer({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "clipBounds": true,
            "height": "27dp",
            "id": "flxOptions",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "left": "93%",
            "isModalContainer": false,
            "onClick": controller.AS_FlexContainer_e4c339e0e0b941679ef3bc3986fa22a1,
            "skin": "sknFlxBorffffff1pxRound",
            "top": "10dp",
            "width": "27px",
            "zIndex": 2
        }, {}, {
            "hoverSkin": "sknflxffffffop100Border424242Radius100px"
        });
        flxOptions.setDefaultUnit(kony.flex.DP);
        var lblOptions = new kony.ui.Label({
            "centerX": "46%",
            "centerY": "46%",
            "height": "15dp",
            "id": "lblOptions",
            "isVisible": true,
            "skin": "sknFontIconOptionMenu",
            "text": "",
            "width": "15dp",
            "zIndex": 1
        }, {
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, {});
        flxOptions.add(lblOptions);
        var lblSeperator = new kony.ui.Label({
            "bottom": 0,
            "height": "1px",
            "id": "lblSeperator",
            "isVisible": true,
            "left": "20dp",
            "right": "35dp",
            "skin": "sknlblSeperator",
            "text": ".",
            "zIndex": 1
        }, {
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, {});
        flxServices2.add(lblServicesName, lblServicesCode, lblServicesType, lblServicesCategory, lblServicesSupportedChannels, flxStatus, flxOptions, lblSeperator);
        return flxServices2;
    }
})