define("userflxConfigurationWithDescriptionController", {
    hide: function() {
        this.executeOnParent("toggleVisibility");
    }
});
define("flxConfigurationWithDescriptionControllerActions", {
    /*
        This is an auto generated file and any modifications to it may result in corruption of the action sequence.
    */
});
define("flxConfigurationWithDescriptionController", ["userflxConfigurationWithDescriptionController", "flxConfigurationWithDescriptionControllerActions"], function() {
    var controller = require("userflxConfigurationWithDescriptionController");
    var controllerActions = ["flxConfigurationWithDescriptionControllerActions"];
    return kony.visualizer.mixinControllerActions(controller, controllerActions);
});
