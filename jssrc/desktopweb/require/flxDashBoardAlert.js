define("flxDashBoardAlert", function() {
    return function(controller) {
        var flxDashBoardAlert = new kony.ui.FlexContainer({
            "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
            "clipBounds": true,
            "id": "flxDashBoardAlert",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "left": "0dp",
            "isModalContainer": false,
            "skin": "slFbox",
            "top": "0dp",
            "width": "100%"
        }, {}, {});
        flxDashBoardAlert.setDefaultUnit(kony.flex.DP);
        var flxVariable = new kony.ui.FlexContainer({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "clipBounds": true,
            "height": "50px",
            "id": "flxVariable",
            "isVisible": true,
            "layoutType": kony.flex.FLOW_HORIZONTAL,
            "left": "0%",
            "isModalContainer": false,
            "skin": "slFbox",
            "top": "0dp",
            "width": "100%",
            "zIndex": 1
        }, {}, {});
        flxVariable.setDefaultUnit(kony.flex.DP);
        var lblTitle = new kony.ui.Label({
            "id": "lblTitle",
            "isVisible": true,
            "left": "20px",
            "skin": "sknlblLatoreg485c7515px",
            "text": "Scheduled Maintainance",
            "top": "20px",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, {
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, {});
        var lblPriority = new kony.ui.Label({
            "id": "lblPriority",
            "isVisible": true,
            "left": "0.50%",
            "skin": "sknLblIcomoonMed",
            "text": "",
            "top": "18px",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, {
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, {});
        var lblCategory = new kony.ui.Label({
            "id": "lblCategory",
            "isVisible": true,
            "left": "0.50%",
            "skin": "sknLblLatoReg12px8080dfRad3px",
            "text": "EMPLOYEE",
            "top": "18px",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, {
            "contentAlignment": constants.CONTENT_ALIGN_CENTER,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, {});
        flxVariable.add(lblTitle, lblPriority, lblCategory);
        var flxSeprator = new kony.ui.FlexContainer({
            "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
            "clipBounds": true,
            "id": "flxSeprator",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "left": "20px",
            "isModalContainer": false,
            "right": "20px",
            "skin": "sknFlxSeparoterUp1pxf0eff4",
            "top": "52px",
            "zIndex": 1
        }, {}, {});
        flxSeprator.setDefaultUnit(kony.flex.DP);
        var lblSubTitle = new kony.ui.Label({
            "bottom": "18px",
            "id": "lblSubTitle",
            "isVisible": true,
            "left": "0px",
            "skin": "sknllbl485c75Lato13px",
            "text": "Scheduled maintenance of the admin console is coming up on 31st Mar, 2018. Please ensure all pending messages are responded to correctly in accordance with the SLA",
            "top": "0px",
            "width": "100%",
            "zIndex": 1
        }, {
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, {});
        flxSeprator.add(lblSubTitle);
        flxDashBoardAlert.add(flxVariable, flxSeprator);
        return flxDashBoardAlert;
    }
})