define("flxListOfApplicationsSubmittedHeader", function() {
    return function(controller) {
        var flxListOfApplicationsSubmittedHeader = new kony.ui.FlexContainer({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "clipBounds": true,
            "height": "60dp",
            "id": "flxListOfApplicationsSubmittedHeader",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "left": "0dp",
            "isModalContainer": false,
            "skin": "sknFlxFFFFFF1000",
            "top": "0dp",
            "width": "100%"
        }, {}, {});
        flxListOfApplicationsSubmittedHeader.setDefaultUnit(kony.flex.DP);
        var flxApplicationIDContainer = new kony.ui.FlexContainer({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "clipBounds": true,
            "height": "30dp",
            "id": "flxApplicationIDContainer",
            "isVisible": true,
            "layoutType": kony.flex.FLOW_HORIZONTAL,
            "left": "20dp",
            "isModalContainer": false,
            "skin": "slFbox",
            "top": "14dp",
            "width": "15%",
            "zIndex": 1
        }, {}, {});
        flxApplicationIDContainer.setDefaultUnit(kony.flex.DP);
        var lblApplicationIDHeader = new kony.ui.Label({
            "id": "lblApplicationIDHeader",
            "isVisible": true,
            "left": "0dp",
            "skin": "sknLbl696c73LatoBold",
            "top": "15dp",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, {
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, {
            "hoverSkin": "sknLbl000000"
        });
        var lblApplicationIDHeaderImg = new kony.ui.Label({
            "id": "lblApplicationIDHeaderImg",
            "isVisible": true,
            "left": "5dp",
            "skin": "sknLblApplicationListIconSort",
            "text": "",
            "top": "15dp",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, {
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, {});
        flxApplicationIDContainer.add(lblApplicationIDHeader, lblApplicationIDHeaderImg);
        var flxLoanTypeContainer = new kony.ui.FlexContainer({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "clipBounds": true,
            "height": "30dp",
            "id": "flxLoanTypeContainer",
            "isVisible": true,
            "layoutType": kony.flex.FLOW_HORIZONTAL,
            "left": "150dp",
            "isModalContainer": false,
            "skin": "slFbox",
            "top": "14dp",
            "width": "12%",
            "zIndex": 1
        }, {}, {});
        flxLoanTypeContainer.setDefaultUnit(kony.flex.DP);
        var lblLoanTypeHeader = new kony.ui.Label({
            "id": "lblLoanTypeHeader",
            "isVisible": true,
            "left": "0dp",
            "skin": "sknLbl696c73LatoBold",
            "top": "15dp",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, {
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, {
            "hoverSkin": "sknLbl000000"
        });
        var lblLoanTypeHeaderImg = new kony.ui.Label({
            "id": "lblLoanTypeHeaderImg",
            "isVisible": true,
            "left": "5dp",
            "skin": "sknLblApplicationListIconSort",
            "text": "",
            "top": "15dp",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, {
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, {});
        flxLoanTypeContainer.add(lblLoanTypeHeader, lblLoanTypeHeaderImg);
        var flxAmountContainer = new kony.ui.FlexContainer({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "clipBounds": true,
            "height": "30dp",
            "id": "flxAmountContainer",
            "isVisible": true,
            "layoutType": kony.flex.FLOW_HORIZONTAL,
            "left": "280dp",
            "isModalContainer": false,
            "skin": "slFbox",
            "top": "14dp",
            "width": "12%",
            "zIndex": 1
        }, {}, {});
        flxAmountContainer.setDefaultUnit(kony.flex.DP);
        var lblAmountHeader = new kony.ui.Label({
            "id": "lblAmountHeader",
            "isVisible": true,
            "left": "0dp",
            "skin": "sknLbl696c73LatoBold",
            "top": "15dp",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, {
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, {
            "hoverSkin": "sknLbl000000"
        });
        var lblAmountHeaderImg = new kony.ui.Label({
            "id": "lblAmountHeaderImg",
            "isVisible": true,
            "left": "5dp",
            "skin": "sknLblApplicationListIconSort",
            "text": "",
            "top": "15dp",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, {
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, {});
        flxAmountContainer.add(lblAmountHeader, lblAmountHeaderImg);
        var flxSubmitOnContainer = new kony.ui.FlexContainer({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "clipBounds": true,
            "height": "30dp",
            "id": "flxSubmitOnContainer",
            "isVisible": true,
            "layoutType": kony.flex.FLOW_HORIZONTAL,
            "left": "410dp",
            "isModalContainer": false,
            "skin": "slFbox",
            "top": "14dp",
            "width": "15%",
            "zIndex": 1
        }, {}, {});
        flxSubmitOnContainer.setDefaultUnit(kony.flex.DP);
        var lblSubmitOnHeader = new kony.ui.Label({
            "id": "lblSubmitOnHeader",
            "isVisible": true,
            "left": "0dp",
            "skin": "sknLbl696c73LatoBold",
            "top": "15dp",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, {
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, {
            "hoverSkin": "sknLbl000000"
        });
        var lblSubmitOnHeaderImg = new kony.ui.Label({
            "id": "lblSubmitOnHeaderImg",
            "isVisible": true,
            "left": "5dp",
            "skin": "sknLblApplicationListIconSort",
            "text": "",
            "top": "15dp",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, {
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, {});
        flxSubmitOnContainer.add(lblSubmitOnHeader, lblSubmitOnHeaderImg);
        var flxCloseDateContainer = new kony.ui.FlexContainer({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "clipBounds": true,
            "height": "30dp",
            "id": "flxCloseDateContainer",
            "isVisible": true,
            "layoutType": kony.flex.FLOW_HORIZONTAL,
            "left": "540dp",
            "isModalContainer": false,
            "skin": "slFbox",
            "top": "14dp",
            "width": "17%",
            "zIndex": 1
        }, {}, {});
        flxCloseDateContainer.setDefaultUnit(kony.flex.DP);
        var lblCloseDateHeader = new kony.ui.Label({
            "id": "lblCloseDateHeader",
            "isVisible": true,
            "left": "0dp",
            "skin": "sknLbl696c73LatoBold",
            "top": "15dp",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, {
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, {
            "hoverSkin": "sknLbl000000"
        });
        var lblCloseDateHeaderImg = new kony.ui.Label({
            "id": "lblCloseDateHeaderImg",
            "isVisible": true,
            "left": "5dp",
            "skin": "sknLblApplicationListIconSort",
            "text": "",
            "top": "15dp",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, {
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, {});
        flxCloseDateContainer.add(lblCloseDateHeader, lblCloseDateHeaderImg);
        var flxStatusContainer = new kony.ui.FlexContainer({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "clipBounds": true,
            "height": "30dp",
            "id": "flxStatusContainer",
            "isVisible": true,
            "layoutType": kony.flex.FLOW_HORIZONTAL,
            "left": "670dp",
            "isModalContainer": false,
            "skin": "slFbox",
            "top": "14dp",
            "width": "15%",
            "zIndex": 1
        }, {}, {});
        flxStatusContainer.setDefaultUnit(kony.flex.DP);
        var lblStatus = new kony.ui.Label({
            "id": "lblStatus",
            "isVisible": true,
            "left": "0dp",
            "skin": "sknLbl696c73LatoBold",
            "top": "15dp",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, {
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, {
            "hoverSkin": "sknLbl000000"
        });
        var lblStatusImg = new kony.ui.Label({
            "id": "lblStatusImg",
            "isVisible": true,
            "left": "5dp",
            "skin": "sknLblApplicationListIconSort",
            "text": "",
            "top": "15dp",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, {
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, {});
        flxStatusContainer.add(lblStatus, lblStatusImg);
        var lblSeparatorApplicationHeader = new kony.ui.Label({
            "bottom": "0dp",
            "height": "1dp",
            "id": "lblSeparatorApplicationHeader",
            "isVisible": true,
            "left": "22dp",
            "right": "22dp",
            "skin": "sknLblSeparator696C73",
            "zIndex": 1
        }, {
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, {});
        flxListOfApplicationsSubmittedHeader.add(flxApplicationIDContainer, flxLoanTypeContainer, flxAmountContainer, flxSubmitOnContainer, flxCloseDateContainer, flxStatusContainer, lblSeparatorApplicationHeader);
        return flxListOfApplicationsSubmittedHeader;
    }
})