define("flxEnrollCustomerSearchResult", function() {
    return function(controller) {
        var flxEnrollCustomerSearchResult = new kony.ui.FlexContainer({
            "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
            "clipBounds": true,
            "id": "flxEnrollCustomerSearchResult",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "left": "0dp",
            "isModalContainer": false,
            "skin": "slFbox",
            "top": "0dp",
            "width": "100%"
        }, {}, {});
        flxEnrollCustomerSearchResult.setDefaultUnit(kony.flex.DP);
        var flxSearchResultCont = new kony.ui.FlexContainer({
            "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
            "clipBounds": true,
            "id": "flxSearchResultCont",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "left": "20dp",
            "isModalContainer": false,
            "right": "20dp",
            "skin": "sknflxffffffop100",
            "top": "0"
        }, {}, {
            "hoverSkin": "sknfbfcfc"
        });
        flxSearchResultCont.setDefaultUnit(kony.flex.DP);
        var lblSearchSegHeaderCustId = new kony.ui.Label({
            "bottom": "15dp",
            "id": "lblSearchSegHeaderCustId",
            "isVisible": true,
            "left": "10px",
            "skin": "sknLbl485C75LatoRegular13Px",
            "text": "Label",
            "top": "15px",
            "width": "22%",
            "zIndex": 100
        }, {
            "contentAlignment": constants.CONTENT_ALIGN_TOP_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, {});
        var lblSearchSegHeaderCustName = new kony.ui.Label({
            "bottom": "15dp",
            "id": "lblSearchSegHeaderCustName",
            "isVisible": true,
            "left": "28%",
            "skin": "sknLbl485C75LatoRegular13Px",
            "text": "Label",
            "top": "15px",
            "width": "22%",
            "zIndex": 1
        }, {
            "contentAlignment": constants.CONTENT_ALIGN_TOP_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, {});
        var lblSearchSegHeaderEmail = new kony.ui.Label({
            "bottom": "15dp",
            "id": "lblSearchSegHeaderEmail",
            "isVisible": true,
            "left": "55%",
            "skin": "sknLbl485C75LatoRegular13Px",
            "text": "Label",
            "top": "15px",
            "width": "25%",
            "zIndex": 1
        }, {
            "contentAlignment": constants.CONTENT_ALIGN_TOP_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, {});
        var lblSearchSegHeaderPhoneNum = new kony.ui.Label({
            "bottom": "15dp",
            "id": "lblSearchSegHeaderPhoneNum",
            "isVisible": true,
            "left": "82%",
            "right": "20dp",
            "skin": "sknLbl485C75LatoRegular13Px",
            "text": "Label",
            "top": "15dp",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, {
            "contentAlignment": constants.CONTENT_ALIGN_TOP_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, {});
        var lblSeperator = new kony.ui.Label({
            "bottom": 0,
            "height": "1px",
            "id": "lblSeperator",
            "isVisible": true,
            "left": "0dp",
            "right": "0dp",
            "skin": "sknlblSeperatorD7D9E0",
            "text": ".",
            "zIndex": 1
        }, {
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, {});
        flxSearchResultCont.add(lblSearchSegHeaderCustId, lblSearchSegHeaderCustName, lblSearchSegHeaderEmail, lblSearchSegHeaderPhoneNum, lblSeperator);
        flxEnrollCustomerSearchResult.add(flxSearchResultCont);
        return flxEnrollCustomerSearchResult;
    }
})