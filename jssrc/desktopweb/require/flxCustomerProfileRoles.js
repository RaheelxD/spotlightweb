define("flxCustomerProfileRoles", function() {
    return function(controller) {
        var flxCustomerProfileRoles = new kony.ui.FlexContainer({
            "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
            "clipBounds": true,
            "id": "flxCustomerProfileRoles",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "left": "0dp",
            "isModalContainer": false,
            "skin": "slFbox",
            "top": "0dp",
            "width": "100%"
        }, {}, {});
        flxCustomerProfileRoles.setDefaultUnit(kony.flex.DP);
        var flxRoleNameContainer = new kony.ui.FlexContainer({
            "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
            "clipBounds": true,
            "id": "flxRoleNameContainer",
            "isVisible": true,
            "layoutType": kony.flex.FLOW_HORIZONTAL,
            "left": "20dp",
            "isModalContainer": false,
            "right": "20px",
            "skin": "sknflxffffffBorderE1E5EERadius3px",
            "top": "12dp",
            "zIndex": 3
        }, {}, {});
        flxRoleNameContainer.setDefaultUnit(kony.flex.DP);
        var flxRoleCheckbox = new kony.ui.FlexContainer({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "clipBounds": true,
            "height": "20dp",
            "id": "flxRoleCheckbox",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "left": "8dp",
            "isModalContainer": false,
            "skin": "slFbox",
            "top": "8dp",
            "width": "20dp",
            "zIndex": 1
        }, {}, {
            "hoverSkin": "sknFlxPointer"
        });
        flxRoleCheckbox.setDefaultUnit(kony.flex.DP);
        var imgRoleCheckbox = new kony.ui.Image2({
            "centerX": "50%",
            "centerY": "50%",
            "height": "15dp",
            "id": "imgRoleCheckbox",
            "isVisible": true,
            "skin": "slImage",
            "src": "checkboxnormal.png",
            "width": "15dp",
            "zIndex": 1
        }, {
            "imageScaleMode": constants.IMAGE_SCALE_MODE_MAINTAIN_ASPECT_RATIO,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, {});
        flxRoleCheckbox.add(imgRoleCheckbox);
        var flxRoleRadio = new kony.ui.FlexContainer({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "clipBounds": true,
            "height": "20dp",
            "id": "flxRoleRadio",
            "isVisible": false,
            "layoutType": kony.flex.FREE_FORM,
            "left": "8dp",
            "isModalContainer": false,
            "skin": "slFbox",
            "top": "7dp",
            "width": "20dp",
            "zIndex": 1
        }, {}, {
            "hoverSkin": "sknFlxPointer"
        });
        flxRoleRadio.setDefaultUnit(kony.flex.DP);
        var imgRoleRadio = new kony.ui.Image2({
            "centerX": "50%",
            "centerY": "50%",
            "height": "15dp",
            "id": "imgRoleRadio",
            "isVisible": true,
            "skin": "slImage",
            "src": "radio_notselected.png",
            "width": "15dp",
            "zIndex": 1
        }, {
            "imageScaleMode": constants.IMAGE_SCALE_MODE_MAINTAIN_ASPECT_RATIO,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, {});
        flxRoleRadio.add(imgRoleRadio);
        var flxRoleInfo = new kony.ui.FlexContainer({
            "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
            "clipBounds": true,
            "id": "flxRoleInfo",
            "isVisible": true,
            "layoutType": kony.flex.FLOW_VERTICAL,
            "left": "15dp",
            "isModalContainer": false,
            "skin": "slFbox",
            "top": "8dp",
            "width": "95%"
        }, {}, {});
        flxRoleInfo.setDefaultUnit(kony.flex.DP);
        var flxRoleName = new kony.ui.FlexContainer({
            "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
            "clipBounds": true,
            "id": "flxRoleName",
            "isVisible": true,
            "layoutType": kony.flex.FLOW_HORIZONTAL,
            "left": "0dp",
            "isModalContainer": false,
            "skin": "slFbox",
            "top": "0dp",
            "width": "100%"
        }, {}, {});
        flxRoleName.setDefaultUnit(kony.flex.DP);
        var lblRoleName = new kony.ui.Label({
            "id": "lblRoleName",
            "isVisible": true,
            "left": "0dp",
            "right": "0dp",
            "skin": "sknLbl16pxLato192B45",
            "text": "Bill Payments",
            "top": "0dp",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, {
            "contentAlignment": constants.CONTENT_ALIGN_TOP_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, {});
        var flxDefaultRoleButton = new kony.ui.Button({
            "height": "20px",
            "id": "flxDefaultRoleButton",
            "isVisible": false,
            "left": "20dp",
            "skin": "sknbg1f844dborrad16pxffffff",
            "text": "Default Role",
            "top": "0px",
            "width": "86dp",
            "zIndex": 1
        }, {
            "contentAlignment": constants.CONTENT_ALIGN_CENTER,
            "displayText": true,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, {});
        flxRoleName.add(lblRoleName, flxDefaultRoleButton);
        var lblRoleDesc = new kony.ui.Label({
            "bottom": "10dp",
            "id": "lblRoleDesc",
            "isVisible": true,
            "left": "0dp",
            "skin": "sknLbl485C75LatoRegular13Px",
            "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmAlertsManagement.LoremIpsumA\")",
            "top": "10dp",
            "width": "98.50%",
            "zIndex": 1
        }, {
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, {});
        var btnViewDetails = new kony.ui.Button({
            "bottom": "10dp",
            "id": "btnViewDetails",
            "isVisible": true,
            "left": "0dp",
            "skin": "sknBtnLato117eb013Px",
            "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmGroupsController.viewDetails\")",
            "top": "0dp",
            "width": "80dp",
            "zIndex": 1
        }, {
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "displayText": true,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, {});
        flxRoleInfo.add(flxRoleName, lblRoleDesc, btnViewDetails);
        flxRoleNameContainer.add(flxRoleCheckbox, flxRoleRadio, flxRoleInfo);
        flxCustomerProfileRoles.add(flxRoleNameContainer);
        return flxCustomerProfileRoles;
    }
})