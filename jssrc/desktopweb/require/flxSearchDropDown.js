define("flxSearchDropDown", function() {
    return function(controller) {
        var flxSearchDropDown = new kony.ui.FlexContainer({
            "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
            "clipBounds": true,
            "focusSkin": "sknflxffffffop100",
            "id": "flxSearchDropDown",
            "isVisible": true,
            "layoutType": kony.flex.FLOW_HORIZONTAL,
            "isModalContainer": false,
            "skin": "sknflxffffffop100"
        }, {}, {
            "hoverSkin": "sknFlxffffffOp100Cursor"
        });
        flxSearchDropDown.setDefaultUnit(kony.flex.DP);
        var flxCheckBox = new kony.ui.FlexContainer({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "clipBounds": true,
            "height": "12px",
            "id": "flxCheckBox",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "left": "15px",
            "isModalContainer": false,
            "skin": "slFbox",
            "top": "7dp",
            "width": "12px",
            "zIndex": 2
        }, {}, {});
        flxCheckBox.setDefaultUnit(kony.flex.DP);
        var imgCheckBox = new kony.ui.Image2({
            "centerY": "50%",
            "height": "100%",
            "id": "imgCheckBox",
            "isVisible": true,
            "left": "0%",
            "skin": "slImage",
            "src": "checkboxnormal.png",
            "width": "100%",
            "zIndex": 2
        }, {
            "imageScaleMode": constants.IMAGE_SCALE_MODE_MAINTAIN_ASPECT_RATIO,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, {});
        flxCheckBox.add(imgCheckBox);
        var lblDescription = new kony.ui.Label({
            "bottom": "5dp",
            "id": "lblDescription",
            "isVisible": true,
            "left": "10px",
            "skin": "sknlblLatoBold35475f14px",
            "text": "KL098234 - WestheimerKL09823",
            "top": "5dp",
            "width": "95%",
            "zIndex": 1
        }, {
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, {});
        var btnExt = new kony.ui.Button({
            "bottom": "5dp",
            "id": "btnExt",
            "isVisible": false,
            "left": 5,
            "skin": "sknbtnffffffLatoRegular12PxA94240",
            "text": "other external",
            "width": kony.flex.USE_PREFFERED_SIZE
        }, {
            "contentAlignment": constants.CONTENT_ALIGN_CENTER,
            "displayText": true,
            "padding": [1, 0, 1, 0],
            "paddingInPixel": false
        }, {});
        flxSearchDropDown.add(flxCheckBox, lblDescription, btnExt);
        return flxSearchDropDown;
    }
})