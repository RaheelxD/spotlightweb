define("LeadManagementModule/frmLeadManagement", function() {
    return function(controller) {
        function addWidgetsfrmLeadManagement() {
            this.setDefaultUnit(kony.flex.DP);
            var flxMain = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "900dp",
                "id": "flxMain",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "slFbox0b7aaa2e3bfa340",
                "top": "0dp",
                "width": "100%",
                "zIndex": 1
            }, {}, {});
            flxMain.setDefaultUnit(kony.flex.DP);
            var flxLeftPanel = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "100%",
                "id": "flxLeftPanel",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "masterType": constants.MASTER_TYPE_USERWIDGET,
                "isModalContainer": false,
                "skin": "sknLeftPannel0bb79e4bcca8e45",
                "top": "0dp",
                "width": "305px",
                "zIndex": 5
            }, {}, {});
            flxLeftPanel.setDefaultUnit(kony.flex.DP);
            var leftMenuNew = new com.adminConsole.navigation.leftMenuNew({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "height": "100%",
                "id": "leftMenuNew",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "masterType": constants.MASTER_TYPE_DEFAULT,
                "isModalContainer": false,
                "skin": "sknFlxBg003E75Op100",
                "top": "0dp",
                "width": "100%",
                "overrides": {
                    "brandLogo": {
                        "src": "infinity_dbx_c360_logo_white_2x.png"
                    },
                    "imgBottomLogo": {
                        "src": "logo_white_2x.png"
                    },
                    "leftMenuNew": {
                        "bottom": "viz.val_cleared",
                        "centerX": "viz.val_cleared",
                        "centerY": "viz.val_cleared",
                        "height": "100%",
                        "left": "0dp",
                        "maxHeight": "viz.val_cleared",
                        "maxWidth": "viz.val_cleared",
                        "minHeight": "viz.val_cleared",
                        "minWidth": "viz.val_cleared",
                        "right": "viz.val_cleared",
                        "top": "0dp",
                        "width": "100%"
                    }
                }
            }, {
                "overrides": {}
            }, {
                "overrides": {}
            });
            flxLeftPanel.add(leftMenuNew);
            var flxRightPanel = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "100%",
                "id": "flxRightPanel",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "305px",
                "masterType": constants.MASTER_TYPE_USERWIDGET,
                "isModalContainer": false,
                "right": "0dp",
                "skin": "slFbox0b7aaa2e3bfa340",
                "top": "0px",
                "zIndex": 1
            }, {}, {});
            flxRightPanel.setDefaultUnit(kony.flex.DP);
            var flxMainHeader = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "126px",
                "id": "flxMainHeader",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0px",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0px",
                "width": "100%",
                "zIndex": 10
            }, {}, {});
            flxMainHeader.setDefaultUnit(kony.flex.DP);
            var mainHeader = new com.adminConsole.header.mainHeader({
                "height": "126px",
                "id": "mainHeader",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0px",
                "masterType": constants.MASTER_TYPE_DEFAULT,
                "isModalContainer": false,
                "skin": "CopyslFbox1",
                "top": "0px",
                "width": "100%",
                "overrides": {
                    "btnAddNewOption": {
                        "i18n_text": "kony.i18n.getLocalizedString(\"i18n.LeadManagement.buttonCreateLead\")",
                        "isVisible": false,
                        "right": "0px",
                        "width": "140px"
                    },
                    "btnDropdownList": {
                        "isVisible": false
                    },
                    "flxButtons": {
                        "isVisible": true,
                        "right": "71dp",
                        "width": "400dp",
                        "zIndex": 2
                    },
                    "flxHeaderSeperator": {
                        "bottom": "viz.val_cleared",
                        "height": "1px",
                        "isVisible": false,
                        "left": "0px",
                        "right": "70px",
                        "top": "116px",
                        "zIndex": 3
                    },
                    "flxHeaderUserOptions": {
                        "height": "24px",
                        "isVisible": false,
                        "right": "70px",
                        "top": "20px",
                        "width": "30%",
                        "zIndex": 11
                    },
                    "flxMainHeader": {
                        "height": "126px",
                        "left": "35px",
                        "top": "0px"
                    },
                    "imgLogout": {
                        "src": "img_logout.png"
                    },
                    "lblHeading": {
                        "centerY": "viz.val_cleared",
                        "height": "40px",
                        "i18n_text": "kony.i18n.getLocalizedString(\"i18n.LeadManagement.titleViewLeads\")",
                        "left": "0px",
                        "top": "54px"
                    },
                    "mainHeader": {
                        "height": "126px",
                        "left": "0px",
                        "top": "0px"
                    }
                }
            }, {
                "overrides": {}
            }, {
                "overrides": {}
            });
            var flxBreadcrumb = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "20px",
                "id": "flxBreadcrumb",
                "isVisible": false,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "96px",
                "width": "100%",
                "zIndex": 3
            }, {}, {});
            flxBreadcrumb.setDefaultUnit(kony.flex.DP);
            var breadcrumbs = new com.adminConsole.common.breadcrumbs({
                "height": "20px",
                "id": "breadcrumbs",
                "isVisible": true,
                "layoutType": kony.flex.FLOW_HORIZONTAL,
                "left": "0dp",
                "masterType": constants.MASTER_TYPE_DEFAULT,
                "isModalContainer": false,
                "skin": "sknflxf5f6f8Op100",
                "top": "0dp",
                "width": "100%",
                "overrides": {
                    "btnBackToMain": {
                        "text": "LEAD MANAGEMENT"
                    },
                    "fontIconBreadcrumbsRight": {
                        "centerY": "56%",
                        "height": "20px",
                        "left": "12px",
                        "right": "12px"
                    },
                    "imgBreadcrumbsDown": {
                        "src": "img_down_arrow.png"
                    },
                    "imgBreadcrumbsRight": {
                        "src": "img_breadcrumb_arrow.png"
                    },
                    "imgBreadcrumbsRight2": {
                        "src": "img_breadcrumb_arrow.png"
                    },
                    "imgBreadcrumbsRight3": {
                        "src": "img_breadcrumb_arrow.png"
                    },
                    "lblCurrentScreen": {
                        "centerY": "50%",
                        "i18n_text": "kony.i18n.getLocalizedString(\"i18n.LeadManagement.breadcrumbViewLeads\")",
                        "left": "0px",
                        "top": "viz.val_cleared"
                    }
                }
            }, {
                "overrides": {}
            }, {
                "overrides": {}
            });
            flxBreadcrumb.add(breadcrumbs);
            var flxHeaderButtons = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "bottom": "0px",
                "clipBounds": true,
                "height": "30px",
                "id": "flxHeaderButtons",
                "isVisible": true,
                "layoutType": kony.flex.FLOW_HORIZONTAL,
                "reverseLayoutDirection": true,
                "isModalContainer": false,
                "right": "0px",
                "skin": "slFbox",
                "top": "60px",
                "width": "400px"
            }, {}, {});
            flxHeaderButtons.setDefaultUnit(kony.flex.DP);
            var btnNotes = new kony.ui.Button({
                "focusSkin": "sknBtn003E75LatoRegular13pxFFFFFFRad20px",
                "height": "30px",
                "id": "btnNotes",
                "isVisible": false,
                "left": 0,
                "right": "35px",
                "skin": "sknBtn003E75LatoRegular13pxFFFFFFRad20px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.LeadManagement.buttonNotes\")",
                "top": "0px",
                "width": "100px",
                "zIndex": 2
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_CENTER,
                "displayText": true,
                "padding": [1, 0, 1, 0],
                "paddingInPixel": false
            }, {
                "hoverSkin": "sknBtn005198LatoRegular13pxFFFFFFRad20px"
            });
            var btnCreateLeads = new kony.ui.Button({
                "focusSkin": "sknBtn003E75LatoRegular13pxFFFFFFRad20px",
                "height": "30px",
                "id": "btnCreateLeads",
                "isVisible": true,
                "right": "35px",
                "skin": "sknBtn003E75LatoRegular13pxFFFFFFRad20px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.LeadManagement.buttonCreateLead\")",
                "top": "0px",
                "width": "150px",
                "zIndex": 2
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_CENTER,
                "displayText": true,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {
                "hoverSkin": "sknBtn005198LatoRegular13pxFFFFFFRad20px"
            });
            var btnImportLeads = new kony.ui.Button({
                "focusSkin": "sknBtn003E75LatoRegular13pxFFFFFFRad20px",
                "height": "30px",
                "id": "btnImportLeads",
                "isVisible": true,
                "right": "20px",
                "skin": "sknBtn003E75LatoRegular13pxFFFFFFRad20px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.LeadManagement.buttonImportLeads\")",
                "top": "0px",
                "width": "150px",
                "zIndex": 2
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_CENTER,
                "displayText": true,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {
                "hoverSkin": "sknBtn005198LatoRegular13pxFFFFFFRad20px"
            });
            flxHeaderButtons.add(btnNotes, btnCreateLeads, btnImportLeads);
            flxMainHeader.add(mainHeader, flxBreadcrumb, flxHeaderButtons);
            var flxHeaderDropdown = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                "clipBounds": true,
                "id": "flxHeaderDropdown",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "isModalContainer": false,
                "right": "35dp",
                "skin": "slFbox",
                "top": "20dp",
                "width": "400dp",
                "zIndex": 1
            }, {}, {});
            flxHeaderDropdown.setDefaultUnit(kony.flex.DP);
            var dropdownMainHeader = new com.adminConsole.common.dropdownMainHeader({
                "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                "id": "dropdownMainHeader",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "masterType": constants.MASTER_TYPE_DEFAULT,
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0dp",
                "width": "100%",
                "overrides": {
                    "flxDropdown": {
                        "isVisible": false
                    },
                    "imgUpArrow": {
                        "src": "uparrow_2x.png"
                    }
                }
            }, {
                "overrides": {}
            }, {
                "overrides": {}
            });
            flxHeaderDropdown.add(dropdownMainHeader);
            var flxMainContent = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "774px",
                "id": "flxMainContent",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0px",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "126px",
                "width": "100%",
                "zIndex": 10
            }, {}, {});
            flxMainContent.setDefaultUnit(kony.flex.DP);
            var flxViewLeads = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "100%",
                "id": "flxViewLeads",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0px",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0px",
                "width": "100%",
                "zIndex": 1
            }, {}, {});
            flxViewLeads.setDefaultUnit(kony.flex.DP);
            var flxLeadsUnavailable = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "100%",
                "id": "flxLeadsUnavailable",
                "isVisible": false,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0px",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0px",
                "width": "100%",
                "zIndex": 1
            }, {}, {});
            flxLeadsUnavailable.setDefaultUnit(kony.flex.DP);
            var noStaticData = new com.adminConsole.staticContent.noStaticData({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "height": "340px",
                "id": "noStaticData",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "35px",
                "masterType": constants.MASTER_TYPE_DEFAULT,
                "isModalContainer": false,
                "right": "35px",
                "skin": "slFbox",
                "top": "0dp",
                "zIndex": 1,
                "overrides": {
                    "btnAddStaticContent": {
                        "i18n_text": "kony.i18n.getLocalizedString(\"i18n.LeadManagement.buttonCreateLead\")",
                        "width": "150px"
                    },
                    "lblNoStaticContentCreated": {
                        "i18n_text": "kony.i18n.getLocalizedString(\"i18n.LeadManagement.labelNoLeadHasBeenCreatedYet\")"
                    },
                    "lblNoStaticContentMsg": {
                        "i18n_text": "kony.i18n.getLocalizedString(\"i18n.LeadManagement.labelClickOnCreateLeadToContinue\")"
                    }
                }
            }, {
                "overrides": {}
            }, {
                "overrides": {}
            });
            flxLeadsUnavailable.add(noStaticData);
            var flxLeadsAvailable = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "100%",
                "id": "flxLeadsAvailable",
                "isVisible": false,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0px",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0px",
                "width": "100%",
                "zIndex": 1
            }, {}, {});
            flxLeadsAvailable.setDefaultUnit(kony.flex.DP);
            var flxViewLeadsInner = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "754px",
                "id": "flxViewLeadsInner",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "35px",
                "isModalContainer": false,
                "right": "35px",
                "skin": "sknflxLeadView",
                "top": "0px",
                "zIndex": 1
            }, {}, {});
            flxViewLeadsInner.setDefaultUnit(kony.flex.DP);
            var flxViewLeadsTabs = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "80px",
                "id": "flxViewLeadsTabs",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0px",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0px",
                "width": "100%",
                "zIndex": 1
            }, {}, {});
            flxViewLeadsTabs.setDefaultUnit(kony.flex.DP);
            var countTabs = new com.adminConsole.LeadManagment.countTabs({
                "height": "60px",
                "id": "countTabs",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "20px",
                "masterType": constants.MASTER_TYPE_DEFAULT,
                "isModalContainer": false,
                "right": "20px",
                "skin": "slFbox",
                "top": "20px",
                "overrides": {
                    "countTabs": {
                        "isVisible": true,
                        "left": "20px",
                        "right": "20px",
                        "top": "20px",
                        "width": "viz.val_cleared"
                    }
                }
            }, {
                "overrides": {}
            }, {
                "overrides": {}
            });
            flxViewLeadsTabs.add(countTabs);
            var flxViewLeadsData = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "674px",
                "id": "flxViewLeadsData",
                "isVisible": false,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0px",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "80px",
                "width": "100%",
                "zIndex": 1
            }, {}, {});
            flxViewLeadsData.setDefaultUnit(kony.flex.DP);
            var flxViewLeadsTop = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "55px",
                "id": "flxViewLeadsTop",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0px",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0px",
                "width": "100%",
                "zIndex": 1
            }, {}, {});
            flxViewLeadsTop.setDefaultUnit(kony.flex.DP);
            var flxLeadCount = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "16px",
                "id": "flxLeadCount",
                "isVisible": false,
                "layoutType": kony.flex.FLOW_HORIZONTAL,
                "left": "25px",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "20px",
                "width": "300px",
                "zIndex": 2
            }, {}, {});
            flxLeadCount.setDefaultUnit(kony.flex.DP);
            var lblLeadCountInitial = new kony.ui.Label({
                "height": "16px",
                "id": "lblLeadCountInitial",
                "isVisible": true,
                "left": "0px",
                "skin": "sknlblLato696c7312px",
                "text": "Showing leads ",
                "top": "0px",
                "width": "80px",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var lblLeadCountStart = new kony.ui.Label({
                "height": "16px",
                "id": "lblLeadCountStart",
                "isVisible": true,
                "left": "0px",
                "skin": "sknlblLato696c7312px",
                "text": "1",
                "top": "0px",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var lblLeadCountHyphen = new kony.ui.Label({
                "height": "16px",
                "id": "lblLeadCountHyphen",
                "isVisible": true,
                "left": "0px",
                "skin": "sknlblLato696c7312px",
                "text": " - ",
                "top": "0px",
                "width": "12px",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var lblLeadCountEnd = new kony.ui.Label({
                "height": "16px",
                "id": "lblLeadCountEnd",
                "isVisible": true,
                "left": "0px",
                "skin": "sknlblLato696c7312px",
                "text": "20",
                "top": "0px",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var lblLeadCountMiddle = new kony.ui.Label({
                "height": "16px",
                "id": "lblLeadCountMiddle",
                "isVisible": true,
                "left": "0px",
                "skin": "sknlblLato696c7312px",
                "text": " of ",
                "top": "0px",
                "width": "19px",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var lblLeadCountTotal = new kony.ui.Label({
                "height": "16px",
                "id": "lblLeadCountTotal",
                "isVisible": true,
                "left": "0px",
                "skin": "sknlblLato696c7312px",
                "text": "200",
                "top": "0px",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxLeadCount.add(lblLeadCountInitial, lblLeadCountStart, lblLeadCountHyphen, lblLeadCountEnd, lblLeadCountMiddle, lblLeadCountTotal);
            var flxLeadSearchContainer = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "centerY": "50%",
                "clipBounds": true,
                "height": "35px",
                "id": "flxLeadSearchContainer",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "isModalContainer": false,
                "right": "65px",
                "skin": "sknflxConfigBundlesSearchNormal",
                "width": "350px",
                "zIndex": 1
            }, {}, {});
            flxLeadSearchContainer.setDefaultUnit(kony.flex.DP);
            var tbxLeadSearchBox = new kony.ui.TextBox2({
                "autoCapitalize": constants.TEXTBOX_AUTO_CAPITALIZE_NONE,
                "centerY": "50%",
                "height": "30px",
                "id": "tbxLeadSearchBox",
                "isVisible": true,
                "keyBoardStyle": constants.TEXTBOX_KEY_BOARD_STYLE_DEFAULT,
                "left": "10px",
                "onTouchStart": controller.AS_TextField_cfab06859f0f43eca99495891ee290d5,
                "i18n_placeholder": "kony.i18n.getLocalizedString(\"i18n.LeadManagement.searchLeadsPlaceholder\")",
                "right": "35px",
                "secureTextEntry": false,
                "skin": "sknSearchtbxConfigurationBundlesNormal",
                "textInputMode": constants.TEXTBOX_INPUT_MODE_ANY,
                "zIndex": 1
            }, {
                "containerHeightMode": constants.TEXTBOX_FONT_METRICS_DRIVEN_HEIGHT,
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [2, 0, 0, 0],
                "paddingInPixel": false
            }, {
                "autoCorrect": false
            });
            var flxIconLeadSearch = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "33px",
                "id": "flxIconLeadSearch",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "isModalContainer": false,
                "right": "0px",
                "skin": "sknCursor",
                "top": "0px",
                "width": "33px",
                "zIndex": 1
            }, {}, {
                "hoverSkin": "sknCursor"
            });
            flxIconLeadSearch.setDefaultUnit(kony.flex.DP);
            var btnLeadSearch = new kony.ui.Button({
                "centerY": "50%",
                "focusSkin": "defBtnFocus",
                "height": "30px",
                "id": "btnLeadSearch",
                "isVisible": true,
                "right": "3px",
                "skin": "skbtnLeadSearch",
                "text": "",
                "top": "0px",
                "width": "30px",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_CENTER,
                "displayText": true,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxIconLeadSearch.add(btnLeadSearch);
            var flxIconLeadClear = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "100%",
                "id": "flxIconLeadClear",
                "isVisible": false,
                "layoutType": kony.flex.FREE_FORM,
                "isModalContainer": false,
                "right": "32px",
                "skin": "sknCursor",
                "top": "0px",
                "width": "25px",
                "zIndex": 1
            }, {}, {
                "hoverSkin": "sknCursor"
            });
            flxIconLeadClear.setDefaultUnit(kony.flex.DP);
            var fontIconClear = new kony.ui.Label({
                "centerY": "50%",
                "id": "fontIconClear",
                "isVisible": true,
                "left": "5px",
                "skin": "sknFontIconSearchCross16px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.userwidgetmodel.lblClearSearch\")",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxIconLeadClear.add(fontIconClear);
            flxLeadSearchContainer.add(tbxLeadSearchBox, flxIconLeadSearch, flxIconLeadClear);
            var flxLeadFilter = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "centerY": "50%",
                "clipBounds": true,
                "height": "30px",
                "id": "flxLeadFilter",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "isModalContainer": false,
                "right": "20px",
                "skin": "sknFlxPointer",
                "width": "30px"
            }, {}, {});
            flxLeadFilter.setDefaultUnit(kony.flex.DP);
            var lblLeadFilter = new kony.ui.Label({
                "height": "100%",
                "id": "lblLeadFilter",
                "isVisible": true,
                "left": "0px",
                "right": "20px",
                "skin": "sknlblIcomoonFilter",
                "text": "",
                "top": "0px",
                "width": "100%",
                "zIndex": 2
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxLeadFilter.add(lblLeadFilter);
            flxViewLeadsTop.add(flxLeadCount, flxLeadSearchContainer, flxLeadFilter);
            var flxViewLeadsSeparator = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "1px",
                "id": "flxViewLeadsSeparator",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0px",
                "isModalContainer": false,
                "right": "0px",
                "skin": "slFbox",
                "top": "55px",
                "zIndex": 1
            }, {}, {});
            flxViewLeadsSeparator.setDefaultUnit(kony.flex.DP);
            var lblHeaderSeparator = new kony.ui.Label({
                "bottom": "0px",
                "centerX": "50%",
                "height": "1px",
                "id": "lblHeaderSeparator",
                "isVisible": true,
                "left": "20px",
                "right": "20px",
                "skin": "sknConfigurationBundleSeparator",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.userwidgetmodel.lblHeaderSeparator\")",
                "top": "0px",
                "zIndex": 2
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxViewLeadsSeparator.add(lblHeaderSeparator);
            var flxViewLeadsList = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "618px",
                "id": "flxViewLeadsList",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0px",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "56px",
                "width": "100%",
                "zIndex": 1
            }, {}, {});
            flxViewLeadsList.setDefaultUnit(kony.flex.DP);
            var viewLeads = new com.adminConsole.LeadManagement.viewLeads({
                "height": "100%",
                "id": "viewLeads",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0px",
                "masterType": constants.MASTER_TYPE_DEFAULT,
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0px",
                "width": "100%",
                "overrides": {
                    "flxLeadEmailSort": {
                        "isVisible": false
                    },
                    "flxLeadPhoneSort": {
                        "isVisible": false
                    },
                    "flxTableView": {
                        "height": "558px"
                    },
                    "imgHeaderCheckBox": {
                        "src": "checkbox.png"
                    },
                    "segLeads": {
                        "autogrowMode": kony.flex.AUTOGROW_HEIGHT
                    },
                    "viewLeads": {
                        "height": "100%",
                        "left": "0px",
                        "top": "0px"
                    }
                }
            }, {
                "overrides": {}
            }, {
                "overrides": {}
            });
            var leadContextualMenu = new com.adminConsole.common.leadContextualMenu({
                "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                "id": "leadContextualMenu",
                "isVisible": false,
                "layoutType": kony.flex.FLOW_VERTICAL,
                "masterType": constants.MASTER_TYPE_DEFAULT,
                "isModalContainer": false,
                "right": "40px",
                "skin": "sknflxffffffop100dbdbe6Radius3px",
                "top": "120px",
                "width": "180px",
                "overrides": {
                    "flxClose": {
                        "isVisible": false
                    },
                    "flxMoveToNew": {
                        "isVisible": false
                    },
                    "leadContextualMenu": {
                        "isVisible": false,
                        "left": "viz.val_cleared",
                        "right": "40px",
                        "top": "120px",
                        "width": "180px"
                    }
                }
            }, {
                "overrides": {}
            }, {
                "overrides": {}
            });
            flxViewLeadsList.add(viewLeads, leadContextualMenu);
            var flxNoResultFoundinner = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "220dp",
                "id": "flxNoResultFoundinner",
                "isVisible": false,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0px",
                "isModalContainer": false,
                "right": "20px",
                "skin": "sknFlxTrans",
                "top": "60px",
                "zIndex": 1
            }, {}, {});
            flxNoResultFoundinner.setDefaultUnit(kony.flex.DP);
            var txtNoResultFoundInner = new kony.ui.RichText({
                "bottom": "50px",
                "centerX": "50%",
                "centerY": "50%",
                "height": "15dp",
                "id": "txtNoResultFoundInner",
                "isVisible": true,
                "left": "0",
                "skin": "sknRtxLato84939e12Px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmAlertsManagementController.No_result_found\")",
                "width": "80%",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_CENTER,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxNoResultFoundinner.add(txtNoResultFoundInner);
            var flxFilterOverLay = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "bottom": "0px",
                "clipBounds": true,
                "id": "flxFilterOverLay",
                "isVisible": false,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0px",
                "isModalContainer": false,
                "right": "0px",
                "skin": "sknflxbgffffffOp50Per",
                "top": "60px",
                "zIndex": 1
            }, {}, {});
            flxFilterOverLay.setDefaultUnit(kony.flex.DP);
            flxFilterOverLay.add();
            flxViewLeadsData.add(flxViewLeadsTop, flxViewLeadsSeparator, flxViewLeadsList, flxNoResultFoundinner, flxFilterOverLay);
            var flxLeadsDetails = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "bottom": "0px",
                "clipBounds": true,
                "height": "339px",
                "id": "flxLeadsDetails",
                "isVisible": false,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0px",
                "isModalContainer": false,
                "right": "0px",
                "skin": "slFbox",
                "top": "100px",
                "zIndex": 1
            }, {}, {});
            flxLeadsDetails.setDefaultUnit(kony.flex.DP);
            var flxLeadDetailsSeparator1 = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "1px",
                "id": "flxLeadDetailsSeparator1",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0px",
                "isModalContainer": false,
                "right": "0px",
                "skin": "sknflx115678",
                "top": "0px",
                "zIndex": 1
            }, {}, {});
            flxLeadDetailsSeparator1.setDefaultUnit(kony.flex.DP);
            flxLeadDetailsSeparator1.add();
            var flxOperationsHeader = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "60px",
                "id": "flxOperationsHeader",
                "isVisible": true,
                "layoutType": kony.flex.FLOW_HORIZONTAL,
                "left": "0px",
                "isModalContainer": false,
                "right": "0px",
                "skin": "slFbox",
                "top": "1px",
                "zIndex": 1
            }, {}, {});
            flxOperationsHeader.setDefaultUnit(kony.flex.DP);
            var flxOptions1 = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "100%",
                "id": "flxOptions1",
                "isVisible": true,
                "layoutType": kony.flex.FLOW_HORIZONTAL,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0dp",
                "width": "50%"
            }, {}, {});
            flxOptions1.setDefaultUnit(kony.flex.DP);
            var lblAssignTo = new kony.ui.Label({
                "centerY": "50%",
                "id": "lblAssignTo",
                "isVisible": true,
                "left": "20px",
                "skin": "sknlbl485c7511px",
                "text": "ASSIGNED TO: ",
                "width": "80px",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var lblAssignStatus = new kony.ui.Label({
                "centerY": "50%",
                "id": "lblAssignStatus",
                "isVisible": true,
                "left": "0px",
                "skin": "sknlbl485c7511px",
                "text": "Unassigned",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_CENTER,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var btnAssign = new kony.ui.Button({
                "bottom": "15px",
                "centerY": "50%",
                "focusSkin": "sknbtnf7f7faLatoRegular12Px485c75Border1px485C75Radius20px",
                "height": "22px",
                "id": "btnAssign",
                "isVisible": false,
                "left": "15dp",
                "skin": "sknbtnf7f7faLatoRegular12Px485c75Border1px485C75Radius20px",
                "text": "Assign",
                "width": "66px",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_CENTER,
                "displayText": true,
                "padding": [2, 0, 2, 0],
                "paddingInPixel": false
            }, {
                "hoverSkin": "sknbtnffffffLatoRegular12Px485c75Border1px485C75Radius20px"
            });
            flxOptions1.add(lblAssignTo, lblAssignStatus, btnAssign);
            var flxOptions2 = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "100%",
                "id": "flxOptions2",
                "isVisible": true,
                "layoutType": kony.flex.FLOW_HORIZONTAL,
                "reverseLayoutDirection": true,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0dp",
                "width": "50%"
            }, {}, {});
            flxOptions2.setDefaultUnit(kony.flex.DP);
            var flxLeadDetailsOptions = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "centerY": "50%",
                "clipBounds": true,
                "height": "40dp",
                "id": "flxLeadDetailsOptions",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "isModalContainer": false,
                "onClick": controller.AS_FlexContainer_f846ab074c4c4ba298064b9eba6c1a76,
                "right": "20dp",
                "skin": "slFbox",
                "width": "40px",
                "zIndex": 1
            }, {}, {
                "hoverSkin": "sknflxffffffop100Border424242Radius100px"
            });
            flxLeadDetailsOptions.setDefaultUnit(kony.flex.DP);
            var fonticonLeadDetailsOptions = new kony.ui.Label({
                "centerX": "50%",
                "centerY": "50%",
                "height": "15px",
                "id": "fonticonLeadDetailsOptions",
                "isVisible": true,
                "left": "14dp",
                "skin": "sknFontIconOptionMenu",
                "text": "",
                "width": "15px",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxLeadDetailsOptions.add(fonticonLeadDetailsOptions);
            var btnStartAnApplication = new kony.ui.Button({
                "centerY": "50%",
                "focusSkin": "sknBtn003E75LatoRegular13pxFFFFFFRad20px",
                "height": "30px",
                "id": "btnStartAnApplication",
                "isVisible": false,
                "right": "15px",
                "skin": "sknBtn003E75LatoRegular13pxFFFFFFRad20px",
                "text": "Start an Application",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_CENTER,
                "displayText": true,
                "padding": [2, 0, 2, 0],
                "paddingInPixel": false
            }, {
                "hoverSkin": "sknBtn005198LatoRegular13pxFFFFFFRad20px"
            });
            var btnMoveToInProgress = new kony.ui.Button({
                "centerY": "50%",
                "focusSkin": "sknbtnf7f7faLatoRegular12Px485c75Border1px485C75Radius20px",
                "height": "22px",
                "id": "btnMoveToInProgress",
                "isVisible": true,
                "right": "15px",
                "skin": "sknbtnf7f7faLatoRegular12Px485c75Border1px485C75Radius20px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.LeadManagement.buttonMoveToInProgress\")",
                "width": "146px",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_CENTER,
                "displayText": true,
                "padding": [2, 0, 2, 0],
                "paddingInPixel": false
            }, {
                "hoverSkin": "sknbtnffffffLatoRegular12Px485c75Border1px485C75Radius20px"
            });
            var btnMoveToNew = new kony.ui.Button({
                "centerY": "50%",
                "focusSkin": "sknbtnf7f7faLatoRegular12Px485c75Border1px485C75Radius20px",
                "height": "22px",
                "id": "btnMoveToNew",
                "isVisible": true,
                "right": "15px",
                "skin": "sknbtnf7f7faLatoRegular12Px485c75Border1px485C75Radius20px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.LeadManagement.buttonMoveToNew\")",
                "width": "110px",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_CENTER,
                "displayText": true,
                "padding": [2, 0, 2, 0],
                "paddingInPixel": false
            }, {
                "hoverSkin": "sknbtnffffffLatoRegular12Px485c75Border1px485C75Radius20px"
            });
            flxOptions2.add(flxLeadDetailsOptions, btnStartAnApplication, btnMoveToInProgress, btnMoveToNew);
            flxOperationsHeader.add(flxOptions1, flxOptions2);
            var flxLeadDetailsSeparator2 = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "1px",
                "id": "flxLeadDetailsSeparator2",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0px",
                "isModalContainer": false,
                "right": "0px",
                "skin": "sknflx115678",
                "top": "61px",
                "zIndex": 1
            }, {}, {});
            flxLeadDetailsSeparator2.setDefaultUnit(kony.flex.DP);
            flxLeadDetailsSeparator2.add();
            var leadDetailsContextualMenu = new com.adminConsole.common.leadContextualMenu({
                "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                "id": "leadDetailsContextualMenu",
                "isVisible": false,
                "layoutType": kony.flex.FLOW_VERTICAL,
                "masterType": constants.MASTER_TYPE_DEFAULT,
                "isModalContainer": false,
                "right": "0px",
                "skin": "sknflxffffffop100dbdbe6Radius3px",
                "top": "62px",
                "width": "180px",
                "overrides": {
                    "flxClose": {
                        "isVisible": false
                    },
                    "flxMoveToInProgress": {
                        "isVisible": false
                    },
                    "flxMoveToNew": {
                        "isVisible": false
                    },
                    "leadContextualMenu": {
                        "isVisible": false,
                        "left": "viz.val_cleared",
                        "right": "0px",
                        "top": "62px",
                        "width": "180px"
                    }
                }
            }, {
                "overrides": {}
            }, {
                "overrides": {}
            });
            var flxLeadDetailsName = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "22px",
                "id": "flxLeadDetailsName",
                "isVisible": true,
                "layoutType": kony.flex.FLOW_HORIZONTAL,
                "left": "20px",
                "isModalContainer": false,
                "right": "0px",
                "skin": "slFbox",
                "top": "82px",
                "zIndex": 1
            }, {}, {});
            flxLeadDetailsName.setDefaultUnit(kony.flex.DP);
            var fonticonLeadType = new kony.ui.Label({
                "height": "22px",
                "id": "fonticonLeadType",
                "isVisible": true,
                "left": "0px",
                "skin": "sknlblIcomoonLeadType",
                "text": "",
                "top": "0px",
                "width": "24px",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var lblName = new kony.ui.Label({
                "height": "22px",
                "id": "lblName",
                "isVisible": true,
                "left": "20px",
                "skin": "sknLatoRegular485C7518px",
                "text": "Mr. Johnathan Doe",
                "top": "0px",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var flxViewProfile = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "22px",
                "id": "flxViewProfile",
                "isVisible": false,
                "layoutType": kony.flex.FREE_FORM,
                "left": "20px",
                "isModalContainer": false,
                "skin": "sknFlxPointer",
                "top": "0px",
                "width": "70px",
                "zIndex": 1
            }, {}, {});
            flxViewProfile.setDefaultUnit(kony.flex.DP);
            var lblViewProfile = new kony.ui.Label({
                "height": "22px",
                "id": "lblViewProfile",
                "isVisible": true,
                "left": "0px",
                "skin": "sknlblLeadViewProfile",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.LeadManagement.labelViewProfile\")",
                "top": "0px",
                "width": "70px",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxViewProfile.add(lblViewProfile);
            flxLeadDetailsName.add(fonticonLeadType, lblName, flxViewProfile);
            var detailsRow1 = new com.adminConsole.view.details({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "height": "40px",
                "id": "detailsRow1",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0px",
                "masterType": constants.MASTER_TYPE_DEFAULT,
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "134px",
                "width": "100%",
                "overrides": {
                    "details": {
                        "left": "0px",
                        "top": "134px"
                    },
                    "flxColumn1": {
                        "left": "20px"
                    },
                    "flxColumn2": {
                        "left": "35%"
                    },
                    "flxDataAndImage2": {
                        "bottom": "0px",
                        "left": "0dp"
                    },
                    "imgData1": {
                        "src": "active_circle2x.png"
                    },
                    "imgData2": {
                        "src": "active_circle2x.png"
                    },
                    "imgData3": {
                        "src": "active_circle2x.png"
                    },
                    "lblData1": {
                        "centerY": "50%",
                        "left": "0px",
                        "text": "john.doe@gmail.com"
                    },
                    "lblData2": {
                        "text": "+914258303691"
                    },
                    "lblData3": {
                        "text": "Turbo Auto Loan"
                    },
                    "lblHeading1": {
                        "i18n_text": "kony.i18n.getLocalizedString(\"i18n.LeadManagement.segmentheaderEmail\")"
                    },
                    "lblHeading2": {
                        "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmCustomerCare.lblPhoneNumber\")"
                    },
                    "lblHeading3": {
                        "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmCustomerManagementController.CustomerProduct\")"
                    }
                }
            }, {
                "overrides": {}
            }, {
                "overrides": {}
            });
            var detailsRow2 = new com.adminConsole.view.details({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "height": "40px",
                "id": "detailsRow2",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0px",
                "masterType": constants.MASTER_TYPE_DEFAULT,
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "204px",
                "width": "100%",
                "overrides": {
                    "details": {
                        "left": "0px",
                        "top": "204px"
                    },
                    "flxColumn1": {
                        "left": "20px"
                    },
                    "flxColumn2": {
                        "left": "35%"
                    },
                    "imgData1": {
                        "src": "active_circle2x.png"
                    },
                    "imgData2": {
                        "src": "active_circle2x.png"
                    },
                    "imgData3": {
                        "src": "active_circle2x.png"
                    },
                    "lblData1": {
                        "text": "John"
                    },
                    "lblData2": {
                        "text": "01-03-2019"
                    },
                    "lblData3": {
                        "text": "--"
                    },
                    "lblHeading1": {
                        "text": "CREATED BY"
                    },
                    "lblHeading2": {
                        "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmCustomerManagementController.CREATED_ON\")"
                    },
                    "lblHeading3": {
                        "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmLoansDashboard.ModifiedOn\")"
                    }
                }
            }, {
                "overrides": {}
            }, {
                "overrides": {}
            });
            var detailsRow3 = new com.adminConsole.view.details({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "height": "40px",
                "id": "detailsRow3",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0px",
                "masterType": constants.MASTER_TYPE_DEFAULT,
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "279px",
                "width": "100%",
                "overrides": {
                    "details": {
                        "left": "0px",
                        "top": "279px"
                    },
                    "flxColumn1": {
                        "left": "20px"
                    },
                    "flxColumn2": {
                        "isVisible": false,
                        "left": "35%"
                    },
                    "flxColumn3": {
                        "isVisible": false
                    },
                    "imgData1": {
                        "src": "active_circle2x.png"
                    },
                    "imgData2": {
                        "src": "active_circle2x.png"
                    },
                    "imgData3": {
                        "src": "active_circle2x.png"
                    },
                    "lblData1": {
                        "text": "User is not intrested any more"
                    },
                    "lblData2": {
                        "text": "01-03-2019"
                    },
                    "lblData3": {
                        "text": "--"
                    },
                    "lblHeading1": {
                        "text": "CLOSURE REASON"
                    },
                    "lblHeading2": {
                        "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmCustomerManagementController.CREATED_ON\")"
                    },
                    "lblHeading3": {
                        "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmLoansDashboard.ModifiedOn\")"
                    }
                }
            }, {
                "overrides": {}
            }, {
                "overrides": {}
            });
            flxLeadsDetails.add(flxLeadDetailsSeparator1, flxOperationsHeader, flxLeadDetailsSeparator2, leadDetailsContextualMenu, flxLeadDetailsName, detailsRow1, detailsRow2, detailsRow3);
            var flxNoResultFound = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "220dp",
                "id": "flxNoResultFound",
                "isVisible": false,
                "layoutType": kony.flex.FREE_FORM,
                "left": "20px",
                "isModalContainer": false,
                "right": "20px",
                "skin": "sknFlxTrans",
                "top": "80px",
                "zIndex": 1
            }, {}, {});
            flxNoResultFound.setDefaultUnit(kony.flex.DP);
            var rtxSearchMesg = new kony.ui.RichText({
                "bottom": "50px",
                "centerX": "50%",
                "centerY": "50%",
                "height": "15dp",
                "id": "rtxSearchMesg",
                "isVisible": true,
                "left": "0",
                "skin": "sknRtxLato84939e12Px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmAlertsManagementController.No_result_found\")",
                "width": "80%",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_CENTER,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxNoResultFound.add(rtxSearchMesg);
            flxViewLeadsInner.add(flxViewLeadsTabs, flxViewLeadsData, flxLeadsDetails, flxNoResultFound);
            flxLeadsAvailable.add(flxViewLeadsInner);
            var flxCreateUpdateLeads = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "100%",
                "id": "flxCreateUpdateLeads",
                "isVisible": false,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0px",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0px",
                "width": "100%"
            }, {}, {});
            flxCreateUpdateLeads.setDefaultUnit(kony.flex.DP);
            var createUpdate = new com.adminConsole.LeadManagment.createUpdate({
                "height": "754px",
                "id": "createUpdate",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "35px",
                "masterType": constants.MASTER_TYPE_DEFAULT,
                "isModalContainer": false,
                "right": "25px",
                "skin": "sknflxLeadView",
                "top": "0px",
                "zIndex": 1,
                "overrides": {
                    "createUpdate": {
                        "bottom": "viz.val_cleared",
                        "height": "754px"
                    },
                    "flxCreateUpdate": {
                        "bottom": "viz.val_cleared",
                        "height": "754px"
                    },
                    "flxLeadNote": {
                        "height": "140px"
                    },
                    "flxRow4": {
                        "bottom": "viz.val_cleared",
                        "height": "175px"
                    },
                    "flxSeperatorInner": {
                        "left": "20dp",
                        "right": "20dp"
                    }
                }
            }, {
                "overrides": {}
            }, {
                "overrides": {}
            });
            flxCreateUpdateLeads.add(createUpdate);
            flxViewLeads.add(flxLeadsUnavailable, flxLeadsAvailable, flxCreateUpdateLeads);
            flxMainContent.add(flxViewLeads);
            var flxNote = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "100%",
                "id": "flxNote",
                "isVisible": false,
                "layoutType": kony.flex.FREE_FORM,
                "isModalContainer": false,
                "right": "0px",
                "skin": "sknflxffffffop100dbdbe6Radius3px",
                "top": "0px",
                "width": "30%",
                "zIndex": 20
            }, {}, {});
            flxNote.setDefaultUnit(kony.flex.DP);
            var Notes = new com.adminConsole.customerMang.Notes({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "height": "100%",
                "id": "Notes",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "masterType": constants.MASTER_TYPE_DEFAULT,
                "isModalContainer": false,
                "skin": "slFbox0d4d94de464db42CM",
                "width": "100%",
                "overrides": {
                    "flxNotes": {
                        "top": "viz.val_cleared"
                    },
                    "imgCloseNotes": {
                        "src": "close_blue.png"
                    },
                    "lblNotesSize": {
                        "height": "16px",
                        "text": "0/1000",
                        "top": "0px"
                    },
                    "segNotes": {
                        "data": [
                            [{
                                    "fonticonArrow": "",
                                    "imgArrow": "img_down_arrow.png",
                                    "lblDate": "October 21, 2017"
                                },
                                [{
                                    "imgUser": "option3.png",
                                    "lblTime": " 6:46 am",
                                    "lblUserName": "John Doe",
                                    "rtxNotesDescription": "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua."
                                }, {
                                    "imgUser": "option3.png",
                                    "lblTime": " 6:46 am",
                                    "lblUserName": "John Doe",
                                    "rtxNotesDescription": "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua."
                                }]
                            ]
                        ]
                    }
                }
            }, {
                "overrides": {}
            }, {
                "overrides": {}
            });
            flxNote.add(Notes);
            var flxHeaderUserDropdown = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                "clipBounds": true,
                "id": "flxHeaderUserDropdown",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "isModalContainer": false,
                "right": "20px",
                "skin": "slFbox",
                "top": "20px",
                "width": "250dp",
                "zIndex": 21
            }, {}, {});
            flxHeaderUserDropdown.setDefaultUnit(kony.flex.DP);
            var flxMainHeaderUserDropdown = new com.adminConsole.common.dropdownMainHeader({
                "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                "id": "flxMainHeaderUserDropdown",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "masterType": constants.MASTER_TYPE_DEFAULT,
                "isModalContainer": false,
                "right": "0px",
                "skin": "slFbox",
                "top": "0px",
                "width": "100%",
                "zIndex": 1,
                "overrides": {
                    "dropdownMainHeader": {
                        "isVisible": true,
                        "left": "viz.val_cleared",
                        "right": "0px",
                        "top": "0px",
                        "zIndex": 1
                    },
                    "flxDropdown": {
                        "isVisible": false
                    },
                    "imgUpArrow": {
                        "src": "uparrow_2x.png"
                    }
                }
            }, {
                "overrides": {}
            }, {
                "overrides": {}
            });
            flxHeaderUserDropdown.add(flxMainHeaderUserDropdown);
            flxRightPanel.add(flxMainHeader, flxHeaderDropdown, flxMainContent, flxNote, flxHeaderUserDropdown);
            var flxToastMessage = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "bottom": "0%",
                "clipBounds": true,
                "height": "8%",
                "id": "flxToastMessage",
                "isVisible": false,
                "layoutType": kony.flex.FREE_FORM,
                "left": "305px",
                "isModalContainer": false,
                "skin": "slFbox",
                "width": "78%",
                "zIndex": 1
            }, {}, {});
            flxToastMessage.setDefaultUnit(kony.flex.DP);
            var toastMessage = new com.adminConsole.common.toastMessage({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "id": "toastMessage",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "masterType": constants.MASTER_TYPE_DEFAULT,
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0dp",
                "overrides": {}
            }, {
                "overrides": {}
            }, {
                "overrides": {}
            });
            flxToastMessage.add(toastMessage);
            var flxLoading = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "100%",
                "id": "flxLoading",
                "isVisible": false,
                "layoutType": kony.flex.FREE_FORM,
                "left": "305px",
                "isModalContainer": false,
                "right": "0px",
                "skin": "sknLoadingBlur",
                "top": "0dp",
                "zIndex": 15
            }, {}, {});
            flxLoading.setDefaultUnit(kony.flex.DP);
            var flxImageContainer = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "centerX": "50%",
                "centerY": "50%",
                "clipBounds": true,
                "height": "75px",
                "id": "flxImageContainer",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "isModalContainer": false,
                "skin": "slFbox",
                "width": "75px",
                "zIndex": 1
            }, {}, {});
            flxImageContainer.setDefaultUnit(kony.flex.DP);
            var imgLoading = new kony.ui.Image2({
                "centerX": "50%",
                "centerY": "50%",
                "height": "50px",
                "id": "imgLoading",
                "isVisible": true,
                "skin": "slImage",
                "src": "loadingscreenimage.gif",
                "width": "50px",
                "zIndex": 1
            }, {
                "imageScaleMode": constants.IMAGE_SCALE_MODE_MAINTAIN_ASPECT_RATIO,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxImageContainer.add(imgLoading);
            flxLoading.add(flxImageContainer);
            var flxViewLeadsFilters = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": false,
                "height": "120px",
                "id": "flxViewLeadsFilters",
                "isVisible": false,
                "layoutType": kony.flex.FREE_FORM,
                "isModalContainer": false,
                "right": "55px",
                "skin": "slFbox",
                "top": "260px",
                "width": "750px",
                "zIndex": 2
            }, {}, {});
            flxViewLeadsFilters.setDefaultUnit(kony.flex.DP);
            var flxMainContainer = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "100%",
                "id": "flxMainContainer",
                "isVisible": true,
                "layoutType": kony.flex.FLOW_VERTICAL,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "sknBackgroundFFFFFF",
                "top": "0dp",
                "width": "100%",
                "zIndex": 40
            }, {}, {});
            flxMainContainer.setDefaultUnit(kony.flex.DP);
            var flxUpper = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "40%",
                "id": "flxUpper",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0dp",
                "width": "100%",
                "zIndex": 1
            }, {}, {});
            flxUpper.setDefaultUnit(kony.flex.DP);
            var lblFIlterDataBy = new kony.ui.Label({
                "id": "lblFIlterDataBy",
                "isVisible": true,
                "left": "20px",
                "skin": "sknLbl485C75LatoRegular14px",
                "text": "Filter Data By",
                "top": "15px",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var flxCloseSearchFilter = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "centerY": "50%",
                "clipBounds": true,
                "height": "30px",
                "id": "flxCloseSearchFilter",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "isModalContainer": false,
                "right": 15,
                "skin": "sknCursor",
                "width": "30px",
                "zIndex": 1
            }, {}, {});
            flxCloseSearchFilter.setDefaultUnit(kony.flex.DP);
            var imgCloseSearchFilter = new kony.ui.Image2({
                "centerX": "50%",
                "centerY": "50%",
                "height": "15dp",
                "id": "imgCloseSearchFilter",
                "isVisible": true,
                "skin": "slImage",
                "src": "close_blue.png",
                "width": "15dp",
                "zIndex": 1
            }, {
                "imageScaleMode": constants.IMAGE_SCALE_MODE_MAINTAIN_ASPECT_RATIO,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxCloseSearchFilter.add(imgCloseSearchFilter);
            flxUpper.add(lblFIlterDataBy, flxCloseSearchFilter);
            var flxMiddle = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "60%",
                "id": "flxMiddle",
                "isVisible": true,
                "layoutType": kony.flex.FLOW_HORIZONTAL,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0dp",
                "width": "100%",
                "zIndex": 1
            }, {}, {});
            flxMiddle.setDefaultUnit(kony.flex.DP);
            var flxProduct = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                "clipBounds": false,
                "id": "flxProduct",
                "isVisible": true,
                "layoutType": kony.flex.FLOW_VERTICAL,
                "left": "20px",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0dp",
                "width": "180dp",
                "zIndex": 1
            }, {}, {});
            flxProduct.setDefaultUnit(kony.flex.DP);
            var lblProduct = new kony.ui.Label({
                "id": "lblProduct",
                "isVisible": true,
                "left": "0dp",
                "skin": "sknLbl485C75LatoRegular13Px",
                "text": "Product",
                "top": "0px",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var listBoxProduct = new kony.ui.ListBox({
                "focusSkin": "defListBoxFocus",
                "height": "30dp",
                "id": "listBoxProduct",
                "isVisible": true,
                "left": "0dp",
                "masterData": [
                    ["lb1", "Placeholder One"],
                    ["lb2", "Placeholder Two"],
                    ["lb3", "Placeholder Three"]
                ],
                "skin": "sknlstbxNormal0f9abd8e88aa64a",
                "top": "5dp",
                "width": "100%",
                "zIndex": 40
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [3, 0, 0, 0],
                "paddingInPixel": false
            }, {
                "multiSelect": false
            });
            flxProduct.add(lblProduct, listBoxProduct);
            var flxCustomerType = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                "clipBounds": false,
                "id": "flxCustomerType",
                "isVisible": true,
                "layoutType": kony.flex.FLOW_VERTICAL,
                "left": "20px",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0dp",
                "width": "180dp",
                "zIndex": 1
            }, {}, {});
            flxCustomerType.setDefaultUnit(kony.flex.DP);
            var lblCustomerType = new kony.ui.Label({
                "id": "lblCustomerType",
                "isVisible": true,
                "left": "0dp",
                "skin": "sknLbl485C75LatoRegular13Px",
                "text": "Customer Type",
                "top": "0px",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var lisyBoxCustomerType = new kony.ui.ListBox({
                "focusSkin": "defListBoxFocus",
                "height": "30dp",
                "id": "lisyBoxCustomerType",
                "isVisible": true,
                "left": "0dp",
                "masterData": [
                    ["lb1", "Placeholder One"],
                    ["lb2", "Placeholder Two"],
                    ["lb3", "Placeholder Three"]
                ],
                "skin": "sknlstbxNormal0f9abd8e88aa64a",
                "top": "5dp",
                "width": "100%",
                "zIndex": 40
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [3, 0, 0, 0],
                "paddingInPixel": false
            }, {
                "multiSelect": false
            });
            flxCustomerType.add(lblCustomerType, lisyBoxCustomerType);
            var flxModifiedOn = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                "clipBounds": false,
                "id": "flxModifiedOn",
                "isVisible": true,
                "layoutType": kony.flex.FLOW_VERTICAL,
                "left": "20px",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0dp",
                "width": "180dp",
                "zIndex": 1
            }, {}, {});
            flxModifiedOn.setDefaultUnit(kony.flex.DP);
            var lblModifiedOn = new kony.ui.Label({
                "id": "lblModifiedOn",
                "isVisible": true,
                "left": "0dp",
                "skin": "sknLbl485C75LatoRegular13Px",
                "text": "Modified On",
                "top": "0px",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var flxDatePicker = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": false,
                "height": "30px",
                "id": "flxDatePicker",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "skntxtbxNormald7d9e0",
                "top": "5dp",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {}, {});
            flxDatePicker.setDefaultUnit(kony.flex.DP);
            var datePicker = new kony.ui.CustomWidget({
                "id": "datePicker",
                "isVisible": true,
                "left": "2px",
                "top": "2px",
                "width": "99%",
                "height": "26px",
                "zIndex": 10,
                "clipBounds": true
            }, {
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {
                "widgetName": "dateRangePicker",
                "drops": "left",
                "event": null,
                "opens": "left",
                "rangeType": "",
                "resetData": null,
                "type": "list",
                "value": ""
            });
            flxDatePicker.add(datePicker);
            flxModifiedOn.add(lblModifiedOn, flxDatePicker);
            var flxAssignedTo = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                "clipBounds": false,
                "id": "flxAssignedTo",
                "isVisible": false,
                "layoutType": kony.flex.FLOW_VERTICAL,
                "left": "20px",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0dp",
                "width": "180dp",
                "zIndex": 1
            }, {}, {});
            flxAssignedTo.setDefaultUnit(kony.flex.DP);
            var lblAssignedTo = new kony.ui.Label({
                "id": "lblAssignedTo",
                "isVisible": true,
                "left": "0dp",
                "skin": "sknLbl485C75LatoRegular13Px",
                "text": "Assigned to",
                "top": "0px",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var txtAssignedTo = new kony.ui.TextBox2({
                "autoCapitalize": constants.TEXTBOX_AUTO_CAPITALIZE_NONE,
                "focusSkin": "defTextBoxFocus",
                "height": "30dp",
                "id": "txtAssignedTo",
                "isVisible": true,
                "keyBoardStyle": constants.TEXTBOX_KEY_BOARD_STYLE_DEFAULT,
                "left": "0dp",
                "placeholder": "Select CSR Name",
                "secureTextEntry": false,
                "skin": "sknTbxFFFFFFBorDEDEDE13pxKA",
                "textInputMode": constants.TEXTBOX_INPUT_MODE_ANY,
                "top": "5dp",
                "width": "100%",
                "zIndex": 1
            }, {
                "containerHeightMode": constants.TEXTBOX_FONT_METRICS_DRIVEN_HEIGHT,
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [3, 0, 0, 0],
                "paddingInPixel": false
            }, {
                "autoCorrect": false,
                "placeholderSkin": "defTextBoxPlaceholder"
            });
            flxAssignedTo.add(lblAssignedTo, txtAssignedTo);
            var btnApply = new kony.ui.Button({
                "bottom": "15px",
                "centerY": "50%",
                "focusSkin": "sknbtnf7f7faLatoRegular12Px485c75Border1px485C75Radius20px",
                "height": "22px",
                "id": "btnApply",
                "isVisible": true,
                "left": "30px",
                "right": "20px",
                "skin": "sknbtnf7f7faLatoRegular12Px485c75Border1px485C75Radius20px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmCustomerManagement.btnApply\")",
                "top": "5px",
                "width": "60px",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_CENTER,
                "displayText": true,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {
                "hoverSkin": "sknbtnffffffLatoRegular12Px485c75Border1px485C75Radius20px"
            });
            flxMiddle.add(flxProduct, flxCustomerType, flxModifiedOn, flxAssignedTo, btnApply);
            flxMainContainer.add(flxUpper, flxMiddle);
            flxViewLeadsFilters.add(flxMainContainer);
            flxMain.add(flxLeftPanel, flxRightPanel, flxToastMessage, flxLoading, flxViewLeadsFilters);
            this.add(flxMain);
        };
        return [{
            "addWidgets": addWidgetsfrmLeadManagement,
            "enabledForIdleTimeout": true,
            "id": "frmLeadManagement",
            "layoutType": kony.flex.FREE_FORM,
            "needAppMenu": false,
            "preShow": function(eventobject) {
                controller.AS_Form_cc40ff6b8bb948378365e0044cd33937(eventobject);
            },
            "skin": "slForm"
        }, {
            "displayOrientation": constants.FORM_DISPLAY_ORIENTATION_PORTRAIT,
            "layoutType": kony.flex.FREE_FORM,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, {
            "onDeviceBack": controller.AS_Form_gf91e2d95def4df6bf4baf24f5078107,
            "retainScrollPosition": false
        }]
    }
});