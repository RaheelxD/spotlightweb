define("flxServicesAndFaq", function() {
    return function(controller) {
        var flxServicesAndFaq = new kony.ui.FlexContainer({
            "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
            "clipBounds": true,
            "focusSkin": "sknflxffffffop100",
            "id": "flxServicesAndFaq",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "isModalContainer": false,
            "skin": "sknflxffffffop100"
        }, {}, {
            "hoverSkin": "sknfbfcfc"
        });
        flxServicesAndFaq.setDefaultUnit(kony.flex.DP);
        var flxServicesAndFaqLeft = new kony.ui.FlexContainer({
            "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
            "clipBounds": true,
            "id": "flxServicesAndFaqLeft",
            "isVisible": true,
            "layoutType": kony.flex.FLOW_HORIZONTAL,
            "left": "0dp",
            "isModalContainer": false,
            "skin": "slFbox",
            "top": "0dp",
            "width": "30%",
            "zIndex": 2
        }, {}, {});
        flxServicesAndFaqLeft.setDefaultUnit(kony.flex.DP);
        var flxCheckbox = new kony.ui.FlexContainer({
            "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
            "bottom": "20dp",
            "clipBounds": true,
            "id": "flxCheckbox",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "left": "20px",
            "isModalContainer": false,
            "onClick": controller.AS_FlexContainer_b51794a1f214465d8feae53c30c9c5ce,
            "skin": "sknCursor",
            "top": "20dp",
            "width": "30px",
            "zIndex": 2
        }, {}, {});
        flxCheckbox.setDefaultUnit(kony.flex.DP);
        var imgCheckBox = new kony.ui.Image2({
            "height": "12px",
            "id": "imgCheckBox",
            "isVisible": true,
            "left": "0px",
            "skin": "slImage",
            "src": "checkbox.png",
            "top": "0dp",
            "width": "12px",
            "zIndex": 2
        }, {
            "imageScaleMode": constants.IMAGE_SCALE_MODE_MAINTAIN_ASPECT_RATIO,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, {});
        flxCheckbox.add(imgCheckBox);
        var flxDropdown = new kony.ui.FlexContainer({
            "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
            "bottom": "20dp",
            "clipBounds": true,
            "id": "flxDropdown",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "left": "5px",
            "isModalContainer": false,
            "onClick": controller.AS_FlexContainer_b07e8d8dc1c6407d991f7ee4e2631ee1,
            "right": "10px",
            "skin": "sknCursor",
            "top": "20dp",
            "width": "15px",
            "zIndex": 2
        }, {}, {});
        flxDropdown.setDefaultUnit(kony.flex.DP);
        var ImgArrow = new kony.ui.Image2({
            "height": "12px",
            "id": "ImgArrow",
            "isVisible": false,
            "left": "0dp",
            "skin": "slImage",
            "src": "img_desc_arrow.png",
            "top": "0dp",
            "width": "12px",
            "zIndex": 2
        }, {
            "imageScaleMode": constants.IMAGE_SCALE_MODE_MAINTAIN_ASPECT_RATIO,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, {});
        var fonticonArrow = new kony.ui.Label({
            "height": "15px",
            "id": "fonticonArrow",
            "isVisible": true,
            "left": "0dp",
            "skin": "sknLblIIcoMoon485c7514px",
            "text": "",
            "top": "0dp",
            "width": "15px",
            "zIndex": 1
        }, {
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, {});
        flxDropdown.add(ImgArrow, fonticonArrow);
        var lblServiceName = new kony.ui.Label({
            "bottom": "20dp",
            "id": "lblServiceName",
            "isVisible": true,
            "left": "10px",
            "skin": "sknlblLatoReg485c7513px",
            "text": "Jompay",
            "top": "20dp",
            "width": "150px",
            "zIndex": 1
        }, {
            "contentAlignment": constants.CONTENT_ALIGN_TOP_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, {});
        flxServicesAndFaqLeft.add(flxCheckbox, flxDropdown, lblServiceName);
        var lblDescription = new kony.ui.Label({
            "bottom": "20dp",
            "id": "lblDescription",
            "isVisible": true,
            "left": "29%",
            "skin": "sknlblLatoReg485c7513px",
            "text": "Mobile App",
            "top": "20dp",
            "width": "33%",
            "zIndex": 1
        }, {
            "contentAlignment": constants.CONTENT_ALIGN_TOP_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, {});
        var lblCategory = new kony.ui.Label({
            "bottom": "20dp",
            "id": "lblCategory",
            "isVisible": true,
            "left": "63.50%",
            "skin": "sknlblLatoReg485c7513px",
            "text": "Mobile App",
            "top": "20dp",
            "width": "15%",
            "zIndex": 1
        }, {
            "contentAlignment": constants.CONTENT_ALIGN_TOP_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, {});
        var flxStatus = new kony.ui.FlexContainer({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "bottom": "20dp",
            "clipBounds": true,
            "height": "20px",
            "id": "flxStatus",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "isModalContainer": false,
            "right": "70dp",
            "skin": "slFbox",
            "top": "20dp",
            "width": "120px",
            "zIndex": 1
        }, {}, {});
        flxStatus.setDefaultUnit(kony.flex.DP);
        var fonticonActive = new kony.ui.Label({
            "centerY": "50%",
            "id": "fonticonActive",
            "isVisible": true,
            "left": "0dp",
            "skin": "sknFontIconActivate",
            "text": "",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 2
        }, {
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, {});
        var lblServiceStatus = new kony.ui.Label({
            "centerY": "50%",
            "id": "lblServiceStatus",
            "isVisible": true,
            "left": "16px",
            "skin": "sknlblLatoReg485c7513px",
            "text": "Active",
            "width": "60dp",
            "zIndex": 1
        }, {
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, {});
        flxStatus.add(fonticonActive, lblServiceStatus);
        var flxOptions = new kony.ui.FlexContainer({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "clipBounds": true,
            "height": "25px",
            "id": "flxOptions",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "isModalContainer": false,
            "onClick": controller.AS_FlexContainer_h836184d14614fbdbbceef34dd6f8cf9,
            "right": "40px",
            "skin": "sknFlxBorffffff1pxRound",
            "top": "15dp",
            "width": "25px",
            "zIndex": 2
        }, {}, {
            "hoverSkin": "sknflxffffffop100Border424242Radius100px"
        });
        flxOptions.setDefaultUnit(kony.flex.DP);
        var lblIconOptions = new kony.ui.Label({
            "centerX": "50%",
            "centerY": "50%",
            "height": "15px",
            "id": "lblIconOptions",
            "isVisible": true,
            "left": "14dp",
            "skin": "sknFontIconOptionMenu",
            "text": "",
            "width": "15px",
            "zIndex": 1
        }, {
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, {});
        var imgOptions = new kony.ui.Image2({
            "centerX": "50%",
            "centerY": "50%",
            "height": "12dp",
            "id": "imgOptions",
            "isVisible": false,
            "left": 14,
            "onTouchStart": controller.AS_Image_cf7888fc35544d6fafff46b136d5d4b0,
            "skin": "slImage",
            "src": "dots3x.png",
            "width": "6dp",
            "zIndex": 1
        }, {
            "imageScaleMode": constants.IMAGE_SCALE_MODE_MAINTAIN_ASPECT_RATIO,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, {});
        flxOptions.add(lblIconOptions, imgOptions);
        var lblSeparator = new kony.ui.Label({
            "bottom": "0px",
            "centerX": "50%",
            "height": "1dp",
            "id": "lblSeparator",
            "isVisible": true,
            "left": "20dp",
            "right": "20dp",
            "skin": "sknlblSeperatorD7D9E0",
            "text": "Label",
            "zIndex": 2
        }, {
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, {});
        flxServicesAndFaq.add(flxServicesAndFaqLeft, lblDescription, lblCategory, flxStatus, flxOptions, lblSeparator);
        return flxServicesAndFaq;
    }
})