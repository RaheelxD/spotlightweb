define("flxViewEntitlements", function() {
    return function(controller) {
        var flxViewEntitlements = new kony.ui.FlexContainer({
            "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
            "clipBounds": true,
            "id": "flxViewEntitlements",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "isModalContainer": false,
            "skin": "sknflxffffffop100"
        }, {}, {
            "hoverSkin": "sknfbfcfc"
        });
        flxViewEntitlements.setDefaultUnit(kony.flex.DP);
        var lblEntitlementsName = new kony.ui.Label({
            "bottom": "7px",
            "id": "lblEntitlementsName",
            "isVisible": true,
            "left": "20px",
            "maxWidth": "14%",
            "skin": "sknlblLatoBold35475f14px",
            "text": "Jompay",
            "top": "15px",
            "width": "23%",
            "zIndex": 1
        }, {
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, {});
        var lblEntitlementsDescription = new kony.ui.Label({
            "bottom": "17px",
            "id": "lblEntitlementsDescription",
            "isVisible": true,
            "left": "30%",
            "maxWidth": "65%",
            "skin": "sknlblLatoBold35475f14px",
            "text": "Lorem ipsum dolor sit amet,Lorem ipsum dolor sit amet, consectetur adipiscing elit.  cing elit.Lorem ipsum dolor sit amet, consectetur. Lorem ipsum dolor sit amet, consectetur adipiscing elit.",
            "top": "15px",
            "width": "63%",
            "zIndex": 1
        }, {
            "contentAlignment": constants.CONTENT_ALIGN_TOP_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, {});
        var flxEntitlementsConfigure = new kony.ui.FlexContainer({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "clipBounds": true,
            "height": "40px",
            "id": "flxEntitlementsConfigure",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "isModalContainer": false,
            "onClick": controller.AS_FlexContainer_fb895631855f434eac40b66293684b9b,
            "right": "3%",
            "skin": "slFbox",
            "top": "7px",
            "width": "50px",
            "zIndex": 1
        }, {}, {});
        flxEntitlementsConfigure.setDefaultUnit(kony.flex.DP);
        var imgConfigure = new kony.ui.Image2({
            "height": "15dp",
            "id": "imgConfigure",
            "isVisible": true,
            "left": "15px",
            "skin": "slImage",
            "src": "configure2x.png",
            "top": "10px",
            "width": "15dp",
            "zIndex": 1
        }, {
            "imageScaleMode": constants.IMAGE_SCALE_MODE_MAINTAIN_ASPECT_RATIO,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, {
            "toolTip": "Configure"
        });
        flxEntitlementsConfigure.add(imgConfigure);
        var lblEntitlementsSeperator = new kony.ui.Label({
            "height": "1px",
            "id": "lblEntitlementsSeperator",
            "isVisible": true,
            "left": "0dp",
            "skin": "sknlblSeperator",
            "text": ".",
            "top": "0dp",
            "width": "100%",
            "zIndex": 1
        }, {
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, {});
        flxViewEntitlements.add(lblEntitlementsName, lblEntitlementsDescription, flxEntitlementsConfigure, lblEntitlementsSeperator);
        return flxViewEntitlements;
    }
})