define("userflxEnrollSelectedAccountsSecController", {
    //Type your controller code here 
});
define("flxEnrollSelectedAccountsSecControllerActions", {
    /*
      This is an auto generated file and any modifications to it may result in corruption of the action sequence.
    */
    /** onClick defined for flxClose **/
    AS_FlexContainer_a25f6bfc05774d68992907013d824b88: function AS_FlexContainer_a25f6bfc05774d68992907013d824b88(eventobject, context) {
        var self = this;
        this.executeOnParent("unSelectedOption");
    }
});
define("flxEnrollSelectedAccountsSecController", ["userflxEnrollSelectedAccountsSecController", "flxEnrollSelectedAccountsSecControllerActions"], function() {
    var controller = require("userflxEnrollSelectedAccountsSecController");
    var controllerActions = ["flxEnrollSelectedAccountsSecControllerActions"];
    return kony.visualizer.mixinControllerActions(controller, controllerActions);
});
