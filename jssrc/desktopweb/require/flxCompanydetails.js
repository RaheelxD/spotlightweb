define("flxCompanydetails", function() {
    return function(controller) {
        var flxCompanydetails = new kony.ui.FlexContainer({
            "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
            "clipBounds": true,
            "id": "flxCompanydetails",
            "isVisible": true,
            "layoutType": kony.flex.FLOW_VERTICAL,
            "left": "0dp",
            "isModalContainer": false,
            "skin": "sknBackgroundFFFFFF",
            "top": "0dp",
            "width": "100%"
        }, {}, {});
        flxCompanydetails.setDefaultUnit(kony.flex.DP);
        var flxSectionHeader = new kony.ui.FlexContainer({
            "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
            "clipBounds": true,
            "id": "flxSectionHeader",
            "isVisible": true,
            "layoutType": kony.flex.FLOW_VERTICAL,
            "left": "0dp",
            "isModalContainer": false,
            "skin": "sknBackgroundFFFFFF",
            "top": "0dp",
            "width": "100%",
            "zIndex": 1
        }, {}, {});
        flxSectionHeader.setDefaultUnit(kony.flex.DP);
        var flxCompany = new kony.ui.FlexContainer({
            "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
            "clipBounds": true,
            "id": "flxCompany",
            "isVisible": true,
            "layoutType": kony.flex.FLOW_VERTICAL,
            "left": "0dp",
            "isModalContainer": false,
            "skin": "sknFlxffffffWithOutBorder",
            "top": "0dp",
            "width": "100%",
            "zIndex": 1
        }, {}, {});
        flxCompany.setDefaultUnit(kony.flex.DP);
        var flxCompanyname = new kony.ui.FlexContainer({
            "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
            "clipBounds": true,
            "id": "flxCompanyname",
            "isVisible": true,
            "layoutType": kony.flex.FLOW_HORIZONTAL,
            "left": "0dp",
            "isModalContainer": false,
            "skin": "slFbox",
            "top": "0dp",
            "width": "100%",
            "zIndex": 1
        }, {}, {});
        flxCompanyname.setDefaultUnit(kony.flex.DP);
        var lblCompanyName = new kony.ui.Label({
            "id": "lblCompanyName",
            "isVisible": true,
            "left": "20dp",
            "skin": "sknLbl117EB0LatoReg14px",
            "text": "Temenos Corporation Limited",
            "top": "20dp",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, {
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, {});
        var flxPrimary = new kony.ui.FlexContainer({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "clipBounds": true,
            "height": "20dp",
            "id": "flxPrimary",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "left": "12dp",
            "isModalContainer": false,
            "skin": "sknflx1F844DRadius20px",
            "top": "20dp",
            "width": "80dp",
            "zIndex": 1
        }, {}, {});
        flxPrimary.setDefaultUnit(kony.flex.DP);
        var lblPrimary = new kony.ui.Label({
            "centerX": "50%",
            "centerY": "50%",
            "height": "60dp",
            "id": "lblPrimary",
            "isVisible": true,
            "left": "10dp",
            "skin": "sknlblffffffLato12px",
            "text": "Primary",
            "zIndex": 1
        }, {
            "contentAlignment": constants.CONTENT_ALIGN_CENTER,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, {});
        flxPrimary.add(lblPrimary);
        flxCompanyname.add(lblCompanyName, flxPrimary);
        var flxCompanyDetailsView = new kony.ui.FlexContainer({
            "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
            "clipBounds": true,
            "id": "flxCompanyDetailsView",
            "isVisible": true,
            "layoutType": kony.flex.FLOW_HORIZONTAL,
            "left": "0dp",
            "isModalContainer": false,
            "skin": "slFbox",
            "top": "0dp",
            "width": "100%",
            "zIndex": 1
        }, {}, {});
        flxCompanyDetailsView.setDefaultUnit(kony.flex.DP);
        var flxCustomerID = new kony.ui.FlexContainer({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "bottom": "5dp",
            "clipBounds": true,
            "height": "50dp",
            "id": "flxCustomerID",
            "isVisible": true,
            "layoutType": kony.flex.FLOW_VERTICAL,
            "left": "0dp",
            "isModalContainer": false,
            "skin": "slFbox",
            "top": "5dp",
            "width": "30%",
            "zIndex": 1
        }, {}, {});
        flxCustomerID.setDefaultUnit(kony.flex.DP);
        var lblCustomerID = new kony.ui.Label({
            "id": "lblCustomerID",
            "isVisible": true,
            "left": "20dp",
            "skin": "sknlblLato696c7313px",
            "text": "CUSTOMERID",
            "top": "10dp",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, {
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, {});
        var lblCustomerIDVal = new kony.ui.Label({
            "id": "lblCustomerIDVal",
            "isVisible": true,
            "left": "20dp",
            "skin": "sknLatoSemibold485c7313px",
            "text": "12345677687",
            "top": "5dp",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, {
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, {});
        flxCustomerID.add(lblCustomerID, lblCustomerIDVal);
        var flxTaxID = new kony.ui.FlexContainer({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "bottom": "5dp",
            "clipBounds": true,
            "height": "50dp",
            "id": "flxTaxID",
            "isVisible": true,
            "layoutType": kony.flex.FLOW_VERTICAL,
            "left": "0dp",
            "isModalContainer": false,
            "skin": "slFbox",
            "top": "5dp",
            "width": "25%",
            "zIndex": 1
        }, {}, {});
        flxTaxID.setDefaultUnit(kony.flex.DP);
        var lblTaxiDTitle = new kony.ui.Label({
            "id": "lblTaxiDTitle",
            "isVisible": true,
            "left": "10dp",
            "skin": "sknlblLato696c7313px",
            "text": "TAX ID",
            "top": "10dp",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, {
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, {});
        var lblTaxiDValue = new kony.ui.Label({
            "id": "lblTaxiDValue",
            "isVisible": true,
            "left": "10dp",
            "skin": "sknLatoSemibold485c7313px",
            "text": "12345677687",
            "top": "5dp",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, {
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, {});
        flxTaxID.add(lblTaxiDTitle, lblTaxiDValue);
        var flxAddress = new kony.ui.FlexContainer({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "bottom": "5dp",
            "clipBounds": true,
            "height": "50dp",
            "id": "flxAddress",
            "isVisible": true,
            "layoutType": kony.flex.FLOW_VERTICAL,
            "left": "0dp",
            "isModalContainer": false,
            "skin": "slFbox",
            "top": "5dp",
            "width": "25%",
            "zIndex": 1
        }, {}, {});
        flxAddress.setDefaultUnit(kony.flex.DP);
        var lblAddressTitle = new kony.ui.Label({
            "id": "lblAddressTitle",
            "isVisible": true,
            "left": "10dp",
            "skin": "sknlblLato696c7313px",
            "text": "ADDRESS",
            "top": "10dp",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, {
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, {});
        var lblAddressVal = new kony.ui.Label({
            "id": "lblAddressVal",
            "isVisible": true,
            "left": "10dp",
            "skin": "sknLatoSemibold485c7313px",
            "text": "Malvern, United States",
            "top": "5dp",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, {
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, {});
        flxAddress.add(lblAddressTitle, lblAddressVal);
        flxCompanyDetailsView.add(flxCustomerID, flxTaxID, flxAddress);
        flxCompany.add(flxCompanyname, flxCompanyDetailsView);
        var flxAccounts = new kony.ui.FlexContainer({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "clipBounds": true,
            "height": "45dp",
            "id": "flxAccounts",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "left": "0dp",
            "isModalContainer": false,
            "skin": "sknFlxbgF8F9FAbdrD7D9E01px",
            "top": "0dp",
            "width": "100%",
            "zIndex": 1
        }, {}, {});
        flxAccounts.setDefaultUnit(kony.flex.DP);
        var flxArrow = new kony.ui.FlexContainer({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "centerY": "50%",
            "clipBounds": true,
            "height": "17dp",
            "id": "flxArrow",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "left": "15dp",
            "isModalContainer": false,
            "skin": "slFbox",
            "top": "0dp",
            "width": "20dp",
            "zIndex": 1
        }, {}, {});
        flxArrow.setDefaultUnit(kony.flex.DP);
        var imgArrow = new kony.ui.Image2({
            "centerX": "50%",
            "centerY": "50%",
            "height": "10dp",
            "id": "imgArrow",
            "isVisible": true,
            "left": "0dp",
            "skin": "slImage",
            "src": "img_down_arrow.png",
            "top": "0dp",
            "width": "17dp",
            "zIndex": 1
        }, {
            "imageScaleMode": constants.IMAGE_SCALE_MODE_FIT_TO_DIMENSIONS,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, {});
        flxArrow.add(imgArrow);
        var lblAccountCounts = new kony.ui.Label({
            "centerY": "50%",
            "id": "lblAccountCounts",
            "isVisible": true,
            "left": "37dp",
            "skin": "sknLbl192B45LatoRegular14px",
            "text": "Accounts (45)",
            "top": "0dp",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, {
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, {});
        flxAccounts.add(flxArrow, lblAccountCounts);
        var flxAccountHeaderSection = new kony.ui.FlexContainer({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "clipBounds": true,
            "height": "52dp",
            "id": "flxAccountHeaderSection",
            "isVisible": true,
            "layoutType": kony.flex.FLOW_VERTICAL,
            "left": "0dp",
            "isModalContainer": false,
            "skin": "sknBackgroundFFFFFF",
            "top": "0dp",
            "width": "100%",
            "zIndex": 1
        }, {}, {});
        flxAccountHeaderSection.setDefaultUnit(kony.flex.DP);
        var flxCompanyAccounts = new kony.ui.FlexContainer({
            "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
            "clipBounds": true,
            "id": "flxCompanyAccounts",
            "isVisible": true,
            "layoutType": kony.flex.FLOW_HORIZONTAL,
            "left": "0dp",
            "isModalContainer": false,
            "skin": "sknBackgroundFFFFFF",
            "top": "0dp",
            "width": "100%",
            "zIndex": 1
        }, {}, {});
        flxCompanyAccounts.setDefaultUnit(kony.flex.DP);
        var flxAccountNum = new kony.ui.FlexContainer({
            "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
            "clipBounds": true,
            "id": "flxAccountNum",
            "isVisible": true,
            "layoutType": kony.flex.FLOW_HORIZONTAL,
            "left": "0dp",
            "isModalContainer": false,
            "skin": "slFbox",
            "top": "0dp",
            "width": "22.50%",
            "zIndex": 1
        }, {}, {});
        flxAccountNum.setDefaultUnit(kony.flex.DP);
        var lblAccountNumber = new kony.ui.Label({
            "bottom": "15dp",
            "id": "lblAccountNumber",
            "isVisible": true,
            "left": "35dp",
            "skin": "sknlblLatoBold35475f14px",
            "text": "ACCOUNT NUMBER",
            "top": "15dp",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, {
            "contentAlignment": constants.CONTENT_ALIGN_TOP_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, {});
        var lblAcntNumSorticon = new kony.ui.Label({
            "id": "lblAcntNumSorticon",
            "isVisible": true,
            "left": "5dp",
            "skin": "lblsorticonmoon",
            "text": "",
            "top": "15dp",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, {
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, {});
        flxAccountNum.add(lblAccountNumber, lblAcntNumSorticon);
        var flxAccountType = new kony.ui.FlexContainer({
            "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
            "clipBounds": true,
            "id": "flxAccountType",
            "isVisible": true,
            "layoutType": kony.flex.FLOW_HORIZONTAL,
            "left": "15dp",
            "isModalContainer": false,
            "skin": "slFbox",
            "top": "0dp",
            "width": "20%",
            "zIndex": 1
        }, {}, {});
        flxAccountType.setDefaultUnit(kony.flex.DP);
        var lblAccountType = new kony.ui.Label({
            "bottom": "15dp",
            "id": "lblAccountType",
            "isVisible": true,
            "left": "10dp",
            "right": "0px",
            "skin": "sknlblLatoBold35475f14px",
            "text": "ACCOUNT TYPE",
            "top": "15dp",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, {
            "contentAlignment": constants.CONTENT_ALIGN_TOP_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, {});
        var lblAcntTypeSorticon = new kony.ui.Label({
            "id": "lblAcntTypeSorticon",
            "isVisible": true,
            "left": "5dp",
            "skin": "lblsorticonmoon",
            "text": "",
            "top": "15dp",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, {
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, {});
        flxAccountType.add(lblAccountType, lblAcntTypeSorticon);
        var flxAccountName = new kony.ui.FlexContainer({
            "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
            "clipBounds": true,
            "id": "flxAccountName",
            "isVisible": true,
            "layoutType": kony.flex.FLOW_HORIZONTAL,
            "left": "0dp",
            "isModalContainer": false,
            "skin": "slFbox",
            "top": "0dp",
            "width": "20%",
            "zIndex": 1
        }, {}, {});
        flxAccountName.setDefaultUnit(kony.flex.DP);
        var lblAccountName = new kony.ui.Label({
            "bottom": "15dp",
            "id": "lblAccountName",
            "isVisible": true,
            "left": "15dp",
            "skin": "sknlblLatoBold35475f14px",
            "text": "ACCOUNT NAME",
            "top": "15dp",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, {
            "contentAlignment": constants.CONTENT_ALIGN_TOP_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, {});
        var lblAcntNameSorticon = new kony.ui.Label({
            "id": "lblAcntNameSorticon",
            "isVisible": true,
            "left": "5dp",
            "skin": "lblsorticonmoon",
            "text": "",
            "top": "15dp",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, {
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, {});
        flxAccountName.add(lblAccountName, lblAcntNameSorticon);
        var flxOwnershipType = new kony.ui.FlexContainer({
            "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
            "clipBounds": true,
            "id": "flxOwnershipType",
            "isVisible": true,
            "layoutType": kony.flex.FLOW_HORIZONTAL,
            "left": "0dp",
            "isModalContainer": false,
            "skin": "slFbox",
            "top": "0dp",
            "width": "18%",
            "zIndex": 1
        }, {}, {});
        flxOwnershipType.setDefaultUnit(kony.flex.DP);
        var lblOwnershipType = new kony.ui.Label({
            "bottom": "15dp",
            "id": "lblOwnershipType",
            "isVisible": true,
            "left": "10dp",
            "skin": "sknlblLatoBold35475f14px",
            "text": "OWNERSHIP TYPE",
            "top": "15dp",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, {
            "contentAlignment": constants.CONTENT_ALIGN_TOP_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, {});
        var lblAcntOwnershipSorticon = new kony.ui.Label({
            "id": "lblAcntOwnershipSorticon",
            "isVisible": true,
            "left": "5dp",
            "skin": "lblsorticonmoon",
            "text": "",
            "top": "15dp",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, {
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, {});
        flxOwnershipType.add(lblOwnershipType, lblAcntOwnershipSorticon);
        var flxStatus = new kony.ui.FlexContainer({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "clipBounds": true,
            "height": "40dp",
            "id": "flxStatus",
            "isVisible": true,
            "layoutType": kony.flex.FLOW_HORIZONTAL,
            "left": "0dp",
            "isModalContainer": false,
            "skin": "slFbox",
            "top": "0dp",
            "width": "15%",
            "zIndex": 1
        }, {}, {});
        flxStatus.setDefaultUnit(kony.flex.DP);
        var lblStatus = new kony.ui.Label({
            "bottom": "15dp",
            "id": "lblStatus",
            "isVisible": true,
            "left": "20dp",
            "skin": "sknlblLatoBold35475f14px",
            "text": "STATUS",
            "top": "15dp",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, {
            "contentAlignment": constants.CONTENT_ALIGN_TOP_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, {});
        var lblStatusSorticon = new kony.ui.Label({
            "id": "lblStatusSorticon",
            "isVisible": true,
            "left": "5dp",
            "skin": "lblsorticonmoon",
            "text": "",
            "top": "15dp",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, {
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, {});
        flxStatus.add(lblStatus, lblStatusSorticon);
        flxCompanyAccounts.add(flxAccountNum, flxAccountType, flxAccountName, flxOwnershipType, flxStatus);
        var flxSeparator = new kony.ui.FlexContainer({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "bottom": "10dp",
            "clipBounds": true,
            "height": "1px",
            "id": "flxSeparator",
            "isVisible": false,
            "layoutType": kony.flex.FREE_FORM,
            "left": "10dp",
            "isModalContainer": false,
            "right": "10dp",
            "skin": "sknFlx1pxBorder696C73",
            "width": "96%",
            "zIndex": 1
        }, {}, {});
        flxSeparator.setDefaultUnit(kony.flex.DP);
        flxSeparator.add();
        var lblSeperator = new kony.ui.Label({
            "bottom": "0dp",
            "height": "1px",
            "id": "lblSeperator",
            "isVisible": true,
            "left": "20dp",
            "right": "20dp",
            "skin": "sknLblSeparator12px",
            "text": ".",
            "width": "95.50%",
            "zIndex": 1
        }, {
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, {});
        flxAccountHeaderSection.add(flxCompanyAccounts, flxSeparator, lblSeperator);
        flxSectionHeader.add(flxCompany, flxAccounts, flxAccountHeaderSection);
        flxCompanydetails.add(flxSectionHeader);
        return flxCompanydetails;
    }
})