define("AlertsManagementModule/frmAlertsManagement", function() {
    return function(controller) {
        function addWidgetsfrmAlertsManagement() {
            this.setDefaultUnit(kony.flex.DP);
            var flxAlertsManagement = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "100%",
                "id": "flxAlertsManagement",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0.00%",
                "isModalContainer": false,
                "skin": "slFbox0b7aaa2e3bfa340",
                "top": "0.00%",
                "width": "100%",
                "zIndex": 1
            }, {}, {});
            flxAlertsManagement.setDefaultUnit(kony.flex.DP);
            var flxLeftPannel = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "100%",
                "id": "flxLeftPannel",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "masterType": constants.MASTER_TYPE_USERWIDGET,
                "isModalContainer": false,
                "skin": "sknLeftPannel0bb79e4bcca8e45",
                "top": "0dp",
                "width": "305px",
                "zIndex": 5
            }, {}, {});
            flxLeftPannel.setDefaultUnit(kony.flex.DP);
            var leftMenuNew = new com.adminConsole.navigation.leftMenuNew({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "height": "100%",
                "id": "leftMenuNew",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "masterType": constants.MASTER_TYPE_DEFAULT,
                "isModalContainer": false,
                "skin": "sknFlxBg003E75Op100",
                "top": "0dp",
                "width": "100%",
                "overrides": {
                    "brandLogo": {
                        "src": "infinity_dbx_c360_logo_white_2x.png"
                    },
                    "imgBottomLogo": {
                        "src": "logo_white_2x.png"
                    },
                    "leftMenuNew": {
                        "bottom": "viz.val_cleared",
                        "centerX": "viz.val_cleared",
                        "centerY": "viz.val_cleared",
                        "height": "100%",
                        "left": "0dp",
                        "maxHeight": "viz.val_cleared",
                        "maxWidth": "viz.val_cleared",
                        "minHeight": "viz.val_cleared",
                        "minWidth": "viz.val_cleared",
                        "right": "viz.val_cleared",
                        "top": "0dp",
                        "width": "100%"
                    }
                }
            }, {
                "overrides": {}
            }, {
                "overrides": {}
            });
            flxLeftPannel.add(leftMenuNew);
            var flxRightPanel = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "100%",
                "id": "flxRightPanel",
                "isVisible": true,
                "layoutType": kony.flex.FLOW_VERTICAL,
                "left": "305px",
                "masterType": constants.MASTER_TYPE_USERWIDGET,
                "isModalContainer": false,
                "right": 0,
                "skin": "slFbox0b7aaa2e3bfa340",
                "top": "0%",
                "zIndex": 1
            }, {}, {});
            flxRightPanel.setDefaultUnit(kony.flex.DP);
            var flxMainHeader = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "106px",
                "id": "flxMainHeader",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0px",
                "isModalContainer": false,
                "right": "0px",
                "skin": "slFbox",
                "top": "0px",
                "width": "100%",
                "zIndex": 1
            }, {}, {});
            flxMainHeader.setDefaultUnit(kony.flex.DP);
            var mainHeader = new com.adminConsole.header.mainHeader({
                "height": "106px",
                "id": "mainHeader",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "masterType": constants.MASTER_TYPE_DEFAULT,
                "isModalContainer": false,
                "skin": "CopyslFbox1",
                "width": "100%",
                "overrides": {
                    "btnAddNewOption": {
                        "isVisible": false,
                        "right": undefined
                    },
                    "btnDropdownList": {
                        "isVisible": false
                    },
                    "flxHeaderSeperator": {
                        "isVisible": false
                    },
                    "flxHeaderUserOptions": {
                        "isVisible": true
                    },
                    "imgLogout": {
                        "src": "img_logout.png"
                    },
                    "lblHeading": {
                        "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmAlertsManagementController.Alerts\")"
                    },
                    "mainHeader": {
                        "left": undefined,
                        "top": undefined
                    }
                }
            }, {
                "overrides": {}
            }, {
                "overrides": {}
            });
            var flxReorderCategoriestBtn = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "bottom": "5dp",
                "clipBounds": true,
                "height": "40px",
                "id": "flxReorderCategoriestBtn",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "isModalContainer": false,
                "right": "35dp",
                "skin": "sknServiceDetailsContent",
                "width": "75px",
                "zIndex": 1
            }, {}, {});
            flxReorderCategoriestBtn.setDefaultUnit(kony.flex.DP);
            var btnReorderCategories = new kony.ui.Button({
                "centerY": "50%",
                "height": "22px",
                "id": "btnReorderCategories",
                "isVisible": false,
                "left": "0dp",
                "skin": "sknbtnf7f7faLatoRegular12Px485c75Border1px485C75Radius20px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmAlertsManagement.Reorder\")",
                "width": "100%",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_CENTER,
                "displayText": true,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {
                "hoverSkin": "sknbtnffffffLatoRegular12Px485c75Border1px485C75Radius20px"
            });
            flxReorderCategoriestBtn.add(btnReorderCategories);
            flxMainHeader.add(mainHeader, flxReorderCategoriestBtn);
            var flxAlertsBreadCrumb = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "bottom": "9px",
                "clipBounds": true,
                "height": "20px",
                "id": "flxAlertsBreadCrumb",
                "isVisible": false,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0px",
                "isModalContainer": false,
                "right": "0px",
                "skin": "slFbox",
                "top": "-10px",
                "width": "100%",
                "zIndex": 1
            }, {}, {});
            flxAlertsBreadCrumb.setDefaultUnit(kony.flex.DP);
            var breadcrumbs = new com.adminConsole.common.breadcrumbs1({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "height": "20px",
                "id": "breadcrumbs",
                "isVisible": true,
                "layoutType": kony.flex.FLOW_HORIZONTAL,
                "left": "0px",
                "masterType": constants.MASTER_TYPE_DEFAULT,
                "isModalContainer": false,
                "right": "35px",
                "skin": "sknflxf5f6f8Op100",
                "top": "0",
                "zIndex": 10,
                "overrides": {
                    "breadcrumbs1": {
                        "bottom": "viz.val_cleared",
                        "centerX": "viz.val_cleared",
                        "centerY": "viz.val_cleared",
                        "height": "20px",
                        "left": "0px",
                        "maxHeight": "viz.val_cleared",
                        "maxWidth": "viz.val_cleared",
                        "minHeight": "viz.val_cleared",
                        "minWidth": "viz.val_cleared",
                        "right": "35px",
                        "top": "0",
                        "width": "viz.val_cleared",
                        "zIndex": 10
                    }
                }
            }, {
                "overrides": {}
            }, {
                "overrides": {}
            });
            flxAlertsBreadCrumb.add(breadcrumbs);
            var flxAlertConfiguration = new kony.ui.FlexScrollContainer({
                "allowHorizontalBounce": false,
                "allowVerticalBounce": true,
                "bounces": true,
                "clipBounds": true,
                "enableScrolling": true,
                "height": "750dp",
                "horizontalScrollIndicator": true,
                "id": "flxAlertConfiguration",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "pagingEnabled": false,
                "scrollDirection": kony.flex.SCROLL_VERTICAL,
                "skin": "slFSbox",
                "top": "0dp",
                "verticalScrollIndicator": true,
                "width": "100%",
                "zIndex": 1
            }, {}, {});
            flxAlertConfiguration.setDefaultUnit(kony.flex.DP);
            var flxAlertBoxContainer = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                "clipBounds": true,
                "id": "flxAlertBoxContainer",
                "isVisible": true,
                "layoutType": kony.flex.FLOW_VERTICAL,
                "left": "35dp",
                "isModalContainer": false,
                "right": 35,
                "skin": "slFbox",
                "top": "0dp"
            }, {}, {});
            flxAlertBoxContainer.setDefaultUnit(kony.flex.DP);
            var flxTabsAlerts = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "61dp",
                "id": "flxTabsAlerts",
                "isVisible": false,
                "layoutType": kony.flex.FLOW_VERTICAL,
                "left": "0dp",
                "isModalContainer": false,
                "top": "0dp",
                "width": "100%"
            }, {}, {});
            flxTabsAlerts.setDefaultUnit(kony.flex.DP);
            var flxTabButtons = new kony.ui.FlexContainer({
                "clipBounds": true,
                "height": "60px",
                "id": "flxTabButtons",
                "isVisible": true,
                "layoutType": kony.flex.FLOW_HORIZONTAL,
                "left": "20px",
                "isModalContainer": false,
                "preShow": function(eventobject) {
                    controller.AS_FlexContainer_b1a3158c443a4c42acb8d6a705fbc11f(eventobject);
                },
                "skin": "slFbox0hfd18814fd664dCM",
                "top": "0px",
                "width": "95%"
            }, {}, {});
            flxTabButtons.setDefaultUnit(kony.flex.DP);
            var btnTabName1 = new kony.ui.Button({
                "centerY": "50%",
                "height": "100%",
                "id": "btnTabName1",
                "isVisible": true,
                "left": "0dp",
                "right": "20px",
                "skin": "sknBtn9ca9ba12PxNoBorder",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmCustomerManagementController.CONTACT\")",
                "top": "19dp",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_CENTER,
                "displayText": true,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {
                "hoverSkin": "sknBtnLatoBold485c7512PxNoBorder"
            });
            var btnTabName2 = new kony.ui.Button({
                "centerY": "50%",
                "height": "100%",
                "id": "btnTabName2",
                "isVisible": true,
                "left": "0dp",
                "right": "20px",
                "skin": "sknBtn9ca9ba12PxNoBorder",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmCustomerManagementController.PRODUCTS\")",
                "top": "19dp",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_CENTER,
                "displayText": true,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {
                "hoverSkin": "sknBtnLatoBold485c7512PxNoBorder"
            });
            var btnTabName3 = new kony.ui.Button({
                "centerY": "50%",
                "height": "100%",
                "id": "btnTabName3",
                "isVisible": true,
                "left": "0dp",
                "right": "20px",
                "skin": "sknBtn9ca9ba12PxNoBorder",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmCustomerManagementController.HELP_CENTER\")",
                "top": "19dp",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_CENTER,
                "displayText": true,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {
                "hoverSkin": "sknBtnLatoBold485c7512PxNoBorder"
            });
            var btnTabName4 = new kony.ui.Button({
                "centerY": "50%",
                "height": "100%",
                "id": "btnTabName4",
                "isVisible": true,
                "left": "0dp",
                "right": "20px",
                "skin": "sknBtn9ca9ba12PxNoBorder",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmCustomerManagementController.Profile_tabs_CUSTOMER_Roles\")",
                "top": "19dp",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_CENTER,
                "displayText": true,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {
                "hoverSkin": "sknBtnLatoBold485c7512PxNoBorder"
            });
            var btnTabName5 = new kony.ui.Button({
                "centerY": "50%",
                "height": "100%",
                "id": "btnTabName5",
                "isVisible": false,
                "left": "0dp",
                "right": "20px",
                "skin": "sknBtn9ca9ba12PxNoBorder",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmCustomerManagementController.ACTIVITY_HISTORY\")",
                "top": "19dp",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_CENTER,
                "displayText": true,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {
                "hoverSkin": "sknBtnLatoBold485c7512PxNoBorder"
            });
            var btnTabName6 = new kony.ui.Button({
                "centerY": "50%",
                "height": "100%",
                "id": "btnTabName6",
                "isVisible": false,
                "left": "0dp",
                "right": "20px",
                "skin": "sknBtn9ca9ba12PxNoBorder",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.AlertsManagement.ALERT\")",
                "top": "19dp",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_CENTER,
                "displayText": true,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {
                "hoverSkin": "sknBtnLatoBold485c7512PxNoBorder"
            });
            var btnTabName7 = new kony.ui.Button({
                "centerY": "50%",
                "height": "100%",
                "id": "btnTabName7",
                "isVisible": false,
                "left": "0dp",
                "right": "20px",
                "skin": "sknBtn9ca9ba12PxNoBorder",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmCustomerManagementController.DEVICE_INFO\")",
                "top": "19dp",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_CENTER,
                "displayText": true,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {
                "hoverSkin": "sknBtnLatoBold485c7512PxNoBorder"
            });
            var btnTabName8 = new kony.ui.Button({
                "centerY": "50%",
                "height": "100%",
                "id": "btnTabName8",
                "isVisible": false,
                "left": "0dp",
                "minWidth": "160px",
                "right": "20px",
                "skin": "sknBtn9ca9ba12PxNoBorder",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmCustomerManagementController.Profile_tabs_CUSTOMER_Permissions\")",
                "top": "19dp",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_CENTER,
                "displayText": true,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {
                "hoverSkin": "sknBtnLatoBold485c7512PxNoBorder"
            });
            flxTabButtons.add(btnTabName1, btnTabName2, btnTabName3, btnTabName4, btnTabName5, btnTabName6, btnTabName7, btnTabName8);
            var flxAlertsTabsSeprator = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "bottom": "0dp",
                "clipBounds": true,
                "height": "1dp",
                "id": "flxAlertsTabsSeprator",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "sknSeparatorCsr",
                "width": "100%",
                "zIndex": 1
            }, {}, {});
            flxAlertsTabsSeprator.setDefaultUnit(kony.flex.DP);
            flxAlertsTabsSeprator.add();
            flxTabsAlerts.add(flxTabButtons, flxAlertsTabsSeprator);
            var flxAlertCategories = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                "clipBounds": true,
                "id": "flxAlertCategories",
                "isVisible": false,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "isModalContainer": false,
                "right": "0dp",
                "skin": "slFbox",
                "top": "0dp",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {}, {});
            flxAlertCategories.setDefaultUnit(kony.flex.DP);
            var flxAlertCategoriesList = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "400dp",
                "id": "flxAlertCategoriesList",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "sknFlxBgFFFFFFBorE4E6ECsz1pxR4px",
                "top": "0dp",
                "width": "100%",
                "zIndex": 1
            }, {}, {});
            flxAlertCategoriesList.setDefaultUnit(kony.flex.DP);
            var flxCategoryHeader = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "55dp",
                "id": "flxCategoryHeader",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0dp",
                "width": "100%",
                "zIndex": 1
            }, {}, {});
            flxCategoryHeader.setDefaultUnit(kony.flex.DP);
            var lblAlertCategoryHeading = new kony.ui.Label({
                "id": "lblAlertCategoryHeading",
                "isVisible": true,
                "left": "25dp",
                "skin": "sknLatoRegular192B4518px",
                "text": "Alert Categories",
                "top": "17dp",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var flxCategoryHeaderBtns = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "100%",
                "id": "flxCategoryHeaderBtns",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "isModalContainer": false,
                "right": "0dp",
                "skin": "sknServiceDetailsContent",
                "top": "0dp",
                "width": "40%",
                "zIndex": 1
            }, {}, {});
            flxCategoryHeaderBtns.setDefaultUnit(kony.flex.DP);
            var btnAddCategory = new kony.ui.Button({
                "centerY": "50%",
                "height": "22px",
                "id": "btnAddCategory",
                "isVisible": true,
                "right": "20dp",
                "skin": "sknbtnf7f7faLatoRegular12Px485c75Border1px485C75Radius20px",
                "text": "Add Category",
                "width": "103dp",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_CENTER,
                "displayText": true,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {
                "hoverSkin": "sknbtnffffffLatoRegular12Px485c75Border1px485C75Radius20px"
            });
            var btnCategoryReorder = new kony.ui.Button({
                "centerY": "50%",
                "height": "22px",
                "id": "btnCategoryReorder",
                "isVisible": true,
                "right": "142dp",
                "skin": "sknbtnf7f7faLatoRegular12Px485c75Border1px485C75Radius20px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmAlertsManagement.Reorder\")",
                "width": "72dp",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_CENTER,
                "displayText": true,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {
                "hoverSkin": "sknbtnffffffLatoRegular12Px485c75Border1px485C75Radius20px"
            });
            flxCategoryHeaderBtns.add(btnAddCategory, btnCategoryReorder);
            flxCategoryHeader.add(lblAlertCategoryHeading, flxCategoryHeaderBtns);
            var flxAlertCategoryScrollCont = new kony.ui.FlexScrollContainer({
                "allowHorizontalBounce": false,
                "allowVerticalBounce": true,
                "bounces": true,
                "clipBounds": true,
                "enableScrolling": true,
                "height": "500dp",
                "horizontalScrollIndicator": true,
                "id": "flxAlertCategoryScrollCont",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "pagingEnabled": false,
                "scrollDirection": kony.flex.SCROLL_VERTICAL,
                "skin": "slFSbox",
                "top": "55dp",
                "verticalScrollIndicator": true,
                "width": "100%"
            }, {}, {});
            flxAlertCategoryScrollCont.setDefaultUnit(kony.flex.DP);
            var flxAlertCategoryCards = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                "clipBounds": true,
                "id": "flxAlertCategoryCards",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0dp",
                "width": "100%",
                "zIndex": 1
            }, {}, {});
            flxAlertCategoryCards.setDefaultUnit(kony.flex.DP);
            flxAlertCategoryCards.add();
            flxAlertCategoryScrollCont.add(flxAlertCategoryCards);
            flxAlertCategoriesList.add(flxCategoryHeader, flxAlertCategoryScrollCont);
            var flxAlertCategoryDetailsScreen = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                "clipBounds": true,
                "id": "flxAlertCategoryDetailsScreen",
                "isVisible": false,
                "layoutType": kony.flex.FLOW_VERTICAL,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0dp",
                "width": "100%",
                "zIndex": 1
            }, {}, {});
            flxAlertCategoryDetailsScreen.setDefaultUnit(kony.flex.DP);
            var flxAlertCategoryContainer = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                "clipBounds": true,
                "id": "flxAlertCategoryContainer",
                "isVisible": true,
                "layoutType": kony.flex.FLOW_VERTICAL,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "sknFlxBgFFFFFFBorE4E6ECsz1pxR4px",
                "top": "0dp",
                "width": "100%",
                "zIndex": 2
            }, {}, {});
            flxAlertCategoryContainer.setDefaultUnit(kony.flex.DP);
            var flxAlertCategoryMessage = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "bottom": "5px",
                "clipBounds": true,
                "height": "35px",
                "id": "flxAlertCategoryMessage",
                "isVisible": false,
                "layoutType": kony.flex.FREE_FORM,
                "left": "20px",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "20px",
                "width": "95.50%",
                "zIndex": 1
            }, {}, {});
            flxAlertCategoryMessage.setDefaultUnit(kony.flex.DP);
            var alertCategoryMessage = new com.adminConsole.alertMang.alertMessage({
                "height": "35px",
                "id": "alertCategoryMessage",
                "isVisible": true,
                "layoutType": kony.flex.FLOW_HORIZONTAL,
                "left": "0dp",
                "masterType": constants.MASTER_TYPE_DEFAULT,
                "isModalContainer": false,
                "skin": "sknYellowBorder",
                "top": "0dp",
                "width": "100%",
                "overrides": {
                    "alertMessage": {
                        "top": "0dp"
                    }
                }
            }, {
                "overrides": {}
            }, {
                "overrides": {}
            });
            flxAlertCategoryMessage.add(alertCategoryMessage);
            var flxAlertCategoryDetailHeader = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "60px",
                "id": "flxAlertCategoryDetailHeader",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0px",
                "isModalContainer": false,
                "right": "0px",
                "skin": "slFbox",
                "top": "0px",
                "width": "100%",
                "zIndex": 1
            }, {}, {});
            flxAlertCategoryDetailHeader.setDefaultUnit(kony.flex.DP);
            var flxCategoryDetailsArrow = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "centerY": "50%",
                "clipBounds": true,
                "height": "20dp",
                "id": "flxCategoryDetailsArrow",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "15dp",
                "isModalContainer": false,
                "right": "20dp",
                "skin": "sknCursor",
                "width": "20dp",
                "zIndex": 1
            }, {}, {});
            flxCategoryDetailsArrow.setDefaultUnit(kony.flex.DP);
            var lblIconCategoryArrow = new kony.ui.Label({
                "centerY": "50%",
                "id": "lblIconCategoryArrow",
                "isVisible": true,
                "left": "0dp",
                "skin": "sknIcon6E7178Sz15px",
                "text": "",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxCategoryDetailsArrow.add(lblIconCategoryArrow);
            var flxAlertCategoryHeaderContainer = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "centerY": "50%",
                "clipBounds": true,
                "height": "40px",
                "id": "flxAlertCategoryHeaderContainer",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "40px",
                "isModalContainer": false,
                "right": "20px",
                "skin": "slFbox",
                "top": "20px",
                "zIndex": 1
            }, {}, {});
            flxAlertCategoryHeaderContainer.setDefaultUnit(kony.flex.DP);
            var lblViewDetailsCategoryDisplayName = new kony.ui.Label({
                "centerY": "50%",
                "id": "lblViewDetailsCategoryDisplayName",
                "isVisible": true,
                "left": "0px",
                "skin": "sknLatoRegular192B4518px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmAlertsManagement.SecurityAlert\")",
                "top": "0px",
                "width": "80%",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var flxViewDetailsCategoryStatusContainer = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                "clipBounds": true,
                "id": "flxViewDetailsCategoryStatusContainer",
                "isVisible": true,
                "layoutType": kony.flex.FLOW_HORIZONTAL,
                "reverseLayoutDirection": true,
                "isModalContainer": false,
                "right": "50dp",
                "skin": "sknServiceDetailsContent",
                "top": "12dp",
                "width": "8%",
                "zIndex": 1
            }, {}, {});
            flxViewDetailsCategoryStatusContainer.setDefaultUnit(kony.flex.DP);
            var lblViewDetailsCategoryStatus = new kony.ui.Label({
                "id": "lblViewDetailsCategoryStatus",
                "isVisible": true,
                "right": "0dp",
                "skin": "sknLbl485C75LatoRegular13Px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.secureimage.Active\")",
                "top": "0dp",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_TOP_CENTER,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var lblIconViewDetailsCategoryStatus = new kony.ui.Label({
                "height": "12dp",
                "id": "lblIconViewDetailsCategoryStatus",
                "isVisible": true,
                "right": "5dp",
                "skin": "sknIcon13pxGreen",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmAlertsManagement.lblViewAlertStatusKey\")",
                "top": "2dp",
                "width": "12dp",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxViewDetailsCategoryStatusContainer.add(lblViewDetailsCategoryStatus, lblIconViewDetailsCategoryStatus);
            var flxCategoryOptions = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "centerY": "50%",
                "clipBounds": true,
                "height": "30dp",
                "id": "flxCategoryOptions",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "isModalContainer": false,
                "onClick": controller.AS_FlexContainer_b2cebc80febc479da9a2cb6ecfce902e,
                "right": "2dp",
                "skin": "slFbox",
                "width": "30px",
                "zIndex": 2
            }, {}, {
                "hoverSkin": "sknflxffffffop100Border424242Radius100px"
            });
            flxCategoryOptions.setDefaultUnit(kony.flex.DP);
            var lblIconCategoryOptions = new kony.ui.Label({
                "centerX": "50%",
                "centerY": "50%",
                "id": "lblIconCategoryOptions",
                "isVisible": true,
                "skin": "sknFontIconOptionMenu",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.userwidgetmodel.fontIconImgOptions\")",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxCategoryOptions.add(lblIconCategoryOptions);
            flxAlertCategoryHeaderContainer.add(lblViewDetailsCategoryDisplayName, flxViewDetailsCategoryStatusContainer, flxCategoryOptions);
            flxAlertCategoryDetailHeader.add(flxCategoryDetailsArrow, flxAlertCategoryHeaderContainer);
            var flxAlertCategoryDetailBody = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                "bottom": "0dp",
                "clipBounds": true,
                "id": "flxAlertCategoryDetailBody",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0px",
                "isModalContainer": false,
                "right": "0px",
                "skin": "slFbox",
                "top": "0px",
                "width": "100%",
                "zIndex": 1
            }, {}, {});
            flxAlertCategoryDetailBody.setDefaultUnit(kony.flex.DP);
            var flxCategoryBodyContainer = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                "clipBounds": true,
                "id": "flxCategoryBodyContainer",
                "isVisible": true,
                "layoutType": kony.flex.FLOW_VERTICAL,
                "left": "40px",
                "isModalContainer": false,
                "right": "20px",
                "skin": "slFbox",
                "top": "0px",
                "zIndex": 1
            }, {}, {});
            flxCategoryBodyContainer.setDefaultUnit(kony.flex.DP);
            var flxCategoryDetailRow0 = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                "centerX": "50%",
                "clipBounds": true,
                "id": "flxCategoryDetailRow0",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0px",
                "isModalContainer": false,
                "right": "0px",
                "skin": "slFbox",
                "top": "0px",
                "width": "100%",
                "zIndex": 1
            }, {}, {});
            flxCategoryDetailRow0.setDefaultUnit(kony.flex.DP);
            var flxCategoryDetailCol01 = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                "clipBounds": true,
                "id": "flxCategoryDetailCol01",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0px",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0px",
                "width": "33%",
                "zIndex": 1
            }, {}, {});
            flxCategoryDetailCol01.setDefaultUnit(kony.flex.DP);
            var lblCategoryHeaderCol01 = new kony.ui.Label({
                "id": "lblCategoryHeaderCol01",
                "isVisible": true,
                "left": "0px",
                "skin": "sknlblLato696c7311px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmAlertsManagement.AccountLevelAlert_UC\")",
                "top": "0px",
                "width": "100%",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var lblCategoryValueCol01 = new kony.ui.Label({
                "id": "lblCategoryValueCol01",
                "isVisible": true,
                "left": "0px",
                "skin": "sknlbl485C75LatoSemiBold13px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmAlertsManagement.CatSecurity\")",
                "top": "25px",
                "width": "96%",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_TOP_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxCategoryDetailCol01.add(lblCategoryHeaderCol01, lblCategoryValueCol01);
            var flxCategoryDetailCol02 = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                "clipBounds": true,
                "id": "flxCategoryDetailCol02",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "33%",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0px",
                "width": "33%",
                "zIndex": 1
            }, {}, {});
            flxCategoryDetailCol02.setDefaultUnit(kony.flex.DP);
            var lblCategoryHeaderCol02 = new kony.ui.Label({
                "id": "lblCategoryHeaderCol02",
                "isVisible": true,
                "left": "0dp",
                "skin": "sknlblLato696c7311px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmAlertsManagement.DefaultFrequency_UC\")",
                "top": "0dp",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var lblCategoryValueCol02 = new kony.ui.Label({
                "id": "lblCategoryValueCol02",
                "isVisible": true,
                "left": "0px",
                "skin": "sknlbl485C75LatoSemiBold13px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmAlertsManagement.SMSEmail\")",
                "top": "25px",
                "width": "96%",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_TOP_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxCategoryDetailCol02.add(lblCategoryHeaderCol02, lblCategoryValueCol02);
            var flxCategoryDetailCol03 = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                "clipBounds": true,
                "id": "flxCategoryDetailCol03",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "66%",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0px",
                "width": "33%",
                "zIndex": 1
            }, {}, {});
            flxCategoryDetailCol03.setDefaultUnit(kony.flex.DP);
            var lblCategoryHeaderCol03 = new kony.ui.Label({
                "id": "lblCategoryHeaderCol03",
                "isVisible": true,
                "left": "0px",
                "skin": "sknlblLato696c7311px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmAlertsManagement.DefaultChannels_UC\")",
                "top": "0px",
                "width": "100%",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var lblCategoryValueCol03 = new kony.ui.Label({
                "id": "lblCategoryValueCol03",
                "isVisible": true,
                "left": "0px",
                "skin": "sknlbl485C75LatoSemiBold13px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmCustomerManagementController.YES\")",
                "top": "25px",
                "width": "96%",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_TOP_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxCategoryDetailCol03.add(lblCategoryHeaderCol03, lblCategoryValueCol03);
            flxCategoryDetailRow0.add(flxCategoryDetailCol01, flxCategoryDetailCol02, flxCategoryDetailCol03);
            var flxCategoryDetailRow1 = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                "clipBounds": true,
                "id": "flxCategoryDetailRow1",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0px",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "30px",
                "width": "100%",
                "zIndex": 1
            }, {}, {});
            flxCategoryDetailRow1.setDefaultUnit(kony.flex.DP);
            var flxCategoryDescHeader = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "20px",
                "id": "flxCategoryDescHeader",
                "isVisible": true,
                "layoutType": kony.flex.FLOW_HORIZONTAL,
                "left": "0px",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0px",
                "width": "55%",
                "zIndex": 1
            }, {}, {});
            flxCategoryDescHeader.setDefaultUnit(kony.flex.DP);
            var lblCategoryDescHeader = new kony.ui.Label({
                "height": "100%",
                "id": "lblCategoryDescHeader",
                "isVisible": true,
                "left": "0px",
                "skin": "sknlblLato696c7311px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmAlertsManagement.CategoryDescriptionCaps\")",
                "top": "0px",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var lblCategoryDescLanguage = new kony.ui.Label({
                "height": "100%",
                "id": "lblCategoryDescLanguage",
                "isVisible": true,
                "left": "0px",
                "skin": "skn696c73LatoRegItalic11px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmAlertsManagement.CategoryDescriptionCaps\")",
                "top": "0px",
                "width": "100%",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxCategoryDescHeader.add(lblCategoryDescHeader, lblCategoryDescLanguage);
            var flxCategoryDescValue = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                "bottom": "20dp",
                "clipBounds": true,
                "id": "flxCategoryDescValue",
                "isVisible": true,
                "layoutType": kony.flex.FLOW_VERTICAL,
                "left": "0px",
                "isModalContainer": false,
                "skin": "sknServiceDetailsContent",
                "top": "25px",
                "width": "96%",
                "zIndex": 1
            }, {}, {});
            flxCategoryDescValue.setDefaultUnit(kony.flex.DP);
            var lblCategoryDescValue = new kony.ui.Label({
                "id": "lblCategoryDescValue",
                "isVisible": true,
                "left": "0px",
                "skin": "sknlbl485C75LatoSemiBold13px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmAlertsManagement.SecurityAlertsDesc\")",
                "top": "0px",
                "width": "100%",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_TOP_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var lblCategoryShowLangLink = new kony.ui.Label({
                "id": "lblCategoryShowLangLink",
                "isVisible": true,
                "left": "0px",
                "skin": "sknLblLatoReg117eb013px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmAlertsManagement.ViewAllLanguages\")",
                "top": "5dp",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_TOP_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {
                "hoverSkin": "sknLbl117eb013pxHov"
            });
            flxCategoryDescValue.add(lblCategoryDescValue, lblCategoryShowLangLink);
            flxCategoryDetailRow1.add(flxCategoryDescHeader, flxCategoryDescValue);
            flxCategoryBodyContainer.add(flxCategoryDetailRow0, flxCategoryDetailRow1);
            flxAlertCategoryDetailBody.add(flxCategoryBodyContainer);
            flxAlertCategoryContainer.add(flxAlertCategoryMessage, flxAlertCategoryDetailHeader, flxAlertCategoryDetailBody);
            var flxAlertsListing = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                "bottom": "20dp",
                "clipBounds": false,
                "id": "flxAlertsListing",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "isModalContainer": false,
                "right": "0dp",
                "skin": "sknFlxBgFFFFFFBorE4E6ECsz1pxR3px",
                "top": "15dp",
                "zIndex": 3
            }, {}, {});
            flxAlertsListing.setDefaultUnit(kony.flex.DP);
            var flxAlertListingHeader = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "60px",
                "id": "flxAlertListingHeader",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "20px",
                "isModalContainer": false,
                "right": "20px",
                "skin": "slFbox",
                "top": "0px",
                "zIndex": 1
            }, {}, {});
            flxAlertListingHeader.setDefaultUnit(kony.flex.DP);
            var lblAlertListingHeaderValue = new kony.ui.Label({
                "centerY": "50%",
                "id": "lblAlertListingHeaderValue",
                "isVisible": true,
                "left": "0px",
                "skin": "sknLatoRegular192B4518px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmAlertsManagement.AlertGroup\")",
                "top": "0px",
                "width": "80%",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var flxAddAlertBtn = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "100%",
                "id": "flxAddAlertBtn",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "isModalContainer": false,
                "right": "5dp",
                "skin": "sknServiceDetailsContent",
                "top": "0dp",
                "width": "30%",
                "zIndex": 1
            }, {}, {});
            flxAddAlertBtn.setDefaultUnit(kony.flex.DP);
            var btnAddAlerts = new kony.ui.Button({
                "height": "22px",
                "id": "btnAddAlerts",
                "isVisible": true,
                "right": "0dp",
                "skin": "sknbtnf7f7faLatoRegular12Px485c75Border1px485C75Radius20px",
                "text": "Add Group",
                "top": "20dp",
                "width": "88dp",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_CENTER,
                "displayText": true,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {
                "hoverSkin": "sknbtnffffffLatoRegular12Px485c75Border1px485C75Radius20px"
            });
            var btnReorderAlertTypes = new kony.ui.Button({
                "height": "22px",
                "id": "btnReorderAlertTypes",
                "isVisible": true,
                "right": "102dp",
                "skin": "sknbtnf7f7faLatoRegular12Px485c75Border1px485C75Radius20px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmAlertsManagement.Reorder\")",
                "top": "20dp",
                "width": "72dp",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_CENTER,
                "displayText": true,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {
                "hoverSkin": "sknbtnffffffLatoRegular12Px485c75Border1px485C75Radius20px"
            });
            flxAddAlertBtn.add(btnAddAlerts, btnReorderAlertTypes);
            flxAlertListingHeader.add(lblAlertListingHeaderValue, flxAddAlertBtn);
            var flxAlertsSegment = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                "clipBounds": false,
                "id": "flxAlertsSegment",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "20px",
                "isModalContainer": false,
                "right": "20px",
                "skin": "slFbox",
                "top": "50px",
                "zIndex": 1
            }, {}, {});
            flxAlertsSegment.setDefaultUnit(kony.flex.DP);
            var flxAlertListSegHeader = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "60px",
                "id": "flxAlertListSegHeader",
                "isVisible": false,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0px",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0px",
                "width": "100%",
                "zIndex": 1
            }, {}, {});
            flxAlertListSegHeader.setDefaultUnit(kony.flex.DP);
            var flxAlertNameContainer = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "centerY": "50%",
                "clipBounds": true,
                "height": "50%",
                "id": "flxAlertNameContainer",
                "isVisible": true,
                "layoutType": kony.flex.FLOW_HORIZONTAL,
                "left": "20dp",
                "isModalContainer": false,
                "skin": "slFbox",
                "width": "12%",
                "zIndex": 1
            }, {}, {});
            flxAlertNameContainer.setDefaultUnit(kony.flex.DP);
            var lblAlertHeaderName = new kony.ui.Label({
                "centerY": "50%",
                "id": "lblAlertHeaderName",
                "isVisible": true,
                "left": "0dp",
                "skin": "sknlblLato696c7312px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.permission.NAME\")",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {
                "hoverSkin": "sknlblLatoBold00000012px"
            });
            flxAlertNameContainer.add(lblAlertHeaderName);
            var flxAlertCodeContainer = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "centerY": "50%",
                "clipBounds": true,
                "height": "50%",
                "id": "flxAlertCodeContainer",
                "isVisible": true,
                "layoutType": kony.flex.FLOW_HORIZONTAL,
                "left": "22%",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "10dp",
                "width": "15%",
                "zIndex": 1
            }, {}, {});
            flxAlertCodeContainer.setDefaultUnit(kony.flex.DP);
            var lblAlertHeaderCode = new kony.ui.Label({
                "centerY": "50%",
                "id": "lblAlertHeaderCode",
                "isVisible": true,
                "left": "0",
                "skin": "sknlblLato696c7312px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmAlertsManagement.CodeCaps\")",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {
                "hoverSkin": "sknlblLatoBold00000012px"
            });
            flxAlertCodeContainer.add(lblAlertHeaderCode);
            var flxAlertTypeContainer = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "centerY": "50%",
                "clipBounds": true,
                "height": "50%",
                "id": "flxAlertTypeContainer",
                "isVisible": true,
                "layoutType": kony.flex.FLOW_HORIZONTAL,
                "left": "50%",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "10dp",
                "width": "14%",
                "zIndex": 1
            }, {}, {});
            flxAlertTypeContainer.setDefaultUnit(kony.flex.DP);
            var lblAlertHeaderType = new kony.ui.Label({
                "centerY": "50%",
                "id": "lblAlertHeaderType",
                "isVisible": true,
                "left": "0",
                "skin": "sknlblLato696c7312px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmAlertsManagement.TypeCaps\")",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {
                "hoverSkin": "sknlblLatoBold00000012px"
            });
            flxAlertTypeContainer.add(lblAlertHeaderType);
            var flxAlertStatusContainer = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "centerY": "50%",
                "clipBounds": true,
                "height": "50%",
                "id": "flxAlertStatusContainer",
                "isVisible": true,
                "layoutType": kony.flex.FLOW_HORIZONTAL,
                "left": "71%",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "10dp",
                "width": "8%",
                "zIndex": 1
            }, {}, {});
            flxAlertStatusContainer.setDefaultUnit(kony.flex.DP);
            var lblAlertHeaderStatus = new kony.ui.Label({
                "centerY": "50%",
                "id": "lblAlertHeaderStatus",
                "isVisible": true,
                "left": "0",
                "skin": "sknlblLato696c7312px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.roles.STATUS\")",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {
                "hoverSkin": "sknlblLatoBold00000012px"
            });
            flxAlertStatusContainer.add(lblAlertHeaderStatus);
            var lblAlertSeperator = new kony.ui.Label({
                "bottom": "0dp",
                "height": "1px",
                "id": "lblAlertSeperator",
                "isVisible": true,
                "left": "0dp",
                "skin": "sknLblTableHeaderLine",
                "width": "100%",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxAlertListSegHeader.add(flxAlertNameContainer, flxAlertCodeContainer, flxAlertTypeContainer, flxAlertStatusContainer, lblAlertSeperator);
            var flxAlertListSegment = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                "bottom": "20dp",
                "clipBounds": true,
                "id": "flxAlertListSegment",
                "isVisible": true,
                "layoutType": kony.flex.FLOW_VERTICAL,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0dp",
                "width": "100%",
                "zIndex": 1
            }, {}, {});
            flxAlertListSegment.setDefaultUnit(kony.flex.DP);
            flxAlertListSegment.add();
            var flxGroupsListContextualMenu = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                "clipBounds": false,
                "id": "flxGroupsListContextualMenu",
                "isVisible": false,
                "layoutType": kony.flex.FLOW_VERTICAL,
                "isModalContainer": false,
                "right": "10px",
                "skin": "slFbox",
                "top": "105px",
                "width": "130px",
                "zIndex": 5
            }, {}, {});
            flxGroupsListContextualMenu.setDefaultUnit(kony.flex.DP);
            var contextualMenuAlertTypeList = new com.adminConsole.common.contextualMenu1({
                "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                "id": "contextualMenuAlertTypeList",
                "isVisible": true,
                "layoutType": kony.flex.FLOW_VERTICAL,
                "left": "0px",
                "masterType": constants.MASTER_TYPE_DEFAULT,
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0px",
                "width": "100%",
                "overrides": {
                    "btnLink1": {
                        "isVisible": false
                    },
                    "btnLink2": {
                        "isVisible": false
                    },
                    "flxOption1": {
                        "top": "10dp"
                    },
                    "flxOption3": {
                        "bottom": "10dp"
                    },
                    "flxOption4": {
                        "isVisible": false
                    },
                    "flxOptionsSeperator": {
                        "isVisible": false
                    },
                    "imgDownArrow": {
                        "src": "downarrow_2x.png"
                    },
                    "imgUpArrow": {
                        "src": "uparrow_2x.png"
                    },
                    "lblHeader": {
                        "isVisible": false
                    },
                    "lblIconOption2": {
                        "text": ""
                    },
                    "lblIconOption3": {
                        "text": ""
                    },
                    "lblOption1": {
                        "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmAlertsManagement.Edit\")"
                    },
                    "lblOption2": {
                        "i18n_text": "kony.i18n.getLocalizedString(\"i18n.permission.Deactivate\")"
                    },
                    "lblOption3": {
                        "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmAlertsManagement.Reassign\")"
                    }
                }
            }, {
                "overrides": {}
            }, {
                "overrides": {}
            });
            flxGroupsListContextualMenu.add(contextualMenuAlertTypeList);
            var flxAlertGroupTypeTooltip = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                "clipBounds": false,
                "id": "flxAlertGroupTypeTooltip",
                "isVisible": false,
                "layoutType": kony.flex.FLOW_VERTICAL,
                "left": "700dp",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "20dp",
                "width": "85dp",
                "zIndex": 5
            }, {}, {});
            flxAlertGroupTypeTooltip.setDefaultUnit(kony.flex.DP);
            var alertGroupTooltip = new com.adminConsole.common.tooltip({
                "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                "id": "alertGroupTooltip",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "masterType": constants.MASTER_TYPE_DEFAULT,
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0dp",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 10,
                "overrides": {
                    "flxToolTip": {
                        "right": "viz.val_cleared",
                        "width": kony.flex.USE_PREFFERED_SIZE
                    },
                    "flxToolTipMessage": {
                        "width": "80dp"
                    },
                    "lblNoConcentToolTip": {
                        "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmAlertsManagement.GlobalAlert\")"
                    },
                    "lblarrow": {
                        "centerX": "viz.val_cleared",
                        "left": "12px"
                    },
                    "tooltip": {
                        "width": kony.flex.USE_PREFFERED_SIZE
                    }
                }
            }, {
                "overrides": {}
            }, {
                "overrides": {}
            });
            flxAlertGroupTypeTooltip.add(alertGroupTooltip);
            var flxNoAlertGroups = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "150dp",
                "id": "flxNoAlertGroups",
                "isVisible": false,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "isModalContainer": false,
                "right": "0dp",
                "skin": "slFbox",
                "top": "20dp",
                "zIndex": 1
            }, {}, {});
            flxNoAlertGroups.setDefaultUnit(kony.flex.DP);
            var noResultsWithButton = new com.adminConsole.common.noResultsWithButton({
                "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                "centerY": "50%",
                "id": "noResultsWithButton",
                "isVisible": true,
                "layoutType": kony.flex.FLOW_VERTICAL,
                "left": "0dp",
                "masterType": constants.MASTER_TYPE_DEFAULT,
                "isModalContainer": false,
                "skin": "sknFlxFFFFFF100O",
                "width": "100%",
                "overrides": {
                    "btnAddRecord": {
                        "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmAlertsManagement.AddAlertGroup\")",
                        "width": "88dp"
                    },
                    "lblNoRecordsAvailable": {
                        "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmAlertsManagement.AddGroupsUnderCategory\")"
                    },
                    "noResultsWithButton": {
                        "centerY": "50%",
                        "top": "viz.val_cleared"
                    }
                }
            }, {
                "overrides": {}
            }, {
                "overrides": {}
            });
            flxNoAlertGroups.add(noResultsWithButton);
            flxAlertsSegment.add(flxAlertListSegHeader, flxAlertListSegment, flxGroupsListContextualMenu, flxAlertGroupTypeTooltip, flxNoAlertGroups);
            flxAlertsListing.add(flxAlertListingHeader, flxAlertsSegment);
            flxAlertCategoryDetailsScreen.add(flxAlertCategoryContainer, flxAlertsListing);
            var flxEditAlertCategoryScreen = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                "bottom": "10dp",
                "clipBounds": true,
                "id": "flxEditAlertCategoryScreen",
                "isVisible": false,
                "layoutType": kony.flex.FLOW_VERTICAL,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "sknFlxBgFFFFFFBorE4E6ECsz1pxR4px",
                "top": "0dp",
                "width": "100%",
                "zIndex": 1
            }, {}, {});
            flxEditAlertCategoryScreen.setDefaultUnit(kony.flex.DP);
            var flxEditAlertCategory = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                "clipBounds": true,
                "id": "flxEditAlertCategory",
                "isVisible": true,
                "layoutType": kony.flex.FLOW_VERTICAL,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0dp",
                "width": "100%",
                "zIndex": 1
            }, {}, {});
            flxEditAlertCategory.setDefaultUnit(kony.flex.DP);
            var flxEditAlertCategoryHeading = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "60dp",
                "id": "flxEditAlertCategoryHeading",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0dp",
                "width": "100%",
                "zIndex": 1
            }, {}, {});
            flxEditAlertCategoryHeading.setDefaultUnit(kony.flex.DP);
            var lblEditAlertCategoryName = new kony.ui.Label({
                "centerY": "50%",
                "id": "lblEditAlertCategoryName",
                "isVisible": true,
                "left": "20px",
                "skin": "sknlblLatoRegular485c7518Px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmAlertsManagement.AddCategory_LC\")",
                "top": "0px",
                "width": "60%",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var lblEditAlertCategoryActive = new kony.ui.Label({
                "centerY": "50%",
                "height": "20px",
                "id": "lblEditAlertCategoryActive",
                "isVisible": true,
                "right": "67dp",
                "skin": "sknlblLatoBold35475f14px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmAlertsManagement.Active\")",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var flxEditCategoryHeaderSeperator = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "bottom": "0dp",
                "clipBounds": true,
                "height": "1dp",
                "id": "flxEditCategoryHeaderSeperator",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "20dp",
                "isModalContainer": false,
                "right": "20dp",
                "skin": "sknSeparatorCsr",
                "zIndex": 1
            }, {}, {});
            flxEditCategoryHeaderSeperator.setDefaultUnit(kony.flex.DP);
            flxEditCategoryHeaderSeperator.add();
            var flxEditCategoryStatusCont = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "centerY": "51%",
                "clipBounds": true,
                "height": "25dp",
                "id": "flxEditCategoryStatusCont",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "isModalContainer": false,
                "right": "20dp",
                "skin": "slFbox",
                "width": "38dp"
            }, {}, {});
            flxEditCategoryStatusCont.setDefaultUnit(kony.flex.DP);
            var editAlertCategoryStatusSwitch = new com.adminConsole.common.customSwitch({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "height": "25dp",
                "id": "editAlertCategoryStatusSwitch",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "masterType": constants.MASTER_TYPE_DEFAULT,
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0dp",
                "width": "100%",
                "overrides": {
                    "customSwitch": {
                        "centerY": "viz.val_cleared",
                        "height": "25dp",
                        "isVisible": true,
                        "left": "0dp",
                        "right": "viz.val_cleared",
                        "top": "0dp",
                        "width": "100%"
                    },
                    "switchToggle": {
                        "isVisible": true
                    }
                }
            }, {
                "overrides": {}
            }, {
                "overrides": {}
            });
            flxEditCategoryStatusCont.add(editAlertCategoryStatusSwitch);
            flxEditAlertCategoryHeading.add(lblEditAlertCategoryName, lblEditAlertCategoryActive, flxEditCategoryHeaderSeperator, flxEditCategoryStatusCont);
            var flxEditAlertCaegoryRow0 = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                "clipBounds": true,
                "id": "flxEditAlertCaegoryRow0",
                "isVisible": true,
                "layoutType": kony.flex.FLOW_HORIZONTAL,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "20dp",
                "width": "100%",
                "zIndex": 1
            }, {}, {});
            flxEditAlertCaegoryRow0.setDefaultUnit(kony.flex.DP);
            var flxEditCategoryCol00 = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "100dp",
                "id": "flxEditCategoryCol00",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "10dp",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0dp",
                "width": "33%",
                "zIndex": 1
            }, {}, {});
            flxEditCategoryCol00.setDefaultUnit(kony.flex.DP);
            var textBoxEntryCategoryName = new com.adminConsole.common.textBoxEntry({
                "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                "id": "textBoxEntryCategoryName",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "masterType": constants.MASTER_TYPE_DEFAULT,
                "isModalContainer": false,
                "right": "1dp",
                "skin": "slFbox",
                "top": "0dp",
                "zIndex": 1,
                "overrides": {
                    "flxEnterValue": {
                        "left": "10dp"
                    },
                    "flxHeading": {
                        "left": "10dp"
                    },
                    "flxInlineError": {
                        "left": "10dp"
                    },
                    "flxUserNameError": {
                        "left": "10dp"
                    },
                    "lblErrorText": {
                        "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmAlertsManagement.PleaseEnterCategoryName\")"
                    },
                    "lblHeadingText": {
                        "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmAlertsManagement.CategoryName\")"
                    },
                    "tbxEnterValue": {
                        "i18n_placeholder": "kony.i18n.getLocalizedString(\"i18n.frmAlertsManagement.CategoryName\")"
                    },
                    "textBoxEntry": {
                        "left": "0dp",
                        "right": "1dp",
                        "width": "viz.val_cleared"
                    }
                }
            }, {
                "overrides": {}
            }, {
                "overrides": {}
            });
            flxEditCategoryCol00.add(textBoxEntryCategoryName);
            var flxEditCategoryCol11 = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "100dp",
                "id": "flxEditCategoryCol11",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "30dp",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0dp",
                "width": "25%",
                "zIndex": 1
            }, {}, {});
            flxEditCategoryCol11.setDefaultUnit(kony.flex.DP);
            var lblEditAlertCategoryRadioHeader = new kony.ui.Label({
                "id": "lblEditAlertCategoryRadioHeader",
                "isVisible": true,
                "left": "0dp",
                "skin": "sknLbl485C75LatoRegular13Px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmAlertsManagement.AccountLevelAlert\")",
                "top": "0dp",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_TOP_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var customCategoryRadioButtonType = new com.adminConsole.common.customRadioButtonGroup({
                "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                "id": "customCategoryRadioButtonType",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "masterType": constants.MASTER_TYPE_DEFAULT,
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "30dp",
                "width": "100%",
                "overrides": {
                    "customRadioButtonGroup": {
                        "top": "30dp"
                    },
                    "flxRadioButton1": {
                        "width": "50dp"
                    },
                    "flxRadioButton2": {
                        "left": "20dp"
                    },
                    "flxRadioButton3": {
                        "isVisible": false
                    },
                    "imgRadioButton1": {
                        "src": "radionormal_1x.png"
                    },
                    "imgRadioButton2": {
                        "src": "radionormal_1x.png"
                    },
                    "imgRadioButton3": {
                        "src": "radionormal_1x.png"
                    },
                    "imgRadioButton4": {
                        "src": "radionormal_1x.png"
                    },
                    "imgRadioButton5": {
                        "src": "radionormal_1x.png"
                    },
                    "lblRadioButtonValue1": {
                        "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmCustomerManagementController.YES\")"
                    },
                    "lblRadioButtonValue2": {
                        "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmAlertsManagement.No\")"
                    }
                }
            }, {
                "overrides": {}
            }, {
                "overrides": {}
            });
            var flxCategoryRadioButtonError = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                "clipBounds": true,
                "id": "flxCategoryRadioButtonError",
                "isVisible": false,
                "layoutType": kony.flex.FLOW_HORIZONTAL,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "60dp",
                "width": "100%",
                "zIndex": 1
            }, {}, {});
            flxCategoryRadioButtonError.setDefaultUnit(kony.flex.DP);
            var lblIconErrorRadioCategory = new kony.ui.Label({
                "id": "lblIconErrorRadioCategory",
                "isVisible": true,
                "left": "0dp",
                "skin": "sknErrorIcon",
                "text": "",
                "top": "0dp",
                "width": "15dp",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var lblErrMsgRadioCategory = new kony.ui.Label({
                "id": "lblErrMsgRadioCategory",
                "isVisible": true,
                "left": "0dp",
                "skin": "sknlblError",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmAlertsManagement.lblNoCheckBoxSelected\")",
                "top": "0dp",
                "width": "170dp",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxCategoryRadioButtonError.add(lblIconErrorRadioCategory, lblErrMsgRadioCategory);
            flxEditCategoryCol11.add(lblEditAlertCategoryRadioHeader, customCategoryRadioButtonType, flxCategoryRadioButtonError);
            flxEditAlertCaegoryRow0.add(flxEditCategoryCol00, flxEditCategoryCol11);
            var flxEditAlertCategoryRow1 = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                "clipBounds": true,
                "id": "flxEditAlertCategoryRow1",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0dp",
                "width": "100%",
                "zIndex": 1
            }, {}, {});
            flxEditAlertCategoryRow1.setDefaultUnit(kony.flex.DP);
            var lblEditAlertCategoryFreqHeader = new kony.ui.Label({
                "id": "lblEditAlertCategoryFreqHeader",
                "isVisible": true,
                "left": "20px",
                "skin": "sknLbl192b45LatoReg16px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmAlertsManagement.DefaultFrequencyDetails\")",
                "top": "0px",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var frequencySelectionCat = new com.adminConsole.alerts.frequencySelection({
                "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                "id": "frequencySelectionCat",
                "isVisible": true,
                "layoutType": kony.flex.FLOW_HORIZONTAL,
                "left": "20dp",
                "masterType": constants.MASTER_TYPE_DEFAULT,
                "isModalContainer": false,
                "right": "20dp",
                "skin": "sknFlxBgF8F9FABrE1E5EER3px",
                "top": "33dp",
                "overrides": {
                    "frequencySelection": {
                        "left": "20dp",
                        "right": "20dp",
                        "top": "33dp",
                        "width": "viz.val_cleared"
                    }
                }
            }, {
                "overrides": {}
            }, {
                "overrides": {}
            });
            flxEditAlertCategoryRow1.add(lblEditAlertCategoryFreqHeader, frequencySelectionCat);
            var flxEditAlertCaegoryRow2 = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                "clipBounds": true,
                "id": "flxEditAlertCaegoryRow2",
                "isVisible": true,
                "layoutType": kony.flex.FLOW_VERTICAL,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "30dp",
                "width": "100%",
                "zIndex": 1
            }, {}, {});
            flxEditAlertCaegoryRow2.setDefaultUnit(kony.flex.DP);
            var lblEditCategoryChannel = new kony.ui.Label({
                "id": "lblEditCategoryChannel",
                "isVisible": true,
                "left": "20px",
                "skin": "sknLbl192b45LatoReg16px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmAlertsManagement.DefaultChannels\")",
                "top": "0px",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var flxEditCategoryCol20 = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                "clipBounds": true,
                "id": "flxEditCategoryCol20",
                "isVisible": true,
                "layoutType": kony.flex.FLOW_VERTICAL,
                "left": "20dp",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "15dp",
                "width": "80%",
                "zIndex": 1
            }, {}, {});
            flxEditCategoryCol20.setDefaultUnit(kony.flex.DP);
            var flxSupportedChannelEntries = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "30dp",
                "id": "flxSupportedChannelEntries",
                "isVisible": true,
                "layoutType": kony.flex.FLOW_HORIZONTAL,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0dp",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {}, {});
            flxSupportedChannelEntries.setDefaultUnit(kony.flex.DP);
            flxSupportedChannelEntries.add();
            var flxCategoryChannelError = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                "clipBounds": true,
                "id": "flxCategoryChannelError",
                "isVisible": false,
                "layoutType": kony.flex.FLOW_HORIZONTAL,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "5dp",
                "width": "100%",
                "zIndex": 1
            }, {}, {});
            flxCategoryChannelError.setDefaultUnit(kony.flex.DP);
            var lblIconErrorCategoryChannel = new kony.ui.Label({
                "id": "lblIconErrorCategoryChannel",
                "isVisible": true,
                "left": "0dp",
                "skin": "sknErrorIcon",
                "text": "",
                "top": "0dp",
                "width": "15dp",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var lblErrMsgCategoryChannel = new kony.ui.Label({
                "id": "lblErrMsgCategoryChannel",
                "isVisible": true,
                "left": "5dp",
                "skin": "sknlblError",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmAlertsManagement.lblNoCheckBoxSelected\")",
                "top": "0dp",
                "width": "170dp",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxCategoryChannelError.add(lblIconErrorCategoryChannel, lblErrMsgCategoryChannel);
            flxEditCategoryCol20.add(flxSupportedChannelEntries, flxCategoryChannelError);
            flxEditAlertCaegoryRow2.add(lblEditCategoryChannel, flxEditCategoryCol20);
            var flxEditCategoryBottom = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                "clipBounds": true,
                "id": "flxEditCategoryBottom",
                "isVisible": true,
                "layoutType": kony.flex.FLOW_VERTICAL,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "30dp",
                "width": "100%"
            }, {}, {});
            flxEditCategoryBottom.setDefaultUnit(kony.flex.DP);
            var flxEditAlertCategoryLanguageHeader = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                "clipBounds": true,
                "id": "flxEditAlertCategoryLanguageHeader",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0dp",
                "width": "100%",
                "zIndex": 1
            }, {}, {});
            flxEditAlertCategoryLanguageHeader.setDefaultUnit(kony.flex.DP);
            var lblEditAlertCategoryLanguageValue = new kony.ui.Label({
                "id": "lblEditAlertCategoryLanguageValue",
                "isVisible": true,
                "left": "20dp",
                "skin": "sknLbl192b45LatoReg16px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmAlertsManagement.Supported_Language\")",
                "top": "0dp",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var flxEditAlertCategoryLanguagesError = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                "clipBounds": true,
                "id": "flxEditAlertCategoryLanguagesError",
                "isVisible": false,
                "layoutType": kony.flex.FLOW_HORIZONTAL,
                "left": "20dp",
                "isModalContainer": false,
                "right": "20dp",
                "skin": "slFbox",
                "top": "30dp",
                "zIndex": 1
            }, {}, {});
            flxEditAlertCategoryLanguagesError.setDefaultUnit(kony.flex.DP);
            var errorFlagMessage = new com.adminConsole.common.errorFlagMessage({
                "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                "id": "errorFlagMessage",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "masterType": constants.MASTER_TYPE_DEFAULT,
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0dp",
                "width": "100%",
                "overrides": {
                    "lblErrorValue": {
                        "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmAlertsManagement.AddSupportedLanguageToProceed\")"
                    }
                }
            }, {
                "overrides": {}
            }, {
                "overrides": {}
            });
            flxEditAlertCategoryLanguagesError.add(errorFlagMessage);
            flxEditAlertCategoryLanguageHeader.add(lblEditAlertCategoryLanguageValue, flxEditAlertCategoryLanguagesError);
            var flxEditAlertCategoryLanguageSegment = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                "clipBounds": true,
                "id": "flxEditAlertCategoryLanguageSegment",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "20dp",
                "width": "100%",
                "zIndex": 1
            }, {}, {});
            flxEditAlertCategoryLanguageSegment.setDefaultUnit(kony.flex.DP);
            var flxEditCategoryLanguageSep = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "1dp",
                "id": "flxEditCategoryLanguageSep",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "20dp",
                "isModalContainer": false,
                "right": "20dp",
                "skin": "sknSeparatorCsr",
                "top": "0dp",
                "zIndex": 1
            }, {}, {});
            flxEditCategoryLanguageSep.setDefaultUnit(kony.flex.DP);
            flxEditCategoryLanguageSep.add();
            var flxEditAlertCategoryLangSegHeader = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "50dp",
                "id": "flxEditAlertCategoryLangSegHeader",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "20dp",
                "isModalContainer": false,
                "right": "20dp",
                "skin": "slFbox",
                "top": "0dp",
                "zIndex": 1
            }, {}, {});
            flxEditAlertCategoryLangSegHeader.setDefaultUnit(kony.flex.DP);
            var flxEditAlertCategorySegLanguage = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "100%",
                "id": "flxEditAlertCategorySegLanguage",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0dp",
                "width": "18%",
                "zIndex": 1
            }, {}, {});
            flxEditAlertCategorySegLanguage.setDefaultUnit(kony.flex.DP);
            var lblEditAlertCategorySegLanguage = new kony.ui.Label({
                "centerY": "50%",
                "id": "lblEditAlertCategorySegLanguage",
                "isVisible": true,
                "left": "10dp",
                "skin": "sknLbl485c7513pxHover",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmAlertsManagement.Language_UC\")",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxEditAlertCategorySegLanguage.add(lblEditAlertCategorySegLanguage);
            var flxEditAlertCategorySegDisplayName = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "100%",
                "id": "flxEditAlertCategorySegDisplayName",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "21%",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0dp",
                "width": "23%",
                "zIndex": 1
            }, {}, {});
            flxEditAlertCategorySegDisplayName.setDefaultUnit(kony.flex.DP);
            var lblEditAlertCategorySegDisplayName = new kony.ui.Label({
                "centerY": "50%",
                "id": "lblEditAlertCategorySegDisplayName",
                "isVisible": true,
                "left": "20dp",
                "skin": "sknLbl485c7513pxHover",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.View.DISPLAYNAME\")",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxEditAlertCategorySegDisplayName.add(lblEditAlertCategorySegDisplayName);
            var flxEditAlertCategorySegDescription = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "100%",
                "id": "flxEditAlertCategorySegDescription",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "44%",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0dp",
                "width": "30%",
                "zIndex": 1
            }, {}, {});
            flxEditAlertCategorySegDescription.setDefaultUnit(kony.flex.DP);
            var lblEditAlertCategorySegDescription = new kony.ui.Label({
                "centerY": "50%",
                "id": "lblEditAlertCategorySegDescription",
                "isVisible": true,
                "left": "20dp",
                "skin": "sknLbl485c7513pxHover",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmAlertsManagement.CategoryDescriptionCaps\")",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxEditAlertCategorySegDescription.add(lblEditAlertCategorySegDescription);
            var flxEditAlertCategorySegLine = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "bottom": "0dp",
                "clipBounds": true,
                "height": "1dp",
                "id": "flxEditAlertCategorySegLine",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "isModalContainer": false,
                "right": "0dp",
                "skin": "sknFlxBg696C73Op100",
                "zIndex": 1
            }, {}, {});
            flxEditAlertCategorySegLine.setDefaultUnit(kony.flex.DP);
            flxEditAlertCategorySegLine.add();
            flxEditAlertCategoryLangSegHeader.add(flxEditAlertCategorySegLanguage, flxEditAlertCategorySegDisplayName, flxEditAlertCategorySegDescription, flxEditAlertCategorySegLine);
            var flxEditAlertCategoryLangSegList = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                "clipBounds": true,
                "id": "flxEditAlertCategoryLangSegList",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "50dp",
                "width": "100%",
                "zIndex": 1
            }, {}, {});
            flxEditAlertCategoryLangSegList.setDefaultUnit(kony.flex.DP);
            var segEditAlertCategoryLanguages = new kony.ui.SegmentedUI2({
                "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                "data": [{
                    "lblAddAlertTypeSegDescription": "Label",
                    "lblAddAlertTypeSegDisplayName": "Label",
                    "lblAddAlertTypeSegLanguage": "Label",
                    "lblDescCount": "0/500",
                    "lblIconDeleteLang": "",
                    "lblSeprator": "-",
                    "lstBoxSelectLanguage": {
                        "masterData": [
                            ["lb1", "English(US)"],
                            ["lb2", "English(UK)"],
                            ["lb3", "Spanish"],
                            ["lb4", "French"],
                            ["lb0", "Select"]
                        ],
                        "selectedKey": "lb0",
                        "selectedKeys": null,
                        "selectedKeyValue": ["lb0", "Select"]
                    },
                    "tbxDescription": "",
                    "tbxDisplayName": ""
                }, {
                    "lblAddAlertTypeSegDescription": "Label",
                    "lblAddAlertTypeSegDisplayName": "Label",
                    "lblAddAlertTypeSegLanguage": "Label",
                    "lblDescCount": "0/500",
                    "lblIconDeleteLang": "",
                    "lblSeprator": "-",
                    "lstBoxSelectLanguage": {
                        "masterData": [
                            ["lb1", "English(US)"],
                            ["lb2", "English(UK)"],
                            ["lb3", "Spanish"],
                            ["lb4", "French"],
                            ["lb0", "Select"]
                        ],
                        "selectedKey": "lb0",
                        "selectedKeys": null,
                        "selectedKeyValue": ["lb0", "Select"]
                    },
                    "tbxDescription": "",
                    "tbxDisplayName": ""
                }, {
                    "lblAddAlertTypeSegDescription": "Label",
                    "lblAddAlertTypeSegDisplayName": "Label",
                    "lblAddAlertTypeSegLanguage": "Label",
                    "lblDescCount": "0/500",
                    "lblIconDeleteLang": "",
                    "lblSeprator": "-",
                    "lstBoxSelectLanguage": {
                        "masterData": [
                            ["lb1", "English(US)"],
                            ["lb2", "English(UK)"],
                            ["lb3", "Spanish"],
                            ["lb4", "French"],
                            ["lb0", "Select"]
                        ],
                        "selectedKey": "lb0",
                        "selectedKeys": null,
                        "selectedKeyValue": ["lb0", "Select"]
                    },
                    "tbxDescription": "",
                    "tbxDisplayName": ""
                }],
                "groupCells": false,
                "id": "segEditAlertCategoryLanguages",
                "isVisible": true,
                "left": "20dp",
                "needPageIndicator": true,
                "pageOffDotImage": "pageoffdot.png",
                "pageOnDotImage": "pageondot.png",
                "retainSelection": false,
                "right": "20dp",
                "rowFocusSkin": "seg2Focus",
                "rowSkin": "seg2Normal",
                "rowTemplate": "flxAlertsLanguageData",
                "sectionHeaderSkin": "sliPhoneSegmentHeader",
                "selectionBehavior": constants.SEGUI_DEFAULT_BEHAVIOR,
                "separatorRequired": false,
                "showScrollbars": false,
                "top": "0dp",
                "viewType": constants.SEGUI_VIEW_TYPE_TABLEVIEW,
                "widgetDataMap": {
                    "flxAddDescription": "flxAddDescription",
                    "flxAddDisplayName": "flxAddDisplayName",
                    "flxAddLanguage": "flxAddLanguage",
                    "flxAlertsLanguageData": "flxAlertsLanguageData",
                    "flxDelete": "flxDelete",
                    "flxDescription": "flxDescription",
                    "flxLanguageRowContainer": "flxLanguageRowContainer",
                    "lblAddAlertTypeSegDescription": "lblAddAlertTypeSegDescription",
                    "lblAddAlertTypeSegDisplayName": "lblAddAlertTypeSegDisplayName",
                    "lblAddAlertTypeSegLanguage": "lblAddAlertTypeSegLanguage",
                    "lblDescCount": "lblDescCount",
                    "lblIconDeleteLang": "lblIconDeleteLang",
                    "lblSeprator": "lblSeprator",
                    "lstBoxSelectLanguage": "lstBoxSelectLanguage",
                    "tbxDescription": "tbxDescription",
                    "tbxDisplayName": "tbxDisplayName"
                },
                "zIndex": 1
            }, {
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxEditAlertCategoryLangSegList.add(segEditAlertCategoryLanguages);
            flxEditAlertCategoryLanguageSegment.add(flxEditCategoryLanguageSep, flxEditAlertCategoryLangSegHeader, flxEditAlertCategoryLangSegList);
            flxEditCategoryBottom.add(flxEditAlertCategoryLanguageHeader, flxEditAlertCategoryLanguageSegment);
            flxEditAlertCategory.add(flxEditAlertCategoryHeading, flxEditAlertCaegoryRow0, flxEditAlertCategoryRow1, flxEditAlertCaegoryRow2, flxEditCategoryBottom);
            var flxEditAlertCategoryButtons = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "80dp",
                "id": "flxEditAlertCategoryButtons",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0dp",
                "width": "100%",
                "zIndex": 1
            }, {}, {});
            flxEditAlertCategoryButtons.setDefaultUnit(kony.flex.DP);
            var flxEditCategoryButtonSeperator = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "1dp",
                "id": "flxEditCategoryButtonSeperator",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "isModalContainer": false,
                "right": "0dp",
                "skin": "sknSeparatorCsr",
                "top": "0dp",
                "zIndex": 2
            }, {}, {});
            flxEditCategoryButtonSeperator.setDefaultUnit(kony.flex.DP);
            flxEditCategoryButtonSeperator.add();
            var EditAlertCategoryButtons = new com.adminConsole.common.commonButtons({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "height": "80px",
                "id": "EditAlertCategoryButtons",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "20dp",
                "masterType": constants.MASTER_TYPE_DEFAULT,
                "isModalContainer": false,
                "right": 20,
                "skin": "slFbox",
                "top": "0dp",
                "zIndex": 3,
                "overrides": {
                    "btnCancel": {
                        "left": "0dp",
                        "right": "viz.val_cleared"
                    },
                    "btnNext": {
                        "isVisible": false
                    },
                    "commonButtons": {
                        "left": "20dp",
                        "right": 20,
                        "width": "viz.val_cleared",
                        "zIndex": 3
                    }
                }
            }, {
                "overrides": {}
            }, {
                "overrides": {}
            });
            flxEditAlertCategoryButtons.add(flxEditCategoryButtonSeperator, EditAlertCategoryButtons);
            flxEditAlertCategoryScreen.add(flxEditAlertCategory, flxEditAlertCategoryButtons);
            var flxCategoryOptionsMenu = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                "clipBounds": true,
                "id": "flxCategoryOptionsMenu",
                "isVisible": false,
                "layoutType": kony.flex.FREE_FORM,
                "isModalContainer": false,
                "right": "16dp",
                "skin": "slFbox",
                "top": "50dp",
                "width": "150dp",
                "zIndex": 1
            }, {}, {});
            flxCategoryOptionsMenu.setDefaultUnit(kony.flex.DP);
            var contextualMenu1 = new com.adminConsole.common.contextualMenu1({
                "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                "id": "contextualMenu1",
                "isVisible": true,
                "layoutType": kony.flex.FLOW_VERTICAL,
                "left": "0px",
                "masterType": constants.MASTER_TYPE_DEFAULT,
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0px",
                "width": "100%",
                "overrides": {
                    "btnLink1": {
                        "isVisible": false
                    },
                    "btnLink2": {
                        "isVisible": false
                    },
                    "flxDownArrowImage": {
                        "isVisible": false
                    },
                    "flxOption1": {
                        "isVisible": false
                    },
                    "flxOption3": {
                        "isVisible": false
                    },
                    "flxOptionsSeperator": {
                        "isVisible": false
                    },
                    "imgDownArrow": {
                        "src": "downarrow_2x.png"
                    },
                    "imgOption1": {
                        "src": "imagedrag.png"
                    },
                    "imgOption2": {
                        "src": "edit2x.png"
                    },
                    "imgOption3": {
                        "src": "imagedrag.png"
                    },
                    "imgOption4": {
                        "src": "deactive_2x.png"
                    },
                    "imgUpArrow": {
                        "src": "uparrow_2x.png"
                    },
                    "lblHeader": {
                        "isVisible": false
                    }
                }
            }, {
                "overrides": {}
            }, {
                "overrides": {}
            });
            flxCategoryOptionsMenu.add(contextualMenu1);
            flxAlertCategories.add(flxAlertCategoriesList, flxAlertCategoryDetailsScreen, flxEditAlertCategoryScreen, flxCategoryOptionsMenu);
            var flxAlertTypes = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                "clipBounds": true,
                "id": "flxAlertTypes",
                "isVisible": false,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "isModalContainer": false,
                "right": "0dp",
                "skin": "slFbox",
                "top": "0dp",
                "zIndex": 1
            }, {}, {});
            flxAlertTypes.setDefaultUnit(kony.flex.DP);
            var flxAlertTypeDetailsScreen = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                "clipBounds": true,
                "id": "flxAlertTypeDetailsScreen",
                "isVisible": true,
                "layoutType": kony.flex.FLOW_VERTICAL,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0dp",
                "width": "100%",
                "zIndex": 1
            }, {}, {});
            flxAlertTypeDetailsScreen.setDefaultUnit(kony.flex.DP);
            var flxAlertTypeDetailsTop = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                "clipBounds": true,
                "id": "flxAlertTypeDetailsTop",
                "isVisible": true,
                "layoutType": kony.flex.FLOW_VERTICAL,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "sknFlxBgFFFFFFBorE4E6ECsz1pxR3px",
                "top": "0dp",
                "width": "100%"
            }, {}, {});
            flxAlertTypeDetailsTop.setDefaultUnit(kony.flex.DP);
            var flxAlertTypeMessage = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "35px",
                "id": "flxAlertTypeMessage",
                "isVisible": false,
                "layoutType": kony.flex.FREE_FORM,
                "left": "20px",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "20px",
                "width": "96%",
                "zIndex": 1
            }, {}, {});
            flxAlertTypeMessage.setDefaultUnit(kony.flex.DP);
            var alertTypeMessage = new com.adminConsole.alertMang.alertMessage({
                "height": "35px",
                "id": "alertTypeMessage",
                "isVisible": true,
                "layoutType": kony.flex.FLOW_HORIZONTAL,
                "left": "0dp",
                "masterType": constants.MASTER_TYPE_DEFAULT,
                "isModalContainer": false,
                "skin": "sknYellowBorder",
                "top": "0dp",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "overrides": {
                    "alertMessage": {
                        "right": "viz.val_cleared",
                        "top": "0dp",
                        "width": kony.flex.USE_PREFFERED_SIZE
                    }
                }
            }, {
                "overrides": {}
            }, {
                "overrides": {}
            });
            flxAlertTypeMessage.add(alertTypeMessage);
            var flxAlertTypeDetailHeader = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "60px",
                "id": "flxAlertTypeDetailHeader",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0px",
                "isModalContainer": false,
                "right": "0px",
                "skin": "slFbox",
                "top": "0px",
                "width": "100%",
                "zIndex": 1
            }, {}, {});
            flxAlertTypeDetailHeader.setDefaultUnit(kony.flex.DP);
            var flxAlertTypeDetailsArrow = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "centerY": "50%",
                "clipBounds": true,
                "height": "20dp",
                "id": "flxAlertTypeDetailsArrow",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "15dp",
                "isModalContainer": false,
                "skin": "sknCursor",
                "width": "20dp",
                "zIndex": 1
            }, {}, {});
            flxAlertTypeDetailsArrow.setDefaultUnit(kony.flex.DP);
            var lblAlertTypeHeaderArrow = new kony.ui.Label({
                "centerY": "50%",
                "id": "lblAlertTypeHeaderArrow",
                "isVisible": true,
                "left": "0dp",
                "skin": "sknIcon6E7178Sz15px",
                "text": "",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxAlertTypeDetailsArrow.add(lblAlertTypeHeaderArrow);
            var flxAlertTypeHeaderContainer = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "centerY": "50%",
                "clipBounds": true,
                "height": "40px",
                "id": "flxAlertTypeHeaderContainer",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "40px",
                "isModalContainer": false,
                "right": "20px",
                "skin": "slFbox",
                "zIndex": 1
            }, {}, {});
            flxAlertTypeHeaderContainer.setDefaultUnit(kony.flex.DP);
            var flxViewDetailsAlertStatusContainer = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                "clipBounds": true,
                "id": "flxViewDetailsAlertStatusContainer",
                "isVisible": true,
                "layoutType": kony.flex.FLOW_HORIZONTAL,
                "reverseLayoutDirection": true,
                "isModalContainer": false,
                "right": "50dp",
                "skin": "sknServiceDetailsContent",
                "top": "12dp",
                "width": "70dp",
                "zIndex": 1
            }, {}, {});
            flxViewDetailsAlertStatusContainer.setDefaultUnit(kony.flex.DP);
            var lblViewDetailsAlertTypeStatus = new kony.ui.Label({
                "id": "lblViewDetailsAlertTypeStatus",
                "isVisible": true,
                "right": "0dp",
                "skin": "sknLbl485C75LatoRegular13Px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.secureimage.Active\")",
                "top": "0dp",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_TOP_CENTER,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var lblIconViewDetailsStatus = new kony.ui.Label({
                "height": "12dp",
                "id": "lblIconViewDetailsStatus",
                "isVisible": true,
                "right": "5dp",
                "skin": "sknIcon13pxGreen",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmAlertsManagement.lblViewAlertStatusKey\")",
                "top": "2dp",
                "width": "12dp",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxViewDetailsAlertStatusContainer.add(lblViewDetailsAlertTypeStatus, lblIconViewDetailsStatus);
            var flxOptions = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "centerY": "50%",
                "clipBounds": true,
                "height": "30dp",
                "id": "flxOptions",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "isModalContainer": false,
                "onClick": controller.AS_FlexContainer_b2cebc80febc479da9a2cb6ecfce902e,
                "right": "2dp",
                "skin": "sknFlxBgFFFFFFRounded",
                "width": "30px",
                "zIndex": 2
            }, {}, {
                "hoverSkin": "sknflxffffffop100Border424242Radius100px"
            });
            flxOptions.setDefaultUnit(kony.flex.DP);
            var lblIconAlertTypeOptions = new kony.ui.Label({
                "centerX": "50%",
                "centerY": "50%",
                "id": "lblIconAlertTypeOptions",
                "isVisible": true,
                "skin": "sknFontIconOptionMenu",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.userwidgetmodel.fontIconImgOptions\")",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxOptions.add(lblIconAlertTypeOptions);
            var flxTitle = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "100%",
                "id": "flxTitle",
                "isVisible": true,
                "layoutType": kony.flex.FLOW_HORIZONTAL,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0dp",
                "width": "80%"
            }, {}, {});
            flxTitle.setDefaultUnit(kony.flex.DP);
            var lblViewDetailsAlertDisplayName = new kony.ui.Label({
                "centerY": "50%",
                "id": "lblViewDetailsAlertDisplayName",
                "isVisible": true,
                "left": "0px",
                "skin": "sknLatoRegular485C7518px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmAlertsManagement.SecurityAlert\")",
                "top": "0px",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxTitle.add(lblViewDetailsAlertDisplayName);
            flxAlertTypeHeaderContainer.add(flxViewDetailsAlertStatusContainer, flxOptions, flxTitle);
            flxAlertTypeDetailHeader.add(flxAlertTypeDetailsArrow, flxAlertTypeHeaderContainer);
            var flxAlertTypeDetailBody = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                "bottom": "20dp",
                "clipBounds": true,
                "id": "flxAlertTypeDetailBody",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0px",
                "isModalContainer": false,
                "right": "0px",
                "skin": "slFbox",
                "top": "0px",
                "width": "100%",
                "zIndex": 1
            }, {}, {});
            flxAlertTypeDetailBody.setDefaultUnit(kony.flex.DP);
            var flxAlertTypeBodyContainer = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                "clipBounds": true,
                "id": "flxAlertTypeBodyContainer",
                "isVisible": true,
                "layoutType": kony.flex.FLOW_VERTICAL,
                "left": "40px",
                "isModalContainer": false,
                "right": "20px",
                "skin": "slFbox",
                "top": "0px",
                "zIndex": 1
            }, {}, {});
            flxAlertTypeBodyContainer.setDefaultUnit(kony.flex.DP);
            var flxAlertTypeDetailRow01 = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                "centerX": "50%",
                "clipBounds": true,
                "id": "flxAlertTypeDetailRow01",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0px",
                "isModalContainer": false,
                "right": "0px",
                "skin": "slFbox",
                "top": "0px",
                "width": "100%",
                "zIndex": 1
            }, {}, {});
            flxAlertTypeDetailRow01.setDefaultUnit(kony.flex.DP);
            var flxAlertTypeDetailCol11 = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                "clipBounds": true,
                "id": "flxAlertTypeDetailCol11",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0%",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0px",
                "width": "33%",
                "zIndex": 1
            }, {}, {});
            flxAlertTypeDetailCol11.setDefaultUnit(kony.flex.DP);
            var lblAlertTypeColHeader11 = new kony.ui.Label({
                "id": "lblAlertTypeColHeader11",
                "isVisible": true,
                "left": "0dp",
                "skin": "sknlblLato696c7311px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmAlertsManagement.ALERT_GROUP_CODE\")",
                "top": "0dp",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var lblAlertTypeColValue11 = new kony.ui.Label({
                "id": "lblAlertTypeColValue11",
                "isVisible": true,
                "left": "0px",
                "skin": "sknlbl485C75LatoSemiBold13px",
                "text": "AL001AL001AL001",
                "top": "20px",
                "width": "96%",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_TOP_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxAlertTypeDetailCol11.add(lblAlertTypeColHeader11, lblAlertTypeColValue11);
            var flxAlertTypeDetailCol12 = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                "clipBounds": true,
                "id": "flxAlertTypeDetailCol12",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "33%",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0px",
                "width": "33%",
                "zIndex": 1
            }, {}, {});
            flxAlertTypeDetailCol12.setDefaultUnit(kony.flex.DP);
            var lblAlertTypeColHeader12 = new kony.ui.Label({
                "id": "lblAlertTypeColHeader12",
                "isVisible": true,
                "left": "0px",
                "skin": "sknlblLato696c7311px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmAlertsManagement.AccountLevelAlert_UC\")",
                "top": "0px",
                "width": "100%",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var lblAlertTypeColValue12 = new kony.ui.Label({
                "id": "lblAlertTypeColValue12",
                "isVisible": true,
                "left": "0px",
                "skin": "sknlbl485C75LatoSemiBold13px",
                "text": "User Alert",
                "top": "20px",
                "width": "96%",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_TOP_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxAlertTypeDetailCol12.add(lblAlertTypeColHeader12, lblAlertTypeColValue12);
            var flxAlertTypeDetailCol13 = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                "clipBounds": true,
                "id": "flxAlertTypeDetailCol13",
                "isVisible": false,
                "layoutType": kony.flex.FREE_FORM,
                "left": "66%",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0px",
                "width": "33%",
                "zIndex": 1
            }, {}, {});
            flxAlertTypeDetailCol13.setDefaultUnit(kony.flex.DP);
            var lblAlertTypeColHeader13 = new kony.ui.Label({
                "id": "lblAlertTypeColHeader13",
                "isVisible": true,
                "left": "0px",
                "skin": "sknlblLato696c7311px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmAlertsManagement.AlertType_UC\")",
                "top": "0px",
                "width": "100%",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var lblAlertTypeColValue13 = new kony.ui.Label({
                "id": "lblAlertTypeColValue13",
                "isVisible": true,
                "left": "0px",
                "skin": "sknlbl485C75LatoSemiBold13px",
                "text": "AL001AL001AL001",
                "top": "20px",
                "width": "96%",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_TOP_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxAlertTypeDetailCol13.add(lblAlertTypeColHeader13, lblAlertTypeColValue13);
            var flxAlertTypeDetailCol21 = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                "clipBounds": true,
                "id": "flxAlertTypeDetailCol21",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "66%",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0px",
                "width": "33%",
                "zIndex": 1
            }, {}, {});
            flxAlertTypeDetailCol21.setDefaultUnit(kony.flex.DP);
            var lblAlertTypeColHeader21 = new kony.ui.Label({
                "id": "lblAlertTypeColHeader21",
                "isVisible": true,
                "left": "0px",
                "skin": "sknlblLato696c7311px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmAlertsManagement.DefaultFrequency_UC\")",
                "top": "0px",
                "width": "100%",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var lblAlertTypeColValue21 = new kony.ui.Label({
                "id": "lblAlertTypeColValue21",
                "isVisible": true,
                "left": "0px",
                "skin": "sknlbl485C75LatoSemiBold13px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmAlertsManagement.RetailBankingNative\")",
                "top": "20px",
                "width": "96%",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_TOP_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxAlertTypeDetailCol21.add(lblAlertTypeColHeader21, lblAlertTypeColValue21);
            flxAlertTypeDetailRow01.add(flxAlertTypeDetailCol11, flxAlertTypeDetailCol12, flxAlertTypeDetailCol13, flxAlertTypeDetailCol21);
            var flxAlertTypeDetailRow02 = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                "centerX": "50%",
                "clipBounds": true,
                "id": "flxAlertTypeDetailRow02",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0px",
                "isModalContainer": false,
                "right": "0px",
                "skin": "slFbox",
                "top": "30px",
                "width": "100%",
                "zIndex": 1
            }, {}, {});
            flxAlertTypeDetailRow02.setDefaultUnit(kony.flex.DP);
            var flxAlertTypeDetailCol22 = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                "clipBounds": true,
                "id": "flxAlertTypeDetailCol22",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0%",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0px",
                "width": "33%",
                "zIndex": 1
            }, {}, {});
            flxAlertTypeDetailCol22.setDefaultUnit(kony.flex.DP);
            var lblAlertTypeColHeader22 = new kony.ui.Label({
                "id": "lblAlertTypeColHeader22",
                "isVisible": true,
                "left": "0px",
                "skin": "sknlblLato696c7311px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmAlertsManagement.DefaultChannels_UC\")",
                "top": "0px",
                "width": "100%",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var lblAlertTypeColValue22 = new kony.ui.Label({
                "id": "lblAlertTypeColValue22",
                "isVisible": true,
                "left": "0px",
                "skin": "sknlbl485C75LatoSemiBold13px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmAlertsManagement.RetailCustomer\")",
                "top": "20px",
                "width": "96%",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_TOP_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxAlertTypeDetailCol22.add(lblAlertTypeColHeader22, lblAlertTypeColValue22);
            var flxAlertTypeDetailCol23 = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                "clipBounds": true,
                "id": "flxAlertTypeDetailCol23",
                "isVisible": false,
                "layoutType": kony.flex.FREE_FORM,
                "left": "66%",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0px",
                "width": "33%",
                "zIndex": 1
            }, {}, {});
            flxAlertTypeDetailCol23.setDefaultUnit(kony.flex.DP);
            var lblAlertTypeColHeader23 = new kony.ui.Label({
                "id": "lblAlertTypeColHeader23",
                "isVisible": true,
                "left": "0px",
                "skin": "sknlblLato696c7311px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmAlertsManagement.ApplicableAccountType_UC\")",
                "top": "0px",
                "width": "100%",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var lblAlertTypeColValue23 = new kony.ui.Label({
                "id": "lblAlertTypeColValue23",
                "isVisible": true,
                "left": "0px",
                "skin": "sknlbl485C75LatoSemiBold13px",
                "text": "Savings, Checking",
                "top": "20px",
                "width": "96%",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_TOP_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxAlertTypeDetailCol23.add(lblAlertTypeColHeader23, lblAlertTypeColValue23);
            flxAlertTypeDetailRow02.add(flxAlertTypeDetailCol22, flxAlertTypeDetailCol23);
            var flxAlertTypeDetailRow03 = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                "clipBounds": true,
                "id": "flxAlertTypeDetailRow03",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0px",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "30px",
                "width": "100%",
                "zIndex": 1
            }, {}, {});
            flxAlertTypeDetailRow03.setDefaultUnit(kony.flex.DP);
            var flxAlertTypeDescHeader = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "20px",
                "id": "flxAlertTypeDescHeader",
                "isVisible": true,
                "layoutType": kony.flex.FLOW_HORIZONTAL,
                "left": "0px",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0px",
                "width": "70%",
                "zIndex": 1
            }, {}, {});
            flxAlertTypeDescHeader.setDefaultUnit(kony.flex.DP);
            var lblAlertTypeDescHeader = new kony.ui.Label({
                "id": "lblAlertTypeDescHeader",
                "isVisible": true,
                "left": "0px",
                "skin": "sknlblLato696c7311px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmAlertsManagement.GroupDescription_UC\")",
                "top": "0px",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var lblAlertTypeDescLanguageValue = new kony.ui.Label({
                "id": "lblAlertTypeDescLanguageValue",
                "isVisible": true,
                "left": "0px",
                "skin": "skn696c73LatoRegItalic11px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.View.DESCRIPTION\")",
                "top": "0px",
                "width": "250dp",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxAlertTypeDescHeader.add(lblAlertTypeDescHeader, lblAlertTypeDescLanguageValue);
            var flxAlertTypeDescValue = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                "bottom": "10dp",
                "clipBounds": true,
                "id": "flxAlertTypeDescValue",
                "isVisible": true,
                "layoutType": kony.flex.FLOW_VERTICAL,
                "left": "0px",
                "isModalContainer": false,
                "skin": "sknServiceDetailsContent",
                "top": "20px",
                "width": "93%",
                "zIndex": 1
            }, {}, {});
            flxAlertTypeDescValue.setDefaultUnit(kony.flex.DP);
            var lblAlertTypeDescValue = new kony.ui.Label({
                "id": "lblAlertTypeDescValue",
                "isVisible": true,
                "left": "0px",
                "skin": "sknlbl485C75LatoSemiBold13px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmAlertsManagement.LoremIpsumA\")",
                "top": "0px",
                "width": "100%",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_TOP_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var lblAlertTypeViewAllLang = new kony.ui.Label({
                "id": "lblAlertTypeViewAllLang",
                "isVisible": true,
                "left": "0px",
                "skin": "sknLblLatoReg117eb013px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmAlertsManagement.ViewAllLanguages\")",
                "top": "5dp",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_TOP_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {
                "hoverSkin": "sknLbl117eb013pxHov"
            });
            flxAlertTypeDescValue.add(lblAlertTypeDescValue, lblAlertTypeViewAllLang);
            flxAlertTypeDetailRow03.add(flxAlertTypeDescHeader, flxAlertTypeDescValue);
            flxAlertTypeBodyContainer.add(flxAlertTypeDetailRow01, flxAlertTypeDetailRow02, flxAlertTypeDetailRow03);
            flxAlertTypeDetailBody.add(flxAlertTypeBodyContainer);
            flxAlertTypeDetailsTop.add(flxAlertTypeMessage, flxAlertTypeDetailHeader, flxAlertTypeDetailBody);
            var flxSubAlertsListing = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                "clipBounds": true,
                "id": "flxSubAlertsListing",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "isModalContainer": false,
                "right": "0dp",
                "skin": "sknFlxBgFFFFFFBorE4E6ECsz1pxR3px",
                "top": "15dp",
                "zIndex": 1
            }, {}, {});
            flxSubAlertsListing.setDefaultUnit(kony.flex.DP);
            var flxSubAlertListingHeader = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "50px",
                "id": "flxSubAlertListingHeader",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "20px",
                "isModalContainer": false,
                "right": "20px",
                "skin": "slFbox",
                "top": "0px",
                "zIndex": 1
            }, {}, {});
            flxSubAlertListingHeader.setDefaultUnit(kony.flex.DP);
            var lblSubAlertListingHeaderValue = new kony.ui.Label({
                "centerY": "50%",
                "id": "lblSubAlertListingHeaderValue",
                "isVisible": true,
                "left": "0px",
                "skin": "sknLatoRegular485C7518px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmAlertsManagementController.Alerts\")",
                "top": "0px",
                "width": "80%",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var flxAddSubAlertBtn = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "100%",
                "id": "flxAddSubAlertBtn",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "isModalContainer": false,
                "right": "0dp",
                "skin": "sknServiceDetailsContent",
                "top": "0dp",
                "width": "84px",
                "zIndex": 1
            }, {}, {});
            flxAddSubAlertBtn.setDefaultUnit(kony.flex.DP);
            var btnAddSubAlerts = new kony.ui.Button({
                "height": "22px",
                "id": "btnAddSubAlerts",
                "isVisible": true,
                "left": "0dp",
                "skin": "sknbtnf7f7faLatoRegular12Px485c75Border1px485C75Radius20px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmAlertsManagement.AddAlert\")",
                "top": "20dp",
                "width": "100%",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_CENTER,
                "displayText": true,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {
                "hoverSkin": "sknbtnffffffLatoRegular12Px485c75Border1px485C75Radius20px"
            });
            flxAddSubAlertBtn.add(btnAddSubAlerts);
            flxSubAlertListingHeader.add(lblSubAlertListingHeaderValue, flxAddSubAlertBtn);
            var flxSegSubAlerts = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                "bottom": "20dp",
                "clipBounds": false,
                "id": "flxSegSubAlerts",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "20px",
                "isModalContainer": false,
                "right": "20px",
                "skin": "slFbox",
                "top": "43px",
                "zIndex": 2
            }, {}, {});
            flxSegSubAlerts.setDefaultUnit(kony.flex.DP);
            var flxSubAlertsHeader = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "55px",
                "id": "flxSubAlertsHeader",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "isModalContainer": false,
                "right": "0dp",
                "skin": "sknflxffffffop100",
                "top": "0dp",
                "zIndex": 11
            }, {}, {});
            flxSubAlertsHeader.setDefaultUnit(kony.flex.DP);
            var flxSubAlertHeaderDisplayName = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "100%",
                "id": "flxSubAlertHeaderDisplayName",
                "isVisible": true,
                "layoutType": kony.flex.FLOW_HORIZONTAL,
                "left": "25px",
                "isModalContainer": false,
                "onClick": controller.AS_FlexContainer_a43b80cc872f4831807a918bd48f0ac3,
                "skin": "slFbox",
                "top": "0dp",
                "width": "16%",
                "zIndex": 1
            }, {}, {});
            flxSubAlertHeaderDisplayName.setDefaultUnit(kony.flex.DP);
            var lblSubAlertHeaderDisplayName = new kony.ui.Label({
                "centerY": "50%",
                "id": "lblSubAlertHeaderDisplayName",
                "isVisible": true,
                "left": "0px",
                "skin": "sknlblLato696c7312px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmAlertsManagement.AlertName_UC\")",
                "width": "100px",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxSubAlertHeaderDisplayName.add(lblSubAlertHeaderDisplayName);
            var flxSubAlertHeaderCode = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "100%",
                "id": "flxSubAlertHeaderCode",
                "isVisible": true,
                "layoutType": kony.flex.FLOW_HORIZONTAL,
                "left": "20%",
                "isModalContainer": false,
                "onClick": controller.AS_FlexContainer_if8688b9f1b14c0796ccfa73761ffbf7,
                "skin": "slFbox",
                "top": "0dp",
                "width": "12%",
                "zIndex": 1
            }, {}, {});
            flxSubAlertHeaderCode.setDefaultUnit(kony.flex.DP);
            var lblSubAlertHeaderCode = new kony.ui.Label({
                "centerY": "50%",
                "id": "lblSubAlertHeaderCode",
                "isVisible": true,
                "left": "0px",
                "skin": "sknlblLato696c7312px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmAlertsManagement.AlertCodeCaps\")",
                "width": "85dp",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxSubAlertHeaderCode.add(lblSubAlertHeaderCode);
            var flxSubAlertHeaderDescription = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "100%",
                "id": "flxSubAlertHeaderDescription",
                "isVisible": true,
                "layoutType": kony.flex.FLOW_HORIZONTAL,
                "left": "65%",
                "isModalContainer": false,
                "onClick": controller.AS_FlexContainer_f184888da9f042978e3b0332711e0319,
                "skin": "slFbox",
                "top": "0dp",
                "width": "31%",
                "zIndex": 1
            }, {}, {});
            flxSubAlertHeaderDescription.setDefaultUnit(kony.flex.DP);
            var lblSubAlertHeaderDescription = new kony.ui.Label({
                "centerY": "50%",
                "id": "lblSubAlertHeaderDescription",
                "isVisible": true,
                "left": "0px",
                "skin": "sknlblLato696c7312px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmAlertsManagement.FreqEnabled_UC\")",
                "width": "130dp",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxSubAlertHeaderDescription.add(lblSubAlertHeaderDescription);
            var flxAlertType = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "100%",
                "id": "flxAlertType",
                "isVisible": true,
                "layoutType": kony.flex.FLOW_HORIZONTAL,
                "left": "50%",
                "isModalContainer": false,
                "onClick": controller.AS_FlexContainer_f184888da9f042978e3b0332711e0319,
                "skin": "slFbox",
                "top": "0dp",
                "width": "20%",
                "zIndex": 1
            }, {}, {});
            flxAlertType.setDefaultUnit(kony.flex.DP);
            var lblAlertType = new kony.ui.Label({
                "centerY": "50%",
                "id": "lblAlertType",
                "isVisible": true,
                "left": "0px",
                "skin": "sknlblLato696c7312px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmAlertsManagement.AlertType_UC\")",
                "width": "130dp",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxAlertType.add(lblAlertType);
            var flxSubAlertHeaderStatus = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "100%",
                "id": "flxSubAlertHeaderStatus",
                "isVisible": true,
                "layoutType": kony.flex.FLOW_HORIZONTAL,
                "left": "82%",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0dp",
                "width": "10%",
                "zIndex": 1
            }, {}, {});
            flxSubAlertHeaderStatus.setDefaultUnit(kony.flex.DP);
            var lblSubAlertHeaderStatus = new kony.ui.Label({
                "centerY": "50%",
                "id": "lblSubAlertHeaderStatus",
                "isVisible": true,
                "left": "0",
                "skin": "sknlblLato696c7312px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.roles.STATUS\")",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {
                "hoverSkin": "sknlblLatoBold00000012px"
            });
            var lblIconSubAlertStatusFilter = new kony.ui.Label({
                "centerY": "50%",
                "id": "lblIconSubAlertStatusFilter",
                "isVisible": true,
                "left": "10dp",
                "skin": "sknIcon15px",
                "text": "",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxSubAlertHeaderStatus.add(lblSubAlertHeaderStatus, lblIconSubAlertStatusFilter);
            var lblSubAlertHeaderSeperator = new kony.ui.Label({
                "bottom": 0,
                "height": "1px",
                "id": "lblSubAlertHeaderSeperator",
                "isVisible": true,
                "left": "0dp",
                "skin": "sknLblSeparator696C73",
                "text": ".",
                "width": "100%",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxSubAlertsHeader.add(flxSubAlertHeaderDisplayName, flxSubAlertHeaderCode, flxSubAlertHeaderDescription, flxAlertType, flxSubAlertHeaderStatus, lblSubAlertHeaderSeperator);
            var flxSegRoles = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                "clipBounds": true,
                "id": "flxSegRoles",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "55dp",
                "width": "100%"
            }, {}, {});
            flxSegRoles.setDefaultUnit(kony.flex.DP);
            var segSubAlerts = new kony.ui.SegmentedUI2({
                "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                "data": [{
                    "btnAutoSubscribed": "Button",
                    "btnExt": "Button",
                    "fontIconStatusImg": "",
                    "lblAlertStatus": "Active",
                    "lblAlertType": "",
                    "lblCode": "AL001AL001",
                    "lblDescription": "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do",
                    "lblDescriptionTxt": "DESCRIPTION",
                    "lblDisplayName": "Admin Role",
                    "lblFreqEnabled": "Alert when the customer primary address is changed/updated",
                    "lblIconImgOptions": "",
                    "lblSeperator": ".-",
                    "lblToggle": "kony.i18n.getLocalizedString(\"i18n.userwidgetmodel.fontIconBreadcrumbsDown\")"
                }, {
                    "btnAutoSubscribed": "Button",
                    "btnExt": "Button",
                    "fontIconStatusImg": "",
                    "lblAlertStatus": "Active",
                    "lblAlertType": "",
                    "lblCode": "AL001AL001",
                    "lblDescription": "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do",
                    "lblDescriptionTxt": "DESCRIPTION",
                    "lblDisplayName": " Phone Number Change",
                    "lblFreqEnabled": "Alert when the customer primary phone number is changed/updated",
                    "lblIconImgOptions": "",
                    "lblSeperator": "-",
                    "lblToggle": "kony.i18n.getLocalizedString(\"i18n.userwidgetmodel.fontIconBreadcrumbsDown\")"
                }],
                "groupCells": false,
                "id": "segSubAlerts",
                "isVisible": true,
                "left": "1dp",
                "needPageIndicator": true,
                "onRowClick": controller.AS_Segment_d898d5eaf17548d398fc6a18e2acee55,
                "pageOffDotImage": "pageoffdot.png",
                "pageOnDotImage": "pageondot.png",
                "retainSelection": false,
                "right": 0,
                "rowFocusSkin": "seg2Focus",
                "rowSkin": "sknseg3pxRadius",
                "rowTemplate": "flxSubAlerts",
                "sectionHeaderSkin": "sliPhoneSegmentHeader",
                "selectionBehavior": constants.SEGUI_DEFAULT_BEHAVIOR,
                "separatorRequired": false,
                "showScrollbars": true,
                "top": "2dp",
                "viewType": constants.SEGUI_VIEW_TYPE_TABLEVIEW,
                "widgetDataMap": {
                    "btnAutoSubscribed": "btnAutoSubscribed",
                    "btnExt": "btnExt",
                    "flxDesc": "flxDesc",
                    "flxOptions": "flxOptions",
                    "flxRow1": "flxRow1",
                    "flxStatus": "flxStatus",
                    "flxSubAlerts": "flxSubAlerts",
                    "flxTagsGroup": "flxTagsGroup",
                    "flxToggle": "flxToggle",
                    "fontIconStatusImg": "fontIconStatusImg",
                    "lblAlertStatus": "lblAlertStatus",
                    "lblAlertType": "lblAlertType",
                    "lblCode": "lblCode",
                    "lblDescription": "lblDescription",
                    "lblDescriptionTxt": "lblDescriptionTxt",
                    "lblDisplayName": "lblDisplayName",
                    "lblFreqEnabled": "lblFreqEnabled",
                    "lblIconImgOptions": "lblIconImgOptions",
                    "lblSeperator": "lblSeperator",
                    "lblToggle": "lblToggle"
                },
                "zIndex": 1
            }, {
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxSegRoles.add(segSubAlerts);
            var flxNoResultFound = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "100dp",
                "id": "flxNoResultFound",
                "isVisible": false,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "isModalContainer": false,
                "right": "0dp",
                "skin": "slFbox",
                "top": "50dp",
                "zIndex": 1
            }, {}, {});
            flxNoResultFound.setDefaultUnit(kony.flex.DP);
            var rtxSearchMesg = new kony.ui.RichText({
                "centerX": "50%",
                "centerY": "50%",
                "height": "15dp",
                "id": "rtxSearchMesg",
                "isVisible": true,
                "left": "0",
                "skin": "sknRtxLato84939e12Px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmLogsController.No_results_found\")",
                "width": "80%",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_CENTER,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var noResultsWithButtonAlerts = new com.adminConsole.common.noResultsWithButton({
                "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                "id": "noResultsWithButtonAlerts",
                "isVisible": true,
                "layoutType": kony.flex.FLOW_VERTICAL,
                "left": "0dp",
                "masterType": constants.MASTER_TYPE_DEFAULT,
                "isModalContainer": false,
                "skin": "sknFlxFFFFFF100O",
                "top": "0dp",
                "width": "100%",
                "overrides": {
                    "btnAddRecord": {
                        "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmAlertsManagement.AddAlerts_LC\")",
                        "width": "86dp"
                    },
                    "lblNoRecordsAvailable": {
                        "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmAlertsManagement.AddAlertsUnderGroup\")"
                    }
                }
            }, {
                "overrides": {}
            }, {
                "overrides": {}
            });
            flxNoResultFound.add(rtxSearchMesg, noResultsWithButtonAlerts);
            var flxSelectOptions = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                "clipBounds": false,
                "id": "flxSelectOptions",
                "isVisible": false,
                "layoutType": kony.flex.FLOW_VERTICAL,
                "isModalContainer": false,
                "right": "20px",
                "skin": "slFbox",
                "top": "100px",
                "width": "150px",
                "zIndex": 100
            }, {}, {});
            flxSelectOptions.setDefaultUnit(kony.flex.DP);
            var flxSelectUpArrowImage = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "12px",
                "id": "flxSelectUpArrowImage",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0px",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0px",
                "width": "100%",
                "zIndex": 2
            }, {}, {});
            flxSelectUpArrowImage.setDefaultUnit(kony.flex.DP);
            var imgSelectUpArrowImage = new kony.ui.Image2({
                "height": "12dp",
                "id": "imgSelectUpArrowImage",
                "isVisible": true,
                "right": "20dp",
                "skin": "slImage",
                "src": "uparrow_2x.png",
                "top": "0dp",
                "width": "15dp",
                "zIndex": 1
            }, {
                "imageScaleMode": constants.IMAGE_SCALE_MODE_FIT_TO_DIMENSIONS,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxSelectUpArrowImage.add(imgSelectUpArrowImage);
            var flxSelectOptionsContainer = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                "clipBounds": true,
                "id": "flxSelectOptionsContainer",
                "isVisible": true,
                "layoutType": kony.flex.FLOW_VERTICAL,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "sknflxffffffop100dbdbe6Radius3px",
                "top": "0dp",
                "width": "100%"
            }, {}, {});
            flxSelectOptionsContainer.setDefaultUnit(kony.flex.DP);
            var flxEdit = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "35dp",
                "id": "flxEdit",
                "isVisible": true,
                "layoutType": kony.flex.FLOW_HORIZONTAL,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "CopyslFbox0i3963a83703242",
                "top": "0dp",
                "width": "100%",
                "zIndex": 1
            }, {}, {
                "hoverSkin": "sknContextualMenuEntryHover2"
            });
            flxEdit.setDefaultUnit(kony.flex.DP);
            var fonticonEdit = new kony.ui.Label({
                "centerY": "50%",
                "id": "fonticonEdit",
                "isVisible": true,
                "left": "15px",
                "skin": "sknFontIconOptionMenuRow",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmFAQ.fonticonEdit\")",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var imgOption1 = new kony.ui.Image2({
                "centerY": "50%",
                "height": "15dp",
                "id": "imgOption1",
                "isVisible": false,
                "left": "15dp",
                "skin": "slImage",
                "src": "edit2x.png",
                "width": "15dp",
                "zIndex": 1
            }, {
                "imageScaleMode": constants.IMAGE_SCALE_MODE_MAINTAIN_ASPECT_RATIO,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var lblOption1 = new kony.ui.Label({
                "centerY": "50%",
                "id": "lblOption1",
                "isVisible": true,
                "left": "15dp",
                "skin": "sknlblLatoBold35475f14px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.roles.Edit\")",
                "top": "4dp",
                "width": "70px",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxEdit.add(fonticonEdit, imgOption1, lblOption1);
            var flxDeactivate = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "35dp",
                "id": "flxDeactivate",
                "isVisible": true,
                "layoutType": kony.flex.FLOW_HORIZONTAL,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "CopyslFbox0i3963a83703242",
                "top": "0dp",
                "width": "100%",
                "zIndex": 1
            }, {}, {
                "hoverSkin": "sknContextualMenuEntryHover2"
            });
            flxDeactivate.setDefaultUnit(kony.flex.DP);
            var imgDeactivate = new kony.ui.Image2({
                "centerY": "50%",
                "height": "15dp",
                "id": "imgDeactivate",
                "isVisible": false,
                "left": "15dp",
                "skin": "slImage",
                "src": "edit2x.png",
                "width": "15dp",
                "zIndex": 1
            }, {
                "imageScaleMode": constants.IMAGE_SCALE_MODE_MAINTAIN_ASPECT_RATIO,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var lblDeactivateIcon = new kony.ui.Label({
                "centerY": "50%",
                "id": "lblDeactivateIcon",
                "isVisible": true,
                "left": "15px",
                "skin": "sknFontIconOptionMenuRow",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmFAQ.fonticonEdit\")",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var lblDeactivate = new kony.ui.Label({
                "centerY": "50%",
                "id": "lblDeactivate",
                "isVisible": true,
                "left": "15dp",
                "skin": "sknlblLatoBold35475f14px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.roles.Edit\")",
                "top": "4dp",
                "width": "70px",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxDeactivate.add(imgDeactivate, lblDeactivateIcon, lblDeactivate);
            flxSelectOptionsContainer.add(flxEdit, flxDeactivate);
            var flxSelectDownArrowImage = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "12px",
                "id": "flxSelectDownArrowImage",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0px",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "-1px",
                "width": "100%",
                "zIndex": 2
            }, {}, {});
            flxSelectDownArrowImage.setDefaultUnit(kony.flex.DP);
            var imgSelectDownArrowImage = new kony.ui.Image2({
                "height": "12dp",
                "id": "imgSelectDownArrowImage",
                "isVisible": true,
                "right": "20dp",
                "skin": "slImage",
                "src": "downarrow_2x.png",
                "top": "0dp",
                "width": "15dp",
                "zIndex": 1
            }, {
                "imageScaleMode": constants.IMAGE_SCALE_MODE_FIT_TO_DIMENSIONS,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxSelectDownArrowImage.add(imgSelectDownArrowImage);
            flxSelectOptions.add(flxSelectUpArrowImage, flxSelectOptionsContainer, flxSelectDownArrowImage);
            flxSegSubAlerts.add(flxSubAlertsHeader, flxSegRoles, flxNoResultFound, flxSelectOptions);
            var flxSubAlertsStatusFilter = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                "clipBounds": true,
                "id": "flxSubAlertsStatusFilter",
                "isVisible": false,
                "layoutType": kony.flex.FREE_FORM,
                "isModalContainer": false,
                "right": "100dp",
                "skin": "slFbox",
                "top": "72px",
                "width": "120px",
                "zIndex": 5
            }, {}, {});
            flxSubAlertsStatusFilter.setDefaultUnit(kony.flex.DP);
            var subAlertsStatusFilterMenu = new com.adminConsole.common.statusFilterMenu({
                "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                "id": "subAlertsStatusFilterMenu",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "masterType": constants.MASTER_TYPE_DEFAULT,
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "5dp",
                "width": "100%",
                "zIndex": 5,
                "overrides": {
                    "imgUpArrow": {
                        "src": "uparrow_2x.png"
                    },
                    "segStatusFilterDropdown": {
                        "left": "0dp",
                        "top": "5dp"
                    }
                }
            }, {
                "overrides": {}
            }, {
                "overrides": {}
            });
            flxSubAlertsStatusFilter.add(subAlertsStatusFilterMenu);
            flxSubAlertsListing.add(flxSubAlertListingHeader, flxSegSubAlerts, flxSubAlertsStatusFilter);
            flxAlertTypeDetailsScreen.add(flxAlertTypeDetailsTop, flxSubAlertsListing);
            var flxAddAlertTypeScreen = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                "bottom": "15dp",
                "clipBounds": true,
                "id": "flxAddAlertTypeScreen",
                "isVisible": false,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "sknFlxBgFFFFFFBorE4E6ECsz1pxR3px",
                "top": "0dp",
                "width": "100%",
                "zIndex": 1
            }, {}, {});
            flxAddAlertTypeScreen.setDefaultUnit(kony.flex.DP);
            var flxAddAlertTypeContainer = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                "clipBounds": true,
                "id": "flxAddAlertTypeContainer",
                "isVisible": true,
                "layoutType": kony.flex.FLOW_VERTICAL,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0dp",
                "width": "100%",
                "zIndex": 1
            }, {}, {});
            flxAddAlertTypeContainer.setDefaultUnit(kony.flex.DP);
            var flxAddAlertTypeHeading = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "60dp",
                "id": "flxAddAlertTypeHeading",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0dp",
                "width": "100%",
                "zIndex": 1
            }, {}, {});
            flxAddAlertTypeHeading.setDefaultUnit(kony.flex.DP);
            var lblAddAlertTypeName = new kony.ui.Label({
                "centerY": "50%",
                "id": "lblAddAlertTypeName",
                "isVisible": true,
                "left": "20px",
                "skin": "sknLbl192B4518pxLatoReg",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmAlertsManagement.lblViewAlertName\")",
                "top": "0px",
                "width": "70%",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var lblAddAlertTypeStatusValue = new kony.ui.Label({
                "centerY": "50%",
                "height": "20px",
                "id": "lblAddAlertTypeStatusValue",
                "isVisible": true,
                "right": "67dp",
                "skin": "sknlblLatoBold35475f14px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmAlertsManagement.Active\")",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var flxAddAlertTypeStatusSwitch = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "centerY": "51%",
                "clipBounds": true,
                "height": "25dp",
                "id": "flxAddAlertTypeStatusSwitch",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "isModalContainer": false,
                "right": "20dp",
                "skin": "slFbox",
                "width": "38dp"
            }, {}, {});
            flxAddAlertTypeStatusSwitch.setDefaultUnit(kony.flex.DP);
            var addAlertTypeStatusSwitch = new com.adminConsole.common.customSwitch({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "height": "25dp",
                "id": "addAlertTypeStatusSwitch",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "masterType": constants.MASTER_TYPE_DEFAULT,
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0dp",
                "width": "100%",
                "overrides": {
                    "customSwitch": {
                        "centerY": "viz.val_cleared",
                        "height": "25dp",
                        "isVisible": true,
                        "left": "0dp",
                        "right": "viz.val_cleared",
                        "top": "0dp",
                        "width": "100%"
                    },
                    "switchToggle": {
                        "isVisible": true
                    }
                }
            }, {
                "overrides": {}
            }, {
                "overrides": {}
            });
            flxAddAlertTypeStatusSwitch.add(addAlertTypeStatusSwitch);
            var flxEditGroupHeaderSeperator = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "bottom": "0dp",
                "clipBounds": true,
                "height": "1dp",
                "id": "flxEditGroupHeaderSeperator",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "20dp",
                "isModalContainer": false,
                "right": "20dp",
                "skin": "sknflxd5d9ddop100",
                "zIndex": 1
            }, {}, {});
            flxEditGroupHeaderSeperator.setDefaultUnit(kony.flex.DP);
            flxEditGroupHeaderSeperator.add();
            flxAddAlertTypeHeading.add(lblAddAlertTypeName, lblAddAlertTypeStatusValue, flxAddAlertTypeStatusSwitch, flxEditGroupHeaderSeperator);
            var flxAddAlertTypeRow01 = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "90dp",
                "id": "flxAddAlertTypeRow01",
                "isVisible": true,
                "layoutType": kony.flex.FLOW_HORIZONTAL,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "20dp",
                "width": "100%",
                "zIndex": 1
            }, {}, {});
            flxAddAlertTypeRow01.setDefaultUnit(kony.flex.DP);
            var flxAddAlertTypeCol11 = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "95dp",
                "id": "flxAddAlertTypeCol11",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "20dp",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0dp",
                "width": "30%",
                "zIndex": 1
            }, {}, {});
            flxAddAlertTypeCol11.setDefaultUnit(kony.flex.DP);
            var lblAddAlertTypeHeader11 = new kony.ui.Label({
                "id": "lblAddAlertTypeHeader11",
                "isVisible": true,
                "left": "0dp",
                "skin": "sknLbl485C75LatoRegular13Px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmAlertsManagement.Alert_Group_Code\")",
                "top": "0dp",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var dataEntryAddAlertType11 = new com.adminConsole.common.dataEntry({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "height": "85dp",
                "id": "dataEntryAddAlertType11",
                "isVisible": false,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "masterType": constants.MASTER_TYPE_DEFAULT,
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0dp",
                "width": "100%",
                "zIndex": 1,
                "overrides": {
                    "dataEntry": {
                        "isVisible": false
                    },
                    "flxError": {
                        "isVisible": false
                    },
                    "lblData": {
                        "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmAlertsManagement.Alert_Group_Code\")"
                    },
                    "lblErrorMsg": {
                        "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmAlertsManagement.Alert_group_code_name_cannot_be_empty\")",
                        "width": "90%"
                    },
                    "lblTextCounter": {
                        "isVisible": false
                    },
                    "tbxData": {
                        "maxTextLength": 20,
                        "placeholder": "Alert Group Code"
                    }
                }
            }, {
                "overrides": {}
            }, {
                "overrides": {}
            });
            var lstBoxAddAlertType11 = new kony.ui.ListBox({
                "focusSkin": "defListBoxFocus",
                "height": "40dp",
                "id": "lstBoxAddAlertType11",
                "isVisible": true,
                "left": "0dp",
                "masterData": [
                    ["lb1", "Placeholder One"],
                    ["lb2", "Placeholder Two"],
                    ["lb3", "Placeholder Three"]
                ],
                "skin": "sknlbxBgffffffBorderc1c9ceRadius3Px",
                "top": "25dp",
                "width": "100%",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [3, 0, 0, 0],
                "paddingInPixel": false
            }, {
                "multiSelect": false
            });
            var flxAddAlertTypeError11 = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                "clipBounds": true,
                "id": "flxAddAlertTypeError11",
                "isVisible": false,
                "layoutType": kony.flex.FLOW_HORIZONTAL,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "68dp",
                "width": "100%",
                "zIndex": 1
            }, {}, {});
            flxAddAlertTypeError11.setDefaultUnit(kony.flex.DP);
            var lblIconErrAlertTypeCode = new kony.ui.Label({
                "id": "lblIconErrAlertTypeCode",
                "isVisible": true,
                "left": "0dp",
                "skin": "sknErrorIcon",
                "text": "",
                "top": "0dp",
                "width": "15dp",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var lblErrMsgAlertTypeCode = new kony.ui.Label({
                "id": "lblErrMsgAlertTypeCode",
                "isVisible": true,
                "left": "0dp",
                "skin": "sknlblError",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmAlertsManagement.PleaseSelectCode\")",
                "top": "0dp",
                "width": "180dp",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxAddAlertTypeError11.add(lblIconErrAlertTypeCode, lblErrMsgAlertTypeCode);
            flxAddAlertTypeCol11.add(lblAddAlertTypeHeader11, dataEntryAddAlertType11, lstBoxAddAlertType11, flxAddAlertTypeError11);
            var flxAddAlertTypeCol12 = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "95dp",
                "id": "flxAddAlertTypeCol12",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "15dp",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0dp",
                "width": "30%",
                "zIndex": 1
            }, {}, {});
            flxAddAlertTypeCol12.setDefaultUnit(kony.flex.DP);
            var dataEntryAddAlertType12 = new com.adminConsole.common.dataEntry({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "height": "85dp",
                "id": "dataEntryAddAlertType12",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "masterType": constants.MASTER_TYPE_DEFAULT,
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0dp",
                "width": "100%",
                "zIndex": 1,
                "overrides": {
                    "lblData": {
                        "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmAlertsManagement.Alert_Group_Name\")"
                    },
                    "lblErrorMsg": {
                        "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmAlertsManagement.Alert_name_cannot_be_empty\")",
                        "right": "viz.val_cleared",
                        "width": "90%"
                    },
                    "lblTextCounter": {
                        "isVisible": false
                    },
                    "tbxData": {
                        "i18n_placeholder": "kony.i18n.getLocalizedString(\"i18n.frmAlertsManagement.Alert_Group_Name\")",
                        "maxTextLength": 50
                    }
                }
            }, {
                "overrides": {}
            }, {
                "overrides": {}
            });
            flxAddAlertTypeCol12.add(dataEntryAddAlertType12);
            var flxAddAlertTypeCol13 = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "100dp",
                "id": "flxAddAlertTypeCol13",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "15dp",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0dp",
                "width": "30%",
                "zIndex": 1
            }, {}, {});
            flxAddAlertTypeCol13.setDefaultUnit(kony.flex.DP);
            var lblAddAlertTypeHeader13 = new kony.ui.Label({
                "id": "lblAddAlertTypeHeader13",
                "isVisible": true,
                "left": "0dp",
                "skin": "sknLbl485C75LatoRegular13Px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmAlertsManagement.AccountLevelAlert\")",
                "top": "0dp",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_TOP_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var lblAddAlertTypeValue13 = new kony.ui.Label({
                "id": "lblAddAlertTypeValue13",
                "isVisible": false,
                "left": "0dp",
                "skin": "sknLbl485C75LatoRegular13Px",
                "text": "User Alert",
                "top": "30dp",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_TOP_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var radioBtnAddAlertType13 = new com.adminConsole.common.customRadioButtonGroup({
                "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                "id": "radioBtnAddAlertType13",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "masterType": constants.MASTER_TYPE_DEFAULT,
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "35dp",
                "width": "100%",
                "overrides": {
                    "customRadioButtonGroup": {
                        "top": "35dp"
                    },
                    "flxRadioButton1": {
                        "width": "50dp"
                    },
                    "flxRadioButton2": {
                        "left": "10dp"
                    },
                    "flxRadioButton3": {
                        "isVisible": false
                    },
                    "imgRadioButton1": {
                        "src": "radionormal_1x.png"
                    },
                    "imgRadioButton2": {
                        "src": "radionormal_1x.png"
                    },
                    "imgRadioButton3": {
                        "src": "radionormal_1x.png"
                    },
                    "imgRadioButton4": {
                        "src": "radionormal_1x.png"
                    },
                    "imgRadioButton5": {
                        "src": "radionormal_1x.png"
                    },
                    "lblRadioButtonValue1": {
                        "text": "Yes"
                    },
                    "lblRadioButtonValue2": {
                        "text": "No"
                    }
                }
            }, {
                "overrides": {}
            }, {
                "overrides": {}
            });
            var flxAddAlertTypeError13 = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                "clipBounds": true,
                "id": "flxAddAlertTypeError13",
                "isVisible": false,
                "layoutType": kony.flex.FLOW_HORIZONTAL,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "65dp",
                "width": "100%",
                "zIndex": 1
            }, {}, {});
            flxAddAlertTypeError13.setDefaultUnit(kony.flex.DP);
            var lblErrIconAddAlertType13 = new kony.ui.Label({
                "id": "lblErrIconAddAlertType13",
                "isVisible": true,
                "left": "0dp",
                "skin": "sknErrorIcon",
                "text": "",
                "top": "0dp",
                "width": "15dp",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var lblErrMsgAddAlertType13 = new kony.ui.Label({
                "id": "lblErrMsgAddAlertType13",
                "isVisible": true,
                "left": "5dp",
                "skin": "sknlblError",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmAlertsManagement.Please_select_type\")",
                "top": "0dp",
                "width": "170dp",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxAddAlertTypeError13.add(lblErrIconAddAlertType13, lblErrMsgAddAlertType13);
            flxAddAlertTypeCol13.add(lblAddAlertTypeHeader13, lblAddAlertTypeValue13, radioBtnAddAlertType13, flxAddAlertTypeError13);
            flxAddAlertTypeRow01.add(flxAddAlertTypeCol11, flxAddAlertTypeCol12, flxAddAlertTypeCol13);
            var flxAddAlertTypeRow02 = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                "clipBounds": true,
                "id": "flxAddAlertTypeRow02",
                "isVisible": false,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "10dp",
                "width": "100%",
                "zIndex": 1
            }, {}, {});
            flxAddAlertTypeRow02.setDefaultUnit(kony.flex.DP);
            var flxAddAlertTypeCol21 = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "80dp",
                "id": "flxAddAlertTypeCol21",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "20dp",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0dp",
                "width": "30%",
                "zIndex": 1
            }, {}, {});
            flxAddAlertTypeCol21.setDefaultUnit(kony.flex.DP);
            var lblAddAlertTypeHeader21 = new kony.ui.Label({
                "id": "lblAddAlertTypeHeader21",
                "isVisible": true,
                "left": "0dp",
                "skin": "sknLbl485C75LatoRegular13Px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmAlertsManagement.AlertType_LC\")",
                "top": "0dp",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_TOP_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var lblAddAlertTypeValue21 = new kony.ui.Label({
                "id": "lblAddAlertTypeValue21",
                "isVisible": false,
                "left": "0dp",
                "skin": "sknLbl485C75LatoRegular13Px",
                "text": "User Alert",
                "top": "30dp",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_TOP_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var radioBtnAddAlertType21 = new com.adminConsole.common.customRadioButtonGroup({
                "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                "id": "radioBtnAddAlertType21",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "masterType": constants.MASTER_TYPE_DEFAULT,
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "30dp",
                "width": "100%",
                "overrides": {
                    "customRadioButtonGroup": {
                        "top": "30dp"
                    },
                    "flxRadioButton3": {
                        "isVisible": false
                    },
                    "imgRadioButton1": {
                        "src": "radionormal_1x.png"
                    },
                    "imgRadioButton2": {
                        "src": "radionormal_1x.png"
                    },
                    "imgRadioButton3": {
                        "src": "radionormal_1x.png"
                    },
                    "imgRadioButton4": {
                        "src": "radionormal_1x.png"
                    },
                    "imgRadioButton5": {
                        "src": "radionormal_1x.png"
                    },
                    "lblRadioButtonValue1": {
                        "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmAlertsManagement.GlobalAlert\")"
                    },
                    "lblRadioButtonValue2": {
                        "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmAlertsManagement.UserAlert\")"
                    }
                }
            }, {
                "overrides": {}
            }, {
                "overrides": {}
            });
            var flxAddAlertTypeError21 = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                "clipBounds": true,
                "id": "flxAddAlertTypeError21",
                "isVisible": false,
                "layoutType": kony.flex.FLOW_HORIZONTAL,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "55dp",
                "width": "100%",
                "zIndex": 1
            }, {}, {});
            flxAddAlertTypeError21.setDefaultUnit(kony.flex.DP);
            var lblIconErrAddAlertType21 = new kony.ui.Label({
                "id": "lblIconErrAddAlertType21",
                "isVisible": true,
                "left": "0dp",
                "skin": "sknErrorIcon",
                "text": "",
                "top": "0dp",
                "width": "15dp",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var lblErrMsgAddAlertType21 = new kony.ui.Label({
                "id": "lblErrMsgAddAlertType21",
                "isVisible": true,
                "left": "5dp",
                "skin": "sknlblError",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmAlertsManagement.Please_select_type\")",
                "top": "0dp",
                "width": "170dp",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxAddAlertTypeError21.add(lblIconErrAddAlertType21, lblErrMsgAddAlertType21);
            flxAddAlertTypeCol21.add(lblAddAlertTypeHeader21, lblAddAlertTypeValue21, radioBtnAddAlertType21, flxAddAlertTypeError21);
            flxAddAlertTypeRow02.add(flxAddAlertTypeCol21);
            var flxAddAlertTypeRow03 = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                "clipBounds": true,
                "id": "flxAddAlertTypeRow03",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0dp",
                "width": "100%",
                "zIndex": 2
            }, {}, {});
            flxAddAlertTypeRow03.setDefaultUnit(kony.flex.DP);
            var lblAddAlertTypeHeader31 = new kony.ui.Label({
                "id": "lblAddAlertTypeHeader31",
                "isVisible": true,
                "left": "20px",
                "skin": "sknLbl192b45LatoReg16px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmAlertsManagement.DefaultFrequencyDetails\")",
                "top": "0px",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var frequencySelectionAlertType = new com.adminConsole.alerts.frequencySelection({
                "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                "id": "frequencySelectionAlertType",
                "isVisible": true,
                "layoutType": kony.flex.FLOW_HORIZONTAL,
                "left": "20dp",
                "masterType": constants.MASTER_TYPE_DEFAULT,
                "isModalContainer": false,
                "right": "20dp",
                "skin": "sknFlxBgF8F9FABrE1E5EER3px",
                "top": "33dp",
                "overrides": {
                    "frequencySelection": {
                        "left": "20dp",
                        "right": "20dp",
                        "top": "33dp",
                        "width": "viz.val_cleared"
                    }
                }
            }, {
                "overrides": {}
            }, {
                "overrides": {}
            });
            flxAddAlertTypeRow03.add(lblAddAlertTypeHeader31, frequencySelectionAlertType);
            var flxAddAlertTypeRow04 = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                "clipBounds": true,
                "id": "flxAddAlertTypeRow04",
                "isVisible": true,
                "layoutType": kony.flex.FLOW_VERTICAL,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "30dp",
                "width": "100%",
                "zIndex": 2
            }, {}, {});
            flxAddAlertTypeRow04.setDefaultUnit(kony.flex.DP);
            var lblAddAlertTypeHeader41 = new kony.ui.Label({
                "id": "lblAddAlertTypeHeader41",
                "isVisible": true,
                "left": "20px",
                "skin": "sknLbl192b45LatoReg16px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmAlertsManagement.DefaultChannels\")",
                "top": "0px",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var flxAddAlertTypeChannelCont = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "35dp",
                "id": "flxAddAlertTypeChannelCont",
                "isVisible": true,
                "layoutType": kony.flex.FLOW_HORIZONTAL,
                "left": "20dp",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "15dp",
                "width": "100%",
                "zIndex": 2
            }, {}, {});
            flxAddAlertTypeChannelCont.setDefaultUnit(kony.flex.DP);
            flxAddAlertTypeChannelCont.add();
            var flxAddAlertTypeError41 = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                "clipBounds": true,
                "id": "flxAddAlertTypeError41",
                "isVisible": false,
                "layoutType": kony.flex.FLOW_HORIZONTAL,
                "left": "20dp",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "5dp",
                "width": "100%",
                "zIndex": 1
            }, {}, {});
            flxAddAlertTypeError41.setDefaultUnit(kony.flex.DP);
            var lblIconErrAddAlertType41 = new kony.ui.Label({
                "id": "lblIconErrAddAlertType41",
                "isVisible": true,
                "left": "0dp",
                "skin": "sknErrorIcon",
                "text": "",
                "top": "0dp",
                "width": "15dp",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var lblErrMsgAddAlertType41 = new kony.ui.Label({
                "id": "lblErrMsgAddAlertType41",
                "isVisible": true,
                "left": "5dp",
                "skin": "sknlblError",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmAlertsManagement.lblNoCheckBoxSelected\")",
                "top": "0dp",
                "width": "170dp",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxAddAlertTypeError41.add(lblIconErrAddAlertType41, lblErrMsgAddAlertType41);
            flxAddAlertTypeRow04.add(lblAddAlertTypeHeader41, flxAddAlertTypeChannelCont, flxAddAlertTypeError41);
            var flxAddAlertBottom = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                "clipBounds": true,
                "id": "flxAddAlertBottom",
                "isVisible": true,
                "layoutType": kony.flex.FLOW_VERTICAL,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "30dp",
                "width": "100%"
            }, {}, {});
            flxAddAlertBottom.setDefaultUnit(kony.flex.DP);
            var flxAddAlertTypeLanguageHeader = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                "clipBounds": true,
                "id": "flxAddAlertTypeLanguageHeader",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0dp",
                "width": "100%",
                "zIndex": 1
            }, {}, {});
            flxAddAlertTypeLanguageHeader.setDefaultUnit(kony.flex.DP);
            var lblAddAlertTypeLanguageValue = new kony.ui.Label({
                "id": "lblAddAlertTypeLanguageValue",
                "isVisible": true,
                "left": "20dp",
                "skin": "sknLbl192b45LatoReg16px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmAlertsManagement.Supported_Language\")",
                "top": "0dp",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var flxAddAlertTypeLangError = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                "clipBounds": true,
                "id": "flxAddAlertTypeLangError",
                "isVisible": true,
                "layoutType": kony.flex.FLOW_HORIZONTAL,
                "left": "20dp",
                "isModalContainer": false,
                "right": "20dp",
                "skin": "slFbox",
                "top": "30dp",
                "zIndex": 1
            }, {}, {});
            flxAddAlertTypeLangError.setDefaultUnit(kony.flex.DP);
            var errorFlagMsgAddAlertType = new com.adminConsole.common.errorFlagMessage({
                "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                "id": "errorFlagMsgAddAlertType",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "masterType": constants.MASTER_TYPE_DEFAULT,
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0dp",
                "width": "100%",
                "overrides": {
                    "lblErrorValue": {
                        "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmAlertsManagement.AddSupportedLanguageToProceed\")"
                    }
                }
            }, {
                "overrides": {}
            }, {
                "overrides": {}
            });
            flxAddAlertTypeLangError.add(errorFlagMsgAddAlertType);
            flxAddAlertTypeLanguageHeader.add(lblAddAlertTypeLanguageValue, flxAddAlertTypeLangError);
            var flxAddAlertTypeLanguageSegment = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                "clipBounds": true,
                "id": "flxAddAlertTypeLanguageSegment",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "20dp",
                "width": "100%",
                "zIndex": 1
            }, {}, {});
            flxAddAlertTypeLanguageSegment.setDefaultUnit(kony.flex.DP);
            var flxAddAlertTypeLangSep = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "1dp",
                "id": "flxAddAlertTypeLangSep",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "20dp",
                "isModalContainer": false,
                "right": "20dp",
                "skin": "sknSeparatorCsr",
                "top": "0dp",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {}, {});
            flxAddAlertTypeLangSep.setDefaultUnit(kony.flex.DP);
            flxAddAlertTypeLangSep.add();
            var flxAddAlertTypeLangSegHeader = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "50dp",
                "id": "flxAddAlertTypeLangSegHeader",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "20dp",
                "isModalContainer": false,
                "right": "20dp",
                "skin": "slFbox",
                "top": "0dp",
                "zIndex": 1
            }, {}, {});
            flxAddAlertTypeLangSegHeader.setDefaultUnit(kony.flex.DP);
            var flxAddAlertTypeSegLanguage = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "100%",
                "id": "flxAddAlertTypeSegLanguage",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0dp",
                "width": "18%",
                "zIndex": 1
            }, {}, {});
            flxAddAlertTypeSegLanguage.setDefaultUnit(kony.flex.DP);
            var lblAddAlertTypeSegLanguage = new kony.ui.Label({
                "centerY": "50%",
                "id": "lblAddAlertTypeSegLanguage",
                "isVisible": true,
                "left": "10dp",
                "skin": "sknlblLato696c7312px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmAlertsManagement.Language_UC\")",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxAddAlertTypeSegLanguage.add(lblAddAlertTypeSegLanguage);
            var flxAddAlertTypeSegDisplayName = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "100%",
                "id": "flxAddAlertTypeSegDisplayName",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "21%",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0dp",
                "width": "23%",
                "zIndex": 1
            }, {}, {});
            flxAddAlertTypeSegDisplayName.setDefaultUnit(kony.flex.DP);
            var lblAddAlertTypeSegDisplayName = new kony.ui.Label({
                "centerY": "50%",
                "id": "lblAddAlertTypeSegDisplayName",
                "isVisible": true,
                "left": "20dp",
                "skin": "sknlblLato696c7312px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.View.DISPLAYNAME\")",
                "width": "100dp",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxAddAlertTypeSegDisplayName.add(lblAddAlertTypeSegDisplayName);
            var flxAddAlertTypeSegDescription = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "100%",
                "id": "flxAddAlertTypeSegDescription",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "44%",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0dp",
                "width": "20%",
                "zIndex": 1
            }, {}, {});
            flxAddAlertTypeSegDescription.setDefaultUnit(kony.flex.DP);
            var lblAddAlertTypeSegDescription = new kony.ui.Label({
                "centerY": "50%",
                "id": "lblAddAlertTypeSegDescription",
                "isVisible": true,
                "left": "20dp",
                "skin": "sknlblLato696c7312px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmAlertsManagement.GroupDescription_UC\")",
                "width": "150dp",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxAddAlertTypeSegDescription.add(lblAddAlertTypeSegDescription);
            var flxAddAlertTypeSegLine = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "bottom": "0dp",
                "clipBounds": true,
                "height": "1dp",
                "id": "flxAddAlertTypeSegLine",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "isModalContainer": false,
                "right": "0dp",
                "skin": "sknFlxBg696C73Op100",
                "zIndex": 1
            }, {}, {});
            flxAddAlertTypeSegLine.setDefaultUnit(kony.flex.DP);
            flxAddAlertTypeSegLine.add();
            flxAddAlertTypeLangSegHeader.add(flxAddAlertTypeSegLanguage, flxAddAlertTypeSegDisplayName, flxAddAlertTypeSegDescription, flxAddAlertTypeSegLine);
            var flxAddAlertTypeLangSegList = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                "clipBounds": true,
                "id": "flxAddAlertTypeLangSegList",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "50dp",
                "width": "100%",
                "zIndex": 1
            }, {}, {});
            flxAddAlertTypeLangSegList.setDefaultUnit(kony.flex.DP);
            var segaddalertTypeLanguages = new kony.ui.SegmentedUI2({
                "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                "data": [{
                    "lblAddAlertTypeSegDescription": "Label",
                    "lblAddAlertTypeSegDisplayName": "Label",
                    "lblAddAlertTypeSegLanguage": "Label",
                    "lblDescCount": "Label",
                    "lblIconDeleteLang": "Label",
                    "lblSeprator": "Label",
                    "lstBoxSelectLanguage": {
                        "masterData": [
                            ["lb1", "English(US)"],
                            ["lb2", "English(UK)"],
                            ["lb3", "Spanish"],
                            ["lb4", "French"],
                            ["lb0", "Select"]
                        ],
                        "selectedKey": "lb0",
                        "selectedKeys": null,
                        "selectedKeyValue": ["lb0", "Select"]
                    },
                    "tbxDescription": "",
                    "tbxDisplayName": {
                        "placeholder": "",
                        "text": ""
                    }
                }, {
                    "lblAddAlertTypeSegDescription": "Label",
                    "lblAddAlertTypeSegDisplayName": "Label",
                    "lblAddAlertTypeSegLanguage": "Label",
                    "lblDescCount": "Label",
                    "lblIconDeleteLang": "Label",
                    "lblSeprator": "Label",
                    "lstBoxSelectLanguage": {
                        "masterData": [
                            ["lb1", "English(US)"],
                            ["lb2", "English(UK)"],
                            ["lb3", "Spanish"],
                            ["lb4", "French"],
                            ["lb0", "Select"]
                        ],
                        "selectedKey": "lb0",
                        "selectedKeys": null,
                        "selectedKeyValue": ["lb0", "Select"]
                    },
                    "tbxDescription": "",
                    "tbxDisplayName": {
                        "placeholder": "",
                        "text": ""
                    }
                }, {
                    "lblAddAlertTypeSegDescription": "Label",
                    "lblAddAlertTypeSegDisplayName": "Label",
                    "lblAddAlertTypeSegLanguage": "Label",
                    "lblDescCount": "Label",
                    "lblIconDeleteLang": "Label",
                    "lblSeprator": "Label",
                    "lstBoxSelectLanguage": {
                        "masterData": [
                            ["lb1", "English(US)"],
                            ["lb2", "English(UK)"],
                            ["lb3", "Spanish"],
                            ["lb4", "French"],
                            ["lb0", "Select"]
                        ],
                        "selectedKey": "lb0",
                        "selectedKeys": null,
                        "selectedKeyValue": ["lb0", "Select"]
                    },
                    "tbxDescription": "",
                    "tbxDisplayName": {
                        "placeholder": "",
                        "text": ""
                    }
                }],
                "groupCells": false,
                "id": "segaddalertTypeLanguages",
                "isVisible": true,
                "left": "20dp",
                "needPageIndicator": true,
                "pageOffDotImage": "pageoffdot.png",
                "pageOnDotImage": "pageondot.png",
                "retainSelection": false,
                "right": "20dp",
                "rowFocusSkin": "seg2Focus",
                "rowSkin": "seg2Normal",
                "rowTemplate": "flxAlertsLanguageData",
                "sectionHeaderSkin": "sliPhoneSegmentHeader",
                "selectionBehavior": constants.SEGUI_DEFAULT_BEHAVIOR,
                "separatorRequired": false,
                "showScrollbars": false,
                "top": "0dp",
                "viewType": constants.SEGUI_VIEW_TYPE_TABLEVIEW,
                "widgetDataMap": {
                    "flxAddDescription": "flxAddDescription",
                    "flxAddDisplayName": "flxAddDisplayName",
                    "flxAddLanguage": "flxAddLanguage",
                    "flxAlertsLanguageData": "flxAlertsLanguageData",
                    "flxDelete": "flxDelete",
                    "flxDescription": "flxDescription",
                    "flxLanguageRowContainer": "flxLanguageRowContainer",
                    "lblAddAlertTypeSegDescription": "lblAddAlertTypeSegDescription",
                    "lblAddAlertTypeSegDisplayName": "lblAddAlertTypeSegDisplayName",
                    "lblAddAlertTypeSegLanguage": "lblAddAlertTypeSegLanguage",
                    "lblDescCount": "lblDescCount",
                    "lblIconDeleteLang": "lblIconDeleteLang",
                    "lblSeprator": "lblSeprator",
                    "lstBoxSelectLanguage": "lstBoxSelectLanguage",
                    "tbxDescription": "tbxDescription",
                    "tbxDisplayName": "tbxDisplayName"
                },
                "zIndex": 1
            }, {
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxAddAlertTypeLangSegList.add(segaddalertTypeLanguages);
            flxAddAlertTypeLanguageSegment.add(flxAddAlertTypeLangSep, flxAddAlertTypeLangSegHeader, flxAddAlertTypeLangSegList);
            flxAddAlertBottom.add(flxAddAlertTypeLanguageHeader, flxAddAlertTypeLanguageSegment);
            var flxAddAlertTypeButtons = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "80dp",
                "id": "flxAddAlertTypeButtons",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0dp",
                "width": "100%",
                "zIndex": 1
            }, {}, {});
            flxAddAlertTypeButtons.setDefaultUnit(kony.flex.DP);
            var flxAddAlertTypeButtonSep = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "1dp",
                "id": "flxAddAlertTypeButtonSep",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "isModalContainer": false,
                "right": "0dp",
                "skin": "sknSeparatorCsr",
                "top": "0dp",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {}, {});
            flxAddAlertTypeButtonSep.setDefaultUnit(kony.flex.DP);
            flxAddAlertTypeButtonSep.add();
            var addAlertTypeButtons = new com.adminConsole.common.commonButtons({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "height": "80px",
                "id": "addAlertTypeButtons",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "20dp",
                "masterType": constants.MASTER_TYPE_DEFAULT,
                "isModalContainer": false,
                "right": 20,
                "skin": "slFbox",
                "top": "0dp",
                "overrides": {
                    "btnCancel": {
                        "left": "0dp",
                        "right": "viz.val_cleared",
                        "zIndex": 3
                    },
                    "btnNext": {
                        "isVisible": false
                    },
                    "btnSave": {
                        "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmAlertsManagement.AddGroup_UC\")",
                        "width": "137px"
                    },
                    "commonButtons": {
                        "left": "20dp",
                        "right": 20,
                        "width": "viz.val_cleared"
                    }
                }
            }, {
                "overrides": {}
            }, {
                "overrides": {}
            });
            flxAddAlertTypeButtons.add(flxAddAlertTypeButtonSep, addAlertTypeButtons);
            flxAddAlertTypeContainer.add(flxAddAlertTypeHeading, flxAddAlertTypeRow01, flxAddAlertTypeRow02, flxAddAlertTypeRow03, flxAddAlertTypeRow04, flxAddAlertBottom, flxAddAlertTypeButtons);
            flxAddAlertTypeScreen.add(flxAddAlertTypeContainer);
            var flxAlertTypesContextualMenu = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                "clipBounds": true,
                "id": "flxAlertTypesContextualMenu",
                "isVisible": false,
                "layoutType": kony.flex.FREE_FORM,
                "isModalContainer": false,
                "right": "15dp",
                "skin": "slFbox",
                "top": "50dp",
                "width": "130dp",
                "zIndex": 1
            }, {}, {});
            flxAlertTypesContextualMenu.setDefaultUnit(kony.flex.DP);
            var contextualMenuAlertTypeDetail = new com.adminConsole.common.contextualMenu1({
                "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                "id": "contextualMenuAlertTypeDetail",
                "isVisible": true,
                "layoutType": kony.flex.FLOW_VERTICAL,
                "left": "0px",
                "masterType": constants.MASTER_TYPE_DEFAULT,
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0px",
                "width": "100%",
                "overrides": {
                    "btnLink1": {
                        "isVisible": false
                    },
                    "btnLink2": {
                        "isVisible": false
                    },
                    "flxDownArrowImage": {
                        "isVisible": false
                    },
                    "flxOption1": {
                        "isVisible": true
                    },
                    "flxOption3": {
                        "isVisible": true
                    },
                    "flxOption4": {
                        "isVisible": false
                    },
                    "flxOptionsSeperator": {
                        "isVisible": false
                    },
                    "imgDownArrow": {
                        "src": "downarrow_2x.png"
                    },
                    "imgOption1": {
                        "src": "imagedrag.png"
                    },
                    "imgOption2": {
                        "src": "edit2x.png"
                    },
                    "imgOption3": {
                        "src": "imagedrag.png"
                    },
                    "imgOption4": {
                        "src": "deactive_2x.png"
                    },
                    "imgUpArrow": {
                        "src": "uparrow_2x.png"
                    },
                    "lblHeader": {
                        "isVisible": false
                    },
                    "lblIconOption3": {
                        "text": ""
                    },
                    "lblOption1": {
                        "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmAlertsManagement.Edit\")"
                    },
                    "lblOption2": {
                        "i18n_text": "kony.i18n.getLocalizedString(\"i18n.roles.Deactivate\")"
                    },
                    "lblOption3": {
                        "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmAlertsManagement.Reassign\")"
                    }
                }
            }, {
                "overrides": {}
            }, {
                "overrides": {}
            });
            flxAlertTypesContextualMenu.add(contextualMenuAlertTypeDetail);
            flxAlertTypes.add(flxAlertTypeDetailsScreen, flxAddAlertTypeScreen, flxAlertTypesContextualMenu);
            var flxSubAlerts = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                "clipBounds": true,
                "id": "flxSubAlerts",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "isModalContainer": false,
                "right": "0dp",
                "skin": "slFbox",
                "top": "0dp",
                "zIndex": 1
            }, {}, {});
            flxSubAlerts.setDefaultUnit(kony.flex.DP);
            var flxViewSubAlert = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                "clipBounds": true,
                "id": "flxViewSubAlert",
                "isVisible": true,
                "layoutType": kony.flex.FLOW_VERTICAL,
                "left": "0dp",
                "isModalContainer": false,
                "right": "0dp",
                "skin": "slFbox",
                "top": "0dp"
            }, {}, {});
            flxViewSubAlert.setDefaultUnit(kony.flex.DP);
            var flxViewSubAlertContainer1 = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                "clipBounds": true,
                "id": "flxViewSubAlertContainer1",
                "isVisible": true,
                "layoutType": kony.flex.FLOW_VERTICAL,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "sknFlxBgFFFFFFBorE4E6ECsz1pxR3px",
                "top": "0dp",
                "width": "100%"
            }, {}, {});
            flxViewSubAlertContainer1.setDefaultUnit(kony.flex.DP);
            var flxSubAlertDetails = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                "clipBounds": true,
                "id": "flxSubAlertDetails",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0dp",
                "width": "100%",
                "zIndex": 1
            }, {}, {});
            flxSubAlertDetails.setDefaultUnit(kony.flex.DP);
            var flxAlertDetailsArrow = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "20dp",
                "id": "flxAlertDetailsArrow",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "20dp",
                "isModalContainer": false,
                "right": "20dp",
                "skin": "slFbox",
                "top": 15,
                "width": "20dp",
                "zIndex": 1
            }, {}, {});
            flxAlertDetailsArrow.setDefaultUnit(kony.flex.DP);
            var lblIconAlertArrow = new kony.ui.Label({
                "centerY": "50%",
                "id": "lblIconAlertArrow",
                "isVisible": true,
                "left": "0dp",
                "skin": "sknIcon6E7178Sz15px",
                "text": "",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxAlertDetailsArrow.add(lblIconAlertArrow);
            var flxAlertDetailsContainer = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                "clipBounds": true,
                "id": "flxAlertDetailsContainer",
                "isVisible": true,
                "layoutType": kony.flex.FLOW_VERTICAL,
                "left": "40dp",
                "isModalContainer": false,
                "right": "20dp",
                "skin": "slFbox",
                "top": "0dp"
            }, {}, {});
            flxAlertDetailsContainer.setDefaultUnit(kony.flex.DP);
            var flxSubAlertHeader = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                "bottom": "10dp",
                "clipBounds": true,
                "id": "flxSubAlertHeader",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0dp",
                "width": "100%"
            }, {}, {});
            flxSubAlertHeader.setDefaultUnit(kony.flex.DP);
            var flxViewDetailsAlertStatus = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                "clipBounds": true,
                "id": "flxViewDetailsAlertStatus",
                "isVisible": true,
                "layoutType": kony.flex.FLOW_HORIZONTAL,
                "reverseLayoutDirection": true,
                "isModalContainer": false,
                "right": "50dp",
                "skin": "sknServiceDetailsContent",
                "top": "17dp",
                "width": "7%",
                "zIndex": 1
            }, {}, {});
            flxViewDetailsAlertStatus.setDefaultUnit(kony.flex.DP);
            var lblViewDetailsAlertStatus = new kony.ui.Label({
                "id": "lblViewDetailsAlertStatus",
                "isVisible": true,
                "right": "0dp",
                "skin": "sknlblLato5bc06cBold14px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.secureimage.Active\")",
                "top": "0dp",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_TOP_CENTER,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var lblViewDetailsStatusIcon = new kony.ui.Label({
                "height": "13dp",
                "id": "lblViewDetailsStatusIcon",
                "isVisible": true,
                "right": "5dp",
                "skin": "sknIcon13pxGreen",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmAlertsManagement.lblViewAlertStatusKey\")",
                "top": "2dp",
                "width": "13dp",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxViewDetailsAlertStatus.add(lblViewDetailsAlertStatus, lblViewDetailsStatusIcon);
            var flxOptionsSubAlerts = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "30dp",
                "id": "flxOptionsSubAlerts",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "isModalContainer": false,
                "onClick": controller.AS_FlexContainer_b2cebc80febc479da9a2cb6ecfce902e,
                "right": "0px",
                "skin": "sknFlxBgFFFFFFRounded",
                "top": "10dp",
                "width": "30px",
                "zIndex": 2
            }, {}, {
                "hoverSkin": "sknflxffffffop100Border424242Radius100px"
            });
            flxOptionsSubAlerts.setDefaultUnit(kony.flex.DP);
            var lblIconSubAlertOptions = new kony.ui.Label({
                "centerX": "50%",
                "centerY": "50%",
                "id": "lblIconSubAlertOptions",
                "isVisible": true,
                "skin": "sknFontIconOptionMenu",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.userwidgetmodel.fontIconImgOptions\")",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxOptionsSubAlerts.add(lblIconSubAlertOptions);
            var flxGrpHeading = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                "clipBounds": true,
                "id": "flxGrpHeading",
                "isVisible": true,
                "layoutType": kony.flex.FLOW_HORIZONTAL,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0dp",
                "width": "100%"
            }, {}, {});
            flxGrpHeading.setDefaultUnit(kony.flex.DP);
            var lblSubAlertName = new kony.ui.Label({
                "height": "20px",
                "id": "lblSubAlertName",
                "isVisible": true,
                "left": "0px",
                "skin": "sknLatoRegular192B4518px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmAlertsManagement.AddressChange\")",
                "top": "15px",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_CENTER,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var btnExt = new kony.ui.Button({
                "id": "btnExt",
                "isVisible": true,
                "left": 5,
                "skin": "sknbtnffffffLatoRegular12PxA94240",
                "text": "other external",
                "top": "15px",
                "width": kony.flex.USE_PREFFERED_SIZE
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_CENTER,
                "displayText": true,
                "padding": [1, 0, 1, 0],
                "paddingInPixel": false
            }, {});
            flxGrpHeading.add(lblSubAlertName, btnExt);
            flxSubAlertHeader.add(flxViewDetailsAlertStatus, flxOptionsSubAlerts, flxGrpHeading);
            var flxAlertFields = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                "clipBounds": true,
                "id": "flxAlertFields",
                "isVisible": true,
                "layoutType": kony.flex.FLOW_VERTICAL,
                "left": "0px",
                "isModalContainer": false,
                "right": "0px",
                "skin": "slFbox",
                "top": "5px",
                "zIndex": 1
            }, {}, {});
            flxAlertFields.setDefaultUnit(kony.flex.DP);
            var flxAlertMessage = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "bottom": "20px",
                "clipBounds": true,
                "height": "35px",
                "id": "flxAlertMessage",
                "isVisible": false,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0px",
                "isModalContainer": false,
                "right": "0dp",
                "skin": "slFbox",
                "top": "0px",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {}, {});
            flxAlertMessage.setDefaultUnit(kony.flex.DP);
            var alertMessage = new com.adminConsole.alertMang.alertMessage({
                "height": "35px",
                "id": "alertMessage",
                "isVisible": true,
                "layoutType": kony.flex.FLOW_HORIZONTAL,
                "left": "0dp",
                "masterType": constants.MASTER_TYPE_DEFAULT,
                "isModalContainer": false,
                "skin": "sknYellowBorder",
                "top": "0dp",
                "width": "100%",
                "overrides": {
                    "alertMessage": {
                        "top": "0dp"
                    }
                }
            }, {
                "overrides": {}
            }, {
                "overrides": {}
            });
            flxAlertMessage.add(alertMessage);
            var flxAlertDetailRow01 = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                "centerX": "50%",
                "clipBounds": true,
                "id": "flxAlertDetailRow01",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0px",
                "isModalContainer": false,
                "right": "0px",
                "skin": "slFbox",
                "top": "0px",
                "width": "100%",
                "zIndex": 1
            }, {}, {});
            flxAlertDetailRow01.setDefaultUnit(kony.flex.DP);
            var flxAlertDetailCol11 = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                "clipBounds": true,
                "id": "flxAlertDetailCol11",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0%",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0px",
                "width": "33%",
                "zIndex": 1
            }, {}, {});
            flxAlertDetailCol11.setDefaultUnit(kony.flex.DP);
            var lblAlertCode = new kony.ui.Label({
                "id": "lblAlertCode",
                "isVisible": true,
                "left": "0dp",
                "skin": "sknlblLato696c7311px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmAlertsManagement.AlertCodeCaps\")",
                "top": "0dp",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var lblAlertCodeValue = new kony.ui.Label({
                "id": "lblAlertCodeValue",
                "isVisible": true,
                "left": "0px",
                "skin": "sknlbl485C75LatoSemiBold13px",
                "text": "AL001AL001AL001",
                "top": "25px",
                "width": "96%",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_TOP_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxAlertDetailCol11.add(lblAlertCode, lblAlertCodeValue);
            var flxAlertDetailCol12 = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                "clipBounds": true,
                "id": "flxAlertDetailCol12",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "33%",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0px",
                "width": "33%",
                "zIndex": 1
            }, {}, {});
            flxAlertDetailCol12.setDefaultUnit(kony.flex.DP);
            var lblAccountLevelAlert = new kony.ui.Label({
                "id": "lblAccountLevelAlert",
                "isVisible": true,
                "left": "0px",
                "skin": "sknlblLato696c7311px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmAlertsManagement.AccountLevelAlert_UC\")",
                "top": "0px",
                "width": "100%",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var lblAccountLevelAlertValue = new kony.ui.Label({
                "id": "lblAccountLevelAlertValue",
                "isVisible": true,
                "left": "0px",
                "skin": "sknlbl485C75LatoSemiBold13px",
                "text": "Yes",
                "top": "25px",
                "width": "96%",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_TOP_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxAlertDetailCol12.add(lblAccountLevelAlert, lblAccountLevelAlertValue);
            var flxAlertDetailCol13 = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                "clipBounds": true,
                "id": "flxAlertDetailCol13",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "66%",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0px",
                "width": "33%",
                "zIndex": 1
            }, {}, {});
            flxAlertDetailCol13.setDefaultUnit(kony.flex.DP);
            var lblAlertTypeHeader = new kony.ui.Label({
                "id": "lblAlertTypeHeader",
                "isVisible": true,
                "left": "0px",
                "skin": "sknlblLato696c7311px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmAlertsManagement.AlertType_UC\")",
                "top": "0px",
                "width": "100%",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var lblSubAlertTypeValue = new kony.ui.Label({
                "id": "lblSubAlertTypeValue",
                "isVisible": true,
                "left": "0px",
                "skin": "sknlbl485C75LatoSemiBold13px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmAlertsManagement.UserAlert\")",
                "top": "25px",
                "width": "96%",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_TOP_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxAlertDetailCol13.add(lblAlertTypeHeader, lblSubAlertTypeValue);
            flxAlertDetailRow01.add(flxAlertDetailCol11, flxAlertDetailCol12, flxAlertDetailCol13);
            var flxAlertDetailRow02 = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                "centerX": "50%",
                "clipBounds": true,
                "id": "flxAlertDetailRow02",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0px",
                "isModalContainer": false,
                "right": "0px",
                "skin": "slFbox",
                "top": "25px",
                "width": "100%",
                "zIndex": 1
            }, {}, {});
            flxAlertDetailRow02.setDefaultUnit(kony.flex.DP);
            var flxAlertDetailColDefSub = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                "clipBounds": true,
                "id": "flxAlertDetailColDefSub",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0%",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0px",
                "width": "30%",
                "zIndex": 1
            }, {}, {});
            flxAlertDetailColDefSub.setDefaultUnit(kony.flex.DP);
            var lblAlertAppsAuto = new kony.ui.Label({
                "id": "lblAlertAppsAuto",
                "isVisible": true,
                "left": "0px",
                "skin": "sknlblLato696c7311px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmAlertsManagement.AutoSubscribeNewUsers\")",
                "top": "0px",
                "width": "100%",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var lblAlertAutoSubscribe = new kony.ui.Label({
                "id": "lblAlertAutoSubscribe",
                "isVisible": true,
                "left": "0px",
                "skin": "sknlbl485C75LatoSemiBold13px",
                "text": "Yes",
                "top": "25px",
                "width": "96%",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_TOP_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxAlertDetailColDefSub.add(lblAlertAppsAuto, lblAlertAutoSubscribe);
            var flxAlertDetailCol21 = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                "clipBounds": true,
                "id": "flxAlertDetailCol21",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "33%",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0px",
                "width": "30%",
                "zIndex": 1
            }, {}, {});
            flxAlertDetailCol21.setDefaultUnit(kony.flex.DP);
            var lblAlertApps = new kony.ui.Label({
                "id": "lblAlertApps",
                "isVisible": true,
                "left": "0px",
                "skin": "sknlblLato696c7311px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmAlertsManagement.APPS\")",
                "top": "0px",
                "width": "100%",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var lblAlertAppsValue = new kony.ui.Label({
                "id": "lblAlertAppsValue",
                "isVisible": true,
                "left": "0px",
                "skin": "sknlbl485C75LatoSemiBold13px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmAlertsManagement.RetailBankingNative\")",
                "top": "25px",
                "width": "96%",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_TOP_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxAlertDetailCol21.add(lblAlertApps, lblAlertAppsValue);
            var flxAlertDetailCol22 = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                "clipBounds": true,
                "id": "flxAlertDetailCol22",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "66%",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0px",
                "width": "30%",
                "zIndex": 1
            }, {}, {});
            flxAlertDetailCol22.setDefaultUnit(kony.flex.DP);
            var lblAlertUserTypes = new kony.ui.Label({
                "id": "lblAlertUserTypes",
                "isVisible": true,
                "left": "0px",
                "skin": "sknlblLato696c7311px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmAlertsManagement.USER_TYPES\")",
                "top": "0px",
                "width": "100%",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var lblAlertUserTypesValues = new kony.ui.Label({
                "id": "lblAlertUserTypesValues",
                "isVisible": true,
                "left": "0px",
                "skin": "sknlbl485C75LatoSemiBold13px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmAlertsManagement.RetailCustomer\")",
                "top": "25px",
                "width": "96%",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_TOP_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxAlertDetailCol22.add(lblAlertUserTypes, lblAlertUserTypesValues);
            flxAlertDetailRow02.add(flxAlertDetailColDefSub, flxAlertDetailCol21, flxAlertDetailCol22);
            var flxAlertDetailRow03 = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                "centerX": "50%",
                "clipBounds": true,
                "id": "flxAlertDetailRow03",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0px",
                "isModalContainer": false,
                "right": "0px",
                "skin": "slFbox",
                "top": "25px",
                "width": "100%",
                "zIndex": 1
            }, {}, {});
            flxAlertDetailRow03.setDefaultUnit(kony.flex.DP);
            var flxAlertDetailCol23 = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                "clipBounds": true,
                "id": "flxAlertDetailCol23",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0%",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0px",
                "width": "33%",
                "zIndex": 1
            }, {}, {});
            flxAlertDetailCol23.setDefaultUnit(kony.flex.DP);
            var lblAlertRecipient = new kony.ui.Label({
                "id": "lblAlertRecipient",
                "isVisible": true,
                "left": "0px",
                "skin": "sknlblLato696c7311px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmAlertsManagement.Recipients_UC\")",
                "top": "0px",
                "width": "100%",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var lblAlertRecipientValues = new kony.ui.Label({
                "id": "lblAlertRecipientValues",
                "isVisible": true,
                "left": "0px",
                "skin": "sknlbl485C75LatoSemiBold13px",
                "text": "N/A",
                "top": "25px",
                "width": "96%",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_TOP_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxAlertDetailCol23.add(lblAlertRecipient, lblAlertRecipientValues);
            var flxAlertDetailCol31 = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                "clipBounds": true,
                "id": "flxAlertDetailCol31",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "33%",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0px",
                "width": "33%",
                "zIndex": 1
            }, {}, {});
            flxAlertDetailCol31.setDefaultUnit(kony.flex.DP);
            var lblAlertDetailHeading31 = new kony.ui.Label({
                "id": "lblAlertDetailHeading31",
                "isVisible": true,
                "left": "0px",
                "skin": "sknlblLato696c7311px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmAlertsManagement.DefaultFrequency_UC\")",
                "top": "0px",
                "width": "100%",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var lblAlertDetailValue31 = new kony.ui.Label({
                "id": "lblAlertDetailValue31",
                "isVisible": true,
                "left": "0px",
                "skin": "sknlbl485C75LatoSemiBold13px",
                "text": "Daily, 10AM",
                "top": "25px",
                "width": "96%",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_TOP_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxAlertDetailCol31.add(lblAlertDetailHeading31, lblAlertDetailValue31);
            var flxAlertDetailCol32 = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                "clipBounds": true,
                "id": "flxAlertDetailCol32",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "66%",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0px",
                "width": "33%",
                "zIndex": 1
            }, {}, {});
            flxAlertDetailCol32.setDefaultUnit(kony.flex.DP);
            var lblAlertDetailHeading32 = new kony.ui.Label({
                "id": "lblAlertDetailHeading32",
                "isVisible": true,
                "left": "0px",
                "skin": "sknlblLato696c7311px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmAlertsManagement.DefaultChannels_UC\")",
                "top": "0px",
                "width": "100%",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var lblAlertDetailValue32 = new kony.ui.Label({
                "id": "lblAlertDetailValue32",
                "isVisible": true,
                "left": "0px",
                "skin": "sknlbl485C75LatoSemiBold13px",
                "text": "SMS/Text, Email",
                "top": "25px",
                "width": "96%",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_TOP_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxAlertDetailCol32.add(lblAlertDetailHeading32, lblAlertDetailValue32);
            flxAlertDetailRow03.add(flxAlertDetailCol23, flxAlertDetailCol31, flxAlertDetailCol32);
            var flxAlertDetailRow04 = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                "centerX": "50%",
                "clipBounds": true,
                "id": "flxAlertDetailRow04",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0px",
                "isModalContainer": false,
                "right": "0px",
                "skin": "slFbox",
                "top": "25px",
                "width": "100%",
                "zIndex": 1
            }, {}, {});
            flxAlertDetailRow04.setDefaultUnit(kony.flex.DP);
            var flxAlertDetailCol33 = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                "clipBounds": true,
                "id": "flxAlertDetailCol33",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0%",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0px",
                "width": "33%",
                "zIndex": 1
            }, {}, {});
            flxAlertDetailCol33.setDefaultUnit(kony.flex.DP);
            var lblAlertDetailHeading33 = new kony.ui.Label({
                "id": "lblAlertDetailHeading33",
                "isVisible": true,
                "left": "0px",
                "skin": "sknlblLato696c7311px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmAdManagement.AttributesCAPS\")",
                "top": "0px",
                "width": "100%",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var lblAlertDetailValue33 = new kony.ui.Label({
                "id": "lblAlertDetailValue33",
                "isVisible": true,
                "left": "0px",
                "skin": "sknlbl485C75LatoSemiBold13px",
                "text": "SMS/Text, Email",
                "top": "25px",
                "width": "96%",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_TOP_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxAlertDetailCol33.add(lblAlertDetailHeading33, lblAlertDetailValue33);
            var flxAlertDetailCol41 = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                "clipBounds": true,
                "id": "flxAlertDetailCol41",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "33%",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0px",
                "width": "33%",
                "zIndex": 1
            }, {}, {});
            flxAlertDetailCol41.setDefaultUnit(kony.flex.DP);
            var lblAlertDetailHeading41 = new kony.ui.Label({
                "id": "lblAlertDetailHeading41",
                "isVisible": true,
                "left": "0px",
                "skin": "sknlblLato696c7311px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmAlertsManagement.DefaultChannels_UC\")",
                "top": "0px",
                "width": "100%",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var lblAlertDetailValue41 = new kony.ui.Label({
                "id": "lblAlertDetailValue41",
                "isVisible": true,
                "left": "0px",
                "skin": "sknlbl485C75LatoSemiBold13px",
                "text": "Daily, 10AM",
                "top": "25px",
                "width": "96%",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_TOP_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxAlertDetailCol41.add(lblAlertDetailHeading41, lblAlertDetailValue41);
            flxAlertDetailRow04.add(flxAlertDetailCol33, flxAlertDetailCol41);
            flxAlertFields.add(flxAlertMessage, flxAlertDetailRow01, flxAlertDetailRow02, flxAlertDetailRow03, flxAlertDetailRow04);
            var flxAlertDesc = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                "clipBounds": true,
                "id": "flxAlertDesc",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0px",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "20px",
                "width": "100%",
                "zIndex": 1
            }, {}, {});
            flxAlertDesc.setDefaultUnit(kony.flex.DP);
            var flxAlertDescHeader = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "20px",
                "id": "flxAlertDescHeader",
                "isVisible": true,
                "layoutType": kony.flex.FLOW_HORIZONTAL,
                "left": "0px",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0px",
                "width": "55%",
                "zIndex": 1
            }, {}, {});
            flxAlertDescHeader.setDefaultUnit(kony.flex.DP);
            var lblAlertDescHeader = new kony.ui.Label({
                "height": "100%",
                "id": "lblAlertDescHeader",
                "isVisible": true,
                "left": "0px",
                "skin": "sknlblLato696c7311px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmAlertsManagement.AlertDescription_UC\")",
                "top": "0px",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var lblAlertDescLanguage = new kony.ui.Label({
                "height": "100%",
                "id": "lblAlertDescLanguage",
                "isVisible": true,
                "left": "0px",
                "skin": "skn696c73LatoRegItalic11px",
                "text": "(ENGLISH-US)",
                "top": "0px",
                "width": "100%",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxAlertDescHeader.add(lblAlertDescHeader, lblAlertDescLanguage);
            var flxAlertDescValue = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                "bottom": "20dp",
                "clipBounds": true,
                "id": "flxAlertDescValue",
                "isVisible": true,
                "layoutType": kony.flex.FLOW_VERTICAL,
                "left": "0px",
                "isModalContainer": false,
                "skin": "sknServiceDetailsContent",
                "top": "25px",
                "width": "96%",
                "zIndex": 1
            }, {}, {});
            flxAlertDescValue.setDefaultUnit(kony.flex.DP);
            var lblAlertDescValue = new kony.ui.Label({
                "id": "lblAlertDescValue",
                "isVisible": true,
                "left": "0px",
                "skin": "sknlbl485C75LatoSemiBold13px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmAlertsManagement.SecurityAlertsDesc\")",
                "top": "0px",
                "width": "100%",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_TOP_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var lblAlertShowLangLink = new kony.ui.Label({
                "id": "lblAlertShowLangLink",
                "isVisible": true,
                "left": "0px",
                "skin": "sknLblLatoReg117eb013px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmAlertsManagement.ViewAllLanguages\")",
                "top": "5dp",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_TOP_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {
                "hoverSkin": "sknLbl117eb013pxHov"
            });
            flxAlertDescValue.add(lblAlertDescValue, lblAlertShowLangLink);
            flxAlertDesc.add(flxAlertDescHeader, flxAlertDescValue);
            flxAlertDetailsContainer.add(flxSubAlertHeader, flxAlertFields, flxAlertDesc);
            flxSubAlertDetails.add(flxAlertDetailsArrow, flxAlertDetailsContainer);
            flxViewSubAlertContainer1.add(flxSubAlertDetails);
            var flxViewSubAlertContainer2 = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                "bottom": "20dp",
                "clipBounds": true,
                "id": "flxViewSubAlertContainer2",
                "isVisible": true,
                "layoutType": kony.flex.FLOW_VERTICAL,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "sknFlxBgFFFFFFBorE4E6ECsz1pxR3px",
                "top": "15dp",
                "width": "100%"
            }, {}, {});
            flxViewSubAlertContainer2.setDefaultUnit(kony.flex.DP);
            var flxAlertContentTemplate = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                "clipBounds": true,
                "id": "flxAlertContentTemplate",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "20dp",
                "isModalContainer": false,
                "right": "20dp",
                "skin": "slFbox",
                "top": "0dp",
                "width": "96%"
            }, {}, {});
            flxAlertContentTemplate.setDefaultUnit(kony.flex.DP);
            var lblTemplateHeader = new kony.ui.Label({
                "id": "lblTemplateHeader",
                "isVisible": true,
                "left": "0dp",
                "skin": "sknLatoRegular192B4518px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmAlertsManagement.AlertContentTemplate\")",
                "top": "20dp",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var flxAddTemplateButton = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "25dp",
                "id": "flxAddTemplateButton",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "isModalContainer": false,
                "right": "160dp",
                "skin": "sknFlxBGF7F7FAFC485C75R20pxBor485C75",
                "top": "20dp",
                "width": "55px",
                "zIndex": 1
            }, {}, {
                "hoverSkin": "sknflxBorder485c75Radius20PxPointer"
            });
            flxAddTemplateButton.setDefaultUnit(kony.flex.DP);
            var lblAddTemplateButton = new kony.ui.Label({
                "centerX": "48%",
                "centerY": "47%",
                "id": "lblAddTemplateButton",
                "isVisible": true,
                "skin": "sknlblLatoBold35475f14px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmPermissionsController.Add\")",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxAddTemplateButton.add(lblAddTemplateButton);
            var flxOptionEdit = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "25dp",
                "id": "flxOptionEdit",
                "isVisible": true,
                "layoutType": kony.flex.FLOW_HORIZONTAL,
                "isModalContainer": false,
                "right": "90dp",
                "skin": "sknFlxBGF7F7FAFC485C75R20pxBor485C75",
                "top": "20dp",
                "width": "55px",
                "zIndex": 1
            }, {}, {
                "hoverSkin": "sknflxBorder485c75Radius20PxPointer"
            });
            flxOptionEdit.setDefaultUnit(kony.flex.DP);
            var fontIconOptionEdit = new kony.ui.Label({
                "centerX": "48%",
                "centerY": "47%",
                "id": "fontIconOptionEdit",
                "isVisible": true,
                "skin": "sknlblLatoBold35475f14px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.ConfigurationBundles.edit\")",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxOptionEdit.add(fontIconOptionEdit);
            var flxEyeicon = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "25dp",
                "id": "flxEyeicon",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "isModalContainer": false,
                "right": "0dp",
                "skin": "sknFlxBGF7F7FAFC485C75R20pxBor485C75",
                "top": "20dp",
                "width": "75dp",
                "zIndex": 2
            }, {}, {
                "hoverSkin": "sknflxBorder485c75Radius20PxPointer"
            });
            flxEyeicon.setDefaultUnit(kony.flex.DP);
            var lblEyeicon = new kony.ui.Label({
                "centerX": "48%",
                "centerY": "47%",
                "height": 25,
                "id": "lblEyeicon",
                "isVisible": true,
                "skin": "sknlblLatoBold35475f14px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmAlertsManagement.lblEditAlertPreview\")",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 2
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxEyeicon.add(lblEyeicon);
            var flxViewTemplateListBox = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "150dp",
                "id": "flxViewTemplateListBox",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": 0,
                "isModalContainer": false,
                "skin": "sknflxf5f6f8Op100BorderE1E5Ed",
                "top": "60dp",
                "width": "100%"
            }, {}, {});
            flxViewTemplateListBox.setDefaultUnit(kony.flex.DP);
            var lblContentBy = new kony.ui.Label({
                "id": "lblContentBy",
                "isVisible": true,
                "left": "20dp",
                "skin": "sknLatoRegular192B4518px",
                "text": "View Content By",
                "top": "15dp",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var flxSubAlertResponseState = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                "clipBounds": true,
                "id": "flxSubAlertResponseState",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "20dp",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "55dp",
                "width": "300dp",
                "zIndex": 1
            }, {}, {});
            flxSubAlertResponseState.setDefaultUnit(kony.flex.DP);
            var lblSubAlertResponseState = new kony.ui.Label({
                "id": "lblSubAlertResponseState",
                "isVisible": true,
                "left": "0dp",
                "skin": "sknlblLatoRegular484b5213px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmAlertsManagement.ResponseState\")",
                "top": "0dp",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var lstBoxSubAlertResponseState = new kony.ui.ListBox({
                "focusSkin": "defListBoxFocus",
                "height": "40dp",
                "id": "lstBoxSubAlertResponseState",
                "isVisible": true,
                "left": "0dp",
                "masterData": [
                    ["lb1", "Success"],
                    ["lb2", "Failure"]
                ],
                "skin": "sknlbxBgffffffBorderc1c9ceRadius3Px",
                "top": "25dp",
                "width": "300dp",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [3, 0, 0, 0],
                "paddingInPixel": false
            }, {
                "multiSelect": false
            });
            flxSubAlertResponseState.add(lblSubAlertResponseState, lstBoxSubAlertResponseState);
            var flxSubAlertLanguages = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                "clipBounds": true,
                "id": "flxSubAlertLanguages",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "340dp",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "55dp",
                "width": "300dp",
                "zIndex": 1
            }, {}, {});
            flxSubAlertLanguages.setDefaultUnit(kony.flex.DP);
            var lblSubAlertLanguages = new kony.ui.Label({
                "id": "lblSubAlertLanguages",
                "isVisible": true,
                "left": "0dp",
                "skin": "sknlblLatoRegular484b5213px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmAlertsManagement.Languages\")",
                "top": "0dp",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var lstBoxSubAlertLanguages = new kony.ui.ListBox({
                "focusSkin": "defListBoxFocus",
                "height": "40dp",
                "id": "lstBoxSubAlertLanguages",
                "isVisible": true,
                "left": "0dp",
                "masterData": [
                    ["lb1", "English-UK"],
                    ["lb2", "German"],
                    ["lb3", "Spanish"]
                ],
                "skin": "sknlbxBgffffffBorderc1c9ceRadius3Px",
                "top": "25dp",
                "width": "300dp",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [3, 0, 0, 0],
                "paddingInPixel": false
            }, {
                "multiSelect": false
            });
            flxSubAlertLanguages.add(lblSubAlertLanguages, lstBoxSubAlertLanguages);
            flxViewTemplateListBox.add(lblContentBy, flxSubAlertResponseState, flxSubAlertLanguages);
            flxAlertContentTemplate.add(lblTemplateHeader, flxAddTemplateButton, flxOptionEdit, flxEyeicon, flxViewTemplateListBox);
            var flxViewTemplate = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                "clipBounds": true,
                "id": "flxViewTemplate",
                "isVisible": false,
                "layoutType": kony.flex.FLOW_VERTICAL,
                "left": "20dp",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "15dp",
                "width": "97.50%"
            }, {}, {});
            flxViewTemplate.setDefaultUnit(kony.flex.DP);
            var flxViewSMSTemplate = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                "bottom": "10dp",
                "clipBounds": true,
                "id": "flxViewSMSTemplate",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": 0,
                "isModalContainer": false,
                "right": "0dp",
                "skin": "slFbox",
                "top": "10dp",
                "zIndex": 1
            }, {}, {});
            flxViewSMSTemplate.setDefaultUnit(kony.flex.DP);
            var ViewTemplateSMS = new com.adminConsole.alerts.ViewTemplate({
                "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                "id": "ViewTemplateSMS",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "masterType": constants.MASTER_TYPE_DEFAULT,
                "isModalContainer": false,
                "right": "0dp",
                "skin": "sknflxffffffop100",
                "top": "0dp",
                "overrides": {
                    "ViewTemplate": {
                        "bottom": "viz.val_cleared",
                        "top": "0dp"
                    },
                    "flxTitle": {
                        "isVisible": false
                    },
                    "lblChannelMsg": {
                        "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmAlertsManagement.YouHaveSuccessfully\")"
                    },
                    "lblChannelName": {
                        "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmAlertsManagement.SMSText\")"
                    },
                    "lblTitle": {
                        "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmAlertsManagement.TitleColon\")"
                    }
                }
            }, {
                "overrides": {}
            }, {
                "overrides": {}
            });
            flxViewSMSTemplate.add(ViewTemplateSMS);
            var flxViewNotificationCenter = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                "bottom": "10dp",
                "clipBounds": true,
                "id": "flxViewNotificationCenter",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": 0,
                "isModalContainer": false,
                "right": "0dp",
                "skin": "slFbox",
                "top": "10dp",
                "zIndex": 1
            }, {}, {});
            flxViewNotificationCenter.setDefaultUnit(kony.flex.DP);
            var ViewTemplateCenter = new com.adminConsole.alerts.ViewTemplate({
                "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                "id": "ViewTemplateCenter",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "masterType": constants.MASTER_TYPE_DEFAULT,
                "isModalContainer": false,
                "right": "0dp",
                "skin": "sknflxffffffop100",
                "top": "0dp",
                "overrides": {
                    "ViewTemplate": {
                        "bottom": "viz.val_cleared",
                        "top": "0dp"
                    },
                    "flxTitle": {
                        "isVisible": true
                    },
                    "lblChannelMsg": {
                        "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmAlertsManagement.YouHaveSuccessfully\")"
                    },
                    "lblChannelName": {
                        "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmAlertsManagement.NotificationCenter\")"
                    },
                    "lblTitle": {
                        "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmAlertsManagement.TitleColon\")"
                    },
                    "lblTitleValue": {
                        "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmAlertsManagement.PasswordChange\")"
                    }
                }
            }, {
                "overrides": {}
            }, {
                "overrides": {}
            });
            flxViewNotificationCenter.add(ViewTemplateCenter);
            var flxViewPushNotification = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                "bottom": "10dp",
                "clipBounds": true,
                "id": "flxViewPushNotification",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "isModalContainer": false,
                "right": "0dp",
                "skin": "slFbox",
                "top": "10dp",
                "zIndex": 1
            }, {}, {});
            flxViewPushNotification.setDefaultUnit(kony.flex.DP);
            var ViewTemplatePush = new com.adminConsole.alerts.ViewTemplate({
                "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                "id": "ViewTemplatePush",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "masterType": constants.MASTER_TYPE_DEFAULT,
                "isModalContainer": false,
                "right": "0dp",
                "skin": "sknflxffffffop100",
                "top": "0dp",
                "overrides": {
                    "ViewTemplate": {
                        "bottom": "viz.val_cleared"
                    },
                    "lblChannelMsg": {
                        "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmAlertsManagement.YouHaveSuccessfully\")"
                    },
                    "lblChannelName": {
                        "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmAlertsManagement.PushNotification\")"
                    },
                    "lblTitle": {
                        "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmAlertsManagement.TitleColon\")"
                    },
                    "lblTitleValue": {
                        "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmAlertsManagement.PasswordChange\")"
                    }
                }
            }, {
                "overrides": {}
            }, {
                "overrides": {}
            });
            flxViewPushNotification.add(ViewTemplatePush);
            var flxViewEmailTemplate = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                "bottom": "10dp",
                "clipBounds": true,
                "id": "flxViewEmailTemplate",
                "isVisible": false,
                "layoutType": kony.flex.FREE_FORM,
                "left": 0,
                "isModalContainer": false,
                "right": "0dp",
                "skin": "slFbox",
                "top": "10dp",
                "zIndex": 1
            }, {}, {});
            flxViewEmailTemplate.setDefaultUnit(kony.flex.DP);
            var lblChannelName = new kony.ui.Label({
                "id": "lblChannelName",
                "isVisible": true,
                "left": "0dp",
                "skin": "sknLbl192b45LatoBold13px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmAlertsManagementController._Email\")",
                "top": "0dp",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var flxEmailTemplate = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                "clipBounds": true,
                "id": "flxEmailTemplate",
                "isVisible": true,
                "layoutType": kony.flex.FLOW_VERTICAL,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "15dp",
                "width": "100%"
            }, {}, {});
            flxEmailTemplate.setDefaultUnit(kony.flex.DP);
            var flxEmailTitle = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                "clipBounds": true,
                "id": "flxEmailTitle",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "isModalContainer": false,
                "right": "0dp",
                "skin": "slFbox",
                "top": "10dp"
            }, {}, {});
            flxEmailTitle.setDefaultUnit(kony.flex.DP);
            var lblEmailTitle = new kony.ui.Label({
                "id": "lblEmailTitle",
                "isVisible": true,
                "left": "0dp",
                "skin": "sknlblLatoReg485c7513px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmAlertsManagement.SubjectColon\")",
                "top": "0dp",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var lblEmailTitleValue = new kony.ui.Label({
                "id": "lblEmailTitleValue",
                "isVisible": true,
                "left": "50dp",
                "right": "30dp",
                "skin": "sknlblLatoReg485c7513px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmAlertsManagement.UsernameChanged\")",
                "top": "0dp",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxEmailTitle.add(lblEmailTitle, lblEmailTitleValue);
            var rtxEmailViewer = new kony.ui.Browser({
                "detectTelNumber": true,
                "enableNativeCommunication": false,
                "enableZoom": false,
                "id": "rtxEmailViewer",
                "isVisible": true,
                "left": "-8dp",
                "setAsContent": false,
                "requestURLConfig": {
                    "URL": "richtextViewer.html",
                    "requestMethod": constants.BROWSER_REQUEST_METHOD_GET
                },
                "top": "10px",
                "width": "100%",
                "zIndex": 1
            }, {}, {});
            flxEmailTemplate.add(flxEmailTitle, rtxEmailViewer);
            flxViewEmailTemplate.add(lblChannelName, flxEmailTemplate);
            flxViewTemplate.add(flxViewSMSTemplate, flxViewNotificationCenter, flxViewPushNotification, flxViewEmailTemplate);
            var flxAddTemplate = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                "bottom": "20dp",
                "clipBounds": true,
                "id": "flxAddTemplate",
                "isVisible": false,
                "layoutType": kony.flex.FLOW_VERTICAL,
                "left": "20dp",
                "isModalContainer": false,
                "right": "20dp",
                "skin": "slFbox",
                "top": "15dp",
                "width": "97.50%"
            }, {}, {});
            flxAddTemplate.setDefaultUnit(kony.flex.DP);
            var flxAddSMSTemplate = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                "clipBounds": true,
                "id": "flxAddSMSTemplate",
                "isVisible": false,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "isModalContainer": false,
                "right": "20dp",
                "skin": "slFbox",
                "top": "0dp",
                "zIndex": 1
            }, {}, {});
            flxAddSMSTemplate.setDefaultUnit(kony.flex.DP);
            var flxSMSHeader = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "20px",
                "id": "flxSMSHeader",
                "isVisible": true,
                "layoutType": kony.flex.FLOW_HORIZONTAL,
                "left": "0px",
                "isModalContainer": false,
                "right": "0dp",
                "skin": "sknFlxPointer",
                "top": "0px",
                "zIndex": 1
            }, {}, {});
            flxSMSHeader.setDefaultUnit(kony.flex.DP);
            var lblSMSHeader = new kony.ui.Label({
                "id": "lblSMSHeader",
                "isVisible": true,
                "left": "0dp",
                "skin": "sknLbl485C75LatoRegular13Px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmAlertsManagement.SMSText\")",
                "top": "0dp",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var lblSMSVariableReference = new kony.ui.Label({
                "height": "20px",
                "id": "lblSMSVariableReference",
                "isVisible": true,
                "left": "30px",
                "skin": "sknAlertBlue",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmAlertsManagement.lblEditAlertVariableReference\")",
                "top": "0px",
                "width": "200px",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxSMSHeader.add(lblSMSHeader, lblSMSVariableReference);
            var txtSMSMsg = new kony.ui.TextArea2({
                "autoCapitalize": constants.TEXTAREA_AUTO_CAPITALIZE_NONE,
                "focusSkin": "skntxtAreaLato9ca9ba12Px",
                "height": "100dp",
                "id": "txtSMSMsg",
                "isVisible": true,
                "keyBoardStyle": constants.TEXTAREA_KEY_BOARD_STYLE_DEFAULT,
                "left": "0dp",
                "maxTextLength": 300,
                "numberOfVisibleLines": 3,
                "i18n_placeholder": "kony.i18n.getLocalizedString(\"i18n.frmAlertsManagement.addContentHere\")",
                "right": "0dp",
                "skin": "skntxtAreaLato35475f14Px",
                "textInputMode": constants.TEXTAREA_INPUT_MODE_ANY,
                "top": "30dp",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_TOP_LEFT,
                "padding": [1, 1, 1, 1],
                "paddingInPixel": false
            }, {
                "autoCorrect": false
            });
            var lblSMSMsgSize = new kony.ui.Label({
                "id": "lblSMSMsgSize",
                "isVisible": false,
                "right": "0dp",
                "skin": "sknlbl485c7514px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmCSR.lblCharCount2\")",
                "top": "0px",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var flxNoSMSMsgError = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "25px",
                "id": "flxNoSMSMsgError",
                "isVisible": false,
                "layoutType": kony.flex.FLOW_HORIZONTAL,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "130dp",
                "width": "100%",
                "zIndex": 1
            }, {}, {});
            flxNoSMSMsgError.setDefaultUnit(kony.flex.DP);
            var lblNoSMSMsgErrorIcon = new kony.ui.Label({
                "height": "15dp",
                "id": "lblNoSMSMsgErrorIcon",
                "isVisible": true,
                "left": "0dp",
                "skin": "sknErrorIcon",
                "text": "",
                "top": "0dp",
                "width": "15dp",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var lblNoSMSMsgError = new kony.ui.Label({
                "id": "lblNoSMSMsgError",
                "isVisible": true,
                "left": "0dp",
                "skin": "sknlblError",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmAlertsManagement.SMSCannotBeEmpty\")",
                "top": "0dp",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxNoSMSMsgError.add(lblNoSMSMsgErrorIcon, lblNoSMSMsgError);
            flxAddSMSTemplate.add(flxSMSHeader, txtSMSMsg, lblSMSMsgSize, flxNoSMSMsgError);
            var flxAddPushNotification = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                "clipBounds": true,
                "id": "flxAddPushNotification",
                "isVisible": false,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "isModalContainer": false,
                "right": "20dp",
                "skin": "slFbox",
                "top": "15dp",
                "zIndex": 1
            }, {}, {});
            flxAddPushNotification.setDefaultUnit(kony.flex.DP);
            var flxPushNotificationHeader = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "20px",
                "id": "flxPushNotificationHeader",
                "isVisible": true,
                "layoutType": kony.flex.FLOW_HORIZONTAL,
                "left": "0px",
                "isModalContainer": false,
                "right": "0dp",
                "skin": "sknFlxPointer",
                "top": "0px",
                "zIndex": 1
            }, {}, {});
            flxPushNotificationHeader.setDefaultUnit(kony.flex.DP);
            var lblPushNotificationHeader = new kony.ui.Label({
                "id": "lblPushNotificationHeader",
                "isVisible": true,
                "left": "0dp",
                "skin": "sknLbl485C75LatoRegular13Px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmAlertsManagement.PushNotification\")",
                "top": "0dp",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var lblPushNotiVariableReference = new kony.ui.Label({
                "height": "20px",
                "id": "lblPushNotiVariableReference",
                "isVisible": true,
                "left": "30px",
                "skin": "sknAlertBlue",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmAlertsManagement.lblEditAlertVariableReference\")",
                "top": "0px",
                "width": "200px",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxPushNotificationHeader.add(lblPushNotificationHeader, lblPushNotiVariableReference);
            var txtPushNotification = new kony.ui.TextArea2({
                "autoCapitalize": constants.TEXTAREA_AUTO_CAPITALIZE_NONE,
                "focusSkin": "skntxtAreaLato9ca9ba12Px",
                "height": "100dp",
                "id": "txtPushNotification",
                "isVisible": true,
                "keyBoardStyle": constants.TEXTAREA_KEY_BOARD_STYLE_DEFAULT,
                "left": "0dp",
                "maxTextLength": 300,
                "numberOfVisibleLines": 3,
                "i18n_placeholder": "kony.i18n.getLocalizedString(\"i18n.frmAlertsManagement.addContentHere\")",
                "right": "0dp",
                "skin": "skntxtAreaLato35475f14Px",
                "textInputMode": constants.TEXTAREA_INPUT_MODE_ANY,
                "top": "65dp",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_TOP_LEFT,
                "padding": [1, 1, 1, 1],
                "paddingInPixel": false
            }, {
                "autoCorrect": false
            });
            var lblPushNotificationSize = new kony.ui.Label({
                "id": "lblPushNotificationSize",
                "isVisible": false,
                "right": "0dp",
                "skin": "sknlbl485c7514px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmCSR.lblCharCount2\")",
                "top": "0px",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var flxNoPushNotification = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "25px",
                "id": "flxNoPushNotification",
                "isVisible": false,
                "layoutType": kony.flex.FLOW_HORIZONTAL,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "170dp",
                "width": "100%",
                "zIndex": 1
            }, {}, {});
            flxNoPushNotification.setDefaultUnit(kony.flex.DP);
            var lblNoPushNotificationErrorIcon = new kony.ui.Label({
                "height": "15dp",
                "id": "lblNoPushNotificationErrorIcon",
                "isVisible": true,
                "left": "0dp",
                "skin": "sknErrorIcon",
                "text": "",
                "top": "0dp",
                "width": "15dp",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var lblNoPushNotificationError = new kony.ui.Label({
                "id": "lblNoPushNotificationError",
                "isVisible": true,
                "left": "0dp",
                "skin": "sknlblError",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmAlertsManagement.PushCannotBeEmpty\")",
                "top": "0dp",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxNoPushNotification.add(lblNoPushNotificationErrorIcon, lblNoPushNotificationError);
            var flxPushNotificationTitle = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "35dp",
                "id": "flxPushNotificationTitle",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "isModalContainer": false,
                "right": "0dp",
                "skin": "sknFlxBorE1E5ED1pxKA",
                "top": "30dp",
                "zIndex": 1
            }, {}, {});
            flxPushNotificationTitle.setDefaultUnit(kony.flex.DP);
            var lblPushNotificationTitle = new kony.ui.Label({
                "centerY": "50%",
                "id": "lblPushNotificationTitle",
                "isVisible": true,
                "left": "5dp",
                "skin": "sknlbl485C75LatoSemiBold13px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmAlertsManagement.TitleColon\")",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var tbxPushNotificationTitle = new kony.ui.TextBox2({
                "autoCapitalize": constants.TEXTBOX_AUTO_CAPITALIZE_NONE,
                "centerY": "50%",
                "height": "30px",
                "id": "tbxPushNotificationTitle",
                "isVisible": true,
                "keyBoardStyle": constants.TEXTBOX_KEY_BOARD_STYLE_DEFAULT,
                "left": "40px",
                "maxTextLength": 100,
                "onTouchStart": controller.AS_TextField_ee75a43a99344e72b47654fe9d064f6e,
                "right": "35px",
                "secureTextEntry": false,
                "skin": "sknSearchtbxConfigurationBundlesNormal",
                "textInputMode": constants.TEXTBOX_INPUT_MODE_ANY,
                "zIndex": 1
            }, {
                "containerHeightMode": constants.TEXTBOX_FONT_METRICS_DRIVEN_HEIGHT,
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [1, 0, 0, 0],
                "paddingInPixel": false
            }, {
                "autoCorrect": false
            });
            flxPushNotificationTitle.add(lblPushNotificationTitle, tbxPushNotificationTitle);
            flxAddPushNotification.add(flxPushNotificationHeader, txtPushNotification, lblPushNotificationSize, flxNoPushNotification, flxPushNotificationTitle);
            var flxAddNotificationCenter = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                "clipBounds": true,
                "id": "flxAddNotificationCenter",
                "isVisible": false,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "isModalContainer": false,
                "right": "20dp",
                "skin": "slFbox",
                "top": "15dp",
                "zIndex": 1
            }, {}, {});
            flxAddNotificationCenter.setDefaultUnit(kony.flex.DP);
            var flxNotiCenterHeader = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "20px",
                "id": "flxNotiCenterHeader",
                "isVisible": true,
                "layoutType": kony.flex.FLOW_HORIZONTAL,
                "left": "0px",
                "isModalContainer": false,
                "right": "0dp",
                "skin": "sknFlxPointer",
                "top": "0px",
                "zIndex": 1
            }, {}, {});
            flxNotiCenterHeader.setDefaultUnit(kony.flex.DP);
            var lblNotiCenterHeader = new kony.ui.Label({
                "id": "lblNotiCenterHeader",
                "isVisible": true,
                "left": "0dp",
                "skin": "sknLbl485C75LatoRegular13Px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmAlertsManagement.NotificationCenter\")",
                "top": "0dp",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var lblNotiCenterVariableReference = new kony.ui.Label({
                "height": "20px",
                "id": "lblNotiCenterVariableReference",
                "isVisible": true,
                "left": "30px",
                "skin": "sknAlertBlue",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmAlertsManagement.lblEditAlertVariableReference\")",
                "top": "0px",
                "width": "200px",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxNotiCenterHeader.add(lblNotiCenterHeader, lblNotiCenterVariableReference);
            var txtNotiCenterMsg = new kony.ui.TextArea2({
                "autoCapitalize": constants.TEXTAREA_AUTO_CAPITALIZE_NONE,
                "focusSkin": "skntxtAreaLato9ca9ba12Px",
                "height": "100dp",
                "id": "txtNotiCenterMsg",
                "isVisible": true,
                "keyBoardStyle": constants.TEXTAREA_KEY_BOARD_STYLE_DEFAULT,
                "left": "0dp",
                "maxTextLength": 300,
                "numberOfVisibleLines": 3,
                "i18n_placeholder": "kony.i18n.getLocalizedString(\"i18n.frmAlertsManagement.addContentHere\")",
                "right": "0dp",
                "skin": "skntxtAreaLato35475f14Px",
                "textInputMode": constants.TEXTAREA_INPUT_MODE_ANY,
                "top": "65dp",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_TOP_LEFT,
                "padding": [1, 1, 1, 1],
                "paddingInPixel": false
            }, {
                "autoCorrect": false
            });
            var lblNotiCenterMsgSize = new kony.ui.Label({
                "id": "lblNotiCenterMsgSize",
                "isVisible": false,
                "right": "0dp",
                "skin": "sknlbl485c7514px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmCSR.lblCharCount2\")",
                "top": "0px",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var flxNoNotiCenterMsgError = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "25px",
                "id": "flxNoNotiCenterMsgError",
                "isVisible": false,
                "layoutType": kony.flex.FLOW_HORIZONTAL,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "170dp",
                "width": "100%",
                "zIndex": 1
            }, {}, {});
            flxNoNotiCenterMsgError.setDefaultUnit(kony.flex.DP);
            var lblNoNotiCenterErrorIcon = new kony.ui.Label({
                "height": "15dp",
                "id": "lblNoNotiCenterErrorIcon",
                "isVisible": true,
                "left": "0dp",
                "skin": "sknErrorIcon",
                "text": "",
                "top": "0dp",
                "width": "15dp",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var lblNoNotiCenterError = new kony.ui.Label({
                "id": "lblNoNotiCenterError",
                "isVisible": true,
                "left": "0dp",
                "skin": "sknlblError",
                "text": "Title cannot be empty",
                "top": "0dp",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxNoNotiCenterMsgError.add(lblNoNotiCenterErrorIcon, lblNoNotiCenterError);
            var flxNotiCenterTitle = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "35dp",
                "id": "flxNotiCenterTitle",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "isModalContainer": false,
                "right": "0dp",
                "skin": "sknFlxBorE1E5ED1pxKA",
                "top": "30dp",
                "zIndex": 1
            }, {}, {});
            flxNotiCenterTitle.setDefaultUnit(kony.flex.DP);
            var lblNotiCenterTitle = new kony.ui.Label({
                "centerY": "50%",
                "id": "lblNotiCenterTitle",
                "isVisible": true,
                "left": "5dp",
                "skin": "sknlbl485C75LatoSemiBold13px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmAlertsManagement.TitleColon\")",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var tbxNotiCenterTitle = new kony.ui.TextBox2({
                "autoCapitalize": constants.TEXTBOX_AUTO_CAPITALIZE_NONE,
                "centerY": "50%",
                "height": "30px",
                "id": "tbxNotiCenterTitle",
                "isVisible": true,
                "keyBoardStyle": constants.TEXTBOX_KEY_BOARD_STYLE_DEFAULT,
                "left": "40px",
                "maxTextLength": 100,
                "onTouchStart": controller.AS_TextField_ee75a43a99344e72b47654fe9d064f6e,
                "right": "35px",
                "secureTextEntry": false,
                "skin": "sknSearchtbxConfigurationBundlesNormal",
                "textInputMode": constants.TEXTBOX_INPUT_MODE_ANY,
                "zIndex": 1
            }, {
                "containerHeightMode": constants.TEXTBOX_FONT_METRICS_DRIVEN_HEIGHT,
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [1, 0, 0, 0],
                "paddingInPixel": false
            }, {
                "autoCorrect": false
            });
            flxNotiCenterTitle.add(lblNotiCenterTitle, tbxNotiCenterTitle);
            flxAddNotificationCenter.add(flxNotiCenterHeader, txtNotiCenterMsg, lblNotiCenterMsgSize, flxNoNotiCenterMsgError, flxNotiCenterTitle);
            var flxAddEmailTemplate = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                "bottom": "20dp",
                "clipBounds": true,
                "id": "flxAddEmailTemplate",
                "isVisible": false,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "isModalContainer": false,
                "right": "20dp",
                "skin": "slFbox",
                "top": "15dp",
                "zIndex": 1
            }, {}, {});
            flxAddEmailTemplate.setDefaultUnit(kony.flex.DP);
            var flxEmailHeader = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "20px",
                "id": "flxEmailHeader",
                "isVisible": true,
                "layoutType": kony.flex.FLOW_HORIZONTAL,
                "left": "0px",
                "isModalContainer": false,
                "right": "0dp",
                "skin": "sknFlxPointer",
                "top": "0px",
                "zIndex": 1
            }, {}, {});
            flxEmailHeader.setDefaultUnit(kony.flex.DP);
            var lblEmailHeader = new kony.ui.Label({
                "id": "lblEmailHeader",
                "isVisible": true,
                "left": "0dp",
                "skin": "sknLbl485C75LatoRegular13Px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmAlertsManagementController._Email\")",
                "top": "0dp",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var lblEmailVariableReference = new kony.ui.Label({
                "height": "20px",
                "id": "lblEmailVariableReference",
                "isVisible": true,
                "left": "20px",
                "skin": "sknAlertBlue",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmAlertsManagement.lblEditAlertVariableReference\")",
                "top": "0px",
                "width": "200px",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxEmailHeader.add(lblEmailHeader, lblEmailVariableReference);
            var flxEmailSubject = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "35dp",
                "id": "flxEmailSubject",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "isModalContainer": false,
                "right": "0dp",
                "skin": "sknFlxBorE1E5ED1pxKA",
                "top": "30dp",
                "zIndex": 1
            }, {}, {});
            flxEmailSubject.setDefaultUnit(kony.flex.DP);
            var lblEmailSubject = new kony.ui.Label({
                "centerY": "50%",
                "id": "lblEmailSubject",
                "isVisible": true,
                "left": "5dp",
                "skin": "sknlbl485C75LatoSemiBold13px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmAlertsManagement.SubjectColon\")",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var tbxEmailSubject = new kony.ui.TextBox2({
                "autoCapitalize": constants.TEXTBOX_AUTO_CAPITALIZE_NONE,
                "centerY": "50%",
                "height": "30px",
                "id": "tbxEmailSubject",
                "isVisible": true,
                "keyBoardStyle": constants.TEXTBOX_KEY_BOARD_STYLE_DEFAULT,
                "left": "55px",
                "maxTextLength": 100,
                "onTouchStart": controller.AS_TextField_ee75a43a99344e72b47654fe9d064f6e,
                "right": "35px",
                "secureTextEntry": false,
                "skin": "sknSearchtbxConfigurationBundlesNormal",
                "textInputMode": constants.TEXTBOX_INPUT_MODE_ANY,
                "zIndex": 1
            }, {
                "containerHeightMode": constants.TEXTBOX_FONT_METRICS_DRIVEN_HEIGHT,
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [1, 0, 0, 0],
                "paddingInPixel": false
            }, {
                "autoCorrect": false
            });
            flxEmailSubject.add(lblEmailSubject, tbxEmailSubject);
            var flxEmailRtxEditor = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                "bottom": "20dp",
                "clipBounds": true,
                "id": "flxEmailRtxEditor",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0px",
                "isModalContainer": false,
                "right": "0px",
                "skin": "sknflxFFFFFFBr1pxe1e5edR3px3sided",
                "top": "65dp",
                "zIndex": 1
            }, {}, {});
            flxEmailRtxEditor.setDefaultUnit(kony.flex.DP);
            var rtxEmailTemplate = new kony.ui.Browser({
                "detectTelNumber": true,
                "enableZoom": false,
                "id": "rtxEmailTemplate",
                "isVisible": true,
                "left": "0px",
                "setAsContent": false,
                "requestURLConfig": {
                    "URL": "richtext.html",
                    "requestMethod": constants.BROWSER_REQUEST_METHOD_GET
                },
                "right": "0px",
                "top": "0px",
                "zIndex": 1
            }, {}, {});
            flxEmailRtxEditor.add(rtxEmailTemplate);
            var lblSubjectSize = new kony.ui.Label({
                "id": "lblSubjectSize",
                "isVisible": false,
                "right": "0dp",
                "skin": "sknlbl485c7514px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmCSR.lblCharCount2\")",
                "top": "0px",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var flxNoEmailSubject = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "25px",
                "id": "flxNoEmailSubject",
                "isVisible": false,
                "layoutType": kony.flex.FLOW_HORIZONTAL,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "310dp",
                "width": "100%",
                "zIndex": 1
            }, {}, {});
            flxNoEmailSubject.setDefaultUnit(kony.flex.DP);
            var lblNoSubjectErrorIcon = new kony.ui.Label({
                "height": "15dp",
                "id": "lblNoSubjectErrorIcon",
                "isVisible": true,
                "left": "0dp",
                "skin": "sknErrorIcon",
                "text": "",
                "top": "0dp",
                "width": "15dp",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var lblNoSubjectError = new kony.ui.Label({
                "id": "lblNoSubjectError",
                "isVisible": true,
                "left": "0dp",
                "skin": "sknlblError",
                "text": "Email subject cannot be empty",
                "top": "0dp",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxNoEmailSubject.add(lblNoSubjectErrorIcon, lblNoSubjectError);
            flxAddEmailTemplate.add(flxEmailHeader, flxEmailSubject, flxEmailRtxEditor, lblSubjectSize, flxNoEmailSubject);
            flxAddTemplate.add(flxAddSMSTemplate, flxAddPushNotification, flxAddNotificationCenter, flxAddEmailTemplate);
            var flxSaveTemplateButtons = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "bottom": "0dp",
                "clipBounds": true,
                "height": "80px",
                "id": "flxSaveTemplateButtons",
                "isVisible": false,
                "layoutType": kony.flex.FREE_FORM,
                "isModalContainer": false,
                "skin": "sknFlxBorE1E5ED1pxKA",
                "width": "100%",
                "zIndex": 1
            }, {}, {});
            flxSaveTemplateButtons.setDefaultUnit(kony.flex.DP);
            var btnTemplateCancel = new kony.ui.Button({
                "centerY": "50%",
                "focusSkin": "sknBtnLatoRegulara5abc413pxKA",
                "height": "40px",
                "id": "btnTemplateCancel",
                "isVisible": true,
                "onClick": controller.AS_Button_fe48adad632c43c5b97a05155a450331,
                "right": "155px",
                "skin": "sknbtnf7f7faLatoRegular4f555dBorder1px8b96a5",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmCustomers.CANCEL\")",
                "top": "0%",
                "width": "120px",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_CENTER,
                "displayText": true,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {
                "hoverSkin": "sknBtnLatoRegular8b96a513pxKAhover"
            });
            var btnTemplateSave = new kony.ui.Button({
                "centerY": "50%",
                "focusSkin": "sknBtn003E75LatoRegular13pxFFFFFFRad20px",
                "height": "40dp",
                "id": "btnTemplateSave",
                "isVisible": true,
                "onClick": controller.AS_Button_c973a89c85624425bb033af39fa13bd2,
                "right": "20px",
                "skin": "sknBtn003E75LatoRegular13pxFFFFFFRad20px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.permission.SAVE\")",
                "top": "0%",
                "width": "120dp",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_CENTER,
                "displayText": true,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {
                "hoverSkin": "sknBtn005198LatoRegular13pxFFFFFFRad20px"
            });
            flxSaveTemplateButtons.add(btnTemplateCancel, btnTemplateSave);
            var flxNoContentTemplate = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "bottom": "15dp",
                "clipBounds": true,
                "height": "160dp",
                "id": "flxNoContentTemplate",
                "isVisible": false,
                "layoutType": kony.flex.FREE_FORM,
                "left": "20dp",
                "isModalContainer": false,
                "skin": "sknflxf5f6f8Op100BorderE1E5Ed",
                "top": "10dp",
                "width": "95%",
                "zIndex": 1
            }, {}, {});
            flxNoContentTemplate.setDefaultUnit(kony.flex.DP);
            var rtxNoTemplate = new kony.ui.RichText({
                "bottom": "50px",
                "centerX": "50%",
                "centerY": "50%",
                "height": "15dp",
                "id": "rtxNoTemplate",
                "isVisible": true,
                "left": "0",
                "skin": "sknRtxLato84939e12Px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmAlertsManagement.NoAssociatedSubAlerts\")",
                "width": "80%",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_CENTER,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxNoContentTemplate.add(rtxNoTemplate);
            flxViewSubAlertContainer2.add(flxAlertContentTemplate, flxViewTemplate, flxAddTemplate, flxSaveTemplateButtons, flxNoContentTemplate);
            flxViewSubAlert.add(flxViewSubAlertContainer1, flxViewSubAlertContainer2);
            var flxSubAlertContextualMenu = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                "clipBounds": true,
                "id": "flxSubAlertContextualMenu",
                "isVisible": false,
                "layoutType": kony.flex.FLOW_VERTICAL,
                "isModalContainer": false,
                "right": "5dp",
                "skin": "slFbox",
                "top": "100dp",
                "width": "150dp",
                "zIndex": 1
            }, {}, {});
            flxSubAlertContextualMenu.setDefaultUnit(kony.flex.DP);
            var flxSubAlertUpArrow = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "12px",
                "id": "flxSubAlertUpArrow",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0px",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0px",
                "width": "100%",
                "zIndex": 2
            }, {}, {});
            flxSubAlertUpArrow.setDefaultUnit(kony.flex.DP);
            var imgSubAlertUpArrow = new kony.ui.Image2({
                "height": "12dp",
                "id": "imgSubAlertUpArrow",
                "isVisible": true,
                "right": "20dp",
                "skin": "slImage",
                "src": "uparrow_2x.png",
                "top": "0dp",
                "width": "15dp",
                "zIndex": 1
            }, {
                "imageScaleMode": constants.IMAGE_SCALE_MODE_FIT_TO_DIMENSIONS,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxSubAlertUpArrow.add(imgSubAlertUpArrow);
            var flxSubAlertContextContainer = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                "clipBounds": true,
                "id": "flxSubAlertContextContainer",
                "isVisible": true,
                "layoutType": kony.flex.FLOW_VERTICAL,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "sknflxffffffop100dbdbe6Radius3px",
                "top": "0dp",
                "width": "100%"
            }, {}, {});
            flxSubAlertContextContainer.setDefaultUnit(kony.flex.DP);
            var flxDeactivateAlert = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "35dp",
                "id": "flxDeactivateAlert",
                "isVisible": true,
                "layoutType": kony.flex.FLOW_HORIZONTAL,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "CopyslFbox0i3963a83703242",
                "top": "0dp",
                "width": "100%",
                "zIndex": 1
            }, {}, {
                "hoverSkin": "sknContextualMenuEntryHover2"
            });
            flxDeactivateAlert.setDefaultUnit(kony.flex.DP);
            var imgDeactivateAlert = new kony.ui.Image2({
                "centerY": "50%",
                "height": "15dp",
                "id": "imgDeactivateAlert",
                "isVisible": false,
                "left": "15dp",
                "skin": "slImage",
                "src": "edit2x.png",
                "width": "15dp",
                "zIndex": 1
            }, {
                "imageScaleMode": constants.IMAGE_SCALE_MODE_MAINTAIN_ASPECT_RATIO,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var lblDeactivateAlertIcon = new kony.ui.Label({
                "centerY": "50%",
                "id": "lblDeactivateAlertIcon",
                "isVisible": true,
                "left": "15px",
                "skin": "sknFontIconOptionMenuRow",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmFAQ.fonticonEdit\")",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var lblDeactivateAlert = new kony.ui.Label({
                "centerY": "50%",
                "id": "lblDeactivateAlert",
                "isVisible": true,
                "left": "15dp",
                "skin": "sknlblLatoBold35475f14px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.roles.Edit\")",
                "top": "4dp",
                "width": "70px",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxDeactivateAlert.add(imgDeactivateAlert, lblDeactivateAlertIcon, lblDeactivateAlert);
            var flxEditSubAlertView = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "35dp",
                "id": "flxEditSubAlertView",
                "isVisible": true,
                "layoutType": kony.flex.FLOW_HORIZONTAL,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "CopyslFbox0i3963a83703242",
                "top": "0dp",
                "width": "100%",
                "zIndex": 1
            }, {}, {
                "hoverSkin": "sknContextualMenuEntryHover2"
            });
            flxEditSubAlertView.setDefaultUnit(kony.flex.DP);
            var fonticonEditView = new kony.ui.Label({
                "centerY": "50%",
                "id": "fonticonEditView",
                "isVisible": true,
                "left": "15px",
                "skin": "sknFontIconOptionMenuRow",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmFAQ.fonticonEdit\")",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var imgOptionView = new kony.ui.Image2({
                "centerY": "50%",
                "height": "15dp",
                "id": "imgOptionView",
                "isVisible": false,
                "left": "15dp",
                "skin": "slImage",
                "src": "edit2x.png",
                "width": "15dp",
                "zIndex": 1
            }, {
                "imageScaleMode": constants.IMAGE_SCALE_MODE_MAINTAIN_ASPECT_RATIO,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var lblOptionView = new kony.ui.Label({
                "centerY": "50%",
                "id": "lblOptionView",
                "isVisible": true,
                "left": "15dp",
                "skin": "sknlblLatoBold35475f14px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.roles.Edit\")",
                "top": "4dp",
                "width": "70px",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxEditSubAlertView.add(fonticonEditView, imgOptionView, lblOptionView);
            flxSubAlertContextContainer.add(flxDeactivateAlert, flxEditSubAlertView);
            var flxSubAlertDownArrow = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "12px",
                "id": "flxSubAlertDownArrow",
                "isVisible": false,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0px",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "-1px",
                "width": "100%",
                "zIndex": 2
            }, {}, {});
            flxSubAlertDownArrow.setDefaultUnit(kony.flex.DP);
            var imgSubAlertDownArrow = new kony.ui.Image2({
                "height": "12dp",
                "id": "imgSubAlertDownArrow",
                "isVisible": true,
                "right": "20dp",
                "skin": "slImage",
                "src": "downarrow_2x.png",
                "top": "0dp",
                "width": "15dp",
                "zIndex": 1
            }, {
                "imageScaleMode": constants.IMAGE_SCALE_MODE_FIT_TO_DIMENSIONS,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxSubAlertDownArrow.add(imgSubAlertDownArrow);
            flxSubAlertContextualMenu.add(flxSubAlertUpArrow, flxSubAlertContextContainer, flxSubAlertDownArrow);
            var flxVariableReferenceContainer = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "180px",
                "id": "flxVariableReferenceContainer",
                "isVisible": false,
                "layoutType": kony.flex.FLOW_HORIZONTAL,
                "left": "233dp",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "316dp",
                "width": "280px",
                "zIndex": 2
            }, {}, {});
            flxVariableReferenceContainer.setDefaultUnit(kony.flex.DP);
            var imgVariableReferencePionter = new kony.ui.Image2({
                "centerY": "50%",
                "height": "20px",
                "id": "imgVariableReferencePionter",
                "isVisible": true,
                "left": "0px",
                "skin": "slImage",
                "src": "arrowvariableref2x.png",
                "width": "20px",
                "zIndex": 2
            }, {
                "imageScaleMode": constants.IMAGE_SCALE_MODE_MAINTAIN_ASPECT_RATIO,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var flxVariableReference = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "180px",
                "id": "flxVariableReference",
                "isVisible": true,
                "layoutType": kony.flex.FLOW_VERTICAL,
                "left": "-5px",
                "isModalContainer": false,
                "skin": "CopyslFbox0fd3f91d547a249",
                "top": "0px",
                "width": "240px",
                "zIndex": 1
            }, {}, {});
            flxVariableReference.setDefaultUnit(kony.flex.DP);
            var flxVariableReferenceHeader = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "centerX": "50%",
                "clipBounds": true,
                "height": "40px",
                "id": "flxVariableReferenceHeader",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0px",
                "width": "100%",
                "zIndex": 1
            }, {}, {});
            flxVariableReferenceHeader.setDefaultUnit(kony.flex.DP);
            var lblVariableReferenceHeader = new kony.ui.Label({
                "centerY": "50.00%",
                "id": "lblVariableReferenceHeader",
                "isVisible": true,
                "left": "15px",
                "right": "10px",
                "skin": "sknlblLatoBold12px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmAlertsManagement.lblVariableReferenceHeader\")",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxVariableReferenceHeader.add(lblVariableReferenceHeader);
            var flxVariableReferenceSeparator = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "centerX": "49%",
                "clipBounds": true,
                "height": "1px",
                "id": "flxVariableReferenceSeparator",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "isModalContainer": false,
                "skin": "CopyslFbox0a4e08ac4307f4a",
                "width": "85%",
                "zIndex": 1
            }, {}, {});
            flxVariableReferenceSeparator.setDefaultUnit(kony.flex.DP);
            flxVariableReferenceSeparator.add();
            var flxVariableReferenceContent = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "centerX": "50%",
                "clipBounds": true,
                "height": "125px",
                "id": "flxVariableReferenceContent",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "5px",
                "width": "100%",
                "zIndex": 1
            }, {}, {});
            flxVariableReferenceContent.setDefaultUnit(kony.flex.DP);
            var flxVariableReferenceInner = new kony.ui.FlexScrollContainer({
                "allowHorizontalBounce": false,
                "allowVerticalBounce": true,
                "bounces": true,
                "clipBounds": true,
                "enableScrolling": true,
                "height": "100%",
                "horizontalScrollIndicator": true,
                "id": "flxVariableReferenceInner",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "15px",
                "pagingEnabled": false,
                "right": "10px",
                "scrollDirection": kony.flex.SCROLL_VERTICAL,
                "skin": "slFSbox",
                "top": "0dp",
                "verticalScrollIndicator": true,
                "zIndex": 1
            }, {}, {});
            flxVariableReferenceInner.setDefaultUnit(kony.flex.DP);
            var segVariableReference = new kony.ui.SegmentedUI2({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "data": [{
                    "lbVariableReference": "Variable reference"
                }, {
                    "lbVariableReference": "Variable reference"
                }, {
                    "lbVariableReference": "Variable reference"
                }],
                "groupCells": false,
                "height": "100%",
                "id": "segVariableReference",
                "isVisible": true,
                "left": "0dp",
                "needPageIndicator": true,
                "pageOffDotImage": "pageoffdot.png",
                "pageOnDotImage": "pageondot.png",
                "retainSelection": false,
                "rowFocusSkin": "seg2Focus",
                "rowSkin": "Copyseg0f43ae421b7954e",
                "rowTemplate": "segVariableReference",
                "sectionHeaderSkin": "sliPhoneSegmentHeader",
                "selectionBehavior": constants.SEGUI_DEFAULT_BEHAVIOR,
                "separatorRequired": false,
                "showScrollbars": false,
                "top": "0dp",
                "viewType": constants.SEGUI_VIEW_TYPE_TABLEVIEW,
                "widgetDataMap": {
                    "lbVariableReference": "lbVariableReference",
                    "segVariableReference": "segVariableReference"
                },
                "width": "100%",
                "zIndex": 1
            }, {
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxVariableReferenceInner.add(segVariableReference);
            flxVariableReferenceContent.add(flxVariableReferenceInner);
            flxVariableReference.add(flxVariableReferenceHeader, flxVariableReferenceSeparator, flxVariableReferenceContent);
            flxVariableReferenceContainer.add(imgVariableReferencePionter, flxVariableReference);
            var flxAddAlertScreen = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                "clipBounds": true,
                "id": "flxAddAlertScreen",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "sknFlxBgFFFFFFBorE4E6ECsz1pxR3px",
                "top": "0dp",
                "width": "100%",
                "zIndex": 1
            }, {}, {});
            flxAddAlertScreen.setDefaultUnit(kony.flex.DP);
            var flxAddAlertContainer = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                "clipBounds": true,
                "id": "flxAddAlertContainer",
                "isVisible": true,
                "layoutType": kony.flex.FLOW_VERTICAL,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0dp",
                "width": "100%",
                "zIndex": 1
            }, {}, {});
            flxAddAlertContainer.setDefaultUnit(kony.flex.DP);
            var flxAddAlertHeading = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "60dp",
                "id": "flxAddAlertHeading",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0dp",
                "width": "100%",
                "zIndex": 1
            }, {}, {});
            flxAddAlertHeading.setDefaultUnit(kony.flex.DP);
            var lblAddAlertName = new kony.ui.Label({
                "centerY": "50%",
                "id": "lblAddAlertName",
                "isVisible": true,
                "left": "20px",
                "skin": "sknlblLatoRegular485c7518Px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmAlertsManagement.lblViewAlertName\")",
                "top": "0px",
                "width": "100%",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var flxEditAlrtHeaderSeperator = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "bottom": "0dp",
                "clipBounds": true,
                "height": "1dp",
                "id": "flxEditAlrtHeaderSeperator",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "20dp",
                "isModalContainer": false,
                "right": "20dp",
                "skin": "sknSeparatorCsr",
                "zIndex": 1
            }, {}, {});
            flxEditAlrtHeaderSeperator.setDefaultUnit(kony.flex.DP);
            flxEditAlrtHeaderSeperator.add();
            var flxAddSubAlertAlertStatus = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "100%",
                "id": "flxAddSubAlertAlertStatus",
                "isVisible": true,
                "layoutType": kony.flex.FLOW_HORIZONTAL,
                "reverseLayoutDirection": true,
                "isModalContainer": false,
                "right": "20dp",
                "skin": "slFbox",
                "top": "0dp",
                "width": "300px",
                "zIndex": 2
            }, {}, {});
            flxAddSubAlertAlertStatus.setDefaultUnit(kony.flex.DP);
            var flxAddAlertIconStatus = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "centerY": "50%",
                "clipBounds": true,
                "height": "25px",
                "id": "flxAddAlertIconStatus",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "isModalContainer": false,
                "right": "0dp",
                "skin": "slFbox",
                "width": "38px",
                "zIndex": 1
            }, {}, {});
            flxAddAlertIconStatus.setDefaultUnit(kony.flex.DP);
            var addAlertStatusSwitch = new com.adminConsole.common.customSwitch({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "height": "25dp",
                "id": "addAlertStatusSwitch",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0px",
                "masterType": constants.MASTER_TYPE_DEFAULT,
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0px",
                "width": "100%",
                "overrides": {
                    "customSwitch": {
                        "height": "25dp",
                        "isVisible": true,
                        "left": "0px",
                        "top": "0px",
                        "width": "100%"
                    },
                    "switchToggle": {
                        "isVisible": true
                    }
                }
            }, {
                "overrides": {}
            }, {
                "overrides": {}
            });
            flxAddAlertIconStatus.add(addAlertStatusSwitch);
            var lblAddAlertActive = new kony.ui.Label({
                "centerY": "50%",
                "height": "20px",
                "id": "lblAddAlertActive",
                "isVisible": true,
                "right": "15dp",
                "skin": "sknlblLatoBold35475f14px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.permission.Active\")",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxAddSubAlertAlertStatus.add(flxAddAlertIconStatus, lblAddAlertActive);
            flxAddAlertHeading.add(lblAddAlertName, flxEditAlrtHeaderSeperator, flxAddSubAlertAlertStatus);
            var flxAddAlertRow1 = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": false,
                "height": "90dp",
                "id": "flxAddAlertRow1",
                "isVisible": true,
                "layoutType": kony.flex.FLOW_HORIZONTAL,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "30dp",
                "width": "100%",
                "zIndex": 10
            }, {}, {});
            flxAddAlertRow1.setDefaultUnit(kony.flex.DP);
            var flxAddAlertCol11 = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": false,
                "height": "95dp",
                "id": "flxAddAlertCol11",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "20dp",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0dp",
                "width": "30%",
                "zIndex": 1
            }, {}, {});
            flxAddAlertCol11.setDefaultUnit(kony.flex.DP);
            var lblAddAlertCodeHeading = new kony.ui.Label({
                "id": "lblAddAlertCodeHeading",
                "isVisible": true,
                "left": "0dp",
                "skin": "sknLbl485C75LatoRegular13Px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmAlertsManagement.AlertCode\")",
                "top": "0dp",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var lstBoxAddAlertCode = new kony.ui.ListBox({
                "focusSkin": "defListBoxFocus",
                "height": "40dp",
                "id": "lstBoxAddAlertCode",
                "isVisible": false,
                "left": "0dp",
                "masterData": [
                    ["lb1", "Placeholder One"],
                    ["lb2", "Placeholder Two"],
                    ["lb3", "Placeholder Three"]
                ],
                "skin": "sknlbxBgffffffBorderc1c9ceRadius3Px",
                "top": "25dp",
                "width": "100%",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [3, 0, 0, 0],
                "paddingInPixel": false
            }, {
                "multiSelect": false
            });
            var flxAddAlertCodeError = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                "clipBounds": true,
                "id": "flxAddAlertCodeError",
                "isVisible": false,
                "layoutType": kony.flex.FLOW_HORIZONTAL,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "68dp",
                "width": "100%",
                "zIndex": 1
            }, {}, {});
            flxAddAlertCodeError.setDefaultUnit(kony.flex.DP);
            var lblIconErrAlertCode = new kony.ui.Label({
                "id": "lblIconErrAlertCode",
                "isVisible": true,
                "left": "0dp",
                "skin": "sknErrorIcon",
                "text": "",
                "top": "0dp",
                "width": "15dp",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var lblErrMsgAlertCode = new kony.ui.Label({
                "id": "lblErrMsgAlertCode",
                "isVisible": true,
                "left": "0dp",
                "skin": "sknlblError",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmAlertsManagement.PleaseSelectCode\")",
                "top": "0dp",
                "width": "180dp",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxAddAlertCodeError.add(lblIconErrAlertCode, lblErrMsgAlertCode);
            var flxColumnServType = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                "clipBounds": false,
                "id": "flxColumnServType",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0dp",
                "width": "100%",
                "zIndex": 40
            }, {}, {});
            flxColumnServType.setDefaultUnit(kony.flex.DP);
            var flxDropDownServType = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": false,
                "height": "41px",
                "id": "flxDropDownServType",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "isModalContainer": false,
                "right": "0dp",
                "skin": "sknflxffffffoptemplateop3px",
                "top": "24dp",
                "zIndex": 1
            }, {}, {
                "hoverSkin": "sknFlxSegRowHover11abeb"
            });
            flxDropDownServType.setDefaultUnit(kony.flex.DP);
            var lblSelectedRowsServType = new kony.ui.Label({
                "centerY": "49%",
                "id": "lblSelectedRowsServType",
                "isVisible": true,
                "left": "12px",
                "right": "45dp",
                "skin": "sknlbl0h2ff0d6b13f947AD",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.userwidgetmodel.lblCol13\")",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var flxSearchDownImgServType = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "centerY": "50%",
                "clipBounds": true,
                "height": "12px",
                "id": "flxSearchDownImgServType",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "isModalContainer": false,
                "right": "15px",
                "skin": "slFbox",
                "width": "15px",
                "zIndex": 1
            }, {}, {});
            flxSearchDownImgServType.setDefaultUnit(kony.flex.DP);
            var CopyfontIconSearchDown0f468ee73067a42 = new kony.ui.Label({
                "centerY": "50%",
                "id": "CopyfontIconSearchDown0f468ee73067a42",
                "isVisible": true,
                "left": "0dp",
                "skin": "sknIcon696C73sz13px",
                "text": "",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 2
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxSearchDownImgServType.add(CopyfontIconSearchDown0f468ee73067a42);
            flxDropDownServType.add(lblSelectedRowsServType, flxSearchDownImgServType);
            var flxDropDownDetailServType = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "167px",
                "id": "flxDropDownDetailServType",
                "isVisible": false,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "isModalContainer": false,
                "right": "0dp",
                "skin": "skntxtbxNormald7d9e0",
                "top": "67px",
                "width": "500dp",
                "zIndex": 400
            }, {}, {
                "hoverSkin": "skntxtbxhover11abeb"
            });
            flxDropDownDetailServType.setDefaultUnit(kony.flex.DP);
            var AdvancedSearchDropDownServType = new com.adminConsole.Groups.AdvancedSearchDropDown({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "height": "100%",
                "id": "AdvancedSearchDropDownServType",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "masterType": constants.MASTER_TYPE_DEFAULT,
                "isModalContainer": false,
                "skin": "sknNormalDefault",
                "top": "0dp",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 100,
                "overrides": {
                    "AdvancedSearchDropDown": {
                        "height": "100%"
                    },
                    "flxDropDown": {
                        "height": "167px"
                    },
                    "flxPopupBody": {
                        "top": "40dp"
                    },
                    "flxSearch": {
                        "isVisible": true
                    },
                    "richTexNoResult": {
                        "centerX": "50%"
                    },
                    "sgmentData": {
                        "height": "120dp"
                    }
                }
            }, {
                "overrides": {}
            }, {
                "overrides": {}
            });
            flxDropDownDetailServType.add(AdvancedSearchDropDownServType);
            flxColumnServType.add(flxDropDownServType, flxDropDownDetailServType);
            flxAddAlertCol11.add(lblAddAlertCodeHeading, lstBoxAddAlertCode, flxAddAlertCodeError, flxColumnServType);
            var flxAddAlertCol12 = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": false,
                "height": "95dp",
                "id": "flxAddAlertCol12",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "20dp",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0dp",
                "width": "30%",
                "zIndex": 1
            }, {}, {});
            flxAddAlertCol12.setDefaultUnit(kony.flex.DP);
            var dataEntrySubAlertName = new com.adminConsole.common.dataEntry({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "height": "85dp",
                "id": "dataEntrySubAlertName",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "masterType": constants.MASTER_TYPE_DEFAULT,
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0dp",
                "width": "100%",
                "zIndex": 1,
                "overrides": {
                    "lblData": {
                        "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmAlertsManagement.AlertName\")"
                    },
                    "lblErrorMsg": {
                        "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmAlertsManagement.Alert_name_cannot_be_empty\")",
                        "right": "viz.val_cleared",
                        "width": "90%"
                    },
                    "lblTextCounter": {
                        "isVisible": false
                    },
                    "tbxData": {
                        "i18n_placeholder": "kony.i18n.getLocalizedString(\"i18n.frmAlertsManagement.AlertName\")",
                        "maxTextLength": 50
                    }
                }
            }, {
                "overrides": {}
            }, {
                "overrides": {}
            });
            var flxSubAlertNameAvailable = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "bottom": "2px",
                "clipBounds": true,
                "height": "15dp",
                "id": "flxSubAlertNameAvailable",
                "isVisible": false,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "70dp",
                "width": "100%",
                "zIndex": 1
            }, {}, {});
            flxSubAlertNameAvailable.setDefaultUnit(kony.flex.DP);
            var lblSubAlertNameAvailable = new kony.ui.Label({
                "height": "15dp",
                "id": "lblSubAlertNameAvailable",
                "isVisible": true,
                "left": "0dp",
                "skin": "sknlblLato1f844d13Px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmAlertsManagement.AlertNameAvailable\")",
                "top": "0dp",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxSubAlertNameAvailable.add(lblSubAlertNameAvailable);
            flxAddAlertCol12.add(dataEntrySubAlertName, flxSubAlertNameAvailable);
            var flxAddAlertCol13 = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "100dp",
                "id": "flxAddAlertCol13",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "15dp",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0dp",
                "width": "30%",
                "zIndex": 1
            }, {}, {});
            flxAddAlertCol13.setDefaultUnit(kony.flex.DP);
            var lblAddAlertRadioHeader = new kony.ui.Label({
                "id": "lblAddAlertRadioHeader",
                "isVisible": true,
                "left": "0dp",
                "skin": "sknLbl485C75LatoRegular13Px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmAlertsManagement.AccountLevelAlert\")",
                "top": "0dp",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_TOP_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var lblAddAlertUserAlert = new kony.ui.Label({
                "id": "lblAddAlertUserAlert",
                "isVisible": false,
                "left": "0dp",
                "skin": "sknLbl485C75LatoRegular13Px",
                "text": "User Alert",
                "top": "30dp",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_TOP_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var customRadioButtonAccountLevel = new com.adminConsole.common.customRadioButtonGroup({
                "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                "id": "customRadioButtonAccountLevel",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "masterType": constants.MASTER_TYPE_DEFAULT,
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "30dp",
                "width": "100%",
                "overrides": {
                    "customRadioButtonGroup": {
                        "top": "30dp"
                    },
                    "flxRadioButton1": {
                        "width": "70dp"
                    },
                    "flxRadioButton3": {
                        "isVisible": false
                    },
                    "imgRadioButton1": {
                        "src": "radionormal_1x.png"
                    },
                    "imgRadioButton2": {
                        "src": "radionormal_1x.png"
                    },
                    "imgRadioButton3": {
                        "src": "radionormal_1x.png"
                    },
                    "imgRadioButton4": {
                        "src": "radionormal_1x.png"
                    },
                    "imgRadioButton5": {
                        "src": "radionormal_1x.png"
                    },
                    "lblRadioButtonValue1": {
                        "i18n_text": "kony.i18n.getLocalizedString(\"i18n.common.yes\")"
                    },
                    "lblRadioButtonValue2": {
                        "i18n_text": "kony.i18n.getLocalizedString(\"i18n.common.no\")"
                    }
                }
            }, {
                "overrides": {}
            }, {
                "overrides": {}
            });
            var flxRadioGroupErrorAlertLevel = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                "clipBounds": true,
                "id": "flxRadioGroupErrorAlertLevel",
                "isVisible": false,
                "layoutType": kony.flex.FLOW_HORIZONTAL,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "50dp",
                "width": "100%",
                "zIndex": 1
            }, {}, {});
            flxRadioGroupErrorAlertLevel.setDefaultUnit(kony.flex.DP);
            var lblIconErrorRadioAlertLevel = new kony.ui.Label({
                "id": "lblIconErrorRadioAlertLevel",
                "isVisible": true,
                "left": "0dp",
                "skin": "sknErrorIcon",
                "text": "",
                "top": "0dp",
                "width": "15dp",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var lblErrMsgRadioGroupAlertLevel = new kony.ui.Label({
                "id": "lblErrMsgRadioGroupAlertLevel",
                "isVisible": true,
                "left": "0dp",
                "skin": "sknlblError",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmAlertsManagement.Please_select_type\")",
                "top": "0dp",
                "width": "170dp",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxRadioGroupErrorAlertLevel.add(lblIconErrorRadioAlertLevel, lblErrMsgRadioGroupAlertLevel);
            flxAddAlertCol13.add(lblAddAlertRadioHeader, lblAddAlertUserAlert, customRadioButtonAccountLevel, flxRadioGroupErrorAlertLevel);
            flxAddAlertRow1.add(flxAddAlertCol11, flxAddAlertCol12, flxAddAlertCol13);
            var flxAddAlertRow2 = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": false,
                "height": "90px",
                "id": "flxAddAlertRow2",
                "isVisible": true,
                "layoutType": kony.flex.FLOW_HORIZONTAL,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "5dp",
                "width": "100%",
                "zIndex": 5
            }, {}, {});
            flxAddAlertRow2.setDefaultUnit(kony.flex.DP);
            var flxAddAlertCol21 = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": false,
                "height": "70dp",
                "id": "flxAddAlertCol21",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "20dp",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0dp",
                "width": "30%",
                "zIndex": 1
            }, {}, {});
            flxAddAlertCol21.setDefaultUnit(kony.flex.DP);
            var lblAddAlertUserRadioHeader = new kony.ui.Label({
                "id": "lblAddAlertUserRadioHeader",
                "isVisible": true,
                "left": "0dp",
                "skin": "sknLbl485C75LatoRegular13Px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmAlertsManagement.AlertType_LC\")",
                "top": "0dp",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_TOP_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var lblAddAlertUserType = new kony.ui.Label({
                "id": "lblAddAlertUserType",
                "isVisible": false,
                "left": "0dp",
                "skin": "sknLbl485C75LatoRegular13Px",
                "text": "User Alert",
                "top": "30dp",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_TOP_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var customRadioButtonAlertType = new com.adminConsole.common.customRadioButtonGroup({
                "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                "id": "customRadioButtonAlertType",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "masterType": constants.MASTER_TYPE_DEFAULT,
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "30dp",
                "width": "100%",
                "overrides": {
                    "customRadioButtonGroup": {
                        "top": "30dp"
                    },
                    "flxRadioButton3": {
                        "isVisible": false
                    },
                    "imgRadioButton1": {
                        "src": "radionormal_1x.png"
                    },
                    "imgRadioButton2": {
                        "src": "radionormal_1x.png"
                    },
                    "imgRadioButton3": {
                        "src": "radionormal_1x.png"
                    },
                    "imgRadioButton4": {
                        "src": "radionormal_1x.png"
                    },
                    "imgRadioButton5": {
                        "src": "radionormal_1x.png"
                    },
                    "lblRadioButtonValue1": {
                        "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmAlertsManagement.GlobalAlert\")"
                    },
                    "lblRadioButtonValue2": {
                        "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmAlertsManagement.UserAlert\")"
                    }
                }
            }, {
                "overrides": {}
            }, {
                "overrides": {}
            });
            var flxRadioGroupErrorAlertType = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                "clipBounds": true,
                "id": "flxRadioGroupErrorAlertType",
                "isVisible": false,
                "layoutType": kony.flex.FLOW_HORIZONTAL,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "50dp",
                "width": "100%",
                "zIndex": 1
            }, {}, {});
            flxRadioGroupErrorAlertType.setDefaultUnit(kony.flex.DP);
            var lblIconErrorRadioAlertType = new kony.ui.Label({
                "id": "lblIconErrorRadioAlertType",
                "isVisible": true,
                "left": "0dp",
                "skin": "sknErrorIcon",
                "text": "",
                "top": "0dp",
                "width": "15dp",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var lblErrMsgRadioAlertType = new kony.ui.Label({
                "id": "lblErrMsgRadioAlertType",
                "isVisible": true,
                "left": "0dp",
                "skin": "sknlblError",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmAlertsManagement.Please_select_type\")",
                "top": "0dp",
                "width": "170dp",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxRadioGroupErrorAlertType.add(lblIconErrorRadioAlertType, lblErrMsgRadioAlertType);
            flxAddAlertCol21.add(lblAddAlertUserRadioHeader, lblAddAlertUserType, customRadioButtonAlertType, flxRadioGroupErrorAlertType);
            var flxAutoSubscribe = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": false,
                "height": "70dp",
                "id": "flxAutoSubscribe",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "20dp",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0dp",
                "width": "30%",
                "zIndex": 1
            }, {}, {});
            flxAutoSubscribe.setDefaultUnit(kony.flex.DP);
            var lblAddAlertAutoRadioHeader = new kony.ui.Label({
                "id": "lblAddAlertAutoRadioHeader",
                "isVisible": true,
                "left": "0dp",
                "skin": "sknLbl485C75LatoRegular13Px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmAlertsManagement.AutoSubscribeNewUsers\")",
                "top": "0dp",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_TOP_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var lblAutoSubTxt = new kony.ui.Label({
                "id": "lblAutoSubTxt",
                "isVisible": false,
                "left": "0dp",
                "skin": "sknLbl485c75LatoRegItalic13px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmAlertsManagement.onlyApplicableUserAlert\")",
                "top": "30dp",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_TOP_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var customRadioButtonAlertTypeAutoSubscribe = new com.adminConsole.common.customRadioButtonGroup({
                "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                "id": "customRadioButtonAlertTypeAutoSubscribe",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "masterType": constants.MASTER_TYPE_DEFAULT,
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "30dp",
                "width": "100%",
                "overrides": {
                    "customRadioButtonGroup": {
                        "isVisible": true,
                        "minWidth": "viz.val_cleared",
                        "top": "30dp",
                        "width": "100%"
                    },
                    "flxRadioButton1": {
                        "minWidth": "viz.val_cleared",
                        "width": "70dp"
                    },
                    "flxRadioButton3": {
                        "isVisible": false
                    },
                    "imgRadioButton1": {
                        "src": "radionormal_1x.png"
                    },
                    "imgRadioButton2": {
                        "src": "radionormal_1x.png"
                    },
                    "imgRadioButton3": {
                        "src": "radionormal_1x.png"
                    },
                    "imgRadioButton4": {
                        "src": "radionormal_1x.png"
                    },
                    "imgRadioButton5": {
                        "src": "radionormal_1x.png"
                    },
                    "lblRadioButtonValue1": {
                        "i18n_text": "kony.i18n.getLocalizedString(\"i18n.common.yes\")"
                    },
                    "lblRadioButtonValue2": {
                        "i18n_text": "kony.i18n.getLocalizedString(\"i18n.common.no\")"
                    }
                }
            }, {
                "overrides": {}
            }, {
                "overrides": {}
            });
            flxAutoSubscribe.add(lblAddAlertAutoRadioHeader, lblAutoSubTxt, customRadioButtonAlertTypeAutoSubscribe);
            flxAddAlertRow2.add(flxAddAlertCol21, flxAutoSubscribe);
            var flxAddAlertRow3 = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": false,
                "height": "90px",
                "id": "flxAddAlertRow3",
                "isVisible": true,
                "layoutType": kony.flex.FLOW_HORIZONTAL,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "5dp",
                "width": "100%",
                "zIndex": 5
            }, {}, {});
            flxAddAlertRow3.setDefaultUnit(kony.flex.DP);
            var flxAddAlertCol22 = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                "clipBounds": false,
                "id": "flxAddAlertCol22",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "20dp",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0dp",
                "width": "30%",
                "zIndex": 5
            }, {}, {});
            flxAddAlertCol22.setDefaultUnit(kony.flex.DP);
            var lblAddAlertApps = new kony.ui.Label({
                "id": "lblAddAlertApps",
                "isVisible": true,
                "left": "0dp",
                "skin": "sknLbl485C75LatoRegular13Px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmAlertsManagement.Apps\")",
                "top": "0dp",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var customListboxAlertApps = new com.adminConsole.alerts.customListbox({
                "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                "id": "customListboxAlertApps",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "masterType": constants.MASTER_TYPE_DEFAULT,
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "25dp",
                "width": "100%",
                "zIndex": 5,
                "overrides": {
                    "customListbox": {
                        "top": "25dp",
                        "zIndex": 5
                    },
                    "imgCheckBox": {
                        "src": "checkboxnormal.png"
                    },
                    "lblErrorMsg": {
                        "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmAlertsManagement.SelectApplicableAppsError\")",
                        "width": "90%"
                    },
                    "lblIconDropdown": {
                        "text": ""
                    }
                }
            }, {
                "overrides": {}
            }, {
                "overrides": {}
            });
            flxAddAlertCol22.add(lblAddAlertApps, customListboxAlertApps);
            var flxAddAlertCol23 = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                "clipBounds": false,
                "id": "flxAddAlertCol23",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "15dp",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0dp",
                "width": "30%",
                "zIndex": 1
            }, {}, {});
            flxAddAlertCol23.setDefaultUnit(kony.flex.DP);
            var lblAddAlertUserTypes = new kony.ui.Label({
                "id": "lblAddAlertUserTypes",
                "isVisible": true,
                "left": "0dp",
                "skin": "sknLbl485C75LatoRegular13Px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmAlertsManagement.User_Type\")",
                "top": "0dp",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var customListboxAlertUsertypes = new com.adminConsole.alerts.customListbox({
                "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                "id": "customListboxAlertUsertypes",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "masterType": constants.MASTER_TYPE_DEFAULT,
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "25dp",
                "width": "100%",
                "zIndex": 5,
                "overrides": {
                    "customListbox": {
                        "top": "25dp",
                        "zIndex": 5
                    },
                    "imgCheckBox": {
                        "src": "checkboxnormal.png"
                    },
                    "lblErrorMsg": {
                        "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmAlertsManagement.SelectUserTypesError\")",
                        "width": "90%"
                    },
                    "lblIconDropdown": {
                        "text": ""
                    }
                }
            }, {
                "overrides": {}
            }, {
                "overrides": {}
            });
            flxAddAlertCol23.add(lblAddAlertUserTypes, customListboxAlertUsertypes);
            var flxAddAlertRecipient = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                "clipBounds": true,
                "id": "flxAddAlertRecipient",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "20dp",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0dp",
                "width": "30%",
                "zIndex": 1
            }, {}, {});
            flxAddAlertRecipient.setDefaultUnit(kony.flex.DP);
            var lblAddAlertRecipent = new kony.ui.Label({
                "id": "lblAddAlertRecipent",
                "isVisible": true,
                "left": "0dp",
                "skin": "sknLbl485C75LatoRegular13Px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmAlertsManagement.Recipients_LC\")",
                "top": "0dp",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var lstBoxRecipients = new kony.ui.ListBox({
                "focusSkin": "defListBoxFocus",
                "height": "40dp",
                "id": "lstBoxRecipients",
                "isVisible": true,
                "left": "0dp",
                "masterData": [
                    ["lb1", "Placeholder One"],
                    ["lb2", "Placeholder Two"],
                    ["lb3", "Placeholder Three"]
                ],
                "skin": "sknlbxBgffffffBorderc1c9ceRadius3Px",
                "top": "25dp",
                "width": "100%",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [3, 0, 0, 0],
                "paddingInPixel": false
            }, {
                "multiSelect": false
            });
            var flxAddAlertErrorRecipient = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                "clipBounds": true,
                "id": "flxAddAlertErrorRecipient",
                "isVisible": false,
                "layoutType": kony.flex.FLOW_HORIZONTAL,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "68dp",
                "width": "100%",
                "zIndex": 1
            }, {}, {});
            flxAddAlertErrorRecipient.setDefaultUnit(kony.flex.DP);
            var lblIconErrorAlertRecipient = new kony.ui.Label({
                "id": "lblIconErrorAlertRecipient",
                "isVisible": true,
                "left": "0dp",
                "skin": "sknErrorIcon",
                "text": "",
                "top": "0dp",
                "width": "15dp",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var lblErrorMsgAlertRecipient = new kony.ui.Label({
                "id": "lblErrorMsgAlertRecipient",
                "isVisible": true,
                "left": "0dp",
                "skin": "sknlblError",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmAlertsManagement.PleaseSelectARecipient\")",
                "top": "0dp",
                "width": "200dp",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxAddAlertErrorRecipient.add(lblIconErrorAlertRecipient, lblErrorMsgAlertRecipient);
            flxAddAlertRecipient.add(lblAddAlertRecipent, lstBoxRecipients, flxAddAlertErrorRecipient);
            flxAddAlertRow3.add(flxAddAlertCol22, flxAddAlertCol23, flxAddAlertRecipient);
            var flxAddAlertRow4 = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                "bottom": 15,
                "clipBounds": false,
                "id": "flxAddAlertRow4",
                "isVisible": true,
                "layoutType": kony.flex.FLOW_HORIZONTAL,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "5dp",
                "width": "100%",
                "zIndex": 2
            }, {}, {});
            flxAddAlertRow4.setDefaultUnit(kony.flex.DP);
            var flxAddAlertAccountTypes = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                "clipBounds": true,
                "id": "flxAddAlertAccountTypes",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "20dp",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0dp",
                "width": "30%",
                "zIndex": 1
            }, {}, {});
            flxAddAlertAccountTypes.setDefaultUnit(kony.flex.DP);
            var lblAddAlertAccountsType = new kony.ui.Label({
                "id": "lblAddAlertAccountsType",
                "isVisible": true,
                "left": "0dp",
                "skin": "sknLbl485C75LatoRegular13Px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmAlertsManagement.ApplicableAccountType_LC\")",
                "top": "0dp",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var customListboxAlertAccountTypes = new com.adminConsole.alerts.customListbox({
                "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                "id": "customListboxAlertAccountTypes",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "masterType": constants.MASTER_TYPE_DEFAULT,
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "25dp",
                "width": "100%",
                "zIndex": 1,
                "overrides": {
                    "customListbox": {
                        "top": "25dp",
                        "zIndex": 1
                    },
                    "flxSegmentList": {
                        "isVisible": false
                    },
                    "imgCheckBox": {
                        "src": "checkboxnormal.png"
                    },
                    "lblErrorMsg": {
                        "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmAlertsManagement.SelectAccountTypesError\")",
                        "width": "90%"
                    },
                    "lblIconDropdown": {
                        "text": ""
                    },
                    "segList": {
                        "height": "200dp",
                        "maxHeight": "viz.val_cleared"
                    }
                }
            }, {
                "overrides": {}
            }, {
                "overrides": {}
            });
            var flxAddAlertErrorAccounts = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                "clipBounds": true,
                "id": "flxAddAlertErrorAccounts",
                "isVisible": false,
                "layoutType": kony.flex.FLOW_HORIZONTAL,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "68dp",
                "width": "100%",
                "zIndex": 1
            }, {}, {});
            flxAddAlertErrorAccounts.setDefaultUnit(kony.flex.DP);
            var lblIconErrorAlertAccounts = new kony.ui.Label({
                "id": "lblIconErrorAlertAccounts",
                "isVisible": true,
                "left": "0dp",
                "skin": "sknErrorIcon",
                "text": "",
                "top": "0dp",
                "width": "15dp",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var lblErrorMsgAlertAccounts = new kony.ui.Label({
                "id": "lblErrorMsgAlertAccounts",
                "isVisible": true,
                "left": "0dp",
                "skin": "sknlblError",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmAlertsManagement.Please_select_usertype\")",
                "top": "0dp",
                "width": "200dp",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxAddAlertErrorAccounts.add(lblIconErrorAlertAccounts, lblErrorMsgAlertAccounts);
            flxAddAlertAccountTypes.add(lblAddAlertAccountsType, customListboxAlertAccountTypes, flxAddAlertErrorAccounts);
            flxAddAlertRow4.add(flxAddAlertAccountTypes);
            var flxAddAlertFrequency = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "160dp",
                "id": "flxAddAlertFrequency",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "5dp",
                "width": "100%"
            }, {}, {});
            flxAddAlertFrequency.setDefaultUnit(kony.flex.DP);
            var lblAddAlertFrequency = new kony.ui.Label({
                "id": "lblAddAlertFrequency",
                "isVisible": true,
                "left": "20px",
                "skin": "sknLatoRegular192B4516px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmAlertsManagement.DefaultFrequencyDetails\")",
                "top": "0px",
                "width": "100%",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var AlertFrequencySelectionCat = new com.adminConsole.alerts.frequencySelection({
                "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                "id": "AlertFrequencySelectionCat",
                "isVisible": true,
                "layoutType": kony.flex.FLOW_HORIZONTAL,
                "left": "20dp",
                "masterType": constants.MASTER_TYPE_DEFAULT,
                "isModalContainer": false,
                "right": "20dp",
                "skin": "sknFlxBgF8F9FABrE1E5EER3px",
                "top": "33dp",
                "overrides": {
                    "flxFrequencyColumn1": {
                        "bottom": "15dp"
                    },
                    "flxFrequencyColumn2": {
                        "bottom": "viz.val_cleared"
                    },
                    "flxFrequencyColumn3": {
                        "isVisible": false
                    },
                    "flxInlineError1": {
                        "isVisible": false,
                        "top": "75dp"
                    },
                    "flxInlineError2": {
                        "isVisible": false,
                        "top": "75dp"
                    },
                    "flxInlineError3": {
                        "top": "75dp"
                    },
                    "frequencySelection": {
                        "left": "20dp",
                        "right": "20dp",
                        "top": "33dp",
                        "width": "viz.val_cleared"
                    },
                    "timePicker.lstbxAMPM": {
                        "left": "146px",
                        "zIndex": 5
                    }
                }
            }, {
                "overrides": {}
            }, {
                "overrides": {}
            });
            flxAddAlertFrequency.add(lblAddAlertFrequency, AlertFrequencySelectionCat);
            var flxAddAlertAttributes = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "130dp",
                "id": "flxAddAlertAttributes",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "5dp",
                "width": "100%"
            }, {}, {});
            flxAddAlertAttributes.setDefaultUnit(kony.flex.DP);
            var lblAddAlertAttributes = new kony.ui.Label({
                "id": "lblAddAlertAttributes",
                "isVisible": true,
                "left": "20px",
                "skin": "sknLatoRegular192B4516px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmAlertsManagement.AttributeDetails\")",
                "top": "0px",
                "width": "100%",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var flxAlertAttributes = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                "clipBounds": true,
                "id": "flxAlertAttributes",
                "isVisible": true,
                "layoutType": kony.flex.FLOW_HORIZONTAL,
                "left": 20,
                "isModalContainer": false,
                "right": "20dp",
                "skin": "sknflxf5f6f8Op100BorderE1E5Ed",
                "top": "35dp"
            }, {}, {});
            flxAlertAttributes.setDefaultUnit(kony.flex.DP);
            var flxAlertAttribute1 = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                "clipBounds": true,
                "id": "flxAlertAttribute1",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "15dp",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0px",
                "width": "30%",
                "zIndex": 1
            }, {}, {});
            flxAlertAttribute1.setDefaultUnit(kony.flex.DP);
            var lstBoxAlertAttribute1 = new kony.ui.ListBox({
                "bottom": "20dp",
                "focusSkin": "defListBoxFocus",
                "height": "40dp",
                "id": "lstBoxAlertAttribute1",
                "isVisible": true,
                "left": "0dp",
                "masterData": [
                    ["lb1", "Success"],
                    ["lb2", "Failure"]
                ],
                "skin": "sknlbxBgffffffBorderc1c9ceRadius3Px",
                "top": "20dp",
                "width": "100%",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [3, 0, 0, 0],
                "paddingInPixel": false
            }, {
                "multiSelect": false
            });
            var flxAttributeError1 = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                "bottom": 10,
                "clipBounds": true,
                "id": "flxAttributeError1",
                "isVisible": false,
                "layoutType": kony.flex.FLOW_HORIZONTAL,
                "left": "0dp",
                "isModalContainer": false,
                "right": "0dp",
                "skin": "slFbox",
                "top": "65px",
                "width": "95%"
            }, {}, {});
            flxAttributeError1.setDefaultUnit(kony.flex.DP);
            var lblErrorIcon1 = new kony.ui.Label({
                "id": "lblErrorIcon1",
                "isVisible": true,
                "left": "0dp",
                "skin": "sknFontIconError",
                "text": "",
                "top": "1dp",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var lblErrorText1 = new kony.ui.Label({
                "id": "lblErrorText1",
                "isVisible": true,
                "left": "5dp",
                "skin": "sknlblError",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmAlertsManagement.PleaseSelectFrequency\")",
                "top": "0dp",
                "width": "85%",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxAttributeError1.add(lblErrorIcon1, lblErrorText1);
            flxAlertAttribute1.add(lstBoxAlertAttribute1, flxAttributeError1);
            var flxAlertAttribute2 = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                "clipBounds": true,
                "id": "flxAlertAttribute2",
                "isVisible": false,
                "layoutType": kony.flex.FREE_FORM,
                "left": "15dp",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0px",
                "width": "30%",
                "zIndex": 1
            }, {}, {});
            flxAlertAttribute2.setDefaultUnit(kony.flex.DP);
            var lstBoxAlertAttribute2 = new kony.ui.ListBox({
                "bottom": "20dp",
                "focusSkin": "defListBoxFocus",
                "height": "40dp",
                "id": "lstBoxAlertAttribute2",
                "isVisible": true,
                "left": "0dp",
                "masterData": [
                    ["lb1", "English-UK"],
                    ["lb2", "German"],
                    ["lb3", "Spanish"]
                ],
                "skin": "sknlbxBgffffffBorderc1c9ceRadius3Px",
                "top": "20px",
                "width": "100%",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [3, 0, 0, 0],
                "paddingInPixel": false
            }, {
                "multiSelect": false
            });
            var flxAttributeError2 = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                "bottom": "10px",
                "clipBounds": true,
                "id": "flxAttributeError2",
                "isVisible": false,
                "layoutType": kony.flex.FLOW_HORIZONTAL,
                "left": "0dp",
                "isModalContainer": false,
                "right": "0dp",
                "skin": "slFbox",
                "top": "65px",
                "width": "95%"
            }, {}, {});
            flxAttributeError2.setDefaultUnit(kony.flex.DP);
            var lblErrorIcon2 = new kony.ui.Label({
                "id": "lblErrorIcon2",
                "isVisible": true,
                "left": "0dp",
                "skin": "sknFontIconError",
                "text": "",
                "top": "1dp",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var lblErrorText2 = new kony.ui.Label({
                "id": "lblErrorText2",
                "isVisible": true,
                "left": "5dp",
                "skin": "sknlblError",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmAlertsManagement.PleaseSelectFrequency\")",
                "top": "0dp",
                "width": "85%",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxAttributeError2.add(lblErrorIcon2, lblErrorText2);
            flxAlertAttribute2.add(lstBoxAlertAttribute2, flxAttributeError2);
            var flxAlertAttribute3 = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                "clipBounds": true,
                "id": "flxAlertAttribute3",
                "isVisible": false,
                "layoutType": kony.flex.FREE_FORM,
                "left": "15dp",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0dp",
                "width": "33%"
            }, {}, {});
            flxAlertAttribute3.setDefaultUnit(kony.flex.DP);
            var flxValues = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "bottom": "20dp",
                "clipBounds": true,
                "height": "40dp",
                "id": "flxValues",
                "isVisible": true,
                "layoutType": kony.flex.FLOW_HORIZONTAL,
                "reverseLayoutDirection": false,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "20dp",
                "width": "100%"
            }, {}, {});
            flxValues.setDefaultUnit(kony.flex.DP);
            var ValueEntryFrom = new com.adminConsole.Features.ValueEntry({
                "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                "id": "ValueEntryFrom",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "masterType": constants.MASTER_TYPE_DEFAULT,
                "isModalContainer": false,
                "right": "0dp",
                "skin": "slFbox",
                "top": "0px",
                "width": "100%",
                "overrides": {
                    "ValueEntry": {
                        "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                        "bottom": "viz.val_cleared",
                        "centerY": "viz.val_cleared",
                        "left": "0dp",
                        "right": "0dp",
                        "top": "0px",
                        "width": "100%"
                    },
                    "tbxEnterValue": {
                        "placeholder": "Value From"
                    }
                }
            }, {
                "overrides": {}
            }, {
                "overrides": {}
            });
            var ValueEntryTo = new com.adminConsole.Features.ValueEntry({
                "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                "id": "ValueEntryTo",
                "isVisible": false,
                "layoutType": kony.flex.FREE_FORM,
                "left": "15dp",
                "masterType": constants.MASTER_TYPE_DEFAULT,
                "isModalContainer": false,
                "right": "0dp",
                "skin": "slFbox",
                "top": "0px",
                "width": "45%",
                "overrides": {
                    "ValueEntry": {
                        "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                        "bottom": "viz.val_cleared",
                        "centerY": "viz.val_cleared",
                        "isVisible": false,
                        "left": "15dp",
                        "right": "0dp",
                        "top": "0px",
                        "width": "45%"
                    },
                    "tbxEnterValue": {
                        "i18n_placeholder": "kony.i18n.getLocalizedString(\"i18n.frmAlertsManagement.Value_to\")"
                    }
                }
            }, {
                "overrides": {}
            }, {
                "overrides": {}
            });
            flxValues.add(ValueEntryFrom, ValueEntryTo);
            var flxAttributeError3 = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                "bottom": "10dp",
                "clipBounds": true,
                "id": "flxAttributeError3",
                "isVisible": false,
                "layoutType": kony.flex.FLOW_HORIZONTAL,
                "left": "0dp",
                "isModalContainer": false,
                "right": "0dp",
                "skin": "slFbox",
                "top": "65px",
                "width": "96%"
            }, {}, {});
            flxAttributeError3.setDefaultUnit(kony.flex.DP);
            var lblErrorIcon3 = new kony.ui.Label({
                "id": "lblErrorIcon3",
                "isVisible": true,
                "left": "0dp",
                "skin": "sknFontIconError",
                "text": "",
                "top": "1dp",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var lblErrorText3 = new kony.ui.Label({
                "id": "lblErrorText3",
                "isVisible": true,
                "left": "5dp",
                "skin": "sknlblError",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmAlertsManagement.PleaseSelectFrequency\")",
                "top": "0dp",
                "width": "85%",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxAttributeError3.add(lblErrorIcon3, lblErrorText3);
            flxAlertAttribute3.add(flxValues, flxAttributeError3);
            flxAlertAttributes.add(flxAlertAttribute1, flxAlertAttribute2, flxAlertAttribute3);
            flxAddAlertAttributes.add(lblAddAlertAttributes, flxAlertAttributes);
            var flxAddAlertChannels = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                "clipBounds": true,
                "id": "flxAddAlertChannels",
                "isVisible": true,
                "layoutType": kony.flex.FLOW_VERTICAL,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "10dp",
                "width": "100%",
                "zIndex": 1
            }, {}, {});
            flxAddAlertChannels.setDefaultUnit(kony.flex.DP);
            var lblAddAlertChannels = new kony.ui.Label({
                "id": "lblAddAlertChannels",
                "isVisible": true,
                "left": "20px",
                "skin": "sknLbl192b45LatoReg16px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmAlertsManagement.DefaultChannels\")",
                "top": "0px",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var flxAddAlertChannelsContainer = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                "clipBounds": true,
                "id": "flxAddAlertChannelsContainer",
                "isVisible": true,
                "layoutType": kony.flex.FLOW_VERTICAL,
                "left": "20dp",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "15dp",
                "width": "80%",
                "zIndex": 1
            }, {}, {});
            flxAddAlertChannelsContainer.setDefaultUnit(kony.flex.DP);
            var flxAlertSupportedChannelEntries = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "30dp",
                "id": "flxAlertSupportedChannelEntries",
                "isVisible": true,
                "layoutType": kony.flex.FLOW_HORIZONTAL,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0dp",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {}, {});
            flxAlertSupportedChannelEntries.setDefaultUnit(kony.flex.DP);
            flxAlertSupportedChannelEntries.add();
            var flxAlertChannelError = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                "clipBounds": true,
                "id": "flxAlertChannelError",
                "isVisible": false,
                "layoutType": kony.flex.FLOW_HORIZONTAL,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "5dp",
                "width": "100%",
                "zIndex": 1
            }, {}, {});
            flxAlertChannelError.setDefaultUnit(kony.flex.DP);
            var lblIconErrorAlertChannel = new kony.ui.Label({
                "id": "lblIconErrorAlertChannel",
                "isVisible": true,
                "left": "0dp",
                "skin": "sknErrorIcon",
                "text": "",
                "top": "0dp",
                "width": "15dp",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var lblErrMsgAlertChannel = new kony.ui.Label({
                "id": "lblErrMsgAlertChannel",
                "isVisible": true,
                "left": "0dp",
                "skin": "sknlblError",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmAlertsManagement.lblNoCheckBoxSelected\")",
                "top": "0dp",
                "width": "170dp",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxAlertChannelError.add(lblIconErrorAlertChannel, lblErrMsgAlertChannel);
            flxAddAlertChannelsContainer.add(flxAlertSupportedChannelEntries, flxAlertChannelError);
            flxAddAlertChannels.add(lblAddAlertChannels, flxAddAlertChannelsContainer);
            var flxAddAlertLanguage = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                "clipBounds": true,
                "id": "flxAddAlertLanguage",
                "isVisible": true,
                "layoutType": kony.flex.FLOW_VERTICAL,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "10dp",
                "width": "100%"
            }, {}, {});
            flxAddAlertLanguage.setDefaultUnit(kony.flex.DP);
            var flxAddAlertLanguageHeader = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "50dp",
                "id": "flxAddAlertLanguageHeader",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0dp",
                "width": "100%",
                "zIndex": 1
            }, {}, {});
            flxAddAlertLanguageHeader.setDefaultUnit(kony.flex.DP);
            var lblAddAlertLanguageValue = new kony.ui.Label({
                "centerY": "50%",
                "id": "lblAddAlertLanguageValue",
                "isVisible": true,
                "left": "20dp",
                "skin": "sknLbl192b45Lato15px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmAlertsManagement.Supported_Language\")",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var flxAddAlertLanguagesError = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                "clipBounds": true,
                "id": "flxAddAlertLanguagesError",
                "isVisible": false,
                "layoutType": kony.flex.FLOW_HORIZONTAL,
                "left": "200dp",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "20dp",
                "width": "70%",
                "zIndex": 1
            }, {}, {});
            flxAddAlertLanguagesError.setDefaultUnit(kony.flex.DP);
            var lblIconAlertLanguagesError = new kony.ui.Label({
                "id": "lblIconAlertLanguagesError",
                "isVisible": true,
                "left": "0dp",
                "skin": "sknErrorIcon",
                "text": "",
                "top": "0dp",
                "width": "15dp",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var lblErrorMsgAlertLanguages = new kony.ui.Label({
                "id": "lblErrorMsgAlertLanguages",
                "isVisible": true,
                "left": "0dp",
                "skin": "sknlblError",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmAlertsManagement.Please_fill_all_the_fields\")",
                "top": "0dp",
                "width": "170dp",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxAddAlertLanguagesError.add(lblIconAlertLanguagesError, lblErrorMsgAlertLanguages);
            flxAddAlertLanguageHeader.add(lblAddAlertLanguageValue, flxAddAlertLanguagesError);
            var flxAddAlertLanguageSegment = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                "clipBounds": true,
                "id": "flxAddAlertLanguageSegment",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0dp",
                "width": "100%",
                "zIndex": 1
            }, {}, {});
            flxAddAlertLanguageSegment.setDefaultUnit(kony.flex.DP);
            var flxAddAlertLangSegHeader = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "50dp",
                "id": "flxAddAlertLangSegHeader",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "20dp",
                "isModalContainer": false,
                "right": "20dp",
                "skin": "slFbox",
                "top": "0dp",
                "zIndex": 1
            }, {}, {});
            flxAddAlertLangSegHeader.setDefaultUnit(kony.flex.DP);
            var flxAddAlertSegLanguage = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "100%",
                "id": "flxAddAlertSegLanguage",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0dp",
                "width": "18%",
                "zIndex": 1
            }, {}, {});
            flxAddAlertSegLanguage.setDefaultUnit(kony.flex.DP);
            var lblAddAlertSegLanguage = new kony.ui.Label({
                "centerY": "50%",
                "id": "lblAddAlertSegLanguage",
                "isVisible": true,
                "left": "10dp",
                "skin": "sknlblLato696c7312px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmAlertsManagement.Language_UC\")",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxAddAlertSegLanguage.add(lblAddAlertSegLanguage);
            var flxAddAlertSegDisplayName = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "100%",
                "id": "flxAddAlertSegDisplayName",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "21%",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0dp",
                "width": "23%",
                "zIndex": 1
            }, {}, {});
            flxAddAlertSegDisplayName.setDefaultUnit(kony.flex.DP);
            var lblAddAlertSegDisplayName = new kony.ui.Label({
                "centerY": "50%",
                "id": "lblAddAlertSegDisplayName",
                "isVisible": true,
                "left": "20dp",
                "skin": "sknlblLato696c7312px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.View.DISPLAYNAME\")",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxAddAlertSegDisplayName.add(lblAddAlertSegDisplayName);
            var flxAddAlertSegDescription = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "100%",
                "id": "flxAddAlertSegDescription",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "44%",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0dp",
                "width": "20%",
                "zIndex": 1
            }, {}, {});
            flxAddAlertSegDescription.setDefaultUnit(kony.flex.DP);
            var lblAddAlertSegDescription = new kony.ui.Label({
                "centerY": "50%",
                "id": "lblAddAlertSegDescription",
                "isVisible": true,
                "left": "20dp",
                "skin": "sknlblLato696c7312px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.Group.DISPLAY_DESCRIPTION\")",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxAddAlertSegDescription.add(lblAddAlertSegDescription);
            var flxAddAlertSegLine = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "bottom": "0dp",
                "clipBounds": true,
                "height": "1dp",
                "id": "flxAddAlertSegLine",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "isModalContainer": false,
                "right": "0dp",
                "skin": "sknSeparatorCsr",
                "zIndex": 1
            }, {}, {});
            flxAddAlertSegLine.setDefaultUnit(kony.flex.DP);
            flxAddAlertSegLine.add();
            flxAddAlertLangSegHeader.add(flxAddAlertSegLanguage, flxAddAlertSegDisplayName, flxAddAlertSegDescription, flxAddAlertSegLine);
            var flxAddAlertLangSegList = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                "clipBounds": true,
                "id": "flxAddAlertLangSegList",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "50dp",
                "width": "100%",
                "zIndex": 1
            }, {}, {});
            flxAddAlertLangSegList.setDefaultUnit(kony.flex.DP);
            var segAddAlertLanguages = new kony.ui.SegmentedUI2({
                "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                "data": [{
                    "lblAddAlertTypeSegDescription": "Label",
                    "lblAddAlertTypeSegDisplayName": "Label",
                    "lblAddAlertTypeSegLanguage": "Label",
                    "lblDescCount": "Label",
                    "lblIconDeleteLang": "Label",
                    "lblSeprator": "Label",
                    "lstBoxSelectLanguage": {
                        "masterData": [
                            ["lb1", "English(US)"],
                            ["lb2", "English(UK)"],
                            ["lb3", "Spanish"],
                            ["lb4", "French"],
                            ["lb0", "Select"]
                        ],
                        "selectedKey": "lb0",
                        "selectedKeys": null,
                        "selectedKeyValue": ["lb0", "Select"]
                    },
                    "tbxDescription": "",
                    "tbxDisplayName": {
                        "placeholder": "",
                        "text": ""
                    }
                }, {
                    "lblAddAlertTypeSegDescription": "Label",
                    "lblAddAlertTypeSegDisplayName": "Label",
                    "lblAddAlertTypeSegLanguage": "Label",
                    "lblDescCount": "Label",
                    "lblIconDeleteLang": "Label",
                    "lblSeprator": "Label",
                    "lstBoxSelectLanguage": {
                        "masterData": [
                            ["lb1", "English(US)"],
                            ["lb2", "English(UK)"],
                            ["lb3", "Spanish"],
                            ["lb4", "French"],
                            ["lb0", "Select"]
                        ],
                        "selectedKey": "lb0",
                        "selectedKeys": null,
                        "selectedKeyValue": ["lb0", "Select"]
                    },
                    "tbxDescription": "",
                    "tbxDisplayName": {
                        "placeholder": "",
                        "text": ""
                    }
                }, {
                    "lblAddAlertTypeSegDescription": "Label",
                    "lblAddAlertTypeSegDisplayName": "Label",
                    "lblAddAlertTypeSegLanguage": "Label",
                    "lblDescCount": "Label",
                    "lblIconDeleteLang": "Label",
                    "lblSeprator": "Label",
                    "lstBoxSelectLanguage": {
                        "masterData": [
                            ["lb1", "English(US)"],
                            ["lb2", "English(UK)"],
                            ["lb3", "Spanish"],
                            ["lb4", "French"],
                            ["lb0", "Select"]
                        ],
                        "selectedKey": "lb0",
                        "selectedKeys": null,
                        "selectedKeyValue": ["lb0", "Select"]
                    },
                    "tbxDescription": "",
                    "tbxDisplayName": {
                        "placeholder": "",
                        "text": ""
                    }
                }],
                "groupCells": false,
                "id": "segAddAlertLanguages",
                "isVisible": true,
                "left": "20dp",
                "needPageIndicator": true,
                "pageOffDotImage": "pageoffdot.png",
                "pageOnDotImage": "pageondot.png",
                "retainSelection": false,
                "right": "20dp",
                "rowFocusSkin": "seg2Focus",
                "rowSkin": "seg2Normal",
                "rowTemplate": "flxAlertsLanguageData",
                "sectionHeaderSkin": "sliPhoneSegmentHeader",
                "selectionBehavior": constants.SEGUI_DEFAULT_BEHAVIOR,
                "separatorRequired": false,
                "showScrollbars": false,
                "top": "0dp",
                "viewType": constants.SEGUI_VIEW_TYPE_TABLEVIEW,
                "widgetDataMap": {
                    "flxAddDescription": "flxAddDescription",
                    "flxAddDisplayName": "flxAddDisplayName",
                    "flxAddLanguage": "flxAddLanguage",
                    "flxAlertsLanguageData": "flxAlertsLanguageData",
                    "flxDelete": "flxDelete",
                    "flxDescription": "flxDescription",
                    "flxLanguageRowContainer": "flxLanguageRowContainer",
                    "lblAddAlertTypeSegDescription": "lblAddAlertTypeSegDescription",
                    "lblAddAlertTypeSegDisplayName": "lblAddAlertTypeSegDisplayName",
                    "lblAddAlertTypeSegLanguage": "lblAddAlertTypeSegLanguage",
                    "lblDescCount": "lblDescCount",
                    "lblIconDeleteLang": "lblIconDeleteLang",
                    "lblSeprator": "lblSeprator",
                    "lstBoxSelectLanguage": "lstBoxSelectLanguage",
                    "tbxDescription": "tbxDescription",
                    "tbxDisplayName": "tbxDisplayName"
                },
                "zIndex": 1
            }, {
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxAddAlertLangSegList.add(segAddAlertLanguages);
            flxAddAlertLanguageSegment.add(flxAddAlertLangSegHeader, flxAddAlertLangSegList);
            flxAddAlertLanguage.add(flxAddAlertLanguageHeader, flxAddAlertLanguageSegment);
            var flxAddAlertButtons = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "bottom": "0dp",
                "clipBounds": true,
                "height": "80dp",
                "id": "flxAddAlertButtons",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "slFbox",
                "width": "100%",
                "zIndex": 1
            }, {}, {});
            flxAddAlertButtons.setDefaultUnit(kony.flex.DP);
            var addAlertButtons = new com.adminConsole.common.commonButtons({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "height": "80px",
                "id": "addAlertButtons",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "20dp",
                "masterType": constants.MASTER_TYPE_DEFAULT,
                "isModalContainer": false,
                "right": 20,
                "skin": "slFbox",
                "top": "0dp",
                "overrides": {
                    "btnCancel": {
                        "left": "0px",
                        "right": "viz.val_cleared",
                        "zIndex": 3
                    },
                    "btnNext": {
                        "isVisible": false
                    },
                    "btnSave": {
                        "width": "130px"
                    },
                    "commonButtons": {
                        "bottom": "viz.val_cleared",
                        "left": "20dp",
                        "right": 20,
                        "top": "0dp",
                        "width": "viz.val_cleared"
                    }
                }
            }, {
                "overrides": {}
            }, {
                "overrides": {}
            });
            flxAddAlertButtons.add(addAlertButtons);
            flxAddAlertContainer.add(flxAddAlertHeading, flxAddAlertRow1, flxAddAlertRow2, flxAddAlertRow3, flxAddAlertRow4, flxAddAlertFrequency, flxAddAlertAttributes, flxAddAlertChannels, flxAddAlertLanguage, flxAddAlertButtons);
            flxAddAlertScreen.add(flxAddAlertContainer);
            flxSubAlerts.add(flxViewSubAlert, flxSubAlertContextualMenu, flxVariableReferenceContainer, flxAddAlertScreen);
            flxAlertBoxContainer.add(flxTabsAlerts, flxAlertCategories, flxAlertTypes, flxSubAlerts);
            flxAlertConfiguration.add(flxAlertBoxContainer);
            flxRightPanel.add(flxMainHeader, flxAlertsBreadCrumb, flxAlertConfiguration);
            var flxHeaderDropdown = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                "clipBounds": true,
                "id": "flxHeaderDropdown",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "isModalContainer": false,
                "right": "35dp",
                "skin": "slFbox",
                "top": "20dp",
                "width": "400dp",
                "zIndex": 5
            }, {}, {});
            flxHeaderDropdown.setDefaultUnit(kony.flex.DP);
            var dropdownMainHeader = new com.adminConsole.common.dropdownMainHeader({
                "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                "id": "dropdownMainHeader",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "masterType": constants.MASTER_TYPE_DEFAULT,
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0dp",
                "width": "100%",
                "overrides": {
                    "flxDropdown": {
                        "isVisible": false
                    },
                    "imgUpArrow": {
                        "src": "uparrow_2x.png"
                    }
                }
            }, {
                "overrides": {}
            }, {
                "overrides": {}
            });
            flxHeaderDropdown.add(dropdownMainHeader);
            var flxToastMessage = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "bottom": "0%",
                "clipBounds": true,
                "height": "8%",
                "id": "flxToastMessage",
                "isVisible": false,
                "layoutType": kony.flex.FREE_FORM,
                "left": "305px",
                "isModalContainer": false,
                "skin": "slFbox",
                "width": "78%",
                "zIndex": 20
            }, {}, {});
            flxToastMessage.setDefaultUnit(kony.flex.DP);
            var toastMessage = new com.adminConsole.common.toastMessage({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "height": "70px",
                "id": "toastMessage",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "masterType": constants.MASTER_TYPE_DEFAULT,
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0dp",
                "width": "100%",
                "overrides": {}
            }, {
                "overrides": {}
            }, {
                "overrides": {}
            });
            flxToastMessage.add(toastMessage);
            var flxLoading = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "100%",
                "id": "flxLoading",
                "isVisible": false,
                "layoutType": kony.flex.FREE_FORM,
                "left": "305px",
                "isModalContainer": false,
                "right": "0px",
                "skin": "sknLoadingBlur",
                "top": "0dp",
                "zIndex": 10
            }, {}, {});
            flxLoading.setDefaultUnit(kony.flex.DP);
            var flxImageContainer = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "centerX": "50%",
                "centerY": "50%",
                "clipBounds": true,
                "height": "75px",
                "id": "flxImageContainer",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "isModalContainer": false,
                "skin": "slFbox",
                "width": "75px",
                "zIndex": 1
            }, {}, {});
            flxImageContainer.setDefaultUnit(kony.flex.DP);
            var imgLoading = new kony.ui.Image2({
                "centerX": "50%",
                "centerY": "50%",
                "height": "50px",
                "id": "imgLoading",
                "isVisible": true,
                "skin": "slImage",
                "src": "loadingscreenimage.gif",
                "width": "50px",
                "zIndex": 1
            }, {
                "imageScaleMode": constants.IMAGE_SCALE_MODE_MAINTAIN_ASPECT_RATIO,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxImageContainer.add(imgLoading);
            flxLoading.add(flxImageContainer);
            var flxDeleteAlert = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "100%",
                "id": "flxDeleteAlert",
                "isVisible": false,
                "layoutType": kony.flex.FREE_FORM,
                "masterType": constants.MASTER_TYPE_USERWIDGET,
                "isModalContainer": false,
                "skin": "slFbox",
                "width": "100%",
                "zIndex": 10
            }, {}, {});
            flxDeleteAlert.setDefaultUnit(kony.flex.DP);
            var popUpDeactivate = new com.adminConsole.common.popUp({
                "height": "100%",
                "id": "popUpDeactivate",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "masterType": constants.MASTER_TYPE_DEFAULT,
                "isModalContainer": false,
                "skin": "sknflxBg000000Op50",
                "top": "0dp",
                "width": "100%",
                "zIndex": 1,
                "overrides": {
                    "btnPopUpCancel": {
                        "centerY": "50.00%",
                        "i18n_text": "kony.i18n.getLocalizedString(\"i18n.PopUp.NoLeaveAsIS\")",
                        "minWidth": "viz.val_cleared",
                        "right": "20px",
                        "width": kony.flex.USE_PREFFERED_SIZE
                    },
                    "btnPopUpDelete": {
                        "left": undefined,
                        "minWidth": "85dp",
                        "text": "YES",
                        "width": kony.flex.USE_PREFFERED_SIZE
                    },
                    "flxPopUp": {
                        "centerY": "50%",
                        "top": "viz.val_cleared"
                    },
                    "flxPopUpButtons": {
                        "reverseLayoutDirection": true,
                        "layoutType": kony.flex.FLOW_HORIZONTAL
                    },
                    "rtxPopUpDisclaimer": {
                        "bottom": 30,
                        "right": "viz.val_cleared",
                        "width": "93%"
                    }
                }
            }, {
                "overrides": {}
            }, {
                "overrides": {}
            });
            flxDeleteAlert.add(popUpDeactivate);
            var flxAlertPreview = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "100%",
                "id": "flxAlertPreview",
                "isVisible": false,
                "layoutType": kony.flex.FREE_FORM,
                "left": 0,
                "isModalContainer": false,
                "skin": "CopyslFbox0ie07b6077b734c",
                "top": 0,
                "width": "100%",
                "zIndex": 10
            }, {}, {});
            flxAlertPreview.setDefaultUnit(kony.flex.DP);
            var flxAlertPreviewMain = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "centerX": "50%",
                "centerY": "50%",
                "clipBounds": true,
                "height": "400px",
                "id": "flxAlertPreviewMain",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": 0,
                "isModalContainer": false,
                "skin": "CopyslFbox0a55815af731d49",
                "top": 0,
                "width": "700px",
                "zIndex": 10
            }, {}, {});
            flxAlertPreviewMain.setDefaultUnit(kony.flex.DP);
            var flxAlertPreviewTopColor = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "10dp",
                "id": "flxAlertPreviewTopColor",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "CopyslFbox0f849449508464e",
                "top": "0dp",
                "width": "100%",
                "zIndex": 1
            }, {}, {});
            flxAlertPreviewTopColor.setDefaultUnit(kony.flex.DP);
            flxAlertPreviewTopColor.add();
            var lblAlertPreviewHeader = new kony.ui.Label({
                "id": "lblAlertPreviewHeader",
                "isVisible": true,
                "left": "30px",
                "skin": "sknlblLatoBold35475f23px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmAlertsManagement.lblAlertPreviewHeader\")",
                "top": "35dp",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var flxAlertPreviewClose = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "25px",
                "id": "flxAlertPreviewClose",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "isModalContainer": false,
                "right": 15,
                "skin": "sknCursor",
                "top": "35dp",
                "width": "25px",
                "zIndex": 20
            }, {}, {});
            flxAlertPreviewClose.setDefaultUnit(kony.flex.DP);
            var imgAlertPreviewClose = new kony.ui.Image2({
                "centerX": "50%",
                "centerY": "50%",
                "height": "15dp",
                "id": "imgAlertPreviewClose",
                "isVisible": false,
                "skin": "slImage",
                "src": "close_blue.png",
                "width": "15dp",
                "zIndex": 1
            }, {
                "imageScaleMode": constants.IMAGE_SCALE_MODE_MAINTAIN_ASPECT_RATIO,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var lblAlertPreviewClose = new kony.ui.Label({
                "centerY": "50%",
                "height": "20dp",
                "id": "lblAlertPreviewClose",
                "isVisible": true,
                "left": "0dp",
                "skin": "sknIcon18pxGray",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmAlertsManagement.lblAlertPreviewClose\")",
                "width": "20dp",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxAlertPreviewClose.add(imgAlertPreviewClose, lblAlertPreviewClose);
            var flxAlertPreviewContent = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "300dp",
                "id": "flxAlertPreviewContent",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "30px",
                "isModalContainer": false,
                "skin": "CopyslFbox0gad664d41d864b",
                "top": "100dp",
                "width": "91%",
                "zIndex": 10
            }, {}, {});
            flxAlertPreviewContent.setDefaultUnit(kony.flex.DP);
            var flxAlertPreviewContentInner = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "300px",
                "id": "flxAlertPreviewContentInner",
                "isVisible": true,
                "layoutType": kony.flex.FLOW_VERTICAL,
                "left": "0px",
                "isModalContainer": false,
                "right": "0px",
                "skin": "slFbox",
                "top": "0px",
                "zIndex": 1
            }, {}, {});
            flxAlertPreviewContentInner.setDefaultUnit(kony.flex.DP);
            var flxAlertPreviewContent1 = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                "centerX": "50%",
                "clipBounds": true,
                "id": "flxAlertPreviewContent1",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0px",
                "isModalContainer": false,
                "right": "0px",
                "skin": "slFbox",
                "top": "0px",
                "width": "100%",
                "zIndex": 1
            }, {}, {});
            flxAlertPreviewContent1.setDefaultUnit(kony.flex.DP);
            var flxAlertPreviewNameHeader = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "20px",
                "id": "flxAlertPreviewNameHeader",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0px",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0px",
                "width": "25%",
                "zIndex": 1
            }, {}, {});
            flxAlertPreviewNameHeader.setDefaultUnit(kony.flex.DP);
            var lblAlertPreviewNameHeader = new kony.ui.Label({
                "height": "100%",
                "id": "lblAlertPreviewNameHeader",
                "isVisible": true,
                "left": "0px",
                "skin": "lblServicesDataHeader",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.permission.NAME\")",
                "top": "0px",
                "width": "100%",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxAlertPreviewNameHeader.add(lblAlertPreviewNameHeader);
            var flxAlertPreviewNameContent = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                "clipBounds": true,
                "id": "flxAlertPreviewNameContent",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0px",
                "isModalContainer": false,
                "skin": "sknServiceDetailsContent",
                "top": "20px",
                "width": "95%",
                "zIndex": 2
            }, {}, {});
            flxAlertPreviewNameContent.setDefaultUnit(kony.flex.DP);
            var lblAlertPreviewNameContent = new kony.ui.Label({
                "id": "lblAlertPreviewNameContent",
                "isVisible": true,
                "left": "0px",
                "skin": "sknlblLatoRegular484b5213px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmAlertsManagement.lblAlertPreviewNameContent\")",
                "top": "0px",
                "width": "100%",
                "zIndex": 2
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_TOP_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxAlertPreviewNameContent.add(lblAlertPreviewNameContent);
            flxAlertPreviewContent1.add(flxAlertPreviewNameHeader, flxAlertPreviewNameContent);
            var flxAlertPreviewContent2 = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "centerX": "50%",
                "clipBounds": true,
                "height": "40px",
                "id": "flxAlertPreviewContent2",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0px",
                "isModalContainer": false,
                "right": "0px",
                "skin": "slFbox",
                "top": "25px",
                "width": "100%",
                "zIndex": 1
            }, {}, {});
            flxAlertPreviewContent2.setDefaultUnit(kony.flex.DP);
            var flxAlertPreviewStatusHeader = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "20px",
                "id": "flxAlertPreviewStatusHeader",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0px",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0px",
                "width": "25%",
                "zIndex": 1
            }, {}, {});
            flxAlertPreviewStatusHeader.setDefaultUnit(kony.flex.DP);
            var lblAlertPreviewStatusHeader = new kony.ui.Label({
                "height": "100%",
                "id": "lblAlertPreviewStatusHeader",
                "isVisible": true,
                "left": "0px",
                "skin": "lblServicesDataHeader",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.roles.STATUS\")",
                "top": "0px",
                "width": "100%",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxAlertPreviewStatusHeader.add(lblAlertPreviewStatusHeader);
            var flxAlertPreviewStatusContent = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "20px",
                "id": "flxAlertPreviewStatusContent",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0px",
                "isModalContainer": false,
                "skin": "sknServiceDetailsContent",
                "top": "20px",
                "width": "25%",
                "zIndex": 1
            }, {}, {});
            flxAlertPreviewStatusContent.setDefaultUnit(kony.flex.DP);
            var lblAlertPreviewStatusContent = new kony.ui.Label({
                "height": "100%",
                "id": "lblAlertPreviewStatusContent",
                "isVisible": true,
                "left": "0px",
                "skin": "sknlblLatoRegular484b5213px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmServiceManagementController.Yes\")",
                "top": "0px",
                "width": "100%",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxAlertPreviewStatusContent.add(lblAlertPreviewStatusContent);
            var flxAlertPreviewChannelHeader = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "20px",
                "id": "flxAlertPreviewChannelHeader",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "20%",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0px",
                "width": "25%",
                "zIndex": 1
            }, {}, {});
            flxAlertPreviewChannelHeader.setDefaultUnit(kony.flex.DP);
            var lblAlertPreviewChannelHeader = new kony.ui.Label({
                "height": "100%",
                "id": "lblAlertPreviewChannelHeader",
                "isVisible": true,
                "left": "0px",
                "skin": "lblServicesDataHeader",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmAlertsManagement.lblAlertPreviewChannelHeader\")",
                "top": "0px",
                "width": "100%",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxAlertPreviewChannelHeader.add(lblAlertPreviewChannelHeader);
            var flxAlertPreviewChannelContent = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "20px",
                "id": "flxAlertPreviewChannelContent",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "20%",
                "isModalContainer": false,
                "skin": "sknServiceDetailsContent",
                "top": "20px",
                "width": "25%",
                "zIndex": 1
            }, {}, {});
            flxAlertPreviewChannelContent.setDefaultUnit(kony.flex.DP);
            var lblAlertPreviewChannelContent = new kony.ui.Label({
                "height": "100%",
                "id": "lblAlertPreviewChannelContent",
                "isVisible": true,
                "left": "0px",
                "skin": "sknlblLatoRegular484b5213px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmAlertsManagement.lblAlertPreviewChannelContent\")",
                "top": "0px",
                "width": "100%",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxAlertPreviewChannelContent.add(lblAlertPreviewChannelContent);
            flxAlertPreviewContent2.add(flxAlertPreviewStatusHeader, flxAlertPreviewStatusContent, flxAlertPreviewChannelHeader, flxAlertPreviewChannelContent);
            var flxAlertPreviewDescription = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                "clipBounds": true,
                "id": "flxAlertPreviewDescription",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0px",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "25px",
                "width": "100%",
                "zIndex": 1
            }, {}, {});
            flxAlertPreviewDescription.setDefaultUnit(kony.flex.DP);
            var flxAlertPreviewDescriptionHeader = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "20px",
                "id": "flxAlertPreviewDescriptionHeader",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0px",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0px",
                "width": "25%",
                "zIndex": 1
            }, {}, {});
            flxAlertPreviewDescriptionHeader.setDefaultUnit(kony.flex.DP);
            var lblAlertPreviewDescription = new kony.ui.Label({
                "height": "100%",
                "id": "lblAlertPreviewDescription",
                "isVisible": true,
                "left": "0px",
                "skin": "lblServicesDataHeader",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.View.DESCRIPTION\")",
                "top": "0px",
                "width": "100%",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxAlertPreviewDescriptionHeader.add(lblAlertPreviewDescription);
            var flxAlertPreviewDescriptionContent = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                "clipBounds": true,
                "id": "flxAlertPreviewDescriptionContent",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0px",
                "isModalContainer": false,
                "skin": "sknServiceDetailsContent",
                "top": "20px",
                "width": "90%",
                "zIndex": 1
            }, {}, {});
            flxAlertPreviewDescriptionContent.setDefaultUnit(kony.flex.DP);
            var lblAlertPreviewDescriptionContent = new kony.ui.Label({
                "id": "lblAlertPreviewDescriptionContent",
                "isVisible": true,
                "left": "0px",
                "skin": "sknlblLatoRegular484b5213px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmAlertsManagement.lblAlertPreviewDescriptionContent\")",
                "top": "0px",
                "width": "100%",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxAlertPreviewDescriptionContent.add(lblAlertPreviewDescriptionContent);
            flxAlertPreviewDescription.add(flxAlertPreviewDescriptionHeader, flxAlertPreviewDescriptionContent);
            var flxAlertPreviewAlertContent = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                "clipBounds": true,
                "id": "flxAlertPreviewAlertContent",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0px",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "25px",
                "width": "100%",
                "zIndex": 1
            }, {}, {});
            flxAlertPreviewAlertContent.setDefaultUnit(kony.flex.DP);
            var flxAlertPreviewAlertContentHeader = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "20px",
                "id": "flxAlertPreviewAlertContentHeader",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0px",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0px",
                "width": "25%",
                "zIndex": 1
            }, {}, {});
            flxAlertPreviewAlertContentHeader.setDefaultUnit(kony.flex.DP);
            var lblAlertPreviewAlertContentHeader = new kony.ui.Label({
                "height": "100%",
                "id": "lblAlertPreviewAlertContentHeader",
                "isVisible": true,
                "left": "0px",
                "skin": "lblServicesDataHeader",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmAlertsManagement.lblAlertPreviewAlertContentHeader\")",
                "top": "0px",
                "width": "100%",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxAlertPreviewAlertContentHeader.add(lblAlertPreviewAlertContentHeader);
            var flxAlertPreviewAlertContentContent = new kony.ui.FlexScrollContainer({
                "allowHorizontalBounce": false,
                "allowVerticalBounce": true,
                "bounces": true,
                "clipBounds": true,
                "enableScrolling": true,
                "height": "70px",
                "horizontalScrollIndicator": true,
                "id": "flxAlertPreviewAlertContentContent",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0px",
                "pagingEnabled": false,
                "scrollDirection": kony.flex.SCROLL_VERTICAL,
                "top": "20px",
                "verticalScrollIndicator": true,
                "width": "90%",
                "zIndex": 1
            }, {}, {});
            flxAlertPreviewAlertContentContent.setDefaultUnit(kony.flex.DP);
            var lblAlertPreviewAlertContentContent = new kony.ui.Label({
                "id": "lblAlertPreviewAlertContentContent",
                "isVisible": true,
                "left": "0px",
                "skin": "sknlblLatoRegular484b5213px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmAlertsManagement.lblAlertPreviewAlertContentContent\")",
                "top": "0px",
                "width": "100%",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxAlertPreviewAlertContentContent.add(lblAlertPreviewAlertContentContent);
            flxAlertPreviewAlertContent.add(flxAlertPreviewAlertContentHeader, flxAlertPreviewAlertContentContent);
            flxAlertPreviewContentInner.add(flxAlertPreviewContent1, flxAlertPreviewContent2, flxAlertPreviewDescription, flxAlertPreviewAlertContent);
            flxAlertPreviewContent.add(flxAlertPreviewContentInner);
            flxAlertPreviewMain.add(flxAlertPreviewTopColor, lblAlertPreviewHeader, flxAlertPreviewClose, flxAlertPreviewContent);
            flxAlertPreview.add(flxAlertPreviewMain);
            var flxAddSubAlertPopUp = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "100%",
                "id": "flxAddSubAlertPopUp",
                "isVisible": false,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "sknFocus50",
                "top": "0dp",
                "width": "100%",
                "zIndex": 5
            }, {}, {});
            flxAddSubAlertPopUp.setDefaultUnit(kony.flex.DP);
            var flxAddSubAlert = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                "centerX": "50%",
                "clipBounds": true,
                "id": "flxAddSubAlert",
                "isVisible": true,
                "layoutType": kony.flex.FLOW_VERTICAL,
                "left": "27%",
                "isModalContainer": false,
                "skin": "slFbox0j9f841cc563e4e",
                "top": "80px",
                "width": "650px",
                "zIndex": 1
            }, {}, {});
            flxAddSubAlert.setDefaultUnit(kony.flex.DP);
            var flxAlertTopColor = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "10px",
                "id": "flxAlertTopColor",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0px",
                "isModalContainer": false,
                "skin": "sknFlxTopColor4A77A0",
                "top": "0dp",
                "width": "100%",
                "zIndex": 1
            }, {}, {});
            flxAlertTopColor.setDefaultUnit(kony.flex.DP);
            flxAlertTopColor.add();
            var flxPopupHeader = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                "clipBounds": true,
                "id": "flxPopupHeader",
                "isVisible": true,
                "layoutType": kony.flex.FLOW_VERTICAL,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0px",
                "width": "100%",
                "zIndex": 1
            }, {}, {});
            flxPopupHeader.setDefaultUnit(kony.flex.DP);
            var flxAlertClose = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "25px",
                "id": "flxAlertClose",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "isModalContainer": false,
                "right": "13dp",
                "skin": "sknCursor",
                "top": "15dp",
                "width": "25px",
                "zIndex": 20
            }, {}, {
                "hoverSkin": "sknCursor"
            });
            flxAlertClose.setDefaultUnit(kony.flex.DP);
            var fontIconImgCLose = new kony.ui.Label({
                "centerX": "51%",
                "centerY": "50.00%",
                "height": "15dp",
                "id": "fontIconImgCLose",
                "isVisible": true,
                "left": "17dp",
                "skin": "sknFontIconSearchCross16px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmAlertsManagement.lblAlertPreviewClose\")",
                "width": "15dp",
                "zIndex": 2
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxAlertClose.add(fontIconImgCLose);
            var flxheader = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "35dp",
                "id": "flxheader",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0dp",
                "width": "100%",
                "zIndex": 20
            }, {}, {});
            flxheader.setDefaultUnit(kony.flex.DP);
            var lblAddSubAlertHeader = new kony.ui.Label({
                "centerY": "50%",
                "id": "lblAddSubAlertHeader",
                "isVisible": true,
                "left": "20dp",
                "skin": "sknlblLatoBold35475f23px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmAlertsManagement.AddSubAlert\")",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxheader.add(lblAddSubAlertHeader);
            var flxAlertDetails = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "85dp",
                "id": "flxAlertDetails",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "20dp",
                "isModalContainer": false,
                "right": "20dp",
                "skin": "slFbox0afd6fda7b2514b",
                "top": "15dp",
                "width": "98%",
                "zIndex": 1
            }, {}, {});
            flxAlertDetails.setDefaultUnit(kony.flex.DP);
            var flxSubAlertName = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "85dp",
                "id": "flxSubAlertName",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0dp",
                "width": "200dp",
                "zIndex": 1
            }, {}, {});
            flxSubAlertName.setDefaultUnit(kony.flex.DP);
            var txtbxSubAlertName = new kony.ui.TextBox2({
                "autoCapitalize": constants.TEXTBOX_AUTO_CAPITALIZE_NONE,
                "focusSkin": "skntxtbxDetailsFocus14px",
                "height": "40px",
                "id": "txtbxSubAlertName",
                "isVisible": true,
                "keyBoardStyle": constants.TEXTBOX_KEY_BOARD_STYLE_DEFAULT,
                "left": "0dp",
                "maxTextLength": 50,
                "placeholder": "Sub Alert Name",
                "secureTextEntry": false,
                "skin": "skntbxLato35475f14px",
                "textInputMode": constants.TEXTBOX_INPUT_MODE_ANY,
                "top": "25dp",
                "width": "200dp",
                "zIndex": 1
            }, {
                "containerHeightMode": constants.TEXTBOX_FONT_METRICS_DRIVEN_HEIGHT,
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [4, 0, 0, 0],
                "paddingInPixel": false
            }, {
                "autoCorrect": false
            });
            var lblAlertName = new kony.ui.Label({
                "id": "lblAlertName",
                "isVisible": true,
                "left": "0dp",
                "skin": "sknlbl485c7514px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmAlertsManagement.AlertName\")",
                "top": "0dp",
                "width": "150dp",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var lblAlertNameCount = new kony.ui.Label({
                "id": "lblAlertNameCount",
                "isVisible": false,
                "right": "0px",
                "skin": "sknlbl485c7514px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmGroups.lblGroupNameCount\")",
                "top": "0dp",
                "width": "40dp",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_CENTER,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var flxErrorAlertName = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "15dp",
                "id": "flxErrorAlertName",
                "isVisible": false,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "70dp",
                "width": "100%",
                "zIndex": 1
            }, {}, {});
            flxErrorAlertName.setDefaultUnit(kony.flex.DP);
            var lblErrorAlertNameIcon = new kony.ui.Label({
                "height": "15dp",
                "id": "lblErrorAlertNameIcon",
                "isVisible": true,
                "left": "0dp",
                "skin": "sknErrorIcon",
                "text": "",
                "top": "0dp",
                "width": "15dp",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var lblErrorAlertName = new kony.ui.Label({
                "height": "15dp",
                "id": "lblErrorAlertName",
                "isVisible": true,
                "left": "15dp",
                "skin": "sknlblError",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmAlertsManagement.AlertNameCannotBeEmpty\")",
                "top": "0dp",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxErrorAlertName.add(lblErrorAlertNameIcon, lblErrorAlertName);
            var flxAlertNameAvailable = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "bottom": "2px",
                "clipBounds": true,
                "height": "15dp",
                "id": "flxAlertNameAvailable",
                "isVisible": false,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "70dp",
                "width": "100%",
                "zIndex": 1
            }, {}, {});
            flxAlertNameAvailable.setDefaultUnit(kony.flex.DP);
            var lblAlertNameAvailable = new kony.ui.Label({
                "height": "15dp",
                "id": "lblAlertNameAvailable",
                "isVisible": true,
                "left": "0dp",
                "skin": "sknlblError",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmAlertsManagement.AlertNameAvailable\")",
                "top": "0dp",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxAlertNameAvailable.add(lblAlertNameAvailable);
            flxSubAlertName.add(txtbxSubAlertName, lblAlertName, lblAlertNameCount, flxErrorAlertName, flxAlertNameAvailable);
            var flxSubAlertCodeListBox = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                "clipBounds": false,
                "id": "flxSubAlertCodeListBox",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "217dp",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0dp",
                "width": "200dp",
                "zIndex": 1
            }, {}, {});
            flxSubAlertCodeListBox.setDefaultUnit(kony.flex.DP);
            var lblSubAlertCodeKey = new kony.ui.Label({
                "id": "lblSubAlertCodeKey",
                "isVisible": true,
                "left": "0dp",
                "skin": "sknlblLatoRegular484b5213px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmAlertsManagement.AlertCode\")",
                "top": "0dp",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var lstbxSubAlertCode = new kony.ui.ListBox({
                "focusSkin": "defListBoxFocus",
                "height": "40dp",
                "id": "lstbxSubAlertCode",
                "isVisible": true,
                "left": "0dp",
                "masterData": [
                    ["lb1", "select alert code"],
                    ["lb2", "Alert_Code_1"],
                    ["lb3", "Alert_Code_2"],
                    ["lb4", "Alert_Code_3"]
                ],
                "selectedKey": "lb1",
                "skin": "sknlbxBgffffffBorderc1c9ceRadius3Px",
                "top": "25dp",
                "width": "200dp",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [3, 0, 0, 0],
                "paddingInPixel": false
            }, {
                "multiSelect": false
            });
            var flxErrorAlertCode = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "15dp",
                "id": "flxErrorAlertCode",
                "isVisible": false,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "70dp",
                "width": "100%",
                "zIndex": 1
            }, {}, {});
            flxErrorAlertCode.setDefaultUnit(kony.flex.DP);
            var lblErrorAlertCodeIcon = new kony.ui.Label({
                "height": "15dp",
                "id": "lblErrorAlertCodeIcon",
                "isVisible": true,
                "left": "0dp",
                "skin": "sknErrorIcon",
                "text": "",
                "top": "0dp",
                "width": "15dp",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var lblErrorAlertCode = new kony.ui.Label({
                "height": "15dp",
                "id": "lblErrorAlertCode",
                "isVisible": true,
                "left": "15dp",
                "skin": "sknlblError",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmAlertsManagement.SelectAlertCode\")",
                "top": "0dp",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxErrorAlertCode.add(lblErrorAlertCodeIcon, lblErrorAlertCode);
            flxSubAlertCodeListBox.add(lblSubAlertCodeKey, lstbxSubAlertCode, flxErrorAlertCode);
            var flxSubAlertCodeGrey = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "40dp",
                "id": "flxSubAlertCodeGrey",
                "isVisible": false,
                "layoutType": kony.flex.FREE_FORM,
                "left": 217,
                "isModalContainer": false,
                "skin": "sknTbxDisabledf3f3f3",
                "top": "25dp",
                "width": "200dp",
                "zIndex": 1
            }, {}, {
                "hoverSkin": "sknCursorDisabled"
            });
            flxSubAlertCodeGrey.setDefaultUnit(kony.flex.DP);
            flxSubAlertCodeGrey.add();
            var flxAlertStatus = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "85px",
                "id": "flxAlertStatus",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "445dp",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0dp",
                "width": "100dp"
            }, {}, {});
            flxAlertStatus.setDefaultUnit(kony.flex.DP);
            var lblAlertStatus = new kony.ui.Label({
                "id": "lblAlertStatus",
                "isVisible": true,
                "left": "0dp",
                "skin": "sknlbl485c7514px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmAlertsManagement.Status\")",
                "top": "0dp",
                "width": "150dp",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var lblAlertStatusActive = new kony.ui.Label({
                "id": "lblAlertStatusActive",
                "isVisible": true,
                "left": "0dp",
                "skin": "sknlbl485c7514px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmAlertsManagement.Active\")",
                "top": "37dp",
                "width": "38dp",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var flxAlertStatusIcon = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "25px",
                "id": "flxAlertStatusIcon",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "48px",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "35px",
                "width": "38px",
                "zIndex": 1
            }, {}, {});
            flxAlertStatusIcon.setDefaultUnit(kony.flex.DP);
            var AlertStatusSwitch = new com.adminConsole.common.customSwitch({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "height": "25dp",
                "id": "AlertStatusSwitch",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0px",
                "masterType": constants.MASTER_TYPE_DEFAULT,
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0px",
                "width": "100%",
                "overrides": {
                    "customSwitch": {
                        "height": "25dp",
                        "isVisible": true,
                        "left": "0px",
                        "top": "0px",
                        "width": "100%"
                    },
                    "switchToggle": {
                        "isVisible": true
                    }
                }
            }, {
                "overrides": {}
            }, {
                "overrides": {}
            });
            flxAlertStatusIcon.add(AlertStatusSwitch);
            flxAlertStatus.add(lblAlertStatus, lblAlertStatusActive, flxAlertStatusIcon);
            flxAlertDetails.add(flxSubAlertName, flxSubAlertCodeListBox, flxSubAlertCodeGrey, flxAlertStatus);
            var flxTextArea = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "bottom": "10dp",
                "clipBounds": true,
                "height": "200dp",
                "id": "flxTextArea",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "20dp",
                "isModalContainer": false,
                "right": "35dp",
                "skin": "slFbox",
                "top": "10dp",
                "width": "93%",
                "zIndex": 1
            }, {}, {});
            flxTextArea.setDefaultUnit(kony.flex.DP);
            var lblAlertDescription = new kony.ui.Label({
                "id": "lblAlertDescription",
                "isVisible": true,
                "left": "0px",
                "skin": "sknlbl485c7514px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmAlertsManagement.AlertDesc\")",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var lblAlertDescriptionSize = new kony.ui.Label({
                "id": "lblAlertDescriptionSize",
                "isVisible": false,
                "right": "0px",
                "skin": "sknlbl485c7514px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmCSR.lblCharCount2\")",
                "top": "0px",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var txtAlertDescription = new kony.ui.TextArea2({
                "autoCapitalize": constants.TEXTAREA_AUTO_CAPITALIZE_NONE,
                "focusSkin": "skntxtAreaLato9ca9ba12Px",
                "height": "170dp",
                "id": "txtAlertDescription",
                "isVisible": true,
                "keyBoardStyle": constants.TEXTAREA_KEY_BOARD_STYLE_DEFAULT,
                "left": "0dp",
                "maxTextLength": 200,
                "numberOfVisibleLines": 3,
                "right": "0dp",
                "skin": "skntxtAreaLato35475f14Px",
                "textInputMode": constants.TEXTAREA_INPUT_MODE_ANY,
                "top": "30dp",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_TOP_LEFT,
                "padding": [2, 2, 2, 2],
                "paddingInPixel": false
            }, {
                "autoCorrect": false
            });
            flxTextArea.add(lblAlertDescription, lblAlertDescriptionSize, txtAlertDescription);
            var flxNoDescriptionError = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "bottom": "5dp",
                "clipBounds": true,
                "height": "15px",
                "id": "flxNoDescriptionError",
                "isVisible": false,
                "layoutType": kony.flex.FREE_FORM,
                "left": "20px",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "5px",
                "width": "350px",
                "zIndex": 1
            }, {}, {});
            flxNoDescriptionError.setDefaultUnit(kony.flex.DP);
            var lblNoDescriptionErrorIcon = new kony.ui.Label({
                "height": "15px",
                "id": "lblNoDescriptionErrorIcon",
                "isVisible": true,
                "left": "0px",
                "skin": "sknErrorIcon",
                "text": "",
                "top": "0px",
                "width": "15px",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var lblNoDescriptionError = new kony.ui.Label({
                "centerY": "50%",
                "height": "15px",
                "id": "lblNoDescriptionError",
                "isVisible": true,
                "left": "15px",
                "skin": "sknlblError",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmAlertsManagement.AlertDescCannotBeEmpty\")",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxNoDescriptionError.add(lblNoDescriptionErrorIcon, lblNoDescriptionError);
            flxPopupHeader.add(flxAlertClose, flxheader, flxAlertDetails, flxTextArea, flxNoDescriptionError);
            var flxEditButtons = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "80px",
                "id": "flxEditButtons",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "isModalContainer": false,
                "skin": "sknFooter",
                "top": "0px",
                "width": "100%",
                "zIndex": 1
            }, {}, {});
            flxEditButtons.setDefaultUnit(kony.flex.DP);
            var flxError = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "centerY": "50%",
                "clipBounds": true,
                "height": "80px",
                "id": "flxError",
                "isVisible": false,
                "layoutType": kony.flex.FREE_FORM,
                "left": "10dp",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0dp",
                "width": "50%",
                "zIndex": 1
            }, {}, {});
            flxError.setDefaultUnit(kony.flex.DP);
            var lblErrorMsg = new kony.ui.Label({
                "centerY": "50%",
                "id": "lblErrorMsg",
                "isVisible": true,
                "left": "40dp",
                "skin": "sknlblError",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmPolicies.lblErrorMsg\")",
                "top": "0dp",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var imgError = new kony.ui.Image2({
                "centerY": "50%",
                "height": "15dp",
                "id": "imgError",
                "isVisible": true,
                "left": "15dp",
                "skin": "slImage",
                "src": "error_1x.png",
                "top": "0dp",
                "width": "15dp",
                "zIndex": 1
            }, {
                "imageScaleMode": constants.IMAGE_SCALE_MODE_MAINTAIN_ASPECT_RATIO,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxError.add(lblErrorMsg, imgError);
            var btnCancel = new kony.ui.Button({
                "centerY": "50%",
                "focusSkin": "sknBtnLatoRegulara5abc413pxKA",
                "height": "40px",
                "id": "btnCancel",
                "isVisible": true,
                "onClick": controller.AS_Button_fe48adad632c43c5b97a05155a450331,
                "right": "155px",
                "skin": "sknBtnLatoRegulara5abc413pxKA",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmCustomers.CANCEL\")",
                "top": "0%",
                "width": "120px",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_CENTER,
                "displayText": true,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {
                "hoverSkin": "sknbtnffffffLatoBolda5abc413Pxd7d8e01pxBorderRadius20px"
            });
            var btnsave = new kony.ui.Button({
                "centerY": "50%",
                "focusSkin": "sknBtn003E75LatoRegular13pxFFFFFFRad20px",
                "height": "40dp",
                "id": "btnsave",
                "isVisible": true,
                "onClick": controller.AS_Button_c973a89c85624425bb033af39fa13bd2,
                "right": "20px",
                "skin": "sknBtn003E75LatoRegular13pxFFFFFFRad20px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.DragBox.ADD\")",
                "top": "0%",
                "width": "120dp",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_CENTER,
                "displayText": true,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {
                "hoverSkin": "sknBtn005198LatoRegular13pxFFFFFFRad20px"
            });
            flxEditButtons.add(flxError, btnCancel, btnsave);
            flxAddSubAlert.add(flxAlertTopColor, flxPopupHeader, flxEditButtons);
            flxAddSubAlertPopUp.add(flxAddSubAlert);
            var flxPreviewPopup = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "100%",
                "id": "flxPreviewPopup",
                "isVisible": false,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "masterType": constants.MASTER_TYPE_USERWIDGET,
                "isModalContainer": false,
                "skin": "sknFlxBg222B35Op50PopupBg",
                "top": "0dp",
                "width": "100%",
                "zIndex": 10
            }, {}, {});
            flxPreviewPopup.setDefaultUnit(kony.flex.DP);
            var flxPopUp = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "centerX": "50%",
                "clipBounds": false,
                "height": "470dp",
                "id": "flxPopUp",
                "isVisible": true,
                "layoutType": kony.flex.FLOW_VERTICAL,
                "isModalContainer": false,
                "skin": "slFbox0j9f841cc563e4e",
                "top": "120px",
                "width": "800px",
                "zIndex": 10
            }, {}, {});
            flxPopUp.setDefaultUnit(kony.flex.DP);
            var flxPopUpTopColor = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "10px",
                "id": "flxPopUpTopColor",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "sknFlxTopColor4A77A0",
                "top": "0dp",
                "width": "100%",
                "zIndex": 1
            }, {}, {});
            flxPopUpTopColor.setDefaultUnit(kony.flex.DP);
            flxPopUpTopColor.add();
            var flxPreviewPopupHeader = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                "clipBounds": true,
                "id": "flxPreviewPopupHeader",
                "isVisible": true,
                "layoutType": kony.flex.FLOW_VERTICAL,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "slFbox0d9c3974835234d",
                "top": "0px",
                "width": "100%",
                "zIndex": 1
            }, {}, {});
            flxPreviewPopupHeader.setDefaultUnit(kony.flex.DP);
            var flxPopUpClose = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "25px",
                "id": "flxPopUpClose",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "isModalContainer": false,
                "right": 15,
                "skin": "slFbox",
                "top": "15dp",
                "width": "25px",
                "zIndex": 20
            }, {}, {
                "hoverSkin": "sknCursor"
            });
            flxPopUpClose.setDefaultUnit(kony.flex.DP);
            var lblPopupClose = new kony.ui.Label({
                "centerX": "50%",
                "centerY": "50%",
                "height": "20dp",
                "id": "lblPopupClose",
                "isVisible": true,
                "left": "0dp",
                "skin": "sknIcon18pxGray",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.userwidgetmodel.lblClearSearch\")",
                "width": "20dp",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxPopUpClose.add(lblPopupClose);
            var lblPopUpHeader = new kony.ui.Label({
                "id": "lblPopUpHeader",
                "isVisible": true,
                "left": "20px",
                "skin": "sknlblLatoBold35475f23px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmAlertsManagement.PreviewMode\")",
                "top": "0px",
                "width": "80%",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxPreviewPopupHeader.add(flxPopUpClose, lblPopUpHeader);
            var flxProductsTabs = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "50px",
                "id": "flxProductsTabs",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0px",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "30px",
                "width": "100%",
                "zIndex": 1
            }, {}, {});
            flxProductsTabs.setDefaultUnit(kony.flex.DP);
            var flxProductsTabsWrapper = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "50px",
                "id": "flxProductsTabsWrapper",
                "isVisible": true,
                "layoutType": kony.flex.FLOW_HORIZONTAL,
                "left": "0px",
                "isModalContainer": false,
                "right": "0px",
                "skin": "sknBackgroundFFFFFF",
                "top": "0dp",
                "zIndex": 1
            }, {}, {});
            flxProductsTabsWrapper.setDefaultUnit(kony.flex.DP);
            var btnSMS = new kony.ui.Button({
                "height": "37px",
                "id": "btnSMS",
                "isVisible": true,
                "left": "20px",
                "skin": "sknbtnBgffffffLato485c75Radius3Px12Px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmAlertsManagement.SMSCaps\")",
                "top": "13px",
                "width": "60dp",
                "zIndex": 2
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_CENTER,
                "displayText": true,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var btnPushNoti = new kony.ui.Button({
                "height": "37px",
                "id": "btnPushNoti",
                "isVisible": true,
                "left": "10px",
                "skin": "sknBtnBgD2D7E2Rou3pxLato485B7512px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmAlertsManagement.PUSHCaps\")",
                "top": "13px",
                "width": "70dp",
                "zIndex": 2
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_CENTER,
                "displayText": true,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var btnEmail = new kony.ui.Button({
                "height": "37px",
                "id": "btnEmail",
                "isVisible": true,
                "left": "10px",
                "skin": "sknBtnBgD2D7E2Rou3pxLato485B7512px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmAlertsManagement.EMAILCaps\")",
                "top": "13px",
                "width": "70dp",
                "zIndex": 2
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_CENTER,
                "displayText": true,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var btnNotifCenter = new kony.ui.Button({
                "height": "37px",
                "id": "btnNotifCenter",
                "isVisible": true,
                "left": "10px",
                "skin": "sknBtnBgD2D7E2Rou3pxLato485B7512px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmAlertsManagement.NOTIFCENTERCaps\")",
                "top": "13px",
                "width": "200dp",
                "zIndex": 2
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_CENTER,
                "displayText": true,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxProductsTabsWrapper.add(btnSMS, btnPushNoti, btnEmail, btnNotifCenter);
            var flxButtonsSeperator = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "bottom": "0dp",
                "clipBounds": true,
                "height": "1dp",
                "id": "flxButtonsSeperator",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "20dp",
                "isModalContainer": false,
                "right": "20dp",
                "skin": "sknflx115678",
                "zIndex": 1
            }, {}, {});
            flxButtonsSeperator.setDefaultUnit(kony.flex.DP);
            flxButtonsSeperator.add();
            flxProductsTabs.add(flxProductsTabsWrapper, flxButtonsSeperator);
            var flxTemplatePreviewHeader = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "68px",
                "id": "flxTemplatePreviewHeader",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0px",
                "isModalContainer": false,
                "skin": "slFbox0d9c3974835234d",
                "top": "0dp",
                "width": "100%",
                "zIndex": 1
            }, {}, {});
            flxTemplatePreviewHeader.setDefaultUnit(kony.flex.DP);
            var flxPreviewHeader = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "100%",
                "id": "flxPreviewHeader",
                "isVisible": true,
                "layoutType": kony.flex.FLOW_HORIZONTAL,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0dp",
                "width": "100%"
            }, {}, {});
            flxPreviewHeader.setDefaultUnit(kony.flex.DP);
            var lblPreviewSubHeader1 = new kony.ui.Label({
                "centerY": "50%",
                "id": "lblPreviewSubHeader1",
                "isVisible": true,
                "left": 35,
                "onTouchEnd": controller.AS_Label_b388eebaef3c4fe594f561b7cfbd6c59,
                "skin": "sknlbl485C75LatoSemiBold13px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmAlertsManagement.SummerDiscount\")",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var flxVerticalSeprator = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "centerY": "50%",
                "clipBounds": true,
                "height": 20,
                "id": "flxVerticalSeprator",
                "isVisible": false,
                "layoutType": kony.flex.FREE_FORM,
                "left": "15dp",
                "isModalContainer": false,
                "skin": "sknflxd6dbe7",
                "width": "1dp",
                "zIndex": 1
            }, {}, {});
            flxVerticalSeprator.setDefaultUnit(kony.flex.DP);
            flxVerticalSeprator.add();
            var lblPreviewSubHeader2 = new kony.ui.Label({
                "centerY": "50%",
                "id": "lblPreviewSubHeader2",
                "isVisible": false,
                "left": 15,
                "onTouchEnd": controller.AS_Label_b388eebaef3c4fe594f561b7cfbd6c59,
                "skin": "sknlbl485C75LatoSemiBold13px",
                "text": "12/12/2017",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxPreviewHeader.add(lblPreviewSubHeader1, flxVerticalSeprator, lblPreviewSubHeader2);
            var flxHeaderSeperator = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "bottom": "1dp",
                "clipBounds": true,
                "height": "1dp",
                "id": "flxHeaderSeperator",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "35dp",
                "isModalContainer": false,
                "right": "20dp",
                "skin": "sknflx115678",
                "zIndex": 1
            }, {}, {});
            flxHeaderSeperator.setDefaultUnit(kony.flex.DP);
            flxHeaderSeperator.add();
            flxTemplatePreviewHeader.add(flxPreviewHeader, flxHeaderSeperator);
            var flxTemplatePreviewBody = new kony.ui.FlexScrollContainer({
                "allowHorizontalBounce": false,
                "allowVerticalBounce": true,
                "bottom": 20,
                "bounces": true,
                "clipBounds": true,
                "enableScrolling": true,
                "height": "150dp",
                "horizontalScrollIndicator": true,
                "id": "flxTemplatePreviewBody",
                "isVisible": true,
                "layoutType": kony.flex.FLOW_VERTICAL,
                "left": "35dp",
                "pagingEnabled": false,
                "scrollDirection": kony.flex.SCROLL_VERTICAL,
                "skin": "slFSbox",
                "top": "0dp",
                "verticalScrollIndicator": true,
                "width": "92%"
            }, {}, {});
            flxTemplatePreviewBody.setDefaultUnit(kony.flex.DP);
            var lblPreviewTemplateBody = new kony.ui.Label({
                "id": "lblPreviewTemplateBody",
                "isVisible": true,
                "left": "0dp",
                "skin": "sknlbl485C75LatoSemiBold13px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmAlertsManagement.TitleGetCredited\")",
                "top": "30dp",
                "width": "730dp",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var rtxViewer = new kony.ui.Browser({
                "detectTelNumber": true,
                "enableZoom": false,
                "height": "100%",
                "id": "rtxViewer",
                "isVisible": false,
                "left": "0px",
                "setAsContent": false,
                "requestURLConfig": {
                    "URL": "richtextViewer.html",
                    "requestMethod": constants.BROWSER_REQUEST_METHOD_GET
                },
                "top": "0px",
                "width": "100%",
                "zIndex": 1
            }, {}, {});
            flxTemplatePreviewBody.add(lblPreviewTemplateBody, rtxViewer);
            var flxPopUpButtons = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "80px",
                "id": "flxPopUpButtons",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0%",
                "isModalContainer": false,
                "skin": "slFbox0dc900c4572aa40",
                "top": "3px",
                "width": "100%",
                "zIndex": 1
            }, {}, {});
            flxPopUpButtons.setDefaultUnit(kony.flex.DP);
            var btnPopUpCancel = new kony.ui.Button({
                "centerY": "50%",
                "focusSkin": "sknBtnLatoRegulara5abc413pxKA",
                "height": "40px",
                "id": "btnPopUpCancel",
                "isVisible": true,
                "right": "20px",
                "skin": "sknbtnf7f7faLatoRegular4f555dBorder1px8b96a5",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmAssistedOnboarding.close\")",
                "top": "0%",
                "width": "100px",
                "zIndex": 20
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_CENTER,
                "displayText": true,
                "padding": [3, 0, 3, 0],
                "paddingInPixel": false
            }, {
                "hoverSkin": "sknBtnLatoRegular8b96a513pxKAhover"
            });
            flxPopUpButtons.add(btnPopUpCancel);
            flxPopUp.add(flxPopUpTopColor, flxPreviewPopupHeader, flxProductsTabs, flxTemplatePreviewHeader, flxTemplatePreviewBody, flxPopUpButtons);
            flxPreviewPopup.add(flxPopUp);
            var flxSequencePopUp = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "100%",
                "id": "flxSequencePopUp",
                "isVisible": false,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "sknFlxBg222B35Op50PopupBg",
                "top": "0dp",
                "width": "100%",
                "zIndex": 5
            }, {}, {});
            flxSequencePopUp.setDefaultUnit(kony.flex.DP);
            var flxSequenceContainer = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                "centerX": "50%",
                "centerY": "50%",
                "clipBounds": true,
                "id": "flxSequenceContainer",
                "isVisible": true,
                "layoutType": kony.flex.FLOW_VERTICAL,
                "left": "27%",
                "isModalContainer": false,
                "skin": "slFbox0j9f841cc563e4e",
                "width": "380px",
                "zIndex": 1
            }, {}, {});
            flxSequenceContainer.setDefaultUnit(kony.flex.DP);
            var flxTopVerticalColor = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "10px",
                "id": "flxTopVerticalColor",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0px",
                "isModalContainer": false,
                "skin": "sknFlxTopColor4A77A0",
                "top": "0dp",
                "width": "100%",
                "zIndex": 1
            }, {}, {});
            flxTopVerticalColor.setDefaultUnit(kony.flex.DP);
            flxTopVerticalColor.add();
            var flxSequenceBody = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                "clipBounds": true,
                "id": "flxSequenceBody",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0px",
                "width": "100%",
                "zIndex": 1
            }, {}, {});
            flxSequenceBody.setDefaultUnit(kony.flex.DP);
            var flxReorderAlertHeader = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "55dp",
                "id": "flxReorderAlertHeader",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0dp",
                "width": "100%",
                "zIndex": 20
            }, {}, {});
            flxReorderAlertHeader.setDefaultUnit(kony.flex.DP);
            var lblReorderAlert = new kony.ui.Label({
                "id": "lblReorderAlert",
                "isVisible": true,
                "left": "20dp",
                "skin": "sknLbl192b45LatoReg16px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmAlertsManagement.ReorderAlertGroups\")",
                "top": "30dp",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var flxPopupCloseSequence = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "20px",
                "id": "flxPopupCloseSequence",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "isModalContainer": false,
                "right": "13dp",
                "skin": "sknCursor",
                "top": "10dp",
                "width": "20px",
                "zIndex": 20
            }, {}, {
                "hoverSkin": "sknCursor"
            });
            flxPopupCloseSequence.setDefaultUnit(kony.flex.DP);
            var lblIconCloseSeqPopUp = new kony.ui.Label({
                "centerX": "51%",
                "height": "15dp",
                "id": "lblIconCloseSeqPopUp",
                "isVisible": true,
                "left": "17dp",
                "skin": "sknFontIconSearchCross16px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmAlertsManagement.lblAlertPreviewClose\")",
                "top": "0dp",
                "width": "15dp",
                "zIndex": 2
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxPopupCloseSequence.add(lblIconCloseSeqPopUp);
            flxReorderAlertHeader.add(lblReorderAlert, flxPopupCloseSequence);
            var flxSubHeaderSequencePopUp = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "25dp",
                "id": "flxSubHeaderSequencePopUp",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "20dp",
                "isModalContainer": false,
                "right": "20dp",
                "skin": "slFbox0afd6fda7b2514b",
                "top": "55dp",
                "zIndex": 1
            }, {}, {});
            flxSubHeaderSequencePopUp.setDefaultUnit(kony.flex.DP);
            var lblSubHeaderSequencePopUp = new kony.ui.Label({
                "centerY": "50%",
                "id": "lblSubHeaderSequencePopUp",
                "isVisible": true,
                "left": "0dp",
                "skin": "sknLbl485C75LatoRegular13Px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmAlertsManagement.ReorderAlertGroup\")",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxSubHeaderSequencePopUp.add(lblSubHeaderSequencePopUp);
            var flxSequenceListSegment = new kony.ui.FlexScrollContainer({
                "allowHorizontalBounce": false,
                "allowVerticalBounce": true,
                "bottom": "20dp",
                "bounces": true,
                "clipBounds": true,
                "enableScrolling": true,
                "height": "250dp",
                "horizontalScrollIndicator": true,
                "id": "flxSequenceListSegment",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "20dp",
                "pagingEnabled": false,
                "right": 15,
                "scrollDirection": kony.flex.SCROLL_VERTICAL,
                "skin": "sknFlxffffffbordere1e5ed",
                "top": "90dp",
                "verticalScrollIndicator": true,
                "zIndex": 1
            }, {}, {});
            flxSequenceListSegment.setDefaultUnit(kony.flex.DP);
            var segSequenceList = new kony.ui.SegmentedUI2({
                "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                "groupCells": false,
                "id": "segSequenceList",
                "isVisible": true,
                "left": "0dp",
                "needPageIndicator": true,
                "pageOffDotImage": "pageoffdot.png",
                "pageOnDotImage": "pageondot.png",
                "retainSelection": false,
                "right": "59dp",
                "rowFocusSkin": "seg2Focus",
                "rowSkin": "seg2Normal",
                "sectionHeaderSkin": "sliPhoneSegmentHeader",
                "selectionBehavior": constants.SEGUI_DEFAULT_BEHAVIOR,
                "separatorColor": "e1e5ed00",
                "separatorRequired": true,
                "separatorThickness": 1,
                "showScrollbars": false,
                "top": "0dp",
                "viewType": constants.SEGUI_VIEW_TYPE_TABLEVIEW,
                "widgetDataMap": {},
                "zIndex": 1
            }, {
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxSequenceListSegment.add(segSequenceList);
            var flxAlertSequenceIcons = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "248dp",
                "id": "flxAlertSequenceIcons",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "isModalContainer": false,
                "right": "20dp",
                "skin": "sknFlxFFFFFF1000",
                "top": "91dp",
                "width": "60dp",
                "zIndex": 1
            }, {}, {});
            flxAlertSequenceIcons.setDefaultUnit(kony.flex.DP);
            var lblVerticalSep = new kony.ui.Label({
                "height": "250dp",
                "id": "lblVerticalSep",
                "isVisible": true,
                "left": "0dp",
                "skin": "sknSeparator",
                "top": "0dp",
                "width": "1dp",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var flxArrowTopMostPosition = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "60dp",
                "id": "flxArrowTopMostPosition",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0dp",
                "width": "60dp",
                "zIndex": 1
            }, {}, {
                "hoverSkin": "sknCursor"
            });
            flxArrowTopMostPosition.setDefaultUnit(kony.flex.DP);
            var lblArrowTopMostPosition = new kony.ui.Label({
                "centerX": "50%",
                "centerY": "50.00%",
                "id": "lblArrowTopMostPosition",
                "isVisible": true,
                "skin": "sknIcon20px",
                "text": "",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 2
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var lblSeperator1 = new kony.ui.Label({
                "bottom": "0dp",
                "height": "1dp",
                "id": "lblSeperator1",
                "isVisible": true,
                "left": "5dp",
                "right": "5dp",
                "skin": "sknSeparator",
                "zIndex": 2
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxArrowTopMostPosition.add(lblArrowTopMostPosition, lblSeperator1);
            var flxArrowTopPosition = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "60dp",
                "id": "flxArrowTopPosition",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "60dp",
                "width": "60dp",
                "zIndex": 1
            }, {}, {
                "hoverSkin": "sknCursor"
            });
            flxArrowTopPosition.setDefaultUnit(kony.flex.DP);
            var lblArrowTopPosition = new kony.ui.Label({
                "centerX": "50%",
                "centerY": "50.00%",
                "id": "lblArrowTopPosition",
                "isVisible": true,
                "skin": "sknIcon00000014px",
                "text": "",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 2
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var lblSeperator2 = new kony.ui.Label({
                "bottom": "0dp",
                "height": "1dp",
                "id": "lblSeperator2",
                "isVisible": true,
                "left": "5dp",
                "right": "5dp",
                "skin": "sknSeparator",
                "zIndex": 2
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxArrowTopPosition.add(lblArrowTopPosition, lblSeperator2);
            var flxArrowBottomPosition = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "60dp",
                "id": "flxArrowBottomPosition",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "120dp",
                "width": "60dp",
                "zIndex": 1
            }, {}, {
                "hoverSkin": "sknCursor"
            });
            flxArrowBottomPosition.setDefaultUnit(kony.flex.DP);
            var lblArrowBottomPosition = new kony.ui.Label({
                "centerX": "50%",
                "centerY": "50.00%",
                "id": "lblArrowBottomPosition",
                "isVisible": true,
                "skin": "sknIcon00000014px",
                "text": "",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 2
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var lblSeperator3 = new kony.ui.Label({
                "bottom": "0dp",
                "height": "1dp",
                "id": "lblSeperator3",
                "isVisible": true,
                "left": "5dp",
                "right": "5dp",
                "skin": "sknSeparator",
                "zIndex": 2
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxArrowBottomPosition.add(lblArrowBottomPosition, lblSeperator3);
            var flxArrowBottomMostPosition = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "60dp",
                "id": "flxArrowBottomMostPosition",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "180dp",
                "width": "60dp",
                "zIndex": 1
            }, {}, {
                "hoverSkin": "sknCursor"
            });
            flxArrowBottomMostPosition.setDefaultUnit(kony.flex.DP);
            var lblArrowBottomMostPosition = new kony.ui.Label({
                "centerX": "50%",
                "centerY": "50.00%",
                "id": "lblArrowBottomMostPosition",
                "isVisible": true,
                "skin": "sknIcon20px",
                "text": "",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 2
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxArrowBottomMostPosition.add(lblArrowBottomMostPosition);
            flxAlertSequenceIcons.add(lblVerticalSep, flxArrowTopMostPosition, flxArrowTopPosition, flxArrowBottomPosition, flxArrowBottomMostPosition);
            flxSequenceBody.add(flxReorderAlertHeader, flxSubHeaderSequencePopUp, flxSequenceListSegment, flxAlertSequenceIcons);
            var flxSequencePopUpButtons = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "80px",
                "id": "flxSequencePopUpButtons",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "isModalContainer": false,
                "skin": "sknFooter",
                "top": "0px",
                "width": "100%",
                "zIndex": 1
            }, {}, {});
            flxSequencePopUpButtons.setDefaultUnit(kony.flex.DP);
            var flxSequencePopUpError = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "centerY": "50%",
                "clipBounds": true,
                "height": "80px",
                "id": "flxSequencePopUpError",
                "isVisible": false,
                "layoutType": kony.flex.FREE_FORM,
                "left": "10dp",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0dp",
                "width": "50%",
                "zIndex": 1
            }, {}, {});
            flxSequencePopUpError.setDefaultUnit(kony.flex.DP);
            var lblErrorMessageSequencePopUp = new kony.ui.Label({
                "centerY": "50%",
                "id": "lblErrorMessageSequencePopUp",
                "isVisible": true,
                "left": "40dp",
                "skin": "sknlblError",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmPolicies.lblErrorMsg\")",
                "top": "0dp",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var imgErrorSequencePopUp = new kony.ui.Image2({
                "centerY": "50%",
                "height": "15dp",
                "id": "imgErrorSequencePopUp",
                "isVisible": true,
                "left": "15dp",
                "skin": "slImage",
                "src": "error_1x.png",
                "top": "0dp",
                "width": "15dp",
                "zIndex": 1
            }, {
                "imageScaleMode": constants.IMAGE_SCALE_MODE_MAINTAIN_ASPECT_RATIO,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxSequencePopUpError.add(lblErrorMessageSequencePopUp, imgErrorSequencePopUp);
            var btnCancelSequencePopup = new kony.ui.Button({
                "centerY": "50%",
                "height": "40px",
                "id": "btnCancelSequencePopup",
                "isVisible": true,
                "onClick": controller.AS_Button_fe48adad632c43c5b97a05155a450331,
                "right": "155px",
                "skin": "sknbtnf7f7faLatoRegular4f555dBorder1px8b96a5",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmCustomers.CANCEL\")",
                "top": "0%",
                "width": "120px",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_CENTER,
                "displayText": true,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {
                "hoverSkin": "sknbtnffffffLatoRegular4f555dBorder1px485c75"
            });
            var btnUpdateSequencePopup = new kony.ui.Button({
                "centerY": "50%",
                "focusSkin": "sknBtn003E75LatoRegular13pxFFFFFFRad20px",
                "height": "40dp",
                "id": "btnUpdateSequencePopup",
                "isVisible": true,
                "onClick": controller.AS_Button_c973a89c85624425bb033af39fa13bd2,
                "right": "20px",
                "skin": "sknBtn003E75LatoRegular13pxFFFFFFRad20px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmOutageMessageController.UPDATE\")",
                "top": "0%",
                "width": "120dp",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_CENTER,
                "displayText": true,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {
                "hoverSkin": "sknBtn005198LatoRegular13pxFFFFFFRad20px"
            });
            flxSequencePopUpButtons.add(flxSequencePopUpError, btnCancelSequencePopup, btnUpdateSequencePopup);
            flxSequenceContainer.add(flxTopVerticalColor, flxSequenceBody, flxSequencePopUpButtons);
            flxSequencePopUp.add(flxSequenceContainer);
            var flxViewAlertLanguagesPopup = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "100%",
                "id": "flxViewAlertLanguagesPopup",
                "isVisible": false,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "masterType": constants.MASTER_TYPE_USERWIDGET,
                "isModalContainer": false,
                "skin": "sknFlxBg222B35Op50PopupBg",
                "top": "0dp",
                "width": "100%",
                "zIndex": 10
            }, {}, {});
            flxViewAlertLanguagesPopup.setDefaultUnit(kony.flex.DP);
            var flxAllLanguagesContainer = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                "centerX": "50%",
                "centerY": "50%",
                "clipBounds": false,
                "id": "flxAllLanguagesContainer",
                "isVisible": true,
                "layoutType": kony.flex.FLOW_VERTICAL,
                "isModalContainer": false,
                "skin": "slFbox0j9f841cc563e4e",
                "width": "920px",
                "zIndex": 10
            }, {}, {});
            flxAllLanguagesContainer.setDefaultUnit(kony.flex.DP);
            var flxLanguagesPopupTopColor = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "10px",
                "id": "flxLanguagesPopupTopColor",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "sknFlxTopColor4A77A0",
                "top": "0dp",
                "width": "100%",
                "zIndex": 1
            }, {}, {});
            flxLanguagesPopupTopColor.setDefaultUnit(kony.flex.DP);
            flxLanguagesPopupTopColor.add();
            var flxLanguagesPopupHeader = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                "clipBounds": true,
                "id": "flxLanguagesPopupHeader",
                "isVisible": true,
                "layoutType": kony.flex.FLOW_VERTICAL,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "slFbox0d9c3974835234d",
                "top": "0px",
                "width": "100%",
                "zIndex": 1
            }, {}, {});
            flxLanguagesPopupHeader.setDefaultUnit(kony.flex.DP);
            var flxLanguagesPopupClose = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "20px",
                "id": "flxLanguagesPopupClose",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "isModalContainer": false,
                "right": 15,
                "skin": "slFbox",
                "top": "10dp",
                "width": "25px",
                "zIndex": 20
            }, {}, {
                "hoverSkin": "sknCursor"
            });
            flxLanguagesPopupClose.setDefaultUnit(kony.flex.DP);
            var lblIconLangaugesPopupClose = new kony.ui.Label({
                "centerX": "50%",
                "id": "lblIconLangaugesPopupClose",
                "isVisible": true,
                "left": "0dp",
                "skin": "sknIcon00000015px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.userwidgetmodel.lblClearSearch\")",
                "top": "0dp",
                "width": "20dp",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxLanguagesPopupClose.add(lblIconLangaugesPopupClose);
            var lblLanguagesPopupHeading = new kony.ui.Label({
                "id": "lblLanguagesPopupHeading",
                "isVisible": true,
                "left": "20px",
                "skin": "sknLbl192b45LatoReg16px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmAlertsManagement.Supported_Language\")",
                "top": "0px",
                "width": "80%",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxLanguagesPopupHeader.add(flxLanguagesPopupClose, lblLanguagesPopupHeading);
            var flxLanguagesSegHeader = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "50px",
                "id": "flxLanguagesSegHeader",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0px",
                "isModalContainer": false,
                "skin": "slFbox0d9c3974835234d",
                "top": "30dp",
                "width": "100%",
                "zIndex": 1
            }, {}, {});
            flxLanguagesSegHeader.setDefaultUnit(kony.flex.DP);
            var flxLanguageHeaderSeperator = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "1dp",
                "id": "flxLanguageHeaderSeperator",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "20dp",
                "isModalContainer": false,
                "right": "20dp",
                "skin": "sknflxd5d9ddop100",
                "top": "0dp",
                "zIndex": 1
            }, {}, {});
            flxLanguageHeaderSeperator.setDefaultUnit(kony.flex.DP);
            flxLanguageHeaderSeperator.add();
            var lblLanguageHeader = new kony.ui.Label({
                "centerY": "50%",
                "id": "lblLanguageHeader",
                "isVisible": true,
                "left": "30dp",
                "onTouchEnd": controller.AS_Label_b388eebaef3c4fe594f561b7cfbd6c59,
                "skin": "sknlblLato696c7311px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmAlertsManagement.Language_UC\")",
                "width": "20%",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var lblAlertDisplayNameHeader = new kony.ui.Label({
                "centerY": "50%",
                "id": "lblAlertDisplayNameHeader",
                "isVisible": true,
                "left": "24%",
                "onTouchEnd": controller.AS_Label_b388eebaef3c4fe594f561b7cfbd6c59,
                "skin": "sknlblLato696c7311px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.View.DISPLAYNAME\")",
                "width": "20%",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var lblAlertDisplayDescHeader = new kony.ui.Label({
                "centerY": "50%",
                "id": "lblAlertDisplayDescHeader",
                "isVisible": true,
                "left": "48%",
                "onTouchEnd": controller.AS_Label_b388eebaef3c4fe594f561b7cfbd6c59,
                "skin": "sknlblLato696c7311px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmAlertsManagement.CategoryDescriptionCaps\")",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var flxLanguageSegHeaderSep = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "bottom": "1dp",
                "clipBounds": true,
                "height": "1dp",
                "id": "flxLanguageSegHeaderSep",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "20dp",
                "isModalContainer": false,
                "right": "20dp",
                "skin": "sknFlxBg696C73Op100",
                "zIndex": 1
            }, {}, {});
            flxLanguageSegHeaderSep.setDefaultUnit(kony.flex.DP);
            flxLanguageSegHeaderSep.add();
            flxLanguagesSegHeader.add(flxLanguageHeaderSeperator, lblLanguageHeader, lblAlertDisplayNameHeader, lblAlertDisplayDescHeader, flxLanguageSegHeaderSep);
            var flxLanguagesSegmentContainer = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                "bottom": "20dp",
                "clipBounds": true,
                "id": "flxLanguagesSegmentContainer",
                "isVisible": true,
                "layoutType": kony.flex.FLOW_VERTICAL,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0dp",
                "width": "100%"
            }, {}, {});
            flxLanguagesSegmentContainer.setDefaultUnit(kony.flex.DP);
            var segLanguagesPopup = new kony.ui.SegmentedUI2({
                "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                "data": [{
                    "lblAlertDisplayDesc": "Desc",
                    "lblAlertDisplayName": "Name",
                    "lblLanguage": "Language",
                    "lblSeperator": "."
                }, {
                    "lblAlertDisplayDesc": "Desc",
                    "lblAlertDisplayName": "Name",
                    "lblLanguage": "Language",
                    "lblSeperator": "."
                }],
                "groupCells": false,
                "id": "segLanguagesPopup",
                "isVisible": true,
                "left": "0dp",
                "needPageIndicator": true,
                "pageOffDotImage": "pageoffdot.png",
                "pageOnDotImage": "pageondot.png",
                "retainSelection": false,
                "rowFocusSkin": "seg2Focus",
                "rowSkin": "seg2Normal",
                "rowTemplate": "flxViewAllLanguagePopup",
                "sectionHeaderSkin": "sliPhoneSegmentHeader",
                "selectionBehavior": constants.SEGUI_DEFAULT_BEHAVIOR,
                "separatorRequired": false,
                "showScrollbars": false,
                "top": "0dp",
                "viewType": constants.SEGUI_VIEW_TYPE_TABLEVIEW,
                "widgetDataMap": {
                    "flxViewAllLanguagePopup": "flxViewAllLanguagePopup",
                    "lblAlertDisplayDesc": "lblAlertDisplayDesc",
                    "lblAlertDisplayName": "lblAlertDisplayName",
                    "lblLanguage": "lblLanguage",
                    "lblSeperator": "lblSeperator"
                },
                "width": "100%",
                "zIndex": 1
            }, {
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxLanguagesSegmentContainer.add(segLanguagesPopup);
            flxAllLanguagesContainer.add(flxLanguagesPopupTopColor, flxLanguagesPopupHeader, flxLanguagesSegHeader, flxLanguagesSegmentContainer);
            flxViewAlertLanguagesPopup.add(flxAllLanguagesContainer);
            var flxReassignGroupPopup = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "100%",
                "id": "flxReassignGroupPopup",
                "isVisible": false,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "masterType": constants.MASTER_TYPE_USERWIDGET,
                "isModalContainer": false,
                "skin": "sknFlxBg222B35Op50PopupBg",
                "top": "0dp",
                "width": "100%",
                "zIndex": 10
            }, {}, {});
            flxReassignGroupPopup.setDefaultUnit(kony.flex.DP);
            var flxReassignGroupCont = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                "centerX": "50%",
                "centerY": "50%",
                "clipBounds": false,
                "id": "flxReassignGroupCont",
                "isVisible": true,
                "layoutType": kony.flex.FLOW_VERTICAL,
                "isModalContainer": false,
                "skin": "slFbox0j9f841cc563e4e",
                "width": "521px",
                "zIndex": 10
            }, {}, {});
            flxReassignGroupCont.setDefaultUnit(kony.flex.DP);
            var flxReassignGroupHeaderLine = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "10px",
                "id": "flxReassignGroupHeaderLine",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "sknFlxTopColor4A77A0",
                "top": "0dp",
                "width": "100%",
                "zIndex": 1
            }, {}, {});
            flxReassignGroupHeaderLine.setDefaultUnit(kony.flex.DP);
            flxReassignGroupHeaderLine.add();
            var flxReassignGroupPopupHeading = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                "clipBounds": true,
                "id": "flxReassignGroupPopupHeading",
                "isVisible": true,
                "layoutType": kony.flex.FLOW_VERTICAL,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "slFbox0d9c3974835234d",
                "top": "0px",
                "width": "100%",
                "zIndex": 1
            }, {}, {});
            flxReassignGroupPopupHeading.setDefaultUnit(kony.flex.DP);
            var flxReassignGroupClose = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "25px",
                "id": "flxReassignGroupClose",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "isModalContainer": false,
                "right": 15,
                "skin": "slFbox",
                "top": "8dp",
                "width": "25px",
                "zIndex": 20
            }, {}, {
                "hoverSkin": "sknCursor"
            });
            flxReassignGroupClose.setDefaultUnit(kony.flex.DP);
            var lblIconReassignGroupClose = new kony.ui.Label({
                "centerX": "50%",
                "centerY": "50%",
                "height": "20dp",
                "id": "lblIconReassignGroupClose",
                "isVisible": true,
                "left": "0dp",
                "skin": "sknIcon18pxGray",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.userwidgetmodel.lblClearSearch\")",
                "width": "20dp",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxReassignGroupClose.add(lblIconReassignGroupClose);
            var lblReassignGroupHeading = new kony.ui.Label({
                "id": "lblReassignGroupHeading",
                "isVisible": true,
                "left": "20px",
                "skin": "sknLbl192b45LatoReg16px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmAlertsManagement.MoveToAlertCategory\")",
                "top": "0px",
                "width": "80%",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxReassignGroupPopupHeading.add(flxReassignGroupClose, lblReassignGroupHeading);
            var flxReasignPopupBody = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                "bottom": "20dp",
                "clipBounds": true,
                "id": "flxReasignPopupBody",
                "isVisible": true,
                "layoutType": kony.flex.FLOW_VERTICAL,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "30dp",
                "width": "100%"
            }, {}, {});
            flxReasignPopupBody.setDefaultUnit(kony.flex.DP);
            var flxReassignGroupListCont = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "90dp",
                "id": "flxReassignGroupListCont",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "20dp",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0dp",
                "width": "350dp",
                "zIndex": 1
            }, {}, {});
            flxReassignGroupListCont.setDefaultUnit(kony.flex.DP);
            var lblReassignGroupListHeader = new kony.ui.Label({
                "id": "lblReassignGroupListHeader",
                "isVisible": true,
                "left": "0dp",
                "skin": "sknLbl485C75LatoRegular13Px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmAlertsManagement.SelectAnAlertCategoryToAssign\")",
                "top": "0dp",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var lstBoxReassignGroup = new kony.ui.ListBox({
                "focusSkin": "defListBoxFocus",
                "height": "40dp",
                "id": "lstBoxReassignGroup",
                "isVisible": true,
                "left": "0dp",
                "masterData": [
                    ["lb1", "Placeholder One"],
                    ["lb2", "Placeholder Two"],
                    ["lb3", "Placeholder Three"]
                ],
                "skin": "sknlbxBgffffffBorderc1c9ceRadius3Px",
                "top": "30dp",
                "width": "100%",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [3, 0, 0, 0],
                "paddingInPixel": false
            }, {
                "multiSelect": false
            });
            var flxReassignGroupError = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                "clipBounds": true,
                "id": "flxReassignGroupError",
                "isVisible": false,
                "layoutType": kony.flex.FLOW_HORIZONTAL,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "73dp",
                "width": "100%",
                "zIndex": 1
            }, {}, {});
            flxReassignGroupError.setDefaultUnit(kony.flex.DP);
            var lblIconReassignError = new kony.ui.Label({
                "id": "lblIconReassignError",
                "isVisible": true,
                "left": "0dp",
                "skin": "sknErrorIcon",
                "text": "",
                "top": "0dp",
                "width": "15dp",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var lblReassignErrorMsg = new kony.ui.Label({
                "id": "lblReassignErrorMsg",
                "isVisible": true,
                "left": "0dp",
                "skin": "sknlblError",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmAlertsManagement.PleaseSelectCategory\")",
                "top": "0dp",
                "width": "180dp",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxReassignGroupError.add(lblIconReassignError, lblReassignErrorMsg);
            flxReassignGroupListCont.add(lblReassignGroupListHeader, lstBoxReassignGroup, flxReassignGroupError);
            var lblReassignGroupNote = new kony.ui.Label({
                "id": "lblReassignGroupNote",
                "isVisible": true,
                "left": "20dp",
                "skin": "sknLbl485C75LatoRegular13Px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmAlertsManagement.ReassignGroupPopupMsg\")",
                "top": "30dp",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxReasignPopupBody.add(flxReassignGroupListCont, lblReassignGroupNote);
            var flxReasignGroupButtons = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "bottom": "0dp",
                "clipBounds": true,
                "height": "80dp",
                "id": "flxReasignGroupButtons",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "sknflxBgE4E6EC",
                "top": "0dp",
                "width": "100%"
            }, {}, {});
            flxReasignGroupButtons.setDefaultUnit(kony.flex.DP);
            var reassignPopupButtons = new com.adminConsole.common.commonButtons({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "centerY": "50%",
                "height": "80px",
                "id": "reassignPopupButtons",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "20dp",
                "masterType": constants.MASTER_TYPE_DEFAULT,
                "isModalContainer": false,
                "right": "20dp",
                "skin": "slFbox",
                "overrides": {
                    "btnNext": {
                        "isVisible": false
                    },
                    "btnSave": {
                        "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmAlertsManagement.Move_UC\")",
                        "width": "98px"
                    },
                    "commonButtons": {
                        "centerY": "50%",
                        "left": "20dp",
                        "right": "20dp",
                        "top": "viz.val_cleared",
                        "width": "viz.val_cleared"
                    }
                }
            }, {
                "overrides": {}
            }, {
                "overrides": {}
            });
            flxReasignGroupButtons.add(reassignPopupButtons);
            flxReassignGroupCont.add(flxReassignGroupHeaderLine, flxReassignGroupPopupHeading, flxReasignPopupBody, flxReasignGroupButtons);
            flxReassignGroupPopup.add(flxReassignGroupCont);
            var flxErrorPopup = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "100%",
                "id": "flxErrorPopup",
                "isVisible": false,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0dp",
                "width": "100%",
                "zIndex": 20
            }, {}, {});
            flxErrorPopup.setDefaultUnit(kony.flex.DP);
            var popupError = new com.adminConsole.common.popupError({
                "height": "100%",
                "id": "popupError",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "masterType": constants.MASTER_TYPE_DEFAULT,
                "isModalContainer": false,
                "skin": "sknFlxBg222B35Op50PopupBg",
                "top": "0dp",
                "width": "100%",
                "overrides": {
                    "flxPopUp": {
                        "centerY": "50%"
                    }
                }
            }, {
                "overrides": {}
            }, {
                "overrides": {}
            });
            flxErrorPopup.add(popupError);
            var ToolTip = new com.adminConsole.Customers.ToolTip({
                "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                "id": "ToolTip",
                "isVisible": false,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "masterType": constants.MASTER_TYPE_DEFAULT,
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0dp",
                "width": "80dp",
                "zIndex": 10,
                "overrides": {
                    "ToolTip": {
                        "isVisible": false,
                        "left": "0dp",
                        "right": "viz.val_cleared",
                        "top": "0dp",
                        "width": "80dp",
                        "zIndex": 10
                    },
                    "flxToolTipMessage": {
                        "centerX": "50%",
                        "height": "28dp",
                        "left": "0dp",
                        "minHeight": "70dp",
                        "top": "9dp",
                        "width": kony.flex.USE_PREFFERED_SIZE
                    },
                    "lblNoConcentToolTip": {
                        "bottom": "5dp",
                        "height": "18dp",
                        "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmAlertsManagement.GlobalAlert\")",
                        "left": "5dp",
                        "top": "5dp",
                        "width": kony.flex.USE_PREFFERED_SIZE,
                        "zIndex": 10
                    },
                    "lblarrow": {
                        "centerX": "viz.val_cleared",
                        "height": "10dp",
                        "left": "viz.val_cleared",
                        "right": "20dp",
                        "width": "17dp"
                    }
                }
            }, {
                "overrides": {}
            }, {
                "overrides": {}
            });
            flxAlertsManagement.add(flxLeftPannel, flxRightPanel, flxHeaderDropdown, flxToastMessage, flxLoading, flxDeleteAlert, flxAlertPreview, flxAddSubAlertPopUp, flxPreviewPopup, flxSequencePopUp, flxViewAlertLanguagesPopup, flxReassignGroupPopup, flxErrorPopup, ToolTip);
            var flxEditCancelConfirmation = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "100%",
                "id": "flxEditCancelConfirmation",
                "isVisible": false,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "masterType": constants.MASTER_TYPE_USERWIDGET,
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0dp",
                "width": "100%",
                "zIndex": 5
            }, {}, {});
            flxEditCancelConfirmation.setDefaultUnit(kony.flex.DP);
            var popUpCancelEdits = new com.adminConsole.common.popUpCancelEdits({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "height": "100%",
                "id": "popUpCancelEdits",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "masterType": constants.MASTER_TYPE_DEFAULT,
                "isModalContainer": false,
                "skin": "sknflxBg000000Op50",
                "top": "0dp",
                "width": "100%",
                "zIndex": 1,
                "overrides": {
                    "btnPopUpCancel": {
                        "i18n_text": "kony.i18n.getLocalizedString(\"i8n.navigation.cancelButton\")",
                        "right": "120px"
                    },
                    "btnPopUpDelete": {
                        "i18n_text": "kony.i18n.getLocalizedString(\"i8n.navigation.okButton\")",
                        "minWidth": "viz.val_cleared"
                    },
                    "imgPopUpClose": {
                        "src": "close_blue.png"
                    },
                    "lblPopUpMainMessage": {
                        "i18n_text": "kony.i18n.getLocalizedString(\"i8n.navigation.cancelChanges\")"
                    },
                    "popUpCancelEdits": {
                        "bottom": "viz.val_cleared",
                        "centerX": "viz.val_cleared",
                        "centerY": "viz.val_cleared",
                        "height": "100%",
                        "left": "0dp",
                        "maxHeight": "viz.val_cleared",
                        "maxWidth": "viz.val_cleared",
                        "minHeight": "viz.val_cleared",
                        "minWidth": "viz.val_cleared",
                        "right": "viz.val_cleared",
                        "top": "0dp",
                        "width": "100%"
                    },
                    "rtxPopUpDisclaimer": {
                        "i18n_text": "kony.i18n.getLocalizedString(\"i8n.navigation.cancelChangesBody\")"
                    }
                }
            }, {
                "overrides": {}
            }, {
                "overrides": {}
            });
            flxEditCancelConfirmation.add(popUpCancelEdits);
            this.add(flxAlertsManagement, flxEditCancelConfirmation);
        };
        return [{
            "addWidgets": addWidgetsfrmAlertsManagement,
            "enabledForIdleTimeout": true,
            "id": "frmAlertsManagement",
            "layoutType": kony.flex.FREE_FORM,
            "needAppMenu": false,
            "preShow": function(eventobject) {
                controller.AS_Form_c9e9acfca53b4907a1442bb367a1ca10(eventobject);
            },
            "skin": "slForm"
        }, {
            "displayOrientation": constants.FORM_DISPLAY_ORIENTATION_PORTRAIT,
            "layoutType": kony.flex.FREE_FORM,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, {
            "onDeviceBack": controller.AS_Form_cfc12b9489e74d88ba6cee52fcb55a15,
            "retainScrollPosition": false
        }]
    }
});