define("flxViewAllLanguagePopup", function() {
    return function(controller) {
        var flxViewAllLanguagePopup = new kony.ui.FlexContainer({
            "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
            "clipBounds": true,
            "id": "flxViewAllLanguagePopup",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "left": "0dp",
            "isModalContainer": false,
            "skin": "sknFlxFFFFFF100O",
            "top": "0dp",
            "width": "100%"
        }, {}, {});
        flxViewAllLanguagePopup.setDefaultUnit(kony.flex.DP);
        var lblLanguage = new kony.ui.Label({
            "bottom": "15dp",
            "id": "lblLanguage",
            "isVisible": true,
            "left": "30dp",
            "onTouchEnd": controller.AS_Label_b388eebaef3c4fe594f561b7cfbd6c59,
            "skin": "sknLbl485C75LatoRegular13Px",
            "text": "Language",
            "top": "15dp",
            "width": "20%",
            "zIndex": 1
        }, {
            "contentAlignment": constants.CONTENT_ALIGN_TOP_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, {});
        var lblAlertDisplayName = new kony.ui.Label({
            "bottom": "15dp",
            "id": "lblAlertDisplayName",
            "isVisible": true,
            "left": "24%",
            "onTouchEnd": controller.AS_Label_b388eebaef3c4fe594f561b7cfbd6c59,
            "skin": "sknLbl485C75LatoRegular13Px",
            "text": "Name",
            "top": "15dp",
            "width": "20%",
            "zIndex": 1
        }, {
            "contentAlignment": constants.CONTENT_ALIGN_TOP_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, {});
        var lblAlertDisplayDesc = new kony.ui.Label({
            "bottom": "15dp",
            "id": "lblAlertDisplayDesc",
            "isVisible": true,
            "left": "48%",
            "onTouchEnd": controller.AS_Label_b388eebaef3c4fe594f561b7cfbd6c59,
            "right": "20dp",
            "skin": "sknLbl485C75LatoRegular13Px",
            "text": "Desc",
            "top": "15dp",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, {
            "contentAlignment": constants.CONTENT_ALIGN_TOP_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, {});
        var lblSeperator = new kony.ui.Label({
            "bottom": "0dp",
            "height": "1dp",
            "id": "lblSeperator",
            "isVisible": false,
            "left": "0dp",
            "onTouchEnd": controller.AS_Label_b388eebaef3c4fe594f561b7cfbd6c59,
            "right": "0dp",
            "skin": "sknlblSeperatorD7D9E0",
            "text": ".",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, {
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, {});
        flxViewAllLanguagePopup.add(lblLanguage, lblAlertDisplayName, lblAlertDisplayDesc, lblSeperator);
        return flxViewAllLanguagePopup;
    }
})