define("flxSegVariableOptions", function() {
    return function(controller) {
        var flxSegVariableOptions = new kony.ui.FlexContainer({
            "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
            "clipBounds": true,
            "id": "flxSegVariableOptions",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "left": "0dp",
            "isModalContainer": false,
            "skin": "sknbackGroundffffff100",
            "top": "0dp",
            "width": "100%"
        }, {}, {
            "hoverSkin": "sknFlxffffffCursorPointer"
        });
        flxSegVariableOptions.setDefaultUnit(kony.flex.DP);
        var lblIdName = new kony.ui.Label({
            "bottom": "2dp",
            "id": "lblIdName",
            "isVisible": true,
            "left": "0dp",
            "skin": "sknlbl485c7511px",
            "text": "Granville Street, Vancover, BC, Canada",
            "top": "2dp",
            "width": "100%",
            "zIndex": 1
        }, {
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, {});
        flxSegVariableOptions.add(lblIdName);
        return flxSegVariableOptions;
    }
})