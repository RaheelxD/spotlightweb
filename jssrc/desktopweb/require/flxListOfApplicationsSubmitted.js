define("flxListOfApplicationsSubmitted", function() {
    return function(controller) {
        var flxListOfApplicationsSubmitted = new kony.ui.FlexContainer({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "clipBounds": true,
            "height": "50dp",
            "id": "flxListOfApplicationsSubmitted",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "left": "0dp",
            "isModalContainer": false,
            "skin": "sknFlxFFFFFF1000",
            "top": "0dp",
            "width": "100%"
        }, {}, {
            "hoverSkin": "sknFlxf9fbfd1000"
        });
        flxListOfApplicationsSubmitted.setDefaultUnit(kony.flex.DP);
        var lblApplicationId = new kony.ui.Label({
            "id": "lblApplicationId",
            "isVisible": true,
            "left": "30dp",
            "skin": "sknLblFont485C75100O",
            "text": "Label",
            "top": "22dp",
            "width": "15%",
            "zIndex": 1
        }, {
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, {});
        var lblLoanType = new kony.ui.Label({
            "id": "lblLoanType",
            "isVisible": true,
            "left": "19%",
            "skin": "sknLblFont485C75100O",
            "text": "Label",
            "top": "22dp",
            "width": "12%",
            "zIndex": 1
        }, {
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, {});
        var lblAmount = new kony.ui.Label({
            "id": "lblAmount",
            "isVisible": true,
            "left": "32%",
            "skin": "sknLblFont485C75100O",
            "text": "Label",
            "top": "22dp",
            "width": "12%",
            "zIndex": 1
        }, {
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, {});
        var lblApplicant = new kony.ui.Label({
            "id": "lblApplicant",
            "isVisible": true,
            "left": "45%",
            "skin": "sknLblFont485C75100O",
            "text": "Label",
            "top": "22dp",
            "width": "15%",
            "zIndex": 1
        }, {
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, {});
        var lblSubmitOn = new kony.ui.Label({
            "id": "lblSubmitOn",
            "isVisible": true,
            "left": "61%",
            "skin": "sknLblFont485C75100O",
            "text": "Label",
            "top": "22dp",
            "width": "15%",
            "zIndex": 1
        }, {
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, {});
        var lblStatus = new kony.ui.Label({
            "id": "lblStatus",
            "isVisible": true,
            "left": "77%",
            "skin": "sknLblFont485C75100O",
            "text": "Label",
            "top": "22dp",
            "width": "15%",
            "zIndex": 1
        }, {
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, {});
        var flxOptions = new kony.ui.FlexContainer({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "clipBounds": true,
            "height": "25dp",
            "id": "flxOptions",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "left": "93%",
            "isModalContainer": false,
            "skin": "sknFlxBorffffff1pxRound",
            "top": "18dp",
            "width": "25dp",
            "zIndex": 2
        }, {}, {
            "hoverSkin": "sknflxffffffop100Border424242Radius100px"
        });
        flxOptions.setDefaultUnit(kony.flex.DP);
        var imgOptions = new kony.ui.Image2({
            "centerX": "50%",
            "centerY": "50%",
            "height": "12dp",
            "id": "imgOptions",
            "isVisible": false,
            "left": 14,
            "skin": "slImage",
            "src": "dots3x.png",
            "width": "6dp",
            "zIndex": 1
        }, {
            "imageScaleMode": constants.IMAGE_SCALE_MODE_MAINTAIN_ASPECT_RATIO,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, {});
        var lblOptions = new kony.ui.Label({
            "centerY": "50%",
            "height": "15dp",
            "id": "lblOptions",
            "isVisible": true,
            "left": "5dp",
            "skin": "sknIcon15px",
            "text": "",
            "width": "15dp",
            "zIndex": 1
        }, {
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, {});
        flxOptions.add(imgOptions, lblOptions);
        var lblSeparatorListOfApplications = new kony.ui.Label({
            "bottom": "0dp",
            "height": "1dp",
            "id": "lblSeparatorListOfApplications",
            "isVisible": true,
            "left": "22dp",
            "right": "22dp",
            "skin": "sknLblD5D9DD1000",
            "zIndex": 1
        }, {
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, {});
        var lblResume = new kony.ui.Label({
            "id": "lblResume",
            "isVisible": false,
            "left": "860dp",
            "skin": "sknLbladminfonticon",
            "text": "",
            "top": "17dp",
            "width": "10%",
            "zIndex": 1
        }, {
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, {});
        flxListOfApplicationsSubmitted.add(lblApplicationId, lblLoanType, lblAmount, lblApplicant, lblSubmitOn, lblStatus, flxOptions, lblSeparatorListOfApplications, lblResume);
        return flxListOfApplicationsSubmitted;
    }
})