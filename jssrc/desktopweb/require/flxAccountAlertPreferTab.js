define("flxAccountAlertPreferTab", function() {
    return function(controller) {
        var flxAccountAlertPreferTab = new kony.ui.FlexContainer({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "clipBounds": true,
            "height": "30dp",
            "id": "flxAccountAlertPreferTab",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "left": "0dp",
            "isModalContainer": false,
            "skin": "sknflxf9f9f9NoBorder",
            "top": "0dp",
            "width": "100%"
        }, {}, {});
        flxAccountAlertPreferTab.setDefaultUnit(kony.flex.DP);
        var lblDropArrow = new kony.ui.Label({
            "centerY": "50%",
            "height": "15dp",
            "id": "lblDropArrow",
            "isVisible": true,
            "left": "20dp",
            "skin": "sknIcon73767815px",
            "text": "",
            "top": "10dp",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, {
            "contentAlignment": constants.CONTENT_ALIGN_TOP_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, {});
        var btnAccountName = new kony.ui.Button({
            "centerY": "50%",
            "id": "btnAccountName",
            "isVisible": true,
            "left": "42dp",
            "skin": "sknBtnUtilRest73767812pxReg",
            "text": "Savings",
            "width": "80%",
            "zIndex": 1
        }, {
            "contentAlignment": constants.CONTENT_ALIGN_TOP_LEFT,
            "displayText": true,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, {});
        var btnAccountsCount = new kony.ui.Button({
            "centerY": "50%",
            "id": "btnAccountsCount",
            "isVisible": true,
            "right": "50dp",
            "skin": "sknBtnUtilRest73767812pxReg",
            "text": "08",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, {
            "contentAlignment": constants.CONTENT_ALIGN_TOP_LEFT,
            "displayText": true,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, {
            "hoverSkin": "sknBtnUtilHover30353612pxReg"
        });
        var lblArrow = new kony.ui.Label({
            "centerY": "50%",
            "height": "13px",
            "id": "lblArrow",
            "isVisible": true,
            "right": "0px",
            "skin": "sknIcon485C7513px",
            "text": "",
            "width": "30px",
            "zIndex": 1
        }, {
            "contentAlignment": constants.CONTENT_ALIGN_TOP_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, {});
        var lblLine = new kony.ui.Label({
            "bottom": "0dp",
            "height": "1dp",
            "id": "lblLine",
            "isVisible": true,
            "left": "20dp",
            "right": "30dp",
            "skin": "sknlblSeperator",
            "text": "-",
            "zIndex": 1
        }, {
            "contentAlignment": constants.CONTENT_ALIGN_TOP_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, {});
        flxAccountAlertPreferTab.add(lblDropArrow, btnAccountName, btnAccountsCount, lblArrow, lblLine);
        return flxAccountAlertPreferTab;
    }
})