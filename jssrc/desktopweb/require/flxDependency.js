define("flxDependency", function() {
    return function(controller) {
        var flxDependency = new kony.ui.FlexContainer({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "clipBounds": true,
            "height": "60dp",
            "id": "flxDependency",
            "isVisible": true,
            "layoutType": kony.flex.FLOW_VERTICAL,
            "left": "0dp",
            "isModalContainer": false,
            "skin": "sknBackgroundFFFFFF",
            "top": "0dp",
            "width": "100%"
        }, {}, {});
        flxDependency.setDefaultUnit(kony.flex.DP);
        var flxDependencyDetails = new kony.ui.FlexContainer({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "clipBounds": true,
            "height": "98%",
            "id": "flxDependencyDetails",
            "isVisible": true,
            "layoutType": kony.flex.FLOW_HORIZONTAL,
            "left": "0dp",
            "isModalContainer": false,
            "skin": "slFbox",
            "top": "0dp",
            "width": "100%"
        }, {}, {});
        flxDependencyDetails.setDefaultUnit(kony.flex.DP);
        var lblFeatureName = new kony.ui.Label({
            "centerY": "50%",
            "id": "lblFeatureName",
            "isVisible": true,
            "left": "5%",
            "skin": "sknLbl485C75LatoRegular13Px",
            "text": "Label",
            "width": "28%",
            "zIndex": 1
        }, {
            "contentAlignment": constants.CONTENT_ALIGN_TOP_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, {});
        var lblActionName = new kony.ui.Label({
            "centerY": "50%",
            "id": "lblActionName",
            "isVisible": true,
            "left": "5%",
            "skin": "sknLbl485C75LatoRegular13Px",
            "text": "Label",
            "width": "28%",
            "zIndex": 1
        }, {
            "contentAlignment": constants.CONTENT_ALIGN_TOP_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, {});
        var lblActionCode = new kony.ui.Label({
            "centerY": "50%",
            "id": "lblActionCode",
            "isVisible": true,
            "left": "5%",
            "skin": "sknLbl485C75LatoRegular13Px",
            "text": "Label",
            "width": "25%",
            "zIndex": 1
        }, {
            "contentAlignment": constants.CONTENT_ALIGN_TOP_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, {});
        flxDependencyDetails.add(lblFeatureName, lblActionName, lblActionCode);
        var lblSeperatorLine = new kony.ui.Label({
            "centerX": "50%",
            "height": "1dp",
            "id": "lblSeperatorLine",
            "isVisible": true,
            "left": "0dp",
            "skin": "sknlblSeperatorBgD5D9DD",
            "text": "Label",
            "top": "0dp",
            "width": "94%",
            "zIndex": 1
        }, {
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, {});
        flxDependency.add(flxDependencyDetails, lblSeperatorLine);
        return flxDependency;
    }
})