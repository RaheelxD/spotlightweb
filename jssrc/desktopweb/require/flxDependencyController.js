define("userflxDependencyController", {
    //Type your controller code here 
});
define("flxDependencyControllerActions", {
    /* 
    This is an auto generated file and any modifications to it may result in corruption of the action sequence.
    */
});
define("flxDependencyController", ["userflxDependencyController", "flxDependencyControllerActions"], function() {
    var controller = require("userflxDependencyController");
    var controllerActions = ["flxDependencyControllerActions"];
    return kony.visualizer.mixinControllerActions(controller, controllerActions);
});
