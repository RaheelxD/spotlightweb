define("flxPermissions", function() {
    return function(controller) {
        var flxPermissions = new kony.ui.FlexContainer({
            "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
            "clipBounds": true,
            "id": "flxPermissions",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "isModalContainer": false,
            "skin": "sknflxffffffop100"
        }, {}, {});
        flxPermissions.setDefaultUnit(kony.flex.DP);
        var flxPermissionsContainer = new kony.ui.FlexContainer({
            "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
            "clipBounds": true,
            "id": "flxPermissionsContainer",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "left": "20dp",
            "isModalContainer": false,
            "right": "20dp",
            "skin": "slFbox",
            "top": "0dp"
        }, {}, {
            "hoverSkin": "sknfbfcfc"
        });
        flxPermissionsContainer.setDefaultUnit(kony.flex.DP);
        var lblPermissionName = new kony.ui.Label({
            "id": "lblPermissionName",
            "isVisible": true,
            "left": "10px",
            "skin": "sknlblLatoRegular484b5213px",
            "text": "View Permissions",
            "top": "18px",
            "width": "120dp",
            "zIndex": 1
        }, {
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, {});
        var lblDescription = new kony.ui.Label({
            "bottom": "18px",
            "id": "lblDescription",
            "isVisible": true,
            "left": "26%",
            "skin": "sknlblLatoRegular484b5213px",
            "text": "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do",
            "top": "18px",
            "width": "34%",
            "zIndex": 1
        }, {
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, {});
        var lblNoOfRoles = new kony.ui.Label({
            "id": "lblNoOfRoles",
            "isVisible": true,
            "left": "64%",
            "skin": "sknlblLatoRegular484b5213px",
            "text": "25",
            "top": "18px",
            "width": "10%",
            "zIndex": 1
        }, {
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, {});
        var lblNoOfUsers = new kony.ui.Label({
            "id": "lblNoOfUsers",
            "isVisible": true,
            "left": "73%",
            "skin": "sknlblLatoRegular484b5213px",
            "text": "20",
            "top": "18px",
            "width": "10%",
            "zIndex": 1
        }, {
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, {});
        var flxStatus = new kony.ui.FlexContainer({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "clipBounds": true,
            "height": "50px",
            "id": "flxStatus",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "left": "83.50%",
            "isModalContainer": false,
            "skin": "slFbox",
            "top": 0,
            "width": "8%",
            "zIndex": 1
        }, {}, {});
        flxStatus.setDefaultUnit(kony.flex.DP);
        var lblPermissionStatus = new kony.ui.Label({
            "centerY": "50%",
            "id": "lblPermissionStatus",
            "isVisible": true,
            "left": "16px",
            "skin": "sknlblLato5bc06cBold14px",
            "text": "Active",
            "width": "45px",
            "zIndex": 1
        }, {
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, {});
        var fontIconStatusImg = new kony.ui.Label({
            "centerY": "50%",
            "id": "fontIconStatusImg",
            "isVisible": true,
            "left": "0dp",
            "skin": "sknFontIconActivate",
            "text": "",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, {
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, {});
        flxStatus.add(lblPermissionStatus, fontIconStatusImg);
        var flxOptions = new kony.ui.FlexContainer({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "clipBounds": true,
            "height": "25dp",
            "id": "flxOptions",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "isModalContainer": false,
            "onClick": controller.AS_FlexContainer_b8b1da7213854a02bd717290f2554d7d,
            "right": "10dp",
            "skin": "sknFlxBorffffff1pxRound",
            "top": "11dp",
            "width": "25px",
            "zIndex": 2
        }, {}, {
            "hoverSkin": "sknflxffffffop100Border424242Radius100px"
        });
        flxOptions.setDefaultUnit(kony.flex.DP);
        var lblIconOptions = new kony.ui.Label({
            "centerX": "50%",
            "centerY": "49%",
            "id": "lblIconOptions",
            "isVisible": true,
            "skin": "sknFontIconOptionMenu",
            "text": "",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, {
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, {});
        flxOptions.add(lblIconOptions);
        flxPermissionsContainer.add(lblPermissionName, lblDescription, lblNoOfRoles, lblNoOfUsers, flxStatus, flxOptions);
        var lblSeperator = new kony.ui.Label({
            "bottom": 0,
            "height": "1px",
            "id": "lblSeperator",
            "isVisible": true,
            "left": "20dp",
            "right": "20dp",
            "skin": "sknlblSeperator",
            "text": ".",
            "zIndex": 1
        }, {
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, {});
        flxPermissions.add(flxPermissionsContainer, lblSeperator);
        return flxPermissions;
    }
})