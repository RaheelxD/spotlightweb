define("ReportsManagementModule/frmReportsManagement", function() {
    return function(controller) {
        function addWidgetsfrmReportsManagement() {
            this.setDefaultUnit(kony.flex.DP);
            var flxMain = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "900dp",
                "id": "flxMain",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0dp",
                "width": "100%",
                "zIndex": 1
            }, {}, {});
            flxMain.setDefaultUnit(kony.flex.DP);
            var flxLeftPannel = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "100%",
                "id": "flxLeftPannel",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "masterType": constants.MASTER_TYPE_USERWIDGET,
                "isModalContainer": false,
                "skin": "sknLeftPannel0bb79e4bcca8e45",
                "top": "0dp",
                "width": "305px",
                "zIndex": 5
            }, {}, {});
            flxLeftPannel.setDefaultUnit(kony.flex.DP);
            var leftMenuNew = new com.adminConsole.navigation.leftMenuNew({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "height": "100%",
                "id": "leftMenuNew",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "masterType": constants.MASTER_TYPE_DEFAULT,
                "isModalContainer": false,
                "skin": "sknFlxBg003E75Op100",
                "top": "0dp",
                "width": "100%",
                "overrides": {
                    "brandLogo": {
                        "src": "infinity_dbx_c360_logo_white_2x.png"
                    },
                    "imgBottomLogo": {
                        "src": "logo_white_2x.png"
                    },
                    "leftMenuNew": {
                        "bottom": "viz.val_cleared",
                        "centerX": "viz.val_cleared",
                        "centerY": "viz.val_cleared",
                        "height": "100%",
                        "left": "0dp",
                        "maxHeight": "viz.val_cleared",
                        "maxWidth": "viz.val_cleared",
                        "minHeight": "viz.val_cleared",
                        "minWidth": "viz.val_cleared",
                        "right": "viz.val_cleared",
                        "top": "0dp",
                        "width": "100%"
                    }
                }
            }, {
                "overrides": {}
            }, {
                "overrides": {}
            });
            flxLeftPannel.add(leftMenuNew);
            var flxRightPannel = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "100%",
                "id": "flxRightPannel",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "305px",
                "isModalContainer": false,
                "right": "0px",
                "skin": "slFbox0b7aaa2e3bfa340",
                "top": "0dp",
                "zIndex": 5
            }, {}, {});
            flxRightPannel.setDefaultUnit(kony.flex.DP);
            var flxMainHeader = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "102dp",
                "id": "flxMainHeader",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "isModalContainer": false,
                "right": "0px",
                "skin": "slFbox",
                "top": "0dp",
                "zIndex": 10
            }, {}, {});
            flxMainHeader.setDefaultUnit(kony.flex.DP);
            var mainHeader = new com.adminConsole.header.mainHeader({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "height": "106px",
                "id": "mainHeader",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0",
                "masterType": constants.MASTER_TYPE_DEFAULT,
                "isModalContainer": false,
                "skin": "CopyslFbox1",
                "top": "0",
                "width": "100%",
                "overrides": {
                    "btnAddNewOption": {
                        "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmReportsManagement.label.createNewReport_caps\")",
                        "isVisible": true,
                        "right": "0dp"
                    },
                    "btnDropdownList": {
                        "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmReportsManagement.label.datasources_caps\")",
                        "isVisible": true,
                        "right": "20dp"
                    },
                    "flxAddNewOption": {
                        "isVisible": false
                    },
                    "flxButtons": {
                        "isVisible": true,
                        "reverseLayoutDirection": true,
                        "top": "55px",
                        "layoutType": kony.flex.FLOW_HORIZONTAL
                    },
                    "flxDownloadList": {
                        "isVisible": false
                    },
                    "flxHeaderSeperator": {
                        "isVisible": false
                    },
                    "imgLogout": {
                        "src": "img_logout.png"
                    },
                    "lblHeading": {
                        "centerY": "viz.val_cleared",
                        "height": kony.flex.USE_PREFFERED_SIZE,
                        "i18n_text": "kony.i18n.getLocalizedString(\"i18n.leftmenu.reports\")",
                        "top": "55dp"
                    },
                    "mainHeader": {
                        "left": "0",
                        "top": "0"
                    }
                }
            }, {
                "overrides": {}
            }, {
                "overrides": {}
            });
            flxMainHeader.add(mainHeader);
            var flxHeaderDropdown = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                "clipBounds": true,
                "id": "flxHeaderDropdown",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "isModalContainer": false,
                "right": "35dp",
                "skin": "slFbox",
                "top": "20dp",
                "width": "400dp",
                "zIndex": 11
            }, {}, {});
            flxHeaderDropdown.setDefaultUnit(kony.flex.DP);
            var dropdownMainHeader = new com.adminConsole.common.dropdownMainHeader({
                "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                "id": "dropdownMainHeader",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "masterType": constants.MASTER_TYPE_DEFAULT,
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0dp",
                "width": "100%",
                "overrides": {
                    "flxDropdown": {
                        "isVisible": false
                    },
                    "imgUpArrow": {
                        "src": "uparrow_2x.png"
                    }
                }
            }, {
                "overrides": {}
            }, {
                "overrides": {}
            });
            flxHeaderDropdown.add(dropdownMainHeader);
            var flxBreadCrumbs = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "26px",
                "id": "flxBreadCrumbs",
                "isVisible": false,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "100px",
                "width": "100%",
                "zIndex": 10
            }, {}, {});
            flxBreadCrumbs.setDefaultUnit(kony.flex.DP);
            var breadcrumbs = new com.adminConsole.common.breadcrumbs({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "bottom": "0px",
                "height": "20px",
                "id": "breadcrumbs",
                "isVisible": true,
                "layoutType": kony.flex.FLOW_HORIZONTAL,
                "left": "0px",
                "masterType": constants.MASTER_TYPE_DEFAULT,
                "isModalContainer": false,
                "right": "0px",
                "skin": "sknflxf5f6f8Op100",
                "top": "0px",
                "zIndex": 100,
                "overrides": {
                    "breadcrumbs": {
                        "bottom": "0px",
                        "isVisible": true,
                        "left": "0px",
                        "right": "0px",
                        "top": "0px",
                        "width": "viz.val_cleared",
                        "zIndex": 100
                    },
                    "btnBackToMain": {
                        "left": "35px",
                        "text": "REPORTS"
                    },
                    "btnPreviousPage": {
                        "isVisible": false
                    },
                    "btnPreviousPage1": {
                        "isVisible": false
                    },
                    "imgBreadcrumbsDown": {
                        "src": "img_down_arrow.png"
                    },
                    "imgBreadcrumbsRight": {
                        "src": "img_breadcrumb_arrow.png"
                    },
                    "imgBreadcrumbsRight2": {
                        "isVisible": false,
                        "src": "img_breadcrumb_arrow.png"
                    },
                    "imgBreadcrumbsRight3": {
                        "isVisible": false,
                        "src": "img_breadcrumb_arrow.png"
                    },
                    "lblCurrentScreen": {
                        "text": "MESSAGE REPORT"
                    }
                }
            }, {
                "overrides": {}
            }, {
                "overrides": {}
            });
            var flxHeaderSeperator = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "bottom": "1dp",
                "clipBounds": true,
                "height": "1dp",
                "id": "flxHeaderSeperator",
                "isVisible": false,
                "layoutType": kony.flex.FREE_FORM,
                "left": "35dp",
                "isModalContainer": false,
                "right": "35dp",
                "skin": "sknflx115678",
                "zIndex": 1
            }, {}, {});
            flxHeaderSeperator.setDefaultUnit(kony.flex.DP);
            flxHeaderSeperator.add();
            flxBreadCrumbs.add(breadcrumbs, flxHeaderSeperator);
            var flxMainReportsOuter = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "bottom": "20dp",
                "clipBounds": true,
                "id": "flxMainReportsOuter",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "35dp",
                "isModalContainer": false,
                "right": 35,
                "skin": "sknflxffffffBorderd6dbe7Radius4px",
                "top": "102dp"
            }, {}, {});
            flxMainReportsOuter.setDefaultUnit(kony.flex.DP);
            var flxMainContainer = new kony.ui.FlexContainer({
                "bottom": "0px",
                "clipBounds": true,
                "id": "flxMainContainer",
                "isVisible": false,
                "layoutType": kony.flex.FREE_FORM,
                "left": "20px",
                "isModalContainer": false,
                "right": "20px",
                "skin": "slFbox",
                "top": "0px"
            }, {}, {});
            flxMainContainer.setDefaultUnit(kony.flex.DP);
            var flxUsersReport = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "bottom": "0px",
                "clipBounds": true,
                "id": "flxUsersReport",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "isModalContainer": false,
                "right": "0px",
                "skin": "slFbox",
                "top": "0px",
                "zIndex": 1
            }, {}, {});
            flxUsersReport.setDefaultUnit(kony.flex.DP);
            var datePickerUsers = new kony.ui.CustomWidget({
                "id": "datePickerUsers",
                "isVisible": true,
                "left": "1px",
                "right": "3px",
                "bottom": "1px",
                "top": "48px",
                "width": "175px",
                "height": "30px",
                "zIndex": 10,
                "clipBounds": true
            }, {
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {
                "widgetName": "dateRangePicker",
                "drops": "down",
                "event": null,
                "maxDate": "true",
                "opens": "right",
                "rangeType": null,
                "resetData": "Today",
                "type": "list",
                "value": ""
            });
            var flxCloseCal3 = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "15px",
                "id": "flxCloseCal3",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "178px",
                "isModalContainer": false,
                "right": "15px",
                "skin": "slFbox",
                "top": "52px",
                "width": "15px",
                "zIndex": 11
            }, {}, {
                "hoverSkin": "sknCursor"
            });
            flxCloseCal3.setDefaultUnit(kony.flex.DP);
            var lblCloseCal3 = new kony.ui.Label({
                "centerX": "50%",
                "centerY": "50%",
                "id": "lblCloseCal3",
                "isVisible": true,
                "skin": "sknfontIconDescDownArrow12px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmAlertsManagement.lblAlertPreviewClose\")",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxCloseCal3.add(lblCloseCal3);
            var search = new com.adminConsole.reports.search({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "height": "80px",
                "id": "search",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0px",
                "masterType": constants.MASTER_TYPE_DEFAULT,
                "isModalContainer": false,
                "right": "0px",
                "skin": "slFbox",
                "top": "0px",
                "overrides": {
                    "btnAdd": {
                        "left": "220px"
                    },
                    "flxCSR": {
                        "isVisible": false
                    },
                    "flxCategory": {
                        "isVisible": false
                    },
                    "flxDownload": {
                        "right": "0px"
                    },
                    "imgClose": {
                        "src": "closetags_2x.png"
                    },
                    "imgDownload": {
                        "src": "download_2x.png"
                    },
                    "search": {
                        "height": "80px",
                        "left": "0px",
                        "right": "0px",
                        "top": "0px",
                        "width": "viz.val_cleared"
                    },
                    "tbxCSR": {
                        "placeholder": "Select CSR Name"
                    }
                }
            }, {
                "overrides": {}
            }, {
                "overrides": {}
            });
            var flxUserReport = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                "clipBounds": true,
                "id": "flxUserReport",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0px",
                "isModalContainer": false,
                "right": "0px",
                "skin": "sknFlxCalendar",
                "top": "100px",
                "zIndex": 10
            }, {}, {});
            flxUserReport.setDefaultUnit(kony.flex.DP);
            var flxUsersReportHeader = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "60dp",
                "id": "flxUsersReportHeader",
                "isVisible": true,
                "layoutType": kony.flex.FLOW_HORIZONTAL,
                "left": "35px",
                "isModalContainer": false,
                "right": "35px",
                "skin": "slFbox",
                "top": "0px",
                "zIndex": 1
            }, {}, {});
            flxUsersReportHeader.setDefaultUnit(kony.flex.DP);
            var flxDescription = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "100%",
                "id": "flxDescription",
                "isVisible": true,
                "layoutType": kony.flex.FLOW_HORIZONTAL,
                "left": "0px",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0dp",
                "width": "50%",
                "zIndex": 1
            }, {}, {});
            flxDescription.setDefaultUnit(kony.flex.DP);
            var lblDescription = new kony.ui.Label({
                "centerY": "50%",
                "id": "lblDescription",
                "isVisible": true,
                "left": 20,
                "skin": "sknlblLato696c7312px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.View.DESCRIPTION\")",
                "top": 0,
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxDescription.add(lblDescription);
            var flxMobile = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "100%",
                "id": "flxMobile",
                "isVisible": true,
                "layoutType": kony.flex.FLOW_HORIZONTAL,
                "left": "0%",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0dp",
                "width": "17%",
                "zIndex": 1
            }, {}, {});
            flxMobile.setDefaultUnit(kony.flex.DP);
            var lblMobile = new kony.ui.Label({
                "centerY": "50%",
                "id": "lblMobile",
                "isVisible": true,
                "left": "0px",
                "skin": "sknlblLato5d6c7f12px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmReportsManagement.lblMobile\")",
                "top": 0,
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxMobile.add(lblMobile);
            var flxOnline = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "100%",
                "id": "flxOnline",
                "isVisible": true,
                "layoutType": kony.flex.FLOW_HORIZONTAL,
                "left": "0",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0dp",
                "width": "16%",
                "zIndex": 1
            }, {}, {});
            flxOnline.setDefaultUnit(kony.flex.DP);
            var lblOnline = new kony.ui.Label({
                "centerY": "50%",
                "id": "lblOnline",
                "isVisible": true,
                "left": "0px",
                "skin": "sknlblLato696c7312px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.View.ONLINE\")",
                "top": 0,
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxOnline.add(lblOnline);
            var flxTotal = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "100%",
                "id": "flxTotal",
                "isVisible": true,
                "layoutType": kony.flex.FLOW_HORIZONTAL,
                "left": "0",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0dp",
                "width": "17%",
                "zIndex": 1
            }, {}, {});
            flxTotal.setDefaultUnit(kony.flex.DP);
            var lblTotal = new kony.ui.Label({
                "centerY": "50%",
                "id": "lblTotal",
                "isVisible": true,
                "left": "0px",
                "skin": "sknlblLato696c7312px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmReportsManagement.lblTotal\")",
                "top": 0,
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxTotal.add(lblTotal);
            flxUsersReportHeader.add(flxDescription, flxMobile, flxOnline, flxTotal);
            var flxUserReportHeaderSeperator = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "bottom": "1dp",
                "clipBounds": true,
                "height": "1dp",
                "id": "flxUserReportHeaderSeperator",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "35dp",
                "isModalContainer": false,
                "right": "35dp",
                "skin": "sknTableHeaderLine",
                "top": "60px",
                "zIndex": 1
            }, {}, {});
            flxUserReportHeaderSeperator.setDefaultUnit(kony.flex.DP);
            flxUserReportHeaderSeperator.add();
            var segListing = new kony.ui.SegmentedUI2({
                "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                "bottom": "20px",
                "groupCells": false,
                "id": "segListing",
                "isVisible": true,
                "left": "0px",
                "maxHeight": "500px",
                "needPageIndicator": true,
                "pageOffDotImage": "pageoffdot.png",
                "pageOnDotImage": "pageondot.png",
                "retainSelection": false,
                "rowFocusSkin": "seg2Focus",
                "rowSkin": "seg2Normal",
                "sectionHeaderSkin": "sliPhoneSegmentHeader",
                "selectionBehavior": constants.SEGUI_DEFAULT_BEHAVIOR,
                "separatorRequired": false,
                "showScrollbars": false,
                "top": "61px",
                "viewType": constants.SEGUI_VIEW_TYPE_TABLEVIEW,
                "widgetDataMap": {},
                "width": "100%",
                "zIndex": 1
            }, {
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var rtxNoResultsFound = new kony.ui.RichText({
                "bottom": "150px",
                "centerX": "50%",
                "id": "rtxNoResultsFound",
                "isVisible": false,
                "skin": "sknRtxLato84939e12Px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmLogs.rtxNoResultsFound\")",
                "top": "150px",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_CENTER,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxUserReport.add(flxUsersReportHeader, flxUserReportHeaderSeperator, segListing, rtxNoResultsFound);
            flxUsersReport.add(datePickerUsers, flxCloseCal3, search, flxUserReport);
            var flxTransactionsReport = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "bottom": "0px",
                "clipBounds": true,
                "id": "flxTransactionsReport",
                "isVisible": false,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "isModalContainer": false,
                "right": "0px",
                "skin": "slFbox",
                "top": "0px",
                "zIndex": 1
            }, {}, {});
            flxTransactionsReport.setDefaultUnit(kony.flex.DP);
            var datePickerTransactions = new kony.ui.CustomWidget({
                "id": "datePickerTransactions",
                "isVisible": true,
                "left": "1px",
                "right": "3px",
                "bottom": "1px",
                "top": "48px",
                "width": "175px",
                "height": "30px",
                "zIndex": 10,
                "clipBounds": true
            }, {
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {
                "widgetName": "dateRangePicker",
                "drops": "down",
                "event": null,
                "maxDate": "true",
                "opens": "right",
                "rangeType": null,
                "resetData": "Today",
                "type": "list",
                "value": ""
            });
            var flxCloseCal1 = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "15px",
                "id": "flxCloseCal1",
                "isVisible": false,
                "layoutType": kony.flex.FREE_FORM,
                "left": "178px",
                "isModalContainer": false,
                "right": "15px",
                "skin": "slFbox",
                "top": "52px",
                "width": "15px",
                "zIndex": 11
            }, {}, {
                "hoverSkin": "sknCursor"
            });
            flxCloseCal1.setDefaultUnit(kony.flex.DP);
            var lblCloseCal1 = new kony.ui.Label({
                "centerX": "50%",
                "centerY": "50%",
                "id": "lblCloseCal1",
                "isVisible": true,
                "skin": "sknfontIconDescDownArrow12px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmAlertsManagement.lblAlertPreviewClose\")",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxCloseCal1.add(lblCloseCal1);
            var search1 = new com.adminConsole.reports.search({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "height": "80px",
                "id": "search1",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "masterType": constants.MASTER_TYPE_DEFAULT,
                "isModalContainer": false,
                "right": "0px",
                "skin": "sknFlx",
                "top": "0dp",
                "overrides": {
                    "btnAdd": {
                        "left": "220px"
                    },
                    "flxCSR": {
                        "isVisible": false
                    },
                    "flxCategory": {
                        "isVisible": false
                    },
                    "flxDownload": {
                        "right": "0px"
                    },
                    "imgClose": {
                        "src": "closetags_2x.png"
                    },
                    "imgDownload": {
                        "src": "download_2x.png"
                    },
                    "search": {
                        "height": "80px",
                        "right": "0px",
                        "width": "viz.val_cleared"
                    }
                }
            }, {
                "overrides": {}
            }, {
                "overrides": {}
            });
            var flxScrollTransactions = new kony.ui.FlexScrollContainer({
                "allowHorizontalBounce": false,
                "allowVerticalBounce": true,
                "bottom": "0px",
                "bounces": true,
                "clipBounds": true,
                "enableScrolling": true,
                "horizontalScrollIndicator": true,
                "id": "flxScrollTransactions",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0px",
                "pagingEnabled": false,
                "right": "0px",
                "scrollDirection": kony.flex.SCROLL_VERTICAL,
                "skin": "slFSbox",
                "top": "100px",
                "verticalScrollIndicator": true,
                "zIndex": 10
            }, {}, {});
            flxScrollTransactions.setDefaultUnit(kony.flex.DP);
            var segTransactionReports = new kony.ui.SegmentedUI2({
                "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                "groupCells": false,
                "id": "segTransactionReports",
                "isVisible": true,
                "left": "0px",
                "needPageIndicator": true,
                "pageOffDotImage": "pageoffdot.png",
                "pageOnDotImage": "pageondot.png",
                "retainSelection": false,
                "right": "20px",
                "rowFocusSkin": "seg2Focus",
                "sectionHeaderSkin": "sliPhoneSegmentHeader",
                "selectionBehavior": constants.SEGUI_DEFAULT_BEHAVIOR,
                "separatorColor": "ffffff64",
                "separatorRequired": true,
                "separatorThickness": 20,
                "showScrollbars": false,
                "top": "0px",
                "viewType": constants.SEGUI_VIEW_TYPE_TABLEVIEW,
                "widgetDataMap": {},
                "zIndex": 1
            }, {
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var flxNoResultsFound = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "90%",
                "id": "flxNoResultsFound",
                "isVisible": false,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "isModalContainer": false,
                "right": "50px",
                "skin": "sknflxffffffop100dbdbe6Radius3px",
                "top": "0dp",
                "zIndex": 10
            }, {}, {});
            flxNoResultsFound.setDefaultUnit(kony.flex.DP);
            var rtxNorecords = new kony.ui.RichText({
                "centerX": "50%",
                "centerY": "50%",
                "id": "rtxNorecords",
                "isVisible": true,
                "skin": "sknRtxLato84939e12Px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmLogs.rtxNorecords\")",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_CENTER,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxNoResultsFound.add(rtxNorecords);
            flxScrollTransactions.add(segTransactionReports, flxNoResultsFound);
            flxTransactionsReport.add(datePickerTransactions, flxCloseCal1, search1, flxScrollTransactions);
            var flxMessagesReport = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "bottom": "0px",
                "clipBounds": true,
                "id": "flxMessagesReport",
                "isVisible": false,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "isModalContainer": false,
                "right": "0px",
                "skin": "slFbox",
                "top": "0px",
                "zIndex": 1
            }, {}, {});
            flxMessagesReport.setDefaultUnit(kony.flex.DP);
            var datePickerMessages = new kony.ui.CustomWidget({
                "id": "datePickerMessages",
                "isVisible": true,
                "left": "1px",
                "right": "3px",
                "bottom": "1px",
                "top": "48px",
                "width": "175px",
                "height": "35px",
                "zIndex": 10,
                "clipBounds": true
            }, {
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {
                "widgetName": "dateRangePicker",
                "drops": "down",
                "event": null,
                "maxDate": "true",
                "opens": "right",
                "rangeType": null,
                "resetData": "Today",
                "type": "list",
                "value": ""
            });
            var flxCloseCal2 = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "15px",
                "id": "flxCloseCal2",
                "isVisible": false,
                "layoutType": kony.flex.FREE_FORM,
                "left": "178px",
                "isModalContainer": false,
                "right": "15px",
                "skin": "slFbox",
                "top": "52px",
                "width": "15px",
                "zIndex": 11
            }, {}, {
                "hoverSkin": "sknCursor"
            });
            flxCloseCal2.setDefaultUnit(kony.flex.DP);
            var lblCloseCal2 = new kony.ui.Label({
                "centerX": "50%",
                "centerY": "50%",
                "id": "lblCloseCal2",
                "isVisible": true,
                "skin": "sknfontIconDescDownArrow12px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmAlertsManagement.lblAlertPreviewClose\")",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxCloseCal2.add(lblCloseCal2);
            var search2 = new com.adminConsole.reports.search({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "height": "80px",
                "id": "search2",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "masterType": constants.MASTER_TYPE_DEFAULT,
                "isModalContainer": false,
                "right": "0px",
                "skin": "slFbox",
                "top": "0dp",
                "overrides": {
                    "flxDownload": {
                        "right": "0px"
                    },
                    "imgClose": {
                        "src": "closetags_2x.png"
                    },
                    "imgDownload": {
                        "src": "download_2x.png"
                    },
                    "tbxCSR": {
                        "i18n_placeholder": "kony.i18n.getLocalizedString(\"i18n.frmReportsManagement.searchCSRname\")"
                    }
                }
            }, {
                "overrides": {}
            }, {
                "overrides": {}
            });
            var flxContent = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "bottom": "0px",
                "clipBounds": true,
                "id": "flxContent",
                "isVisible": true,
                "layoutType": kony.flex.FLOW_HORIZONTAL,
                "left": "0px",
                "isModalContainer": false,
                "right": "0px",
                "skin": "slFbox",
                "top": "100px",
                "zIndex": 10
            }, {}, {});
            flxContent.setDefaultUnit(kony.flex.DP);
            var flxMessages = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                "clipBounds": true,
                "id": "flxMessages",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0px",
                "isModalContainer": false,
                "skin": "sknFlxCalendar",
                "top": "0px",
                "width": "49%",
                "zIndex": 1
            }, {}, {});
            flxMessages.setDefaultUnit(kony.flex.DP);
            var flxHeader = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "60dp",
                "id": "flxHeader",
                "isVisible": true,
                "layoutType": kony.flex.FLOW_HORIZONTAL,
                "left": "20px",
                "isModalContainer": false,
                "right": "20px",
                "skin": "slFbox",
                "top": "0px",
                "zIndex": 1
            }, {}, {});
            flxHeader.setDefaultUnit(kony.flex.DP);
            var flxMessage = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "100%",
                "id": "flxMessage",
                "isVisible": true,
                "layoutType": kony.flex.FLOW_HORIZONTAL,
                "left": "0px",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0dp",
                "width": "65%",
                "zIndex": 1
            }, {}, {});
            flxMessage.setDefaultUnit(kony.flex.DP);
            var lblMessage = new kony.ui.Label({
                "centerY": "50%",
                "id": "lblMessage",
                "isVisible": true,
                "left": 20,
                "skin": "sknlblLato696c7312px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.leftmenu.MESSAGES\")",
                "top": 0,
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxMessage.add(lblMessage);
            var flxMessageValue = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "100%",
                "id": "flxMessageValue",
                "isVisible": true,
                "layoutType": kony.flex.FLOW_HORIZONTAL,
                "left": "0%",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0dp",
                "width": "35%",
                "zIndex": 1
            }, {}, {});
            flxMessageValue.setDefaultUnit(kony.flex.DP);
            var lblMessageValue = new kony.ui.Label({
                "centerY": "50%",
                "id": "lblMessageValue",
                "isVisible": true,
                "left": "0px",
                "skin": "sknlblLato696c7312px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmReportsManagement.lblMessageValue\")",
                "top": 0,
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxMessageValue.add(lblMessageValue);
            flxHeader.add(flxMessage, flxMessageValue);
            var flxMessageHeaderSeperator = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "bottom": "1dp",
                "clipBounds": true,
                "height": "1dp",
                "id": "flxMessageHeaderSeperator",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "20dp",
                "isModalContainer": false,
                "right": "20dp",
                "skin": "sknTableHeaderLine",
                "top": "60px",
                "zIndex": 1
            }, {}, {});
            flxMessageHeaderSeperator.setDefaultUnit(kony.flex.DP);
            flxMessageHeaderSeperator.add();
            var segMessages = new kony.ui.SegmentedUI2({
                "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                "bottom": "20px",
                "groupCells": false,
                "id": "segMessages",
                "isVisible": true,
                "left": "0px",
                "maxHeight": "500px",
                "needPageIndicator": true,
                "pageOffDotImage": "pageoffdot.png",
                "pageOnDotImage": "pageondot.png",
                "retainSelection": false,
                "rowFocusSkin": "seg2Focus",
                "rowSkin": "seg2Normal",
                "sectionHeaderSkin": "sliPhoneSegmentHeader",
                "selectionBehavior": constants.SEGUI_DEFAULT_BEHAVIOR,
                "separatorRequired": false,
                "showScrollbars": false,
                "top": "61px",
                "viewType": constants.SEGUI_VIEW_TYPE_TABLEVIEW,
                "widgetDataMap": {},
                "width": "100%",
                "zIndex": 1
            }, {
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxMessages.add(flxHeader, flxMessageHeaderSeperator, segMessages);
            var flxThreads = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                "clipBounds": true,
                "id": "flxThreads",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "20px",
                "isModalContainer": false,
                "skin": "sknFlxCalendar",
                "top": "0px",
                "width": "49%",
                "zIndex": 1
            }, {}, {});
            flxThreads.setDefaultUnit(kony.flex.DP);
            var flxThreadsHeader = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "60dp",
                "id": "flxThreadsHeader",
                "isVisible": true,
                "layoutType": kony.flex.FLOW_HORIZONTAL,
                "left": "20px",
                "isModalContainer": false,
                "right": "20px",
                "skin": "slFbox",
                "top": "0px",
                "zIndex": 1
            }, {}, {});
            flxThreadsHeader.setDefaultUnit(kony.flex.DP);
            var flxThread = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "100%",
                "id": "flxThread",
                "isVisible": true,
                "layoutType": kony.flex.FLOW_HORIZONTAL,
                "left": "0px",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0dp",
                "width": "65%",
                "zIndex": 1
            }, {}, {});
            flxThread.setDefaultUnit(kony.flex.DP);
            var lblThread = new kony.ui.Label({
                "centerY": "50%",
                "id": "lblThread",
                "isVisible": true,
                "left": 20,
                "skin": "sknlblLato696c7312px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmReportsManagement.lblThread\")",
                "top": 0,
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxThread.add(lblThread);
            var flxThreadValue = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "100%",
                "id": "flxThreadValue",
                "isVisible": true,
                "layoutType": kony.flex.FLOW_HORIZONTAL,
                "left": "0%",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0dp",
                "width": "35%",
                "zIndex": 1
            }, {}, {});
            flxThreadValue.setDefaultUnit(kony.flex.DP);
            var lblThreadValue = new kony.ui.Label({
                "centerY": "50%",
                "id": "lblThreadValue",
                "isVisible": true,
                "left": "0px",
                "skin": "sknlblLato696c7312px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmReportsManagement.lblMessageValue\")",
                "top": 0,
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxThreadValue.add(lblThreadValue);
            flxThreadsHeader.add(flxThread, flxThreadValue);
            var flxThreadHeader = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "bottom": "1dp",
                "clipBounds": true,
                "height": "1dp",
                "id": "flxThreadHeader",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "20dp",
                "isModalContainer": false,
                "right": "20dp",
                "skin": "sknTableHeaderLine",
                "top": "60px",
                "zIndex": 1
            }, {}, {});
            flxThreadHeader.setDefaultUnit(kony.flex.DP);
            flxThreadHeader.add();
            var segThreads = new kony.ui.SegmentedUI2({
                "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                "bottom": "20px",
                "groupCells": false,
                "id": "segThreads",
                "isVisible": true,
                "left": "0px",
                "maxHeight": "500px",
                "needPageIndicator": true,
                "pageOffDotImage": "pageoffdot.png",
                "pageOnDotImage": "pageondot.png",
                "retainSelection": false,
                "rowFocusSkin": "seg2Focus",
                "rowSkin": "seg2Normal",
                "sectionHeaderSkin": "sliPhoneSegmentHeader",
                "selectionBehavior": constants.SEGUI_DEFAULT_BEHAVIOR,
                "separatorRequired": false,
                "showScrollbars": false,
                "top": "61px",
                "viewType": constants.SEGUI_VIEW_TYPE_TABLEVIEW,
                "widgetDataMap": {},
                "width": "100%",
                "zIndex": 1
            }, {
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxThreads.add(flxThreadsHeader, flxThreadHeader, segThreads);
            flxContent.add(flxMessages, flxThreads);
            var flxSuggestions = new kony.ui.FlexScrollContainer({
                "allowHorizontalBounce": false,
                "allowVerticalBounce": true,
                "bounces": true,
                "clipBounds": true,
                "enableScrolling": true,
                "height": "200px",
                "horizontalScrollIndicator": true,
                "id": "flxSuggestions",
                "isVisible": true,
                "layoutType": kony.flex.FLOW_VERTICAL,
                "left": "440px",
                "pagingEnabled": false,
                "scrollDirection": kony.flex.SCROLL_VERTICAL,
                "skin": "sknscrflxffffffop0d0b87507b0274eSe",
                "top": "80px",
                "verticalScrollIndicator": true,
                "width": "200px",
                "zIndex": 20
            }, {}, {});
            flxSuggestions.setDefaultUnit(kony.flex.DP);
            var segUserSuggestion = new kony.ui.SegmentedUI2({
                "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                "centerX": "50%",
                "data": [{
                    "lblName": "Internal user"
                }, {
                    "lblName": "Internal user"
                }, {
                    "lblName": "Internal user"
                }, {
                    "lblName": "Internal user"
                }, {
                    "lblName": "Internal user"
                }],
                "groupCells": false,
                "id": "segUserSuggestion",
                "isVisible": true,
                "needPageIndicator": true,
                "pageOffDotImage": "pageoffdot.png",
                "pageOnDotImage": "pageondot.png",
                "retainSelection": false,
                "rowFocusSkin": "seg2Focus",
                "rowSkin": "seg2Normal",
                "rowTemplate": "flxReportsMangSuggestions",
                "sectionHeaderSkin": "sliPhoneSegmentHeader",
                "selectionBehavior": constants.SEGUI_DEFAULT_BEHAVIOR,
                "separatorRequired": false,
                "showScrollbars": false,
                "top": "10px",
                "viewType": constants.SEGUI_VIEW_TYPE_TABLEVIEW,
                "widgetDataMap": {
                    "flxReportsMangSuggestions": "flxReportsMangSuggestions",
                    "lblName": "lblName"
                },
                "width": "100%",
                "zIndex": 20
            }, {
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var richtextNoResult = new kony.ui.RichText({
                "centerX": "50%",
                "centerY": "50%",
                "id": "richtextNoResult",
                "isVisible": false,
                "left": "0dp",
                "skin": "sknrichtext12pxlatoregular",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmCSR.richtextNoResult\")",
                "top": "0dp",
                "width": "100%",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_CENTER,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxSuggestions.add(segUserSuggestion, richtextNoResult);
            flxMessagesReport.add(datePickerMessages, flxCloseCal2, search2, flxContent, flxSuggestions);
            flxMainContainer.add(flxUsersReport, flxTransactionsReport, flxMessagesReport);
            var flxReportsTabs = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "140px",
                "id": "flxReportsTabs",
                "isVisible": false,
                "layoutType": kony.flex.FLOW_HORIZONTAL,
                "left": "20dp",
                "isModalContainer": false,
                "right": "20px",
                "skin": "slFbox",
                "top": "20px",
                "zIndex": 1
            }, {}, {});
            flxReportsTabs.setDefaultUnit(kony.flex.DP);
            var flxReportsTabs1 = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "140px",
                "id": "flxReportsTabs1",
                "isVisible": false,
                "layoutType": kony.flex.FLOW_HORIZONTAL,
                "left": "20dp",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0dp",
                "width": "30.80%"
            }, {}, {});
            flxReportsTabs1.setDefaultUnit(kony.flex.DP);
            var usersReport = new com.adminConsole.logs.LogDefaultTabs({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "height": "100%",
                "id": "usersReport",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0px",
                "masterType": constants.MASTER_TYPE_DEFAULT,
                "isModalContainer": false,
                "skin": "sknflxffffffop100dbdbe6Radius3px",
                "top": "0px",
                "width": "100%",
                "overrides": {
                    "LogDefaultTabs": {
                        "bottom": "viz.val_cleared",
                        "centerX": "viz.val_cleared",
                        "centerY": "viz.val_cleared",
                        "height": "100%",
                        "left": "0px",
                        "maxHeight": "viz.val_cleared",
                        "maxWidth": "viz.val_cleared",
                        "minHeight": "viz.val_cleared",
                        "minWidth": "viz.val_cleared",
                        "right": "viz.val_cleared",
                        "top": "0px",
                        "width": "100%"
                    },
                    "imgLog": {
                        "src": "users_2x.png"
                    },
                    "lblLog": {
                        "centerY": "50%",
                        "height": "60dp",
                        "left": "33dp",
                        "top": "30dp",
                        "width": "70dp"
                    },
                    "lblLogDesc": {
                        "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmReportsManagementController.UserReportsTabDesc\")"
                    },
                    "lblLogHeading": {
                        "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmReportsManagementController.UserReportsTabHeading\")"
                    }
                }
            }, {
                "overrides": {}
            }, {
                "overrides": {}
            });
            flxReportsTabs1.add(usersReport);
            var flxReportsTabs2 = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "140px",
                "id": "flxReportsTabs2",
                "isVisible": false,
                "layoutType": kony.flex.FLOW_HORIZONTAL,
                "left": "20px",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0dp",
                "width": "30.80%"
            }, {}, {});
            flxReportsTabs2.setDefaultUnit(kony.flex.DP);
            var transactionReport = new com.adminConsole.logs.LogDefaultTabs({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "height": "100%",
                "id": "transactionReport",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0px",
                "masterType": constants.MASTER_TYPE_DEFAULT,
                "isModalContainer": false,
                "skin": "sknflxffffffop100dbdbe6Radius3px",
                "top": "0px",
                "width": "100%",
                "overrides": {
                    "LogDefaultTabs": {
                        "bottom": "viz.val_cleared",
                        "centerX": "viz.val_cleared",
                        "centerY": "viz.val_cleared",
                        "height": "100%",
                        "left": "0px",
                        "maxHeight": "viz.val_cleared",
                        "maxWidth": "viz.val_cleared",
                        "minHeight": "viz.val_cleared",
                        "minWidth": "viz.val_cleared",
                        "right": "viz.val_cleared",
                        "top": "0px",
                        "width": "100%"
                    },
                    "imgLog": {
                        "src": "transactions_2x.png"
                    },
                    "lblLog": {
                        "centerY": "50%",
                        "height": "60dp",
                        "left": "33dp",
                        "top": "30dp",
                        "width": "70dp"
                    },
                    "lblLogDesc": {
                        "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmReportsManagementController.TransactionalReportsTabDesc\")"
                    },
                    "lblLogHeading": {
                        "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmReportsManagementController.TransactionalReportsTabHeading\")"
                    }
                }
            }, {
                "overrides": {}
            }, {
                "overrides": {}
            });
            flxReportsTabs2.add(transactionReport);
            var flxReportsTabs3 = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "140px",
                "id": "flxReportsTabs3",
                "isVisible": true,
                "layoutType": kony.flex.FLOW_HORIZONTAL,
                "left": "0px",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0dp",
                "width": "30.80%"
            }, {}, {});
            flxReportsTabs3.setDefaultUnit(kony.flex.DP);
            var messagesReport = new com.adminConsole.logs.LogDefaultTabs({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "height": "100%",
                "id": "messagesReport",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0.00%",
                "masterType": constants.MASTER_TYPE_DEFAULT,
                "isModalContainer": false,
                "skin": "sknflxffffffop100dbdbe6Radius3px",
                "top": "0dp",
                "width": "100%",
                "overrides": {
                    "LogDefaultTabs": {
                        "bottom": "viz.val_cleared",
                        "centerX": "viz.val_cleared",
                        "centerY": "viz.val_cleared",
                        "height": "100%",
                        "left": "0.00%",
                        "maxHeight": "viz.val_cleared",
                        "maxWidth": "viz.val_cleared",
                        "minHeight": "viz.val_cleared",
                        "minWidth": "viz.val_cleared",
                        "right": "viz.val_cleared",
                        "top": "0dp",
                        "width": "100%"
                    },
                    "imgLog": {
                        "src": "messages_2x.png"
                    },
                    "lblLog": {
                        "centerX": "viz.val_cleared",
                        "centerY": "50%",
                        "height": "60dp",
                        "left": "33dp",
                        "top": "viz.val_cleared",
                        "width": "70dp"
                    },
                    "lblLogDesc": {
                        "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmReportsManagementController.MessageReportsTabDesc\")"
                    },
                    "lblLogHeading": {
                        "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmReportsManagementController.MessageReportsTabHeading\")"
                    }
                }
            }, {
                "overrides": {}
            }, {
                "overrides": {}
            });
            flxReportsTabs3.add(messagesReport);
            flxReportsTabs.add(flxReportsTabs1, flxReportsTabs2, flxReportsTabs3);
            var flxViewReports = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "bottom": "0dp",
                "clipBounds": true,
                "id": "flxViewReports",
                "isVisible": false,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "isModalContainer": false,
                "right": "0dp",
                "skin": "slFbox",
                "top": "0dp",
                "zIndex": 1
            }, {}, {});
            flxViewReports.setDefaultUnit(kony.flex.DP);
            var flxViewContainer = new kony.ui.FlexScrollContainer({
                "allowHorizontalBounce": false,
                "allowVerticalBounce": true,
                "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                "bottom": "0px",
                "bounces": true,
                "clipBounds": true,
                "enableScrolling": true,
                "horizontalScrollIndicator": true,
                "id": "flxViewContainer",
                "isVisible": true,
                "layoutType": kony.flex.FLOW_VERTICAL,
                "left": "0dp",
                "pagingEnabled": false,
                "right": "0px",
                "scrollDirection": kony.flex.SCROLL_VERTICAL,
                "skin": "slFSbox",
                "top": "0dp",
                "verticalScrollIndicator": true,
                "zIndex": 1
            }, {}, {});
            flxViewContainer.setDefaultUnit(kony.flex.DP);
            var flxViewDetails = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                "clipBounds": true,
                "id": "flxViewDetails",
                "isVisible": true,
                "layoutType": kony.flex.FLOW_VERTICAL,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0dp",
                "width": "100%",
                "zIndex": 2
            }, {}, {});
            flxViewDetails.setDefaultUnit(kony.flex.DP);
            var flxReportViewContainer = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                "clipBounds": true,
                "id": "flxReportViewContainer",
                "isVisible": true,
                "layoutType": kony.flex.FLOW_VERTICAL,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0dp",
                "width": "100%",
                "zIndex": 1
            }, {}, {});
            flxReportViewContainer.setDefaultUnit(kony.flex.DP);
            var flxViewDescription = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": false,
                "height": "60dp",
                "id": "flxViewDescription",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0px",
                "isModalContainer": false,
                "right": "0px",
                "skin": "slFbox",
                "top": "0px",
                "zIndex": 2
            }, {}, {});
            flxViewDescription.setDefaultUnit(kony.flex.DP);
            var flxRoleDetails = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "100%",
                "id": "flxRoleDetails",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "20dp",
                "isModalContainer": false,
                "right": "50dp",
                "skin": "slFbox",
                "top": "0dp"
            }, {}, {});
            flxRoleDetails.setDefaultUnit(kony.flex.DP);
            var flxToggle = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "centerY": "50%",
                "clipBounds": true,
                "height": "20dp",
                "id": "flxToggle",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "isModalContainer": false,
                "right": "5px",
                "skin": "slFbox",
                "width": "20px",
                "zIndex": 1
            }, {}, {
                "hoverSkin": "sknCursor"
            });
            flxToggle.setDefaultUnit(kony.flex.DP);
            var imgToggleDescription = new kony.ui.Image2({
                "centerX": "50%",
                "centerY": "50%",
                "height": "10dp",
                "id": "imgToggleDescription",
                "isVisible": false,
                "skin": "slImage",
                "src": "img_down_arrow.png",
                "width": "10dp",
                "zIndex": 1
            }, {
                "imageScaleMode": constants.IMAGE_SCALE_MODE_MAINTAIN_ASPECT_RATIO,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var lblToggleDescription = new kony.ui.Label({
                "centerY": "50%",
                "height": "12dp",
                "id": "lblToggleDescription",
                "isVisible": true,
                "left": "0dp",
                "skin": "sknIcon12pxBlack",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.userwidgetmodel.fontIconBreadcrumbsDown\")",
                "width": "12dp",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxToggle.add(imgToggleDescription, lblToggleDescription);
            var lblDescriptionReport = new kony.ui.Label({
                "centerY": "50.00%",
                "id": "lblDescriptionReport",
                "isVisible": true,
                "left": "21dp",
                "right": "20dp",
                "skin": "sknLbl192b45Lato15px",
                "text": "2019 Credit Report",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxRoleDetails.add(flxToggle, lblDescriptionReport);
            var flxDeleteOption = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "centerY": "50%",
                "clipBounds": true,
                "height": "20dp",
                "id": "flxDeleteOption",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "isModalContainer": false,
                "right": "18px",
                "skin": "sknFlxPointer",
                "top": "0px",
                "width": "20px",
                "zIndex": 3
            }, {}, {
                "hoverSkin": "sknFlxPointer"
            });
            flxDeleteOption.setDefaultUnit(kony.flex.DP);
            var lblIconOptions = new kony.ui.Label({
                "centerX": "50%",
                "centerY": "50%",
                "id": "lblIconOptions",
                "isVisible": true,
                "skin": "sknIcon20px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.userwidgetmodel.lblIconOption2\")",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {
                "hoverSkin": "sknIcon20pxWhiteHover"
            });
            flxDeleteOption.add(lblIconOptions);
            flxViewDescription.add(flxRoleDetails, flxDeleteOption);
            var flxToggleContent = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                "bottom": "20dp",
                "clipBounds": true,
                "id": "flxToggleContent",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "isModalContainer": false,
                "right": "20dp",
                "skin": "slFbox",
                "top": "0dp",
                "width": "100%"
            }, {}, {});
            flxToggleContent.setDefaultUnit(kony.flex.DP);
            var flxViewRow1 = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                "clipBounds": true,
                "id": "flxViewRow1",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "20dp",
                "isModalContainer": false,
                "right": "0dp",
                "skin": "slFbox",
                "top": "0dp"
            }, {}, {});
            flxViewRow1.setDefaultUnit(kony.flex.DP);
            var flxDescriptionField = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                "clipBounds": true,
                "id": "flxDescriptionField",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "isModalContainer": false,
                "right": "20dp",
                "skin": "slFbox",
                "top": "0dp",
                "width": "35%",
                "zIndex": 1
            }, {}, {});
            flxDescriptionField.setDefaultUnit(kony.flex.DP);
            var lblDesc = new kony.ui.Label({
                "id": "lblDesc",
                "isVisible": true,
                "left": "21dp",
                "skin": "sknlblLato696c7311px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmTrackApplication.DescriptionCAPS\")",
                "top": "1dp",
                "width": "90px",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var lblDescriptionValue = new kony.ui.Label({
                "id": "lblDescriptionValue",
                "isVisible": true,
                "left": "21px",
                "right": "5dp",
                "skin": "sknlbl485C75LatoSemiBold13px",
                "text": "This Report includes users of all categories belonging to different age groups.",
                "top": "25px",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxDescriptionField.add(lblDesc, lblDescriptionValue);
            var flxDataSourceField = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "50dp",
                "id": "flxDataSourceField",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "36%",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0dp",
                "width": "30%",
                "zIndex": 1
            }, {}, {});
            flxDataSourceField.setDefaultUnit(kony.flex.DP);
            var lblDataSourceField = new kony.ui.Label({
                "id": "lblDataSourceField",
                "isVisible": true,
                "left": "0dp",
                "skin": "sknlblLato696c7311px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmReportsManagement.label.datasource_caps\")",
                "top": "1dp",
                "width": "90px",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var lblDataSourceValue = new kony.ui.Label({
                "id": "lblDataSourceValue",
                "isVisible": true,
                "left": "0px",
                "skin": "sknlbl485C75LatoSemiBold13px",
                "text": "Fabric",
                "top": "25px",
                "width": "100%",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxDataSourceField.add(lblDataSourceField, lblDataSourceValue);
            var flxCreatedOn = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "50dp",
                "id": "flxCreatedOn",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "68%",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0dp",
                "width": "30%",
                "zIndex": 1
            }, {}, {});
            flxCreatedOn.setDefaultUnit(kony.flex.DP);
            var lblCreatedOnField = new kony.ui.Label({
                "id": "lblCreatedOnField",
                "isVisible": true,
                "left": "0dp",
                "skin": "sknlblLato696c7311px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmLoansDashboard.CreatedOn\")",
                "top": "1dp",
                "width": "90px",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var lblCreatedOnValue = new kony.ui.Label({
                "id": "lblCreatedOnValue",
                "isVisible": true,
                "left": "0px",
                "skin": "sknlbl485C75LatoSemiBold13px",
                "text": "31/01/2019",
                "top": "25px",
                "width": "100%",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxCreatedOn.add(lblCreatedOnField, lblCreatedOnValue);
            flxViewRow1.add(flxDescriptionField, flxDataSourceField, flxCreatedOn);
            var btnEdit = new kony.ui.Button({
                "focusSkin": "sknbtnf7f7faLatoRegular12Px485c75Border1px485C75Radius20px",
                "height": "22dp",
                "id": "btnEdit",
                "isVisible": false,
                "right": "20dp",
                "skin": "sknbtnf7f7faLatoRegular12Px485c75Border1px485C75Radius20px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.roles.Edit\")",
                "top": "15dp",
                "width": "54dp",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_CENTER,
                "displayText": true,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {
                "hoverSkin": "sknbtnffffffLatoRegular12Px485c75Border1px485C75Radius20px"
            });
            var flxEditDisableSkin = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "25dp",
                "id": "flxEditDisableSkin",
                "isVisible": false,
                "layoutType": kony.flex.FREE_FORM,
                "isModalContainer": false,
                "right": "35dp",
                "skin": "sknflxbgffffffOp50Per",
                "top": "15dp",
                "width": "60dp",
                "zIndex": 2
            }, {}, {
                "hoverSkin": "sknCursorDisabled"
            });
            flxEditDisableSkin.setDefaultUnit(kony.flex.DP);
            flxEditDisableSkin.add();
            flxToggleContent.add(flxViewRow1, btnEdit, flxEditDisableSkin);
            flxReportViewContainer.add(flxViewDescription, flxToggleContent);
            flxViewDetails.add(flxReportViewContainer);
            var flxParameters = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                "clipBounds": false,
                "id": "flxParameters",
                "isVisible": true,
                "layoutType": kony.flex.FLOW_VERTICAL,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0dp",
                "width": "100%",
                "zIndex": 2
            }, {}, {});
            flxParameters.setDefaultUnit(kony.flex.DP);
            var flxSeparator1 = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "1px",
                "id": "flxSeparator1",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "isModalContainer": false,
                "right": "0dp",
                "skin": "sknFlxBorE1E5ED1pxKA",
                "top": "0dp",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 2
            }, {}, {});
            flxSeparator1.setDefaultUnit(kony.flex.DP);
            flxSeparator1.add();
            var lblDefineParameters = new kony.ui.Label({
                "id": "lblDefineParameters",
                "isVisible": true,
                "left": "20dp",
                "skin": "sknLbl192b45Lato15px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmReportsManagement.label.defineParameters\")",
                "top": 20,
                "width": kony.flex.USE_PREFFERED_SIZE
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var flxParamsContainer = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                "clipBounds": false,
                "id": "flxParamsContainer",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0dp",
                "width": "100%",
                "zIndex": 2
            }, {}, {});
            flxParamsContainer.setDefaultUnit(kony.flex.DP);
            var flxParametersContent = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                "clipBounds": false,
                "id": "flxParametersContent",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "20dp",
                "isModalContainer": false,
                "right": "0dp",
                "skin": "slFbox",
                "top": "20dp",
                "zIndex": 2
            }, {}, {});
            flxParametersContent.setDefaultUnit(kony.flex.DP);
            flxParametersContent.add();
            flxParamsContainer.add(flxParametersContent);
            var btnsGenerateReport = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "bottom": "20dp",
                "clipBounds": false,
                "height": "30px",
                "id": "btnsGenerateReport",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "20dp",
                "width": "100%",
                "zIndex": 1
            }, {}, {});
            btnsGenerateReport.setDefaultUnit(kony.flex.DP);
            var btnGenerateReport = new kony.ui.Button({
                "centerY": "50%",
                "focusSkin": "sknBtn003E75LatoRegular13pxFFFFFFRad20px",
                "height": "30dp",
                "id": "btnGenerateReport",
                "isVisible": true,
                "right": "20px",
                "skin": "sknBtn003E75LatoRegular13pxFFFFFFRad20px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmReportsManagement.label.generateReport_caps\")",
                "top": "0%",
                "width": "178dp",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_CENTER,
                "displayText": true,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {
                "hoverSkin": "sknBtn005198LatoRegular13pxFFFFFFRad20px"
            });
            btnsGenerateReport.add(btnGenerateReport);
            flxParameters.add(flxSeparator1, lblDefineParameters, flxParamsContainer, btnsGenerateReport);
            var flxChart = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "bottom": "0dp",
                "clipBounds": true,
                "height": "555dp",
                "id": "flxChart",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0dp",
                "width": kony.flex.USE_PREFFERED_SIZE
            }, {}, {});
            flxChart.setDefaultUnit(kony.flex.DP);
            var flxChartSeparator = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "1px",
                "id": "flxChartSeparator",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "isModalContainer": false,
                "right": "0dp",
                "skin": "sknFlxBorE1E5ED1pxKA",
                "top": "0dp",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 2
            }, {}, {});
            flxChartSeparator.setDefaultUnit(kony.flex.DP);
            flxChartSeparator.add();
            var flxChartDisplay = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                "clipBounds": true,
                "id": "flxChartDisplay",
                "isVisible": true,
                "layoutType": kony.flex.FLOW_VERTICAL,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0dp",
                "width": "100%"
            }, {}, {});
            flxChartDisplay.setDefaultUnit(kony.flex.DP);
            var flxDownloadShare = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                "clipBounds": true,
                "id": "flxDownloadShare",
                "isVisible": true,
                "layoutType": kony.flex.FLOW_HORIZONTAL,
                "reverseLayoutDirection": true,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "20dp",
                "width": "100%"
            }, {}, {});
            flxDownloadShare.setDefaultUnit(kony.flex.DP);
            var flxBtnShare = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "22dp",
                "id": "flxBtnShare",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "isModalContainer": false,
                "right": 20,
                "skin": "sknFlxBGF7F7FAFC485C75R20pxBor485C75",
                "top": "0dp",
                "width": "92px"
            }, {}, {
                "hoverSkin": "sknflxBorder485c75Radius20PxPointer"
            });
            flxBtnShare.setDefaultUnit(kony.flex.DP);
            var lblIconOption1 = new kony.ui.Label({
                "centerY": "50%",
                "height": "19px",
                "id": "lblIconOption1",
                "isVisible": true,
                "left": "9px",
                "skin": "sknIcon18px",
                "text": "",
                "width": "19px",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var lblOption1 = new kony.ui.Label({
                "centerY": "50%",
                "id": "lblOption1",
                "isVisible": true,
                "left": "38dp",
                "skin": "sknLblLato485c7513px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.LocateUs.SHARE\")",
                "top": "0dp",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxBtnShare.add(lblIconOption1, lblOption1);
            var flxDownload = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "22dp",
                "id": "flxDownload",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "isModalContainer": false,
                "right": 20,
                "skin": "sknFlxBGF7F7FAFC485C75R20pxBor485C75",
                "top": "0dp",
                "width": "119px"
            }, {}, {
                "hoverSkin": "sknflxBorder485c75Radius20PxPointer"
            });
            flxDownload.setDefaultUnit(kony.flex.DP);
            var lblIconOption2 = new kony.ui.Label({
                "centerY": "50%",
                "height": "19px",
                "id": "lblIconOption2",
                "isVisible": true,
                "left": "9px",
                "skin": "sknIcon18px",
                "text": "",
                "width": "19px",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var lblOption2 = new kony.ui.Label({
                "centerY": "50%",
                "id": "lblOption2",
                "isVisible": true,
                "left": "38dp",
                "skin": "sknLblLato485c7513px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.common.Download\")",
                "top": "0dp",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxDownload.add(lblIconOption2, lblOption2);
            flxDownloadShare.add(flxBtnShare, flxDownload);
            var flxChartView = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                "bottom": "20dp",
                "centerX": "50%",
                "clipBounds": true,
                "id": "flxChartView",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0",
                "width": kony.flex.USE_PREFFERED_SIZE
            }, {}, {});
            flxChartView.setDefaultUnit(kony.flex.DP);
            flxChartView.add();
            flxChartDisplay.add(flxDownloadShare, flxChartView);
            var flxAddParametersMessage = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                "centerY": "50%",
                "clipBounds": true,
                "id": "flxAddParametersMessage",
                "isVisible": false,
                "layoutType": kony.flex.FLOW_VERTICAL,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "slFbox",
                "width": "100%"
            }, {}, {});
            flxAddParametersMessage.setDefaultUnit(kony.flex.DP);
            var imgAddParams = new kony.ui.Image2({
                "centerX": "50%",
                "height": "61dp",
                "id": "imgAddParams",
                "isVisible": true,
                "left": "0",
                "skin": "slImage",
                "src": "generate_reports.png",
                "top": "0",
                "width": "83dp"
            }, {
                "imageScaleMode": constants.IMAGE_SCALE_MODE_MAINTAIN_ASPECT_RATIO,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var lblAddParams = new kony.ui.Label({
                "centerX": "50%",
                "id": "lblAddParams",
                "isVisible": true,
                "skin": "sknLbl485C75LatoRegular13Px",
                "text": "Add Parameters and click generate to visualize reports",
                "top": "20dp",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxAddParametersMessage.add(imgAddParams, lblAddParams);
            flxChart.add(flxChartSeparator, flxChartDisplay, flxAddParametersMessage);
            flxViewContainer.add(flxViewDetails, flxParameters, flxChart);
            var assignContextualMenu = new com.adminConsole.common.contextualMenu1({
                "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                "id": "assignContextualMenu",
                "isVisible": false,
                "layoutType": kony.flex.FLOW_VERTICAL,
                "masterType": constants.MASTER_TYPE_DEFAULT,
                "isModalContainer": false,
                "right": "12dp",
                "skin": "slFbox",
                "top": "43px",
                "width": "142dp",
                "zIndex": 2,
                "overrides": {
                    "btnLink1": {
                        "isVisible": false,
                        "text": "Features and Actions",
                        "top": "15dp"
                    },
                    "btnLink2": {
                        "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmGroupsController.Customers\")",
                        "isVisible": false
                    },
                    "contextualMenu1": {
                        "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                        "isVisible": false,
                        "left": "viz.val_cleared",
                        "right": "12dp",
                        "top": "43px",
                        "width": "142dp",
                        "zIndex": 2
                    },
                    "contextualMenu1Inner": {
                        "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                        "left": "0px",
                        "right": "viz.val_cleared",
                        "width": "100%",
                        "layoutType": kony.flex.FREE_FORM
                    },
                    "flxDownArrowImage": {
                        "isVisible": false
                    },
                    "flxOption1": {
                        "height": "30dp",
                        "top": "10dp"
                    },
                    "flxOption2": {
                        "bottom": "viz.val_cleared",
                        "height": "30dp",
                        "top": "40dp"
                    },
                    "flxOption3": {
                        "bottom": "10dp",
                        "height": "30dp",
                        "top": "80dp"
                    },
                    "flxOption4": {
                        "isVisible": false
                    },
                    "flxOptionsSeperator": {
                        "isVisible": true,
                        "top": "75px"
                    },
                    "flxUpArrowImage": {
                        "top": "-1px"
                    },
                    "imgDownArrow": {
                        "src": "downarrow_2x.png"
                    },
                    "imgOption1": {
                        "src": "imagedrag.png"
                    },
                    "imgOption2": {
                        "src": "edit2x.png"
                    },
                    "imgOption3": {
                        "src": "imagedrag.png"
                    },
                    "imgOption4": {
                        "src": "deactive_2x.png"
                    },
                    "imgUpArrow": {
                        "src": "uparrow_2x.png"
                    },
                    "lblHeader": {
                        "isVisible": false,
                        "top": "20px"
                    },
                    "lblIconOption1": {
                        "left": "20px"
                    },
                    "lblIconOption2": {
                        "i18n_text": "kony.i18n.getLocalizedString(\"i18n.userwidgetmodel.lblIconOption4\")",
                        "left": "20dp"
                    },
                    "lblIconOption3": {
                        "left": "20dp",
                        "text": ""
                    },
                    "lblOption1": {
                        "i18n_text": "kony.i18n.getLocalizedString(\"i18n.LeadManagement.contextualMenuEdit\")",
                        "maxHeight": "viz.val_cleared"
                    },
                    "lblOption2": {
                        "i18n_text": "kony.i18n.getLocalizedString(\"i18n.SecurityQuestions.Deactivate\")"
                    },
                    "lblOption3": {
                        "text": "Copy Role"
                    }
                }
            }, {
                "overrides": {}
            }, {
                "overrides": {}
            });
            var flxPagination = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "bottom": "0dp",
                "clipBounds": true,
                "height": "50dp",
                "id": "flxPagination",
                "isVisible": false,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "slFbox",
                "width": "100%"
            }, {}, {});
            flxPagination.setDefaultUnit(kony.flex.DP);
            var flxPaginationSeparator = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "1px",
                "id": "flxPaginationSeparator",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "isModalContainer": false,
                "right": "0dp",
                "skin": "sknFlxBorE1E5ED1pxKA",
                "top": "0dp",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 2
            }, {}, {});
            flxPaginationSeparator.setDefaultUnit(kony.flex.DP);
            flxPaginationSeparator.add();
            var reportPagination = new com.adminConsole.reports.reportPagination({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "bottom": "0dp",
                "height": "50dp",
                "id": "reportPagination",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "masterType": constants.MASTER_TYPE_DEFAULT,
                "isModalContainer": false,
                "skin": "sknBackgroundFFFFFF",
                "top": "0dp",
                "width": "100%",
                "overrides": {
                    "reportPagination": {
                        "bottom": "0dp",
                        "left": "0dp",
                        "top": "0dp"
                    }
                }
            }, {
                "overrides": {}
            }, {
                "overrides": {}
            });
            flxPagination.add(flxPaginationSeparator, reportPagination);
            flxViewReports.add(flxViewContainer, assignContextualMenu, flxPagination);
            var flxListingCommon = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "bottom": "0px",
                "clipBounds": true,
                "id": "flxListingCommon",
                "isVisible": false,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0px",
                "isModalContainer": false,
                "right": "0px",
                "skin": "slFbox",
                "top": "0px",
                "zIndex": 1
            }, {}, {});
            flxListingCommon.setDefaultUnit(kony.flex.DP);
            var flxAvailableRecords = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "40dp",
                "id": "flxAvailableRecords",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "isModalContainer": false,
                "right": "0dp",
                "skin": "slFbox",
                "top": "20dp"
            }, {}, {});
            flxAvailableRecords.setDefaultUnit(kony.flex.DP);
            var lblAvailableHeader = new kony.ui.Label({
                "centerY": "50%",
                "height": "16dp",
                "id": "lblAvailableHeader",
                "isVisible": true,
                "left": "20dp",
                "skin": "sknLbl192B45LatoRegular14px",
                "text": "22 Datasources Available",
                "top": "0",
                "width": kony.flex.USE_PREFFERED_SIZE
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var flxRecordsSearch = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "centerY": "50%",
                "clipBounds": true,
                "height": "40px",
                "id": "flxRecordsSearch",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "isModalContainer": false,
                "right": "20dp",
                "skin": "sknflxd5d9ddop100",
                "top": "0dp",
                "width": "380px",
                "zIndex": 1
            }, {}, {});
            flxRecordsSearch.setDefaultUnit(kony.flex.DP);
            var fontIconRecordsSearch = new kony.ui.Label({
                "centerY": "50%",
                "id": "fontIconRecordsSearch",
                "isVisible": true,
                "left": "12px",
                "skin": "sknFontIconSearchImg20px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.userwidgetmodel.lblSearchIcon\")",
                "top": "0dp",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var tbxRecordsSearch = new kony.ui.TextBox2({
                "autoCapitalize": constants.TEXTBOX_AUTO_CAPITALIZE_NONE,
                "centerY": "50%",
                "height": "35px",
                "id": "tbxRecordsSearch",
                "isVisible": true,
                "keyBoardStyle": constants.TEXTBOX_KEY_BOARD_STYLE_DEFAULT,
                "left": "30dp",
                "onTouchStart": controller.AS_TextField_cfab06859f0f43eca99495891ee290d5,
                "placeholder": "Search by Database Name",
                "right": "35px",
                "secureTextEntry": false,
                "skin": "skntbxffffffNoBorderlato35475f14px",
                "textInputMode": constants.TEXTBOX_INPUT_MODE_ANY,
                "zIndex": 1
            }, {
                "containerHeightMode": constants.TEXTBOX_FONT_METRICS_DRIVEN_HEIGHT,
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [2, 0, 0, 0],
                "paddingInPixel": false
            }, {
                "autoCorrect": false
            });
            var flxClearRecordsSearch = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "100%",
                "id": "flxClearRecordsSearch",
                "isVisible": false,
                "layoutType": kony.flex.FREE_FORM,
                "isModalContainer": false,
                "right": "0px",
                "skin": "sknCursor",
                "top": "0dp",
                "width": "35px",
                "zIndex": 1
            }, {}, {
                "hoverSkin": "sknCursor"
            });
            flxClearRecordsSearch.setDefaultUnit(kony.flex.DP);
            var fontIconCrossRecordsSearch = new kony.ui.Label({
                "centerY": "50%",
                "id": "fontIconCrossRecordsSearch",
                "isVisible": true,
                "left": "5px",
                "skin": "sknFontIconSearchCross16px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.userwidgetmodel.lblClearSearch\")",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxClearRecordsSearch.add(fontIconCrossRecordsSearch);
            flxRecordsSearch.add(fontIconRecordsSearch, tbxRecordsSearch, flxClearRecordsSearch);
            flxAvailableRecords.add(lblAvailableHeader, flxRecordsSearch);
            var flxSeparartorRecords = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "1dp",
                "id": "flxSeparartorRecords",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "isModalContainer": false,
                "right": "0dp",
                "skin": "sknFlxBgFFFFFFBorE4E6ECsz1pxR3px",
                "top": "75dp",
                "zIndex": 10
            }, {}, {});
            flxSeparartorRecords.setDefaultUnit(kony.flex.DP);
            flxSeparartorRecords.add();
            var flxDatasourcesListContainer = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "bottom": "0dp",
                "clipBounds": true,
                "id": "flxDatasourcesListContainer",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "75dp",
                "width": "100%",
                "zIndex": 1
            }, {}, {});
            flxDatasourcesListContainer.setDefaultUnit(kony.flex.DP);
            var flxDatasourcesHeader = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "60px",
                "id": "flxDatasourcesHeader",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "30px",
                "isModalContainer": false,
                "right": "20px",
                "skin": "sknflxffffffop100",
                "top": "0"
            }, {}, {});
            flxDatasourcesHeader.setDefaultUnit(kony.flex.DP);
            var flxDatasourceName = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "centerY": "50%",
                "clipBounds": true,
                "height": "100%",
                "id": "flxDatasourceName",
                "isVisible": true,
                "layoutType": kony.flex.FLOW_HORIZONTAL,
                "left": "0px",
                "isModalContainer": false,
                "onClick": controller.AS_FlexContainer_aa9ff028c98444408c909813010b2efa,
                "skin": "slFbox",
                "top": 0,
                "width": "33%",
                "zIndex": 1
            }, {}, {
                "hoverSkin": "sknFlxPointer"
            });
            flxDatasourceName.setDefaultUnit(kony.flex.DP);
            var lblDatasourceName = new kony.ui.Label({
                "centerY": "50%",
                "id": "lblDatasourceName",
                "isVisible": true,
                "left": "0px",
                "skin": "sknlblLato696c7311px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmReportsManagement.label.datasource_caps\")",
                "top": 0,
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var fontIconDatasourceName = new kony.ui.Label({
                "centerY": "50%",
                "id": "fontIconDatasourceName",
                "isVisible": true,
                "left": "10dp",
                "skin": "sknIcon15px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmAlertsManagement.lblSortName\")",
                "top": "0dp",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {
                "hoverSkin": "sknlblCursorFont"
            });
            flxDatasourceName.add(lblDatasourceName, fontIconDatasourceName);
            var flxConnectedOn = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "centerY": "50%",
                "clipBounds": true,
                "height": "100%",
                "id": "flxConnectedOn",
                "isVisible": true,
                "layoutType": kony.flex.FLOW_HORIZONTAL,
                "left": "33%",
                "isModalContainer": false,
                "onClick": controller.AS_FlexContainer_d0c82a1545084c569df06fa1ca405fa6,
                "skin": "slFbox",
                "top": "0dp",
                "width": "33%",
                "zIndex": 1
            }, {}, {
                "hoverSkin": "sknFlxPointer"
            });
            flxConnectedOn.setDefaultUnit(kony.flex.DP);
            var lblConnectedOn = new kony.ui.Label({
                "centerY": "50%",
                "id": "lblConnectedOn",
                "isVisible": true,
                "left": "0px",
                "skin": "sknlblLato696c7311px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmReportsManagement.label.connectedOn_caps\")",
                "top": 0,
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var fontIconConnectedOn = new kony.ui.Label({
                "centerY": "50%",
                "id": "fontIconConnectedOn",
                "isVisible": true,
                "left": "10dp",
                "skin": "sknIcon15px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmAlertsManagement.lblSortName\")",
                "top": "0dp",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {
                "hoverSkin": "sknlblCursorFont"
            });
            flxConnectedOn.add(lblConnectedOn, fontIconConnectedOn);
            var flxGeneratedReports = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "centerY": "50%",
                "clipBounds": true,
                "height": "100%",
                "id": "flxGeneratedReports",
                "isVisible": true,
                "layoutType": kony.flex.FLOW_HORIZONTAL,
                "left": "66%",
                "isModalContainer": false,
                "onClick": controller.AS_FlexContainer_d0c82a1545084c569df06fa1ca405fa6,
                "skin": "slFbox",
                "top": "0dp",
                "width": "33%",
                "zIndex": 1
            }, {}, {
                "hoverSkin": "sknFlxPointer"
            });
            flxGeneratedReports.setDefaultUnit(kony.flex.DP);
            var lblGeneratedReports = new kony.ui.Label({
                "centerY": "50%",
                "id": "lblGeneratedReports",
                "isVisible": true,
                "left": "0px",
                "skin": "sknlblLato696c7311px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmReportsManagement.label.generatedReports_caps\")",
                "top": 0,
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var fontIconGeneratedReports = new kony.ui.Label({
                "centerY": "50%",
                "id": "fontIconGeneratedReports",
                "isVisible": true,
                "left": "10dp",
                "skin": "sknIcon15px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmAlertsManagement.lblSortName\")",
                "top": "0dp",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {
                "hoverSkin": "sknlblCursorFont"
            });
            flxGeneratedReports.add(lblGeneratedReports, fontIconGeneratedReports);
            flxDatasourcesHeader.add(flxDatasourceName, flxConnectedOn, flxGeneratedReports);
            var flxDatasourceSeparator2 = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "1dp",
                "id": "flxDatasourceSeparator2",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "20dp",
                "isModalContainer": false,
                "right": "20dp",
                "skin": "sknTableHeaderLine",
                "top": "60dp",
                "zIndex": 1
            }, {}, {});
            flxDatasourceSeparator2.setDefaultUnit(kony.flex.DP);
            flxDatasourceSeparator2.add();
            var flxDatasourcesSegContainer = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "bottom": "0px",
                "clipBounds": true,
                "id": "flxDatasourcesSegContainer",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "61dp",
                "width": "100%",
                "zIndex": 1
            }, {}, {});
            flxDatasourcesSegContainer.setDefaultUnit(kony.flex.DP);
            var segDatasource = new com.adminConsole.common.listingSegmentClient({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "height": "100%",
                "id": "segDatasource",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "masterType": constants.MASTER_TYPE_DEFAULT,
                "isModalContainer": false,
                "right": "0dp",
                "skin": "slFbox",
                "top": "0dp",
                "overrides": {
                    "contextualMenu.btnLink1": {
                        "bottom": "10dp",
                        "isVisible": false,
                        "top": "0dp"
                    },
                    "contextualMenu.btnLink2": {
                        "bottom": "10dp",
                        "isVisible": false,
                        "top": "0dp"
                    },
                    "contextualMenu.contextualMenu1Inner": {
                        "layoutType": kony.flex.FREE_FORM
                    },
                    "contextualMenu.flxOption1": {
                        "isVisible": false,
                        "top": "0dp"
                    },
                    "contextualMenu.flxOption2": {
                        "height": "30dp",
                        "top": "15dp"
                    },
                    "contextualMenu.flxOption3": {
                        "bottom": "10dp",
                        "height": "30dp",
                        "isVisible": true,
                        "top": "90dp"
                    },
                    "contextualMenu.flxOption4": {
                        "bottom": "viz.val_cleared",
                        "height": "30dp",
                        "top": "50dp"
                    },
                    "contextualMenu.flxOptionsSeperator": {
                        "bottom": "5dp",
                        "isVisible": true,
                        "top": "85px"
                    },
                    "contextualMenu.imgDownArrow": {
                        "src": "downarrow_2x.png"
                    },
                    "contextualMenu.imgOption1": {
                        "src": "imagedrag.png"
                    },
                    "contextualMenu.imgOption2": {
                        "src": "edit2x.png"
                    },
                    "contextualMenu.imgOption3": {
                        "src": "imagedrag.png"
                    },
                    "contextualMenu.imgOption4": {
                        "src": "deactive_2x.png"
                    },
                    "contextualMenu.imgUpArrow": {
                        "src": "uparrow_2x.png"
                    },
                    "contextualMenu.lblHeader": {
                        "bottom": "15dp",
                        "isVisible": false,
                        "top": "20dp"
                    },
                    "contextualMenu.lblIconOption2": {
                        "i18n_text": "kony.i18n.getLocalizedString(\"i18n.userwidgetmodel.lblIconOption1\")",
                        "left": "20dp"
                    },
                    "contextualMenu.lblIconOption3": {
                        "left": "20dp",
                        "text": ""
                    },
                    "contextualMenu.lblIconOption4": {
                        "left": "20dp"
                    },
                    "contextualMenu.lblOption3": {
                        "text": "Copy Role"
                    },
                    "flxContextualMenu": {
                        "isVisible": false,
                        "left": "viz.val_cleared",
                        "right": "20dp",
                        "width": "137px"
                    },
                    "flxListingSegmentWrapper": {
                        "bottom": "viz.val_cleared",
                        "height": "100%"
                    },
                    "flxPagination": {
                        "isVisible": false
                    },
                    "listingSegmentClient": {
                        "bottom": "viz.val_cleared",
                        "centerX": "viz.val_cleared",
                        "centerY": "viz.val_cleared",
                        "height": "100%",
                        "left": "0dp",
                        "maxHeight": "viz.val_cleared",
                        "maxWidth": "viz.val_cleared",
                        "minHeight": "viz.val_cleared",
                        "minWidth": "viz.val_cleared",
                        "right": "0dp",
                        "top": "0dp",
                        "width": "viz.val_cleared"
                    },
                    "pagination": {
                        "isVisible": false
                    },
                    "rtxNoResultsFound": {
                        "isVisible": false
                    },
                    "segListing": {
                        "bottom": "viz.val_cleared",
                        "height": "100%",
                        "isVisible": true
                    }
                }
            }, {
                "overrides": {}
            }, {
                "overrides": {}
            });
            flxDatasourcesSegContainer.add(segDatasource);
            flxDatasourcesListContainer.add(flxDatasourcesHeader, flxDatasourceSeparator2, flxDatasourcesSegContainer);
            var flxReportsListContainer = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "bottom": "0dp",
                "clipBounds": true,
                "id": "flxReportsListContainer",
                "isVisible": false,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "75dp",
                "width": "100%",
                "zIndex": 1
            }, {}, {});
            flxReportsListContainer.setDefaultUnit(kony.flex.DP);
            var flxReportsListHeader = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "60px",
                "id": "flxReportsListHeader",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "30px",
                "isModalContainer": false,
                "right": "20px",
                "skin": "sknflxffffffop100",
                "top": "0"
            }, {}, {});
            flxReportsListHeader.setDefaultUnit(kony.flex.DP);
            var flxNameReport = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "centerY": "50%",
                "clipBounds": true,
                "height": "100%",
                "id": "flxNameReport",
                "isVisible": true,
                "layoutType": kony.flex.FLOW_HORIZONTAL,
                "left": "0px",
                "isModalContainer": false,
                "onClick": controller.AS_FlexContainer_aa9ff028c98444408c909813010b2efa,
                "skin": "slFbox",
                "top": 0,
                "width": "25%",
                "zIndex": 1
            }, {}, {
                "hoverSkin": "sknFlxPointer"
            });
            flxNameReport.setDefaultUnit(kony.flex.DP);
            var lblNameReport = new kony.ui.Label({
                "centerY": "50%",
                "id": "lblNameReport",
                "isVisible": true,
                "left": "0px",
                "skin": "sknlblLato696c7311px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmReportsManagement.label.reportName_caps\")",
                "top": 0,
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var fontIconSortName = new kony.ui.Label({
                "centerY": "50%",
                "id": "fontIconSortName",
                "isVisible": true,
                "left": "10dp",
                "skin": "sknIcon15px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmAlertsManagement.lblSortName\")",
                "top": "0dp",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxNameReport.add(lblNameReport, fontIconSortName);
            var flxInDescription = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "centerY": "50%",
                "clipBounds": true,
                "height": "100%",
                "id": "flxInDescription",
                "isVisible": true,
                "layoutType": kony.flex.FLOW_HORIZONTAL,
                "left": "25%",
                "isModalContainer": false,
                "onClick": controller.AS_FlexContainer_d0c82a1545084c569df06fa1ca405fa6,
                "skin": "slFbox",
                "top": "0dp",
                "width": "35%",
                "zIndex": 1
            }, {}, {});
            flxInDescription.setDefaultUnit(kony.flex.DP);
            var lblInDescription = new kony.ui.Label({
                "centerY": "50%",
                "id": "lblInDescription",
                "isVisible": true,
                "left": "0px",
                "skin": "sknlblLato696c7311px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.ConfigurationBundles.descriptionInCaps\")",
                "top": 0,
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var fontIconFilteInDescription = new kony.ui.Label({
                "centerY": "50%",
                "id": "fontIconFilteInDescription",
                "isVisible": false,
                "left": "10dp",
                "skin": "sknIcon15px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmAlertsManagement.lblIconChannels\")",
                "top": "0dp",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {
                "hoverSkin": "sknlblCursorFont"
            });
            flxInDescription.add(lblInDescription, fontIconFilteInDescription);
            var flxDataSource = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "centerY": "50%",
                "clipBounds": true,
                "height": "100%",
                "id": "flxDataSource",
                "isVisible": true,
                "layoutType": kony.flex.FLOW_HORIZONTAL,
                "left": "60%",
                "isModalContainer": false,
                "onClick": controller.AS_FlexContainer_d0c82a1545084c569df06fa1ca405fa6,
                "skin": "slFbox",
                "top": "0dp",
                "width": "20%",
                "zIndex": 1
            }, {}, {
                "hoverSkin": "sknFlxPointer"
            });
            flxDataSource.setDefaultUnit(kony.flex.DP);
            var lblDataSource = new kony.ui.Label({
                "centerY": "50%",
                "id": "lblDataSource",
                "isVisible": true,
                "left": "0px",
                "skin": "sknlblLato696c7311px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmReportsManagement.label.datasource_caps\")",
                "top": 0,
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var fontIconDataSource = new kony.ui.Label({
                "centerY": "50%",
                "id": "fontIconDataSource",
                "isVisible": false,
                "left": "10dp",
                "skin": "sknIcon15px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmAlertsManagement.lblIconChannels\")",
                "top": "0dp",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxDataSource.add(lblDataSource, fontIconDataSource);
            var flxCreatedDate = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "centerY": "50%",
                "clipBounds": true,
                "height": "100%",
                "id": "flxCreatedDate",
                "isVisible": true,
                "layoutType": kony.flex.FLOW_HORIZONTAL,
                "left": "80%",
                "isModalContainer": false,
                "onClick": controller.AS_FlexContainer_d3a97a0f3de34380a91ef7e725e093be,
                "skin": "slFbox",
                "top": "0dp",
                "width": "15%",
                "zIndex": 1
            }, {}, {
                "hoverSkin": "sknFlxPointer"
            });
            flxCreatedDate.setDefaultUnit(kony.flex.DP);
            var lblCreatedDate = new kony.ui.Label({
                "centerY": "50%",
                "id": "lblCreatedDate",
                "isVisible": true,
                "left": "0px",
                "skin": "sknlblLato696c7311px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmReportsManagement.label.createdDate_caps\")",
                "top": 0,
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var fontIconCreatedDate = new kony.ui.Label({
                "centerY": "50%",
                "id": "fontIconCreatedDate",
                "isVisible": true,
                "left": "10dp",
                "skin": "sknIcon15px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmAlertsManagement.lblSortName\")",
                "top": "0dp",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {
                "hoverSkin": "sknlblCursorFont"
            });
            flxCreatedDate.add(lblCreatedDate, fontIconCreatedDate);
            flxReportsListHeader.add(flxNameReport, flxInDescription, flxDataSource, flxCreatedDate);
            var flxReportListSeparator = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "1dp",
                "id": "flxReportListSeparator",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "20dp",
                "isModalContainer": false,
                "right": "20dp",
                "skin": "sknTableHeaderLine",
                "top": "60dp",
                "zIndex": 1
            }, {}, {});
            flxReportListSeparator.setDefaultUnit(kony.flex.DP);
            flxReportListSeparator.add();
            var flxReportSegContainer = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "bottom": "0px",
                "clipBounds": true,
                "id": "flxReportSegContainer",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "61dp",
                "width": "100%",
                "zIndex": 1
            }, {}, {});
            flxReportSegContainer.setDefaultUnit(kony.flex.DP);
            var segReports = new com.adminConsole.common.listingSegmentClient({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "height": "100%",
                "id": "segReports",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "masterType": constants.MASTER_TYPE_DEFAULT,
                "isModalContainer": false,
                "right": "0dp",
                "skin": "slFbox",
                "top": "0dp",
                "overrides": {
                    "contextualMenu.btnLink1": {
                        "bottom": "10dp",
                        "isVisible": false,
                        "top": "0dp"
                    },
                    "contextualMenu.btnLink2": {
                        "bottom": "10dp",
                        "isVisible": false,
                        "top": "0dp"
                    },
                    "contextualMenu.contextualMenu1Inner": {
                        "layoutType": kony.flex.FREE_FORM
                    },
                    "contextualMenu.flxOption1": {
                        "isVisible": false,
                        "top": "0dp"
                    },
                    "contextualMenu.flxOption2": {
                        "height": "30dp",
                        "top": "15dp"
                    },
                    "contextualMenu.flxOption3": {
                        "bottom": "10dp",
                        "height": "30dp",
                        "isVisible": true,
                        "top": "90dp"
                    },
                    "contextualMenu.flxOption4": {
                        "bottom": "viz.val_cleared",
                        "height": "30dp",
                        "top": "50dp"
                    },
                    "contextualMenu.flxOptionsSeperator": {
                        "bottom": "5dp",
                        "isVisible": true,
                        "top": "85px"
                    },
                    "contextualMenu.imgDownArrow": {
                        "src": "downarrow_2x.png"
                    },
                    "contextualMenu.imgOption1": {
                        "src": "imagedrag.png"
                    },
                    "contextualMenu.imgOption2": {
                        "src": "edit2x.png"
                    },
                    "contextualMenu.imgOption3": {
                        "src": "imagedrag.png"
                    },
                    "contextualMenu.imgOption4": {
                        "src": "deactive_2x.png"
                    },
                    "contextualMenu.imgUpArrow": {
                        "src": "uparrow_2x.png"
                    },
                    "contextualMenu.lblHeader": {
                        "bottom": "15dp",
                        "isVisible": false,
                        "top": "20dp"
                    },
                    "contextualMenu.lblIconOption2": {
                        "i18n_text": "kony.i18n.getLocalizedString(\"i18n.userwidgetmodel.lblIconOption1\")",
                        "left": "20dp"
                    },
                    "contextualMenu.lblIconOption3": {
                        "left": "20dp",
                        "text": ""
                    },
                    "contextualMenu.lblIconOption4": {
                        "left": "20dp"
                    },
                    "contextualMenu.lblOption3": {
                        "text": "Copy Role"
                    },
                    "flxContextualMenu": {
                        "isVisible": false,
                        "left": "viz.val_cleared",
                        "right": "20dp",
                        "width": "137px"
                    },
                    "flxListingSegmentWrapper": {
                        "bottom": "viz.val_cleared",
                        "height": "100%"
                    },
                    "flxPagination": {
                        "isVisible": false
                    },
                    "listingSegmentClient": {
                        "bottom": "viz.val_cleared",
                        "centerX": "viz.val_cleared",
                        "centerY": "viz.val_cleared",
                        "height": "100%",
                        "left": "0dp",
                        "maxHeight": "viz.val_cleared",
                        "maxWidth": "viz.val_cleared",
                        "minHeight": "viz.val_cleared",
                        "minWidth": "viz.val_cleared",
                        "right": "0dp",
                        "top": "0dp",
                        "width": "viz.val_cleared"
                    },
                    "pagination": {
                        "isVisible": false
                    },
                    "segListing": {
                        "bottom": "viz.val_cleared",
                        "height": "100%"
                    }
                }
            }, {
                "overrides": {}
            }, {
                "overrides": {}
            });
            flxReportSegContainer.add(segReports);
            var flxFilter = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                "clipBounds": true,
                "id": "flxFilter",
                "isVisible": false,
                "layoutType": kony.flex.FREE_FORM,
                "isModalContainer": false,
                "right": "80px",
                "skin": "slFbox",
                "top": "37px",
                "width": "120px",
                "zIndex": 5
            }, {}, {});
            flxFilter.setDefaultUnit(kony.flex.DP);
            var statusFilterMenu = new com.adminConsole.common.statusFilterMenu({
                "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                "id": "statusFilterMenu",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "masterType": constants.MASTER_TYPE_DEFAULT,
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "5dp",
                "width": "100%",
                "zIndex": 5,
                "overrides": {
                    "imgUpArrow": {
                        "src": "uparrow_2x.png"
                    },
                    "segStatusFilterDropdown": {
                        "left": "0dp",
                        "top": "5dp"
                    }
                }
            }, {
                "overrides": {}
            }, {
                "overrides": {}
            });
            flxFilter.add(statusFilterMenu);
            flxReportsListContainer.add(flxReportsListHeader, flxReportListSeparator, flxReportSegContainer, flxFilter);
            flxListingCommon.add(flxAvailableRecords, flxSeparartorRecords, flxDatasourcesListContainer, flxReportsListContainer);
            var flxNoReportsConfigured = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "centerY": "50%",
                "clipBounds": true,
                "height": "182dp",
                "id": "flxNoReportsConfigured",
                "isVisible": false,
                "layoutType": kony.flex.FLOW_VERTICAL,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0dp",
                "width": "100%"
            }, {}, {});
            flxNoReportsConfigured.setDefaultUnit(kony.flex.DP);
            var imgNoReports = new kony.ui.Image2({
                "centerX": "50%",
                "height": "79dp",
                "id": "imgNoReports",
                "isVisible": true,
                "left": "0",
                "skin": "slImage",
                "src": "group.png",
                "top": "0",
                "width": "73dp"
            }, {
                "imageScaleMode": constants.IMAGE_SCALE_MODE_MAINTAIN_ASPECT_RATIO,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var lblNoReports = new kony.ui.Label({
                "centerX": "50%",
                "id": "lblNoReports",
                "isVisible": true,
                "skin": "sknLbl485C75LatoRegular13Px",
                "text": "No Reports Configured yet Click Create New Report",
                "top": "34dp",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var btnAddCustomer = new kony.ui.Button({
                "centerX": "50%",
                "height": "30dp",
                "id": "btnAddCustomer",
                "isVisible": true,
                "skin": "sknbtnf7f7faLatoRegular12Px485c75Border1px485C75Radius20px",
                "text": "CREATE NEW REPORT",
                "top": "20dp",
                "width": "192dp",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_CENTER,
                "displayText": true,
                "padding": [2, 0, 2, 0],
                "paddingInPixel": false
            }, {
                "hoverSkin": "sknbtnffffffLatoRegular12Px485c75Border1px485C75Radius20px"
            });
            flxNoReportsConfigured.add(imgNoReports, lblNoReports, btnAddCustomer);
            var DeleteToolTip = new com.adminConsole.Customers.ToolTip({
                "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                "id": "DeleteToolTip",
                "isVisible": false,
                "layoutType": kony.flex.FREE_FORM,
                "masterType": constants.MASTER_TYPE_DEFAULT,
                "isModalContainer": false,
                "right": "25px",
                "skin": "slFbox",
                "top": "40dp",
                "width": "60dp",
                "overrides": {
                    "ToolTip": {
                        "isVisible": false,
                        "left": "viz.val_cleared",
                        "right": "25px",
                        "top": "40dp",
                        "width": "60dp"
                    },
                    "flxToolTipMessage": {
                        "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                        "centerX": "viz.val_cleared",
                        "width": kony.flex.USE_PREFFERED_SIZE
                    },
                    "lblDownArrow": {
                        "centerX": "viz.val_cleared",
                        "left": "viz.val_cleared",
                        "right": "10px",
                        "top": "55dp"
                    },
                    "lblNoConcentToolTip": {
                        "bottom": "5px",
                        "height": kony.flex.USE_PREFFERED_SIZE,
                        "left": "10px",
                        "right": "10px",
                        "text": "Delete",
                        "top": "3px",
                        "width": kony.flex.USE_PREFFERED_SIZE
                    },
                    "lblarrow": {
                        "centerX": "viz.val_cleared",
                        "left": "viz.val_cleared",
                        "right": "10px"
                    }
                }
            }, {
                "overrides": {}
            }, {
                "overrides": {}
            });
            flxMainReportsOuter.add(flxMainContainer, flxReportsTabs, flxViewReports, flxListingCommon, flxNoReportsConfigured, DeleteToolTip);
            var flxLoading = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "100%",
                "id": "flxLoading",
                "isVisible": false,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "sknLoadingBlur",
                "top": "0dp",
                "width": "100%",
                "zIndex": 15
            }, {}, {});
            flxLoading.setDefaultUnit(kony.flex.DP);
            var flxImageContainer = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "centerX": "50%",
                "centerY": "50%",
                "clipBounds": true,
                "height": "80px",
                "id": "flxImageContainer",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "isModalContainer": false,
                "skin": "slFbox",
                "width": "80px",
                "zIndex": 1
            }, {}, {});
            flxImageContainer.setDefaultUnit(kony.flex.DP);
            var imgLoading = new kony.ui.Image2({
                "centerX": "50%",
                "centerY": "50%",
                "height": "60px",
                "id": "imgLoading",
                "isVisible": true,
                "skin": "slImage",
                "src": "loadingscreenimage.gif",
                "width": "60px",
                "zIndex": 1
            }, {
                "imageScaleMode": constants.IMAGE_SCALE_MODE_MAINTAIN_ASPECT_RATIO,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxImageContainer.add(imgLoading);
            flxLoading.add(flxImageContainer);
            flxRightPannel.add(flxMainHeader, flxHeaderDropdown, flxBreadCrumbs, flxMainReportsOuter, flxLoading);
            var flxToastMessage = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "bottom": "0px",
                "clipBounds": true,
                "height": "70px",
                "id": "flxToastMessage",
                "isVisible": false,
                "layoutType": kony.flex.FREE_FORM,
                "left": "305px",
                "isModalContainer": false,
                "right": "0px",
                "skin": "slFbox",
                "zIndex": 6
            }, {}, {});
            flxToastMessage.setDefaultUnit(kony.flex.DP);
            var toastMessage = new com.adminConsole.common.toastMessage({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "height": "70px",
                "id": "toastMessage",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "masterType": constants.MASTER_TYPE_DEFAULT,
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0dp",
                "width": "100%",
                "overrides": {}
            }, {
                "overrides": {}
            }, {
                "overrides": {}
            });
            flxToastMessage.add(toastMessage);
            flxMain.add(flxLeftPannel, flxRightPannel, flxToastMessage);
            var flxEditCancelConfirmation = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "100%",
                "id": "flxEditCancelConfirmation",
                "isVisible": false,
                "layoutType": kony.flex.FREE_FORM,
                "left": "10dp",
                "masterType": constants.MASTER_TYPE_USERWIDGET,
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "10dp",
                "width": "100%",
                "zIndex": 5
            }, {}, {});
            flxEditCancelConfirmation.setDefaultUnit(kony.flex.DP);
            var popUpCancelEdits = new com.adminConsole.common.popUpCancelEdits({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "height": "100%",
                "id": "popUpCancelEdits",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "masterType": constants.MASTER_TYPE_DEFAULT,
                "isModalContainer": false,
                "skin": "sknflxBg000000Op50",
                "top": "0dp",
                "width": "100%",
                "zIndex": 1,
                "overrides": {
                    "btnPopUpCancel": {
                        "i18n_text": "kony.i18n.getLocalizedString(\"i8n.navigation.cancelButton\")"
                    },
                    "btnPopUpDelete": {
                        "i18n_text": "kony.i18n.getLocalizedString(\"i8n.navigation.okButton\")"
                    },
                    "lblPopUpMainMessage": {
                        "i18n_text": "kony.i18n.getLocalizedString(\"i8n.navigation.cancelChanges\")"
                    },
                    "popUpCancelEdits": {
                        "bottom": "viz.val_cleared",
                        "centerX": "viz.val_cleared",
                        "centerY": "viz.val_cleared",
                        "height": "100%",
                        "left": "0dp",
                        "maxHeight": "viz.val_cleared",
                        "maxWidth": "viz.val_cleared",
                        "minHeight": "viz.val_cleared",
                        "minWidth": "viz.val_cleared",
                        "right": "viz.val_cleared",
                        "top": "0dp",
                        "width": "100%"
                    },
                    "rtxPopUpDisclaimer": {
                        "i18n_text": "kony.i18n.getLocalizedString(\"i8n.navigation.cancelChangesBody\")"
                    }
                }
            }, {
                "overrides": {}
            }, {
                "overrides": {}
            });
            flxEditCancelConfirmation.add(popUpCancelEdits);
            var flxCreateReport = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "bottom": "0dp",
                "clipBounds": true,
                "height": "100%",
                "id": "flxCreateReport",
                "isVisible": false,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "isModalContainer": false,
                "right": "0dp",
                "skin": "flxPopupTrans0ba858b5d5d304e",
                "top": "0dp",
                "width": "100%",
                "zIndex": 10
            }, {}, {});
            flxCreateReport.setDefaultUnit(kony.flex.DP);
            var flxCreateContainer = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "bottom": "62dp",
                "clipBounds": true,
                "id": "flxCreateContainer",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "190dp",
                "isModalContainer": false,
                "right": "190dp",
                "skin": "slFbox0j9f841cc563e4e",
                "top": "62dp"
            }, {}, {});
            flxCreateContainer.setDefaultUnit(kony.flex.DP);
            var flxTopBar = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "10px",
                "id": "flxTopBar",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0px",
                "isModalContainer": false,
                "skin": "sknFlxTopColor4A77A0",
                "top": "0dp",
                "width": "100%",
                "zIndex": 1
            }, {}, {});
            flxTopBar.setDefaultUnit(kony.flex.DP);
            flxTopBar.add();
            var flxCreateBody = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "bottom": "80dp",
                "clipBounds": true,
                "id": "flxCreateBody",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0dp",
                "width": "100%"
            }, {}, {});
            flxCreateBody.setDefaultUnit(kony.flex.DP);
            var flxTopHeader = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "75dp",
                "id": "flxTopHeader",
                "isVisible": true,
                "layoutType": kony.flex.FLOW_VERTICAL,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0dp",
                "width": "100%"
            }, {}, {});
            flxTopHeader.setDefaultUnit(kony.flex.DP);
            var flxCreateClose = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "15dp",
                "id": "flxCreateClose",
                "isVisible": true,
                "layoutType": kony.flex.FLOW_VERTICAL,
                "isModalContainer": false,
                "right": "20dp",
                "skin": "slFbox",
                "top": "20dp",
                "width": "15dp"
            }, {}, {
                "hoverSkin": "sknFlxPointer"
            });
            flxCreateClose.setDefaultUnit(kony.flex.DP);
            var lblflxAddMessageCloseIcon = new kony.ui.Label({
                "height": "15dp",
                "id": "lblflxAddMessageCloseIcon",
                "isVisible": true,
                "skin": "sknFontIconSearchCross16px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmAlertsManagement.lblAlertPreviewClose\")",
                "width": "15dp",
                "zIndex": 2
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxCreateClose.add(lblflxAddMessageCloseIcon);
            var lblCreateReportTitle = new kony.ui.Label({
                "id": "lblCreateReportTitle",
                "isVisible": true,
                "left": "20dp",
                "skin": "sknLbl16pxLato192B45",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmReportsManagement.label.createReport\")",
                "top": "0dp",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 2
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var flxSeparator = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "1dp",
                "id": "flxSeparator",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "isModalContainer": false,
                "right": "0dp",
                "skin": "sknFlxBgFFFFFFBorE4E6ECsz1pxR3px",
                "top": "20dp",
                "width": "100%",
                "zIndex": 10
            }, {}, {});
            flxSeparator.setDefaultUnit(kony.flex.DP);
            flxSeparator.add();
            flxTopHeader.add(flxCreateClose, lblCreateReportTitle, flxSeparator);
            var flxCenterContainer = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "bottom": "0dp",
                "clipBounds": true,
                "id": "flxCenterContainer",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "75dp",
                "width": "100%"
            }, {}, {});
            flxCenterContainer.setDefaultUnit(kony.flex.DP);
            var flxVerticalTabs = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "bottom": "0px",
                "clipBounds": true,
                "id": "flxVerticalTabs",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0dp",
                "width": "232px",
                "zIndex": 1
            }, {}, {});
            flxVerticalTabs.setDefaultUnit(kony.flex.DP);
            var verticalTabs = new com.adminConsole.common.verticalTabs1({
                "height": "100%",
                "id": "verticalTabs",
                "isVisible": true,
                "layoutType": kony.flex.FLOW_VERTICAL,
                "left": "0dp",
                "masterType": constants.MASTER_TYPE_DEFAULT,
                "isModalContainer": false,
                "skin": "sknflxf9f9f9NoBorder",
                "top": "0dp",
                "width": "100%",
                "overrides": {
                    "btnOption0": {
                        "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmReportsManagement.label.datasource_caps\")"
                    },
                    "btnOption1": {
                        "bottom": "0dp",
                        "centerX": "viz.val_cleared",
                        "centerY": "viz.val_cleared",
                        "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmReportsManagement.label.report_caps\")",
                        "left": "20px",
                        "right": "39dp",
                        "top": "0dp",
                        "width": "viz.val_cleared"
                    },
                    "btnOption2": {
                        "left": "20px",
                        "right": "40dp",
                        "text": "ASSIGN FEATURES AND ACTIONS",
                        "top": "0dp",
                        "width": "viz.val_cleared"
                    },
                    "btnOption3": {
                        "i18n_text": "kony.i18n.getLocalizedString(\"i18n.Group.ASSIGNCUSTOMERS\")",
                        "left": "20px",
                        "right": "40dp",
                        "width": "viz.val_cleared"
                    },
                    "btnOption4": {
                        "left": "20px",
                        "right": 40,
                        "width": "viz.val_cleared"
                    },
                    "flxImgArrow1": {
                        "bottom": "0dp",
                        "centerY": "viz.val_cleared",
                        "right": "20px",
                        "top": "0dp"
                    },
                    "flxImgArrow2": {
                        "bottom": "0dp",
                        "right": "20px",
                        "top": "0px"
                    },
                    "flxImgArrow3": {
                        "right": "20px"
                    },
                    "flxImgArrow4": {
                        "right": "20px"
                    },
                    "flxOption0": {
                        "isVisible": true
                    },
                    "flxOption1": {
                        "isVisible": true
                    },
                    "flxOption2": {
                        "isVisible": false
                    },
                    "flxOption3": {
                        "isVisible": false
                    },
                    "flxOption4": {
                        "isVisible": false
                    },
                    "imgSelected1": {
                        "src": "right_arrow2x.png"
                    },
                    "imgSelected2": {
                        "src": "right_arrow2x.png"
                    },
                    "imgSelected3": {
                        "src": "right_arrow2x.png"
                    },
                    "imgSelected4": {
                        "src": "right_arrow2x.png"
                    },
                    "lblOptional0": {
                        "isVisible": false,
                        "text": "FABRIC"
                    },
                    "lblOptional2": {
                        "isVisible": false,
                        "left": "20px",
                        "top": "10px"
                    },
                    "lblOptional3": {
                        "left": "20px",
                        "top": "10px"
                    },
                    "lblOptional4": {
                        "left": "20px"
                    },
                    "lblSelected1": {
                        "centerX": "50%",
                        "centerY": "50%"
                    },
                    "lblSelected2": {
                        "centerY": "50%"
                    }
                }
            }, {
                "overrides": {}
            }, {
                "overrides": {}
            });
            flxVerticalTabs.add(verticalTabs);
            var flxListinCommonCreate = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "100%",
                "id": "flxListinCommonCreate",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "232dp",
                "isModalContainer": false,
                "right": "0dp",
                "skin": "slFbox",
                "top": "0dp"
            }, {}, {});
            flxListinCommonCreate.setDefaultUnit(kony.flex.DP);
            var flxAvailableReportsHeader = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "40dp",
                "id": "flxAvailableReportsHeader",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "isModalContainer": false,
                "right": "0dp",
                "skin": "slFbox",
                "top": "15dp"
            }, {}, {});
            flxAvailableReportsHeader.setDefaultUnit(kony.flex.DP);
            var lblAvailableReports = new kony.ui.Label({
                "centerY": "50%",
                "height": "16dp",
                "id": "lblAvailableReports",
                "isVisible": true,
                "left": "20dp",
                "skin": "sknLbl192B45LatoRegular14px",
                "text": "223 Reports Found",
                "top": "0",
                "width": kony.flex.USE_PREFFERED_SIZE
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var flxSearchContainer = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "centerY": "50%",
                "clipBounds": true,
                "height": "40px",
                "id": "flxSearchContainer",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "isModalContainer": false,
                "right": "20dp",
                "skin": "sknflxd5d9ddop100",
                "top": "0dp",
                "width": "380px",
                "zIndex": 1
            }, {}, {});
            flxSearchContainer.setDefaultUnit(kony.flex.DP);
            var fontIconSearchImg = new kony.ui.Label({
                "centerY": "50%",
                "id": "fontIconSearchImg",
                "isVisible": true,
                "left": "12px",
                "skin": "sknFontIconSearchImg20px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.userwidgetmodel.lblSearchIcon\")",
                "top": "0dp",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var tbxSearchBox = new kony.ui.TextBox2({
                "autoCapitalize": constants.TEXTBOX_AUTO_CAPITALIZE_NONE,
                "centerY": "50%",
                "height": "35px",
                "id": "tbxSearchBox",
                "isVisible": true,
                "keyBoardStyle": constants.TEXTBOX_KEY_BOARD_STYLE_DEFAULT,
                "left": "30dp",
                "onTouchStart": controller.AS_TextField_cfab06859f0f43eca99495891ee290d5,
                "placeholder": "Search by Report Name",
                "right": "35px",
                "secureTextEntry": false,
                "skin": "skntbxffffffNoBorderlato35475f14px",
                "textInputMode": constants.TEXTBOX_INPUT_MODE_ANY,
                "zIndex": 1
            }, {
                "containerHeightMode": constants.TEXTBOX_FONT_METRICS_DRIVEN_HEIGHT,
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [2, 0, 0, 0],
                "paddingInPixel": false
            }, {
                "autoCorrect": false
            });
            var flxClearSearchImage = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "100%",
                "id": "flxClearSearchImage",
                "isVisible": false,
                "layoutType": kony.flex.FREE_FORM,
                "isModalContainer": false,
                "right": "0px",
                "skin": "sknCursor",
                "top": "0dp",
                "width": "35px",
                "zIndex": 1
            }, {}, {
                "hoverSkin": "sknCursor"
            });
            flxClearSearchImage.setDefaultUnit(kony.flex.DP);
            var fontIconCross = new kony.ui.Label({
                "centerY": "50%",
                "id": "fontIconCross",
                "isVisible": true,
                "left": "5px",
                "skin": "sknFontIconSearchCross16px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.userwidgetmodel.lblClearSearch\")",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxClearSearchImage.add(fontIconCross);
            flxSearchContainer.add(fontIconSearchImg, tbxSearchBox, flxClearSearchImage);
            flxAvailableReportsHeader.add(lblAvailableReports, flxSearchContainer);
            var flxSeparator2 = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "1dp",
                "id": "flxSeparator2",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "20dp",
                "isModalContainer": false,
                "right": "20dp",
                "skin": "sknFlxBgFFFFFFBorE4E6ECsz1pxR3px",
                "top": "70dp",
                "zIndex": 10
            }, {}, {});
            flxSeparator2.setDefaultUnit(kony.flex.DP);
            flxSeparator2.add();
            var flxDatasourcesAvailableSegment = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "bottom": "0dp",
                "clipBounds": true,
                "id": "flxDatasourcesAvailableSegment",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "isModalContainer": false,
                "right": "0dp",
                "skin": "slFbox",
                "top": "70dp",
                "zIndex": 1
            }, {}, {});
            flxDatasourcesAvailableSegment.setDefaultUnit(kony.flex.DP);
            var flxDatasourcesHeaderCreate = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "60px",
                "id": "flxDatasourcesHeaderCreate",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "30px",
                "isModalContainer": false,
                "right": "20px",
                "skin": "sknflxffffffop100",
                "top": "0"
            }, {}, {});
            flxDatasourcesHeaderCreate.setDefaultUnit(kony.flex.DP);
            var flxDatasourceNameCreate = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "centerY": "50%",
                "clipBounds": true,
                "height": "100%",
                "id": "flxDatasourceNameCreate",
                "isVisible": true,
                "layoutType": kony.flex.FLOW_HORIZONTAL,
                "left": "0px",
                "isModalContainer": false,
                "onClick": controller.AS_FlexContainer_aa9ff028c98444408c909813010b2efa,
                "skin": "slFbox",
                "top": 0,
                "width": "33%",
                "zIndex": 1
            }, {}, {
                "hoverSkin": "sknFlxPointer"
            });
            flxDatasourceNameCreate.setDefaultUnit(kony.flex.DP);
            var lblDatasourceNameCreate = new kony.ui.Label({
                "centerY": "50%",
                "id": "lblDatasourceNameCreate",
                "isVisible": true,
                "left": "30px",
                "skin": "sknlblLato696c7311px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmReportsManagement.label.datasource_caps\")",
                "top": 0,
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var fontIconDatasourceNameCreate = new kony.ui.Label({
                "centerY": "50%",
                "id": "fontIconDatasourceNameCreate",
                "isVisible": true,
                "left": "10dp",
                "skin": "sknIcon15px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmAlertsManagement.lblSortName\")",
                "top": "0dp",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {
                "hoverSkin": "sknlblCursorFont"
            });
            flxDatasourceNameCreate.add(lblDatasourceNameCreate, fontIconDatasourceNameCreate);
            var flxConnectedOnCreate = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "centerY": "50%",
                "clipBounds": true,
                "height": "100%",
                "id": "flxConnectedOnCreate",
                "isVisible": true,
                "layoutType": kony.flex.FLOW_HORIZONTAL,
                "left": "33%",
                "isModalContainer": false,
                "onClick": controller.AS_FlexContainer_d0c82a1545084c569df06fa1ca405fa6,
                "skin": "slFbox",
                "top": "0dp",
                "width": "33%",
                "zIndex": 1
            }, {}, {
                "hoverSkin": "sknFlxPointer"
            });
            flxConnectedOnCreate.setDefaultUnit(kony.flex.DP);
            var lblConnectedOnCreate = new kony.ui.Label({
                "centerY": "50%",
                "id": "lblConnectedOnCreate",
                "isVisible": true,
                "left": "0px",
                "skin": "sknlblLato696c7311px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmReportsManagement.label.connectedOn_caps\")",
                "top": 0,
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var fontIconConnectedOnCreate = new kony.ui.Label({
                "centerY": "50%",
                "id": "fontIconConnectedOnCreate",
                "isVisible": true,
                "left": "10dp",
                "skin": "sknIcon15px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmAlertsManagement.lblSortName\")",
                "top": "0dp",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {
                "hoverSkin": "sknlblCursorFont"
            });
            flxConnectedOnCreate.add(lblConnectedOnCreate, fontIconConnectedOnCreate);
            var flxGeneratedReportsCreate = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "centerY": "50%",
                "clipBounds": true,
                "height": "100%",
                "id": "flxGeneratedReportsCreate",
                "isVisible": true,
                "layoutType": kony.flex.FLOW_HORIZONTAL,
                "left": "66%",
                "isModalContainer": false,
                "onClick": controller.AS_FlexContainer_d0c82a1545084c569df06fa1ca405fa6,
                "skin": "slFbox",
                "top": "0dp",
                "width": "33%",
                "zIndex": 1
            }, {}, {
                "hoverSkin": "sknFlxPointer"
            });
            flxGeneratedReportsCreate.setDefaultUnit(kony.flex.DP);
            var lblGeneratedReportsCreate = new kony.ui.Label({
                "centerY": "50%",
                "id": "lblGeneratedReportsCreate",
                "isVisible": true,
                "left": "0px",
                "skin": "sknlblLato696c7311px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmReportsManagement.label.generatedReports_caps\")",
                "top": 0,
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var fontIconGeneratedReportsCreate = new kony.ui.Label({
                "centerY": "50%",
                "id": "fontIconGeneratedReportsCreate",
                "isVisible": true,
                "left": "10dp",
                "skin": "sknIcon15px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmAlertsManagement.lblSortName\")",
                "top": "0dp",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {
                "hoverSkin": "sknlblCursorFont"
            });
            flxGeneratedReportsCreate.add(lblGeneratedReportsCreate, fontIconGeneratedReportsCreate);
            flxDatasourcesHeaderCreate.add(flxDatasourceNameCreate, flxConnectedOnCreate, flxGeneratedReportsCreate);
            var flxDatasourceSeparatorCreate = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "1dp",
                "id": "flxDatasourceSeparatorCreate",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "20dp",
                "isModalContainer": false,
                "right": "20dp",
                "skin": "sknTableHeaderLine",
                "top": "60dp",
                "zIndex": 1
            }, {}, {});
            flxDatasourceSeparatorCreate.setDefaultUnit(kony.flex.DP);
            flxDatasourceSeparatorCreate.add();
            var flxDatasourcesSegContainerCreate = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "bottom": "0px",
                "clipBounds": true,
                "id": "flxDatasourcesSegContainerCreate",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "61dp",
                "width": "100%",
                "zIndex": 1
            }, {}, {});
            flxDatasourcesSegContainerCreate.setDefaultUnit(kony.flex.DP);
            var segDatasourceCreate = new com.adminConsole.common.listingSegmentClient({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "height": "100%",
                "id": "segDatasourceCreate",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "masterType": constants.MASTER_TYPE_DEFAULT,
                "isModalContainer": false,
                "right": "0dp",
                "skin": "slFbox",
                "top": "0dp",
                "overrides": {
                    "contextualMenu.btnLink1": {
                        "bottom": "10dp",
                        "isVisible": false,
                        "top": "0dp"
                    },
                    "contextualMenu.btnLink2": {
                        "bottom": "10dp",
                        "isVisible": false,
                        "top": "0dp"
                    },
                    "contextualMenu.contextualMenu1Inner": {
                        "layoutType": kony.flex.FREE_FORM
                    },
                    "contextualMenu.flxOption1": {
                        "isVisible": false,
                        "top": "0dp"
                    },
                    "contextualMenu.flxOption2": {
                        "height": "30dp",
                        "top": "15dp"
                    },
                    "contextualMenu.flxOption3": {
                        "bottom": "10dp",
                        "height": "30dp",
                        "isVisible": true,
                        "top": "90dp"
                    },
                    "contextualMenu.flxOption4": {
                        "bottom": "viz.val_cleared",
                        "height": "30dp",
                        "top": "50dp"
                    },
                    "contextualMenu.flxOptionsSeperator": {
                        "bottom": "5dp",
                        "isVisible": true,
                        "top": "85px"
                    },
                    "contextualMenu.imgDownArrow": {
                        "src": "downarrow_2x.png"
                    },
                    "contextualMenu.imgOption1": {
                        "src": "imagedrag.png"
                    },
                    "contextualMenu.imgOption2": {
                        "src": "edit2x.png"
                    },
                    "contextualMenu.imgOption3": {
                        "src": "imagedrag.png"
                    },
                    "contextualMenu.imgOption4": {
                        "src": "deactive_2x.png"
                    },
                    "contextualMenu.imgUpArrow": {
                        "src": "uparrow_2x.png"
                    },
                    "contextualMenu.lblHeader": {
                        "bottom": "15dp",
                        "isVisible": false,
                        "top": "20dp"
                    },
                    "contextualMenu.lblIconOption2": {
                        "i18n_text": "kony.i18n.getLocalizedString(\"i18n.userwidgetmodel.lblIconOption1\")",
                        "left": "20dp"
                    },
                    "contextualMenu.lblIconOption3": {
                        "left": "20dp",
                        "text": ""
                    },
                    "contextualMenu.lblIconOption4": {
                        "left": "20dp"
                    },
                    "contextualMenu.lblOption3": {
                        "text": "Copy Role"
                    },
                    "flxContextualMenu": {
                        "isVisible": false,
                        "left": "viz.val_cleared",
                        "right": "20dp",
                        "width": "137px"
                    },
                    "flxListingSegmentWrapper": {
                        "bottom": "viz.val_cleared",
                        "height": "100%"
                    },
                    "flxPagination": {
                        "isVisible": false
                    },
                    "listingSegmentClient": {
                        "bottom": "viz.val_cleared",
                        "centerX": "viz.val_cleared",
                        "centerY": "viz.val_cleared",
                        "height": "100%",
                        "left": "0dp",
                        "maxHeight": "viz.val_cleared",
                        "maxWidth": "viz.val_cleared",
                        "minHeight": "viz.val_cleared",
                        "minWidth": "viz.val_cleared",
                        "right": "0dp",
                        "top": "0dp",
                        "width": "viz.val_cleared"
                    },
                    "pagination": {
                        "isVisible": false
                    },
                    "rtxNoResultsFound": {
                        "isVisible": false,
                        "top": "100px"
                    },
                    "segListing": {
                        "bottom": "viz.val_cleared",
                        "height": "100%",
                        "isVisible": true
                    }
                }
            }, {
                "overrides": {}
            }, {
                "overrides": {}
            });
            flxDatasourcesSegContainerCreate.add(segDatasourceCreate);
            flxDatasourcesAvailableSegment.add(flxDatasourcesHeaderCreate, flxDatasourceSeparatorCreate, flxDatasourcesSegContainerCreate);
            var flxReportsAvailableSegment = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "bottom": "0dp",
                "clipBounds": true,
                "id": "flxReportsAvailableSegment",
                "isVisible": false,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "isModalContainer": false,
                "right": "0dp",
                "skin": "slFbox",
                "top": "70dp"
            }, {}, {});
            flxReportsAvailableSegment.setDefaultUnit(kony.flex.DP);
            var flxReportsHeader = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "60px",
                "id": "flxReportsHeader",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "30px",
                "isModalContainer": false,
                "right": "20px",
                "skin": "sknflxffffffop100",
                "top": "0"
            }, {}, {});
            flxReportsHeader.setDefaultUnit(kony.flex.DP);
            var flxReportName = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "centerY": "50%",
                "clipBounds": true,
                "height": "100%",
                "id": "flxReportName",
                "isVisible": true,
                "layoutType": kony.flex.FLOW_HORIZONTAL,
                "left": "0px",
                "isModalContainer": false,
                "onClick": controller.AS_FlexContainer_aa9ff028c98444408c909813010b2efa,
                "skin": "slFbox",
                "top": 0,
                "width": "50%",
                "zIndex": 1
            }, {}, {});
            flxReportName.setDefaultUnit(kony.flex.DP);
            var lblReportName = new kony.ui.Label({
                "centerY": "50%",
                "id": "lblReportName",
                "isVisible": true,
                "left": "30px",
                "skin": "sknlblLato696c7311px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmReportsManagement.label.reportName_caps\")",
                "top": 0,
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var fontIconSortReportName = new kony.ui.Label({
                "centerY": "50%",
                "id": "fontIconSortReportName",
                "isVisible": true,
                "left": "3dp",
                "skin": "sknIcon15px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmAlertsManagement.lblSortName\")",
                "top": "0dp",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxReportName.add(lblReportName, fontIconSortReportName);
            var flxReportDescription = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "100%",
                "id": "flxReportDescription",
                "isVisible": true,
                "layoutType": kony.flex.FLOW_HORIZONTAL,
                "left": "50%",
                "isModalContainer": false,
                "onClick": controller.AS_FlexContainer_aa9ff028c98444408c909813010b2efa,
                "skin": "slFbox",
                "top": "0dp",
                "width": "50%",
                "zIndex": 1
            }, {}, {});
            flxReportDescription.setDefaultUnit(kony.flex.DP);
            var lblReportDescription = new kony.ui.Label({
                "centerY": "50%",
                "id": "lblReportDescription",
                "isVisible": true,
                "left": "0px",
                "skin": "sknlblLato696c7311px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmAdManagement.DescriptionCAPS\")",
                "top": 0,
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var fontIconReportDescription = new kony.ui.Label({
                "centerY": "50%",
                "id": "fontIconReportDescription",
                "isVisible": false,
                "left": "3dp",
                "skin": "sknIcon15px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmAlertsManagement.lblSortName\")",
                "top": "0dp",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxReportDescription.add(lblReportDescription, fontIconReportDescription);
            var flxFiltersAvailable = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "100%",
                "id": "flxFiltersAvailable",
                "isVisible": false,
                "layoutType": kony.flex.FLOW_HORIZONTAL,
                "left": "70%",
                "isModalContainer": false,
                "onClick": controller.AS_FlexContainer_aa9ff028c98444408c909813010b2efa,
                "skin": "slFbox",
                "top": "0dp",
                "width": "30%",
                "zIndex": 1
            }, {}, {});
            flxFiltersAvailable.setDefaultUnit(kony.flex.DP);
            var lblFiltersAvailable = new kony.ui.Label({
                "centerY": "50%",
                "id": "lblFiltersAvailable",
                "isVisible": true,
                "left": "0px",
                "skin": "sknlblLato696c7311px",
                "text": "FILTERS AVAILABLE",
                "top": 0,
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var fontIconFiltersAvailable = new kony.ui.Label({
                "centerY": "50%",
                "id": "fontIconFiltersAvailable",
                "isVisible": false,
                "left": "3dp",
                "skin": "sknIcon15px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmAlertsManagement.lblSortName\")",
                "top": "0dp",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxFiltersAvailable.add(lblFiltersAvailable, fontIconFiltersAvailable);
            flxReportsHeader.add(flxReportName, flxReportDescription, flxFiltersAvailable);
            var flxSeparator3 = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "1dp",
                "id": "flxSeparator3",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "20dp",
                "isModalContainer": false,
                "right": "20dp",
                "skin": "sknTableHeaderLine",
                "top": "60dp",
                "zIndex": 1
            }, {}, {});
            flxSeparator3.setDefaultUnit(kony.flex.DP);
            flxSeparator3.add();
            var flxReportsSegContent = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "bottom": "0px",
                "clipBounds": true,
                "id": "flxReportsSegContent",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "61dp",
                "width": "100%",
                "zIndex": 1
            }, {}, {});
            flxReportsSegContent.setDefaultUnit(kony.flex.DP);
            var segReportsCreate = new com.adminConsole.common.listingSegmentClient({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "height": "100%",
                "id": "segReportsCreate",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "masterType": constants.MASTER_TYPE_DEFAULT,
                "isModalContainer": false,
                "right": "0dp",
                "skin": "slFbox",
                "top": "0dp",
                "overrides": {
                    "contextualMenu.btnLink1": {
                        "bottom": "10dp",
                        "isVisible": false,
                        "top": "0dp"
                    },
                    "contextualMenu.btnLink2": {
                        "bottom": "10dp",
                        "isVisible": false,
                        "top": "0dp"
                    },
                    "contextualMenu.contextualMenu1Inner": {
                        "layoutType": kony.flex.FREE_FORM
                    },
                    "contextualMenu.flxOption1": {
                        "isVisible": false,
                        "top": "0dp"
                    },
                    "contextualMenu.flxOption2": {
                        "height": "30dp",
                        "top": "15dp"
                    },
                    "contextualMenu.flxOption3": {
                        "bottom": "10dp",
                        "height": "30dp",
                        "isVisible": true,
                        "top": "90dp"
                    },
                    "contextualMenu.flxOption4": {
                        "bottom": "viz.val_cleared",
                        "height": "30dp",
                        "top": "50dp"
                    },
                    "contextualMenu.flxOptionsSeperator": {
                        "bottom": "5dp",
                        "isVisible": true,
                        "top": "85px"
                    },
                    "contextualMenu.imgDownArrow": {
                        "src": "downarrow_2x.png"
                    },
                    "contextualMenu.imgOption1": {
                        "src": "imagedrag.png"
                    },
                    "contextualMenu.imgOption2": {
                        "src": "edit2x.png"
                    },
                    "contextualMenu.imgOption3": {
                        "src": "imagedrag.png"
                    },
                    "contextualMenu.imgOption4": {
                        "src": "deactive_2x.png"
                    },
                    "contextualMenu.imgUpArrow": {
                        "src": "uparrow_2x.png"
                    },
                    "contextualMenu.lblHeader": {
                        "bottom": "15dp",
                        "isVisible": false,
                        "top": "20dp"
                    },
                    "contextualMenu.lblIconOption2": {
                        "i18n_text": "kony.i18n.getLocalizedString(\"i18n.userwidgetmodel.lblIconOption1\")",
                        "left": "20dp"
                    },
                    "contextualMenu.lblIconOption3": {
                        "left": "20dp",
                        "text": ""
                    },
                    "contextualMenu.lblIconOption4": {
                        "left": "20dp"
                    },
                    "contextualMenu.lblOption3": {
                        "text": "Copy Role"
                    },
                    "flxContextualMenu": {
                        "isVisible": false,
                        "left": "viz.val_cleared",
                        "right": "20dp",
                        "width": "137px"
                    },
                    "flxListingSegmentWrapper": {
                        "bottom": "viz.val_cleared",
                        "height": "100%"
                    },
                    "flxPagination": {
                        "isVisible": false
                    },
                    "listingSegmentClient": {
                        "bottom": "viz.val_cleared",
                        "centerX": "viz.val_cleared",
                        "centerY": "viz.val_cleared",
                        "height": "100%",
                        "left": "0dp",
                        "maxHeight": "viz.val_cleared",
                        "maxWidth": "viz.val_cleared",
                        "minHeight": "viz.val_cleared",
                        "minWidth": "viz.val_cleared",
                        "right": "0dp",
                        "top": "0dp",
                        "width": "viz.val_cleared"
                    },
                    "pagination": {
                        "isVisible": false
                    },
                    "rtxNoResultsFound": {
                        "isVisible": false,
                        "top": "100px"
                    },
                    "segListing": {
                        "bottom": "viz.val_cleared",
                        "height": "100%",
                        "isVisible": true
                    }
                }
            }, {
                "overrides": {}
            }, {
                "overrides": {}
            });
            flxReportsSegContent.add(segReportsCreate);
            flxReportsAvailableSegment.add(flxReportsHeader, flxSeparator3, flxReportsSegContent);
            flxListinCommonCreate.add(flxAvailableReportsHeader, flxSeparator2, flxDatasourcesAvailableSegment, flxReportsAvailableSegment);
            flxCenterContainer.add(flxVerticalTabs, flxListinCommonCreate);
            flxCreateBody.add(flxTopHeader, flxCenterContainer);
            var flxBtns = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "bottom": "0dp",
                "clipBounds": true,
                "height": "80dp",
                "id": "flxBtns",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "isModalContainer": false,
                "right": "0dp",
                "skin": "sknFooter",
                "zIndex": 1
            }, {}, {});
            flxBtns.setDefaultUnit(kony.flex.DP);
            var commonButtons = new com.adminConsole.common.commonButtons({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "height": "80px",
                "id": "commonButtons",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "masterType": constants.MASTER_TYPE_DEFAULT,
                "isModalContainer": false,
                "right": "0dp",
                "skin": "slFbox",
                "top": "0dp",
                "overrides": {
                    "btnCancel": {
                        "left": "20px",
                        "width": "100px"
                    },
                    "btnNext": {
                        "isVisible": false,
                        "left": "viz.val_cleared",
                        "right": "110px",
                        "text": "SAVE "
                    },
                    "btnSave": {
                        "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmReportsManagement.label.createReport_caps\")",
                        "left": "viz.val_cleared",
                        "minWidth": "viz.val_cleared",
                        "right": "20dp",
                        "width": "161dp",
                        "zIndex": 1
                    },
                    "commonButtons": {
                        "left": "0dp",
                        "right": "0dp",
                        "top": "0dp",
                        "width": "viz.val_cleared"
                    },
                    "flxRightButtons": {
                        "width": "300px"
                    }
                }
            }, {
                "overrides": {}
            }, {
                "overrides": {}
            });
            flxBtns.add(commonButtons);
            flxCreateContainer.add(flxTopBar, flxCreateBody, flxBtns);
            var flxLoadingCreatePopup = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "centerX": "50%",
                "centerY": "50%",
                "clipBounds": true,
                "height": "528dp",
                "id": "flxLoadingCreatePopup",
                "isVisible": false,
                "layoutType": kony.flex.FREE_FORM,
                "isModalContainer": false,
                "skin": "sknLoadingBlur",
                "width": "830dp",
                "zIndex": 150
            }, {}, {});
            flxLoadingCreatePopup.setDefaultUnit(kony.flex.DP);
            var flxImageCont = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "centerX": "50%",
                "clipBounds": true,
                "height": "75px",
                "id": "flxImageCont",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "250px",
                "width": "75px",
                "zIndex": 1
            }, {}, {});
            flxImageCont.setDefaultUnit(kony.flex.DP);
            var imageLoading = new kony.ui.Image2({
                "centerX": "50%",
                "centerY": "50%",
                "height": "50px",
                "id": "imageLoading",
                "isVisible": true,
                "skin": "slImage",
                "src": "loadingscreenimage.gif",
                "width": "50px",
                "zIndex": 1
            }, {
                "imageScaleMode": constants.IMAGE_SCALE_MODE_MAINTAIN_ASPECT_RATIO,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxImageCont.add(imageLoading);
            flxLoadingCreatePopup.add(flxImageCont);
            flxCreateReport.add(flxCreateContainer, flxLoadingCreatePopup);
            var flxConfirmationPopup = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "100%",
                "id": "flxConfirmationPopup",
                "isVisible": false,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "masterType": constants.MASTER_TYPE_USERWIDGET,
                "isModalContainer": false,
                "skin": "flxPopupTrans0ba858b5d5d304e",
                "top": "0dp",
                "width": "100%",
                "zIndex": 10
            }, {}, {});
            flxConfirmationPopup.setDefaultUnit(kony.flex.DP);
            var popUp = new com.adminConsole.common.popUp1({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "height": "100%",
                "id": "popUp",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "masterType": constants.MASTER_TYPE_DEFAULT,
                "isModalContainer": false,
                "skin": "sknflxBg000000Op50",
                "top": "0dp",
                "width": "100%",
                "zIndex": 1,
                "overrides": {
                    "btnPopUpCancel": {
                        "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmCustomers.CANCEL\")",
                        "right": "20px",
                        "width": kony.flex.USE_PREFFERED_SIZE
                    },
                    "btnPopUpDelete": {
                        "minWidth": "viz.val_cleared",
                        "text": "DELETE"
                    },
                    "lblPopUpMainMessage": {
                        "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmReportsManagement.label.deleteReport\")"
                    },
                    "popUp1": {
                        "bottom": "viz.val_cleared",
                        "centerX": "viz.val_cleared",
                        "centerY": "viz.val_cleared",
                        "height": "100%",
                        "left": "0dp",
                        "maxHeight": "viz.val_cleared",
                        "maxWidth": "viz.val_cleared",
                        "minHeight": "viz.val_cleared",
                        "minWidth": "viz.val_cleared",
                        "right": "viz.val_cleared",
                        "top": "0dp",
                        "width": "100%"
                    },
                    "rtxPopUpDisclaimer": {
                        "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmReportsManagement.label.deleteReportMessage\")"
                    }
                }
            }, {
                "overrides": {}
            }, {
                "overrides": {}
            });
            flxConfirmationPopup.add(popUp);
            var flxShareReport = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "bottom": "0dp",
                "clipBounds": false,
                "height": "100%",
                "id": "flxShareReport",
                "isVisible": false,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "isModalContainer": false,
                "right": "0dp",
                "skin": "flxPopupTrans0ba858b5d5d304e",
                "top": "0dp",
                "width": "100%",
                "zIndex": 10
            }, {}, {});
            flxShareReport.setDefaultUnit(kony.flex.DP);
            var flxShareContainer = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "centerX": "50%",
                "centerY": "50%",
                "clipBounds": false,
                "height": "516dp",
                "id": "flxShareContainer",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "isModalContainer": false,
                "skin": "slFbox0j9f841cc563e4e",
                "width": "665dp",
                "zIndex": 1
            }, {}, {});
            flxShareContainer.setDefaultUnit(kony.flex.DP);
            var flxTopBarShare = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "10px",
                "id": "flxTopBarShare",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0px",
                "isModalContainer": false,
                "skin": "sknFlxTopColor4A77A0",
                "top": "0dp",
                "width": "100%",
                "zIndex": 1
            }, {}, {});
            flxTopBarShare.setDefaultUnit(kony.flex.DP);
            flxTopBarShare.add();
            var flxShareReportClose = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "15dp",
                "id": "flxShareReportClose",
                "isVisible": true,
                "layoutType": kony.flex.FLOW_VERTICAL,
                "isModalContainer": false,
                "right": "20dp",
                "skin": "slFbox",
                "top": "20dp",
                "width": "15dp"
            }, {}, {
                "hoverSkin": "sknFlxPointer"
            });
            flxShareReportClose.setDefaultUnit(kony.flex.DP);
            var lblShareReportClose = new kony.ui.Label({
                "height": "15dp",
                "id": "lblShareReportClose",
                "isVisible": true,
                "left": "0",
                "skin": "sknIcon00000014px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmAlertsManagement.lblAlertPreviewClose\")",
                "top": "0",
                "width": "15dp",
                "zIndex": 2
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxShareReportClose.add(lblShareReportClose);
            var flxShareReportBody = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "bottom": "80dp",
                "clipBounds": false,
                "id": "flxShareReportBody",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "isModalContainer": false,
                "right": "0dp",
                "skin": "slFbox",
                "top": "45dp",
                "zIndex": 2
            }, {}, {});
            flxShareReportBody.setDefaultUnit(kony.flex.DP);
            var lblShareReportTitle = new kony.ui.Label({
                "height": "16dp",
                "id": "lblShareReportTitle",
                "isVisible": true,
                "left": "0dp",
                "skin": "sknLbl16pxLato192B45",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmReportsManagement.label.shareReport\")",
                "top": "0dp",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 2
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [2, 1, 2, 2],
                "paddingInPixel": false
            }, {});
            var flxSeparatorShareReport = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "1dp",
                "id": "flxSeparatorShareReport",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "20dp",
                "isModalContainer": false,
                "right": "20dp",
                "skin": "sknFlxBgFFFFFFBorE4E6ECsz1pxR3px",
                "top": "38dp",
                "zIndex": 10
            }, {}, {});
            flxSeparatorShareReport.setDefaultUnit(kony.flex.DP);
            flxSeparatorShareReport.add();
            var flxScroll = new kony.ui.FlexScrollContainer({
                "allowHorizontalBounce": false,
                "allowVerticalBounce": true,
                "bottom": "0dp",
                "bounces": true,
                "clipBounds": true,
                "enableScrolling": true,
                "horizontalScrollIndicator": true,
                "id": "flxScroll",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "pagingEnabled": false,
                "right": "0dp",
                "scrollDirection": kony.flex.SCROLL_VERTICAL,
                "skin": "slFSbox",
                "top": "39dp",
                "verticalScrollIndicator": true
            }, {}, {});
            flxScroll.setDefaultUnit(kony.flex.DP);
            var flxShareReportContent = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                "clipBounds": false,
                "id": "flxShareReportContent",
                "isVisible": true,
                "layoutType": kony.flex.FLOW_VERTICAL,
                "left": "20dp",
                "isModalContainer": false,
                "right": "20dp",
                "skin": "slFbox",
                "top": "0dp",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {}, {});
            flxShareReportContent.setDefaultUnit(kony.flex.DP);
            var flxTag = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "20px",
                "id": "flxTag",
                "isVisible": false,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "minWidth": "100dp",
                "isModalContainer": false,
                "skin": "sknFlxBgFFFFFFbor1px003E75",
                "top": "0dp",
                "width": "60dp",
                "zIndex": 1
            }, {}, {});
            flxTag.setDefaultUnit(kony.flex.DP);
            var lblName = new kony.ui.Label({
                "centerY": "50%",
                "height": "12dp",
                "id": "lblName",
                "isVisible": true,
                "left": "5dp",
                "skin": "skn003E75Lato12px",
                "text": "Admin",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var lblIconCrossName = new kony.ui.Label({
                "centerY": "52%",
                "height": "10dp",
                "id": "lblIconCrossName",
                "isVisible": true,
                "right": "5dp",
                "skin": "sknIcon10px003E75",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmAlertsManagement.lblAlertPreviewClose\")",
                "width": "10dp",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {
                "hoverSkin": "sknIcoMoon10pxCursor003E75"
            });
            flxTag.add(lblName, lblIconCrossName);
            var flxRow1 = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                "clipBounds": false,
                "id": "flxRow1",
                "isVisible": true,
                "layoutType": kony.flex.FLOW_VERTICAL,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "15dp",
                "width": "100%",
                "zIndex": 2
            }, {}, {});
            flxRow1.setDefaultUnit(kony.flex.DP);
            var flxRoles = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                "clipBounds": false,
                "id": "flxRoles",
                "isVisible": true,
                "layoutType": kony.flex.FLOW_HORIZONTAL,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0dp",
                "width": "100%"
            }, {}, {});
            flxRoles.setDefaultUnit(kony.flex.DP);
            var lblApps = new kony.ui.Label({
                "id": "lblApps",
                "isVisible": true,
                "left": "0dp",
                "skin": "sknllbl485c75Lato13px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.leftmenu.Roles\")",
                "top": "0dp",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxRoles.add(lblApps);
            var flxNoNameError = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": false,
                "height": "15dp",
                "id": "flxNoNameError",
                "isVisible": false,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "5dp",
                "width": "388dp",
                "zIndex": 1
            }, {}, {});
            flxNoNameError.setDefaultUnit(kony.flex.DP);
            var lblNoNameErrorIcon = new kony.ui.Label({
                "height": "15px",
                "id": "lblNoNameErrorIcon",
                "isVisible": true,
                "left": "0px",
                "skin": "sknErrorIcon",
                "text": "",
                "top": "0px",
                "width": "15px",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var lblNoNameError = new kony.ui.Label({
                "height": "15dp",
                "id": "lblNoNameError",
                "isVisible": true,
                "left": "15dp",
                "skin": "sknlblError",
                "i18n_text": "kony.i18n.getLocalizedString(\"i8n.outageMessages.error.name\")",
                "top": "0dp",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxNoNameError.add(lblNoNameErrorIcon, lblNoNameError);
            var flxRolesContent = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": false,
                "height": "40dp",
                "id": "flxRolesContent",
                "isVisible": true,
                "layoutType": kony.flex.FLOW_VERTICAL,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0dp",
                "width": "100%",
                "zIndex": 2
            }, {}, {});
            flxRolesContent.setDefaultUnit(kony.flex.DP);
            var customListboxRoles = new com.adminConsole.alerts.customListbox({
                "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                "id": "customListboxRoles",
                "isVisible": false,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "masterType": constants.MASTER_TYPE_DEFAULT,
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "10dp",
                "width": "465dp",
                "zIndex": 3,
                "overrides": {
                    "customListbox": {
                        "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                        "isVisible": false,
                        "left": "0dp",
                        "right": "viz.val_cleared",
                        "top": "10dp",
                        "width": "465dp",
                        "zIndex": 3
                    },
                    "flxListboxError": {
                        "isVisible": false,
                        "width": "100%"
                    },
                    "flxSegmentList": {
                        "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                        "clipBounds": false,
                        "isVisible": false,
                        "width": "100%",
                        "zIndex": 4
                    },
                    "flxSelectAll": {
                        "isVisible": true
                    },
                    "flxSelectedText": {
                        "clipBounds": true,
                        "width": "100%"
                    },
                    "imgCheckBox": {
                        "src": "checkboxnormal.png"
                    },
                    "lblErrorMsg": {
                        "i18n_text": "kony.i18n.getLocalizedString(\"i8n.outageMessages.error.apps\")",
                        "isVisible": true,
                        "width": "100%"
                    },
                    "lblSelectedValue": {
                        "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmAlertsManagement.All\")"
                    },
                    "segList": {
                        "data": [{
                            "imgCheckBox": "",
                            "lblDescription": ""
                        }],
                        "height": "100dp",
                        "zIndex": 1
                    }
                }
            }, {
                "overrides": {}
            }, {
                "overrides": {}
            });
            flxRolesContent.add(customListboxRoles);
            var flxRoleTagsContainer = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                "clipBounds": false,
                "id": "flxRoleTagsContainer",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "25dp",
                "width": "100%"
            }, {}, {});
            flxRoleTagsContainer.setDefaultUnit(kony.flex.DP);
            flxRoleTagsContainer.add();
            flxRow1.add(flxRoles, flxNoNameError, flxRolesContent, flxRoleTagsContainer);
            var flxRow2 = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                "clipBounds": false,
                "id": "flxRow2",
                "isVisible": true,
                "layoutType": kony.flex.FLOW_VERTICAL,
                "left": "0dp",
                "isModalContainer": false,
                "right": "20dp",
                "skin": "slFbox",
                "top": "35dp",
                "width": "100%",
                "zIndex": 1
            }, {}, {});
            flxRow2.setDefaultUnit(kony.flex.DP);
            var flxMessageHeader = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                "clipBounds": false,
                "id": "flxMessageHeader",
                "isVisible": true,
                "layoutType": kony.flex.FLOW_HORIZONTAL,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0dp",
                "width": "100%"
            }, {}, {});
            flxMessageHeader.setDefaultUnit(kony.flex.DP);
            var lblAddUsers = new kony.ui.Label({
                "bottom": "0px",
                "id": "lblAddUsers",
                "isVisible": true,
                "left": "0dp",
                "skin": "sknLblLato485c7513px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmReportsManagement.label.addUsers\")",
                "top": 0,
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var lblSelectedCountText = new kony.ui.Label({
                "bottom": 0,
                "id": "lblSelectedCountText",
                "isVisible": true,
                "left": "16dp",
                "skin": "skn003E75Lato12px",
                "text": "4 Selected",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_TOP_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxMessageHeader.add(lblAddUsers, lblSelectedCountText);
            var flxSearchUsers = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                "bottom": "0dp",
                "clipBounds": false,
                "id": "flxSearchUsers",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "isModalContainer": false,
                "right": "20dp",
                "skin": "slFbox",
                "top": "20dp",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {}, {});
            flxSearchUsers.setDefaultUnit(kony.flex.DP);
            var flxSearchUsersContainer = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": false,
                "height": "40px",
                "id": "flxSearchUsersContainer",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "sknflxd5d9ddop100",
                "top": "0dp",
                "width": "508px",
                "zIndex": 1
            }, {}, {});
            flxSearchUsersContainer.setDefaultUnit(kony.flex.DP);
            var fontIconSearchImgUsers = new kony.ui.Label({
                "centerY": "50%",
                "id": "fontIconSearchImgUsers",
                "isVisible": true,
                "left": "12px",
                "skin": "sknFontIconSearchImg20px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.userwidgetmodel.lblSearchIcon\")",
                "top": "0dp",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var tbxSearchBoxUsers = new kony.ui.TextBox2({
                "autoCapitalize": constants.TEXTBOX_AUTO_CAPITALIZE_NONE,
                "centerY": "50%",
                "height": "35px",
                "id": "tbxSearchBoxUsers",
                "isVisible": true,
                "keyBoardStyle": constants.TEXTBOX_KEY_BOARD_STYLE_DEFAULT,
                "left": "30dp",
                "onTouchStart": controller.AS_TextField_cfab06859f0f43eca99495891ee290d5,
                "i18n_placeholder": "kony.i18n.getLocalizedString(\"i18n.frmReportsManagement.placeholder.searchUser\")",
                "right": "35px",
                "secureTextEntry": false,
                "skin": "skntbxffffffNoBorderlato35475f14px",
                "textInputMode": constants.TEXTBOX_INPUT_MODE_ANY,
                "zIndex": 1
            }, {
                "containerHeightMode": constants.TEXTBOX_FONT_METRICS_DRIVEN_HEIGHT,
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [2, 0, 0, 0],
                "paddingInPixel": false
            }, {
                "autoCorrect": false
            });
            var flxClearSearchImageUsers = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "100%",
                "id": "flxClearSearchImageUsers",
                "isVisible": false,
                "layoutType": kony.flex.FREE_FORM,
                "isModalContainer": false,
                "right": "0px",
                "skin": "sknCursor",
                "top": "0dp",
                "width": "35px",
                "zIndex": 1
            }, {}, {
                "hoverSkin": "sknCursor"
            });
            flxClearSearchImageUsers.setDefaultUnit(kony.flex.DP);
            var fontIconCrossUsers = new kony.ui.Label({
                "centerY": "50%",
                "id": "fontIconCrossUsers",
                "isVisible": true,
                "left": "5px",
                "skin": "sknFontIconSearchCross16px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.userwidgetmodel.lblClearSearch\")",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxClearSearchImageUsers.add(fontIconCrossUsers);
            flxSearchUsersContainer.add(fontIconSearchImgUsers, tbxSearchBoxUsers, flxClearSearchImageUsers);
            var flxUsersDropdown = new kony.ui.FlexScrollContainer({
                "allowHorizontalBounce": false,
                "allowVerticalBounce": true,
                "bounces": true,
                "clipBounds": false,
                "enableScrolling": true,
                "height": "200px",
                "horizontalScrollIndicator": true,
                "id": "flxUsersDropdown",
                "isVisible": false,
                "layoutType": kony.flex.FLOW_VERTICAL,
                "left": "0px",
                "pagingEnabled": false,
                "right": "20px",
                "scrollDirection": kony.flex.SCROLL_VERTICAL,
                "skin": "sknffffffbord7d9e03px",
                "top": "40px",
                "verticalScrollIndicator": true,
                "width": "508px",
                "zIndex": 20
            }, {}, {});
            flxUsersDropdown.setDefaultUnit(kony.flex.DP);
            var segUsers = new kony.ui.SegmentedUI2({
                "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                "centerX": "50%",
                "data": [{
                    "lblViewFullName": "Edward"
                }, {
                    "lblViewFullName": "Edward"
                }, {
                    "lblViewFullName": "Edward"
                }, {
                    "lblViewFullName": "Edward"
                }, {
                    "lblViewFullName": "Edward"
                }],
                "groupCells": false,
                "id": "segUsers",
                "isVisible": true,
                "needPageIndicator": true,
                "pageOffDotImage": "pageoffdot.png",
                "pageOnDotImage": "pageondot.png",
                "retainSelection": false,
                "rowFocusSkin": "seg2Focus",
                "rowSkin": "seg2Normal",
                "rowTemplate": "flxUsersDropDown",
                "sectionHeaderSkin": "sliPhoneSegmentHeader",
                "selectionBehavior": constants.SEGUI_DEFAULT_BEHAVIOR,
                "separatorRequired": false,
                "showScrollbars": false,
                "top": "10px",
                "viewType": constants.SEGUI_VIEW_TYPE_TABLEVIEW,
                "widgetDataMap": {
                    "flxName": "flxName",
                    "flxUsersDropDown": "flxUsersDropDown",
                    "lblViewFullName": "lblViewFullName"
                },
                "width": "100%",
                "zIndex": 20
            }, {
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var richTextNoUsers = new kony.ui.RichText({
                "centerX": "50%",
                "centerY": "50%",
                "id": "richTextNoUsers",
                "isVisible": false,
                "left": "0dp",
                "skin": "sknrichtext12pxlatoregular",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmCSR.richtextNoResult\")",
                "top": "0dp",
                "width": "100%",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_CENTER,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxUsersDropdown.add(segUsers, richTextNoUsers);
            var flxUserTagsContainer = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                "clipBounds": true,
                "id": "flxUserTagsContainer",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "isModalContainer": false,
                "right": "0dp",
                "skin": "slFbox",
                "top": "55dp",
                "width": "100%"
            }, {}, {});
            flxUserTagsContainer.setDefaultUnit(kony.flex.DP);
            flxUserTagsContainer.add();
            flxSearchUsers.add(flxSearchUsersContainer, flxUsersDropdown, flxUserTagsContainer);
            var flx20px = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "20px",
                "id": "flx20px",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0dp",
                "width": "100%"
            }, {}, {});
            flx20px.setDefaultUnit(kony.flex.DP);
            flx20px.add();
            flxRow2.add(flxMessageHeader, flxSearchUsers, flx20px);
            flxShareReportContent.add(flxTag, flxRow1, flxRow2);
            flxScroll.add(flxShareReportContent);
            flxShareReportBody.add(lblShareReportTitle, flxSeparatorShareReport, flxScroll);
            var flxBtnsShare = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "bottom": "0dp",
                "clipBounds": false,
                "height": "80dp",
                "id": "flxBtnsShare",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "isModalContainer": false,
                "right": "0dp",
                "skin": "sknFooter",
                "zIndex": 1
            }, {}, {});
            flxBtnsShare.setDefaultUnit(kony.flex.DP);
            var commonShareButtons = new com.adminConsole.common.commonButtons({
                "height": "80px",
                "id": "commonShareButtons",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "masterType": constants.MASTER_TYPE_DEFAULT,
                "isModalContainer": false,
                "right": "0dp",
                "skin": "slFbox",
                "top": "0dp",
                "overrides": {
                    "btnCancel": {
                        "left": "20px",
                        "width": "100px"
                    },
                    "btnNext": {
                        "isVisible": false,
                        "left": "viz.val_cleared",
                        "right": "110px",
                        "text": "SAVE AND UPDATE"
                    },
                    "btnSave": {
                        "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmReportsManagement.label.share_caps\")",
                        "right": "20dp"
                    },
                    "commonButtons": {
                        "isVisible": true,
                        "left": "0dp",
                        "right": "0dp",
                        "top": "0dp",
                        "width": "viz.val_cleared"
                    },
                    "flxRightButtons": {
                        "width": "300px"
                    }
                }
            }, {
                "overrides": {}
            }, {
                "overrides": {}
            });
            flxBtnsShare.add(commonShareButtons);
            flxShareContainer.add(flxTopBarShare, flxShareReportClose, flxShareReportBody, flxBtnsShare);
            var flxLoading2 = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "centerX": "50%",
                "centerY": "50%",
                "clipBounds": true,
                "height": "528dp",
                "id": "flxLoading2",
                "isVisible": false,
                "layoutType": kony.flex.FREE_FORM,
                "isModalContainer": false,
                "skin": "sknLoadingBlur",
                "width": "830dp",
                "zIndex": 150
            }, {}, {});
            flxLoading2.setDefaultUnit(kony.flex.DP);
            var flxImageCont1 = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "centerX": "50%",
                "clipBounds": true,
                "height": "75px",
                "id": "flxImageCont1",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "250px",
                "width": "75px",
                "zIndex": 1
            }, {}, {});
            flxImageCont1.setDefaultUnit(kony.flex.DP);
            var imgLoading1 = new kony.ui.Image2({
                "centerX": "50%",
                "centerY": "50%",
                "height": "50px",
                "id": "imgLoading1",
                "isVisible": true,
                "skin": "slImage",
                "src": "loadingscreenimage.gif",
                "width": "50px",
                "zIndex": 1
            }, {
                "imageScaleMode": constants.IMAGE_SCALE_MODE_MAINTAIN_ASPECT_RATIO,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxImageCont1.add(imgLoading1);
            flxLoading2.add(flxImageCont1);
            flxShareReport.add(flxShareContainer, flxLoading2);
            this.add(flxMain, flxEditCancelConfirmation, flxCreateReport, flxConfirmationPopup, flxShareReport);
        };
        return [{
            "addWidgets": addWidgetsfrmReportsManagement,
            "enabledForIdleTimeout": true,
            "id": "frmReportsManagement",
            "layoutType": kony.flex.FREE_FORM,
            "needAppMenu": false,
            "preShow": function(eventobject) {
                controller.AS_Form_e480b4ce691f4ca0a279c18a8ecc78ae(eventobject);
            },
            "skin": "slForm"
        }, {
            "displayOrientation": constants.FORM_DISPLAY_ORIENTATION_PORTRAIT,
            "layoutType": kony.flex.FREE_FORM,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, {
            "onDeviceBack": controller.AS_Form_a5380e6ae74142b7b1584e46088490a4,
            "retainScrollPosition": false
        }]
    }
});