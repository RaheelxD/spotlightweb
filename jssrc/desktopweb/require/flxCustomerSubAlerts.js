define("flxCustomerSubAlerts", function() {
    return function(controller) {
        var flxCustomerSubAlerts = new kony.ui.FlexContainer({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "clipBounds": true,
            "height": "60px",
            "id": "flxCustomerSubAlerts",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "left": "0dp",
            "isModalContainer": false,
            "skin": "slFbox",
            "top": "0dp",
            "width": "100%"
        }, {}, {});
        flxCustomerSubAlerts.setDefaultUnit(kony.flex.DP);
        var flxHeader = new kony.ui.FlexContainer({
            "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
            "clipBounds": true,
            "id": "flxHeader",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "left": "0px",
            "isModalContainer": false,
            "top": "0px",
            "width": "100%",
            "zIndex": 1
        }, {}, {});
        flxHeader.setDefaultUnit(kony.flex.DP);
        var flxHeaderDescription = new kony.ui.FlexContainer({
            "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
            "clipBounds": true,
            "id": "flxHeaderDescription",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "left": "30px",
            "isModalContainer": false,
            "skin": "slFbox",
            "top": "0px",
            "width": "220px",
            "zIndex": 1
        }, {}, {});
        flxHeaderDescription.setDefaultUnit(kony.flex.DP);
        var lblHeaderDescription = new kony.ui.Label({
            "bottom": "23px",
            "id": "lblHeaderDescription",
            "isVisible": true,
            "left": "0px",
            "skin": "sknlatoregular485c75Font10px",
            "text": "DISCRIPTION",
            "top": "23px",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, {
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, {});
        flxHeaderDescription.add(lblHeaderDescription);
        var flxHeaderStatus = new kony.ui.FlexContainer({
            "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
            "clipBounds": true,
            "id": "flxHeaderStatus",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "left": "250px",
            "isModalContainer": false,
            "skin": "slFbox",
            "top": "0px",
            "width": "120px",
            "zIndex": 1
        }, {}, {});
        flxHeaderStatus.setDefaultUnit(kony.flex.DP);
        var lblHeaderStatus = new kony.ui.Label({
            "bottom": "23px",
            "id": "lblHeaderStatus",
            "isVisible": true,
            "left": "15px",
            "skin": "sknlatoregular485c75Font10px",
            "text": "STATUS",
            "top": "23px",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, {
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, {});
        var lblHeaderStatusIcon = new kony.ui.Label({
            "id": "lblHeaderStatusIcon",
            "isVisible": true,
            "left": "0px",
            "skin": "sknFontIconActivate",
            "text": "",
            "top": "24px",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, {
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, {});
        flxHeaderStatus.add(lblHeaderStatus, lblHeaderStatusIcon);
        var flxValue = new kony.ui.FlexContainer({
            "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
            "clipBounds": true,
            "id": "flxValue",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "left": "370px",
            "isModalContainer": false,
            "skin": "slFbox",
            "top": "0px",
            "width": "120px",
            "zIndex": 1
        }, {}, {});
        flxValue.setDefaultUnit(kony.flex.DP);
        var lblValue = new kony.ui.Label({
            "bottom": "23px",
            "id": "lblValue",
            "isVisible": true,
            "left": "0px",
            "skin": "sknlatoregular485c75Font10px",
            "text": "Value",
            "top": "23px",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, {
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, {});
        flxValue.add(lblValue);
        var flxHeaderPush = new kony.ui.FlexContainer({
            "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
            "clipBounds": true,
            "id": "flxHeaderPush",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "isModalContainer": false,
            "right": 205,
            "skin": "slFbox",
            "top": "0px",
            "width": 36,
            "zIndex": 1
        }, {}, {});
        flxHeaderPush.setDefaultUnit(kony.flex.DP);
        var lblHeaderPush = new kony.ui.Label({
            "bottom": "20px",
            "id": "lblHeaderPush",
            "isVisible": true,
            "left": "0px",
            "skin": "sknIcomoonGreenTick",
            "text": "PUSH",
            "top": "20px",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, {
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, {});
        flxHeaderPush.add(lblHeaderPush);
        var flxHeaderSMS = new kony.ui.FlexContainer({
            "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
            "clipBounds": true,
            "id": "flxHeaderSMS",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "isModalContainer": false,
            "right": "124px",
            "skin": "slFbox",
            "top": "0px",
            "width": "28px",
            "zIndex": 1
        }, {}, {});
        flxHeaderSMS.setDefaultUnit(kony.flex.DP);
        var lblHeaderSMS = new kony.ui.Label({
            "bottom": "20px",
            "id": "lblHeaderSMS",
            "isVisible": true,
            "left": "0px",
            "skin": "sknIcomoonGreenTick",
            "text": "SMS",
            "top": "20px",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, {
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, {});
        flxHeaderSMS.add(lblHeaderSMS);
        var flxHeaderEmail = new kony.ui.FlexContainer({
            "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
            "clipBounds": true,
            "id": "flxHeaderEmail",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "isModalContainer": false,
            "right": "30px",
            "skin": "slFbox",
            "top": "0px",
            "width": "41px",
            "zIndex": 1
        }, {}, {});
        flxHeaderEmail.setDefaultUnit(kony.flex.DP);
        var lblheaderEmail = new kony.ui.Label({
            "bottom": "20px",
            "id": "lblheaderEmail",
            "isVisible": true,
            "right": "0px",
            "skin": "sknIcomoonGreenTick",
            "text": "EMAIL",
            "top": "23px",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, {
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, {});
        flxHeaderEmail.add(lblheaderEmail);
        var flxSeprator = new kony.ui.FlexContainer({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "clipBounds": true,
            "height": "2px",
            "id": "flxSeprator",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "left": "0dp",
            "isModalContainer": false,
            "skin": "sknFlxF0EFF4NoBorder",
            "top": 0,
            "width": "100%",
            "zIndex": 1
        }, {}, {});
        flxSeprator.setDefaultUnit(kony.flex.DP);
        flxSeprator.add();
        flxHeader.add(flxHeaderDescription, flxHeaderStatus, flxValue, flxHeaderPush, flxHeaderSMS, flxHeaderEmail, flxSeprator);
        flxCustomerSubAlerts.add(flxHeader);
        return flxCustomerSubAlerts;
    }
})