define("CustomerGroupsModule/frmGroups", function() {
    return function(controller) {
        function addWidgetsfrmGroups() {
            this.setDefaultUnit(kony.flex.DP);
            var flxMain = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "900dp",
                "id": "flxMain",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "slFbox0b7aaa2e3bfa340",
                "top": "0dp",
                "width": "100%",
                "zIndex": 1
            }, {}, {});
            flxMain.setDefaultUnit(kony.flex.DP);
            var flxLeftPannel = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "100%",
                "id": "flxLeftPannel",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "sknLeftPannel0bb79e4bcca8e45",
                "top": "0dp",
                "width": "305px",
                "zIndex": 5
            }, {}, {});
            flxLeftPannel.setDefaultUnit(kony.flex.DP);
            var leftMenuNew = new com.adminConsole.navigation.leftMenuNew({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "height": "100%",
                "id": "leftMenuNew",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "masterType": constants.MASTER_TYPE_DEFAULT,
                "isModalContainer": false,
                "skin": "sknFlxBg003E75Op100",
                "top": "0dp",
                "width": "100%",
                "overrides": {
                    "brandLogo": {
                        "src": "infinity_dbx_c360_logo_white_2x.png"
                    },
                    "imgBottomLogo": {
                        "src": "logo_white_2x.png"
                    },
                    "leftMenuNew": {
                        "bottom": "viz.val_cleared",
                        "centerX": "viz.val_cleared",
                        "centerY": "viz.val_cleared",
                        "height": "100%",
                        "left": "0dp",
                        "maxHeight": "viz.val_cleared",
                        "maxWidth": "viz.val_cleared",
                        "minHeight": "viz.val_cleared",
                        "minWidth": "viz.val_cleared",
                        "right": "viz.val_cleared",
                        "top": "0dp",
                        "width": "100%"
                    },
                    "segMenu": {
                        "left": "0dp",
                        "top": "0dp"
                    }
                }
            }, {
                "overrides": {}
            }, {
                "overrides": {}
            });
            flxLeftPannel.add(leftMenuNew);
            var flxRightPannel = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "100%",
                "id": "flxRightPannel",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "305px",
                "isModalContainer": false,
                "right": 0,
                "skin": "slFbox0b7aaa2e3bfa340",
                "top": "0.00%",
                "zIndex": 5
            }, {}, {});
            flxRightPannel.setDefaultUnit(kony.flex.DP);
            var flxMainHeader = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "106dp",
                "id": "flxMainHeader",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0dp",
                "width": "100%",
                "zIndex": 1
            }, {}, {});
            flxMainHeader.setDefaultUnit(kony.flex.DP);
            var mainHeader = new com.adminConsole.header.mainHeader({
                "height": "106px",
                "id": "mainHeader",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0",
                "masterType": constants.MASTER_TYPE_DEFAULT,
                "isModalContainer": false,
                "skin": "CopyslFbox1",
                "top": "0",
                "width": "100%",
                "overrides": {
                    "btnAddNewOption": {
                        "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmGroups.ADD_NEW_GROUP\")",
                        "right": "170dp"
                    },
                    "btnDropdownList": {
                        "i18n_text": "kony.i18n.getLocalizedString(\"i18n.mainHeader.DOWNLOADLIST\")"
                    },
                    "flxHeaderSeperator": {
                        "isVisible": true
                    },
                    "imgLogout": {
                        "right": "0px",
                        "src": "img_logout.png"
                    },
                    "lblHeading": {
                        "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmRolesController.View_Roles\")"
                    },
                    "lblUserName": {
                        "right": "25px",
                        "text": "Preetish",
                        "top": "viz.val_cleared"
                    },
                    "mainHeader": {
                        "left": "0",
                        "top": "0"
                    }
                }
            }, {
                "overrides": {}
            }, {
                "overrides": {}
            });
            flxMainHeader.add(mainHeader);
            var flxBreadCrumbs = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "30px",
                "id": "flxBreadCrumbs",
                "isVisible": false,
                "layoutType": kony.flex.FLOW_HORIZONTAL,
                "left": "0dp",
                "masterType": constants.MASTER_TYPE_USERWIDGET,
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "93px",
                "width": "100%",
                "zIndex": 1
            }, {}, {});
            flxBreadCrumbs.setDefaultUnit(kony.flex.DP);
            var breadcrumbs = new com.adminConsole.common.breadcrumbs({
                "centerY": "50%",
                "height": "20px",
                "id": "breadcrumbs",
                "isVisible": true,
                "layoutType": kony.flex.FLOW_HORIZONTAL,
                "left": "0dp",
                "masterType": constants.MASTER_TYPE_DEFAULT,
                "isModalContainer": false,
                "skin": "sknflxf5f6f8Op100",
                "top": "20dp",
                "width": "100%",
                "overrides": {
                    "breadcrumbs": {
                        "centerY": "50%",
                        "top": "20dp"
                    },
                    "btnBackToMain": {
                        "text": "CUSTOMER ROLES"
                    },
                    "imgBreadcrumbsDown": {
                        "src": "img_down_arrow.png"
                    },
                    "imgBreadcrumbsRight": {
                        "src": "img_breadcrumb_arrow.png"
                    },
                    "imgBreadcrumbsRight2": {
                        "src": "img_breadcrumb_arrow.png"
                    },
                    "imgBreadcrumbsRight3": {
                        "src": "img_breadcrumb_arrow.png"
                    }
                }
            }, {
                "overrides": {}
            }, {
                "overrides": {}
            });
            flxBreadCrumbs.add(breadcrumbs);
            var flxToastMessage = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "bottom": "-70px",
                "clipBounds": true,
                "height": "70px",
                "id": "flxToastMessage",
                "isVisible": false,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0px",
                "isModalContainer": false,
                "right": "0px",
                "skin": "slFbox",
                "zIndex": 99
            }, {}, {});
            flxToastMessage.setDefaultUnit(kony.flex.DP);
            var toastMessage = new com.adminConsole.common.toastMessage({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "height": "70px",
                "id": "toastMessage",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "masterType": constants.MASTER_TYPE_DEFAULT,
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0dp",
                "width": "100%",
                "overrides": {
                    "lbltoastMessage": {
                        "text": "Branch deactivated successfully"
                    }
                }
            }, {
                "overrides": {}
            }, {
                "overrides": {}
            });
            flxToastMessage.add(toastMessage);
            var flxNoGroups = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                "clipBounds": true,
                "id": "flxNoGroups",
                "isVisible": false,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "106dp",
                "width": "100%",
                "zIndex": 1
            }, {}, {});
            flxNoGroups.setDefaultUnit(kony.flex.DP);
            var noStaticData = new com.adminConsole.staticContent.noStaticData({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "id": "noStaticData",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "masterType": constants.MASTER_TYPE_DEFAULT,
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0dp",
                "zIndex": 1,
                "overrides": {
                    "btnAddStaticContent": {
                        "i18n_text": "kony.i18n.getLocalizedString(\"i18n.DragBox.ADD\")",
                        "width": "70dp"
                    },
                    "lblNoStaticContentCreated": {
                        "i18n_text": "kony.i18n.getLocalizedString(\"i18n.Groups.noGroupAdded\")"
                    },
                    "lblNoStaticContentMsg": {
                        "i18n_text": "kony.i18n.getLocalizedString(\"i18n.Groups.AddGroup\")"
                    }
                }
            }, {
                "overrides": {}
            }, {
                "overrides": {}
            });
            flxNoGroups.add(noStaticData);
            var flxAddGroups = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "bottom": "15px",
                "clipBounds": true,
                "id": "flxAddGroups",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "35dp",
                "isModalContainer": false,
                "right": "35px",
                "skin": "sknflxffffffop100Bordercbcdd1Radius3px",
                "top": "131dp",
                "zIndex": 1
            }, {}, {});
            flxAddGroups.setDefaultUnit(kony.flex.DP);
            var flxVerticalTabs = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "bottom": "0px",
                "clipBounds": true,
                "id": "flxVerticalTabs",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0dp",
                "width": "230px",
                "zIndex": 1
            }, {}, {});
            flxVerticalTabs.setDefaultUnit(kony.flex.DP);
            var verticalTabs = new com.adminConsole.common.verticalTabs1({
                "height": "100%",
                "id": "verticalTabs",
                "isVisible": true,
                "layoutType": kony.flex.FLOW_VERTICAL,
                "left": "0dp",
                "masterType": constants.MASTER_TYPE_DEFAULT,
                "isModalContainer": false,
                "skin": "sknflxf9f9f9NoBorder",
                "top": "0dp",
                "width": "100%",
                "overrides": {
                    "btnOption1": {
                        "bottom": "0dp",
                        "centerX": "viz.val_cleared",
                        "centerY": "viz.val_cleared",
                        "i18n_text": "kony.i18n.getLocalizedString(\"i18n.Group.GroupDetails\")",
                        "left": "20px",
                        "right": "39dp",
                        "top": "0dp",
                        "width": "viz.val_cleared"
                    },
                    "btnOption2": {
                        "left": "20px",
                        "right": "40dp",
                        "text": "ASSIGN FEATURES AND ACTIONS",
                        "top": "0dp",
                        "width": "viz.val_cleared"
                    },
                    "btnOption3": {
                        "i18n_text": "kony.i18n.getLocalizedString(\"i18n.Group.ASSIGNCUSTOMERS\")",
                        "left": "20px",
                        "right": "40dp",
                        "width": "viz.val_cleared"
                    },
                    "btnOption4": {
                        "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmCustomerCreate.AssignLimits_UC\")",
                        "left": "20px",
                        "right": 40,
                        "width": "viz.val_cleared"
                    },
                    "flxContentContainer4": {
                        "height": "20dp"
                    },
                    "flxImgArrow1": {
                        "bottom": "0dp",
                        "centerY": "viz.val_cleared",
                        "right": "20px",
                        "top": "0dp"
                    },
                    "flxImgArrow2": {
                        "bottom": "0dp",
                        "right": "20px",
                        "top": "0px"
                    },
                    "flxImgArrow3": {
                        "right": "20px"
                    },
                    "flxImgArrow4": {
                        "right": "20px"
                    },
                    "flxOption3": {
                        "isVisible": false
                    },
                    "flxOption4": {
                        "autogrowMode": kony.flex.AUTOGROW_HEIGHT
                    },
                    "imgSelected1": {
                        "src": "right_arrow2x.png"
                    },
                    "imgSelected2": {
                        "src": "right_arrow2x.png"
                    },
                    "imgSelected3": {
                        "src": "right_arrow2x.png"
                    },
                    "imgSelected4": {
                        "src": "right_arrow2x.png"
                    },
                    "lblOptional2": {
                        "isVisible": false,
                        "left": "20px",
                        "top": "10px"
                    },
                    "lblOptional3": {
                        "left": "20px",
                        "top": "10px"
                    },
                    "lblOptional4": {
                        "left": "20px",
                        "top": "12px"
                    },
                    "lblSelected1": {
                        "centerX": "50%",
                        "centerY": "50%"
                    },
                    "lblSelected2": {
                        "centerY": "50%"
                    }
                }
            }, {
                "overrides": {}
            }, {
                "overrides": {}
            });
            flxVerticalTabs.add(verticalTabs);
            var flxGroupDetails = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "bottom": "0dp",
                "clipBounds": true,
                "id": "flxGroupDetails",
                "isVisible": false,
                "layoutType": kony.flex.FREE_FORM,
                "left": "230dp",
                "isModalContainer": false,
                "right": "0dp",
                "skin": "slFbox",
                "top": "0dp",
                "zIndex": 1
            }, {}, {});
            flxGroupDetails.setDefaultUnit(kony.flex.DP);
            var flxAddGroupDetails = new kony.ui.FlexScrollContainer({
                "allowHorizontalBounce": false,
                "allowVerticalBounce": true,
                "bottom": "81dp",
                "bounces": true,
                "clipBounds": true,
                "enableScrolling": true,
                "horizontalScrollIndicator": true,
                "id": "flxAddGroupDetails",
                "isVisible": true,
                "layoutType": kony.flex.FLOW_VERTICAL,
                "left": "0dp",
                "pagingEnabled": false,
                "right": 0,
                "scrollDirection": kony.flex.SCROLL_VERTICAL,
                "skin": "slFSbox",
                "top": "0dp",
                "verticalScrollIndicator": true,
                "zIndex": 1
            }, {}, {});
            flxAddGroupDetails.setDefaultUnit(kony.flex.DP);
            var flxRow1 = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "90dp",
                "id": "flxRow1",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "20dp",
                "width": "100%"
            }, {}, {});
            flxRow1.setDefaultUnit(kony.flex.DP);
            var flxName = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "90px",
                "id": "flxName",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "20dp",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0dp",
                "width": "46%",
                "zIndex": 1
            }, {}, {});
            flxName.setDefaultUnit(kony.flex.DP);
            var lblGroupNameKey = new kony.ui.Label({
                "id": "lblGroupNameKey",
                "isVisible": true,
                "left": "0px",
                "skin": "slLabel0d20174dce8ea42",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.Group.NameoftheGroup\")",
                "top": 0,
                "width": "100%",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var lblGroupNameCount = new kony.ui.Label({
                "id": "lblGroupNameCount",
                "isVisible": false,
                "right": "2px",
                "skin": "slLabel0d20174dce8ea42",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmGroups.lblGroupNameCount\")",
                "top": "0px",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var tbxGroupNameValue = new kony.ui.TextBox2({
                "autoCapitalize": constants.TEXTBOX_AUTO_CAPITALIZE_NONE,
                "focusSkin": "skntxtbxDetailsFocus14px",
                "height": "40dp",
                "id": "tbxGroupNameValue",
                "isVisible": true,
                "keyBoardStyle": constants.TEXTBOX_KEY_BOARD_STYLE_DEFAULT,
                "left": "0dp",
                "maxTextLength": 100,
                "placeholder": "Enter Customer Role Name",
                "secureTextEntry": false,
                "skin": "skntbxLato35475f14px",
                "textInputMode": constants.TEXTBOX_INPUT_MODE_ANY,
                "top": "30dp",
                "width": "100%",
                "zIndex": 1
            }, {
                "containerHeightMode": constants.TEXTBOX_FONT_METRICS_DRIVEN_HEIGHT,
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [3, 0, 0, 0],
                "paddingInPixel": false
            }, {
                "autoCorrect": false
            });
            var flxNoGroupNameError = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "25dp",
                "id": "flxNoGroupNameError",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "70dp",
                "width": "100%"
            }, {}, {});
            flxNoGroupNameError.setDefaultUnit(kony.flex.DP);
            var lblNoGroupNameError = new kony.ui.Label({
                "centerY": "50%",
                "height": "15dp",
                "id": "lblNoGroupNameError",
                "isVisible": true,
                "left": "15dp",
                "skin": "sknlblError",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmGroups.lblNoGroupNameError\")",
                "top": "0dp",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var lblNoGroupNameErrorIcon = new kony.ui.Label({
                "centerY": "52%",
                "height": "15dp",
                "id": "lblNoGroupNameErrorIcon",
                "isVisible": true,
                "left": "0dp",
                "skin": "sknErrorIcon",
                "text": "",
                "width": "15dp",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxNoGroupNameError.add(lblNoGroupNameError, lblNoGroupNameErrorIcon);
            var imgMandatoryName = new kony.ui.Image2({
                "height": "12px",
                "id": "imgMandatoryName",
                "isVisible": false,
                "left": "110px",
                "skin": "slImage",
                "src": "mandatory_2x.png",
                "top": "3px",
                "width": "6px",
                "zIndex": 1
            }, {
                "imageScaleMode": constants.IMAGE_SCALE_MODE_MAINTAIN_ASPECT_RATIO,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxName.add(lblGroupNameKey, lblGroupNameCount, tbxGroupNameValue, flxNoGroupNameError, imgMandatoryName);
            var flxType = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "90dp",
                "id": "flxType",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "52%",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0dp",
                "width": "46%",
                "zIndex": 1
            }, {}, {});
            flxType.setDefaultUnit(kony.flex.DP);
            var lblTypeHeader = new kony.ui.Label({
                "id": "lblTypeHeader",
                "isVisible": true,
                "left": "0px",
                "skin": "slLabel0d20174dce8ea42",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.Group.RoleType\")",
                "top": 0,
                "width": "100%",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var lstBoxType = new kony.ui.ListBox({
                "focusSkin": "defListBoxFocus",
                "height": "40dp",
                "id": "lstBoxType",
                "isVisible": true,
                "left": "0dp",
                "masterData": [
                    ["lb1", "Placeholder One"],
                    ["lb2", "Placeholder Two"],
                    ["lb3", "Placeholder Three"]
                ],
                "skin": "sknlbxBgffffffBorderc1c9ceRadius3Px",
                "top": "30dp",
                "width": "100%",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [3, 0, 0, 0],
                "paddingInPixel": false
            }, {
                "hoverSkin": "sknlbxBgffffffBorderc1c9ceRadius3Px",
                "multiSelect": false
            });
            var flxTypeError = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "25dp",
                "id": "flxTypeError",
                "isVisible": false,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "70dp",
                "width": "100%"
            }, {}, {});
            flxTypeError.setDefaultUnit(kony.flex.DP);
            var lblIconTypeError = new kony.ui.Label({
                "centerY": "52%",
                "height": "15dp",
                "id": "lblIconTypeError",
                "isVisible": true,
                "left": "0dp",
                "skin": "sknErrorIcon",
                "text": "",
                "top": "0dp",
                "width": "15dp",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var lblTypeError = new kony.ui.Label({
                "centerY": "50%",
                "height": "15dp",
                "id": "lblTypeError",
                "isVisible": true,
                "left": "15dp",
                "skin": "sknlblError",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmGroups.Please_select_a_role_type\")",
                "top": "0dp",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxTypeError.add(lblIconTypeError, lblTypeError);
            flxType.add(lblTypeHeader, lstBoxType, flxTypeError);
            flxRow1.add(flxName, flxType);
            var flxValidity = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "75px",
                "id": "flxValidity",
                "isVisible": false,
                "layoutType": kony.flex.FREE_FORM,
                "isModalContainer": false,
                "right": "30dp",
                "skin": "slFbox",
                "top": "0dp",
                "width": "50%",
                "zIndex": 1
            }, {}, {});
            flxValidity.setDefaultUnit(kony.flex.DP);
            var lblValidity = new kony.ui.Label({
                "id": "lblValidity",
                "isVisible": true,
                "left": "0px",
                "skin": "slLabel0d20174dce8ea42",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmGroups.lblValidity\")",
                "top": 0,
                "width": "40%",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var CopylblValidity0d9c3f33b4acc44 = new kony.ui.Label({
                "id": "CopylblValidity0d9c3f33b4acc44",
                "isVisible": true,
                "left": "53%",
                "skin": "slLabel0d20174dce8ea42",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmGroups.CopylblValidity0d9c3f33b4acc44\")",
                "top": 0,
                "width": "40%",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var calValidStartDate = new kony.ui.Calendar({
                "calendarIcon": "calicon.png",
                "dateFormat": "dd/MM/yyyy",
                "height": "40dp",
                "id": "calValidStartDate",
                "isVisible": true,
                "left": "0dp",
                "placeholder": "  DD/MM/YYYY",
                "skin": "sknCal35475f14Px",
                "top": "30dp",
                "viewConfig": {
                    "gridConfig": {
                        "allowWeekendSelectable": true
                    }
                },
                "viewType": constants.CALENDAR_VIEW_TYPE_GRID_POPUP,
                "width": "47%",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_TOP_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {
                "noOfMonths": 1
            });
            var calValidEndDate = new kony.ui.Calendar({
                "calendarIcon": "calicon.png",
                "dateFormat": "dd/MM/yyyy",
                "height": "40dp",
                "id": "calValidEndDate",
                "isVisible": true,
                "placeholder": "  DD/MM/YYYY",
                "right": 2,
                "skin": "sknCal35475f14Px",
                "top": "30dp",
                "viewType": constants.CALENDAR_VIEW_TYPE_DEFAULT,
                "width": "47%",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_TOP_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {
                "noOfMonths": 1
            });
            var Label0b4617ed1f1af41 = new kony.ui.Label({
                "id": "Label0b4617ed1f1af41",
                "isVisible": true,
                "left": "406dp",
                "skin": "slLabel0a3041ed9d2454e",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmAlertsManagement.lblAlertTypeDescriptionCount\")",
                "top": "51dp",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var Label0a005cb85bbe644 = new kony.ui.Label({
                "id": "Label0a005cb85bbe644",
                "isVisible": true,
                "left": "67dp",
                "skin": "slLabel0d9a2a20de91c43",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmAlertsManagement.lblAlertTypeDescriptionCount\")",
                "top": "-13dp",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxValidity.add(lblValidity, CopylblValidity0d9c3f33b4acc44, calValidStartDate, calValidEndDate, Label0b4617ed1f1af41, Label0a005cb85bbe644);
            var flxBusinessTypesContainer = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                "bottom": "15dp",
                "clipBounds": true,
                "id": "flxBusinessTypesContainer",
                "isVisible": false,
                "layoutType": kony.flex.FLOW_VERTICAL,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "8dp",
                "width": "100%"
            }, {}, {});
            flxBusinessTypesContainer.setDefaultUnit(kony.flex.DP);
            var flxBusinessTypes = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                "clipBounds": true,
                "id": "flxBusinessTypes",
                "isVisible": true,
                "layoutType": kony.flex.FLOW_VERTICAL,
                "left": "20dp",
                "isModalContainer": false,
                "right": "20dp",
                "skin": "sknflxffffffop100Bordercbcdd1Radius3px",
                "top": "0dp",
                "width": "96%"
            }, {}, {});
            flxBusinessTypes.setDefaultUnit(kony.flex.DP);
            var flxBusinessTypesHeader = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "46px",
                "id": "flxBusinessTypesHeader",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0px",
                "isModalContainer": false,
                "skin": "sknf5f6f8border0px",
                "top": "0",
                "width": "100%"
            }, {}, {});
            flxBusinessTypesHeader.setDefaultUnit(kony.flex.DP);
            var lblBusinessTypeName = new kony.ui.Label({
                "centerY": "50%",
                "id": "lblBusinessTypeName",
                "isVisible": true,
                "left": "20px",
                "skin": "sknLbl485C75LatoRegular13Px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.Group.RetailServices\")",
                "top": 0,
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var lblSelectBusinessType = new kony.ui.Label({
                "centerY": "50%",
                "id": "lblSelectBusinessType",
                "isVisible": true,
                "left": "41%",
                "skin": "sknLbl485C75LatoRegular13Px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.Group.selectServiceType\")",
                "top": 0,
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var flxDefaultType = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "100%",
                "id": "flxDefaultType",
                "isVisible": false,
                "layoutType": kony.flex.FLOW_HORIZONTAL,
                "left": "68%",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0dp",
                "width": "30%"
            }, {}, {});
            flxDefaultType.setDefaultUnit(kony.flex.DP);
            var lblDefaultBusinessType = new kony.ui.Label({
                "centerY": "50%",
                "id": "lblDefaultBusinessType",
                "isVisible": true,
                "left": "0dp",
                "skin": "sknLbl485C75LatoRegular13Px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.Group.setRoleDefault\")",
                "top": 0,
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var flxDefaultInfo = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                "centerY": "50%",
                "clipBounds": true,
                "id": "flxDefaultInfo",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "10dp",
                "isModalContainer": false,
                "onClick": controller.AS_FlexContainer_aeb8168ea3624740bf06d583316c5596,
                "skin": "sknCursor",
                "width": "17dp",
                "zIndex": 2
            }, {}, {});
            flxDefaultInfo.setDefaultUnit(kony.flex.DP);
            var fontIconDefaultInfo = new kony.ui.Label({
                "id": "fontIconDefaultInfo",
                "isVisible": true,
                "left": "0dp",
                "skin": "sknfontIcon33333316px",
                "text": "",
                "top": "0dp",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_TOP_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxDefaultInfo.add(fontIconDefaultInfo);
            flxDefaultType.add(lblDefaultBusinessType, flxDefaultInfo);
            var flxCheckboxSelect = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                "clipBounds": true,
                "id": "flxCheckboxSelect",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "38%",
                "isModalContainer": false,
                "skin": "sknCursor",
                "top": "18dp",
                "width": "20px",
                "zIndex": 2
            }, {}, {});
            flxCheckboxSelect.setDefaultUnit(kony.flex.DP);
            var imgCheckBoxSelect = new kony.ui.Image2({
                "height": "12px",
                "id": "imgCheckBoxSelect",
                "isVisible": true,
                "left": "4px",
                "skin": "slImage",
                "src": "checkbox.png",
                "top": "0dp",
                "width": "12px",
                "zIndex": 2
            }, {
                "imageScaleMode": constants.IMAGE_SCALE_MODE_MAINTAIN_ASPECT_RATIO,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxCheckboxSelect.add(imgCheckBoxSelect);
            flxBusinessTypesHeader.add(lblBusinessTypeName, lblSelectBusinessType, flxDefaultType, flxCheckboxSelect);
            var flxBusinessTypeseparator = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "1dp",
                "id": "flxBusinessTypeseparator",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "isModalContainer": false,
                "right": "0dp",
                "skin": "sknflxe1e5edop100",
                "top": "0dp",
                "zIndex": 1
            }, {}, {});
            flxBusinessTypeseparator.setDefaultUnit(kony.flex.DP);
            flxBusinessTypeseparator.add();
            var lblEditBusinessTypeMsg = new kony.ui.Label({
                "id": "lblEditBusinessTypeMsg",
                "isVisible": true,
                "left": "20px",
                "skin": "skn73767812pxLato",
                "text": "Services and Default Role cannot be removed if it has been assigned to few customers.",
                "top": 12,
                "width": "90%",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var segRoleBusinessTypes = new kony.ui.SegmentedUI2({
                "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                "bottom": "15px",
                "data": [{
                    "imgCheckBoxDefault": "checkbox.png",
                    "imgCheckBoxSelect": "checkbox.png",
                    "lblDescription": "Mobile App"
                }, {
                    "imgCheckBoxDefault": "checkbox.png",
                    "imgCheckBoxSelect": "checkbox.png",
                    "lblDescription": "Trust"
                }],
                "groupCells": false,
                "id": "segRoleBusinessTypes",
                "isVisible": true,
                "left": "0px",
                "maxHeight": "300px",
                "needPageIndicator": true,
                "pageOffDotImage": "pageoffdot.png",
                "pageOnDotImage": "pageondot.png",
                "retainSelection": false,
                "right": "0px",
                "rowFocusSkin": "seg2Focus",
                "rowSkin": "sknseg3pxRadius",
                "rowTemplate": "flxRoleBusinessTypes",
                "sectionHeaderSkin": "sliPhoneSegmentHeader",
                "selectionBehavior": constants.SEGUI_DEFAULT_BEHAVIOR,
                "separatorRequired": false,
                "showScrollbars": false,
                "top": "0dp",
                "viewType": constants.SEGUI_VIEW_TYPE_TABLEVIEW,
                "widgetDataMap": {
                    "flxCheckboxDefault": "flxCheckboxDefault",
                    "flxCheckboxSelect": "flxCheckboxSelect",
                    "flxRoleBusinessTypes": "flxRoleBusinessTypes",
                    "imgCheckBoxDefault": "imgCheckBoxDefault",
                    "imgCheckBoxSelect": "imgCheckBoxSelect",
                    "lblDescription": "lblDescription"
                },
                "zIndex": 1
            }, {
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxBusinessTypes.add(flxBusinessTypesHeader, flxBusinessTypeseparator, lblEditBusinessTypeMsg, segRoleBusinessTypes);
            var flxSelectOtherServices = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "25dp",
                "id": "flxSelectOtherServices",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "20dp",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "10dp",
                "width": "96%"
            }, {}, {});
            flxSelectOtherServices.setDefaultUnit(kony.flex.DP);
            var flxCheckboxSelect2 = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                "clipBounds": true,
                "id": "flxCheckboxSelect2",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "sknCursor",
                "top": "5dp",
                "width": "20px",
                "zIndex": 2
            }, {}, {});
            flxCheckboxSelect2.setDefaultUnit(kony.flex.DP);
            var imgCheckBoxSelect2 = new kony.ui.Image2({
                "height": "12px",
                "id": "imgCheckBoxSelect2",
                "isVisible": true,
                "left": "0px",
                "skin": "slImage",
                "src": "checkbox.png",
                "top": "0dp",
                "width": "12px",
                "zIndex": 2
            }, {
                "imageScaleMode": constants.IMAGE_SCALE_MODE_MAINTAIN_ASPECT_RATIO,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxCheckboxSelect2.add(imgCheckBoxSelect2);
            var lblApplyToServices = new kony.ui.Label({
                "centerY": "47%",
                "id": "lblApplyToServices",
                "isVisible": true,
                "left": "20dp",
                "skin": "slLabel0d20174dce8ea42",
                "text": "Apply to Other Business Services",
                "top": 0,
                "width": "80%",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxSelectOtherServices.add(flxCheckboxSelect2, lblApplyToServices);
            var flxNoBusinessTypeError = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "25dp",
                "id": "flxNoBusinessTypeError",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "20dp",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0dp",
                "width": "100%"
            }, {}, {});
            flxNoBusinessTypeError.setDefaultUnit(kony.flex.DP);
            var lblNoBusinessTypeError = new kony.ui.Label({
                "centerY": "50%",
                "height": "15dp",
                "id": "lblNoBusinessTypeError",
                "isVisible": true,
                "left": "15dp",
                "skin": "sknlblError",
                "text": "Please select the Service Definition to proceed",
                "top": "0dp",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var lblNoBusinessTypeErrorIcon = new kony.ui.Label({
                "centerY": "52%",
                "height": "15dp",
                "id": "lblNoBusinessTypeErrorIcon",
                "isVisible": true,
                "left": "0dp",
                "skin": "sknErrorIcon",
                "text": "",
                "width": "15dp",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxNoBusinessTypeError.add(lblNoBusinessTypeError, lblNoBusinessTypeErrorIcon);
            flxBusinessTypesContainer.add(flxBusinessTypes, flxSelectOtherServices, flxNoBusinessTypeError);
            var flxDescription = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                "clipBounds": true,
                "id": "flxDescription",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "10dp",
                "width": "100%",
                "zIndex": 1
            }, {}, {});
            flxDescription.setDefaultUnit(kony.flex.DP);
            var lblGroupDescription = new kony.ui.Label({
                "id": "lblGroupDescription",
                "isVisible": true,
                "left": "20px",
                "skin": "slLabel0d20174dce8ea42",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.permission.Description\")",
                "top": 0,
                "width": "30%",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var lblGroupDescriptionCount = new kony.ui.Label({
                "id": "lblGroupDescriptionCount",
                "isVisible": true,
                "right": "20px",
                "skin": "slLabel0d20174dce8ea42",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmGroups.lblGroupDescriptionCount\")",
                "top": "0px",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var txtGroupDescription = new kony.ui.TextArea2({
                "autoCapitalize": constants.TEXTAREA_AUTO_CAPITALIZE_NONE,
                "focusSkin": "skntxtAreaLato9ca9ba12Px",
                "height": "80dp",
                "id": "txtGroupDescription",
                "isVisible": true,
                "keyBoardStyle": constants.TEXTAREA_KEY_BOARD_STYLE_DEFAULT,
                "left": "20dp",
                "maxTextLength": 250,
                "numberOfVisibleLines": 3,
                "placeholder": "Enter Description",
                "right": "20dp",
                "skin": "skntxtAreaLato35475f14Px",
                "textInputMode": constants.TEXTAREA_INPUT_MODE_ANY,
                "top": "30dp",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_TOP_LEFT,
                "padding": [2, 2, 2, 2],
                "paddingInPixel": false
            }, {
                "autoCorrect": false
            });
            var flxNoGroupDescriptionError = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "25px",
                "id": "flxNoGroupDescriptionError",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "20dp",
                "isModalContainer": false,
                "right": "20dp",
                "skin": "slFbox",
                "top": "115dp"
            }, {}, {});
            flxNoGroupDescriptionError.setDefaultUnit(kony.flex.DP);
            var lblNoGroupDescriptionError = new kony.ui.Label({
                "centerY": "50%",
                "height": "15px",
                "id": "lblNoGroupDescriptionError",
                "isVisible": true,
                "left": "15px",
                "skin": "sknlblError",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmGroups.lblNoGroupDescriptionError\")",
                "top": "0px",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var lblNoGroupDescriptionErrorIcon = new kony.ui.Label({
                "centerY": "52%",
                "height": "15dp",
                "id": "lblNoGroupDescriptionErrorIcon",
                "isVisible": true,
                "left": "0dp",
                "skin": "sknErrorIcon",
                "text": "",
                "top": "0dp",
                "width": "15dp",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxNoGroupDescriptionError.add(lblNoGroupDescriptionError, lblNoGroupDescriptionErrorIcon);
            flxDescription.add(lblGroupDescription, lblGroupDescriptionCount, txtGroupDescription, flxNoGroupDescriptionError);
            var flxStatusAndAgreement = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "40px",
                "id": "flxStatusAndAgreement",
                "isVisible": true,
                "layoutType": kony.flex.FLOW_HORIZONTAL,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "25dp",
                "width": "100%"
            }, {}, {});
            flxStatusAndAgreement.setDefaultUnit(kony.flex.DP);
            var flxGroupStatus = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "25px",
                "id": "flxGroupStatus",
                "isVisible": true,
                "layoutType": kony.flex.FLOW_HORIZONTAL,
                "left": "20dp",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0dp",
                "width": "110px",
                "zIndex": 1
            }, {}, {});
            flxGroupStatus.setDefaultUnit(kony.flex.DP);
            var lblSwitchActivate = new kony.ui.Label({
                "centerY": "48%",
                "id": "lblSwitchActivate",
                "isVisible": true,
                "left": "0px",
                "skin": "sknLbl485C75LatoRegular13Px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.permission.Active\")",
                "top": 0,
                "width": "55px",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var switchStatus = new kony.ui.Switch({
                "centerY": "50%",
                "height": "25dp",
                "id": "switchStatus",
                "isVisible": true,
                "left": "0dp",
                "leftSideText": "ON",
                "rightSideText": "OFF",
                "selectedIndex": 0,
                "skin": "sknSwitchActiveDisabled",
                "width": "36dp",
                "zIndex": 1
            }, {
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxGroupStatus.add(lblSwitchActivate, switchStatus);
            var flxEAgreement = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "25px",
                "id": "flxEAgreement",
                "isVisible": false,
                "layoutType": kony.flex.FLOW_HORIZONTAL,
                "left": "20dp",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0dp",
                "width": "200dp",
                "zIndex": 1
            }, {}, {});
            flxEAgreement.setDefaultUnit(kony.flex.DP);
            var lblEAgreementStatus = new kony.ui.Label({
                "centerY": "50%",
                "id": "lblEAgreementStatus",
                "isVisible": true,
                "left": "0px",
                "skin": "sknlblLato546e7a12px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmGroups.EAgreement_Required\")",
                "top": 0,
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var switchEAgreement = new kony.ui.Switch({
                "centerY": "50%",
                "height": "25dp",
                "id": "switchEAgreement",
                "isVisible": true,
                "left": "20dp",
                "leftSideText": "ON",
                "rightSideText": "OFF",
                "selectedIndex": 0,
                "skin": "sknSwitchServiceManagement",
                "width": "36dp",
                "zIndex": 1
            }, {
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxEAgreement.add(lblEAgreementStatus, switchEAgreement);
            flxStatusAndAgreement.add(flxGroupStatus, flxEAgreement);
            flxAddGroupDetails.add(flxRow1, flxValidity, flxBusinessTypesContainer, flxDescription, flxStatusAndAgreement);
            var flxSeparator11 = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "bottom": 81,
                "clipBounds": true,
                "height": "1dp",
                "id": "flxSeparator11",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "sknflxd6dbe7",
                "width": "100%",
                "zIndex": 1
            }, {}, {});
            flxSeparator11.setDefaultUnit(kony.flex.DP);
            flxSeparator11.add();
            var flxGroupDetailsButtons = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "bottom": "0dp",
                "clipBounds": true,
                "height": "80px",
                "id": "flxGroupDetailsButtons",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0%",
                "isModalContainer": false,
                "skin": "slFbox",
                "width": "100%",
                "zIndex": 1
            }, {}, {});
            flxGroupDetailsButtons.setDefaultUnit(kony.flex.DP);
            var btnAddGroupCancel = new kony.ui.Button({
                "centerY": "50%",
                "focusSkin": "sknBtnLatoRegulara5abc413pxKA",
                "height": "40dp",
                "id": "btnAddGroupCancel",
                "isVisible": true,
                "left": "20px",
                "right": "29.50%",
                "skin": "sknbtnf7f7faLatoRegular4f555dBorder1px8b96a5",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.permission.CANCEL\")",
                "top": "0%",
                "width": "100px",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_CENTER,
                "displayText": true,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {
                "hoverSkin": "sknbtnffffffLatoRegular4f555dBorder1px485c75"
            });
            var flxAddGroupRightButtons = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "centerY": "50%",
                "clipBounds": true,
                "height": "100%",
                "id": "flxAddGroupRightButtons",
                "isVisible": true,
                "layoutType": kony.flex.FLOW_HORIZONTAL,
                "reverseLayoutDirection": true,
                "isModalContainer": false,
                "right": "0px",
                "skin": "slFbox",
                "width": "280px",
                "zIndex": 1
            }, {}, {});
            flxAddGroupRightButtons.setDefaultUnit(kony.flex.DP);
            var btnAddGroupSave = new kony.ui.Button({
                "centerY": "50%",
                "focusSkin": "sknBtn003E75LatoRegular13pxFFFFFFRad20px",
                "height": "40dp",
                "id": "btnAddGroupSave",
                "isVisible": false,
                "right": "20px",
                "skin": "sknBtn003E75LatoRegular13pxFFFFFFRad20px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmAdManagement.CreateRoleCAPS\")",
                "top": "0%",
                "width": "125px",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_CENTER,
                "displayText": true,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {
                "hoverSkin": "sknBtn005198LatoRegular13pxFFFFFFRad20px"
            });
            var btnAddGroupNext = new kony.ui.Button({
                "centerY": "50%",
                "focusSkin": "sknBtn4A77A0LatoRegular13pxFFFFFFRad20px",
                "height": "40dp",
                "id": "btnAddGroupNext",
                "isVisible": true,
                "right": "20dp",
                "skin": "sknBtn003E75LatoRegular13pxFFFFFFRad20px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.permission.NEXT\")",
                "width": "100dp",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_CENTER,
                "displayText": true,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {
                "hoverSkin": "sknBtn2D5982LatoRegular13pxFFFFFFRad20px"
            });
            flxAddGroupRightButtons.add(btnAddGroupSave, btnAddGroupNext);
            flxGroupDetailsButtons.add(btnAddGroupCancel, flxAddGroupRightButtons);
            var BusinessTypesToolTip = new com.adminConsole.Customers.ToolTip({
                "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                "id": "BusinessTypesToolTip",
                "isVisible": false,
                "layoutType": kony.flex.FREE_FORM,
                "left": "94dp",
                "masterType": constants.MASTER_TYPE_DEFAULT,
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "490dp",
                "width": "181dp",
                "overrides": {
                    "ToolTip": {
                        "isVisible": false,
                        "left": "94dp",
                        "top": "490dp",
                        "width": "181dp"
                    },
                    "flxToolTipMessage": {
                        "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                        "centerX": "viz.val_cleared",
                        "width": kony.flex.USE_PREFFERED_SIZE
                    },
                    "lblDownArrow": {
                        "centerX": "viz.val_cleared",
                        "left": "10px",
                        "top": "55dp"
                    },
                    "lblNoConcentToolTip": {
                        "bottom": "5px",
                        "height": kony.flex.USE_PREFFERED_SIZE,
                        "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmAdManagement.smsEmailPushNotification\")",
                        "left": "10px",
                        "right": "10px",
                        "top": "3px",
                        "width": kony.flex.USE_PREFFERED_SIZE
                    },
                    "lblarrow": {
                        "centerX": "viz.val_cleared",
                        "left": "viz.val_cleared",
                        "right": "10px"
                    }
                }
            }, {
                "overrides": {}
            }, {
                "overrides": {}
            });
            flxGroupDetails.add(flxAddGroupDetails, flxSeparator11, flxGroupDetailsButtons, BusinessTypesToolTip);
            var flxSeparator = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "100%",
                "id": "flxSeparator",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "231dp",
                "isModalContainer": false,
                "skin": "sknflxd6dbe7",
                "top": "0dp",
                "width": "1dp",
                "zIndex": 10
            }, {}, {});
            flxSeparator.setDefaultUnit(kony.flex.DP);
            flxSeparator.add();
            var flxSelectFeatures = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "bottom": "0dp",
                "clipBounds": true,
                "id": "flxSelectFeatures",
                "isVisible": false,
                "layoutType": kony.flex.FREE_FORM,
                "left": "230dp",
                "isModalContainer": false,
                "right": "0dp",
                "skin": "slFbox",
                "top": "0dp",
                "zIndex": 1
            }, {}, {});
            flxSelectFeatures.setDefaultUnit(kony.flex.DP);
            var flxAddFeatures = new kony.ui.FlexScrollContainer({
                "allowHorizontalBounce": false,
                "allowVerticalBounce": true,
                "bottom": "81dp",
                "bounces": true,
                "clipBounds": true,
                "enableScrolling": true,
                "horizontalScrollIndicator": true,
                "id": "flxAddFeatures",
                "isVisible": true,
                "layoutType": kony.flex.FLOW_VERTICAL,
                "left": "0dp",
                "pagingEnabled": false,
                "right": 0,
                "scrollDirection": kony.flex.SCROLL_VERTICAL,
                "skin": "slFSbox",
                "top": "0dp",
                "verticalScrollIndicator": true,
                "zIndex": 1
            }, {}, {});
            flxAddFeatures.setDefaultUnit(kony.flex.DP);
            var flxHeader = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "25dp",
                "id": "flxHeader",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "20dp",
                "isModalContainer": false,
                "right": "20dp",
                "skin": "slFbox",
                "top": "10dp",
                "width": "95%",
                "zIndex": 1
            }, {}, {});
            flxHeader.setDefaultUnit(kony.flex.DP);
            var lblHeader = new kony.ui.Label({
                "id": "lblHeader",
                "isVisible": true,
                "left": "0px",
                "skin": "sknLbl192B4518pxLatoReg",
                "text": "Features",
                "top": 0,
                "width": "30%",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxHeader.add(lblHeader);
            var imgIcon = new kony.ui.Image2({
                "centerX": "50%",
                "centerY": "30%",
                "height": "104dp",
                "id": "imgIcon",
                "isVisible": true,
                "left": "0dp",
                "skin": "slImage",
                "src": "selectacheckbox.png",
                "top": "0dp",
                "width": "131dp",
                "zIndex": 1
            }, {
                "imageScaleMode": constants.IMAGE_SCALE_MODE_MAINTAIN_ASPECT_RATIO,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var flxSelect = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "centerX": "50%",
                "clipBounds": true,
                "height": "150dp",
                "id": "flxSelect",
                "isVisible": true,
                "layoutType": kony.flex.FLOW_VERTICAL,
                "left": "44dp",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "40dp",
                "width": "100%",
                "zIndex": 1
            }, {}, {});
            flxSelect.setDefaultUnit(kony.flex.DP);
            var lblSelect = new kony.ui.Label({
                "centerX": "50%",
                "id": "lblSelect",
                "isVisible": true,
                "left": "0dp",
                "skin": "sknLbl485C75LatoRegular13Px",
                "text": "Select Features and assign Actions",
                "top": "0dp",
                "width": "600dp",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_CENTER,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var flxFeatureButtons = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "50dp",
                "id": "flxFeatureButtons",
                "isVisible": true,
                "layoutType": kony.flex.FLOW_HORIZONTAL,
                "left": "104dp",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "30dp",
                "width": "88.24%",
                "zIndex": 1
            }, {}, {});
            flxFeatureButtons.setDefaultUnit(kony.flex.DP);
            var btnAddpermissions = new kony.ui.Button({
                "centerX": "30%",
                "centerY": "50%",
                "height": "22dp",
                "id": "btnAddpermissions",
                "isVisible": true,
                "left": "0dp",
                "skin": "sknbtnbrdr485C75rad15px",
                "text": "Add Permissions",
                "top": "0dp",
                "width": "150dp",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_CENTER,
                "displayText": true,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var btnAdvancedSelection = new kony.ui.Button({
                "centerY": "50%",
                "height": "22dp",
                "id": "btnAdvancedSelection",
                "isVisible": true,
                "left": "20dp",
                "skin": "sknbtnbrdr485C75rad15px",
                "text": "Advanced Selection",
                "top": "0dp",
                "width": "150dp",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_CENTER,
                "displayText": true,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxFeatureButtons.add(btnAddpermissions, btnAdvancedSelection);
            flxSelect.add(lblSelect, flxFeatureButtons);
            flxAddFeatures.add(flxHeader, imgIcon, flxSelect);
            var flxButtons1 = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "bottom": "0dp",
                "clipBounds": true,
                "height": "80px",
                "id": "flxButtons1",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0%",
                "isModalContainer": false,
                "skin": "slFbox",
                "width": "100%",
                "zIndex": 1
            }, {}, {});
            flxButtons1.setDefaultUnit(kony.flex.DP);
            var btnCancel1 = new kony.ui.Button({
                "centerY": "50%",
                "focusSkin": "sknBtnLatoRegulara5abc413pxKA",
                "height": "40dp",
                "id": "btnCancel1",
                "isVisible": true,
                "left": "20px",
                "right": "29.50%",
                "skin": "sknbtnf7f7faLatoRegular4f555dBorder1px8b96a5",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.permission.CANCEL\")",
                "top": "0%",
                "width": "100px",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_CENTER,
                "displayText": true,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {
                "hoverSkin": "sknbtnffffffLatoRegular4f555dBorder1px485c75"
            });
            var flxAddGroupRightButtons1 = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "centerY": "50%",
                "clipBounds": true,
                "height": "100%",
                "id": "flxAddGroupRightButtons1",
                "isVisible": true,
                "layoutType": kony.flex.FLOW_HORIZONTAL,
                "reverseLayoutDirection": true,
                "isModalContainer": false,
                "right": "0px",
                "skin": "slFbox",
                "width": "280px",
                "zIndex": 1
            }, {}, {});
            flxAddGroupRightButtons1.setDefaultUnit(kony.flex.DP);
            var btnAddGroupSave1 = new kony.ui.Button({
                "centerY": "50%",
                "focusSkin": "sknBtnDisable7B7B7Brad20px",
                "height": "40dp",
                "id": "btnAddGroupSave1",
                "isVisible": true,
                "right": "20px",
                "skin": "sknBtnDisable7B7B7Brad20px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmAdManagement.CreateRoleCAPS\")",
                "top": "0%",
                "width": "125px",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_CENTER,
                "displayText": true,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {
                "hoverSkin": "sknBtnDisable7B7B7Brad20px"
            });
            var btnAddGroupNext1 = new kony.ui.Button({
                "centerY": "50%",
                "focusSkin": "sknBtnDisable7B7B7Brad20px",
                "height": "40dp",
                "id": "btnAddGroupNext1",
                "isVisible": true,
                "right": "20dp",
                "skin": "sknBtnDisable7B7B7Brad20px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.permission.NEXT\")",
                "width": "100dp",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_CENTER,
                "displayText": true,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {
                "hoverSkin": "sknBtnDisable7B7B7Brad20px"
            });
            flxAddGroupRightButtons1.add(btnAddGroupSave1, btnAddGroupNext1);
            flxButtons1.add(btnCancel1, flxAddGroupRightButtons1);
            flxSelectFeatures.add(flxAddFeatures, flxButtons1);
            var flxFeatureComponent = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "bottom": "0dp",
                "clipBounds": true,
                "id": "flxFeatureComponent",
                "isVisible": false,
                "layoutType": kony.flex.FREE_FORM,
                "left": "230dp",
                "isModalContainer": false,
                "right": "0dp",
                "skin": "slFbox",
                "top": "0dp",
                "zIndex": 10
            }, {}, {});
            flxFeatureComponent.setDefaultUnit(kony.flex.DP);
            var featureAndActions = new com.adminConsole.serviceDefinition.featureAndActions({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "height": "100%",
                "id": "featureAndActions",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "masterType": constants.MASTER_TYPE_DEFAULT,
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0dp",
                "width": "100%",
                "overrides": {
                    "btnCancel": {
                        "right": "165dp"
                    },
                    "btnSave": {
                        "right": "20px"
                    },
                    "commonButtons.btnCancel": {
                        "left": "20dp"
                    },
                    "commonButtons.btnNext": {
                        "left": "15px"
                    },
                    "commonButtons.btnSave": {
                        "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmAdManagement.CreateRoleCAPS\")",
                        "right": "20px",
                        "width": "125px"
                    },
                    "commonButtons.flxRightButtons": {
                        "right": "0dp",
                        "width": "280px"
                    },
                    "featureAndActions": {
                        "bottom": "viz.val_cleared",
                        "centerX": "viz.val_cleared",
                        "centerY": "viz.val_cleared",
                        "height": "100%",
                        "isVisible": true,
                        "left": "0dp",
                        "maxHeight": "viz.val_cleared",
                        "maxWidth": "viz.val_cleared",
                        "minHeight": "viz.val_cleared",
                        "minWidth": "viz.val_cleared",
                        "right": "viz.val_cleared",
                        "top": "0dp",
                        "width": "100%"
                    },
                    "flxActions": {
                        "width": "40%"
                    },
                    "flxAssignActions": {
                        "isVisible": false
                    },
                    "flxDropDown": {
                        "isVisible": false
                    },
                    "flxFeatures": {
                        "isVisible": true
                    },
                    "flxFeaturesSegment": {
                        "height": "viz.val_cleared"
                    },
                    "flxReview": {
                        "isVisible": false
                    },
                    "flxRightButtons": {
                        "width": "290px"
                    },
                    "imgCheckBox": {
                        "src": "checkboxnormal.png"
                    },
                    "imgFeatureCheckbox": {
                        "src": "checkboxnormal.png"
                    },
                    "lblHeading": {
                        "text": "Features"
                    },
                    "lblSelect": {
                        "text": "Select All"
                    },
                    "segDropdown": {
                        "top": "6dp"
                    },
                    "segReview": {
                        "height": "310dp",
                        "left": "5dp"
                    },
                    "subHeader.flxMenu": {
                        "isVisible": false
                    }
                }
            }, {
                "overrides": {}
            }, {
                "overrides": {}
            });
            flxFeatureComponent.add(featureAndActions);
            var flxAdvancedSelectionComponent = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "bottom": "0dp",
                "clipBounds": true,
                "id": "flxAdvancedSelectionComponent",
                "isVisible": false,
                "layoutType": kony.flex.FLOW_VERTICAL,
                "left": "230dp",
                "isModalContainer": false,
                "right": "0dp",
                "skin": "slFbox",
                "top": "0dp",
                "zIndex": 1
            }, {}, {});
            flxAdvancedSelectionComponent.setDefaultUnit(kony.flex.DP);
            var flxAdvancedSelection = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "bottom": "0dp",
                "clipBounds": true,
                "height": "100%",
                "id": "flxAdvancedSelection",
                "isVisible": true,
                "layoutType": kony.flex.FLOW_VERTICAL,
                "left": "0dp",
                "isModalContainer": false,
                "right": "0dp",
                "skin": "slFbox",
                "top": "0dp",
                "zIndex": 1
            }, {}, {});
            flxAdvancedSelection.setDefaultUnit(kony.flex.DP);
            var flxAdvanceSelectionHeader = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "bottom": 0,
                "clipBounds": true,
                "height": "12%",
                "id": "flxAdvanceSelectionHeader",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "isModalContainer": false,
                "right": 0,
                "skin": "slFbox",
                "top": "0dp",
                "zIndex": 1
            }, {}, {});
            flxAdvanceSelectionHeader.setDefaultUnit(kony.flex.DP);
            var lblHeading = new kony.ui.Label({
                "id": "lblHeading",
                "isVisible": true,
                "left": "20dp",
                "skin": "sknLbl192B4518pxLatoReg",
                "text": "Advanced Selection",
                "top": "20px",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var flxSearch = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                "clipBounds": true,
                "id": "flxSearch",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "isModalContainer": false,
                "right": "20dp",
                "skin": "CopyslFbox2",
                "top": "15px",
                "width": "350px",
                "zIndex": 1
            }, {}, {});
            flxSearch.setDefaultUnit(kony.flex.DP);
            var flxSearchContainer1 = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "35px",
                "id": "flxSearchContainer1",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "sknflxd5d9ddop100",
                "top": "2dp",
                "width": "350px",
                "zIndex": 1
            }, {}, {});
            flxSearchContainer1.setDefaultUnit(kony.flex.DP);
            var fontIconSearchImg1 = new kony.ui.Label({
                "centerY": "50%",
                "id": "fontIconSearchImg1",
                "isVisible": true,
                "left": "12px",
                "skin": "sknFontIconSearchImg20px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.userwidgetmodel.lblSearchIcon\")",
                "top": "0dp",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var tbxSearchBox1 = new kony.ui.TextBox2({
                "autoCapitalize": constants.TEXTBOX_AUTO_CAPITALIZE_NONE,
                "centerY": "50%",
                "height": "30px",
                "id": "tbxSearchBox1",
                "isVisible": true,
                "keyBoardStyle": constants.TEXTBOX_KEY_BOARD_STYLE_DEFAULT,
                "left": "30dp",
                "onTouchStart": controller.AS_TextField_cfab06859f0f43eca99495891ee290d5,
                "placeholder": "Search By Feature",
                "right": "35px",
                "secureTextEntry": false,
                "skin": "skntbxffffffNoBorderlato35475f14px",
                "textInputMode": constants.TEXTBOX_INPUT_MODE_ANY,
                "zIndex": 1
            }, {
                "containerHeightMode": constants.TEXTBOX_FONT_METRICS_DRIVEN_HEIGHT,
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [2, 0, 0, 0],
                "paddingInPixel": false
            }, {
                "autoCorrect": false
            });
            var flxClearSearchImage1 = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "100%",
                "id": "flxClearSearchImage1",
                "isVisible": false,
                "layoutType": kony.flex.FREE_FORM,
                "isModalContainer": false,
                "right": "0px",
                "skin": "sknCursor",
                "top": "0dp",
                "width": "35px",
                "zIndex": 1
            }, {}, {});
            flxClearSearchImage1.setDefaultUnit(kony.flex.DP);
            var fontIconCross = new kony.ui.Label({
                "centerY": "50%",
                "id": "fontIconCross",
                "isVisible": true,
                "left": "5px",
                "skin": "sknFontIconSearchCross16px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.userwidgetmodel.lblClearSearch\")",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxClearSearchImage1.add(fontIconCross);
            flxSearchContainer1.add(fontIconSearchImg1, tbxSearchBox1, flxClearSearchImage1);
            flxSearch.add(flxSearchContainer1);
            flxAdvanceSelectionHeader.add(lblHeading, flxSearch);
            var flxAdvanceSelectionContent = new kony.ui.FlexScrollContainer({
                "allowHorizontalBounce": false,
                "allowVerticalBounce": true,
                "bounces": true,
                "centerX": "50%",
                "clipBounds": true,
                "enableScrolling": true,
                "height": "73%",
                "horizontalScrollIndicator": true,
                "id": "flxAdvanceSelectionContent",
                "isVisible": true,
                "layoutType": kony.flex.FLOW_VERTICAL,
                "left": "0dp",
                "pagingEnabled": false,
                "right": 0,
                "scrollDirection": kony.flex.SCROLL_VERTICAL,
                "skin": "slFSbox",
                "top": "10dp",
                "verticalScrollIndicator": true,
                "width": "94%",
                "zIndex": 1
            }, {}, {});
            flxAdvanceSelectionContent.setDefaultUnit(kony.flex.DP);
            flxAdvanceSelectionContent.add();
            var flxCommonButtons = new kony.ui.FlexContainer({
                "bottom": "0dp",
                "clipBounds": true,
                "height": "14%",
                "id": "flxCommonButtons",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "slFbox",
                "width": "100%"
            }, {}, {});
            flxCommonButtons.setDefaultUnit(kony.flex.DP);
            var flxSeparator4 = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "1dp",
                "id": "flxSeparator4",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "sknflxD5D9DD",
                "top": "5dp",
                "width": "100%",
                "zIndex": 1
            }, {}, {});
            flxSeparator4.setDefaultUnit(kony.flex.DP);
            flxSeparator4.add();
            var flxRightButtons = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "centerY": "50%",
                "clipBounds": true,
                "height": "100%",
                "id": "flxRightButtons",
                "isVisible": true,
                "layoutType": kony.flex.FLOW_HORIZONTAL,
                "reverseLayoutDirection": true,
                "isModalContainer": false,
                "right": "0dp",
                "skin": "slFbox",
                "width": "300px",
                "zIndex": 1
            }, {}, {});
            flxRightButtons.setDefaultUnit(kony.flex.DP);
            var btnSave = new kony.ui.Button({
                "centerY": "50%",
                "focusSkin": "sknBtn003E75LatoRegular13pxFFFFFFRad20px",
                "height": "40dp",
                "id": "btnSave",
                "isVisible": true,
                "right": "30dp",
                "skin": "sknBtn003E75LatoRegular13pxFFFFFFRad20px",
                "text": "SAVE",
                "top": "0%",
                "width": "125px",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_CENTER,
                "displayText": true,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var btnCancelAdvSel = new kony.ui.Button({
                "centerY": "50%",
                "focusSkin": "sknBtnLatoRegular8b96a513pxKAhover",
                "height": "40dp",
                "id": "btnCancelAdvSel",
                "isVisible": true,
                "right": "30dp",
                "skin": "sknbtnf7f7faLatoRegular4f555dBorder1px8b96a5",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.permission.CANCEL\")",
                "top": "0%",
                "width": "100px",
                "zIndex": 2
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_CENTER,
                "displayText": true,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxRightButtons.add(btnSave, btnCancelAdvSel);
            flxCommonButtons.add(flxSeparator4, flxRightButtons);
            flxAdvancedSelection.add(flxAdvanceSelectionHeader, flxAdvanceSelectionContent, flxCommonButtons);
            flxAdvancedSelectionComponent.add(flxAdvancedSelection);
            var flxAssignLimitsComponent = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "bottom": "0dp",
                "clipBounds": true,
                "id": "flxAssignLimitsComponent",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "230dp",
                "isModalContainer": false,
                "right": "0dp",
                "skin": "slFbox",
                "top": "0dp",
                "zIndex": 10
            }, {}, {});
            flxAssignLimitsComponent.setDefaultUnit(kony.flex.DP);
            var assignLimitsContainer = new com.adminConsole.common.assignLimitsContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "height": "100%",
                "id": "assignLimitsContainer",
                "isVisible": false,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "masterType": constants.MASTER_TYPE_DEFAULT,
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0dp",
                "width": "100%",
                "overrides": {
                    "assignLimitsContainer": {
                        "isVisible": false
                    },
                    "btnOption1": {
                        "bottom": "0dp",
                        "right": "25dp"
                    },
                    "flxBtnContainer": {
                        "width": "100dp"
                    },
                    "flxReset": {
                        "bottom": "viz.val_cleared",
                        "height": "45dp"
                    },
                    "flxScrollAssignLimits": {
                        "top": "45dp"
                    }
                }
            }, {
                "overrides": {}
            }, {
                "overrides": {}
            });
            var flxReset = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "bottom": "610dp",
                "clipBounds": true,
                "height": "45dp",
                "id": "flxReset",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0dp",
                "width": "100%"
            }, {}, {});
            flxReset.setDefaultUnit(kony.flex.DP);
            var flxBtnContainer = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                "centerY": "50%",
                "clipBounds": true,
                "id": "flxBtnContainer",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "isModalContainer": false,
                "right": "0%",
                "skin": "slFbox",
                "width": "150dp"
            }, {}, {});
            flxBtnContainer.setDefaultUnit(kony.flex.DP);
            var btnOption1 = new kony.ui.Button({
                "bottom": "0dp",
                "id": "btnOption1",
                "isVisible": true,
                "right": "25dp",
                "skin": "sknBtnLato117eb013Px",
                "text": "Reset To Default",
                "top": "0dp",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_TOP_LEFT,
                "displayText": true,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {
                "hoverSkin": "Btn000000font14px"
            });
            flxBtnContainer.add(btnOption1);
            flxReset.add(flxBtnContainer);
            var flxScrollAssignLimits = new kony.ui.FlexScrollContainer({
                "allowHorizontalBounce": false,
                "allowVerticalBounce": true,
                "bottom": "81dp",
                "bounces": true,
                "clipBounds": true,
                "enableScrolling": true,
                "horizontalScrollIndicator": true,
                "id": "flxScrollAssignLimits",
                "isVisible": true,
                "layoutType": kony.flex.FLOW_VERTICAL,
                "left": "0dp",
                "pagingEnabled": false,
                "scrollDirection": kony.flex.SCROLL_VERTICAL,
                "skin": "slFSbox",
                "top": "45dp",
                "verticalScrollIndicator": true,
                "width": kony.flex.USE_PREFFERED_SIZE
            }, {}, {});
            flxScrollAssignLimits.setDefaultUnit(kony.flex.DP);
            flxScrollAssignLimits.add();
            var flxSeparator2 = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "bottom": "80dp",
                "clipBounds": true,
                "height": "1dp",
                "id": "flxSeparator2",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": 0,
                "isModalContainer": false,
                "skin": "sknflxd6dbe7",
                "width": "100%",
                "zIndex": 1
            }, {}, {});
            flxSeparator2.setDefaultUnit(kony.flex.DP);
            flxSeparator2.add();
            var flxGroupDetailsButtons1 = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "bottom": "0dp",
                "clipBounds": true,
                "height": "80dp",
                "id": "flxGroupDetailsButtons1",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0.00%",
                "isModalContainer": false,
                "skin": "slFbox",
                "width": "100%",
                "zIndex": 1
            }, {}, {});
            flxGroupDetailsButtons1.setDefaultUnit(kony.flex.DP);
            var btnCancel = new kony.ui.Button({
                "centerY": "50%",
                "focusSkin": "sknBtnLatoRegulara5abc413pxKA",
                "height": "40dp",
                "id": "btnCancel",
                "isVisible": true,
                "left": "20px",
                "right": "29.50%",
                "skin": "sknbtnf7f7faLatoRegular4f555dBorder1px8b96a5",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.permission.CANCEL\")",
                "top": "0%",
                "width": "100px",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_CENTER,
                "displayText": true,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {
                "hoverSkin": "sknbtnffffffLatoRegular4f555dBorder1px485c75"
            });
            var flxRightButton = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "centerY": "50%",
                "clipBounds": true,
                "height": "100%",
                "id": "flxRightButton",
                "isVisible": true,
                "layoutType": kony.flex.FLOW_HORIZONTAL,
                "reverseLayoutDirection": true,
                "isModalContainer": false,
                "right": "0px",
                "skin": "slFbox",
                "width": "280px",
                "zIndex": 1
            }, {}, {});
            flxRightButton.setDefaultUnit(kony.flex.DP);
            var btnCreate = new kony.ui.Button({
                "centerY": "50%",
                "focusSkin": "sknBtn003E75LatoRegular13pxFFFFFFRad20px",
                "height": "40dp",
                "id": "btnCreate",
                "isVisible": true,
                "right": "20dp",
                "skin": "sknBtn003E75LatoRegular13pxFFFFFFRad20px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmGroupsController.Updaterole_UC\")",
                "top": "0%",
                "width": "125dp",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_CENTER,
                "displayText": true,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {
                "hoverSkin": "sknBtn005198LatoRegular13pxFFFFFFRad20px"
            });
            flxRightButton.add(btnCreate);
            flxGroupDetailsButtons1.add(btnCancel, flxRightButton);
            var flxNoLimitsForFeatureSelected = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "centerY": "50%",
                "clipBounds": true,
                "height": "140dp",
                "id": "flxNoLimitsForFeatureSelected",
                "isVisible": false,
                "layoutType": kony.flex.FLOW_VERTICAL,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0dp",
                "width": "100%"
            }, {}, {});
            flxNoLimitsForFeatureSelected.setDefaultUnit(kony.flex.DP);
            var lblNoLimits = new kony.ui.Label({
                "centerX": "50%",
                "id": "lblNoLimits",
                "isVisible": true,
                "skin": "sknLbl485C75LatoRegular13Px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmGroups.NoLimits\")",
                "top": "0dp",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxNoLimitsForFeatureSelected.add(lblNoLimits);
            flxAssignLimitsComponent.add(assignLimitsContainer, flxReset, flxScrollAssignLimits, flxSeparator2, flxGroupDetailsButtons1, flxNoLimitsForFeatureSelected);
            flxAddGroups.add(flxVerticalTabs, flxGroupDetails, flxSeparator, flxSelectFeatures, flxFeatureComponent, flxAdvancedSelectionComponent, flxAssignLimitsComponent);
            var flxGroupList = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "bottom": "0px",
                "clipBounds": true,
                "id": "flxGroupList",
                "isVisible": false,
                "layoutType": kony.flex.FREE_FORM,
                "left": "35px",
                "isModalContainer": false,
                "right": "35px",
                "skin": "slFbox",
                "top": "105px",
                "zIndex": 1
            }, {}, {});
            flxGroupList.setDefaultUnit(kony.flex.DP);
            var flxGroupListMain = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "bottom": "0dp",
                "clipBounds": true,
                "id": "flxGroupListMain",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0dp",
                "width": "100%",
                "zIndex": 1
            }, {}, {});
            flxGroupListMain.setDefaultUnit(kony.flex.DP);
            var flxGroupSearch = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "60dp",
                "id": "flxGroupSearch",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0dp",
                "width": "100%",
                "zIndex": 1
            }, {}, {});
            flxGroupSearch.setDefaultUnit(kony.flex.DP);
            var subHeader = new com.adminConsole.header.subHeader({
                "centerY": "50%",
                "height": "48dp",
                "id": "subHeader",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "masterType": constants.MASTER_TYPE_DEFAULT,
                "isModalContainer": false,
                "skin": "CopyslFbox2",
                "top": "0dp",
                "width": "100%",
                "overrides": {
                    "flxMenu": {
                        "isVisible": false,
                        "left": "0dp"
                    },
                    "flxSearch": {
                        "right": 0
                    },
                    "subHeader": {
                        "centerY": "50%"
                    },
                    "tbxSearchBox": {
                        "i18n_placeholder": "kony.i18n.getLocalizedString(\"i18n.frmGroups.Search_by_Group_Name\")"
                    }
                }
            }, {
                "overrides": {}
            }, {
                "overrides": {}
            });
            flxGroupSearch.add(subHeader);
            var flxSegGroups = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "bottom": "0dp",
                "clipBounds": true,
                "id": "flxSegGroups",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "sknflxffffffOp100BorderE1E5Ed",
                "top": "60dp",
                "width": "100%",
                "zIndex": 1
            }, {}, {});
            flxSegGroups.setDefaultUnit(kony.flex.DP);
            var flxGroupHeader = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "60px",
                "id": "flxGroupHeader",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "20px",
                "isModalContainer": false,
                "right": "20px",
                "skin": "sknflxffffffop100",
                "top": "0"
            }, {}, {});
            flxGroupHeader.setDefaultUnit(kony.flex.DP);
            var flxGroupName = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "centerY": "50%",
                "clipBounds": true,
                "height": "100%",
                "id": "flxGroupName",
                "isVisible": true,
                "layoutType": kony.flex.FLOW_HORIZONTAL,
                "left": "35px",
                "isModalContainer": false,
                "onClick": controller.AS_FlexContainer_aa9ff028c98444408c909813010b2efa,
                "skin": "slFbox",
                "top": 0,
                "width": "18.50%",
                "zIndex": 1
            }, {}, {});
            flxGroupName.setDefaultUnit(kony.flex.DP);
            var lblGroupName = new kony.ui.Label({
                "centerY": "50%",
                "id": "lblGroupName",
                "isVisible": true,
                "left": "0px",
                "skin": "sknlblLato696c7312px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmAdManagement.RoleNameCAPS\")",
                "top": 0,
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {
                "hoverSkin": "sknlblLato00000011px"
            });
            var fontIconSortName = new kony.ui.Label({
                "centerY": "50%",
                "id": "fontIconSortName",
                "isVisible": true,
                "left": "3dp",
                "skin": "sknIcon15px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmAlertsManagement.lblSortName\")",
                "top": "0dp",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxGroupName.add(lblGroupName, fontIconSortName);
            var flxGroupType = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "centerY": "50%",
                "clipBounds": true,
                "height": "100%",
                "id": "flxGroupType",
                "isVisible": true,
                "layoutType": kony.flex.FLOW_HORIZONTAL,
                "left": "19.50%",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0dp",
                "width": "12%",
                "zIndex": 1
            }, {}, {});
            flxGroupType.setDefaultUnit(kony.flex.DP);
            var lblHeaderGroupType = new kony.ui.Label({
                "centerY": "50%",
                "id": "lblHeaderGroupType",
                "isVisible": true,
                "left": "0px",
                "skin": "sknlblLato696c7312px",
                "text": "ROLE TYPE",
                "top": 0,
                "width": "75px",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var fontIconFilterGrpType = new kony.ui.Label({
                "centerY": "50%",
                "id": "fontIconFilterGrpType",
                "isVisible": true,
                "left": "0dp",
                "skin": "sknIcon15px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmAlertsManagement.lblIconChannels\")",
                "top": "0dp",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {
                "hoverSkin": "sknlblCursorFont"
            });
            flxGroupType.add(lblHeaderGroupType, fontIconFilterGrpType);
            var flxGroupBusinessTypes = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "centerY": "50%",
                "clipBounds": true,
                "height": "100%",
                "id": "flxGroupBusinessTypes",
                "isVisible": true,
                "layoutType": kony.flex.FLOW_HORIZONTAL,
                "left": "35.50%",
                "isModalContainer": false,
                "onClick": controller.AS_FlexContainer_d0c82a1545084c569df06fa1ca405fa6,
                "skin": "slFbox",
                "top": "0dp",
                "width": "30%",
                "zIndex": 1
            }, {}, {});
            flxGroupBusinessTypes.setDefaultUnit(kony.flex.DP);
            var lblGroupBusinessType = new kony.ui.Label({
                "centerY": "50%",
                "id": "lblGroupBusinessType",
                "isVisible": true,
                "left": "0px",
                "skin": "sknlblLato696c7312px",
                "text": "SERVICE",
                "top": 0,
                "width": "60px",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {
                "hoverSkin": "sknlblLato00000011px"
            });
            var fontIconFilterBusinessTypes = new kony.ui.Label({
                "centerY": "50%",
                "id": "fontIconFilterBusinessTypes",
                "isVisible": true,
                "left": "0dp",
                "skin": "sknIcon15px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmAlertsManagement.lblIconChannels\")",
                "top": "0dp",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {
                "hoverSkin": "sknlblCursorFont"
            });
            flxGroupBusinessTypes.add(lblGroupBusinessType, fontIconFilterBusinessTypes);
            var flxGroupCustomers = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "centerY": "50%",
                "clipBounds": true,
                "height": "100%",
                "id": "flxGroupCustomers",
                "isVisible": true,
                "layoutType": kony.flex.FLOW_HORIZONTAL,
                "left": "70%",
                "isModalContainer": false,
                "onClick": controller.AS_FlexContainer_d0c82a1545084c569df06fa1ca405fa6,
                "skin": "slFbox",
                "top": "0dp",
                "width": "12%",
                "zIndex": 1
            }, {}, {});
            flxGroupCustomers.setDefaultUnit(kony.flex.DP);
            var lblGroupCustomerss = new kony.ui.Label({
                "centerY": "50%",
                "id": "lblGroupCustomerss",
                "isVisible": true,
                "left": "0px",
                "skin": "sknlblLato696c7312px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.permission.USERS\")",
                "top": 0,
                "width": "50px",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {
                "hoverSkin": "sknlblLato00000011px"
            });
            var fontIconSortCustomers = new kony.ui.Label({
                "centerY": "50%",
                "id": "fontIconSortCustomers",
                "isVisible": true,
                "left": "0dp",
                "skin": "sknIcon15px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmAlertsManagement.lblSortName\")",
                "top": "0dp",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxGroupCustomers.add(lblGroupCustomerss, fontIconSortCustomers);
            var flxGroupHeaderStatus = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "centerY": "50%",
                "clipBounds": true,
                "height": "100%",
                "id": "flxGroupHeaderStatus",
                "isVisible": true,
                "layoutType": kony.flex.FLOW_HORIZONTAL,
                "isModalContainer": false,
                "onClick": controller.AS_FlexContainer_d3a97a0f3de34380a91ef7e725e093be,
                "right": "1.50%",
                "skin": "slFbox",
                "top": "0dp",
                "width": "13.80%",
                "zIndex": 1
            }, {}, {});
            flxGroupHeaderStatus.setDefaultUnit(kony.flex.DP);
            var lblGroupStatus = new kony.ui.Label({
                "centerY": "50%",
                "id": "lblGroupStatus",
                "isVisible": true,
                "left": "0px",
                "skin": "sknlblLato696c7312px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.permission.STATUS\")",
                "top": 0,
                "width": "46px",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {
                "hoverSkin": "sknlblLato00000011px"
            });
            var fontIconGroupStatusFilter = new kony.ui.Label({
                "centerY": "50%",
                "id": "fontIconGroupStatusFilter",
                "isVisible": true,
                "left": "2.50%",
                "skin": "sknIcon15px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmAlertsManagement.lblIconChannels\")",
                "top": "0dp",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {
                "hoverSkin": "sknlblCursorFont"
            });
            flxGroupHeaderStatus.add(lblGroupStatus, fontIconGroupStatusFilter);
            flxGroupHeader.add(flxGroupName, flxGroupType, flxGroupBusinessTypes, flxGroupCustomers, flxGroupHeaderStatus);
            var flxGroupSeparator = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "1dp",
                "id": "flxGroupSeparator",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "20dp",
                "isModalContainer": false,
                "right": "20dp",
                "skin": "sknTableHeaderLine",
                "top": "60dp",
                "zIndex": 1
            }, {}, {});
            flxGroupSeparator.setDefaultUnit(kony.flex.DP);
            flxGroupSeparator.add();
            var flxGroupListContainer = new kony.ui.FlexContainer({
                "bottom": "10px",
                "clipBounds": true,
                "height": "676dp",
                "id": "flxGroupListContainer",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "61dp",
                "width": "100%",
                "zIndex": 1
            }, {}, {});
            flxGroupListContainer.setDefaultUnit(kony.flex.DP);
            var listingSegmentClient = new com.adminConsole.common.listingSegmentClient({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "height": "100%",
                "id": "listingSegmentClient",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "masterType": constants.MASTER_TYPE_DEFAULT,
                "isModalContainer": false,
                "right": "0dp",
                "skin": "slFbox",
                "top": "0dp",
                "overrides": {
                    "contextualMenu.btnLink1": {
                        "bottom": "10dp",
                        "isVisible": false,
                        "top": "0dp"
                    },
                    "contextualMenu.btnLink2": {
                        "bottom": "10dp",
                        "isVisible": false,
                        "top": "0dp"
                    },
                    "contextualMenu.contextualMenu1Inner": {
                        "layoutType": kony.flex.FREE_FORM
                    },
                    "contextualMenu.flxOption1": {
                        "isVisible": false,
                        "top": "0dp"
                    },
                    "contextualMenu.flxOption2": {
                        "height": "30dp",
                        "top": "15dp"
                    },
                    "contextualMenu.flxOption3": {
                        "bottom": "10dp",
                        "height": "30dp",
                        "isVisible": true,
                        "top": "90dp"
                    },
                    "contextualMenu.flxOption4": {
                        "bottom": "viz.val_cleared",
                        "height": "30dp",
                        "top": "50dp"
                    },
                    "contextualMenu.flxOptionsSeperator": {
                        "bottom": "5dp",
                        "isVisible": true,
                        "top": "85px"
                    },
                    "contextualMenu.imgDownArrow": {
                        "src": "downarrow_2x.png"
                    },
                    "contextualMenu.imgOption1": {
                        "src": "imagedrag.png"
                    },
                    "contextualMenu.imgOption2": {
                        "src": "edit2x.png"
                    },
                    "contextualMenu.imgOption3": {
                        "src": "imagedrag.png"
                    },
                    "contextualMenu.imgOption4": {
                        "src": "deactive_2x.png"
                    },
                    "contextualMenu.imgUpArrow": {
                        "src": "uparrow_2x.png"
                    },
                    "contextualMenu.lblHeader": {
                        "bottom": "15dp",
                        "isVisible": false,
                        "top": "20dp"
                    },
                    "contextualMenu.lblIconOption2": {
                        "i18n_text": "kony.i18n.getLocalizedString(\"i18n.userwidgetmodel.lblIconOption1\")",
                        "left": "20dp"
                    },
                    "contextualMenu.lblIconOption3": {
                        "left": "20dp",
                        "text": ""
                    },
                    "contextualMenu.lblIconOption4": {
                        "left": "20dp"
                    },
                    "contextualMenu.lblOption3": {
                        "text": "Copy Role"
                    },
                    "flxContextualMenu": {
                        "left": "viz.val_cleared",
                        "right": "20dp",
                        "width": "137px"
                    },
                    "flxListingSegmentWrapper": {
                        "bottom": "viz.val_cleared",
                        "height": "100%"
                    },
                    "flxPagination": {
                        "isVisible": false
                    },
                    "listingSegmentClient": {
                        "bottom": "viz.val_cleared",
                        "centerX": "viz.val_cleared",
                        "centerY": "viz.val_cleared",
                        "height": "100%",
                        "left": "0dp",
                        "maxHeight": "viz.val_cleared",
                        "maxWidth": "viz.val_cleared",
                        "minHeight": "viz.val_cleared",
                        "minWidth": "viz.val_cleared",
                        "right": "0dp",
                        "top": "0dp",
                        "width": "viz.val_cleared"
                    },
                    "pagination": {
                        "isVisible": false
                    },
                    "segListing": {
                        "bottom": "viz.val_cleared",
                        "height": "100%"
                    }
                }
            }, {
                "overrides": {}
            }, {
                "overrides": {}
            });
            flxGroupListContainer.add(listingSegmentClient);
            var flxStatusFilter = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                "clipBounds": true,
                "id": "flxStatusFilter",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "isModalContainer": false,
                "right": "80px",
                "skin": "slFbox",
                "top": "37px",
                "width": "120px",
                "zIndex": 5
            }, {}, {});
            flxStatusFilter.setDefaultUnit(kony.flex.DP);
            var statusFilterMenu = new com.adminConsole.common.statusFilterMenu({
                "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                "id": "statusFilterMenu",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "masterType": constants.MASTER_TYPE_DEFAULT,
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "5dp",
                "width": "100%",
                "zIndex": 5,
                "overrides": {
                    "imgUpArrow": {
                        "src": "uparrow_2x.png"
                    },
                    "segStatusFilterDropdown": {
                        "left": "0dp",
                        "top": "5dp"
                    }
                }
            }, {
                "overrides": {}
            }, {
                "overrides": {}
            });
            flxStatusFilter.add(statusFilterMenu);
            var flxTypeFilter = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                "clipBounds": true,
                "id": "flxTypeFilter",
                "isVisible": false,
                "layoutType": kony.flex.FREE_FORM,
                "isModalContainer": false,
                "right": "200px",
                "skin": "slFbox",
                "top": "37px",
                "width": "120px",
                "zIndex": 5
            }, {}, {});
            flxTypeFilter.setDefaultUnit(kony.flex.DP);
            var typeFilterMenu = new com.adminConsole.common.statusFilterMenu({
                "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                "id": "typeFilterMenu",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "masterType": constants.MASTER_TYPE_DEFAULT,
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "5dp",
                "width": "100%",
                "zIndex": 5,
                "overrides": {
                    "imgUpArrow": {
                        "src": "uparrow_2x.png"
                    },
                    "segStatusFilterDropdown": {
                        "left": "0dp",
                        "top": "5dp"
                    }
                }
            }, {
                "overrides": {}
            }, {
                "overrides": {}
            });
            flxTypeFilter.add(typeFilterMenu);
            var flxBusinessTypeFilter = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                "clipBounds": true,
                "id": "flxBusinessTypeFilter",
                "isVisible": false,
                "layoutType": kony.flex.FREE_FORM,
                "isModalContainer": false,
                "right": "150px",
                "skin": "slFbox",
                "top": "47px",
                "width": "170px",
                "zIndex": 5
            }, {}, {});
            flxBusinessTypeFilter.setDefaultUnit(kony.flex.DP);
            var BusinessTypeFilterMenu = new com.adminConsole.common.statusFilterMenu({
                "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                "id": "BusinessTypeFilterMenu",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "masterType": constants.MASTER_TYPE_DEFAULT,
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "5dp",
                "width": "100%",
                "zIndex": 5,
                "overrides": {
                    "imgUpArrow": {
                        "src": "uparrow_2x.png"
                    },
                    "segStatusFilterDropdown": {
                        "left": "0dp",
                        "top": "5dp"
                    }
                }
            }, {
                "overrides": {}
            }, {
                "overrides": {}
            });
            flxBusinessTypeFilter.add(BusinessTypeFilterMenu);
            var flxSegMore = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                "clipBounds": true,
                "id": "flxSegMore",
                "isVisible": false,
                "layoutType": kony.flex.FLOW_VERTICAL,
                "left": "170px",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "190px",
                "width": "250px",
                "zIndex": 5
            }, {}, {});
            flxSegMore.setDefaultUnit(kony.flex.DP);
            var flxUpArrowImage = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "12px",
                "id": "flxUpArrowImage",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0px",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0px",
                "width": "100%",
                "zIndex": 2
            }, {}, {});
            flxUpArrowImage.setDefaultUnit(kony.flex.DP);
            var imgUpArrow = new kony.ui.Image2({
                "centerX": "50%",
                "height": "12dp",
                "id": "imgUpArrow",
                "isVisible": true,
                "skin": "slImage",
                "src": "uparrow_2x.png",
                "top": "0dp",
                "width": "15dp",
                "zIndex": 1
            }, {
                "imageScaleMode": constants.IMAGE_SCALE_MODE_FIT_TO_DIMENSIONS,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxUpArrowImage.add(imgUpArrow);
            var flxBusinessTypesOuter = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                "clipBounds": true,
                "id": "flxBusinessTypesOuter",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "sknflxffffffop100dbdbe6Radius3px",
                "top": "-1dp",
                "width": "100%",
                "zIndex": 1
            }, {}, {});
            flxBusinessTypesOuter.setDefaultUnit(kony.flex.DP);
            var segBusinessTypes = new kony.ui.SegmentedUI2({
                "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                "bottom": 10,
                "data": [{
                    "lblName": "Granville Street, Vancover, BC, Canada"
                }, {
                    "lblName": "Granville Street, Vancover, BC, Canada"
                }, {
                    "lblName": "Granville Street, Vancover, BC, Canada"
                }],
                "groupCells": false,
                "id": "segBusinessTypes",
                "isVisible": true,
                "left": "15dp",
                "needPageIndicator": true,
                "pageOffDotImage": "pageoffdot.png",
                "pageOnDotImage": "pageondot.png",
                "retainSelection": false,
                "right": "15px",
                "rowFocusSkin": "seg2Focus",
                "rowSkin": "seg2Normal",
                "rowTemplate": "flxMore",
                "sectionHeaderSkin": "sliPhoneSegmentHeader",
                "selectionBehavior": constants.SEGUI_DEFAULT_BEHAVIOR,
                "separatorColor": "aaaaaa00",
                "separatorRequired": false,
                "separatorThickness": 0,
                "showScrollbars": false,
                "top": "0dp",
                "viewType": constants.SEGUI_VIEW_TYPE_TABLEVIEW,
                "widgetDataMap": {
                    "flxMore": "flxMore",
                    "lblName": "lblName"
                },
                "zIndex": 1
            }, {
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxBusinessTypesOuter.add(segBusinessTypes);
            var flxDownArrowImage = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "12px",
                "id": "flxDownArrowImage",
                "isVisible": false,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0px",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "-1px",
                "width": "100%",
                "zIndex": 2
            }, {}, {});
            flxDownArrowImage.setDefaultUnit(kony.flex.DP);
            var imgDownArrow = new kony.ui.Image2({
                "centerX": "50%",
                "height": "12dp",
                "id": "imgDownArrow",
                "isVisible": true,
                "right": "15dp",
                "skin": "slImage",
                "src": "downarrow_2x.png",
                "top": "0dp",
                "width": "15dp",
                "zIndex": 1
            }, {
                "imageScaleMode": constants.IMAGE_SCALE_MODE_FIT_TO_DIMENSIONS,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxDownArrowImage.add(imgDownArrow);
            flxSegMore.add(flxUpArrowImage, flxBusinessTypesOuter, flxDownArrowImage);
            flxSegGroups.add(flxGroupHeader, flxGroupSeparator, flxGroupListContainer, flxStatusFilter, flxTypeFilter, flxBusinessTypeFilter, flxSegMore);
            flxGroupListMain.add(flxGroupSearch, flxSegGroups);
            flxGroupList.add(flxGroupListMain);
            var flxHeaderDropdown = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                "clipBounds": true,
                "id": "flxHeaderDropdown",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "isModalContainer": false,
                "right": "35dp",
                "skin": "slFbox",
                "top": "20dp",
                "width": "400dp",
                "zIndex": 5
            }, {}, {});
            flxHeaderDropdown.setDefaultUnit(kony.flex.DP);
            var dropdownMainHeader = new com.adminConsole.common.dropdownMainHeader({
                "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                "id": "dropdownMainHeader",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "masterType": constants.MASTER_TYPE_DEFAULT,
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0dp",
                "width": "100%",
                "overrides": {
                    "flxDropdown": {
                        "isVisible": false
                    },
                    "imgUpArrow": {
                        "src": "uparrow_2x.png"
                    }
                }
            }, {
                "overrides": {}
            }, {
                "overrides": {}
            });
            flxHeaderDropdown.add(dropdownMainHeader);
            var flxViewRoles = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "bottom": "20dp",
                "clipBounds": true,
                "id": "flxViewRoles",
                "isVisible": false,
                "layoutType": kony.flex.FREE_FORM,
                "left": "35dp",
                "isModalContainer": false,
                "right": "35dp",
                "skin": "slFbox",
                "top": "129dp",
                "zIndex": 1
            }, {}, {});
            flxViewRoles.setDefaultUnit(kony.flex.DP);
            var flxGroupview = new kony.ui.FlexScrollContainer({
                "allowHorizontalBounce": false,
                "allowVerticalBounce": true,
                "bottom": "0px",
                "bounces": true,
                "clipBounds": true,
                "enableScrolling": true,
                "horizontalScrollIndicator": true,
                "id": "flxGroupview",
                "isVisible": true,
                "layoutType": kony.flex.FLOW_VERTICAL,
                "left": "0dp",
                "pagingEnabled": false,
                "right": "0px",
                "scrollDirection": kony.flex.SCROLL_VERTICAL,
                "skin": "sknFlxBgFFFFFFbrE4E6EC1pxr4px",
                "top": "0dp",
                "verticalScrollIndicator": true,
                "zIndex": 1
            }, {}, {});
            flxGroupview.setDefaultUnit(kony.flex.DP);
            var flxGroupViewDetails = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                "clipBounds": true,
                "id": "flxGroupViewDetails",
                "isVisible": true,
                "layoutType": kony.flex.FLOW_VERTICAL,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0dp",
                "width": "100%",
                "zIndex": 2
            }, {}, {});
            flxGroupViewDetails.setDefaultUnit(kony.flex.DP);
            var flxGroupViewContainer = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                "clipBounds": true,
                "id": "flxGroupViewContainer",
                "isVisible": true,
                "layoutType": kony.flex.FLOW_VERTICAL,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0dp",
                "width": "100%",
                "zIndex": 1
            }, {}, {});
            flxGroupViewContainer.setDefaultUnit(kony.flex.DP);
            var flxViewDescription = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": false,
                "height": "40dp",
                "id": "flxViewDescription",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0px",
                "isModalContainer": false,
                "right": "35px",
                "skin": "slFbox",
                "top": "10px",
                "zIndex": 2
            }, {}, {});
            flxViewDescription.setDefaultUnit(kony.flex.DP);
            var flxRoleDetails = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "100%",
                "id": "flxRoleDetails",
                "isVisible": true,
                "layoutType": kony.flex.FLOW_HORIZONTAL,
                "left": "20dp",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0dp",
                "width": "200dp"
            }, {}, {});
            flxRoleDetails.setDefaultUnit(kony.flex.DP);
            var flxToggle = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "centerY": "50%",
                "clipBounds": true,
                "height": "20dp",
                "id": "flxToggle",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "isModalContainer": false,
                "right": "5px",
                "skin": "slFbox",
                "width": "20px",
                "zIndex": 1
            }, {}, {
                "hoverSkin": "sknCursor"
            });
            flxToggle.setDefaultUnit(kony.flex.DP);
            var imgToggleDescription = new kony.ui.Image2({
                "centerX": "50%",
                "centerY": "50%",
                "height": "10dp",
                "id": "imgToggleDescription",
                "isVisible": false,
                "skin": "slImage",
                "src": "img_down_arrow.png",
                "width": "10dp",
                "zIndex": 1
            }, {
                "imageScaleMode": constants.IMAGE_SCALE_MODE_MAINTAIN_ASPECT_RATIO,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var lblToggleDescription = new kony.ui.Label({
                "centerY": "50%",
                "height": "12dp",
                "id": "lblToggleDescription",
                "isVisible": true,
                "left": "0dp",
                "skin": "sknIcon12pxBlack",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.userwidgetmodel.fontIconBreadcrumbsDown\")",
                "width": "12dp",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxToggle.add(imgToggleDescription, lblToggleDescription);
            var lblDescription = new kony.ui.Label({
                "centerY": "50.00%",
                "id": "lblDescription",
                "isVisible": true,
                "left": "0dp",
                "skin": "sknLbl192B4518pxLatoReg",
                "text": "Role Details",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxRoleDetails.add(flxToggle, lblDescription);
            var flxStatusDetails = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "100%",
                "id": "flxStatusDetails",
                "isVisible": true,
                "layoutType": kony.flex.FLOW_HORIZONTAL,
                "reverseLayoutDirection": true,
                "isModalContainer": false,
                "right": "0dp",
                "skin": "slFbox",
                "top": "0dp",
                "width": "200dp",
                "zIndex": 1
            }, {}, {});
            flxStatusDetails.setDefaultUnit(kony.flex.DP);
            var flxOptions = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "centerY": "50%",
                "clipBounds": true,
                "height": "24dp",
                "id": "flxOptions",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "isModalContainer": false,
                "right": "20px",
                "skin": "sknFlxBorffffff1pxRound",
                "top": "0px",
                "width": "24px",
                "zIndex": 3
            }, {}, {
                "hoverSkin": "sknflxffffffop100Border424242Radius100px"
            });
            flxOptions.setDefaultUnit(kony.flex.DP);
            var lblIconOptions = new kony.ui.Label({
                "centerX": "50%",
                "centerY": "50%",
                "id": "lblIconOptions",
                "isVisible": true,
                "skin": "sknFontIconOptionMenu",
                "text": "",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxOptions.add(lblIconOptions);
            var statusValue = new kony.ui.Label({
                "centerY": "50%",
                "id": "statusValue",
                "isVisible": true,
                "right": "35dp",
                "skin": "sknlblLato5bc06cBold14px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.secureimage.Active\")",
                "top": "0px",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var statusIcon = new kony.ui.Label({
                "centerY": "50%",
                "id": "statusIcon",
                "isVisible": true,
                "right": "5dp",
                "skin": "sknFontIconActivate",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmAlertsManagement.lblViewAlertStatusKey\")",
                "top": 0,
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxStatusDetails.add(flxOptions, statusValue, statusIcon);
            flxViewDescription.add(flxRoleDetails, flxStatusDetails);
            var flxToggleContent = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                "clipBounds": true,
                "id": "flxToggleContent",
                "isVisible": true,
                "layoutType": kony.flex.FLOW_VERTICAL,
                "left": "0dp",
                "isModalContainer": false,
                "right": "20dp",
                "skin": "slFbox",
                "top": "0dp",
                "width": "100%"
            }, {}, {});
            flxToggleContent.setDefaultUnit(kony.flex.DP);
            var flxViewRow1 = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                "clipBounds": true,
                "id": "flxViewRow1",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0dp",
                "width": "100%"
            }, {}, {});
            flxViewRow1.setDefaultUnit(kony.flex.DP);
            var flxGroupViewName = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                "clipBounds": true,
                "id": "flxGroupViewName",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "45dp",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "15dp",
                "width": "30%",
                "zIndex": 1
            }, {}, {});
            flxGroupViewName.setDefaultUnit(kony.flex.DP);
            var fontIconViewName = new kony.ui.Label({
                "id": "fontIconViewName",
                "isVisible": false,
                "left": "0dp",
                "skin": "sknfontIconViewScreen",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmGroups.fontIconViewName\")",
                "top": "1dp",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var lblViewName = new kony.ui.Label({
                "id": "lblViewName",
                "isVisible": true,
                "left": "0dp",
                "skin": "sknlblLato696c7311px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmAdManagement.RoleNameCAPS\")",
                "top": "1dp",
                "width": "90px",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var lblViewNameValue = new kony.ui.Label({
                "id": "lblViewNameValue",
                "isVisible": true,
                "left": "0px",
                "skin": "sknlbl485C75LatoSemiBold13px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmGroups.lblViewNameValue\")",
                "top": "25px",
                "width": "100%",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxGroupViewName.add(fontIconViewName, lblViewName, lblViewNameValue);
            var flxGroupRoleType = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "50dp",
                "id": "flxGroupRoleType",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "36%",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "15dp",
                "width": "30%",
                "zIndex": 1
            }, {}, {});
            flxGroupRoleType.setDefaultUnit(kony.flex.DP);
            var lblIconGroupType = new kony.ui.Label({
                "id": "lblIconGroupType",
                "isVisible": false,
                "left": "0dp",
                "skin": "sknfontIconViewScreen",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmGroups.fontIconViewName\")",
                "top": "1dp",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var lblViewGroupType = new kony.ui.Label({
                "id": "lblViewGroupType",
                "isVisible": true,
                "left": "0dp",
                "skin": "sknlblLato696c7311px",
                "text": "ROLE TYPE",
                "top": "1dp",
                "width": "90px",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var lblViewGroupTypeValue = new kony.ui.Label({
                "id": "lblViewGroupTypeValue",
                "isVisible": true,
                "left": "0px",
                "skin": "sknlbl485C75LatoSemiBold13px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmGroups.lblViewNameValue\")",
                "top": "25px",
                "width": "100%",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxGroupRoleType.add(lblIconGroupType, lblViewGroupType, lblViewGroupTypeValue);
            var flxViewAgreement = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "50dp",
                "id": "flxViewAgreement",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "68%",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "15dp",
                "width": "30%",
                "zIndex": 1
            }, {}, {});
            flxViewAgreement.setDefaultUnit(kony.flex.DP);
            var lblIconViewAgreement = new kony.ui.Label({
                "id": "lblIconViewAgreement",
                "isVisible": false,
                "left": "0dp",
                "skin": "sknfontIconViewScreen",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmGroups.fontIconViewName\")",
                "top": "1dp",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var lblViewAgreement = new kony.ui.Label({
                "id": "lblViewAgreement",
                "isVisible": true,
                "left": "0dp",
                "skin": "sknlblLato696c7311px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmGroups.E-AGREEMENT\")",
                "top": "1dp",
                "width": "90px",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var lblViewAgreementValue = new kony.ui.Label({
                "id": "lblViewAgreementValue",
                "isVisible": true,
                "left": "0px",
                "skin": "sknlbl485C75LatoSemiBold13px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmGroups.lblViewNameValue\")",
                "top": "25px",
                "width": "100%",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxViewAgreement.add(lblIconViewAgreement, lblViewAgreement, lblViewAgreementValue);
            flxViewRow1.add(flxGroupViewName, flxGroupRoleType, flxViewAgreement);
            var flxViewRow2 = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                "clipBounds": true,
                "id": "flxViewRow2",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "20dp",
                "width": "100%"
            }, {}, {});
            flxViewRow2.setDefaultUnit(kony.flex.DP);
            var flxGroupViewBusinessTypes = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                "clipBounds": true,
                "id": "flxGroupViewBusinessTypes",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "45dp",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0dp",
                "width": "30%",
                "zIndex": 1
            }, {}, {});
            flxGroupViewBusinessTypes.setDefaultUnit(kony.flex.DP);
            var lblViewBusinessTypes = new kony.ui.Label({
                "id": "lblViewBusinessTypes",
                "isVisible": true,
                "left": "0dp",
                "skin": "sknlblLato696c7311px",
                "text": "SERVICE",
                "top": "1dp",
                "width": "90%",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var lblViewBusinessTypesValue = new kony.ui.Label({
                "id": "lblViewBusinessTypesValue",
                "isVisible": true,
                "left": "0px",
                "skin": "sknlbl485C75LatoSemiBold13px",
                "text": "Sole Proprietor, Partnership, Unicorporated Association, Limited Liability Company,",
                "top": "25px",
                "width": "100%",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var flxMoreTypes = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "15px",
                "id": "flxMoreTypes",
                "isVisible": false,
                "layoutType": kony.flex.FREE_FORM,
                "isModalContainer": false,
                "right": "40dp",
                "skin": "sknCursor",
                "top": "62dp",
                "width": "60dp",
                "zIndex": 10
            }, {}, {
                "hoverSkin": "hoverhandSkin2"
            });
            flxMoreTypes.setDefaultUnit(kony.flex.DP);
            var lblMoreTypes = new kony.ui.Label({
                "centerY": "50%",
                "id": "lblMoreTypes",
                "isVisible": true,
                "right": "0dp",
                "skin": "sknLblLato13px117eb0",
                "text": "+4 more",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_CENTER,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxMoreTypes.add(lblMoreTypes);
            flxGroupViewBusinessTypes.add(lblViewBusinessTypes, lblViewBusinessTypesValue, flxMoreTypes);
            var flxGroupRoleDefaultType = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                "clipBounds": true,
                "id": "flxGroupRoleDefaultType",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "68%",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0dp",
                "width": "30%",
                "zIndex": 1
            }, {}, {});
            flxGroupRoleDefaultType.setDefaultUnit(kony.flex.DP);
            var lblIconGroupDefaultType = new kony.ui.Label({
                "id": "lblIconGroupDefaultType",
                "isVisible": true,
                "left": "0dp",
                "skin": "sknlblLato696c7311px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.Group.DefaultForService\")",
                "top": "1dp",
                "width": "90%",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var lblViewGroupDefaultTypeValue = new kony.ui.Label({
                "id": "lblViewGroupDefaultTypeValue",
                "isVisible": true,
                "left": "0px",
                "skin": "sknlbl485C75LatoSemiBold13px",
                "text": "Sole Proprietor, Partnership, Unicorporated Association, Limited Liability Company,",
                "top": "25px",
                "width": "100%",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var flxMoreDefaultTypes = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "15px",
                "id": "flxMoreDefaultTypes",
                "isVisible": false,
                "layoutType": kony.flex.FREE_FORM,
                "isModalContainer": false,
                "right": "40dp",
                "skin": "sknCursor",
                "top": "62dp",
                "width": "60dp",
                "zIndex": 10
            }, {}, {
                "hoverSkin": "hoverhandSkin2"
            });
            flxMoreDefaultTypes.setDefaultUnit(kony.flex.DP);
            var lblMoreDefaultTypes = new kony.ui.Label({
                "centerY": "50%",
                "id": "lblMoreDefaultTypes",
                "isVisible": true,
                "right": "0dp",
                "skin": "sknLblLato13px117eb0",
                "text": "+4 more",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_CENTER,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxMoreDefaultTypes.add(lblMoreDefaultTypes);
            flxGroupRoleDefaultType.add(lblIconGroupDefaultType, lblViewGroupDefaultTypeValue, flxMoreDefaultTypes);
            var flxApplytoAll = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                "clipBounds": true,
                "id": "flxApplytoAll",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "36%",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0dp",
                "width": "30%",
                "zIndex": 1
            }, {}, {});
            flxApplytoAll.setDefaultUnit(kony.flex.DP);
            var lblViewApplyToAll = new kony.ui.Label({
                "id": "lblViewApplyToAll",
                "isVisible": true,
                "left": "0dp",
                "skin": "sknlblLato696c7311px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmGroupsController.appliedToAll\")",
                "top": "1dp",
                "width": "90%",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var lblViewApplyToAllValue = new kony.ui.Label({
                "id": "lblViewApplyToAllValue",
                "isVisible": true,
                "left": "0px",
                "skin": "sknlbl485C75LatoSemiBold13px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmCustomerManagementController.YES\")",
                "top": "25px",
                "width": "100%",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxApplytoAll.add(lblViewApplyToAll, lblViewApplyToAllValue);
            flxViewRow2.add(flxGroupViewBusinessTypes, flxGroupRoleDefaultType, flxApplytoAll);
            var flxGroupViewStatus = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "50dp",
                "id": "flxGroupViewStatus",
                "isVisible": false,
                "layoutType": kony.flex.FREE_FORM,
                "left": "64%",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "15dp",
                "width": "25%",
                "zIndex": 1
            }, {}, {});
            flxGroupViewStatus.setDefaultUnit(kony.flex.DP);
            var fontIconViewStatus = new kony.ui.Label({
                "id": "fontIconViewStatus",
                "isVisible": false,
                "left": "0dp",
                "skin": "sknfontIconViewScreen",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmGroups.fontIconViewStatus\")",
                "top": "1dp",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var fontIconStatusValue = new kony.ui.Label({
                "id": "fontIconStatusValue",
                "isVisible": true,
                "left": "0dp",
                "skin": "sknFontIconActivate",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmAlertsManagement.lblViewAlertStatusKey\")",
                "top": "25dp",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var lblStatusView = new kony.ui.Label({
                "id": "lblStatusView",
                "isVisible": true,
                "left": "0dp",
                "skin": "sknlblLato5d6c7f12px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.roles.STATUS\")",
                "top": "1dp",
                "width": "90px",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var lblViewValue2 = new kony.ui.Label({
                "id": "lblViewValue2",
                "isVisible": true,
                "left": "20px",
                "skin": "sknlblLato5bc06cBold14px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.secureimage.Active\")",
                "top": "22px",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxGroupViewStatus.add(fontIconViewStatus, fontIconStatusValue, lblStatusView, lblViewValue2);
            var flxGroupViewValidTill = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "50dp",
                "id": "flxGroupViewValidTill",
                "isVisible": false,
                "layoutType": kony.flex.FREE_FORM,
                "left": "60%",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "15dp",
                "width": "25%",
                "zIndex": 1
            }, {}, {});
            flxGroupViewValidTill.setDefaultUnit(kony.flex.DP);
            var fontIconValidTill = new kony.ui.Label({
                "id": "fontIconValidTill",
                "isVisible": true,
                "left": "0dp",
                "skin": "sknfontIconViewScreen",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmGroups.fontIconValidTill\")",
                "top": "1dp",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var lblValidTillView = new kony.ui.Label({
                "id": "lblValidTillView",
                "isVisible": true,
                "left": "20dp",
                "skin": "sknlblLato5d6c7f12px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.roles.VALIDTILL\")",
                "top": "1dp",
                "width": "90px",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var lblViewValidTillValue = new kony.ui.Label({
                "id": "lblViewValidTillValue",
                "isVisible": true,
                "left": "0px",
                "skin": "sknlblLatoBold35475f14px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmGroups.lblViewValidTillValue\")",
                "top": "25px",
                "width": "100%",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxGroupViewValidTill.add(fontIconValidTill, lblValidTillView, lblViewValidTillValue);
            var btnEdit = new kony.ui.Button({
                "focusSkin": "sknbtnf7f7faLatoRegular12Px485c75Border1px485C75Radius20px",
                "height": "22dp",
                "id": "btnEdit",
                "isVisible": false,
                "right": "20dp",
                "skin": "sknbtnf7f7faLatoRegular12Px485c75Border1px485C75Radius20px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.roles.Edit\")",
                "top": "15dp",
                "width": "54dp",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_CENTER,
                "displayText": true,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {
                "hoverSkin": "sknbtnffffffLatoRegular12Px485c75Border1px485C75Radius20px"
            });
            var Description = new com.adminConsole.view.Description1({
                "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                "id": "Description",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "masterType": constants.MASTER_TYPE_DEFAULT,
                "isModalContainer": false,
                "right": "20dp",
                "skin": "slFbox",
                "top": "10dp",
                "overrides": {
                    "Description1": {
                        "bottom": "viz.val_cleared",
                        "right": "20dp",
                        "top": "10dp",
                        "width": "viz.val_cleared"
                    },
                    "flxToggleDescription": {
                        "isVisible": false
                    },
                    "flxViewDescription": {
                        "left": "45px"
                    },
                    "imgToggleDescription": {
                        "src": "img_down_arrow.png"
                    },
                    "rtxDescription": {
                        "left": "45px",
                        "text": "Lorem ipsum dolor sit amet, consectetur adipiscing elit.  cing elit.Lorem ipsum dolor sit amet, consectetur. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Lorem ipsum dolor sit amet, consectetur adipiscing elit."
                    }
                }
            }, {
                "overrides": {}
            }, {
                "overrides": {}
            });
            var flxEditDisableSkin = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "25dp",
                "id": "flxEditDisableSkin",
                "isVisible": false,
                "layoutType": kony.flex.FREE_FORM,
                "isModalContainer": false,
                "right": "35dp",
                "skin": "sknflxbgffffffOp50Per",
                "top": "15dp",
                "width": "60dp",
                "zIndex": 2
            }, {}, {
                "hoverSkin": "sknCursorDisabled"
            });
            flxEditDisableSkin.setDefaultUnit(kony.flex.DP);
            flxEditDisableSkin.add();
            flxToggleContent.add(flxViewRow1, flxViewRow2, flxGroupViewStatus, flxGroupViewValidTill, btnEdit, Description, flxEditDisableSkin);
            flxGroupViewContainer.add(flxViewDescription, flxToggleContent);
            flxGroupViewDetails.add(flxGroupViewContainer);
            var flxFeaturesContent = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                "clipBounds": true,
                "id": "flxFeaturesContent",
                "isVisible": true,
                "layoutType": kony.flex.FLOW_VERTICAL,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "25dp",
                "width": kony.flex.USE_PREFFERED_SIZE
            }, {}, {});
            flxFeaturesContent.setDefaultUnit(kony.flex.DP);
            var flxSeparator1 = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "1px",
                "id": "flxSeparator1",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "isModalContainer": false,
                "right": "0dp",
                "skin": "sknFlxBorE1E5ED1pxKA",
                "top": "0dp",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 2
            }, {}, {});
            flxSeparator1.setDefaultUnit(kony.flex.DP);
            flxSeparator1.add();
            var flxTableViewTabs = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "50dp",
                "id": "flxTableViewTabs",
                "isVisible": true,
                "layoutType": kony.flex.FLOW_HORIZONTAL,
                "left": "20dp",
                "isModalContainer": false,
                "right": "20dp",
                "skin": "slFbox",
                "top": "0dp",
                "zIndex": 1
            }, {}, {});
            flxTableViewTabs.setDefaultUnit(kony.flex.DP);
            var flxFeaturesHeader = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "100%",
                "id": "flxFeaturesHeader",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0dp",
                "width": "150dp",
                "zIndex": 1
            }, {}, {
                "hoverSkin": "sknCursor"
            });
            flxFeaturesHeader.setDefaultUnit(kony.flex.DP);
            var lblFeatures = new kony.ui.Label({
                "height": "30dp",
                "id": "lblFeatures",
                "isVisible": true,
                "left": "0dp",
                "skin": "sknLatoSemibold485C7512px",
                "text": "FEATURES AND ACTIONS",
                "top": "20dp",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_TOP_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {
                "hoverSkin": "sknlblLato0hd9dd99ab6d14aHover"
            });
            flxFeaturesHeader.add(lblFeatures);
            var flxCustomers = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "100%",
                "id": "flxCustomers",
                "isVisible": false,
                "layoutType": kony.flex.FREE_FORM,
                "left": "20dp",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0dp",
                "width": "95px",
                "zIndex": 1
            }, {}, {
                "hoverSkin": "sknCursor"
            });
            flxCustomers.setDefaultUnit(kony.flex.DP);
            var lblCustomersFix = new kony.ui.Label({
                "height": "30dp",
                "id": "lblCustomersFix",
                "isVisible": true,
                "left": "0dp",
                "skin": "sknLatoSemibold485C7512px",
                "text": "CUSTOMERS ",
                "top": "20dp",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_TOP_CENTER,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {
                "hoverSkin": "sknlblLato0hd9dd99ab6d14aHover"
            });
            var flxIdentifier2 = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "bottom": 0,
                "clipBounds": true,
                "height": "3dp",
                "id": "flxIdentifier2",
                "isVisible": false,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "sknflx11abebRadius3px",
                "width": "95px",
                "zIndex": 1
            }, {}, {});
            flxIdentifier2.setDefaultUnit(kony.flex.DP);
            flxIdentifier2.add();
            flxCustomers.add(lblCustomersFix, flxIdentifier2);
            var flxLimits = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "100%",
                "id": "flxLimits",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "20dp",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0dp",
                "width": "50px",
                "zIndex": 1
            }, {}, {
                "hoverSkin": "sknCursor"
            });
            flxLimits.setDefaultUnit(kony.flex.DP);
            var lblLimitsFix = new kony.ui.Label({
                "height": "30dp",
                "id": "lblLimitsFix",
                "isVisible": true,
                "left": "5dp",
                "skin": "sknLatoSemibold485C7512px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.Group.Limits\")",
                "top": "20dp",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_TOP_CENTER,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {
                "hoverSkin": "sknlblLato0hd9dd99ab6d14aHover"
            });
            flxLimits.add(lblLimitsFix);
            flxTableViewTabs.add(flxFeaturesHeader, flxCustomers, flxLimits);
            var flxViewSeperator = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "1px",
                "id": "flxViewSeperator",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "isModalContainer": false,
                "right": "0dp",
                "skin": "sknFlxBorE1E5ED1pxKA",
                "top": "0dp",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 2
            }, {}, {});
            flxViewSeperator.setDefaultUnit(kony.flex.DP);
            flxViewSeperator.add();
            var flxAddDynamicSegments = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                "bottom": "35dp",
                "clipBounds": true,
                "id": "flxAddDynamicSegments",
                "isVisible": false,
                "layoutType": kony.flex.FLOW_VERTICAL,
                "left": "20dp",
                "isModalContainer": false,
                "skin": "slFbox",
                "width": kony.flex.USE_PREFFERED_SIZE
            }, {}, {});
            flxAddDynamicSegments.setDefaultUnit(kony.flex.DP);
            flxAddDynamicSegments.add();
            var flxNoResultMessage = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "170dp",
                "id": "flxNoResultMessage",
                "isVisible": false,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0dp",
                "width": "100%",
                "zIndex": 1
            }, {}, {});
            flxNoResultMessage.setDefaultUnit(kony.flex.DP);
            var rtxNoRecordsAvailable = new kony.ui.RichText({
                "centerY": "50%",
                "id": "rtxNoRecordsAvailable",
                "isVisible": true,
                "left": "0dp",
                "linkSkin": "defRichTextLink",
                "skin": "sknRtxLato84939e12Px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmGroups.rtxNoRecordsAvailable\")",
                "width": "100%",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_CENTER,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxNoResultMessage.add(rtxNoRecordsAvailable);
            var flxDynamicLimits = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                "bottom": "35dp",
                "clipBounds": true,
                "id": "flxDynamicLimits",
                "isVisible": false,
                "layoutType": kony.flex.FLOW_VERTICAL,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "slFbox",
                "width": kony.flex.USE_PREFFERED_SIZE
            }, {}, {});
            flxDynamicLimits.setDefaultUnit(kony.flex.DP);
            flxDynamicLimits.add();
            flxFeaturesContent.add(flxSeparator1, flxTableViewTabs, flxViewSeperator, flxAddDynamicSegments, flxNoResultMessage, flxDynamicLimits);
            flxGroupview.add(flxGroupViewDetails, flxFeaturesContent);
            var assignContextualMenu = new com.adminConsole.common.contextualMenu1({
                "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                "id": "assignContextualMenu",
                "isVisible": false,
                "layoutType": kony.flex.FLOW_VERTICAL,
                "masterType": constants.MASTER_TYPE_DEFAULT,
                "isModalContainer": false,
                "right": "12dp",
                "skin": "slFbox",
                "top": "43px",
                "width": "142dp",
                "zIndex": 2,
                "overrides": {
                    "btnLink1": {
                        "isVisible": false,
                        "text": "Features and Actions",
                        "top": "15dp"
                    },
                    "btnLink2": {
                        "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmGroupsController.Customers\")",
                        "isVisible": false
                    },
                    "contextualMenu1": {
                        "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                        "isVisible": false,
                        "left": "viz.val_cleared",
                        "right": "12dp",
                        "top": "43px",
                        "width": "142dp",
                        "zIndex": 2
                    },
                    "contextualMenu1Inner": {
                        "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                        "left": "0px",
                        "right": "viz.val_cleared",
                        "width": "100%",
                        "layoutType": kony.flex.FREE_FORM
                    },
                    "flxDownArrowImage": {
                        "isVisible": false
                    },
                    "flxOption1": {
                        "height": "30dp",
                        "top": "10dp"
                    },
                    "flxOption2": {
                        "bottom": "viz.val_cleared",
                        "height": "30dp",
                        "top": "40dp"
                    },
                    "flxOption3": {
                        "bottom": "10dp",
                        "height": "30dp",
                        "top": "80dp"
                    },
                    "flxOption4": {
                        "isVisible": false
                    },
                    "flxOptionsSeperator": {
                        "isVisible": true,
                        "top": "75px"
                    },
                    "flxUpArrowImage": {
                        "top": "-1px"
                    },
                    "imgDownArrow": {
                        "src": "downarrow_2x.png"
                    },
                    "imgOption1": {
                        "src": "imagedrag.png"
                    },
                    "imgOption2": {
                        "src": "edit2x.png"
                    },
                    "imgOption3": {
                        "src": "imagedrag.png"
                    },
                    "imgOption4": {
                        "src": "deactive_2x.png"
                    },
                    "imgUpArrow": {
                        "src": "uparrow_2x.png"
                    },
                    "lblHeader": {
                        "isVisible": false,
                        "top": "20px"
                    },
                    "lblIconOption1": {
                        "left": "20px"
                    },
                    "lblIconOption2": {
                        "i18n_text": "kony.i18n.getLocalizedString(\"i18n.userwidgetmodel.lblIconOption4\")",
                        "left": "20dp"
                    },
                    "lblIconOption3": {
                        "left": "20dp",
                        "text": ""
                    },
                    "lblOption1": {
                        "i18n_text": "kony.i18n.getLocalizedString(\"i18n.LeadManagement.contextualMenuEdit\")",
                        "maxHeight": "viz.val_cleared"
                    },
                    "lblOption2": {
                        "i18n_text": "kony.i18n.getLocalizedString(\"i18n.SecurityQuestions.Deactivate\")"
                    },
                    "lblOption3": {
                        "text": "Copy Role"
                    }
                }
            }, {
                "overrides": {}
            }, {
                "overrides": {}
            });
            var flxViewSegMore = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                "clipBounds": true,
                "id": "flxViewSegMore",
                "isVisible": false,
                "layoutType": kony.flex.FLOW_VERTICAL,
                "left": "170px",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "210px",
                "width": "250px",
                "zIndex": 5
            }, {}, {});
            flxViewSegMore.setDefaultUnit(kony.flex.DP);
            var flxViewUpArrowImage = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "12px",
                "id": "flxViewUpArrowImage",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0px",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0px",
                "width": "100%",
                "zIndex": 2
            }, {}, {});
            flxViewUpArrowImage.setDefaultUnit(kony.flex.DP);
            var imgViewUpArrow = new kony.ui.Image2({
                "centerX": "50%",
                "height": "12dp",
                "id": "imgViewUpArrow",
                "isVisible": true,
                "skin": "slImage",
                "src": "uparrow_2x.png",
                "top": "0dp",
                "width": "15dp",
                "zIndex": 1
            }, {
                "imageScaleMode": constants.IMAGE_SCALE_MODE_FIT_TO_DIMENSIONS,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxViewUpArrowImage.add(imgViewUpArrow);
            var flxViewBusinessTypesOuter = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                "clipBounds": true,
                "id": "flxViewBusinessTypesOuter",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "sknflxffffffop100dbdbe6Radius3px",
                "top": "-1dp",
                "width": "100%",
                "zIndex": 1
            }, {}, {});
            flxViewBusinessTypesOuter.setDefaultUnit(kony.flex.DP);
            var segViewBusinessTypes = new kony.ui.SegmentedUI2({
                "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                "bottom": 10,
                "data": [{
                    "lblName": "Granville Street, Vancover, BC, Canada"
                }, {
                    "lblName": "Granville Street, Vancover, BC, Canada"
                }, {
                    "lblName": "Granville Street, Vancover, BC, Canada"
                }],
                "groupCells": false,
                "id": "segViewBusinessTypes",
                "isVisible": true,
                "left": "15dp",
                "needPageIndicator": true,
                "pageOffDotImage": "pageoffdot.png",
                "pageOnDotImage": "pageondot.png",
                "retainSelection": false,
                "right": "15px",
                "rowFocusSkin": "seg2Focus",
                "rowSkin": "seg2Normal",
                "rowTemplate": "flxMore",
                "sectionHeaderSkin": "sliPhoneSegmentHeader",
                "selectionBehavior": constants.SEGUI_DEFAULT_BEHAVIOR,
                "separatorColor": "aaaaaa00",
                "separatorRequired": false,
                "separatorThickness": 0,
                "showScrollbars": false,
                "top": "0dp",
                "viewType": constants.SEGUI_VIEW_TYPE_TABLEVIEW,
                "widgetDataMap": {
                    "flxMore": "flxMore",
                    "lblName": "lblName"
                },
                "zIndex": 1
            }, {
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxViewBusinessTypesOuter.add(segViewBusinessTypes);
            flxViewSegMore.add(flxViewUpArrowImage, flxViewBusinessTypesOuter);
            flxViewRoles.add(flxGroupview, assignContextualMenu, flxViewSegMore);
            flxRightPannel.add(flxMainHeader, flxBreadCrumbs, flxToastMessage, flxNoGroups, flxAddGroups, flxGroupList, flxHeaderDropdown, flxViewRoles);
            var flxLoading = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "100%",
                "id": "flxLoading",
                "isVisible": false,
                "layoutType": kony.flex.FREE_FORM,
                "left": "305dp",
                "isModalContainer": false,
                "right": "0dp",
                "skin": "sknLoadingBlur",
                "top": "0dp",
                "zIndex": 10
            }, {}, {});
            flxLoading.setDefaultUnit(kony.flex.DP);
            var flxImageContainer = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "centerX": "50%",
                "centerY": "50%",
                "clipBounds": true,
                "height": "75px",
                "id": "flxImageContainer",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "isModalContainer": false,
                "skin": "slFbox",
                "width": "75px",
                "zIndex": 1
            }, {}, {});
            flxImageContainer.setDefaultUnit(kony.flex.DP);
            var imgLoading = new kony.ui.Image2({
                "centerX": "50%",
                "centerY": "50%",
                "height": "50px",
                "id": "imgLoading",
                "isVisible": true,
                "skin": "slImage",
                "src": "loadingscreenimage.gif",
                "width": "50px",
                "zIndex": 1
            }, {
                "imageScaleMode": constants.IMAGE_SCALE_MODE_MAINTAIN_ASPECT_RATIO,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxImageContainer.add(imgLoading);
            flxLoading.add(flxImageContainer);
            var flxDeactivateGroup = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "100%",
                "id": "flxDeactivateGroup",
                "isVisible": false,
                "layoutType": kony.flex.FREE_FORM,
                "masterType": constants.MASTER_TYPE_USERWIDGET,
                "isModalContainer": false,
                "skin": "slFbox",
                "width": "100%",
                "zIndex": 7
            }, {}, {});
            flxDeactivateGroup.setDefaultUnit(kony.flex.DP);
            var popUpDeactivate = new com.adminConsole.common.popUp1({
                "height": "100%",
                "id": "popUpDeactivate",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "masterType": constants.MASTER_TYPE_DEFAULT,
                "isModalContainer": false,
                "skin": "sknflxBg000000Op50",
                "top": "0dp",
                "width": "100%",
                "zIndex": 1,
                "overrides": {
                    "btnPopUpCancel": {
                        "i18n_text": "kony.i18n.getLocalizedString(\"i18n.PopUp.NoLeaveAsIS\")",
                        "right": "20px",
                        "width": kony.flex.USE_PREFFERED_SIZE
                    },
                    "btnPopUpDelete": {
                        "i18n_text": "kony.i18n.getLocalizedString(\"i18n.PopUp.YesDeactivate\")",
                        "minWidth": "viz.val_cleared"
                    },
                    "lblPopUpMainMessage": {
                        "text": "Deactivate Group"
                    },
                    "rtxPopUpDisclaimer": {
                        "bottom": "30dp",
                        "right": "viz.val_cleared",
                        "text": "Are you sure to deactivate the Group Auto Loan.\nThe Group has been assigned to few customers. Deactivating this may result in losing certain entitlements.",
                        "width": "93%"
                    }
                }
            }, {
                "overrides": {}
            }, {
                "overrides": {}
            });
            flxDeactivateGroup.add(popUpDeactivate);
            var flxErrorPopup = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "100%",
                "id": "flxErrorPopup",
                "isVisible": false,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "sknLoadingBlur",
                "top": "0dp",
                "width": "100%",
                "zIndex": 10
            }, {}, {});
            flxErrorPopup.setDefaultUnit(kony.flex.DP);
            var popUp = new com.adminConsole.common.popUp({
                "height": "100%",
                "id": "popUp",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "masterType": constants.MASTER_TYPE_DEFAULT,
                "isModalContainer": false,
                "skin": "sknflxBg000000Op50",
                "top": "0dp",
                "width": "100%",
                "zIndex": 1,
                "overrides": {
                    "btnPopUpCancel": {
                        "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmAssistedOnboarding.close\")",
                        "right": "20px"
                    },
                    "btnPopUpDelete": {
                        "i18n_text": "kony.i18n.getLocalizedString(\"i18n.PopUp.YesDeactivate\")",
                        "isVisible": false
                    }
                }
            }, {
                "overrides": {}
            }, {
                "overrides": {}
            });
            flxErrorPopup.add(popUp);
            flxMain.add(flxLeftPannel, flxRightPannel, flxLoading, flxDeactivateGroup, flxErrorPopup);
            var flxEditCancelConfirmation = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "100%",
                "id": "flxEditCancelConfirmation",
                "isVisible": false,
                "layoutType": kony.flex.FREE_FORM,
                "left": "10dp",
                "masterType": constants.MASTER_TYPE_USERWIDGET,
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "10dp",
                "width": "100%",
                "zIndex": 5
            }, {}, {});
            flxEditCancelConfirmation.setDefaultUnit(kony.flex.DP);
            var popUpCancelEdits = new com.adminConsole.common.popUpCancelEdits({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "height": "100%",
                "id": "popUpCancelEdits",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "masterType": constants.MASTER_TYPE_DEFAULT,
                "isModalContainer": false,
                "skin": "sknflxBg000000Op50",
                "top": "0dp",
                "width": "100%",
                "zIndex": 1,
                "overrides": {
                    "btnPopUpCancel": {
                        "i18n_text": "kony.i18n.getLocalizedString(\"i8n.navigation.cancelButton\")",
                        "right": "160px"
                    },
                    "btnPopUpDelete": {
                        "i18n_text": "kony.i18n.getLocalizedString(\"i8n.navigation.okButton\")"
                    },
                    "lblPopUpMainMessage": {
                        "i18n_text": "kony.i18n.getLocalizedString(\"i8n.navigation.cancelChanges\")"
                    },
                    "popUpCancelEdits": {
                        "bottom": "viz.val_cleared",
                        "centerX": "viz.val_cleared",
                        "centerY": "viz.val_cleared",
                        "height": "100%",
                        "left": "0dp",
                        "maxHeight": "viz.val_cleared",
                        "maxWidth": "viz.val_cleared",
                        "minHeight": "viz.val_cleared",
                        "minWidth": "viz.val_cleared",
                        "right": "viz.val_cleared",
                        "top": "0dp",
                        "width": "100%"
                    },
                    "rtxPopUpDisclaimer": {
                        "i18n_text": "kony.i18n.getLocalizedString(\"i8n.navigation.cancelChangesBody\")"
                    }
                }
            }, {
                "overrides": {}
            }, {
                "overrides": {}
            });
            flxEditCancelConfirmation.add(popUpCancelEdits);
            this.add(flxMain, flxEditCancelConfirmation);
        };
        return [{
            "addWidgets": addWidgetsfrmGroups,
            "enabledForIdleTimeout": true,
            "id": "frmGroups",
            "layoutType": kony.flex.FREE_FORM,
            "needAppMenu": false,
            "preShow": function(eventobject) {
                controller.AS_Form_ecc42658ad144373969e9f10f48dc500(eventobject);
            },
            "skin": "slForm"
        }, {
            "displayOrientation": constants.FORM_DISPLAY_ORIENTATION_PORTRAIT,
            "layoutType": kony.flex.FREE_FORM,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, {
            "onDeviceBack": controller.AS_Form_fee725e711d941f89d7ce01764bf2095,
            "retainScrollPosition": false
        }]
    }
});