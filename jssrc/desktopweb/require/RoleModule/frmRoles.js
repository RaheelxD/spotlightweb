define("RoleModule/frmRoles", function() {
    return function(controller) {
        function addWidgetsfrmRoles() {
            this.setDefaultUnit(kony.flex.DP);
            var flxMain = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "900dp",
                "id": "flxMain",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "slFbox0b7aaa2e3bfa340",
                "top": "0dp",
                "width": "100%",
                "zIndex": 1
            }, {}, {});
            flxMain.setDefaultUnit(kony.flex.DP);
            var flxLeftPannel = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "100%",
                "id": "flxLeftPannel",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "masterType": constants.MASTER_TYPE_USERWIDGET,
                "isModalContainer": false,
                "skin": "sknLeftPannel0bb79e4bcca8e45",
                "top": "0dp",
                "width": "305px",
                "zIndex": 5
            }, {}, {});
            flxLeftPannel.setDefaultUnit(kony.flex.DP);
            var leftMenuNew = new com.adminConsole.navigation.leftMenuNew({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "height": "100%",
                "id": "leftMenuNew",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "masterType": constants.MASTER_TYPE_DEFAULT,
                "isModalContainer": false,
                "skin": "sknFlxBg003E75Op100",
                "top": "0dp",
                "width": "100%",
                "overrides": {
                    "leftMenuNew": {
                        "bottom": "viz.val_cleared",
                        "centerX": "viz.val_cleared",
                        "centerY": "viz.val_cleared",
                        "height": "100%",
                        "left": "0dp",
                        "maxHeight": "viz.val_cleared",
                        "maxWidth": "viz.val_cleared",
                        "minHeight": "viz.val_cleared",
                        "minWidth": "viz.val_cleared",
                        "right": "viz.val_cleared",
                        "top": "0dp",
                        "width": "100%"
                    }
                }
            }, {
                "overrides": {}
            }, {
                "overrides": {}
            });
            flxLeftPannel.add(leftMenuNew);
            var flxRightPanel = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "100%",
                "id": "flxRightPanel",
                "isVisible": true,
                "layoutType": kony.flex.FLOW_VERTICAL,
                "left": "305px",
                "masterType": constants.MASTER_TYPE_USERWIDGET,
                "isModalContainer": false,
                "right": 0,
                "skin": "slFbox0b7aaa2e3bfa340",
                "top": "0.00%",
                "zIndex": 1
            }, {}, {});
            flxRightPanel.setDefaultUnit(kony.flex.DP);
            var flxMainHeader = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "106dp",
                "id": "flxMainHeader",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0dp",
                "width": "100%",
                "zIndex": 100
            }, {}, {});
            flxMainHeader.setDefaultUnit(kony.flex.DP);
            var mainHeader = new com.adminConsole.header.mainHeader({
                "height": "106px",
                "id": "mainHeader",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": 0,
                "masterType": constants.MASTER_TYPE_DEFAULT,
                "isModalContainer": false,
                "skin": "CopyslFbox1",
                "top": 0,
                "width": "100%",
                "overrides": {
                    "btnDropdownList": {
                        "i18n_text": "kony.i18n.getLocalizedString(\"i18n.mainHeader.DOWNLOADLIST\")"
                    },
                    "lblHeading": {
                        "i18n_text": "kony.i18n.getLocalizedString(\"i18n.leftmenu.roles\")"
                    }
                }
            }, {
                "overrides": {}
            }, {
                "overrides": {}
            });
            flxMainHeader.add(mainHeader);
            var flxMainSubHeader = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "63dp",
                "id": "flxMainSubHeader",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0dp",
                "width": "100%",
                "zIndex": 100
            }, {}, {});
            flxMainSubHeader.setDefaultUnit(kony.flex.DP);
            var subHeader = new com.adminConsole.header.subHeader({
                "height": "48dp",
                "id": "subHeader",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "masterType": constants.MASTER_TYPE_DEFAULT,
                "isModalContainer": false,
                "skin": "CopyslFbox2",
                "top": "0dp",
                "width": "100%",
                "overrides": {
                    "flxMenu": {
                        "isVisible": false
                    }
                }
            }, {
                "overrides": {}
            }, {
                "overrides": {}
            });
            flxMainSubHeader.add(subHeader);
            var flxScrollMainContent = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "bottom": "0dp",
                "clipBounds": false,
                "height": "82.50%",
                "id": "flxScrollMainContent",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "isModalContainer": false,
                "right": "0px",
                "top": "-10dp",
                "zIndex": 102
            }, {}, {});
            flxScrollMainContent.setDefaultUnit(kony.flex.DP);
            var flxPermissions = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": false,
                "height": "100%",
                "id": "flxPermissions",
                "isVisible": false,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0px",
                "isModalContainer": false,
                "right": "0px",
                "skin": "slFbox",
                "top": "0px",
                "zIndex": 1
            }, {}, {});
            flxPermissions.setDefaultUnit(kony.flex.DP);
            var flxPermissionsContainer = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": false,
                "height": "100%",
                "id": "flxPermissionsContainer",
                "isVisible": true,
                "layoutType": kony.flex.FLOW_VERTICAL,
                "left": "35dp",
                "isModalContainer": false,
                "right": 35,
                "skin": "sknflxffffffBorderd6dbe7Radius4px",
                "top": "0dp",
                "zIndex": 1
            }, {}, {});
            flxPermissionsContainer.setDefaultUnit(kony.flex.DP);
            var flxSegmentPermmissions = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": false,
                "height": "100%",
                "id": "flxSegmentPermmissions",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0%",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0px",
                "width": "100%",
                "zIndex": 1
            }, {}, {});
            flxSegmentPermmissions.setDefaultUnit(kony.flex.DP);
            var flxRolesHeader = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "60px",
                "id": "flxRolesHeader",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "20px",
                "isModalContainer": false,
                "right": "20px",
                "skin": "sknflxffffffop100",
                "top": "0",
                "zIndex": 11
            }, {}, {});
            flxRolesHeader.setDefaultUnit(kony.flex.DP);
            var flxRoleHeaderName = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "100%",
                "id": "flxRoleHeaderName",
                "isVisible": true,
                "layoutType": kony.flex.FLOW_HORIZONTAL,
                "left": "10px",
                "isModalContainer": false,
                "onClick": controller.AS_FlexContainer_a43b80cc872f4831807a918bd48f0ac3,
                "skin": "slFbox",
                "top": "0dp",
                "width": "13.85%",
                "zIndex": 1
            }, {}, {});
            flxRoleHeaderName.setDefaultUnit(kony.flex.DP);
            var lblRoleHeaderName = new kony.ui.Label({
                "centerY": "50%",
                "id": "lblRoleHeaderName",
                "isVisible": true,
                "left": "0px",
                "skin": "sknlblLato696c7312px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.permission.NAME\")",
                "width": "40px",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {
                "hoverSkin": "sknlblLatoBold00000012px"
            });
            var fontIconSortName = new kony.ui.Label({
                "centerY": "50%",
                "id": "fontIconSortName",
                "isVisible": true,
                "left": "10dp",
                "skin": "sknIcon15px",
                "text": "",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {
                "hoverSkin": "sknlblCursorFont"
            });
            flxRoleHeaderName.add(lblRoleHeaderName, fontIconSortName);
            var flxRoleHeaderDescription = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "100%",
                "id": "flxRoleHeaderDescription",
                "isVisible": true,
                "layoutType": kony.flex.FLOW_HORIZONTAL,
                "left": "18%",
                "isModalContainer": false,
                "onClick": controller.AS_FlexContainer_f184888da9f042978e3b0332711e0319,
                "skin": "slFbox",
                "top": "0dp",
                "width": "25%",
                "zIndex": 1
            }, {}, {});
            flxRoleHeaderDescription.setDefaultUnit(kony.flex.DP);
            var lblRoleHeaderDescription = new kony.ui.Label({
                "centerY": "50%",
                "id": "lblRoleHeaderDescription",
                "isVisible": true,
                "left": "0px",
                "skin": "sknlblLato696c7312px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.permission.DESCRIPTION\")",
                "width": "90px",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxRoleHeaderDescription.add(lblRoleHeaderDescription);
            var flxRoleHeaderUsers = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "100%",
                "id": "flxRoleHeaderUsers",
                "isVisible": true,
                "layoutType": kony.flex.FLOW_HORIZONTAL,
                "left": "61%",
                "isModalContainer": false,
                "onClick": controller.AS_FlexContainer_if8688b9f1b14c0796ccfa73761ffbf7,
                "skin": "slFbox",
                "top": "0dp",
                "width": "8%",
                "zIndex": 1
            }, {}, {});
            flxRoleHeaderUsers.setDefaultUnit(kony.flex.DP);
            var lblRoleHeaderUsers = new kony.ui.Label({
                "centerY": "50%",
                "id": "lblRoleHeaderUsers",
                "isVisible": true,
                "left": "0px",
                "skin": "sknlblLato696c7312px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.roles.USERS\")",
                "width": "40px",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {
                "hoverSkin": "sknlblLatoBold00000012px"
            });
            var fontIconSortUser = new kony.ui.Label({
                "centerY": "50%",
                "id": "fontIconSortUser",
                "isVisible": true,
                "left": "10dp",
                "skin": "sknIcon15px",
                "text": "",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {
                "hoverSkin": "sknlblCursorFont"
            });
            flxRoleHeaderUsers.add(lblRoleHeaderUsers, fontIconSortUser);
            var flxRoleHeaderPermissions = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "100%",
                "id": "flxRoleHeaderPermissions",
                "isVisible": true,
                "layoutType": kony.flex.FLOW_HORIZONTAL,
                "left": "71%",
                "isModalContainer": false,
                "onClick": controller.AS_FlexContainer_e8c18fc5a3a34ece96d2cc198ff5d25a,
                "skin": "slFbox",
                "top": "0dp",
                "width": "13%",
                "zIndex": 1
            }, {}, {});
            flxRoleHeaderPermissions.setDefaultUnit(kony.flex.DP);
            var lblRoleHeaderPermissions = new kony.ui.Label({
                "centerY": "50%",
                "id": "lblRoleHeaderPermissions",
                "isVisible": true,
                "left": "0px",
                "skin": "sknlblLato696c7312px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.roles.PERMISSIONS\")",
                "width": "80px",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {
                "hoverSkin": "sknlblLatoBold00000012px"
            });
            var fontIconSortPermissions = new kony.ui.Label({
                "centerY": "50%",
                "id": "fontIconSortPermissions",
                "isVisible": true,
                "left": "10dp",
                "skin": "sknIcon15px",
                "text": "",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {
                "hoverSkin": "sknlblCursorFont"
            });
            flxRoleHeaderPermissions.add(lblRoleHeaderPermissions, fontIconSortPermissions);
            var flxRoleHeaderStatus = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "100%",
                "id": "flxRoleHeaderStatus",
                "isVisible": true,
                "layoutType": kony.flex.FLOW_HORIZONTAL,
                "left": "86%",
                "isModalContainer": false,
                "onClick": controller.AS_FlexContainer_ia2738f1fdda45cf914f716c251ff157,
                "skin": "slFbox",
                "top": "0dp",
                "width": "8%",
                "zIndex": 1
            }, {}, {});
            flxRoleHeaderStatus.setDefaultUnit(kony.flex.DP);
            var lblRoleHeaderStatus = new kony.ui.Label({
                "centerY": "50%",
                "id": "lblRoleHeaderStatus",
                "isVisible": true,
                "left": "0px",
                "skin": "sknlblLato696c7312px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.roles.STATUS\")",
                "width": "45px",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {
                "hoverSkin": "sknlblLatoBold00000012px"
            });
            var fontIconFilterStatus = new kony.ui.Label({
                "centerY": "50%",
                "id": "fontIconFilterStatus",
                "isVisible": true,
                "left": "10dp",
                "skin": "sknIcon15px",
                "text": "",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {
                "hoverSkin": "sknlblCursorFont"
            });
            flxRoleHeaderStatus.add(lblRoleHeaderStatus, fontIconFilterStatus);
            var lblRoleHeaderSeperator = new kony.ui.Label({
                "bottom": 0,
                "height": "1px",
                "id": "lblRoleHeaderSeperator",
                "isVisible": true,
                "left": "0dp",
                "skin": "sknLblTableHeaderLine",
                "text": ".",
                "width": "100%",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxRolesHeader.add(flxRoleHeaderName, flxRoleHeaderDescription, flxRoleHeaderUsers, flxRoleHeaderPermissions, flxRoleHeaderStatus, lblRoleHeaderSeperator);
            var flxRoleStatusFilter = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                "clipBounds": true,
                "id": "flxRoleStatusFilter",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "isModalContainer": false,
                "right": "20dp",
                "skin": "slFbox",
                "top": "40dp",
                "width": "100dp",
                "zIndex": 11
            }, {}, {});
            flxRoleStatusFilter.setDefaultUnit(kony.flex.DP);
            var statusFilterMenu = new com.adminConsole.common.statusFilterMenu({
                "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                "id": "statusFilterMenu",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "masterType": constants.MASTER_TYPE_DEFAULT,
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "5dp",
                "width": "100%",
                "zIndex": 5,
                "overrides": {
                    "imgUpArrow": {
                        "src": "uparrow_2x.png"
                    }
                }
            }, {
                "overrides": {}
            }, {
                "overrides": {}
            });
            flxRoleStatusFilter.add(statusFilterMenu);
            var flxSegRoles = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "200dp",
                "id": "flxSegRoles",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "60dp",
                "width": "100%"
            }, {}, {});
            flxSegRoles.setDefaultUnit(kony.flex.DP);
            var segPermissions = new kony.ui.SegmentedUI2({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "data": [{
                    "fontIconStatusImg": "Label",
                    "lblDescription": "",
                    "lblIconOptions": "",
                    "lblNoOfRoles": "",
                    "lblNoOfUsers": "",
                    "lblPermissionName": "",
                    "lblPermissionStatus": "",
                    "lblSeperator": ""
                }],
                "groupCells": false,
                "height": "100%",
                "id": "segPermissions",
                "isVisible": true,
                "left": "0px",
                "needPageIndicator": true,
                "onRowClick": controller.AS_Segment_g16288b28c3b4baf849b11cb93de6e1c,
                "pageOffDotImage": "pageoffdot.png",
                "pageOnDotImage": "pageondot.png",
                "retainSelection": false,
                "right": "0px",
                "rowFocusSkin": "seg2Focus",
                "rowSkin": "sknseg3pxRadius",
                "rowTemplate": "flxPermissions",
                "sectionHeaderSkin": "sliPhoneSegmentHeader",
                "selectionBehavior": constants.SEGUI_DEFAULT_BEHAVIOR,
                "separatorRequired": false,
                "showScrollbars": true,
                "top": "0dp",
                "viewType": constants.SEGUI_VIEW_TYPE_TABLEVIEW,
                "widgetDataMap": {
                    "flxOptions": "flxOptions",
                    "flxPermissions": "flxPermissions",
                    "flxPermissionsContainer": "flxPermissionsContainer",
                    "flxStatus": "flxStatus",
                    "fontIconStatusImg": "fontIconStatusImg",
                    "lblDescription": "lblDescription",
                    "lblIconOptions": "lblIconOptions",
                    "lblNoOfRoles": "lblNoOfRoles",
                    "lblNoOfUsers": "lblNoOfUsers",
                    "lblPermissionName": "lblPermissionName",
                    "lblPermissionStatus": "lblPermissionStatus",
                    "lblSeperator": "lblSeperator"
                },
                "zIndex": 1
            }, {
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxSegRoles.add(segPermissions);
            var flxNoResultFound = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                "clipBounds": true,
                "id": "flxNoResultFound",
                "isVisible": false,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "60dp",
                "width": "100%",
                "zIndex": 1
            }, {}, {});
            flxNoResultFound.setDefaultUnit(kony.flex.DP);
            var rtxSearchMesg = new kony.ui.RichText({
                "bottom": "50px",
                "centerX": "52%",
                "id": "rtxSearchMesg",
                "isVisible": true,
                "left": "0",
                "skin": "sknRtxLato84939e12Px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmCSR.richtextNoResult\")",
                "top": "90px",
                "width": "80%",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_CENTER,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxNoResultFound.add(rtxSearchMesg);
            flxSegmentPermmissions.add(flxRolesHeader, flxRoleStatusFilter, flxSegRoles, flxNoResultFound);
            flxPermissionsContainer.add(flxSegmentPermmissions);
            var flxSelectOptions = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                "clipBounds": true,
                "id": "flxSelectOptions",
                "isVisible": true,
                "layoutType": kony.flex.FLOW_VERTICAL,
                "isModalContainer": false,
                "right": "55px",
                "skin": "slFbox",
                "top": "100dp",
                "width": "200px",
                "zIndex": 100
            }, {}, {});
            flxSelectOptions.setDefaultUnit(kony.flex.DP);
            var flxTopArrowImage = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "12dp",
                "id": "flxTopArrowImage",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0dp",
                "width": "100%",
                "zIndex": 101
            }, {}, {});
            flxTopArrowImage.setDefaultUnit(kony.flex.DP);
            var imgUpArrow = new kony.ui.Image2({
                "height": "12dp",
                "id": "imgUpArrow",
                "isVisible": true,
                "right": "15dp",
                "skin": "slImage",
                "src": "uparrow_2x.png",
                "top": "0dp",
                "width": "15dp",
                "zIndex": 1
            }, {
                "imageScaleMode": constants.IMAGE_SCALE_MODE_FIT_TO_DIMENSIONS,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxTopArrowImage.add(imgUpArrow);
            var flxMenuOptions = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                "clipBounds": false,
                "id": "flxMenuOptions",
                "isVisible": true,
                "layoutType": kony.flex.FLOW_VERTICAL,
                "isModalContainer": false,
                "right": "0px",
                "skin": "sknflxffffffop100dbdbe6Radius3px",
                "top": "-1px",
                "width": "200px",
                "zIndex": 100
            }, {}, {});
            flxMenuOptions.setDefaultUnit(kony.flex.DP);
            var lblDescription = new kony.ui.Label({
                "id": "lblDescription",
                "isVisible": true,
                "left": "15px",
                "skin": "sknlblLatoBold9ca9ba14px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.roles.ASSIGN\")",
                "top": "20dp",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var btnRoles = new kony.ui.Button({
                "id": "btnRoles",
                "isVisible": true,
                "left": "16dp",
                "skin": "contextuallinks",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmPermissions.btnRoles\")",
                "top": "20dp",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_CENTER,
                "displayText": true,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var btnPermissions = new kony.ui.Button({
                "bottom": "15dp",
                "id": "btnPermissions",
                "isVisible": true,
                "left": "16dp",
                "skin": "contextuallinks",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.users.Permissions\")",
                "top": "15dp",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_CENTER,
                "displayText": true,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var flxSeperator = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "centerX": "50%",
                "clipBounds": true,
                "height": "1px",
                "id": "flxSeperator",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "isModalContainer": false,
                "skin": "sknflxd6dbe7",
                "top": "0px",
                "width": "100%",
                "zIndex": 1
            }, {}, {});
            flxSeperator.setDefaultUnit(kony.flex.DP);
            flxSeperator.add();
            var flxOption1 = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "35dp",
                "id": "flxOption1",
                "isVisible": false,
                "layoutType": kony.flex.FLOW_HORIZONTAL,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "CopyslFbox0fbaa097cce2f4c",
                "top": "0dp",
                "width": "100%",
                "zIndex": 1
            }, {}, {
                "hoverSkin": "sknContextualMenuEntryHover"
            });
            flxOption1.setDefaultUnit(kony.flex.DP);
            var imgOption1 = new kony.ui.Image2({
                "centerY": "50%",
                "height": "15dp",
                "id": "imgOption1",
                "isVisible": true,
                "left": "15dp",
                "skin": "slImage",
                "src": "imagedrag.png",
                "width": "15dp",
                "zIndex": 1
            }, {
                "imageScaleMode": constants.IMAGE_SCALE_MODE_MAINTAIN_ASPECT_RATIO,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var lblOption1 = new kony.ui.Label({
                "centerY": "50%",
                "id": "lblOption1",
                "isVisible": true,
                "left": "15dp",
                "skin": "sknlblLatoBold35475f14px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.permission.View\")",
                "top": "4dp",
                "width": "40px",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxOption1.add(imgOption1, lblOption1);
            var flxOption2 = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "35dp",
                "id": "flxOption2",
                "isVisible": true,
                "layoutType": kony.flex.FLOW_HORIZONTAL,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "CopyslFbox0f1f13dff5f2b4e",
                "top": "7dp",
                "width": "100%",
                "zIndex": 1
            }, {}, {
                "hoverSkin": "sknContextualMenuEntryHover"
            });
            flxOption2.setDefaultUnit(kony.flex.DP);
            var fontIconOptionEdit = new kony.ui.Label({
                "centerY": "50%",
                "id": "fontIconOptionEdit",
                "isVisible": true,
                "left": "15dp",
                "skin": "sknFontIconOptionMenuRow",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmFAQ.fonticonEdit\")",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var lblOption2 = new kony.ui.Label({
                "centerY": "50%",
                "id": "lblOption2",
                "isVisible": true,
                "left": "10dp",
                "skin": "sknlblLatoRegular484b5213px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.roles.Edit\")",
                "top": "4dp",
                "width": "40px",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxOption2.add(fontIconOptionEdit, lblOption2);
            var flxOption3 = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "35dp",
                "id": "flxOption3",
                "isVisible": false,
                "layoutType": kony.flex.FLOW_HORIZONTAL,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "CopyslFbox0jdc50fa722ab4d",
                "top": "0dp",
                "width": "100%",
                "zIndex": 1
            }, {}, {
                "hoverSkin": "sknContextualMenuEntryHover"
            });
            flxOption3.setDefaultUnit(kony.flex.DP);
            var fontIconSuspend = new kony.ui.Label({
                "centerY": "50%",
                "id": "fontIconSuspend",
                "isVisible": true,
                "left": "15dp",
                "skin": "sknFontIconOptionMenuRow",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmPermissions.fontIconOption3\")",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var lblOption3 = new kony.ui.Label({
                "centerY": "50%",
                "id": "lblOption3",
                "isVisible": true,
                "left": "10dp",
                "skin": "sknlblLatoBold35475f14px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.permission.Suspend\")",
                "top": "4dp",
                "width": "60px",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxOption3.add(fontIconSuspend, lblOption3);
            var flxOption4 = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "bottom": "10dp",
                "clipBounds": true,
                "height": "35dp",
                "id": "flxOption4",
                "isVisible": true,
                "layoutType": kony.flex.FLOW_HORIZONTAL,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "CopyslFbox0bd5fb7d842504a",
                "top": "0dp",
                "width": "100%",
                "zIndex": 1
            }, {}, {
                "hoverSkin": "sknContextualMenuEntryHover"
            });
            flxOption4.setDefaultUnit(kony.flex.DP);
            var fontIconDeactivate = new kony.ui.Label({
                "centerY": "50%",
                "id": "fontIconDeactivate",
                "isVisible": true,
                "left": "15dp",
                "skin": "sknFontIconOptionMenuRow",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmFAQ.fonticonDeactive\")",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var lblOption4 = new kony.ui.Label({
                "centerY": "50%",
                "id": "lblOption4",
                "isVisible": true,
                "left": "10dp",
                "skin": "sknlblLatoRegular484b5213px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.roles.Deactivate\")",
                "top": "4dp",
                "width": "70px",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxOption4.add(fontIconDeactivate, lblOption4);
            flxMenuOptions.add(lblDescription, btnRoles, btnPermissions, flxSeperator, flxOption1, flxOption2, flxOption3, flxOption4);
            var flxDownArrowImage = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "12dp",
                "id": "flxDownArrowImage",
                "isVisible": false,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "-1dp",
                "width": "100%",
                "zIndex": 101
            }, {}, {});
            flxDownArrowImage.setDefaultUnit(kony.flex.DP);
            var imgDownArrow = new kony.ui.Image2({
                "height": "12dp",
                "id": "imgDownArrow",
                "isVisible": true,
                "right": "15dp",
                "skin": "slImage",
                "src": "downarrow_2x.png",
                "top": "0dp",
                "width": "15dp",
                "zIndex": 1
            }, {
                "imageScaleMode": constants.IMAGE_SCALE_MODE_FIT_TO_DIMENSIONS,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxDownArrowImage.add(imgDownArrow);
            flxSelectOptions.add(flxTopArrowImage, flxMenuOptions, flxDownArrowImage);
            flxPermissions.add(flxPermissionsContainer, flxSelectOptions);
            var flxViews = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                "clipBounds": true,
                "id": "flxViews",
                "isVisible": false,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "masterType": constants.MASTER_TYPE_USERWIDGET,
                "isModalContainer": false,
                "skin": "sknflxf5f6f8Op100",
                "top": "0dp",
                "width": "100%",
                "zIndex": 1
            }, {}, {});
            flxViews.setDefaultUnit(kony.flex.DP);
            var flxRolesBreadCrumb = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "20px",
                "id": "flxRolesBreadCrumb",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0px",
                "isModalContainer": false,
                "right": "0px",
                "skin": "slFbox",
                "top": "0px",
                "width": "100%",
                "zIndex": 1
            }, {}, {});
            flxRolesBreadCrumb.setDefaultUnit(kony.flex.DP);
            var breadcrumbs = new com.adminConsole.common.breadcrumbs({
                "bottom": "0px",
                "height": "20px",
                "id": "breadcrumbs",
                "isVisible": true,
                "layoutType": kony.flex.FLOW_HORIZONTAL,
                "left": "35px",
                "masterType": constants.MASTER_TYPE_DEFAULT,
                "isModalContainer": false,
                "right": "35px",
                "skin": "sknflxf5f6f8Op100",
                "top": "0px",
                "zIndex": 1,
                "overrides": {
                    "breadcrumbs": {
                        "bottom": "0px",
                        "left": "35px",
                        "right": "35px",
                        "top": "0px",
                        "width": "viz.val_cleared",
                        "zIndex": 1
                    },
                    "btnBackToMain": {
                        "i18n_text": "kony.i18n.getLocalizedString(\"i18n.permission.ROLES\")",
                        "left": "0px"
                    },
                    "btnPreviousPage": {
                        "isVisible": true
                    },
                    "imgBreadcrumbsDown": {
                        "src": "img_down_arrow.png"
                    },
                    "imgBreadcrumbsRight": {
                        "isVisible": false,
                        "src": "img_breadcrumb_arrow.png"
                    },
                    "imgBreadcrumbsRight2": {
                        "isVisible": false,
                        "src": "img_breadcrumb_arrow.png"
                    },
                    "imgBreadcrumbsRight3": {
                        "src": "img_breadcrumb_arrow.png"
                    },
                    "lblCurrentScreen": {
                        "isVisible": true,
                        "width": kony.flex.USE_PREFFERED_SIZE
                    }
                }
            }, {
                "overrides": {}
            }, {
                "overrides": {}
            });
            flxRolesBreadCrumb.add(breadcrumbs);
            var flxAddMainContainer = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "480dp",
                "id": "flxAddMainContainer",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "35dp",
                "masterType": constants.MASTER_TYPE_USERWIDGET,
                "isModalContainer": false,
                "right": 35,
                "skin": "sknflxf5f6f8Op100BorderE1E5Ed",
                "top": "33px",
                "zIndex": 1
            }, {}, {});
            flxAddMainContainer.setDefaultUnit(kony.flex.DP);
            var flxOptions = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "100%",
                "id": "flxOptions",
                "isVisible": true,
                "layoutType": kony.flex.FLOW_VERTICAL,
                "left": "0px",
                "isModalContainer": false,
                "skin": "sknflxf9f9f9NoBorder",
                "top": "0%",
                "width": "230px",
                "zIndex": 1
            }, {}, {});
            flxOptions.setDefaultUnit(kony.flex.DP);
            var flxOptionDetails = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                "clipBounds": true,
                "id": "flxOptionDetails",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0dp",
                "width": "100%",
                "zIndex": 1
            }, {}, {});
            flxOptionDetails.setDefaultUnit(kony.flex.DP);
            var btnOptionDetails = new kony.ui.Button({
                "id": "btnOptionDetails",
                "isVisible": true,
                "left": "20px",
                "skin": "Btna1acbafont12pxKA",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.roles.ROLEDETAILS\")",
                "top": "20px",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_CENTER,
                "displayText": true,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var flxoptionSeperator1 = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "bottom": 0,
                "clipBounds": true,
                "height": "1px",
                "id": "flxoptionSeperator1",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "20px",
                "isModalContainer": false,
                "right": "20px",
                "skin": "sknflxe1e5edop100",
                "top": "48px",
                "zIndex": 1
            }, {}, {});
            flxoptionSeperator1.setDefaultUnit(kony.flex.DP);
            flxoptionSeperator1.add();
            var fontIconImgSelected1 = new kony.ui.Label({
                "id": "fontIconImgSelected1",
                "isVisible": true,
                "right": "20dp",
                "skin": "sknFontIconRightArrow16px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmPermissions.fontIconRightArrow1\")",
                "top": "20px",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxOptionDetails.add(btnOptionDetails, flxoptionSeperator1, fontIconImgSelected1);
            var flxAddPermissions = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                "clipBounds": true,
                "id": "flxAddPermissions",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0dp",
                "width": "100%",
                "zIndex": 1
            }, {}, {});
            flxAddPermissions.setDefaultUnit(kony.flex.DP);
            var lblIconDropPermissions = new kony.ui.Label({
                "id": "lblIconDropPermissions",
                "isVisible": true,
                "left": "20dp",
                "skin": "sknIcon12pxBlack",
                "text": "",
                "top": "17dp",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var btnAddPermissions = new kony.ui.Button({
                "id": "btnAddPermissions",
                "isVisible": true,
                "left": "45dp",
                "skin": "Btna1acbafont12pxKA",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.roles.ASSIGNPERMISSIONS\")",
                "top": "15dp",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_CENTER,
                "displayText": true,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var fontIconImgSelected2 = new kony.ui.Label({
                "id": "fontIconImgSelected2",
                "isVisible": false,
                "right": "20px",
                "skin": "sknFontIconRightArrow16px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmPermissions.fontIconRightArrow1\")",
                "top": "15dp",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var flxSubPermissions = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                "clipBounds": true,
                "id": "flxSubPermissions",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "55dp",
                "isModalContainer": false,
                "right": "20px",
                "skin": "slFbox",
                "top": "43dp",
                "zIndex": 1
            }, {}, {});
            flxSubPermissions.setDefaultUnit(kony.flex.DP);
            var flxAddCustomerAccess = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                "clipBounds": true,
                "id": "flxAddCustomerAccess",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0dp",
                "width": "100%",
                "zIndex": 1
            }, {}, {});
            flxAddCustomerAccess.setDefaultUnit(kony.flex.DP);
            var btnAddCustomerAccess = new kony.ui.Button({
                "id": "btnAddCustomerAccess",
                "isVisible": true,
                "left": "0px",
                "skin": "Btna1acbafont12pxKA",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.roles.CustomerAccess_LC\")",
                "top": "0dp",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_CENTER,
                "displayText": true,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var lblOptionalSubPermission1 = new kony.ui.Label({
                "id": "lblOptionalSubPermission1",
                "isVisible": true,
                "left": "0px",
                "skin": "slLabel0d4f692dab05249",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.permission.Optional\")",
                "top": "22px",
                "width": "93px",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var lblIconCustomerAccessSelected = new kony.ui.Label({
                "id": "lblIconCustomerAccessSelected",
                "isVisible": true,
                "right": "0dp",
                "skin": "sknFontIconRightArrow16px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmPermissions.fontIconRightArrow1\")",
                "top": "0dp",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var flxLineCustomerAccess = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "centerX": "50%",
                "clipBounds": true,
                "height": "1px",
                "id": "flxLineCustomerAccess",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "isModalContainer": false,
                "right": "0dp",
                "skin": "sknflxe1e5edop100",
                "top": "44dp",
                "width": "100%",
                "zIndex": 1
            }, {}, {});
            flxLineCustomerAccess.setDefaultUnit(kony.flex.DP);
            flxLineCustomerAccess.add();
            flxAddCustomerAccess.add(btnAddCustomerAccess, lblOptionalSubPermission1, lblIconCustomerAccessSelected, flxLineCustomerAccess);
            var flxAddSystemPermission = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                "bottom": "20px",
                "clipBounds": true,
                "id": "flxAddSystemPermission",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "53dp",
                "width": "100%",
                "zIndex": 1
            }, {}, {});
            flxAddSystemPermission.setDefaultUnit(kony.flex.DP);
            var btnAddSysPermissions = new kony.ui.Button({
                "id": "btnAddSysPermissions",
                "isVisible": true,
                "left": "0px",
                "skin": "Btna1acbafont12pxKA",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.roles.SystemPermissions_LC\")",
                "top": "0dp",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_CENTER,
                "displayText": true,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var lblIconSysPermisionsSelected = new kony.ui.Label({
                "id": "lblIconSysPermisionsSelected",
                "isVisible": true,
                "right": "0dp",
                "skin": "sknFontIconRightArrow16px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmPermissions.fontIconRightArrow1\")",
                "top": "0dp",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var flxLineSubPermissions = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "bottom": 0,
                "centerX": "50%",
                "clipBounds": true,
                "height": "1px",
                "id": "flxLineSubPermissions",
                "isVisible": false,
                "layoutType": kony.flex.FREE_FORM,
                "isModalContainer": false,
                "skin": "sknflxe1e5edop100",
                "top": "20px",
                "width": "100%",
                "zIndex": 1
            }, {}, {});
            flxLineSubPermissions.setDefaultUnit(kony.flex.DP);
            flxLineSubPermissions.add();
            flxAddSystemPermission.add(btnAddSysPermissions, lblIconSysPermisionsSelected, flxLineSubPermissions);
            flxSubPermissions.add(flxAddCustomerAccess, flxAddSystemPermission);
            var flxAddSeperator2 = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "bottom": 0,
                "centerX": "50%",
                "clipBounds": true,
                "height": "1px",
                "id": "flxAddSeperator2",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "20dp",
                "isModalContainer": false,
                "right": "20dp",
                "skin": "sknflxe1e5edop100",
                "zIndex": 1
            }, {}, {});
            flxAddSeperator2.setDefaultUnit(kony.flex.DP);
            flxAddSeperator2.add();
            flxAddPermissions.add(lblIconDropPermissions, btnAddPermissions, fontIconImgSelected2, flxSubPermissions, flxAddSeperator2);
            var flxAddRoles = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "85dp",
                "id": "flxAddRoles",
                "isVisible": false,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0dp",
                "width": "100%",
                "zIndex": 1
            }, {}, {});
            flxAddRoles.setDefaultUnit(kony.flex.DP);
            var btnAddRoles = new kony.ui.Button({
                "id": "btnAddRoles",
                "isVisible": true,
                "left": "25px",
                "skin": "Btna1acbafont12pxKA",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmPermissionsController.ADD_ROLES\")",
                "top": "30dp",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_CENTER,
                "displayText": true,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var lblAddOptional3 = new kony.ui.Label({
                "id": "lblAddOptional3",
                "isVisible": true,
                "left": "30px",
                "skin": "slLabel0d4f692dab05249",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.permission.Optional\")",
                "top": "50px",
                "width": "93px",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var flxAddSeperator4 = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "bottom": 0,
                "centerX": "50%",
                "clipBounds": true,
                "height": "2px",
                "id": "flxAddSeperator4",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "isModalContainer": false,
                "skin": "sknflxe1e5edop100",
                "width": "72.09%",
                "zIndex": 1
            }, {}, {});
            flxAddSeperator4.setDefaultUnit(kony.flex.DP);
            flxAddSeperator4.add();
            var fontIconImgSelected4 = new kony.ui.Label({
                "id": "fontIconImgSelected4",
                "isVisible": true,
                "left": "180dp",
                "skin": "sknFontIconRightArrow16px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmPermissions.fontIconRightArrow1\")",
                "top": "30dp",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxAddRoles.add(btnAddRoles, lblAddOptional3, flxAddSeperator4, fontIconImgSelected4);
            var flxAddUsers = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "85dp",
                "id": "flxAddUsers",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0dp",
                "width": "100%",
                "zIndex": 1
            }, {}, {});
            flxAddUsers.setDefaultUnit(kony.flex.DP);
            var btnAddUsers = new kony.ui.Button({
                "id": "btnAddUsers",
                "isVisible": true,
                "left": "20px",
                "skin": "Btna1acbafont12pxKA",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.permission.ASSIGNUSERS\")",
                "top": "15dp",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_CENTER,
                "displayText": true,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var lblAddOptional2 = new kony.ui.Label({
                "id": "lblAddOptional2",
                "isVisible": true,
                "left": "20px",
                "skin": "slLabel0d4f692dab05249",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.permission.Optional\")",
                "top": "37px",
                "width": "93px",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var flxAddSeperator3 = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "bottom": 0,
                "centerX": "50%",
                "clipBounds": true,
                "height": "1px",
                "id": "flxAddSeperator3",
                "isVisible": false,
                "layoutType": kony.flex.FREE_FORM,
                "left": "20dp",
                "isModalContainer": false,
                "right": "20dp",
                "skin": "sknflxe1e5edop100",
                "zIndex": 1
            }, {}, {});
            flxAddSeperator3.setDefaultUnit(kony.flex.DP);
            flxAddSeperator3.add();
            var fontIconImgSelected3 = new kony.ui.Label({
                "id": "fontIconImgSelected3",
                "isVisible": true,
                "right": "20dp",
                "skin": "sknFontIconRightArrow16px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmPermissions.fontIconRightArrow1\")",
                "top": "15dp",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxAddUsers.add(btnAddUsers, lblAddOptional2, flxAddSeperator3, fontIconImgSelected3);
            flxOptions.add(flxOptionDetails, flxAddPermissions, flxAddRoles, flxAddUsers);
            var flxAddOptionsSection = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "100%",
                "id": "flxAddOptionsSection",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "230px",
                "isModalContainer": false,
                "right": "0px",
                "skin": "sknflxffffffop100",
                "top": "0px",
                "zIndex": 1
            }, {}, {});
            flxAddOptionsSection.setDefaultUnit(kony.flex.DP);
            var lblAddOptionsHeading = new kony.ui.Label({
                "id": "lblAddOptionsHeading",
                "isVisible": false,
                "left": "30px",
                "skin": "sknlblLato35475f14px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmPermissionsController.ADD_USERS\")",
                "top": 30,
                "width": "70%",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var flxAddOptionsContainer = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "bottom": "20px",
                "clipBounds": true,
                "id": "flxAddOptionsContainer",
                "isVisible": false,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "isModalContainer": false,
                "right": "0px",
                "skin": "slFbox",
                "top": "20dp",
                "zIndex": 1
            }, {}, {});
            flxAddOptionsContainer.setDefaultUnit(kony.flex.DP);
            var flxAvailableOptionsSection = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "bottom": "60dp",
                "clipBounds": true,
                "id": "flxAvailableOptionsSection",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "20px",
                "isModalContainer": false,
                "skin": "slFbox0c11dce22bfe447",
                "top": "0px",
                "width": "57%",
                "zIndex": 1
            }, {}, {});
            flxAvailableOptionsSection.setDefaultUnit(kony.flex.DP);
            var lblAvailableOptionsHeading = new kony.ui.Label({
                "id": "lblAvailableOptionsHeading",
                "isVisible": true,
                "left": "0px",
                "skin": "sknLbl485C75LatoRegular13Px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.permission.AvailableUsers\")",
                "top": 0,
                "width": "60%",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var btnAddAll = new kony.ui.Button({
                "id": "btnAddAll",
                "isVisible": true,
                "right": 0,
                "skin": "sknBtn11abeb12px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmGroupsController.Add_All\")",
                "top": 0,
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "displayText": true,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var flxAvailableOptSearchSeg = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "bottom": "20dp",
                "clipBounds": true,
                "id": "flxAvailableOptSearchSeg",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "sknflxffffffBordere1e5ed4Px",
                "top": "30dp",
                "width": "100%"
            }, {}, {});
            flxAvailableOptSearchSeg.setDefaultUnit(kony.flex.DP);
            var flxSearch = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "55px",
                "id": "flxSearch",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": 0,
                "isModalContainer": false,
                "right": 0,
                "skin": "sknflxf5f6f8Op100",
                "top": "0px",
                "zIndex": 1
            }, {}, {});
            flxSearch.setDefaultUnit(kony.flex.DP);
            var flxSearchContainer = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "centerY": "50%",
                "clipBounds": true,
                "focusSkin": "slFbox0ebc847fa67a243Search",
                "height": "35px",
                "id": "flxSearchContainer",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "10dp",
                "isModalContainer": false,
                "right": 10,
                "skin": "sknflxd5d9ddop100",
                "zIndex": 1
            }, {}, {});
            flxSearchContainer.setDefaultUnit(kony.flex.DP);
            var tbxSearchBox = new kony.ui.TextBox2({
                "autoCapitalize": constants.TEXTBOX_AUTO_CAPITALIZE_NONE,
                "centerY": "50%",
                "height": "100%",
                "id": "tbxSearchBox",
                "isVisible": true,
                "keyBoardStyle": constants.TEXTBOX_KEY_BOARD_STYLE_DEFAULT,
                "left": "32dp",
                "onTouchStart": controller.AS_TextField_gd0effc0c9f24917a2970c68626a1284,
                "i18n_placeholder": "kony.i18n.getLocalizedString(\"i18n.permission.search\")",
                "right": "35dp",
                "secureTextEntry": false,
                "skin": "skntbxffffffNoBorderlato35475f14px",
                "textInputMode": constants.TEXTBOX_INPUT_MODE_ANY,
                "zIndex": 1
            }, {
                "containerHeightMode": constants.TEXTBOX_FONT_METRICS_DRIVEN_HEIGHT,
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [3, 0, 0, 0],
                "paddingInPixel": false
            }, {
                "autoCorrect": false
            });
            var flxClearSearch = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "centerY": "47%",
                "clipBounds": true,
                "height": "35px",
                "id": "flxClearSearch",
                "isVisible": false,
                "layoutType": kony.flex.FREE_FORM,
                "isModalContainer": false,
                "right": "20px",
                "skin": "slFbox",
                "width": "35px",
                "zIndex": 1
            }, {}, {
                "hoverSkin": "sknCursor"
            });
            flxClearSearch.setDefaultUnit(kony.flex.DP);
            var fontIconImgCLose = new kony.ui.Label({
                "centerY": "50.00%",
                "id": "fontIconImgCLose",
                "isVisible": true,
                "left": "17dp",
                "skin": "sknFontIconSearchCross16px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmAlertsManagement.lblAlertPreviewClose\")",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 2
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxClearSearch.add(fontIconImgCLose);
            var fontIconImgSearchIcon = new kony.ui.Label({
                "centerY": "50%",
                "id": "fontIconImgSearchIcon",
                "isVisible": true,
                "left": "17dp",
                "skin": "sknFontIconSearchImg20px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmGroups.fontIconSearchimg\")",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 2
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxSearchContainer.add(tbxSearchBox, flxClearSearch, fontIconImgSearchIcon);
            flxSearch.add(flxSearchContainer);
            var flxAddOptionsSegment = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "bottom": 0,
                "clipBounds": true,
                "id": "flxAddOptionsSegment",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "sknflxffffffOp100BorderE1E5EdNoRadius",
                "top": "55dp",
                "width": "100%",
                "zIndex": 1
            }, {}, {});
            flxAddOptionsSegment.setDefaultUnit(kony.flex.DP);
            var segAddOptions = new kony.ui.SegmentedUI2({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "bottom": 0,
                "data": [{
                    "btnAdd": "kony.i18n.getLocalizedString(\"i18n.DragBox.ADD\")",
                    "lblFullName": "John Doe",
                    "lblUserIdValue": "TDU34256987",
                    "lblUsername": "USERNAME"
                }, {
                    "btnAdd": "ADD",
                    "lblFullName": "John Doe",
                    "lblUserIdValue": "TDU34256987",
                    "lblUsername": "USERNAME"
                }, {
                    "btnAdd": "ADD",
                    "lblFullName": "John Doe",
                    "lblUserIdValue": "TDU34256987",
                    "lblUsername": "USERNAME"
                }, {
                    "btnAdd": "ADD",
                    "lblFullName": "John Doe",
                    "lblUserIdValue": "TDU34256987",
                    "lblUsername": "USERNAME"
                }, {
                    "btnAdd": "ADD",
                    "lblFullName": "John Doe",
                    "lblUserIdValue": "TDU34256987",
                    "lblUsername": "USERNAME"
                }, {
                    "btnAdd": "ADD",
                    "lblFullName": "John Doe",
                    "lblUserIdValue": "TDU34256987",
                    "lblUsername": "USERNAME"
                }, {
                    "btnAdd": "ADD",
                    "lblFullName": "John Doe",
                    "lblUserIdValue": "TDU34256987",
                    "lblUsername": "USERNAME"
                }, {
                    "btnAdd": "ADD",
                    "lblFullName": "John Doe",
                    "lblUserIdValue": "TDU34256987",
                    "lblUsername": "USERNAME"
                }, {
                    "btnAdd": "ADD",
                    "lblFullName": "John Doe",
                    "lblUserIdValue": "TDU34256987",
                    "lblUsername": "USERNAME"
                }],
                "groupCells": false,
                "height": "100%",
                "id": "segAddOptions",
                "isVisible": true,
                "left": "0dp",
                "needPageIndicator": true,
                "pageOffDotImage": "pageoffdot.png",
                "pageOnDotImage": "pageondot.png",
                "retainSelection": false,
                "rowFocusSkin": "seg2Focus",
                "rowSkin": "sknsegffffKA",
                "rowTemplate": "flxAddUsers",
                "sectionHeaderSkin": "sliPhoneSegmentHeader",
                "selectionBehavior": constants.SEGUI_DEFAULT_BEHAVIOR,
                "separatorRequired": false,
                "showScrollbars": true,
                "top": "0dp",
                "viewType": constants.SEGUI_VIEW_TYPE_TABLEVIEW,
                "widgetDataMap": {
                    "btnAdd": "btnAdd",
                    "flxAddUsers": "flxAddUsers",
                    "flxAddUsersWrapper": "flxAddUsersWrapper",
                    "flxxUsernameWrapper": "flxxUsernameWrapper",
                    "lblFullName": "lblFullName",
                    "lblUserIdValue": "lblUserIdValue",
                    "lblUsername": "lblUsername"
                },
                "width": "100%",
                "zIndex": 1
            }, {
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxAddOptionsSegment.add(segAddOptions);
            var rtxAvailableOptionsMessage = new kony.ui.RichText({
                "centerX": "50%",
                "centerY": "50%",
                "id": "rtxAvailableOptionsMessage",
                "isVisible": false,
                "skin": "sknRtxLato84939e12Px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmCSR.richtextNoResult\")",
                "width": "80%",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_CENTER,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxAvailableOptSearchSeg.add(flxSearch, flxAddOptionsSegment, rtxAvailableOptionsMessage);
            flxAvailableOptionsSection.add(lblAvailableOptionsHeading, btnAddAll, flxAvailableOptSearchSeg);
            var flxSelectedOptions = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "bottom": "60dp",
                "clipBounds": true,
                "id": "flxSelectedOptions",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "isModalContainer": false,
                "right": "20dp",
                "skin": "slFbox0c11dce22bfe447",
                "top": "0px",
                "width": "37%",
                "zIndex": 1
            }, {}, {});
            flxSelectedOptions.setDefaultUnit(kony.flex.DP);
            var lblSelectedOption = new kony.ui.Label({
                "id": "lblSelectedOption",
                "isVisible": true,
                "left": "20px",
                "skin": "sknLbl485C75LatoRegular13Px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.permission.SelectedUsers\")",
                "top": 0,
                "width": "60%",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var flxSegSelectedOptions = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "bottom": "20dp",
                "clipBounds": true,
                "id": "flxSegSelectedOptions",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "20px",
                "isModalContainer": false,
                "right": "0dp",
                "skin": "sknflxf9f9f9Border1pxRad4px",
                "top": "30dp",
                "zIndex": 1
            }, {}, {});
            flxSegSelectedOptions.setDefaultUnit(kony.flex.DP);
            var segSelectedOptions = new kony.ui.SegmentedUI2({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "data": [{
                    "imgClose": "close_blue.png",
                    "lblOption": "John Doe"
                }, {
                    "imgClose": "close_blue.png",
                    "lblOption": "John Doe"
                }],
                "groupCells": false,
                "height": "100%",
                "id": "segSelectedOptions",
                "isVisible": true,
                "left": "0px",
                "needPageIndicator": true,
                "pageOffDotImage": "pageoffdot.png",
                "pageOnDotImage": "pageondot.png",
                "retainSelection": false,
                "rowSkin": "sknsegf9f9f9Op100",
                "rowTemplate": "flxOptionAdded",
                "sectionHeaderSkin": "sliPhoneSegmentHeader0h3448bdd9d8941",
                "selectionBehavior": constants.SEGUI_DEFAULT_BEHAVIOR,
                "separatorColor": "64646400",
                "separatorRequired": true,
                "separatorThickness": 0,
                "showScrollbars": true,
                "top": "0px",
                "viewType": constants.SEGUI_VIEW_TYPE_TABLEVIEW,
                "widgetDataMap": {
                    "flxAddOptionWrapper": "flxAddOptionWrapper",
                    "flxClose": "flxClose",
                    "flxOptionAdded": "flxOptionAdded",
                    "imgClose": "imgClose",
                    "lblOption": "lblOption"
                },
                "width": "100%",
                "zIndex": 1
            }, {
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var rtxSelectedOptionsMessage = new kony.ui.RichText({
                "centerX": "50%",
                "centerY": "50%",
                "id": "rtxSelectedOptionsMessage",
                "isVisible": false,
                "skin": "sknRtxLato84939e12Px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmPermissionsController.Click_Add_to_select_a_user\")",
                "width": "80%",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_CENTER,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxSegSelectedOptions.add(segSelectedOptions, rtxSelectedOptionsMessage);
            var btnRemoveAll = new kony.ui.Button({
                "id": "btnRemoveAll",
                "isVisible": true,
                "right": 0,
                "skin": "sknBtn11abeb12px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.Common.button.Reset\")",
                "top": 0,
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "displayText": true,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxSelectedOptions.add(lblSelectedOption, flxSegSelectedOptions, btnRemoveAll);
            var flxBtnSeperatorRoles = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "bottom": "60dp",
                "clipBounds": true,
                "height": "1px",
                "id": "flxBtnSeperatorRoles",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "isModalContainer": false,
                "right": "0dp",
                "skin": "sknflxe1e5edop100",
                "zIndex": 5
            }, {}, {});
            flxBtnSeperatorRoles.setDefaultUnit(kony.flex.DP);
            flxBtnSeperatorRoles.add();
            var flxButtons = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "bottom": "2dp",
                "clipBounds": true,
                "height": "40px",
                "id": "flxButtons",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "20dp",
                "isModalContainer": false,
                "right": "20dp",
                "skin": "slFbox",
                "zIndex": 1
            }, {}, {});
            flxButtons.setDefaultUnit(kony.flex.DP);
            var btnCancel = new kony.ui.Button({
                "centerY": "50%",
                "focusSkin": "sknbtnf7f7faLatoRegular4f555dBorder1px8b96a5",
                "height": "40dp",
                "id": "btnCancel",
                "isVisible": true,
                "left": 0,
                "right": "29.50%",
                "skin": "sknbtnf7f7faLatoRegular4f555dBorder1px8b96a5",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.permission.CANCEL\")",
                "top": "0%",
                "width": "100px",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_CENTER,
                "displayText": true,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {
                "hoverSkin": "sknbtnf7f7faLatoRegular4f555dBorder1px8b96a5"
            });
            var flxRightButtons = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "centerY": "50%",
                "clipBounds": true,
                "height": "100%",
                "id": "flxRightButtons",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "isModalContainer": false,
                "right": 0,
                "skin": "slFbox",
                "width": "263px",
                "zIndex": 1
            }, {}, {});
            flxRightButtons.setDefaultUnit(kony.flex.DP);
            var btnNext = new kony.ui.Button({
                "centerY": "50%",
                "focusSkin": "sknBtn4A77A0LatoRegular13pxFFFFFFRad20px",
                "height": "40dp",
                "id": "btnNext",
                "isVisible": true,
                "left": 0,
                "skin": "sknBtn4A77A0LatoRegular13pxFFFFFFRad20px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.permission.NEXT\")",
                "top": "0%",
                "width": "100dp",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_CENTER,
                "displayText": true,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {
                "hoverSkin": "sknBtn2D5982LatoRegular13pxFFFFFFRad20px"
            });
            var btnSave = new kony.ui.Button({
                "centerY": "50%",
                "focusSkin": "sknBtn003E75LatoRegular13pxFFFFFFRad20px",
                "height": "40dp",
                "id": "btnSave",
                "isVisible": true,
                "right": "0px",
                "skin": "sknBtn003E75LatoRegular13pxFFFFFFRad20px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmAdManagement.CreateRoleCAPS\")",
                "top": "0%",
                "width": "143px",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_CENTER,
                "displayText": true,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {
                "hoverSkin": "sknBtn005198LatoRegular13pxFFFFFFRad20px"
            });
            flxRightButtons.add(btnNext, btnSave);
            flxButtons.add(btnCancel, flxRightButtons);
            flxAddOptionsContainer.add(flxAvailableOptionsSection, flxSelectedOptions, flxBtnSeperatorRoles, flxButtons);
            var flxAssignCustomerAccess = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "bottom": "0dp",
                "clipBounds": true,
                "id": "flxAssignCustomerAccess",
                "isVisible": false,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "isModalContainer": false,
                "right": "0dp",
                "skin": "slFbox",
                "top": "0dp",
                "zIndex": 1
            }, {}, {});
            flxAssignCustomerAccess.setDefaultUnit(kony.flex.DP);
            var lblCustAccessHeading = new kony.ui.Label({
                "id": "lblCustAccessHeading",
                "isVisible": true,
                "left": "15dp",
                "skin": "sknLbl192B45LatoRegular14px",
                "text": "Service Types",
                "top": "20dp",
                "width": kony.flex.USE_PREFFERED_SIZE
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var searchBoxCustAccess = new com.adminConsole.common.searchBox({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "height": "40px",
                "id": "searchBoxCustAccess",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "masterType": constants.MASTER_TYPE_DEFAULT,
                "isModalContainer": false,
                "right": "15dp",
                "skin": "slFbox",
                "top": "35dp",
                "width": "285px",
                "overrides": {
                    "searchBox": {
                        "isVisible": true,
                        "left": "viz.val_cleared",
                        "right": "15dp",
                        "top": "35dp",
                        "width": "285px"
                    },
                    "tbxSearchBox": {
                        "centerY": "50%",
                        "i18n_placeholder": "kony.i18n.getLocalizedString(\"i18n.frmRoles.SearchByServiceTypeorService\")"
                    }
                }
            }, {
                "overrides": {}
            }, {
                "overrides": {}
            });
            var flxCustAccessNoResults = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "bottom": "80dp",
                "clipBounds": true,
                "id": "flxCustAccessNoResults",
                "isVisible": false,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "isModalContainer": false,
                "right": "0dp",
                "skin": "slFbox",
                "top": "80dp",
                "zIndex": 1
            }, {}, {});
            flxCustAccessNoResults.setDefaultUnit(kony.flex.DP);
            var lblCustAccessNoResults = new kony.ui.Label({
                "centerX": "50%",
                "centerY": "50%",
                "id": "lblCustAccessNoResults",
                "isVisible": true,
                "skin": "sknLblLato84939E12px",
                "text": "Service Types",
                "width": "90%"
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_CENTER,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxCustAccessNoResults.add(lblCustAccessNoResults);
            var flxServiceDefinitionSegment = new kony.ui.FlexScrollContainer({
                "allowHorizontalBounce": false,
                "allowVerticalBounce": true,
                "bottom": "80dp",
                "bounces": true,
                "clipBounds": true,
                "enableScrolling": true,
                "horizontalScrollIndicator": true,
                "id": "flxServiceDefinitionSegment",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "pagingEnabled": false,
                "scrollDirection": kony.flex.SCROLL_VERTICAL,
                "skin": "slFSbox",
                "top": "80dp",
                "verticalScrollIndicator": true,
                "width": "100%"
            }, {}, {});
            flxServiceDefinitionSegment.setDefaultUnit(kony.flex.DP);
            var segCustAccess = new kony.ui.SegmentedUI2({
                "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                "data": [
                    [{
                            "imgSectionCheckbox": "checkboxnormal.png",
                            "lblAccountHolder": "kony.i18n.getLocalizedString(\"i18n.frmEnrollCustomer.OwnershipType_UC\")",
                            "lblAccountName": "kony.i18n.getLocalizedString(\"i18n.frmCompanies.ACCOUNTNAME\")",
                            "lblAccountNumber": "kony.i18n.getLocalizedString(\"i18n.frmCompanies.ACCOUNTNUMBER\")",
                            "lblAccountType": "kony.i18n.getLocalizedString(\"i18n.frmCompanies.ACCOUNTTYPE\")",
                            "lblAvailableActions": "Available Actions:",
                            "lblCountActions": "2",
                            "lblFeatureName": "Label",
                            "lblIconAccNameSort": "",
                            "lblIconFilterAccType": "",
                            "lblIconSortAccHolder": "",
                            "lblIconSortAccName": "",
                            "lblIconStatus": "kony.i18n.getLocalizedString(\"i18n.frmAlertsManagement.lblViewAlertStatusKey\")",
                            "lblIconToggleArrow": "",
                            "lblSectionLine": "John Doe",
                            "lblSeperator": "Label",
                            "lblStatusValue": "kony.i18n.getLocalizedString(\"i18n.secureimage.Active\")",
                            "lblTotalActions": "of 2"
                        },
                        [{
                            "imgCheckbox": "",
                            "lblServiceDefDesc": "Ability to view all the transactions",
                            "lblServiceDefName": "Retail Basic"
                        }]
                    ],
                    [{
                            "imgSectionCheckbox": "checkboxnormal.png",
                            "lblAccountHolder": "kony.i18n.getLocalizedString(\"i18n.frmEnrollCustomer.OwnershipType_UC\")",
                            "lblAccountName": "kony.i18n.getLocalizedString(\"i18n.frmCompanies.ACCOUNTNAME\")",
                            "lblAccountNumber": "kony.i18n.getLocalizedString(\"i18n.frmCompanies.ACCOUNTNUMBER\")",
                            "lblAccountType": "kony.i18n.getLocalizedString(\"i18n.frmCompanies.ACCOUNTTYPE\")",
                            "lblAvailableActions": "Available Actions:",
                            "lblCountActions": "2",
                            "lblFeatureName": "Label",
                            "lblIconAccNameSort": "",
                            "lblIconFilterAccType": "",
                            "lblIconSortAccHolder": "",
                            "lblIconSortAccName": "",
                            "lblIconStatus": "kony.i18n.getLocalizedString(\"i18n.frmAlertsManagement.lblViewAlertStatusKey\")",
                            "lblIconToggleArrow": "",
                            "lblSectionLine": "John Doe",
                            "lblSeperator": "Label",
                            "lblStatusValue": "kony.i18n.getLocalizedString(\"i18n.secureimage.Active\")",
                            "lblTotalActions": "of 2"
                        },
                        [{
                            "imgCheckbox": "checkboxnormal.png",
                            "lblServiceDefDesc": "Ability to view list of all transactions made at FI level",
                            "lblServiceDefName": "Retail Basic"
                        }, {
                            "imgCheckbox": "checkboxnormal.png",
                            "lblServiceDefDesc": "Ability to view list of all transactions made at FI level",
                            "lblServiceDefName": "Retail Basic"
                        }, {
                            "imgCheckbox": "checkboxnormal.png",
                            "lblServiceDefDesc": "Ability to view list of all transactions made at FI level",
                            "lblServiceDefName": "Retail Basic"
                        }, {
                            "imgCheckbox": "checkboxnormal.png",
                            "lblServiceDefDesc": "Ability to view list of all transactions made at FI level",
                            "lblServiceDefName": "Retail Basic"
                        }, {
                            "imgCheckbox": "checkboxnormal.png",
                            "lblServiceDefDesc": "Ability to view list of all transactions made at FI level",
                            "lblServiceDefName": "Retail Basic"
                        }, {
                            "imgCheckbox": "checkboxnormal.png",
                            "lblServiceDefDesc": "Ability to view list of all transactions made at FI level",
                            "lblServiceDefName": "Retail Basic"
                        }, {
                            "imgCheckbox": "checkboxnormal.png",
                            "lblServiceDefDesc": "Ability to view list of all transactions made at FI level",
                            "lblServiceDefName": "Retail Basic"
                        }, {
                            "imgCheckbox": "checkboxnormal.png",
                            "lblServiceDefDesc": "Ability to view list of all transactions made at FI level",
                            "lblServiceDefName": "Retail Basic"
                        }, {
                            "imgCheckbox": "checkboxnormal.png",
                            "lblServiceDefDesc": "Ability to view list of all transactions made at FI level",
                            "lblServiceDefName": "Retail Basic"
                        }, {
                            "imgCheckbox": "checkboxnormal.png",
                            "lblServiceDefDesc": "Ability to view list of all transactions made at FI level",
                            "lblServiceDefName": "Retail Basic"
                        }]
                    ]
                ],
                "groupCells": false,
                "id": "segCustAccess",
                "isVisible": true,
                "left": "15dp",
                "needPageIndicator": true,
                "pageOffDotImage": "pageoffdot.png",
                "pageOnDotImage": "pageondot.png",
                "retainSelection": false,
                "right": "15dp",
                "rowFocusSkin": "seg2Focus",
                "rowSkin": "seg2Normal",
                "rowTemplate": "flxRolesServiceDefRow",
                "sectionHeaderSkin": "sliPhoneSegmentHeader",
                "sectionHeaderTemplate": "flxEnrollSelectedAccountsSec",
                "selectionBehavior": constants.SEGUI_DEFAULT_BEHAVIOR,
                "separatorRequired": false,
                "separatorThickness": 0,
                "showScrollbars": false,
                "top": "0dp",
                "viewType": constants.SEGUI_VIEW_TYPE_TABLEVIEW,
                "widgetDataMap": {
                    "flxAccountHolder": "flxAccountHolder",
                    "flxAccountName": "flxAccountName",
                    "flxAccountNumCont": "flxAccountNumCont",
                    "flxAccountSectionCont": "flxAccountSectionCont",
                    "flxAccountType": "flxAccountType",
                    "flxCheckbox": "flxCheckbox",
                    "flxEnrollSelectedAccountsSec": "flxEnrollSelectedAccountsSec",
                    "flxHeaderContainer": "flxHeaderContainer",
                    "flxLeft": "flxLeft",
                    "flxRight": "flxRight",
                    "flxRolesServiceDefRow": "flxRolesServiceDefRow",
                    "flxRow1": "flxRow1",
                    "flxRow2": "flxRow2",
                    "flxServiceDefDesc": "flxServiceDefDesc",
                    "flxServiceDefName": "flxServiceDefName",
                    "flxToggleArrow": "flxToggleArrow",
                    "imgCheckbox": "imgCheckbox",
                    "imgSectionCheckbox": "imgSectionCheckbox",
                    "lblAccountHolder": "lblAccountHolder",
                    "lblAccountName": "lblAccountName",
                    "lblAccountNumber": "lblAccountNumber",
                    "lblAccountType": "lblAccountType",
                    "lblAvailableActions": "lblAvailableActions",
                    "lblCountActions": "lblCountActions",
                    "lblFeatureName": "lblFeatureName",
                    "lblIconAccNameSort": "lblIconAccNameSort",
                    "lblIconFilterAccType": "lblIconFilterAccType",
                    "lblIconSortAccHolder": "lblIconSortAccHolder",
                    "lblIconSortAccName": "lblIconSortAccName",
                    "lblIconStatus": "lblIconStatus",
                    "lblIconToggleArrow": "lblIconToggleArrow",
                    "lblSectionLine": "lblSectionLine",
                    "lblSeperator": "lblSeperator",
                    "lblServiceDefDesc": "lblServiceDefDesc",
                    "lblServiceDefName": "lblServiceDefName",
                    "lblStatusValue": "lblStatusValue",
                    "lblTotalActions": "lblTotalActions"
                }
            }, {
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxServiceDefinitionSegment.add(segCustAccess);
            var flxBtnSeperator = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "bottom": "80dp",
                "clipBounds": true,
                "height": "1px",
                "id": "flxBtnSeperator",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "isModalContainer": false,
                "right": "0dp",
                "skin": "sknflxe1e5edop100",
                "zIndex": 5
            }, {}, {});
            flxBtnSeperator.setDefaultUnit(kony.flex.DP);
            flxBtnSeperator.add();
            var commonButtons = new com.adminConsole.common.commonButtons({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "bottom": "10dp",
                "height": "80px",
                "id": "commonButtons",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "20px",
                "masterType": constants.MASTER_TYPE_DEFAULT,
                "isModalContainer": false,
                "right": "20px",
                "skin": "slFbox",
                "overrides": {
                    "btnCancel": {
                        "left": "0px"
                    },
                    "btnNext": {
                        "isVisible": false
                    },
                    "btnSave": {
                        "i18n_text": "kony.i18n.getLocalizedString(\"i18n.permission.NEXT\")"
                    },
                    "commonButtons": {
                        "bottom": "10dp",
                        "height": "80px",
                        "left": "20px",
                        "right": "20px",
                        "top": "viz.val_cleared",
                        "width": "viz.val_cleared"
                    }
                }
            }, {
                "overrides": {}
            }, {
                "overrides": {}
            });
            flxAssignCustomerAccess.add(lblCustAccessHeading, searchBoxCustAccess, flxCustAccessNoResults, flxServiceDefinitionSegment, flxBtnSeperator, commonButtons);
            var flxAddRoleDetails = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "100%",
                "id": "flxAddRoleDetails",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "isModalContainer": false,
                "right": 0,
                "skin": "slFbox",
                "top": "0dp",
                "width": "100%",
                "zIndex": 1
            }, {}, {});
            flxAddRoleDetails.setDefaultUnit(kony.flex.DP);
            var flxScrollAddRoleDetails = new kony.ui.FlexScrollContainer({
                "allowHorizontalBounce": false,
                "allowVerticalBounce": true,
                "bottom": 0,
                "bounces": true,
                "clipBounds": true,
                "enableScrolling": true,
                "height": "75%",
                "horizontalScrollIndicator": true,
                "id": "flxScrollAddRoleDetails",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "pagingEnabled": false,
                "right": "0dp",
                "scrollDirection": kony.flex.SCROLL_VERTICAL,
                "skin": "slFSbox",
                "top": "20dp",
                "verticalScrollIndicator": true,
                "zIndex": 1
            }, {}, {});
            flxScrollAddRoleDetails.setDefaultUnit(kony.flex.DP);
            var flxName = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "90px",
                "id": "flxName",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "20px",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0dp",
                "width": "41.74%",
                "zIndex": 1
            }, {}, {});
            flxName.setDefaultUnit(kony.flex.DP);
            var lblRoleNameKey = new kony.ui.Label({
                "id": "lblRoleNameKey",
                "isVisible": true,
                "left": "0px",
                "skin": "sknlbl485c7514px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.roles.NameoftheRole\")",
                "top": 0,
                "width": "100%",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var tbxRoleNameValue = new kony.ui.TextBox2({
                "autoCapitalize": constants.TEXTBOX_AUTO_CAPITALIZE_NONE,
                "focusSkin": "skntxtbxDetailsFocus14px",
                "height": "40dp",
                "id": "tbxRoleNameValue",
                "isVisible": true,
                "keyBoardStyle": constants.TEXTBOX_KEY_BOARD_STYLE_DEFAULT,
                "left": "0dp",
                "maxTextLength": 25,
                "placeholder": "Role Name",
                "secureTextEntry": false,
                "skin": "skntxtbx484b5214px",
                "textInputMode": constants.TEXTBOX_INPUT_MODE_ANY,
                "top": "30dp",
                "width": "100%",
                "zIndex": 1
            }, {
                "containerHeightMode": constants.TEXTBOX_FONT_METRICS_DRIVEN_HEIGHT,
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [4, 0, 0, 0],
                "paddingInPixel": false
            }, {
                "autoCorrect": false
            });
            var imgMandatoryName = new kony.ui.Image2({
                "height": "12px",
                "id": "imgMandatoryName",
                "isVisible": false,
                "left": "100px",
                "skin": "slImage",
                "src": "mandatory_2x.png",
                "top": "3px",
                "width": "6px",
                "zIndex": 1
            }, {
                "imageScaleMode": constants.IMAGE_SCALE_MODE_MAINTAIN_ASPECT_RATIO,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var flxNoRoleNameError = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "25px",
                "id": "flxNoRoleNameError",
                "isVisible": false,
                "layoutType": kony.flex.FLOW_HORIZONTAL,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "70dp",
                "width": "100%",
                "zIndex": 1
            }, {}, {});
            flxNoRoleNameError.setDefaultUnit(kony.flex.DP);
            var lblNoRoleNameErrorIcon = new kony.ui.Label({
                "height": "15dp",
                "id": "lblNoRoleNameErrorIcon",
                "isVisible": true,
                "left": "0dp",
                "skin": "sknErrorIcon",
                "text": "",
                "top": "0dp",
                "width": "15dp",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var lblNoRoleNameError = new kony.ui.Label({
                "id": "lblNoRoleNameError",
                "isVisible": true,
                "left": "0dp",
                "skin": "sknlblError",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmRolesController.Role_Name_cannot_be_empty\")",
                "top": "0dp",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxNoRoleNameError.add(lblNoRoleNameErrorIcon, lblNoRoleNameError);
            var lblRoleNameSize = new kony.ui.Label({
                "id": "lblRoleNameSize",
                "isVisible": true,
                "right": "0px",
                "skin": "sknlbl485c7514px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmPermissions.lblRoleNameSize\")",
                "top": "0px",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxName.add(lblRoleNameKey, tbxRoleNameValue, imgMandatoryName, flxNoRoleNameError, lblRoleNameSize);
            var flxValidity = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "75px",
                "id": "flxValidity",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "isModalContainer": false,
                "right": 0,
                "skin": "slFbox",
                "top": "0dp",
                "width": "55.87%",
                "zIndex": 1
            }, {}, {});
            flxValidity.setDefaultUnit(kony.flex.DP);
            var lblValidFrom = new kony.ui.Label({
                "id": "lblValidFrom",
                "isVisible": true,
                "left": "0px",
                "skin": "sknlbl485c7514px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmGroups.lblValidity\")",
                "top": 0,
                "width": "47%",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var calValidStartDate = new kony.ui.Calendar({
                "calendarIcon": "calbtn.png",
                "dateFormat": "MM/dd/yyyy",
                "height": "40dp",
                "id": "calValidStartDate",
                "isVisible": true,
                "left": "0dp",
                "placeholder": "MM/DD/YYYY",
                "skin": "sknCal35475f14Px",
                "top": "30dp",
                "viewConfig": {
                    "gridConfig": {
                        "allowWeekendSelectable": true
                    }
                },
                "viewType": constants.CALENDAR_VIEW_TYPE_GRID_POPUP,
                "width": "47%",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {
                "noOfMonths": 1
            });
            var calValidEndDate = new kony.ui.Calendar({
                "calendarIcon": "calbtn.png",
                "dateFormat": "MM/dd/yyyy",
                "height": "40dp",
                "id": "calValidEndDate",
                "isVisible": true,
                "placeholder": "MM/DD/YYYY",
                "right": "20px",
                "skin": "sknCal35475f14Px",
                "top": "30dp",
                "viewType": constants.CALENDAR_VIEW_TYPE_DEFAULT,
                "width": "47%",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {
                "noOfMonths": 1
            });
            var Label0b4617ed1f1af41 = new kony.ui.Label({
                "id": "Label0b4617ed1f1af41",
                "isVisible": true,
                "left": "406dp",
                "skin": "slLabel0a3041ed9d2454e",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmAlertsManagement.lblAlertTypeDescriptionCount\")",
                "top": "51dp",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var Label0a005cb85bbe644 = new kony.ui.Label({
                "id": "Label0a005cb85bbe644",
                "isVisible": true,
                "left": "67dp",
                "skin": "slLabel0d9a2a20de91c43",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmAlertsManagement.lblAlertTypeDescriptionCount\")",
                "top": "-13dp",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var imgMandatoryValidity = new kony.ui.Image2({
                "height": "12px",
                "id": "imgMandatoryValidity",
                "isVisible": false,
                "left": "48px",
                "skin": "slImage",
                "src": "mandatory_2x.png",
                "top": "3px",
                "width": "6px",
                "zIndex": 1
            }, {
                "imageScaleMode": constants.IMAGE_SCALE_MODE_MAINTAIN_ASPECT_RATIO,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var lblValidTo = new kony.ui.Label({
                "id": "lblValidTo",
                "isVisible": true,
                "right": 20,
                "skin": "sknlbl485c7514px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmGroups.CopylblValidity0d9c3f33b4acc44\")",
                "top": 0,
                "width": "47%",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxValidity.add(lblValidFrom, calValidStartDate, calValidEndDate, Label0b4617ed1f1af41, Label0a005cb85bbe644, imgMandatoryValidity, lblValidTo);
            var flxRoleStatus = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "25px",
                "id": "flxRoleStatus",
                "isVisible": true,
                "layoutType": kony.flex.FLOW_HORIZONTAL,
                "left": "20dp",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "370dp",
                "width": "100%",
                "zIndex": 1
            }, {}, {});
            flxRoleStatus.setDefaultUnit(kony.flex.DP);
            var lblSwitchActivate = new kony.ui.Label({
                "centerY": "50%",
                "id": "lblSwitchActivate",
                "isVisible": true,
                "left": "0px",
                "skin": "sknlbl485c7514px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.permission.Activate\")",
                "top": 0,
                "width": "55px",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var switchStatus = new kony.ui.Switch({
                "centerY": "50%",
                "height": "25dp",
                "id": "switchStatus",
                "isVisible": true,
                "left": "2px",
                "leftSideText": "ON",
                "rightSideText": "OFF",
                "selectedIndex": 0,
                "skin": "sknSwitchServiceManagement",
                "width": "36dp",
                "zIndex": 1
            }, {
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxRoleStatus.add(lblSwitchActivate, switchStatus);
            var flxDescription = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                "clipBounds": true,
                "id": "flxDescription",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "20px",
                "isModalContainer": false,
                "right": "20px",
                "skin": "slFbox",
                "top": "100dp",
                "zIndex": 1
            }, {}, {});
            flxDescription.setDefaultUnit(kony.flex.DP);
            var lblRoleDescription = new kony.ui.Label({
                "id": "lblRoleDescription",
                "isVisible": true,
                "left": "0px",
                "skin": "sknlbl485c7514px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.permission.Description\")",
                "top": 0,
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var lblRoleDescriptionSize = new kony.ui.Label({
                "id": "lblRoleDescriptionSize",
                "isVisible": true,
                "right": "0px",
                "skin": "sknlbl485c7514px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmFAQ.lblMessageSize\")",
                "top": "0px",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var txtRoleDescription = new kony.ui.TextArea2({
                "autoCapitalize": constants.TEXTAREA_AUTO_CAPITALIZE_NONE,
                "focusSkin": "skntxtAreaLato9ca9ba12Px",
                "height": "200dp",
                "id": "txtRoleDescription",
                "isVisible": true,
                "keyBoardStyle": constants.TEXTAREA_KEY_BOARD_STYLE_DEFAULT,
                "left": "0dp",
                "maxTextLength": 300,
                "numberOfVisibleLines": 3,
                "skin": "skntxtAreaLato35475f14Px",
                "textInputMode": constants.TEXTAREA_INPUT_MODE_ANY,
                "top": "30dp",
                "width": "100%",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_TOP_LEFT,
                "padding": [2, 2, 2, 2],
                "paddingInPixel": false
            }, {
                "autoCorrect": false
            });
            var flxNoRoleDescriptionError = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "25px",
                "id": "flxNoRoleDescriptionError",
                "isVisible": false,
                "layoutType": kony.flex.FLOW_HORIZONTAL,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "230dp",
                "width": "100%",
                "zIndex": 1
            }, {}, {});
            flxNoRoleDescriptionError.setDefaultUnit(kony.flex.DP);
            var lblNoRoleDescErrorIcon = new kony.ui.Label({
                "height": "15dp",
                "id": "lblNoRoleDescErrorIcon",
                "isVisible": true,
                "left": "0dp",
                "skin": "sknErrorIcon",
                "text": "",
                "top": "0dp",
                "width": "15dp",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var lblNoRoleDescriptionError = new kony.ui.Label({
                "id": "lblNoRoleDescriptionError",
                "isVisible": true,
                "left": "0dp",
                "skin": "sknlblError",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmRolesController.Role_Description_Cannot_Be_empty\")",
                "top": "0dp",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxNoRoleDescriptionError.add(lblNoRoleDescErrorIcon, lblNoRoleDescriptionError);
            var lblOptional = new kony.ui.Label({
                "id": "lblOptional",
                "isVisible": false,
                "left": "75px",
                "skin": "slLabel0d4f692dab05249",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmPermissions.lblOptional\")",
                "top": "0px",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxDescription.add(lblRoleDescription, lblRoleDescriptionSize, txtRoleDescription, flxNoRoleDescriptionError, lblOptional);
            flxScrollAddRoleDetails.add(flxName, flxValidity, flxRoleStatus, flxDescription);
            var flxAddRoleButtons = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "bottom": 0,
                "clipBounds": true,
                "height": "80px",
                "id": "flxAddRoleButtons",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0%",
                "isModalContainer": false,
                "skin": "slFbox",
                "width": "100%",
                "zIndex": 1
            }, {}, {});
            flxAddRoleButtons.setDefaultUnit(kony.flex.DP);
            var btnAddRoleCancel = new kony.ui.Button({
                "centerY": "50%",
                "focusSkin": "sknbtnf7f7faLatoRegular4f555dBorder1px8b96a5",
                "height": "40dp",
                "id": "btnAddRoleCancel",
                "isVisible": true,
                "left": "20px",
                "right": "29.50%",
                "skin": "sknbtnf7f7faLatoRegular4f555dBorder1px8b96a5",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.permission.CANCEL\")",
                "top": "0%",
                "width": "100px",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_CENTER,
                "displayText": true,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {
                "hoverSkin": "sknbtnf7f7faLatoRegular4f555dBorder1px8b96a5"
            });
            var flxAddRoleRightButtons = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "centerY": "50%",
                "clipBounds": true,
                "height": "100%",
                "id": "flxAddRoleRightButtons",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "isModalContainer": false,
                "right": "20px",
                "skin": "slFbox",
                "width": "220px",
                "zIndex": 1
            }, {}, {});
            flxAddRoleRightButtons.setDefaultUnit(kony.flex.DP);
            var btnAddRoleNext = new kony.ui.Button({
                "centerY": "50%",
                "focusSkin": "sknBtn003E75LatoRegular13pxFFFFFFRad20px",
                "height": "40dp",
                "id": "btnAddRoleNext",
                "isVisible": true,
                "right": 0,
                "skin": "sknBtn003E75LatoRegular13pxFFFFFFRad20px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.permission.NEXT\")",
                "top": "0%",
                "width": "100dp",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_CENTER,
                "displayText": true,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {
                "hoverSkin": "sknBtn005198LatoRegular13pxFFFFFFRad20px"
            });
            flxAddRoleRightButtons.add(btnAddRoleNext);
            var flxSeperatorLine = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "1px",
                "id": "flxSeperatorLine",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "30dp",
                "isModalContainer": false,
                "right": "30dp",
                "skin": "sknflxe1e5edop100",
                "top": "0dp",
                "zIndex": 1
            }, {}, {});
            flxSeperatorLine.setDefaultUnit(kony.flex.DP);
            flxSeperatorLine.add();
            flxAddRoleButtons.add(btnAddRoleCancel, flxAddRoleRightButtons, flxSeperatorLine);
            flxAddRoleDetails.add(flxScrollAddRoleDetails, flxAddRoleButtons);
            var flxAddPermissionDetails = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "bottom": 25,
                "clipBounds": true,
                "id": "flxAddPermissionDetails",
                "isVisible": false,
                "layoutType": kony.flex.FREE_FORM,
                "left": "20dp",
                "isModalContainer": false,
                "right": "20px",
                "skin": "slFbox",
                "top": "60dp",
                "zIndex": 1
            }, {}, {});
            flxAddPermissionDetails.setDefaultUnit(kony.flex.DP);
            var flxPermissionName = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "75px",
                "id": "flxPermissionName",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0dp",
                "width": "41.74%",
                "zIndex": 1
            }, {}, {});
            flxPermissionName.setDefaultUnit(kony.flex.DP);
            var lblPermissionNameKey = new kony.ui.Label({
                "id": "lblPermissionNameKey",
                "isVisible": true,
                "left": "0px",
                "skin": "sknlblLato35475f14px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.permission.NameofthePermission\")",
                "top": 0,
                "width": "100%",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var tbxPermissionNameValue = new kony.ui.TextBox2({
                "autoCapitalize": constants.TEXTBOX_AUTO_CAPITALIZE_NONE,
                "focusSkin": "skntbxLato35475f14px",
                "height": "40dp",
                "id": "tbxPermissionNameValue",
                "isVisible": true,
                "keyBoardStyle": constants.TEXTBOX_KEY_BOARD_STYLE_DEFAULT,
                "left": "0dp",
                "secureTextEntry": false,
                "skin": "skntbxLato35475f14px",
                "textInputMode": constants.TEXTBOX_INPUT_MODE_ANY,
                "top": "30dp",
                "width": "100%",
                "zIndex": 1
            }, {
                "containerHeightMode": constants.TEXTBOX_FONT_METRICS_DRIVEN_HEIGHT,
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [3, 0, 0, 0],
                "paddingInPixel": false
            }, {
                "autoCorrect": false
            });
            flxPermissionName.add(lblPermissionNameKey, tbxPermissionNameValue);
            var flxPermissionDescription = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                "clipBounds": true,
                "id": "flxPermissionDescription",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "100dp",
                "width": "100%",
                "zIndex": 1
            }, {}, {});
            flxPermissionDescription.setDefaultUnit(kony.flex.DP);
            var lblPermissiondescription = new kony.ui.Label({
                "id": "lblPermissiondescription",
                "isVisible": true,
                "left": "0px",
                "skin": "sknlblLato35475f14px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.permission.Description\")",
                "top": 0,
                "width": "100%",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var txtPermissionDescription = new kony.ui.TextArea2({
                "autoCapitalize": constants.TEXTAREA_AUTO_CAPITALIZE_NONE,
                "height": "200dp",
                "id": "txtPermissionDescription",
                "isVisible": true,
                "keyBoardStyle": constants.TEXTAREA_KEY_BOARD_STYLE_DEFAULT,
                "left": "0dp",
                "numberOfVisibleLines": 3,
                "skin": "skntxtAreaLato35475f14Px",
                "textInputMode": constants.TEXTAREA_INPUT_MODE_ANY,
                "top": "30dp",
                "width": "100%",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_TOP_LEFT,
                "padding": [1, 2, 2, 2],
                "paddingInPixel": false
            }, {
                "autoCorrect": false
            });
            flxPermissionDescription.add(lblPermissiondescription, txtPermissionDescription);
            var flxPermissionRadioButtons = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                "clipBounds": true,
                "id": "flxPermissionRadioButtons",
                "isVisible": true,
                "layoutType": kony.flex.FLOW_HORIZONTAL,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "360dp",
                "width": "100%",
                "zIndex": 1
            }, {}, {});
            flxPermissionRadioButtons.setDefaultUnit(kony.flex.DP);
            var imgActivatepermission = new kony.ui.Image2({
                "height": "15dp",
                "id": "imgActivatepermission",
                "isVisible": true,
                "left": "0dp",
                "skin": "slImage",
                "src": "imagedrag.png",
                "top": "0dp",
                "width": "15dp",
                "zIndex": 1
            }, {
                "imageScaleMode": constants.IMAGE_SCALE_MODE_MAINTAIN_ASPECT_RATIO,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var lblActivatePermission = new kony.ui.Label({
                "id": "lblActivatePermission",
                "isVisible": true,
                "left": "10px",
                "skin": "sknlblLato35475f14px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.permission.Activate\")",
                "top": 0,
                "width": "80px",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var imgDeactivatePermission = new kony.ui.Image2({
                "height": "15dp",
                "id": "imgDeactivatePermission",
                "isVisible": true,
                "left": "0dp",
                "skin": "slImage",
                "src": "imagedrag.png",
                "top": "0dp",
                "width": "15dp",
                "zIndex": 1
            }, {
                "imageScaleMode": constants.IMAGE_SCALE_MODE_MAINTAIN_ASPECT_RATIO,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var lblDeactivatePermission = new kony.ui.Label({
                "id": "lblDeactivatePermission",
                "isVisible": true,
                "left": "10px",
                "skin": "sknlblLato35475f14px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.permission.Deactivate\")",
                "top": 0,
                "width": "80px",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxPermissionRadioButtons.add(imgActivatepermission, lblActivatePermission, imgDeactivatePermission, lblDeactivatePermission);
            var flxAddPermissionButtons = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "bottom": 0,
                "clipBounds": true,
                "height": "50px",
                "id": "flxAddPermissionButtons",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0%",
                "isModalContainer": false,
                "skin": "slFbox",
                "width": "100%",
                "zIndex": 1
            }, {}, {});
            flxAddPermissionButtons.setDefaultUnit(kony.flex.DP);
            var btnAddPermissionCancel = new kony.ui.Button({
                "centerY": "50%",
                "focusSkin": "sknBtnLatoRegulara5abc413pxKA",
                "height": "40dp",
                "id": "btnAddPermissionCancel",
                "isVisible": true,
                "left": 0,
                "right": "29.50%",
                "skin": "sknBtnLatoRegulara5abc413pxKA",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.permission.CANCEL\")",
                "top": "0%",
                "width": "100px",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_CENTER,
                "displayText": true,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {
                "hoverSkin": "sknbtnffffffLatoBolda5abc413Pxd7d8e01pxBorderRadius20px"
            });
            var flxAddPermissionRightButtons = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "centerY": "50%",
                "clipBounds": true,
                "height": "100%",
                "id": "flxAddPermissionRightButtons",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "isModalContainer": false,
                "right": 0,
                "skin": "slFbox",
                "width": "220px",
                "zIndex": 1
            }, {}, {});
            flxAddPermissionRightButtons.setDefaultUnit(kony.flex.DP);
            var btnAddPermissionNext = new kony.ui.Button({
                "centerY": "50%",
                "focusSkin": "sknBtn4A77A0LatoRegular13pxFFFFFFRad20px",
                "height": "40dp",
                "id": "btnAddPermissionNext",
                "isVisible": true,
                "left": 0,
                "skin": "sknBtn4A77A0LatoRegular13pxFFFFFFRad20px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.permission.NEXT\")",
                "top": "0%",
                "width": "100dp",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_CENTER,
                "displayText": true,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {
                "hoverSkin": "sknBtn2D5982LatoRegular13pxFFFFFFRad20px"
            });
            var btnAddPermissionSave = new kony.ui.Button({
                "centerY": "50%",
                "focusSkin": "sknBtn003E75LatoRegular13pxFFFFFFRad20px",
                "height": "40dp",
                "id": "btnAddPermissionSave",
                "isVisible": true,
                "right": "0px",
                "skin": "sknBtn003E75LatoRegular13pxFFFFFFRad20px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.permission.SAVE\")",
                "top": "0%",
                "width": "100px",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_CENTER,
                "displayText": true,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {
                "hoverSkin": "sknBtn005198LatoRegular13pxFFFFFFRad20px"
            });
            flxAddPermissionRightButtons.add(btnAddPermissionNext, btnAddPermissionSave);
            flxAddPermissionButtons.add(btnAddPermissionCancel, flxAddPermissionRightButtons);
            flxAddPermissionDetails.add(flxPermissionName, flxPermissionDescription, flxPermissionRadioButtons, flxAddPermissionButtons);
            flxAddOptionsSection.add(lblAddOptionsHeading, flxAddOptionsContainer, flxAssignCustomerAccess, flxAddRoleDetails, flxAddPermissionDetails);
            flxAddMainContainer.add(flxOptions, flxAddOptionsSection);
            var flxViewPermissions = new kony.ui.FlexScrollContainer({
                "allowHorizontalBounce": false,
                "allowVerticalBounce": true,
                "bounces": true,
                "clipBounds": true,
                "enableScrolling": true,
                "height": "480px",
                "horizontalScrollIndicator": true,
                "id": "flxViewPermissions",
                "isVisible": false,
                "layoutType": kony.flex.FLOW_VERTICAL,
                "left": "35dp",
                "pagingEnabled": false,
                "right": 35,
                "scrollDirection": kony.flex.SCROLL_VERTICAL,
                "skin": "sknscrollflxFFFFFFBorder1Px",
                "top": "33dp",
                "verticalScrollIndicator": true,
                "zIndex": 1
            }, {}, {});
            flxViewPermissions.setDefaultUnit(kony.flex.DP);
            var flxViewContainer1 = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                "clipBounds": true,
                "id": "flxViewContainer1",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0dp",
                "width": "100%",
                "zIndex": 1
            }, {}, {});
            flxViewContainer1.setDefaultUnit(kony.flex.DP);
            var flxViewKeyValue1 = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "60dp",
                "id": "flxViewKeyValue1",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "20px",
                "isModalContainer": false,
                "right": "20dp",
                "skin": "slFbox",
                "top": "0dp",
                "zIndex": 1
            }, {}, {});
            flxViewKeyValue1.setDefaultUnit(kony.flex.DP);
            var lblViewKey1 = new kony.ui.Label({
                "bottom": "0dp",
                "height": "1dp",
                "id": "lblViewKey1",
                "isVisible": true,
                "left": "0dp",
                "right": "0dp",
                "skin": "sknlblSeperatorD7D9E0",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.permission.NAME\")",
                "width": "100%",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var lblViewValue1 = new kony.ui.Label({
                "centerY": "50%",
                "id": "lblViewValue1",
                "isVisible": true,
                "left": "0px",
                "skin": "sknLatoRegular192B4518px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmGroups.lblViewNameValue\")",
                "width": "100%",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var fontIconImgViewKey1 = new kony.ui.Label({
                "id": "fontIconImgViewKey1",
                "isVisible": false,
                "left": "0dp",
                "skin": "sknfontIconViewScreen",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmGroups.fontIconViewName\")",
                "top": "1dp",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var flxViewKeyValue2 = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                "centerY": "50%",
                "clipBounds": true,
                "id": "flxViewKeyValue2",
                "isVisible": true,
                "layoutType": kony.flex.FLOW_HORIZONTAL,
                "reverseLayoutDirection": true,
                "isModalContainer": false,
                "right": "70dp",
                "skin": "slFbox",
                "width": "100dp",
                "zIndex": 1
            }, {}, {});
            flxViewKeyValue2.setDefaultUnit(kony.flex.DP);
            var lblViewKey2 = new kony.ui.Label({
                "id": "lblViewKey2",
                "isVisible": false,
                "left": "0dp",
                "skin": "sknlblLato696c7311px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.roles.STATUS\")",
                "top": "0dp",
                "width": "90px",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var lblViewValue2 = new kony.ui.Label({
                "id": "lblViewValue2",
                "isVisible": true,
                "right": "0dp",
                "skin": "sknlbl485C75LatoSemiBold13px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.secureimage.Active\")",
                "top": "0px",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var fontIconImgViewKey2 = new kony.ui.Label({
                "id": "fontIconImgViewKey2",
                "isVisible": false,
                "left": "0dp",
                "skin": "sknfontIconViewScreen",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmGroups.fontIconViewStatus\")",
                "top": "0dp",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var fontIconViewValue2 = new kony.ui.Label({
                "id": "fontIconViewValue2",
                "isVisible": true,
                "right": "5dp",
                "skin": "sknFontIconActivate",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmAlertsManagement.lblViewAlertStatusKey\")",
                "top": "2dp",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxViewKeyValue2.add(lblViewKey2, lblViewValue2, fontIconImgViewKey2, fontIconViewValue2);
            var flxViewEditButton = new kony.ui.Button({
                "centerY": "50%",
                "focusSkin": "sknbtnf7f7faLatoRegular12Px485c75Border1px485C75Radius20px",
                "height": "22px",
                "id": "flxViewEditButton",
                "isVisible": true,
                "right": "1px",
                "skin": "sknbtnf7f7faLatoRegular12Px485c75Border1px485C75Radius20px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.roles.Edit\")",
                "width": "50px",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_CENTER,
                "displayText": true,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {
                "hoverSkin": "sknbtnffffffLatoRegular12Px485c75Border1px485C75Radius20px"
            });
            flxViewKeyValue1.add(lblViewKey1, lblViewValue1, fontIconImgViewKey1, flxViewKeyValue2, flxViewEditButton);
            var flxViewKeyValue3 = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "50dp",
                "id": "flxViewKeyValue3",
                "isVisible": false,
                "layoutType": kony.flex.FREE_FORM,
                "left": "60%",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "15dp",
                "width": "25%",
                "zIndex": 1
            }, {}, {});
            flxViewKeyValue3.setDefaultUnit(kony.flex.DP);
            var lblViewKey3 = new kony.ui.Label({
                "id": "lblViewKey3",
                "isVisible": true,
                "left": "20dp",
                "skin": "sknlblLato5d6c7f12px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.roles.VALIDTILL\")",
                "top": "1dp",
                "width": "90px",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var lblViewValue3 = new kony.ui.Label({
                "id": "lblViewValue3",
                "isVisible": true,
                "left": "0px",
                "skin": "sknlbl485C75LatoSemiBold13px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmGroups.lblViewValidTillValue\")",
                "top": "25px",
                "width": "100%",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var fontIconImgViewKey3 = new kony.ui.Label({
                "id": "fontIconImgViewKey3",
                "isVisible": true,
                "left": "0dp",
                "skin": "sknfontIconViewScreen",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmGroups.fontIconValidTill\")",
                "top": "1dp",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxViewKeyValue3.add(lblViewKey3, lblViewValue3, fontIconImgViewKey3);
            var flxViewDescription = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "bottom": 20,
                "clipBounds": true,
                "height": "20dp",
                "id": "flxViewDescription",
                "isVisible": true,
                "layoutType": kony.flex.FLOW_HORIZONTAL,
                "left": "20px",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "80dp",
                "width": "120px",
                "zIndex": 1
            }, {}, {});
            flxViewDescription.setDefaultUnit(kony.flex.DP);
            var lblViewDescription = new kony.ui.Label({
                "id": "lblViewDescription",
                "isVisible": true,
                "left": "0dp",
                "skin": "sknlblLato696c7311px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.permission.DESCRIPTION\")",
                "top": "1dp",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var fontIconImgViewDescription = new kony.ui.Label({
                "id": "fontIconImgViewDescription",
                "isVisible": false,
                "left": "8dp",
                "skin": "sknfontIconDescRightArrow14px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmLogs.lblBreadcrumbsRight\")",
                "top": "1dp",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxViewDescription.add(lblViewDescription, fontIconImgViewDescription);
            var rtxViewDescription = new kony.ui.Label({
                "bottom": "30dp",
                "id": "rtxViewDescription",
                "isVisible": true,
                "left": "20px",
                "right": "20dp",
                "skin": "sknlbl485C75LatoSemiBold13px",
                "text": "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aenean euismod bibendum laoreet. Proin gravida dolor sit amet lacus accumsan et viverra justo commodo. Proin sodales pulvinar tempor. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Nam fermentum, nulla luctus pharetra vulputate, felis tellus mollis orci, sed rhoncus sapien nunc eget.",
                "top": "105px",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxViewContainer1.add(flxViewKeyValue1, flxViewKeyValue3, flxViewDescription, rtxViewDescription);
            var flxViewContainer2 = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                "clipBounds": true,
                "id": "flxViewContainer2",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0dp",
                "width": "100%",
                "zIndex": 1
            }, {}, {});
            flxViewContainer2.setDefaultUnit(kony.flex.DP);
            var flxViewTabs = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "45dp",
                "id": "flxViewTabs",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0dp",
                "width": "100%",
                "zIndex": 1
            }, {}, {});
            flxViewTabs.setDefaultUnit(kony.flex.DP);
            var flxViewTab1 = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "100%",
                "id": "flxViewTab1",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "20dp",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0dp",
                "width": "80px",
                "zIndex": 1
            }, {}, {
                "hoverSkin": "sknCursor"
            });
            flxViewTab1.setDefaultUnit(kony.flex.DP);
            var lblTabName1 = new kony.ui.Label({
                "bottom": "1dp",
                "centerX": "50%",
                "height": "100%",
                "id": "lblTabName1",
                "isVisible": true,
                "skin": "sknlblLato5d6c7f12px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.roles.PERMISSIONS\")",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_CENTER,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {
                "hoverSkin": "sknlblLato0hd9dd99ab6d14aHover"
            });
            var flxTabUnderline1 = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "3dp",
                "id": "flxTabUnderline1",
                "isVisible": false,
                "layoutType": kony.flex.FREE_FORM,
                "left": "-9dp",
                "isModalContainer": false,
                "skin": "sknflx11abebRadius3px",
                "top": "42dp",
                "width": "90px",
                "zIndex": 1
            }, {}, {});
            flxTabUnderline1.setDefaultUnit(kony.flex.DP);
            flxTabUnderline1.add();
            flxViewTab1.add(lblTabName1, flxTabUnderline1);
            var flxViewTab2 = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "100%",
                "id": "flxViewTab2",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "120dp",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0dp",
                "width": "40px",
                "zIndex": 1
            }, {}, {
                "hoverSkin": "sknCursor"
            });
            flxViewTab2.setDefaultUnit(kony.flex.DP);
            var lblTabName2 = new kony.ui.Label({
                "bottom": "1dp",
                "centerX": "50%",
                "height": "100%",
                "id": "lblTabName2",
                "isVisible": true,
                "skin": "sknlblLato5d6c7f12px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.roles.USERS\")",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_CENTER,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {
                "hoverSkin": "sknlblLato0hd9dd99ab6d14aHover"
            });
            var flxTabUnderline2 = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "3dp",
                "id": "flxTabUnderline2",
                "isVisible": false,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "sknflx11abebRadius3px",
                "top": "45dp",
                "width": "38px",
                "zIndex": 1
            }, {}, {});
            flxTabUnderline2.setDefaultUnit(kony.flex.DP);
            flxTabUnderline2.add();
            flxViewTab2.add(lblTabName2, flxTabUnderline2);
            var flxViewSeperator = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "bottom": "0dp",
                "clipBounds": true,
                "height": "1px",
                "id": "flxViewSeperator",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "sknflxSepratorE1E5ED",
                "width": "100%",
                "zIndex": 1
            }, {}, {});
            flxViewSeperator.setDefaultUnit(kony.flex.DP);
            flxViewSeperator.add();
            flxViewTabs.add(flxViewTab1, flxViewTab2, flxViewSeperator);
            var flxViewDetailsSubTabs = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "50dp",
                "id": "flxViewDetailsSubTabs",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "isModalContainer": false,
                "right": "0dp",
                "skin": "slFbox",
                "top": "45dp",
                "zIndex": 1
            }, {}, {});
            flxViewDetailsSubTabs.setDefaultUnit(kony.flex.DP);
            var tabs = new com.adminConsole.common.tabs({
                "height": "100%",
                "id": "tabs",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "masterType": constants.MASTER_TYPE_DEFAULT,
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0dp",
                "width": "100%",
                "overrides": {
                    "btnTab1": {
                        "height": "40dp",
                        "i18n_text": "kony.i18n.getLocalizedString(\"i18n.roles.SYSTEM_PERMISSIONS\")"
                    },
                    "btnTab2": {
                        "height": "40dp",
                        "i18n_text": "kony.i18n.getLocalizedString(\"i18n.roles.CUSTOMER_ACCESS\")"
                    },
                    "flxTabsContainer": {
                        "height": "50dp"
                    }
                }
            }, {
                "overrides": {}
            }, {
                "overrides": {}
            });
            flxViewDetailsSubTabs.add(tabs);
            var flxViewSegmentAndHeaders = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                "clipBounds": true,
                "id": "flxViewSegmentAndHeaders",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "20dp",
                "isModalContainer": false,
                "right": 20,
                "skin": "slFbox",
                "top": "105dp",
                "zIndex": 1
            }, {}, {});
            flxViewSegmentAndHeaders.setDefaultUnit(kony.flex.DP);
            var flxPermissionsHeader = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "60px",
                "id": "flxPermissionsHeader",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0",
                "isModalContainer": false,
                "skin": "sknflxffffffop100",
                "top": "0",
                "width": "100%"
            }, {}, {});
            flxPermissionsHeader.setDefaultUnit(kony.flex.DP);
            var flxViewPermissionName = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "100%",
                "id": "flxViewPermissionName",
                "isVisible": true,
                "layoutType": kony.flex.FLOW_HORIZONTAL,
                "left": "10px",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0dp",
                "width": "100px",
                "zIndex": 1
            }, {}, {
                "hoverSkin": "sknCursor"
            });
            flxViewPermissionName.setDefaultUnit(kony.flex.DP);
            var lblViewPermissionName = new kony.ui.Label({
                "centerY": "50%",
                "id": "lblViewPermissionName",
                "isVisible": true,
                "left": "0px",
                "skin": "sknlblLato696c7312px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.permission.NAME\")",
                "top": 0,
                "width": "37px",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {
                "hoverSkin": "sknlblLatoBold00000012px"
            });
            var fontImgViewPermissionNameSort = new kony.ui.Label({
                "centerY": "50%",
                "id": "fontImgViewPermissionNameSort",
                "isVisible": true,
                "left": "0px",
                "skin": "sknlblLato696c7312px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.permission.NAME\")",
                "width": "40px",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {
                "hoverSkin": "sknlblLato00000011px"
            });
            flxViewPermissionName.add(lblViewPermissionName, fontImgViewPermissionNameSort);
            var flxViewPermissionDescription = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "100%",
                "id": "flxViewPermissionDescription",
                "isVisible": true,
                "layoutType": kony.flex.FLOW_HORIZONTAL,
                "left": "30%",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0dp",
                "width": "100px",
                "zIndex": 1
            }, {}, {});
            flxViewPermissionDescription.setDefaultUnit(kony.flex.DP);
            var lblViewPermissionDescription = new kony.ui.Label({
                "centerY": "50%",
                "id": "lblViewPermissionDescription",
                "isVisible": true,
                "left": "0px",
                "skin": "sknlblLato696c7312px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.permission.DESCRIPTION\")",
                "top": 0,
                "width": "90px",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxViewPermissionDescription.add(lblViewPermissionDescription);
            var flxViewPermissionSeperator = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "bottom": 0,
                "clipBounds": true,
                "height": "1px",
                "id": "flxViewPermissionSeperator",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "sknTableHeaderLine",
                "width": "100%",
                "zIndex": 1
            }, {}, {});
            flxViewPermissionSeperator.setDefaultUnit(kony.flex.DP);
            flxViewPermissionSeperator.add();
            flxPermissionsHeader.add(flxViewPermissionName, flxViewPermissionDescription, flxViewPermissionSeperator);
            var flxUsersHeader = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "60px",
                "id": "flxUsersHeader",
                "isVisible": false,
                "layoutType": kony.flex.FREE_FORM,
                "left": 0,
                "isModalContainer": false,
                "skin": "sknflxffffffop100",
                "top": "0",
                "width": "100%"
            }, {}, {});
            flxUsersHeader.setDefaultUnit(kony.flex.DP);
            var flxViewUsersFullName = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "100%",
                "id": "flxViewUsersFullName",
                "isVisible": true,
                "layoutType": kony.flex.FLOW_HORIZONTAL,
                "left": "10px",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0dp",
                "width": "120px",
                "zIndex": 1
            }, {}, {
                "hoverSkin": "sknCursor"
            });
            flxViewUsersFullName.setDefaultUnit(kony.flex.DP);
            var lblViewUsersFullName = new kony.ui.Label({
                "centerY": "50%",
                "id": "lblViewUsersFullName",
                "isVisible": true,
                "left": "0px",
                "skin": "sknlblLato696c7312px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.permission.FULLNAME\")",
                "top": 0,
                "width": "70px",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {
                "hoverSkin": "sknlblLatoBold00000012px"
            });
            var fontImgViewUsersNameSort = new kony.ui.Label({
                "centerY": "50%",
                "id": "fontImgViewUsersNameSort",
                "isVisible": true,
                "left": "0px",
                "skin": "sknlblLato696c7312px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.permission.NAME\")",
                "width": "40px",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {
                "hoverSkin": "sknlblCursorFont"
            });
            flxViewUsersFullName.add(lblViewUsersFullName, fontImgViewUsersNameSort);
            var flxViewUsersUsername = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "100%",
                "id": "flxViewUsersUsername",
                "isVisible": true,
                "layoutType": kony.flex.FLOW_HORIZONTAL,
                "left": "18.93%",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0dp",
                "width": "120px",
                "zIndex": 1
            }, {}, {
                "hoverSkin": "sknCursor"
            });
            flxViewUsersUsername.setDefaultUnit(kony.flex.DP);
            var lblViewUsersUsername = new kony.ui.Label({
                "centerY": "50%",
                "id": "lblViewUsersUsername",
                "isVisible": true,
                "left": "0px",
                "skin": "sknlblLato696c7312px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.permission.USERNAME\")",
                "top": 0,
                "width": "70px",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {
                "hoverSkin": "sknlblLatoBold00000012px"
            });
            var fontImgViewUsersUsernameSort = new kony.ui.Label({
                "centerY": "50%",
                "id": "fontImgViewUsersUsernameSort",
                "isVisible": true,
                "left": "0px",
                "skin": "sknlblLato696c7312px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.permission.NAME\")",
                "width": "40px",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {
                "hoverSkin": "sknlblCursorFont"
            });
            flxViewUsersUsername.add(lblViewUsersUsername, fontImgViewUsersUsernameSort);
            var flxViewUsersEmailId = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "100%",
                "id": "flxViewUsersEmailId",
                "isVisible": true,
                "layoutType": kony.flex.FLOW_HORIZONTAL,
                "left": "36.69%",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0dp",
                "width": "100px",
                "zIndex": 1
            }, {}, {
                "hoverSkin": "sknCursor"
            });
            flxViewUsersEmailId.setDefaultUnit(kony.flex.DP);
            var lblEmail = new kony.ui.Label({
                "centerY": "50%",
                "id": "lblEmail",
                "isVisible": true,
                "left": "0px",
                "skin": "sknlblLato696c7312px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.permission.EMAILID\")",
                "top": 0,
                "width": "55px",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {
                "hoverSkin": "sknlblLatoBold00000012px"
            });
            var fontImgSortEmail = new kony.ui.Label({
                "centerY": "50%",
                "id": "fontImgSortEmail",
                "isVisible": true,
                "left": "0px",
                "skin": "sknlblLato696c7312px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.permission.NAME\")",
                "width": "40px",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {
                "hoverSkin": "sknlblCursorFont"
            });
            flxViewUsersEmailId.add(lblEmail, fontImgSortEmail);
            var flxViewUsersUpdatedBy = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "100%",
                "id": "flxViewUsersUpdatedBy",
                "isVisible": true,
                "layoutType": kony.flex.FLOW_HORIZONTAL,
                "left": "60.35%",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0dp",
                "width": "120px",
                "zIndex": 1
            }, {}, {
                "hoverSkin": "sknCursor"
            });
            flxViewUsersUpdatedBy.setDefaultUnit(kony.flex.DP);
            var lblRole = new kony.ui.Label({
                "centerY": "50%",
                "id": "lblRole",
                "isVisible": true,
                "left": "0px",
                "skin": "sknlblLato696c7312px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.permission.UPDATEDBY\")",
                "top": 0,
                "width": "80px",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {
                "hoverSkin": "sknlblLatoBold00000012px"
            });
            var fontImgSortRole = new kony.ui.Label({
                "centerY": "50%",
                "id": "fontImgSortRole",
                "isVisible": true,
                "left": "0px",
                "skin": "sknlblLato696c7312px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.permission.NAME\")",
                "width": "40px",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {
                "hoverSkin": "sknlblCursorFont"
            });
            flxViewUsersUpdatedBy.add(lblRole, fontImgSortRole);
            var flxViewUsersUpdatedOn = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "100%",
                "id": "flxViewUsersUpdatedOn",
                "isVisible": true,
                "layoutType": kony.flex.FLOW_HORIZONTAL,
                "left": "77.28%",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0dp",
                "width": "120px",
                "zIndex": 1
            }, {}, {
                "hoverSkin": "sknCursor"
            });
            flxViewUsersUpdatedOn.setDefaultUnit(kony.flex.DP);
            var lblPermissions = new kony.ui.Label({
                "centerY": "50%",
                "id": "lblPermissions",
                "isVisible": true,
                "left": "0px",
                "skin": "sknlblLato696c7312px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.permission.UPDATEDON\")",
                "top": 0,
                "width": "82px",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {
                "hoverSkin": "sknlblLatoBold00000012px"
            });
            var fontImgSortPermissions = new kony.ui.Label({
                "centerY": "50%",
                "id": "fontImgSortPermissions",
                "isVisible": true,
                "left": "0px",
                "skin": "sknlblLato696c7312px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.permission.NAME\")",
                "width": "40px",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {
                "hoverSkin": "sknlblCursorFont"
            });
            flxViewUsersUpdatedOn.add(lblPermissions, fontImgSortPermissions);
            var flxViewUsersSeperator = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "bottom": 0,
                "clipBounds": true,
                "height": "1px",
                "id": "flxViewUsersSeperator",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "sknTableHeaderLine",
                "width": "100%",
                "zIndex": 1
            }, {}, {});
            flxViewUsersSeperator.setDefaultUnit(kony.flex.DP);
            flxViewUsersSeperator.add();
            flxUsersHeader.add(flxViewUsersFullName, flxViewUsersUsername, flxViewUsersEmailId, flxViewUsersUpdatedBy, flxViewUsersUpdatedOn, flxViewUsersSeperator);
            var flxViewSegment = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                "bottom": "20px",
                "clipBounds": true,
                "id": "flxViewSegment",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "61dp",
                "width": "100%",
                "zIndex": 1
            }, {}, {});
            flxViewSegment.setDefaultUnit(kony.flex.DP);
            var segViewSegment = new kony.ui.SegmentedUI2({
                "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                "data": [{
                    "lblDescription": "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do",
                    "lblIconAction": "Label",
                    "lblPermissionName": "Admin Role",
                    "lblSeperator": "."
                }, {
                    "lblDescription": "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do",
                    "lblIconAction": "Label",
                    "lblPermissionName": "Admin Role",
                    "lblSeperator": "."
                }, {
                    "lblDescription": "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do",
                    "lblIconAction": "Label",
                    "lblPermissionName": "Admin Role",
                    "lblSeperator": "."
                }, {
                    "lblDescription": "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do",
                    "lblIconAction": "Label",
                    "lblPermissionName": "Admin Role",
                    "lblSeperator": "."
                }, {
                    "lblDescription": "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do",
                    "lblIconAction": "Label",
                    "lblPermissionName": "Admin Role",
                    "lblSeperator": "."
                }, {
                    "lblDescription": "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do",
                    "lblIconAction": "Label",
                    "lblPermissionName": "Admin Role",
                    "lblSeperator": "."
                }, {
                    "lblDescription": "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do",
                    "lblIconAction": "Label",
                    "lblPermissionName": "Admin Role",
                    "lblSeperator": "."
                }, {
                    "lblDescription": "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do",
                    "lblIconAction": "Label",
                    "lblPermissionName": "Admin Role",
                    "lblSeperator": "."
                }, {
                    "lblDescription": "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do",
                    "lblIconAction": "Label",
                    "lblPermissionName": "Admin Role",
                    "lblSeperator": "."
                }],
                "groupCells": false,
                "id": "segViewSegment",
                "isVisible": true,
                "left": "0dp",
                "needPageIndicator": true,
                "pageOffDotImage": "pageoffdot.png",
                "pageOnDotImage": "pageondot.png",
                "retainSelection": false,
                "rowFocusSkin": "seg2Focus",
                "rowSkin": "seg2Normal",
                "rowTemplate": "flxViewPermissions",
                "sectionHeaderSkin": "sliPhoneSegmentHeader",
                "selectionBehavior": constants.SEGUI_DEFAULT_BEHAVIOR,
                "separatorRequired": false,
                "showScrollbars": false,
                "top": "0dp",
                "viewType": constants.SEGUI_VIEW_TYPE_TABLEVIEW,
                "widgetDataMap": {
                    "flxActionIcon": "flxActionIcon",
                    "flxViewPermissions": "flxViewPermissions",
                    "lblDescription": "lblDescription",
                    "lblIconAction": "lblIconAction",
                    "lblPermissionName": "lblPermissionName",
                    "lblSeperator": "lblSeperator"
                },
                "width": "100%",
                "zIndex": 1
            }, {
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxViewSegment.add(segViewSegment);
            var rtxAvailabletxt = new kony.ui.RichText({
                "bottom": "50px",
                "centerX": "50%",
                "id": "rtxAvailabletxt",
                "isVisible": false,
                "skin": "sknRtxLato84939e12Px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmRoles.rtxAvailabletxt\")",
                "top": "50px",
                "width": "80%",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_CENTER,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxViewSegmentAndHeaders.add(flxPermissionsHeader, flxUsersHeader, flxViewSegment, rtxAvailabletxt);
            var flxViewConfigureCsrCont = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                "clipBounds": true,
                "id": "flxViewConfigureCsrCont",
                "isVisible": false,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "105dp",
                "width": "100%",
                "zIndex": 1
            }, {}, {});
            flxViewConfigureCsrCont.setDefaultUnit(kony.flex.DP);
            var viewConfigureCSRAssist = new com.adminConsole.Permissions.viewConfigureCSRAssist({
                "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                "id": "viewConfigureCSRAssist",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "masterType": constants.MASTER_TYPE_DEFAULT,
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0dp",
                "width": "100%",
                "overrides": {
                    "segViewConfigureCSR": {
                        "data": [{
                            "lblEnable": "",
                            "lblEnabled": "",
                            "lblHeadingDesc": "",
                            "lblIconArrow": "",
                            "lblIconGreenTick": "",
                            "lblLine": "",
                            "lblLine2": "",
                            "lblName": "",
                            "rtxDescription": "",
                            "switchToggle": {
                                "selectedIndex": 0
                            }
                        }]
                    }
                }
            }, {
                "overrides": {}
            }, {
                "overrides": {}
            });
            flxViewConfigureCsrCont.add(viewConfigureCSRAssist);
            var flxViewPagination = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "bottom": 20,
                "clipBounds": true,
                "height": "30px",
                "id": "flxViewPagination",
                "isVisible": false,
                "layoutType": kony.flex.FREE_FORM,
                "isModalContainer": false,
                "right": "35dp",
                "skin": "slFbox0d9c3974835234d",
                "width": "215px",
                "zIndex": 1
            }, {}, {});
            flxViewPagination.setDefaultUnit(kony.flex.DP);
            var lbxViewPagination = new kony.ui.ListBox({
                "height": "30dp",
                "id": "lbxViewPagination",
                "isVisible": true,
                "left": "0dp",
                "masterData": [
                    ["lb1", "Page 1 of 20"],
                    ["lb2", "Page 2 of 20"],
                    ["lb3", "Page 3 of 20"],
                    ["lb4", "Page 4 of 20"]
                ],
                "selectedKey": "lb1",
                "skin": "sknlbxNobgNoBorderPagination",
                "top": "0dp",
                "width": "115dp",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {
                "multiSelect": false
            });
            var flxViewPaginationSeperator = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "100%",
                "id": "flxViewPaginationSeperator",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "130dp",
                "isModalContainer": false,
                "skin": "sknflxd6dbe7",
                "top": "2dp",
                "width": "1px",
                "zIndex": 1
            }, {}, {});
            flxViewPaginationSeperator.setDefaultUnit(kony.flex.DP);
            flxViewPaginationSeperator.add();
            var flxViewPrevious = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "30dp",
                "id": "flxViewPrevious",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "isModalContainer": false,
                "right": 40,
                "skin": "sknflxffffffop100Bordercbcdd1Radius100px",
                "top": "0dp",
                "width": "30px",
                "zIndex": 1
            }, {}, {});
            flxViewPrevious.setDefaultUnit(kony.flex.DP);
            var imgPrevious2 = new kony.ui.Image2({
                "centerX": "50%",
                "centerY": "50%",
                "height": "15dp",
                "id": "imgPrevious2",
                "isVisible": true,
                "skin": "slImage",
                "src": "imagedrag.png",
                "width": "15dp",
                "zIndex": 1
            }, {
                "imageScaleMode": constants.IMAGE_SCALE_MODE_MAINTAIN_ASPECT_RATIO,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxViewPrevious.add(imgPrevious2);
            var flxViewNext = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "30dp",
                "id": "flxViewNext",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "isModalContainer": false,
                "right": 0,
                "skin": "sknflxffffffop100Bordercbcdd1Radius100px",
                "top": "0dp",
                "width": "30px",
                "zIndex": 1
            }, {}, {});
            flxViewNext.setDefaultUnit(kony.flex.DP);
            var imgNext2 = new kony.ui.Image2({
                "centerX": "50%",
                "centerY": "50%",
                "height": "15dp",
                "id": "imgNext2",
                "isVisible": true,
                "skin": "slImage",
                "src": "imagedrag.png",
                "width": "15dp",
                "zIndex": 1
            }, {
                "imageScaleMode": constants.IMAGE_SCALE_MODE_MAINTAIN_ASPECT_RATIO,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxViewNext.add(imgNext2);
            flxViewPagination.add(lbxViewPagination, flxViewPaginationSeperator, flxViewPrevious, flxViewNext);
            flxViewContainer2.add(flxViewTabs, flxViewDetailsSubTabs, flxViewSegmentAndHeaders, flxViewConfigureCsrCont, flxViewPagination);
            flxViewPermissions.add(flxViewContainer1, flxViewContainer2);
            flxViews.add(flxRolesBreadCrumb, flxAddMainContainer, flxViewPermissions);
            flxScrollMainContent.add(flxPermissions, flxViews);
            flxRightPanel.add(flxMainHeader, flxMainSubHeader, flxScrollMainContent);
            var flxHeaderDropdown = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                "clipBounds": true,
                "id": "flxHeaderDropdown",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "isModalContainer": false,
                "right": "35dp",
                "skin": "slFbox",
                "top": "20dp",
                "width": "400dp",
                "zIndex": 5
            }, {}, {});
            flxHeaderDropdown.setDefaultUnit(kony.flex.DP);
            var dropdownMainHeader = new com.adminConsole.common.dropdownMainHeader({
                "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                "id": "dropdownMainHeader",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "masterType": constants.MASTER_TYPE_DEFAULT,
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0dp",
                "width": "100%",
                "overrides": {
                    "flxDropdown": {
                        "isVisible": false
                    },
                    "imgUpArrow": {
                        "src": "uparrow_2x.png"
                    }
                }
            }, {
                "overrides": {}
            }, {
                "overrides": {}
            });
            flxHeaderDropdown.add(dropdownMainHeader);
            var flxToastMessage = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "bottom": "0px",
                "clipBounds": true,
                "height": "70px",
                "id": "flxToastMessage",
                "isVisible": false,
                "layoutType": kony.flex.FREE_FORM,
                "left": "305px",
                "isModalContainer": false,
                "right": "0px",
                "skin": "slFbox",
                "zIndex": 1
            }, {}, {});
            flxToastMessage.setDefaultUnit(kony.flex.DP);
            var flxToastContainer = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "centerX": "50%",
                "centerY": "50%",
                "clipBounds": true,
                "height": "45px",
                "id": "flxToastContainer",
                "isVisible": false,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0%",
                "isModalContainer": false,
                "skin": "sknflxSuccessToast1F844D",
                "top": "0%",
                "width": "60%",
                "zIndex": 1
            }, {}, {});
            flxToastContainer.setDefaultUnit(kony.flex.DP);
            var imgLeft = new kony.ui.Image2({
                "centerY": "50%",
                "height": "15dp",
                "id": "imgLeft",
                "isVisible": true,
                "left": "15px",
                "skin": "slImage",
                "src": "arrow2x.png",
                "top": "0dp",
                "width": "15dp",
                "zIndex": 1
            }, {
                "imageScaleMode": constants.IMAGE_SCALE_MODE_MAINTAIN_ASPECT_RATIO,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var lbltoastMessage = new kony.ui.Label({
                "centerY": "50%",
                "id": "lbltoastMessage",
                "isVisible": true,
                "left": "45px",
                "right": "45px",
                "skin": "lblfffffflatoregular14px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.roles.RoleDeactivatedSuccessfully\")",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var imgRight = new kony.ui.Image2({
                "centerY": "50%",
                "height": "15px",
                "id": "imgRight",
                "isVisible": true,
                "right": "15px",
                "skin": "slImage",
                "src": "close_small2x.png",
                "top": "0dp",
                "width": "15px",
                "zIndex": 1
            }, {
                "imageScaleMode": constants.IMAGE_SCALE_MODE_MAINTAIN_ASPECT_RATIO,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxToastContainer.add(imgLeft, lbltoastMessage, imgRight);
            var toastMessage = new com.adminConsole.common.toastMessage({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "id": "toastMessage",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "masterType": constants.MASTER_TYPE_DEFAULT,
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0dp",
                "overrides": {}
            }, {
                "overrides": {}
            }, {
                "overrides": {}
            });
            flxToastMessage.add(flxToastContainer, toastMessage);
            var flxLoading = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "100%",
                "id": "flxLoading",
                "isVisible": false,
                "layoutType": kony.flex.FREE_FORM,
                "left": "305px",
                "isModalContainer": false,
                "right": "0px",
                "skin": "sknLoadingBlur",
                "top": "0dp",
                "zIndex": 10
            }, {}, {});
            flxLoading.setDefaultUnit(kony.flex.DP);
            var flxImageContainer = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "centerX": "50%",
                "centerY": "50%",
                "clipBounds": true,
                "height": "23%",
                "id": "flxImageContainer",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "isModalContainer": false,
                "skin": "slFbox",
                "width": "15%",
                "zIndex": 1
            }, {}, {});
            flxImageContainer.setDefaultUnit(kony.flex.DP);
            var imgLoading = new kony.ui.Image2({
                "centerX": "50%",
                "centerY": "50%",
                "height": "30%",
                "id": "imgLoading",
                "isVisible": true,
                "skin": "slImage",
                "src": "loadingscreenimage.gif",
                "width": "30%",
                "zIndex": 1
            }, {
                "imageScaleMode": constants.IMAGE_SCALE_MODE_MAINTAIN_ASPECT_RATIO,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxImageContainer.add(imgLoading);
            flxLoading.add(flxImageContainer);
            flxMain.add(flxLeftPannel, flxRightPanel, flxHeaderDropdown, flxToastMessage, flxLoading);
            var flxDeactivatePermission = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "100%",
                "id": "flxDeactivatePermission",
                "isVisible": false,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "masterType": constants.MASTER_TYPE_USERWIDGET,
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0dp",
                "width": "100%",
                "zIndex": 5
            }, {}, {});
            flxDeactivatePermission.setDefaultUnit(kony.flex.DP);
            var popUpDeactivate = new com.adminConsole.common.popUp1({
                "height": "100%",
                "id": "popUpDeactivate",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "masterType": constants.MASTER_TYPE_DEFAULT,
                "isModalContainer": false,
                "skin": "sknflxBg000000Op50",
                "top": "0dp",
                "width": "100%",
                "zIndex": 1,
                "overrides": {
                    "btnPopUpCancel": {
                        "i18n_text": "kony.i18n.getLocalizedString(\"i18n.PopUp.NoLeaveAsIS\")",
                        "minWidth": "81px",
                        "right": "20px",
                        "width": kony.flex.USE_PREFFERED_SIZE
                    },
                    "btnPopUpDelete": {
                        "i18n_text": "kony.i18n.getLocalizedString(\"i18n.PopUp.YesDeactivate\")",
                        "minWidth": "83px"
                    },
                    "lblPopUpMainMessage": {
                        "i18n_text": "kony.i18n.getLocalizedString(\"i18n.roles.DeactivateRole\")"
                    },
                    "rtxPopUpDisclaimer": {
                        "bottom": "30px",
                        "centerX": "viz.val_cleared",
                        "right": "viz.val_cleared",
                        "text": "Are you sure to deactivate \"<b>Role Admin</b>\"?<br><br>\nThe permission has been assigned to few roles and Users. Deactivation may impact their ability to perform certain actions.",
                        "width": "93%"
                    }
                }
            }, {
                "overrides": {}
            }, {
                "overrides": {}
            });
            flxDeactivatePermission.add(popUpDeactivate);
            var flxErrorPopup = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "100%",
                "id": "flxErrorPopup",
                "isVisible": false,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "sknLoadingBlur",
                "top": "0dp",
                "width": "100%",
                "zIndex": 1
            }, {}, {});
            flxErrorPopup.setDefaultUnit(kony.flex.DP);
            var popUp = new com.adminConsole.common.popUp({
                "height": "100%",
                "id": "popUp",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "masterType": constants.MASTER_TYPE_DEFAULT,
                "isModalContainer": false,
                "skin": "sknflxBg000000Op50",
                "top": "0dp",
                "width": "100%",
                "zIndex": 1,
                "overrides": {
                    "btnPopUpCancel": {
                        "i18n_text": "kony.i18n.getLocalizedString(\"i18n.PopUp.NoLeaveAsIS\")",
                        "right": "20px"
                    },
                    "btnPopUpDelete": {
                        "i18n_text": "kony.i18n.getLocalizedString(\"i18n.PopUp.YesDeactivate\")"
                    }
                }
            }, {
                "overrides": {}
            }, {
                "overrides": {}
            });
            flxErrorPopup.add(popUp);
            var flxEditCancelConfirmation = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "100%",
                "id": "flxEditCancelConfirmation",
                "isVisible": false,
                "layoutType": kony.flex.FREE_FORM,
                "left": "10dp",
                "masterType": constants.MASTER_TYPE_USERWIDGET,
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "10dp",
                "width": "100%",
                "zIndex": 5
            }, {}, {});
            flxEditCancelConfirmation.setDefaultUnit(kony.flex.DP);
            var popUpCancelEdits = new com.adminConsole.common.popUpCancelEdits({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "height": "100%",
                "id": "popUpCancelEdits",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "masterType": constants.MASTER_TYPE_DEFAULT,
                "isModalContainer": false,
                "skin": "sknflxBg000000Op50",
                "top": "0dp",
                "width": "100%",
                "zIndex": 1,
                "overrides": {
                    "btnPopUpCancel": {
                        "i18n_text": "kony.i18n.getLocalizedString(\"i8n.navigation.cancelButton\")"
                    },
                    "btnPopUpDelete": {
                        "i18n_text": "kony.i18n.getLocalizedString(\"i8n.navigation.okButton\")"
                    },
                    "lblPopUpMainMessage": {
                        "i18n_text": "kony.i18n.getLocalizedString(\"i8n.navigation.cancelChanges\")"
                    },
                    "popUpCancelEdits": {
                        "bottom": "viz.val_cleared",
                        "centerX": "viz.val_cleared",
                        "centerY": "viz.val_cleared",
                        "height": "100%",
                        "left": "0dp",
                        "maxHeight": "viz.val_cleared",
                        "maxWidth": "viz.val_cleared",
                        "minHeight": "viz.val_cleared",
                        "minWidth": "viz.val_cleared",
                        "right": "viz.val_cleared",
                        "top": "0dp",
                        "width": "100%"
                    },
                    "rtxPopUpDisclaimer": {
                        "i18n_text": "kony.i18n.getLocalizedString(\"i8n.navigation.cancelChangesBody\")"
                    }
                }
            }, {
                "overrides": {}
            }, {
                "overrides": {}
            });
            flxEditCancelConfirmation.add(popUpCancelEdits);
            this.add(flxMain, flxDeactivatePermission, flxErrorPopup, flxEditCancelConfirmation);
        };
        return [{
            "addWidgets": addWidgetsfrmRoles,
            "enabledForIdleTimeout": true,
            "id": "frmRoles",
            "layoutType": kony.flex.FREE_FORM,
            "needAppMenu": false,
            "postShow": controller.AS_Form_cd45c4f20943412c914118e7af896ca2,
            "preShow": function(eventobject) {
                controller.AS_Form_a1db9eda1c02408e9cd521de33ca41cc(eventobject);
            },
            "skin": "slForm"
        }, {
            "displayOrientation": constants.FORM_DISPLAY_ORIENTATION_PORTRAIT,
            "layoutType": kony.flex.FREE_FORM,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, {
            "onDeviceBack": controller.AS_Form_idf26aba4473404c9b1a98e460308b34,
            "retainScrollPosition": false
        }]
    }
});