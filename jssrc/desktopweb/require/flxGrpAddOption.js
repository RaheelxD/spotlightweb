define("flxGrpAddOption", function() {
    return function(controller) {
        var flxGrpAddOption = new kony.ui.FlexContainer({
            "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
            "clipBounds": true,
            "id": "flxGrpAddOption",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "isModalContainer": false,
            "skin": "slFbox"
        }, {}, {});
        flxGrpAddOption.setDefaultUnit(kony.flex.DP);
        var flxAdd = new kony.ui.FlexContainer({
            "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
            "clipBounds": true,
            "id": "flxAdd",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "left": "10dp",
            "minHeight": "85px",
            "isModalContainer": false,
            "right": 20,
            "skin": "sknflxffffffOp100Border5e90cbRadius3Px",
            "top": "10dp",
            "zIndex": 1
        }, {}, {
            "hoverSkin": "blocksHover"
        });
        flxAdd.setDefaultUnit(kony.flex.DP);
        var btnAdd = new kony.ui.Button({
            "height": "20px",
            "id": "btnAdd",
            "isVisible": true,
            "onClick": controller.AS_Button_f5a5f3781fa641cf9f3fdb8692a05a99,
            "right": 20,
            "skin": "contextuallinks",
            "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmPermissionsController.Add\")",
            "top": "15dp",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 2
        }, {
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_RIGHT,
            "displayText": true,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, {
            "hoverSkin": "sknBtnLatoRegular1682b212px"
        });
        var flxNameDesc = new kony.ui.FlexContainer({
            "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
            "clipBounds": true,
            "id": "flxNameDesc",
            "isVisible": true,
            "layoutType": kony.flex.FLOW_VERTICAL,
            "left": "0dp",
            "isModalContainer": false,
            "skin": "slFbox",
            "top": "15dp",
            "width": "100%"
        }, {}, {});
        flxNameDesc.setDefaultUnit(kony.flex.DP);
        var lblName = new kony.ui.Label({
            "id": "lblName",
            "isVisible": true,
            "left": "15px",
            "skin": "sknlblLatoBold35475f14px",
            "text": "IBFT",
            "top": "0px",
            "width": "80%",
            "zIndex": 1
        }, {
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, {});
        var rtxDescription = new kony.ui.RichText({
            "bottom": "10px",
            "id": "rtxDescription",
            "isVisible": true,
            "left": "15px",
            "skin": "sknrtxLato485c7514px",
            "text": "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do incididunt ut labore et dolore magna aliqua.",
            "top": "5px",
            "width": "80%",
            "zIndex": 1
        }, {
            "contentAlignment": constants.CONTENT_ALIGN_TOP_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, {});
        flxNameDesc.add(lblName, rtxDescription);
        flxAdd.add(btnAdd, flxNameDesc);
        flxGrpAddOption.add(flxAdd);
        return flxGrpAddOption;
    }
})