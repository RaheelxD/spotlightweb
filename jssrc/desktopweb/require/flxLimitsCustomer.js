define("flxLimitsCustomer", function() {
    return function(controller) {
        var flxLimitsCustomer = new kony.ui.FlexContainer({
            "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
            "clipBounds": true,
            "id": "flxLimitsCustomer",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "left": "0dp",
            "isModalContainer": false,
            "skin": "sknFlxFFFFFF100O",
            "top": "0dp",
            "width": "100%"
        }, {}, {});
        flxLimitsCustomer.setDefaultUnit(kony.flex.DP);
        var lblPermissionName = new kony.ui.Label({
            "id": "lblPermissionName",
            "isVisible": true,
            "left": "16dp",
            "skin": "sknlblLato485c7514px",
            "text": "Label",
            "top": "30dp",
            "width": "32%",
            "zIndex": 1
        }, {
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, {});
        var flxDailyLimit = new kony.ui.FlexContainer({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "bottom": "10dp",
            "clipBounds": true,
            "height": "40dp",
            "id": "flxDailyLimit",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "left": "35%",
            "isModalContainer": false,
            "skin": "sknflxffffffop100dbdbe6Radius3px",
            "top": "20dp",
            "width": "180dp",
            "zIndex": 1
        }, {}, {});
        flxDailyLimit.setDefaultUnit(kony.flex.DP);
        var flxCurrency = new kony.ui.FlexContainer({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "clipBounds": true,
            "height": "100%",
            "id": "flxCurrency",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "left": "0dp",
            "isModalContainer": false,
            "skin": "slFboxBGF9F9F9",
            "top": "0dp",
            "width": "40dp",
            "zIndex": 2
        }, {}, {});
        flxCurrency.setDefaultUnit(kony.flex.DP);
        var lblCurrencyValue = new kony.ui.Label({
            "centerX": "53%",
            "centerY": "50%",
            "id": "lblCurrencyValue",
            "isVisible": true,
            "skin": "sknIcomoonCurrencySymbol",
            "text": "",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, {
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, {});
        var flxLine = new kony.ui.FlexContainer({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "clipBounds": true,
            "height": "100%",
            "id": "flxLine",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "isModalContainer": false,
            "right": "0dp",
            "skin": "sknSeparatorCsr",
            "top": "0dp",
            "width": "1dp",
            "zIndex": 1
        }, {}, {});
        flxLine.setDefaultUnit(kony.flex.DP);
        flxLine.add();
        flxCurrency.add(lblCurrencyValue, flxLine);
        var tbxMaxDailyLimit = new kony.ui.TextBox2({
            "autoCapitalize": constants.TEXTBOX_AUTO_CAPITALIZE_NONE,
            "height": "100%",
            "id": "tbxMaxDailyLimit",
            "isVisible": true,
            "keyBoardStyle": constants.TEXTBOX_KEY_BOARD_STYLE_DEFAULT,
            "left": "40dp",
            "placeholder": "Placeholder",
            "secureTextEntry": false,
            "skin": "skntbxffffffNoBorderlato35475f14px",
            "textInputMode": constants.TEXTBOX_INPUT_MODE_NUMERIC,
            "top": "0dp",
            "width": "138dp",
            "zIndex": 1
        }, {
            "containerHeightMode": constants.TEXTBOX_FONT_METRICS_DRIVEN_HEIGHT,
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [3, 0, 0, 0],
            "paddingInPixel": false
        }, {
            "autoCorrect": false,
            "placeholderSkin": "defTextBoxPlaceholder"
        });
        var lblDailyLimit = new kony.ui.Label({
            "centerY": "50%",
            "id": "lblDailyLimit",
            "isVisible": false,
            "left": "10dp",
            "skin": "sknlblLato485c7514px",
            "text": "Label",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, {
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, {});
        flxDailyLimit.add(flxCurrency, tbxMaxDailyLimit, lblDailyLimit);
        var flxTransactionLimit = new kony.ui.FlexContainer({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "bottom": "10dp",
            "clipBounds": true,
            "height": "40dp",
            "id": "flxTransactionLimit",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "left": "67%",
            "isModalContainer": false,
            "skin": "sknflxffffffop100dbdbe6Radius3px",
            "top": "20dp",
            "width": "180dp",
            "zIndex": 1
        }, {}, {});
        flxTransactionLimit.setDefaultUnit(kony.flex.DP);
        var flxCurrencyTransactional = new kony.ui.FlexContainer({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "clipBounds": true,
            "height": "100%",
            "id": "flxCurrencyTransactional",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "left": "0dp",
            "isModalContainer": false,
            "skin": "slFboxBGF9F9F9",
            "top": "0dp",
            "width": "40dp",
            "zIndex": 2
        }, {}, {});
        flxCurrencyTransactional.setDefaultUnit(kony.flex.DP);
        var lblTransactionCurrency = new kony.ui.Label({
            "centerX": "53%",
            "centerY": "50%",
            "id": "lblTransactionCurrency",
            "isVisible": true,
            "skin": "sknIcomoonCurrencySymbol",
            "text": "",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, {
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, {});
        var flxLineVertical = new kony.ui.FlexContainer({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "clipBounds": true,
            "height": "100%",
            "id": "flxLineVertical",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "isModalContainer": false,
            "right": "0dp",
            "skin": "sknSeparatorCsr",
            "top": "0dp",
            "width": "1dp",
            "zIndex": 1
        }, {}, {});
        flxLineVertical.setDefaultUnit(kony.flex.DP);
        flxLineVertical.add();
        flxCurrencyTransactional.add(lblTransactionCurrency, flxLineVertical);
        var tbxTransactionLimit = new kony.ui.TextBox2({
            "autoCapitalize": constants.TEXTBOX_AUTO_CAPITALIZE_NONE,
            "height": "100%",
            "id": "tbxTransactionLimit",
            "isVisible": true,
            "keyBoardStyle": constants.TEXTBOX_KEY_BOARD_STYLE_DEFAULT,
            "left": "40dp",
            "placeholder": "Placeholder",
            "secureTextEntry": false,
            "skin": "skntbxffffffNoBorderlato35475f14px",
            "textInputMode": constants.TEXTBOX_INPUT_MODE_NUMERIC,
            "top": "0dp",
            "width": "140dp",
            "zIndex": 1
        }, {
            "containerHeightMode": constants.TEXTBOX_FONT_METRICS_DRIVEN_HEIGHT,
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [3, 0, 0, 0],
            "paddingInPixel": false
        }, {
            "autoCorrect": false,
            "placeholderSkin": "defTextBoxPlaceholder"
        });
        var lblTransactionLimit = new kony.ui.Label({
            "centerY": "50%",
            "id": "lblTransactionLimit",
            "isVisible": false,
            "left": "10dp",
            "skin": "sknlblLato485c7514px",
            "text": "Label",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, {
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, {});
        flxTransactionLimit.add(flxCurrencyTransactional, tbxTransactionLimit, lblTransactionLimit);
        var flxErrorDailyLimit = new kony.ui.FlexContainer({
            "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
            "clipBounds": true,
            "id": "flxErrorDailyLimit",
            "isVisible": false,
            "layoutType": kony.flex.FLOW_HORIZONTAL,
            "left": "35%",
            "isModalContainer": false,
            "right": "20dp",
            "skin": "slFbox",
            "top": "70dp",
            "width": "25%"
        }, {}, {});
        flxErrorDailyLimit.setDefaultUnit(kony.flex.DP);
        var lblErrorIconDaily = new kony.ui.Label({
            "id": "lblErrorIconDaily",
            "isVisible": true,
            "left": "0dp",
            "skin": "sknFontIconError",
            "text": "",
            "top": "2dp",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, {
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, {});
        var lblErrorTextDaily = new kony.ui.Label({
            "bottom": "10dp",
            "id": "lblErrorTextDaily",
            "isVisible": true,
            "left": "10dp",
            "skin": "sknlblError",
            "text": "Error",
            "top": "0dp",
            "width": "85%",
            "zIndex": 1
        }, {
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, {});
        flxErrorDailyLimit.add(lblErrorIconDaily, lblErrorTextDaily);
        var flxErrorTransaction = new kony.ui.FlexContainer({
            "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
            "clipBounds": true,
            "id": "flxErrorTransaction",
            "isVisible": false,
            "layoutType": kony.flex.FLOW_HORIZONTAL,
            "left": "67%",
            "isModalContainer": false,
            "right": "20dp",
            "skin": "slFbox",
            "top": "70dp",
            "width": "25%"
        }, {}, {});
        flxErrorTransaction.setDefaultUnit(kony.flex.DP);
        var lblErrorIconTransaction = new kony.ui.Label({
            "id": "lblErrorIconTransaction",
            "isVisible": true,
            "left": "0dp",
            "skin": "sknFontIconError",
            "text": "",
            "top": "2dp",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, {
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, {});
        var lblErrorTextTransaction = new kony.ui.Label({
            "bottom": "10dp",
            "id": "lblErrorTextTransaction",
            "isVisible": true,
            "left": "10dp",
            "skin": "sknlblError",
            "text": "Error",
            "top": "0dp",
            "width": "85%",
            "zIndex": 1
        }, {
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, {});
        flxErrorTransaction.add(lblErrorIconTransaction, lblErrorTextTransaction);
        flxLimitsCustomer.add(lblPermissionName, flxDailyLimit, flxTransactionLimit, flxErrorDailyLimit, flxErrorTransaction);
        return flxLimitsCustomer;
    }
})