define("flxSimulationResults", function() {
    return function(controller) {
        var flxSimulationResults = new kony.ui.FlexContainer({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "clipBounds": true,
            "focusSkin": "sknfbfcfc",
            "height": "110dp",
            "id": "flxSimulationResults",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "left": "0dp",
            "isModalContainer": false,
            "skin": "sknfbfcfc",
            "top": "0dp",
            "width": "100%"
        }, {}, {
            "onHover": controller.AS_FlexContainer_f3dc2a5d57ea42c1bb50329e6d22fff2
        });
        flxSimulationResults.setDefaultUnit(kony.flex.DP);
        var flxRecommendedLoans = new kony.ui.FlexContainer({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "bottom": "0dp",
            "clipBounds": true,
            "focusSkin": "sknflxffffffBorder3ebaed7Radius4px",
            "id": "flxRecommendedLoans",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "left": "10dp",
            "isModalContainer": false,
            "right": "10dp",
            "skin": "sknflxffffffBorderd6dbe7Radius4px",
            "top": "10dp",
            "zIndex": 1
        }, {}, {});
        flxRecommendedLoans.setDefaultUnit(kony.flex.DP);
        var lblAPR = new kony.ui.Label({
            "id": "lblAPR",
            "isVisible": true,
            "right": "18dp",
            "skin": "sknlblLato9ca9ba11px",
            "text": "Label",
            "top": "45dp",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, {
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, {});
        var lblAPRValue = new kony.ui.Label({
            "id": "lblAPRValue",
            "isVisible": true,
            "right": "18dp",
            "skin": "sknLbl274060100PerKA",
            "text": "Label",
            "top": "65dp",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, {
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, {});
        var lblRate = new kony.ui.Label({
            "centerX": "50%",
            "id": "lblRate",
            "isVisible": true,
            "skin": "sknlblLato9ca9ba11px",
            "text": "Label",
            "top": "45dp",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, {
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, {});
        var lblRateValue = new kony.ui.Label({
            "centerX": "50%",
            "id": "lblRateValue",
            "isVisible": true,
            "skin": "sknLbl274060100PerKA",
            "text": "Label",
            "top": "65dp",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, {
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, {});
        var lblTerms = new kony.ui.Label({
            "id": "lblTerms",
            "isVisible": true,
            "left": "18dp",
            "skin": "sknlblLato9ca9ba11px",
            "text": "Label",
            "top": "45dp",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, {
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, {});
        var lblTermsValue = new kony.ui.Label({
            "id": "lblTermsValue",
            "isVisible": true,
            "left": 18,
            "skin": "sknLbl274060100PerKA",
            "text": "Label",
            "top": "65dp",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, {
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, {});
        var flxHeaderSimulationResults = new kony.ui.FlexContainer({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "bottom": "63dp",
            "clipBounds": true,
            "id": "flxHeaderSimulationResults",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "left": "0dp",
            "isModalContainer": false,
            "right": "0dp",
            "skin": "slFbox",
            "top": "0dp",
            "zIndex": 2
        }, {}, {});
        flxHeaderSimulationResults.setDefaultUnit(kony.flex.DP);
        var lblLoanType = new kony.ui.Label({
            "centerY": "50%",
            "id": "lblLoanType",
            "isVisible": true,
            "left": "18dp",
            "skin": "sknLbl274060100PerKA",
            "text": "Label",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, {
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, {});
        var lblApply = new kony.ui.Label({
            "centerY": "50%",
            "id": "lblApply",
            "isVisible": false,
            "onTouchEnd": controller.AS_Label_b794caa48e554f7daec958a878cef5f0,
            "right": "18dp",
            "skin": "sknlbl3ebaed100PerKA",
            "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmLoansDashboard.ApplyStaticMessage\")",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, {
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, {
            "hoverSkin": "sknLbl3ebaedHoverHandKA"
        });
        var lblSeperator = new kony.ui.Label({
            "bottom": "0dp",
            "height": "1dp",
            "id": "lblSeperator",
            "isVisible": true,
            "left": "18dp",
            "right": "18dp",
            "skin": "sknlblSeperatorBgD5D9DD",
            "zIndex": 2
        }, {
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, {});
        flxHeaderSimulationResults.add(lblLoanType, lblApply, lblSeperator);
        flxRecommendedLoans.add(lblAPR, lblAPRValue, lblRate, lblRateValue, lblTerms, lblTermsValue, flxHeaderSimulationResults);
        flxSimulationResults.add(flxRecommendedLoans);
        return flxSimulationResults;
    }
})