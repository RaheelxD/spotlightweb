define("userflxSegMFAConfigsController", {
    //Type your controller code here 
});
define("flxSegMFAConfigsControllerActions", {
    /*
        This is an auto generated file and any modifications to it may result in corruption of the action sequence.
    */
    AS_FlexContainer_b5840c35f5c64575896ea4c51eae05f4: function AS_FlexContainer_b5840c35f5c64575896ea4c51eae05f4(eventobject, context) {
        var self = this;
        this.executeOnParent("showSelectedRow");
    },
});
define("flxSegMFAConfigsController", ["userflxSegMFAConfigsController", "flxSegMFAConfigsControllerActions"], function() {
    var controller = require("userflxSegMFAConfigsController");
    var controllerActions = ["flxSegMFAConfigsControllerActions"];
    return kony.visualizer.mixinControllerActions(controller, controllerActions);
});
