define("flxJobs", function() {
    return function(controller) {
        var flxJobs = new kony.ui.FlexContainer({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "clipBounds": true,
            "height": "70dp",
            "id": "flxJobs",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "left": "0dp",
            "isModalContainer": false,
            "skin": "sknflxffffffop100",
            "top": "0dp",
            "width": "100%"
        }, {}, {
            "hoverSkin": "sknfbfcfc"
        });
        flxJobs.setDefaultUnit(kony.flex.DP);
        var flxJobName = new kony.ui.FlexContainer({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "clipBounds": true,
            "height": "100%",
            "id": "flxJobName",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "left": "30dp",
            "isModalContainer": false,
            "skin": "slFbox",
            "top": "0dp",
            "width": "18%"
        }, {}, {});
        flxJobName.setDefaultUnit(kony.flex.DP);
        var lblJobName = new kony.ui.Label({
            "centerY": "35%",
            "id": "lblJobName",
            "isVisible": true,
            "left": "0px",
            "right": "85%",
            "skin": "sknlblLatoRegular484b5213px",
            "text": "Daily P2P update",
            "width": "100%",
            "zIndex": 1
        }, {
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, {});
        var flxTransferTag = new kony.ui.FlexContainer({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "bottom": "10px",
            "clipBounds": true,
            "height": "20px",
            "id": "flxTransferTag",
            "isVisible": true,
            "layoutType": kony.flex.FLOW_HORIZONTAL,
            "left": "0px",
            "isModalContainer": false,
            "skin": "sknflxJobtag5769803PX",
            "width": "100dp"
        }, {}, {});
        flxTransferTag.setDefaultUnit(kony.flex.DP);
        var lblContent2 = new kony.ui.Label({
            "centerX": "50%",
            "centerY": "45%",
            "id": "lblContent2",
            "isVisible": true,
            "skin": "sknCustomerTypeTag",
            "text": "P2P Transfers",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, {
            "contentAlignment": constants.CONTENT_ALIGN_CENTER,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, {});
        flxTransferTag.add(lblContent2);
        flxJobName.add(lblJobName, flxTransferTag);
        var flxRunStatus = new kony.ui.FlexContainer({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "clipBounds": true,
            "height": "100%",
            "id": "flxRunStatus",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "left": "22%",
            "isModalContainer": false,
            "skin": "slFbox",
            "top": "0dp",
            "width": "100px",
            "zIndex": 1
        }, {}, {});
        flxRunStatus.setDefaultUnit(kony.flex.DP);
        var lblRunStatus = new kony.ui.Label({
            "centerY": "35%",
            "id": "lblRunStatus",
            "isVisible": true,
            "left": "20px",
            "skin": "sknlblLatoRegular484b5213px",
            "text": "Completed",
            "width": "70px",
            "zIndex": 1
        }, {
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, {});
        var fonticonRunStatus = new kony.ui.Label({
            "centerY": "35%",
            "id": "fonticonRunStatus",
            "isVisible": true,
            "left": "0dp",
            "skin": "sknFontIconCompleted",
            "text": "",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, {
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, {});
        flxRunStatus.add(lblRunStatus, fonticonRunStatus);
        var lblLastRun = new kony.ui.Label({
            "centerY": "35%",
            "id": "lblLastRun",
            "isVisible": true,
            "left": "40%",
            "skin": "sknlblLatoRegular484b5213px",
            "text": "24/10/2019 12:00 AM",
            "width": "20%",
            "zIndex": 1
        }, {
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, {});
        var lblNextRun = new kony.ui.Label({
            "centerY": "35%",
            "id": "lblNextRun",
            "isVisible": true,
            "left": "60%",
            "skin": "sknlblLatoRegular484b5213px",
            "text": "24/10/2019 12:00 AM",
            "width": "20%",
            "zIndex": 1
        }, {
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, {});
        var flxStatus = new kony.ui.FlexContainer({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "clipBounds": true,
            "height": "100%",
            "id": "flxStatus",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "isModalContainer": false,
            "right": "75px",
            "skin": "slFbox",
            "top": "0dp",
            "width": "12%",
            "zIndex": 1
        }, {}, {});
        flxStatus.setDefaultUnit(kony.flex.DP);
        var lblUsersStatus = new kony.ui.Label({
            "centerY": "35%",
            "id": "lblUsersStatus",
            "isVisible": true,
            "left": "20px",
            "skin": "sknlblLatoRegular484b5213px",
            "text": "Inactive",
            "width": "70px",
            "zIndex": 1
        }, {
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, {});
        var fontIconStatusImg = new kony.ui.Label({
            "centerY": "35%",
            "id": "fontIconStatusImg",
            "isVisible": true,
            "left": "0dp",
            "skin": "sknfontIconInactive",
            "text": "",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, {
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, {});
        flxStatus.add(lblUsersStatus, fontIconStatusImg);
        var flxOptions = new kony.ui.FlexContainer({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "centerY": "35%",
            "clipBounds": true,
            "height": "25dp",
            "id": "flxOptions",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "isModalContainer": false,
            "onClick": controller.AS_FlexContainer_ba1244050ad84eada65511db4c7120cb,
            "right": "40px",
            "skin": "slFbox",
            "width": "25px",
            "zIndex": 10
        }, {}, {
            "hoverSkin": "sknflxffffffop100Border424242Radius100px"
        });
        flxOptions.setDefaultUnit(kony.flex.DP);
        var lblFontIconOptions = new kony.ui.Label({
            "centerX": "45%",
            "centerY": "50%",
            "height": "12dp",
            "id": "lblFontIconOptions",
            "isVisible": true,
            "skin": "sknFontIconOptionMenu",
            "text": "",
            "width": "10dp",
            "zIndex": 1
        }, {
            "contentAlignment": constants.CONTENT_ALIGN_CENTER,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, {});
        flxOptions.add(lblFontIconOptions);
        var lblSeperator = new kony.ui.Label({
            "bottom": 0,
            "height": "1px",
            "id": "lblSeperator",
            "isVisible": true,
            "left": "20dp",
            "right": "20dp",
            "skin": "sknlblSeperator",
            "text": ".",
            "zIndex": 1
        }, {
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, {});
        flxJobs.add(flxJobName, flxRunStatus, lblLastRun, lblNextRun, flxStatus, flxOptions, lblSeperator);
        return flxJobs;
    }
})