define("flxAlertsLanguageData", function() {
    return function(controller) {
        var flxAlertsLanguageData = new kony.ui.FlexContainer({
            "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
            "clipBounds": true,
            "id": "flxAlertsLanguageData",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "left": "0dp",
            "isModalContainer": false,
            "skin": "sknFlxFFFFFF100O",
            "top": "0dp",
            "width": "100%"
        }, {}, {
            "hoverSkin": "sknfbfcfcWithoutPointer"
        });
        flxAlertsLanguageData.setDefaultUnit(kony.flex.DP);
        var flxLanguageRowContainer = new kony.ui.FlexContainer({
            "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
            "bottom": "15dp",
            "clipBounds": true,
            "id": "flxLanguageRowContainer",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "left": "0dp",
            "isModalContainer": false,
            "right": "0dp",
            "skin": "slFbox",
            "top": "0dp",
            "zIndex": 1
        }, {}, {});
        flxLanguageRowContainer.setDefaultUnit(kony.flex.DP);
        var flxAddLanguage = new kony.ui.FlexContainer({
            "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
            "clipBounds": true,
            "id": "flxAddLanguage",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "left": "0dp",
            "isModalContainer": false,
            "skin": "slFbox",
            "top": "5dp",
            "width": "19%",
            "zIndex": 1
        }, {}, {});
        flxAddLanguage.setDefaultUnit(kony.flex.DP);
        var lblAddAlertTypeSegLanguage = new kony.ui.Label({
            "id": "lblAddAlertTypeSegLanguage",
            "isVisible": true,
            "left": "10dp",
            "right": "20dp",
            "skin": "sknLbl485c7513pxHover",
            "text": "Label",
            "top": "15dp",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, {
            "contentAlignment": constants.CONTENT_ALIGN_TOP_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, {});
        var lstBoxSelectLanguage = new kony.ui.ListBox({
            "focusSkin": "defListBoxFocus",
            "height": "30dp",
            "id": "lstBoxSelectLanguage",
            "isVisible": false,
            "left": "20dp",
            "masterData": [
                ["lb1", "English(US)"],
                ["lb2", "English(UK)"],
                ["lb3", "Spanish"],
                ["lb4", "French"],
                ["lb0", "Select"]
            ],
            "right": "0dp",
            "selectedKey": "lb0",
            "skin": "sknlbxBgffffffBorderc1c9ceRadius3Px",
            "top": "10dp",
            "zIndex": 1
        }, {
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [3, 0, 0, 0],
            "paddingInPixel": false
        }, {
            "multiSelect": false
        });
        flxAddLanguage.add(lblAddAlertTypeSegLanguage, lstBoxSelectLanguage);
        var flxAddDisplayName = new kony.ui.FlexContainer({
            "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
            "clipBounds": true,
            "id": "flxAddDisplayName",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "left": "21%",
            "isModalContainer": false,
            "skin": "slFbox",
            "top": "5dp",
            "width": "23%",
            "zIndex": 1
        }, {}, {});
        flxAddDisplayName.setDefaultUnit(kony.flex.DP);
        var lblAddAlertTypeSegDisplayName = new kony.ui.Label({
            "id": "lblAddAlertTypeSegDisplayName",
            "isVisible": true,
            "left": "20dp",
            "right": "20dp",
            "skin": "sknLbl485c7513pxHover",
            "text": "Label",
            "top": "15dp",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, {
            "contentAlignment": constants.CONTENT_ALIGN_TOP_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, {});
        var tbxDisplayName = new kony.ui.TextBox2({
            "autoCapitalize": constants.TEXTBOX_AUTO_CAPITALIZE_NONE,
            "focusSkin": "defTextBoxFocus",
            "height": "35dp",
            "id": "tbxDisplayName",
            "isVisible": false,
            "keyBoardStyle": constants.TEXTBOX_KEY_BOARD_STYLE_DEFAULT,
            "left": "20dp",
            "maxTextLength": 30,
            "placeholder": "Display name",
            "right": "0dp",
            "secureTextEntry": false,
            "skin": "skntxtbx484b5214px",
            "textInputMode": constants.TEXTBOX_INPUT_MODE_ANY,
            "top": "10dp",
            "zIndex": 1
        }, {
            "containerHeightMode": constants.TEXTBOX_FONT_METRICS_DRIVEN_HEIGHT,
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [3, 0, 0, 0],
            "paddingInPixel": false
        }, {
            "autoCorrect": false,
            "placeholderSkin": "defTextBoxPlaceholder"
        });
        flxAddDisplayName.add(lblAddAlertTypeSegDisplayName, tbxDisplayName);
        var flxAddDescription = new kony.ui.FlexContainer({
            "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
            "clipBounds": true,
            "id": "flxAddDescription",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "left": "44%",
            "isModalContainer": false,
            "right": "80dp",
            "skin": "slFbox",
            "top": "0dp",
            "zIndex": 1
        }, {}, {});
        flxAddDescription.setDefaultUnit(kony.flex.DP);
        var lblAddAlertTypeSegDescription = new kony.ui.Label({
            "id": "lblAddAlertTypeSegDescription",
            "isVisible": true,
            "left": "20dp",
            "right": "30dp",
            "skin": "sknLbl485c7513pxHover",
            "text": "Label",
            "top": "15dp",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, {
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, {});
        var flxDescription = new kony.ui.FlexContainer({
            "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
            "clipBounds": true,
            "id": "flxDescription",
            "isVisible": false,
            "layoutType": kony.flex.FREE_FORM,
            "left": "0dp",
            "isModalContainer": false,
            "skin": "slFbox",
            "top": "0dp",
            "width": "100%"
        }, {}, {});
        flxDescription.setDefaultUnit(kony.flex.DP);
        var lblDescCount = new kony.ui.Label({
            "id": "lblDescCount",
            "isVisible": false,
            "right": "10dp",
            "skin": "sknLbl485c7513pxHover",
            "text": "0/500",
            "top": "0dp",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, {
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, {});
        var tbxDescription = new kony.ui.TextArea2({
            "autoCapitalize": constants.TEXTAREA_AUTO_CAPITALIZE_NONE,
            "focusSkin": "defTextAreaFocus",
            "height": "65dp",
            "id": "tbxDescription",
            "isVisible": true,
            "keyBoardStyle": constants.TEXTAREA_KEY_BOARD_STYLE_DEFAULT,
            "left": "20dp",
            "maxTextLength": 150,
            "numberOfVisibleLines": 3,
            "placeholder": "Write description here",
            "skin": "skntxtAreaLato35475f14Px",
            "textInputMode": constants.TEXTAREA_INPUT_MODE_ANY,
            "top": "15dp",
            "width": "95%",
            "zIndex": 1
        }, {
            "contentAlignment": constants.CONTENT_ALIGN_TOP_LEFT,
            "padding": [2, 2, 2, 2],
            "paddingInPixel": false
        }, {
            "autoCorrect": false,
            "placeholderSkin": "defTextAreaPlaceholder"
        });
        flxDescription.add(lblDescCount, tbxDescription);
        flxAddDescription.add(lblAddAlertTypeSegDescription, flxDescription);
        var flxDelete = new kony.ui.FlexContainer({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "clipBounds": true,
            "height": "20dp",
            "id": "flxDelete",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "isModalContainer": false,
            "right": "35dp",
            "skin": "slFbox",
            "top": "10dp",
            "width": "25dp",
            "zIndex": 1
        }, {}, {
            "hoverSkin": "sknCursor"
        });
        flxDelete.setDefaultUnit(kony.flex.DP);
        var lblIconDeleteLang = new kony.ui.Label({
            "centerX": "50%",
            "id": "lblIconDeleteLang",
            "isVisible": true,
            "skin": "sknIcon20px",
            "text": "",
            "top": "0dp",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, {
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, {});
        flxDelete.add(lblIconDeleteLang);
        flxLanguageRowContainer.add(flxAddLanguage, flxAddDisplayName, flxAddDescription, flxDelete);
        var lblSeprator = new kony.ui.Label({
            "bottom": "0dp",
            "height": "1dp",
            "id": "lblSeprator",
            "isVisible": true,
            "left": "0dp",
            "right": "0dp",
            "skin": "sknSeparator",
            "text": "-",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, {
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, {});
        flxAlertsLanguageData.add(flxLanguageRowContainer, lblSeprator);
        return flxAlertsLanguageData;
    }
})