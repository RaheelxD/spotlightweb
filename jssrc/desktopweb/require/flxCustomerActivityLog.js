define("flxCustomerActivityLog", function() {
    return function(controller) {
        var flxCustomerActivityLog = new kony.ui.FlexContainer({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "clipBounds": false,
            "height": "50px",
            "id": "flxCustomerActivityLog",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "isModalContainer": false,
            "skin": "slFbox"
        }, {}, {
            "hoverSkin": "sknfbfcfc"
        });
        flxCustomerActivityLog.setDefaultUnit(kony.flex.DP);
        var flxCustomerActivityLogWrapper = new kony.ui.FlexContainer({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "clipBounds": false,
            "height": "50px",
            "id": "flxCustomerActivityLogWrapper",
            "isVisible": true,
            "layoutType": kony.flex.FLOW_HORIZONTAL,
            "left": "0px",
            "isModalContainer": false,
            "right": "35px",
            "skin": "slFbox",
            "top": "0",
            "width": "1568dp"
        }, {}, {});
        flxCustomerActivityLogWrapper.setDefaultUnit(kony.flex.DP);
        var lblModuleName = new kony.ui.Label({
            "centerY": "50%",
            "id": "lblModuleName",
            "isVisible": true,
            "left": "35px",
            "skin": "sknlblLatoBold35475f14px",
            "text": "module name",
            "width": "7%",
            "zIndex": 1
        }, {
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, {});
        var lblLogType = new kony.ui.Label({
            "centerY": "50%",
            "id": "lblLogType",
            "isVisible": true,
            "left": "0.70%",
            "skin": "sknlblLatoBold35475f14px",
            "text": "Log Type",
            "width": "7%",
            "zIndex": 1
        }, {
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, {});
        var lblActivityType = new kony.ui.Label({
            "centerY": "50%",
            "id": "lblActivityType",
            "isVisible": true,
            "left": "0.70%",
            "skin": "sknlblLatoBold35475f14px",
            "text": "create",
            "top": "0%",
            "width": "7%",
            "zIndex": 1
        }, {
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, {});
        var lblDescription = new kony.ui.Label({
            "centerY": "50%",
            "id": "lblDescription",
            "isVisible": true,
            "left": "0.70%",
            "right": "0dp",
            "skin": "sknlblLatoBold35475f14px",
            "text": "johndoe@gmail.com",
            "width": "10%",
            "zIndex": 1
        }, {
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, {});
        var lblDateAndTime = new kony.ui.Label({
            "centerY": "50%",
            "id": "lblDateAndTime",
            "isVisible": true,
            "left": "0.70%",
            "skin": "sknlblLatoBold35475f14px",
            "text": "+91-1234-567",
            "width": "7%",
            "zIndex": 1
        }, {
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, {});
        var flxStatus = new kony.ui.FlexContainer({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "clipBounds": true,
            "height": "100%",
            "id": "flxStatus",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "left": "0.70%",
            "isModalContainer": false,
            "skin": "slFbox",
            "top": "0dp",
            "width": "3.30%",
            "zIndex": 1
        }, {}, {});
        flxStatus.setDefaultUnit(kony.flex.DP);
        var imgStatus = new kony.ui.Image2({
            "centerY": "50%",
            "height": "8px",
            "id": "imgStatus",
            "isVisible": false,
            "left": "0dp",
            "skin": "slImage",
            "src": "active_circle2x.png",
            "width": "8px",
            "zIndex": 1
        }, {
            "imageScaleMode": constants.IMAGE_SCALE_MODE_MAINTAIN_ASPECT_RATIO,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, {});
        var lblStatus = new kony.ui.Label({
            "centerY": "49%",
            "id": "lblStatus",
            "isVisible": true,
            "left": "15px",
            "skin": "sknlblLatoBold35475f14px",
            "text": "Active",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, {
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, {});
        var lblIconStatus = new kony.ui.Label({
            "centerY": "49%",
            "height": "15dp",
            "id": "lblIconStatus",
            "isVisible": true,
            "left": "0dp",
            "skin": "sknIcon13pxGreen",
            "text": "",
            "top": "1dp",
            "width": "15dp",
            "zIndex": 1
        }, {
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, {});
        flxStatus.add(imgStatus, lblStatus, lblIconStatus);
        var lblChannel = new kony.ui.Label({
            "centerY": "50%",
            "id": "lblChannel",
            "isVisible": true,
            "left": "0.70%",
            "skin": "sknlblLatoBold35475f14px",
            "text": "bryanvn",
            "width": "7%",
            "zIndex": 1
        }, {
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, {});
        var lblIPAddress = new kony.ui.Label({
            "centerY": "50%",
            "id": "lblIPAddress",
            "isVisible": true,
            "left": "0.70%",
            "skin": "sknlblLatoBold35475f14px",
            "text": "role name",
            "width": "7%",
            "zIndex": 1
        }, {
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, {});
        var lblDevice = new kony.ui.Label({
            "centerY": "50%",
            "id": "lblDevice",
            "isVisible": true,
            "left": "0.70%",
            "right": 0,
            "skin": "sknlblLatoBold35475f14px",
            "text": "create",
            "top": "0%",
            "width": "7%",
            "zIndex": 1
        }, {
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, {});
        var lblOS = new kony.ui.Label({
            "centerY": "50%",
            "id": "lblOS",
            "isVisible": true,
            "left": "0.70%",
            "skin": "sknlblLatoBold35475f14px",
            "text": "bry",
            "width": "7%",
            "zIndex": 1
        }, {
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, {});
        var lblReferenceId = new kony.ui.Label({
            "centerY": "50%",
            "id": "lblReferenceId",
            "isVisible": true,
            "left": "0.70%",
            "skin": "sknlblLatoBold35475f14px",
            "text": "role name",
            "width": "7%",
            "zIndex": 1
        }, {
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, {});
        var lblErrorCode = new kony.ui.Label({
            "centerY": "50%",
            "id": "lblErrorCode",
            "isVisible": true,
            "left": "0.70%",
            "skin": "sknlblLatoBold35475f14px",
            "text": "module name",
            "width": "7%",
            "zIndex": 1
        }, {
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, {});
        var lblMFAType = new kony.ui.Label({
            "centerY": "50%",
            "id": "lblMFAType",
            "isVisible": true,
            "left": "0.70%",
            "skin": "sknlblLatoBold35475f14px",
            "text": "MFA Type",
            "width": "7%",
            "zIndex": 1
        }, {
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, {});
        flxCustomerActivityLogWrapper.add(lblModuleName, lblLogType, lblActivityType, lblDescription, lblDateAndTime, flxStatus, lblChannel, lblIPAddress, lblDevice, lblOS, lblReferenceId, lblErrorCode, lblMFAType);
        var lblSeperator = new kony.ui.Label({
            "bottom": "0px",
            "height": "1px",
            "id": "lblSeperator",
            "isVisible": true,
            "left": "0px",
            "right": "0px",
            "skin": "sknlblSeperator",
            "text": ".",
            "width": "1568px",
            "zIndex": 1
        }, {
            "contentAlignment": constants.CONTENT_ALIGN_TOP_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, {});
        flxCustomerActivityLog.add(flxCustomerActivityLogWrapper, lblSeperator);
        return flxCustomerActivityLog;
    }
})