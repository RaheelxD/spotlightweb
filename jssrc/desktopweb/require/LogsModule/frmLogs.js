define("LogsModule/frmLogs", function() {
    return function(controller) {
        function addWidgetsfrmLogs() {
            this.setDefaultUnit(kony.flex.DP);
            var flxMain = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": false,
                "height": "900dp",
                "id": "flxMain",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "slFbox0b7aaa2e3bfa340",
                "top": "0dp",
                "width": "100%",
                "zIndex": 1
            }, {}, {});
            flxMain.setDefaultUnit(kony.flex.DP);
            var flxLeftPannel = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "100%",
                "id": "flxLeftPannel",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "masterType": constants.MASTER_TYPE_USERWIDGET,
                "isModalContainer": false,
                "skin": "sknLeftPannel0bb79e4bcca8e45",
                "top": "0dp",
                "width": "305px",
                "zIndex": 5
            }, {}, {});
            flxLeftPannel.setDefaultUnit(kony.flex.DP);
            var leftMenuNew = new com.adminConsole.navigation.leftMenuNew({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "height": "100%",
                "id": "leftMenuNew",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "masterType": constants.MASTER_TYPE_DEFAULT,
                "isModalContainer": false,
                "skin": "sknFlxBg003E75Op100",
                "top": "0dp",
                "width": "100%",
                "overrides": {
                    "flxBrandLogo": {
                        "top": undefined
                    },
                    "leftMenuNew": {
                        "bottom": "viz.val_cleared",
                        "centerX": "viz.val_cleared",
                        "centerY": "viz.val_cleared",
                        "height": "100%",
                        "left": "0dp",
                        "maxHeight": "viz.val_cleared",
                        "maxWidth": "viz.val_cleared",
                        "minHeight": "viz.val_cleared",
                        "minWidth": "viz.val_cleared",
                        "right": "viz.val_cleared",
                        "top": "0dp",
                        "width": "100%"
                    }
                }
            }, {
                "overrides": {}
            }, {
                "overrides": {}
            });
            flxLeftPannel.add(leftMenuNew);
            var flxRightPannel = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": false,
                "height": "100%",
                "id": "flxRightPannel",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "305px",
                "isModalContainer": false,
                "right": "0dp",
                "skin": "CopysknTextAreaNormal0i7deca82290347",
                "top": "0.00%",
                "zIndex": 5
            }, {}, {});
            flxRightPannel.setDefaultUnit(kony.flex.DP);
            var flxMainHeader = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "106dp",
                "id": "flxMainHeader",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0dp",
                "width": "100%",
                "zIndex": 3
            }, {}, {});
            flxMainHeader.setDefaultUnit(kony.flex.DP);
            var mainHeader = new com.adminConsole.header.mainHeader({
                "height": "106px",
                "id": "mainHeader",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": 0,
                "masterType": constants.MASTER_TYPE_DEFAULT,
                "isModalContainer": false,
                "skin": "CopyslFbox1",
                "top": 0,
                "width": "100%",
                "overrides": {
                    "btnAddNewOption": {
                        "isVisible": false
                    },
                    "btnDropdownList": {
                        "isVisible": false,
                        "text": "DOWNLOAD LIST"
                    },
                    "flxButtons": {
                        "isVisible": true
                    },
                    "flxHeaderSeperator": {
                        "isVisible": false
                    },
                    "flxHeaderUserOptions": {
                        "isVisible": true
                    },
                    "flxMainHeader": {
                        "zIndex": 4
                    },
                    "imgLogout": {
                        "src": "img_logout.png"
                    },
                    "lblHeading": {
                        "text": "Logs"
                    }
                }
            }, {
                "overrides": {}
            }, {
                "overrides": {}
            });
            flxMainHeader.add(mainHeader);
            var flxBreadCrumbs = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "20dp",
                "id": "flxBreadCrumbs",
                "isVisible": false,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "100px",
                "width": "100%",
                "zIndex": 4
            }, {}, {});
            flxBreadCrumbs.setDefaultUnit(kony.flex.DP);
            var breadcrumbs = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "bottom": "0px",
                "clipBounds": true,
                "height": "100%",
                "id": "breadcrumbs",
                "isVisible": true,
                "layoutType": kony.flex.FLOW_HORIZONTAL,
                "left": "0dp",
                "isModalContainer": false,
                "right": "0px",
                "skin": "slFbox",
                "top": "0dp",
                "zIndex": 100
            }, {}, {});
            breadcrumbs.setDefaultUnit(kony.flex.DP);
            var btnBackToMain = new kony.ui.Button({
                "centerY": "50%",
                "height": "20px",
                "id": "btnBackToMain",
                "isVisible": true,
                "left": "35px",
                "skin": "sknBtnLato11ABEB11Px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmLogsController.SYSTEM_LOGS\")",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "displayText": true,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var lblBreadcrumbsRight = new kony.ui.Label({
                "centerY": "50%",
                "height": "12dp",
                "id": "lblBreadcrumbsRight",
                "isVisible": true,
                "left": "10dp",
                "right": 8,
                "skin": "sknIcon12px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmLogs.lblBreadcrumbsRight\")",
                "width": "12dp",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var btnPreviousPage = new kony.ui.Button({
                "centerY": "50%",
                "height": "20px",
                "id": "btnPreviousPage",
                "isVisible": false,
                "left": "0",
                "skin": "sknBtnLato11ABEB11Px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.SecurityQuestions.SECURITYQUESTIONS\")",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "displayText": true,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var lblBreadcrumbsRight2 = new kony.ui.Label({
                "centerY": "50%",
                "height": "12dp",
                "id": "lblBreadcrumbsRight2",
                "isVisible": false,
                "left": "10dp",
                "right": 8,
                "skin": "sknIcon12px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmLogs.lblBreadcrumbsRight\")",
                "width": "12dp",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var btnPreviousPage1 = new kony.ui.Button({
                "centerY": "50%",
                "height": "20px",
                "id": "btnPreviousPage1",
                "isVisible": false,
                "left": "0",
                "skin": "sknBtnLato11ABEB11Px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.SecurityQuestions.SECURITYQUESTIONS\")",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "displayText": true,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var lblBreadcrumbsRight3 = new kony.ui.Label({
                "centerY": "50%",
                "height": "12dp",
                "id": "lblBreadcrumbsRight3",
                "isVisible": false,
                "left": "10dp",
                "right": 8,
                "skin": "sknIcon12px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmLogs.lblBreadcrumbsRight\")",
                "width": "12dp",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var flxCurrent = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "100%",
                "id": "flxCurrent",
                "isVisible": true,
                "layoutType": kony.flex.FLOW_HORIZONTAL,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "CopyslFbox0efc73ba91cad4b",
                "top": "0dp",
                "width": kony.flex.USE_PREFFERED_SIZE
            }, {}, {});
            flxCurrent.setDefaultUnit(kony.flex.DP);
            var lblCurrentScreen = new kony.ui.Label({
                "height": "20px",
                "id": "lblCurrentScreen",
                "isVisible": true,
                "left": "0dp",
                "skin": "sknlblLatoReg485c7512px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmLogsController.TRANSACTIONAL\")",
                "top": "0dp",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var lblBreadcrumbsDown = new kony.ui.Label({
                "centerY": "50%",
                "height": "12dp",
                "id": "lblBreadcrumbsDown",
                "isVisible": false,
                "left": "10dp",
                "right": 8,
                "skin": "sknIcon12px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmAlertsManagement.lblIconDescription\")",
                "width": "14dp",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxCurrent.add(lblCurrentScreen, lblBreadcrumbsDown);
            breadcrumbs.add(btnBackToMain, lblBreadcrumbsRight, btnPreviousPage, lblBreadcrumbsRight2, btnPreviousPage1, lblBreadcrumbsRight3, flxCurrent);
            flxBreadCrumbs.add(breadcrumbs);
            var flxLogsList = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "bottom": "35dp",
                "clipBounds": false,
                "id": "flxLogsList",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "35px",
                "isModalContainer": false,
                "right": "35px",
                "skin": "sknFlxFFFFFF1000",
                "top": "135px",
                "zIndex": 1
            }, {}, {});
            flxLogsList.setDefaultUnit(kony.flex.DP);
            var flxViewContainer2 = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "51dp",
                "id": "flxViewContainer2",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "isModalContainer": false,
                "right": "0px",
                "skin": "Copysknflxffffffop0e0ae7cd7f71746",
                "top": "0dp",
                "zIndex": 3
            }, {}, {});
            flxViewContainer2.setDefaultUnit(kony.flex.DP);
            var flxViewTabs = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                "clipBounds": true,
                "id": "flxViewTabs",
                "isVisible": true,
                "layoutType": kony.flex.FLOW_HORIZONTAL,
                "left": "20dp",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0px",
                "width": "100%",
                "zIndex": 4
            }, {}, {});
            flxViewTabs.setDefaultUnit(kony.flex.DP);
            var flxViewTab1 = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "50px",
                "id": "flxViewTab1",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "CopyslFbox0efc73ba91cad4b",
                "top": "0dp",
                "width": "137px",
                "zIndex": 1
            }, {}, {});
            flxViewTab1.setDefaultUnit(kony.flex.DP);
            var lblTabName1 = new kony.ui.Label({
                "height": "100%",
                "id": "lblTabName1",
                "isVisible": true,
                "left": "0dp",
                "skin": "sknLblTabUtilActive",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.Logs.defaultSystemLogs\")",
                "top": "0dp",
                "width": "100%",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_CENTER,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var flxTabUnderline1 = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "3dp",
                "id": "flxTabUnderline1",
                "isVisible": false,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0px",
                "isModalContainer": false,
                "skin": "sknflx11abebRadius3px",
                "top": "23px",
                "width": "140px",
                "zIndex": 1
            }, {}, {});
            flxTabUnderline1.setDefaultUnit(kony.flex.DP);
            flxTabUnderline1.add();
            flxViewTab1.add(lblTabName1, flxTabUnderline1);
            var flxViewTab2 = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "50px",
                "id": "flxViewTab2",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "20px",
                "isModalContainer": false,
                "skin": "CopyslFbox0efc73ba91cad4b",
                "top": "0dp",
                "width": "112px",
                "zIndex": 1
            }, {}, {});
            flxViewTab2.setDefaultUnit(kony.flex.DP);
            var lblTabName2 = new kony.ui.Label({
                "height": "100%",
                "id": "lblTabName2",
                "isVisible": true,
                "left": "0dp",
                "skin": "sknLblTabUtilActive",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.Logs.myFilteredLogs\")",
                "top": "0px",
                "width": "100%",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_CENTER,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var flxTabUnderline2 = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "4dp",
                "id": "flxTabUnderline2",
                "isVisible": false,
                "layoutType": kony.flex.FREE_FORM,
                "left": "-1px",
                "isModalContainer": false,
                "skin": "sknflx11abebRadius3px",
                "top": "23px",
                "width": "125px",
                "zIndex": 1
            }, {}, {});
            flxTabUnderline2.setDefaultUnit(kony.flex.DP);
            flxTabUnderline2.add();
            flxViewTab2.add(lblTabName2, flxTabUnderline2);
            flxViewTabs.add(flxViewTab1, flxViewTab2);
            var flxViewSeperator = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "bottom": 0,
                "clipBounds": true,
                "height": "1px",
                "id": "flxViewSeperator",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0px",
                "isModalContainer": false,
                "skin": "sknflxSepratorE1E5ED",
                "width": "100%",
                "zIndex": 2
            }, {}, {});
            flxViewSeperator.setDefaultUnit(kony.flex.DP);
            flxViewSeperator.add();
            flxViewContainer2.add(flxViewTabs, flxViewSeperator);
            var flxSystemLogsList = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "140px",
                "id": "flxSystemLogsList",
                "isVisible": true,
                "layoutType": kony.flex.FLOW_HORIZONTAL,
                "left": "20dp",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "75dp",
                "width": "100%",
                "zIndex": 1
            }, {}, {});
            flxSystemLogsList.setDefaultUnit(kony.flex.DP);
            var flxLogDefaultTabs1 = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "140px",
                "id": "flxLogDefaultTabs1",
                "isVisible": true,
                "layoutType": kony.flex.FLOW_HORIZONTAL,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0dp",
                "width": "30.80%"
            }, {}, {});
            flxLogDefaultTabs1.setDefaultUnit(kony.flex.DP);
            var LogDefaultTabs = new com.adminConsole.logs.LogDefaultTabs({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "height": "100%",
                "id": "LogDefaultTabs",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0px",
                "masterType": constants.MASTER_TYPE_DEFAULT,
                "isModalContainer": false,
                "skin": "sknflxffffffop100dbdbe6Radius3px",
                "top": "0px",
                "width": "100%",
                "overrides": {
                    "LogDefaultTabs": {
                        "bottom": "viz.val_cleared",
                        "centerX": "viz.val_cleared",
                        "centerY": "viz.val_cleared",
                        "height": "100%",
                        "left": "0px",
                        "maxHeight": "viz.val_cleared",
                        "maxWidth": "viz.val_cleared",
                        "minHeight": "viz.val_cleared",
                        "minWidth": "viz.val_cleared",
                        "right": "viz.val_cleared",
                        "top": "0px",
                        "width": "100%"
                    },
                    "imgLog": {
                        "isVisible": false,
                        "src": "icontransactional_2x.png"
                    },
                    "lblLog": {
                        "height": "60dp",
                        "left": "25dp",
                        "top": "30dp",
                        "width": "75dp"
                    },
                    "lblLogDesc": {
                        "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmLogsController.TransactionalLogTabDesc\")"
                    },
                    "lblLogHeading": {
                        "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmLogsController.TransactionalLogTabHeading\")"
                    }
                }
            }, {
                "overrides": {}
            }, {
                "overrides": {}
            });
            flxLogDefaultTabs1.add(LogDefaultTabs);
            var flxLogDefaultTabs2 = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "140px",
                "id": "flxLogDefaultTabs2",
                "isVisible": true,
                "layoutType": kony.flex.FLOW_HORIZONTAL,
                "left": "20px",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0dp",
                "width": "30.80%"
            }, {}, {});
            flxLogDefaultTabs2.setDefaultUnit(kony.flex.DP);
            var LogDefaultTabs2 = new com.adminConsole.logs.LogDefaultTabs({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "height": "100%",
                "id": "LogDefaultTabs2",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0px",
                "masterType": constants.MASTER_TYPE_DEFAULT,
                "isModalContainer": false,
                "skin": "sknflxffffffop100dbdbe6Radius3px",
                "top": "0px",
                "width": "100%",
                "overrides": {
                    "LogDefaultTabs": {
                        "bottom": "viz.val_cleared",
                        "centerX": "viz.val_cleared",
                        "centerY": "viz.val_cleared",
                        "height": "100%",
                        "left": "0px",
                        "maxHeight": "viz.val_cleared",
                        "maxWidth": "viz.val_cleared",
                        "minHeight": "viz.val_cleared",
                        "minWidth": "viz.val_cleared",
                        "right": "viz.val_cleared",
                        "top": "0px",
                        "width": "100%"
                    },
                    "imgLog": {
                        "isVisible": false,
                        "src": "iconadminconsole_2x.png"
                    },
                    "lblLog": {
                        "height": "60dp",
                        "left": "30dp",
                        "width": "70dp"
                    },
                    "lblLogDesc": {
                        "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmLogsController.AdminConsoleLogTabDesc\")"
                    },
                    "lblLogHeading": {
                        "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmLogsController.AdminConsoleLogTabHeading\")"
                    }
                }
            }, {
                "overrides": {}
            }, {
                "overrides": {}
            });
            flxLogDefaultTabs2.add(LogDefaultTabs2);
            var flxLogDefaultTabs3 = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "140px",
                "id": "flxLogDefaultTabs3",
                "isVisible": true,
                "layoutType": kony.flex.FLOW_HORIZONTAL,
                "left": "20px",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0dp",
                "width": "30.80%"
            }, {}, {});
            flxLogDefaultTabs3.setDefaultUnit(kony.flex.DP);
            var LogDefaultTabs3 = new com.adminConsole.logs.LogDefaultTabs({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "height": "100%",
                "id": "LogDefaultTabs3",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0%",
                "masterType": constants.MASTER_TYPE_DEFAULT,
                "isModalContainer": false,
                "skin": "sknflxffffffop100dbdbe6Radius3px",
                "top": "0",
                "width": "100%",
                "overrides": {
                    "LogDefaultTabs": {
                        "bottom": "viz.val_cleared",
                        "centerX": "viz.val_cleared",
                        "centerY": "viz.val_cleared",
                        "height": "100%",
                        "left": "0%",
                        "maxHeight": "viz.val_cleared",
                        "maxWidth": "viz.val_cleared",
                        "minHeight": "viz.val_cleared",
                        "minWidth": "viz.val_cleared",
                        "right": "viz.val_cleared",
                        "top": "0",
                        "width": "100%"
                    },
                    "imgLog": {
                        "src": "iconcustomeractivity_2x.png"
                    },
                    "lblLog": {
                        "height": "60dp",
                        "left": "30dp",
                        "width": "70dp"
                    },
                    "lblLogDesc": {
                        "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmLogsController.CustomerActivityTabDesc\")"
                    },
                    "lblLogHeading": {
                        "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmLogs.lblCustomerSpecificLogs\")"
                    }
                }
            }, {
                "overrides": {}
            }, {
                "overrides": {}
            });
            flxLogDefaultTabs3.add(LogDefaultTabs3);
            flxSystemLogsList.add(flxLogDefaultTabs1, flxLogDefaultTabs2, flxLogDefaultTabs3);
            var flxCustomLogsList = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                "clipBounds": false,
                "id": "flxCustomLogsList",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "20dp",
                "isModalContainer": false,
                "skin": "CopyslFbox0eab7b4ccac7d40",
                "top": "50dp",
                "width": "100%",
                "zIndex": 4
            }, {}, {});
            flxCustomLogsList.setDefaultUnit(kony.flex.DP);
            var flxAllTypes = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "26px",
                "id": "flxAllTypes",
                "isVisible": true,
                "layoutType": kony.flex.FLOW_HORIZONTAL,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "CopyslFbox0efc73ba91cad4b",
                "top": "20px",
                "width": "85px",
                "zIndex": 4
            }, {}, {});
            flxAllTypes.setDefaultUnit(kony.flex.DP);
            var lblName = new kony.ui.Label({
                "id": "lblName",
                "isVisible": true,
                "left": "0px",
                "skin": "CopysknlblLato0c696f51a15df4c",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmLogsController.All_Types\")",
                "top": "0px",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 4
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {
                "toolTip": "Transaction Logs"
            });
            var lblDown = new kony.ui.Label({
                "id": "lblDown",
                "isVisible": true,
                "left": "5dp",
                "skin": "sknIcon15px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmAlertsManagement.lblIconDescription\")",
                "top": "2dp",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 4
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var imgDown = new kony.ui.Image2({
                "height": "15dp",
                "id": "imgDown",
                "isVisible": false,
                "left": "5dp",
                "skin": "slImage",
                "src": "img_down_arrow.png",
                "top": "1px",
                "width": "15dp",
                "zIndex": 4
            }, {
                "imageScaleMode": constants.IMAGE_SCALE_MODE_MAINTAIN_ASPECT_RATIO,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxAllTypes.add(lblName, lblDown, imgDown);
            var flxTypesList = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                "clipBounds": true,
                "id": "flxTypesList",
                "isVisible": false,
                "layoutType": kony.flex.FLOW_VERTICAL,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "sknflxffffffop100dbdbe6Radius3px",
                "top": "43dp",
                "width": "120px",
                "zIndex": 200
            }, {}, {});
            flxTypesList.setDefaultUnit(kony.flex.DP);
            var flxOption4 = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "25dp",
                "id": "flxOption4",
                "isVisible": false,
                "layoutType": kony.flex.FLOW_HORIZONTAL,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "CopyslFbox0i3b050eeb8134e",
                "top": "5dp",
                "width": "100%",
                "zIndex": 1
            }, {}, {});
            flxOption4.setDefaultUnit(kony.flex.DP);
            var lblOption4 = new kony.ui.Label({
                "centerY": "50%",
                "id": "lblOption4",
                "isVisible": true,
                "left": "10dp",
                "skin": "sknlblLatoBold35475f14px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmLogsController.All_Types\")",
                "top": "4dp",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxOption4.add(lblOption4);
            var flxOption1 = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "25dp",
                "id": "flxOption1",
                "isVisible": true,
                "layoutType": kony.flex.FLOW_HORIZONTAL,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "CopyslFbox0i3b050eeb8134e",
                "top": "5dp",
                "width": "100%",
                "zIndex": 1
            }, {}, {});
            flxOption1.setDefaultUnit(kony.flex.DP);
            var lblOption1 = new kony.ui.Label({
                "centerY": "50%",
                "id": "lblOption1",
                "isVisible": true,
                "left": "10dp",
                "skin": "sknlblLatoBold35475f14px",
                "text": "Transactional",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxOption1.add(lblOption1);
            var flxOption2 = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "25dp",
                "id": "flxOption2",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "CopyslFbox0i3b050eeb8134e",
                "top": "5dp",
                "width": "100%",
                "zIndex": 1
            }, {}, {});
            flxOption2.setDefaultUnit(kony.flex.DP);
            var lblOption2 = new kony.ui.Label({
                "centerY": "50%",
                "id": "lblOption2",
                "isVisible": true,
                "left": "10dp",
                "skin": "sknlblLatoBold35475f14px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmLogs.lblOption2\")",
                "top": "4dp",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxOption2.add(lblOption2);
            var flxOption3 = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "25dp",
                "id": "flxOption3",
                "isVisible": true,
                "layoutType": kony.flex.FLOW_HORIZONTAL,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "CopyslFbox0i3b050eeb8134e",
                "top": "5dp",
                "width": "100%",
                "zIndex": 1
            }, {}, {});
            flxOption3.setDefaultUnit(kony.flex.DP);
            var lblOption3 = new kony.ui.Label({
                "centerY": "50%",
                "id": "lblOption3",
                "isVisible": true,
                "left": "10dp",
                "skin": "sknlblLatoBold35475f14px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmLogs.lblOption3\")",
                "top": "4dp",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxOption3.add(lblOption3);
            flxTypesList.add(flxOption4, flxOption1, flxOption2, flxOption3);
            var search = new com.adminConsole.header.search({
                "height": "48dp",
                "id": "search",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "masterType": constants.MASTER_TYPE_DEFAULT,
                "isModalContainer": false,
                "skin": "CopyslFbox2",
                "top": "18px",
                "width": "99%",
                "overrides": {
                    "flxSearchContainer": {
                        "top": "0dp"
                    },
                    "imgSearchCancel": {
                        "src": "close_blue.png"
                    },
                    "imgSearchIcon": {
                        "src": "search_1x.png"
                    },
                    "search": {
                        "top": "18px",
                        "width": "99%"
                    },
                    "tbxSearchBox": {
                        "placeholder": "Search by Log name"
                    }
                }
            }, {
                "overrides": {}
            }, {
                "overrides": {}
            });
            var flxScrollableList = new kony.ui.FlexScrollContainer({
                "allowHorizontalBounce": false,
                "allowVerticalBounce": true,
                "bounces": true,
                "clipBounds": false,
                "enableScrolling": true,
                "height": "355px",
                "horizontalScrollIndicator": true,
                "id": "flxScrollableList",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "-15dp",
                "pagingEnabled": false,
                "right": "0dp",
                "scrollDirection": kony.flex.SCROLL_VERTICAL,
                "skin": "slFSbox",
                "top": "91px",
                "verticalScrollIndicator": true,
                "zIndex": 100
            }, {}, {});
            flxScrollableList.setDefaultUnit(kony.flex.DP);
            var flxMainList = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                "bottom": "0px",
                "clipBounds": false,
                "id": "flxMainList",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "isModalContainer": false,
                "right": "0dp",
                "skin": "slFbox",
                "top": "0dp",
                "zIndex": 10
            }, {}, {});
            flxMainList.setDefaultUnit(kony.flex.DP);
            flxMainList.add();
            flxScrollableList.add(flxMainList);
            var flxNoRecords = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "300px",
                "id": "flxNoRecords",
                "isVisible": false,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "isModalContainer": false,
                "right": "35px",
                "skin": "sknflxffffffop100Bordercbcdd1Radius3px",
                "top": "91dp",
                "zIndex": 5
            }, {}, {});
            flxNoRecords.setDefaultUnit(kony.flex.DP);
            var rtxNoResultsFound = new kony.ui.RichText({
                "centerX": "50%",
                "centerY": "50%",
                "id": "rtxNoResultsFound",
                "isVisible": true,
                "skin": "sknRtxLato84939e12Px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmLogs.rtxNoResultsFound\")",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_CENTER,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxNoRecords.add(rtxNoResultsFound);
            flxCustomLogsList.add(flxAllTypes, flxTypesList, search, flxScrollableList, flxNoRecords);
            flxLogsList.add(flxViewContainer2, flxSystemLogsList, flxCustomLogsList);
            var flxCustomerActivityLog = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                "clipBounds": true,
                "id": "flxCustomerActivityLog",
                "isVisible": false,
                "layoutType": kony.flex.FLOW_VERTICAL,
                "left": "35px",
                "isModalContainer": false,
                "right": "35px",
                "skin": "sknflxffffffBorderd6dbe7Radius4px",
                "top": "130px",
                "zIndex": 1
            }, {}, {});
            flxCustomerActivityLog.setDefaultUnit(kony.flex.DP);
            var searchCustomer = new com.adminConsole.logs.searchCustomer({
                "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                "id": "searchCustomer",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "masterType": constants.MASTER_TYPE_DEFAULT,
                "isModalContainer": false,
                "skin": "slFbox0hfd18814fd664dCM",
                "top": "20dp",
                "width": "100%",
                "overrides": {
                    "flxFirstRow": {
                        "left": "20px",
                        "right": "20px",
                        "width": "viz.val_cleared",
                        "layoutType": kony.flex.FLOW_HORIZONTAL
                    },
                    "flxSearchCriteria1": {
                        "width": "32.20%"
                    },
                    "flxSearchCriteria2": {
                        "centerX": "viz.val_cleared",
                        "left": "15dp",
                        "width": "32.20%"
                    },
                    "flxSearchCriteria3": {
                        "left": "15dp",
                        "right": "viz.val_cleared",
                        "width": "32.20%"
                    },
                    "imgSearchError": {
                        "isVisible": false,
                        "left": "169dp",
                        "src": "error_2x.png",
                        "top": "0dp"
                    },
                    "lblDefaultSearchHeader": {
                        "left": "20px"
                    },
                    "lblSearchError": {
                        "isVisible": false,
                        "left": "185px"
                    },
                    "lblSearchParam1": {
                        "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmCustomerManagementController.Customer_Name\")"
                    },
                    "lblSearchParam2": {
                        "i18n_text": "kony.i18n.getLocalizedString(\"i18n.customerSearch.MemberID\")",
                        "right": "viz.val_cleared"
                    },
                    "lblSearchParam3": {
                        "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmCustomerManagementController.User_ID\")"
                    },
                    "searchCustomer": {
                        "left": "0dp",
                        "top": "20dp",
                        "width": "100%"
                    },
                    "txtSearchParam2": {
                        "i18n_placeholder": "kony.i18n.getLocalizedString(\"i18n.customerSearch.MemberID\")"
                    },
                    "txtSearchParam3": {
                        "i18n_placeholder": "kony.i18n.getLocalizedString(\"i18n.frmCustomerManagementController.User_ID\")"
                    }
                }
            }, {
                "overrides": {}
            }, {
                "overrides": {}
            });
            var flxSearchButtonsWrapper = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "80dp",
                "id": "flxSearchButtonsWrapper",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "20dp",
                "width": "100%",
                "zIndex": 1
            }, {}, {});
            flxSearchButtonsWrapper.setDefaultUnit(kony.flex.DP);
            var flxSearchSeperator = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "1px",
                "id": "flxSearchSeperator",
                "isVisible": false,
                "layoutType": kony.flex.FREE_FORM,
                "left": "35px",
                "isModalContainer": false,
                "right": "35px",
                "skin": "sknflxd6dbe7",
                "top": "0dp",
                "zIndex": 1
            }, {}, {});
            flxSearchSeperator.setDefaultUnit(kony.flex.DP);
            flxSearchSeperator.add();
            var flxSearchButtons = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "80dp",
                "id": "flxSearchButtons",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "20px",
                "isModalContainer": false,
                "right": "20px",
                "skin": "slFbox",
                "top": "0dp",
                "zIndex": 1
            }, {}, {});
            flxSearchButtons.setDefaultUnit(kony.flex.DP);
            var searchButtons = new com.adminConsole.common.commonButtons({
                "height": "80px",
                "id": "searchButtons",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "masterType": constants.MASTER_TYPE_DEFAULT,
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0dp",
                "width": "100%",
                "overrides": {
                    "btnCancel": {
                        "bottom": "viz.val_cleared",
                        "centerY": "50%",
                        "height": "40dp",
                        "text": "RESET"
                    },
                    "btnNext": {
                        "isVisible": false
                    },
                    "btnSave": {
                        "text": "SEARCH"
                    }
                }
            }, {
                "overrides": {}
            }, {
                "overrides": {}
            });
            flxSearchButtons.add(searchButtons);
            flxSearchButtonsWrapper.add(flxSearchSeperator, flxSearchButtons);
            flxCustomerActivityLog.add(searchCustomer, flxSearchButtonsWrapper);
            var flxModifySearch = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "20px",
                "id": "flxModifySearch",
                "isVisible": false,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "128px",
                "width": "100%",
                "zIndex": 100
            }, {}, {});
            flxModifySearch.setDefaultUnit(kony.flex.DP);
            var modifySearch = new com.adminConsole.search.modifySearch({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "bottom": "10px",
                "height": "20px",
                "id": "modifySearch",
                "isVisible": true,
                "layoutType": kony.flex.FLOW_HORIZONTAL,
                "left": "0dp",
                "masterType": constants.MASTER_TYPE_DEFAULT,
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0dp",
                "width": "100%",
                "overrides": {
                    "modifySearch": {
                        "bottom": "10px"
                    }
                }
            }, {
                "overrides": {}
            }, {
                "overrides": {}
            });
            flxModifySearch.add(modifySearch);
            var flxHeaderDropdown = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                "clipBounds": true,
                "id": "flxHeaderDropdown",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "isModalContainer": false,
                "right": "35dp",
                "skin": "slFbox",
                "top": "20dp",
                "width": "400dp",
                "zIndex": 5
            }, {}, {});
            flxHeaderDropdown.setDefaultUnit(kony.flex.DP);
            var dropdownMainHeader = new com.adminConsole.common.dropdownMainHeader({
                "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                "id": "dropdownMainHeader",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "masterType": constants.MASTER_TYPE_DEFAULT,
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0dp",
                "width": "100%",
                "overrides": {
                    "flxDropdown": {
                        "isVisible": false
                    },
                    "imgUpArrow": {
                        "src": "uparrow_2x.png"
                    }
                }
            }, {
                "overrides": {}
            }, {
                "overrides": {}
            });
            flxHeaderDropdown.add(dropdownMainHeader);
            var flxMainContent = new kony.ui.FlexContainer({
                "bottom": "0dp",
                "clipBounds": false,
                "id": "flxMainContent",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0px",
                "isModalContainer": false,
                "right": "0px",
                "top": "170px",
                "zIndex": 1
            }, {}, {});
            flxMainContent.setDefaultUnit(kony.flex.DP);
            var flxCustomerSearchResults = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                "clipBounds": true,
                "id": "flxCustomerSearchResults",
                "isVisible": false,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "30dp",
                "width": "100%"
            }, {}, {});
            flxCustomerSearchResults.setDefaultUnit(kony.flex.DP);
            var flxSearchResult = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                "bottom": "35px",
                "clipBounds": true,
                "id": "flxSearchResult",
                "isVisible": true,
                "layoutType": kony.flex.FLOW_VERTICAL,
                "left": "35px",
                "isModalContainer": false,
                "right": "35px",
                "skin": "sknflxffffffop100Bordercbcdd1Radius3px",
                "top": "0px",
                "zIndex": 1
            }, {}, {});
            flxSearchResult.setDefaultUnit(kony.flex.DP);
            var flxSearchResultExistingCustomerHeader = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "60px",
                "id": "flxSearchResultExistingCustomerHeader",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0px",
                "isModalContainer": false,
                "right": "0px",
                "skin": "slFbox",
                "top": "0",
                "zIndex": 2
            }, {}, {});
            flxSearchResultExistingCustomerHeader.setDefaultUnit(kony.flex.DP);
            var flxExistingUserHeader = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "60dp",
                "id": "flxExistingUserHeader",
                "isVisible": true,
                "layoutType": kony.flex.FLOW_HORIZONTAL,
                "left": "20px",
                "isModalContainer": false,
                "right": "20px",
                "skin": "slFbox",
                "top": "0px",
                "zIndex": 1
            }, {}, {});
            flxExistingUserHeader.setDefaultUnit(kony.flex.DP);
            var flxExistingCustomerName = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "100%",
                "id": "flxExistingCustomerName",
                "isVisible": true,
                "layoutType": kony.flex.FLOW_HORIZONTAL,
                "left": "3%",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0dp",
                "width": "20%",
                "zIndex": 1
            }, {}, {});
            flxExistingCustomerName.setDefaultUnit(kony.flex.DP);
            var lblExistingCustomerName = new kony.ui.Label({
                "centerY": "50%",
                "id": "lblExistingCustomerName",
                "isVisible": true,
                "left": 0,
                "skin": "sknlblLato696c7312px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.permission.NAME\")",
                "top": 0,
                "width": "38px",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {
                "hoverSkin": "sknlblLato00000011px"
            });
            var flxSortExistingCustomerName = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "centerY": "50%",
                "clipBounds": true,
                "height": "20dp",
                "id": "flxSortExistingCustomerName",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "slFbox",
                "width": "20px",
                "zIndex": 1
            }, {}, {});
            flxSortExistingCustomerName.setDefaultUnit(kony.flex.DP);
            var lblSortExistingCustomerName = new kony.ui.Label({
                "centerY": "50%",
                "height": "15dp",
                "id": "lblSortExistingCustomerName",
                "isVisible": true,
                "left": "0dp",
                "skin": "sknIcon15px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmAlertsManagement.lblSortName\")",
                "width": "15dp",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxSortExistingCustomerName.add(lblSortExistingCustomerName);
            flxExistingCustomerName.add(lblExistingCustomerName, flxSortExistingCustomerName);
            var flxExistingCustomerUsername = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "100%",
                "id": "flxExistingCustomerUsername",
                "isVisible": true,
                "layoutType": kony.flex.FLOW_HORIZONTAL,
                "left": "0%",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0dp",
                "width": "20%",
                "zIndex": 1
            }, {}, {});
            flxExistingCustomerUsername.setDefaultUnit(kony.flex.DP);
            var lblExistingCustomerUsername = new kony.ui.Label({
                "centerY": "50%",
                "id": "lblExistingCustomerUsername",
                "isVisible": true,
                "left": "0px",
                "skin": "sknlblLato696c7312px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmLogs.customerUserName\")",
                "top": 0,
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {
                "hoverSkin": "sknlblLato00000011px"
            });
            var flxSortExistingCustomerUsername = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "centerY": "50%",
                "clipBounds": true,
                "height": "20dp",
                "id": "flxSortExistingCustomerUsername",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "slFbox",
                "width": "20px",
                "zIndex": 1
            }, {}, {});
            flxSortExistingCustomerUsername.setDefaultUnit(kony.flex.DP);
            var lblSortExistingCustomerUsername = new kony.ui.Label({
                "centerY": "50%",
                "height": "15dp",
                "id": "lblSortExistingCustomerUsername",
                "isVisible": true,
                "left": "0dp",
                "skin": "sknIcon15px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmAlertsManagement.lblSortName\")",
                "width": "15dp",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxSortExistingCustomerUsername.add(lblSortExistingCustomerUsername);
            flxExistingCustomerUsername.add(lblExistingCustomerUsername, flxSortExistingCustomerUsername);
            var flxExistingCustomerUserID = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "100%",
                "id": "flxExistingCustomerUserID",
                "isVisible": true,
                "layoutType": kony.flex.FLOW_HORIZONTAL,
                "left": "0%",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0dp",
                "width": "16%",
                "zIndex": 1
            }, {}, {});
            flxExistingCustomerUserID.setDefaultUnit(kony.flex.DP);
            var lblExistingCustomerUserId = new kony.ui.Label({
                "centerY": "50%",
                "id": "lblExistingCustomerUserId",
                "isVisible": true,
                "left": "0px",
                "skin": "sknlblLato696c7312px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmCustomerManagementController.RetailCustomerId\")",
                "top": 0,
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var flxSortExistingCustomerUserId = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "centerY": "50%",
                "clipBounds": true,
                "height": "20dp",
                "id": "flxSortExistingCustomerUserId",
                "isVisible": false,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "slFbox",
                "width": "20px",
                "zIndex": 1
            }, {}, {});
            flxSortExistingCustomerUserId.setDefaultUnit(kony.flex.DP);
            var imgSortExistingCustomerUserId = new kony.ui.Image2({
                "centerX": "50%",
                "centerY": "50%",
                "height": "13dp",
                "id": "imgSortExistingCustomerUserId",
                "isVisible": true,
                "skin": "slImage",
                "src": "sorting3x.png",
                "top": "0dp",
                "width": "13dp",
                "zIndex": 1
            }, {
                "imageScaleMode": constants.IMAGE_SCALE_MODE_MAINTAIN_ASPECT_RATIO,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxSortExistingCustomerUserId.add(imgSortExistingCustomerUserId);
            flxExistingCustomerUserID.add(lblExistingCustomerUserId, flxSortExistingCustomerUserId);
            var flxExistingCustomerContactNumber = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "100%",
                "id": "flxExistingCustomerContactNumber",
                "isVisible": true,
                "layoutType": kony.flex.FLOW_HORIZONTAL,
                "left": "0",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0dp",
                "width": "19%",
                "zIndex": 1
            }, {}, {});
            flxExistingCustomerContactNumber.setDefaultUnit(kony.flex.DP);
            var lblExistingCustomerContactNumber = new kony.ui.Label({
                "centerY": "50%",
                "id": "lblExistingCustomerContactNumber",
                "isVisible": true,
                "left": "0px",
                "skin": "sknlblLato696c7312px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmCustomerManagementController.CONTACT_NUMBER\")",
                "top": 0,
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var flxSortExistingCustomerContactNumber = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "centerY": "50%",
                "clipBounds": true,
                "height": "20dp",
                "id": "flxSortExistingCustomerContactNumber",
                "isVisible": false,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "slFbox",
                "width": "20px",
                "zIndex": 1
            }, {}, {});
            flxSortExistingCustomerContactNumber.setDefaultUnit(kony.flex.DP);
            var imgSortExistingCustomerContactNumber = new kony.ui.Image2({
                "centerX": "50%",
                "centerY": "50%",
                "height": "13dp",
                "id": "imgSortExistingCustomerContactNumber",
                "isVisible": true,
                "skin": "slImage",
                "src": "sorting3x.png",
                "top": "0dp",
                "width": "13dp",
                "zIndex": 1
            }, {
                "imageScaleMode": constants.IMAGE_SCALE_MODE_MAINTAIN_ASPECT_RATIO,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxSortExistingCustomerContactNumber.add(imgSortExistingCustomerContactNumber);
            flxExistingCustomerContactNumber.add(lblExistingCustomerContactNumber, flxSortExistingCustomerContactNumber);
            var flxExistingCustomerEmailId = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "100%",
                "id": "flxExistingCustomerEmailId",
                "isVisible": true,
                "layoutType": kony.flex.FLOW_HORIZONTAL,
                "left": "0",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0dp",
                "width": "25%",
                "zIndex": 1
            }, {}, {});
            flxExistingCustomerEmailId.setDefaultUnit(kony.flex.DP);
            var lblExistingCustomerEmailId = new kony.ui.Label({
                "centerY": "50%",
                "id": "lblExistingCustomerEmailId",
                "isVisible": true,
                "left": "0px",
                "skin": "sknlblLato696c7312px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.View.EMAILID\")",
                "top": 0,
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var flxSortExistingCustomerEmailId = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "centerY": "50%",
                "clipBounds": true,
                "height": "20dp",
                "id": "flxSortExistingCustomerEmailId",
                "isVisible": false,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "slFbox",
                "width": "20px",
                "zIndex": 1
            }, {}, {});
            flxSortExistingCustomerEmailId.setDefaultUnit(kony.flex.DP);
            var imgSortExistingCustomerEmailId = new kony.ui.Image2({
                "centerX": "50%",
                "centerY": "50%",
                "height": "13dp",
                "id": "imgSortExistingCustomerEmailId",
                "isVisible": true,
                "skin": "slImage",
                "src": "sorting3x.png",
                "top": "0dp",
                "width": "13dp",
                "zIndex": 1
            }, {
                "imageScaleMode": constants.IMAGE_SCALE_MODE_MAINTAIN_ASPECT_RATIO,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxSortExistingCustomerEmailId.add(imgSortExistingCustomerEmailId);
            flxExistingCustomerEmailId.add(lblExistingCustomerEmailId, flxSortExistingCustomerEmailId);
            flxExistingUserHeader.add(flxExistingCustomerName, flxExistingCustomerUsername, flxExistingCustomerUserID, flxExistingCustomerContactNumber, flxExistingCustomerEmailId);
            var lblUsersHeaderSeperator = new kony.ui.Label({
                "bottom": 0,
                "height": "1px",
                "id": "lblUsersHeaderSeperator",
                "isVisible": true,
                "left": "20px",
                "right": "20px",
                "skin": "sknLblHeaderLine",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmCSR.lblSeperator\")",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxSearchResultExistingCustomerHeader.add(flxExistingUserHeader, lblUsersHeaderSeperator);
            var flxSearchResultSegment = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                "clipBounds": true,
                "id": "flxSearchResultSegment",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0dp",
                "width": "100%",
                "zIndex": 1
            }, {}, {});
            flxSearchResultSegment.setDefaultUnit(kony.flex.DP);
            var searchResults = new com.adminConsole.common.listingSegmentLogs({
                "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                "id": "searchResults",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "masterType": constants.MASTER_TYPE_DEFAULT,
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0dp",
                "width": "100%",
                "overrides": {
                    "contextualMenu.imgOption1": {
                        "src": "imagedrag.png"
                    },
                    "contextualMenu.imgOption2": {
                        "src": "edit2x.png"
                    },
                    "contextualMenu.imgOption3": {
                        "src": "imagedrag.png"
                    },
                    "contextualMenu.imgOption4": {
                        "src": "deactive_2x.png"
                    },
                    "flxContextualMenu": {
                        "isVisible": false
                    }
                }
            }, {
                "overrides": {}
            }, {
                "overrides": {}
            });
            flxSearchResultSegment.add(searchResults);
            flxSearchResult.add(flxSearchResultExistingCustomerHeader, flxSearchResultSegment);
            flxCustomerSearchResults.add(flxSearchResult);
            var flxCustomerDetailedResults = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "bottom": "0dp",
                "clipBounds": false,
                "id": "flxCustomerDetailedResults",
                "isVisible": false,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "isModalContainer": false,
                "right": "35dp",
                "skin": "slFbox",
                "top": "0dp"
            }, {}, {});
            flxCustomerDetailedResults.setDefaultUnit(kony.flex.DP);
            var flxCustomerContent = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                "bottom": "35dp",
                "clipBounds": true,
                "id": "flxCustomerContent",
                "isVisible": true,
                "layoutType": kony.flex.FLOW_VERTICAL,
                "left": "40dp",
                "isModalContainer": false,
                "right": "10dp",
                "skin": "sknflxffffffOp0b",
                "top": "0dp"
            }, {}, {});
            flxCustomerContent.setDefaultUnit(kony.flex.DP);
            var flxHeader3 = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                "clipBounds": true,
                "id": "flxHeader3",
                "isVisible": true,
                "layoutType": kony.flex.FLOW_VERTICAL,
                "left": "20dp",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "20dp",
                "width": "100%"
            }, {}, {});
            flxHeader3.setDefaultUnit(kony.flex.DP);
            var lblCustomerName3 = new kony.ui.Label({
                "id": "lblCustomerName3",
                "isVisible": true,
                "left": "0px",
                "skin": "sknlblLato18px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmLogs.lblCustomerName3\")",
                "top": "0px",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var flxColumn = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                "clipBounds": true,
                "id": "flxColumn",
                "isVisible": true,
                "layoutType": kony.flex.FLOW_HORIZONTAL,
                "left": "0px",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "10dp",
                "width": "100%",
                "zIndex": 1
            }, {}, {});
            flxColumn.setDefaultUnit(kony.flex.DP);
            var lblUserHeading = new kony.ui.Label({
                "id": "lblUserHeading",
                "isVisible": true,
                "left": "0px",
                "skin": "sknlblLato5d6c7f12px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmLogs.customerUseName\")",
                "top": "0px",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var lblUserData = new kony.ui.Label({
                "id": "lblUserData",
                "isVisible": true,
                "left": "10px",
                "skin": "sknlblLatoBold35475f14px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.userwidgetmodel.lblData1\")",
                "top": "0px",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var lblCustomerIdHeading = new kony.ui.Label({
                "id": "lblCustomerIdHeading",
                "isVisible": true,
                "left": "35dp",
                "skin": "sknlblLato5d6c7f12px",
                "text": "CUSTOMER ID:",
                "top": "0px",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var lblCustomerIDData = new kony.ui.Label({
                "id": "lblCustomerIDData",
                "isVisible": true,
                "left": "10px",
                "skin": "sknlblLatoBold35475f14px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.userwidgetmodel.lblData1\")",
                "top": "0px",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxColumn.add(lblUserHeading, lblUserData, lblCustomerIdHeading, lblCustomerIDData);
            flxHeader3.add(lblCustomerName3, flxColumn);
            var flxActivityType3 = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "72dp",
                "id": "flxActivityType3",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "-1dp",
                "isModalContainer": false,
                "right": "20dp",
                "skin": "sknbor1pxe4e6ecbgf8f9faborrad3px",
                "top": "20dp",
                "width": "100%",
                "zIndex": 1
            }, {}, {});
            flxActivityType3.setDefaultUnit(kony.flex.DP);
            var flxViewActivity = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                "clipBounds": true,
                "id": "flxViewActivity",
                "isVisible": true,
                "layoutType": kony.flex.FLOW_VERTICAL,
                "left": "20dp",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0dp",
                "width": "200dp"
            }, {}, {});
            flxViewActivity.setDefaultUnit(kony.flex.DP);
            var lblCustomerType = new kony.ui.Label({
                "id": "lblCustomerType",
                "isVisible": true,
                "left": "0px",
                "skin": "slLabel0d20174dce8ea42",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmLogs.lblCustomerType\")",
                "top": "13px",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var flxType3 = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "40dp",
                "id": "flxType3",
                "isVisible": true,
                "layoutType": kony.flex.FLOW_HORIZONTAL,
                "left": "0px",
                "isModalContainer": false,
                "right": "7px",
                "skin": "slFbox",
                "top": "0px",
                "zIndex": 1
            }, {}, {});
            flxType3.setDefaultUnit(kony.flex.DP);
            var flxRadioExistingCustomer = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "centerY": "50%",
                "clipBounds": true,
                "height": "20dp",
                "id": "flxRadioExistingCustomer",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "-2dp",
                "isModalContainer": false,
                "skin": "sknCursor",
                "width": "20px",
                "zIndex": 1
            }, {}, {
                "hoverSkin": "sknCursor"
            });
            flxRadioExistingCustomer.setDefaultUnit(kony.flex.DP);
            var imgRadioExistingCustomer = new kony.ui.Image2({
                "centerX": "50%",
                "centerY": "50%",
                "height": "15dp",
                "id": "imgRadioExistingCustomer",
                "isVisible": true,
                "skin": "slImage",
                "src": "radio_selected.png",
                "width": "15px",
                "zIndex": 1
            }, {
                "imageScaleMode": constants.IMAGE_SCALE_MODE_MAINTAIN_ASPECT_RATIO,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxRadioExistingCustomer.add(imgRadioExistingCustomer);
            var lblRadioExistingCustomer = new kony.ui.Label({
                "centerY": "50%",
                "id": "lblRadioExistingCustomer",
                "isVisible": true,
                "left": "5dp",
                "skin": "slLabel0d20174dce8ea42",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmCustomerManagementController.Customer\")",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var flxRadioApplicant = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "centerY": "50%",
                "clipBounds": true,
                "height": "20dp",
                "id": "flxRadioApplicant",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "20px",
                "isModalContainer": false,
                "skin": "sknCursor",
                "width": "20px",
                "zIndex": 1
            }, {}, {
                "hoverSkin": "sknCursor"
            });
            flxRadioApplicant.setDefaultUnit(kony.flex.DP);
            var imgRadioApplicant = new kony.ui.Image2({
                "centerX": "50%",
                "centerY": "50%",
                "height": "15dp",
                "id": "imgRadioApplicant",
                "isVisible": true,
                "skin": "slImage",
                "src": "radio_notselected.png",
                "width": "15dp",
                "zIndex": 1
            }, {
                "imageScaleMode": constants.IMAGE_SCALE_MODE_MAINTAIN_ASPECT_RATIO,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxRadioApplicant.add(imgRadioApplicant);
            var lblRadioApplicant = new kony.ui.Label({
                "centerY": "50%",
                "id": "lblRadioApplicant",
                "isVisible": true,
                "left": "5dp",
                "skin": "slLabel0d20174dce8ea42",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmLogsController.CSR_Assist\")",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxType3.add(flxRadioExistingCustomer, lblRadioExistingCustomer, flxRadioApplicant, lblRadioApplicant);
            flxViewActivity.add(lblCustomerType, flxType3);
            var flxSearchFilter = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                "clipBounds": true,
                "id": "flxSearchFilter",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "isModalContainer": false,
                "right": "0dp",
                "skin": "slFbox",
                "top": "16dp",
                "width": "500dp"
            }, {}, {});
            flxSearchFilter.setDefaultUnit(kony.flex.DP);
            var flxSearchCustomerContainer = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "40px",
                "id": "flxSearchCustomerContainer",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "isModalContainer": false,
                "right": "60dp",
                "skin": "sknflxd5d9ddop100",
                "top": "0px",
                "width": "410px",
                "zIndex": 1
            }, {}, {});
            flxSearchCustomerContainer.setDefaultUnit(kony.flex.DP);
            var tbxCustomerSearchBox = new kony.ui.TextBox2({
                "autoCapitalize": constants.TEXTBOX_AUTO_CAPITALIZE_NONE,
                "height": "40dp",
                "id": "tbxCustomerSearchBox",
                "isVisible": true,
                "keyBoardStyle": constants.TEXTBOX_KEY_BOARD_STYLE_DEFAULT,
                "left": "5px",
                "onTouchStart": controller.AS_TextField_cfab06859f0f43eca99495891ee290d5,
                "i18n_placeholder": "kony.i18n.getLocalizedString(\"i18n.frmLogs.searchCustomerActivity\")",
                "right": "35px",
                "secureTextEntry": false,
                "skin": "sknSearchtbxConfigurationBundlesNormal",
                "textInputMode": constants.TEXTBOX_INPUT_MODE_ANY,
                "top": "0dp",
                "zIndex": 1
            }, {
                "containerHeightMode": constants.TEXTBOX_FONT_METRICS_DRIVEN_HEIGHT,
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [2, 0, 0, 0],
                "paddingInPixel": false
            }, {
                "autoCorrect": false
            });
            var flxClearCustomerSearchImage = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "bottom": "0dp",
                "clipBounds": true,
                "height": "40dp",
                "id": "flxClearCustomerSearchImage",
                "isVisible": false,
                "layoutType": kony.flex.FREE_FORM,
                "isModalContainer": false,
                "right": "35dp",
                "skin": "sknCursor",
                "top": "0dp",
                "width": "20dp",
                "zIndex": 1
            }, {}, {
                "hoverSkin": "sknCursor"
            });
            flxClearCustomerSearchImage.setDefaultUnit(kony.flex.DP);
            var fontIconCustomerCross = new kony.ui.Label({
                "centerX": "50%",
                "centerY": "50%",
                "id": "fontIconCustomerCross",
                "isVisible": true,
                "skin": "sknFontIconSearchCross16px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.userwidgetmodel.lblClearSearch\")",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxClearCustomerSearchImage.add(fontIconCustomerCross);
            var flxSearchIcon = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                "clipBounds": true,
                "id": "flxSearchIcon",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "isModalContainer": false,
                "right": "12px",
                "skin": "slFbox",
                "top": "0dp",
                "width": "20dp"
            }, {}, {
                "hoverSkin": "sknFlxPointer"
            });
            flxSearchIcon.setDefaultUnit(kony.flex.DP);
            var fontIconSearchCustomerImg = new kony.ui.Label({
                "bottom": "10dp",
                "id": "fontIconSearchCustomerImg",
                "isVisible": true,
                "right": "0dp",
                "skin": "sknFontIconSearchImg20px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.userwidgetmodel.lblSearchIcon\")",
                "top": "10dp",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxSearchIcon.add(fontIconSearchCustomerImg);
            flxSearchCustomerContainer.add(tbxCustomerSearchBox, flxClearCustomerSearchImage, flxSearchIcon);
            var flxCustomerFilterIcon = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "25dp",
                "id": "flxCustomerFilterIcon",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "isModalContainer": false,
                "right": "20dp",
                "skin": "slFbox",
                "top": "5dp",
                "width": "25dp"
            }, {}, {
                "hoverSkin": "sknCursor"
            });
            flxCustomerFilterIcon.setDefaultUnit(kony.flex.DP);
            var lblFilterCustomerLogs = new kony.ui.Label({
                "centerY": "50%",
                "height": "25dp",
                "id": "lblFilterCustomerLogs",
                "isVisible": true,
                "right": "0dp",
                "skin": "sknIconBlack25px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmAlertsManagement.lblIconChannels\")",
                "width": "25dp",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxCustomerFilterIcon.add(lblFilterCustomerLogs);
            flxSearchFilter.add(flxSearchCustomerContainer, flxCustomerFilterIcon);
            flxActivityType3.add(flxViewActivity, flxSearchFilter);
            var flxResults = new kony.ui.FlexScrollContainer({
                "allowHorizontalBounce": false,
                "allowVerticalBounce": true,
                "bottom": "20dp",
                "bounces": true,
                "clipBounds": false,
                "enableScrolling": true,
                "height": "335dp",
                "horizontalScrollIndicator": true,
                "id": "flxResults",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "pagingEnabled": false,
                "right": "35dp",
                "scrollDirection": kony.flex.SCROLL_HORIZONTAL,
                "skin": "slFSbox",
                "top": "10dp",
                "verticalScrollIndicator": true,
                "width": "100%"
            }, {}, {});
            flxResults.setDefaultUnit(kony.flex.DP);
            var flxInnerResults = new kony.ui.FlexContainer({
                "clipBounds": false,
                "height": "100%",
                "id": "flxInnerResults",
                "isVisible": true,
                "layoutType": kony.flex.FLOW_VERTICAL,
                "left": "0dp",
                "isModalContainer": false,
                "right": "20dp",
                "skin": "slFbox",
                "top": "0dp",
                "width": kony.flex.USE_PREFFERED_SIZE
            }, {}, {});
            flxInnerResults.setDefaultUnit(kony.flex.DP);
            var flxCustomerLogsHeader = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "50dp",
                "id": "flxCustomerLogsHeader",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "20dp",
                "isModalContainer": false,
                "skin": "sknFlxFFFFFF100O",
                "top": "0dp",
                "width": "3120dp"
            }, {}, {});
            flxCustomerLogsHeader.setDefaultUnit(kony.flex.DP);
            var flxLogs = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "100%",
                "id": "flxLogs",
                "isVisible": true,
                "layoutType": kony.flex.FLOW_HORIZONTAL,
                "left": "20dp",
                "isModalContainer": false,
                "right": "20dp",
                "skin": "slFbox",
                "top": "0dp",
                "zIndex": 1
            }, {}, {});
            flxLogs.setDefaultUnit(kony.flex.DP);
            var flxcusEvent = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "100%",
                "id": "flxcusEvent",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0",
                "width": "120dp"
            }, {}, {});
            flxcusEvent.setDefaultUnit(kony.flex.DP);
            var lblCusEvent = new kony.ui.Label({
                "centerY": "50%",
                "id": "lblCusEvent",
                "isVisible": true,
                "left": "0",
                "skin": "sknlblLato696c7312px",
                "text": "MODULE",
                "top": 0,
                "width": "100%"
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxcusEvent.add(lblCusEvent);
            var flxCusEventSubType = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "100%",
                "id": "flxCusEventSubType",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0",
                "width": "120dp"
            }, {}, {});
            flxCusEventSubType.setDefaultUnit(kony.flex.DP);
            var lblEventSubType = new kony.ui.Label({
                "centerY": "50%",
                "id": "lblEventSubType",
                "isVisible": true,
                "left": "0",
                "skin": "sknlblLato696c7312px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmLogs.lblActivityType\")",
                "top": 0,
                "width": "100%"
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxCusEventSubType.add(lblEventSubType);
            var flxAdminsName = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "100%",
                "id": "flxAdminsName",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0",
                "width": "120dp"
            }, {}, {});
            flxAdminsName.setDefaultUnit(kony.flex.DP);
            var lblAdminsName = new kony.ui.Label({
                "centerY": "50%",
                "id": "lblAdminsName",
                "isVisible": true,
                "left": "0",
                "skin": "sknlblLato696c7312px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmLogs.employeeUseName\")",
                "top": 0,
                "width": "100%"
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxAdminsName.add(lblAdminsName);
            var flxAdminsRole = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "100%",
                "id": "flxAdminsRole",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0",
                "width": "120dp"
            }, {}, {});
            flxAdminsRole.setDefaultUnit(kony.flex.DP);
            var lblAdminsRole = new kony.ui.Label({
                "centerY": "50%",
                "id": "lblAdminsRole",
                "isVisible": true,
                "left": "0",
                "skin": "sknlblLato696c7312px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmLogs.employeeRole\")",
                "top": 0,
                "width": "100%"
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxAdminsRole.add(lblAdminsRole);
            var flxDate = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "100%",
                "id": "flxDate",
                "isVisible": true,
                "layoutType": kony.flex.FLOW_HORIZONTAL,
                "left": "0",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0",
                "width": "120dp"
            }, {}, {});
            flxDate.setDefaultUnit(kony.flex.DP);
            var lblDate = new kony.ui.Label({
                "centerY": "50%",
                "id": "lblDate",
                "isVisible": true,
                "left": "0",
                "skin": "sknlblLato696c7312px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmCustomerManagement.lblDateAndTime\")",
                "top": 0,
                "width": kony.flex.USE_PREFFERED_SIZE
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var flxFilterDateAndTime = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "centerY": "50%",
                "clipBounds": true,
                "height": "20dp",
                "id": "flxFilterDateAndTime",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "CopyslFbox0efc73ba91cad4b",
                "width": "20px",
                "zIndex": 1
            }, {}, {});
            flxFilterDateAndTime.setDefaultUnit(kony.flex.DP);
            var imgFilterDateAndTime = new kony.ui.Image2({
                "centerX": "50%",
                "centerY": "50%",
                "height": "10dp",
                "id": "imgFilterDateAndTime",
                "isVisible": false,
                "skin": "slImage",
                "src": "sorting3x.png",
                "top": "0dp",
                "width": "10dp",
                "zIndex": 1
            }, {
                "imageScaleMode": constants.IMAGE_SCALE_MODE_MAINTAIN_ASPECT_RATIO,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var lblFilterDateAndTime = new kony.ui.Label({
                "centerY": "50%",
                "height": "15dp",
                "id": "lblFilterDateAndTime",
                "isVisible": true,
                "left": "5dp",
                "skin": "sknIcon15px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmAlertsManagement.lblSortName\")",
                "width": "15dp",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxFilterDateAndTime.add(imgFilterDateAndTime, lblFilterDateAndTime);
            flxDate.add(lblDate, flxFilterDateAndTime);
            var flxFromAccountNum = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "100%",
                "id": "flxFromAccountNum",
                "isVisible": true,
                "layoutType": kony.flex.FLOW_HORIZONTAL,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0dp",
                "width": "120dp"
            }, {}, {});
            flxFromAccountNum.setDefaultUnit(kony.flex.DP);
            var lblFromAccountNum = new kony.ui.Label({
                "centerY": "50%",
                "id": "lblFromAccountNum",
                "isVisible": true,
                "left": "0",
                "skin": "sknlblLato696c7312px",
                "text": "FROM ACCOUNT",
                "top": 0,
                "width": "100%"
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxFromAccountNum.add(lblFromAccountNum);
            var flxToAccountNum = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "100%",
                "id": "flxToAccountNum",
                "isVisible": true,
                "layoutType": kony.flex.FLOW_HORIZONTAL,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0dp",
                "width": "120dp"
            }, {}, {});
            flxToAccountNum.setDefaultUnit(kony.flex.DP);
            var lblToAccountNum = new kony.ui.Label({
                "centerY": "50%",
                "id": "lblToAccountNum",
                "isVisible": true,
                "left": "0",
                "skin": "sknlblLato696c7312px",
                "text": "TO ACCOUNT",
                "top": 0,
                "width": "100%"
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxToAccountNum.add(lblToAccountNum);
            var flxAmt = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "100%",
                "id": "flxAmt",
                "isVisible": true,
                "layoutType": kony.flex.FLOW_HORIZONTAL,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0dp",
                "width": "120dp"
            }, {}, {});
            flxAmt.setDefaultUnit(kony.flex.DP);
            var lblAmnt = new kony.ui.Label({
                "centerY": "50%",
                "id": "lblAmnt",
                "isVisible": true,
                "left": "0",
                "skin": "sknlblLato696c7312px",
                "text": "AMOUNT",
                "top": 0,
                "width": "100%"
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxAmt.add(lblAmnt);
            var flxCustCurrency = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "100%",
                "id": "flxCustCurrency",
                "isVisible": true,
                "layoutType": kony.flex.FLOW_HORIZONTAL,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0dp",
                "width": "120dp"
            }, {}, {});
            flxCustCurrency.setDefaultUnit(kony.flex.DP);
            var lblCustCurrency = new kony.ui.Label({
                "centerY": "50%",
                "id": "lblCustCurrency",
                "isVisible": true,
                "left": "0",
                "skin": "sknlblLato696c7312px",
                "text": "CURRENCY",
                "top": 0,
                "width": "200dp"
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxCustCurrency.add(lblCustCurrency);
            var flxRefNumber = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "100%",
                "id": "flxRefNumber",
                "isVisible": true,
                "layoutType": kony.flex.FLOW_HORIZONTAL,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0dp",
                "width": "120dp"
            }, {}, {});
            flxRefNumber.setDefaultUnit(kony.flex.DP);
            var lblRefNumber = new kony.ui.Label({
                "centerY": "50%",
                "id": "lblRefNumber",
                "isVisible": true,
                "left": "0",
                "skin": "sknlblLato696c7312px",
                "text": "REFERENCE NUMBER",
                "top": 0,
                "width": "100%"
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxRefNumber.add(lblRefNumber);
            var flxMaskedCardNo = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "100%",
                "id": "flxMaskedCardNo",
                "isVisible": false,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0",
                "width": "120dp"
            }, {}, {});
            flxMaskedCardNo.setDefaultUnit(kony.flex.DP);
            var lblMaskedCardNo = new kony.ui.Label({
                "centerY": "50%",
                "id": "lblMaskedCardNo",
                "isVisible": true,
                "left": "0",
                "skin": "sknlblLato696c7312px",
                "text": "MASKED CARD NUMBER",
                "top": 0,
                "width": "100%"
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxMaskedCardNo.add(lblMaskedCardNo);
            var flxPayeesName = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "100%",
                "id": "flxPayeesName",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0",
                "width": "120dp"
            }, {}, {});
            flxPayeesName.setDefaultUnit(kony.flex.DP);
            var lblPayeesName = new kony.ui.Label({
                "centerY": "50%",
                "id": "lblPayeesName",
                "isVisible": true,
                "left": "0",
                "skin": "sknlblLato696c7312px",
                "text": "PAYEE NAME / RECIPIENT NAME",
                "top": 0,
                "width": "100%"
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxPayeesName.add(lblPayeesName);
            var flxPayeesId = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "100%",
                "id": "flxPayeesId",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0",
                "width": "120dp"
            }, {}, {});
            flxPayeesId.setDefaultUnit(kony.flex.DP);
            var lblPayeesId = new kony.ui.Label({
                "centerY": "50%",
                "id": "lblPayeesId",
                "isVisible": true,
                "left": "0",
                "skin": "sknlblLato696c7312px",
                "text": "PAYEE ID",
                "top": 0,
                "width": "100%"
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxPayeesId.add(lblPayeesId);
            var flxPersonsId = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "100%",
                "id": "flxPersonsId",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0",
                "width": "120dp"
            }, {}, {});
            flxPersonsId.setDefaultUnit(kony.flex.DP);
            var lblPersonsId = new kony.ui.Label({
                "centerY": "50%",
                "id": "lblPersonsId",
                "isVisible": true,
                "left": "0",
                "skin": "sknlblLato696c7312px",
                "text": "PERSON ID",
                "top": 0,
                "width": "100%"
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxPersonsId.add(lblPersonsId);
            var flxAccount = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "100%",
                "id": "flxAccount",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0",
                "width": "120dp"
            }, {}, {});
            flxAccount.setDefaultUnit(kony.flex.DP);
            var lblAccount = new kony.ui.Label({
                "centerY": "50%",
                "id": "lblAccount",
                "isVisible": true,
                "left": "0",
                "skin": "sknlblLato696c7312px",
                "text": "ACCOUNT / RELATIONSHIP NUMBER",
                "top": 0,
                "width": "100%"
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxAccount.add(lblAccount);
            var flxLogStatus = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "100%",
                "id": "flxLogStatus",
                "isVisible": true,
                "layoutType": kony.flex.FLOW_HORIZONTAL,
                "left": "0",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0",
                "width": "120dp"
            }, {}, {});
            flxLogStatus.setDefaultUnit(kony.flex.DP);
            var lblLogStatus = new kony.ui.Label({
                "centerY": "50%",
                "id": "lblLogStatus",
                "isVisible": true,
                "left": "0",
                "skin": "sknlblLato696c7312px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.permission.STATUS\")",
                "top": 0,
                "width": kony.flex.USE_PREFFERED_SIZE
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var flxFilterStatus = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "centerY": "50%",
                "clipBounds": true,
                "height": "20dp",
                "id": "flxFilterStatus",
                "isVisible": false,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "slFbox",
                "width": "20px",
                "zIndex": 1
            }, {}, {});
            flxFilterStatus.setDefaultUnit(kony.flex.DP);
            var imgFilterStatus = new kony.ui.Image2({
                "centerX": "50%",
                "centerY": "50%",
                "height": "10dp",
                "id": "imgFilterStatus",
                "isVisible": false,
                "skin": "slImage",
                "src": "filter.png",
                "top": "0dp",
                "width": "10dp",
                "zIndex": 1
            }, {
                "imageScaleMode": constants.IMAGE_SCALE_MODE_MAINTAIN_ASPECT_RATIO,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var lblFilterStatus = new kony.ui.Label({
                "centerY": "50%",
                "height": "15dp",
                "id": "lblFilterStatus",
                "isVisible": true,
                "left": "5dp",
                "skin": "sknIcon15px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmAlertsManagement.lblIconChannels\")",
                "width": "15dp",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {
                "hoverSkin": "sknlblCursorFont"
            });
            flxFilterStatus.add(imgFilterStatus, lblFilterStatus);
            flxLogStatus.add(lblLogStatus, flxFilterStatus);
            var flxLogMFAType = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "100%",
                "id": "flxLogMFAType",
                "isVisible": true,
                "layoutType": kony.flex.FLOW_HORIZONTAL,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0dp",
                "width": "120dp"
            }, {}, {});
            flxLogMFAType.setDefaultUnit(kony.flex.DP);
            var lblLogMFAType = new kony.ui.Label({
                "centerY": "50%",
                "id": "lblLogMFAType",
                "isVisible": true,
                "left": "0",
                "skin": "sknlblLato696c7312px",
                "text": "MFA TYPE",
                "top": 0,
                "width": "100%"
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxLogMFAType.add(lblLogMFAType);
            var flxLogMFAKey = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "100%",
                "id": "flxLogMFAKey",
                "isVisible": true,
                "layoutType": kony.flex.FLOW_HORIZONTAL,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0dp",
                "width": "120dp"
            }, {}, {});
            flxLogMFAKey.setDefaultUnit(kony.flex.DP);
            var lblLogMFAKey = new kony.ui.Label({
                "centerY": "50%",
                "id": "lblLogMFAKey",
                "isVisible": true,
                "left": "0",
                "skin": "sknlblLato696c7312px",
                "text": "MFA SERVICE KEY",
                "top": 0,
                "width": "100%"
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxLogMFAKey.add(lblLogMFAKey);
            var flxMFAState = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "100%",
                "id": "flxMFAState",
                "isVisible": true,
                "layoutType": kony.flex.FLOW_HORIZONTAL,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0dp",
                "width": "120dp"
            }, {}, {});
            flxMFAState.setDefaultUnit(kony.flex.DP);
            var lblMFAState = new kony.ui.Label({
                "centerY": "50%",
                "id": "lblMFAState",
                "isVisible": true,
                "left": "0",
                "skin": "sknlblLato696c7312px",
                "text": "MFA STATE",
                "top": 0,
                "width": "100%"
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxMFAState.add(lblMFAState);
            var flxLogChannel = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "100%",
                "id": "flxLogChannel",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0",
                "width": "120dp"
            }, {}, {});
            flxLogChannel.setDefaultUnit(kony.flex.DP);
            var lblLogChannel = new kony.ui.Label({
                "centerY": "50%",
                "id": "lblLogChannel",
                "isVisible": true,
                "left": "0",
                "skin": "sknlblLato696c7312px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmCustomerManagementController.CHANNEL\")",
                "top": 0,
                "width": "100%"
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxLogChannel.add(lblLogChannel);
            var flxDeviceBrowser = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "100%",
                "id": "flxDeviceBrowser",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0",
                "width": "120dp"
            }, {}, {});
            flxDeviceBrowser.setDefaultUnit(kony.flex.DP);
            var lblDeviceBrowser = new kony.ui.Label({
                "centerY": "50%",
                "id": "lblDeviceBrowser",
                "isVisible": true,
                "left": "0",
                "skin": "sknlblLato696c7312px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmCustomerManagementController.DEVICEORBROWSER\")",
                "top": 0,
                "width": "100%"
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxDeviceBrowser.add(lblDeviceBrowser);
            var flxLogOS = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "100%",
                "id": "flxLogOS",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0",
                "width": "120dp"
            }, {}, {});
            flxLogOS.setDefaultUnit(kony.flex.DP);
            var lblLogOS = new kony.ui.Label({
                "centerY": "50%",
                "id": "lblLogOS",
                "isVisible": true,
                "left": "0",
                "skin": "sknlblLato696c7312px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmCustomerManagementController.OS\")",
                "top": 0,
                "width": "100%"
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxLogOS.add(lblLogOS);
            var flxDeviceID = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "100%",
                "id": "flxDeviceID",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0",
                "width": "120dp"
            }, {}, {});
            flxDeviceID.setDefaultUnit(kony.flex.DP);
            var lblDeviceId = new kony.ui.Label({
                "centerY": "50%",
                "id": "lblDeviceId",
                "isVisible": true,
                "left": "0",
                "skin": "sknlblLato696c7312px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmCustomerManagementController.DEVICEID\")",
                "top": 0,
                "width": "100%"
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxDeviceID.add(lblDeviceId);
            var flxLogIPAddress = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "100%",
                "id": "flxLogIPAddress",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0",
                "width": "120dp"
            }, {}, {});
            flxLogIPAddress.setDefaultUnit(kony.flex.DP);
            var lblLogIPAddress = new kony.ui.Label({
                "centerY": "50%",
                "id": "lblLogIPAddress",
                "isVisible": true,
                "left": "0",
                "skin": "sknlblLato696c7312px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmLogs.lblIPAddress\")",
                "top": 0,
                "width": "100%"
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxLogIPAddress.add(lblLogIPAddress);
            var flxOtherInfo = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "100%",
                "id": "flxOtherInfo",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0",
                "width": "120dp"
            }, {}, {});
            flxOtherInfo.setDefaultUnit(kony.flex.DP);
            var lblOtherInfo = new kony.ui.Label({
                "centerY": "50%",
                "id": "lblOtherInfo",
                "isVisible": true,
                "left": "0",
                "skin": "sknlblLato696c7312px",
                "text": "OTHER INFO",
                "top": 0,
                "width": "100%"
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxOtherInfo.add(lblOtherInfo);
            flxLogs.add(flxcusEvent, flxCusEventSubType, flxAdminsName, flxAdminsRole, flxDate, flxFromAccountNum, flxToAccountNum, flxAmt, flxCustCurrency, flxRefNumber, flxMaskedCardNo, flxPayeesName, flxPayeesId, flxPersonsId, flxAccount, flxLogStatus, flxLogMFAType, flxLogMFAKey, flxMFAState, flxLogChannel, flxDeviceBrowser, flxLogOS, flxDeviceID, flxLogIPAddress, flxOtherInfo);
            var lblSeperator = new kony.ui.Label({
                "bottom": 0,
                "height": "1px",
                "id": "lblSeperator",
                "isVisible": true,
                "left": "0dp",
                "right": "20dp",
                "skin": "sknLblTableHeaderLine",
                "text": ".",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxCustomerLogsHeader.add(flxLogs, lblSeperator);
            var flxDetailedResultSegment = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                "clipBounds": false,
                "id": "flxDetailedResultSegment",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "20dp",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0dp",
                "width": "3120dp",
                "zIndex": 1
            }, {}, {});
            flxDetailedResultSegment.setDefaultUnit(kony.flex.DP);
            var flxDynamicLogsScroll = new kony.ui.FlexScrollContainer({
                "allowHorizontalBounce": false,
                "allowVerticalBounce": true,
                "bounces": true,
                "clipBounds": true,
                "enableScrolling": true,
                "height": "240dp",
                "horizontalScrollIndicator": true,
                "id": "flxDynamicLogsScroll",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "pagingEnabled": false,
                "right": "0dp",
                "scrollDirection": kony.flex.SCROLL_VERTICAL,
                "skin": "slFSbox",
                "top": "0dp",
                "verticalScrollIndicator": true,
                "width": "100%"
            }, {}, {});
            flxDynamicLogsScroll.setDefaultUnit(kony.flex.DP);
            var segDynamicCustomerLogs = new kony.ui.SegmentedUI2({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "data": [{
                    "lblAccount": "Label",
                    "lblAdminName": "Label",
                    "lblAdminRole": "Label",
                    "lblAmount": "Label",
                    "lblChannel": "Label",
                    "lblCurrency": "Label",
                    "lblDate": "Label",
                    "lblDeviceBrowser": "Label",
                    "lblDeviceId": "Label",
                    "lblEvent": "Label",
                    "lblEventSubType": "Label",
                    "lblFromAccount": "Label",
                    "lblIPAddress": "Label",
                    "lblIconStatus": "",
                    "lblMFAServiceKey": "Label",
                    "lblMFAState": "Label",
                    "lblMFAType": "Label",
                    "lblMaskedCardNo": "Label",
                    "lblOs": "Label",
                    "lblOtherInfo": "Label",
                    "lblPayeeId": "Label",
                    "lblPayeeName": "Label",
                    "lblPersonId": "Label",
                    "lblRefNumber": "Label",
                    "lblSeperator": ".",
                    "lblStatus": "Label",
                    "lblToAccount": "Label"
                }, {
                    "lblAccount": "Label",
                    "lblAdminName": "Label",
                    "lblAdminRole": "Label",
                    "lblAmount": "Label",
                    "lblChannel": "Label",
                    "lblCurrency": "Label",
                    "lblDate": "Label",
                    "lblDeviceBrowser": "Label",
                    "lblDeviceId": "Label",
                    "lblEvent": "Label",
                    "lblEventSubType": "Label",
                    "lblFromAccount": "Label",
                    "lblIPAddress": "Label",
                    "lblIconStatus": "",
                    "lblMFAServiceKey": "Label",
                    "lblMFAState": "Label",
                    "lblMFAType": "Label",
                    "lblMaskedCardNo": "Label",
                    "lblOs": "Label",
                    "lblOtherInfo": "Label",
                    "lblPayeeId": "Label",
                    "lblPayeeName": "Label",
                    "lblPersonId": "Label",
                    "lblRefNumber": "Label",
                    "lblSeperator": ".",
                    "lblStatus": "Label",
                    "lblToAccount": "Label"
                }, {
                    "lblAccount": "Label",
                    "lblAdminName": "Label",
                    "lblAdminRole": "Label",
                    "lblAmount": "Label",
                    "lblChannel": "Label",
                    "lblCurrency": "Label",
                    "lblDate": "Label",
                    "lblDeviceBrowser": "Label",
                    "lblDeviceId": "Label",
                    "lblEvent": "Label",
                    "lblEventSubType": "Label",
                    "lblFromAccount": "Label",
                    "lblIPAddress": "Label",
                    "lblIconStatus": "",
                    "lblMFAServiceKey": "Label",
                    "lblMFAState": "Label",
                    "lblMFAType": "Label",
                    "lblMaskedCardNo": "Label",
                    "lblOs": "Label",
                    "lblOtherInfo": "Label",
                    "lblPayeeId": "Label",
                    "lblPayeeName": "Label",
                    "lblPersonId": "Label",
                    "lblRefNumber": "Label",
                    "lblSeperator": ".",
                    "lblStatus": "Label",
                    "lblToAccount": "Label"
                }, {
                    "lblAccount": "Label",
                    "lblAdminName": "Label",
                    "lblAdminRole": "Label",
                    "lblAmount": "Label",
                    "lblChannel": "Label",
                    "lblCurrency": "Label",
                    "lblDate": "Label",
                    "lblDeviceBrowser": "Label",
                    "lblDeviceId": "Label",
                    "lblEvent": "Label",
                    "lblEventSubType": "Label",
                    "lblFromAccount": "Label",
                    "lblIPAddress": "Label",
                    "lblIconStatus": "",
                    "lblMFAServiceKey": "Label",
                    "lblMFAState": "Label",
                    "lblMFAType": "Label",
                    "lblMaskedCardNo": "Label",
                    "lblOs": "Label",
                    "lblOtherInfo": "Label",
                    "lblPayeeId": "Label",
                    "lblPayeeName": "Label",
                    "lblPersonId": "Label",
                    "lblRefNumber": "Label",
                    "lblSeperator": ".",
                    "lblStatus": "Label",
                    "lblToAccount": "Label"
                }, {
                    "lblAccount": "Label",
                    "lblAdminName": "Label",
                    "lblAdminRole": "Label",
                    "lblAmount": "Label",
                    "lblChannel": "Label",
                    "lblCurrency": "Label",
                    "lblDate": "Label",
                    "lblDeviceBrowser": "Label",
                    "lblDeviceId": "Label",
                    "lblEvent": "Label",
                    "lblEventSubType": "Label",
                    "lblFromAccount": "Label",
                    "lblIPAddress": "Label",
                    "lblIconStatus": "",
                    "lblMFAServiceKey": "Label",
                    "lblMFAState": "Label",
                    "lblMFAType": "Label",
                    "lblMaskedCardNo": "Label",
                    "lblOs": "Label",
                    "lblOtherInfo": "Label",
                    "lblPayeeId": "Label",
                    "lblPayeeName": "Label",
                    "lblPersonId": "Label",
                    "lblRefNumber": "Label",
                    "lblSeperator": ".",
                    "lblStatus": "Label",
                    "lblToAccount": "Label"
                }, {
                    "lblAccount": "Label",
                    "lblAdminName": "Label",
                    "lblAdminRole": "Label",
                    "lblAmount": "Label",
                    "lblChannel": "Label",
                    "lblCurrency": "Label",
                    "lblDate": "Label",
                    "lblDeviceBrowser": "Label",
                    "lblDeviceId": "Label",
                    "lblEvent": "Label",
                    "lblEventSubType": "Label",
                    "lblFromAccount": "Label",
                    "lblIPAddress": "Label",
                    "lblIconStatus": "",
                    "lblMFAServiceKey": "Label",
                    "lblMFAState": "Label",
                    "lblMFAType": "Label",
                    "lblMaskedCardNo": "Label",
                    "lblOs": "Label",
                    "lblOtherInfo": "Label",
                    "lblPayeeId": "Label",
                    "lblPayeeName": "Label",
                    "lblPersonId": "Label",
                    "lblRefNumber": "Label",
                    "lblSeperator": ".",
                    "lblStatus": "Label",
                    "lblToAccount": "Label"
                }, {
                    "lblAccount": "Label",
                    "lblAdminName": "Label",
                    "lblAdminRole": "Label",
                    "lblAmount": "Label",
                    "lblChannel": "Label",
                    "lblCurrency": "Label",
                    "lblDate": "Label",
                    "lblDeviceBrowser": "Label",
                    "lblDeviceId": "Label",
                    "lblEvent": "Label",
                    "lblEventSubType": "Label",
                    "lblFromAccount": "Label",
                    "lblIPAddress": "Label",
                    "lblIconStatus": "",
                    "lblMFAServiceKey": "Label",
                    "lblMFAState": "Label",
                    "lblMFAType": "Label",
                    "lblMaskedCardNo": "Label",
                    "lblOs": "Label",
                    "lblOtherInfo": "Label",
                    "lblPayeeId": "Label",
                    "lblPayeeName": "Label",
                    "lblPersonId": "Label",
                    "lblRefNumber": "Label",
                    "lblSeperator": ".",
                    "lblStatus": "Label",
                    "lblToAccount": "Label"
                }, {
                    "lblAccount": "Label",
                    "lblAdminName": "Label",
                    "lblAdminRole": "Label",
                    "lblAmount": "Label",
                    "lblChannel": "Label",
                    "lblCurrency": "Label",
                    "lblDate": "Label",
                    "lblDeviceBrowser": "Label",
                    "lblDeviceId": "Label",
                    "lblEvent": "Label",
                    "lblEventSubType": "Label",
                    "lblFromAccount": "Label",
                    "lblIPAddress": "Label",
                    "lblIconStatus": "",
                    "lblMFAServiceKey": "Label",
                    "lblMFAState": "Label",
                    "lblMFAType": "Label",
                    "lblMaskedCardNo": "Label",
                    "lblOs": "Label",
                    "lblOtherInfo": "Label",
                    "lblPayeeId": "Label",
                    "lblPayeeName": "Label",
                    "lblPersonId": "Label",
                    "lblRefNumber": "Label",
                    "lblSeperator": ".",
                    "lblStatus": "Label",
                    "lblToAccount": "Label"
                }, {
                    "lblAccount": "Label",
                    "lblAdminName": "Label",
                    "lblAdminRole": "Label",
                    "lblAmount": "Label",
                    "lblChannel": "Label",
                    "lblCurrency": "Label",
                    "lblDate": "Label",
                    "lblDeviceBrowser": "Label",
                    "lblDeviceId": "Label",
                    "lblEvent": "Label",
                    "lblEventSubType": "Label",
                    "lblFromAccount": "Label",
                    "lblIPAddress": "Label",
                    "lblIconStatus": "",
                    "lblMFAServiceKey": "Label",
                    "lblMFAState": "Label",
                    "lblMFAType": "Label",
                    "lblMaskedCardNo": "Label",
                    "lblOs": "Label",
                    "lblOtherInfo": "Label",
                    "lblPayeeId": "Label",
                    "lblPayeeName": "Label",
                    "lblPersonId": "Label",
                    "lblRefNumber": "Label",
                    "lblSeperator": ".",
                    "lblStatus": "Label",
                    "lblToAccount": "Label"
                }, {
                    "lblAccount": "Label",
                    "lblAdminName": "Label",
                    "lblAdminRole": "Label",
                    "lblAmount": "Label",
                    "lblChannel": "Label",
                    "lblCurrency": "Label",
                    "lblDate": "Label",
                    "lblDeviceBrowser": "Label",
                    "lblDeviceId": "Label",
                    "lblEvent": "Label",
                    "lblEventSubType": "Label",
                    "lblFromAccount": "Label",
                    "lblIPAddress": "Label",
                    "lblIconStatus": "",
                    "lblMFAServiceKey": "Label",
                    "lblMFAState": "Label",
                    "lblMFAType": "Label",
                    "lblMaskedCardNo": "Label",
                    "lblOs": "Label",
                    "lblOtherInfo": "Label",
                    "lblPayeeId": "Label",
                    "lblPayeeName": "Label",
                    "lblPersonId": "Label",
                    "lblRefNumber": "Label",
                    "lblSeperator": ".",
                    "lblStatus": "Label",
                    "lblToAccount": "Label"
                }],
                "groupCells": false,
                "height": "1024dp",
                "id": "segDynamicCustomerLogs",
                "isVisible": true,
                "left": "0dp",
                "needPageIndicator": true,
                "pageOffDotImage": "pageoffdot.png",
                "pageOnDotImage": "pageondot.png",
                "retainSelection": false,
                "right": "20dp",
                "rowFocusSkin": "seg2Focus",
                "rowSkin": "seg2Normal",
                "rowTemplate": "flxDynamicLogs",
                "sectionHeaderSkin": "sliPhoneSegmentHeader",
                "selectionBehavior": constants.SEGUI_DEFAULT_BEHAVIOR,
                "separatorRequired": false,
                "showScrollbars": false,
                "top": "0dp",
                "viewType": constants.SEGUI_VIEW_TYPE_TABLEVIEW,
                "widgetDataMap": {
                    "flxDynamicLogs": "flxDynamicLogs",
                    "flxLogs": "flxLogs",
                    "flxOtherInfo": "flxOtherInfo",
                    "flxStatus": "flxStatus",
                    "lblAccount": "lblAccount",
                    "lblAdminName": "lblAdminName",
                    "lblAdminRole": "lblAdminRole",
                    "lblAmount": "lblAmount",
                    "lblChannel": "lblChannel",
                    "lblCurrency": "lblCurrency",
                    "lblDate": "lblDate",
                    "lblDeviceBrowser": "lblDeviceBrowser",
                    "lblDeviceId": "lblDeviceId",
                    "lblEvent": "lblEvent",
                    "lblEventSubType": "lblEventSubType",
                    "lblFromAccount": "lblFromAccount",
                    "lblIPAddress": "lblIPAddress",
                    "lblIconStatus": "lblIconStatus",
                    "lblMFAServiceKey": "lblMFAServiceKey",
                    "lblMFAState": "lblMFAState",
                    "lblMFAType": "lblMFAType",
                    "lblMaskedCardNo": "lblMaskedCardNo",
                    "lblOs": "lblOs",
                    "lblOtherInfo": "lblOtherInfo",
                    "lblPayeeId": "lblPayeeId",
                    "lblPayeeName": "lblPayeeName",
                    "lblPersonId": "lblPersonId",
                    "lblRefNumber": "lblRefNumber",
                    "lblSeperator": "lblSeperator",
                    "lblStatus": "lblStatus",
                    "lblToAccount": "lblToAccount"
                }
            }, {
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxDynamicLogsScroll.add(segDynamicCustomerLogs);
            flxDetailedResultSegment.add(flxDynamicLogsScroll);
            flxInnerResults.add(flxCustomerLogsHeader, flxDetailedResultSegment);
            var flxStatusFilter = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                "clipBounds": false,
                "id": "flxStatusFilter",
                "isVisible": false,
                "layoutType": kony.flex.FREE_FORM,
                "isModalContainer": false,
                "right": "760px",
                "skin": "slFbox",
                "top": "10px",
                "width": "14%",
                "zIndex": 500
            }, {}, {});
            flxStatusFilter.setDefaultUnit(kony.flex.DP);
            var statusFilterMenu = new com.adminConsole.common.statusFilterMenu({
                "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                "id": "statusFilterMenu",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "masterType": constants.MASTER_TYPE_DEFAULT,
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "5dp",
                "width": "100%",
                "zIndex": 5,
                "overrides": {
                    "imgUpArrow": {
                        "src": "uparrow_2x.png"
                    },
                    "segStatusFilterDropdown": {
                        "data": [{
                            "imgCheckBox": "checkbox.png",
                            "lblDescription": "Posted"
                        }, {
                            "imgCheckBox": "checkbox.png",
                            "lblDescription": "Pending"
                        }, {
                            "imgCheckBox": "checkbox.png",
                            "lblDescription": "Failed"
                        }]
                    }
                }
            }, {
                "overrides": {}
            }, {
                "overrides": {}
            });
            flxStatusFilter.add(statusFilterMenu);
            flxResults.add(flxInnerResults, flxStatusFilter);
            flxCustomerContent.add(flxHeader3, flxActivityType3, flxResults);
            var flxPaginationCustomerLog = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "bottom": "35dp",
                "clipBounds": true,
                "height": "70px",
                "id": "flxPaginationCustomerLog",
                "isVisible": false,
                "layoutType": kony.flex.FREE_FORM,
                "left": "35dp",
                "isModalContainer": false,
                "skin": "CopyCopyCopyslFbox0d090d27f416e48",
                "top": "0dp",
                "width": "992dp",
                "zIndex": 1
            }, {}, {});
            flxPaginationCustomerLog.setDefaultUnit(kony.flex.DP);
            var pagination0 = new com.adminConsole.common.pagination1({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "height": "220dp",
                "id": "pagination0",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "masterType": constants.MASTER_TYPE_DEFAULT,
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0dp",
                "width": "100%",
                "overrides": {
                    "pagination1": {
                        "bottom": "viz.val_cleared",
                        "centerX": "viz.val_cleared",
                        "centerY": "viz.val_cleared",
                        "height": "220dp",
                        "left": "0dp",
                        "maxHeight": "viz.val_cleared",
                        "maxWidth": "viz.val_cleared",
                        "minHeight": "viz.val_cleared",
                        "minWidth": "viz.val_cleared",
                        "right": "viz.val_cleared",
                        "top": "0dp",
                        "width": "100%"
                    }
                }
            }, {
                "overrides": {}
            }, {
                "overrides": {}
            });
            flxPaginationCustomerLog.add(pagination0);
            flxCustomerDetailedResults.add(flxCustomerContent, flxPaginationCustomerLog);
            var flxTransactionLog = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "bottom": "20dp",
                "clipBounds": false,
                "id": "flxTransactionLog",
                "isVisible": false,
                "layoutType": kony.flex.FREE_FORM,
                "left": "35dp",
                "isModalContainer": false,
                "right": "35dp",
                "skin": "sknflxffffffOp0b",
                "top": 0,
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 10
            }, {}, {});
            flxTransactionLog.setDefaultUnit(kony.flex.DP);
            var flxTransactionFiltersAndHeaders = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": false,
                "height": "60px",
                "id": "flxTransactionFiltersAndHeaders",
                "isVisible": true,
                "layoutType": kony.flex.FLOW_HORIZONTAL,
                "reverseLayoutDirection": true,
                "left": "0",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "10dp",
                "width": "100%",
                "zIndex": 1
            }, {}, {});
            flxTransactionFiltersAndHeaders.setDefaultUnit(kony.flex.DP);
            var flxTransactionFilterIcon = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "25dp",
                "id": "flxTransactionFilterIcon",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "isModalContainer": false,
                "right": "20dp",
                "skin": "CopyslFbox0efc73ba91cad4b",
                "top": "12px",
                "width": "25dp"
            }, {}, {});
            flxTransactionFilterIcon.setDefaultUnit(kony.flex.DP);
            var lblTransactionFilterIcon = new kony.ui.Label({
                "centerY": "50%",
                "height": "25dp",
                "id": "lblTransactionFilterIcon",
                "isVisible": true,
                "right": "0dp",
                "skin": "sknIconBlack25px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmAlertsManagement.lblIconChannels\")",
                "width": "25dp",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxTransactionFilterIcon.add(lblTransactionFilterIcon);
            var flxTransactionSearchContainer = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "35px",
                "id": "flxTransactionSearchContainer",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "isModalContainer": false,
                "right": "30px",
                "skin": "sknflxd5d9ddop100",
                "top": "10px",
                "width": "350px",
                "zIndex": 1
            }, {}, {});
            flxTransactionSearchContainer.setDefaultUnit(kony.flex.DP);
            var fontIconTransactionSearch = new kony.ui.Label({
                "centerY": "50%",
                "id": "fontIconTransactionSearch",
                "isVisible": true,
                "right": "12dp",
                "skin": "sknFontIconSearchImg20px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.userwidgetmodel.lblSearchIcon\")",
                "top": "0dp",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var tbxTransactionSearch = new kony.ui.TextBox2({
                "autoCapitalize": constants.TEXTBOX_AUTO_CAPITALIZE_NONE,
                "centerY": "50%",
                "height": "30px",
                "id": "tbxTransactionSearch",
                "isVisible": true,
                "keyBoardStyle": constants.TEXTBOX_KEY_BOARD_STYLE_DEFAULT,
                "left": "5px",
                "onTouchStart": controller.AS_TextField_cfab06859f0f43eca99495891ee290d5,
                "i18n_placeholder": "kony.i18n.getLocalizedString(\"i18n.frmLogs.searchTransactionalLogs\")",
                "right": "35px",
                "secureTextEntry": false,
                "skin": "sknSearchtbxConfigurationBundlesNormal",
                "textInputMode": constants.TEXTBOX_INPUT_MODE_ANY,
                "zIndex": 1
            }, {
                "containerHeightMode": constants.TEXTBOX_FONT_METRICS_DRIVEN_HEIGHT,
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [2, 0, 0, 0],
                "paddingInPixel": false
            }, {
                "autoCorrect": false
            });
            var flxClearTransactionSearch = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "100%",
                "id": "flxClearTransactionSearch",
                "isVisible": false,
                "layoutType": kony.flex.FREE_FORM,
                "isModalContainer": false,
                "right": "25dp",
                "skin": "sknCursor",
                "top": "0dp",
                "width": "35px",
                "zIndex": 1
            }, {}, {
                "hoverSkin": "sknCursor"
            });
            flxClearTransactionSearch.setDefaultUnit(kony.flex.DP);
            var fontIconTransactionSearchClose = new kony.ui.Label({
                "centerY": "50%",
                "id": "fontIconTransactionSearchClose",
                "isVisible": true,
                "left": "5px",
                "skin": "sknFontIconSearchCross16px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.userwidgetmodel.lblClearSearch\")",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxClearTransactionSearch.add(fontIconTransactionSearchClose);
            flxTransactionSearchContainer.add(fontIconTransactionSearch, tbxTransactionSearch, flxClearTransactionSearch);
            flxTransactionFiltersAndHeaders.add(flxTransactionFilterIcon, flxTransactionSearchContainer);
            var flxTransactionResults = new kony.ui.FlexScrollContainer({
                "allowHorizontalBounce": false,
                "allowVerticalBounce": true,
                "bounces": true,
                "clipBounds": false,
                "enableScrolling": true,
                "height": "85%",
                "horizontalScrollIndicator": true,
                "id": "flxTransactionResults",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "pagingEnabled": false,
                "scrollDirection": kony.flex.SCROLL_HORIZONTAL,
                "skin": "slFSbox",
                "top": "60dp",
                "verticalScrollIndicator": true,
                "width": "99%",
                "zIndex": 70
            }, {}, {});
            flxTransactionResults.setDefaultUnit(kony.flex.DP);
            var flxInnerTransactionResults = new kony.ui.FlexContainer({
                "clipBounds": false,
                "height": "100%",
                "id": "flxInnerTransactionResults",
                "isVisible": true,
                "layoutType": kony.flex.FLOW_VERTICAL,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0dp",
                "width": kony.flex.USE_PREFFERED_SIZE
            }, {}, {});
            flxInnerTransactionResults.setDefaultUnit(kony.flex.DP);
            var flxTransactionResultHeader = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": false,
                "height": "60px",
                "id": "flxTransactionResultHeader",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0px",
                "isModalContainer": false,
                "right": "0px",
                "skin": "CopyCopyslFbox0e8dde5f64bde4c",
                "top": "0dp",
                "width": "100%",
                "zIndex": 1
            }, {}, {});
            flxTransactionResultHeader.setDefaultUnit(kony.flex.DP);
            var flxTransactionHeader = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": false,
                "height": "60dp",
                "id": "flxTransactionHeader",
                "isVisible": true,
                "layoutType": kony.flex.FLOW_HORIZONTAL,
                "left": "20px",
                "isModalContainer": false,
                "right": "35px",
                "skin": "slFbox",
                "top": "0px",
                "width": "100%",
                "zIndex": 1
            }, {}, {});
            flxTransactionHeader.setDefaultUnit(kony.flex.DP);
            var flxModule = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "100%",
                "id": "flxModule",
                "isVisible": true,
                "layoutType": kony.flex.FLOW_HORIZONTAL,
                "left": "10px",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0dp",
                "width": "100px",
                "zIndex": 1
            }, {}, {});
            flxModule.setDefaultUnit(kony.flex.DP);
            var lblModule = new kony.ui.Label({
                "centerY": "50%",
                "id": "lblModule",
                "isVisible": true,
                "left": 0,
                "skin": "sknlblLato696c7312px",
                "text": "MODULE",
                "top": 0,
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {
                "hoverSkin": "sknlblLato00000011px"
            });
            var flxSortModule = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "centerY": "50%",
                "clipBounds": true,
                "height": "20dp",
                "id": "flxSortModule",
                "isVisible": false,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "CopyslFbox0efc73ba91cad4b",
                "width": "20px",
                "zIndex": 1
            }, {}, {});
            flxSortModule.setDefaultUnit(kony.flex.DP);
            var lblSortModule = new kony.ui.Label({
                "centerY": "50%",
                "height": "15dp",
                "id": "lblSortModule",
                "isVisible": true,
                "left": "5dp",
                "skin": "sknIcon15px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmAlertsManagement.lblSortName\")",
                "width": "15dp",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxSortModule.add(lblSortModule);
            flxModule.add(lblModule, flxSortModule);
            var flxTransactionLogType = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "100%",
                "id": "flxTransactionLogType",
                "isVisible": true,
                "layoutType": kony.flex.FLOW_HORIZONTAL,
                "left": "10px",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0dp",
                "width": "100px",
                "zIndex": 1
            }, {}, {});
            flxTransactionLogType.setDefaultUnit(kony.flex.DP);
            var lblTransactionLogType = new kony.ui.Label({
                "centerY": "50%",
                "id": "lblTransactionLogType",
                "isVisible": true,
                "left": "0px",
                "skin": "sknlblLato696c7312px",
                "text": "ACTIVITY TYPE",
                "top": 0,
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {
                "hoverSkin": "sknlblLato00000011px"
            });
            var flxSortTransactionLogType = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "centerY": "50%",
                "clipBounds": true,
                "height": "20dp",
                "id": "flxSortTransactionLogType",
                "isVisible": false,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "slFbox",
                "width": "20px",
                "zIndex": 1
            }, {}, {});
            flxSortTransactionLogType.setDefaultUnit(kony.flex.DP);
            var imgSortTransactionLogType = new kony.ui.Label({
                "centerY": "50%",
                "height": "15dp",
                "id": "imgSortTransactionLogType",
                "isVisible": true,
                "left": "5dp",
                "skin": "sknIcon15px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmAlertsManagement.lblSortName\")",
                "width": "15dp",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxSortTransactionLogType.add(imgSortTransactionLogType);
            flxTransactionLogType.add(lblTransactionLogType, flxSortTransactionLogType);
            var flxCustomerId = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "100%",
                "id": "flxCustomerId",
                "isVisible": true,
                "layoutType": kony.flex.FLOW_HORIZONTAL,
                "left": "10px",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0dp",
                "width": "100px",
                "zIndex": 1
            }, {}, {});
            flxCustomerId.setDefaultUnit(kony.flex.DP);
            var lblCustomerId = new kony.ui.Label({
                "centerY": "50%",
                "id": "lblCustomerId",
                "isVisible": true,
                "left": "0px",
                "skin": "sknlblLato696c7312px",
                "text": "CUSTOMER ID",
                "top": 0,
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {
                "hoverSkin": "sknlblLato00000011px"
            });
            var flxSortCustomerId = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "centerY": "50%",
                "clipBounds": true,
                "height": "20dp",
                "id": "flxSortCustomerId",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "slFbox",
                "width": "20px",
                "zIndex": 1
            }, {}, {});
            flxSortCustomerId.setDefaultUnit(kony.flex.DP);
            var lblSortCustomerId = new kony.ui.Label({
                "centerY": "50%",
                "height": "15dp",
                "id": "lblSortCustomerId",
                "isVisible": true,
                "left": "5dp",
                "skin": "sknIcon15px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmAlertsManagement.lblSortName\")",
                "width": "15dp",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxSortCustomerId.add(lblSortCustomerId);
            flxCustomerId.add(lblCustomerId, flxSortCustomerId);
            var flxUserName = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "100%",
                "id": "flxUserName",
                "isVisible": true,
                "layoutType": kony.flex.FLOW_HORIZONTAL,
                "left": "10px",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0dp",
                "width": "100px",
                "zIndex": 1
            }, {}, {});
            flxUserName.setDefaultUnit(kony.flex.DP);
            var lblUserName = new kony.ui.Label({
                "centerY": "50%",
                "id": "lblUserName",
                "isVisible": true,
                "left": "0px",
                "skin": "sknlblLato696c7312px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmLogs.customerUserName\")",
                "top": 0,
                "width": "70%",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {
                "hoverSkin": "sknlblLato00000011px"
            });
            var flxSortUserName = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "centerY": "50%",
                "clipBounds": true,
                "height": "20dp",
                "id": "flxSortUserName",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "CopyslFbox0efc73ba91cad4b",
                "width": "20px",
                "zIndex": 1
            }, {}, {});
            flxSortUserName.setDefaultUnit(kony.flex.DP);
            var lblSortUserName = new kony.ui.Label({
                "centerY": "50%",
                "height": "15dp",
                "id": "lblSortUserName",
                "isVisible": true,
                "left": "5dp",
                "skin": "sknIcon15px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmAlertsManagement.lblSortName\")",
                "width": "15dp",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxSortUserName.add(lblSortUserName);
            flxUserName.add(lblUserName, flxSortUserName);
            var flxTransactionDate = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "100%",
                "id": "flxTransactionDate",
                "isVisible": true,
                "layoutType": kony.flex.FLOW_HORIZONTAL,
                "left": "10px",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0dp",
                "width": "100px",
                "zIndex": 1
            }, {}, {});
            flxTransactionDate.setDefaultUnit(kony.flex.DP);
            var lblTransactionDate = new kony.ui.Label({
                "centerY": "50%",
                "id": "lblTransactionDate",
                "isVisible": true,
                "left": "0px",
                "skin": "sknlblLato696c7312px",
                "text": "DATE & TIME",
                "top": 0,
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var flxSortTransactionDate = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "centerY": "50%",
                "clipBounds": true,
                "height": "20dp",
                "id": "flxSortTransactionDate",
                "isVisible": false,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "slFbox",
                "width": "20px",
                "zIndex": 1
            }, {}, {});
            flxSortTransactionDate.setDefaultUnit(kony.flex.DP);
            var imgSortTransactionDate = new kony.ui.Image2({
                "centerX": "50%",
                "centerY": "50%",
                "height": "10dp",
                "id": "imgSortTransactionDate",
                "isVisible": true,
                "skin": "slImage",
                "src": "sorting3x.png",
                "top": "0dp",
                "width": "10dp",
                "zIndex": 1
            }, {
                "imageScaleMode": constants.IMAGE_SCALE_MODE_MAINTAIN_ASPECT_RATIO,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxSortTransactionDate.add(imgSortTransactionDate);
            flxTransactionDate.add(lblTransactionDate, flxSortTransactionDate);
            var flxFromAccount = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "100%",
                "id": "flxFromAccount",
                "isVisible": true,
                "layoutType": kony.flex.FLOW_HORIZONTAL,
                "left": "10px",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0dp",
                "width": "100px",
                "zIndex": 1
            }, {}, {});
            flxFromAccount.setDefaultUnit(kony.flex.DP);
            var lblFromAccount = new kony.ui.Label({
                "centerY": "50%",
                "id": "lblFromAccount",
                "isVisible": true,
                "left": "0px",
                "skin": "sknlblLato696c7312px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmLogs.lblFromAccount\")",
                "top": 0,
                "width": "100%",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var flxSortFromAccount = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "centerY": "50%",
                "clipBounds": true,
                "height": "20dp",
                "id": "flxSortFromAccount",
                "isVisible": false,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "slFbox",
                "width": "20px",
                "zIndex": 1
            }, {}, {});
            flxSortFromAccount.setDefaultUnit(kony.flex.DP);
            var imgSortFromAccount = new kony.ui.Image2({
                "centerX": "50%",
                "centerY": "50%",
                "height": "10dp",
                "id": "imgSortFromAccount",
                "isVisible": true,
                "skin": "slImage",
                "src": "sorting3x.png",
                "top": "0dp",
                "width": "10dp",
                "zIndex": 1
            }, {
                "imageScaleMode": constants.IMAGE_SCALE_MODE_MAINTAIN_ASPECT_RATIO,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxSortFromAccount.add(imgSortFromAccount);
            flxFromAccount.add(lblFromAccount, flxSortFromAccount);
            var flxToAccount = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "100%",
                "id": "flxToAccount",
                "isVisible": true,
                "layoutType": kony.flex.FLOW_HORIZONTAL,
                "left": "10px",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0dp",
                "width": "100px",
                "zIndex": 1
            }, {}, {});
            flxToAccount.setDefaultUnit(kony.flex.DP);
            var lblToAccount = new kony.ui.Label({
                "centerY": "50%",
                "id": "lblToAccount",
                "isVisible": true,
                "left": "0px",
                "skin": "sknlblLato696c7312px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmLogs.lblToAccount\")",
                "top": 0,
                "width": "100%",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var flxSortToAccount = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "centerY": "50%",
                "clipBounds": true,
                "height": "20dp",
                "id": "flxSortToAccount",
                "isVisible": false,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "slFbox",
                "width": "20px",
                "zIndex": 1
            }, {}, {});
            flxSortToAccount.setDefaultUnit(kony.flex.DP);
            var imgSortToAccount = new kony.ui.Image2({
                "centerX": "50%",
                "centerY": "50%",
                "height": "10dp",
                "id": "imgSortToAccount",
                "isVisible": true,
                "skin": "slImage",
                "src": "sorting3x.png",
                "top": "0dp",
                "width": "10dp",
                "zIndex": 1
            }, {
                "imageScaleMode": constants.IMAGE_SCALE_MODE_MAINTAIN_ASPECT_RATIO,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxSortToAccount.add(imgSortToAccount);
            flxToAccount.add(lblToAccount, flxSortToAccount);
            var flxAmount = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "100%",
                "id": "flxAmount",
                "isVisible": true,
                "layoutType": kony.flex.FLOW_HORIZONTAL,
                "left": "10px",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0dp",
                "width": "100px",
                "zIndex": 1
            }, {}, {});
            flxAmount.setDefaultUnit(kony.flex.DP);
            var lblAmount = new kony.ui.Label({
                "centerY": "50%",
                "id": "lblAmount",
                "isVisible": true,
                "left": "0px",
                "skin": "sknlblLato696c7312px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmCustomerManagement.lblTransactionAmount\")",
                "top": 0,
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {
                "hoverSkin": "sknlblLato00000011px"
            });
            var flxSortAmount = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "centerY": "50%",
                "clipBounds": true,
                "height": "20dp",
                "id": "flxSortAmount",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "CopyslFbox0efc73ba91cad4b",
                "top": "0dp",
                "width": "20px",
                "zIndex": 1
            }, {}, {});
            flxSortAmount.setDefaultUnit(kony.flex.DP);
            var lblSortAmount = new kony.ui.Label({
                "centerY": "50%",
                "height": "15dp",
                "id": "lblSortAmount",
                "isVisible": true,
                "left": "5dp",
                "skin": "sknIcon15px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmAlertsManagement.lblSortName\")",
                "width": "15dp",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxSortAmount.add(lblSortAmount);
            flxAmount.add(lblAmount, flxSortAmount);
            var flxCurrency = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "100%",
                "id": "flxCurrency",
                "isVisible": true,
                "layoutType": kony.flex.FLOW_HORIZONTAL,
                "left": "10px",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0dp",
                "width": "100px",
                "zIndex": 1
            }, {}, {});
            flxCurrency.setDefaultUnit(kony.flex.DP);
            var lblCurrency = new kony.ui.Label({
                "centerY": "50%",
                "id": "lblCurrency",
                "isVisible": true,
                "left": "0px",
                "skin": "sknlblLato696c7312px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmLogs.lblCurrency\")",
                "top": 0,
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {
                "hoverSkin": "sknlblLato00000011px"
            });
            var flxSortCurrency = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "centerY": "50%",
                "clipBounds": true,
                "height": "20dp",
                "id": "flxSortCurrency",
                "isVisible": false,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "CopyslFbox0efc73ba91cad4b",
                "top": "0dp",
                "width": "20px",
                "zIndex": 1
            }, {}, {});
            flxSortCurrency.setDefaultUnit(kony.flex.DP);
            var lblSortCurrency = new kony.ui.Label({
                "centerY": "50%",
                "height": "15dp",
                "id": "lblSortCurrency",
                "isVisible": true,
                "left": "5dp",
                "skin": "sknIcon15px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmAlertsManagement.lblIconChannels\")",
                "width": "15dp",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxSortCurrency.add(lblSortCurrency);
            flxCurrency.add(lblCurrency, flxSortCurrency);
            var flxReferenceNumber = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "100%",
                "id": "flxReferenceNumber",
                "isVisible": true,
                "layoutType": kony.flex.FLOW_HORIZONTAL,
                "left": "10px",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0dp",
                "width": "100px",
                "zIndex": 1
            }, {}, {});
            flxReferenceNumber.setDefaultUnit(kony.flex.DP);
            var lblReferenceNumber = new kony.ui.Label({
                "centerY": "50%",
                "id": "lblReferenceNumber",
                "isVisible": true,
                "left": "0px",
                "skin": "sknlblLato696c7312px",
                "text": "REFERENCE NUMBER",
                "top": 0,
                "width": "100%",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {
                "hoverSkin": "sknlblLato00000011px"
            });
            var flxSortReferenceNumber = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "centerY": "50%",
                "clipBounds": true,
                "height": "20dp",
                "id": "flxSortReferenceNumber",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "CopyslFbox0efc73ba91cad4b",
                "top": "0dp",
                "width": "20px",
                "zIndex": 1
            }, {}, {});
            flxSortReferenceNumber.setDefaultUnit(kony.flex.DP);
            var lblSortReferenceNumber = new kony.ui.Label({
                "centerY": "50%",
                "height": "15dp",
                "id": "lblSortReferenceNumber",
                "isVisible": true,
                "left": "5dp",
                "skin": "sknIcon15px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmAlertsManagement.lblIconChannels\")",
                "width": "15dp",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxSortReferenceNumber.add(lblSortReferenceNumber);
            flxReferenceNumber.add(lblReferenceNumber, flxSortReferenceNumber);
            var flxPayeeName = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "100%",
                "id": "flxPayeeName",
                "isVisible": true,
                "layoutType": kony.flex.FLOW_HORIZONTAL,
                "left": "10px",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0dp",
                "width": "100px",
                "zIndex": 1
            }, {}, {});
            flxPayeeName.setDefaultUnit(kony.flex.DP);
            var lblPayeeName = new kony.ui.Label({
                "centerY": "50%",
                "id": "lblPayeeName",
                "isVisible": true,
                "left": "0px",
                "skin": "sknlblLato696c7312px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmLogs.lblPayeeName\")",
                "top": 0,
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {
                "hoverSkin": "sknlblLato00000011px"
            });
            var flxSortPayeeName = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "centerY": "50%",
                "clipBounds": true,
                "height": "20dp",
                "id": "flxSortPayeeName",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "CopyslFbox0efc73ba91cad4b",
                "top": "0dp",
                "width": "20px",
                "zIndex": 1
            }, {}, {});
            flxSortPayeeName.setDefaultUnit(kony.flex.DP);
            var lblSortPayeeName = new kony.ui.Label({
                "centerY": "50%",
                "height": "15dp",
                "id": "lblSortPayeeName",
                "isVisible": true,
                "left": "5dp",
                "skin": "sknIcon15px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmAlertsManagement.lblSortName\")",
                "width": "15dp",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxSortPayeeName.add(lblSortPayeeName);
            flxPayeeName.add(lblPayeeName, flxSortPayeeName);
            var flxPayeeId = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "100%",
                "id": "flxPayeeId",
                "isVisible": true,
                "layoutType": kony.flex.FLOW_HORIZONTAL,
                "left": "10px",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0dp",
                "width": "100px",
                "zIndex": 1
            }, {}, {});
            flxPayeeId.setDefaultUnit(kony.flex.DP);
            var lblPayeeId = new kony.ui.Label({
                "centerY": "50%",
                "id": "lblPayeeId",
                "isVisible": true,
                "left": "0px",
                "skin": "sknlblLato696c7312px",
                "text": "PAYEE ID",
                "top": 0,
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {
                "hoverSkin": "sknlblLato00000011px"
            });
            flxPayeeId.add(lblPayeeId);
            var flxPersonId = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "100%",
                "id": "flxPersonId",
                "isVisible": true,
                "layoutType": kony.flex.FLOW_HORIZONTAL,
                "left": "10px",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0dp",
                "width": "100px",
                "zIndex": 1
            }, {}, {});
            flxPersonId.setDefaultUnit(kony.flex.DP);
            var lblPersonId = new kony.ui.Label({
                "centerY": "50%",
                "id": "lblPersonId",
                "isVisible": true,
                "left": "0px",
                "skin": "sknlblLato696c7312px",
                "text": "PERSON ID",
                "top": 0,
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {
                "hoverSkin": "sknlblLato00000011px"
            });
            flxPersonId.add(lblPersonId);
            var flxTransactionStatus = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "100%",
                "id": "flxTransactionStatus",
                "isVisible": true,
                "layoutType": kony.flex.FLOW_HORIZONTAL,
                "left": "10px",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0dp",
                "width": "100px",
                "zIndex": 1
            }, {}, {});
            flxTransactionStatus.setDefaultUnit(kony.flex.DP);
            var lblTransactionStatus = new kony.ui.Label({
                "centerY": "50%",
                "id": "lblTransactionStatus",
                "isVisible": true,
                "left": "0px",
                "skin": "sknlblLato696c7312px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.roles.STATUS\")",
                "top": 0,
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {
                "hoverSkin": "sknlblLato00000011px"
            });
            var flxSortTransactionStatus = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "centerY": "50%",
                "clipBounds": true,
                "height": "20dp",
                "id": "flxSortTransactionStatus",
                "isVisible": false,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "CopyslFbox0efc73ba91cad4b",
                "width": "20px",
                "zIndex": 1
            }, {}, {});
            flxSortTransactionStatus.setDefaultUnit(kony.flex.DP);
            var lblSortTransactionStatus = new kony.ui.Label({
                "centerY": "50%",
                "height": "15dp",
                "id": "lblSortTransactionStatus",
                "isVisible": true,
                "left": "5dp",
                "skin": "sknIcon15px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmAlertsManagement.lblIconChannels\")",
                "width": "15dp",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxSortTransactionStatus.add(lblSortTransactionStatus);
            flxTransactionStatus.add(lblTransactionStatus, flxSortTransactionStatus);
            var flxTransactionMFAType = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "100%",
                "id": "flxTransactionMFAType",
                "isVisible": true,
                "layoutType": kony.flex.FLOW_HORIZONTAL,
                "left": "10px",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0dp",
                "width": "100px",
                "zIndex": 1
            }, {}, {});
            flxTransactionMFAType.setDefaultUnit(kony.flex.DP);
            var lblTransactionMFAType = new kony.ui.Label({
                "centerY": "50%",
                "id": "lblTransactionMFAType",
                "isVisible": true,
                "left": "0px",
                "skin": "sknlblLato696c7312px",
                "text": "MFA TYPE",
                "top": 0,
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {
                "hoverSkin": "sknlblLato00000011px"
            });
            var flxSortTransactionMFAType = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "centerY": "50%",
                "clipBounds": true,
                "height": "20dp",
                "id": "flxSortTransactionMFAType",
                "isVisible": false,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "CopyslFbox0efc73ba91cad4b",
                "width": "20px",
                "zIndex": 1
            }, {}, {});
            flxSortTransactionMFAType.setDefaultUnit(kony.flex.DP);
            var lblSortTransactionMFAType = new kony.ui.Label({
                "centerY": "50%",
                "height": "15dp",
                "id": "lblSortTransactionMFAType",
                "isVisible": true,
                "left": "5dp",
                "skin": "sknIcon15px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmAlertsManagement.lblIconChannels\")",
                "width": "15dp",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxSortTransactionMFAType.add(lblSortTransactionMFAType);
            flxTransactionMFAType.add(lblTransactionMFAType, flxSortTransactionMFAType);
            var flxTransactionMFAServiceKey = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "100%",
                "id": "flxTransactionMFAServiceKey",
                "isVisible": true,
                "layoutType": kony.flex.FLOW_HORIZONTAL,
                "left": "10px",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0dp",
                "width": "100px",
                "zIndex": 1
            }, {}, {});
            flxTransactionMFAServiceKey.setDefaultUnit(kony.flex.DP);
            var lblTransactionMFAServiceKey = new kony.ui.Label({
                "centerY": "50%",
                "id": "lblTransactionMFAServiceKey",
                "isVisible": true,
                "left": "0px",
                "skin": "sknlblLato696c7312px",
                "text": "MFA SERVICE KEY",
                "top": 0,
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {
                "hoverSkin": "sknlblLato00000011px"
            });
            var flxSortTransactionMFAServiceKey = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "centerY": "50%",
                "clipBounds": true,
                "height": "20dp",
                "id": "flxSortTransactionMFAServiceKey",
                "isVisible": false,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "CopyslFbox0efc73ba91cad4b",
                "width": "20px",
                "zIndex": 1
            }, {}, {});
            flxSortTransactionMFAServiceKey.setDefaultUnit(kony.flex.DP);
            var lblSortTransactionMFAServiceKey = new kony.ui.Label({
                "centerY": "50%",
                "height": "15dp",
                "id": "lblSortTransactionMFAServiceKey",
                "isVisible": true,
                "left": "5dp",
                "skin": "sknIcon15px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmAlertsManagement.lblIconChannels\")",
                "width": "15dp",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxSortTransactionMFAServiceKey.add(lblSortTransactionMFAServiceKey);
            flxTransactionMFAServiceKey.add(lblTransactionMFAServiceKey, flxSortTransactionMFAServiceKey);
            var flxTransactionMFAState = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "100%",
                "id": "flxTransactionMFAState",
                "isVisible": true,
                "layoutType": kony.flex.FLOW_HORIZONTAL,
                "left": "10px",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0dp",
                "width": "100px",
                "zIndex": 1
            }, {}, {});
            flxTransactionMFAState.setDefaultUnit(kony.flex.DP);
            var lblTransactionMFAState = new kony.ui.Label({
                "centerY": "50%",
                "id": "lblTransactionMFAState",
                "isVisible": true,
                "left": "0px",
                "skin": "sknlblLato696c7312px",
                "text": "MFA STATE",
                "top": 0,
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {
                "hoverSkin": "sknlblLato00000011px"
            });
            var flxSortTransactionMFAState = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "centerY": "50%",
                "clipBounds": true,
                "height": "20dp",
                "id": "flxSortTransactionMFAState",
                "isVisible": false,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "CopyslFbox0efc73ba91cad4b",
                "width": "20px",
                "zIndex": 1
            }, {}, {});
            flxSortTransactionMFAState.setDefaultUnit(kony.flex.DP);
            var lblSortTransactionMFAState = new kony.ui.Label({
                "centerY": "50%",
                "height": "15dp",
                "id": "lblSortTransactionMFAState",
                "isVisible": true,
                "left": "5dp",
                "skin": "sknIcon15px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmAlertsManagement.lblIconChannels\")",
                "width": "15dp",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxSortTransactionMFAState.add(lblSortTransactionMFAState);
            flxTransactionMFAState.add(lblTransactionMFAState, flxSortTransactionMFAState);
            var flxChannel = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "100%",
                "id": "flxChannel",
                "isVisible": true,
                "layoutType": kony.flex.FLOW_HORIZONTAL,
                "left": "10px",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0dp",
                "width": "100px",
                "zIndex": 1
            }, {}, {});
            flxChannel.setDefaultUnit(kony.flex.DP);
            var lblChannel = new kony.ui.Label({
                "centerY": "50%",
                "id": "lblChannel",
                "isVisible": true,
                "left": "0px",
                "skin": "sknlblLato696c7312px",
                "text": "CHANNEL",
                "top": 0,
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var flxSortChannel = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "centerY": "50%",
                "clipBounds": true,
                "height": "20dp",
                "id": "flxSortChannel",
                "isVisible": false,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "slFbox",
                "width": "20px",
                "zIndex": 1
            }, {}, {});
            flxSortChannel.setDefaultUnit(kony.flex.DP);
            var imgSortChannel = new kony.ui.Image2({
                "centerX": "50%",
                "centerY": "50%",
                "height": "10dp",
                "id": "imgSortChannel",
                "isVisible": true,
                "skin": "slImage",
                "src": "sorting3x.png",
                "top": "0dp",
                "width": "10dp",
                "zIndex": 1
            }, {
                "imageScaleMode": constants.IMAGE_SCALE_MODE_MAINTAIN_ASPECT_RATIO,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxSortChannel.add(imgSortChannel);
            flxChannel.add(lblChannel, flxSortChannel);
            var flxTransactionDevice = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "100%",
                "id": "flxTransactionDevice",
                "isVisible": true,
                "layoutType": kony.flex.FLOW_HORIZONTAL,
                "left": "10px",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0dp",
                "width": "100px",
                "zIndex": 1
            }, {}, {});
            flxTransactionDevice.setDefaultUnit(kony.flex.DP);
            var lblTransactionDevice = new kony.ui.Label({
                "centerY": "50%",
                "id": "lblTransactionDevice",
                "isVisible": true,
                "left": "0px",
                "skin": "sknlblLato696c7312px",
                "text": "DEVICE",
                "top": 0,
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var flxSortTransactionDevice = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "centerY": "50%",
                "clipBounds": true,
                "height": "20dp",
                "id": "flxSortTransactionDevice",
                "isVisible": false,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "slFbox",
                "width": "20px",
                "zIndex": 1
            }, {}, {});
            flxSortTransactionDevice.setDefaultUnit(kony.flex.DP);
            var imgSortTransactionDevice = new kony.ui.Image2({
                "centerX": "50%",
                "centerY": "50%",
                "height": "10dp",
                "id": "imgSortTransactionDevice",
                "isVisible": true,
                "skin": "slImage",
                "src": "sorting3x.png",
                "top": "0dp",
                "width": "10dp",
                "zIndex": 1
            }, {
                "imageScaleMode": constants.IMAGE_SCALE_MODE_MAINTAIN_ASPECT_RATIO,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxSortTransactionDevice.add(imgSortTransactionDevice);
            flxTransactionDevice.add(lblTransactionDevice, flxSortTransactionDevice);
            var flxTransactionOS = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "100%",
                "id": "flxTransactionOS",
                "isVisible": true,
                "layoutType": kony.flex.FLOW_HORIZONTAL,
                "left": "10px",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0dp",
                "width": "100px",
                "zIndex": 1
            }, {}, {});
            flxTransactionOS.setDefaultUnit(kony.flex.DP);
            var lblTransactionOS = new kony.ui.Label({
                "centerY": "50%",
                "id": "lblTransactionOS",
                "isVisible": true,
                "left": "0px",
                "skin": "sknlblLato696c7312px",
                "text": "OS",
                "top": 0,
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var flxSortTransactionOS = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "centerY": "50%",
                "clipBounds": true,
                "height": "20dp",
                "id": "flxSortTransactionOS",
                "isVisible": false,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "slFbox",
                "width": "20px",
                "zIndex": 1
            }, {}, {});
            flxSortTransactionOS.setDefaultUnit(kony.flex.DP);
            var imgSortTransactionOS = new kony.ui.Image2({
                "centerX": "50%",
                "centerY": "50%",
                "height": "10dp",
                "id": "imgSortTransactionOS",
                "isVisible": true,
                "skin": "slImage",
                "src": "sorting3x.png",
                "top": "0dp",
                "width": "10dp",
                "zIndex": 1
            }, {
                "imageScaleMode": constants.IMAGE_SCALE_MODE_MAINTAIN_ASPECT_RATIO,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxSortTransactionOS.add(imgSortTransactionOS);
            flxTransactionOS.add(lblTransactionOS, flxSortTransactionOS);
            var flxTransactionDeviceId = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "100%",
                "id": "flxTransactionDeviceId",
                "isVisible": true,
                "layoutType": kony.flex.FLOW_HORIZONTAL,
                "left": "10px",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0dp",
                "width": "100px",
                "zIndex": 1
            }, {}, {});
            flxTransactionDeviceId.setDefaultUnit(kony.flex.DP);
            var lblTransactionDeviceId = new kony.ui.Label({
                "centerY": "50%",
                "id": "lblTransactionDeviceId",
                "isVisible": true,
                "left": "0px",
                "skin": "sknlblLato696c7312px",
                "text": "DEVICE ID",
                "top": 0,
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var flxSortTransactionDeviceId = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "centerY": "50%",
                "clipBounds": true,
                "height": "20dp",
                "id": "flxSortTransactionDeviceId",
                "isVisible": false,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "slFbox",
                "width": "20px",
                "zIndex": 1
            }, {}, {});
            flxSortTransactionDeviceId.setDefaultUnit(kony.flex.DP);
            var imgSortTransactionDeviceId = new kony.ui.Image2({
                "centerX": "50%",
                "centerY": "50%",
                "height": "10dp",
                "id": "imgSortTransactionDeviceId",
                "isVisible": true,
                "skin": "slImage",
                "src": "sorting3x.png",
                "top": "0dp",
                "width": "10dp",
                "zIndex": 1
            }, {
                "imageScaleMode": constants.IMAGE_SCALE_MODE_MAINTAIN_ASPECT_RATIO,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxSortTransactionDeviceId.add(imgSortTransactionDeviceId);
            flxTransactionDeviceId.add(lblTransactionDeviceId, flxSortTransactionDeviceId);
            var flxTransactionIPAddress = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "100%",
                "id": "flxTransactionIPAddress",
                "isVisible": true,
                "layoutType": kony.flex.FLOW_HORIZONTAL,
                "left": "10px",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0dp",
                "width": "100px",
                "zIndex": 1
            }, {}, {});
            flxTransactionIPAddress.setDefaultUnit(kony.flex.DP);
            var lblTransactionIPAddress = new kony.ui.Label({
                "centerY": "50%",
                "id": "lblTransactionIPAddress",
                "isVisible": true,
                "left": "0px",
                "skin": "sknlblLato696c7312px",
                "text": "IP ADDRESS",
                "top": 0,
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var flxSortTransactionIPAddress = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "centerY": "50%",
                "clipBounds": true,
                "height": "20dp",
                "id": "flxSortTransactionIPAddress",
                "isVisible": false,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "slFbox",
                "width": "20px",
                "zIndex": 1
            }, {}, {});
            flxSortTransactionIPAddress.setDefaultUnit(kony.flex.DP);
            var imgSortTransactionIPAddress = new kony.ui.Image2({
                "centerX": "50%",
                "centerY": "50%",
                "height": "10dp",
                "id": "imgSortTransactionIPAddress",
                "isVisible": true,
                "skin": "slImage",
                "src": "sorting3x.png",
                "top": "0dp",
                "width": "10dp",
                "zIndex": 1
            }, {
                "imageScaleMode": constants.IMAGE_SCALE_MODE_MAINTAIN_ASPECT_RATIO,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxSortTransactionIPAddress.add(imgSortTransactionIPAddress);
            flxTransactionIPAddress.add(lblTransactionIPAddress, flxSortTransactionIPAddress);
            var flxTransactionErrorCode = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "100%",
                "id": "flxTransactionErrorCode",
                "isVisible": true,
                "layoutType": kony.flex.FLOW_HORIZONTAL,
                "left": "10px",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0dp",
                "width": "100px",
                "zIndex": 1
            }, {}, {});
            flxTransactionErrorCode.setDefaultUnit(kony.flex.DP);
            var lblTransactionErrorCode = new kony.ui.Label({
                "centerY": "50%",
                "id": "lblTransactionErrorCode",
                "isVisible": true,
                "left": "0px",
                "skin": "sknlblLato696c7312px",
                "text": "OTHER INFO",
                "top": 0,
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {
                "hoverSkin": "sknlblLato00000011px"
            });
            var flxSortTransactionErrorCode = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "centerY": "50%",
                "clipBounds": true,
                "height": "20dp",
                "id": "flxSortTransactionErrorCode",
                "isVisible": false,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "CopyslFbox0efc73ba91cad4b",
                "top": "0dp",
                "width": "20px",
                "zIndex": 1
            }, {}, {});
            flxSortTransactionErrorCode.setDefaultUnit(kony.flex.DP);
            var lblSortTransactionErrorCode = new kony.ui.Label({
                "centerY": "50%",
                "height": "15dp",
                "id": "lblSortTransactionErrorCode",
                "isVisible": true,
                "left": "5dp",
                "skin": "sknIcon15px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmAlertsManagement.lblIconChannels\")",
                "width": "15dp",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxSortTransactionErrorCode.add(lblSortTransactionErrorCode);
            flxTransactionErrorCode.add(lblTransactionErrorCode, flxSortTransactionErrorCode);
            flxTransactionHeader.add(flxModule, flxTransactionLogType, flxCustomerId, flxUserName, flxTransactionDate, flxFromAccount, flxToAccount, flxAmount, flxCurrency, flxReferenceNumber, flxPayeeName, flxPayeeId, flxPersonId, flxTransactionStatus, flxTransactionMFAType, flxTransactionMFAServiceKey, flxTransactionMFAState, flxChannel, flxTransactionDevice, flxTransactionOS, flxTransactionDeviceId, flxTransactionIPAddress, flxTransactionErrorCode);
            var lblTransactionHeaderSeperator = new kony.ui.Label({
                "bottom": 0,
                "height": "1px",
                "id": "lblTransactionHeaderSeperator",
                "isVisible": true,
                "left": "20px",
                "right": "0px",
                "skin": "sknLblHeaderLine",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmCSR.lblSeperator\")",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxTransactionResultHeader.add(flxTransactionHeader, lblTransactionHeaderSeperator);
            var flxSegResults = new kony.ui.FlexScrollContainer({
                "allowHorizontalBounce": false,
                "allowVerticalBounce": true,
                "bounces": true,
                "clipBounds": true,
                "enableScrolling": true,
                "height": "85%",
                "horizontalScrollIndicator": true,
                "id": "flxSegResults",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "20dp",
                "pagingEnabled": false,
                "scrollDirection": kony.flex.SCROLL_VERTICAL,
                "skin": "slFSbox",
                "top": "0dp",
                "verticalScrollIndicator": true,
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {}, {});
            flxSegResults.setDefaultUnit(kony.flex.DP);
            var segTransactionresult = new kony.ui.SegmentedUI2({
                "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                "data": [{
                    "imgStatus": "active_circle2x.png",
                    "lblAmount": "transact",
                    "lblChannel": "bryanvnash",
                    "lblCurrency": "userid",
                    "lblCustomerId": "userid",
                    "lblDevice": "bryanvnash",
                    "lblDeviceId": "bryanvnash",
                    "lblErrorCode": "bryanvnash1",
                    "lblFromAccount": "from account",
                    "lblIPAddress": "bryanvnash",
                    "lblIconStatus": "",
                    "lblLogType": "userid",
                    "lblMFAServiceType": "bryanvnash",
                    "lblMFAState": "bryanvnash",
                    "lblMFAType": "bryanvnash",
                    "lblModule": "transactioniD",
                    "lblOS": "bryanvnash",
                    "lblPayeeId": "username",
                    "lblPayeeName": "username",
                    "lblPersonId": "username",
                    "lblReferenceNumber": "bryanvnash1",
                    "lblSeperator": ".",
                    "lblStatus": "Active",
                    "lblToAccount": "TD12323",
                    "lblTransactionDate": "from account",
                    "lblUserName": "username"
                }, {
                    "imgStatus": "active_circle2x.png",
                    "lblAmount": "transact",
                    "lblChannel": "bryanvnash",
                    "lblCurrency": "userid",
                    "lblCustomerId": "userid",
                    "lblDevice": "bryanvnash",
                    "lblDeviceId": "bryanvnash",
                    "lblErrorCode": "bryanvnash1",
                    "lblFromAccount": "from account",
                    "lblIPAddress": "bryanvnash",
                    "lblIconStatus": "",
                    "lblLogType": "userid",
                    "lblMFAServiceType": "bryanvnash",
                    "lblMFAState": "bryanvnash",
                    "lblMFAType": "bryanvnash",
                    "lblModule": "transactioniD",
                    "lblOS": "bryanvnash",
                    "lblPayeeId": "username",
                    "lblPayeeName": "username",
                    "lblPersonId": "username",
                    "lblReferenceNumber": "bryanvnash1",
                    "lblSeperator": ".",
                    "lblStatus": "Active",
                    "lblToAccount": "TD12323",
                    "lblTransactionDate": "from account",
                    "lblUserName": "username"
                }, {
                    "imgStatus": "active_circle2x.png",
                    "lblAmount": "transact",
                    "lblChannel": "bryanvnash",
                    "lblCurrency": "userid",
                    "lblCustomerId": "userid",
                    "lblDevice": "bryanvnash",
                    "lblDeviceId": "bryanvnash",
                    "lblErrorCode": "bryanvnash1",
                    "lblFromAccount": "from account",
                    "lblIPAddress": "bryanvnash",
                    "lblIconStatus": "",
                    "lblLogType": "userid",
                    "lblMFAServiceType": "bryanvnash",
                    "lblMFAState": "bryanvnash",
                    "lblMFAType": "bryanvnash",
                    "lblModule": "transactioniD",
                    "lblOS": "bryanvnash",
                    "lblPayeeId": "username",
                    "lblPayeeName": "username",
                    "lblPersonId": "username",
                    "lblReferenceNumber": "bryanvnash1",
                    "lblSeperator": ".",
                    "lblStatus": "Active",
                    "lblToAccount": "TD12323",
                    "lblTransactionDate": "from account",
                    "lblUserName": "username"
                }, {
                    "imgStatus": "active_circle2x.png",
                    "lblAmount": "transact",
                    "lblChannel": "bryanvnash",
                    "lblCurrency": "userid",
                    "lblCustomerId": "userid",
                    "lblDevice": "bryanvnash",
                    "lblDeviceId": "bryanvnash",
                    "lblErrorCode": "bryanvnash1",
                    "lblFromAccount": "from account",
                    "lblIPAddress": "bryanvnash",
                    "lblIconStatus": "",
                    "lblLogType": "userid",
                    "lblMFAServiceType": "bryanvnash",
                    "lblMFAState": "bryanvnash",
                    "lblMFAType": "bryanvnash",
                    "lblModule": "transactioniD",
                    "lblOS": "bryanvnash",
                    "lblPayeeId": "username",
                    "lblPayeeName": "username",
                    "lblPersonId": "username",
                    "lblReferenceNumber": "bryanvnash1",
                    "lblSeperator": ".",
                    "lblStatus": "Active",
                    "lblToAccount": "TD12323",
                    "lblTransactionDate": "from account",
                    "lblUserName": "username"
                }, {
                    "imgStatus": "active_circle2x.png",
                    "lblAmount": "transact",
                    "lblChannel": "bryanvnash",
                    "lblCurrency": "userid",
                    "lblCustomerId": "userid",
                    "lblDevice": "bryanvnash",
                    "lblDeviceId": "bryanvnash",
                    "lblErrorCode": "bryanvnash1",
                    "lblFromAccount": "from account",
                    "lblIPAddress": "bryanvnash",
                    "lblIconStatus": "",
                    "lblLogType": "userid",
                    "lblMFAServiceType": "bryanvnash",
                    "lblMFAState": "bryanvnash",
                    "lblMFAType": "bryanvnash",
                    "lblModule": "transactioniD",
                    "lblOS": "bryanvnash",
                    "lblPayeeId": "username",
                    "lblPayeeName": "username",
                    "lblPersonId": "username",
                    "lblReferenceNumber": "bryanvnash1",
                    "lblSeperator": ".",
                    "lblStatus": "Active",
                    "lblToAccount": "TD12323",
                    "lblTransactionDate": "from account",
                    "lblUserName": "username"
                }, {
                    "imgStatus": "active_circle2x.png",
                    "lblAmount": "transact",
                    "lblChannel": "bryanvnash",
                    "lblCurrency": "userid",
                    "lblCustomerId": "userid",
                    "lblDevice": "bryanvnash",
                    "lblDeviceId": "bryanvnash",
                    "lblErrorCode": "bryanvnash1",
                    "lblFromAccount": "from account",
                    "lblIPAddress": "bryanvnash",
                    "lblIconStatus": "",
                    "lblLogType": "userid",
                    "lblMFAServiceType": "bryanvnash",
                    "lblMFAState": "bryanvnash",
                    "lblMFAType": "bryanvnash",
                    "lblModule": "transactioniD",
                    "lblOS": "bryanvnash",
                    "lblPayeeId": "username",
                    "lblPayeeName": "username",
                    "lblPersonId": "username",
                    "lblReferenceNumber": "bryanvnash1",
                    "lblSeperator": ".",
                    "lblStatus": "Active",
                    "lblToAccount": "TD12323",
                    "lblTransactionDate": "from account",
                    "lblUserName": "username"
                }, {
                    "imgStatus": "active_circle2x.png",
                    "lblAmount": "transact",
                    "lblChannel": "bryanvnash",
                    "lblCurrency": "userid",
                    "lblCustomerId": "userid",
                    "lblDevice": "bryanvnash",
                    "lblDeviceId": "bryanvnash",
                    "lblErrorCode": "bryanvnash1",
                    "lblFromAccount": "from account",
                    "lblIPAddress": "bryanvnash",
                    "lblIconStatus": "",
                    "lblLogType": "userid",
                    "lblMFAServiceType": "bryanvnash",
                    "lblMFAState": "bryanvnash",
                    "lblMFAType": "bryanvnash",
                    "lblModule": "transactioniD",
                    "lblOS": "bryanvnash",
                    "lblPayeeId": "username",
                    "lblPayeeName": "username",
                    "lblPersonId": "username",
                    "lblReferenceNumber": "bryanvnash1",
                    "lblSeperator": ".",
                    "lblStatus": "Active",
                    "lblToAccount": "TD12323",
                    "lblTransactionDate": "from account",
                    "lblUserName": "username"
                }, {
                    "imgStatus": "active_circle2x.png",
                    "lblAmount": "transact",
                    "lblChannel": "bryanvnash",
                    "lblCurrency": "userid",
                    "lblCustomerId": "userid",
                    "lblDevice": "bryanvnash",
                    "lblDeviceId": "bryanvnash",
                    "lblErrorCode": "bryanvnash1",
                    "lblFromAccount": "from account",
                    "lblIPAddress": "bryanvnash",
                    "lblIconStatus": "",
                    "lblLogType": "userid",
                    "lblMFAServiceType": "bryanvnash",
                    "lblMFAState": "bryanvnash",
                    "lblMFAType": "bryanvnash",
                    "lblModule": "transactioniD",
                    "lblOS": "bryanvnash",
                    "lblPayeeId": "username",
                    "lblPayeeName": "username",
                    "lblPersonId": "username",
                    "lblReferenceNumber": "bryanvnash1",
                    "lblSeperator": ".",
                    "lblStatus": "Active",
                    "lblToAccount": "TD12323",
                    "lblTransactionDate": "from account",
                    "lblUserName": "username"
                }, {
                    "imgStatus": "active_circle2x.png",
                    "lblAmount": "transact",
                    "lblChannel": "bryanvnash",
                    "lblCurrency": "userid",
                    "lblCustomerId": "userid",
                    "lblDevice": "bryanvnash",
                    "lblDeviceId": "bryanvnash",
                    "lblErrorCode": "bryanvnash1",
                    "lblFromAccount": "from account",
                    "lblIPAddress": "bryanvnash",
                    "lblIconStatus": "",
                    "lblLogType": "userid",
                    "lblMFAServiceType": "bryanvnash",
                    "lblMFAState": "bryanvnash",
                    "lblMFAType": "bryanvnash",
                    "lblModule": "transactioniD",
                    "lblOS": "bryanvnash",
                    "lblPayeeId": "username",
                    "lblPayeeName": "username",
                    "lblPersonId": "username",
                    "lblReferenceNumber": "bryanvnash1",
                    "lblSeperator": ".",
                    "lblStatus": "Active",
                    "lblToAccount": "TD12323",
                    "lblTransactionDate": "from account",
                    "lblUserName": "username"
                }, {
                    "imgStatus": "active_circle2x.png",
                    "lblAmount": "transact",
                    "lblChannel": "bryanvnash",
                    "lblCurrency": "userid",
                    "lblCustomerId": "userid",
                    "lblDevice": "bryanvnash",
                    "lblDeviceId": "bryanvnash",
                    "lblErrorCode": "bryanvnash1",
                    "lblFromAccount": "from account",
                    "lblIPAddress": "bryanvnash",
                    "lblIconStatus": "",
                    "lblLogType": "userid",
                    "lblMFAServiceType": "bryanvnash",
                    "lblMFAState": "bryanvnash",
                    "lblMFAType": "bryanvnash",
                    "lblModule": "transactioniD",
                    "lblOS": "bryanvnash",
                    "lblPayeeId": "username",
                    "lblPayeeName": "username",
                    "lblPersonId": "username",
                    "lblReferenceNumber": "bryanvnash1",
                    "lblSeperator": ".",
                    "lblStatus": "Active",
                    "lblToAccount": "TD12323",
                    "lblTransactionDate": "from account",
                    "lblUserName": "username"
                }],
                "groupCells": false,
                "id": "segTransactionresult",
                "isVisible": true,
                "left": "0px",
                "needPageIndicator": true,
                "pageOffDotImage": "pageoffdot.png",
                "pageOnDotImage": "pageondot.png",
                "retainSelection": false,
                "rowFocusSkin": "seg2Focus",
                "rowSkin": "seg2Normal",
                "rowTemplate": "flxTransactionLogs",
                "sectionHeaderSkin": "sliPhoneSegmentHeader",
                "selectionBehavior": constants.SEGUI_DEFAULT_BEHAVIOR,
                "separatorRequired": false,
                "showScrollbars": false,
                "top": "0dp",
                "viewType": constants.SEGUI_VIEW_TYPE_TABLEVIEW,
                "widgetDataMap": {
                    "flxStatus": "flxStatus",
                    "flxTransactionLogs": "flxTransactionLogs",
                    "flxTransactionWrapper": "flxTransactionWrapper",
                    "imgStatus": "imgStatus",
                    "lblAmount": "lblAmount",
                    "lblChannel": "lblChannel",
                    "lblCurrency": "lblCurrency",
                    "lblCustomerId": "lblCustomerId",
                    "lblDevice": "lblDevice",
                    "lblDeviceId": "lblDeviceId",
                    "lblErrorCode": "lblErrorCode",
                    "lblFromAccount": "lblFromAccount",
                    "lblIPAddress": "lblIPAddress",
                    "lblIconStatus": "lblIconStatus",
                    "lblLogType": "lblLogType",
                    "lblMFAServiceType": "lblMFAServiceType",
                    "lblMFAState": "lblMFAState",
                    "lblMFAType": "lblMFAType",
                    "lblModule": "lblModule",
                    "lblOS": "lblOS",
                    "lblPayeeId": "lblPayeeId",
                    "lblPayeeName": "lblPayeeName",
                    "lblPersonId": "lblPersonId",
                    "lblReferenceNumber": "lblReferenceNumber",
                    "lblSeperator": "lblSeperator",
                    "lblStatus": "lblStatus",
                    "lblToAccount": "lblToAccount",
                    "lblTransactionDate": "lblTransactionDate",
                    "lblUserName": "lblUserName"
                },
                "width": "100%",
                "zIndex": 1
            }, {
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxSegResults.add(segTransactionresult);
            flxInnerTransactionResults.add(flxTransactionResultHeader, flxSegResults);
            var flxStatusFilterTransaction = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                "clipBounds": false,
                "id": "flxStatusFilterTransaction",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "isModalContainer": false,
                "right": "760px",
                "skin": "slFbox",
                "top": "10px",
                "width": "13%",
                "zIndex": 500
            }, {}, {});
            flxStatusFilterTransaction.setDefaultUnit(kony.flex.DP);
            var statusFilterMenuTransaction = new com.adminConsole.common.statusFilterMenu({
                "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                "id": "statusFilterMenuTransaction",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "masterType": constants.MASTER_TYPE_DEFAULT,
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "5dp",
                "width": "100%",
                "zIndex": 5,
                "overrides": {
                    "imgUpArrow": {
                        "src": "uparrow_2x.png"
                    },
                    "segStatusFilterDropdown": {
                        "data": [{
                            "imgCheckBox": "checkbox.png",
                            "lblDescription": "Posted"
                        }, {
                            "imgCheckBox": "checkbox.png",
                            "lblDescription": "Pending"
                        }, {
                            "imgCheckBox": "checkbox.png",
                            "lblDescription": "Failed"
                        }]
                    }
                }
            }, {
                "overrides": {}
            }, {
                "overrides": {}
            });
            flxStatusFilterTransaction.add(statusFilterMenuTransaction);
            flxTransactionResults.add(flxInnerTransactionResults, flxStatusFilterTransaction);
            flxTransactionLog.add(flxTransactionFiltersAndHeaders, flxTransactionResults);
            var flxAdminConsoleLog = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "bottom": "0dp",
                "clipBounds": false,
                "id": "flxAdminConsoleLog",
                "isVisible": false,
                "layoutType": kony.flex.FREE_FORM,
                "left": "35dp",
                "isModalContainer": false,
                "right": "35dp",
                "skin": "sknflxffffffOp0b",
                "top": "0dp",
                "zIndex": 10
            }, {}, {});
            flxAdminConsoleLog.setDefaultUnit(kony.flex.DP);
            var flxAdminConsoleFiltersAndHeaders = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": false,
                "height": "80dp",
                "id": "flxAdminConsoleFiltersAndHeaders",
                "isVisible": true,
                "layoutType": kony.flex.FLOW_HORIZONTAL,
                "reverseLayoutDirection": true,
                "left": "0",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0dp",
                "width": "100%",
                "zIndex": 1
            }, {}, {});
            flxAdminConsoleFiltersAndHeaders.setDefaultUnit(kony.flex.DP);
            var flxAdminConsoleFilterIcon = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "25dp",
                "id": "flxAdminConsoleFilterIcon",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "isModalContainer": false,
                "right": "30dp",
                "skin": "CopyslFbox0efc73ba91cad4b",
                "top": "12px",
                "width": "25dp"
            }, {}, {});
            flxAdminConsoleFilterIcon.setDefaultUnit(kony.flex.DP);
            var lblAdminConsoleFilterIcon = new kony.ui.Label({
                "centerY": "50%",
                "height": "25dp",
                "id": "lblAdminConsoleFilterIcon",
                "isVisible": true,
                "right": "0dp",
                "skin": "sknIconBlack25px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmAlertsManagement.lblIconChannels\")",
                "width": "25dp",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxAdminConsoleFilterIcon.add(lblAdminConsoleFilterIcon);
            var flxAdminConsoleSearchContainer = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "35px",
                "id": "flxAdminConsoleSearchContainer",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "isModalContainer": false,
                "right": "30px",
                "skin": "sknflxd5d9ddop100",
                "top": "10px",
                "width": "350px",
                "zIndex": 1
            }, {}, {});
            flxAdminConsoleSearchContainer.setDefaultUnit(kony.flex.DP);
            var fontIconSearch = new kony.ui.Label({
                "centerY": "50%",
                "id": "fontIconSearch",
                "isVisible": true,
                "right": "12dp",
                "skin": "sknFontIconSearchImg20px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.userwidgetmodel.lblSearchIcon\")",
                "top": "0dp",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var tbxAdminConsoleSearch = new kony.ui.TextBox2({
                "autoCapitalize": constants.TEXTBOX_AUTO_CAPITALIZE_NONE,
                "centerY": "50%",
                "height": "30px",
                "id": "tbxAdminConsoleSearch",
                "isVisible": true,
                "keyBoardStyle": constants.TEXTBOX_KEY_BOARD_STYLE_DEFAULT,
                "left": "5px",
                "onTouchStart": controller.AS_TextField_cfab06859f0f43eca99495891ee290d5,
                "i18n_placeholder": "kony.i18n.getLocalizedString(\"i18n.frmLogs.searchAdminConsoleLogs\")",
                "right": "35px",
                "secureTextEntry": false,
                "skin": "sknSearchtbxConfigurationBundlesNormal",
                "textInputMode": constants.TEXTBOX_INPUT_MODE_ANY,
                "zIndex": 1
            }, {
                "containerHeightMode": constants.TEXTBOX_FONT_METRICS_DRIVEN_HEIGHT,
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [2, 0, 0, 0],
                "paddingInPixel": false
            }, {
                "autoCorrect": false
            });
            var flxClearAdminConsoleSearch = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "100%",
                "id": "flxClearAdminConsoleSearch",
                "isVisible": false,
                "layoutType": kony.flex.FREE_FORM,
                "isModalContainer": false,
                "right": "25dp",
                "skin": "sknCursor",
                "top": "0dp",
                "width": "35px",
                "zIndex": 1
            }, {}, {
                "hoverSkin": "sknCursor"
            });
            flxClearAdminConsoleSearch.setDefaultUnit(kony.flex.DP);
            var fontIconAdminConsoleClose = new kony.ui.Label({
                "centerY": "50%",
                "id": "fontIconAdminConsoleClose",
                "isVisible": true,
                "left": "5px",
                "skin": "sknFontIconSearchCross16px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.userwidgetmodel.lblClearSearch\")",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxClearAdminConsoleSearch.add(fontIconAdminConsoleClose);
            flxAdminConsoleSearchContainer.add(fontIconSearch, tbxAdminConsoleSearch, flxClearAdminConsoleSearch);
            flxAdminConsoleFiltersAndHeaders.add(flxAdminConsoleFilterIcon, flxAdminConsoleSearchContainer);
            var flxAdminConsoleResults = new kony.ui.FlexScrollContainer({
                "allowHorizontalBounce": false,
                "allowVerticalBounce": true,
                "bounces": true,
                "clipBounds": false,
                "enableScrolling": true,
                "height": "290px",
                "horizontalScrollIndicator": true,
                "id": "flxAdminConsoleResults",
                "isVisible": true,
                "layoutType": kony.flex.FLOW_HORIZONTAL,
                "reverseLayoutDirection": false,
                "left": 0,
                "pagingEnabled": false,
                "scrollDirection": kony.flex.SCROLL_VERTICAL,
                "skin": "slFSbox",
                "top": "80dp",
                "verticalScrollIndicator": true,
                "width": "1000dp"
            }, {}, {});
            flxAdminConsoleResults.setDefaultUnit(kony.flex.DP);
            var flxInnerAdminConsoleResults = new kony.ui.FlexContainer({
                "clipBounds": false,
                "height": "100%",
                "id": "flxInnerAdminConsoleResults",
                "isVisible": true,
                "layoutType": kony.flex.FLOW_VERTICAL,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0dp",
                "width": "100%",
                "zIndex": 1
            }, {}, {});
            flxInnerAdminConsoleResults.setDefaultUnit(kony.flex.DP);
            var flxAdminConsoleResultHeader = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": false,
                "height": "60px",
                "id": "flxAdminConsoleResultHeader",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0px",
                "isModalContainer": false,
                "right": "35dp",
                "skin": "CopyCopyslFbox0e8dde5f64bde4c",
                "top": "0dp",
                "width": "100%",
                "zIndex": 1
            }, {}, {});
            flxAdminConsoleResultHeader.setDefaultUnit(kony.flex.DP);
            var flxAdminConsoleHeader = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": false,
                "height": "60dp",
                "id": "flxAdminConsoleHeader",
                "isVisible": true,
                "layoutType": kony.flex.FLOW_HORIZONTAL,
                "left": "35dp",
                "isModalContainer": false,
                "right": "30px",
                "skin": "slFbox",
                "top": "0px",
                "width": "100%",
                "zIndex": 1
            }, {}, {});
            flxAdminConsoleHeader.setDefaultUnit(kony.flex.DP);
            var flxEvent = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "100%",
                "id": "flxEvent",
                "isVisible": true,
                "layoutType": kony.flex.FLOW_HORIZONTAL,
                "left": "20px",
                "isModalContainer": false,
                "skin": "slFbox",
                "width": "8%",
                "zIndex": 1
            }, {}, {});
            flxEvent.setDefaultUnit(kony.flex.DP);
            var lblEvent = new kony.ui.Label({
                "centerY": "50%",
                "id": "lblEvent",
                "isVisible": true,
                "left": 0,
                "skin": "sknlblLato696c7312px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmLogs.lblEvent\")",
                "top": 0,
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {
                "hoverSkin": "sknlblLato00000011px"
            });
            var fllxSortEvent = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "centerY": "50%",
                "clipBounds": true,
                "height": "20dp",
                "id": "fllxSortEvent",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "CopyslFbox0efc73ba91cad4b",
                "width": "20px",
                "zIndex": 1
            }, {}, {});
            fllxSortEvent.setDefaultUnit(kony.flex.DP);
            var imgSortEvent = new kony.ui.Image2({
                "centerX": "50%",
                "centerY": "50%",
                "height": "10dp",
                "id": "imgSortEvent",
                "isVisible": false,
                "skin": "slImage",
                "src": "filter.png",
                "top": "0dp",
                "width": "10dp",
                "zIndex": 1
            }, {
                "imageScaleMode": constants.IMAGE_SCALE_MODE_MAINTAIN_ASPECT_RATIO,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var lblSortEvent = new kony.ui.Label({
                "centerY": "50%",
                "height": "15dp",
                "id": "lblSortEvent",
                "isVisible": true,
                "left": "5dp",
                "skin": "sknIcon15px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmAlertsManagement.lblIconChannels\")",
                "width": "15dp",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            fllxSortEvent.add(imgSortEvent, lblSortEvent);
            flxEvent.add(lblEvent, fllxSortEvent);
            var flxAdminConsoleUserName = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "100%",
                "id": "flxAdminConsoleUserName",
                "isVisible": true,
                "layoutType": kony.flex.FLOW_HORIZONTAL,
                "left": "10px",
                "isModalContainer": false,
                "skin": "slFbox",
                "width": "13%",
                "zIndex": 1
            }, {}, {});
            flxAdminConsoleUserName.setDefaultUnit(kony.flex.DP);
            var lblAdminConsoleUserName = new kony.ui.Label({
                "centerY": "50%",
                "id": "lblAdminConsoleUserName",
                "isVisible": true,
                "left": "0px",
                "skin": "sknlblLato696c7312px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmLogs.employeeUseName\")",
                "top": 0,
                "width": "55%",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {
                "hoverSkin": "sknlblLato00000011px"
            });
            var flxSortAdminConsoleUserName = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "centerY": "50%",
                "clipBounds": true,
                "height": "20dp",
                "id": "flxSortAdminConsoleUserName",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "CopyslFbox0efc73ba91cad4b",
                "width": "20px",
                "zIndex": 1
            }, {}, {});
            flxSortAdminConsoleUserName.setDefaultUnit(kony.flex.DP);
            var imgSortAdminConsoleUserName = new kony.ui.Image2({
                "centerX": "50%",
                "centerY": "50%",
                "height": "10dp",
                "id": "imgSortAdminConsoleUserName",
                "isVisible": false,
                "skin": "slImage",
                "src": "sorting3x.png",
                "top": "0dp",
                "width": "10dp",
                "zIndex": 1
            }, {
                "imageScaleMode": constants.IMAGE_SCALE_MODE_MAINTAIN_ASPECT_RATIO,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var lblSortAdminConsoleUserName = new kony.ui.Label({
                "centerY": "50%",
                "height": "15dp",
                "id": "lblSortAdminConsoleUserName",
                "isVisible": true,
                "left": "5dp",
                "skin": "sknIcon15px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmAlertsManagement.lblSortName\")",
                "width": "15dp",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxSortAdminConsoleUserName.add(imgSortAdminConsoleUserName, lblSortAdminConsoleUserName);
            flxAdminConsoleUserName.add(lblAdminConsoleUserName, flxSortAdminConsoleUserName);
            var flxUserRole = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "100%",
                "id": "flxUserRole",
                "isVisible": true,
                "layoutType": kony.flex.FLOW_HORIZONTAL,
                "isModalContainer": false,
                "skin": "slFbox",
                "width": "12%",
                "zIndex": 1
            }, {}, {});
            flxUserRole.setDefaultUnit(kony.flex.DP);
            var lblUserRole = new kony.ui.Label({
                "centerY": "50%",
                "id": "lblUserRole",
                "isVisible": true,
                "skin": "sknlblLato696c7312px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmLogs.lblUserRole\")",
                "top": 0,
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {
                "hoverSkin": "sknlblLato00000011px"
            });
            var flxSortUserRole = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "centerY": "50%",
                "clipBounds": true,
                "height": "20dp",
                "id": "flxSortUserRole",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "3dp",
                "isModalContainer": false,
                "skin": "CopyslFbox0efc73ba91cad4b",
                "width": "20px",
                "zIndex": 1
            }, {}, {});
            flxSortUserRole.setDefaultUnit(kony.flex.DP);
            var imgSortUserRole = new kony.ui.Image2({
                "centerX": "50%",
                "centerY": "50%",
                "height": "10dp",
                "id": "imgSortUserRole",
                "isVisible": false,
                "skin": "slImage",
                "src": "filter.png",
                "top": "0dp",
                "width": "10dp",
                "zIndex": 1
            }, {
                "imageScaleMode": constants.IMAGE_SCALE_MODE_MAINTAIN_ASPECT_RATIO,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var lblSortUserRole = new kony.ui.Label({
                "centerY": "50%",
                "height": "15dp",
                "id": "lblSortUserRole",
                "isVisible": true,
                "left": "5dp",
                "skin": "sknIcon15px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmAlertsManagement.lblIconChannels\")",
                "width": "15dp",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxSortUserRole.add(imgSortUserRole, lblSortUserRole);
            flxUserRole.add(lblUserRole, flxSortUserRole);
            var flxAdminConsoleModuleName = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "100%",
                "id": "flxAdminConsoleModuleName",
                "isVisible": true,
                "layoutType": kony.flex.FLOW_HORIZONTAL,
                "isModalContainer": false,
                "skin": "slFbox",
                "width": "16%",
                "zIndex": 1
            }, {}, {});
            flxAdminConsoleModuleName.setDefaultUnit(kony.flex.DP);
            var lblAdminConsoleModuleName = new kony.ui.Label({
                "centerY": "50%",
                "id": "lblAdminConsoleModuleName",
                "isVisible": true,
                "left": "0px",
                "skin": "sknlblLato696c7312px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmLogs.lblAdminConsoleModuleName\")",
                "top": 0,
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {
                "hoverSkin": "sknlblLato00000011px"
            });
            var flxSortAdminConsoleModuleName = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "centerY": "50%",
                "clipBounds": true,
                "height": "20dp",
                "id": "flxSortAdminConsoleModuleName",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "CopyslFbox0efc73ba91cad4b",
                "width": "20px",
                "zIndex": 1
            }, {}, {});
            flxSortAdminConsoleModuleName.setDefaultUnit(kony.flex.DP);
            var imgSortAdminConsoleModuleName = new kony.ui.Image2({
                "centerX": "50%",
                "centerY": "50%",
                "height": "10dp",
                "id": "imgSortAdminConsoleModuleName",
                "isVisible": false,
                "skin": "slImage",
                "src": "sorting3x.png",
                "top": "0dp",
                "width": "10dp",
                "zIndex": 1
            }, {
                "imageScaleMode": constants.IMAGE_SCALE_MODE_MAINTAIN_ASPECT_RATIO,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var lblSortAdminConsoleModuleName = new kony.ui.Label({
                "centerY": "50%",
                "height": "15dp",
                "id": "lblSortAdminConsoleModuleName",
                "isVisible": true,
                "left": "5dp",
                "skin": "sknIcon15px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmAlertsManagement.lblSortName\")",
                "width": "15dp",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxSortAdminConsoleModuleName.add(imgSortAdminConsoleModuleName, lblSortAdminConsoleModuleName);
            flxAdminConsoleModuleName.add(lblAdminConsoleModuleName, flxSortAdminConsoleModuleName);
            var flxAdminConsoleDateAndTime = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "100%",
                "id": "flxAdminConsoleDateAndTime",
                "isVisible": true,
                "layoutType": kony.flex.FLOW_HORIZONTAL,
                "isModalContainer": false,
                "skin": "slFbox",
                "width": "20%",
                "zIndex": 1
            }, {}, {});
            flxAdminConsoleDateAndTime.setDefaultUnit(kony.flex.DP);
            var lblAdminConsoleDateAndTime = new kony.ui.Label({
                "centerY": "50%",
                "id": "lblAdminConsoleDateAndTime",
                "isVisible": true,
                "left": "0px",
                "skin": "sknlblLato696c7312px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmCustomerManagement.lblDateAndTime\")",
                "top": 0,
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {
                "hoverSkin": "sknlblLato00000011px"
            });
            var flxSortAdminConsoleDateAndTime = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "centerY": "50%",
                "clipBounds": true,
                "height": "20dp",
                "id": "flxSortAdminConsoleDateAndTime",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "CopyslFbox0efc73ba91cad4b",
                "width": "20px",
                "zIndex": 1
            }, {}, {});
            flxSortAdminConsoleDateAndTime.setDefaultUnit(kony.flex.DP);
            var imgSortAdminConsoleDateAndTime = new kony.ui.Image2({
                "centerX": "50%",
                "centerY": "50%",
                "height": "10dp",
                "id": "imgSortAdminConsoleDateAndTime",
                "isVisible": false,
                "skin": "slImage",
                "src": "sorting3x.png",
                "top": "0dp",
                "width": "10dp",
                "zIndex": 1
            }, {
                "imageScaleMode": constants.IMAGE_SCALE_MODE_MAINTAIN_ASPECT_RATIO,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var lblSortAdminConsoleDateAndTime = new kony.ui.Label({
                "centerY": "50%",
                "height": "15dp",
                "id": "lblSortAdminConsoleDateAndTime",
                "isVisible": true,
                "left": "5dp",
                "skin": "sknIcon15px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmAlertsManagement.lblSortName\")",
                "width": "15dp",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxSortAdminConsoleDateAndTime.add(imgSortAdminConsoleDateAndTime, lblSortAdminConsoleDateAndTime);
            flxAdminConsoleDateAndTime.add(lblAdminConsoleDateAndTime, flxSortAdminConsoleDateAndTime);
            var flxAdminConsoleStatus = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "100%",
                "id": "flxAdminConsoleStatus",
                "isVisible": true,
                "layoutType": kony.flex.FLOW_HORIZONTAL,
                "left": "0",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0dp",
                "width": "13%",
                "zIndex": 1
            }, {}, {});
            flxAdminConsoleStatus.setDefaultUnit(kony.flex.DP);
            var lblAdminConsoleStatus = new kony.ui.Label({
                "centerY": "50%",
                "id": "lblAdminConsoleStatus",
                "isVisible": true,
                "left": "0px",
                "skin": "sknlblLato696c7312px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.roles.STATUS\")",
                "top": 0,
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {
                "hoverSkin": "sknlblLato00000011px"
            });
            var flxSortAdminConsoleStatus = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "centerY": "50%",
                "clipBounds": true,
                "height": "20dp",
                "id": "flxSortAdminConsoleStatus",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "CopyslFbox0efc73ba91cad4b",
                "width": "20px",
                "zIndex": 1
            }, {}, {});
            flxSortAdminConsoleStatus.setDefaultUnit(kony.flex.DP);
            var imgSortAdminConsoleStatus = new kony.ui.Image2({
                "centerX": "50%",
                "centerY": "50%",
                "height": "10dp",
                "id": "imgSortAdminConsoleStatus",
                "isVisible": false,
                "skin": "slImage",
                "src": "filter.png",
                "top": "0dp",
                "width": "10dp",
                "zIndex": 1
            }, {
                "imageScaleMode": constants.IMAGE_SCALE_MODE_MAINTAIN_ASPECT_RATIO,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var lblSortAdminConsoleStatus = new kony.ui.Label({
                "centerY": "50%",
                "height": "15dp",
                "id": "lblSortAdminConsoleStatus",
                "isVisible": true,
                "left": "5dp",
                "skin": "sknIcon15px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmAlertsManagement.lblIconChannels\")",
                "width": "15dp",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxSortAdminConsoleStatus.add(imgSortAdminConsoleStatus, lblSortAdminConsoleStatus);
            flxAdminConsoleStatus.add(lblAdminConsoleStatus, flxSortAdminConsoleStatus);
            var flxAdminConsoleJSONData = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "100%",
                "id": "flxAdminConsoleJSONData",
                "isVisible": true,
                "layoutType": kony.flex.FLOW_HORIZONTAL,
                "reverseLayoutDirection": false,
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0",
                "width": "10%",
                "zIndex": 1
            }, {}, {});
            flxAdminConsoleJSONData.setDefaultUnit(kony.flex.DP);
            var lblAdminConsoleJSONData = new kony.ui.Label({
                "centerY": "50%",
                "id": "lblAdminConsoleJSONData",
                "isVisible": true,
                "left": "0px",
                "skin": "sknlblLato696c7312px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.View.JSONData\")",
                "top": 0,
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxAdminConsoleJSONData.add(lblAdminConsoleJSONData);
            flxAdminConsoleHeader.add(flxEvent, flxAdminConsoleUserName, flxUserRole, flxAdminConsoleModuleName, flxAdminConsoleDateAndTime, flxAdminConsoleStatus, flxAdminConsoleJSONData);
            var lblAdminConsoleHeaderSeperator = new kony.ui.Label({
                "bottom": 0,
                "height": "1px",
                "id": "lblAdminConsoleHeaderSeperator",
                "isVisible": true,
                "left": "35px",
                "right": "50px",
                "skin": "sknLblHeaderLine",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmCSR.lblSeperator\")",
                "width": "93%",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxAdminConsoleResultHeader.add(flxAdminConsoleHeader, lblAdminConsoleHeaderSeperator);
            var flxAdminConsoleResultSegment = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                "clipBounds": false,
                "id": "flxAdminConsoleResultSegment",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "isModalContainer": false,
                "right": "0dp",
                "skin": "slFbox",
                "top": "0dp",
                "width": "100%",
                "zIndex": 1
            }, {}, {});
            flxAdminConsoleResultSegment.setDefaultUnit(kony.flex.DP);
            var segAdminConsoleResult = new com.adminConsole.common.listingSegmentLogs({
                "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                "id": "segAdminConsoleResult",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "masterType": constants.MASTER_TYPE_DEFAULT,
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0dp",
                "width": "100%",
                "overrides": {
                    "contextualMenu.imgOption1": {
                        "src": "imagedrag.png"
                    },
                    "contextualMenu.imgOption2": {
                        "src": "edit2x.png"
                    },
                    "contextualMenu.imgOption3": {
                        "src": "imagedrag.png"
                    },
                    "contextualMenu.imgOption4": {
                        "src": "deactive_2x.png"
                    },
                    "flxContextualMenu": {
                        "isVisible": false
                    },
                    "flxPagination": {
                        "isVisible": false
                    },
                    "listingSegmentLogs": {
                        "width": "100%"
                    },
                    "segListing": {
                        "top": "0px",
                        "width": "100%"
                    }
                }
            }, {
                "overrides": {}
            }, {
                "overrides": {}
            });
            flxAdminConsoleResultSegment.add(segAdminConsoleResult);
            flxInnerAdminConsoleResults.add(flxAdminConsoleResultHeader, flxAdminConsoleResultSegment);
            flxAdminConsoleResults.add(flxInnerAdminConsoleResults);
            var flxPaginationAdminConsoleLog = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "bottom": "35dp",
                "clipBounds": true,
                "height": "70px",
                "id": "flxPaginationAdminConsoleLog",
                "isVisible": false,
                "layoutType": kony.flex.FREE_FORM,
                "left": "35dp",
                "isModalContainer": false,
                "skin": "CopyCopyCopyslFbox0d090d27f416e48",
                "top": "0dp",
                "width": "992dp",
                "zIndex": 1
            }, {}, {});
            flxPaginationAdminConsoleLog.setDefaultUnit(kony.flex.DP);
            var pagination1 = new com.adminConsole.common.pagination1({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "height": "220dp",
                "id": "pagination1",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "masterType": constants.MASTER_TYPE_DEFAULT,
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0dp",
                "width": "100%",
                "overrides": {
                    "pagination1": {
                        "bottom": "viz.val_cleared",
                        "centerX": "viz.val_cleared",
                        "centerY": "viz.val_cleared",
                        "height": "220dp",
                        "left": "0dp",
                        "maxHeight": "viz.val_cleared",
                        "maxWidth": "viz.val_cleared",
                        "minHeight": "viz.val_cleared",
                        "minWidth": "viz.val_cleared",
                        "right": "viz.val_cleared",
                        "top": "0dp",
                        "width": "100%"
                    }
                }
            }, {
                "overrides": {}
            }, {
                "overrides": {}
            });
            flxPaginationAdminConsoleLog.add(pagination1);
            flxAdminConsoleLog.add(flxAdminConsoleFiltersAndHeaders, flxAdminConsoleResults, flxPaginationAdminConsoleLog);
            var flxDropDownDetail1 = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": false,
                "height": "380px",
                "id": "flxDropDownDetail1",
                "isVisible": false,
                "layoutType": kony.flex.FREE_FORM,
                "left": "440dp",
                "isModalContainer": false,
                "skin": "skntxtbxNormald7d9e0",
                "top": "185px",
                "width": "200dp",
                "zIndex": 200
            }, {}, {
                "hoverSkin": "skntxtbxhover11abeb"
            });
            flxDropDownDetail1.setDefaultUnit(kony.flex.DP);
            var selectDate = new com.adminConsole.selectDate.selectDate({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "height": "100%",
                "id": "selectDate",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "masterType": constants.MASTER_TYPE_DEFAULT,
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0dp",
                "width": "100%",
                "overrides": {
                    "btnApply": {
                        "centerX": "viz.val_cleared",
                        "right": "5%",
                        "top": undefined
                    },
                    "calFrom": {
                        "centerX": "viz.val_cleared",
                        "left": "10px",
                        "right": "viz.val_cleared",
                        "width": undefined
                    },
                    "calTo": {
                        "centerX": "viz.val_cleared",
                        "left": "10px",
                        "right": undefined,
                        "width": undefined
                    },
                    "flxDate": {
                        "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                        "width": "100%"
                    },
                    "flxDatepicker": {
                        "bottom": undefined,
                        "top": undefined
                    },
                    "flxDays": {
                        "left": "0dp",
                        "top": "0dp"
                    },
                    "selectDate": {
                        "height": "100%",
                        "width": "100%"
                    }
                }
            }, {
                "overrides": {}
            }, {
                "overrides": {}
            });
            flxDropDownDetail1.add(selectDate);
            var flxNoResultsFound = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "250dp",
                "id": "flxNoResultsFound",
                "isVisible": false,
                "layoutType": kony.flex.FREE_FORM,
                "left": "35dp",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0dp",
                "width": "992dp",
                "zIndex": 15
            }, {}, {});
            flxNoResultsFound.setDefaultUnit(kony.flex.DP);
            var rtxNorecords = new kony.ui.RichText({
                "centerX": "50%",
                "centerY": "50%",
                "id": "rtxNorecords",
                "isVisible": true,
                "skin": "sknRtxLato84939e12Px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmLogs.rtxNorecords\")",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_CENTER,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxNoResultsFound.add(rtxNorecords);
            var flxFiltersAndHeaders = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                "clipBounds": false,
                "id": "flxFiltersAndHeaders",
                "isVisible": false,
                "layoutType": kony.flex.FLOW_VERTICAL,
                "isModalContainer": false,
                "right": "55dp",
                "skin": "slFbox",
                "top": "140dp",
                "width": "865dp",
                "zIndex": 20
            }, {}, {});
            flxFiltersAndHeaders.setDefaultUnit(kony.flex.DP);
            var flxArrowImageCustomer = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "bottom": "-2dp",
                "clipBounds": true,
                "height": "12dp",
                "id": "flxArrowImageCustomer",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "slFbox",
                "width": "100%",
                "zIndex": 2
            }, {}, {});
            flxArrowImageCustomer.setDefaultUnit(kony.flex.DP);
            var imgUpArrowCustomer = new kony.ui.Image2({
                "height": "12dp",
                "id": "imgUpArrowCustomer",
                "isVisible": true,
                "right": "15dp",
                "skin": "slImage",
                "src": "uparrow_dropdown.png",
                "top": "0dp",
                "width": "15dp",
                "zIndex": 1
            }, {
                "imageScaleMode": constants.IMAGE_SCALE_MODE_FIT_TO_DIMENSIONS,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxArrowImageCustomer.add(imgUpArrowCustomer);
            var flxFilters3 = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": false,
                "height": "173dp",
                "id": "flxFilters3",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "isModalContainer": false,
                "right": "0dp",
                "skin": "sknFlxBgF8F9FAbrD7D9E01pxRd3pxShd0D0D11",
                "top": "0px",
                "width": "100%",
                "zIndex": 1
            }, {}, {});
            flxFilters3.setDefaultUnit(kony.flex.DP);
            var flxFontIconCustomerClose = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "16dp",
                "id": "flxFontIconCustomerClose",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "isModalContainer": false,
                "right": "10dp",
                "skin": "sknCursor",
                "top": "10dp",
                "width": "16dp"
            }, {}, {
                "hoverSkin": "sknCursor"
            });
            flxFontIconCustomerClose.setDefaultUnit(kony.flex.DP);
            var lblFontIconCustomerClose = new kony.ui.Label({
                "id": "lblFontIconCustomerClose",
                "isVisible": true,
                "left": "0dp",
                "skin": "sknIcon485C7513px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.userwidgetmodel.lblClearSearch\")",
                "top": "0dp",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxFontIconCustomerClose.add(lblFontIconCustomerClose);
            var flxHeaderCustomerFilter = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                "clipBounds": true,
                "id": "flxHeaderCustomerFilter",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "10dp",
                "isModalContainer": false,
                "right": "20dp",
                "skin": "slFbox",
                "top": "10dp",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {}, {});
            flxHeaderCustomerFilter.setDefaultUnit(kony.flex.DP);
            var lblFilterByCustomer = new kony.ui.Label({
                "id": "lblFilterByCustomer",
                "isVisible": true,
                "left": "0px",
                "skin": "slLabel0d20174dce8ea42",
                "text": "Filter Data By",
                "top": "0px",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxHeaderCustomerFilter.add(lblFilterByCustomer);
            var flxFiltersList3 = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                "clipBounds": false,
                "id": "flxFiltersList3",
                "isVisible": true,
                "layoutType": kony.flex.FLOW_HORIZONTAL,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "36dp",
                "width": "100%",
                "zIndex": 2
            }, {}, {});
            flxFiltersList3.setDefaultUnit(kony.flex.DP);
            var flxFilterCriteriaCustomer1 = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "68dp",
                "id": "flxFilterCriteriaCustomer1",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "10px",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0px",
                "width": "200px",
                "zIndex": 1
            }, {}, {});
            flxFilterCriteriaCustomer1.setDefaultUnit(kony.flex.DP);
            var lblSearchParamCustomer1 = new kony.ui.Label({
                "id": "lblSearchParamCustomer1",
                "isVisible": true,
                "left": "0px",
                "skin": "slLabel0d20174dce8ea42",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmLogs.lblSearchParamAdmin1\")",
                "top": "0px",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var listBoxSearchParamCustomer3 = new kony.ui.ListBox({
                "focusSkin": "sknlbxBgffffffBorder11abebRadius3Px14px",
                "height": "40dp",
                "id": "listBoxSearchParamCustomer3",
                "isVisible": true,
                "left": "0dp",
                "skin": "sknlbxBgffffffBorderc1c9ceRadius3Px",
                "top": "25px",
                "width": "200px",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_CENTER,
                "padding": [3, 0, 0, 0],
                "paddingInPixel": false
            }, {
                "hoverSkin": "lstboxcursor",
                "multiSelect": false
            });
            flxFilterCriteriaCustomer1.add(lblSearchParamCustomer1, listBoxSearchParamCustomer3);
            var flxFilterCriteriaCustomer2 = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "68dp",
                "id": "flxFilterCriteriaCustomer2",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "15px",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0px",
                "width": "200px",
                "zIndex": 1
            }, {}, {});
            flxFilterCriteriaCustomer2.setDefaultUnit(kony.flex.DP);
            var lblSearchParam2 = new kony.ui.Label({
                "id": "lblSearchParam2",
                "isVisible": true,
                "left": "0px",
                "skin": "slLabel0d20174dce8ea42",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmLogs.lblSearchParam2\")",
                "top": "0px",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var listBoxSearchParam2 = new kony.ui.ListBox({
                "focusSkin": "sknlbxBgffffffBorder11abebRadius3Px14px",
                "height": "40dp",
                "id": "listBoxSearchParam2",
                "isVisible": true,
                "left": "0dp",
                "skin": "sknlbxBgffffffBorderc1c9ceRadius3Px",
                "top": "25px",
                "width": "200px",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_CENTER,
                "padding": [3, 0, 0, 0],
                "paddingInPixel": false
            }, {
                "hoverSkin": "lstboxcursor",
                "multiSelect": false
            });
            flxFilterCriteriaCustomer2.add(lblSearchParam2, listBoxSearchParam2);
            var flxFilterCriteriaCustomer3 = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": false,
                "height": "68px",
                "id": "flxFilterCriteriaCustomer3",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "15px",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0dp",
                "width": "200px",
                "zIndex": 1
            }, {}, {});
            flxFilterCriteriaCustomer3.setDefaultUnit(kony.flex.DP);
            var lblCol3 = new kony.ui.Label({
                "id": "lblCol3",
                "isVisible": true,
                "left": "0dp",
                "skin": "sknlblLatoRegular484b5213px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmLogs.lblCol1\")",
                "top": "0px",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var flxDropDown3 = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": false,
                "height": "41px",
                "id": "flxDropDown3",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "sknflxffffffope1e5edcsr",
                "top": "25px",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {}, {
                "hoverSkin": "skntxtbxhover11abeb"
            });
            flxDropDown3.setDefaultUnit(kony.flex.DP);
            var datePickerCustomerLog = new kony.ui.CustomWidget({
                "id": "datePickerCustomerLog",
                "isVisible": true,
                "left": "1dp",
                "right": "3px",
                "bottom": "1px",
                "top": "5dp",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 10,
                "clipBounds": true
            }, {
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {
                "widgetName": "dateRangePicker",
                "drops": "up",
                "event": null,
                "maxDate": "true",
                "opens": "center",
                "rangeType": "",
                "resetData": null,
                "type": "list",
                "value": ""
            });
            var flxCloseCal3 = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "centerY": "50%",
                "clipBounds": true,
                "height": "10px",
                "id": "flxCloseCal3",
                "isVisible": false,
                "layoutType": kony.flex.FREE_FORM,
                "isModalContainer": false,
                "right": "10px",
                "skin": "slFbox",
                "width": "12px",
                "zIndex": 2
            }, {}, {
                "hoverSkin": "sknCursor"
            });
            flxCloseCal3.setDefaultUnit(kony.flex.DP);
            var lblCloseCal3 = new kony.ui.Label({
                "centerY": "50%",
                "id": "lblCloseCal3",
                "isVisible": true,
                "right": "0dp",
                "skin": "sknfontIconDescDownArrow12px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmAlertsManagement.lblAlertPreviewClose\")",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxCloseCal3.add(lblCloseCal3);
            flxDropDown3.add(datePickerCustomerLog, flxCloseCal3);
            flxFilterCriteriaCustomer3.add(lblCol3, flxDropDown3);
            var flxFilterCriteriaCustomer4 = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                "clipBounds": false,
                "id": "flxFilterCriteriaCustomer4",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "15px",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0dp",
                "width": "200px",
                "zIndex": 1
            }, {}, {});
            flxFilterCriteriaCustomer4.setDefaultUnit(kony.flex.DP);
            var lblAddAlertTypeApps = new kony.ui.Label({
                "id": "lblAddAlertTypeApps",
                "isVisible": true,
                "left": "0dp",
                "skin": "sknlblLatoRegular484b5213px",
                "text": "View Column Items",
                "top": "0dp",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var customListboxApps = new com.adminConsole.alerts.customListbox({
                "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                "id": "customListboxApps",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "masterType": constants.MASTER_TYPE_DEFAULT,
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "25dp",
                "width": "100%",
                "zIndex": 25,
                "overrides": {
                    "customListbox": {
                        "top": "25dp",
                        "zIndex": 25
                    },
                    "flxSegmentList": {
                        "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                        "clipBounds": false,
                        "isVisible": false,
                        "zIndex": 1
                    },
                    "flxSelectAll": {
                        "isVisible": true
                    },
                    "imgCheckBox": {
                        "src": "checkboxnormal.png"
                    },
                    "segList": {
                        "data": [{
                            "imgCheckBox": "",
                            "lblDescription": ""
                        }],
                        "height": "100dp",
                        "zIndex": 1
                    }
                }
            }, {
                "overrides": {}
            }, {
                "overrides": {}
            });
            flxFilterCriteriaCustomer4.add(lblAddAlertTypeApps, customListboxApps);
            flxFiltersList3.add(flxFilterCriteriaCustomer1, flxFilterCriteriaCustomer2, flxFilterCriteriaCustomer3, flxFilterCriteriaCustomer4);
            var flxButtons = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                "clipBounds": true,
                "id": "flxButtons",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "isModalContainer": false,
                "right": 0,
                "skin": "slFbox",
                "top": "135dp",
                "width": kony.flex.USE_PREFFERED_SIZE
            }, {}, {});
            flxButtons.setDefaultUnit(kony.flex.DP);
            var btnCustomerActivityLogsSaveFilter = new kony.ui.Button({
                "id": "btnCustomerActivityLogsSaveFilter",
                "isVisible": true,
                "right": "85dp",
                "skin": "sknBtnLatoRegular11abeb14px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmLogs.btnAdvSearch\")",
                "top": "2dp",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_CENTER,
                "displayText": true,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var btnAdd3 = new kony.ui.Button({
                "focusSkin": "sknbtnf7f7faLatoRegular12Px485c75Border1px485C75Radius20px",
                "height": "22dp",
                "id": "btnAdd3",
                "isVisible": true,
                "right": "10dp",
                "skin": "sknbtnf7f7faLatoRegular12Px485c75Border1px485C75Radius20px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmCustomerManagement.btnApply\")",
                "top": "0dp",
                "width": "60px",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_CENTER,
                "displayText": true,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {
                "hoverSkin": "sknbtnffffffLatoRegular12Px485c75Border1px485C75Radius20px"
            });
            flxButtons.add(btnCustomerActivityLogsSaveFilter, btnAdd3);
            flxFilters3.add(flxFontIconCustomerClose, flxHeaderCustomerFilter, flxFiltersList3, flxButtons);
            flxFiltersAndHeaders.add(flxArrowImageCustomer, flxFilters3);
            var flxTrasactionalFilter = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                "clipBounds": false,
                "id": "flxTrasactionalFilter",
                "isVisible": false,
                "layoutType": kony.flex.FLOW_VERTICAL,
                "isModalContainer": false,
                "right": "50dp",
                "skin": "slFbox",
                "top": "50dp",
                "width": "865px",
                "zIndex": 50
            }, {}, {});
            flxTrasactionalFilter.setDefaultUnit(kony.flex.DP);
            var flxArrowImage = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "bottom": "-1dp",
                "clipBounds": true,
                "height": "12dp",
                "id": "flxArrowImage",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "slFbox",
                "width": "100%",
                "zIndex": 2
            }, {}, {});
            flxArrowImage.setDefaultUnit(kony.flex.DP);
            var imgUpArrow = new kony.ui.Image2({
                "height": "12dp",
                "id": "imgUpArrow",
                "isVisible": true,
                "right": "10dp",
                "skin": "slImage",
                "src": "uparrow_dropdown.png",
                "top": "0dp",
                "width": "15dp",
                "zIndex": 2
            }, {
                "imageScaleMode": constants.IMAGE_SCALE_MODE_FIT_TO_DIMENSIONS,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxArrowImage.add(imgUpArrow);
            var flxTransactionLogsFilter = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "bottom": "0dp",
                "clipBounds": false,
                "height": "176px",
                "id": "flxTransactionLogsFilter",
                "isVisible": true,
                "layoutType": kony.flex.FLOW_VERTICAL,
                "left": "0px",
                "isModalContainer": false,
                "skin": "sknFlxBgF8F9FAbrD7D9E01pxRd3pxShd0D0D11",
                "top": "0px",
                "width": "865dp",
                "zIndex": 1
            }, {}, {});
            flxTransactionLogsFilter.setDefaultUnit(kony.flex.DP);
            var flxHeader1 = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "35px",
                "id": "flxHeader1",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0dp",
                "width": "100%"
            }, {}, {});
            flxHeader1.setDefaultUnit(kony.flex.DP);
            var lblCustomerName = new kony.ui.Label({
                "id": "lblCustomerName",
                "isVisible": true,
                "left": "10px",
                "skin": "sknlblLatoRegular484b5213px",
                "text": "Filter Data By",
                "top": "10px",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var flxImage = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "20px",
                "id": "flxImage",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "isModalContainer": false,
                "right": "10px",
                "skin": "CopyslFbox0efc73ba91cad4b",
                "top": "12px",
                "width": "20px"
            }, {}, {});
            flxImage.setDefaultUnit(kony.flex.DP);
            var imgStatus = new kony.ui.Image2({
                "height": "100%",
                "id": "imgStatus",
                "isVisible": false,
                "left": "0px",
                "skin": "slImage",
                "src": "img_down_arrow.png",
                "top": "0px",
                "width": "100%",
                "zIndex": 1
            }, {
                "imageScaleMode": constants.IMAGE_SCALE_MODE_MAINTAIN_ASPECT_RATIO,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var lblIconStatus = new kony.ui.Label({
                "centerY": "50%",
                "height": "15dp",
                "id": "lblIconStatus",
                "isVisible": true,
                "left": "0dp",
                "skin": "sknIcon485C7513px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.userwidgetmodel.lblClearSearch\")",
                "width": "15dp",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxImage.add(imgStatus, lblIconStatus);
            flxHeader1.add(lblCustomerName, flxImage);
            var flxFilters = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                "clipBounds": false,
                "id": "flxFilters",
                "isVisible": true,
                "layoutType": kony.flex.FLOW_VERTICAL,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0px",
                "width": "100%",
                "zIndex": 1
            }, {}, {});
            flxFilters.setDefaultUnit(kony.flex.DP);
            var flxFiltersList = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                "clipBounds": false,
                "id": "flxFiltersList",
                "isVisible": true,
                "layoutType": kony.flex.FLOW_HORIZONTAL,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0dp",
                "width": "100%",
                "zIndex": 60
            }, {}, {});
            flxFiltersList.setDefaultUnit(kony.flex.DP);
            var flxFilterCriteria1 = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "68dp",
                "id": "flxFilterCriteria1",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "10px",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0px",
                "width": "200px",
                "zIndex": 1
            }, {}, {});
            flxFilterCriteria1.setDefaultUnit(kony.flex.DP);
            var lblSearchParam1 = new kony.ui.Label({
                "id": "lblSearchParam1",
                "isVisible": true,
                "left": "0px",
                "skin": "slLabel0d20174dce8ea42",
                "text": "Activity Type",
                "top": "0px",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var listBoxSearchParam1 = new kony.ui.ListBox({
                "focusSkin": "sknlbxBgffffffBorder11abebRadius3Px14px",
                "height": "40dp",
                "id": "listBoxSearchParam1",
                "isVisible": true,
                "left": "0dp",
                "masterData": [
                    ["Select", "Select service"]
                ],
                "skin": "sknlbxBgffffffBorderc1c9ceRadius3Px",
                "top": "25px",
                "width": "100%",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_CENTER,
                "padding": [3, 0, 0, 0],
                "paddingInPixel": false
            }, {
                "hoverSkin": "lstboxcursor",
                "multiSelect": false
            });
            flxFilterCriteria1.add(lblSearchParam1, listBoxSearchParam1);
            var flxFilterCriteria2 = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": false,
                "height": "68px",
                "id": "flxFilterCriteria2",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "15dp",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0dp",
                "width": "200px",
                "zIndex": 20
            }, {}, {});
            flxFilterCriteria2.setDefaultUnit(kony.flex.DP);
            var flxDropDown01 = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": false,
                "height": "41px",
                "id": "flxDropDown01",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "sknflxffffffoptemplateop3px",
                "top": "25px",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {}, {
                "hoverSkin": "sknFlxSegRowHover11abeb"
            });
            flxDropDown01.setDefaultUnit(kony.flex.DP);
            var datePickerTransaction = new kony.ui.CustomWidget({
                "id": "datePickerTransaction",
                "isVisible": true,
                "left": "1dp",
                "right": "3dp",
                "top": "7dp",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "height": "100%",
                "zIndex": 10,
                "clipBounds": true
            }, {
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {
                "widgetName": "dateRangePicker",
                "drops": "left",
                "event": null,
                "maxDate": "true",
                "opens": "left",
                "rangeType": null,
                "resetData": null,
                "type": "list",
                "value": ""
            });
            var flxCloseCal1 = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "centerY": "50%",
                "clipBounds": true,
                "height": "10px",
                "id": "flxCloseCal1",
                "isVisible": false,
                "layoutType": kony.flex.FREE_FORM,
                "isModalContainer": false,
                "right": "10px",
                "skin": "slFbox",
                "width": "12px",
                "zIndex": 2
            }, {}, {
                "hoverSkin": "sknCursor"
            });
            flxCloseCal1.setDefaultUnit(kony.flex.DP);
            var lblCloseCal1 = new kony.ui.Label({
                "centerY": "50%",
                "id": "lblCloseCal1",
                "isVisible": true,
                "right": "0dp",
                "skin": "sknfontIconDescDownArrow12px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmAlertsManagement.lblAlertPreviewClose\")",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxCloseCal1.add(lblCloseCal1);
            flxDropDown01.add(datePickerTransaction, flxCloseCal1);
            var lblCol1 = new kony.ui.Label({
                "id": "lblCol1",
                "isVisible": true,
                "left": "0dp",
                "skin": "sknlblLatoRegular484b5213px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmLogs.lblCol1\")",
                "top": "0px",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxFilterCriteria2.add(flxDropDown01, lblCol1);
            var flxFilterCriteria3 = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": false,
                "height": "68dp",
                "id": "flxFilterCriteria3",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "15px",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0px",
                "width": "200px",
                "zIndex": 1
            }, {}, {});
            flxFilterCriteria3.setDefaultUnit(kony.flex.DP);
            var flxAmountDropDown = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": false,
                "height": "360px",
                "id": "flxAmountDropDown",
                "isVisible": false,
                "layoutType": kony.flex.FREE_FORM,
                "left": "40dp",
                "isModalContainer": false,
                "skin": "skntxtbxNormald7d9e0",
                "top": "64px",
                "width": "160px",
                "zIndex": 210
            }, {}, {
                "hoverSkin": "skntxtbxhover11abeb"
            });
            flxAmountDropDown.setDefaultUnit(kony.flex.DP);
            var selectAmount = new com.adminConsole.logs.selectAmount({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "height": "99%",
                "id": "selectAmount",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "masterType": constants.MASTER_TYPE_DEFAULT,
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0dp",
                "width": "100%",
                "overrides": {
                    "flxAmount": {
                        "width": kony.flex.USE_PREFFERED_SIZE
                    },
                    "flxAmountpicker": {
                        "height": "210px"
                    },
                    "selectAmount": {
                        "height": "99%",
                        "width": "100%"
                    }
                }
            }, {
                "overrides": {}
            }, {
                "overrides": {}
            });
            flxAmountDropDown.add(selectAmount);
            var blSearchParam3 = new kony.ui.Label({
                "id": "blSearchParam3",
                "isVisible": true,
                "left": "0px",
                "skin": "sknlblLatoRegular484b5213px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmLogs.blSearchParam3\")",
                "top": "0px",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var flxAmountDropDown1 = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": false,
                "height": "41px",
                "id": "flxAmountDropDown1",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "sknflxffffffope1e5edcsr",
                "top": "25px",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {}, {
                "hoverSkin": "sknCursor"
            });
            flxAmountDropDown1.setDefaultUnit(kony.flex.DP);
            var flxAmountImage = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "100%",
                "id": "flxAmountImage",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0dp",
                "width": "40px",
                "zIndex": 1
            }, {}, {});
            flxAmountImage.setDefaultUnit(kony.flex.DP);
            var lblAmountCurrencySymbol = new kony.ui.Label({
                "centerX": "50%",
                "centerY": "50%",
                "id": "lblAmountCurrencySymbol",
                "isVisible": true,
                "skin": "sknFontIconCurrency",
                "text": "",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var lblSeparator = new kony.ui.Label({
                "height": "100%",
                "id": "lblSeparator",
                "isVisible": true,
                "left": "39dp",
                "skin": "sknlblSeperator",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmGroups.lblDollar\")",
                "top": "0dp",
                "width": "1dp",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxAmountImage.add(lblAmountCurrencySymbol, lblSeparator);
            var lblSelectedAmount = new kony.ui.Label({
                "centerY": "50%",
                "id": "lblSelectedAmount",
                "isVisible": true,
                "left": "40px",
                "skin": "sknlbl0h2ff0d6b13f947AD",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmLogsController.Select_amount_range\")",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [3, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var flxAmountClose = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "centerY": "50%",
                "clipBounds": true,
                "height": "12px",
                "id": "flxAmountClose",
                "isVisible": false,
                "layoutType": kony.flex.FREE_FORM,
                "isModalContainer": false,
                "right": "10px",
                "skin": "slFbox",
                "width": "12px",
                "zIndex": 2
            }, {}, {
                "hoverSkin": "sknCursor"
            });
            flxAmountClose.setDefaultUnit(kony.flex.DP);
            var lblAmountClose = new kony.ui.Label({
                "centerY": "50%",
                "id": "lblAmountClose",
                "isVisible": true,
                "right": "0dp",
                "skin": "sknfontIconDescDownArrow12px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmAlertsManagement.lblAlertPreviewClose\")",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxAmountClose.add(lblAmountClose);
            flxAmountDropDown1.add(flxAmountImage, lblSelectedAmount, flxAmountClose);
            flxFilterCriteria3.add(flxAmountDropDown, blSearchParam3, flxAmountDropDown1);
            var flxFilterCriteria4 = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": false,
                "height": "68px",
                "id": "flxFilterCriteria4",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "15dp",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0dp",
                "width": "200px",
                "zIndex": 70
            }, {}, {});
            flxFilterCriteria4.setDefaultUnit(kony.flex.DP);
            var lblViewColumnItems = new kony.ui.Label({
                "id": "lblViewColumnItems",
                "isVisible": true,
                "left": "0dp",
                "skin": "sknlblLatoRegular484b5213px",
                "text": "View Column Items",
                "top": "0dp",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var customListBoxTransactional = new com.adminConsole.alerts.customListbox({
                "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                "id": "customListBoxTransactional",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "masterType": constants.MASTER_TYPE_DEFAULT,
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "25dp",
                "width": "100%",
                "zIndex": 80,
                "overrides": {
                    "customListbox": {
                        "top": "25dp",
                        "zIndex": 80
                    },
                    "flxSegmentList": {
                        "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                        "zIndex": 150
                    },
                    "flxSelectAll": {
                        "isVisible": true
                    },
                    "imgCheckBox": {
                        "src": "checkboxnormal.png"
                    },
                    "segList": {
                        "height": "160dp",
                        "zIndex": 200
                    }
                }
            }, {
                "overrides": {}
            }, {
                "overrides": {}
            });
            flxFilterCriteria4.add(lblViewColumnItems, customListBoxTransactional);
            flxFiltersList.add(flxFilterCriteria1, flxFilterCriteria2, flxFilterCriteria3, flxFilterCriteria4);
            var flxTlButtons = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "22dp",
                "id": "flxTlButtons",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "isModalContainer": false,
                "right": "0dp",
                "skin": "slFbox",
                "top": "35dp",
                "width": "100%"
            }, {}, {});
            flxTlButtons.setDefaultUnit(kony.flex.DP);
            var btnTransactionLogsSaveFilter = new kony.ui.Button({
                "bottom": 0,
                "id": "btnTransactionLogsSaveFilter",
                "isVisible": true,
                "right": "85px",
                "skin": "sknBtnLatoRegular11abeb14px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmLogs.btnAdvSearch\")",
                "top": "0px",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_CENTER,
                "displayText": true,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var btnAdd = new kony.ui.Button({
                "focusSkin": "sknbtnf7f7faLatoRegular12Px485c75Border1px485C75Radius20px",
                "height": "22px",
                "id": "btnAdd",
                "isVisible": true,
                "right": "15px",
                "skin": "sknbtnf7f7faLatoRegular12Px485c75Border1px485C75Radius20px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmCustomerManagement.btnApply\")",
                "top": "0px",
                "width": "60px",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_CENTER,
                "displayText": true,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {
                "hoverSkin": "sknbtnffffffLatoRegular12Px485c75Border1px485C75Radius20px"
            });
            flxTlButtons.add(btnTransactionLogsSaveFilter, btnAdd);
            flxFilters.add(flxFiltersList, flxTlButtons);
            flxTransactionLogsFilter.add(flxHeader1, flxFilters);
            flxTrasactionalFilter.add(flxArrowImage, flxTransactionLogsFilter);
            var flxAminConsoleFilterContainer = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                "bottom": "0dp",
                "clipBounds": true,
                "id": "flxAminConsoleFilterContainer",
                "isVisible": false,
                "layoutType": kony.flex.FLOW_VERTICAL,
                "isModalContainer": false,
                "right": "54dp",
                "skin": "slFbox",
                "top": "40px",
                "width": "435dp",
                "zIndex": 50
            }, {}, {});
            flxAminConsoleFilterContainer.setDefaultUnit(kony.flex.DP);
            var flxArraowImageAdminConsole = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "bottom": "-2dp",
                "clipBounds": true,
                "height": "12dp",
                "id": "flxArraowImageAdminConsole",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "slFbox",
                "width": "100%",
                "zIndex": 3
            }, {}, {});
            flxArraowImageAdminConsole.setDefaultUnit(kony.flex.DP);
            var imgUpArrowAdminConsole = new kony.ui.Image2({
                "height": "12dp",
                "id": "imgUpArrowAdminConsole",
                "isVisible": true,
                "right": "15dp",
                "skin": "slImage",
                "src": "uparrow_dropdown.png",
                "top": "0dp",
                "width": "15dp",
                "zIndex": 1
            }, {
                "imageScaleMode": constants.IMAGE_SCALE_MODE_FIT_TO_DIMENSIONS,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxArraowImageAdminConsole.add(imgUpArrowAdminConsole);
            var flxAdminConsoleFilter = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "179dp",
                "id": "flxAdminConsoleFilter",
                "isVisible": true,
                "layoutType": kony.flex.FLOW_VERTICAL,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "sknheaderLogs",
                "top": "0dp",
                "width": "100%",
                "zIndex": 2
            }, {}, {});
            flxAdminConsoleFilter.setDefaultUnit(kony.flex.DP);
            var flxHeader2 = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                "clipBounds": true,
                "id": "flxHeader2",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "10dp",
                "width": "100%"
            }, {}, {});
            flxHeader2.setDefaultUnit(kony.flex.DP);
            var lblCustomerName2 = new kony.ui.Label({
                "id": "lblCustomerName2",
                "isVisible": true,
                "left": "10px",
                "skin": "sknlblLatoRegular484b5213px",
                "text": "Filter Data By",
                "top": "0px",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var flxImage2 = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "15px",
                "id": "flxImage2",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "isModalContainer": false,
                "right": "10dp",
                "skin": "CopyslFbox0efc73ba91cad4b",
                "top": "2px",
                "width": "15px"
            }, {}, {});
            flxImage2.setDefaultUnit(kony.flex.DP);
            var imgStatus2 = new kony.ui.Image2({
                "height": "100%",
                "id": "imgStatus2",
                "isVisible": false,
                "left": "0px",
                "skin": "slImage",
                "src": "img_down_arrow.png",
                "top": "0px",
                "width": "100%",
                "zIndex": 1
            }, {
                "imageScaleMode": constants.IMAGE_SCALE_MODE_MAINTAIN_ASPECT_RATIO,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var lblIconStatus2 = new kony.ui.Label({
                "centerY": "50%",
                "height": "15dp",
                "id": "lblIconStatus2",
                "isVisible": true,
                "left": "0dp",
                "skin": "sknIcon485C7513px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.userwidgetmodel.lblClearSearch\")",
                "width": "15dp",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxImage2.add(imgStatus2, lblIconStatus2);
            flxHeader2.add(lblCustomerName2, flxImage2);
            var flxFilters2 = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                "clipBounds": true,
                "id": "flxFilters2",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "10px",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {}, {});
            flxFilters2.setDefaultUnit(kony.flex.DP);
            var flxFiltersList2 = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                "clipBounds": true,
                "id": "flxFiltersList2",
                "isVisible": true,
                "layoutType": kony.flex.FLOW_HORIZONTAL,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0dp",
                "width": "100%"
            }, {}, {});
            flxFiltersList2.setDefaultUnit(kony.flex.DP);
            var flxFilterCriteriaAdmin1 = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "68dp",
                "id": "flxFilterCriteriaAdmin1",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "10px",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0px",
                "width": "200px",
                "zIndex": 1
            }, {}, {});
            flxFilterCriteriaAdmin1.setDefaultUnit(kony.flex.DP);
            var lblSearchParamAdmin1 = new kony.ui.Label({
                "id": "lblSearchParamAdmin1",
                "isVisible": true,
                "left": "0px",
                "skin": "slLabel0d20174dce8ea42",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmLogs.lblSearchParamAdmin1\")",
                "top": "0px",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var listBoxSearchParamAdmin1 = new kony.ui.ListBox({
                "focusSkin": "sknlbxBgffffffBorder11abebRadius3Px14px",
                "height": "40dp",
                "id": "listBoxSearchParamAdmin1",
                "isVisible": true,
                "left": "0dp",
                "masterData": [
                    ["op1", "All"],
                    ["op2", "Internal User Management"],
                    ["op3", "Security Images"],
                    ["op4", "Transaction Limits"],
                    ["op5", "CSR Portal"],
                    ["op6", "Permissions"],
                    ["op7", "Access Customer"]
                ],
                "skin": "sknlbxBgffffffBorderc1c9ceRadius3Px",
                "top": "25px",
                "width": "100%",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_CENTER,
                "padding": [3, 0, 0, 0],
                "paddingInPixel": false
            }, {
                "hoverSkin": "lstboxcursor",
                "multiSelect": false
            });
            flxFilterCriteriaAdmin1.add(lblSearchParamAdmin1, listBoxSearchParamAdmin1);
            var flxFilterCriteriaAdmin2 = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": false,
                "height": "68px",
                "id": "flxFilterCriteriaAdmin2",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "15dp",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0dp",
                "width": "200px",
                "zIndex": 1
            }, {}, {});
            flxFilterCriteriaAdmin2.setDefaultUnit(kony.flex.DP);
            var lblSearchParamAdmin2 = new kony.ui.Label({
                "id": "lblSearchParamAdmin2",
                "isVisible": true,
                "left": "0dp",
                "skin": "sknlblLatoRegular485c75Font12px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmLogs.lblCol1\")",
                "top": "0px",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var flxDropDownAdmin2 = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": false,
                "height": "41px",
                "id": "flxDropDownAdmin2",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "sknflxffffffope1e5edcsr",
                "top": "25px",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {}, {
                "hoverSkin": "sknFlxSegRowHover11abeb"
            });
            flxDropDownAdmin2.setDefaultUnit(kony.flex.DP);
            var datePickerAdminConsole = new kony.ui.CustomWidget({
                "id": "datePickerAdminConsole",
                "isVisible": true,
                "left": "1dp",
                "right": "3px",
                "top": "7dp",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "height": "100%",
                "zIndex": 10,
                "clipBounds": true
            }, {
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {
                "widgetName": "dateRangePicker",
                "drops": "left",
                "event": null,
                "maxDate": "true",
                "opens": "left",
                "rangeType": "",
                "resetData": null,
                "type": "list",
                "value": ""
            });
            var flxCloseCal2 = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "centerY": "50%",
                "clipBounds": true,
                "height": "10px",
                "id": "flxCloseCal2",
                "isVisible": false,
                "layoutType": kony.flex.FREE_FORM,
                "isModalContainer": false,
                "right": "10px",
                "skin": "slFbox",
                "width": "12px",
                "zIndex": 2
            }, {}, {
                "hoverSkin": "sknCursor"
            });
            flxCloseCal2.setDefaultUnit(kony.flex.DP);
            var lblCloseCal2 = new kony.ui.Label({
                "centerY": "50%",
                "id": "lblCloseCal2",
                "isVisible": true,
                "right": "0dp",
                "skin": "sknfontIconDescDownArrow12px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmAlertsManagement.lblAlertPreviewClose\")",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxCloseCal2.add(lblCloseCal2);
            flxDropDownAdmin2.add(datePickerAdminConsole, flxCloseCal2);
            flxFilterCriteriaAdmin2.add(lblSearchParamAdmin2, flxDropDownAdmin2);
            flxFiltersList2.add(flxFilterCriteriaAdmin1, flxFilterCriteriaAdmin2);
            var btnAdminConsoleLogsSaveFilter = new kony.ui.Button({
                "bottom": "0px",
                "id": "btnAdminConsoleLogsSaveFilter",
                "isVisible": true,
                "right": "85dp",
                "skin": "sknBtnLatoRegular11abeb14px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmLogs.btnAdvSearch\")",
                "top": "103px",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_CENTER,
                "displayText": true,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var btnAdd2 = new kony.ui.Button({
                "focusSkin": "sknbtnf7f7faLatoRegular12Px485c75Border1px485C75Radius20px",
                "height": "22px",
                "id": "btnAdd2",
                "isVisible": true,
                "right": "10px",
                "skin": "sknbtnf7f7faLatoRegular12Px485c75Border1px485C75Radius20px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmCustomerManagement.btnApply\")",
                "top": "103px",
                "width": "60px",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_CENTER,
                "displayText": true,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {
                "hoverSkin": "sknbtnf7f7faLatoBolda5abc414Px485c751pxBorderRadius20px"
            });
            flxFilters2.add(flxFiltersList2, btnAdminConsoleLogsSaveFilter, btnAdd2);
            flxAdminConsoleFilter.add(flxHeader2, flxFilters2);
            flxAminConsoleFilterContainer.add(flxArraowImageAdminConsole, flxAdminConsoleFilter);
            var flxStatusFilterAdmin = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                "clipBounds": false,
                "id": "flxStatusFilterAdmin",
                "isVisible": false,
                "layoutType": kony.flex.FREE_FORM,
                "isModalContainer": false,
                "right": "760px",
                "skin": "slFbox",
                "top": "10px",
                "width": "14.50%",
                "zIndex": 500
            }, {}, {});
            flxStatusFilterAdmin.setDefaultUnit(kony.flex.DP);
            var statusFilterMenuAdmin = new com.adminConsole.common.statusFilterMenu({
                "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                "id": "statusFilterMenuAdmin",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "masterType": constants.MASTER_TYPE_DEFAULT,
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "5dp",
                "width": "100%",
                "zIndex": 5,
                "overrides": {
                    "flxChechboxOuter": {
                        "maxHeight": "170px",
                        "maxWidth": "viz.val_cleared"
                    },
                    "imgUpArrow": {
                        "left": "viz.val_cleared",
                        "src": "uparrow_2x.png"
                    },
                    "segStatusFilterDropdown": {
                        "data": [{
                            "imgCheckBox": "checkbox.png",
                            "lblDescription": "Posted"
                        }, {
                            "imgCheckBox": "checkbox.png",
                            "lblDescription": "Pending"
                        }, {
                            "imgCheckBox": "checkbox.png",
                            "lblDescription": "Failed"
                        }],
                        "right": "0px"
                    }
                }
            }, {
                "overrides": {}
            }, {
                "overrides": {}
            });
            flxStatusFilterAdmin.add(statusFilterMenuAdmin);
            flxMainContent.add(flxCustomerSearchResults, flxCustomerDetailedResults, flxTransactionLog, flxAdminConsoleLog, flxDropDownDetail1, flxNoResultsFound, flxFiltersAndHeaders, flxTrasactionalFilter, flxAminConsoleFilterContainer, flxStatusFilterAdmin);
            var flxbreadcrumbList = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                "clipBounds": false,
                "id": "flxbreadcrumbList",
                "isVisible": false,
                "layoutType": kony.flex.FREE_FORM,
                "left": "125dp",
                "isModalContainer": false,
                "skin": "sknflxffffffop100dbdbe6Radius3px",
                "top": "125px",
                "width": "125px",
                "zIndex": 300
            }, {}, {});
            flxbreadcrumbList.setDefaultUnit(kony.flex.DP);
            var flxList = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                "clipBounds": true,
                "id": "flxList",
                "isVisible": true,
                "layoutType": kony.flex.FLOW_VERTICAL,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "sknflxffffffop0a",
                "top": "0dp",
                "width": "100%",
                "zIndex": 1
            }, {}, {});
            flxList.setDefaultUnit(kony.flex.DP);
            var flxNo1 = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                "clipBounds": true,
                "id": "flxNo1",
                "isVisible": true,
                "layoutType": kony.flex.FLOW_VERTICAL,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "CopyslFbox0i3b050eeb8134e",
                "top": "0dp",
                "width": "100%"
            }, {}, {});
            flxNo1.setDefaultUnit(kony.flex.DP);
            var lblNo1 = new kony.ui.Label({
                "bottom": "10dp",
                "id": "lblNo1",
                "isVisible": true,
                "left": "10dp",
                "skin": "sknlblLatoBold35475f14px",
                "text": "Transactional",
                "top": "10dp",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxNo1.add(lblNo1);
            var flxNo2 = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                "clipBounds": true,
                "id": "flxNo2",
                "isVisible": true,
                "layoutType": kony.flex.FLOW_VERTICAL,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "CopyslFbox0i3b050eeb8134e",
                "top": "0dp",
                "width": "100%"
            }, {}, {});
            flxNo2.setDefaultUnit(kony.flex.DP);
            var lblNo2 = new kony.ui.Label({
                "bottom": "10dp",
                "id": "lblNo2",
                "isVisible": true,
                "left": "10dp",
                "skin": "sknlblLatoBold35475f14px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmLogs.lblNo2\")",
                "top": "10dp",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxNo2.add(lblNo2);
            var flxNo3 = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                "clipBounds": true,
                "id": "flxNo3",
                "isVisible": true,
                "layoutType": kony.flex.FLOW_VERTICAL,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "CopyslFbox0i3b050eeb8134e",
                "top": "0dp",
                "width": "100%"
            }, {}, {});
            flxNo3.setDefaultUnit(kony.flex.DP);
            var lblNo3 = new kony.ui.Label({
                "bottom": "10dp",
                "id": "lblNo3",
                "isVisible": true,
                "left": "10dp",
                "skin": "sknlblLatoBold35475f14px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmLogs.lblCustomerSpecific\")",
                "top": "10dp",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxNo3.add(lblNo3);
            flxList.add(flxNo1, flxNo2, flxNo3);
            flxbreadcrumbList.add(flxList);
            var flxLoading = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "100%",
                "id": "flxLoading",
                "isVisible": false,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0px",
                "isModalContainer": false,
                "skin": "sknLoadingBlur",
                "top": "0dp",
                "width": "100%",
                "zIndex": 11
            }, {}, {});
            flxLoading.setDefaultUnit(kony.flex.DP);
            var flxImageContainer = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "centerX": "50%",
                "centerY": "50%",
                "clipBounds": true,
                "height": "75px",
                "id": "flxImageContainer",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "isModalContainer": false,
                "skin": "slFbox",
                "top": 250,
                "width": "75px",
                "zIndex": 1
            }, {}, {});
            flxImageContainer.setDefaultUnit(kony.flex.DP);
            var imgLoading = new kony.ui.Image2({
                "centerX": "50%",
                "centerY": "50%",
                "height": "50px",
                "id": "imgLoading",
                "isVisible": true,
                "skin": "slImage",
                "src": "loadingscreenimage.gif",
                "width": "50px",
                "zIndex": 1
            }, {
                "imageScaleMode": constants.IMAGE_SCALE_MODE_MAINTAIN_ASPECT_RATIO,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxImageContainer.add(imgLoading);
            flxLoading.add(flxImageContainer);
            flxRightPannel.add(flxMainHeader, flxBreadCrumbs, flxLogsList, flxCustomerActivityLog, flxModifySearch, flxHeaderDropdown, flxMainContent, flxbreadcrumbList, flxLoading);
            var flxPopUpSaveFilter = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "100%",
                "id": "flxPopUpSaveFilter",
                "isVisible": false,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0dp",
                "width": "100%",
                "zIndex": 10
            }, {}, {});
            flxPopUpSaveFilter.setDefaultUnit(kony.flex.DP);
            var popUpSaveFilter = new com.adminConsole.logs.popUpSaveFilter({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "height": "100%",
                "id": "popUpSaveFilter",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "masterType": constants.MASTER_TYPE_DEFAULT,
                "isModalContainer": false,
                "skin": "sknflxBg000000Op50",
                "top": "0dp",
                "width": "100%",
                "overrides": {
                    "flxPopUp": {
                        "centerX": "50%",
                        "isVisible": true,
                        "top": "250px"
                    }
                }
            }, {
                "overrides": {}
            }, {
                "overrides": {}
            });
            flxPopUpSaveFilter.add(popUpSaveFilter);
            var flxToastMessage = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "bottom": "35dp",
                "clipBounds": true,
                "height": "8%",
                "id": "flxToastMessage",
                "isVisible": false,
                "layoutType": kony.flex.FREE_FORM,
                "left": "305px",
                "isModalContainer": false,
                "skin": "slFbox",
                "width": "78%",
                "zIndex": 20
            }, {}, {});
            flxToastMessage.setDefaultUnit(kony.flex.DP);
            var toastMessage = new com.adminConsole.common.toastMessage({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "height": "70px",
                "id": "toastMessage",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "masterType": constants.MASTER_TYPE_DEFAULT,
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0dp",
                "width": "100%",
                "overrides": {}
            }, {
                "overrides": {}
            }, {
                "overrides": {}
            });
            flxToastMessage.add(toastMessage);
            var flxPopUp = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "100%",
                "id": "flxPopUp",
                "isVisible": false,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0dp",
                "width": "100%",
                "zIndex": 10
            }, {}, {});
            flxPopUp.setDefaultUnit(kony.flex.DP);
            var popUp = new com.adminConsole.common.popUp({
                "height": "100%",
                "id": "popUp",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "masterType": constants.MASTER_TYPE_DEFAULT,
                "isModalContainer": false,
                "skin": "sknflxBg000000Op50",
                "top": "0dp",
                "width": "100%",
                "zIndex": 1,
                "overrides": {
                    "btnPopUpCancel": {
                        "i18n_text": "kony.i18n.getLocalizedString(\"i18n.PopUp.NoLeaveAsIS\")",
                        "right": "20px"
                    },
                    "btnPopUpDelete": {
                        "i18n_text": "kony.i18n.getLocalizedString(\"i18n.PopUp.YesDeactivate\")"
                    },
                    "flxPopUpButtons": {
                        "isVisible": true
                    },
                    "lblPopUpMainMessage": {
                        "text": "Delete Saved log"
                    },
                    "rtxPopUpDisclaimer": {
                        "text": "Are you sure you want to delete this filtered log?"
                    }
                }
            }, {
                "overrides": {}
            }, {
                "overrides": {}
            });
            flxPopUp.add(popUp);
            flxMain.add(flxLeftPannel, flxRightPannel, flxPopUpSaveFilter, flxToastMessage, flxPopUp);
            var flxChequeImageDsplay = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "100%",
                "id": "flxChequeImageDsplay",
                "isVisible": false,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "sknLoadingBlur",
                "top": "0dp",
                "width": "100%",
                "zIndex": 20
            }, {}, {});
            flxChequeImageDsplay.setDefaultUnit(kony.flex.DP);
            var flxImageChqContainer = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "centerX": "50%",
                "centerY": "50%",
                "clipBounds": true,
                "height": "400px",
                "id": "flxImageChqContainer",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "isModalContainer": false,
                "skin": "slFbox",
                "width": "700px",
                "zIndex": 1
            }, {}, {});
            flxImageChqContainer.setDefaultUnit(kony.flex.DP);
            var CopyimgChq = new kony.ui.Image2({
                "centerX": "50%",
                "centerY": "50%",
                "height": "100%",
                "id": "CopyimgChq",
                "isVisible": true,
                "skin": "slImage",
                "src": "right_arrow2x.png",
                "width": "100%",
                "zIndex": 1
            }, {
                "imageScaleMode": constants.IMAGE_SCALE_MODE_MAINTAIN_ASPECT_RATIO,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxImageChqContainer.add(CopyimgChq);
            var flxImageClose = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "centerX": "74%",
                "centerY": "25.56%",
                "clipBounds": true,
                "height": "40px",
                "id": "flxImageClose",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "isModalContainer": false,
                "skin": "slFbox",
                "width": "40px",
                "zIndex": 1
            }, {}, {});
            flxImageClose.setDefaultUnit(kony.flex.DP);
            var imgChqClose = new kony.ui.Image2({
                "centerX": "50%",
                "centerY": "50%",
                "height": "100%",
                "id": "imgChqClose",
                "isVisible": true,
                "skin": "slImage",
                "src": "close_blue.png",
                "width": "100%",
                "zIndex": 1
            }, {
                "imageScaleMode": constants.IMAGE_SCALE_MODE_MAINTAIN_ASPECT_RATIO,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxImageClose.add(imgChqClose);
            flxChequeImageDsplay.add(flxImageChqContainer, flxImageClose);
            var flxEditCancelConfirmation = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "100%",
                "id": "flxEditCancelConfirmation",
                "isVisible": false,
                "layoutType": kony.flex.FREE_FORM,
                "left": "10dp",
                "masterType": constants.MASTER_TYPE_USERWIDGET,
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "10dp",
                "width": "100%",
                "zIndex": 5
            }, {}, {});
            flxEditCancelConfirmation.setDefaultUnit(kony.flex.DP);
            var popUpCancelEdits = new com.adminConsole.common.popUpCancelEdits({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "height": "100%",
                "id": "popUpCancelEdits",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "masterType": constants.MASTER_TYPE_DEFAULT,
                "isModalContainer": false,
                "skin": "sknflxBg000000Op50",
                "top": "0dp",
                "width": "100%",
                "zIndex": 1,
                "overrides": {
                    "btnPopUpCancel": {
                        "i18n_text": "kony.i18n.getLocalizedString(\"i8n.navigation.cancelButton\")"
                    },
                    "btnPopUpDelete": {
                        "i18n_text": "kony.i18n.getLocalizedString(\"i8n.navigation.okButton\")"
                    },
                    "lblPopUpMainMessage": {
                        "i18n_text": "kony.i18n.getLocalizedString(\"i8n.navigation.cancelChanges\")"
                    },
                    "popUpCancelEdits": {
                        "bottom": "viz.val_cleared",
                        "centerX": "viz.val_cleared",
                        "centerY": "viz.val_cleared",
                        "height": "100%",
                        "left": "0dp",
                        "maxHeight": "viz.val_cleared",
                        "maxWidth": "viz.val_cleared",
                        "minHeight": "viz.val_cleared",
                        "minWidth": "viz.val_cleared",
                        "right": "viz.val_cleared",
                        "top": "0dp",
                        "width": "100%"
                    },
                    "rtxPopUpDisclaimer": {
                        "i18n_text": "kony.i18n.getLocalizedString(\"i8n.navigation.cancelChangesBody\")"
                    }
                }
            }, {
                "overrides": {}
            }, {
                "overrides": {}
            });
            flxEditCancelConfirmation.add(popUpCancelEdits);
            var flxOtherInfoPopup = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "bottom": "0dp",
                "centerX": "50.00%",
                "clipBounds": false,
                "height": "100%",
                "id": "flxOtherInfoPopup",
                "isVisible": false,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "isModalContainer": false,
                "right": "0dp",
                "skin": "flxPopupTrans0db4ac791cac04d",
                "top": "0dp",
                "width": "100%",
                "zIndex": 10
            }, {}, {});
            flxOtherInfoPopup.setDefaultUnit(kony.flex.DP);
            var flxOtherInfoContainer = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "centerX": "50%",
                "clipBounds": true,
                "height": "508dp",
                "id": "flxOtherInfoContainer",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": 0,
                "isModalContainer": false,
                "skin": "slFbox0e35d4d07b2054b",
                "top": "60dp",
                "width": "986dp"
            }, {}, {});
            flxOtherInfoContainer.setDefaultUnit(kony.flex.DP);
            var flxPopUpTopColor = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "10px",
                "id": "flxPopUpTopColor",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "sknflx11abeb",
                "top": "0dp",
                "width": "100%",
                "zIndex": 1
            }, {}, {});
            flxPopUpTopColor.setDefaultUnit(kony.flex.DP);
            flxPopUpTopColor.add();
            var flxPopUpClose = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "25px",
                "id": "flxPopUpClose",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "isModalContainer": false,
                "right": 15,
                "skin": "sknFlxPointer",
                "top": "15dp",
                "width": "25px",
                "zIndex": 20
            }, {}, {
                "hoverSkin": "sknFlxPointer"
            });
            flxPopUpClose.setDefaultUnit(kony.flex.DP);
            var lblPopUpClose = new kony.ui.Label({
                "centerY": "50%",
                "id": "lblPopUpClose",
                "isVisible": true,
                "left": "5dp",
                "skin": "sknIcon18pxGray",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.userwidgetmodel.lblClearSearch\")",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxPopUpClose.add(lblPopUpClose);
            var flxPopupHeader = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                "clipBounds": true,
                "id": "flxPopupHeader",
                "isVisible": false,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "slFbox0d9c3974835234d",
                "top": "40dp",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {}, {});
            flxPopupHeader.setDefaultUnit(kony.flex.DP);
            var lblPopUpMainMessage = new kony.ui.Label({
                "id": "lblPopUpMainMessage",
                "isVisible": true,
                "left": "20px",
                "skin": "sknlblLatoBold35475f23px",
                "text": "Other Information",
                "top": "0px",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxPopupHeader.add(lblPopUpMainMessage);
            var flxViewInfoContainer = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "100%",
                "id": "flxViewInfoContainer",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "10dp",
                "width": "100%"
            }, {}, {});
            flxViewInfoContainer.setDefaultUnit(kony.flex.DP);
            var tabs = new com.adminConsole.common.tabs({
                "height": "100%",
                "id": "tabs",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "masterType": constants.MASTER_TYPE_DEFAULT,
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0dp",
                "width": "100%",
                "overrides": {
                    "btnTab1": {
                        "left": "20dp",
                        "text": "OTHER INFORMATION"
                    },
                    "btnTab2": {
                        "text": "JSON DATA"
                    },
                    "flxTabsContainer": {
                        "left": "0dp",
                        "right": "viz.val_cleared",
                        "width": "100%",
                        "zIndex": 2
                    },
                    "lblSeperator": {
                        "height": "1px",
                        "isVisible": true,
                        "top": "59dp"
                    },
                    "tabs": {
                        "left": "0dp",
                        "top": "0dp"
                    }
                }
            }, {
                "overrides": {}
            }, {
                "overrides": {}
            });
            var flxScrollOtherInfo = new kony.ui.FlexScrollContainer({
                "allowHorizontalBounce": false,
                "allowVerticalBounce": true,
                "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                "bottom": "35dp",
                "bounces": true,
                "clipBounds": true,
                "enableScrolling": true,
                "horizontalScrollIndicator": true,
                "id": "flxScrollOtherInfo",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "pagingEnabled": false,
                "scrollDirection": kony.flex.SCROLL_VERTICAL,
                "skin": "slFSbox",
                "top": "89dp",
                "verticalScrollIndicator": true,
                "width": "100%"
            }, {}, {});
            flxScrollOtherInfo.setDefaultUnit(kony.flex.DP);
            flxScrollOtherInfo.add();
            var flxScrollViewJson = new kony.ui.FlexScrollContainer({
                "allowHorizontalBounce": false,
                "allowVerticalBounce": true,
                "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                "bottom": "35dp",
                "bounces": true,
                "clipBounds": true,
                "enableScrolling": true,
                "horizontalScrollIndicator": true,
                "id": "flxScrollViewJson",
                "isVisible": false,
                "layoutType": kony.flex.FREE_FORM,
                "left": 0,
                "pagingEnabled": false,
                "scrollDirection": kony.flex.SCROLL_VERTICAL,
                "skin": "slFSbox",
                "top": "89dp",
                "verticalScrollIndicator": true,
                "width": "100%"
            }, {}, {});
            flxScrollViewJson.setDefaultUnit(kony.flex.DP);
            var rtxViewJson = new kony.ui.RichText({
                "id": "rtxViewJson",
                "isVisible": true,
                "left": "20dp",
                "linkSkin": "defRichTextLink",
                "right": "20dp",
                "skin": "sknrtxLato35475f14px",
                "text": "\n",
                "top": "0dp",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxScrollViewJson.add(rtxViewJson);
            flxViewInfoContainer.add(tabs, flxScrollOtherInfo, flxScrollViewJson);
            flxOtherInfoContainer.add(flxPopUpTopColor, flxPopUpClose, flxPopupHeader, flxViewInfoContainer);
            flxOtherInfoPopup.add(flxOtherInfoContainer);
            var flxJsonDataPopup = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "100%",
                "id": "flxJsonDataPopup",
                "isVisible": false,
                "layoutType": kony.flex.FREE_FORM,
                "left": 0,
                "isModalContainer": false,
                "skin": "flxPopupTrans0db4ac791cac04d",
                "top": 0,
                "width": "100%"
            }, {}, {});
            flxJsonDataPopup.setDefaultUnit(kony.flex.DP);
            var flxJsonDataPopupContainer = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "bottom": "50px",
                "clipBounds": true,
                "id": "flxJsonDataPopupContainer",
                "isVisible": true,
                "layoutType": kony.flex.FLOW_VERTICAL,
                "reverseLayoutDirection": false,
                "left": "355px",
                "isModalContainer": false,
                "right": "355px",
                "skin": "slFbox0e35d4d07b2054b",
                "top": "140px",
                "zIndex": 1
            }, {}, {});
            flxJsonDataPopupContainer.setDefaultUnit(kony.flex.DP);
            var flxJsonDataPopUpTopColor = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "10px",
                "id": "flxJsonDataPopUpTopColor",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "reverseLayoutDirection": false,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "sknflx11abeb",
                "top": "0dp",
                "width": "100%",
                "zIndex": 1
            }, {}, {});
            flxJsonDataPopUpTopColor.setDefaultUnit(kony.flex.DP);
            flxJsonDataPopUpTopColor.add();
            var flxJsonDataViewInfoContainer = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "98%",
                "id": "flxJsonDataViewInfoContainer",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "reverseLayoutDirection": false,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0dp",
                "width": "100%"
            }, {}, {});
            flxJsonDataViewInfoContainer.setDefaultUnit(kony.flex.DP);
            var lblJsonData = new kony.ui.Label({
                "height": "19px",
                "id": "lblJsonData",
                "isVisible": true,
                "left": "20px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.View.JsonDataPopup.JsonData\")",
                "top": "30px",
                "width": kony.flex.USE_PREFFERED_SIZE
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var lblJsonDataSeperator = new kony.ui.Label({
                "height": "1dp",
                "id": "lblJsonDataSeperator",
                "isVisible": true,
                "left": 20,
                "right": 20,
                "skin": "sknlblSeperator",
                "text": ".",
                "top": 70,
                "width": kony.flex.USE_PREFFERED_SIZE
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var flxScrollJsonData = new kony.ui.FlexScrollContainer({
                "allowHorizontalBounce": false,
                "allowVerticalBounce": true,
                "bottom": "60dp",
                "bounces": true,
                "clipBounds": true,
                "enableScrolling": true,
                "horizontalScrollIndicator": true,
                "id": "flxScrollJsonData",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "20dp",
                "pagingEnabled": false,
                "right": "20dp",
                "scrollDirection": kony.flex.SCROLL_VERTICAL,
                "skin": "slFSbox",
                "top": "95px",
                "verticalScrollIndicator": true
            }, {}, {});
            flxScrollJsonData.setDefaultUnit(kony.flex.DP);
            var rtxViewJsonData = new kony.ui.RichText({
                "id": "rtxViewJsonData",
                "isVisible": true,
                "left": "20dp",
                "right": "20dp",
                "text": "\n",
                "top": "15dp",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxScrollJsonData.add(rtxViewJsonData);
            var flxJsonDataPopUpClose = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "bottom": "489px",
                "clipBounds": true,
                "height": "15px",
                "id": "flxJsonDataPopUpClose",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "masterType": constants.MASTER_TYPE_USERWIDGET,
                "isModalContainer": false,
                "right": "20px",
                "top": "20px",
                "width": "15px",
                "zIndex": 20
            }, {}, {
                "hoverSkin": "sknFlxPointer"
            });
            flxJsonDataPopUpClose.setDefaultUnit(kony.flex.DP);
            var lblJsonDataPopUpClose = new kony.ui.Label({
                "centerY": "50%",
                "id": "lblJsonDataPopUpClose",
                "isVisible": true,
                "left": "0dp",
                "skin": "sknIcon18pxGray",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.userwidgetmodel.lblClearSearch\")",
                "width": kony.flex.USE_PREFFERED_SIZE
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxJsonDataPopUpClose.add(lblJsonDataPopUpClose);
            flxJsonDataViewInfoContainer.add(lblJsonData, lblJsonDataSeperator, flxScrollJsonData, flxJsonDataPopUpClose);
            flxJsonDataPopupContainer.add(flxJsonDataPopUpTopColor, flxJsonDataViewInfoContainer);
            flxJsonDataPopup.add(flxJsonDataPopupContainer);
            this.add(flxMain, flxChequeImageDsplay, flxEditCancelConfirmation, flxOtherInfoPopup, flxJsonDataPopup);
        };
        return [{
            "addWidgets": addWidgetsfrmLogs,
            "enabledForIdleTimeout": true,
            "id": "frmLogs",
            "layoutType": kony.flex.FREE_FORM,
            "needAppMenu": false,
            "preShow": function(eventobject) {
                controller.AS_Form_f586e203dd3a4bc994a27a291226f29f(eventobject);
            },
            "skin": "slForm"
        }, {
            "displayOrientation": constants.FORM_DISPLAY_ORIENTATION_PORTRAIT,
            "layoutType": kony.flex.FREE_FORM,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, {
            "onDeviceBack": controller.AS_Form_a1a61b9e8db34b7b96bfc96342558f1e,
            "retainScrollPosition": false
        }]
    }
});