define("flxDecisionComments", function() {
    return function(controller) {
        var flxDecisionComments = new kony.ui.FlexContainer({
            "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
            "clipBounds": true,
            "id": "flxDecisionComments",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "left": "0dp",
            "isModalContainer": false,
            "skin": "slFbox",
            "top": "0dp",
            "width": "100%"
        }, {}, {});
        flxDecisionComments.setDefaultUnit(kony.flex.DP);
        var flxContent = new kony.ui.FlexContainer({
            "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
            "centerX": "50%",
            "clipBounds": true,
            "id": "flxContent",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "isModalContainer": false,
            "skin": "sknFlxFFFFFF1000",
            "top": "0dp",
            "width": "100%",
            "zIndex": 2
        }, {}, {
            "hoverSkin": "sknfbfcfcnopointer"
        });
        flxContent.setDefaultUnit(kony.flex.DP);
        var flxCommentsContainer = new kony.ui.FlexContainer({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "clipBounds": true,
            "height": "60dp",
            "id": "flxCommentsContainer",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "left": "55dp",
            "isModalContainer": false,
            "right": "55px",
            "skin": "slFbox",
            "top": "0dp",
            "zIndex": 1
        }, {}, {});
        flxCommentsContainer.setDefaultUnit(kony.flex.DP);
        var lblCommentsDetail = new kony.ui.Label({
            "centerY": "50%",
            "id": "lblCommentsDetail",
            "isVisible": true,
            "left": "30px",
            "right": "0px",
            "skin": "sknlblLatoReg485c7513px",
            "text": "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Sed volutpat tortor quis arcu interdum pretium vitae at lorem. Nullam sit amet nisl a diam suscipit ullamcorper vel non dolor. Proin sed interdum lectus, et laoreet neque. Aliquam pretium orci.",
            "zIndex": 1
        }, {
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, {});
        var fontIconComments = new kony.ui.Label({
            "centerY": "50%",
            "id": "fontIconComments",
            "isVisible": true,
            "left": "0dp",
            "skin": "sknfontIcon33333316px",
            "i18n_text": "kony.i18n.getLocalizedString(\"i18n.decisionManagement.commentsIcon\")",
            "width": "20px",
            "zIndex": 1
        }, {
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, {});
        flxCommentsContainer.add(lblCommentsDetail, fontIconComments);
        var lblSeperator = new kony.ui.Label({
            "bottom": 0,
            "height": "1px",
            "id": "lblSeperator",
            "isVisible": true,
            "left": "35dp",
            "right": "35dp",
            "skin": "sknlblSeperator",
            "text": ".",
            "zIndex": 1
        }, {
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, {});
        flxContent.add(flxCommentsContainer, lblSeperator);
        flxDecisionComments.add(flxContent);
        return flxDecisionComments;
    }
})