define("flxDepositsLeads", function() {
    return function(controller) {
        var flxDepositsLeads = new kony.ui.FlexContainer({
            "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
            "clipBounds": true,
            "id": "flxDepositsLeads",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "left": "0dp",
            "isModalContainer": false,
            "skin": "sknFlxFFFFFF1000",
            "top": "0dp",
            "width": "100%"
        }, {}, {
            "hoverSkin": "sknFlxf9fbfd1000"
        });
        flxDepositsLeads.setDefaultUnit(kony.flex.DP);
        var flxListOfLeadsInner = new kony.ui.FlexContainer({
            "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
            "bottom": "13dp",
            "clipBounds": true,
            "id": "flxListOfLeadsInner",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "left": "0dp",
            "isModalContainer": false,
            "skin": "slFbox",
            "top": "0dp",
            "width": "100%"
        }, {}, {});
        flxListOfLeadsInner.setDefaultUnit(kony.flex.DP);
        var lblProduct = new kony.ui.Label({
            "id": "lblProduct",
            "isVisible": true,
            "left": "30dp",
            "skin": "sknLblFont485C75100O",
            "text": "Label",
            "top": "15dp",
            "width": "18%",
            "zIndex": 1
        }, {
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, {});
        var lblStatus = new kony.ui.Label({
            "id": "lblStatus",
            "isVisible": true,
            "left": "22%",
            "skin": "sknLblFont485C75100O",
            "text": "Label",
            "top": "15dp",
            "width": "12%",
            "zIndex": 1
        }, {
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, {});
        var lblCreatedOn = new kony.ui.Label({
            "id": "lblCreatedOn",
            "isVisible": true,
            "left": "36%",
            "skin": "sknLblFont485C75100O",
            "text": "Label",
            "top": "15dp",
            "width": "12%",
            "zIndex": 1
        }, {
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, {});
        var lblCreatedBy = new kony.ui.Label({
            "id": "lblCreatedBy",
            "isVisible": true,
            "left": "50%",
            "skin": "sknLblFont485C75100O",
            "text": "Label",
            "top": "15dp",
            "width": "20%",
            "zIndex": 1
        }, {
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, {});
        var lblAssignTo = new kony.ui.Label({
            "id": "lblAssignTo",
            "isVisible": true,
            "left": "72%",
            "skin": "sknLblFont485C75100O",
            "text": "Label",
            "top": "15dp",
            "width": "20%",
            "zIndex": 1
        }, {
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, {});
        var flxLeadsOptionsMenu = new kony.ui.FlexContainer({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "clipBounds": true,
            "height": "25dp",
            "id": "flxLeadsOptionsMenu",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "isModalContainer": false,
            "right": "30dp",
            "skin": "sknFlxBorffffff1pxRound",
            "top": "10dp",
            "width": "25dp",
            "zIndex": 1
        }, {}, {
            "hoverSkin": "sknflxffffffop100Border424242Radius100px"
        });
        flxLeadsOptionsMenu.setDefaultUnit(kony.flex.DP);
        var lblIconOptionsMenu = new kony.ui.Label({
            "centerX": "50%",
            "centerY": "50%",
            "id": "lblIconOptionsMenu",
            "isVisible": true,
            "skin": "sknFontIconOptionMenu",
            "text": "",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, {
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, {});
        flxLeadsOptionsMenu.add(lblIconOptionsMenu);
        flxListOfLeadsInner.add(lblProduct, lblStatus, lblCreatedOn, lblCreatedBy, lblAssignTo, flxLeadsOptionsMenu);
        var lblSeparatorListOfLeads = new kony.ui.Label({
            "bottom": "0dp",
            "height": "1dp",
            "id": "lblSeparatorListOfLeads",
            "isVisible": true,
            "left": "20dp",
            "right": "20dp",
            "skin": "sknLblD5D9DD1000",
            "zIndex": 1
        }, {
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, {});
        flxDepositsLeads.add(flxListOfLeadsInner, lblSeparatorListOfLeads);
        return flxDepositsLeads;
    }
})