define("flxSecurityQuestions", function() {
    return function(controller) {
        var flxSecurityQuestions = new kony.ui.FlexContainer({
            "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
            "clipBounds": true,
            "id": "flxSecurityQuestions",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "isModalContainer": false,
            "skin": "sknflxffffffop100"
        }, {}, {});
        flxSecurityQuestions.setDefaultUnit(kony.flex.DP);
        var lblSecurityQuestion = new kony.ui.Label({
            "bottom": "20px",
            "id": "lblSecurityQuestion",
            "isVisible": true,
            "left": "55dp",
            "skin": "sknlblLatoBold35475f14px",
            "text": "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Duis sit amet semper massa. Maecenas varius purus lacus, nec tempor leo egestas a. Nullam posuere tincidunt laoreet.",
            "top": "20dp",
            "width": "70%",
            "zIndex": 1
        }, {
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, {});
        var flxQuestionStatus = new kony.ui.FlexContainer({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "clipBounds": true,
            "height": "20px",
            "id": "flxQuestionStatus",
            "isVisible": true,
            "layoutType": kony.flex.FLOW_HORIZONTAL,
            "left": "84%",
            "isModalContainer": false,
            "right": "16%",
            "skin": "slFbox",
            "top": "20px",
            "width": "9%",
            "zIndex": 1
        }, {}, {});
        flxQuestionStatus.setDefaultUnit(kony.flex.DP);
        var lblIconStatus = new kony.ui.Label({
            "centerY": "55%",
            "height": "12dp",
            "id": "lblIconStatus",
            "isVisible": true,
            "left": 0,
            "skin": "sknIcon13pxGreen",
            "text": "",
            "width": "12dp",
            "zIndex": 1
        }, {
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, {});
        var lblSecurityQuestionStatus = new kony.ui.Label({
            "centerY": "50%",
            "height": "20dp",
            "id": "lblSecurityQuestionStatus",
            "isVisible": true,
            "left": "5px",
            "skin": "sknlblLato5bc06cBold14px",
            "i18n_text": "kony.i18n.getLocalizedString(\"i18n.permission.Active\")",
            "width": "70px",
            "zIndex": 1
        }, {
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, {});
        flxQuestionStatus.add(lblIconStatus, lblSecurityQuestionStatus);
        var flxOptions = new kony.ui.FlexContainer({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "clipBounds": true,
            "height": "27dp",
            "id": "flxOptions",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "isModalContainer": false,
            "onClick": controller.AS_FlexContainer_a0a898a723684fd086e67b2a946411ad,
            "right": "40px",
            "skin": "slFbox",
            "top": "20dp",
            "width": "27px",
            "zIndex": 1
        }, {}, {
            "hoverSkin": "sknflxffffffop100Border424242Radius100px"
        });
        flxOptions.setDefaultUnit(kony.flex.DP);
        var lblIconOptions = new kony.ui.Label({
            "centerX": "46%",
            "centerY": "50%",
            "height": "16dp",
            "id": "lblIconOptions",
            "isVisible": true,
            "skin": "sknFontIconOptionMenu",
            "text": "",
            "width": "14dp",
            "zIndex": 1
        }, {
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, {});
        flxOptions.add(lblIconOptions);
        var lblSecurityQuestionsSeperator = new kony.ui.Label({
            "bottom": 0,
            "height": "1px",
            "id": "lblSecurityQuestionsSeperator",
            "isVisible": true,
            "left": "20px",
            "right": "20px",
            "skin": "sknlblSeperator",
            "text": ".",
            "zIndex": 1
        }, {
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, {});
        flxSecurityQuestions.add(lblSecurityQuestion, flxQuestionStatus, flxOptions, lblSecurityQuestionsSeperator);
        return flxSecurityQuestions;
    }
})