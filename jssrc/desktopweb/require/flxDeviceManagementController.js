define("userflxDeviceManagementController", {
    //Type your controller code here 
});
define("flxDeviceManagementControllerActions", {
    /*
        This is an auto generated file and any modifications to it may result in corruption of the action sequence.
    */
});
define("flxDeviceManagementController", ["userflxDeviceManagementController", "flxDeviceManagementControllerActions"], function() {
    var controller = require("userflxDeviceManagementController");
    var controllerActions = ["flxDeviceManagementControllerActions"];
    return kony.visualizer.mixinControllerActions(controller, controllerActions);
});
