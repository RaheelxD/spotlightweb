define("flxDropDown", function() {
    return function(controller) {
        var flxDropDown = new kony.ui.FlexContainer({
            "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
            "clipBounds": true,
            "id": "flxDropDown",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "left": "0dp",
            "isModalContainer": false,
            "skin": "slFbox",
            "top": "0dp",
            "width": "100%"
        }, {}, {});
        flxDropDown.setDefaultUnit(kony.flex.DP);
        var flxDropDownContainer = new kony.ui.FlexContainer({
            "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
            "clipBounds": true,
            "id": "flxDropDownContainer",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "left": "0dp",
            "isModalContainer": false,
            "skin": "slFbox",
            "top": "0dp",
            "width": "100%"
        }, {}, {
            "hoverSkin": "sknCursor"
        });
        flxDropDownContainer.setDefaultUnit(kony.flex.DP);
        var lblValue = new kony.ui.Label({
            "bottom": "10dp",
            "centerX": "50%",
            "id": "lblValue",
            "isVisible": true,
            "skin": "sknLbl485C75Font12pxKA",
            "text": "Label",
            "top": "10dp",
            "width": "90%",
            "zIndex": 1
        }, {
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, {});
        flxDropDownContainer.add(lblValue);
        flxDropDown.add(flxDropDownContainer);
        return flxDropDown;
    }
})