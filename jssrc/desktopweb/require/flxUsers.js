define("flxUsers", function() {
    return function(controller) {
        var flxUsers = new kony.ui.FlexContainer({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "clipBounds": true,
            "height": "50px",
            "id": "flxUsers",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "isModalContainer": false,
            "skin": "sknflxffffffop100"
        }, {}, {});
        flxUsers.setDefaultUnit(kony.flex.DP);
        var flxUsersContainer = new kony.ui.FlexContainer({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "clipBounds": true,
            "height": "100%",
            "id": "flxUsersContainer",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "left": "20dp",
            "isModalContainer": false,
            "right": "20dp",
            "skin": "slFbox",
            "top": "0dp"
        }, {}, {
            "hoverSkin": "sknfbfcfc"
        });
        flxUsersContainer.setDefaultUnit(kony.flex.DP);
        var lblFullName = new kony.ui.Label({
            "centerY": "50%",
            "id": "lblFullName",
            "isVisible": true,
            "left": "10px",
            "right": "85%",
            "skin": "sknlblLatoRegular484b5213px",
            "text": "John Doe",
            "width": "15%",
            "zIndex": 1
        }, {
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, {});
        var lblUsername = new kony.ui.Label({
            "centerY": "50%",
            "id": "lblUsername",
            "isVisible": true,
            "left": "19.75%",
            "skin": "sknlblLatoRegular484b5213px",
            "text": "john.doe",
            "width": "20%",
            "zIndex": 1
        }, {
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, {});
        var lblEmailId = new kony.ui.Label({
            "centerY": "50%",
            "id": "lblEmailId",
            "isVisible": true,
            "left": "40%",
            "skin": "sknlblLatoRegular484b5213px",
            "text": "john.doe@kony.com",
            "width": "29%",
            "zIndex": 1
        }, {
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, {});
        var lblRole = new kony.ui.Label({
            "centerY": "50%",
            "id": "lblRole",
            "isVisible": true,
            "left": "69%",
            "skin": "sknlblLatoRegular484b5213px",
            "text": "20",
            "width": "13%",
            "zIndex": 1
        }, {
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, {});
        var lblPermissions = new kony.ui.Label({
            "centerY": "50%",
            "id": "lblPermissions",
            "isVisible": false,
            "left": "69%",
            "skin": "sknlblLatoRegular484b5213px",
            "text": "25",
            "width": "13%",
            "zIndex": 1
        }, {
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, {});
        var flxStatus = new kony.ui.FlexContainer({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "clipBounds": true,
            "height": "100%",
            "id": "flxStatus",
            "isVisible": true,
            "layoutType": kony.flex.FLOW_HORIZONTAL,
            "left": "82%",
            "isModalContainer": false,
            "skin": "slFbox",
            "top": "0dp",
            "width": "100px",
            "zIndex": 1
        }, {}, {});
        flxStatus.setDefaultUnit(kony.flex.DP);
        var fontIconStatusImg = new kony.ui.Label({
            "centerY": "50%",
            "id": "fontIconStatusImg",
            "isVisible": true,
            "left": "0dp",
            "skin": "sknFontIconActivate",
            "text": "",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, {
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, {});
        var lblUsersStatus = new kony.ui.Label({
            "centerY": "50%",
            "id": "lblUsersStatus",
            "isVisible": true,
            "left": "5px",
            "skin": "sknlblLato5bc06cBold14px",
            "text": "Active",
            "width": "70px",
            "zIndex": 1
        }, {
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, {});
        flxStatus.add(fontIconStatusImg, lblUsersStatus);
        var flxOptions = new kony.ui.FlexContainer({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "centerY": "50%",
            "clipBounds": true,
            "height": "25dp",
            "id": "flxOptions",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "left": "94%",
            "isModalContainer": false,
            "onClick": controller.AS_FlexContainer_c805bcc4d3ad47418d4b8b9e07492755,
            "skin": "slFbox",
            "width": "25px",
            "zIndex": 10
        }, {}, {
            "hoverSkin": "sknflxffffffop100Border424242Radius100px"
        });
        flxOptions.setDefaultUnit(kony.flex.DP);
        var lblFontIconOptions = new kony.ui.Label({
            "centerX": "45%",
            "centerY": "50%",
            "height": "12dp",
            "id": "lblFontIconOptions",
            "isVisible": true,
            "skin": "sknFontIconOptionMenu",
            "text": "",
            "width": "10dp",
            "zIndex": 1
        }, {
            "contentAlignment": constants.CONTENT_ALIGN_CENTER,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, {});
        flxOptions.add(lblFontIconOptions);
        var lblSeperator = new kony.ui.Label({
            "bottom": 0,
            "height": "1px",
            "id": "lblSeperator",
            "isVisible": true,
            "left": "0dp",
            "right": "0dp",
            "skin": "sknlblSeperator",
            "text": ".",
            "zIndex": 1
        }, {
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, {});
        flxUsersContainer.add(lblFullName, lblUsername, lblEmailId, lblRole, lblPermissions, flxStatus, flxOptions, lblSeperator);
        flxUsers.add(flxUsersContainer);
        return flxUsers;
    }
})