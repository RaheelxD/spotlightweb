define("flxCompanies", function() {
    return function(controller) {
        var flxCompanies = new kony.ui.FlexContainer({
            "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
            "clipBounds": true,
            "id": "flxCompanies",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "isModalContainer": false,
            "skin": "sknflxffffffop100"
        }, {}, {});
        flxCompanies.setDefaultUnit(kony.flex.DP);
        var flxWrapper = new kony.ui.FlexContainer({
            "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
            "clipBounds": true,
            "id": "flxWrapper",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "left": "20dp",
            "isModalContainer": false,
            "right": "20dp",
            "skin": "sknflxffffffop100",
            "top": "0dp"
        }, {}, {
            "hoverSkin": "sknfbfcfc"
        });
        flxWrapper.setDefaultUnit(kony.flex.DP);
        var lblCompanyId = new kony.ui.Label({
            "id": "lblCompanyId",
            "isVisible": true,
            "left": "10dp",
            "skin": "sknlblLatoRegular484b5213px",
            "text": "TRF63736",
            "top": "18px",
            "width": "17%",
            "zIndex": 1
        }, {
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, {});
        var lblCompanyName = new kony.ui.Label({
            "id": "lblCompanyName",
            "isVisible": true,
            "left": "23%",
            "right": "78%",
            "skin": "sknlblLatoRegular484b5213px",
            "text": "View Permissions",
            "top": "18px",
            "width": "25%",
            "zIndex": 1
        }, {
            "contentAlignment": constants.CONTENT_ALIGN_TOP_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, {});
        var rtxContractCustomers = new kony.ui.RichText({
            "bottom": "15dp",
            "id": "rtxContractCustomers",
            "isVisible": true,
            "left": "24%",
            "linkSkin": "defRichTextLink",
            "skin": "sknRtxLato84939e12Px",
            "text": "-Temenos Corporation(3548838)<br/>-<b>Kon</b>y Private Limited(23487483)",
            "top": "40dp",
            "width": "24%",
            "zIndex": 1
        }, {
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, {});
        var lblType = new kony.ui.Label({
            "id": "lblType",
            "isVisible": true,
            "left": "50%",
            "skin": "sknlblLatoRegular484b5213px",
            "text": "20",
            "top": "18px",
            "width": "23%",
            "zIndex": 1
        }, {
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, {});
        var lblTIN = new kony.ui.Label({
            "id": "lblTIN",
            "isVisible": false,
            "left": "59%",
            "skin": "sknlblLatoRegular484b5213px",
            "text": "TRF63736",
            "top": "18px",
            "width": "15%",
            "zIndex": 1
        }, {
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, {});
        var lblEmail = new kony.ui.Label({
            "bottom": "18px",
            "id": "lblEmail",
            "isVisible": true,
            "left": "75%",
            "right": "35dp",
            "skin": "sknlblLatoRegular484b5213px",
            "text": "25",
            "top": "18px",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, {
            "contentAlignment": constants.CONTENT_ALIGN_TOP_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, {});
        var lblSeperator = new kony.ui.Label({
            "bottom": "0dp",
            "height": "1px",
            "id": "lblSeperator",
            "isVisible": true,
            "left": "0dp",
            "right": "0dp",
            "skin": "sknlblSeperator",
            "text": ".",
            "zIndex": 1
        }, {
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, {});
        flxWrapper.add(lblCompanyId, lblCompanyName, rtxContractCustomers, lblType, lblTIN, lblEmail, lblSeperator);
        flxCompanies.add(flxWrapper);
        return flxCompanies;
    }
})