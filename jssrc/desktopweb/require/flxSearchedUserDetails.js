define("flxSearchedUserDetails", function() {
    return function(controller) {
        var flxSearchedUserDetails = new kony.ui.FlexContainer({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "clipBounds": true,
            "height": "50px",
            "id": "flxSearchedUserDetails",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "isModalContainer": false,
            "skin": "sknflxffffffop100"
        }, {}, {
            "hoverSkin": "sknfbfcfc"
        });
        flxSearchedUserDetails.setDefaultUnit(kony.flex.DP);
        var flxAssignedCustomers = new kony.ui.FlexContainer({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "clipBounds": true,
            "height": "50px",
            "id": "flxAssignedCustomers",
            "isVisible": true,
            "layoutType": kony.flex.FLOW_HORIZONTAL,
            "left": "0",
            "isModalContainer": false,
            "skin": "slFbox",
            "top": "0",
            "width": "100%"
        }, {}, {});
        flxAssignedCustomers.setDefaultUnit(kony.flex.DP);
        var lblName = new kony.ui.Label({
            "centerY": "50%",
            "id": "lblName",
            "isVisible": true,
            "left": "6.50%",
            "skin": "sknlblLatoRegular484b5213px",
            "text": "Name",
            "width": "16%",
            "zIndex": 1
        }, {
            "contentAlignment": constants.CONTENT_ALIGN_TOP_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, {});
        var lblUserID = new kony.ui.Label({
            "centerY": "50%",
            "id": "lblUserID",
            "isVisible": true,
            "left": "1%",
            "skin": "sknlblLatoRegular484b5213px",
            "text": "User ID",
            "width": "16%",
            "zIndex": 1
        }, {
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, {});
        var lblrole = new kony.ui.Label({
            "centerY": "50%",
            "id": "lblrole",
            "isVisible": true,
            "left": "1.50%",
            "skin": "sknlblLatoRegular484b5213px",
            "text": "Role",
            "width": "20%",
            "zIndex": 1
        }, {
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, {});
        var lblOtherInfo = new kony.ui.Label({
            "centerY": "50%",
            "id": "lblOtherInfo",
            "isVisible": true,
            "left": "5%",
            "text": "View",
            "top": 0,
            "width": kony.flex.USE_PREFFERED_SIZE
        }, {
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, {});
        var flxDelete = new kony.ui.FlexContainer({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "centerY": "50%",
            "clipBounds": true,
            "height": "30dp",
            "id": "flxDelete",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "left": "22%",
            "isModalContainer": false,
            "skin": "slFbox",
            "width": "30px",
            "zIndex": 1
        }, {}, {
            "hoverSkin": "sknCursor"
        });
        flxDelete.setDefaultUnit(kony.flex.DP);
        var fontIconDelete = new kony.ui.Label({
            "centerX": "50%",
            "centerY": "50%",
            "id": "fontIconDelete",
            "isVisible": true,
            "skin": "sknLblIcon484b5218px",
            "text": "",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, {
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, {});
        flxDelete.add(fontIconDelete);
        flxAssignedCustomers.add(lblName, lblUserID, lblrole, lblOtherInfo, flxDelete);
        var lblSeperator = new kony.ui.Label({
            "bottom": 0,
            "height": "1px",
            "id": "lblSeperator",
            "isVisible": true,
            "left": "0dp",
            "skin": "sknlblSeperator",
            "text": ".",
            "width": "100%",
            "zIndex": 1
        }, {
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, {});
        flxSearchedUserDetails.add(flxAssignedCustomers, lblSeperator);
        return flxSearchedUserDetails;
    }
})