define("flxDecisionFiles", function() {
    return function(controller) {
        var flxDecisionFiles = new kony.ui.FlexContainer({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "clipBounds": true,
            "height": "60dp",
            "id": "flxDecisionFiles",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "left": "0dp",
            "isModalContainer": false,
            "skin": "sknbackGroundffffff100",
            "top": "0dp",
            "width": "100%"
        }, {}, {});
        flxDecisionFiles.setDefaultUnit(kony.flex.DP);
        var flxFileName = new kony.ui.FlexContainer({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "clipBounds": true,
            "height": "100%",
            "id": "flxFileName",
            "isVisible": true,
            "layoutType": kony.flex.FLOW_HORIZONTAL,
            "left": "55px",
            "isModalContainer": false,
            "skin": "slFbox",
            "top": "0dp",
            "width": "18%",
            "zIndex": 1
        }, {}, {});
        flxFileName.setDefaultUnit(kony.flex.DP);
        var lblFileName = new kony.ui.Label({
            "centerY": "50%",
            "id": "lblFileName",
            "isVisible": true,
            "left": "0px",
            "skin": "sknlblLato696c7312px",
            "text": "FILE NAME",
            "top": 0,
            "width": "92%",
            "zIndex": 1
        }, {
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, {});
        flxFileName.add(lblFileName);
        var flxVersion = new kony.ui.FlexContainer({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "clipBounds": true,
            "height": "100%",
            "id": "flxVersion",
            "isVisible": true,
            "layoutType": kony.flex.FLOW_HORIZONTAL,
            "left": "25%",
            "isModalContainer": false,
            "skin": "slFbox",
            "top": "0dp",
            "width": "12%",
            "zIndex": 1
        }, {}, {});
        flxVersion.setDefaultUnit(kony.flex.DP);
        var lblVersion = new kony.ui.Label({
            "centerY": "50%",
            "id": "lblVersion",
            "isVisible": true,
            "left": "0px",
            "skin": "sknlblLato696c7312px",
            "text": "VERSION",
            "top": 0,
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, {
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, {});
        flxVersion.add(lblVersion);
        var flxCreatedOn = new kony.ui.FlexContainer({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "clipBounds": true,
            "height": "100%",
            "id": "flxCreatedOn",
            "isVisible": true,
            "layoutType": kony.flex.FLOW_HORIZONTAL,
            "left": "37%",
            "isModalContainer": false,
            "skin": "slFbox",
            "top": "0dp",
            "width": "20%",
            "zIndex": 1
        }, {}, {});
        flxCreatedOn.setDefaultUnit(kony.flex.DP);
        var lblCreatedOn = new kony.ui.Label({
            "centerY": "50%",
            "id": "lblCreatedOn",
            "isVisible": true,
            "left": "0px",
            "skin": "sknlblLato696c7312px",
            "text": "CREATED ON",
            "top": 0,
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, {
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, {});
        flxCreatedOn.add(lblCreatedOn);
        var flxCreatedBy = new kony.ui.FlexContainer({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "clipBounds": true,
            "height": "100%",
            "id": "flxCreatedBy",
            "isVisible": true,
            "layoutType": kony.flex.FLOW_HORIZONTAL,
            "left": "57%",
            "isModalContainer": false,
            "skin": "slFbox",
            "top": "0dp",
            "width": "16%",
            "zIndex": 1
        }, {}, {});
        flxCreatedBy.setDefaultUnit(kony.flex.DP);
        var lblCreatedBy = new kony.ui.Label({
            "centerY": "50%",
            "id": "lblCreatedBy",
            "isVisible": true,
            "left": "0px",
            "skin": "sknlblLato696c7312px",
            "text": "CREATED BY",
            "top": 0,
            "width": "92%",
            "zIndex": 1
        }, {
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, {});
        flxCreatedBy.add(lblCreatedBy);
        var flxFileStatus = new kony.ui.FlexContainer({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "clipBounds": true,
            "height": "100%",
            "id": "flxFileStatus",
            "isVisible": true,
            "layoutType": kony.flex.FLOW_HORIZONTAL,
            "left": "73%",
            "isModalContainer": false,
            "skin": "slFbox",
            "top": "0dp",
            "width": "10%",
            "zIndex": 1
        }, {}, {});
        flxFileStatus.setDefaultUnit(kony.flex.DP);
        var lblIconStatus = new kony.ui.Label({
            "centerY": "50%",
            "id": "lblIconStatus",
            "isVisible": true,
            "left": "0dp",
            "skin": "sknIcon15px",
            "text": "",
            "top": 20,
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 10
        }, {
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, {});
        var lblFileStatus = new kony.ui.Label({
            "centerY": "50%",
            "id": "lblFileStatus",
            "isVisible": true,
            "left": "10px",
            "skin": "sknlblLato5bc06cBold14px",
            "text": "Active",
            "width": "70px",
            "zIndex": 1
        }, {
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, {});
        flxFileStatus.add(lblIconStatus, lblFileStatus);
        var flxComments = new kony.ui.FlexContainer({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "clipBounds": true,
            "height": "100%",
            "id": "flxComments",
            "isVisible": true,
            "layoutType": kony.flex.FLOW_HORIZONTAL,
            "left": "83%",
            "isModalContainer": false,
            "skin": "sknFlxPointer",
            "top": "0dp",
            "width": "10%",
            "zIndex": 1
        }, {}, {});
        flxComments.setDefaultUnit(kony.flex.DP);
        var lblComments = new kony.ui.Label({
            "centerX": "50%",
            "centerY": "50%",
            "height": "15px",
            "id": "lblComments",
            "isVisible": true,
            "skin": "sknfontIcon33333316px",
            "i18n_text": "kony.i18n.getLocalizedString(\"i18n.decisionManagement.commentsIcon\")",
            "width": "15px",
            "zIndex": 1
        }, {
            "contentAlignment": constants.CONTENT_ALIGN_CENTER,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, {});
        flxComments.add(lblComments);
        var flxOptions = new kony.ui.FlexContainer({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "centerY": "50%",
            "clipBounds": true,
            "height": "25dp",
            "id": "flxOptions",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "left": "92%",
            "isModalContainer": false,
            "skin": "sknFlxPointer",
            "width": "25dp",
            "zIndex": 1
        }, {}, {
            "hoverSkin": "sknflxffffffop100Border424242Radius100px"
        });
        flxOptions.setDefaultUnit(kony.flex.DP);
        var lblFontIconOptions = new kony.ui.Label({
            "centerX": "45%",
            "centerY": "50%",
            "height": "15dp",
            "id": "lblFontIconOptions",
            "isVisible": true,
            "left": "14dp",
            "skin": "sknFontIconOptionMenu",
            "text": "",
            "width": "15dp",
            "zIndex": 1
        }, {
            "contentAlignment": constants.CONTENT_ALIGN_CENTER,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, {});
        flxOptions.add(lblFontIconOptions);
        var lblSeperator = new kony.ui.Label({
            "bottom": 0,
            "height": "1px",
            "id": "lblSeperator",
            "isVisible": true,
            "left": "35dp",
            "right": "35dp",
            "skin": "sknlblSeperator",
            "text": ".",
            "zIndex": 1
        }, {
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, {});
        flxDecisionFiles.add(flxFileName, flxVersion, flxCreatedOn, flxCreatedBy, flxFileStatus, flxComments, flxOptions, lblSeperator);
        return flxDecisionFiles;
    }
})