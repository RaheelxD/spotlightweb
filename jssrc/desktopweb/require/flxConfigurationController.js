define("userflxConfigurationController", {
    hide: function() {
        this.executeOnParent("toggleVisibility");
    }
});
define("flxConfigurationControllerActions", {
    /*
        This is an auto generated file and any modifications to it may result in corruption of the action sequence.
    */
});
define("flxConfigurationController", ["userflxConfigurationController", "flxConfigurationControllerActions"], function() {
    var controller = require("userflxConfigurationController");
    var controllerActions = ["flxConfigurationControllerActions"];
    return kony.visualizer.mixinControllerActions(controller, controllerActions);
});
