define("flxApprovalMatrixRangeRow", function() {
    return function(controller) {
        var flxApprovalMatrixRangeRow = new kony.ui.FlexContainer({
            "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
            "clipBounds": true,
            "id": "flxApprovalMatrixRangeRow",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "left": "0dp",
            "isModalContainer": false,
            "skin": "sknFlxFFFFFF100O",
            "top": "0dp",
            "width": "100%"
        }, {}, {});
        flxApprovalMatrixRangeRow.setDefaultUnit(kony.flex.DP);
        var flxApprovalRangeCont = new kony.ui.FlexContainer({
            "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
            "bottom": "10dp",
            "clipBounds": true,
            "id": "flxApprovalRangeCont",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "left": "20px",
            "isModalContainer": false,
            "right": "20px",
            "skin": "slFbox",
            "top": "0dp"
        }, {}, {});
        flxApprovalRangeCont.setDefaultUnit(kony.flex.DP);
        var lblApprovalValue1 = new kony.ui.Label({
            "id": "lblApprovalValue1",
            "isVisible": true,
            "left": "40dp",
            "skin": "sknLatoSemibold485c7313px",
            "text": "Upto $100",
            "top": "0dp",
            "width": "43%",
            "zIndex": 1
        }, {
            "contentAlignment": constants.CONTENT_ALIGN_TOP_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, {});
        var lblApprovalValue2 = new kony.ui.Label({
            "id": "lblApprovalValue2",
            "isVisible": true,
            "left": "40%",
            "skin": "sknLatoSemibold485c7313px",
            "text": "Yes",
            "top": "0dp",
            "width": "25%",
            "zIndex": 1
        }, {
            "contentAlignment": constants.CONTENT_ALIGN_TOP_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, {});
        var lblApprovalView = new kony.ui.Label({
            "id": "lblApprovalView",
            "isVisible": true,
            "left": "75%",
            "skin": "sknLblLatoReg117eb013px",
            "i18n_text": "kony.i18n.getLocalizedString(\"i18n.permission.View\")",
            "top": "0dp",
            "width": "15%",
            "zIndex": 1
        }, {
            "contentAlignment": constants.CONTENT_ALIGN_TOP_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, {
            "hoverSkin": "sknLblLato13px117eb0Cursor"
        });
        var lblApprovalNA = new kony.ui.Label({
            "id": "lblApprovalNA",
            "isVisible": false,
            "left": "80%",
            "skin": "sknLatoSemibold485c7313px",
            "text": "N/A",
            "top": "0dp",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, {
            "contentAlignment": constants.CONTENT_ALIGN_TOP_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, {});
        flxApprovalRangeCont.add(lblApprovalValue1, lblApprovalValue2, lblApprovalView, lblApprovalNA);
        var lblLine = new kony.ui.Label({
            "bottom": "0dp",
            "id": "lblLine",
            "isVisible": true,
            "right": "95dp",
            "skin": "sknLblSeparatore7e7e7",
            "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmCSR.lblSeperator\")",
            "top": "0dp",
            "width": "1dp",
            "zIndex": 1
        }, {
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, {});
        var flxApprovalEdit = new kony.ui.FlexContainer({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "clipBounds": true,
            "height": "20dp",
            "id": "flxApprovalEdit",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "isModalContainer": false,
            "right": "35dp",
            "skin": "sknCursor",
            "top": "1dp",
            "width": "20dp"
        }, {}, {});
        flxApprovalEdit.setDefaultUnit(kony.flex.DP);
        var lblIconAction = new kony.ui.Label({
            "centerX": "50%",
            "centerY": "50%",
            "id": "lblIconAction",
            "isVisible": true,
            "skin": "sknFontIcon485C7518px",
            "text": "",
            "width": kony.flex.USE_PREFFERED_SIZE
        }, {
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, {});
        flxApprovalEdit.add(lblIconAction);
        flxApprovalMatrixRangeRow.add(flxApprovalRangeCont, lblLine, flxApprovalEdit);
        return flxApprovalMatrixRangeRow;
    }
})