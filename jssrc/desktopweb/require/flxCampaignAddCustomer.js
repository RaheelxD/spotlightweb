define("flxCampaignAddCustomer", function() {
    return function(controller) {
        var flxCampaignAddCustomer = new kony.ui.FlexContainer({
            "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
            "clipBounds": true,
            "id": "flxCampaignAddCustomer",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "isModalContainer": false,
            "skin": "slFbox"
        }, {}, {});
        flxCampaignAddCustomer.setDefaultUnit(kony.flex.DP);
        var flxAddUsersWrapper = new kony.ui.FlexContainer({
            "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
            "clipBounds": true,
            "id": "flxAddUsersWrapper",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "left": "10dp",
            "isModalContainer": false,
            "right": 20,
            "skin": "sknflxffffffOp100Border5e90cbRadius3Px",
            "top": "10dp",
            "zIndex": 1
        }, {}, {
            "hoverSkin": "blocksHover"
        });
        flxAddUsersWrapper.setDefaultUnit(kony.flex.DP);
        var flxDetails = new kony.ui.FlexContainer({
            "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
            "clipBounds": true,
            "id": "flxDetails",
            "isVisible": true,
            "layoutType": kony.flex.FLOW_VERTICAL,
            "left": "20dp",
            "isModalContainer": false,
            "skin": "slFbox",
            "top": "0dp",
            "width": "95%"
        }, {}, {});
        flxDetails.setDefaultUnit(kony.flex.DP);
        var lblName = new kony.ui.Label({
            "id": "lblName",
            "isVisible": true,
            "left": 0,
            "skin": "sknlbl485c7514px",
            "text": "John Doe",
            "top": "15px",
            "width": "100%",
            "zIndex": 1
        }, {
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, {});
        var lblDesc = new kony.ui.Label({
            "bottom": "15dp",
            "id": "lblDesc",
            "isVisible": true,
            "left": "0",
            "skin": "sknLatoRegular93a5bc12px",
            "text": "USERNAME",
            "top": "10dp",
            "width": "100%",
            "zIndex": 1
        }, {
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, {});
        flxDetails.add(lblName, lblDesc);
        var btnAdd = new kony.ui.Button({
            "id": "btnAdd",
            "isVisible": true,
            "right": 20,
            "skin": "sknBtnLatoRegular11abeb12px",
            "i18n_text": "kony.i18n.getLocalizedString(\"i18n.DragBox.ADD\")",
            "top": "15dp",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, {
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_RIGHT,
            "displayText": true,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, {
            "hoverSkin": "sknBtnLatoRegular1682b212px"
        });
        flxAddUsersWrapper.add(flxDetails, btnAdd);
        flxCampaignAddCustomer.add(flxAddUsersWrapper);
        return flxCampaignAddCustomer;
    }
})