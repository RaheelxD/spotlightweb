define("CustomerCreateModule/frmCustomerCreate", function() {
    return function(controller) {
        function addWidgetsfrmCustomerCreate() {
            this.setDefaultUnit(kony.flex.DP);
            var flxMain = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "100%",
                "id": "flxMain",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0dp",
                "width": "100%",
                "zIndex": 1
            }, {}, {});
            flxMain.setDefaultUnit(kony.flex.DP);
            var flxLinkProfilesPopup = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "bottom": "0dp",
                "clipBounds": true,
                "id": "flxLinkProfilesPopup",
                "isVisible": false,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0dp",
                "width": "100%",
                "zIndex": 20
            }, {}, {});
            flxLinkProfilesPopup.setDefaultUnit(kony.flex.DP);
            var linkProfilesPopup = new com.adminConsole.businessBanking.linkProfilesPopup({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "height": "100%",
                "id": "linkProfilesPopup",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "masterType": constants.MASTER_TYPE_DEFAULT,
                "isModalContainer": false,
                "skin": "sknFlxBg222B35Op50PopupBg",
                "top": "0dp",
                "width": "100%",
                "overrides": {
                    "flxLinkProfileContainer": {
                        "isVisible": false
                    }
                }
            }, {
                "overrides": {}
            }, {
                "overrides": {}
            });
            flxLinkProfilesPopup.add(linkProfilesPopup);
            var flxLeftPanel = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "100%",
                "id": "flxLeftPanel",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0dp",
                "width": "305dp",
                "zIndex": 1
            }, {}, {});
            flxLeftPanel.setDefaultUnit(kony.flex.DP);
            var leftMenuNew = new com.adminConsole.navigation.leftMenuNew({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "height": "100%",
                "id": "leftMenuNew",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "masterType": constants.MASTER_TYPE_DEFAULT,
                "isModalContainer": false,
                "skin": "sknFlxBg003E75Op100",
                "top": "0dp",
                "width": "100%",
                "overrides": {
                    "brandLogo": {
                        "src": "infinity_dbx_c360_logo_white_2x.png"
                    },
                    "imgBottomLogo": {
                        "src": "logo_white_2x.png"
                    },
                    "leftMenuNew": {
                        "bottom": "viz.val_cleared",
                        "centerX": "viz.val_cleared",
                        "centerY": "viz.val_cleared",
                        "height": "100%",
                        "left": "0dp",
                        "maxHeight": "viz.val_cleared",
                        "maxWidth": "viz.val_cleared",
                        "minHeight": "viz.val_cleared",
                        "minWidth": "viz.val_cleared",
                        "right": "viz.val_cleared",
                        "top": "0dp",
                        "width": "100%"
                    }
                }
            }, {
                "overrides": {}
            }, {
                "overrides": {}
            });
            flxLeftPanel.add(leftMenuNew);
            var flxRightPanel = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "bottom": "0dp",
                "clipBounds": true,
                "height": "100%",
                "id": "flxRightPanel",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "305dp",
                "isModalContainer": false,
                "right": "0dp",
                "skin": "slFbox",
                "top": "0dp",
                "zIndex": 1
            }, {}, {});
            flxRightPanel.setDefaultUnit(kony.flex.DP);
            var flxMainHeader = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "106dp",
                "id": "flxMainHeader",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0dp",
                "width": "100%",
                "zIndex": 1
            }, {}, {});
            flxMainHeader.setDefaultUnit(kony.flex.DP);
            var mainHeader = new com.adminConsole.header.mainHeader({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "height": "106px",
                "id": "mainHeader",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": 0,
                "masterType": constants.MASTER_TYPE_DEFAULT,
                "isModalContainer": false,
                "skin": "CopyslFbox1",
                "top": 0,
                "width": "100%",
                "overrides": {
                    "btnAddNewOption": {
                        "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmCompanies.CreateCompany\")",
                        "isVisible": false,
                        "right": "0dp"
                    },
                    "btnDropdownList": {
                        "i18n_text": "kony.i18n.getLocalizedString(\"i18n.mainHeader.DOWNLOADLIST\")",
                        "isVisible": false
                    },
                    "flxButtons": {
                        "right": "35dp",
                        "width": "400dp"
                    },
                    "flxHeaderSeperator": {
                        "isVisible": false
                    },
                    "imgLogout": {
                        "src": "img_logout.png"
                    },
                    "lblHeading": {
                        "i18n_text": "kony.i18n.getLocalizedString(\"i18n.leftmenu.companies\")"
                    }
                }
            }, {
                "overrides": {}
            }, {
                "overrides": {}
            });
            flxMainHeader.add(mainHeader);
            var flxBreadcrumb = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "20px",
                "id": "flxBreadcrumb",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "106px",
                "width": "100%",
                "zIndex": 10
            }, {}, {});
            flxBreadcrumb.setDefaultUnit(kony.flex.DP);
            var breadcrumbs = new com.adminConsole.common.breadcrumbs({
                "height": "20px",
                "id": "breadcrumbs",
                "isVisible": true,
                "layoutType": kony.flex.FLOW_HORIZONTAL,
                "left": "0dp",
                "masterType": constants.MASTER_TYPE_DEFAULT,
                "isModalContainer": false,
                "skin": "sknflxf5f6f8Op100",
                "top": "0dp",
                "width": "100%",
                "overrides": {
                    "breadcrumbs": {
                        "isVisible": true
                    },
                    "btnBackToMain": {
                        "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmCompanies.ViewCompanies\")"
                    },
                    "imgBreadcrumbsDown": {
                        "src": "img_down_arrow.png"
                    },
                    "imgBreadcrumbsRight": {
                        "src": "img_breadcrumb_arrow.png"
                    },
                    "imgBreadcrumbsRight2": {
                        "src": "img_breadcrumb_arrow.png"
                    },
                    "imgBreadcrumbsRight3": {
                        "src": "img_breadcrumb_arrow.png"
                    },
                    "lblCurrentScreen": {
                        "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmCompanies.CREATE_CUSTOMER\")"
                    }
                }
            }, {
                "overrides": {}
            }, {
                "overrides": {}
            });
            flxBreadcrumb.add(breadcrumbs);
            var flxMainContainer = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "bottom": "0dp",
                "clipBounds": true,
                "id": "flxMainContainer",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "sknflxf5f6f8Op100",
                "top": "126dp",
                "width": "100%",
                "zIndex": 10
            }, {}, {});
            flxMainContainer.setDefaultUnit(kony.flex.DP);
            var flxCustomerContainer = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "bottom": "0dp",
                "clipBounds": true,
                "id": "flxCustomerContainer",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "isModalContainer": false,
                "right": "0dp",
                "skin": "slFbox",
                "top": "0dp",
                "zIndex": 1
            }, {}, {});
            flxCustomerContainer.setDefaultUnit(kony.flex.DP);
            var flxCustomerCreate = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "bottom": "10dp",
                "clipBounds": true,
                "id": "flxCustomerCreate",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "35dp",
                "isModalContainer": false,
                "right": "35dp",
                "skin": "sknflxffffffop100Bordercbcdd1Radius3px",
                "top": "10dp",
                "zIndex": 1
            }, {}, {});
            flxCustomerCreate.setDefaultUnit(kony.flex.DP);
            var flxCustomerVerticalTabs = new kony.ui.FlexScrollContainer({
                "allowHorizontalBounce": false,
                "allowVerticalBounce": true,
                "bottom": "0dp",
                "bounces": true,
                "clipBounds": true,
                "enableScrolling": true,
                "height": "100%",
                "horizontalScrollIndicator": true,
                "id": "flxCustomerVerticalTabs",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "pagingEnabled": false,
                "scrollDirection": kony.flex.SCROLL_VERTICAL,
                "skin": "sknf9f9f9bor0px",
                "top": "0dp",
                "verticalScrollIndicator": true,
                "width": "230dp",
                "zIndex": 1
            }, {}, {});
            flxCustomerVerticalTabs.setDefaultUnit(kony.flex.DP);
            var verticalTabsCustomer = new com.adminConsole.common.verticalTabs1({
                "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                "id": "verticalTabsCustomer",
                "isVisible": true,
                "layoutType": kony.flex.FLOW_VERTICAL,
                "left": "0dp",
                "masterType": constants.MASTER_TYPE_DEFAULT,
                "isModalContainer": false,
                "skin": "sknflxf9f9f9NoBorder",
                "top": "0dp",
                "width": "100%",
                "overrides": {
                    "btnOption0": {
                        "left": "40dp",
                        "text": "SELECT CUSTOMERS"
                    },
                    "btnOption1": {
                        "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmCompanies.CUSTOMER_DETAILS\")",
                        "left": "40dp"
                    },
                    "btnOption2": {
                        "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmCompanies.ACCOUNT_ACCESS\")",
                        "left": "40dp"
                    },
                    "btnOption3": {
                        "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmUsers.btnAddRoles\")",
                        "left": "40dp"
                    },
                    "btnOption4": {
                        "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmCustomerProfileFeaturesActions.AccountLevelFeaturesAndActions_UC\")",
                        "left": "40dp",
                        "width": "150dp"
                    },
                    "btnOption5": {
                        "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmCustomerProfileFeaturesActions.OtherFeaturesAndActions_UC\")",
                        "left": "40dp",
                        "width": "150dp"
                    },
                    "btnOption6": {
                        "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmCustomerCreate.AssignLimits_UC\")",
                        "left": "40dp"
                    },
                    "flxAccountsLeftMenu": {
                        "isVisible": true,
                        "left": "0dp",
                        "top": "0dp"
                    },
                    "flxAccountsLeftMenu6": {
                        "isVisible": true
                    },
                    "flxImgArrow4": {
                        "isVisible": false
                    },
                    "flxImgArrow6": {
                        "isVisible": false
                    },
                    "flxOption0": {
                        "isVisible": false
                    },
                    "flxOption1": {
                        "isVisible": true
                    },
                    "flxOption3": {
                        "isVisible": true,
                        "left": "0dp",
                        "top": "0dp"
                    },
                    "flxOption4": {
                        "isVisible": true
                    },
                    "flxOption5": {
                        "isVisible": true
                    },
                    "flxOption6": {
                        "isVisible": true
                    },
                    "flxSepratorContainer": {
                        "isVisible": false
                    },
                    "flxSepratorContainer6": {
                        "isVisible": false
                    },
                    "imgSelected1": {
                        "src": "right_arrow2x.png"
                    },
                    "imgSelected2": {
                        "src": "right_arrow2x.png"
                    },
                    "imgSelected3": {
                        "src": "right_arrow2x.png"
                    },
                    "imgSelected4": {
                        "src": "right_arrow2x.png"
                    },
                    "lblIconDropArrow4": {
                        "isVisible": true
                    },
                    "lblIconDropArrow6": {
                        "isVisible": true
                    },
                    "lblOptional2": {
                        "isVisible": false
                    },
                    "lblOptional3": {
                        "isVisible": false
                    },
                    "lblOptional4": {
                        "isVisible": false,
                        "left": "40px"
                    },
                    "lblOptional5": {
                        "isVisible": false,
                        "left": "40px"
                    },
                    "lblOptional6": {
                        "left": "40px"
                    },
                    "segCompanyAccount6": {
                        "left": "30dp",
                        "right": "25dp"
                    },
                    "segCompanyAccounts": {
                        "left": "30dp",
                        "right": "25dp"
                    },
                    "verticalTabs1": {
                        "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                        "left": "0dp",
                        "top": "0dp"
                    }
                }
            }, {
                "overrides": {}
            }, {
                "overrides": {}
            });
            var flxVerticalTabsSeperator = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "bottom": "0dp",
                "clipBounds": true,
                "id": "flxVerticalTabsSeperator",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "isModalContainer": false,
                "right": "0dp",
                "skin": "sknflxd6dbe7",
                "top": "0dp",
                "width": "1dp",
                "zIndex": 1
            }, {}, {});
            flxVerticalTabsSeperator.setDefaultUnit(kony.flex.DP);
            flxVerticalTabsSeperator.add();
            flxCustomerVerticalTabs.add(verticalTabsCustomer, flxVerticalTabsSeperator);
            var flxCustomerCentricSearch = new kony.ui.FlexScrollContainer({
                "allowHorizontalBounce": false,
                "allowVerticalBounce": true,
                "bottom": "80dp",
                "bounces": true,
                "clipBounds": true,
                "enableScrolling": true,
                "horizontalScrollIndicator": true,
                "id": "flxCustomerCentricSearch",
                "isVisible": false,
                "layoutType": kony.flex.FLOW_VERTICAL,
                "left": "230dp",
                "pagingEnabled": false,
                "right": "0dp",
                "scrollDirection": kony.flex.SCROLL_VERTICAL,
                "skin": "slFSbox",
                "top": "0",
                "verticalScrollIndicator": true
            }, {}, {});
            flxCustomerCentricSearch.setDefaultUnit(kony.flex.DP);
            var flxCustCentricSelectError = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "40dp",
                "id": "flxCustCentricSelectError",
                "isVisible": false,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "20dp",
                "width": "100%"
            }, {}, {});
            flxCustCentricSelectError.setDefaultUnit(kony.flex.DP);
            var flxCustCentricSelectErrorContainer = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "40dp",
                "id": "flxCustCentricSelectErrorContainer",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "20dp",
                "isModalContainer": false,
                "right": "20dp",
                "skin": "sknFlxBorder1pxe61919",
                "top": "0dp",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {}, {});
            flxCustCentricSelectErrorContainer.setDefaultUnit(kony.flex.DP);
            var flxCustCentricErrorIcon = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "40dp",
                "id": "flxCustCentricErrorIcon",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "sknFlxBge61919",
                "top": "0dp",
                "width": "40dp",
                "zIndex": 1
            }, {}, {});
            flxCustCentricErrorIcon.setDefaultUnit(kony.flex.DP);
            var lblCustCentricErrorIcon = new kony.ui.Label({
                "centerX": "50%",
                "centerY": "50%",
                "height": "11px",
                "id": "lblCustCentricErrorIcon",
                "isVisible": true,
                "skin": "sknErrorWhiteIcon",
                "text": "",
                "width": "15px",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxCustCentricErrorIcon.add(lblCustCentricErrorIcon);
            var lblCustCentricErrorValue = new kony.ui.Label({
                "centerY": "50%",
                "id": "lblCustCentricErrorValue",
                "isVisible": true,
                "left": "50dp",
                "skin": "sknLbl485C75LatoRegular13Px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18.authorizedSignatories.selectAuthorizedSignatory\")",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxCustCentricSelectErrorContainer.add(flxCustCentricErrorIcon, lblCustCentricErrorValue);
            flxCustCentricSelectError.add(flxCustCentricSelectErrorContainer);
            var flxCustomerSearchContainer = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "305dp",
                "id": "flxCustomerSearchContainer",
                "isVisible": true,
                "layoutType": kony.flex.FLOW_VERTICAL,
                "left": "0dp",
                "isModalContainer": false,
                "right": "0dp",
                "skin": "slFbox",
                "top": "0dp",
                "width": "100%"
            }, {}, {});
            flxCustomerSearchContainer.setDefaultUnit(kony.flex.DP);
            var flxSearchHeader = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "15px",
                "id": "flxSearchHeader",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "20px",
                "width": "100%"
            }, {}, {});
            flxSearchHeader.setDefaultUnit(kony.flex.DP);
            var lblDefaultSearchHeader = new kony.ui.Label({
                "centerY": "50%",
                "id": "lblDefaultSearchHeader",
                "isVisible": true,
                "left": "20dp",
                "skin": "sknlblLatoBold485c7512px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18.authorizedSignatories.searchBy\")",
                "top": "0px",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var flxSearchError = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "100%",
                "id": "flxSearchError",
                "isVisible": false,
                "layoutType": kony.flex.FLOW_HORIZONTAL,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0dp",
                "width": "50%"
            }, {}, {});
            flxSearchError.setDefaultUnit(kony.flex.DP);
            var lblErrorIcon = new kony.ui.Label({
                "id": "lblErrorIcon",
                "isVisible": true,
                "left": "10dp",
                "skin": "sknFontIconError",
                "text": "",
                "top": "1dp",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var lblSearchError = new kony.ui.Label({
                "centerY": "50%",
                "id": "lblSearchError",
                "isVisible": true,
                "left": "2px",
                "skin": "sknlblError",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.userwidgetmodel.lblSearchError\")",
                "top": "0px",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxSearchError.add(lblErrorIcon, lblSearchError);
            var btnReset = new kony.ui.Button({
                "id": "btnReset",
                "isVisible": true,
                "right": "20px",
                "skin": "contextuallinks",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.Common.button.Reset\")",
                "top": 0,
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "displayText": true,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxSearchHeader.add(lblDefaultSearchHeader, flxSearchError, btnReset);
            var flxFirstRow = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "93dp",
                "id": "flxFirstRow",
                "isVisible": true,
                "layoutType": kony.flex.FLOW_HORIZONTAL,
                "left": "0px",
                "isModalContainer": false,
                "right": "0px",
                "skin": "slFbox",
                "top": "20px",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {}, {});
            flxFirstRow.setDefaultUnit(kony.flex.DP);
            var flxSearchCriteria1 = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "100%",
                "id": "flxSearchCriteria1",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "20dp",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0px",
                "width": "31%",
                "zIndex": 1
            }, {}, {});
            flxSearchCriteria1.setDefaultUnit(kony.flex.DP);
            var lblSearchParam1 = new kony.ui.Label({
                "id": "lblSearchParam1",
                "isVisible": true,
                "left": "0px",
                "skin": "sknLbl485C75LatoRegular13Px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.customerSearch.MemberID\")",
                "top": "0px",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var lbxSearchParam1 = new kony.ui.ListBox({
                "focusSkin": "sknlbxBgffffffBorder11abebRadius3Px14px",
                "height": "40px",
                "id": "lbxSearchParam1",
                "isVisible": true,
                "left": "0dp",
                "masterData": [
                    ["Select Customer ID", "Select Customer ID"],
                    ["1234567890", "1234567890"],
                    ["2345678901", "2345678901"]
                ],
                "selectedKey": "Select Customer ID",
                "skin": "sknlstbxNormal0f9abd8e88aa64a",
                "top": "28dp",
                "width": "100%",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [3, 0, 0, 0],
                "paddingInPixel": false
            }, {
                "hoverSkin": "lstboxcursor",
                "multiSelect": false
            });
            var flxErrorRow11 = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                "clipBounds": true,
                "id": "flxErrorRow11",
                "isVisible": true,
                "layoutType": kony.flex.FLOW_HORIZONTAL,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "70dp",
                "width": "100%",
                "zIndex": 1
            }, {}, {});
            flxErrorRow11.setDefaultUnit(kony.flex.DP);
            var lblIconEditError11 = new kony.ui.Label({
                "id": "lblIconEditError11",
                "isVisible": true,
                "left": "0dp",
                "skin": "sknErrorIcon",
                "text": "",
                "top": "0dp",
                "width": "15dp",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var lblEditErrorRow11 = new kony.ui.Label({
                "id": "lblEditErrorRow11",
                "isVisible": true,
                "left": "0dp",
                "skin": "sknlblError",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmAlertsManagement.Value_cannot_be_empty\")",
                "top": "0dp",
                "width": "170dp",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxErrorRow11.add(lblIconEditError11, lblEditErrorRow11);
            var txtCustomerId = new kony.ui.TextBox2({
                "autoCapitalize": constants.TEXTBOX_AUTO_CAPITALIZE_NONE,
                "focusSkin": "skntxtbxDetailsFocus14px",
                "height": "40dp",
                "id": "txtCustomerId",
                "isVisible": false,
                "keyBoardStyle": constants.TEXTBOX_KEY_BOARD_STYLE_DEFAULT,
                "left": "0px",
                "placeholder": "Customer ID",
                "secureTextEntry": false,
                "skin": "txtF3F3F3BorderE1E5ED",
                "textInputMode": constants.TEXTBOX_INPUT_MODE_ANY,
                "top": "25px",
                "width": "100%",
                "zIndex": 1
            }, {
                "containerHeightMode": constants.TEXTBOX_FONT_METRICS_DRIVEN_HEIGHT,
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [3, 0, 0, 0],
                "paddingInPixel": false
            }, {
                "autoCorrect": false
            });
            flxSearchCriteria1.add(lblSearchParam1, lbxSearchParam1, flxErrorRow11, txtCustomerId);
            flxFirstRow.add(flxSearchCriteria1);
            var flxSecondRow = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "90dp",
                "id": "flxSecondRow",
                "isVisible": true,
                "layoutType": kony.flex.FLOW_HORIZONTAL,
                "left": "0px",
                "isModalContainer": false,
                "right": "0px",
                "skin": "slFbox",
                "top": "10px",
                "zIndex": 1
            }, {}, {});
            flxSecondRow.setDefaultUnit(kony.flex.DP);
            var flxSearchCriteria2 = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "100%",
                "id": "flxSearchCriteria2",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "20dp",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0px",
                "width": "31%",
                "zIndex": 1
            }, {}, {});
            flxSearchCriteria2.setDefaultUnit(kony.flex.DP);
            var lblSearchParam2 = new kony.ui.Label({
                "id": "lblSearchParam2",
                "isVisible": true,
                "left": "0px",
                "skin": "sknLbl485C75LatoRegular13Px",
                "text": "Name",
                "top": "0px",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var txtSearchParam2 = new kony.ui.TextBox2({
                "autoCapitalize": constants.TEXTBOX_AUTO_CAPITALIZE_NONE,
                "focusSkin": "skntxtbxDetailsFocus14px",
                "height": "40dp",
                "id": "txtSearchParam2",
                "isVisible": true,
                "keyBoardStyle": constants.TEXTBOX_KEY_BOARD_STYLE_DEFAULT,
                "left": "0px",
                "placeholder": "Name",
                "right": "0px",
                "secureTextEntry": false,
                "skin": "skntxtbxDetails0bbf1235271384a",
                "textInputMode": constants.TEXTBOX_INPUT_MODE_ANY,
                "top": "25px",
                "zIndex": 1
            }, {
                "containerHeightMode": constants.TEXTBOX_FONT_METRICS_DRIVEN_HEIGHT,
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [3, 0, 0, 0],
                "paddingInPixel": false
            }, {
                "autoCorrect": false
            });
            flxSearchCriteria2.add(lblSearchParam2, txtSearchParam2);
            var flxSearchCriteria3 = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "100%",
                "id": "flxSearchCriteria3",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "10dp",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0px",
                "width": "31%",
                "zIndex": 1
            }, {}, {});
            flxSearchCriteria3.setDefaultUnit(kony.flex.DP);
            var lblSearchParam3 = new kony.ui.Label({
                "id": "lblSearchParam3",
                "isVisible": true,
                "left": "0px",
                "skin": "sknLbl485C75LatoRegular13Px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.Applications.DateOfBirth\")",
                "top": "0px",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var flxSearchParam3 = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "40dp",
                "id": "flxSearchParam3",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "isModalContainer": false,
                "right": "0dp",
                "skin": "sknflxffffffoptemplateop3px",
                "top": "25dp",
                "zIndex": 1
            }, {}, {
                "hoverSkin": "sknFlxSegRowHover11abeb"
            });
            flxSearchParam3.setDefaultUnit(kony.flex.DP);
            var customCalDob = new kony.ui.CustomWidget({
                "id": "customCalDob",
                "isVisible": true,
                "left": "0dp",
                "top": "5dp",
                "width": "100%",
                "height": "100%",
                "zIndex": 1,
                "clipBounds": true
            }, {
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {
                "widgetName": "dateRangePicker",
                "drops": "up",
                "maxDate": "true",
                "opens": "center",
                "rangeType": "",
                "resetData": "",
                "type": "single",
                "value": ""
            });
            flxSearchParam3.add(customCalDob);
            flxSearchCriteria3.add(lblSearchParam3, flxSearchParam3);
            var flxSearchCriteria4 = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "100%",
                "id": "flxSearchCriteria4",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "10dp",
                "isModalContainer": false,
                "right": "0dp",
                "skin": "slFbox",
                "top": "0px",
                "width": "31%",
                "zIndex": 1
            }, {}, {});
            flxSearchCriteria4.setDefaultUnit(kony.flex.DP);
            var lblSearchParam4 = new kony.ui.Label({
                "id": "lblSearchParam4",
                "isVisible": true,
                "left": "0px",
                "skin": "sknLbl485C75LatoRegular13Px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmCustomerManagementController.RetailCustomerSSN\")",
                "top": "0px",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var txtSearchParam4 = new kony.ui.TextBox2({
                "autoCapitalize": constants.TEXTBOX_AUTO_CAPITALIZE_NONE,
                "focusSkin": "skntxtbxDetailsFocus14px",
                "height": "40dp",
                "id": "txtSearchParam4",
                "isVisible": true,
                "keyBoardStyle": constants.TEXTBOX_KEY_BOARD_STYLE_DEFAULT,
                "left": "0px",
                "i18n_placeholder": "kony.i18n.getLocalizedString(\"i18n.frmCustomerManagementController.RetailCustomerSSN\")",
                "right": "0px",
                "secureTextEntry": false,
                "skin": "skntxtbxDetails0bbf1235271384a",
                "textInputMode": constants.TEXTBOX_INPUT_MODE_ANY,
                "top": "25px",
                "zIndex": 1
            }, {
                "containerHeightMode": constants.TEXTBOX_FONT_METRICS_DRIVEN_HEIGHT,
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [3, 0, 0, 0],
                "paddingInPixel": false
            }, {
                "autoCorrect": false
            });
            flxSearchCriteria4.add(lblSearchParam4, txtSearchParam4);
            flxSecondRow.add(flxSearchCriteria2, flxSearchCriteria3, flxSearchCriteria4);
            var btnSearch = new kony.ui.Button({
                "bottom": "0px",
                "height": "30dp",
                "id": "btnSearch",
                "isVisible": true,
                "right": "20dp",
                "skin": "sknbtnf7f7faLatoRegular12Px485c75Border1px485C75Radius20px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmCustomerManagementController.SEARCH\")",
                "width": "110px",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_CENTER,
                "displayText": true,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {
                "hoverSkin": "sknbtnffffffLatoRegular12Px485c75Border1px485C75Radius20px"
            });
            var flxSearchSeaparator = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "1dp",
                "id": "flxSearchSeaparator",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "isModalContainer": false,
                "right": "0dp",
                "skin": "slFbox",
                "top": "20dp",
                "width": "100%",
                "zIndex": 100
            }, {}, {});
            flxSearchSeaparator.setDefaultUnit(kony.flex.DP);
            var flxCustomerSearchSeparator = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "bottom": 0,
                "clipBounds": true,
                "height": "1px",
                "id": "flxCustomerSearchSeparator",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "20dp",
                "isModalContainer": false,
                "right": "20dp",
                "skin": "sknflxD5D9DD",
                "top": "0dp",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {}, {});
            flxCustomerSearchSeparator.setDefaultUnit(kony.flex.DP);
            flxCustomerSearchSeparator.add();
            flxSearchSeaparator.add(flxCustomerSearchSeparator);
            flxCustomerSearchContainer.add(flxSearchHeader, flxFirstRow, flxSecondRow, btnSearch, flxSearchSeaparator);
            var flxCustomerSearchResults = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                "bottom": "0dp",
                "clipBounds": true,
                "id": "flxCustomerSearchResults",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "20dp",
                "isModalContainer": false,
                "right": "0dp",
                "skin": "slFbox",
                "top": "0dp",
                "width": "100%"
            }, {}, {});
            flxCustomerSearchResults.setDefaultUnit(kony.flex.DP);
            var flxShowingResults = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                "clipBounds": true,
                "id": "flxShowingResults",
                "isVisible": true,
                "layoutType": kony.flex.FLOW_HORIZONTAL,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "15dp",
                "width": "100%"
            }, {}, {});
            flxShowingResults.setDefaultUnit(kony.flex.DP);
            var lblShowingResults = new kony.ui.Label({
                "id": "lblShowingResults",
                "isVisible": true,
                "left": "0",
                "skin": "sknllbl485c75Lato13px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18.authorizedSignatories.showingResultsForCustomerID\")",
                "top": "0",
                "width": kony.flex.USE_PREFFERED_SIZE
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var lblCustomerIDshow = new kony.ui.Label({
                "id": "lblCustomerIDshow",
                "isVisible": true,
                "left": "2dp",
                "skin": "sknllbl485c75Lato13px",
                "text": "1234567890",
                "top": "0dp",
                "width": kony.flex.USE_PREFFERED_SIZE
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxShowingResults.add(lblShowingResults, lblCustomerIDshow);
            var flxCustomerResults = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                "bottom": "0dp",
                "clipBounds": true,
                "id": "flxCustomerResults",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0",
                "isModalContainer": false,
                "right": "20dp",
                "skin": "slFbox",
                "top": "0dp",
                "width": kony.flex.USE_PREFFERED_SIZE
            }, {}, {});
            flxCustomerResults.setDefaultUnit(kony.flex.DP);
            var flxNoRecordsAvailable = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "200dp",
                "id": "flxNoRecordsAvailable",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0dp",
                "width": "100%"
            }, {}, {});
            flxNoRecordsAvailable.setDefaultUnit(kony.flex.DP);
            var rtxNoRecordsAvailable = new kony.ui.RichText({
                "centerX": "50%",
                "centerY": "50%",
                "id": "rtxNoRecordsAvailable",
                "isVisible": true,
                "left": "0dp",
                "linkSkin": "defRichTextLink",
                "skin": "sknrtxLatoRegular485c7513px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmGroups.rtxNoRecordsAvailable\")",
                "width": "100%",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_CENTER,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxNoRecordsAvailable.add(rtxNoRecordsAvailable);
            var flxCustomers = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                "bottom": "0dp",
                "clipBounds": true,
                "id": "flxCustomers",
                "isVisible": false,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "40dp",
                "width": "100%"
            }, {}, {});
            flxCustomers.setDefaultUnit(kony.flex.DP);
            var flxCustomerHeader = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "60dp",
                "id": "flxCustomerHeader",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0",
                "isModalContainer": false,
                "right": "0dp",
                "skin": "slFbox",
                "top": "0",
                "width": "100%"
            }, {}, {});
            flxCustomerHeader.setDefaultUnit(kony.flex.DP);
            var flxCustomerSegSeparator = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "bottom": 0,
                "clipBounds": true,
                "height": "1px",
                "id": "flxCustomerSegSeparator",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "isModalContainer": false,
                "right": "20dp",
                "skin": "sknTableHeaderLine",
                "top": "59dp",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {}, {});
            flxCustomerSegSeparator.setDefaultUnit(kony.flex.DP);
            flxCustomerSegSeparator.add();
            var flxContent = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "100%",
                "id": "flxContent",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "10dp",
                "isModalContainer": false,
                "right": "20dp",
                "skin": "slFbox",
                "top": "0dp"
            }, {}, {});
            flxContent.setDefaultUnit(kony.flex.DP);
            var flxDetails = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "100%",
                "id": "flxDetails",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "30px",
                "isModalContainer": false,
                "onClick": controller.AS_FlexContainer_e553ee3b13fe4973aef70f31c7776c5d,
                "right": "30px",
                "skin": "slFbox",
                "top": "0"
            }, {}, {});
            flxDetails.setDefaultUnit(kony.flex.DP);
            var flxCustName = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "100%",
                "id": "flxCustName",
                "isVisible": true,
                "layoutType": kony.flex.FLOW_HORIZONTAL,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0dp",
                "width": "20%"
            }, {}, {
                "hoverSkin": "sknFlxPointer"
            });
            flxCustName.setDefaultUnit(kony.flex.DP);
            var lblCustName = new kony.ui.Label({
                "centerY": "50%",
                "id": "lblCustName",
                "isVisible": true,
                "left": "0dp",
                "skin": "sknlblLato696c7311px",
                "text": "NAME",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var lblNameSortIcon = new kony.ui.Label({
                "centerY": "50%",
                "id": "lblNameSortIcon",
                "isVisible": true,
                "left": "10dp",
                "skin": "sknIcon15px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmAlertsManagement.lblSortName\")",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxCustName.add(lblCustName, lblNameSortIcon);
            var flxSSNNum = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "100%",
                "id": "flxSSNNum",
                "isVisible": true,
                "layoutType": kony.flex.FLOW_HORIZONTAL,
                "left": "21%",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0dp",
                "width": "15%"
            }, {}, {
                "hoverSkin": "sknFlxPointer"
            });
            flxSSNNum.setDefaultUnit(kony.flex.DP);
            var lblSSNNum = new kony.ui.Label({
                "centerY": "50%",
                "id": "lblSSNNum",
                "isVisible": true,
                "left": "0dp",
                "skin": "sknlblLato696c7311px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmCustomerManagementController.SSN\")",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var lblSsnSortIcon = new kony.ui.Label({
                "centerY": "50%",
                "id": "lblSsnSortIcon",
                "isVisible": true,
                "left": "10dp",
                "skin": "sknIcon15px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmAlertsManagement.lblSortName\")",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxSSNNum.add(lblSSNNum, lblSsnSortIcon);
            var flxDob = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "100%",
                "id": "flxDob",
                "isVisible": true,
                "layoutType": kony.flex.FLOW_HORIZONTAL,
                "left": "37%",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0dp",
                "width": "15%"
            }, {}, {
                "hoverSkin": "sknFlxPointer"
            });
            flxDob.setDefaultUnit(kony.flex.DP);
            var lblDob = new kony.ui.Label({
                "centerY": "50%",
                "id": "lblDob",
                "isVisible": true,
                "left": "0dp",
                "skin": "sknlblLato696c7311px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmCompanies.DOB\")",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var lblDobSortIcon = new kony.ui.Label({
                "centerY": "50%",
                "id": "lblDobSortIcon",
                "isVisible": true,
                "left": "10dp",
                "skin": "sknIcon15px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmAlertsManagement.lblSortName\")",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxDob.add(lblDob, lblDobSortIcon);
            var flxContact = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "100%",
                "id": "flxContact",
                "isVisible": true,
                "layoutType": kony.flex.FLOW_HORIZONTAL,
                "left": "53%",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0dp",
                "width": "25%"
            }, {}, {
                "hoverSkin": "sknFlxPointer"
            });
            flxContact.setDefaultUnit(kony.flex.DP);
            var lblContactNumber = new kony.ui.Label({
                "centerY": "50%",
                "id": "lblContactNumber",
                "isVisible": true,
                "left": "0dp",
                "skin": "sknlblLato696c7311px",
                "text": "CONTACT NUMBER",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var lblContactSortIcon = new kony.ui.Label({
                "centerY": "50%",
                "id": "lblContactSortIcon",
                "isVisible": true,
                "left": "10dp",
                "skin": "sknIcon15px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmAlertsManagement.lblSortName\")",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxContact.add(lblContactNumber, lblContactSortIcon);
            var flxEmail = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "100%",
                "id": "flxEmail",
                "isVisible": true,
                "layoutType": kony.flex.FLOW_HORIZONTAL,
                "left": "79%",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0dp",
                "width": "25%"
            }, {}, {
                "hoverSkin": "sknFlxPointer"
            });
            flxEmail.setDefaultUnit(kony.flex.DP);
            var lblEmail = new kony.ui.Label({
                "centerY": "50%",
                "id": "lblEmail",
                "isVisible": true,
                "left": "0dp",
                "skin": "sknlblLato696c7311px",
                "text": "EMAIL ADDRESS",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var lblEmailSortIcon = new kony.ui.Label({
                "centerY": "50%",
                "id": "lblEmailSortIcon",
                "isVisible": true,
                "left": "10dp",
                "skin": "sknIcon15px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmAlertsManagement.lblSortName\")",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxEmail.add(lblEmail, lblEmailSortIcon);
            flxDetails.add(flxCustName, flxSSNNum, flxDob, flxContact, flxEmail);
            flxContent.add(flxDetails);
            flxCustomerHeader.add(flxCustomerSegSeparator, flxContent);
            var segCustomerCentricDetails = new kony.ui.SegmentedUI2({
                "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                "bottom": "0dp",
                "data": [{
                    "flblUnlink": "",
                    "imgRadio": "radio_notselected.png",
                    "lblContactNumber": "+1 555-666-8791",
                    "lblDob": "11-03-1991",
                    "lblEmail": "james.ron@gmail.com",
                    "lblName": "John",
                    "lblSSN": "123-223-990",
                    "lblSeperator": "."
                }, {
                    "flblUnlink": "",
                    "imgRadio": "radio_notselected.png",
                    "lblContactNumber": "+1 555-666-8791",
                    "lblDob": "11-03-1991",
                    "lblEmail": "james.ron@gmail.com",
                    "lblName": "John",
                    "lblSSN": "123-223-990",
                    "lblSeperator": "."
                }, {
                    "flblUnlink": "",
                    "imgRadio": "radio_notselected.png",
                    "lblContactNumber": "+1 555-666-8791",
                    "lblDob": "11-03-1991",
                    "lblEmail": "james.ron@gmail.com",
                    "lblName": "John",
                    "lblSSN": "123-223-990",
                    "lblSeperator": "."
                }, {
                    "flblUnlink": "",
                    "imgRadio": "radio_notselected.png",
                    "lblContactNumber": "+1 555-666-8791",
                    "lblDob": "11-03-1991",
                    "lblEmail": "james.ron@gmail.com",
                    "lblName": "John",
                    "lblSSN": "123-223-990",
                    "lblSeperator": "."
                }, {
                    "flblUnlink": "",
                    "imgRadio": "radio_notselected.png",
                    "lblContactNumber": "+1 555-666-8791",
                    "lblDob": "11-03-1991",
                    "lblEmail": "james.ron@gmail.com",
                    "lblName": "John",
                    "lblSSN": "123-223-990",
                    "lblSeperator": "."
                }, {
                    "flblUnlink": "",
                    "imgRadio": "radio_notselected.png",
                    "lblContactNumber": "+1 555-666-8791",
                    "lblDob": "11-03-1991",
                    "lblEmail": "james.ron@gmail.com",
                    "lblName": "John",
                    "lblSSN": "123-223-990",
                    "lblSeperator": "."
                }, {
                    "flblUnlink": "",
                    "imgRadio": "radio_notselected.png",
                    "lblContactNumber": "+1 555-666-8791",
                    "lblDob": "11-03-1991",
                    "lblEmail": "james.ron@gmail.com",
                    "lblName": "John",
                    "lblSSN": "123-223-990",
                    "lblSeperator": "."
                }, {
                    "flblUnlink": "",
                    "imgRadio": "radio_notselected.png",
                    "lblContactNumber": "+1 555-666-8791",
                    "lblDob": "11-03-1991",
                    "lblEmail": "james.ron@gmail.com",
                    "lblName": "John",
                    "lblSSN": "123-223-990",
                    "lblSeperator": "."
                }, {
                    "flblUnlink": "",
                    "imgRadio": "radio_notselected.png",
                    "lblContactNumber": "+1 555-666-8791",
                    "lblDob": "11-03-1991",
                    "lblEmail": "james.ron@gmail.com",
                    "lblName": "John",
                    "lblSSN": "123-223-990",
                    "lblSeperator": "."
                }, {
                    "flblUnlink": "",
                    "imgRadio": "radio_notselected.png",
                    "lblContactNumber": "+1 555-666-8791",
                    "lblDob": "11-03-1991",
                    "lblEmail": "james.ron@gmail.com",
                    "lblName": "John",
                    "lblSSN": "123-223-990",
                    "lblSeperator": "."
                }],
                "groupCells": false,
                "id": "segCustomerCentricDetails",
                "isVisible": true,
                "left": "0dp",
                "needPageIndicator": true,
                "pageOffDotImage": "pageoffdot.png",
                "pageOnDotImage": "pageondot.png",
                "retainSelection": false,
                "rowFocusSkin": "seg2Focus",
                "rowSkin": "seg2Normal",
                "rowTemplate": "flxCustomerCentricDetails",
                "sectionHeaderSkin": "sliPhoneSegmentHeader",
                "selectionBehavior": constants.SEGUI_DEFAULT_BEHAVIOR,
                "separatorRequired": false,
                "showScrollbars": true,
                "top": "60dp",
                "viewType": constants.SEGUI_VIEW_TYPE_TABLEVIEW,
                "widgetDataMap": {
                    "flblUnlink": "flblUnlink",
                    "flxContent": "flxContent",
                    "flxCustomerCentricDetails": "flxCustomerCentricDetails",
                    "flxDetails": "flxDetails",
                    "flxRadio": "flxRadio",
                    "flxUnlink": "flxUnlink",
                    "imgRadio": "imgRadio",
                    "lblContactNumber": "lblContactNumber",
                    "lblDob": "lblDob",
                    "lblEmail": "lblEmail",
                    "lblName": "lblName",
                    "lblSSN": "lblSSN",
                    "lblSeperator": "lblSeperator"
                },
                "width": "100%"
            }, {
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxCustomers.add(flxCustomerHeader, segCustomerCentricDetails);
            flxCustomerResults.add(flxNoRecordsAvailable, flxCustomers);
            flxCustomerSearchResults.add(flxShowingResults, flxCustomerResults);
            flxCustomerCentricSearch.add(flxCustCentricSelectError, flxCustomerSearchContainer, flxCustomerSearchResults);
            var flxSelectedCustomersAccountCentric = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "bottom": "80dp",
                "clipBounds": true,
                "id": "flxSelectedCustomersAccountCentric",
                "isVisible": false,
                "layoutType": kony.flex.FLOW_VERTICAL,
                "left": "230dp",
                "isModalContainer": false,
                "right": "0dp",
                "skin": "slFbox",
                "top": "0dp"
            }, {}, {});
            flxSelectedCustomersAccountCentric.setDefaultUnit(kony.flex.DP);
            var flxAccountCentricSelectError = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "40dp",
                "id": "flxAccountCentricSelectError",
                "isVisible": false,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "20dp",
                "width": "100%"
            }, {}, {});
            flxAccountCentricSelectError.setDefaultUnit(kony.flex.DP);
            var flxAccountCentricSelectErrorContainer = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "40dp",
                "id": "flxAccountCentricSelectErrorContainer",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "20dp",
                "isModalContainer": false,
                "right": "20dp",
                "skin": "sknFlxBorder1pxe61919",
                "top": "0dp",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {}, {});
            flxAccountCentricSelectErrorContainer.setDefaultUnit(kony.flex.DP);
            var flxAuthErrorIcon = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "40dp",
                "id": "flxAuthErrorIcon",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "sknFlxBge61919",
                "top": "0dp",
                "width": "40dp",
                "zIndex": 1
            }, {}, {});
            flxAuthErrorIcon.setDefaultUnit(kony.flex.DP);
            var lblAuthErrorIcon = new kony.ui.Label({
                "centerX": "50%",
                "centerY": "50%",
                "height": "11px",
                "id": "lblAuthErrorIcon",
                "isVisible": true,
                "skin": "sknErrorWhiteIcon",
                "text": "",
                "width": "15px",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxAuthErrorIcon.add(lblAuthErrorIcon);
            var lblErrorValueAccountCentric = new kony.ui.Label({
                "centerY": "50%",
                "id": "lblErrorValueAccountCentric",
                "isVisible": true,
                "left": "50dp",
                "skin": "sknLbl485C75LatoRegular13Px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18.authorizedSignatories.selectAuthorizedSignatory\")",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxAccountCentricSelectErrorContainer.add(flxAuthErrorIcon, lblErrorValueAccountCentric);
            flxAccountCentricSelectError.add(flxAccountCentricSelectErrorContainer);
            var flxAccountSearchContainer = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "40px",
                "id": "flxAccountSearchContainer",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "isModalContainer": false,
                "right": "20dp",
                "skin": "sknflxd5d9ddop100",
                "top": "20dp",
                "width": "380px",
                "zIndex": 1
            }, {}, {});
            flxAccountSearchContainer.setDefaultUnit(kony.flex.DP);
            var fontIconSearchAccountImg = new kony.ui.Label({
                "centerY": "50%",
                "id": "fontIconSearchAccountImg",
                "isVisible": true,
                "left": "12px",
                "skin": "sknFontIconSearchImg20px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.userwidgetmodel.lblSearchIcon\")",
                "top": "0dp",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var tbxAccountSearchBox = new kony.ui.TextBox2({
                "autoCapitalize": constants.TEXTBOX_AUTO_CAPITALIZE_NONE,
                "centerY": "50%",
                "height": "30px",
                "id": "tbxAccountSearchBox",
                "isVisible": true,
                "keyBoardStyle": constants.TEXTBOX_KEY_BOARD_STYLE_DEFAULT,
                "left": "30dp",
                "onTouchStart": controller.AS_TextField_cfab06859f0f43eca99495891ee290d5,
                "i18n_placeholder": "kony.i18n.getLocalizedString(\"i18.authorizedSignatories.searchByAcountNumberName\")",
                "right": "35px",
                "secureTextEntry": false,
                "skin": "skntbxffffffNoBorderlato35475f14px",
                "textInputMode": constants.TEXTBOX_INPUT_MODE_ANY,
                "zIndex": 1
            }, {
                "containerHeightMode": constants.TEXTBOX_FONT_METRICS_DRIVEN_HEIGHT,
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [2, 0, 0, 0],
                "paddingInPixel": false
            }, {
                "autoCorrect": false
            });
            var flxClearAccountSearchImage = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "100%",
                "id": "flxClearAccountSearchImage",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "isModalContainer": false,
                "right": "0px",
                "skin": "sknCursor",
                "top": "0dp",
                "width": "35px",
                "zIndex": 1
            }, {}, {
                "hoverSkin": "sknCursor"
            });
            flxClearAccountSearchImage.setDefaultUnit(kony.flex.DP);
            var fontIconAccountCross = new kony.ui.Label({
                "centerY": "50%",
                "id": "fontIconAccountCross",
                "isVisible": true,
                "left": "5px",
                "skin": "sknFontIconSearchCross16px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.userwidgetmodel.lblClearSearch\")",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxClearAccountSearchImage.add(fontIconAccountCross);
            flxAccountSearchContainer.add(fontIconSearchAccountImg, tbxAccountSearchBox, flxClearAccountSearchImage);
            var flxAccountCentricResults = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "bottom": "0dp",
                "clipBounds": true,
                "height": "260dp",
                "id": "flxAccountCentricResults",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0dp",
                "width": "100%"
            }, {}, {});
            flxAccountCentricResults.setDefaultUnit(kony.flex.DP);
            var flxAccountCentricSegmentHeader = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "bottom": "0dp",
                "clipBounds": true,
                "height": "100%",
                "id": "flxAccountCentricSegmentHeader",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "20px",
                "isModalContainer": false,
                "right": "0dp",
                "skin": "slFbox",
                "top": "0px",
                "zIndex": 1
            }, {}, {});
            flxAccountCentricSegmentHeader.setDefaultUnit(kony.flex.DP);
            var flxHeader = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "60dp",
                "id": "flxHeader",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "isModalContainer": false,
                "right": "0dp",
                "skin": "slFbox",
                "top": "0dp"
            }, {}, {});
            flxHeader.setDefaultUnit(kony.flex.DP);
            var flxSubHeader1 = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "60dp",
                "id": "flxSubHeader1",
                "isVisible": true,
                "layoutType": kony.flex.FLOW_HORIZONTAL,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0dp",
                "width": "25%"
            }, {}, {});
            flxSubHeader1.setDefaultUnit(kony.flex.DP);
            var lblAccountNumber = new kony.ui.Label({
                "centerY": "50%",
                "id": "lblAccountNumber",
                "isVisible": true,
                "left": "10dp",
                "skin": "sknlblLato696c7312px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmCompanies.ACCOUNTNUMBER\")",
                "top": "0",
                "width": kony.flex.USE_PREFFERED_SIZE
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxSubHeader1.add(lblAccountNumber);
            var flxSubHeader2 = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "60px",
                "id": "flxSubHeader2",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "25%",
                "isModalContainer": false,
                "right": "0dp",
                "skin": "sknflxffffffop100",
                "top": "0"
            }, {}, {});
            flxSubHeader2.setDefaultUnit(kony.flex.DP);
            var flxHeaderSub2 = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "100%",
                "id": "flxHeaderSub2",
                "isVisible": true,
                "layoutType": kony.flex.FLOW_HORIZONTAL,
                "left": "45dp",
                "isModalContainer": false,
                "right": "40dp",
                "skin": "slFbox",
                "top": "0dp"
            }, {}, {});
            flxHeaderSub2.setDefaultUnit(kony.flex.DP);
            var flxName = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "100%",
                "id": "flxName",
                "isVisible": true,
                "layoutType": kony.flex.FLOW_HORIZONTAL,
                "left": "0px",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0dp",
                "width": "40%",
                "zIndex": 1
            }, {}, {
                "hoverSkin": "sknCursor"
            });
            flxName.setDefaultUnit(kony.flex.DP);
            var lblName = new kony.ui.Label({
                "centerY": "50%",
                "id": "lblName",
                "isVisible": true,
                "left": "0px",
                "skin": "sknlblLato696c7312px",
                "text": "NAME",
                "top": 0,
                "width": "70px",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {
                "hoverSkin": "sknlblLatoBold00000012px"
            });
            flxName.add(lblName);
            var flxSSN = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "100%",
                "id": "flxSSN",
                "isVisible": true,
                "layoutType": kony.flex.FLOW_HORIZONTAL,
                "left": "10dp",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0dp",
                "width": "29%",
                "zIndex": 1
            }, {}, {
                "hoverSkin": "sknCursor"
            });
            flxSSN.setDefaultUnit(kony.flex.DP);
            var lblSSN = new kony.ui.Label({
                "centerY": "50%",
                "id": "lblSSN",
                "isVisible": true,
                "left": "0px",
                "skin": "sknlblLato696c7312px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmCustomerManagementController.RetailCustomerSSN\")",
                "top": 0,
                "width": "70px",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {
                "hoverSkin": "sknlblLatoBold00000012px"
            });
            flxSSN.add(lblSSN);
            var flxMemberType = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "100%",
                "id": "flxMemberType",
                "isVisible": true,
                "layoutType": kony.flex.FLOW_HORIZONTAL,
                "left": "10dp",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0dp",
                "width": "35%",
                "zIndex": 1
            }, {}, {
                "hoverSkin": "sknCursor"
            });
            flxMemberType.setDefaultUnit(kony.flex.DP);
            var lblMemberType = new kony.ui.Label({
                "centerY": "50%",
                "id": "lblMemberType",
                "isVisible": true,
                "left": "0px",
                "skin": "sknlblLato696c7312px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18.authorizedSignatories.memberType_UC\")",
                "top": 0,
                "width": "90px",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {
                "hoverSkin": "sknlblLatoBold00000012px"
            });
            var fontIconFilterStatus = new kony.ui.Label({
                "id": "fontIconFilterStatus",
                "isVisible": true,
                "left": "5dp",
                "skin": "sknIcon15px",
                "text": "",
                "top": "20dp",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxMemberType.add(lblMemberType, fontIconFilterStatus);
            flxHeaderSub2.add(flxName, flxSSN, flxMemberType);
            flxSubHeader2.add(flxHeaderSub2);
            var flxViewAccountUsersSeperator = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "bottom": 0,
                "clipBounds": true,
                "height": "1px",
                "id": "flxViewAccountUsersSeperator",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "isModalContainer": false,
                "right": "20dp",
                "skin": "sknTableHeaderLine",
                "top": "59dp",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {}, {});
            flxViewAccountUsersSeperator.setDefaultUnit(kony.flex.DP);
            flxViewAccountUsersSeperator.add();
            flxHeader.add(flxSubHeader1, flxSubHeader2, flxViewAccountUsersSeperator);
            var flxAccountCentricSegment = new kony.ui.FlexScrollContainer({
                "allowHorizontalBounce": false,
                "allowVerticalBounce": true,
                "bottom": "0dp",
                "bounces": true,
                "clipBounds": true,
                "enableScrolling": true,
                "horizontalScrollIndicator": true,
                "id": "flxAccountCentricSegment",
                "isVisible": true,
                "layoutType": kony.flex.FLOW_VERTICAL,
                "left": "0dp",
                "pagingEnabled": false,
                "scrollDirection": kony.flex.SCROLL_VERTICAL,
                "skin": "slFSbox",
                "top": "60dp",
                "verticalScrollIndicator": true,
                "width": "100%"
            }, {}, {});
            flxAccountCentricSegment.setDefaultUnit(kony.flex.DP);
            var flxAccountSegment = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                "bottom": "0dp",
                "clipBounds": true,
                "id": "flxAccountSegment",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0dp",
                "width": "100%"
            }, {}, {});
            flxAccountSegment.setDefaultUnit(kony.flex.DP);
            var segAccountCentricDetails = new kony.ui.SegmentedUI2({
                "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                "data": [{
                    "imgRadio": "radio_notselected.png",
                    "lblMemberType": "Primary Account Holder",
                    "lblName": "John",
                    "lblSSN": "123-223-990",
                    "lblSeperator": ".",
                    "lblUnlink": ""
                }, {
                    "imgRadio": "radio_notselected.png",
                    "lblMemberType": "Primary Account Holder",
                    "lblName": "John",
                    "lblSSN": "123-223-990",
                    "lblSeperator": ".",
                    "lblUnlink": ""
                }, {
                    "imgRadio": "radio_notselected.png",
                    "lblMemberType": "Primary Account Holder",
                    "lblName": "John",
                    "lblSSN": "123-223-990",
                    "lblSeperator": ".",
                    "lblUnlink": ""
                }, {
                    "imgRadio": "radio_notselected.png",
                    "lblMemberType": "Primary Account Holder",
                    "lblName": "John",
                    "lblSSN": "123-223-990",
                    "lblSeperator": ".",
                    "lblUnlink": ""
                }, {
                    "imgRadio": "radio_notselected.png",
                    "lblMemberType": "Primary Account Holder",
                    "lblName": "John",
                    "lblSSN": "123-223-990",
                    "lblSeperator": ".",
                    "lblUnlink": ""
                }, {
                    "imgRadio": "radio_notselected.png",
                    "lblMemberType": "Primary Account Holder",
                    "lblName": "John",
                    "lblSSN": "123-223-990",
                    "lblSeperator": ".",
                    "lblUnlink": ""
                }, {
                    "imgRadio": "radio_notselected.png",
                    "lblMemberType": "Primary Account Holder",
                    "lblName": "John",
                    "lblSSN": "123-223-990",
                    "lblSeperator": ".",
                    "lblUnlink": ""
                }, {
                    "imgRadio": "radio_notselected.png",
                    "lblMemberType": "Primary Account Holder",
                    "lblName": "John",
                    "lblSSN": "123-223-990",
                    "lblSeperator": ".",
                    "lblUnlink": ""
                }, {
                    "imgRadio": "radio_notselected.png",
                    "lblMemberType": "Primary Account Holder",
                    "lblName": "John",
                    "lblSSN": "123-223-990",
                    "lblSeperator": ".",
                    "lblUnlink": ""
                }, {
                    "imgRadio": "radio_notselected.png",
                    "lblMemberType": "Primary Account Holder",
                    "lblName": "John",
                    "lblSSN": "123-223-990",
                    "lblSeperator": ".",
                    "lblUnlink": ""
                }],
                "groupCells": false,
                "id": "segAccountCentricDetails",
                "isVisible": true,
                "left": "25%",
                "needPageIndicator": true,
                "pageOffDotImage": "pageoffdot.png",
                "pageOnDotImage": "pageondot.png",
                "retainSelection": false,
                "right": "0dp",
                "rowFocusSkin": "seg2Focus",
                "rowTemplate": "flxAccountCentricDetails",
                "sectionHeaderSkin": "sliPhoneSegmentHeader",
                "selectionBehavior": constants.SEGUI_SINGLE_SELECT_BEHAVIOR,
                "separatorRequired": false,
                "showScrollbars": false,
                "top": "0dp",
                "viewType": constants.SEGUI_VIEW_TYPE_TABLEVIEW,
                "widgetDataMap": {
                    "flxAccountCentricDetails": "flxAccountCentricDetails",
                    "flxContent": "flxContent",
                    "flxDetails": "flxDetails",
                    "flxRadio": "flxRadio",
                    "flxUnlink": "flxUnlink",
                    "imgRadio": "imgRadio",
                    "lblMemberType": "lblMemberType",
                    "lblName": "lblName",
                    "lblSSN": "lblSSN",
                    "lblSeperator": "lblSeperator",
                    "lblUnlink": "lblUnlink"
                },
                "width": "75%"
            }, {
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var flxSegmentLeft = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                "clipBounds": true,
                "id": "flxSegmentLeft",
                "isVisible": true,
                "layoutType": kony.flex.FLOW_VERTICAL,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0dp",
                "width": "25%"
            }, {}, {});
            flxSegmentLeft.setDefaultUnit(kony.flex.DP);
            var accountCentricAccounts1 = new com.adminConsole.authorizedSignatories.accountCentricAccounts({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "height": "200dp",
                "id": "accountCentricAccounts1",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "masterType": constants.MASTER_TYPE_DEFAULT,
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0dp",
                "width": "100%",
                "overrides": {
                    "accountCentricAccounts": {
                        "width": "100%"
                    }
                }
            }, {
                "overrides": {}
            }, {
                "overrides": {}
            });
            var accountCentricAccounts2 = new com.adminConsole.authorizedSignatories.accountCentricAccounts({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "height": "200dp",
                "id": "accountCentricAccounts2",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "masterType": constants.MASTER_TYPE_DEFAULT,
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0dp",
                "width": "100%",
                "overrides": {
                    "accountCentricAccounts": {
                        "width": "100%"
                    }
                }
            }, {
                "overrides": {}
            }, {
                "overrides": {}
            });
            flxSegmentLeft.add(accountCentricAccounts1, accountCentricAccounts2);
            flxAccountSegment.add(segAccountCentricDetails, flxSegmentLeft);
            flxAccountCentricSegment.add(flxAccountSegment);
            var flxNoAccountsFound = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "bottom": "0dp",
                "clipBounds": true,
                "id": "flxNoAccountsFound",
                "isVisible": false,
                "layoutType": kony.flex.FLOW_VERTICAL,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "60dp",
                "width": "100%"
            }, {}, {});
            flxNoAccountsFound.setDefaultUnit(kony.flex.DP);
            var lblAccountNoResults = new kony.ui.Label({
                "centerX": "47%",
                "centerY": "50%",
                "id": "lblAccountNoResults",
                "isVisible": true,
                "left": "0dp",
                "skin": "slLabel0d20174dce8ea42",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmLogs.rtxNoResultsFound\")",
                "top": "0dp",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_CENTER,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxNoAccountsFound.add(lblAccountNoResults);
            flxAccountCentricSegmentHeader.add(flxHeader, flxAccountCentricSegment, flxNoAccountsFound);
            var flxFilter = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                "clipBounds": true,
                "id": "flxFilter",
                "isVisible": false,
                "layoutType": kony.flex.FREE_FORM,
                "isModalContainer": false,
                "right": "130dp",
                "skin": "slFbox",
                "top": "30dp",
                "width": "225px",
                "zIndex": 50
            }, {}, {});
            flxFilter.setDefaultUnit(kony.flex.DP);
            var filterMenu = new com.adminConsole.common.statusFilterMenu({
                "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                "id": "filterMenu",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "masterType": constants.MASTER_TYPE_DEFAULT,
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "5dp",
                "width": "100%",
                "zIndex": 5,
                "overrides": {
                    "imgUpArrow": {
                        "left": "viz.val_cleared",
                        "right": "15dp",
                        "src": "uparrow_2x.png"
                    },
                    "statusFilterMenu": {
                        "width": "100%"
                    }
                }
            }, {
                "overrides": {}
            }, {
                "overrides": {}
            });
            flxFilter.add(filterMenu);
            flxAccountCentricResults.add(flxAccountCentricSegmentHeader, flxFilter);
            flxSelectedCustomersAccountCentric.add(flxAccountCentricSelectError, flxAccountSearchContainer, flxAccountCentricResults);
            var flxCustomerDetails = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "bottom": "80dp",
                "clipBounds": true,
                "id": "flxCustomerDetails",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "230dp",
                "isModalContainer": false,
                "right": "0dp",
                "skin": "slFbox",
                "top": "0dp",
                "zIndex": 1
            }, {}, {});
            flxCustomerDetails.setDefaultUnit(kony.flex.DP);
            var flxCustomerDetailsContainer = new kony.ui.FlexScrollContainer({
                "allowHorizontalBounce": false,
                "allowVerticalBounce": true,
                "bottom": "0dp",
                "bounces": true,
                "clipBounds": true,
                "enableScrolling": true,
                "horizontalScrollIndicator": true,
                "id": "flxCustomerDetailsContainer",
                "isVisible": true,
                "layoutType": kony.flex.FLOW_VERTICAL,
                "left": "0dp",
                "pagingEnabled": false,
                "right": "0dp",
                "scrollDirection": kony.flex.SCROLL_VERTICAL,
                "skin": "slFSbox",
                "top": "0dp",
                "verticalScrollIndicator": true,
                "zIndex": 1
            }, {}, {});
            flxCustomerDetailsContainer.setDefaultUnit(kony.flex.DP);
            var flxOFACError = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "centerX": "50%",
                "clipBounds": true,
                "height": "40px",
                "id": "flxOFACError",
                "isVisible": false,
                "layoutType": kony.flex.FREE_FORM,
                "left": "20dp",
                "isModalContainer": false,
                "right": "20dp",
                "skin": "slFbox",
                "top": "10dp",
                "width": "95%",
                "zIndex": 1
            }, {}, {});
            flxOFACError.setDefaultUnit(kony.flex.DP);
            var alertMessage = new com.adminConsole.customerMang.alertMessage({
                "height": "40px",
                "id": "alertMessage",
                "isVisible": true,
                "layoutType": kony.flex.FLOW_HORIZONTAL,
                "left": "0dp",
                "masterType": constants.MASTER_TYPE_DEFAULT,
                "isModalContainer": false,
                "skin": "sknYellowBorder",
                "top": "0dp",
                "width": "100%",
                "overrides": {
                    "alertMessage": {
                        "height": "40px",
                        "width": "100%"
                    },
                    "flxMessage": {
                        "left": "0px",
                        "right": "viz.val_cleared",
                        "width": "91%"
                    },
                    "flxRow1": {
                        "centerX": "50%",
                        "centerY": "50%",
                        "height": "100%",
                        "left": "viz.val_cleared",
                        "top": "15%",
                        "width": "100%"
                    },
                    "flxRow2": {
                        "isVisible": false
                    },
                    "fonticonBullet1": {
                        "isVisible": false,
                        "left": "0px"
                    },
                    "lblData": {
                        "left": "10px"
                    }
                }
            }, {
                "overrides": {}
            }, {
                "overrides": {}
            });
            flxOFACError.add(alertMessage);
            var flxSalutation = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "90dp",
                "id": "flxSalutation",
                "isVisible": false,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "20dp",
                "width": "100%",
                "zIndex": 1
            }, {}, {});
            flxSalutation.setDefaultUnit(kony.flex.DP);
            var flxDetails1 = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                "clipBounds": true,
                "id": "flxDetails1",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "20dp",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0dp",
                "width": "29.80%",
                "zIndex": 2
            }, {}, {});
            flxDetails1.setDefaultUnit(kony.flex.DP);
            var lblSalutation = new kony.ui.Label({
                "id": "lblSalutation",
                "isVisible": true,
                "left": "0px",
                "skin": "sknlblLatoBold35475f14px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.userwidgetmodel.lblContactDetails1\")",
                "top": "5px",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var lstBxSalutation = new kony.ui.ListBox({
                "focusSkin": "sknlbxBgffffffBorder11abebRadius3Px14px",
                "height": "40dp",
                "id": "lstBxSalutation",
                "isVisible": true,
                "left": "0px",
                "masterData": [
                    ["k1", "Select Salutation"],
                    ["k2", "Mrs."],
                    ["k3", "Dr."],
                    ["k4", "Ms."]
                ],
                "selectedKey": "k1",
                "skin": "sknlstbxNormal0f9abd8e88aa64a",
                "top": "30dp",
                "width": "100%",
                "zIndex": 2
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [3, 0, 0, 0],
                "paddingInPixel": false
            }, {
                "hoverSkin": "sknlstbxcursor",
                "multiSelect": false
            });
            var lblSalutationError = new kony.ui.Label({
                "bottom": "0px",
                "id": "lblSalutationError",
                "isVisible": false,
                "left": "0dp",
                "skin": "sknlblError",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmCustomerManagementController.selectSalutaion\")",
                "top": 75,
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxDetails1.add(lblSalutation, lstBxSalutation, lblSalutationError);
            flxSalutation.add(flxDetails1);
            var flxLinkProfilesHeader = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                "clipBounds": true,
                "id": "flxLinkProfilesHeader",
                "isVisible": false,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "20dp",
                "width": "100%"
            }, {}, {});
            flxLinkProfilesHeader.setDefaultUnit(kony.flex.DP);
            var flxLinkProfileButton = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "22px",
                "id": "flxLinkProfileButton",
                "isVisible": false,
                "layoutType": kony.flex.FLOW_HORIZONTAL,
                "isModalContainer": false,
                "right": "20dp",
                "skin": "sknBackgroundColorUnlockCustomer",
                "top": "0dp",
                "width": "146dp",
                "zIndex": 1
            }, {}, {
                "hoverSkin": "sknHoverUnlock"
            });
            flxLinkProfileButton.setDefaultUnit(kony.flex.DP);
            var lblIconLinkProfile = new kony.ui.Label({
                "centerY": "50%",
                "id": "lblIconLinkProfile",
                "isVisible": true,
                "left": "9px",
                "skin": "sknIcon485C7513px",
                "text": "",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_CENTER,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var lblLinkProfile = new kony.ui.Label({
                "centerY": "45%",
                "id": "lblLinkProfile",
                "isVisible": true,
                "left": "5px",
                "skin": "sknlblLatoBold35475f14px",
                "text": "Link Retail Profile",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxLinkProfileButton.add(lblIconLinkProfile, lblLinkProfile);
            var lblProfileLinkingMessage = new kony.ui.Label({
                "id": "lblProfileLinkingMessage",
                "isVisible": false,
                "left": "20dp",
                "skin": "sknLbl192B45LatoRegular14px",
                "text": "This Profile is linked with the existing Retail Profile",
                "top": "0",
                "width": kony.flex.USE_PREFFERED_SIZE
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxLinkProfilesHeader.add(flxLinkProfileButton, lblProfileLinkingMessage);
            var flxRow1 = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "110dp",
                "id": "flxRow1",
                "isVisible": true,
                "layoutType": kony.flex.FLOW_HORIZONTAL,
                "left": "0dp",
                "isModalContainer": false,
                "right": "20px",
                "skin": "slFbox",
                "top": "20dp",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {}, {});
            flxRow1.setDefaultUnit(kony.flex.DP);
            var flxColumn11 = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "100%",
                "id": "flxColumn11",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "10dp",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0dp",
                "width": "31.40%"
            }, {}, {});
            flxColumn11.setDefaultUnit(kony.flex.DP);
            var textBoxEntry11 = new com.adminConsole.common.textBoxEntry({
                "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                "id": "textBoxEntry11",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "masterType": constants.MASTER_TYPE_DEFAULT,
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0dp",
                "width": "100%",
                "zIndex": 1,
                "overrides": {
                    "flxInlineError": {
                        "isVisible": false
                    },
                    "lblErrorText": {
                        "right": "viz.val_cleared"
                    },
                    "lblHeadingText": {
                        "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmUsers.lblFirstName\")"
                    },
                    "tbxEnterValue": {
                        "i18n_placeholder": "kony.i18n.getLocalizedString(\"i18n.frmUsers.lblFirstName\")"
                    }
                }
            }, {
                "overrides": {}
            }, {
                "overrides": {}
            });
            flxColumn11.add(textBoxEntry11);
            var flxColumn12 = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "100%",
                "id": "flxColumn12",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "5dp",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0dp",
                "width": "31.40%",
                "zIndex": 1
            }, {}, {});
            flxColumn12.setDefaultUnit(kony.flex.DP);
            var textBoxEntry12 = new com.adminConsole.common.textBoxEntry({
                "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                "id": "textBoxEntry12",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "masterType": constants.MASTER_TYPE_DEFAULT,
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0dp",
                "width": "100%",
                "zIndex": 1,
                "overrides": {
                    "lblHeadingText": {
                        "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmUsers.lblMiddleName\")"
                    },
                    "lblOptional": {
                        "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmPermissions.lblOptional\")",
                        "isVisible": true,
                        "left": "5dp"
                    },
                    "tbxEnterValue": {
                        "i18n_placeholder": "kony.i18n.getLocalizedString(\"i18n.frmUsers.lblMiddleName\")"
                    }
                }
            }, {
                "overrides": {}
            }, {
                "overrides": {}
            });
            flxColumn12.add(textBoxEntry12);
            var flxColumn13 = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "100%",
                "id": "flxColumn13",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "5dp",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0dp",
                "width": "31.45%",
                "zIndex": 1
            }, {}, {});
            flxColumn13.setDefaultUnit(kony.flex.DP);
            var textBoxEntry13 = new com.adminConsole.common.textBoxEntry({
                "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                "id": "textBoxEntry13",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "masterType": constants.MASTER_TYPE_DEFAULT,
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0dp",
                "width": "100%",
                "zIndex": 1,
                "overrides": {
                    "lblHeadingText": {
                        "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmUsers.lblLastName\")"
                    },
                    "tbxEnterValue": {
                        "i18n_placeholder": "kony.i18n.getLocalizedString(\"i18n.frmUsers.lblLastName\")"
                    }
                }
            }, {
                "overrides": {}
            }, {
                "overrides": {}
            });
            flxColumn13.add(textBoxEntry13);
            flxRow1.add(flxColumn11, flxColumn12, flxColumn13);
            var flxRow2 = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "110dp",
                "id": "flxRow2",
                "isVisible": true,
                "layoutType": kony.flex.FLOW_HORIZONTAL,
                "left": "0dp",
                "isModalContainer": false,
                "right": "20px",
                "skin": "slFbox",
                "top": "0dp",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {}, {});
            flxRow2.setDefaultUnit(kony.flex.DP);
            var flxColumn21 = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "100%",
                "id": "flxColumn21",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "10dp",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0dp",
                "width": "31.40%"
            }, {}, {});
            flxColumn21.setDefaultUnit(kony.flex.DP);
            var textBoxEntry21 = new com.adminConsole.common.textBoxEntry({
                "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                "id": "textBoxEntry21",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "masterType": constants.MASTER_TYPE_DEFAULT,
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0dp",
                "width": "100%",
                "zIndex": 1,
                "overrides": {
                    "flxBtnCheck": {
                        "isVisible": true
                    },
                    "lblHeadingText": {
                        "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmGroupsController.Username\")"
                    },
                    "tbxEnterValue": {
                        "i18n_placeholder": "kony.i18n.getLocalizedString(\"i18n.frmGroupsController.Username\")",
                        "right": "70dp"
                    },
                    "textBoxEntry": {
                        "left": "0dp",
                        "top": "0dp"
                    }
                }
            }, {
                "overrides": {}
            }, {
                "overrides": {}
            });
            var lblUsernameCheck = new kony.ui.Label({
                "id": "lblUsernameCheck",
                "isVisible": false,
                "left": "20dp",
                "skin": "sknlblLato5bc06cBold14px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmUsersController.Username_Available\")",
                "top": "67dp",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var flxRulesLabel = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "20px",
                "id": "flxRulesLabel",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "isModalContainer": false,
                "right": "0px",
                "skin": "slFbox",
                "top": "0px",
                "width": "30px",
                "zIndex": 10
            }, {}, {
                "hoverSkin": "sknCursor"
            });
            flxRulesLabel.setDefaultUnit(kony.flex.DP);
            var lblRules = new kony.ui.RichText({
                "id": "lblRules",
                "isVisible": true,
                "onClick": controller.AS_RichText_bd35a9814af54de48c78a578bdfcf027,
                "right": "0px",
                "skin": "slRichText0f825e4c814004c",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmErrorLogin.Rulestxt\")",
                "top": "0px",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 10
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxRulesLabel.add(lblRules);
            flxColumn21.add(textBoxEntry21, lblUsernameCheck, flxRulesLabel);
            var flxColumn22 = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "100%",
                "id": "flxColumn22",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "5dp",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0dp",
                "width": "31.40%",
                "zIndex": 1
            }, {}, {});
            flxColumn22.setDefaultUnit(kony.flex.DP);
            var textBoxEntry22 = new com.adminConsole.common.textBoxEntry({
                "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                "id": "textBoxEntry22",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "masterType": constants.MASTER_TYPE_DEFAULT,
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0dp",
                "width": "100%",
                "zIndex": 1,
                "overrides": {
                    "flxInlineError": {
                        "isVisible": false,
                        "width": kony.flex.USE_PREFFERED_SIZE
                    },
                    "lblHeadingText": {
                        "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmCustomerCareController.Email_ID\")"
                    },
                    "tbxEnterValue": {
                        "i18n_placeholder": "kony.i18n.getLocalizedString(\"i18n.frmCustomerCareController.Email_ID\")"
                    }
                }
            }, {
                "overrides": {}
            }, {
                "overrides": {}
            });
            flxColumn22.add(textBoxEntry22);
            var flxColumn23 = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "100%",
                "id": "flxColumn23",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "5dp",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0dp",
                "width": "31.45%",
                "zIndex": 1
            }, {}, {});
            flxColumn23.setDefaultUnit(kony.flex.DP);
            var lblHeading = new kony.ui.Label({
                "id": "lblHeading",
                "isVisible": true,
                "left": "10dp",
                "skin": "sknlblLato485c7514px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.userwidgetmodel.lblSearchParam4\")",
                "top": "0dp",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var textBoxEntry23 = new com.adminConsole.common.contactNumber({
                "height": "70.56%",
                "id": "textBoxEntry23",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "10dp",
                "masterType": constants.MASTER_TYPE_DEFAULT,
                "isModalContainer": false,
                "right": "1dp",
                "skin": "slFbox",
                "top": "26dp",
                "overrides": {
                    "contactNumber": {
                        "height": "70.56%",
                        "left": "10dp",
                        "right": "1dp",
                        "top": "26dp",
                        "width": "viz.val_cleared"
                    },
                    "flxContactNumberWrapper": {
                        "right": "0dp",
                        "width": "viz.val_cleared"
                    },
                    "flxError": {
                        "height": "33dp",
                        "isVisible": false,
                        "left": "0%",
                        "right": "0dp",
                        "top": "44dp",
                        "width": "viz.val_cleared"
                    },
                    "lblDash": {
                        "isVisible": true,
                        "left": "32.50%",
                        "width": "5dp"
                    },
                    "lblErrorText": {
                        "height": kony.flex.USE_PREFFERED_SIZE,
                        "right": "0dp",
                        "width": "viz.val_cleared"
                    },
                    "txtContactNumber": {
                        "left": "36.50%",
                        "right": "0dp",
                        "width": "viz.val_cleared"
                    },
                    "txtISDCode": {
                        "width": "32%"
                    }
                }
            }, {
                "overrides": {}
            }, {
                "overrides": {}
            });
            flxColumn23.add(lblHeading, textBoxEntry23);
            flxRow2.add(flxColumn21, flxColumn22, flxColumn23);
            var flxRow3 = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "110dp",
                "id": "flxRow3",
                "isVisible": true,
                "layoutType": kony.flex.FLOW_HORIZONTAL,
                "left": "0dp",
                "isModalContainer": false,
                "right": "20px",
                "skin": "slFbox",
                "top": "0dp",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {}, {});
            flxRow3.setDefaultUnit(kony.flex.DP);
            var flxColumn31 = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "100%",
                "id": "flxColumn31",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "10dp",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0dp",
                "width": "31.40%"
            }, {}, {});
            flxColumn31.setDefaultUnit(kony.flex.DP);
            var textBoxEntry31 = new com.adminConsole.common.textBoxEntry({
                "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                "id": "textBoxEntry31",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "masterType": constants.MASTER_TYPE_DEFAULT,
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0dp",
                "width": "100%",
                "zIndex": 1,
                "overrides": {
                    "flxEnterValue": {
                        "isVisible": true
                    },
                    "lblHeadingText": {
                        "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmCompanies.DateOfBirth\")"
                    },
                    "tbxEnterValue": {
                        "i18n_placeholder": "kony.i18n.getLocalizedString(\"i18n.frmCompanies.DateOfBirth\")",
                        "isVisible": false
                    },
                    "textBoxEntry": {
                        "isVisible": true
                    }
                }
            }, {
                "overrides": {}
            }, {
                "overrides": {}
            });
            var flxCalendarDOB = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "40dp",
                "id": "flxCalendarDOB",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "10dp",
                "isModalContainer": false,
                "right": "0dp",
                "skin": "sknflxffffffoptemplateop3px",
                "top": "25dp",
                "zIndex": 1
            }, {}, {
                "hoverSkin": "sknFlxSegRowHover11abeb"
            });
            flxCalendarDOB.setDefaultUnit(kony.flex.DP);
            var customCalCustomerDOB = new kony.ui.CustomWidget({
                "id": "customCalCustomerDOB",
                "isVisible": true,
                "left": "0dp",
                "top": "5dp",
                "width": "100%",
                "height": "100%",
                "zIndex": 1,
                "clipBounds": true
            }, {
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {
                "widgetName": "dateRangePicker",
                "drops": "up",
                "maxDate": "true",
                "opens": "center",
                "rangeType": "",
                "resetData": "",
                "type": "single",
                "value": ""
            });
            flxCalendarDOB.add(customCalCustomerDOB);
            flxColumn31.add(textBoxEntry31, flxCalendarDOB);
            var flxColumn32 = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "100%",
                "id": "flxColumn32",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "5dp",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0dp",
                "width": "31.40%",
                "zIndex": 1
            }, {}, {});
            flxColumn32.setDefaultUnit(kony.flex.DP);
            var textBoxEntry32 = new com.adminConsole.common.textBoxEntry({
                "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                "id": "textBoxEntry32",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "masterType": constants.MASTER_TYPE_DEFAULT,
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0dp",
                "width": "100%",
                "zIndex": 1,
                "overrides": {
                    "lblHeadingText": {
                        "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmCustomerManagementController.SSN\")"
                    },
                    "tbxEnterValue": {
                        "i18n_placeholder": "kony.i18n.getLocalizedString(\"i18n.frmCustomerManagementController.SSN\")",
                        "textInputMode": constants.TEXTBOX_INPUT_MODE_ANY
                    }
                }
            }, {
                "overrides": {}
            }, {
                "overrides": {}
            });
            flxColumn32.add(textBoxEntry32);
            var flxColumn33 = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "100%",
                "id": "flxColumn33",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "5dp",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0dp",
                "width": "31.45%",
                "zIndex": 1
            }, {}, {});
            flxColumn33.setDefaultUnit(kony.flex.DP);
            var textBoxEntry33 = new com.adminConsole.common.textBoxEntry({
                "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                "id": "textBoxEntry33",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "masterType": constants.MASTER_TYPE_DEFAULT,
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0dp",
                "width": "100%",
                "zIndex": 1,
                "overrides": {
                    "flxHeading": {
                        "width": "220dp"
                    },
                    "lblHeadingText": {
                        "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmCompanies.Drivers_License\")"
                    },
                    "lblOptional": {
                        "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmPermissions.lblOptional\")",
                        "isVisible": true,
                        "left": "5dp"
                    },
                    "tbxEnterValue": {
                        "i18n_placeholder": "kony.i18n.getLocalizedString(\"i18n.frmCompanies.Drivers_License\")"
                    }
                }
            }, {
                "overrides": {}
            }, {
                "overrides": {}
            });
            flxColumn33.add(textBoxEntry33);
            flxRow3.add(flxColumn31, flxColumn32, flxColumn33);
            var flxType = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "100dp",
                "id": "flxType",
                "isVisible": false,
                "layoutType": kony.flex.FREE_FORM,
                "left": "10dp",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0dp",
                "width": "31.40%",
                "zIndex": 1
            }, {}, {});
            flxType.setDefaultUnit(kony.flex.DP);
            var lblType = new kony.ui.Label({
                "id": "lblType",
                "isVisible": true,
                "left": "10dp",
                "skin": "sknLbl485C75LatoRegular13Px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18.authorizedSignatories.typeAuthorizedSignatory\")",
                "top": "0dp",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var lbxAuthType = new kony.ui.ListBox({
                "focusSkin": "defListBoxFocus",
                "height": "40dp",
                "id": "lbxAuthType",
                "isVisible": true,
                "left": "10dp",
                "masterData": [
                    ["owner", "Owner"],
                    ["retailer", "Retailer"],
                    ["creator", "Creator"]
                ],
                "right": "5dp",
                "skin": "sknlbxBgffffffBorderc1c9ceRadius3Px",
                "top": "25dp",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [3, 0, 0, 0],
                "paddingInPixel": false
            }, {
                "multiSelect": false
            });
            var authTypeError = new com.adminConsole.loans.applications.errorMsg({
                "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                "id": "authTypeError",
                "isVisible": false,
                "layoutType": kony.flex.FLOW_HORIZONTAL,
                "left": "10dp",
                "masterType": constants.MASTER_TYPE_DEFAULT,
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "70dp",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "overrides": {
                    "errorMsg": {
                        "bottom": "viz.val_cleared",
                        "isVisible": false,
                        "left": "10dp",
                        "top": "70dp"
                    },
                    "lblErrorText": {
                        "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmAlertsManagement.Value_cannot_be_empty\")"
                    }
                }
            }, {
                "overrides": {}
            }, {
                "overrides": {}
            });
            var tbxAuthType = new kony.ui.TextBox2({
                "autoCapitalize": constants.TEXTBOX_AUTO_CAPITALIZE_NONE,
                "focusSkin": "skntxtbxDetailsFocus14px",
                "height": "40dp",
                "id": "tbxAuthType",
                "isVisible": false,
                "keyBoardStyle": constants.TEXTBOX_KEY_BOARD_STYLE_DEFAULT,
                "left": "10px",
                "placeholder": "Type of Authorized SIgnatory",
                "right": "5dp",
                "secureTextEntry": false,
                "skin": "txtF3F3F3BorderE1E5ED",
                "textInputMode": constants.TEXTBOX_INPUT_MODE_ANY,
                "top": "25px",
                "zIndex": 1
            }, {
                "containerHeightMode": constants.TEXTBOX_FONT_METRICS_DRIVEN_HEIGHT,
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [3, 0, 0, 0],
                "paddingInPixel": false
            }, {
                "autoCorrect": false
            });
            flxType.add(lblType, lbxAuthType, authTypeError, tbxAuthType);
            var flxRow4 = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "110dp",
                "id": "flxRow4",
                "isVisible": false,
                "layoutType": kony.flex.FLOW_HORIZONTAL,
                "left": "0dp",
                "isModalContainer": false,
                "right": "20px",
                "skin": "slFbox",
                "top": "0dp",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {}, {});
            flxRow4.setDefaultUnit(kony.flex.DP);
            var flxDetailsRisk = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                "clipBounds": true,
                "id": "flxDetailsRisk",
                "isVisible": true,
                "layoutType": kony.flex.FLOW_VERTICAL,
                "left": "20dp",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0dp",
                "width": "62%"
            }, {}, {});
            flxDetailsRisk.setDefaultUnit(kony.flex.DP);
            var lblRisksHeader = new kony.ui.Label({
                "id": "lblRisksHeader",
                "isVisible": true,
                "left": "0px",
                "skin": "sknlblLatoBold35475f14px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.userwidgetmodel.lblDetails8\")",
                "top": "0px",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var flxSubDetailsRisk = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "36px",
                "id": "flxSubDetailsRisk",
                "isVisible": true,
                "layoutType": kony.flex.FLOW_HORIZONTAL,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "sknflxOuterBorder",
                "top": "10px",
                "width": "100%",
                "zIndex": 2
            }, {}, {});
            flxSubDetailsRisk.setDefaultUnit(kony.flex.DP);
            var flxRiskStatus = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "36dp",
                "id": "flxRiskStatus",
                "isVisible": false,
                "layoutType": kony.flex.FLOW_HORIZONTAL,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "sknflxfdf6f1bg100px",
                "top": "0dp",
                "width": "100dp",
                "zIndex": 111
            }, {}, {});
            flxRiskStatus.setDefaultUnit(kony.flex.DP);
            var lblDetails8 = new kony.ui.Label({
                "centerX": "50%",
                "centerY": "50%",
                "id": "lblDetails8",
                "isVisible": true,
                "skin": "sknlblLatoBold35475f14px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.userwidgetmodel.lblDetails8\")",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxRiskStatus.add(lblDetails8);
            var flxFlagSeperator = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "36dp",
                "id": "flxFlagSeperator",
                "isVisible": false,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "sknflxOuterBorder",
                "top": "0dp",
                "width": "1px",
                "zIndex": 111
            }, {}, {});
            flxFlagSeperator.setDefaultUnit(kony.flex.DP);
            flxFlagSeperator.add();
            var flxSelectFlags = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": false,
                "height": "36px",
                "id": "flxSelectFlags",
                "isVisible": true,
                "layoutType": kony.flex.FLOW_HORIZONTAL,
                "left": "0dp",
                "isModalContainer": false,
                "right": "0dp",
                "skin": "slFbox",
                "top": "0px",
                "width": "100%",
                "zIndex": 111
            }, {}, {});
            flxSelectFlags.setDefaultUnit(kony.flex.DP);
            var flxFlag3 = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "centerY": "50%",
                "clipBounds": true,
                "height": "13dp",
                "id": "flxFlag3",
                "isVisible": true,
                "layoutType": kony.flex.FLOW_HORIZONTAL,
                "left": "20px",
                "isModalContainer": false,
                "skin": "sknCursor",
                "top": "0dp",
                "width": "13px",
                "zIndex": 1
            }, {}, {});
            flxFlag3.setDefaultUnit(kony.flex.DP);
            var imgFlag3 = new kony.ui.Image2({
                "centerX": "50%",
                "centerY": "50%",
                "height": "13dp",
                "id": "imgFlag3",
                "isVisible": true,
                "left": "15dp",
                "skin": "slImage",
                "src": "checkbox.png",
                "width": "13dp",
                "zIndex": 1
            }, {
                "imageScaleMode": constants.IMAGE_SCALE_MODE_FIT_TO_DIMENSIONS,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {
                "toolTip": "Delete"
            });
            flxFlag3.add(imgFlag3);
            var lblFlag3 = new kony.ui.Label({
                "centerY": "50%",
                "id": "lblFlag3",
                "isVisible": true,
                "left": "6px",
                "skin": "sknlblLatoBold35475f14px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.userwidgetmodel.lblFlag3\")",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var flxFlag2 = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "centerY": "50%",
                "clipBounds": true,
                "height": "13dp",
                "id": "flxFlag2",
                "isVisible": true,
                "layoutType": kony.flex.FLOW_HORIZONTAL,
                "left": "10%",
                "isModalContainer": false,
                "skin": "sknCursor",
                "top": "0dp",
                "width": "13px",
                "zIndex": 1
            }, {}, {});
            flxFlag2.setDefaultUnit(kony.flex.DP);
            var imgFlag2 = new kony.ui.Image2({
                "centerX": "50%",
                "centerY": "50%",
                "height": "13dp",
                "id": "imgFlag2",
                "isVisible": true,
                "skin": "slImage",
                "src": "checkbox.png",
                "width": "13dp",
                "zIndex": 1
            }, {
                "imageScaleMode": constants.IMAGE_SCALE_MODE_FIT_TO_DIMENSIONS,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {
                "toolTip": "Deactive"
            });
            flxFlag2.add(imgFlag2);
            var lblFlag2 = new kony.ui.Label({
                "centerY": "50%",
                "id": "lblFlag2",
                "isVisible": true,
                "left": "6px",
                "skin": "sknlblLatoBold35475f14px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmCustomerManagementController.Defaulter\")",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var flxFlag1 = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "centerY": "50%",
                "clipBounds": true,
                "height": "13dp",
                "id": "flxFlag1",
                "isVisible": true,
                "layoutType": kony.flex.FLOW_HORIZONTAL,
                "left": "10%",
                "isModalContainer": false,
                "skin": "sknCursor",
                "top": 15,
                "width": "13px",
                "zIndex": 2
            }, {}, {});
            flxFlag1.setDefaultUnit(kony.flex.DP);
            var imgFlag1 = new kony.ui.Image2({
                "centerX": "50%",
                "centerY": "50%",
                "height": "13dp",
                "id": "imgFlag1",
                "isVisible": true,
                "skin": "slImage",
                "src": "checkbox.png",
                "width": "13dp",
                "zIndex": 1
            }, {
                "imageScaleMode": constants.IMAGE_SCALE_MODE_FIT_TO_DIMENSIONS,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {
                "toolTip": "Active"
            });
            flxFlag1.add(imgFlag1);
            var lblFlag1 = new kony.ui.Label({
                "centerY": "50%",
                "id": "lblFlag1",
                "isVisible": true,
                "left": "6px",
                "skin": "sknlblLatoBold35475f14px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmCustomerManagementController.High_Risk\")",
                "top": "15px",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxSelectFlags.add(flxFlag3, lblFlag3, flxFlag2, lblFlag2, flxFlag1, lblFlag1);
            flxSubDetailsRisk.add(flxRiskStatus, flxFlagSeperator, flxSelectFlags);
            flxDetailsRisk.add(lblRisksHeader, flxSubDetailsRisk);
            var flxEagreementDetails = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                "clipBounds": true,
                "id": "flxEagreementDetails",
                "isVisible": true,
                "layoutType": kony.flex.FLOW_VERTICAL,
                "left": 15,
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0dp",
                "width": "29%",
                "zIndex": 2
            }, {}, {});
            flxEagreementDetails.setDefaultUnit(kony.flex.DP);
            var lblEagreementHeader = new kony.ui.Label({
                "id": "lblEagreementHeader",
                "isVisible": true,
                "left": "0px",
                "skin": "sknlblLatoBold35475f14px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmCustomerManagementController.Edit_eagreementStatus\")",
                "top": "0px",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var switchEagreement = new kony.ui.Switch({
                "height": "25dp",
                "id": "switchEagreement",
                "isVisible": true,
                "left": "0dp",
                "leftSideText": "ON",
                "rightSideText": "OFF",
                "selectedIndex": 0,
                "skin": "sknSwitchServiceManagement",
                "top": "17px",
                "width": "38dp",
                "zIndex": 1
            }, {
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxEagreementDetails.add(lblEagreementHeader, switchEagreement);
            flxRow4.add(flxDetailsRisk, flxEagreementDetails);
            flxCustomerDetailsContainer.add(flxOFACError, flxSalutation, flxLinkProfilesHeader, flxRow1, flxRow2, flxRow3, flxType, flxRow4);
            var flxUsernameRules = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                "clipBounds": true,
                "id": "flxUsernameRules",
                "isVisible": false,
                "layoutType": kony.flex.FREE_FORM,
                "left": "20px",
                "isModalContainer": false,
                "skin": "slFbox0a30851fc80b24b",
                "top": "150px",
                "width": "370px",
                "zIndex": 10
            }, {}, {});
            flxUsernameRules.setDefaultUnit(kony.flex.DP);
            var lblUsernameRules = new kony.ui.Label({
                "height": "15px",
                "id": "lblUsernameRules",
                "isVisible": true,
                "left": "10px",
                "skin": "slLabel0ff9cfa99545646",
                "text": "Username rules:",
                "top": "10px",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var flxUsernameRulesInner = new kony.ui.FlexScrollContainer({
                "allowHorizontalBounce": false,
                "allowVerticalBounce": true,
                "bottom": "10px",
                "bounces": true,
                "clipBounds": true,
                "enableScrolling": true,
                "height": "120px",
                "horizontalScrollIndicator": true,
                "id": "flxUsernameRulesInner",
                "isVisible": true,
                "layoutType": kony.flex.FLOW_VERTICAL,
                "left": "10px",
                "pagingEnabled": false,
                "right": "10px",
                "scrollDirection": kony.flex.SCROLL_VERTICAL,
                "skin": "slFSbox",
                "top": "40px",
                "verticalScrollIndicator": true,
                "zIndex": 1
            }, {}, {});
            flxUsernameRulesInner.setDefaultUnit(kony.flex.DP);
            var rtxUsernameRules = new kony.ui.RichText({
                "bottom": "10px",
                "id": "rtxUsernameRules",
                "isVisible": true,
                "left": "0px",
                "linkSkin": "defRichTextLink",
                "skin": "sknrtxLato485c7514px",
                "text": "<ul><li>Minimum Length- 8, Maximum Length- 24</li> <li>Can be alpha-numeric (a-z, A-Z, 0-9)</li> <li>No spaces or special characters allowed except period '.'</li> <li>Should Start with an alphabet or digit.</li> <li>Username is not case sensitive.</li> <li>Period '.' cannot be used more than once.</li></ul>",
                "top": "0px",
                "width": "100%",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_TOP_LEFT,
                "padding": [5, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxUsernameRulesInner.add(rtxUsernameRules);
            flxUsernameRules.add(lblUsernameRules, flxUsernameRulesInner);
            flxCustomerDetails.add(flxCustomerDetailsContainer, flxUsernameRules);
            var flxCustomerAccounts = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "bottom": "0dp",
                "clipBounds": true,
                "id": "flxCustomerAccounts",
                "isVisible": false,
                "layoutType": kony.flex.FREE_FORM,
                "left": "230dp",
                "isModalContainer": false,
                "right": "0dp",
                "skin": "slFbox",
                "top": "0dp",
                "zIndex": 1
            }, {}, {});
            flxCustomerAccounts.setDefaultUnit(kony.flex.DP);
            var flxHeading = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "40dp",
                "id": "flxHeading",
                "isVisible": false,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0dp",
                "width": "100%",
                "zIndex": 1
            }, {}, {});
            flxHeading.setDefaultUnit(kony.flex.DP);
            var flxMembership = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "100%",
                "id": "flxMembership",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "20dp",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0dp",
                "width": "290dp",
                "zIndex": 1
            }, {}, {});
            flxMembership.setDefaultUnit(kony.flex.DP);
            var lblMembershipHeading = new kony.ui.Label({
                "id": "lblMembershipHeading",
                "isVisible": true,
                "left": "0dp",
                "skin": "slLabel0d20174dce8ea42",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmCompanies.MEMBERSHIP_ID\")",
                "top": "15dp",
                "width": "130dp",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var lblMembershipValue = new kony.ui.Label({
                "id": "lblMembershipValue",
                "isVisible": true,
                "left": "125dp",
                "skin": "slLabel0d20174dce8ea42",
                "text": "234163546786453216",
                "top": "15dp",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxMembership.add(lblMembershipHeading, lblMembershipValue);
            var flxTin = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "100%",
                "id": "flxTin",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "350dp",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0dp",
                "width": "250dp",
                "zIndex": 1
            }, {}, {});
            flxTin.setDefaultUnit(kony.flex.DP);
            var lblTinHeading = new kony.ui.Label({
                "id": "lblTinHeading",
                "isVisible": true,
                "left": "0dp",
                "skin": "slLabel0d20174dce8ea42",
                "text": "TAX NUMBER",
                "top": "15dp",
                "width": "90dp",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var lblTinValue = new kony.ui.Label({
                "id": "lblTinValue",
                "isVisible": true,
                "left": "95dp",
                "skin": "slLabel0d20174dce8ea42",
                "text": "234163546",
                "top": "15dp",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxTin.add(lblTinHeading, lblTinValue);
            var flxLine = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "bottom": "0dp",
                "clipBounds": true,
                "height": "1dp",
                "id": "flxLine",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "20dp",
                "isModalContainer": false,
                "right": "20dp",
                "skin": "sknSeparatorCsr",
                "zIndex": 1
            }, {}, {});
            flxLine.setDefaultUnit(kony.flex.DP);
            flxLine.add();
            flxHeading.add(flxMembership, flxTin, flxLine);
            var flxAccountsAddAndRemove = new kony.ui.FlexScrollContainer({
                "allowHorizontalBounce": false,
                "allowVerticalBounce": true,
                "bottom": "80dp",
                "bounces": true,
                "clipBounds": true,
                "enableScrolling": true,
                "horizontalScrollIndicator": true,
                "id": "flxAccountsAddAndRemove",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "pagingEnabled": false,
                "scrollDirection": kony.flex.SCROLL_VERTICAL,
                "skin": "slFSbox",
                "top": "0dp",
                "verticalScrollIndicator": true,
                "width": "100%",
                "zIndex": 1
            }, {}, {});
            flxAccountsAddAndRemove.setDefaultUnit(kony.flex.DP);
            var flxAccountError = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "40dp",
                "id": "flxAccountError",
                "isVisible": false,
                "layoutType": kony.flex.FREE_FORM,
                "left": "20dp",
                "isModalContainer": false,
                "right": "20dp",
                "skin": "sknFlxBorder1pxe61919",
                "top": "20dp",
                "zIndex": 1
            }, {}, {});
            flxAccountError.setDefaultUnit(kony.flex.DP);
            var flxAccountErrorIconContainer = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "40dp",
                "id": "flxAccountErrorIconContainer",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "sknFlxBge61919",
                "top": "0dp",
                "width": "40dp",
                "zIndex": 1
            }, {}, {});
            flxAccountErrorIconContainer.setDefaultUnit(kony.flex.DP);
            var lblAccountErrorIcon = new kony.ui.Label({
                "centerX": "50%",
                "centerY": "50%",
                "height": "11px",
                "id": "lblAccountErrorIcon",
                "isVisible": true,
                "skin": "sknErrorWhiteIcon",
                "text": "",
                "width": "15px",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxAccountErrorIconContainer.add(lblAccountErrorIcon);
            var lblAccountErrorValue = new kony.ui.Label({
                "centerY": "50%",
                "id": "lblAccountErrorValue",
                "isVisible": true,
                "left": "50dp",
                "skin": "sknLbl485C75LatoRegular13Px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmCompanies.Select_atleast_one_account_to_proceed\")",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxAccountError.add(flxAccountErrorIconContainer, lblAccountErrorValue);
            var addAndRemoveAccounts = new com.adminConsole.common.addAndRemoveOptions1({
                "bottom": "0dp",
                "id": "addAndRemoveAccounts",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "masterType": constants.MASTER_TYPE_DEFAULT,
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0dp",
                "width": "100%",
                "overrides": {
                    "addAndRemoveOptions1": {
                        "bottom": "0dp",
                        "height": "viz.val_cleared",
                        "isVisible": true,
                        "right": "viz.val_cleared",
                        "top": "0dp",
                        "width": "100%"
                    },
                    "btnRemoveAll": {
                        "i18n_text": "kony.i18n.getLocalizedString(\"i18n.Common.button.Reset\")"
                    },
                    "btnSelectAll": {
                        "isVisible": false
                    },
                    "flxAccountSearchLoading": {
                        "isVisible": false
                    },
                    "flxAvailableOptions": {
                        "height": "100%",
                        "left": "20px",
                        "right": "15px"
                    },
                    "flxAvailableOptionsWrapper": {
                        "width": "50%"
                    },
                    "flxError": {
                        "isVisible": false,
                        "left": "30%"
                    },
                    "flxFilteredSearch": {
                        "isVisible": true
                    },
                    "flxSearchContainer": {
                        "isVisible": false
                    },
                    "flxSegSearchType": {
                        "isVisible": false
                    },
                    "flxSelectedOptions": {
                        "left": "15px",
                        "right": "20px"
                    },
                    "flxSelectedOptionsWrapper": {
                        "width": "50%"
                    },
                    "flxSelectedType": {
                        "isVisible": true,
                        "width": "150dp"
                    },
                    "imgLoadingAccountSearch": {
                        "src": "loadingscreenimage.gif"
                    },
                    "lblAvailableOptionsHeading": {
                        "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmCompanies.Available_Accounts\")"
                    },
                    "lblSelectedOption": {
                        "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmCompanies.Selected_Accounts\")"
                    },
                    "rtxAvailableOptionsMessage": {
                        "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmCompanies.Search_to_see_available_accounts\")"
                    },
                    "rtxSelectedOptionsMessage": {
                        "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmCompanies.Select_atleast_one_account_to_proceed\")"
                    },
                    "segAddOptions": {
                        "data": [{
                            "btnAdd": "",
                            "lblFullName": "",
                            "lblUserIdValue": "",
                            "lblUsername": ""
                        }]
                    },
                    "segSelectedOptions": {
                        "data": [{
                            "fontIconClose": "",
                            "lblOption": ""
                        }]
                    },
                    "tbxFilterSearch": {
                        "left": "100dp"
                    },
                    "tbxSearchBox": {
                        "i18n_placeholder": "kony.i18n.getLocalizedString(\"i18n.frmCompanies.Search_by_AccountNo_TIN_MembershipID\")"
                    }
                }
            }, {
                "overrides": {}
            }, {
                "overrides": {}
            });
            flxAccountsAddAndRemove.add(flxAccountError, addAndRemoveAccounts);
            flxCustomerAccounts.add(flxHeading, flxAccountsAddAndRemove);
            var flxCustomerAssignRole = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "bottom": "0dp",
                "clipBounds": true,
                "id": "flxCustomerAssignRole",
                "isVisible": false,
                "layoutType": kony.flex.FREE_FORM,
                "left": "230dp",
                "isModalContainer": false,
                "right": "0dp",
                "skin": "slFbox",
                "top": "0dp"
            }, {}, {});
            flxCustomerAssignRole.setDefaultUnit(kony.flex.DP);
            var flxAssignRolesContainer = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "bottom": "80dp",
                "clipBounds": true,
                "id": "flxAssignRolesContainer",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "isModalContainer": false,
                "right": "0dp",
                "skin": "slFbox",
                "top": "0dp"
            }, {}, {});
            flxAssignRolesContainer.setDefaultUnit(kony.flex.DP);
            var flxAssignRoleHeader = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                "clipBounds": true,
                "id": "flxAssignRoleHeader",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "20dp",
                "width": "100%"
            }, {}, {});
            flxAssignRoleHeader.setDefaultUnit(kony.flex.DP);
            var lblRolesTitle = new kony.ui.Label({
                "id": "lblRolesTitle",
                "isVisible": true,
                "left": "20dp",
                "skin": "sknLbl192B4518pxLatoReg",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.leftmenu.Roles\")",
                "top": "0",
                "width": kony.flex.USE_PREFFERED_SIZE
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var flxSearchContainer = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "35px",
                "id": "flxSearchContainer",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "isModalContainer": false,
                "right": "20dp",
                "skin": "sknflxd5d9ddop100",
                "top": "0dp",
                "width": "380px",
                "zIndex": 1
            }, {}, {});
            flxSearchContainer.setDefaultUnit(kony.flex.DP);
            var fontIconSearchImg = new kony.ui.Label({
                "centerY": "50%",
                "id": "fontIconSearchImg",
                "isVisible": true,
                "left": "12px",
                "skin": "sknFontIconSearchImg20px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.userwidgetmodel.lblSearchIcon\")",
                "top": "0dp",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var tbxSearchBox = new kony.ui.TextBox2({
                "autoCapitalize": constants.TEXTBOX_AUTO_CAPITALIZE_NONE,
                "centerY": "50%",
                "height": "30px",
                "id": "tbxSearchBox",
                "isVisible": true,
                "keyBoardStyle": constants.TEXTBOX_KEY_BOARD_STYLE_DEFAULT,
                "left": "30dp",
                "onTouchStart": controller.AS_TextField_cfab06859f0f43eca99495891ee290d5,
                "i18n_placeholder": "kony.i18n.getLocalizedString(\"i18n. frmCreateCustomer.Search_by_Role_Name\")",
                "right": "35px",
                "secureTextEntry": false,
                "skin": "skntbxffffffNoBorderlato35475f14px",
                "textInputMode": constants.TEXTBOX_INPUT_MODE_ANY,
                "zIndex": 1
            }, {
                "containerHeightMode": constants.TEXTBOX_FONT_METRICS_DRIVEN_HEIGHT,
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [2, 0, 0, 0],
                "paddingInPixel": false
            }, {
                "autoCorrect": false
            });
            var flxClearSearchImage = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "100%",
                "id": "flxClearSearchImage",
                "isVisible": false,
                "layoutType": kony.flex.FREE_FORM,
                "isModalContainer": false,
                "right": "0px",
                "skin": "sknCursor",
                "top": "0dp",
                "width": "35px",
                "zIndex": 1
            }, {}, {
                "hoverSkin": "sknCursor"
            });
            flxClearSearchImage.setDefaultUnit(kony.flex.DP);
            var fontIconCross = new kony.ui.Label({
                "centerY": "50%",
                "id": "fontIconCross",
                "isVisible": true,
                "left": "5px",
                "skin": "sknFontIconSearchCross16px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.userwidgetmodel.lblClearSearch\")",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxClearSearchImage.add(fontIconCross);
            flxSearchContainer.add(fontIconSearchImg, tbxSearchBox, flxClearSearchImage);
            flxAssignRoleHeader.add(lblRolesTitle, flxSearchContainer);
            var flxNoResultsFound = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "centerX": "50%",
                "clipBounds": true,
                "height": "20px",
                "id": "flxNoResultsFound",
                "isVisible": false,
                "layoutType": kony.flex.FLOW_HORIZONTAL,
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "50%",
                "width": "100%",
                "zIndex": 1
            }, {}, {});
            flxNoResultsFound.setDefaultUnit(kony.flex.DP);
            var lblNoResults = new kony.ui.Label({
                "centerX": "47%",
                "id": "lblNoResults",
                "isVisible": true,
                "left": "0dp",
                "skin": "slLabel0d20174dce8ea42",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmLogs.rtxNoResultsFound\")",
                "top": "0dp",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_CENTER,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxNoResultsFound.add(lblNoResults);
            var flxResults = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                "bottom": "0dp",
                "clipBounds": true,
                "id": "flxResults",
                "isVisible": true,
                "layoutType": kony.flex.FLOW_VERTICAL,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "60dp",
                "width": "100%"
            }, {}, {});
            flxResults.setDefaultUnit(kony.flex.DP);
            var flxRoleError = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "40dp",
                "id": "flxRoleError",
                "isVisible": false,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "20dp",
                "width": "100%"
            }, {}, {});
            flxRoleError.setDefaultUnit(kony.flex.DP);
            var flxRoleErrorContainer = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "40dp",
                "id": "flxRoleErrorContainer",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "20dp",
                "isModalContainer": false,
                "right": "20dp",
                "skin": "sknFlxBorder1pxe61919",
                "top": "0dp",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {}, {});
            flxRoleErrorContainer.setDefaultUnit(kony.flex.DP);
            var flxRoleErrorIconContainer = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "40dp",
                "id": "flxRoleErrorIconContainer",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "sknFlxBge61919",
                "top": "0dp",
                "width": "40dp",
                "zIndex": 1
            }, {}, {});
            flxRoleErrorIconContainer.setDefaultUnit(kony.flex.DP);
            var lblRoleErrorIcon = new kony.ui.Label({
                "centerX": "50%",
                "centerY": "50%",
                "height": "11px",
                "id": "lblRoleErrorIcon",
                "isVisible": true,
                "skin": "sknErrorWhiteIcon",
                "text": "",
                "width": "15px",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxRoleErrorIconContainer.add(lblRoleErrorIcon);
            var lblErrorValue = new kony.ui.Label({
                "centerY": "50%",
                "id": "lblErrorValue",
                "isVisible": true,
                "left": "50dp",
                "skin": "sknLbl485C75LatoRegular13Px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmCreateCustomer.select_a_role\")",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxRoleErrorContainer.add(flxRoleErrorIconContainer, lblErrorValue);
            flxRoleError.add(flxRoleErrorContainer);
            var flxScrollCustomerRoles = new kony.ui.FlexScrollContainer({
                "allowHorizontalBounce": false,
                "allowVerticalBounce": true,
                "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                "bottom": "0dp",
                "bounces": true,
                "clipBounds": true,
                "enableScrolling": true,
                "horizontalScrollIndicator": true,
                "id": "flxScrollCustomerRoles",
                "isVisible": true,
                "layoutType": kony.flex.FLOW_VERTICAL,
                "left": "0dp",
                "pagingEnabled": false,
                "scrollDirection": kony.flex.SCROLL_VERTICAL,
                "skin": "slFSbox",
                "top": "15dp",
                "verticalScrollIndicator": true,
                "width": "100%"
            }, {}, {});
            flxScrollCustomerRoles.setDefaultUnit(kony.flex.DP);
            var segCustomerRolesEdit = new kony.ui.SegmentedUI2({
                "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                "data": [{
                    "btnViewDetails": "kony.i18n.getLocalizedString(\"i18n.frmGroupsController.viewDetails\")",
                    "imgRoleCheckbox": "checkboxnormal.png",
                    "imgRoleRadio": "checkboxnormal.png",
                    "lblRoleDesc": "kony.i18n.getLocalizedString(\"i18n.frmAlertsManagement.LoremIpsumA\")",
                    "lblRoleName": "Bill Payments"
                }],
                "groupCells": false,
                "id": "segCustomerRolesEdit",
                "isVisible": true,
                "left": "0dp",
                "needPageIndicator": true,
                "pageOffDotImage": "pageoffdot.png",
                "pageOnDotImage": "pageondot.png",
                "retainSelection": false,
                "rowFocusSkin": "seg2Focus",
                "rowSkin": "seg2Normal",
                "rowTemplate": "flxCustomerProfileRoles",
                "sectionHeaderSkin": "sliPhoneSegmentHeader",
                "selectionBehavior": constants.SEGUI_SINGLE_SELECT_BEHAVIOR,
                "selectionBehaviorConfig": {
                    "imageIdentifier": "imgRoleCheckbox",
                    "selectedStateImage": "radio_selected.png",
                    "unselectedStateImage": "radio_notselected.png"
                },
                "separatorRequired": false,
                "showScrollbars": false,
                "top": "0dp",
                "viewType": constants.SEGUI_VIEW_TYPE_TABLEVIEW,
                "widgetDataMap": {
                    "btnViewDetails": "btnViewDetails",
                    "flxCustomerProfileRoles": "flxCustomerProfileRoles",
                    "flxRoleCheckbox": "flxRoleCheckbox",
                    "flxRoleInfo": "flxRoleInfo",
                    "flxRoleNameContainer": "flxRoleNameContainer",
                    "flxRoleRadio": "flxRoleRadio",
                    "imgRoleCheckbox": "imgRoleCheckbox",
                    "imgRoleRadio": "imgRoleRadio",
                    "lblRoleDesc": "lblRoleDesc",
                    "lblRoleName": "lblRoleName"
                },
                "width": "100%",
                "zIndex": 1
            }, {
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var flx20px = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "20dp",
                "id": "flx20px",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0dp",
                "width": "100%"
            }, {}, {});
            flx20px.setDefaultUnit(kony.flex.DP);
            flx20px.add();
            flxScrollCustomerRoles.add(segCustomerRolesEdit, flx20px);
            flxResults.add(flxRoleError, flxScrollCustomerRoles);
            flxAssignRolesContainer.add(flxAssignRoleHeader, flxNoResultsFound, flxResults);
            flxCustomerAssignRole.add(flxAssignRolesContainer);
            var flxCustomerAssignFeaturesAndActions = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "bottom": "0dp",
                "clipBounds": true,
                "id": "flxCustomerAssignFeaturesAndActions",
                "isVisible": false,
                "layoutType": kony.flex.FREE_FORM,
                "left": "230dp",
                "isModalContainer": false,
                "right": "0dp",
                "skin": "slFbox",
                "top": "0dp",
                "zIndex": 1
            }, {}, {});
            flxCustomerAssignFeaturesAndActions.setDefaultUnit(kony.flex.DP);
            var flxAssignFeaturesActionsError = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "40dp",
                "id": "flxAssignFeaturesActionsError",
                "isVisible": false,
                "layoutType": kony.flex.FREE_FORM,
                "left": "20dp",
                "isModalContainer": false,
                "right": "20dp",
                "skin": "sknFlxBorder1pxe61919",
                "top": "20dp",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {}, {});
            flxAssignFeaturesActionsError.setDefaultUnit(kony.flex.DP);
            var flxAssignFeaturesErrorIcon = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "40dp",
                "id": "flxAssignFeaturesErrorIcon",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "sknFlxBge61919",
                "top": "0dp",
                "width": "40dp",
                "zIndex": 1
            }, {}, {});
            flxAssignFeaturesErrorIcon.setDefaultUnit(kony.flex.DP);
            var lblAssignFeaturesActionsErrorIcon = new kony.ui.Label({
                "centerX": "50%",
                "centerY": "50%",
                "height": "11px",
                "id": "lblAssignFeaturesActionsErrorIcon",
                "isVisible": true,
                "skin": "sknErrorWhiteIcon",
                "text": "",
                "width": "15px",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxAssignFeaturesErrorIcon.add(lblAssignFeaturesActionsErrorIcon);
            var lblAssignFeaturesErrorMessage = new kony.ui.Label({
                "centerY": "50%",
                "id": "lblAssignFeaturesErrorMessage",
                "isVisible": true,
                "left": "50dp",
                "skin": "sknLbl485C75LatoRegular13Px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmMFAScenarios.select_feature\")",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxAssignFeaturesActionsError.add(flxAssignFeaturesErrorIcon, lblAssignFeaturesErrorMessage);
            var flxFeaturesActionsList = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "bottom": "80dp",
                "clipBounds": true,
                "id": "flxFeaturesActionsList",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0dp",
                "width": "100%",
                "zIndex": 1
            }, {}, {});
            flxFeaturesActionsList.setDefaultUnit(kony.flex.DP);
            var addFeaturesAndActions = new com.adminConsole.customerRoles.addFeaturesAndActions({
                "height": "100%",
                "id": "addFeaturesAndActions",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "masterType": constants.MASTER_TYPE_DEFAULT,
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0dp",
                "width": "100%",
                "overrides": {
                    "lblActionHeaderLine": {
                        "left": "0dp",
                        "right": "0dp"
                    },
                    "segSelectedOptions": {
                        "data": [{
                            "imgActionCheckbox": "checkboxnormal.png",
                            "lblActionName": "Label",
                            "lblCurrencySymbol1": "",
                            "lblCurrencySymbol2": "",
                            "lblCurrencySymbol3": "",
                            "lblCurrencySymbol4": "",
                            "lblIconActionArrow": "",
                            "lblLimitErrorIcon1": "",
                            "lblLimitErrorIcon2": "",
                            "lblLimitErrorIcon3": "",
                            "lblLimitErrorIcon4": "",
                            "lblLimitErrorMsg1": "kony.i18n.getLocalizedString(\"i18n.frmServiceManagement.lblNoServiceTypeError\")",
                            "lblLimitErrorMsg2": "kony.i18n.getLocalizedString(\"i18n.frmServiceManagement.lblNoServiceTypeError\")",
                            "lblLimitErrorMsg3": "kony.i18n.getLocalizedString(\"i18n.frmServiceManagement.lblNoServiceTypeError\")",
                            "lblLimitErrorMsg4": "kony.i18n.getLocalizedString(\"i18n.frmServiceManagement.lblNoServiceTypeError\")",
                            "lblMandatory": "Label",
                            "lblNoDesc": "Label",
                            "lblRowHeading1": "Label",
                            "lblRowHeading2": "Label",
                            "lblRowHeading3": "Label",
                            "lblRowHeading4": "Label",
                            "tbxLimitValue1": "",
                            "tbxLimitValue2": "",
                            "tbxLimitValue3": "",
                            "tbxLimitValue4": ""
                        }]
                    },
                    "tbxSearchAvailableItems": {
                        "i18n_placeholder": "kony.i18n.getLocalizedString(\"i18n.frmCreateCustomer.Search_By_Feature\")"
                    }
                }
            }, {
                "overrides": {}
            }, {
                "overrides": {}
            });
            flxFeaturesActionsList.add(addFeaturesAndActions);
            flxCustomerAssignFeaturesAndActions.add(flxAssignFeaturesActionsError, flxFeaturesActionsList);
            var flxCustomerAssignLimits = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "bottom": "0dp",
                "clipBounds": true,
                "id": "flxCustomerAssignLimits",
                "isVisible": false,
                "layoutType": kony.flex.FREE_FORM,
                "left": "230dp",
                "isModalContainer": false,
                "right": "0dp",
                "skin": "slFbox",
                "top": "0dp",
                "zIndex": 1
            }, {}, {});
            flxCustomerAssignLimits.setDefaultUnit(kony.flex.DP);
            var flxAssignLimitsContainer = new kony.ui.FlexScrollContainer({
                "allowHorizontalBounce": false,
                "allowVerticalBounce": true,
                "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                "bottom": "80dp",
                "bounces": true,
                "clipBounds": true,
                "enableScrolling": true,
                "horizontalScrollIndicator": true,
                "id": "flxAssignLimitsContainer",
                "isVisible": true,
                "layoutType": kony.flex.FLOW_VERTICAL,
                "left": "0dp",
                "pagingEnabled": false,
                "right": "0dp",
                "scrollDirection": kony.flex.SCROLL_VERTICAL,
                "skin": "slFSbox",
                "top": "0dp",
                "verticalScrollIndicator": true
            }, {}, {});
            flxAssignLimitsContainer.setDefaultUnit(kony.flex.DP);
            var flxNoLimitsFound = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "centerX": "50%",
                "clipBounds": true,
                "height": "20px",
                "id": "flxNoLimitsFound",
                "isVisible": false,
                "layoutType": kony.flex.FLOW_HORIZONTAL,
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "50%",
                "width": "100%",
                "zIndex": 1
            }, {}, {});
            flxNoLimitsFound.setDefaultUnit(kony.flex.DP);
            var lblNoLimitsFound = new kony.ui.Label({
                "centerX": "47%",
                "id": "lblNoLimitsFound",
                "isVisible": true,
                "left": "0dp",
                "skin": "slLabel0d20174dce8ea42",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18.frmCreateCustomer.message.no_limits_to_features\")",
                "top": "0dp",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_CENTER,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxNoLimitsFound.add(lblNoLimitsFound);
            var flxLimitsScroll = new kony.ui.FlexScrollContainer({
                "allowHorizontalBounce": false,
                "allowVerticalBounce": true,
                "bounces": true,
                "clipBounds": true,
                "enableScrolling": true,
                "height": "100%",
                "horizontalScrollIndicator": true,
                "id": "flxLimitsScroll",
                "isVisible": false,
                "layoutType": kony.flex.FLOW_VERTICAL,
                "left": "0dp",
                "pagingEnabled": false,
                "scrollDirection": kony.flex.SCROLL_VERTICAL,
                "skin": "slFSbox",
                "top": "0dp",
                "verticalScrollIndicator": true,
                "width": "100%"
            }, {}, {});
            flxLimitsScroll.setDefaultUnit(kony.flex.DP);
            var segCustomerAssignLimits = new kony.ui.SegmentedUI2({
                "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                "data": [
                    [{
                            "lblAutoDenyLimits": "kony.i18n.getLocalizedString(\"i8n.companies.autoDenyLimits_UC\")",
                            "lblDailyCurrencyADLimits": "$",
                            "lblDailyCurrencyPreLimit": "$",
                            "lblDailyTransactionLimitDollar": "$",
                            "lblDailyTransactionLimitValue": "20,000",
                            "lblDailyTransactionLimits": "kony.i18n.getLocalizedString(\"i18n.companies.dailyTransactionLimits_SC\")",
                            "lblErrorIcon11": "",
                            "lblErrorIcon12": "",
                            "lblErrorIcon21": "",
                            "lblErrorIcon22": "",
                            "lblErrorIcon31": "",
                            "lblErrorIcon32": "",
                            "lblErrorMsg11": "kony.i18n.getLocalizedString(\"i18n.decisionManagement.nameError\")",
                            "lblErrorMsg12": "kony.i18n.getLocalizedString(\"i18n.frmMFAConfigurations.Value_cannot_be_more_than\")",
                            "lblErrorMsg21": "kony.i18n.getLocalizedString(\"i18n.frmMFAConfigurations.Value_cannot_be_more_than\")",
                            "lblErrorMsg22": "kony.i18n.getLocalizedString(\"i18n.frmMFAConfigurations.Value_cannot_be_more_than\")",
                            "lblErrorMsg31": "kony.i18n.getLocalizedString(\"i18n.frmMFAConfigurations.Value_cannot_be_more_than\")",
                            "lblErrorMsg32": "kony.i18n.getLocalizedString(\"i18n.frmMFAConfigurations.Value_cannot_be_more_than\")",
                            "lblFeatureName": "Transfer to account in other FIs",
                            "lblPerCurrencyADLimits": "$",
                            "lblPerCurrencyPreLimit": "$",
                            "lblPerTransactionLimitDollar": "$",
                            "lblPerTransactionLimitValue": "$20,000",
                            "lblPreApprovedLimit": "kony.i18n.getLocalizedString(\"i18n.companies.preApprovedLimit_UC\")",
                            "lblSeperator": ".",
                            "lblTransactionLimits": "kony.i18n.getLocalizedString(\"i8n.companies.transactionLimits_UC\")",
                            "lblTransactionType": "kony.i18n.getLocalizedString(\"i18n.frmMFAScenarios.Transaction_Type_Caps\")",
                            "lblWeeklyCurrencyADLimits": "$",
                            "lblWeeklyCurrencyPreLimit": "$",
                            "lblWeeklyTransactionLimitDollar": "$",
                            "lblWeeklyTransactionLimitValue": "20,000",
                            "lblWeeklyTransactionLimits": "kony.i18n.getLocalizedString(\"i18n.companies.weeklyTransLimits_SC\")",
                            "lblperTransactionLimits": "kony.i18n.getLocalizedString(\"i18n.companies.perTransactionLimits_SC\")",
                            "statusIcon": "kony.i18n.getLocalizedString(\"i18n.frmAlertsManagement.lblViewAlertStatusKey\")",
                            "statusValue": "kony.i18n.getLocalizedString(\"i18n.secureimage.Active\")",
                            "txtDailyTransAutoDenyLimits": {
                                "placeholder": "",
                                "text": ""
                            },
                            "txtDailyTransPreApprovedLimit": {
                                "placeholder": "",
                                "text": ""
                            },
                            "txtPerTransAutoDenyLimits": {
                                "placeholder": "",
                                "text": ""
                            },
                            "txtPerTransPreApprovedLimit": {
                                "placeholder": "",
                                "text": ""
                            },
                            "txtWeeklyTransAutoDenyLimits": {
                                "placeholder": "",
                                "text": ""
                            },
                            "txtWeeklyTransPreApprovedLimit": {
                                "placeholder": "",
                                "text": ""
                            }
                        },
                        [{
                            "lblAutoDenyLimits": "kony.i18n.getLocalizedString(\"i8n.companies.autoDenyLimits_UC\")",
                            "lblDailyCurrencyADLimits": "$",
                            "lblDailyCurrencyPreLimit": "$",
                            "lblDailyTransactionLimitDollar": "$",
                            "lblDailyTransactionLimitValue": "20,000",
                            "lblDailyTransactionLimits": "kony.i18n.getLocalizedString(\"i18n.companies.dailyTransactionLimits_SC\")",
                            "lblErrorIcon11": "",
                            "lblErrorIcon12": "",
                            "lblErrorIcon21": "",
                            "lblErrorIcon22": "",
                            "lblErrorIcon31": "",
                            "lblErrorIcon32": "",
                            "lblErrorMsg11": "kony.i18n.getLocalizedString(\"i18n.decisionManagement.nameError\")",
                            "lblErrorMsg12": "kony.i18n.getLocalizedString(\"i18n.frmMFAConfigurations.Value_cannot_be_more_than\")",
                            "lblErrorMsg21": "kony.i18n.getLocalizedString(\"i18n.frmMFAConfigurations.Value_cannot_be_more_than\")",
                            "lblErrorMsg22": "kony.i18n.getLocalizedString(\"i18n.frmMFAConfigurations.Value_cannot_be_more_than\")",
                            "lblErrorMsg31": "kony.i18n.getLocalizedString(\"i18n.frmMFAConfigurations.Value_cannot_be_more_than\")",
                            "lblErrorMsg32": "kony.i18n.getLocalizedString(\"i18n.frmMFAConfigurations.Value_cannot_be_more_than\")",
                            "lblFeatureName": "Transfer to account in other FIs",
                            "lblPerCurrencyADLimits": "$",
                            "lblPerCurrencyPreLimit": "$",
                            "lblPerTransactionLimitDollar": "$",
                            "lblPerTransactionLimitValue": "$20,000",
                            "lblPreApprovedLimit": "kony.i18n.getLocalizedString(\"i18n.companies.preApprovedLimit_UC\")",
                            "lblSeperator": ".",
                            "lblTransactionLimits": "kony.i18n.getLocalizedString(\"i8n.companies.transactionLimits_UC\")",
                            "lblTransactionType": "kony.i18n.getLocalizedString(\"i18n.frmMFAScenarios.Transaction_Type_Caps\")",
                            "lblWeeklyCurrencyADLimits": "$",
                            "lblWeeklyCurrencyPreLimit": "$",
                            "lblWeeklyTransactionLimitDollar": "$",
                            "lblWeeklyTransactionLimitValue": "20,000",
                            "lblWeeklyTransactionLimits": "kony.i18n.getLocalizedString(\"i18n.companies.weeklyTransLimits_SC\")",
                            "lblperTransactionLimits": "kony.i18n.getLocalizedString(\"i18n.companies.perTransactionLimits_SC\")",
                            "statusIcon": "kony.i18n.getLocalizedString(\"i18n.frmAlertsManagement.lblViewAlertStatusKey\")",
                            "statusValue": "kony.i18n.getLocalizedString(\"i18n.secureimage.Active\")",
                            "txtDailyTransAutoDenyLimits": {
                                "placeholder": "",
                                "text": ""
                            },
                            "txtDailyTransPreApprovedLimit": {
                                "placeholder": "",
                                "text": ""
                            },
                            "txtPerTransAutoDenyLimits": {
                                "placeholder": "",
                                "text": ""
                            },
                            "txtPerTransPreApprovedLimit": {
                                "placeholder": "",
                                "text": ""
                            },
                            "txtWeeklyTransAutoDenyLimits": {
                                "placeholder": "",
                                "text": ""
                            },
                            "txtWeeklyTransPreApprovedLimit": {
                                "placeholder": "",
                                "text": ""
                            }
                        }]
                    ],
                    [{
                            "lblAutoDenyLimits": "kony.i18n.getLocalizedString(\"i8n.companies.autoDenyLimits_UC\")",
                            "lblDailyCurrencyADLimits": "$",
                            "lblDailyCurrencyPreLimit": "$",
                            "lblDailyTransactionLimitDollar": "$",
                            "lblDailyTransactionLimitValue": "20,000",
                            "lblDailyTransactionLimits": "kony.i18n.getLocalizedString(\"i18n.companies.dailyTransactionLimits_SC\")",
                            "lblErrorIcon11": "",
                            "lblErrorIcon12": "",
                            "lblErrorIcon21": "",
                            "lblErrorIcon22": "",
                            "lblErrorIcon31": "",
                            "lblErrorIcon32": "",
                            "lblErrorMsg11": "kony.i18n.getLocalizedString(\"i18n.decisionManagement.nameError\")",
                            "lblErrorMsg12": "kony.i18n.getLocalizedString(\"i18n.frmMFAConfigurations.Value_cannot_be_more_than\")",
                            "lblErrorMsg21": "kony.i18n.getLocalizedString(\"i18n.frmMFAConfigurations.Value_cannot_be_more_than\")",
                            "lblErrorMsg22": "kony.i18n.getLocalizedString(\"i18n.frmMFAConfigurations.Value_cannot_be_more_than\")",
                            "lblErrorMsg31": "kony.i18n.getLocalizedString(\"i18n.frmMFAConfigurations.Value_cannot_be_more_than\")",
                            "lblErrorMsg32": "kony.i18n.getLocalizedString(\"i18n.frmMFAConfigurations.Value_cannot_be_more_than\")",
                            "lblFeatureName": "Transfer to account in other FIs",
                            "lblPerCurrencyADLimits": "$",
                            "lblPerCurrencyPreLimit": "$",
                            "lblPerTransactionLimitDollar": "$",
                            "lblPerTransactionLimitValue": "$20,000",
                            "lblPreApprovedLimit": "kony.i18n.getLocalizedString(\"i18n.companies.preApprovedLimit_UC\")",
                            "lblSeperator": ".",
                            "lblTransactionLimits": "kony.i18n.getLocalizedString(\"i8n.companies.transactionLimits_UC\")",
                            "lblTransactionType": "kony.i18n.getLocalizedString(\"i18n.frmMFAScenarios.Transaction_Type_Caps\")",
                            "lblWeeklyCurrencyADLimits": "$",
                            "lblWeeklyCurrencyPreLimit": "$",
                            "lblWeeklyTransactionLimitDollar": "$",
                            "lblWeeklyTransactionLimitValue": "20,000",
                            "lblWeeklyTransactionLimits": "kony.i18n.getLocalizedString(\"i18n.companies.weeklyTransLimits_SC\")",
                            "lblperTransactionLimits": "kony.i18n.getLocalizedString(\"i18n.companies.perTransactionLimits_SC\")",
                            "statusIcon": "kony.i18n.getLocalizedString(\"i18n.frmAlertsManagement.lblViewAlertStatusKey\")",
                            "statusValue": "kony.i18n.getLocalizedString(\"i18n.secureimage.Active\")",
                            "txtDailyTransAutoDenyLimits": {
                                "placeholder": "",
                                "text": ""
                            },
                            "txtDailyTransPreApprovedLimit": {
                                "placeholder": "",
                                "text": ""
                            },
                            "txtPerTransAutoDenyLimits": {
                                "placeholder": "",
                                "text": ""
                            },
                            "txtPerTransPreApprovedLimit": {
                                "placeholder": "",
                                "text": ""
                            },
                            "txtWeeklyTransAutoDenyLimits": {
                                "placeholder": "",
                                "text": ""
                            },
                            "txtWeeklyTransPreApprovedLimit": {
                                "placeholder": "",
                                "text": ""
                            }
                        },
                        [{
                            "lblAutoDenyLimits": "kony.i18n.getLocalizedString(\"i8n.companies.autoDenyLimits_UC\")",
                            "lblDailyCurrencyADLimits": "$",
                            "lblDailyCurrencyPreLimit": "$",
                            "lblDailyTransactionLimitDollar": "$",
                            "lblDailyTransactionLimitValue": "20,000",
                            "lblDailyTransactionLimits": "kony.i18n.getLocalizedString(\"i18n.companies.dailyTransactionLimits_SC\")",
                            "lblErrorIcon11": "",
                            "lblErrorIcon12": "",
                            "lblErrorIcon21": "",
                            "lblErrorIcon22": "",
                            "lblErrorIcon31": "",
                            "lblErrorIcon32": "",
                            "lblErrorMsg11": "kony.i18n.getLocalizedString(\"i18n.decisionManagement.nameError\")",
                            "lblErrorMsg12": "kony.i18n.getLocalizedString(\"i18n.frmMFAConfigurations.Value_cannot_be_more_than\")",
                            "lblErrorMsg21": "kony.i18n.getLocalizedString(\"i18n.frmMFAConfigurations.Value_cannot_be_more_than\")",
                            "lblErrorMsg22": "kony.i18n.getLocalizedString(\"i18n.frmMFAConfigurations.Value_cannot_be_more_than\")",
                            "lblErrorMsg31": "kony.i18n.getLocalizedString(\"i18n.frmMFAConfigurations.Value_cannot_be_more_than\")",
                            "lblErrorMsg32": "kony.i18n.getLocalizedString(\"i18n.frmMFAConfigurations.Value_cannot_be_more_than\")",
                            "lblFeatureName": "Transfer to account in other FIs",
                            "lblPerCurrencyADLimits": "$",
                            "lblPerCurrencyPreLimit": "$",
                            "lblPerTransactionLimitDollar": "$",
                            "lblPerTransactionLimitValue": "$20,000",
                            "lblPreApprovedLimit": "kony.i18n.getLocalizedString(\"i18n.companies.preApprovedLimit_UC\")",
                            "lblSeperator": ".",
                            "lblTransactionLimits": "kony.i18n.getLocalizedString(\"i8n.companies.transactionLimits_UC\")",
                            "lblTransactionType": "kony.i18n.getLocalizedString(\"i18n.frmMFAScenarios.Transaction_Type_Caps\")",
                            "lblWeeklyCurrencyADLimits": "$",
                            "lblWeeklyCurrencyPreLimit": "$",
                            "lblWeeklyTransactionLimitDollar": "$",
                            "lblWeeklyTransactionLimitValue": "20,000",
                            "lblWeeklyTransactionLimits": "kony.i18n.getLocalizedString(\"i18n.companies.weeklyTransLimits_SC\")",
                            "lblperTransactionLimits": "kony.i18n.getLocalizedString(\"i18n.companies.perTransactionLimits_SC\")",
                            "statusIcon": "kony.i18n.getLocalizedString(\"i18n.frmAlertsManagement.lblViewAlertStatusKey\")",
                            "statusValue": "kony.i18n.getLocalizedString(\"i18n.secureimage.Active\")",
                            "txtDailyTransAutoDenyLimits": {
                                "placeholder": "",
                                "text": ""
                            },
                            "txtDailyTransPreApprovedLimit": {
                                "placeholder": "",
                                "text": ""
                            },
                            "txtPerTransAutoDenyLimits": {
                                "placeholder": "",
                                "text": ""
                            },
                            "txtPerTransPreApprovedLimit": {
                                "placeholder": "",
                                "text": ""
                            },
                            "txtWeeklyTransAutoDenyLimits": {
                                "placeholder": "",
                                "text": ""
                            },
                            "txtWeeklyTransPreApprovedLimit": {
                                "placeholder": "",
                                "text": ""
                            }
                        }]
                    ]
                ],
                "groupCells": false,
                "id": "segCustomerAssignLimits",
                "isVisible": true,
                "left": "0dp",
                "needPageIndicator": true,
                "pageOffDotImage": "pageoffdot.png",
                "pageOnDotImage": "pageondot.png",
                "retainSelection": false,
                "rowFocusSkin": "seg2Focus",
                "rowSkin": "seg2Normal",
                "rowTemplate": "flxAssignLimits",
                "sectionHeaderSkin": "sliPhoneSegmentHeader",
                "sectionHeaderTemplate": "flxAssignLimits",
                "selectionBehavior": constants.SEGUI_DEFAULT_BEHAVIOR,
                "separatorRequired": false,
                "showScrollbars": false,
                "top": "0dp",
                "viewType": constants.SEGUI_VIEW_TYPE_TABLEVIEW,
                "widgetDataMap": {
                    "flxAssignLimits": "flxAssignLimits",
                    "flxColumn11": "flxColumn11",
                    "flxColumn12": "flxColumn12",
                    "flxColumn21": "flxColumn21",
                    "flxColumn22": "flxColumn22",
                    "flxColumn31": "flxColumn31",
                    "flxColumn32": "flxColumn32",
                    "flxDailyCurrencyADLimits": "flxDailyCurrencyADLimits",
                    "flxDailyCurrencyPreLimit": "flxDailyCurrencyPreLimit",
                    "flxDailyTransactionLimit": "flxDailyTransactionLimit",
                    "flxErrorRow1": "flxErrorRow1",
                    "flxErrorRow2": "flxErrorRow2",
                    "flxErrorRow3": "flxErrorRow3",
                    "flxHeader": "flxHeader",
                    "flxLeft": "flxLeft",
                    "flxLimits": "flxLimits",
                    "flxLimitsContainer": "flxLimitsContainer",
                    "flxLimitsContent": "flxLimitsContent",
                    "flxLimitsHeader": "flxLimitsHeader",
                    "flxPerCurrencyADLimits": "flxPerCurrencyADLimits",
                    "flxPerCurrencyPreLimit": "flxPerCurrencyPreLimit",
                    "flxPerTransactionLimit": "flxPerTransactionLimit",
                    "flxRight": "flxRight",
                    "flxRow1": "flxRow1",
                    "flxRow2": "flxRow2",
                    "flxRow3": "flxRow3",
                    "flxWeeklyCurrencyADLimits": "flxWeeklyCurrencyADLimits",
                    "flxWeeklyCurrencyPreLimit": "flxWeeklyCurrencyPreLimit",
                    "flxWeeklyTransactionLimit": "flxWeeklyTransactionLimit",
                    "lblAutoDenyLimits": "lblAutoDenyLimits",
                    "lblDailyCurrencyADLimits": "lblDailyCurrencyADLimits",
                    "lblDailyCurrencyPreLimit": "lblDailyCurrencyPreLimit",
                    "lblDailyTransactionLimitDollar": "lblDailyTransactionLimitDollar",
                    "lblDailyTransactionLimitValue": "lblDailyTransactionLimitValue",
                    "lblDailyTransactionLimits": "lblDailyTransactionLimits",
                    "lblErrorIcon11": "lblErrorIcon11",
                    "lblErrorIcon12": "lblErrorIcon12",
                    "lblErrorIcon21": "lblErrorIcon21",
                    "lblErrorIcon22": "lblErrorIcon22",
                    "lblErrorIcon31": "lblErrorIcon31",
                    "lblErrorIcon32": "lblErrorIcon32",
                    "lblErrorMsg11": "lblErrorMsg11",
                    "lblErrorMsg12": "lblErrorMsg12",
                    "lblErrorMsg21": "lblErrorMsg21",
                    "lblErrorMsg22": "lblErrorMsg22",
                    "lblErrorMsg31": "lblErrorMsg31",
                    "lblErrorMsg32": "lblErrorMsg32",
                    "lblFeatureName": "lblFeatureName",
                    "lblPerCurrencyADLimits": "lblPerCurrencyADLimits",
                    "lblPerCurrencyPreLimit": "lblPerCurrencyPreLimit",
                    "lblPerTransactionLimitDollar": "lblPerTransactionLimitDollar",
                    "lblPerTransactionLimitValue": "lblPerTransactionLimitValue",
                    "lblPreApprovedLimit": "lblPreApprovedLimit",
                    "lblSeperator": "lblSeperator",
                    "lblTransactionLimits": "lblTransactionLimits",
                    "lblTransactionType": "lblTransactionType",
                    "lblWeeklyCurrencyADLimits": "lblWeeklyCurrencyADLimits",
                    "lblWeeklyCurrencyPreLimit": "lblWeeklyCurrencyPreLimit",
                    "lblWeeklyTransactionLimitDollar": "lblWeeklyTransactionLimitDollar",
                    "lblWeeklyTransactionLimitValue": "lblWeeklyTransactionLimitValue",
                    "lblWeeklyTransactionLimits": "lblWeeklyTransactionLimits",
                    "lblperTransactionLimits": "lblperTransactionLimits",
                    "statusIcon": "statusIcon",
                    "statusValue": "statusValue",
                    "txtDailyTransAutoDenyLimits": "txtDailyTransAutoDenyLimits",
                    "txtDailyTransPreApprovedLimit": "txtDailyTransPreApprovedLimit",
                    "txtPerTransAutoDenyLimits": "txtPerTransAutoDenyLimits",
                    "txtPerTransPreApprovedLimit": "txtPerTransPreApprovedLimit",
                    "txtWeeklyTransAutoDenyLimits": "txtWeeklyTransAutoDenyLimits",
                    "txtWeeklyTransPreApprovedLimit": "txtWeeklyTransPreApprovedLimit"
                },
                "width": "100%",
                "zIndex": 1
            }, {
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var flxLimits20px = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "20dp",
                "id": "flxLimits20px",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0dp",
                "width": "100%"
            }, {}, {});
            flxLimits20px.setDefaultUnit(kony.flex.DP);
            flxLimits20px.add();
            flxLimitsScroll.add(segCustomerAssignLimits, flxLimits20px);
            flxAssignLimitsContainer.add(flxNoLimitsFound, flxLimitsScroll);
            flxCustomerAssignLimits.add(flxAssignLimitsContainer);
            var flxDetailsButtons = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "bottom": "0dp",
                "clipBounds": true,
                "height": "80dp",
                "id": "flxDetailsButtons",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "230dp",
                "isModalContainer": false,
                "right": "0dp",
                "skin": "slFbox",
                "zIndex": 1
            }, {}, {});
            flxDetailsButtons.setDefaultUnit(kony.flex.DP);
            var commonButtons = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "80px",
                "id": "commonButtons",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0dp",
                "width": "100%",
                "zIndex": 1
            }, {}, {});
            commonButtons.setDefaultUnit(kony.flex.DP);
            var btnCancel = new kony.ui.Button({
                "centerY": "50%",
                "focusSkin": "sknbtnf7f7faLatoRegular4f555dBorder1px8b96a5",
                "height": "40dp",
                "id": "btnCancel",
                "isVisible": true,
                "left": "20dp",
                "skin": "sknbtnf7f7faLatoRegular4f555dBorder1px8b96a5",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.permission.CANCEL\")",
                "top": "0%",
                "width": "100px",
                "zIndex": 2
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_CENTER,
                "displayText": true,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {
                "hoverSkin": "sknbtnf7f7faLatoRegular4f555dBorder1px8b96a5"
            });
            var flxRightButtons = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "centerY": "50%",
                "clipBounds": true,
                "height": "100%",
                "id": "flxRightButtons",
                "isVisible": true,
                "layoutType": kony.flex.FLOW_HORIZONTAL,
                "reverseLayoutDirection": true,
                "isModalContainer": false,
                "right": "20dp",
                "skin": "slFbox",
                "width": "50%",
                "zIndex": 1
            }, {}, {});
            flxRightButtons.setDefaultUnit(kony.flex.DP);
            var btnCreate = new kony.ui.Button({
                "centerY": "50%",
                "focusSkin": "sknBtn003E75LatoRegular13pxFFFFFFRad20px",
                "height": "40dp",
                "id": "btnCreate",
                "isVisible": true,
                "right": "0px",
                "skin": "sknBtn003E75LatoRegular13pxFFFFFFRad20px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.permission.NEXT\")",
                "top": "0%",
                "width": "100dp",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_CENTER,
                "displayText": true,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {
                "hoverSkin": "sknBtn005198LatoRegular13pxFFFFFFRad20px"
            });
            var btnNext = new kony.ui.Button({
                "centerY": "50%",
                "focusSkin": "sknBtn4A77A0LatoRegular13pxFFFFFFRad20px",
                "height": "40dp",
                "id": "btnNext",
                "isVisible": true,
                "left": 0,
                "right": "20dp",
                "skin": "sknBtn4A77A0LatoRegular13pxFFFFFFRad20px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.permission.NEXT\")",
                "top": "0%",
                "width": "100dp",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_CENTER,
                "displayText": true,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {
                "hoverSkin": "sknBtn2D5982LatoRegular13pxFFFFFFRad20px"
            });
            flxRightButtons.add(btnCreate, btnNext);
            commonButtons.add(btnCancel, flxRightButtons);
            var flxDetailsButtonsSeperator = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "1dp",
                "id": "flxDetailsButtonsSeperator",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "isModalContainer": false,
                "right": "0dp",
                "skin": "sknflxd6dbe7",
                "top": "0dp",
                "zIndex": 1
            }, {}, {});
            flxDetailsButtonsSeperator.setDefaultUnit(kony.flex.DP);
            flxDetailsButtonsSeperator.add();
            flxDetailsButtons.add(commonButtons, flxDetailsButtonsSeperator);
            flxCustomerCreate.add(flxCustomerVerticalTabs, flxCustomerCentricSearch, flxSelectedCustomersAccountCentric, flxCustomerDetails, flxCustomerAccounts, flxCustomerAssignRole, flxCustomerAssignFeaturesAndActions, flxCustomerAssignLimits, flxDetailsButtons);
            var flxInlineErrorAccount = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "25dp",
                "id": "flxInlineErrorAccount",
                "isVisible": false,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0dp",
                "width": "100%",
                "zIndex": 1
            }, {}, {});
            flxInlineErrorAccount.setDefaultUnit(kony.flex.DP);
            var lblIconError = new kony.ui.Label({
                "centerX": "41%",
                "centerY": "52%",
                "id": "lblIconError",
                "isVisible": true,
                "skin": "sknErrorIcon",
                "text": "",
                "top": "0dp",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var lblErrorText = new kony.ui.Label({
                "centerX": "50%",
                "centerY": "50%",
                "id": "lblErrorText",
                "isVisible": true,
                "skin": "sknLabelRed",
                "text": "Please select an account",
                "top": "0dp",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxInlineErrorAccount.add(lblIconError, lblErrorText);
            flxCustomerContainer.add(flxCustomerCreate, flxInlineErrorAccount);
            flxMainContainer.add(flxCustomerContainer);
            flxRightPanel.add(flxMainHeader, flxBreadcrumb, flxMainContainer);
            var flxHeaderDropdown = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                "clipBounds": true,
                "id": "flxHeaderDropdown",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "isModalContainer": false,
                "right": "35dp",
                "skin": "slFbox",
                "top": "20dp",
                "width": "400dp",
                "zIndex": 5
            }, {}, {});
            flxHeaderDropdown.setDefaultUnit(kony.flex.DP);
            var dropdownMainHeader = new com.adminConsole.common.dropdownMainHeader({
                "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                "id": "dropdownMainHeader",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "masterType": constants.MASTER_TYPE_DEFAULT,
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0dp",
                "width": "100%",
                "overrides": {
                    "flxDropdown": {
                        "isVisible": false
                    },
                    "imgUpArrow": {
                        "src": "uparrow_2x.png"
                    }
                }
            }, {
                "overrides": {}
            }, {
                "overrides": {}
            });
            flxHeaderDropdown.add(dropdownMainHeader);
            var flxToastMessage = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "bottom": "0%",
                "clipBounds": true,
                "height": "8%",
                "id": "flxToastMessage",
                "isVisible": false,
                "layoutType": kony.flex.FREE_FORM,
                "left": "305px",
                "isModalContainer": false,
                "skin": "slFbox",
                "width": "78%",
                "zIndex": 1
            }, {}, {});
            flxToastMessage.setDefaultUnit(kony.flex.DP);
            var toastMessage = new com.adminConsole.common.toastMessage({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "id": "toastMessage",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "masterType": constants.MASTER_TYPE_DEFAULT,
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0dp",
                "overrides": {}
            }, {
                "overrides": {}
            }, {
                "overrides": {}
            });
            flxToastMessage.add(toastMessage);
            var flxLoading = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "100%",
                "id": "flxLoading",
                "isVisible": false,
                "layoutType": kony.flex.FREE_FORM,
                "left": "305px",
                "isModalContainer": false,
                "right": "0px",
                "skin": "sknLoadingBlur",
                "top": "0dp",
                "zIndex": 10
            }, {}, {});
            flxLoading.setDefaultUnit(kony.flex.DP);
            var flxImageContainer = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "centerX": "50%",
                "clipBounds": true,
                "height": "75px",
                "id": "flxImageContainer",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "250px",
                "width": "75px",
                "zIndex": 1
            }, {}, {});
            flxImageContainer.setDefaultUnit(kony.flex.DP);
            var imgLoading = new kony.ui.Image2({
                "centerX": "50%",
                "centerY": "50%",
                "height": "50px",
                "id": "imgLoading",
                "isVisible": true,
                "skin": "slImage",
                "src": "loadingscreenimage.gif",
                "width": "50px",
                "zIndex": 1
            }, {
                "imageScaleMode": constants.IMAGE_SCALE_MODE_MAINTAIN_ASPECT_RATIO,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxImageContainer.add(imgLoading);
            flxLoading.add(flxImageContainer);
            var flxResetActionsPopup = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "100%",
                "id": "flxResetActionsPopup",
                "isVisible": false,
                "layoutType": kony.flex.FREE_FORM,
                "masterType": constants.MASTER_TYPE_USERWIDGET,
                "isModalContainer": false,
                "skin": "slFbox",
                "width": "100%",
                "zIndex": 10
            }, {}, {});
            flxResetActionsPopup.setDefaultUnit(kony.flex.DP);
            var resetActionsPopup = new com.adminConsole.common.popUp1({
                "height": "100%",
                "id": "resetActionsPopup",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "masterType": constants.MASTER_TYPE_DEFAULT,
                "isModalContainer": false,
                "skin": "sknflxBg000000Op50",
                "top": "0dp",
                "width": "100%",
                "zIndex": 1,
                "overrides": {
                    "btnPopUpCancel": {
                        "i18n_text": "kony.i18n.getLocalizedString(\"i18n.PopUp.NoLeaveAsIS\")",
                        "right": "20px"
                    },
                    "btnPopUpDelete": {
                        "i18n_text": "kony.i18n.getLocalizedString(\"i18n.PopUp.YesProceed\")",
                        "minWidth": "viz.val_cleared"
                    },
                    "lblPopUpMainMessage": {
                        "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmGroupsController.ResetActions\")"
                    },
                    "rtxPopUpDisclaimer": {
                        "bottom": "30dp",
                        "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmGroupsController.ResetActionsPopupMessage\")",
                        "right": "viz.val_cleared",
                        "width": "93%"
                    }
                }
            }, {
                "overrides": {}
            }, {
                "overrides": {}
            });
            flxResetActionsPopup.add(resetActionsPopup);
            var flxViewRolePopup = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "100%",
                "id": "flxViewRolePopup",
                "isVisible": false,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "sknFocus50",
                "top": "0dp",
                "width": "100%",
                "zIndex": 6
            }, {}, {});
            flxViewRolePopup.setDefaultUnit(kony.flex.DP);
            var flxRoleDetailsContainer = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "centerX": "50%",
                "centerY": "50%",
                "clipBounds": true,
                "height": "540dp",
                "id": "flxRoleDetailsContainer",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "17%",
                "isModalContainer": false,
                "right": "17%",
                "skin": "slFbox0j9f841cc563e4e",
                "zIndex": 1
            }, {}, {});
            flxRoleDetailsContainer.setDefaultUnit(kony.flex.DP);
            var flxRolePopupTopColor = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "10px",
                "id": "flxRolePopupTopColor",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0px",
                "isModalContainer": false,
                "skin": "sknFlxInfoToastBg357C9ENoRadius",
                "top": "0dp",
                "width": "100%",
                "zIndex": 1
            }, {}, {});
            flxRolePopupTopColor.setDefaultUnit(kony.flex.DP);
            flxRolePopupTopColor.add();
            var flxRolePopupBody = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                "bottom": "0dp",
                "clipBounds": true,
                "id": "flxRolePopupBody",
                "isVisible": true,
                "layoutType": kony.flex.FLOW_VERTICAL,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "10px",
                "width": "100%",
                "zIndex": 1
            }, {}, {});
            flxRolePopupBody.setDefaultUnit(kony.flex.DP);
            var flxRoleClosePopup = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "25px",
                "id": "flxRoleClosePopup",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "isModalContainer": false,
                "right": "13dp",
                "skin": "sknCursor",
                "top": "10dp",
                "width": "25px",
                "zIndex": 20
            }, {}, {
                "hoverSkin": "sknCursor"
            });
            flxRoleClosePopup.setDefaultUnit(kony.flex.DP);
            var lblIconClosePopup = new kony.ui.Label({
                "centerX": "51%",
                "centerY": "50.00%",
                "height": "15dp",
                "id": "lblIconClosePopup",
                "isVisible": true,
                "left": "17dp",
                "skin": "sknFontIconSearchCross16px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmAlertsManagement.lblAlertPreviewClose\")",
                "width": "15dp",
                "zIndex": 2
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxRoleClosePopup.add(lblIconClosePopup);
            var flxRoleName = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                "clipBounds": true,
                "id": "flxRoleName",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "-5dp",
                "width": "100%"
            }, {}, {});
            flxRoleName.setDefaultUnit(kony.flex.DP);
            var lblFeatureName = new kony.ui.Label({
                "id": "lblFeatureName",
                "isVisible": true,
                "left": "20dp",
                "skin": "sknLbl192b45LatoReg16px",
                "text": "Bill Payment",
                "top": 0,
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var lblSeperator = new kony.ui.Label({
                "height": "1px",
                "id": "lblSeperator",
                "isVisible": true,
                "left": "20dp",
                "right": "20dp",
                "skin": "sknlblSeperator",
                "text": ".",
                "top": "40dp",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxRoleName.add(lblFeatureName, lblSeperator);
            var flxViewRoleWrapper = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                "bottom": "0dp",
                "clipBounds": true,
                "id": "flxViewRoleWrapper",
                "isVisible": true,
                "layoutType": kony.flex.FLOW_VERTICAL,
                "left": "0px",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "20px",
                "width": "100%",
                "zIndex": 1
            }, {}, {});
            flxViewRoleWrapper.setDefaultUnit(kony.flex.DP);
            var flxDescriptionViewRole = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                "clipBounds": true,
                "id": "flxDescriptionViewRole",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0dp",
                "width": "100%"
            }, {}, {});
            flxDescriptionViewRole.setDefaultUnit(kony.flex.DP);
            var lblViewRoleDescriptionHeading = new kony.ui.Label({
                "id": "lblViewRoleDescriptionHeading",
                "isVisible": true,
                "left": "20dp",
                "skin": "sknLbl78818A11px",
                "text": "DESCRIPTION",
                "top": "0dp",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var lblViewRoleDescriptionValue = new kony.ui.Label({
                "id": "lblViewRoleDescriptionValue",
                "isVisible": true,
                "left": "20dp",
                "right": "20dp",
                "skin": "sknLbl485C75LatoRegular13Px",
                "text": "This terms & conditions will be used in the footer link of retail banking",
                "top": "25dp",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxDescriptionViewRole.add(lblViewRoleDescriptionHeading, lblViewRoleDescriptionValue);
            var flxFeaturesList = new kony.ui.FlexScrollContainer({
                "allowHorizontalBounce": false,
                "allowVerticalBounce": true,
                "bottom": "0px",
                "bounces": true,
                "clipBounds": true,
                "enableScrolling": true,
                "height": "350dp",
                "horizontalScrollIndicator": true,
                "id": "flxFeaturesList",
                "isVisible": true,
                "layoutType": kony.flex.FLOW_VERTICAL,
                "left": "0dp",
                "pagingEnabled": false,
                "scrollDirection": kony.flex.SCROLL_VERTICAL,
                "skin": "slFSbox",
                "top": "0dp",
                "verticalScrollIndicator": true,
                "width": "100%"
            }, {}, {});
            flxFeaturesList.setDefaultUnit(kony.flex.DP);
            var ViewRoleFeaturesActions = new com.adminConsole.customerRoles.ViewRoleFeaturesActions({
                "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                "id": "ViewRoleFeaturesActions",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "masterType": constants.MASTER_TYPE_DEFAULT,
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0dp",
                "width": "100%",
                "overrides": {
                    "ViewRoleFeaturesActions": {
                        "left": "0dp",
                        "right": "viz.val_cleared",
                        "top": "0dp"
                    }
                }
            }, {
                "overrides": {}
            }, {
                "overrides": {}
            });
            flxFeaturesList.add(ViewRoleFeaturesActions);
            flxViewRoleWrapper.add(flxDescriptionViewRole, flxFeaturesList);
            flxRolePopupBody.add(flxRoleClosePopup, flxRoleName, flxViewRoleWrapper);
            flxRoleDetailsContainer.add(flxRolePopupTopColor, flxRolePopupBody);
            flxViewRolePopup.add(flxRoleDetailsContainer);
            var flxFeatureDetails = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "100%",
                "id": "flxFeatureDetails",
                "isVisible": false,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "sknFocus50",
                "top": "0dp",
                "width": "100%",
                "zIndex": 5
            }, {}, {});
            flxFeatureDetails.setDefaultUnit(kony.flex.DP);
            var flxFeatureDetailsPopUp = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "centerX": "50%",
                "centerY": "50%",
                "clipBounds": true,
                "height": "80%",
                "id": "flxFeatureDetailsPopUp",
                "isVisible": true,
                "layoutType": kony.flex.FLOW_VERTICAL,
                "isModalContainer": false,
                "skin": "slFbox0j9f841cc563e4e",
                "width": "70%",
                "zIndex": 1
            }, {}, {});
            flxFeatureDetailsPopUp.setDefaultUnit(kony.flex.DP);
            var flxBlueTop = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "10px",
                "id": "flxBlueTop",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0px",
                "isModalContainer": false,
                "skin": "sknFlxBg4A77A0",
                "top": "0dp",
                "width": "100%",
                "zIndex": 1
            }, {}, {});
            flxBlueTop.setDefaultUnit(kony.flex.DP);
            flxBlueTop.add();
            var flxFeatureDetailsContainer = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "98.50%",
                "id": "flxFeatureDetailsContainer",
                "isVisible": true,
                "layoutType": kony.flex.FLOW_VERTICAL,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0px",
                "width": "100%",
                "zIndex": 1
            }, {}, {});
            flxFeatureDetailsContainer.setDefaultUnit(kony.flex.DP);
            var flxFeatureDetailsClose = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "25px",
                "id": "flxFeatureDetailsClose",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "isModalContainer": false,
                "right": "13dp",
                "skin": "sknCursor",
                "top": "10dp",
                "width": "25px",
                "zIndex": 20
            }, {}, {
                "hoverSkin": "sknCursor"
            });
            flxFeatureDetailsClose.setDefaultUnit(kony.flex.DP);
            var lblFontIconClose = new kony.ui.Label({
                "centerX": "51%",
                "centerY": "50.00%",
                "height": "15dp",
                "id": "lblFontIconClose",
                "isVisible": true,
                "left": "17dp",
                "skin": "sknFontIconSearchCross16px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmAlertsManagement.lblAlertPreviewClose\")",
                "width": "15dp",
                "zIndex": 2
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxFeatureDetailsClose.add(lblFontIconClose);
            var flxFeatureDetailsHeader = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "20dp",
                "id": "flxFeatureDetailsHeader",
                "isVisible": true,
                "layoutType": kony.flex.FLOW_HORIZONTAL,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "-5dp",
                "width": "100%",
                "zIndex": 20
            }, {}, {});
            flxFeatureDetailsHeader.setDefaultUnit(kony.flex.DP);
            var lblFeatureDetailsHeader1 = new kony.ui.Label({
                "centerY": "50%",
                "id": "lblFeatureDetailsHeader1",
                "isVisible": true,
                "left": "20dp",
                "skin": "sknlblLatoBold35475f23px",
                "text": "Feature Details -",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var lblFeatureDetailsHeader2 = new kony.ui.Label({
                "centerY": "50%",
                "id": "lblFeatureDetailsHeader2",
                "isVisible": true,
                "left": "5dp",
                "skin": "sknlblLatoBold35475f23px",
                "text": "Bill Payments",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxFeatureDetailsHeader.add(lblFeatureDetailsHeader1, lblFeatureDetailsHeader2);
            var flxFeatureStatus = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                "clipBounds": true,
                "id": "flxFeatureStatus",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "20dp",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "10dp",
                "width": "31%",
                "zIndex": 1
            }, {}, {});
            flxFeatureStatus.setDefaultUnit(kony.flex.DP);
            var fontIconActive = new kony.ui.Label({
                "id": "fontIconActive",
                "isVisible": true,
                "left": "0dp",
                "skin": "sknFontIconActivate",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmAlertsManagement.lblViewAlertStatusKey\")",
                "top": "2dp",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var lblStatus = new kony.ui.Label({
                "id": "lblStatus",
                "isVisible": true,
                "left": "15px",
                "skin": "sknlblLato5bc06cBold14px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.secureimage.Active\")",
                "top": "0px",
                "width": "100%",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxFeatureStatus.add(fontIconActive, lblStatus);
            var lblSeparator = new kony.ui.Label({
                "bottom": "15px",
                "height": "1px",
                "id": "lblSeparator",
                "isVisible": true,
                "left": "20dp",
                "right": "20dp",
                "skin": "sknlblSeperator",
                "text": "-",
                "top": "15px",
                "zIndex": 2
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_TOP_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var flxFeatureDescription = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                "clipBounds": true,
                "id": "flxFeatureDescription",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "isModalContainer": false,
                "right": "50dp",
                "skin": "slFbox",
                "top": "5dp",
                "width": kony.flex.USE_PREFFERED_SIZE
            }, {}, {});
            flxFeatureDescription.setDefaultUnit(kony.flex.DP);
            var lblFeatureDescriptionHeading = new kony.ui.Label({
                "id": "lblFeatureDescriptionHeading",
                "isVisible": true,
                "left": "20dp",
                "skin": "sknLbl78818A11px",
                "text": "DESCRIPTION",
                "top": "0dp",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var lblFeatureDescriptionValue = new kony.ui.Label({
                "id": "lblFeatureDescriptionValue",
                "isVisible": true,
                "left": "20dp",
                "right": "20dp",
                "skin": "sknLatoSemibold485c7313px",
                "text": "This terms & conditions will be used in the footer link of retail banking",
                "top": "25dp",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxFeatureDescription.add(lblFeatureDescriptionHeading, lblFeatureDescriptionValue);
            var flxActionsList = new kony.ui.FlexScrollContainer({
                "allowHorizontalBounce": false,
                "allowVerticalBounce": true,
                "bounces": true,
                "clipBounds": true,
                "enableScrolling": true,
                "height": "70%",
                "horizontalScrollIndicator": true,
                "id": "flxActionsList",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "pagingEnabled": false,
                "right": "0dp",
                "scrollDirection": kony.flex.SCROLL_VERTICAL,
                "skin": "slFSbox",
                "top": "20dp",
                "verticalScrollIndicator": true
            }, {}, {});
            flxActionsList.setDefaultUnit(kony.flex.DP);
            var flxActionsHeader = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "45px",
                "id": "flxActionsHeader",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": 0,
                "isModalContainer": false,
                "right": "0dp",
                "skin": "sknflxffffffop100",
                "top": "0",
                "zIndex": 10
            }, {}, {});
            flxActionsHeader.setDefaultUnit(kony.flex.DP);
            var lblHeaderSeperator1 = new kony.ui.Label({
                "centerX": "50%",
                "height": "1px",
                "id": "lblHeaderSeperator1",
                "isVisible": true,
                "left": "20dp",
                "right": "20px",
                "skin": "sknlblSeperator",
                "text": "-",
                "top": "0dp",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var lblActionName = new kony.ui.Label({
                "centerY": "50%",
                "id": "lblActionName",
                "isVisible": true,
                "left": "40dp",
                "skin": "sknlblLato696c7312px",
                "text": "ACTION",
                "width": "90px",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {
                "hoverSkin": "sknlblLatoBold00000012px"
            });
            var lblDescriptionHeader = new kony.ui.Label({
                "centerY": "50%",
                "id": "lblDescriptionHeader",
                "isVisible": true,
                "left": "40%",
                "skin": "sknlblLato696c7312px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.View.DESCRIPTION\")",
                "top": 0,
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {
                "hoverSkin": "sknlblLatoBold00000012px"
            });
            var lblHeaderSeperator2 = new kony.ui.Label({
                "bottom": "1px",
                "centerX": "50%",
                "height": "1px",
                "id": "lblHeaderSeperator2",
                "isVisible": true,
                "left": "20dp",
                "right": "20px",
                "skin": "sknLblTableHeaderLine",
                "text": "-",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxActionsHeader.add(lblHeaderSeperator1, lblActionName, lblDescriptionHeader, lblHeaderSeperator2);
            var segActions = new kony.ui.SegmentedUI2({
                "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                "data": [{
                    "lblActionDescription": "Handle your business Payables  efficiently",
                    "lblActionName": "Mobile App",
                    "lblSeparator": "Label"
                }, {
                    "lblActionDescription": "Handle your business Payables  efficiently",
                    "lblActionName": "Mobile App",
                    "lblSeparator": "Label"
                }, {
                    "lblActionDescription": "Handle your business Payables  efficiently",
                    "lblActionName": "Mobile App",
                    "lblSeparator": "Label"
                }, {
                    "lblActionDescription": "Handle your business Payables  efficiently",
                    "lblActionName": "Mobile App",
                    "lblSeparator": "Label"
                }, {
                    "lblActionDescription": "Handle your business Payables  efficiently",
                    "lblActionName": "Mobile App",
                    "lblSeparator": "Label"
                }, {
                    "lblActionDescription": "Handle your business Payables  efficiently",
                    "lblActionName": "Mobile App",
                    "lblSeparator": "Label"
                }, {
                    "lblActionDescription": "Handle your business Payables  efficiently",
                    "lblActionName": "Mobile App",
                    "lblSeparator": "Label"
                }, {
                    "lblActionDescription": "Handle your business Payables  efficiently",
                    "lblActionName": "Mobile App",
                    "lblSeparator": "Label"
                }, {
                    "lblActionDescription": "Handle your business Payables  efficiently",
                    "lblActionName": "Mobile App",
                    "lblSeparator": "Label"
                }, {
                    "lblActionDescription": "Handle your business Payables  efficiently",
                    "lblActionName": "Mobile App",
                    "lblSeparator": "Label"
                }],
                "groupCells": false,
                "id": "segActions",
                "isVisible": true,
                "left": "0px",
                "needPageIndicator": true,
                "pageOffDotImage": "pageoffdot.png",
                "pageOnDotImage": "pageondot.png",
                "retainSelection": false,
                "right": "0dp",
                "rowFocusSkin": "seg2Focus",
                "rowSkin": "seg2Normal",
                "rowTemplate": "flxFeatureDetailsActions",
                "sectionHeaderSkin": "sliPhoneSegmentHeader",
                "selectionBehavior": constants.SEGUI_DEFAULT_BEHAVIOR,
                "separatorRequired": false,
                "showScrollbars": false,
                "top": "45dp",
                "viewType": constants.SEGUI_VIEW_TYPE_TABLEVIEW,
                "widgetDataMap": {
                    "flxFeatureDetailsActions": "flxFeatureDetailsActions",
                    "lblActionDescription": "lblActionDescription",
                    "lblActionName": "lblActionName",
                    "lblSeparator": "lblSeparator"
                },
                "zIndex": 10
            }, {
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var flxNoRecordsFound = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "200dp",
                "id": "flxNoRecordsFound",
                "isVisible": false,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0dp",
                "width": "100%",
                "zIndex": 1
            }, {}, {});
            flxNoRecordsFound.setDefaultUnit(kony.flex.DP);
            var rtxNoRecords = new kony.ui.RichText({
                "centerX": "50%",
                "centerY": "50%",
                "id": "rtxNoRecords",
                "isVisible": true,
                "skin": "sknRtxLato84939e12Px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.userwidgetmodel.rtxNoRecords\")",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxNoRecordsFound.add(rtxNoRecords);
            flxActionsList.add(flxActionsHeader, segActions, flxNoRecordsFound);
            flxFeatureDetailsContainer.add(flxFeatureDetailsClose, flxFeatureDetailsHeader, flxFeatureStatus, lblSeparator, flxFeatureDescription, flxActionsList);
            flxFeatureDetailsPopUp.add(flxBlueTop, flxFeatureDetailsContainer);
            flxFeatureDetails.add(flxFeatureDetailsPopUp);
            var flxPopUpConfirmation = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "100%",
                "id": "flxPopUpConfirmation",
                "isVisible": false,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "masterType": constants.MASTER_TYPE_USERWIDGET,
                "isModalContainer": false,
                "skin": "flxPopupTrans0ba858b5d5d304e",
                "top": "0dp",
                "width": "100%",
                "zIndex": 10
            }, {}, {});
            flxPopUpConfirmation.setDefaultUnit(kony.flex.DP);
            var popUpConfirmation = new com.adminConsole.common.popUp({
                "height": "100%",
                "id": "popUpConfirmation",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "masterType": constants.MASTER_TYPE_DEFAULT,
                "isModalContainer": false,
                "skin": "sknflxBg000000Op50",
                "top": "0dp",
                "width": "100%",
                "zIndex": 1,
                "overrides": {
                    "btnPopUpCancel": {
                        "i18n_text": "kony.i18n.getLocalizedString(\"i18n.PopUp.NoLeaveAsIS\")",
                        "minWidth": "80dp",
                        "right": "20px",
                        "width": kony.flex.USE_PREFFERED_SIZE
                    },
                    "btnPopUpDelete": {
                        "i18n_text": "kony.i18n.getLocalizedString(\"i18n.PopUp.YesProceed\")",
                        "width": kony.flex.USE_PREFFERED_SIZE
                    },
                    "lblPopUpMainMessage": {
                        "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmCustomerManagement.LinkProfiles\")"
                    },
                    "rtxPopUpDisclaimer": {
                        "maxWidth": "540px",
                        "minWidth": "viz.val_cleared",
                        "text": "Are you sure you want to link the existing Retail and the new Business profile for this user ?\nThe user can access the online banking services for combined access using the existing retail banking credentials."
                    }
                }
            }, {
                "overrides": {}
            }, {
                "overrides": {}
            });
            flxPopUpConfirmation.add(popUpConfirmation);
            flxMain.add(flxLinkProfilesPopup, flxLeftPanel, flxRightPanel, flxHeaderDropdown, flxToastMessage, flxLoading, flxResetActionsPopup, flxViewRolePopup, flxFeatureDetails, flxPopUpConfirmation);
            this.add(flxMain);
        };
        return [{
            "addWidgets": addWidgetsfrmCustomerCreate,
            "enabledForIdleTimeout": true,
            "id": "frmCustomerCreate",
            "layoutType": kony.flex.FREE_FORM,
            "needAppMenu": false,
            "preShow": function(eventobject) {
                controller.AS_Form_e2ab347545e548ea99359704703a939f(eventobject);
            },
            "skin": "slForm"
        }, {
            "displayOrientation": constants.FORM_DISPLAY_ORIENTATION_PORTRAIT,
            "layoutType": kony.flex.FREE_FORM,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, {
            "retainScrollPosition": false
        }]
    }
});