define("flxLeftMenuSectionHeader", function() {
    return function(controller) {
        var flxLeftMenuSectionHeader = new kony.ui.FlexContainer({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "clipBounds": true,
            "height": "26dp",
            "id": "flxLeftMenuSectionHeader",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "left": "0dp",
            "isModalContainer": false,
            "skin": "slFbox",
            "top": "0dp",
            "width": "100%"
        }, {}, {});
        flxLeftMenuSectionHeader.setDefaultUnit(kony.flex.DP);
        var lblMenuSectionHeader = new kony.ui.Label({
            "id": "lblMenuSectionHeader",
            "isVisible": true,
            "left": "20dp",
            "skin": "sknLeftMenuHeaderLabel",
            "text": "Section Heading",
            "top": "0dp",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, {
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, {});
        flxLeftMenuSectionHeader.add(lblMenuSectionHeader);
        return flxLeftMenuSectionHeader;
    }
})