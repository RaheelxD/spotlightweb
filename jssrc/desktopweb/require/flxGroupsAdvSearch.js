define("flxGroupsAdvSearch", function() {
    return function(controller) {
        var flxGroupsAdvSearch = new kony.ui.FlexContainer({
            "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
            "clipBounds": true,
            "id": "flxGroupsAdvSearch",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "isModalContainer": false,
            "skin": "sknflxffffffop100"
        }, {}, {
            "hoverSkin": "sknfbfcfc"
        });
        flxGroupsAdvSearch.setDefaultUnit(kony.flex.DP);
        var flxGroupSearch = new kony.ui.FlexContainer({
            "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
            "bottom": "10dp",
            "clipBounds": true,
            "id": "flxGroupSearch",
            "isVisible": true,
            "layoutType": kony.flex.FLOW_HORIZONTAL,
            "left": "20dp",
            "isModalContainer": false,
            "right": "20dp",
            "skin": "sknflxffffffop100",
            "top": "0"
        }, {}, {
            "hoverSkin": "sknfbfcfc"
        });
        flxGroupSearch.setDefaultUnit(kony.flex.DP);
        var flxCheckbox = new kony.ui.FlexContainer({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "clipBounds": true,
            "height": "15px",
            "id": "flxCheckbox",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "left": "11px",
            "isModalContainer": false,
            "onClick": controller.AS_FlexContainer_ebb7c15cd55444cd84969056cc624513,
            "right": "10px",
            "skin": "sknCursor",
            "top": "15px",
            "width": "15px",
            "zIndex": 2
        }, {}, {});
        flxCheckbox.setDefaultUnit(kony.flex.DP);
        var imgCheckBox = new kony.ui.Image2({
            "centerX": "50%",
            "centerY": "50%",
            "height": "12px",
            "id": "imgCheckBox",
            "isVisible": true,
            "skin": "slImage",
            "src": "checkbox.png",
            "width": "12px",
            "zIndex": 2
        }, {
            "imageScaleMode": constants.IMAGE_SCALE_MODE_MAINTAIN_ASPECT_RATIO,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, {});
        flxCheckbox.add(imgCheckBox);
        var flxCheckboxHidden = new kony.ui.FlexContainer({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "clipBounds": true,
            "height": "15px",
            "id": "flxCheckboxHidden",
            "isVisible": false,
            "layoutType": kony.flex.FREE_FORM,
            "left": "11px",
            "isModalContainer": false,
            "onClick": controller.AS_FlexContainer_ebb7c15cd55444cd84969056cc624513,
            "right": "10px",
            "skin": "slFbox",
            "top": "15px",
            "width": "15px",
            "zIndex": 4
        }, {}, {});
        flxCheckboxHidden.setDefaultUnit(kony.flex.DP);
        var imgCheckBoxHidden = new kony.ui.Image2({
            "centerX": "50%",
            "centerY": "50%",
            "height": "12px",
            "id": "imgCheckBoxHidden",
            "isVisible": false,
            "skin": "slImage",
            "src": "checkboxdisable.png",
            "width": "12px",
            "zIndex": 1
        }, {
            "imageScaleMode": constants.IMAGE_SCALE_MODE_MAINTAIN_ASPECT_RATIO,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, {});
        flxCheckboxHidden.add(imgCheckBoxHidden);
        var lblName = new kony.ui.Label({
            "id": "lblName",
            "isVisible": true,
            "skin": "sknlblLatoBold35475f14px",
            "text": "Name",
            "top": "15dp",
            "width": "17.25%",
            "zIndex": 1
        }, {
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, {});
        var lblUserName = new kony.ui.Label({
            "id": "lblUserName",
            "isVisible": true,
            "left": "0dp",
            "skin": "sknlblLatoBold35475f14px",
            "text": "User name",
            "top": "15dp",
            "width": "15%",
            "zIndex": 1
        }, {
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, {});
        var lblCustID = new kony.ui.Label({
            "id": "lblCustID",
            "isVisible": true,
            "left": "0dp",
            "skin": "sknlblLatoBold35475f14px",
            "text": "Customer ID",
            "top": "15dp",
            "width": "14%",
            "zIndex": 1
        }, {
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, {});
        var lblBranch = new kony.ui.Label({
            "id": "lblBranch",
            "isVisible": true,
            "left": "0dp",
            "skin": "sknlblLatoBold35475f14px",
            "text": "Branch",
            "top": "15dp",
            "width": "18%",
            "zIndex": 1
        }, {
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, {});
        var lblCity = new kony.ui.Label({
            "id": "lblCity",
            "isVisible": true,
            "left": "0dp",
            "skin": "sknlblLatoBold35475f14px",
            "text": "City",
            "top": "15dp",
            "width": "14%",
            "zIndex": 1
        }, {
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, {});
        var flxStatus = new kony.ui.FlexContainer({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "clipBounds": true,
            "height": "20px",
            "id": "flxStatus",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "left": "0%",
            "isModalContainer": false,
            "skin": "slFbox",
            "top": "13dp",
            "width": "10%",
            "zIndex": 1
        }, {}, {});
        flxStatus.setDefaultUnit(kony.flex.DP);
        var lblServicesStatus = new kony.ui.Label({
            "centerY": "50%",
            "id": "lblServicesStatus",
            "isVisible": true,
            "left": "20px",
            "skin": "sknlblLato5bc06cBold14px",
            "text": "Suspended",
            "width": "70px",
            "zIndex": 1
        }, {
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, {});
        var fontIconCustomerStatus = new kony.ui.Label({
            "centerY": "50%",
            "id": "fontIconCustomerStatus",
            "isVisible": true,
            "left": "0dp",
            "skin": "sknFontIconActivate",
            "text": "",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, {
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, {});
        flxStatus.add(lblServicesStatus, fontIconCustomerStatus);
        var btnAdd = new kony.ui.Button({
            "id": "btnAdd",
            "isVisible": true,
            "left": "10dp",
            "onClick": controller.AS_Button_c220007c64924529b75444af89c9e5e3,
            "skin": "sknBtnLatoRegular11abeb14px",
            "text": "Add",
            "top": "15dp",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, {
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_RIGHT,
            "displayText": true,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, {
            "hoverSkin": "sknBtnLatoRegular11abeb14px"
        });
        var lblAdded = new kony.ui.Label({
            "id": "lblAdded",
            "isVisible": false,
            "left": "10dp",
            "skin": "sknlblLatoBold35475f14px",
            "text": "Added",
            "top": "15dp",
            "width": "14%",
            "zIndex": 1
        }, {
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, {});
        flxGroupSearch.add(flxCheckbox, flxCheckboxHidden, lblName, lblUserName, lblCustID, lblBranch, lblCity, flxStatus, btnAdd, lblAdded);
        var lblSeperator = new kony.ui.Label({
            "bottom": 0,
            "height": "1px",
            "id": "lblSeperator",
            "isVisible": true,
            "left": "0dp",
            "right": "0dp",
            "skin": "sknlblSeperator",
            "text": ".",
            "zIndex": 1
        }, {
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, {});
        flxGroupsAdvSearch.add(flxGroupSearch, lblSeperator);
        return flxGroupsAdvSearch;
    }
})