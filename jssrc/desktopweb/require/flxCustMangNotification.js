define("flxCustMangNotification", function() {
    return function(controller) {
        var flxCustMangNotification = new kony.ui.FlexContainer({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "clipBounds": true,
            "height": "50px",
            "id": "flxCustMangNotification",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "isModalContainer": false,
            "onClick": controller.AS_FlexContainer_e553ee3b13fe4973aef70f31c7776c5d,
            "skin": "slFbox"
        }, {}, {
            "hoverSkin": "sknfbfcfc"
        });
        flxCustMangNotification.setDefaultUnit(kony.flex.DP);
        var flxFirstColoum = new kony.ui.FlexContainer({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "clipBounds": true,
            "height": "50dp",
            "id": "flxFirstColoum",
            "isVisible": true,
            "layoutType": kony.flex.FLOW_HORIZONTAL,
            "left": "0dp",
            "isModalContainer": false,
            "skin": "slFbox",
            "top": "0dp",
            "width": "46%",
            "zIndex": 2
        }, {}, {});
        flxFirstColoum.setDefaultUnit(kony.flex.DP);
        var flxArrow = new kony.ui.FlexContainer({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "centerY": "50%",
            "clipBounds": true,
            "height": "25px",
            "id": "flxArrow",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "left": "35px",
            "isModalContainer": false,
            "onClick": controller.AS_FlexContainer_ec8edea2bd1f48aa8bbc24f8f9f22b05,
            "skin": "sknCursor",
            "width": "25px",
            "zIndex": 2
        }, {}, {});
        flxArrow.setDefaultUnit(kony.flex.DP);
        var ImgArrow = new kony.ui.Image2({
            "centerX": "50%",
            "centerY": "50%",
            "height": "15px",
            "id": "ImgArrow",
            "isVisible": false,
            "left": "0%",
            "skin": "slImage",
            "src": "img_desc_arrow.png",
            "width": "15px",
            "zIndex": 2
        }, {
            "imageScaleMode": constants.IMAGE_SCALE_MODE_MAINTAIN_ASPECT_RATIO,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, {});
        var fonticonArrow = new kony.ui.Label({
            "centerX": "50%",
            "centerY": "50%",
            "height": "12px",
            "id": "fonticonArrow",
            "isVisible": true,
            "skin": "sknfontIconDescRightArrow14px",
            "text": "",
            "width": "12px",
            "zIndex": 1
        }, {
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, {});
        flxArrow.add(ImgArrow, fonticonArrow);
        var lblName = new kony.ui.Label({
            "centerY": "50%",
            "id": "lblName",
            "isVisible": true,
            "left": "10px",
            "skin": "sknlblLatoBold35475f14px",
            "text": "Summer Discount",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, {
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, {});
        flxFirstColoum.add(flxArrow, lblName);
        var lblStartDate = new kony.ui.Label({
            "centerY": "50%",
            "id": "lblStartDate",
            "isVisible": true,
            "left": "46%",
            "skin": "sknlblLatoBold35475f14px",
            "text": "12/12/2017",
            "width": "15%",
            "zIndex": 1
        }, {
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, {});
        var lblExpDate = new kony.ui.Label({
            "centerY": "50%",
            "id": "lblExpDate",
            "isVisible": true,
            "left": "61%",
            "skin": "sknlblLatoBold35475f14px",
            "text": "01/21/2018",
            "width": "15%",
            "zIndex": 1
        }, {
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, {});
        var lblStatus = new kony.ui.Label({
            "centerY": "50%",
            "id": "lblStatus",
            "isVisible": true,
            "left": "76%",
            "right": "55px",
            "skin": "sknlblLatoBold35475f14px",
            "text": "Starting soon",
            "zIndex": 1
        }, {
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, {});
        var lblSeperator = new kony.ui.Label({
            "bottom": 0,
            "height": "1px",
            "id": "lblSeperator",
            "isVisible": true,
            "left": "35px",
            "right": "35px",
            "skin": "sknlblSeperator",
            "text": ".",
            "zIndex": 1
        }, {
            "contentAlignment": constants.CONTENT_ALIGN_TOP_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, {});
        flxCustMangNotification.add(flxFirstColoum, lblStartDate, lblExpDate, lblStatus, lblSeperator);
        return flxCustMangNotification;
    }
})