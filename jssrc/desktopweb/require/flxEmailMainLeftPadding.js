define("flxEmailMainLeftPadding", function() {
    return function(controller) {
        var flxEmailMainLeftPadding = new kony.ui.FlexContainer({
            "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
            "clipBounds": true,
            "id": "flxEmailMainLeftPadding",
            "isVisible": true,
            "layoutType": kony.flex.FLOW_VERTICAL,
            "isModalContainer": false,
            "skin": "sknfbfcfc"
        }, {}, {});
        flxEmailMainLeftPadding.setDefaultUnit(kony.flex.DP);
        var flxMessage = new kony.ui.FlexContainer({
            "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
            "clipBounds": true,
            "id": "flxMessage",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "left": "0dp",
            "isModalContainer": false,
            "right": "0dp",
            "skin": "sknflxffffffop0a",
            "top": "0dp",
            "zIndex": 1
        }, {}, {});
        flxMessage.setDefaultUnit(kony.flex.DP);
        var flxMailAdr = new kony.ui.FlexContainer({
            "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
            "clipBounds": true,
            "id": "flxMailAdr",
            "isVisible": true,
            "layoutType": kony.flex.FLOW_HORIZONTAL,
            "left": "25dp",
            "isModalContainer": false,
            "right": "100px",
            "skin": "slFbox",
            "top": "20dp",
            "zIndex": 1
        }, {}, {});
        flxMailAdr.setDefaultUnit(kony.flex.DP);
        var lblEmailName = new kony.ui.Label({
            "id": "lblEmailName",
            "isVisible": true,
            "left": "0dp",
            "skin": "sknlblLatoBold35475f14px",
            "text": "Nicky Paretla",
            "top": "0dp",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, {
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, {});
        var btnMail = new kony.ui.Button({
            "id": "btnMail",
            "isVisible": false,
            "left": "20px",
            "skin": "sknBtnLato0b6689b17f1bf4f12px",
            "text": "< johndoe23@gmail.com> ",
            "top": "0dp",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, {
            "contentAlignment": constants.CONTENT_ALIGN_CENTER,
            "displayText": true,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, {});
        var lblEmailDate = new kony.ui.Label({
            "id": "lblEmailDate",
            "isVisible": true,
            "left": "30dp",
            "skin": "sknlblLato5d6c7f12px",
            "text": "12 December 2017 09:30 a.m.",
            "top": "0dp",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, {
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, {});
        flxMailAdr.add(lblEmailName, btnMail, lblEmailDate);
        var btnDraftMain = new kony.ui.Button({
            "height": "22px",
            "id": "btnDraftMain",
            "isVisible": true,
            "right": "25px",
            "skin": "sknBtnLatoee6565DraftNew",
            "text": "Draft",
            "top": "20px",
            "width": "50px",
            "zIndex": 10
        }, {
            "contentAlignment": constants.CONTENT_ALIGN_CENTER,
            "displayText": true,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, {});
        var rtxMail = new kony.ui.RichText({
            "bottom": "30px",
            "id": "rtxMail",
            "isVisible": true,
            "left": "25dp",
            "right": "35dp",
            "skin": "sknrtxLato485c7514px",
            "text": "Hi Team,<br>\n\n<br>\n\nLorem ipsum dolor sit amet, consectetur adipiscing elit. Maecenas sit amet turpis purus. Sed consectetur quam non est lacinia blandit. Nullam condimentum turpis metus, sed egestas ante sagittis ut. Nam bibendum lorem turpis, non venenatis dui imperdiet at. Donec quis dictum arcu. Nam varius est nec mattis sodales. Suspendisse ac leo sed dui interdum volutpat. Aliquam mollis ipsum ac purus vestibulum, vel viverra metus finibus. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Phasellus vel lacus et quam consectetur accumsan.\n\n<br>\n<br>\nJohnathan Doe",
            "top": "50dp",
            "zIndex": 1
        }, {
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [2, 0, 0, 0],
            "paddingInPixel": false
        }, {});
        var lblSeperator1 = new kony.ui.Label({
            "bottom": 0,
            "height": "1px",
            "id": "lblSeperator1",
            "isVisible": true,
            "left": "35px",
            "right": "35px",
            "skin": "sknSeparator",
            "zIndex": 1
        }, {
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, {});
        flxMessage.add(flxMailAdr, btnDraftMain, rtxMail, lblSeperator1);
        var flxAttatchments = new kony.ui.FlexContainer({
            "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
            "clipBounds": true,
            "id": "flxAttatchments",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "left": "0dp",
            "isModalContainer": false,
            "right": "0px",
            "skin": "sknflxffffffop0a",
            "top": "0dp",
            "width": "100%",
            "zIndex": 1
        }, {}, {});
        flxAttatchments.setDefaultUnit(kony.flex.DP);
        var lblAttatchments = new kony.ui.Label({
            "id": "lblAttatchments",
            "isVisible": true,
            "left": "25dp",
            "skin": "sknlblLatoBold35475f14px",
            "text": "1 Attatchment",
            "top": "20px",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, {
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, {});
        var flxAttachmnetWrapper = new kony.ui.FlexContainer({
            "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
            "clipBounds": true,
            "id": "flxAttachmnetWrapper",
            "isVisible": true,
            "layoutType": kony.flex.FLOW_HORIZONTAL,
            "left": "2dp",
            "isModalContainer": false,
            "skin": "slFbox",
            "top": "38dp",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, {}, {});
        flxAttachmnetWrapper.setDefaultUnit(kony.flex.DP);
        var flxAttachment = new kony.ui.FlexContainer({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "bottom": "30px",
            "clipBounds": true,
            "height": "40px",
            "id": "flxAttachment",
            "isVisible": true,
            "layoutType": kony.flex.FLOW_HORIZONTAL,
            "left": "25dp",
            "isModalContainer": false,
            "skin": "skne15edop100",
            "top": "5px",
            "width": "180px",
            "zIndex": 1
        }, {}, {});
        flxAttachment.setDefaultUnit(kony.flex.DP);
        var flxAttatcmentImage = new kony.ui.FlexContainer({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "centerY": "50%",
            "clipBounds": true,
            "height": "40dp",
            "id": "flxAttatcmentImage",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "isModalContainer": false,
            "skin": "slFbox",
            "top": "20dp",
            "width": "40dp",
            "zIndex": 1
        }, {}, {});
        flxAttatcmentImage.setDefaultUnit(kony.flex.DP);
        var lblIconDownload = new kony.ui.Label({
            "centerX": "50%",
            "centerY": "50%",
            "height": "25dp",
            "id": "lblIconDownload",
            "isVisible": true,
            "skin": "sknIconDownload24pxBlue",
            "text": "",
            "width": "24dp",
            "zIndex": 1
        }, {
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, {});
        var imgDownload = new kony.ui.Image2({
            "centerX": "50%",
            "centerY": "50%",
            "height": "15dp",
            "id": "imgDownload",
            "isVisible": false,
            "skin": "slImage",
            "src": "download_blue_2x.png",
            "width": "15dp",
            "zIndex": 1
        }, {
            "imageScaleMode": constants.IMAGE_SCALE_MODE_MAINTAIN_ASPECT_RATIO,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, {});
        flxAttatcmentImage.add(lblIconDownload, imgDownload);
        var flxFlagSeperator = new kony.ui.FlexContainer({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "centerY": "50%",
            "clipBounds": true,
            "height": "40px",
            "id": "flxFlagSeperator",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "left": "0px",
            "isModalContainer": false,
            "skin": "sknflxOuterBorderdfd9dd",
            "top": "0%",
            "width": "1px",
            "zIndex": 111
        }, {}, {});
        flxFlagSeperator.setDefaultUnit(kony.flex.DP);
        flxFlagSeperator.add();
        var flxAttachments = new kony.ui.FlexContainer({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "centerY": "50%",
            "clipBounds": true,
            "height": "40dp",
            "id": "flxAttachments",
            "isVisible": true,
            "layoutType": kony.flex.FLOW_VERTICAL,
            "isModalContainer": false,
            "skin": "slFbox",
            "width": "140px",
            "zIndex": 1
        }, {}, {});
        flxAttachments.setDefaultUnit(kony.flex.DP);
        var lblAttatchmentName = new kony.ui.Label({
            "id": "lblAttatchmentName",
            "isVisible": true,
            "left": "10dp",
            "skin": "sknlblLatoBold35475f14px",
            "text": "Attatchment name.zip ",
            "top": "5dp",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, {
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, {});
        var lblAttatchmentSize = new kony.ui.Label({
            "id": "lblAttatchmentSize",
            "isVisible": true,
            "left": "10dp",
            "skin": "sknlblLatoBold35475f14px",
            "text": "1.5 MB",
            "top": "0dp",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, {
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, {});
        flxAttachments.add(lblAttatchmentName, lblAttatchmentSize);
        flxAttachment.add(flxAttatcmentImage, flxFlagSeperator, flxAttachments);
        var flxAttachment2 = new kony.ui.FlexContainer({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "bottom": "30px",
            "clipBounds": true,
            "height": "40px",
            "id": "flxAttachment2",
            "isVisible": true,
            "layoutType": kony.flex.FLOW_HORIZONTAL,
            "left": "10px",
            "isModalContainer": false,
            "skin": "skne15edop100",
            "top": "5px",
            "width": "180px",
            "zIndex": 1
        }, {}, {});
        flxAttachment2.setDefaultUnit(kony.flex.DP);
        var flxAttatcmentImage2 = new kony.ui.FlexContainer({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "centerY": "50%",
            "clipBounds": true,
            "height": "40dp",
            "id": "flxAttatcmentImage2",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "left": "0",
            "isModalContainer": false,
            "skin": "slFbox",
            "top": "20dp",
            "width": "40dp",
            "zIndex": 1
        }, {}, {});
        flxAttatcmentImage2.setDefaultUnit(kony.flex.DP);
        var imgDownload2 = new kony.ui.Image2({
            "centerX": "50%",
            "centerY": "50%",
            "height": "15dp",
            "id": "imgDownload2",
            "isVisible": false,
            "skin": "slImage",
            "src": "download_blue_2x.png",
            "width": "15dp",
            "zIndex": 1
        }, {
            "imageScaleMode": constants.IMAGE_SCALE_MODE_MAINTAIN_ASPECT_RATIO,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, {});
        var lblIconDownload2 = new kony.ui.Label({
            "centerX": "50%",
            "centerY": "50%",
            "height": "25dp",
            "id": "lblIconDownload2",
            "isVisible": true,
            "skin": "sknIconDownload24pxBlue",
            "text": "",
            "width": "24dp",
            "zIndex": 1
        }, {
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, {});
        flxAttatcmentImage2.add(imgDownload2, lblIconDownload2);
        var flxFlagSeperator2 = new kony.ui.FlexContainer({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "centerY": "50%",
            "clipBounds": true,
            "height": "40px",
            "id": "flxFlagSeperator2",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "left": "0px",
            "isModalContainer": false,
            "skin": "sknflxOuterBorderdfd9dd",
            "top": "0%",
            "width": "1px",
            "zIndex": 111
        }, {}, {});
        flxFlagSeperator2.setDefaultUnit(kony.flex.DP);
        flxFlagSeperator2.add();
        var flxAttachments2 = new kony.ui.FlexContainer({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "centerY": "50%",
            "clipBounds": true,
            "height": "40dp",
            "id": "flxAttachments2",
            "isVisible": true,
            "layoutType": kony.flex.FLOW_VERTICAL,
            "left": "0",
            "isModalContainer": false,
            "skin": "slFbox",
            "width": "140dp",
            "zIndex": 1
        }, {}, {});
        flxAttachments2.setDefaultUnit(kony.flex.DP);
        var lblAttatchmentName2 = new kony.ui.Label({
            "id": "lblAttatchmentName2",
            "isVisible": true,
            "left": "10dp",
            "skin": "sknlblLatoBold35475f14px",
            "text": "Attatchment name.zip",
            "top": "5dp",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, {
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, {});
        var lblAttatchmentSize2 = new kony.ui.Label({
            "id": "lblAttatchmentSize2",
            "isVisible": true,
            "left": "10dp",
            "skin": "sknlblLatoBold35475f14px",
            "text": "1.5 MB",
            "top": "0dp",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, {
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, {});
        flxAttachments2.add(lblAttatchmentName2, lblAttatchmentSize2);
        flxAttachment2.add(flxAttatcmentImage2, flxFlagSeperator2, flxAttachments2);
        var flxAttachment3 = new kony.ui.FlexContainer({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "bottom": "30px",
            "clipBounds": true,
            "height": "40px",
            "id": "flxAttachment3",
            "isVisible": true,
            "layoutType": kony.flex.FLOW_HORIZONTAL,
            "left": "10px",
            "isModalContainer": false,
            "skin": "skne15edop100",
            "top": "5px",
            "width": "180px",
            "zIndex": 1
        }, {}, {});
        flxAttachment3.setDefaultUnit(kony.flex.DP);
        var flxAttatcmentImage3 = new kony.ui.FlexContainer({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "centerY": "50%",
            "clipBounds": true,
            "height": "40dp",
            "id": "flxAttatcmentImage3",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "left": "0",
            "isModalContainer": false,
            "skin": "slFbox",
            "top": "20dp",
            "width": "40dp",
            "zIndex": 1
        }, {}, {});
        flxAttatcmentImage3.setDefaultUnit(kony.flex.DP);
        var imgDownload3 = new kony.ui.Image2({
            "centerX": "50%",
            "centerY": "50%",
            "height": "15dp",
            "id": "imgDownload3",
            "isVisible": false,
            "skin": "slImage",
            "src": "download_blue_2x.png",
            "width": "15dp",
            "zIndex": 1
        }, {
            "imageScaleMode": constants.IMAGE_SCALE_MODE_MAINTAIN_ASPECT_RATIO,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, {});
        var lblIconDownload3 = new kony.ui.Label({
            "centerX": "50%",
            "centerY": "50%",
            "height": "25dp",
            "id": "lblIconDownload3",
            "isVisible": true,
            "skin": "sknIconDownload24pxBlue",
            "text": "",
            "width": "24dp",
            "zIndex": 1
        }, {
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, {});
        flxAttatcmentImage3.add(imgDownload3, lblIconDownload3);
        var flxFlagSeperator3 = new kony.ui.FlexContainer({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "centerY": "50%",
            "clipBounds": true,
            "height": "40px",
            "id": "flxFlagSeperator3",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "left": "0px",
            "isModalContainer": false,
            "skin": "sknflxOuterBorderdfd9dd",
            "top": "0%",
            "width": "1px",
            "zIndex": 111
        }, {}, {});
        flxFlagSeperator3.setDefaultUnit(kony.flex.DP);
        flxFlagSeperator3.add();
        var flxAttachments3 = new kony.ui.FlexContainer({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "centerY": "50%",
            "clipBounds": true,
            "height": "40dp",
            "id": "flxAttachments3",
            "isVisible": true,
            "layoutType": kony.flex.FLOW_VERTICAL,
            "left": "0",
            "isModalContainer": false,
            "skin": "slFbox",
            "width": "140dp",
            "zIndex": 1
        }, {}, {});
        flxAttachments3.setDefaultUnit(kony.flex.DP);
        var lblAttatchmentName3 = new kony.ui.Label({
            "id": "lblAttatchmentName3",
            "isVisible": true,
            "left": "10dp",
            "skin": "sknlblLatoBold35475f14px",
            "text": "Attatchment name.zip",
            "top": "5dp",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, {
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, {});
        var lblAttatchmentSize3 = new kony.ui.Label({
            "id": "lblAttatchmentSize3",
            "isVisible": true,
            "left": "10dp",
            "skin": "sknlblLatoBold35475f14px",
            "text": "1.5 MB",
            "top": "0dp",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, {
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, {});
        flxAttachments3.add(lblAttatchmentName3, lblAttatchmentSize3);
        flxAttachment3.add(flxAttatcmentImage3, flxFlagSeperator3, flxAttachments3);
        var flxAttachment4 = new kony.ui.FlexContainer({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "bottom": "30px",
            "clipBounds": true,
            "height": "40px",
            "id": "flxAttachment4",
            "isVisible": true,
            "layoutType": kony.flex.FLOW_HORIZONTAL,
            "left": "10px",
            "isModalContainer": false,
            "skin": "skne15edop100",
            "top": "5px",
            "width": "180px",
            "zIndex": 1
        }, {}, {});
        flxAttachment4.setDefaultUnit(kony.flex.DP);
        var flxAttatcmentImage4 = new kony.ui.FlexContainer({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "centerY": "50%",
            "clipBounds": true,
            "height": "40dp",
            "id": "flxAttatcmentImage4",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "left": "0",
            "isModalContainer": false,
            "skin": "slFbox",
            "top": "20dp",
            "width": "40dp",
            "zIndex": 1
        }, {}, {});
        flxAttatcmentImage4.setDefaultUnit(kony.flex.DP);
        var imgDownload4 = new kony.ui.Image2({
            "centerX": "50%",
            "centerY": "50%",
            "height": "15dp",
            "id": "imgDownload4",
            "isVisible": false,
            "skin": "slImage",
            "src": "download_blue_2x.png",
            "width": "15dp",
            "zIndex": 1
        }, {
            "imageScaleMode": constants.IMAGE_SCALE_MODE_MAINTAIN_ASPECT_RATIO,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, {});
        var lblIconDownload4 = new kony.ui.Label({
            "centerX": "50%",
            "centerY": "50%",
            "height": "25dp",
            "id": "lblIconDownload4",
            "isVisible": true,
            "skin": "sknIconDownload24pxBlue",
            "text": "",
            "width": "24dp",
            "zIndex": 1
        }, {
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, {});
        flxAttatcmentImage4.add(imgDownload4, lblIconDownload4);
        var flxFlagSeperator4 = new kony.ui.FlexContainer({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "centerY": "50%",
            "clipBounds": true,
            "height": "40px",
            "id": "flxFlagSeperator4",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "left": "0px",
            "isModalContainer": false,
            "skin": "sknflxOuterBorderdfd9dd",
            "top": "0%",
            "width": "1px",
            "zIndex": 111
        }, {}, {});
        flxFlagSeperator4.setDefaultUnit(kony.flex.DP);
        flxFlagSeperator4.add();
        var flxAttachments4 = new kony.ui.FlexContainer({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "centerY": "50%",
            "clipBounds": true,
            "height": "40dp",
            "id": "flxAttachments4",
            "isVisible": true,
            "layoutType": kony.flex.FLOW_VERTICAL,
            "left": "0",
            "isModalContainer": false,
            "skin": "slFbox",
            "width": "140dp",
            "zIndex": 1
        }, {}, {});
        flxAttachments4.setDefaultUnit(kony.flex.DP);
        var lblAttatchmentName4 = new kony.ui.Label({
            "id": "lblAttatchmentName4",
            "isVisible": true,
            "left": "10dp",
            "skin": "sknlblLatoBold35475f14px",
            "text": "Attatchment name.zip",
            "top": "5dp",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, {
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, {});
        var lblAttatchmentSize4 = new kony.ui.Label({
            "id": "lblAttatchmentSize4",
            "isVisible": true,
            "left": "10dp",
            "skin": "sknlblLatoBold35475f14px",
            "text": "1.5 MB",
            "top": "0dp",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, {
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, {});
        flxAttachments4.add(lblAttatchmentName4, lblAttatchmentSize4);
        flxAttachment4.add(flxAttatcmentImage4, flxFlagSeperator4, flxAttachments4);
        var flxAttachment5 = new kony.ui.FlexContainer({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "bottom": "30px",
            "clipBounds": true,
            "height": "40px",
            "id": "flxAttachment5",
            "isVisible": true,
            "layoutType": kony.flex.FLOW_HORIZONTAL,
            "left": "10px",
            "isModalContainer": false,
            "skin": "skne15edop100",
            "top": "5px",
            "width": "180px",
            "zIndex": 1
        }, {}, {});
        flxAttachment5.setDefaultUnit(kony.flex.DP);
        var flxAttatcmentImage5 = new kony.ui.FlexContainer({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "centerY": "50%",
            "clipBounds": true,
            "height": "40dp",
            "id": "flxAttatcmentImage5",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "left": "0",
            "isModalContainer": false,
            "skin": "slFbox",
            "top": "20dp",
            "width": "40dp",
            "zIndex": 1
        }, {}, {});
        flxAttatcmentImage5.setDefaultUnit(kony.flex.DP);
        var imgDownload5 = new kony.ui.Image2({
            "centerX": "50%",
            "centerY": "50%",
            "height": "15dp",
            "id": "imgDownload5",
            "isVisible": false,
            "skin": "slImage",
            "src": "download_blue_2x.png",
            "width": "15dp",
            "zIndex": 1
        }, {
            "imageScaleMode": constants.IMAGE_SCALE_MODE_MAINTAIN_ASPECT_RATIO,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, {});
        var lblIconDownload5 = new kony.ui.Label({
            "centerX": "50%",
            "centerY": "50%",
            "height": "25dp",
            "id": "lblIconDownload5",
            "isVisible": true,
            "skin": "sknIconDownload24pxBlue",
            "text": "",
            "width": "24dp",
            "zIndex": 1
        }, {
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, {});
        flxAttatcmentImage5.add(imgDownload5, lblIconDownload5);
        var flxFlagSeperator5 = new kony.ui.FlexContainer({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "centerY": "50%",
            "clipBounds": true,
            "height": "40px",
            "id": "flxFlagSeperator5",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "left": "0px",
            "isModalContainer": false,
            "skin": "sknflxOuterBorderdfd9dd",
            "top": "0%",
            "width": "1px",
            "zIndex": 111
        }, {}, {});
        flxFlagSeperator5.setDefaultUnit(kony.flex.DP);
        flxFlagSeperator5.add();
        var flxAttachments5 = new kony.ui.FlexContainer({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "centerY": "50%",
            "clipBounds": true,
            "height": "40dp",
            "id": "flxAttachments5",
            "isVisible": true,
            "layoutType": kony.flex.FLOW_VERTICAL,
            "left": "0",
            "isModalContainer": false,
            "skin": "slFbox",
            "width": "140dp",
            "zIndex": 1
        }, {}, {});
        flxAttachments5.setDefaultUnit(kony.flex.DP);
        var lblAttatchmentName5 = new kony.ui.Label({
            "id": "lblAttatchmentName5",
            "isVisible": true,
            "left": "10dp",
            "skin": "sknlblLatoBold35475f14px",
            "text": "Attatchment name.zip",
            "top": "5dp",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, {
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, {});
        var lblAttatchmentSize5 = new kony.ui.Label({
            "id": "lblAttatchmentSize5",
            "isVisible": true,
            "left": "10dp",
            "skin": "sknlblLatoBold35475f14px",
            "text": "1.5 MB",
            "top": "0dp",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, {
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, {});
        flxAttachments5.add(lblAttatchmentName5, lblAttatchmentSize5);
        flxAttachment5.add(flxAttatcmentImage5, flxFlagSeperator5, flxAttachments5);
        flxAttachmnetWrapper.add(flxAttachment, flxAttachment2, flxAttachment3, flxAttachment4, flxAttachment5);
        var lblSeperator2 = new kony.ui.Label({
            "bottom": 0,
            "height": "1px",
            "id": "lblSeperator2",
            "isVisible": true,
            "left": "0px",
            "right": "0px",
            "skin": "sknSeparator",
            "zIndex": 1
        }, {
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, {});
        flxAttatchments.add(lblAttatchments, flxAttachmnetWrapper, lblSeperator2);
        flxEmailMainLeftPadding.add(flxMessage, flxAttatchments);
        return flxEmailMainLeftPadding;
    }
})