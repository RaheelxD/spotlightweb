define("flxUsersHeader", function() {
    return function(controller) {
        var flxUsersHeader = new kony.ui.FlexContainer({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "clipBounds": true,
            "height": "65px",
            "id": "flxUsersHeader",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "isModalContainer": false,
            "skin": "sknflxffffffop100"
        }, {}, {});
        flxUsersHeader.setDefaultUnit(kony.flex.DP);
        var flxUsersHeaderFullName = new kony.ui.FlexContainer({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "clipBounds": true,
            "height": "100%",
            "id": "flxUsersHeaderFullName",
            "isVisible": true,
            "layoutType": kony.flex.FLOW_HORIZONTAL,
            "left": "35px",
            "isModalContainer": false,
            "skin": "slFbox",
            "top": "0dp",
            "width": "120px",
            "zIndex": 1
        }, {}, {});
        flxUsersHeaderFullName.setDefaultUnit(kony.flex.DP);
        var lblUsersHeaderName = new kony.ui.Label({
            "centerY": "50%",
            "id": "lblUsersHeaderName",
            "isVisible": true,
            "skin": "sknlblLato696c7312px",
            "text": "FULL NAME",
            "top": 0,
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, {
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, {
            "hoverSkin": "sknlblLatoBold00000012px"
        });
        var lblSortName = new kony.ui.Label({
            "centerY": "50%",
            "id": "lblSortName",
            "isVisible": true,
            "left": "7dp",
            "skin": "sknIcon15px",
            "text": "",
            "top": "0dp",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, {
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, {
            "hoverSkin": "sknlblCursorFont"
        });
        flxUsersHeaderFullName.add(lblUsersHeaderName, lblSortName);
        var flxUsersHeaderUsername = new kony.ui.FlexContainer({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "clipBounds": true,
            "height": "100%",
            "id": "flxUsersHeaderUsername",
            "isVisible": true,
            "layoutType": kony.flex.FLOW_HORIZONTAL,
            "left": "17.75%",
            "isModalContainer": false,
            "skin": "slFbox",
            "top": "0dp",
            "width": "120px",
            "zIndex": 1
        }, {}, {});
        flxUsersHeaderUsername.setDefaultUnit(kony.flex.DP);
        var lblUsersHeaderUsername = new kony.ui.Label({
            "centerY": "50%",
            "id": "lblUsersHeaderUsername",
            "isVisible": true,
            "left": "0px",
            "skin": "sknlblLato5d6c7f12px",
            "text": "USERNAME",
            "top": 0,
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, {
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, {
            "hoverSkin": "sknlblLatoBold00000012px"
        });
        var lblSortUsername = new kony.ui.Label({
            "centerY": "50%",
            "id": "lblSortUsername",
            "isVisible": true,
            "left": "7dp",
            "skin": "sknIcon15px",
            "text": "",
            "top": "0dp",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, {
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, {
            "hoverSkin": "sknlblCursorFont"
        });
        flxUsersHeaderUsername.add(lblUsersHeaderUsername, lblSortUsername);
        var flxUsersHeaderEmailId = new kony.ui.FlexContainer({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "clipBounds": true,
            "height": "100%",
            "id": "flxUsersHeaderEmailId",
            "isVisible": true,
            "layoutType": kony.flex.FLOW_HORIZONTAL,
            "left": "32.47%",
            "isModalContainer": false,
            "skin": "slFbox",
            "top": "0dp",
            "width": "180px",
            "zIndex": 1
        }, {}, {});
        flxUsersHeaderEmailId.setDefaultUnit(kony.flex.DP);
        var lblUsersHeaderEmail = new kony.ui.Label({
            "centerY": "50%",
            "id": "lblUsersHeaderEmail",
            "isVisible": true,
            "left": "0px",
            "skin": "sknlblLato696c7312px",
            "text": "EMAIL",
            "top": 0,
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, {
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, {
            "hoverSkin": "sknlblLatoBold00000012px"
        });
        var lblSortEmail = new kony.ui.Label({
            "centerY": "50%",
            "id": "lblSortEmail",
            "isVisible": true,
            "left": "7dp",
            "skin": "sknIcon15px",
            "text": "",
            "top": "0dp",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, {
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, {
            "hoverSkin": "sknlblCursorFont"
        });
        flxUsersHeaderEmailId.add(lblUsersHeaderEmail, lblSortEmail);
        var flxUsersHeaderRole = new kony.ui.FlexContainer({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "clipBounds": true,
            "height": "100%",
            "id": "flxUsersHeaderRole",
            "isVisible": true,
            "layoutType": kony.flex.FLOW_HORIZONTAL,
            "left": "55%",
            "isModalContainer": false,
            "skin": "slFbox",
            "top": "0dp",
            "width": "100px",
            "zIndex": 1
        }, {}, {});
        flxUsersHeaderRole.setDefaultUnit(kony.flex.DP);
        var lblUsersHeaderRole = new kony.ui.Label({
            "centerY": "50%",
            "id": "lblUsersHeaderRole",
            "isVisible": true,
            "left": "0px",
            "skin": "sknlblLato696c7312px",
            "text": "ROLE",
            "top": 0,
            "width": "35px",
            "zIndex": 1
        }, {
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, {
            "hoverSkin": "sknlblLatoBold00000012px"
        });
        var lblSortRole = new kony.ui.Label({
            "centerY": "50%",
            "id": "lblSortRole",
            "isVisible": true,
            "left": "7dp",
            "skin": "sknIcon15px",
            "text": "",
            "top": "0dp",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, {
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, {
            "hoverSkin": "sknlblCursorFont"
        });
        flxUsersHeaderRole.add(lblUsersHeaderRole, lblSortRole);
        var flxUsersHeaderPermissions = new kony.ui.FlexContainer({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "clipBounds": true,
            "height": "100%",
            "id": "flxUsersHeaderPermissions",
            "isVisible": true,
            "layoutType": kony.flex.FLOW_HORIZONTAL,
            "left": "69%",
            "isModalContainer": false,
            "skin": "slFbox",
            "top": "0dp",
            "width": "120px",
            "zIndex": 1
        }, {}, {});
        flxUsersHeaderPermissions.setDefaultUnit(kony.flex.DP);
        var lblUsersHeaderPermissions = new kony.ui.Label({
            "centerY": "50%",
            "id": "lblUsersHeaderPermissions",
            "isVisible": true,
            "left": "0px",
            "skin": "sknlblLato696c7312px",
            "text": "PERMISSIONS",
            "top": 0,
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, {
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, {
            "hoverSkin": "sknlblLatoBold00000012px"
        });
        var lblSortPermissions = new kony.ui.Label({
            "centerY": "50%",
            "id": "lblSortPermissions",
            "isVisible": true,
            "left": "7dp",
            "skin": "sknIcon15px",
            "text": "",
            "top": "0dp",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, {
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, {
            "hoverSkin": "sknlblCursorFont"
        });
        flxUsersHeaderPermissions.add(lblUsersHeaderPermissions, lblSortPermissions);
        var flxUsersHeaderStatus = new kony.ui.FlexContainer({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "clipBounds": true,
            "height": "100%",
            "id": "flxUsersHeaderStatus",
            "isVisible": true,
            "layoutType": kony.flex.FLOW_HORIZONTAL,
            "left": "82%",
            "isModalContainer": false,
            "skin": "slFbox",
            "top": "0dp",
            "width": "100px",
            "zIndex": 1
        }, {}, {});
        flxUsersHeaderStatus.setDefaultUnit(kony.flex.DP);
        var lblUsersHeaderStatus = new kony.ui.Label({
            "centerY": "50%",
            "id": "lblUsersHeaderStatus",
            "isVisible": true,
            "left": "0px",
            "skin": "sknlblLato696c7312px",
            "text": "STATUS",
            "top": 0,
            "width": "45px",
            "zIndex": 1
        }, {
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, {
            "hoverSkin": "sknlblLatoBold00000012px"
        });
        var lblFilterStatus = new kony.ui.Label({
            "centerY": "50%",
            "id": "lblFilterStatus",
            "isVisible": true,
            "left": "7dp",
            "skin": "sknIcon15px",
            "text": "",
            "top": "0dp",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, {
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, {
            "hoverSkin": "sknlblCursorFont"
        });
        flxUsersHeaderStatus.add(lblUsersHeaderStatus, lblFilterStatus);
        var lblUsersHeaderSeperator = new kony.ui.Label({
            "bottom": 0,
            "height": "1px",
            "id": "lblUsersHeaderSeperator",
            "isVisible": true,
            "left": "0dp",
            "skin": "sknLblTableHeaderLine",
            "text": ".",
            "width": "100%",
            "zIndex": 1
        }, {
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, {});
        flxUsersHeader.add(flxUsersHeaderFullName, flxUsersHeaderUsername, flxUsersHeaderEmailId, flxUsersHeaderRole, flxUsersHeaderPermissions, flxUsersHeaderStatus, lblUsersHeaderSeperator);
        return flxUsersHeader;
    }
})