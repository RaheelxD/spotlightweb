define("flxListOfLeads", function() {
    return function(controller) {
        var flxListOfLeads = new kony.ui.FlexContainer({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "clipBounds": true,
            "height": "50dp",
            "id": "flxListOfLeads",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "left": "0dp",
            "isModalContainer": false,
            "skin": "sknFlxFFFFFF1000",
            "top": "0dp",
            "width": "100%"
        }, {}, {
            "hoverSkin": "sknFlxf9fbfd1000"
        });
        flxListOfLeads.setDefaultUnit(kony.flex.DP);
        var flxListOfLeadsInner = new kony.ui.FlexContainer({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "clipBounds": true,
            "height": "100%",
            "id": "flxListOfLeadsInner",
            "isVisible": true,
            "layoutType": kony.flex.FLOW_HORIZONTAL,
            "left": "0dp",
            "isModalContainer": false,
            "skin": "slFbox",
            "top": "0dp",
            "width": "100%"
        }, {}, {});
        flxListOfLeadsInner.setDefaultUnit(kony.flex.DP);
        var lblProduct = new kony.ui.Label({
            "id": "lblProduct",
            "isVisible": true,
            "left": "30dp",
            "skin": "sknLblFont485C75100O",
            "text": "Label",
            "top": "22dp",
            "width": "22%",
            "zIndex": 1
        }, {
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, {});
        var lblStatus = new kony.ui.Label({
            "id": "lblStatus",
            "isVisible": true,
            "left": "2%",
            "skin": "sknLblFont485C75100O",
            "text": "Label",
            "top": "22dp",
            "width": "12%",
            "zIndex": 1
        }, {
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, {});
        var lblCreatedOn = new kony.ui.Label({
            "id": "lblCreatedOn",
            "isVisible": true,
            "left": "2%",
            "skin": "sknLblFont485C75100O",
            "text": "Label",
            "top": "22dp",
            "width": "12%",
            "zIndex": 1
        }, {
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, {});
        var lblCreatedBy = new kony.ui.Label({
            "id": "lblCreatedBy",
            "isVisible": true,
            "left": "2%",
            "skin": "sknLblFont485C75100O",
            "text": "Label",
            "top": "22dp",
            "width": "20%",
            "zIndex": 1
        }, {
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, {});
        var lblAssignTo = new kony.ui.Label({
            "id": "lblAssignTo",
            "isVisible": true,
            "left": "2%",
            "skin": "sknLblFont485C75100O",
            "text": "Label",
            "top": "22dp",
            "width": "20%",
            "zIndex": 1
        }, {
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, {});
        flxListOfLeadsInner.add(lblProduct, lblStatus, lblCreatedOn, lblCreatedBy, lblAssignTo);
        var lblSeparatorListOfLeads = new kony.ui.Label({
            "bottom": "0dp",
            "height": "1dp",
            "id": "lblSeparatorListOfLeads",
            "isVisible": true,
            "left": "22dp",
            "right": "22dp",
            "skin": "sknLblD5D9DD1000",
            "zIndex": 1
        }, {
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, {});
        flxListOfLeads.add(flxListOfLeadsInner, lblSeparatorListOfLeads);
        return flxListOfLeads;
    }
})