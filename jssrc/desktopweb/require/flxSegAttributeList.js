define("flxSegAttributeList", function() {
    return function(controller) {
        var flxSegAttributeList = new kony.ui.FlexContainer({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "clipBounds": true,
            "height": "46px",
            "id": "flxSegAttributeList",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "left": "0dp",
            "isModalContainer": false,
            "skin": "sknBackgroundFFFFFF",
            "top": "0dp",
            "width": "100%"
        }, {}, {});
        flxSegAttributeList.setDefaultUnit(kony.flex.DP);
        var lblAttribute = new kony.ui.Label({
            "height": "16px",
            "id": "lblAttribute",
            "isVisible": true,
            "left": "10px",
            "skin": "sknLbl485C75LatoRegular13Px",
            "text": "Credit Score",
            "top": "15px",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, {
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, {});
        var lblCriteria = new kony.ui.Label({
            "height": "16px",
            "id": "lblCriteria",
            "isVisible": true,
            "left": "34%",
            "skin": "sknLbl485C75LatoRegular13Px",
            "text": "Greater than",
            "top": "15px",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, {
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, {});
        var lblValue = new kony.ui.Label({
            "height": "16px",
            "id": "lblValue",
            "isVisible": true,
            "left": "67%",
            "skin": "sknLbl485C75LatoRegular13Px",
            "text": "350",
            "top": "15px",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, {
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, {});
        var lblSeparator = new kony.ui.Label({
            "bottom": "0px",
            "height": "1dp",
            "id": "lblSeparator",
            "isVisible": true,
            "left": "0dp",
            "right": "0px",
            "skin": "sknLblSeparator",
            "text": "_",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, {
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, {});
        flxSegAttributeList.add(lblAttribute, lblCriteria, lblValue, lblSeparator);
        return flxSegAttributeList;
    }
})