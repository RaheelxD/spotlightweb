define("flxCustomerMangGroup", function() {
    return function(controller) {
        var flxCustomerMangGroup = new kony.ui.FlexContainer({
            "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
            "clipBounds": true,
            "id": "flxCustomerMangGroup",
            "isVisible": true,
            "layoutType": kony.flex.FLOW_VERTICAL,
            "isModalContainer": false,
            "skin": "slFbox"
        }, {}, {});
        flxCustomerMangGroup.setDefaultUnit(kony.flex.DP);
        var flxGroupNameAndDescription = new kony.ui.FlexContainer({
            "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
            "clipBounds": true,
            "id": "flxGroupNameAndDescription",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "left": "0dp",
            "isModalContainer": false,
            "skin": "slFbox",
            "top": "0dp",
            "width": "100%",
            "zIndex": 1
        }, {}, {});
        flxGroupNameAndDescription.setDefaultUnit(kony.flex.DP);
        var lblGroupName = new kony.ui.Label({
            "id": "lblGroupName",
            "isVisible": true,
            "left": "30px",
            "right": "72%",
            "skin": "sknlblLatoBold35475f14px",
            "text": "Platinum Customers",
            "top": "19px",
            "zIndex": 1
        }, {
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, {});
        var rtxDescription = new kony.ui.Label({
            "id": "rtxDescription",
            "isVisible": true,
            "left": "30%",
            "right": "95px",
            "skin": "sknlblLatoBold35475f14px",
            "text": "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Duis sit amet semper massa. Maecenas varius purus lacus, nec tempor leo egestas a. Nullam posuere tincidunt laoreet.",
            "top": "19px",
            "zIndex": 1
        }, {
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, {});
        var flxDelete = new kony.ui.FlexContainer({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "clipBounds": true,
            "height": "25px",
            "id": "flxDelete",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "isModalContainer": false,
            "onClick": controller.AS_FlexContainer_a7137a3f29fd43d4b9e9e0e09102f661,
            "right": "30px",
            "skin": "sknCursor",
            "top": "15px",
            "width": "25px",
            "zIndex": 1
        }, {}, {
            "hoverSkin": "sknflxffffffop100Border424242Radius100px"
        });
        flxDelete.setDefaultUnit(kony.flex.DP);
        var imgDelete = new kony.ui.Image2({
            "centerX": "50%",
            "centerY": "50%",
            "height": "15dp",
            "id": "imgDelete",
            "isVisible": false,
            "left": 14,
            "skin": "slImage",
            "src": "delete_2x.png",
            "width": "15dp",
            "zIndex": 1
        }, {
            "imageScaleMode": constants.IMAGE_SCALE_MODE_MAINTAIN_ASPECT_RATIO,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, {});
        var fonticonDelete = new kony.ui.Label({
            "centerX": "50%",
            "centerY": "50%",
            "id": "fonticonDelete",
            "isVisible": true,
            "skin": "sknFontIconOptionMenuRow",
            "text": "",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, {
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, {});
        flxDelete.add(imgDelete, fonticonDelete);
        flxGroupNameAndDescription.add(lblGroupName, rtxDescription, flxDelete);
        var flxSeperator = new kony.ui.FlexContainer({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "clipBounds": true,
            "height": "1px",
            "id": "flxSeperator",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "left": "0px",
            "isModalContainer": false,
            "skin": "slFbox",
            "top": "19px",
            "width": "100%",
            "zIndex": 1
        }, {}, {});
        flxSeperator.setDefaultUnit(kony.flex.DP);
        var lblSeperator = new kony.ui.Label({
            "height": "1px",
            "id": "lblSeperator",
            "isVisible": true,
            "left": "20px",
            "right": "20px",
            "skin": "sknlblSeperator",
            "text": ".",
            "top": "0px",
            "zIndex": 1
        }, {
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, {});
        flxSeperator.add(lblSeperator);
        flxCustomerMangGroup.add(flxGroupNameAndDescription, flxSeperator);
        return flxCustomerMangGroup;
    }
})