define("flxAlertsCategoryLanguageData", function() {
    return function(controller) {
        var flxAlertsCategoryLanguageData = new kony.ui.FlexContainer({
            "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
            "clipBounds": true,
            "id": "flxAlertsCategoryLanguageData",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "left": "0dp",
            "isModalContainer": false,
            "skin": "sknFlxFFFFFF100O",
            "top": "0dp",
            "width": "100%"
        }, {}, {
            "hoverSkin": "sknfbfcfcWithoutPointer"
        });
        flxAlertsCategoryLanguageData.setDefaultUnit(kony.flex.DP);
        var flxLanguageRowContainer = new kony.ui.FlexContainer({
            "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
            "bottom": "15dp",
            "clipBounds": true,
            "id": "flxLanguageRowContainer",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "left": "0dp",
            "isModalContainer": false,
            "skin": "slFbox",
            "top": "0dp",
            "width": "100%",
            "zIndex": 1
        }, {}, {});
        flxLanguageRowContainer.setDefaultUnit(kony.flex.DP);
        var flxCategoryLanguage = new kony.ui.FlexContainer({
            "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
            "clipBounds": true,
            "id": "flxCategoryLanguage",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "left": "0dp",
            "isModalContainer": false,
            "skin": "slFbox",
            "top": "5dp",
            "width": "19%",
            "zIndex": 1
        }, {}, {});
        flxCategoryLanguage.setDefaultUnit(kony.flex.DP);
        var lblCategoryLanguage = new kony.ui.Label({
            "id": "lblCategoryLanguage",
            "isVisible": true,
            "left": "20dp",
            "skin": "sknLbl485c7513pxHover",
            "text": "Label",
            "top": "15dp",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, {
            "contentAlignment": constants.CONTENT_ALIGN_TOP_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, {});
        var lstBoxSelectLanguage = new kony.ui.ListBox({
            "focusSkin": "defListBoxFocus",
            "height": "30dp",
            "id": "lstBoxSelectLanguage",
            "isVisible": false,
            "left": "20dp",
            "masterData": [
                ["lb1", "English(US)"],
                ["lb2", "English(UK)"],
                ["lb3", "Spanish"],
                ["lb4", "French"],
                ["lb0", "Select"]
            ],
            "right": "0dp",
            "selectedKey": "lb0",
            "skin": "sknlbxBgffffffBorderc1c9ceRadius3Px",
            "top": "15dp",
            "zIndex": 1
        }, {
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [3, 0, 0, 0],
            "paddingInPixel": false
        }, {
            "multiSelect": false
        });
        flxCategoryLanguage.add(lblCategoryLanguage, lstBoxSelectLanguage);
        var flxCategoryDisplayName = new kony.ui.FlexContainer({
            "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
            "clipBounds": true,
            "id": "flxCategoryDisplayName",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "left": "21%",
            "isModalContainer": false,
            "skin": "slFbox",
            "top": "5dp",
            "width": "23%",
            "zIndex": 1
        }, {}, {});
        flxCategoryDisplayName.setDefaultUnit(kony.flex.DP);
        var lblCategoryDisplayName = new kony.ui.Label({
            "id": "lblCategoryDisplayName",
            "isVisible": true,
            "left": "20dp",
            "right": "10dp",
            "skin": "sknLbl485c7513pxHover",
            "text": "Label",
            "top": "15dp",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, {
            "contentAlignment": constants.CONTENT_ALIGN_TOP_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, {});
        var tbxDisplayName = new kony.ui.TextBox2({
            "autoCapitalize": constants.TEXTBOX_AUTO_CAPITALIZE_NONE,
            "focusSkin": "defTextBoxFocus",
            "height": "35dp",
            "id": "tbxDisplayName",
            "isVisible": false,
            "keyBoardStyle": constants.TEXTBOX_KEY_BOARD_STYLE_DEFAULT,
            "left": "20dp",
            "placeholder": "Display name",
            "right": "0dp",
            "secureTextEntry": false,
            "skin": "skntxtbx484b5214px",
            "textInputMode": constants.TEXTBOX_INPUT_MODE_ANY,
            "top": "10dp",
            "zIndex": 1
        }, {
            "containerHeightMode": constants.TEXTBOX_FONT_METRICS_DRIVEN_HEIGHT,
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [3, 0, 0, 0],
            "paddingInPixel": false
        }, {
            "autoCorrect": false,
            "placeholderSkin": "defTextBoxPlaceholder"
        });
        flxCategoryDisplayName.add(lblCategoryDisplayName, tbxDisplayName);
        var flxCategoryDescription = new kony.ui.FlexContainer({
            "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
            "clipBounds": true,
            "id": "flxCategoryDescription",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "left": "44%",
            "isModalContainer": false,
            "right": "80dp",
            "skin": "slFbox",
            "top": "0dp",
            "zIndex": 1
        }, {}, {});
        flxCategoryDescription.setDefaultUnit(kony.flex.DP);
        var lblCategoryDescription = new kony.ui.Label({
            "id": "lblCategoryDescription",
            "isVisible": true,
            "left": "20dp",
            "right": "20dp",
            "skin": "sknLbl485c7513pxHover",
            "text": "Label",
            "top": "15dp",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, {
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, {});
        var flxDescription = new kony.ui.FlexContainer({
            "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
            "clipBounds": true,
            "id": "flxDescription",
            "isVisible": false,
            "layoutType": kony.flex.FREE_FORM,
            "left": "0dp",
            "isModalContainer": false,
            "skin": "slFbox",
            "top": "5dp",
            "width": "100%"
        }, {}, {});
        flxDescription.setDefaultUnit(kony.flex.DP);
        var lblDescCount = new kony.ui.Label({
            "id": "lblDescCount",
            "isVisible": false,
            "right": "10dp",
            "skin": "sknLbl485c7513pxHover",
            "text": "0/500",
            "top": "0dp",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, {
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, {});
        var tbxDescription = new kony.ui.TextArea2({
            "autoCapitalize": constants.TEXTAREA_AUTO_CAPITALIZE_NONE,
            "focusSkin": "defTextAreaFocus",
            "height": "65dp",
            "id": "tbxDescription",
            "isVisible": true,
            "keyBoardStyle": constants.TEXTAREA_KEY_BOARD_STYLE_DEFAULT,
            "left": "20dp",
            "numberOfVisibleLines": 3,
            "placeholder": "Write description here",
            "skin": "skntxtAreaLato35475f14Px",
            "textInputMode": constants.TEXTAREA_INPUT_MODE_ANY,
            "top": "15dp",
            "width": "95%",
            "zIndex": 1
        }, {
            "contentAlignment": constants.CONTENT_ALIGN_TOP_LEFT,
            "padding": [2, 2, 2, 2],
            "paddingInPixel": false
        }, {
            "autoCorrect": false,
            "placeholderSkin": "defTextAreaPlaceholder"
        });
        flxDescription.add(lblDescCount, tbxDescription);
        flxCategoryDescription.add(lblCategoryDescription, flxDescription);
        var flxCategoryLangDelete = new kony.ui.FlexContainer({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "clipBounds": true,
            "height": "20dp",
            "id": "flxCategoryLangDelete",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "isModalContainer": false,
            "right": "35dp",
            "skin": "slFbox",
            "top": "10dp",
            "width": "25dp",
            "zIndex": 1
        }, {}, {
            "hoverSkin": "sknCursor"
        });
        flxCategoryLangDelete.setDefaultUnit(kony.flex.DP);
        var lblIconDeleteLang = new kony.ui.Label({
            "centerX": "50%",
            "centerY": "50%",
            "id": "lblIconDeleteLang",
            "isVisible": true,
            "skin": "sknIcon20px",
            "text": "",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, {
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, {});
        flxCategoryLangDelete.add(lblIconDeleteLang);
        flxLanguageRowContainer.add(flxCategoryLanguage, flxCategoryDisplayName, flxCategoryDescription, flxCategoryLangDelete);
        var lblSeprator = new kony.ui.Label({
            "bottom": "0dp",
            "height": "1dp",
            "id": "lblSeprator",
            "isVisible": true,
            "left": "20dp",
            "right": "20dp",
            "skin": "sknSeparator",
            "text": "-",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, {
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, {});
        flxAlertsCategoryLanguageData.add(flxLanguageRowContainer, lblSeprator);
        return flxAlertsCategoryLanguageData;
    }
})