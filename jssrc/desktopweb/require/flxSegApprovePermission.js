define("flxSegApprovePermission", function() {
    return function(controller) {
        var flxSegApprovePermission = new kony.ui.FlexContainer({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "clipBounds": true,
            "height": "50px",
            "id": "flxSegApprovePermission",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "isModalContainer": false,
            "skin": "sknflxffffffop100"
        }, {}, {});
        flxSegApprovePermission.setDefaultUnit(kony.flex.DP);
        var flxAccountContainer = new kony.ui.FlexContainer({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "clipBounds": true,
            "height": "100%",
            "id": "flxAccountContainer",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "left": "20dp",
            "isModalContainer": false,
            "right": "20dp",
            "skin": "slFbox",
            "top": "0dp"
        }, {}, {
            "hoverSkin": "sknfbfcfc"
        });
        flxAccountContainer.setDefaultUnit(kony.flex.DP);
        var lbAccountName = new kony.ui.Label({
            "centerY": "50%",
            "height": "16px",
            "id": "lbAccountName",
            "isVisible": true,
            "left": "16px",
            "right": "85%",
            "skin": "sknlblLatoRegular484b5213px",
            "text": "Account Name",
            "width": "15%",
            "zIndex": 1
        }, {
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, {});
        var lblAccountNumber = new kony.ui.Label({
            "centerY": "50%",
            "height": "16px",
            "id": "lblAccountNumber",
            "isVisible": true,
            "left": "31%",
            "skin": "sknlblLatoRegular484b5213px",
            "text": "Account Number",
            "width": "24%",
            "zIndex": 1
        }, {
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, {});
        var lblAccountType = new kony.ui.Label({
            "centerY": "50%",
            "height": "16px",
            "id": "lblAccountType",
            "isVisible": true,
            "left": "60%",
            "skin": "sknlblLatoRegular484b5213px",
            "text": "Account Type",
            "width": "29%",
            "zIndex": 1
        }, {
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, {});
        flxAccountContainer.add(lbAccountName, lblAccountNumber, lblAccountType);
        flxSegApprovePermission.add(flxAccountContainer);
        return flxSegApprovePermission;
    }
})