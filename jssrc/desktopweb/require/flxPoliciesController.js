define("userflxPoliciesController", {
    //Type your controller code here 
});
define("flxPoliciesControllerActions", {
    /*
        This is an auto generated file and any modifications to it may result in corruption of the action sequence.
    */
    AS_FlexContainer_gad12a58e60c494384b37fd321e2a1fb: function AS_FlexContainer_gad12a58e60c494384b37fd321e2a1fb(eventobject, context) {
        var self = this;
        this.executeOnParent("showSelectedRow");
    }
});
define("flxPoliciesController", ["userflxPoliciesController", "flxPoliciesControllerActions"], function() {
    var controller = require("userflxPoliciesController");
    var controllerActions = ["flxPoliciesControllerActions"];
    return kony.visualizer.mixinControllerActions(controller, controllerActions);
});
