define("flxContractsLimitsHeaderCreate", function() {
    return function(controller) {
        var flxContractsLimitsHeaderCreate = new kony.ui.FlexContainer({
            "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
            "clipBounds": true,
            "id": "flxContractsLimitsHeaderCreate",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "left": "0dp",
            "isModalContainer": false,
            "skin": "sknFlxFFFFFF1000",
            "top": "0dp",
            "width": "100%"
        }, {}, {});
        flxContractsLimitsHeaderCreate.setDefaultUnit(kony.flex.DP);
        var flxHeader = new kony.ui.FlexContainer({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "clipBounds": true,
            "height": "65dp",
            "id": "flxHeader",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "left": "15px",
            "isModalContainer": false,
            "right": "20px",
            "skin": "slFbox",
            "top": "0px",
            "zIndex": 1
        }, {}, {});
        flxHeader.setDefaultUnit(kony.flex.DP);
        var flxActionDetails = new kony.ui.FlexContainer({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "clipBounds": true,
            "height": "65dp",
            "id": "flxActionDetails",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "left": "0dp",
            "isModalContainer": false,
            "skin": "slFbox",
            "top": "0dp",
            "width": "100%"
        }, {}, {});
        flxActionDetails.setDefaultUnit(kony.flex.DP);
        var flxRow1 = new kony.ui.FlexContainer({
            "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
            "centerY": "50%",
            "clipBounds": true,
            "id": "flxRow1",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "left": "0dp",
            "isModalContainer": false,
            "skin": "slFbox",
            "top": "12px",
            "width": "100%"
        }, {}, {});
        flxRow1.setDefaultUnit(kony.flex.DP);
        var flxToggle = new kony.ui.FlexContainer({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "clipBounds": true,
            "height": "13dp",
            "id": "flxToggle",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "left": "0dp",
            "isModalContainer": false,
            "right": "0px",
            "skin": "slFbox",
            "top": "3dp",
            "width": "13dp",
            "zIndex": 1
        }, {}, {
            "hoverSkin": "sknCursor"
        });
        flxToggle.setDefaultUnit(kony.flex.DP);
        var lblToggle = new kony.ui.Label({
            "centerX": "50%",
            "centerY": "50%",
            "height": "100%",
            "id": "lblToggle",
            "isVisible": true,
            "skin": "sknLblIIcoMoon485c7511px",
            "i18n_text": "kony.i18n.getLocalizedString(\"i18n.userwidgetmodel.fontIconBreadcrumbsDown\")",
            "width": "100%",
            "zIndex": 1
        }, {
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, {});
        flxToggle.add(lblToggle);
        var lblFeatureName = new kony.ui.Label({
            "id": "lblFeatureName",
            "isVisible": true,
            "left": "25px",
            "skin": "sknLbl192B45LatoRegular14px",
            "text": "International Transfers",
            "top": "0",
            "width": "70%",
            "zIndex": 1
        }, {
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, {});
        flxRow1.add(flxToggle, lblFeatureName);
        var flxRow2 = new kony.ui.FlexContainer({
            "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
            "clipBounds": true,
            "id": "flxRow2",
            "isVisible": true,
            "layoutType": kony.flex.FLOW_HORIZONTAL,
            "left": "0dp",
            "isModalContainer": false,
            "skin": "slFbox",
            "top": "40dp",
            "width": "100%"
        }, {}, {});
        flxRow2.setDefaultUnit(kony.flex.DP);
        var lblMonetaryActions = new kony.ui.Label({
            "id": "lblMonetaryActions",
            "isVisible": true,
            "left": "25dp",
            "skin": "sknLbl485C75LatoRegular13Px",
            "text": "Monetary Actions:",
            "top": "0dp",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, {
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, {});
        var lblCountActions = new kony.ui.Label({
            "id": "lblCountActions",
            "isVisible": true,
            "left": "10dp",
            "skin": "sknLbl192b45LatoBold13px",
            "text": "2",
            "top": "0dp",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, {
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, {});
        flxRow2.add(lblMonetaryActions, lblCountActions);
        flxActionDetails.add(flxRow1, flxRow2);
        flxHeader.add(flxActionDetails);
        var flxViewLimitsHeader = new kony.ui.FlexContainer({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "clipBounds": true,
            "height": "40px",
            "id": "flxViewLimitsHeader",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "left": "10px",
            "isModalContainer": false,
            "right": "10px",
            "skin": "slFbox",
            "top": "65px"
        }, {}, {});
        flxViewLimitsHeader.setDefaultUnit(kony.flex.DP);
        var lblActionHeader = new kony.ui.Label({
            "centerY": "50%",
            "id": "lblActionHeader",
            "isVisible": true,
            "left": "30dp",
            "skin": "sknlblLato696c7312px",
            "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmMfascenarios.ACTION_CAP\")",
            "width": "10%",
            "zIndex": 1
        }, {
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, {
            "hoverSkin": "sknlblLato00000011px"
        });
        var flxPerLimitHeader = new kony.ui.FlexContainer({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "centerY": "50%",
            "clipBounds": true,
            "height": "100%",
            "id": "flxPerLimitHeader",
            "isVisible": true,
            "layoutType": kony.flex.FLOW_HORIZONTAL,
            "left": "25%",
            "isModalContainer": false,
            "skin": "slFbox",
            "width": "23%"
        }, {}, {});
        flxPerLimitHeader.setDefaultUnit(kony.flex.DP);
        var lblPerLimitHeader = new kony.ui.Label({
            "centerY": "50%",
            "id": "lblPerLimitHeader",
            "isVisible": true,
            "left": "0dp",
            "skin": "sknlblLato696c7311px",
            "text": "PER TRANSACTION",
            "top": 0,
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, {
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, {});
        var flxLimitInfo1 = new kony.ui.FlexContainer({
            "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
            "centerY": "50%",
            "clipBounds": true,
            "id": "flxLimitInfo1",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "left": "7dp",
            "isModalContainer": false,
            "onClick": controller.AS_FlexContainer_aeb8168ea3624740bf06d583316c5596,
            "skin": "sknCursor",
            "width": "17dp",
            "zIndex": 2
        }, {}, {});
        flxLimitInfo1.setDefaultUnit(kony.flex.DP);
        var fontIconInfo1 = new kony.ui.Label({
            "id": "fontIconInfo1",
            "isVisible": true,
            "left": "0dp",
            "skin": "sknIcon18px",
            "text": "",
            "top": "0dp",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, {
            "contentAlignment": constants.CONTENT_ALIGN_TOP_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, {});
        flxLimitInfo1.add(fontIconInfo1);
        flxPerLimitHeader.add(lblPerLimitHeader, flxLimitInfo1);
        var flxDailyLimitHeader = new kony.ui.FlexContainer({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "centerY": "50%",
            "clipBounds": true,
            "height": "100%",
            "id": "flxDailyLimitHeader",
            "isVisible": true,
            "layoutType": kony.flex.FLOW_HORIZONTAL,
            "left": "50%",
            "isModalContainer": false,
            "skin": "slFbox",
            "width": "23%"
        }, {}, {});
        flxDailyLimitHeader.setDefaultUnit(kony.flex.DP);
        var lblDailyLimitHeader = new kony.ui.Label({
            "centerY": "50%",
            "id": "lblDailyLimitHeader",
            "isVisible": true,
            "left": "0dp",
            "skin": "sknlblLato696c7311px",
            "text": "DAILY TRANSACTION",
            "top": 0,
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, {
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, {});
        var flxLimitInfo2 = new kony.ui.FlexContainer({
            "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
            "centerY": "50%",
            "clipBounds": true,
            "id": "flxLimitInfo2",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "left": "7dp",
            "isModalContainer": false,
            "onClick": controller.AS_FlexContainer_aeb8168ea3624740bf06d583316c5596,
            "skin": "sknCursor",
            "width": "17dp",
            "zIndex": 2
        }, {}, {});
        flxLimitInfo2.setDefaultUnit(kony.flex.DP);
        var fontIconInfo2 = new kony.ui.Label({
            "id": "fontIconInfo2",
            "isVisible": true,
            "left": "0dp",
            "skin": "sknIcon18px",
            "text": "",
            "top": "0dp",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, {
            "contentAlignment": constants.CONTENT_ALIGN_TOP_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, {});
        flxLimitInfo2.add(fontIconInfo2);
        flxDailyLimitHeader.add(lblDailyLimitHeader, flxLimitInfo2);
        var flxWeeklyLimitHeader = new kony.ui.FlexContainer({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "centerY": "50%",
            "clipBounds": true,
            "height": "100%",
            "id": "flxWeeklyLimitHeader",
            "isVisible": true,
            "layoutType": kony.flex.FLOW_HORIZONTAL,
            "left": "75%",
            "isModalContainer": false,
            "skin": "slFbox",
            "width": "26%"
        }, {}, {});
        flxWeeklyLimitHeader.setDefaultUnit(kony.flex.DP);
        var lblWeeklyLimitHeader = new kony.ui.Label({
            "centerY": "50%",
            "id": "lblWeeklyLimitHeader",
            "isVisible": true,
            "left": "0dp",
            "skin": "sknlblLato696c7311px",
            "text": "WEEKLY TRANSACTION",
            "top": 0,
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, {
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, {});
        var flxLimitInfo3 = new kony.ui.FlexContainer({
            "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
            "centerY": "50%",
            "clipBounds": true,
            "id": "flxLimitInfo3",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "left": "7dp",
            "isModalContainer": false,
            "onClick": controller.AS_FlexContainer_aeb8168ea3624740bf06d583316c5596,
            "skin": "sknCursor",
            "width": "17dp",
            "zIndex": 2
        }, {}, {});
        flxLimitInfo3.setDefaultUnit(kony.flex.DP);
        var fontIconInfo3 = new kony.ui.Label({
            "id": "fontIconInfo3",
            "isVisible": true,
            "left": "0dp",
            "skin": "sknIcon18px",
            "text": "",
            "top": "0dp",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, {
            "contentAlignment": constants.CONTENT_ALIGN_TOP_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, {});
        flxLimitInfo3.add(fontIconInfo3);
        flxWeeklyLimitHeader.add(lblWeeklyLimitHeader, flxLimitInfo3);
        var lblFASeperator2 = new kony.ui.Label({
            "bottom": 0,
            "height": "1px",
            "id": "lblFASeperator2",
            "isVisible": true,
            "left": "10px",
            "right": "10px",
            "skin": "sknLblSeparator696C73",
            "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmCSR.lblSeperator\")",
            "zIndex": 1
        }, {
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, {});
        flxViewLimitsHeader.add(lblActionHeader, flxPerLimitHeader, flxDailyLimitHeader, flxWeeklyLimitHeader, lblFASeperator2);
        var lblFASeperator1 = new kony.ui.Label({
            "height": "1px",
            "id": "lblFASeperator1",
            "isVisible": true,
            "left": "0px",
            "right": "0dp",
            "skin": "sknLblSeparatore7e7e7",
            "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmCSR.lblSeperator\")",
            "top": "65dp",
            "zIndex": 1
        }, {
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, {});
        var lblFASeperatorTop = new kony.ui.Label({
            "height": "1px",
            "id": "lblFASeperatorTop",
            "isVisible": true,
            "left": "0px",
            "right": "0dp",
            "skin": "sknLblSeparatore7e7e7",
            "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmCSR.lblSeperator\")",
            "top": "0dp",
            "zIndex": 1
        }, {
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, {});
        flxContractsLimitsHeaderCreate.add(flxHeader, flxViewLimitsHeader, lblFASeperator1, lblFASeperatorTop);
        return flxContractsLimitsHeaderCreate;
    }
})