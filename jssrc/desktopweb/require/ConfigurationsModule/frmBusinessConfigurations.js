define("ConfigurationsModule/frmBusinessConfigurations", function() {
    return function(controller) {
        function addWidgetsfrmBusinessConfigurations() {
            this.setDefaultUnit(kony.flex.DP);
            var flxMain = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "100%",
                "id": "flxMain",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "slFbox0b7aaa2e3bfa340",
                "top": "0dp",
                "width": "100%",
                "zIndex": 1
            }, {}, {});
            flxMain.setDefaultUnit(kony.flex.DP);
            var flxLeftPannel = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "100%",
                "id": "flxLeftPannel",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "masterType": constants.MASTER_TYPE_USERWIDGET,
                "isModalContainer": false,
                "skin": "sknLeftPannel0bb79e4bcca8e45",
                "top": "0dp",
                "width": "305px",
                "zIndex": 5
            }, {}, {});
            flxLeftPannel.setDefaultUnit(kony.flex.DP);
            var leftMenuNew = new com.adminConsole.navigation.leftMenuNew({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "height": "100%",
                "id": "leftMenuNew",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "masterType": constants.MASTER_TYPE_DEFAULT,
                "isModalContainer": false,
                "skin": "sknFlxBg003E75Op100",
                "top": "0dp",
                "width": "100%",
                "overrides": {
                    "brandLogo": {
                        "src": "infinity_dbx_c360_logo_white_2x.png"
                    },
                    "imgBottomLogo": {
                        "src": "logo_white_2x.png"
                    },
                    "leftMenuNew": {
                        "bottom": "viz.val_cleared",
                        "centerX": "viz.val_cleared",
                        "centerY": "viz.val_cleared",
                        "height": "100%",
                        "left": "0dp",
                        "maxHeight": "viz.val_cleared",
                        "maxWidth": "viz.val_cleared",
                        "minHeight": "viz.val_cleared",
                        "minWidth": "viz.val_cleared",
                        "right": "viz.val_cleared",
                        "top": "0dp",
                        "width": "100%"
                    }
                }
            }, {
                "overrides": {}
            }, {
                "overrides": {}
            });
            flxLeftPannel.add(leftMenuNew);
            var flxRightPanel = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "100%",
                "id": "flxRightPanel",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "305px",
                "masterType": constants.MASTER_TYPE_USERWIDGET,
                "isModalContainer": false,
                "right": "0dp",
                "skin": "slFbox0b7aaa2e3bfa340",
                "top": "0px",
                "zIndex": 1
            }, {}, {});
            flxRightPanel.setDefaultUnit(kony.flex.DP);
            var flxMainHeader = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "106dp",
                "id": "flxMainHeader",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0dp",
                "width": "100%",
                "zIndex": 1
            }, {}, {});
            flxMainHeader.setDefaultUnit(kony.flex.DP);
            var mainHeader = new com.adminConsole.header.mainHeader({
                "height": "106px",
                "id": "mainHeader",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "masterType": constants.MASTER_TYPE_DEFAULT,
                "isModalContainer": false,
                "skin": "CopyslFbox1",
                "width": "100%",
                "overrides": {
                    "btnAddNewOption": {
                        "right": "212px",
                        "text": "TEMPLATES"
                    },
                    "btnDropdownList": {
                        "text": "CREATE NEW MESSAGE"
                    },
                    "flxButtons": {
                        "isVisible": false
                    },
                    "flxHeaderSeperator": {
                        "isVisible": false
                    },
                    "flxMainHeader": {
                        "left": undefined,
                        "top": undefined
                    },
                    "imgLogout": {
                        "right": "0px",
                        "src": "img_logout.png"
                    },
                    "lblHeading": {
                        "i18n_text": "kony.i18n.getLocalizedString(\"i18n.leftmenu.businessconfigurations\")"
                    },
                    "lblUserName": {
                        "right": "25px",
                        "text": "Preetish",
                        "top": "viz.val_cleared"
                    },
                    "mainHeader": {
                        "left": undefined,
                        "top": undefined
                    }
                }
            }, {
                "overrides": {}
            }, {
                "overrides": {}
            });
            flxMainHeader.add(mainHeader);
            var flxScrollMainContent = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": false,
                "height": "700px",
                "id": "flxScrollMainContent",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0px",
                "isModalContainer": false,
                "right": "0px",
                "top": "105px",
                "width": "100%",
                "zIndex": 1
            }, {}, {});
            flxScrollMainContent.setDefaultUnit(kony.flex.DP);
            var flxConfigurationsContainer = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "bottom": "0dp",
                "clipBounds": true,
                "id": "flxConfigurationsContainer",
                "isVisible": true,
                "layoutType": kony.flex.FLOW_HORIZONTAL,
                "left": "35dp",
                "isModalContainer": false,
                "right": "35px",
                "skin": "sknFlxBgFFFFFFBorE4E6ECsz1pxR3px",
                "top": "0dp"
            }, {}, {});
            flxConfigurationsContainer.setDefaultUnit(kony.flex.DP);
            var flxLeftOptions = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "100%",
                "id": "flxLeftOptions",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0dp",
                "width": "25%",
                "zIndex": 1
            }, {}, {});
            flxLeftOptions.setDefaultUnit(kony.flex.DP);
            var configLeftMenu = new com.adminConsole.BusinessConfigurations.configLeftMenu({
                "height": "100%",
                "id": "configLeftMenu",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "masterType": constants.MASTER_TYPE_DEFAULT,
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0dp",
                "width": "100%",
                "overrides": {
                    "configLeftMenu": {
                        "width": "100%"
                    },
                    "flxHeader2": {
                        "isVisible": true
                    },
                    "flxHeader3": {
                        "isVisible": true
                    },
                    "flxSelected2": {
                        "height": "100%",
                        "isVisible": false
                    },
                    "flxSelected3": {
                        "isVisible": false
                    },
                    "flxSeperator22": {
                        "isVisible": false
                    },
                    "flxseparator21": {
                        "isVisible": true
                    },
                    "lblConfigurationName1": {
                        "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmBusinessConfigurations.elegibleCriteria\")"
                    },
                    "lblConfigurationName22": {
                        "centerX": "viz.val_cleared",
                        "right": "25px",
                        "width": "viz.val_cleared"
                    }
                }
            }, {
                "overrides": {}
            }, {
                "overrides": {}
            });
            flxLeftOptions.add(configLeftMenu);
            var flxVerticalSeprator = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "100%",
                "id": "flxVerticalSeprator",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "flxe4e6ec1px",
                "top": "0dp",
                "width": "1dp",
                "zIndex": 1
            }, {}, {});
            flxVerticalSeprator.setDefaultUnit(kony.flex.DP);
            flxVerticalSeprator.add();
            var flxNoConfigurations = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "100%",
                "id": "flxNoConfigurations",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "isModalContainer": false,
                "right": "0dp",
                "skin": "slFbox",
                "top": "0dp",
                "width": "75%",
                "zIndex": 1
            }, {}, {});
            flxNoConfigurations.setDefaultUnit(kony.flex.DP);
            var flxConfigurationsList = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "bottom": "10dp",
                "clipBounds": true,
                "id": "flxConfigurationsList",
                "isVisible": false,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0dp",
                "width": "100%",
                "zIndex": 1
            }, {}, {});
            flxConfigurationsList.setDefaultUnit(kony.flex.DP);
            var flxAddButton = new kony.ui.Button({
                "focusSkin": "sknbtnf7f7faLatoRegular12Px485c75Border1px485C75Radius20px",
                "height": "22px",
                "id": "flxAddButton",
                "isVisible": true,
                "right": "20px",
                "skin": "sknbtnf7f7faLatoRegular12Px485c75Border1px485C75Radius20px",
                "text": "Add New Criteria",
                "top": "15px",
                "width": "122px",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_CENTER,
                "displayText": true,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {
                "hoverSkin": "sknbtnffffffLatoRegular12Px485c75Border1px485C75Radius20px"
            });
            var criteriaList = new com.adminConsole.BusinessConfigurations.criteriaList({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "bottom": "10dp",
                "id": "criteriaList",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "masterType": constants.MASTER_TYPE_DEFAULT,
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "40dp",
                "width": "100%",
                "overrides": {
                    "criteriaList": {
                        "top": "40dp"
                    },
                    "flxTableView": {
                        "bottom": "0dp"
                    },
                    "fontIconFilterStatus": {
                        "isVisible": true
                    },
                    "imgOption1": {
                        "src": "edit2x.png"
                    },
                    "imgOption2": {
                        "src": "deactive_2x.png"
                    },
                    "imgOption3": {
                        "src": "delete_2x.png"
                    },
                    "segCriteria": {
                        "bottom": "0dp",
                        "data": [{
                            "fonticonActive": "",
                            "lblCriteriaDesc": "",
                            "lblCriteriaStatus": "",
                            "lblOptions": "",
                            "lblSeparator": ""
                        }],
                        "height": "viz.val_cleared"
                    }
                }
            }, {
                "overrides": {}
            }, {
                "overrides": {}
            });
            flxConfigurationsList.add(flxAddButton, criteriaList);
            var flxBusinessConfigSegment = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "bottom": 0,
                "clipBounds": true,
                "id": "flxBusinessConfigSegment",
                "isVisible": true,
                "layoutType": kony.flex.FLOW_VERTICAL,
                "left": "20dp",
                "isModalContainer": false,
                "right": "20dp",
                "skin": "slFbox",
                "top": "0dp",
                "zIndex": 1
            }, {}, {});
            flxBusinessConfigSegment.setDefaultUnit(kony.flex.DP);
            var flxBusinessConfigContainer = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                "clipBounds": true,
                "id": "flxBusinessConfigContainer",
                "isVisible": false,
                "layoutType": kony.flex.FLOW_VERTICAL,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "sknflxBgF9F9F9BorderE4E6EC3pxRad",
                "top": "20dp",
                "width": "100%",
                "zIndex": 1
            }, {}, {});
            flxBusinessConfigContainer.setDefaultUnit(kony.flex.DP);
            var lblConfigName1 = new kony.ui.Label({
                "id": "lblConfigName1",
                "isVisible": true,
                "left": "20px",
                "skin": "sknLbl16pxLato192B45",
                "text": "Business Enrollment Approvals",
                "top": "20px",
                "width": "80%",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var flxConfigDescription1 = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                "clipBounds": true,
                "id": "flxConfigDescription1",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "10dp",
                "width": "100%"
            }, {}, {});
            flxConfigDescription1.setDefaultUnit(kony.flex.DP);
            var rtxConfigDescription1 = new kony.ui.Label({
                "bottom": "20px",
                "id": "rtxConfigDescription1",
                "isVisible": true,
                "left": "20px",
                "right": "30%",
                "skin": "sknLbl485C75LatoRegular13Px",
                "text": "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do incididunt ut labore et dolore magna aliqua.",
                "top": "0px",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_TOP_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var flxApprovalStatus = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "25px",
                "id": "flxApprovalStatus",
                "isVisible": true,
                "layoutType": kony.flex.FLOW_HORIZONTAL,
                "reverseLayoutDirection": true,
                "isModalContainer": false,
                "right": "20dp",
                "skin": "slFbox",
                "top": "0dp",
                "width": "30%",
                "zIndex": 1
            }, {}, {});
            flxApprovalStatus.setDefaultUnit(kony.flex.DP);
            var flxSwitch1 = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "100%",
                "id": "flxSwitch1",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "isModalContainer": false,
                "right": "0dp",
                "skin": "slFbox",
                "top": "0",
                "width": "36dp"
            }, {}, {
                "hoverSkin": "sknFlxPointer"
            });
            flxSwitch1.setDefaultUnit(kony.flex.DP);
            var switchStatus1 = new kony.ui.Switch({
                "centerY": "50%",
                "height": "25dp",
                "id": "switchStatus1",
                "isVisible": true,
                "leftSideText": "ON",
                "right": "0dp",
                "rightSideText": "OFF",
                "selectedIndex": 0,
                "skin": "sknSwitchServiceManagement",
                "width": "36dp",
                "zIndex": 1
            }, {
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxSwitch1.add(switchStatus1);
            var lblSwitchApprove1 = new kony.ui.Label({
                "centerY": "50%",
                "id": "lblSwitchApprove1",
                "isVisible": true,
                "left": "0px",
                "right": "10px",
                "skin": "sknlbl485c7514px",
                "text": "Auto-Approval",
                "top": 0,
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxApprovalStatus.add(flxSwitch1, lblSwitchApprove1);
            flxConfigDescription1.add(rtxConfigDescription1, flxApprovalStatus);
            flxBusinessConfigContainer.add(lblConfigName1, flxConfigDescription1);
            var flxLinkProfileConfig = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                "clipBounds": true,
                "id": "flxLinkProfileConfig",
                "isVisible": false,
                "layoutType": kony.flex.FLOW_VERTICAL,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "sknflxBgF9F9F9BorderE4E6EC3pxRad",
                "top": "10dp",
                "width": "100%",
                "zIndex": 1
            }, {}, {});
            flxLinkProfileConfig.setDefaultUnit(kony.flex.DP);
            var lblConfigName2 = new kony.ui.Label({
                "id": "lblConfigName2",
                "isVisible": true,
                "left": "20px",
                "skin": "sknLbl16pxLato192B45",
                "text": "Business Enrollment Approvals",
                "top": "20px",
                "width": "80%",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var flxConfigDescription2 = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                "clipBounds": true,
                "id": "flxConfigDescription2",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "10dp",
                "width": "100%"
            }, {}, {});
            flxConfigDescription2.setDefaultUnit(kony.flex.DP);
            var rtxConfigDescription2 = new kony.ui.Label({
                "bottom": "20px",
                "id": "rtxConfigDescription2",
                "isVisible": true,
                "left": "20px",
                "right": "30%",
                "skin": "sknLbl485C75LatoRegular13Px",
                "text": "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do incididunt ut labore et dolore magna aliqua.",
                "top": "0px",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_TOP_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var flxLinkProfiles = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "25px",
                "id": "flxLinkProfiles",
                "isVisible": true,
                "layoutType": kony.flex.FLOW_HORIZONTAL,
                "reverseLayoutDirection": true,
                "isModalContainer": false,
                "right": "20dp",
                "skin": "slFbox",
                "top": "0dp",
                "width": "30%",
                "zIndex": 1
            }, {}, {});
            flxLinkProfiles.setDefaultUnit(kony.flex.DP);
            var flxSwitch2 = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "100%",
                "id": "flxSwitch2",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "isModalContainer": false,
                "right": "0dp",
                "skin": "slFbox",
                "top": "0",
                "width": "36dp"
            }, {}, {
                "hoverSkin": "sknFlxPointer"
            });
            flxSwitch2.setDefaultUnit(kony.flex.DP);
            var switchStatus2 = new kony.ui.Switch({
                "centerY": "50%",
                "height": "25dp",
                "id": "switchStatus2",
                "isVisible": true,
                "leftSideText": "ON",
                "right": "0dp",
                "rightSideText": "OFF",
                "selectedIndex": 0,
                "skin": "sknSwitchServiceManagement",
                "width": "36dp",
                "zIndex": 1
            }, {
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxSwitch2.add(switchStatus2);
            var lblSwitchApprove2 = new kony.ui.Label({
                "centerY": "50%",
                "id": "lblSwitchApprove2",
                "isVisible": true,
                "left": "0px",
                "right": "10px",
                "skin": "sknlbl485c7514px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmBusinessConfigurations.linkProfiles\")",
                "top": 0,
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxLinkProfiles.add(flxSwitch2, lblSwitchApprove2);
            flxConfigDescription2.add(rtxConfigDescription2, flxLinkProfiles);
            flxLinkProfileConfig.add(lblConfigName2, flxConfigDescription2);
            flxBusinessConfigSegment.add(flxBusinessConfigContainer, flxLinkProfileConfig);
            var noStaticData = new com.adminConsole.staticContent.noStaticData({
                "height": "420px",
                "id": "noStaticData",
                "isVisible": false,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0px",
                "masterType": constants.MASTER_TYPE_DEFAULT,
                "isModalContainer": false,
                "right": "0px",
                "skin": "slFbox",
                "top": "0dp",
                "width": "100%",
                "zIndex": 1,
                "overrides": {
                    "btnAddStaticContent": {
                        "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmBusinessConfigurations.addElegibleCriteria\")",
                        "width": "250dp"
                    },
                    "lblNoStaticContentCreated": {
                        "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmBusinessConfigurations.elegibleCriteriaNotCreatedYet\")"
                    },
                    "lblNoStaticContentMsg": {
                        "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmBusinessConfigurations.elegibleCriteriaClickOn\")"
                    },
                    "noStaticData": {
                        "height": "420px",
                        "isVisible": false,
                        "left": "0px",
                        "right": "0px",
                        "width": "100%"
                    }
                }
            }, {
                "overrides": {}
            }, {
                "overrides": {}
            });
            var flxNoRecordsFound = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "200dp",
                "id": "flxNoRecordsFound",
                "isVisible": false,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "100dp",
                "width": "100%",
                "zIndex": 2
            }, {}, {});
            flxNoRecordsFound.setDefaultUnit(kony.flex.DP);
            var rtxNoRecords = new kony.ui.RichText({
                "centerX": "50%",
                "centerY": "50%",
                "id": "rtxNoRecords",
                "isVisible": true,
                "skin": "sknRtxLato84939e12Px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.userwidgetmodel.rtxNoRecords\")",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxNoRecordsFound.add(rtxNoRecords);
            var flxCriteriaStatusFilter = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                "clipBounds": true,
                "id": "flxCriteriaStatusFilter",
                "isVisible": false,
                "layoutType": kony.flex.FREE_FORM,
                "isModalContainer": false,
                "right": "16%",
                "skin": "slFbox",
                "top": "80dp",
                "width": "130dp",
                "zIndex": 3
            }, {}, {});
            flxCriteriaStatusFilter.setDefaultUnit(kony.flex.DP);
            var statusFilterMenu = new com.adminConsole.common.statusFilterMenu({
                "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                "id": "statusFilterMenu",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "masterType": constants.MASTER_TYPE_DEFAULT,
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "5dp",
                "width": "100%",
                "zIndex": 5,
                "overrides": {
                    "imgUpArrow": {
                        "src": "uparrow_2x.png"
                    },
                    "segStatusFilterDropdown": {
                        "data": [{
                            "imgCheckBox": "checkbox.png",
                            "lblDescription": "Active"
                        }, {
                            "imgCheckBox": "checkbox.png",
                            "lblDescription": "Inactive"
                        }, {
                            "imgCheckBox": "checkbox.png",
                            "lblDescription": "Suspended"
                        }]
                    }
                }
            }, {
                "overrides": {}
            }, {
                "overrides": {}
            });
            flxCriteriaStatusFilter.add(statusFilterMenu);
            var flxAlertConfigContainer = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "100%",
                "id": "flxAlertConfigContainer",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0dp",
                "width": "100%"
            }, {}, {});
            flxAlertConfigContainer.setDefaultUnit(kony.flex.DP);
            var flxEditAlertConfigContainer = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "100%",
                "id": "flxEditAlertConfigContainer",
                "isVisible": false,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0dp",
                "width": "100%"
            }, {}, {});
            flxEditAlertConfigContainer.setDefaultUnit(kony.flex.DP);
            var flxEditAlertConfig = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                "bottom": "0dp",
                "clipBounds": true,
                "id": "flxEditAlertConfig",
                "isVisible": true,
                "layoutType": kony.flex.FLOW_VERTICAL,
                "left": "0dp",
                "isModalContainer": false,
                "right": "0dp",
                "skin": "slFbox",
                "top": "0dp",
                "zIndex": 1
            }, {}, {});
            flxEditAlertConfig.setDefaultUnit(kony.flex.DP);
            var lblConfigName3 = new kony.ui.Label({
                "id": "lblConfigName3",
                "isVisible": true,
                "left": "20px",
                "skin": "sknLbl16pxLato192B45",
                "text": "Alert configurations",
                "top": "20px",
                "width": "80%",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var lblConfigDesc = new kony.ui.Label({
                "bottom": "20px",
                "id": "lblConfigDesc",
                "isVisible": true,
                "left": "20px",
                "right": "20px",
                "skin": "sknLbl485C75LatoRegular13Px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmBusinessConfigurations.EditAlertDesc\")",
                "top": "10px",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_TOP_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var flxChannelFrequency = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                "clipBounds": true,
                "id": "flxChannelFrequency",
                "isVisible": true,
                "layoutType": kony.flex.FLOW_HORIZONTAL,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0dp",
                "width": "100%"
            }, {}, {});
            flxChannelFrequency.setDefaultUnit(kony.flex.DP);
            var lblChannelFrequency = new kony.ui.Label({
                "id": "lblChannelFrequency",
                "isVisible": true,
                "left": "20px",
                "skin": "sknLbl192B45LatoRegular14px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmBusinessConfigurations.channelFrequency\")",
                "top": "0px",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxChannelFrequency.add(lblChannelFrequency);
            var flxAlertConfigCheckList = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                "clipBounds": true,
                "id": "flxAlertConfigCheckList",
                "isVisible": true,
                "layoutType": kony.flex.FLOW_VERTICAL,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0dp",
                "width": "100%"
            }, {}, {});
            flxAlertConfigCheckList.setDefaultUnit(kony.flex.DP);
            var flxRadioButtons = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                "clipBounds": true,
                "id": "flxRadioButtons",
                "isVisible": true,
                "layoutType": kony.flex.FLOW_VERTICAL,
                "left": "44dp",
                "isModalContainer": false,
                "right": "20px",
                "skin": "slFbox",
                "top": "0dp",
                "zIndex": 1
            }, {}, {});
            flxRadioButtons.setDefaultUnit(kony.flex.DP);
            var flxRadioBtnGrp1 = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "20px",
                "id": "flxRadioBtnGrp1",
                "isVisible": true,
                "layoutType": kony.flex.FLOW_HORIZONTAL,
                "left": "0px",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "10px",
                "width": "100%"
            }, {}, {});
            flxRadioBtnGrp1.setDefaultUnit(kony.flex.DP);
            var flxRadioButton1 = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "100%",
                "id": "flxRadioButton1",
                "isVisible": true,
                "layoutType": kony.flex.FLOW_HORIZONTAL,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0dp",
                "width": "18px"
            }, {}, {
                "hoverSkin": "sknCursor"
            });
            flxRadioButton1.setDefaultUnit(kony.flex.DP);
            var imgRadioBtn1 = new kony.ui.Image2({
                "height": "15dp",
                "id": "imgRadioBtn1",
                "isVisible": true,
                "left": "0dp",
                "skin": "slImage",
                "src": "radio_notselected.png",
                "top": "3dp",
                "width": "15dp",
                "zIndex": 1
            }, {
                "imageScaleMode": constants.IMAGE_SCALE_MODE_MAINTAIN_ASPECT_RATIO,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxRadioButton1.add(imgRadioBtn1);
            var lblFrequencyConfig1 = new kony.ui.Label({
                "id": "lblFrequencyConfig1",
                "isVisible": true,
                "left": "10px",
                "skin": "sknLbl485C75LatoRegular13Px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmBusinessConfigurations.categoryLevel\")",
                "top": "3px",
                "width": "90%",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_TOP_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxRadioBtnGrp1.add(flxRadioButton1, lblFrequencyConfig1);
            var flxRadioBtnGrp2 = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "20px",
                "id": "flxRadioBtnGrp2",
                "isVisible": true,
                "layoutType": kony.flex.FLOW_HORIZONTAL,
                "left": "0px",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "10px",
                "width": "100%"
            }, {}, {});
            flxRadioBtnGrp2.setDefaultUnit(kony.flex.DP);
            var flxRadioButton2 = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "100%",
                "id": "flxRadioButton2",
                "isVisible": true,
                "layoutType": kony.flex.FLOW_HORIZONTAL,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0dp",
                "width": "18px"
            }, {}, {
                "hoverSkin": "sknCursor"
            });
            flxRadioButton2.setDefaultUnit(kony.flex.DP);
            var imgRadioBtn2 = new kony.ui.Image2({
                "height": "15dp",
                "id": "imgRadioBtn2",
                "isVisible": true,
                "left": "0dp",
                "skin": "slImage",
                "src": "radio_notselected.png",
                "top": "3dp",
                "width": "15dp",
                "zIndex": 1
            }, {
                "imageScaleMode": constants.IMAGE_SCALE_MODE_MAINTAIN_ASPECT_RATIO,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxRadioButton2.add(imgRadioBtn2);
            var lblFrequencyConfig2 = new kony.ui.Label({
                "id": "lblFrequencyConfig2",
                "isVisible": true,
                "left": "10px",
                "skin": "sknLbl485C75LatoRegular13Px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmBusinessConfigurations.groupLevel\")",
                "top": "3px",
                "width": "90%",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_TOP_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxRadioBtnGrp2.add(flxRadioButton2, lblFrequencyConfig2);
            var flxRadioBtnGrp3 = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "20px",
                "id": "flxRadioBtnGrp3",
                "isVisible": true,
                "layoutType": kony.flex.FLOW_HORIZONTAL,
                "left": "0px",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "10px",
                "width": "100%"
            }, {}, {});
            flxRadioBtnGrp3.setDefaultUnit(kony.flex.DP);
            var flxRadioButton3 = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "100%",
                "id": "flxRadioButton3",
                "isVisible": true,
                "layoutType": kony.flex.FLOW_HORIZONTAL,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0dp",
                "width": "18px"
            }, {}, {
                "hoverSkin": "sknCursor"
            });
            flxRadioButton3.setDefaultUnit(kony.flex.DP);
            var imgRadioBtn3 = new kony.ui.Image2({
                "centerX": "50%",
                "height": "15dp",
                "id": "imgRadioBtn3",
                "isVisible": true,
                "skin": "slImage",
                "src": "radio_notselected.png",
                "top": "3dp",
                "width": "15dp",
                "zIndex": 1
            }, {
                "imageScaleMode": constants.IMAGE_SCALE_MODE_MAINTAIN_ASPECT_RATIO,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxRadioButton3.add(imgRadioBtn3);
            var lblFrequencyConfig3 = new kony.ui.Label({
                "id": "lblFrequencyConfig3",
                "isVisible": true,
                "left": "10px",
                "skin": "sknLbl485C75LatoRegular13Px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmBusinessConfigurations.alertLevel\")",
                "top": "3px",
                "width": "90%",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_TOP_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxRadioBtnGrp3.add(flxRadioButton3, lblFrequencyConfig3);
            flxRadioButtons.add(flxRadioBtnGrp1, flxRadioBtnGrp2, flxRadioBtnGrp3);
            var flxCheckBoxConfig1 = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "15px",
                "id": "flxCheckBoxConfig1",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "20dp",
                "isModalContainer": false,
                "right": "20dp",
                "top": "15dp",
                "width": "100%",
                "zIndex": 1
            }, {}, {});
            flxCheckBoxConfig1.setDefaultUnit(kony.flex.DP);
            var flxCheckBox1 = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "100%",
                "id": "flxCheckBox1",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0dp",
                "width": "15px",
                "zIndex": 1
            }, {}, {
                "hoverSkin": "sknCursor"
            });
            flxCheckBox1.setDefaultUnit(kony.flex.DP);
            var imgCheckBox1 = new kony.ui.Image2({
                "height": "100%",
                "id": "imgCheckBox1",
                "isVisible": true,
                "left": "0px",
                "skin": "slImage",
                "src": "checkboxnormal.png",
                "top": "0px",
                "width": "15px",
                "zIndex": 1
            }, {
                "imageScaleMode": constants.IMAGE_SCALE_MODE_MAINTAIN_ASPECT_RATIO,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxCheckBox1.add(imgCheckBox1);
            var lblCheckConfig1 = new kony.ui.Label({
                "id": "lblCheckConfig1",
                "isVisible": true,
                "left": "25dp",
                "right": "30dp",
                "skin": "slLabel0d20174dce8ea42",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmBusinessConfigurations.EditSeparateContact\")",
                "top": "0dp",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxCheckBoxConfig1.add(flxCheckBox1, lblCheckConfig1);
            var flxCheckBoxConfig2 = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "bottom": 125,
                "clipBounds": true,
                "height": "15px",
                "id": "flxCheckBoxConfig2",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "20dp",
                "isModalContainer": false,
                "top": "15dp",
                "width": "100%",
                "zIndex": 1
            }, {}, {});
            flxCheckBoxConfig2.setDefaultUnit(kony.flex.DP);
            var flxCheckBox2 = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "100%",
                "id": "flxCheckBox2",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0dp",
                "width": "15px",
                "zIndex": 1
            }, {}, {
                "hoverSkin": "sknCursor"
            });
            flxCheckBox2.setDefaultUnit(kony.flex.DP);
            var imgCheckBox2 = new kony.ui.Image2({
                "height": "100%",
                "id": "imgCheckBox2",
                "isVisible": true,
                "left": "0px",
                "skin": "slImage",
                "src": "checkboxnormal.png",
                "top": "0px",
                "width": "15px",
                "zIndex": 1
            }, {
                "imageScaleMode": constants.IMAGE_SCALE_MODE_MAINTAIN_ASPECT_RATIO,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxCheckBox2.add(imgCheckBox2);
            var lblCheckConfig2 = new kony.ui.Label({
                "id": "lblCheckConfig2",
                "isVisible": true,
                "left": "25dp",
                "right": "30px",
                "skin": "slLabel0d20174dce8ea42",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmBusinessConfigurations.EditFrequency\")",
                "top": "0dp",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxCheckBoxConfig2.add(flxCheckBox2, lblCheckConfig2);
            flxAlertConfigCheckList.add(flxRadioButtons, flxCheckBoxConfig1, flxCheckBoxConfig2);
            flxEditAlertConfig.add(lblConfigName3, lblConfigDesc, flxChannelFrequency, flxAlertConfigCheckList);
            var flxEditConfigButtons = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "bottom": "0dp",
                "clipBounds": true,
                "height": "80px",
                "id": "flxEditConfigButtons",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "slFbox",
                "width": "100%"
            }, {}, {});
            flxEditConfigButtons.setDefaultUnit(kony.flex.DP);
            var commonButtons = new com.adminConsole.common.commonButtons({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "centerY": "50%",
                "height": "80px",
                "id": "commonButtons",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "20px",
                "masterType": constants.MASTER_TYPE_DEFAULT,
                "isModalContainer": false,
                "right": "20px",
                "skin": "slFbox",
                "overrides": {
                    "btnCancel": {
                        "left": "0px"
                    },
                    "btnNext": {
                        "isVisible": false
                    },
                    "commonButtons": {
                        "bottom": "viz.val_cleared",
                        "centerX": "viz.val_cleared",
                        "centerY": "50%",
                        "height": "80px",
                        "left": "20px",
                        "right": "20px",
                        "top": "viz.val_cleared",
                        "width": "viz.val_cleared"
                    }
                }
            }, {
                "overrides": {}
            }, {
                "overrides": {}
            });
            flxEditConfigButtons.add(commonButtons);
            var flxBtnSeperator = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "bottom": "81dp",
                "clipBounds": true,
                "height": "1px",
                "id": "flxBtnSeperator",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "isModalContainer": false,
                "right": "0dp",
                "skin": "sknflxe1e5edop100",
                "zIndex": 5
            }, {}, {});
            flxBtnSeperator.setDefaultUnit(kony.flex.DP);
            flxBtnSeperator.add();
            flxEditAlertConfigContainer.add(flxEditAlertConfig, flxEditConfigButtons, flxBtnSeperator);
            var flxViewAlertConfigContainer = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "100%",
                "id": "flxViewAlertConfigContainer",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0dp",
                "width": "100%"
            }, {}, {});
            flxViewAlertConfigContainer.setDefaultUnit(kony.flex.DP);
            var flxViewAlertConfigs = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                "bottom": "0dp",
                "clipBounds": true,
                "id": "flxViewAlertConfigs",
                "isVisible": true,
                "layoutType": kony.flex.FLOW_VERTICAL,
                "left": "0dp",
                "isModalContainer": false,
                "right": "0dp",
                "skin": "slFbox",
                "top": "0dp",
                "zIndex": 1
            }, {}, {});
            flxViewAlertConfigs.setDefaultUnit(kony.flex.DP);
            var flxAlertConfigHeader = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "40dp",
                "id": "flxAlertConfigHeader",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0dp",
                "width": "100%"
            }, {}, {});
            flxAlertConfigHeader.setDefaultUnit(kony.flex.DP);
            var lblViewConfigName = new kony.ui.Label({
                "id": "lblViewConfigName",
                "isVisible": true,
                "left": "20px",
                "skin": "sknLbl16pxLato192B45",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmBusinessConfigurations.alertConfigurations\")",
                "top": "20px",
                "width": "80%",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var btnAdd = new kony.ui.Button({
                "bottom": "0px",
                "focusSkin": "sknbtnf7f7faLatoRegular12Px485c75Border1px485C75Radius20px",
                "height": "22px",
                "id": "btnAdd",
                "isVisible": true,
                "right": "20dp",
                "skin": "sknbtnf7f7faLatoRegular12Px485c75Border1px485C75Radius20px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.roles.Edit\")",
                "width": "52px",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_CENTER,
                "displayText": true,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {
                "hoverSkin": "sknbtnffffffLatoRegular12Px485c75Border1px485C75Radius20px"
            });
            flxAlertConfigHeader.add(lblViewConfigName, btnAdd);
            var flxSeprator = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                "clipBounds": true,
                "id": "flxSeprator",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "20dp",
                "width": "100%",
                "zIndex": 2
            }, {}, {});
            flxSeprator.setDefaultUnit(kony.flex.DP);
            var flxSeperatorInner = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "1px",
                "id": "flxSeperatorInner",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "20dp",
                "isModalContainer": false,
                "right": "20dp",
                "skin": "sknflx115678",
                "top": "0dp",
                "zIndex": 1
            }, {}, {});
            flxSeperatorInner.setDefaultUnit(kony.flex.DP);
            flxSeperatorInner.add();
            flxSeprator.add(flxSeperatorInner);
            var lblViewConfigDesc = new kony.ui.Label({
                "bottom": "20px",
                "id": "lblViewConfigDesc",
                "isVisible": true,
                "left": "20px",
                "right": "20px",
                "skin": "sknLbl485C75LatoRegular13Px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmBusinessConfigurations.alertConfigurationsDesc1\")",
                "top": "20px",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_TOP_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var flxViewChannelFrequency = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                "clipBounds": true,
                "id": "flxViewChannelFrequency",
                "isVisible": true,
                "layoutType": kony.flex.FLOW_HORIZONTAL,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0dp",
                "width": "100%"
            }, {}, {});
            flxViewChannelFrequency.setDefaultUnit(kony.flex.DP);
            var lblViewChannelFrequency = new kony.ui.Label({
                "id": "lblViewChannelFrequency",
                "isVisible": true,
                "left": "20px",
                "skin": "sknLbl192B45LatoRegular14px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmBusinessConfigurations.channelFrequency\")",
                "top": "0px",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var flxDefaultInfo = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                "clipBounds": true,
                "id": "flxDefaultInfo",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "5dp",
                "isModalContainer": false,
                "onClick": controller.AS_FlexContainer_aeb8168ea3624740bf06d583316c5596,
                "skin": "sknCursor",
                "top": 0,
                "width": "20dp",
                "zIndex": 2
            }, {}, {});
            flxDefaultInfo.setDefaultUnit(kony.flex.DP);
            var fontIconDefaultInfo = new kony.ui.Label({
                "id": "fontIconDefaultInfo",
                "isVisible": true,
                "left": "0dp",
                "skin": "sknfontIconInfo18Px",
                "text": "",
                "top": "0dp",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_TOP_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxDefaultInfo.add(fontIconDefaultInfo);
            flxViewChannelFrequency.add(lblViewChannelFrequency, flxDefaultInfo);
            var lblChannelFrequencyValue = new kony.ui.Label({
                "id": "lblChannelFrequencyValue",
                "isVisible": true,
                "left": "20dp",
                "right": "30px",
                "skin": "slLabel0d20174dce8ea42",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmBusinessConfigurations.alertLevel\")",
                "top": "10dp",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var lblContactConfigHeader = new kony.ui.Label({
                "id": "lblContactConfigHeader",
                "isVisible": true,
                "left": "20dp",
                "right": "30dp",
                "skin": "sknLbl192B45LatoRegular14px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmBusinessConfigurations.viewSeparateContact\")",
                "top": "30dp",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var lblContactConfigValue = new kony.ui.Label({
                "id": "lblContactConfigValue",
                "isVisible": true,
                "left": "20dp",
                "right": "30dp",
                "skin": "slLabel0d20174dce8ea42",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmBusinessConfigurations.notEnabled\")",
                "top": "10dp",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var lblFrequencyOptions = new kony.ui.Label({
                "id": "lblFrequencyOptions",
                "isVisible": true,
                "left": "20dp",
                "right": "30dp",
                "skin": "sknLbl192B45LatoRegular14px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmBusinessConfigurations.viewFrequencyOption\")",
                "top": "30dp",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var lblFrequencyValue = new kony.ui.Label({
                "id": "lblFrequencyValue",
                "isVisible": true,
                "left": "20dp",
                "right": "30dp",
                "skin": "slLabel0d20174dce8ea42",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmBusinessConfigurations.notEnabled\")",
                "top": "10dp",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxViewAlertConfigs.add(flxAlertConfigHeader, flxSeprator, lblViewConfigDesc, flxViewChannelFrequency, lblChannelFrequencyValue, lblContactConfigHeader, lblContactConfigValue, lblFrequencyOptions, lblFrequencyValue);
            var DefaultInfo = new com.adminConsole.Customers.ToolTip({
                "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                "id": "DefaultInfo",
                "isVisible": false,
                "layoutType": kony.flex.FREE_FORM,
                "left": "160dp",
                "masterType": constants.MASTER_TYPE_DEFAULT,
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "135dp",
                "width": "185dp",
                "overrides": {
                    "ToolTip": {
                        "isVisible": false,
                        "left": "160dp",
                        "top": "135dp",
                        "width": "185dp"
                    },
                    "flxToolTipMessage": {
                        "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                        "centerX": "viz.val_cleared",
                        "width": "100%",
                        "layoutType": kony.flex.FREE_FORM
                    },
                    "lblDownArrow": {
                        "centerX": "viz.val_cleared",
                        "left": "10px",
                        "top": "55dp"
                    },
                    "lblNoConcentToolTip": {
                        "bottom": "5px",
                        "height": kony.flex.USE_PREFFERED_SIZE,
                        "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmBusinessConfigurations.InfoMsg\")",
                        "left": "10px",
                        "right": "10px",
                        "top": "3px",
                        "width": kony.flex.USE_PREFFERED_SIZE
                    },
                    "lblarrow": {
                        "centerX": "viz.val_cleared",
                        "left": "10px",
                        "right": "viz.val_cleared"
                    }
                }
            }, {
                "overrides": {}
            }, {
                "overrides": {}
            });
            flxViewAlertConfigContainer.add(flxViewAlertConfigs, DefaultInfo);
            flxAlertConfigContainer.add(flxEditAlertConfigContainer, flxViewAlertConfigContainer);
            flxNoConfigurations.add(flxConfigurationsList, flxBusinessConfigSegment, noStaticData, flxNoRecordsFound, flxCriteriaStatusFilter, flxAlertConfigContainer);
            flxConfigurationsContainer.add(flxLeftOptions, flxVerticalSeprator, flxNoConfigurations);
            flxScrollMainContent.add(flxConfigurationsContainer);
            var flxHeaderDropdown = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                "clipBounds": true,
                "id": "flxHeaderDropdown",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "isModalContainer": false,
                "right": "35dp",
                "skin": "slFbox",
                "top": "20dp",
                "width": "400dp",
                "zIndex": 5
            }, {}, {});
            flxHeaderDropdown.setDefaultUnit(kony.flex.DP);
            var dropdownMainHeader = new com.adminConsole.common.dropdownMainHeader({
                "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                "id": "dropdownMainHeader",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "masterType": constants.MASTER_TYPE_DEFAULT,
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0dp",
                "width": "100%",
                "overrides": {
                    "flxDropdown": {
                        "isVisible": false
                    },
                    "imgUpArrow": {
                        "src": "uparrow_2x.png"
                    }
                }
            }, {
                "overrides": {}
            }, {
                "overrides": {}
            });
            flxHeaderDropdown.add(dropdownMainHeader);
            flxRightPanel.add(flxMainHeader, flxScrollMainContent, flxHeaderDropdown);
            var flxLoading = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "100%",
                "id": "flxLoading",
                "isVisible": false,
                "layoutType": kony.flex.FREE_FORM,
                "left": "305px",
                "isModalContainer": false,
                "right": "0px",
                "skin": "sknLoadingBlur",
                "top": "0dp",
                "zIndex": 10
            }, {}, {});
            flxLoading.setDefaultUnit(kony.flex.DP);
            var flxImageContainer = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "centerX": "50%",
                "centerY": "50%",
                "clipBounds": true,
                "height": "75px",
                "id": "flxImageContainer",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "isModalContainer": false,
                "skin": "slFbox",
                "width": "75px",
                "zIndex": 1
            }, {}, {});
            flxImageContainer.setDefaultUnit(kony.flex.DP);
            var imgLoading = new kony.ui.Image2({
                "centerX": "50%",
                "centerY": "50%",
                "height": "50px",
                "id": "imgLoading",
                "isVisible": true,
                "skin": "slImage",
                "src": "loadingscreenimage.gif",
                "width": "50px",
                "zIndex": 1
            }, {
                "imageScaleMode": constants.IMAGE_SCALE_MODE_MAINTAIN_ASPECT_RATIO,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxImageContainer.add(imgLoading);
            flxLoading.add(flxImageContainer);
            var flxToastMessage = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "bottom": "0%",
                "clipBounds": true,
                "height": "8%",
                "id": "flxToastMessage",
                "isVisible": false,
                "layoutType": kony.flex.FREE_FORM,
                "left": "305px",
                "isModalContainer": false,
                "skin": "slFbox",
                "width": "78%",
                "zIndex": 1
            }, {}, {});
            flxToastMessage.setDefaultUnit(kony.flex.DP);
            var toastMessage = new com.adminConsole.common.toastMessage({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "id": "toastMessage",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "masterType": constants.MASTER_TYPE_DEFAULT,
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0dp",
                "overrides": {}
            }, {
                "overrides": {}
            }, {
                "overrides": {}
            });
            flxToastMessage.add(toastMessage);
            var flxAddEligibityCriteria = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "100%",
                "id": "flxAddEligibityCriteria",
                "isVisible": false,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "sknFocus50",
                "top": "0dp",
                "width": "100%",
                "zIndex": 5
            }, {}, {});
            flxAddEligibityCriteria.setDefaultUnit(kony.flex.DP);
            var flxEligibilityEditPolicy = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                "centerX": "50%",
                "clipBounds": true,
                "id": "flxEligibilityEditPolicy",
                "isVisible": true,
                "layoutType": kony.flex.FLOW_VERTICAL,
                "left": "27%",
                "isModalContainer": false,
                "skin": "slFbox0j9f841cc563e4e",
                "top": "80px",
                "width": "650px",
                "zIndex": 1
            }, {}, {});
            flxEligibilityEditPolicy.setDefaultUnit(kony.flex.DP);
            var flxEligibilityTopColor = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "10px",
                "id": "flxEligibilityTopColor",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0px",
                "isModalContainer": false,
                "skin": "sknFlxTopColor4A77A0",
                "top": "0dp",
                "width": "100%",
                "zIndex": 1
            }, {}, {});
            flxEligibilityTopColor.setDefaultUnit(kony.flex.DP);
            flxEligibilityTopColor.add();
            var flxPopupHeader = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                "clipBounds": true,
                "id": "flxPopupHeader",
                "isVisible": true,
                "layoutType": kony.flex.FLOW_VERTICAL,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0px",
                "width": "100%",
                "zIndex": 1
            }, {}, {});
            flxPopupHeader.setDefaultUnit(kony.flex.DP);
            var flxEligibilityClose = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "25px",
                "id": "flxEligibilityClose",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "isModalContainer": false,
                "right": "13dp",
                "skin": "sknCursor",
                "top": "15dp",
                "width": "25px",
                "zIndex": 20
            }, {}, {
                "hoverSkin": "sknCursor"
            });
            flxEligibilityClose.setDefaultUnit(kony.flex.DP);
            var fontIconImgCLose = new kony.ui.Label({
                "centerX": "51%",
                "centerY": "50.00%",
                "height": "15dp",
                "id": "fontIconImgCLose",
                "isVisible": true,
                "left": "17dp",
                "skin": "sknFontIconSearchCross16px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmAlertsManagement.lblAlertPreviewClose\")",
                "width": "15dp",
                "zIndex": 2
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxEligibilityClose.add(fontIconImgCLose);
            var flxheaderEdit = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                "clipBounds": true,
                "id": "flxheaderEdit",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "-10dp",
                "width": "100%",
                "zIndex": 20
            }, {}, {});
            flxheaderEdit.setDefaultUnit(kony.flex.DP);
            var lblAddCriteria = new kony.ui.Label({
                "id": "lblAddCriteria",
                "isVisible": true,
                "left": "20dp",
                "skin": "sknlblLatoBold35475f23px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmBusinessConfigurations.addNewCriteria\")",
                "top": "0dp",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxheaderEdit.add(lblAddCriteria);
            var flxTextArea = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "200dp",
                "id": "flxTextArea",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "20dp",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "27dp",
                "width": "95%",
                "zIndex": 1
            }, {}, {});
            flxTextArea.setDefaultUnit(kony.flex.DP);
            var lblRoleDescription = new kony.ui.Label({
                "id": "lblRoleDescription",
                "isVisible": true,
                "left": "0px",
                "skin": "sknlbl485c7514px",
                "text": "Eligibility Criteria",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var lblRoleDescriptionSize = new kony.ui.Label({
                "id": "lblRoleDescriptionSize",
                "isVisible": true,
                "right": "0px",
                "skin": "sknlbl485c7514px",
                "text": "0/2000",
                "top": "0px",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var txtRoleDescription = new kony.ui.TextArea2({
                "autoCapitalize": constants.TEXTAREA_AUTO_CAPITALIZE_NONE,
                "focusSkin": "skntxtAreaLato9ca9ba12Px",
                "height": "170dp",
                "id": "txtRoleDescription",
                "isVisible": true,
                "keyBoardStyle": constants.TEXTAREA_KEY_BOARD_STYLE_DEFAULT,
                "left": "0dp",
                "maxTextLength": 2000,
                "numberOfVisibleLines": 3,
                "skin": "skntxtAreaLato35475f14Px",
                "textInputMode": constants.TEXTAREA_INPUT_MODE_ANY,
                "top": "30dp",
                "width": "100%",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_TOP_LEFT,
                "padding": [2, 2, 2, 2],
                "paddingInPixel": false
            }, {
                "autoCorrect": false
            });
            flxTextArea.add(lblRoleDescription, lblRoleDescriptionSize, txtRoleDescription);
            var flxNoCriteriaError = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "15px",
                "id": "flxNoCriteriaError",
                "isVisible": false,
                "layoutType": kony.flex.FREE_FORM,
                "left": "20px",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "5px",
                "width": "350px",
                "zIndex": 1
            }, {}, {});
            flxNoCriteriaError.setDefaultUnit(kony.flex.DP);
            var lblNoCriteriaErrorIcon = new kony.ui.Label({
                "height": "15px",
                "id": "lblNoCriteriaErrorIcon",
                "isVisible": true,
                "left": "0px",
                "skin": "sknErrorIcon",
                "text": "",
                "top": "0px",
                "width": "15px",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var lblNoCriteriaError = new kony.ui.Label({
                "centerY": "50%",
                "height": "15px",
                "id": "lblNoCriteriaError",
                "isVisible": true,
                "left": "15px",
                "skin": "sknlblError",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmBusinessConfigurations.criteriaCannotBeBlank\")",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxNoCriteriaError.add(lblNoCriteriaErrorIcon, lblNoCriteriaError);
            var flxPhraseStatus = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "40px",
                "id": "flxPhraseStatus",
                "isVisible": true,
                "layoutType": kony.flex.FLOW_HORIZONTAL,
                "left": "20px",
                "masterType": constants.MASTER_TYPE_USERWIDGET,
                "isModalContainer": false,
                "right": "35px",
                "skin": "slFbox0h43cfab67a134d",
                "top": 5,
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {}, {});
            flxPhraseStatus.setDefaultUnit(kony.flex.DP);
            var lblPhraseStatus = new kony.ui.Label({
                "centerY": "50%",
                "id": "lblPhraseStatus",
                "isVisible": true,
                "skin": "sknlblLatoRegular484b5213px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.permission.Active\")",
                "width": "50px",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var SwitchToggleStatus = new kony.ui.Switch({
                "centerY": "50%",
                "height": "25px",
                "id": "SwitchToggleStatus",
                "isVisible": true,
                "left": "0%",
                "leftSideText": "ON",
                "rightSideText": "OFF",
                "selectedIndex": 0,
                "skin": "sknSwitchServiceManagement",
                "width": "36dp",
                "zIndex": 1
            }, {
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxPhraseStatus.add(lblPhraseStatus, SwitchToggleStatus);
            flxPopupHeader.add(flxEligibilityClose, flxheaderEdit, flxTextArea, flxNoCriteriaError, flxPhraseStatus);
            var flxEditButtons = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "80px",
                "id": "flxEditButtons",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "isModalContainer": false,
                "skin": "sknFooter",
                "top": "0px",
                "width": "100%",
                "zIndex": 1
            }, {}, {});
            flxEditButtons.setDefaultUnit(kony.flex.DP);
            var flxError = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "centerY": "50%",
                "clipBounds": true,
                "height": "80px",
                "id": "flxError",
                "isVisible": false,
                "layoutType": kony.flex.FREE_FORM,
                "left": "10dp",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0dp",
                "width": "50%",
                "zIndex": 1
            }, {}, {});
            flxError.setDefaultUnit(kony.flex.DP);
            var lblErrorMsg = new kony.ui.Label({
                "centerY": "50%",
                "id": "lblErrorMsg",
                "isVisible": true,
                "left": "40dp",
                "skin": "sknlblError",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmPolicies.lblErrorMsg\")",
                "top": "0dp",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var imgError = new kony.ui.Image2({
                "centerY": "50%",
                "height": "15dp",
                "id": "imgError",
                "isVisible": true,
                "left": "15dp",
                "skin": "slImage",
                "src": "error_1x.png",
                "top": "0dp",
                "width": "15dp",
                "zIndex": 1
            }, {
                "imageScaleMode": constants.IMAGE_SCALE_MODE_MAINTAIN_ASPECT_RATIO,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxError.add(lblErrorMsg, imgError);
            var btnCancel = new kony.ui.Button({
                "centerY": "50%",
                "height": "40px",
                "id": "btnCancel",
                "isVisible": true,
                "onClick": controller.AS_Button_fe48adad632c43c5b97a05155a450331,
                "right": "135px",
                "skin": "sknbtnf7f7faLatoRegular4f555dBorder1px8b96a5",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmCustomers.CANCEL\")",
                "top": "0%",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_CENTER,
                "displayText": true,
                "padding": [5, 0, 5, 0],
                "paddingInPixel": false
            }, {
                "hoverSkin": "sknBtnLatoRegular8b96a513pxKAhover"
            });
            var btnsave = new kony.ui.Button({
                "centerY": "50%",
                "focusSkin": "sknBtn003E75LatoRegular13pxFFFFFFRad20px",
                "height": "40dp",
                "id": "btnsave",
                "isVisible": true,
                "onClick": controller.AS_Button_c973a89c85624425bb033af39fa13bd2,
                "right": "20px",
                "skin": "sknBtn003E75LatoRegular13pxFFFFFFRad20px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.permission.SAVE\")",
                "top": "0%",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_CENTER,
                "displayText": true,
                "padding": [5, 0, 5, 0],
                "paddingInPixel": false
            }, {
                "hoverSkin": "sknBtn005198LatoRegular13pxFFFFFFRad20px"
            });
            flxEditButtons.add(flxError, btnCancel, btnsave);
            flxEligibilityEditPolicy.add(flxEligibilityTopColor, flxPopupHeader, flxEditButtons);
            flxAddEligibityCriteria.add(flxEligibilityEditPolicy);
            flxMain.add(flxLeftPannel, flxRightPanel, flxLoading, flxToastMessage, flxAddEligibityCriteria);
            var flxDeactivateCriteria = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "100%",
                "id": "flxDeactivateCriteria",
                "isVisible": false,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "masterType": constants.MASTER_TYPE_USERWIDGET,
                "isModalContainer": false,
                "skin": "flxPopupTrans0ba858b5d5d304e",
                "top": "0dp",
                "width": "100%",
                "zIndex": 10
            }, {}, {});
            flxDeactivateCriteria.setDefaultUnit(kony.flex.DP);
            var popUpDeactivate = new com.adminConsole.common.popUp1({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "height": "100%",
                "id": "popUpDeactivate",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "masterType": constants.MASTER_TYPE_DEFAULT,
                "isModalContainer": false,
                "skin": "sknflxBg000000Op50",
                "top": "0dp",
                "width": "100%",
                "zIndex": 1,
                "overrides": {
                    "btnPopUpCancel": {
                        "i18n_text": "kony.i18n.getLocalizedString(\"i18n.PopUp.NoLeaveAsIS\")",
                        "minWidth": "viz.val_cleared",
                        "right": "20px",
                        "width": kony.flex.USE_PREFFERED_SIZE
                    },
                    "btnPopUpDelete": {
                        "i18n_text": "kony.i18n.getLocalizedString(\"i18n.PopUp.YesDeactivate\")",
                        "minWidth": "viz.val_cleared",
                        "width": kony.flex.USE_PREFFERED_SIZE
                    },
                    "popUp1": {
                        "bottom": "viz.val_cleared",
                        "centerX": "viz.val_cleared",
                        "centerY": "viz.val_cleared",
                        "height": "100%",
                        "left": "0dp",
                        "maxHeight": "viz.val_cleared",
                        "maxWidth": "viz.val_cleared",
                        "minHeight": "viz.val_cleared",
                        "minWidth": "viz.val_cleared",
                        "right": "viz.val_cleared",
                        "top": "0dp",
                        "width": "100%"
                    },
                    "rtxPopUpDisclaimer": {
                        "bottom": "30px",
                        "right": "viz.val_cleared",
                        "text": "Are you sure to deactivate the Branch?<br><br>If you deactivate it, the Branch does not appear for the customer.\n",
                        "width": "93%"
                    }
                }
            }, {
                "overrides": {}
            }, {
                "overrides": {}
            });
            flxDeactivateCriteria.add(popUpDeactivate);
            var flxDeleteCriteria = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "100%",
                "id": "flxDeleteCriteria",
                "isVisible": false,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "masterType": constants.MASTER_TYPE_USERWIDGET,
                "isModalContainer": false,
                "skin": "flxPopupTrans0ba858b5d5d304e",
                "top": "0dp",
                "width": "100%",
                "zIndex": 10
            }, {}, {});
            flxDeleteCriteria.setDefaultUnit(kony.flex.DP);
            var popUp = new com.adminConsole.common.popUp1({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "height": "100%",
                "id": "popUp",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "masterType": constants.MASTER_TYPE_DEFAULT,
                "isModalContainer": false,
                "skin": "sknflxBg000000Op50",
                "top": "0dp",
                "width": "100%",
                "zIndex": 1,
                "overrides": {
                    "btnPopUpCancel": {
                        "minWidth": "viz.val_cleared",
                        "right": "20px"
                    },
                    "btnPopUpDelete": {
                        "minWidth": "viz.val_cleared"
                    },
                    "popUp1": {
                        "bottom": "viz.val_cleared",
                        "centerX": "viz.val_cleared",
                        "centerY": "viz.val_cleared",
                        "height": "100%",
                        "left": "0dp",
                        "maxHeight": "viz.val_cleared",
                        "maxWidth": "viz.val_cleared",
                        "minHeight": "viz.val_cleared",
                        "minWidth": "viz.val_cleared",
                        "right": "viz.val_cleared",
                        "top": "0dp",
                        "width": "100%"
                    },
                    "rtxPopUpDisclaimer": {
                        "text": "Are you sure to deactivate the Branch?<br><br>If you deactivate it, the Branch does not appear for the customer.\n"
                    }
                }
            }, {
                "overrides": {}
            }, {
                "overrides": {}
            });
            flxDeleteCriteria.add(popUp);
            var flxErrorPopUp = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "100%",
                "id": "flxErrorPopUp",
                "isVisible": false,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "sknLoadingBlur",
                "top": "0dp",
                "width": "100%",
                "zIndex": 1
            }, {}, {});
            flxErrorPopUp.setDefaultUnit(kony.flex.DP);
            var ErrorPopUp = new com.adminConsole.common.popUp({
                "height": "100%",
                "id": "ErrorPopUp",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "masterType": constants.MASTER_TYPE_DEFAULT,
                "isModalContainer": false,
                "skin": "sknflxBg000000Op50",
                "top": "0dp",
                "width": "100%",
                "zIndex": 1,
                "overrides": {
                    "btnPopUpCancel": {
                        "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmErrorLogin.closeCAPS\")",
                        "right": "20px"
                    },
                    "btnPopUpDelete": {
                        "i18n_text": "kony.i18n.getLocalizedString(\"i18n.PopUp.YesDeactivate\")",
                        "isVisible": false
                    },
                    "flxPopUp": {
                        "width": "680px"
                    },
                    "lblPopUpMainMessage": {
                        "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmBusinessConfigurations.disableLinkingProfiles\")"
                    },
                    "rtxPopUpDisclaimer": {
                        "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmBusinessConfigurations.disableLinkingProfilesMsg\")"
                    }
                }
            }, {
                "overrides": {}
            }, {
                "overrides": {}
            });
            flxErrorPopUp.add(ErrorPopUp);
            this.add(flxMain, flxDeactivateCriteria, flxDeleteCriteria, flxErrorPopUp);
        };
        return [{
            "addWidgets": addWidgetsfrmBusinessConfigurations,
            "enabledForIdleTimeout": true,
            "id": "frmBusinessConfigurations",
            "layoutType": kony.flex.FREE_FORM,
            "needAppMenu": false,
            "preShow": function(eventobject) {
                controller.AS_Form_g14bd2761e69461c9a1274dd36e599e3(eventobject);
            },
            "skin": "slForm"
        }, {
            "displayOrientation": constants.FORM_DISPLAY_ORIENTATION_PORTRAIT,
            "layoutType": kony.flex.FREE_FORM,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, {
            "onDeviceBack": controller.AS_Form_ddea7c4974584bec88a894ace7f20cb2,
            "retainScrollPosition": false
        }]
    }
});