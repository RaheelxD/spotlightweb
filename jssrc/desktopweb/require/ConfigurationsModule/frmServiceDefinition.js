define("ConfigurationsModule/frmServiceDefinition", function() {
    return function(controller) {
        function addWidgetsfrmServiceDefinition() {
            this.setDefaultUnit(kony.flex.DP);
            var flxMain = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "900dp",
                "id": "flxMain",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "slFbox0b7aaa2e3bfa340",
                "top": "0dp",
                "width": "100%",
                "zIndex": 1
            }, {}, {});
            flxMain.setDefaultUnit(kony.flex.DP);
            var flxLeftPannel = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "100%",
                "id": "flxLeftPannel",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "sknLeftPannel0bb79e4bcca8e45",
                "top": "0dp",
                "width": "305px",
                "zIndex": 5
            }, {}, {});
            flxLeftPannel.setDefaultUnit(kony.flex.DP);
            var leftMenuNew = new com.adminConsole.navigation.leftMenuNew({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "height": "100%",
                "id": "leftMenuNew",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "masterType": constants.MASTER_TYPE_DEFAULT,
                "isModalContainer": false,
                "skin": "sknFlxBg003E75Op100",
                "top": "0dp",
                "width": "100%",
                "overrides": {
                    "brandLogo": {
                        "src": "infinity_dbx_c360_logo_white_2x.png"
                    },
                    "imgBottomLogo": {
                        "src": "logo_white_2x.png"
                    },
                    "leftMenuNew": {
                        "bottom": "viz.val_cleared",
                        "centerX": "viz.val_cleared",
                        "centerY": "viz.val_cleared",
                        "height": "100%",
                        "left": "0dp",
                        "maxHeight": "viz.val_cleared",
                        "maxWidth": "viz.val_cleared",
                        "minHeight": "viz.val_cleared",
                        "minWidth": "viz.val_cleared",
                        "right": "viz.val_cleared",
                        "top": "0dp",
                        "width": "100%"
                    },
                    "segMenu": {
                        "left": "0dp",
                        "top": "0dp"
                    }
                }
            }, {
                "overrides": {}
            }, {
                "overrides": {}
            });
            flxLeftPannel.add(leftMenuNew);
            var flxRightPannel = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "100%",
                "id": "flxRightPannel",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "305px",
                "isModalContainer": false,
                "right": 0,
                "skin": "slFbox0b7aaa2e3bfa340",
                "top": "0.00%",
                "zIndex": 5
            }, {}, {});
            flxRightPannel.setDefaultUnit(kony.flex.DP);
            var flxMainHeader = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "106dp",
                "id": "flxMainHeader",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0dp",
                "width": "100%",
                "zIndex": 1
            }, {}, {});
            flxMainHeader.setDefaultUnit(kony.flex.DP);
            var mainHeader = new com.adminConsole.header.mainHeader({
                "height": "106px",
                "id": "mainHeader",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0",
                "masterType": constants.MASTER_TYPE_DEFAULT,
                "isModalContainer": false,
                "skin": "CopyslFbox1",
                "top": "0",
                "width": "100%",
                "overrides": {
                    "btnAddNewOption": {
                        "i18n_text": "kony.i18n.getLocalizedString(\"i18n.AddServiceDef.AddServiceDefinition\")",
                        "right": "0dp"
                    },
                    "btnDropdownList": {
                        "i18n_text": "kony.i18n.getLocalizedString(\"i18n.mainHeader.DOWNLOADLIST\")",
                        "isVisible": false
                    },
                    "flxAddNewOption": {
                        "isVisible": false
                    },
                    "flxHeaderSeperator": {
                        "isVisible": true
                    },
                    "imgLogout": {
                        "right": "0px",
                        "src": "img_logout.png"
                    },
                    "lblHeading": {
                        "text": "Service Definition Configurations"
                    },
                    "lblUserName": {
                        "right": "25px",
                        "text": "Preetish",
                        "top": "viz.val_cleared"
                    },
                    "mainHeader": {
                        "left": "0",
                        "top": "0"
                    }
                }
            }, {
                "overrides": {}
            }, {
                "overrides": {}
            });
            flxMainHeader.add(mainHeader);
            var flxBreadCrumbs = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "30px",
                "id": "flxBreadCrumbs",
                "isVisible": false,
                "layoutType": kony.flex.FLOW_HORIZONTAL,
                "left": "0dp",
                "masterType": constants.MASTER_TYPE_USERWIDGET,
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "93px",
                "width": "100%",
                "zIndex": 1
            }, {}, {});
            flxBreadCrumbs.setDefaultUnit(kony.flex.DP);
            var breadcrumbs = new com.adminConsole.common.breadcrumbs({
                "centerY": "50%",
                "height": "20px",
                "id": "breadcrumbs",
                "isVisible": true,
                "layoutType": kony.flex.FLOW_HORIZONTAL,
                "left": "0dp",
                "masterType": constants.MASTER_TYPE_DEFAULT,
                "isModalContainer": false,
                "skin": "sknflxf5f6f8Op100",
                "top": "20dp",
                "width": "100%",
                "overrides": {
                    "breadcrumbs": {
                        "centerY": "50%",
                        "top": "20dp"
                    },
                    "btnBackToMain": {
                        "text": "CUSTOMER ROLES"
                    },
                    "btnPreviousPage": {
                        "isVisible": false
                    },
                    "imgBreadcrumbsDown": {
                        "src": "img_down_arrow.png"
                    },
                    "imgBreadcrumbsRight": {
                        "src": "img_breadcrumb_arrow.png"
                    },
                    "imgBreadcrumbsRight2": {
                        "src": "img_breadcrumb_arrow.png"
                    },
                    "imgBreadcrumbsRight3": {
                        "src": "img_breadcrumb_arrow.png"
                    }
                }
            }, {
                "overrides": {}
            }, {
                "overrides": {}
            });
            flxBreadCrumbs.add(breadcrumbs);
            var flxToastMessage = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "bottom": "-70px",
                "clipBounds": true,
                "height": "70px",
                "id": "flxToastMessage",
                "isVisible": false,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0px",
                "isModalContainer": false,
                "right": "0px",
                "skin": "slFbox",
                "zIndex": 99
            }, {}, {});
            flxToastMessage.setDefaultUnit(kony.flex.DP);
            var toastMessage = new com.adminConsole.common.toastMessage({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "height": "70px",
                "id": "toastMessage",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "masterType": constants.MASTER_TYPE_DEFAULT,
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0dp",
                "width": "100%",
                "overrides": {
                    "lbltoastMessage": {
                        "text": "Branch deactivated successfully"
                    }
                }
            }, {
                "overrides": {}
            }, {
                "overrides": {}
            });
            flxToastMessage.add(toastMessage);
            var flxNoGroups = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                "clipBounds": true,
                "id": "flxNoGroups",
                "isVisible": false,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "106dp",
                "width": "100%",
                "zIndex": 1
            }, {}, {});
            flxNoGroups.setDefaultUnit(kony.flex.DP);
            var noStaticData = new com.adminConsole.staticContent.noStaticData({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "id": "noStaticData",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "masterType": constants.MASTER_TYPE_DEFAULT,
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0dp",
                "zIndex": 1,
                "overrides": {
                    "btnAddStaticContent": {
                        "i18n_text": "kony.i18n.getLocalizedString(\"i18n.DragBox.ADD\")",
                        "width": "70dp"
                    },
                    "lblNoStaticContentCreated": {
                        "text": "No Service Definition added yet"
                    },
                    "lblNoStaticContentMsg": {
                        "i18n_text": "kony.i18n.getLocalizedString(\"i18n.ServiceDefinition.AddServiceDefinition\")"
                    }
                }
            }, {
                "overrides": {}
            }, {
                "overrides": {}
            });
            flxNoGroups.add(noStaticData);
            var flxGroupList = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "bottom": "0px",
                "clipBounds": true,
                "id": "flxGroupList",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "35px",
                "isModalContainer": false,
                "right": "35px",
                "skin": "slFbox",
                "top": "105px",
                "zIndex": 1
            }, {}, {});
            flxGroupList.setDefaultUnit(kony.flex.DP);
            var flxGroupListMain = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "bottom": "0dp",
                "clipBounds": true,
                "id": "flxGroupListMain",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0dp",
                "width": "100%",
                "zIndex": 1
            }, {}, {});
            flxGroupListMain.setDefaultUnit(kony.flex.DP);
            var flxGroupSearch = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "60dp",
                "id": "flxGroupSearch",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0dp",
                "width": "100%",
                "zIndex": 1
            }, {}, {});
            flxGroupSearch.setDefaultUnit(kony.flex.DP);
            var subHeader = new com.adminConsole.header.subHeader({
                "centerY": "50%",
                "height": "40dp",
                "id": "subHeader",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "masterType": constants.MASTER_TYPE_DEFAULT,
                "isModalContainer": false,
                "skin": "CopyslFbox2",
                "width": "100%",
                "overrides": {
                    "flxMenu": {
                        "isVisible": false,
                        "left": "0dp"
                    },
                    "flxSearch": {
                        "right": 0
                    },
                    "flxSearchContainer": {
                        "centerY": "50%",
                        "height": "40px"
                    },
                    "flxSubHeader": {
                        "centerY": "viz.val_cleared"
                    },
                    "subHeader": {
                        "centerY": "50%",
                        "height": "40dp",
                        "maxHeight": "viz.val_cleared",
                        "top": "viz.val_cleared"
                    },
                    "tbxSearchBox": {
                        "i18n_placeholder": "kony.i18n.getLocalizedString(\"i18n.frmGroups.Search_by_Group_Name\")"
                    }
                }
            }, {
                "overrides": {}
            }, {
                "overrides": {}
            });
            flxGroupSearch.add(subHeader);
            var flxSegGroups = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "bottom": "0dp",
                "clipBounds": true,
                "id": "flxSegGroups",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "sknflxffffffOp100BorderE1E5Ed",
                "top": "60dp",
                "width": "100%",
                "zIndex": 1
            }, {}, {});
            flxSegGroups.setDefaultUnit(kony.flex.DP);
            var flxGroupHeader = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "60px",
                "id": "flxGroupHeader",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "20px",
                "isModalContainer": false,
                "right": "20px",
                "skin": "sknflxffffffop100",
                "top": "0"
            }, {}, {});
            flxGroupHeader.setDefaultUnit(kony.flex.DP);
            var flxGroupName = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "centerY": "50%",
                "clipBounds": true,
                "height": "100%",
                "id": "flxGroupName",
                "isVisible": true,
                "layoutType": kony.flex.FLOW_HORIZONTAL,
                "left": "1%",
                "isModalContainer": false,
                "onClick": controller.AS_FlexContainer_aa9ff028c98444408c909813010b2efa,
                "skin": "slFbox",
                "top": 0,
                "width": "20.60%",
                "zIndex": 1
            }, {}, {});
            flxGroupName.setDefaultUnit(kony.flex.DP);
            var lblGroupName = new kony.ui.Label({
                "centerY": "50%",
                "id": "lblGroupName",
                "isVisible": true,
                "left": "24dp",
                "skin": "sknlblLato696c7312px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmServiceManagementController.SERVICES\")",
                "top": 0,
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {
                "hoverSkin": "sknlblLato00000011px"
            });
            var fontIconSortName = new kony.ui.Label({
                "centerY": "50%",
                "id": "fontIconSortName",
                "isVisible": true,
                "left": "10dp",
                "skin": "sknIcon15px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmAlertsManagement.lblSortName\")",
                "top": "0dp",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxGroupName.add(lblGroupName, fontIconSortName);
            var flxGroupType = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "centerY": "50%",
                "clipBounds": true,
                "height": "100%",
                "id": "flxGroupType",
                "isVisible": true,
                "layoutType": kony.flex.FLOW_HORIZONTAL,
                "left": "25.80%",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0dp",
                "width": "12%",
                "zIndex": 1
            }, {}, {});
            flxGroupType.setDefaultUnit(kony.flex.DP);
            var lblHeaderGroupType = new kony.ui.Label({
                "centerY": "50%",
                "id": "lblHeaderGroupType",
                "isVisible": true,
                "left": "0px",
                "skin": "sknlblLato696c7312px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmServiceDefinition.ServiceType\")",
                "top": 0,
                "width": "80px",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var fontIconFilterGrpType = new kony.ui.Label({
                "centerY": "50%",
                "id": "fontIconFilterGrpType",
                "isVisible": true,
                "left": "10dp",
                "skin": "sknIcon15px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmAlertsManagement.lblIconChannels\")",
                "top": "0dp",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {
                "hoverSkin": "sknlblCursorFont"
            });
            flxGroupType.add(lblHeaderGroupType, fontIconFilterGrpType);
            var flxContracts = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "centerY": "50%",
                "clipBounds": true,
                "height": "100%",
                "id": "flxContracts",
                "isVisible": true,
                "layoutType": kony.flex.FLOW_HORIZONTAL,
                "left": "42%",
                "isModalContainer": false,
                "onClick": controller.AS_FlexContainer_d0c82a1545084c569df06fa1ca405fa6,
                "skin": "slFbox",
                "top": "0dp",
                "width": "14.37%",
                "zIndex": 1
            }, {}, {});
            flxContracts.setDefaultUnit(kony.flex.DP);
            var lblContracts = new kony.ui.Label({
                "centerY": "50%",
                "id": "lblContracts",
                "isVisible": true,
                "left": "0px",
                "skin": "sknlblLato696c7312px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmServiceDefinition.Contracts\")",
                "top": 0,
                "width": "115dp",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {
                "hoverSkin": "sknlblLato00000011px"
            });
            var fontIconSortContracts = new kony.ui.Label({
                "centerY": "50%",
                "id": "fontIconSortContracts",
                "isVisible": true,
                "left": "10dp",
                "skin": "sknIcon15px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmAlertsManagement.lblSortName\")",
                "top": "0dp",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxContracts.add(lblContracts, fontIconSortContracts);
            var flxGroupFeatures = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "centerY": "50%",
                "clipBounds": true,
                "height": "100%",
                "id": "flxGroupFeatures",
                "isVisible": true,
                "layoutType": kony.flex.FLOW_HORIZONTAL,
                "left": "59%",
                "isModalContainer": false,
                "onClick": controller.AS_FlexContainer_d0c82a1545084c569df06fa1ca405fa6,
                "skin": "slFbox",
                "top": "0dp",
                "width": "15%",
                "zIndex": 1
            }, {}, {});
            flxGroupFeatures.setDefaultUnit(kony.flex.DP);
            var lblGroupFeatures = new kony.ui.Label({
                "centerY": "50%",
                "id": "lblGroupFeatures",
                "isVisible": true,
                "left": "0px",
                "skin": "sknlblLato696c7312px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmServiceDefinition.Features\")",
                "top": 0,
                "width": "103dp",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {
                "hoverSkin": "sknlblLato00000011px"
            });
            var fontIconSortFeatures = new kony.ui.Label({
                "centerY": "50%",
                "id": "fontIconSortFeatures",
                "isVisible": true,
                "left": "10dp",
                "skin": "sknIcon15px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmAlertsManagement.lblSortName\")",
                "top": "0dp",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxGroupFeatures.add(lblGroupFeatures, fontIconSortFeatures);
            var flxRoles = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "centerY": "50%",
                "clipBounds": true,
                "height": "100%",
                "id": "flxRoles",
                "isVisible": true,
                "layoutType": kony.flex.FLOW_HORIZONTAL,
                "left": "74.50%",
                "isModalContainer": false,
                "onClick": controller.AS_FlexContainer_d0c82a1545084c569df06fa1ca405fa6,
                "skin": "slFbox",
                "top": "0dp",
                "width": "12%",
                "zIndex": 1
            }, {}, {});
            flxRoles.setDefaultUnit(kony.flex.DP);
            var lblRoles = new kony.ui.Label({
                "centerY": "50%",
                "id": "lblRoles",
                "isVisible": true,
                "left": "0px",
                "skin": "sknlblLato696c7312px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmServiceDefinition.Roles\")",
                "top": 0,
                "width": "85px",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {
                "hoverSkin": "sknlblLato00000011px"
            });
            var fontIconSortRoles = new kony.ui.Label({
                "centerY": "50%",
                "id": "fontIconSortRoles",
                "isVisible": true,
                "left": "10dp",
                "skin": "sknIcon15px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmAlertsManagement.lblSortName\")",
                "top": "0dp",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxRoles.add(lblRoles, fontIconSortRoles);
            var flxGroupHeaderStatus = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "centerY": "50%",
                "clipBounds": true,
                "height": "100%",
                "id": "flxGroupHeaderStatus",
                "isVisible": true,
                "layoutType": kony.flex.FLOW_HORIZONTAL,
                "left": "88.50%",
                "isModalContainer": false,
                "onClick": controller.AS_FlexContainer_d3a97a0f3de34380a91ef7e725e093be,
                "skin": "slFbox",
                "top": "0dp",
                "width": "9.16%",
                "zIndex": 1
            }, {}, {});
            flxGroupHeaderStatus.setDefaultUnit(kony.flex.DP);
            var lblGroupStatus = new kony.ui.Label({
                "centerY": "50%",
                "id": "lblGroupStatus",
                "isVisible": true,
                "left": "0px",
                "skin": "sknlblLato696c7312px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.permission.STATUS\")",
                "top": 0,
                "width": "46px",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {
                "hoverSkin": "sknlblLato00000011px"
            });
            var fontIconGroupStatusFilter = new kony.ui.Label({
                "centerY": "50%",
                "id": "fontIconGroupStatusFilter",
                "isVisible": true,
                "left": "2.50%",
                "skin": "sknIcon15px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmAlertsManagement.lblIconChannels\")",
                "top": "0dp",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {
                "hoverSkin": "sknlblCursorFont"
            });
            flxGroupHeaderStatus.add(lblGroupStatus, fontIconGroupStatusFilter);
            flxGroupHeader.add(flxGroupName, flxGroupType, flxContracts, flxGroupFeatures, flxRoles, flxGroupHeaderStatus);
            var flxGroupSeparator = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "1dp",
                "id": "flxGroupSeparator",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "20dp",
                "isModalContainer": false,
                "right": "20dp",
                "skin": "sknTableHeaderLine",
                "top": "60dp",
                "zIndex": 1
            }, {}, {});
            flxGroupSeparator.setDefaultUnit(kony.flex.DP);
            flxGroupSeparator.add();
            var flxGroupListContainer = new kony.ui.FlexContainer({
                "bottom": "10px",
                "clipBounds": true,
                "height": "676dp",
                "id": "flxGroupListContainer",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "61dp",
                "width": "100%",
                "zIndex": 1
            }, {}, {});
            flxGroupListContainer.setDefaultUnit(kony.flex.DP);
            var listingSegmentClient = new com.adminConsole.common.listingSegmentClient({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "height": "100%",
                "id": "listingSegmentClient",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "masterType": constants.MASTER_TYPE_DEFAULT,
                "isModalContainer": false,
                "right": "0dp",
                "skin": "slFbox",
                "top": "0dp",
                "overrides": {
                    "contextualMenu.btnLink1": {
                        "bottom": "10dp",
                        "isVisible": false,
                        "top": "0dp"
                    },
                    "contextualMenu.btnLink2": {
                        "bottom": "10dp",
                        "isVisible": false,
                        "top": "0dp"
                    },
                    "contextualMenu.contextualMenu1Inner": {
                        "layoutType": kony.flex.FREE_FORM
                    },
                    "contextualMenu.flxOption1": {
                        "isVisible": false,
                        "top": "0dp"
                    },
                    "contextualMenu.flxOption2": {
                        "height": "30dp",
                        "top": "15dp"
                    },
                    "contextualMenu.flxOption3": {
                        "bottom": "10dp",
                        "height": "30dp",
                        "isVisible": true,
                        "top": "48dp"
                    },
                    "contextualMenu.flxOption4": {
                        "bottom": "viz.val_cleared",
                        "height": "30dp",
                        "top": "80dp"
                    },
                    "contextualMenu.flxOptionsSeperator": {
                        "bottom": "5dp",
                        "isVisible": false,
                        "top": "85px"
                    },
                    "contextualMenu.imgDownArrow": {
                        "src": "downarrow_2x.png"
                    },
                    "contextualMenu.imgOption1": {
                        "src": "imagedrag.png"
                    },
                    "contextualMenu.imgOption2": {
                        "src": "edit2x.png"
                    },
                    "contextualMenu.imgOption3": {
                        "isVisible": true,
                        "left": "20dp",
                        "src": "delete_2x.png",
                        "width": "20dp"
                    },
                    "contextualMenu.imgOption4": {
                        "src": "deactive_2x.png"
                    },
                    "contextualMenu.imgUpArrow": {
                        "src": "uparrow_2x.png"
                    },
                    "contextualMenu.lblHeader": {
                        "bottom": "15dp",
                        "isVisible": false,
                        "top": "20dp"
                    },
                    "contextualMenu.lblIconOption2": {
                        "i18n_text": "kony.i18n.getLocalizedString(\"i18n.userwidgetmodel.lblIconOption1\")",
                        "left": "20dp"
                    },
                    "contextualMenu.lblIconOption3": {
                        "isVisible": false,
                        "left": "20dp",
                        "text": ""
                    },
                    "contextualMenu.lblIconOption4": {
                        "left": "20dp"
                    },
                    "contextualMenu.lblOption3": {
                        "text": "Delete"
                    },
                    "flxContextualMenu": {
                        "left": "viz.val_cleared",
                        "right": "20dp",
                        "width": "137px"
                    },
                    "flxListingSegmentWrapper": {
                        "bottom": "viz.val_cleared",
                        "height": "100%"
                    },
                    "flxPagination": {
                        "isVisible": false
                    },
                    "listingSegmentClient": {
                        "bottom": "viz.val_cleared",
                        "centerX": "viz.val_cleared",
                        "centerY": "viz.val_cleared",
                        "height": "100%",
                        "left": "0dp",
                        "maxHeight": "viz.val_cleared",
                        "maxWidth": "viz.val_cleared",
                        "minHeight": "viz.val_cleared",
                        "minWidth": "viz.val_cleared",
                        "right": "0dp",
                        "top": "0dp",
                        "width": "viz.val_cleared"
                    },
                    "pagination": {
                        "isVisible": false
                    },
                    "segListing": {
                        "bottom": "viz.val_cleared",
                        "height": "100%",
                        "left": "0px",
                        "top": "0px"
                    }
                }
            }, {
                "overrides": {}
            }, {
                "overrides": {}
            });
            flxGroupListContainer.add(listingSegmentClient);
            var flxStatusFilter = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                "clipBounds": true,
                "id": "flxStatusFilter",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "isModalContainer": false,
                "right": "80px",
                "skin": "slFbox",
                "top": "37px",
                "width": "120px",
                "zIndex": 5
            }, {}, {});
            flxStatusFilter.setDefaultUnit(kony.flex.DP);
            var statusFilterMenu = new com.adminConsole.common.statusFilterMenu({
                "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                "id": "statusFilterMenu",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "masterType": constants.MASTER_TYPE_DEFAULT,
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "5dp",
                "width": "100%",
                "zIndex": 5,
                "overrides": {
                    "imgUpArrow": {
                        "src": "uparrow_2x.png"
                    },
                    "segStatusFilterDropdown": {
                        "left": "0dp",
                        "top": "5dp"
                    }
                }
            }, {
                "overrides": {}
            }, {
                "overrides": {}
            });
            flxStatusFilter.add(statusFilterMenu);
            var flxTypeFilter = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                "clipBounds": true,
                "id": "flxTypeFilter",
                "isVisible": false,
                "layoutType": kony.flex.FREE_FORM,
                "isModalContainer": false,
                "right": "200px",
                "skin": "slFbox",
                "top": "37px",
                "width": "120px",
                "zIndex": 5
            }, {}, {});
            flxTypeFilter.setDefaultUnit(kony.flex.DP);
            var typeFilterMenu = new com.adminConsole.common.statusFilterMenu({
                "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                "id": "typeFilterMenu",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "masterType": constants.MASTER_TYPE_DEFAULT,
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "5dp",
                "width": "100%",
                "zIndex": 5,
                "overrides": {
                    "imgUpArrow": {
                        "src": "uparrow_2x.png"
                    },
                    "segStatusFilterDropdown": {
                        "left": "0dp",
                        "top": "5dp"
                    }
                }
            }, {
                "overrides": {}
            }, {
                "overrides": {}
            });
            flxTypeFilter.add(typeFilterMenu);
            var flxBusinessTypeFilter = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                "clipBounds": true,
                "id": "flxBusinessTypeFilter",
                "isVisible": false,
                "layoutType": kony.flex.FREE_FORM,
                "isModalContainer": false,
                "right": "150px",
                "skin": "slFbox",
                "top": "47px",
                "width": "170px",
                "zIndex": 5
            }, {}, {});
            flxBusinessTypeFilter.setDefaultUnit(kony.flex.DP);
            var BusinessTypeFilterMenu = new com.adminConsole.common.statusFilterMenu({
                "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                "id": "BusinessTypeFilterMenu",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "masterType": constants.MASTER_TYPE_DEFAULT,
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "5dp",
                "width": "100%",
                "zIndex": 5,
                "overrides": {
                    "imgUpArrow": {
                        "src": "uparrow_2x.png"
                    },
                    "segStatusFilterDropdown": {
                        "left": "0dp",
                        "top": "5dp"
                    }
                }
            }, {
                "overrides": {}
            }, {
                "overrides": {}
            });
            flxBusinessTypeFilter.add(BusinessTypeFilterMenu);
            var flxSegMore = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                "clipBounds": true,
                "id": "flxSegMore",
                "isVisible": false,
                "layoutType": kony.flex.FLOW_VERTICAL,
                "left": "170px",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "190px",
                "width": "250px",
                "zIndex": 5
            }, {}, {});
            flxSegMore.setDefaultUnit(kony.flex.DP);
            var flxUpArrowImage = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "12px",
                "id": "flxUpArrowImage",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0px",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0px",
                "width": "100%",
                "zIndex": 2
            }, {}, {});
            flxUpArrowImage.setDefaultUnit(kony.flex.DP);
            var imgUpArrow = new kony.ui.Image2({
                "centerX": "50%",
                "height": "12dp",
                "id": "imgUpArrow",
                "isVisible": true,
                "skin": "slImage",
                "src": "uparrow_2x.png",
                "top": "0dp",
                "width": "15dp",
                "zIndex": 1
            }, {
                "imageScaleMode": constants.IMAGE_SCALE_MODE_FIT_TO_DIMENSIONS,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxUpArrowImage.add(imgUpArrow);
            var flxBusinessTypesOuter = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                "clipBounds": true,
                "id": "flxBusinessTypesOuter",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "sknflxffffffop100dbdbe6Radius3px",
                "top": "-1dp",
                "width": "100%",
                "zIndex": 1
            }, {}, {});
            flxBusinessTypesOuter.setDefaultUnit(kony.flex.DP);
            var segBusinessTypes = new kony.ui.SegmentedUI2({
                "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                "bottom": 10,
                "data": [{
                    "lblName": "Granville Street, Vancover, BC, Canada"
                }, {
                    "lblName": "Granville Street, Vancover, BC, Canada"
                }, {
                    "lblName": "Granville Street, Vancover, BC, Canada"
                }],
                "groupCells": false,
                "id": "segBusinessTypes",
                "isVisible": true,
                "left": "15dp",
                "needPageIndicator": true,
                "pageOffDotImage": "pageoffdot.png",
                "pageOnDotImage": "pageondot.png",
                "retainSelection": false,
                "right": "15px",
                "rowFocusSkin": "seg2Focus",
                "rowSkin": "seg2Normal",
                "rowTemplate": "flxMore",
                "sectionHeaderSkin": "sliPhoneSegmentHeader",
                "selectionBehavior": constants.SEGUI_DEFAULT_BEHAVIOR,
                "separatorColor": "aaaaaa00",
                "separatorRequired": false,
                "separatorThickness": 0,
                "showScrollbars": false,
                "top": "0dp",
                "viewType": constants.SEGUI_VIEW_TYPE_TABLEVIEW,
                "widgetDataMap": {
                    "flxMore": "flxMore",
                    "lblName": "lblName"
                },
                "zIndex": 1
            }, {
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxBusinessTypesOuter.add(segBusinessTypes);
            var flxDownArrowImage = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "12px",
                "id": "flxDownArrowImage",
                "isVisible": false,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0px",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "-1px",
                "width": "100%",
                "zIndex": 2
            }, {}, {});
            flxDownArrowImage.setDefaultUnit(kony.flex.DP);
            var imgDownArrow = new kony.ui.Image2({
                "centerX": "50%",
                "height": "12dp",
                "id": "imgDownArrow",
                "isVisible": true,
                "right": "15dp",
                "skin": "slImage",
                "src": "downarrow_2x.png",
                "top": "0dp",
                "width": "15dp",
                "zIndex": 1
            }, {
                "imageScaleMode": constants.IMAGE_SCALE_MODE_FIT_TO_DIMENSIONS,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxDownArrowImage.add(imgDownArrow);
            flxSegMore.add(flxUpArrowImage, flxBusinessTypesOuter, flxDownArrowImage);
            flxSegGroups.add(flxGroupHeader, flxGroupSeparator, flxGroupListContainer, flxStatusFilter, flxTypeFilter, flxBusinessTypeFilter, flxSegMore);
            flxGroupListMain.add(flxGroupSearch, flxSegGroups);
            flxGroupList.add(flxGroupListMain);
            var flxHeaderDropdown = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                "clipBounds": true,
                "id": "flxHeaderDropdown",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "isModalContainer": false,
                "right": "35dp",
                "skin": "slFbox",
                "top": "20dp",
                "width": "400dp",
                "zIndex": 5
            }, {}, {});
            flxHeaderDropdown.setDefaultUnit(kony.flex.DP);
            var dropdownMainHeader = new com.adminConsole.common.dropdownMainHeader({
                "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                "id": "dropdownMainHeader",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "masterType": constants.MASTER_TYPE_DEFAULT,
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0dp",
                "width": "100%",
                "overrides": {
                    "flxDropdown": {
                        "isVisible": false
                    },
                    "imgUpArrow": {
                        "src": "uparrow_2x.png"
                    }
                }
            }, {
                "overrides": {}
            }, {
                "overrides": {}
            });
            flxHeaderDropdown.add(dropdownMainHeader);
            var flxViewServiceDef = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "bottom": "20dp",
                "clipBounds": true,
                "id": "flxViewServiceDef",
                "isVisible": false,
                "layoutType": kony.flex.FREE_FORM,
                "left": "35dp",
                "isModalContainer": false,
                "right": "35dp",
                "skin": "slFbox",
                "top": "129dp",
                "zIndex": 1
            }, {}, {});
            flxViewServiceDef.setDefaultUnit(kony.flex.DP);
            var flxGroupViewServiceDef = new kony.ui.FlexScrollContainer({
                "allowHorizontalBounce": false,
                "allowVerticalBounce": true,
                "bottom": "0px",
                "bounces": true,
                "clipBounds": true,
                "enableScrolling": true,
                "horizontalScrollIndicator": true,
                "id": "flxGroupViewServiceDef",
                "isVisible": true,
                "layoutType": kony.flex.FLOW_VERTICAL,
                "left": "0dp",
                "pagingEnabled": false,
                "right": "0px",
                "scrollDirection": kony.flex.SCROLL_VERTICAL,
                "skin": "sknFlxBgFFFFFFbrE4E6EC1pxr4px",
                "top": "0dp",
                "verticalScrollIndicator": true,
                "zIndex": 1
            }, {}, {});
            flxGroupViewServiceDef.setDefaultUnit(kony.flex.DP);
            var flxViewContainer = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                "clipBounds": true,
                "id": "flxViewContainer",
                "isVisible": true,
                "layoutType": kony.flex.FLOW_VERTICAL,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0dp",
                "width": "100%",
                "zIndex": 1
            }, {}, {});
            flxViewContainer.setDefaultUnit(kony.flex.DP);
            var flxViewHeader = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": false,
                "height": "60dp",
                "id": "flxViewHeader",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0px",
                "isModalContainer": false,
                "right": "35px",
                "skin": "slFbox",
                "top": "0px",
                "zIndex": 2
            }, {}, {});
            flxViewHeader.setDefaultUnit(kony.flex.DP);
            var flxServiceDefNameHeader = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                "centerY": "50%",
                "clipBounds": true,
                "id": "flxServiceDefNameHeader",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "20dp",
                "isModalContainer": false,
                "right": "50dp",
                "skin": "slFbox",
                "width": kony.flex.USE_PREFFERED_SIZE
            }, {}, {});
            flxServiceDefNameHeader.setDefaultUnit(kony.flex.DP);
            var lblName = new kony.ui.Label({
                "id": "lblName",
                "isVisible": true,
                "left": "0dp",
                "skin": "sknLbl192B4518pxLatoReg",
                "text": "Sole Properietor",
                "top": "22px",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var flxStatus = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "40px",
                "id": "flxStatus",
                "isVisible": true,
                "layoutType": kony.flex.FLOW_HORIZONTAL,
                "isModalContainer": false,
                "right": "0px",
                "skin": "slFbox",
                "top": "25px",
                "width": "70px"
            }, {}, {});
            flxStatus.setDefaultUnit(kony.flex.DP);
            var lblStatusIcon = new kony.ui.Label({
                "height": "15px",
                "id": "lblStatusIcon",
                "isVisible": true,
                "left": "0px",
                "skin": "sknFontIconActivate",
                "text": "",
                "top": "0px",
                "width": "15px",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var lblStatus = new kony.ui.Label({
                "id": "lblStatus",
                "isVisible": true,
                "left": "0px",
                "skin": "sknLatoRegular485c7514px",
                "text": "Enabled",
                "top": "0px",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxStatus.add(lblStatusIcon, lblStatus);
            flxServiceDefNameHeader.add(lblName, flxStatus);
            var flxOptions1 = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "centerY": "50%",
                "clipBounds": true,
                "height": "24dp",
                "id": "flxOptions1",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "isModalContainer": false,
                "right": "20px",
                "skin": "sknFlxBorffffff1pxRound",
                "top": "0px",
                "width": "24px",
                "zIndex": 3
            }, {}, {
                "hoverSkin": "sknflxffffffop100Border424242Radius100px"
            });
            flxOptions1.setDefaultUnit(kony.flex.DP);
            var lblIconOptions1 = new kony.ui.Label({
                "centerX": "50%",
                "centerY": "50%",
                "id": "lblIconOptions1",
                "isVisible": true,
                "skin": "sknFontIconOptionMenu",
                "text": "",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxOptions1.add(lblIconOptions1);
            var flxViewSeparator1 = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                "bottom": "0dp",
                "clipBounds": true,
                "id": "flxViewSeparator1",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "slFbox",
                "width": "100%",
                "zIndex": 1
            }, {}, {});
            flxViewSeparator1.setDefaultUnit(kony.flex.DP);
            var flxViewSeparatorVal1 = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "bottom": "0dp",
                "clipBounds": true,
                "height": "1dp",
                "id": "flxViewSeparatorVal1",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "20dp",
                "isModalContainer": false,
                "right": "20dp",
                "skin": "sknflxffffffOp100BorderE1E5EdNoRadius",
                "zIndex": 1
            }, {}, {});
            flxViewSeparatorVal1.setDefaultUnit(kony.flex.DP);
            flxViewSeparatorVal1.add();
            flxViewSeparator1.add(flxViewSeparatorVal1);
            var contextualMenu1 = new com.adminConsole.common.contextualMenu1({
                "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                "id": "contextualMenu1",
                "isVisible": false,
                "layoutType": kony.flex.FLOW_VERTICAL,
                "masterType": constants.MASTER_TYPE_DEFAULT,
                "isModalContainer": false,
                "right": "12dp",
                "skin": "slFbox",
                "top": "40dp",
                "width": "142dp",
                "overrides": {
                    "btnLink1": {
                        "isVisible": false
                    },
                    "btnLink2": {
                        "isVisible": false
                    },
                    "contextualMenu1": {
                        "isVisible": false,
                        "left": "viz.val_cleared",
                        "right": "12dp",
                        "top": "40dp",
                        "width": "142dp"
                    },
                    "flxDownArrowImage": {
                        "isVisible": false
                    },
                    "flxOption2": {
                        "isVisible": false
                    },
                    "imgDownArrow": {
                        "src": "downarrow_2x.png"
                    },
                    "imgOption2": {
                        "isVisible": false,
                        "src": "edit2x.png"
                    },
                    "imgOption3": {
                        "isVisible": true,
                        "left": "20dp",
                        "src": "delete_2x.png",
                        "width": "20dp"
                    },
                    "imgUpArrow": {
                        "src": "uparrow_2x.png"
                    },
                    "lblHeader": {
                        "isVisible": false
                    },
                    "lblIconOption1": {
                        "left": "20dp"
                    },
                    "lblIconOption3": {
                        "isVisible": false
                    },
                    "lblIconOption4": {
                        "left": "20dp"
                    },
                    "lblOption1": {
                        "i18n_text": "kony.i18n.getLocalizedString(\"i18n.permission.Edit\")"
                    },
                    "lblOption3": {
                        "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmLogsController.Delete\")"
                    }
                }
            }, {
                "overrides": {}
            }, {
                "overrides": {}
            });
            flxViewHeader.add(flxServiceDefNameHeader, flxOptions1, flxViewSeparator1, contextualMenu1);
            var flxViewServiceDefDescription = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                "clipBounds": true,
                "id": "flxViewServiceDefDescription",
                "isVisible": true,
                "layoutType": kony.flex.FLOW_VERTICAL,
                "left": "0dp",
                "isModalContainer": false,
                "right": "20dp",
                "skin": "slFbox",
                "top": "0dp",
                "width": "100%"
            }, {}, {});
            flxViewServiceDefDescription.setDefaultUnit(kony.flex.DP);
            var flxServiceType = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "50dp",
                "id": "flxServiceType",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "20dp",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "15dp",
                "width": "31%",
                "zIndex": 1
            }, {}, {});
            flxServiceType.setDefaultUnit(kony.flex.DP);
            var lblServiceType = new kony.ui.Label({
                "id": "lblServiceType",
                "isVisible": true,
                "left": "0dp",
                "skin": "sknlblLato696c7311px",
                "text": "SERVICE TYPE",
                "top": "1dp",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var lblServiceTypeValue = new kony.ui.Label({
                "id": "lblServiceTypeValue",
                "isVisible": true,
                "left": "0px",
                "skin": "sknlbl485C75LatoSemiBold13px",
                "text": "Retail Banking",
                "top": "25px",
                "width": "100%",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxServiceType.add(lblServiceType, lblServiceTypeValue);
            var flxDescription1 = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                "clipBounds": true,
                "id": "flxDescription1",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "20dp",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "15dp",
                "width": "95%",
                "zIndex": 1
            }, {}, {});
            flxDescription1.setDefaultUnit(kony.flex.DP);
            var lblServiceDescription = new kony.ui.Label({
                "id": "lblServiceDescription",
                "isVisible": true,
                "left": "0dp",
                "skin": "sknlblLato696c7311px",
                "text": "DESCRIPTION",
                "top": "1dp",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var lblServiceDescriptionValue = new kony.ui.Label({
                "id": "lblServiceDescriptionValue",
                "isVisible": true,
                "left": "0px",
                "skin": "sknlbl485C75LatoSemiBold13px",
                "top": "25px",
                "width": "100%",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxDescription1.add(lblServiceDescription, lblServiceDescriptionValue);
            var flxViewSeparator3 = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "bottom": 0,
                "clipBounds": true,
                "height": "1px",
                "id": "flxViewSeparator3",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0px",
                "isModalContainer": false,
                "skin": "sknflxSepratorE1E5ED",
                "top": "20px",
                "width": "100%",
                "zIndex": 2
            }, {}, {});
            flxViewSeparator3.setDefaultUnit(kony.flex.DP);
            flxViewSeparator3.add();
            flxViewServiceDefDescription.add(flxServiceType, flxDescription1, flxViewSeparator3);
            var flxViewContentHeader = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "51dp",
                "id": "flxViewContentHeader",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "isModalContainer": false,
                "right": "0px",
                "skin": "Copysknflxffffffop0e0ae7cd7f71746",
                "top": "0dp",
                "zIndex": 3
            }, {}, {});
            flxViewContentHeader.setDefaultUnit(kony.flex.DP);
            var flxViewTabs = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                "clipBounds": true,
                "id": "flxViewTabs",
                "isVisible": true,
                "layoutType": kony.flex.FLOW_HORIZONTAL,
                "left": "20dp",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0px",
                "width": "100%",
                "zIndex": 4
            }, {}, {});
            flxViewTabs.setDefaultUnit(kony.flex.DP);
            var flxViewTab1 = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "50px",
                "id": "flxViewTab1",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "CopyslFbox0efc73ba91cad4b",
                "top": "0dp",
                "width": "150px",
                "zIndex": 1
            }, {}, {});
            flxViewTab1.setDefaultUnit(kony.flex.DP);
            var lblTabName1 = new kony.ui.Label({
                "height": "100%",
                "id": "lblTabName1",
                "isVisible": true,
                "left": "0dp",
                "skin": "sknLblTabUtilActive",
                "text": "FEATURES AND ACTIONS",
                "top": "0dp",
                "width": "100%",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_CENTER,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxViewTab1.add(lblTabName1);
            var flxViewTab2 = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "50px",
                "id": "flxViewTab2",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "20px",
                "isModalContainer": false,
                "skin": "CopyslFbox0efc73ba91cad4b",
                "top": "0dp",
                "width": "50px",
                "zIndex": 1
            }, {}, {});
            flxViewTab2.setDefaultUnit(kony.flex.DP);
            var lblTabName2 = new kony.ui.Label({
                "height": "100%",
                "id": "lblTabName2",
                "isVisible": true,
                "left": "0dp",
                "skin": "sknLblTabUtilRest",
                "text": "LIMITS",
                "top": "0px",
                "width": "100%",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_CENTER,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxViewTab2.add(lblTabName2);
            var flxViewTab3 = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "50px",
                "id": "flxViewTab3",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "20px",
                "isModalContainer": false,
                "skin": "CopyslFbox0efc73ba91cad4b",
                "top": "0dp",
                "width": "120px",
                "zIndex": 1
            }, {}, {});
            flxViewTab3.setDefaultUnit(kony.flex.DP);
            var lblTabName3 = new kony.ui.Label({
                "height": "100%",
                "id": "lblTabName3",
                "isVisible": true,
                "left": "0dp",
                "skin": "sknLblTabUtilRest",
                "text": "ASSOCIATED ROLES",
                "top": "0dp",
                "width": "100%",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_CENTER,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxViewTab3.add(lblTabName3);
            flxViewTabs.add(flxViewTab1, flxViewTab2, flxViewTab3);
            var flxViewSeperator4 = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "bottom": 0,
                "clipBounds": true,
                "height": "1px",
                "id": "flxViewSeperator4",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0px",
                "isModalContainer": false,
                "skin": "sknflxSepratorE1E5ED",
                "width": "100%",
                "zIndex": 2
            }, {}, {});
            flxViewSeperator4.setDefaultUnit(kony.flex.DP);
            flxViewSeperator4.add();
            flxViewContentHeader.add(flxViewTabs, flxViewSeperator4);
            var flxViewContent = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                "bottom": "20dp",
                "clipBounds": true,
                "id": "flxViewContent",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "isModalContainer": false,
                "right": "20dp",
                "skin": "slFbox",
                "top": "0dp",
                "width": "100%"
            }, {}, {});
            flxViewContent.setDefaultUnit(kony.flex.DP);
            var flxDynamicFeaturesAndActionsList = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                "bottom": "35dp",
                "clipBounds": true,
                "id": "flxDynamicFeaturesAndActionsList",
                "isVisible": false,
                "layoutType": kony.flex.FLOW_VERTICAL,
                "left": "20dp",
                "isModalContainer": false,
                "skin": "slFbox",
                "width": kony.flex.USE_PREFFERED_SIZE
            }, {}, {});
            flxDynamicFeaturesAndActionsList.setDefaultUnit(kony.flex.DP);
            flxDynamicFeaturesAndActionsList.add();
            var flxLimits = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                "bottom": "35dp",
                "clipBounds": true,
                "id": "flxLimits",
                "isVisible": false,
                "layoutType": kony.flex.FLOW_VERTICAL,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "slFbox",
                "width": kony.flex.USE_PREFFERED_SIZE
            }, {}, {});
            flxLimits.setDefaultUnit(kony.flex.DP);
            flxLimits.add();
            var flxAssociatedRoles = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                "clipBounds": true,
                "id": "flxAssociatedRoles",
                "isVisible": false,
                "layoutType": kony.flex.FLOW_VERTICAL,
                "left": "0dp",
                "isModalContainer": false,
                "right": "0dp",
                "skin": "slFbox",
                "top": "0dp",
                "zIndex": 1
            }, {}, {});
            flxAssociatedRoles.setDefaultUnit(kony.flex.DP);
            var flxRolesEdit = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "45px",
                "id": "flxRolesEdit",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0dp",
                "width": "100%",
                "zIndex": 1
            }, {}, {});
            flxRolesEdit.setDefaultUnit(kony.flex.DP);
            var btnChangeDefaultRole = new kony.ui.Button({
                "bottom": "0px",
                "centerY": "50%",
                "focusSkin": "sknbtnf7f7faLatoRegular12Px485c75Border1px485C75Radius20px",
                "height": "22dp",
                "id": "btnChangeDefaultRole",
                "isVisible": true,
                "right": "20px",
                "skin": "sknbtnf7f7faLatoRegular12Px485c75Border1px485C75Radius20px",
                "text": "Change Default Role",
                "width": "150dp",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_CENTER,
                "displayText": true,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {
                "hoverSkin": "sknbtnffffffLatoRegular12Px485c75Border1px485C75Radius20px"
            });
            flxRolesEdit.add(btnChangeDefaultRole);
            var flxRoleUndoHeader = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "65px",
                "id": "flxRoleUndoHeader",
                "isVisible": false,
                "layoutType": kony.flex.FREE_FORM,
                "left": "20dp",
                "isModalContainer": false,
                "right": "20dp",
                "skin": "slFbox",
                "top": "0dp",
                "zIndex": 1
            }, {}, {});
            flxRoleUndoHeader.setDefaultUnit(kony.flex.DP);
            var backToRolesList = new com.adminConsole.customerMang.backToPageHeader({
                "height": "20px",
                "id": "backToRolesList",
                "isVisible": true,
                "layoutType": kony.flex.FLOW_HORIZONTAL,
                "left": "0dp",
                "masterType": constants.MASTER_TYPE_DEFAULT,
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "10dp",
                "width": "100%",
                "overrides": {
                    "backToPageHeader": {
                        "top": "10dp"
                    },
                    "btnBack": {
                        "text": "Back"
                    },
                    "flxBack": {
                        "left": "0px"
                    }
                }
            }, {
                "overrides": {}
            }, {
                "overrides": {}
            });
            var lblDefaultRole = new kony.ui.Label({
                "height": "20px",
                "id": "lblDefaultRole",
                "isVisible": true,
                "left": "0dp",
                "skin": "sknLatoRegular192B4518px",
                "text": "Default Role",
                "top": "45dp",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxRoleUndoHeader.add(backToRolesList, lblDefaultRole);
            var flxResults = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                "clipBounds": true,
                "id": "flxResults",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0dp",
                "width": "100%"
            }, {}, {});
            flxResults.setDefaultUnit(kony.flex.DP);
            var segAssociatedRoles = new kony.ui.SegmentedUI2({
                "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                "bottom": 0,
                "data": [{
                    "btnViewDetails": "kony.i18n.getLocalizedString(\"i18n.frmGroupsController.viewDetails\")",
                    "imgRoleCheckbox": "checkboxnormal.png",
                    "lblRoleDesc": "kony.i18n.getLocalizedString(\"i18n.frmAlertsManagement.LoremIpsumA\")",
                    "lblRoleName": "Bill Payments"
                }],
                "groupCells": false,
                "id": "segAssociatedRoles",
                "isVisible": true,
                "left": "0dp",
                "needPageIndicator": true,
                "pageOffDotImage": "pageoffdot.png",
                "pageOnDotImage": "pageondot.png",
                "retainSelection": false,
                "rowFocusSkin": "seg2Focus",
                "rowSkin": "seg2Normal",
                "rowTemplate": "flxCustomerProfileRoles",
                "sectionHeaderSkin": "sliPhoneSegmentHeader",
                "selectionBehavior": constants.SEGUI_DEFAULT_BEHAVIOR,
                "separatorRequired": false,
                "showScrollbars": false,
                "top": 0,
                "viewType": constants.SEGUI_VIEW_TYPE_TABLEVIEW,
                "widgetDataMap": {
                    "btnViewDetails": "btnViewDetails",
                    "flxCustomerProfileRoles": "flxCustomerProfileRoles",
                    "flxDefaultRoleButton": "flxDefaultRoleButton",
                    "flxRoleCheckbox": "flxRoleCheckbox",
                    "flxRoleInfo": "flxRoleInfo",
                    "flxRoleName": "flxRoleName",
                    "flxRoleNameContainer": "flxRoleNameContainer",
                    "flxRoleRadio": "flxRoleRadio",
                    "imgRoleCheckbox": "imgRoleCheckbox",
                    "imgRoleRadio": "imgRoleRadio",
                    "lblRoleDesc": "lblRoleDesc",
                    "lblRoleName": "lblRoleName"
                },
                "width": "100%",
                "zIndex": 1
            }, {
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxResults.add(segAssociatedRoles);
            var flxNoAssociatedRolesAvailable = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "160dp",
                "id": "flxNoAssociatedRolesAvailable",
                "isVisible": false,
                "layoutType": kony.flex.FLOW_VERTICAL,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0dp",
                "width": "100%"
            }, {}, {});
            flxNoAssociatedRolesAvailable.setDefaultUnit(kony.flex.DP);
            var lblNoRolesAvailable = new kony.ui.Label({
                "centerX": "50%",
                "centerY": "45%",
                "id": "lblNoRolesAvailable",
                "isVisible": true,
                "skin": "sknLbl485C75LatoRegular13Px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmCustomerProfileRoles.NoRolesAssigned\")",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_CENTER,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var btnAssignRoles = new kony.ui.Button({
                "centerX": "50%",
                "height": "22dp",
                "id": "btnAssignRoles",
                "isVisible": true,
                "skin": "sknbtnf7f7faLatoRegular12Px485c75Border1px485C75Radius20px",
                "text": "Assign Roles",
                "top": "10dp",
                "width": "100dp",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_CENTER,
                "displayText": true,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {
                "hoverSkin": "sknbtnffffffLatoRegular12Px485c75Border1px485C75Radius20px"
            });
            flxNoAssociatedRolesAvailable.add(lblNoRolesAvailable, btnAssignRoles);
            flxAssociatedRoles.add(flxRolesEdit, flxRoleUndoHeader, flxResults, flxNoAssociatedRolesAvailable);
            flxViewContent.add(flxDynamicFeaturesAndActionsList, flxLimits, flxAssociatedRoles);
            var flxCommonButtonsRolesSave = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "bottom": "0dp",
                "clipBounds": true,
                "height": "80dp",
                "id": "flxCommonButtonsRolesSave",
                "isVisible": false,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "sknFlxFFFFFF100O",
                "width": "100%",
                "zIndex": 1
            }, {}, {});
            flxCommonButtonsRolesSave.setDefaultUnit(kony.flex.DP);
            var flxCommonButtonsRolesSeparator = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "1dp",
                "id": "flxCommonButtonsRolesSeparator",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "isModalContainer": false,
                "right": "0dp",
                "skin": "sknflxe1e5edop100",
                "top": "0dp",
                "zIndex": 1
            }, {}, {});
            flxCommonButtonsRolesSeparator.setDefaultUnit(kony.flex.DP);
            flxCommonButtonsRolesSeparator.add();
            var commonButtonsRoles = new com.adminConsole.common.commonButtons({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "centerY": "50%",
                "height": "80px",
                "id": "commonButtonsRoles",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "masterType": constants.MASTER_TYPE_DEFAULT,
                "isModalContainer": false,
                "skin": "slFbox",
                "width": "100%",
                "overrides": {
                    "btnCancel": {
                        "left": "viz.val_cleared",
                        "right": "140px"
                    },
                    "btnNext": {
                        "isVisible": false,
                        "left": "viz.val_cleared",
                        "right": "20dp",
                        "width": "100dp"
                    },
                    "btnSave": {
                        "i18n_text": "kony.i18n.getLocalizedString(\"i18n.permission.SAVE\")",
                        "isVisible": true,
                        "right": "20dp"
                    },
                    "commonButtons": {
                        "bottom": "viz.val_cleared",
                        "centerY": "50%",
                        "isVisible": true,
                        "top": "viz.val_cleared"
                    },
                    "flxRightButtons": {
                        "width": "140px"
                    }
                }
            }, {
                "overrides": {}
            }, {
                "overrides": {}
            });
            flxCommonButtonsRolesSave.add(flxCommonButtonsRolesSeparator, commonButtonsRoles);
            flxViewContainer.add(flxViewHeader, flxViewServiceDefDescription, flxViewContentHeader, flxViewContent, flxCommonButtonsRolesSave);
            flxGroupViewServiceDef.add(flxViewContainer);
            flxViewServiceDef.add(flxGroupViewServiceDef);
            var flxAddGroups = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "bottom": "0px",
                "clipBounds": true,
                "id": "flxAddGroups",
                "isVisible": false,
                "layoutType": kony.flex.FREE_FORM,
                "left": "35dp",
                "isModalContainer": false,
                "right": "35dp",
                "skin": "sknflxffffffop100Bordercbcdd1Radius3px",
                "top": "125dp",
                "zIndex": 1
            }, {}, {});
            flxAddGroups.setDefaultUnit(kony.flex.DP);
            var flxVerticalTabs = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "bottom": "0px",
                "clipBounds": true,
                "id": "flxVerticalTabs",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0dp",
                "width": "230px",
                "zIndex": 1
            }, {}, {});
            flxVerticalTabs.setDefaultUnit(kony.flex.DP);
            var verticalTabs = new com.adminConsole.common.verticalTabs1({
                "height": "100%",
                "id": "verticalTabs",
                "isVisible": true,
                "layoutType": kony.flex.FLOW_VERTICAL,
                "left": "0dp",
                "masterType": constants.MASTER_TYPE_DEFAULT,
                "isModalContainer": false,
                "skin": "sknflxf9f9f9NoBorder",
                "top": "0dp",
                "width": "100%",
                "overrides": {
                    "btnOption1": {
                        "bottom": "0dp",
                        "centerX": "viz.val_cleared",
                        "centerY": "viz.val_cleared",
                        "i18n_text": "kony.i18n.getLocalizedString(\"i18n.ServiceDefinition.ServiceDetails\")",
                        "left": "20px",
                        "right": "39dp",
                        "top": "0dp",
                        "width": "viz.val_cleared"
                    },
                    "btnOption2": {
                        "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmGroupsController.AssignFeaturesAndActions_UC\")",
                        "left": "20px",
                        "right": "40dp",
                        "top": "0dp",
                        "width": "viz.val_cleared"
                    },
                    "btnOption3": {
                        "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmCustomerCreate.AssignLimits_UC\")",
                        "left": "20px",
                        "right": "40dp",
                        "width": "viz.val_cleared"
                    },
                    "btnOption4": {
                        "i18n_text": "kony.i18n.getLocalizedString(\"i18n.ServiceDefinition.AssociatedRoles\")",
                        "left": "20px",
                        "right": 40,
                        "width": "viz.val_cleared"
                    },
                    "btnOption5": {
                        "i18n_text": "kony.i18n.getLocalizedString(\"i18n.ServiceDefinition.SetDefaultRole\")"
                    },
                    "flxImgArrow1": {
                        "bottom": "0dp",
                        "centerY": "viz.val_cleared",
                        "right": "20px",
                        "top": "0dp"
                    },
                    "flxImgArrow2": {
                        "bottom": "0dp",
                        "clipBounds": true,
                        "isVisible": false,
                        "right": "20px",
                        "top": "0px"
                    },
                    "flxImgArrow3": {
                        "isVisible": false,
                        "right": "20px"
                    },
                    "flxImgArrow4": {
                        "isVisible": false,
                        "right": "20px"
                    },
                    "flxImgArrow5": {
                        "isVisible": false
                    },
                    "flxOption0": {
                        "isVisible": false
                    },
                    "flxOption1": {
                        "minWidth": "viz.val_cleared",
                        "width": "100%"
                    },
                    "flxOption3": {
                        "isVisible": true
                    },
                    "flxOption4": {
                        "clipBounds": true,
                        "isVisible": false
                    },
                    "flxOption5": {
                        "isVisible": false
                    },
                    "imgSelected1": {
                        "src": "right_arrow2x.png"
                    },
                    "imgSelected2": {
                        "isVisible": false,
                        "src": "right_arrow2x.png"
                    },
                    "imgSelected3": {
                        "src": "right_arrow2x.png"
                    },
                    "imgSelected4": {
                        "src": "right_arrow2x.png"
                    },
                    "lblOptional1": {
                        "isVisible": false
                    },
                    "lblOptional2": {
                        "isVisible": false,
                        "left": "20px",
                        "top": "10px"
                    },
                    "lblOptional3": {
                        "isVisible": true,
                        "left": "20px",
                        "top": "10px"
                    },
                    "lblOptional4": {
                        "isVisible": false,
                        "left": "20px"
                    },
                    "lblOptional5": {
                        "isVisible": false
                    },
                    "lblSelected1": {
                        "centerX": "50%",
                        "centerY": "50%"
                    },
                    "lblSelected2": {
                        "centerY": "50%"
                    }
                }
            }, {
                "overrides": {}
            }, {
                "overrides": {}
            });
            flxVerticalTabs.add(verticalTabs);
            var flxGroupDetails = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "bottom": "0dp",
                "clipBounds": true,
                "id": "flxGroupDetails",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "230dp",
                "isModalContainer": false,
                "right": "0dp",
                "skin": "slFbox",
                "top": "0dp",
                "zIndex": 1
            }, {}, {});
            flxGroupDetails.setDefaultUnit(kony.flex.DP);
            var flxAddGroupDetails = new kony.ui.FlexScrollContainer({
                "allowHorizontalBounce": false,
                "allowVerticalBounce": true,
                "bottom": "81dp",
                "bounces": true,
                "clipBounds": true,
                "enableScrolling": true,
                "horizontalScrollIndicator": true,
                "id": "flxAddGroupDetails",
                "isVisible": true,
                "layoutType": kony.flex.FLOW_VERTICAL,
                "left": "0dp",
                "pagingEnabled": false,
                "right": 0,
                "scrollDirection": kony.flex.SCROLL_VERTICAL,
                "skin": "slFSbox",
                "top": "0dp",
                "verticalScrollIndicator": true,
                "zIndex": 1
            }, {}, {});
            flxAddGroupDetails.setDefaultUnit(kony.flex.DP);
            var flxRow1 = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "93dp",
                "id": "flxRow1",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "20dp",
                "width": "100%"
            }, {}, {});
            flxRow1.setDefaultUnit(kony.flex.DP);
            var flxName = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "93dp",
                "id": "flxName",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "20dp",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0dp",
                "width": "365dp",
                "zIndex": 1
            }, {}, {});
            flxName.setDefaultUnit(kony.flex.DP);
            var lblGroupNameKey = new kony.ui.Label({
                "id": "lblGroupNameKey",
                "isVisible": true,
                "left": "0px",
                "skin": "slLabel0d20174dce8ea42",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.AddServiceDef.Service\")",
                "top": 0,
                "width": "100%",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var lblGroupNameCount = new kony.ui.Label({
                "id": "lblGroupNameCount",
                "isVisible": false,
                "right": "2px",
                "skin": "slLabel0d20174dce8ea42",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmGroups.lblGroupNameCount\")",
                "top": "0px",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var tbxGroupNameValue = new kony.ui.TextBox2({
                "autoCapitalize": constants.TEXTBOX_AUTO_CAPITALIZE_NONE,
                "focusSkin": "skntxtbxDetailsFocus14px",
                "height": "40dp",
                "id": "tbxGroupNameValue",
                "isVisible": true,
                "keyBoardStyle": constants.TEXTBOX_KEY_BOARD_STYLE_DEFAULT,
                "left": "0dp",
                "maxTextLength": 50,
                "placeholder": "Service",
                "secureTextEntry": false,
                "skin": "skntbxLato35475f14px",
                "textInputMode": constants.TEXTBOX_INPUT_MODE_ANY,
                "top": "30dp",
                "width": "100%",
                "zIndex": 1
            }, {
                "containerHeightMode": constants.TEXTBOX_FONT_METRICS_DRIVEN_HEIGHT,
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [3, 0, 0, 0],
                "paddingInPixel": false
            }, {
                "autoCorrect": false
            });
            var flxNoGroupNameError = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "25dp",
                "id": "flxNoGroupNameError",
                "isVisible": false,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "70dp",
                "width": "100%"
            }, {}, {});
            flxNoGroupNameError.setDefaultUnit(kony.flex.DP);
            var lblNoGroupNameError = new kony.ui.Label({
                "centerY": "50%",
                "height": "15dp",
                "id": "lblNoGroupNameError",
                "isVisible": true,
                "left": "15dp",
                "skin": "sknlblError",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.AddServiceDef.EmptyServiceName\")",
                "top": "0dp",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var lblNoGroupNameErrorIcon = new kony.ui.Label({
                "centerY": "52%",
                "height": "15dp",
                "id": "lblNoGroupNameErrorIcon",
                "isVisible": true,
                "left": "0dp",
                "skin": "sknErrorIcon",
                "text": "",
                "width": "15dp",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxNoGroupNameError.add(lblNoGroupNameError, lblNoGroupNameErrorIcon);
            var imgMandatoryName = new kony.ui.Image2({
                "height": "12px",
                "id": "imgMandatoryName",
                "isVisible": false,
                "left": "110px",
                "skin": "slImage",
                "src": "mandatory_2x.png",
                "top": "3px",
                "width": "6px",
                "zIndex": 1
            }, {
                "imageScaleMode": constants.IMAGE_SCALE_MODE_MAINTAIN_ASPECT_RATIO,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxName.add(lblGroupNameKey, lblGroupNameCount, tbxGroupNameValue, flxNoGroupNameError, imgMandatoryName);
            var flxType = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "93dp",
                "id": "flxType",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "400dp",
                "isModalContainer": false,
                "right": "20dp",
                "skin": "slFbox",
                "top": "0dp",
                "zIndex": 1
            }, {}, {});
            flxType.setDefaultUnit(kony.flex.DP);
            var lblTypeHeader = new kony.ui.Label({
                "id": "lblTypeHeader",
                "isVisible": true,
                "left": "0px",
                "skin": "slLabel0d20174dce8ea42",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.AddServiceDef.ServiceType\")",
                "top": 0,
                "width": "100%",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var lstBoxType = new kony.ui.ListBox({
                "focusSkin": "defListBoxFocus",
                "height": "40dp",
                "id": "lstBoxType",
                "isVisible": true,
                "left": "0dp",
                "masterData": [
                    ["lb1", "Select a Service Type"],
                    ["lb2", "Retail Banking"],
                    ["lb3", "Business Banking"]
                ],
                "selectedKey": "lb1",
                "skin": "sknlbxBgffffffBorderc1c9ceRadius3Px",
                "top": "30dp",
                "width": "100%",
                "zIndex": 1,
                "blur": {
                    "enabled": false,
                    "value": 0
                }
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [3, 0, 0, 0],
                "paddingInPixel": false
            }, {
                "hoverSkin": "sknlbxBgffffffBorderc1c9ceRadius3Px",
                "multiSelect": false
            });
            var flxTypeError = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "25dp",
                "id": "flxTypeError",
                "isVisible": false,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "70dp",
                "width": "100%"
            }, {}, {});
            flxTypeError.setDefaultUnit(kony.flex.DP);
            var lblIconTypeError = new kony.ui.Label({
                "centerY": "52%",
                "height": "15dp",
                "id": "lblIconTypeError",
                "isVisible": true,
                "left": "0dp",
                "skin": "sknErrorIcon",
                "text": "",
                "top": "0dp",
                "width": "15dp",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var lblTypeError = new kony.ui.Label({
                "centerY": "50%",
                "height": "15dp",
                "id": "lblTypeError",
                "isVisible": true,
                "left": "15dp",
                "skin": "sknlblError",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.AddServiceDef.EmptyServiceType\")",
                "top": "0dp",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxTypeError.add(lblIconTypeError, lblTypeError);
            flxType.add(lblTypeHeader, lstBoxType, flxTypeError);
            flxRow1.add(flxName, flxType);
            var flxDescription = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "125dp",
                "id": "flxDescription",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "10dp",
                "width": "100%",
                "zIndex": 1
            }, {}, {});
            flxDescription.setDefaultUnit(kony.flex.DP);
            var lblGroupDescription = new kony.ui.Label({
                "id": "lblGroupDescription",
                "isVisible": true,
                "left": "20px",
                "skin": "slLabel0d20174dce8ea42",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.permission.Description\")",
                "top": 0,
                "width": "30%",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var lblGroupDescriptionCount = new kony.ui.Label({
                "id": "lblGroupDescriptionCount",
                "isVisible": false,
                "right": "20px",
                "skin": "slLabel0d20174dce8ea42",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.AddServiceDef.DescriptionCount\")",
                "top": "0px",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var txtGroupDescription = new kony.ui.TextArea2({
                "autoCapitalize": constants.TEXTAREA_AUTO_CAPITALIZE_NONE,
                "bottom": "22dp",
                "focusSkin": "skntxtAreaLato9ca9ba12Px",
                "height": "72dp",
                "id": "txtGroupDescription",
                "isVisible": true,
                "keyBoardStyle": constants.TEXTAREA_KEY_BOARD_STYLE_DEFAULT,
                "left": "20dp",
                "maxTextLength": 150,
                "numberOfVisibleLines": 3,
                "placeholder": "Description",
                "right": "20dp",
                "skin": "skntxtAreaLato35475f14Px",
                "textInputMode": constants.TEXTAREA_INPUT_MODE_ANY,
                "top": "30dp",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_TOP_LEFT,
                "padding": [2, 2, 2, 2],
                "paddingInPixel": false
            }, {
                "autoCorrect": false
            });
            var flxNoGroupDescriptionError = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "25px",
                "id": "flxNoGroupDescriptionError",
                "isVisible": false,
                "layoutType": kony.flex.FREE_FORM,
                "left": "20dp",
                "isModalContainer": false,
                "right": "0dp",
                "skin": "slFbox",
                "top": "101dp"
            }, {}, {});
            flxNoGroupDescriptionError.setDefaultUnit(kony.flex.DP);
            var lblNoGroupDescriptionError = new kony.ui.Label({
                "centerY": "40%",
                "height": "15px",
                "id": "lblNoGroupDescriptionError",
                "isVisible": true,
                "left": "15px",
                "skin": "sknlblError",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.AddServiceDef.EmptyServiceDesc\")",
                "top": "0px",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var lblNoGroupDescriptionErrorIcon = new kony.ui.Label({
                "centerY": "52%",
                "height": "15dp",
                "id": "lblNoGroupDescriptionErrorIcon",
                "isVisible": true,
                "left": "0dp",
                "skin": "sknErrorIcon",
                "text": "",
                "top": "0dp",
                "width": "15dp",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxNoGroupDescriptionError.add(lblNoGroupDescriptionError, lblNoGroupDescriptionErrorIcon);
            flxDescription.add(lblGroupDescription, lblGroupDescriptionCount, txtGroupDescription, flxNoGroupDescriptionError);
            var flxStatusAndAgreement = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "40px",
                "id": "flxStatusAndAgreement",
                "isVisible": true,
                "layoutType": kony.flex.FLOW_HORIZONTAL,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "25dp",
                "width": "100%"
            }, {}, {});
            flxStatusAndAgreement.setDefaultUnit(kony.flex.DP);
            var flxGroupStatus = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "25px",
                "id": "flxGroupStatus",
                "isVisible": true,
                "layoutType": kony.flex.FLOW_HORIZONTAL,
                "left": "20dp",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0dp",
                "width": "110px",
                "zIndex": 1
            }, {}, {});
            flxGroupStatus.setDefaultUnit(kony.flex.DP);
            var lblSwitchActivate = new kony.ui.Label({
                "centerY": "48%",
                "id": "lblSwitchActivate",
                "isVisible": true,
                "left": "0px",
                "skin": "sknLbl485C75LatoRegular13Px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.permission.Active\")",
                "top": 0,
                "width": "55px",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var switchStatus = new kony.ui.Switch({
                "centerY": "50%",
                "height": "25dp",
                "id": "switchStatus",
                "isVisible": true,
                "left": "0dp",
                "leftSideText": "ON",
                "rightSideText": "OFF",
                "selectedIndex": 0,
                "skin": "sknSwitchActiveDisabled",
                "width": "36dp",
                "zIndex": 1,
                "blur": {
                    "enabled": false,
                    "value": 0
                }
            }, {
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxGroupStatus.add(lblSwitchActivate, switchStatus);
            flxStatusAndAgreement.add(flxGroupStatus);
            flxAddGroupDetails.add(flxRow1, flxDescription, flxStatusAndAgreement);
            var flxSeparator11 = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "bottom": 81,
                "clipBounds": true,
                "height": "1dp",
                "id": "flxSeparator11",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "sknflxd6dbe7",
                "width": "100%",
                "zIndex": 1
            }, {}, {});
            flxSeparator11.setDefaultUnit(kony.flex.DP);
            flxSeparator11.add();
            var flxGroupDetailsButtons = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "bottom": "0dp",
                "clipBounds": true,
                "height": "80dp",
                "id": "flxGroupDetailsButtons",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0%",
                "isModalContainer": false,
                "skin": "slFbox",
                "width": "100%",
                "zIndex": 1
            }, {}, {});
            flxGroupDetailsButtons.setDefaultUnit(kony.flex.DP);
            var btnAddGroupCancel = new kony.ui.Button({
                "centerY": "50%",
                "focusSkin": "sknBtnLatoRegulara5abc413pxKA",
                "height": "40dp",
                "id": "btnAddGroupCancel",
                "isVisible": true,
                "left": "20px",
                "right": "29.50%",
                "skin": "sknbtnf7f7faLatoRegular4f555dBorder1px8b96a5",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.permission.CANCEL\")",
                "top": "0%",
                "width": "100px",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_CENTER,
                "displayText": true,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {
                "hoverSkin": "sknbtnffffffLatoRegular4f555dBorder1px485c75"
            });
            var flxAddGroupRightButtons = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "centerY": "50%",
                "clipBounds": true,
                "height": "100%",
                "id": "flxAddGroupRightButtons",
                "isVisible": true,
                "layoutType": kony.flex.FLOW_HORIZONTAL,
                "reverseLayoutDirection": true,
                "isModalContainer": false,
                "right": "0px",
                "skin": "slFbox",
                "width": "280px",
                "zIndex": 1
            }, {}, {});
            flxAddGroupRightButtons.setDefaultUnit(kony.flex.DP);
            var btnAddGroupNext = new kony.ui.Button({
                "centerY": "50%",
                "focusSkin": "sknBtn003E75LatoRegular13pxFFFFFFRad20px",
                "height": "40dp",
                "id": "btnAddGroupNext",
                "isVisible": true,
                "right": "20px",
                "skin": "sknBtn003E75LatoRegular13pxFFFFFFRad20px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.permission.NEXT\")",
                "top": "0%",
                "width": "93dp",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_CENTER,
                "displayText": true,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {
                "hoverSkin": "sknBtn005198LatoRegular13pxFFFFFFRad20px"
            });
            flxAddGroupRightButtons.add(btnAddGroupNext);
            flxGroupDetailsButtons.add(btnAddGroupCancel, flxAddGroupRightButtons);
            var BusinessTypesToolTip = new com.adminConsole.Customers.ToolTip({
                "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                "id": "BusinessTypesToolTip",
                "isVisible": false,
                "layoutType": kony.flex.FREE_FORM,
                "left": "94dp",
                "masterType": constants.MASTER_TYPE_DEFAULT,
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "490dp",
                "width": "181dp",
                "overrides": {
                    "ToolTip": {
                        "isVisible": false,
                        "left": "94dp",
                        "top": "490dp",
                        "width": "181dp"
                    },
                    "flxToolTipMessage": {
                        "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                        "centerX": "viz.val_cleared",
                        "width": kony.flex.USE_PREFFERED_SIZE
                    },
                    "lblDownArrow": {
                        "centerX": "viz.val_cleared",
                        "left": "10px",
                        "top": "55dp"
                    },
                    "lblNoConcentToolTip": {
                        "bottom": "5px",
                        "height": kony.flex.USE_PREFFERED_SIZE,
                        "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmAdManagement.smsEmailPushNotification\")",
                        "left": "10px",
                        "right": "10px",
                        "top": "3px",
                        "width": kony.flex.USE_PREFFERED_SIZE
                    },
                    "lblarrow": {
                        "centerX": "viz.val_cleared",
                        "left": "viz.val_cleared",
                        "right": "10px"
                    }
                }
            }, {
                "overrides": {}
            }, {
                "overrides": {}
            });
            flxGroupDetails.add(flxAddGroupDetails, flxSeparator11, flxGroupDetailsButtons, BusinessTypesToolTip);
            var flxSeparator = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "100%",
                "id": "flxSeparator",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "231dp",
                "isModalContainer": false,
                "skin": "sknflxd6dbe7",
                "top": "0dp",
                "width": "1dp",
                "zIndex": 10
            }, {}, {});
            flxSeparator.setDefaultUnit(kony.flex.DP);
            flxSeparator.add();
            var flxSelectFeatures = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "bottom": "-10dp",
                "clipBounds": true,
                "id": "flxSelectFeatures",
                "isVisible": false,
                "layoutType": kony.flex.FREE_FORM,
                "left": "240dp",
                "isModalContainer": false,
                "right": "-10dp",
                "skin": "slFbox",
                "top": "10dp",
                "zIndex": 1
            }, {}, {});
            flxSelectFeatures.setDefaultUnit(kony.flex.DP);
            var flxAddFeatures = new kony.ui.FlexScrollContainer({
                "allowHorizontalBounce": false,
                "allowVerticalBounce": true,
                "bottom": "81dp",
                "bounces": true,
                "clipBounds": true,
                "enableScrolling": true,
                "horizontalScrollIndicator": true,
                "id": "flxAddFeatures",
                "isVisible": true,
                "layoutType": kony.flex.FLOW_VERTICAL,
                "left": "0dp",
                "pagingEnabled": false,
                "right": 0,
                "scrollDirection": kony.flex.SCROLL_VERTICAL,
                "skin": "slFSbox",
                "top": "0dp",
                "verticalScrollIndicator": true,
                "zIndex": 1
            }, {}, {});
            flxAddFeatures.setDefaultUnit(kony.flex.DP);
            var flxHeader = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "25dp",
                "id": "flxHeader",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "20dp",
                "isModalContainer": false,
                "right": "20dp",
                "skin": "slFbox",
                "top": "10dp",
                "width": "95%",
                "zIndex": 1
            }, {}, {});
            flxHeader.setDefaultUnit(kony.flex.DP);
            var lblHeader = new kony.ui.Label({
                "id": "lblHeader",
                "isVisible": true,
                "left": "0px",
                "skin": "sknLbl192B4518pxLatoReg",
                "text": "Features",
                "top": 0,
                "width": "30%",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxHeader.add(lblHeader);
            var imgIcon = new kony.ui.Image2({
                "centerX": "50%",
                "centerY": "30%",
                "height": "104dp",
                "id": "imgIcon",
                "isVisible": true,
                "left": "0dp",
                "skin": "slImage",
                "src": "selectacheckbox.png",
                "top": "0dp",
                "width": "131dp",
                "zIndex": 1
            }, {
                "imageScaleMode": constants.IMAGE_SCALE_MODE_MAINTAIN_ASPECT_RATIO,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var flxSelect = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "centerX": "50%",
                "clipBounds": true,
                "height": "150dp",
                "id": "flxSelect",
                "isVisible": true,
                "layoutType": kony.flex.FLOW_VERTICAL,
                "left": "44dp",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "40dp",
                "width": "100%",
                "zIndex": 1
            }, {}, {});
            flxSelect.setDefaultUnit(kony.flex.DP);
            var lblSelect = new kony.ui.Label({
                "centerX": "50%",
                "id": "lblSelect",
                "isVisible": true,
                "left": "0dp",
                "skin": "sknLbl485C75LatoRegular13Px",
                "text": "Select Features and assign Actions",
                "top": "0dp",
                "width": "600dp",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_CENTER,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var flxButtons = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "50dp",
                "id": "flxButtons",
                "isVisible": true,
                "layoutType": kony.flex.FLOW_HORIZONTAL,
                "left": "104dp",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "30dp",
                "width": "88.24%",
                "zIndex": 1
            }, {}, {});
            flxButtons.setDefaultUnit(kony.flex.DP);
            var btnAddpermissions = new kony.ui.Button({
                "centerX": "30%",
                "centerY": "50%",
                "height": "22dp",
                "id": "btnAddpermissions",
                "isVisible": true,
                "left": "0dp",
                "skin": "sknbtnbrdr485C75rad15px",
                "text": "Add Permissions",
                "top": "0dp",
                "width": "150dp",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_CENTER,
                "displayText": true,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var btnAdvancedSelection = new kony.ui.Button({
                "centerY": "50%",
                "height": "22dp",
                "id": "btnAdvancedSelection",
                "isVisible": true,
                "left": "20dp",
                "skin": "sknbtnbrdr485C75rad15px",
                "text": "Advanced Selection",
                "top": "0dp",
                "width": "150dp",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_CENTER,
                "displayText": true,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxButtons.add(btnAddpermissions, btnAdvancedSelection);
            flxSelect.add(lblSelect, flxButtons);
            flxAddFeatures.add(flxHeader, imgIcon, flxSelect);
            flxSelectFeatures.add(flxAddFeatures);
            var flxAdvancedSelectionComponent = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "bottom": "0dp",
                "clipBounds": true,
                "id": "flxAdvancedSelectionComponent",
                "isVisible": false,
                "layoutType": kony.flex.FLOW_VERTICAL,
                "left": "230dp",
                "isModalContainer": false,
                "right": "0dp",
                "skin": "slFbox",
                "top": "0dp",
                "zIndex": 1
            }, {}, {});
            flxAdvancedSelectionComponent.setDefaultUnit(kony.flex.DP);
            var flxAdvancedSelection = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "bottom": "0dp",
                "clipBounds": true,
                "height": "100%",
                "id": "flxAdvancedSelection",
                "isVisible": true,
                "layoutType": kony.flex.FLOW_VERTICAL,
                "left": "0dp",
                "isModalContainer": false,
                "right": "0dp",
                "skin": "slFbox",
                "top": "0dp",
                "zIndex": 1
            }, {}, {});
            flxAdvancedSelection.setDefaultUnit(kony.flex.DP);
            var flxAdvanceSelectionHeader = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "bottom": 0,
                "clipBounds": true,
                "height": "14%",
                "id": "flxAdvanceSelectionHeader",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "isModalContainer": false,
                "right": 0,
                "skin": "slFbox",
                "top": "0dp",
                "zIndex": 1
            }, {}, {});
            flxAdvanceSelectionHeader.setDefaultUnit(kony.flex.DP);
            var lblHeading = new kony.ui.Label({
                "id": "lblHeading",
                "isVisible": true,
                "left": "20dp",
                "skin": "sknLbl192B4518pxLatoReg",
                "text": "Advanced Selection",
                "top": "20px",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var flxSearch = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "100%",
                "id": "flxSearch",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "isModalContainer": false,
                "right": "20dp",
                "skin": "CopyslFbox2",
                "top": "15px",
                "width": "350px",
                "zIndex": 1
            }, {}, {});
            flxSearch.setDefaultUnit(kony.flex.DP);
            var flxSearchContainer = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "35px",
                "id": "flxSearchContainer",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "sknflxd5d9ddop100",
                "top": "8dp",
                "width": "350px",
                "zIndex": 1
            }, {}, {});
            flxSearchContainer.setDefaultUnit(kony.flex.DP);
            var fontIconSearchImg = new kony.ui.Label({
                "centerY": "50%",
                "id": "fontIconSearchImg",
                "isVisible": true,
                "left": "12px",
                "skin": "sknFontIconSearchImg20px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.userwidgetmodel.lblSearchIcon\")",
                "top": "0dp",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var tbxSearchBox = new kony.ui.TextBox2({
                "autoCapitalize": constants.TEXTBOX_AUTO_CAPITALIZE_NONE,
                "centerY": "50%",
                "height": "30px",
                "id": "tbxSearchBox",
                "isVisible": true,
                "keyBoardStyle": constants.TEXTBOX_KEY_BOARD_STYLE_DEFAULT,
                "left": "30dp",
                "onTouchStart": controller.AS_TextField_cfab06859f0f43eca99495891ee290d5,
                "placeholder": "Search By Feature",
                "right": "35px",
                "secureTextEntry": false,
                "skin": "skntbxffffffNoBorderlato35475f14px",
                "textInputMode": constants.TEXTBOX_INPUT_MODE_ANY,
                "zIndex": 1
            }, {
                "containerHeightMode": constants.TEXTBOX_FONT_METRICS_DRIVEN_HEIGHT,
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [2, 0, 0, 0],
                "paddingInPixel": false
            }, {
                "autoCorrect": false
            });
            var flxClearSearchImage = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "100%",
                "id": "flxClearSearchImage",
                "isVisible": false,
                "layoutType": kony.flex.FREE_FORM,
                "isModalContainer": false,
                "right": "0px",
                "skin": "sknCursor",
                "top": "0dp",
                "width": "35px",
                "zIndex": 1
            }, {}, {});
            flxClearSearchImage.setDefaultUnit(kony.flex.DP);
            var fontIconCross = new kony.ui.Label({
                "centerY": "50%",
                "id": "fontIconCross",
                "isVisible": true,
                "left": "5px",
                "skin": "sknFontIconSearchCross16px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.userwidgetmodel.lblClearSearch\")",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxClearSearchImage.add(fontIconCross);
            flxSearchContainer.add(fontIconSearchImg, tbxSearchBox, flxClearSearchImage);
            flxSearch.add(flxSearchContainer);
            flxAdvanceSelectionHeader.add(lblHeading, flxSearch);
            var flxAdvanceSelectionContent = new kony.ui.FlexScrollContainer({
                "allowHorizontalBounce": false,
                "allowVerticalBounce": true,
                "bounces": true,
                "centerX": "50%",
                "clipBounds": true,
                "enableScrolling": true,
                "height": "72%",
                "horizontalScrollIndicator": true,
                "id": "flxAdvanceSelectionContent",
                "isVisible": true,
                "layoutType": kony.flex.FLOW_VERTICAL,
                "left": "0dp",
                "pagingEnabled": false,
                "right": 0,
                "scrollDirection": kony.flex.SCROLL_VERTICAL,
                "skin": "slFSbox",
                "top": "0dp",
                "verticalScrollIndicator": true,
                "width": "94%",
                "zIndex": 1
            }, {}, {});
            flxAdvanceSelectionContent.setDefaultUnit(kony.flex.DP);
            flxAdvanceSelectionContent.add();
            var flxCommonButtons = new kony.ui.FlexContainer({
                "bottom": "0dp",
                "clipBounds": true,
                "height": "14%",
                "id": "flxCommonButtons",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "slFbox",
                "width": "100%"
            }, {}, {});
            flxCommonButtons.setDefaultUnit(kony.flex.DP);
            var flxSeparator4 = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "1dp",
                "id": "flxSeparator4",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "sknflxD5D9DD",
                "top": "5dp",
                "width": "100%",
                "zIndex": 1
            }, {}, {});
            flxSeparator4.setDefaultUnit(kony.flex.DP);
            flxSeparator4.add();
            var flxRightButtons = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "centerY": "50%",
                "clipBounds": true,
                "height": "100%",
                "id": "flxRightButtons",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "isModalContainer": false,
                "right": "20dp",
                "skin": "slFbox",
                "width": "250px",
                "zIndex": 1
            }, {}, {});
            flxRightButtons.setDefaultUnit(kony.flex.DP);
            var btnSave = new kony.ui.Button({
                "centerY": "50%",
                "focusSkin": "sknBtn003E75LatoRegular13pxFFFFFFRad20px",
                "height": "40dp",
                "id": "btnSave",
                "isVisible": true,
                "right": "0px",
                "skin": "sknBtn003E75LatoRegular13pxFFFFFFRad20px",
                "text": "SAVE",
                "top": "0%",
                "width": "125px",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_CENTER,
                "displayText": true,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var btnCancelAdvSel = new kony.ui.Button({
                "centerY": "50%",
                "focusSkin": "sknBtnLatoRegular8b96a513pxKAhover",
                "height": "40dp",
                "id": "btnCancelAdvSel",
                "isVisible": true,
                "right": "145dp",
                "skin": "sknbtnf7f7faLatoRegular4f555dBorder1px8b96a5",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.permission.CANCEL\")",
                "top": "0%",
                "width": "100px",
                "zIndex": 2
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_CENTER,
                "displayText": true,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxRightButtons.add(btnSave, btnCancelAdvSel);
            flxCommonButtons.add(flxSeparator4, flxRightButtons);
            flxAdvancedSelection.add(flxAdvanceSelectionHeader, flxAdvanceSelectionContent, flxCommonButtons);
            flxAdvancedSelectionComponent.add(flxAdvancedSelection);
            var flxFeatureComponent = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "bottom": "0dp",
                "clipBounds": true,
                "id": "flxFeatureComponent",
                "isVisible": false,
                "layoutType": kony.flex.FREE_FORM,
                "left": "230dp",
                "isModalContainer": false,
                "right": "0dp",
                "skin": "slFbox",
                "top": "0dp",
                "zIndex": 10
            }, {}, {});
            flxFeatureComponent.setDefaultUnit(kony.flex.DP);
            var featureAndActions = new com.adminConsole.serviceDefinition.featureAndActions({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "height": "100%",
                "id": "featureAndActions",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "masterType": constants.MASTER_TYPE_DEFAULT,
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0dp",
                "width": "100%",
                "overrides": {
                    "featureAndActions": {
                        "bottom": "viz.val_cleared",
                        "centerX": "viz.val_cleared",
                        "centerY": "viz.val_cleared",
                        "height": "100%",
                        "left": "0dp",
                        "maxHeight": "viz.val_cleared",
                        "maxWidth": "viz.val_cleared",
                        "minHeight": "viz.val_cleared",
                        "minWidth": "viz.val_cleared",
                        "right": "viz.val_cleared",
                        "top": "0dp",
                        "width": "100%"
                    },
                    "flxActions": {
                        "width": "40%"
                    },
                    "flxAssignActions": {
                        "isVisible": false
                    },
                    "flxDropDown": {
                        "isVisible": false
                    },
                    "flxFeatures": {
                        "isVisible": true
                    },
                    "flxFeaturesSegment": {
                        "height": "viz.val_cleared"
                    },
                    "flxReview": {
                        "isVisible": false
                    },
                    "imgCheckBox": {
                        "src": "checkboxnormal.png"
                    },
                    "imgFeatureCheckbox": {
                        "src": "checkboxnormal.png"
                    },
                    "lblHeading": {
                        "text": "Features"
                    },
                    "lblSelect": {
                        "text": "Select All"
                    },
                    "segDropdown": {
                        "top": "6dp"
                    },
                    "segReview": {
                        "height": "viz.val_cleared",
                        "left": "5dp"
                    }
                }
            }, {
                "overrides": {}
            }, {
                "overrides": {}
            });
            flxFeatureComponent.add(featureAndActions);
            var flxAssignRoles = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "bottom": "0dp",
                "clipBounds": true,
                "id": "flxAssignRoles",
                "isVisible": false,
                "layoutType": kony.flex.FLOW_VERTICAL,
                "left": "230dp",
                "isModalContainer": false,
                "right": "0dp",
                "skin": "slFbox",
                "top": "0dp",
                "zIndex": 1
            }, {}, {});
            flxAssignRoles.setDefaultUnit(kony.flex.DP);
            var associatedRoles = new com.adminConsole.serviceDefinition.associatedRoles({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "height": "100%",
                "id": "associatedRoles",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "masterType": constants.MASTER_TYPE_DEFAULT,
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0dp",
                "width": "100%",
                "overrides": {
                    "associatedRoles": {
                        "bottom": "viz.val_cleared",
                        "centerX": "viz.val_cleared",
                        "centerY": "viz.val_cleared",
                        "height": "100%",
                        "left": "0dp",
                        "maxHeight": "viz.val_cleared",
                        "maxWidth": "viz.val_cleared",
                        "minHeight": "viz.val_cleared",
                        "minWidth": "viz.val_cleared",
                        "right": "viz.val_cleared",
                        "top": "0dp",
                        "width": "100%"
                    },
                    "backToRolesList": {
                        "isVisible": false
                    },
                    "commonButtonsRoles.btnCancel": {
                        "left": "viz.val_cleared",
                        "right": "140px"
                    },
                    "commonButtonsRoles.btnSave": {
                        "i18n_text": "kony.i18n.getLocalizedString(\"i18n.permission.SAVE\")",
                        "width": "100px"
                    },
                    "commonButtonsRoles.flxRightButtons": {
                        "width": "330px"
                    },
                    "flxCommonButtons": {
                        "isVisible": true
                    },
                    "flxCommonButtonsRolesSave": {
                        "isVisible": false
                    },
                    "flxRoleUndoHeader": {
                        "isVisible": true
                    },
                    "flxRolesEdit": {
                        "isVisible": false
                    },
                    "flxRolesHeader": {
                        "isVisible": false
                    },
                    "flxRolesSegment": {
                        "height": "75%"
                    }
                }
            }, {
                "overrides": {}
            }, {
                "overrides": {}
            });
            flxAssignRoles.add(associatedRoles);
            var flxAssignLimitsComponent = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "bottom": "0dp",
                "clipBounds": true,
                "id": "flxAssignLimitsComponent",
                "isVisible": false,
                "layoutType": kony.flex.FREE_FORM,
                "left": "230dp",
                "isModalContainer": false,
                "right": "0dp",
                "skin": "slFbox",
                "top": "0dp",
                "zIndex": 10
            }, {}, {});
            flxAssignLimitsComponent.setDefaultUnit(kony.flex.DP);
            var assignLimitsContainer = new com.adminConsole.common.assignLimitsContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "height": "100%",
                "id": "assignLimitsContainer",
                "isVisible": false,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "masterType": constants.MASTER_TYPE_DEFAULT,
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0dp",
                "width": "100%",
                "overrides": {
                    "assignLimitsContainer": {
                        "isVisible": false
                    },
                    "btnOption1": {
                        "bottom": "0dp",
                        "right": "25dp"
                    },
                    "flxBtnContainer": {
                        "width": "100dp"
                    },
                    "flxReset": {
                        "bottom": "viz.val_cleared",
                        "height": "45dp"
                    },
                    "flxScrollAssignLimits": {
                        "top": "45dp"
                    }
                }
            }, {
                "overrides": {}
            }, {
                "overrides": {}
            });
            var flxReset = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "bottom": "610dp",
                "clipBounds": true,
                "height": "45dp",
                "id": "flxReset",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0dp",
                "width": "100%"
            }, {}, {});
            flxReset.setDefaultUnit(kony.flex.DP);
            var flxBtnContainer = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                "centerY": "50%",
                "clipBounds": true,
                "id": "flxBtnContainer",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "isModalContainer": false,
                "right": "0%",
                "skin": "slFbox",
                "width": "100dp"
            }, {}, {});
            flxBtnContainer.setDefaultUnit(kony.flex.DP);
            var btnOption1 = new kony.ui.Button({
                "bottom": "0dp",
                "id": "btnOption1",
                "isVisible": true,
                "right": "25dp",
                "skin": "sknBtnLato117eb013Px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.Common.button.Reset\")",
                "top": "0dp",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_TOP_LEFT,
                "displayText": true,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {
                "hoverSkin": "Btn000000font14px"
            });
            flxBtnContainer.add(btnOption1);
            flxReset.add(flxBtnContainer);
            var flxScrollAssignLimits = new kony.ui.FlexScrollContainer({
                "allowHorizontalBounce": false,
                "allowVerticalBounce": true,
                "bottom": "81dp",
                "bounces": true,
                "clipBounds": true,
                "enableScrolling": true,
                "horizontalScrollIndicator": true,
                "id": "flxScrollAssignLimits",
                "isVisible": true,
                "layoutType": kony.flex.FLOW_VERTICAL,
                "left": "0dp",
                "pagingEnabled": false,
                "scrollDirection": kony.flex.SCROLL_VERTICAL,
                "skin": "slFSbox",
                "top": "45dp",
                "verticalScrollIndicator": true,
                "width": kony.flex.USE_PREFFERED_SIZE
            }, {}, {});
            flxScrollAssignLimits.setDefaultUnit(kony.flex.DP);
            flxScrollAssignLimits.add();
            var flxSeparator1 = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "bottom": "80dp",
                "clipBounds": true,
                "height": "1dp",
                "id": "flxSeparator1",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": 0,
                "isModalContainer": false,
                "skin": "sknflxd6dbe7",
                "width": "100%",
                "zIndex": 1
            }, {}, {});
            flxSeparator1.setDefaultUnit(kony.flex.DP);
            flxSeparator1.add();
            var flxGroupDetailsButtons1 = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "bottom": "0dp",
                "clipBounds": true,
                "height": "80dp",
                "id": "flxGroupDetailsButtons1",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0.00%",
                "isModalContainer": false,
                "skin": "slFbox",
                "width": "100%",
                "zIndex": 1
            }, {}, {});
            flxGroupDetailsButtons1.setDefaultUnit(kony.flex.DP);
            var btnCancel = new kony.ui.Button({
                "centerY": "50%",
                "focusSkin": "sknBtnLatoRegulara5abc413pxKA",
                "height": "40dp",
                "id": "btnCancel",
                "isVisible": true,
                "left": "20px",
                "right": "29.50%",
                "skin": "sknbtnf7f7faLatoRegular4f555dBorder1px8b96a5",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.permission.CANCEL\")",
                "top": "0%",
                "width": "100px",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_CENTER,
                "displayText": true,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {
                "hoverSkin": "sknbtnffffffLatoRegular4f555dBorder1px485c75"
            });
            var flxRightButton = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "centerY": "50%",
                "clipBounds": true,
                "height": "100%",
                "id": "flxRightButton",
                "isVisible": true,
                "layoutType": kony.flex.FLOW_HORIZONTAL,
                "reverseLayoutDirection": true,
                "isModalContainer": false,
                "right": "0px",
                "skin": "slFbox",
                "width": "280px",
                "zIndex": 1
            }, {}, {});
            flxRightButton.setDefaultUnit(kony.flex.DP);
            var btnNext = new kony.ui.Button({
                "centerY": "50%",
                "focusSkin": "sknBtn003E75LatoRegular13pxFFFFFFRad20px",
                "height": "40dp",
                "id": "btnNext",
                "isVisible": true,
                "right": "20dp",
                "skin": "sknBtn003E75LatoRegular13pxFFFFFFRad20px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.permission.NEXT\")",
                "top": "0%",
                "width": "93dp",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_CENTER,
                "displayText": true,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {
                "hoverSkin": "sknBtn005198LatoRegular13pxFFFFFFRad20px"
            });
            var btnAdd = new kony.ui.Button({
                "centerY": "50%",
                "focusSkin": "sknBtn003E75LatoRegular13pxFFFFFFRad20px",
                "height": "40dp",
                "id": "btnAdd",
                "isVisible": false,
                "right": "20px",
                "skin": "sknBtn003E75LatoRegular13pxFFFFFFRad20px",
                "text": "ADD SERVICE DEFINITION",
                "top": "0%",
                "width": "219dp",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_CENTER,
                "displayText": true,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {
                "hoverSkin": "sknBtn005198LatoRegular13pxFFFFFFRad20px"
            });
            var btnUpdate = new kony.ui.Button({
                "centerY": "50%",
                "focusSkin": "sknBtn003E75LatoRegular13pxFFFFFFRad20px",
                "height": "40dp",
                "id": "btnUpdate",
                "isVisible": false,
                "right": "20px",
                "skin": "sknBtn003E75LatoRegular13pxFFFFFFRad20px",
                "text": "UPDATE",
                "top": "0%",
                "width": "100dp",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_CENTER,
                "displayText": true,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {
                "hoverSkin": "sknBtn005198LatoRegular13pxFFFFFFRad20px"
            });
            flxRightButton.add(btnNext, btnAdd, btnUpdate);
            flxGroupDetailsButtons1.add(btnCancel, flxRightButton);
            var flxNoLimitsForFeatureSelected = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "centerY": "50%",
                "clipBounds": true,
                "height": "140dp",
                "id": "flxNoLimitsForFeatureSelected",
                "isVisible": false,
                "layoutType": kony.flex.FLOW_VERTICAL,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0dp",
                "width": "100%"
            }, {}, {});
            flxNoLimitsForFeatureSelected.setDefaultUnit(kony.flex.DP);
            var lblNoLimits = new kony.ui.Label({
                "centerX": "50%",
                "id": "lblNoLimits",
                "isVisible": true,
                "skin": "sknLbl485C75LatoRegular13Px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmGroups.NoLimits\")",
                "top": "0dp",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxNoLimitsForFeatureSelected.add(lblNoLimits);
            flxAssignLimitsComponent.add(assignLimitsContainer, flxReset, flxScrollAssignLimits, flxSeparator1, flxGroupDetailsButtons1, flxNoLimitsForFeatureSelected);
            flxAddGroups.add(flxVerticalTabs, flxGroupDetails, flxSeparator, flxSelectFeatures, flxAdvancedSelectionComponent, flxFeatureComponent, flxAssignRoles, flxAssignLimitsComponent);
            flxRightPannel.add(flxMainHeader, flxBreadCrumbs, flxToastMessage, flxNoGroups, flxGroupList, flxHeaderDropdown, flxViewServiceDef, flxAddGroups);
            var flxLoading = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "100%",
                "id": "flxLoading",
                "isVisible": false,
                "layoutType": kony.flex.FREE_FORM,
                "left": "305dp",
                "isModalContainer": false,
                "right": "0dp",
                "skin": "sknLoadingBlur",
                "top": "0dp",
                "zIndex": 10
            }, {}, {});
            flxLoading.setDefaultUnit(kony.flex.DP);
            var flxImageContainer = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "centerX": "50%",
                "centerY": "50%",
                "clipBounds": true,
                "height": "75px",
                "id": "flxImageContainer",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "isModalContainer": false,
                "skin": "slFbox",
                "width": "75px",
                "zIndex": 1
            }, {}, {});
            flxImageContainer.setDefaultUnit(kony.flex.DP);
            var imgLoading = new kony.ui.Image2({
                "centerX": "50%",
                "centerY": "50%",
                "height": "50px",
                "id": "imgLoading",
                "isVisible": true,
                "skin": "slImage",
                "src": "loadingscreenimage.gif",
                "width": "50px",
                "zIndex": 1
            }, {
                "imageScaleMode": constants.IMAGE_SCALE_MODE_MAINTAIN_ASPECT_RATIO,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxImageContainer.add(imgLoading);
            flxLoading.add(flxImageContainer);
            var flxConfirmGroup = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "100%",
                "id": "flxConfirmGroup",
                "isVisible": false,
                "layoutType": kony.flex.FREE_FORM,
                "masterType": constants.MASTER_TYPE_USERWIDGET,
                "isModalContainer": false,
                "skin": "slFbox",
                "width": "100%",
                "zIndex": 7
            }, {}, {});
            flxConfirmGroup.setDefaultUnit(kony.flex.DP);
            var popUpConfirm = new com.adminConsole.common.popUp1({
                "height": "100%",
                "id": "popUpConfirm",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "masterType": constants.MASTER_TYPE_DEFAULT,
                "isModalContainer": false,
                "skin": "sknflxBg000000Op50",
                "top": "0dp",
                "width": "100%",
                "zIndex": 1,
                "overrides": {
                    "btnPopUpCancel": {
                        "i18n_text": "kony.i18n.getLocalizedString(\"i18n.PopUp.NoLeaveAsIS\")",
                        "right": "20px",
                        "width": kony.flex.USE_PREFFERED_SIZE
                    },
                    "btnPopUpDelete": {
                        "i18n_text": "kony.i18n.getLocalizedString(\"i18n.PopUp.YesDeactivate\")",
                        "minWidth": "viz.val_cleared"
                    },
                    "lblPopUpMainMessage": {
                        "text": "Deactivate Group"
                    },
                    "rtxPopUpDisclaimer": {
                        "bottom": "30dp",
                        "right": "viz.val_cleared",
                        "text": "Are you sure to deactivate the Group Auto Loan.\nThe Group has been assigned to few customers. Deactivating this may result in losing certain entitlements.",
                        "width": "93%"
                    }
                }
            }, {
                "overrides": {}
            }, {
                "overrides": {}
            });
            flxConfirmGroup.add(popUpConfirm);
            var flxErrorPopup = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "100%",
                "id": "flxErrorPopup",
                "isVisible": false,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "sknLoadingBlur",
                "top": "0dp",
                "width": "100%",
                "zIndex": 10
            }, {}, {});
            flxErrorPopup.setDefaultUnit(kony.flex.DP);
            var popUp = new com.adminConsole.common.popUp({
                "height": "100%",
                "id": "popUp",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "masterType": constants.MASTER_TYPE_DEFAULT,
                "isModalContainer": false,
                "skin": "sknflxBg000000Op50",
                "top": "0dp",
                "width": "100%",
                "zIndex": 1,
                "overrides": {
                    "btnPopUpCancel": {
                        "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmAssistedOnboarding.close\")",
                        "right": "20px"
                    },
                    "btnPopUpDelete": {
                        "i18n_text": "kony.i18n.getLocalizedString(\"i18n.PopUp.YesDeactivate\")",
                        "isVisible": false
                    }
                }
            }, {
                "overrides": {}
            }, {
                "overrides": {}
            });
            flxErrorPopup.add(popUp);
            var flxViewRolePopup = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "100%",
                "id": "flxViewRolePopup",
                "isVisible": false,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "sknFocus50",
                "top": "0dp",
                "width": "100%",
                "zIndex": 6
            }, {}, {});
            flxViewRolePopup.setDefaultUnit(kony.flex.DP);
            var flxRoleDetailsContainer = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "centerX": "50%",
                "centerY": "50%",
                "clipBounds": true,
                "height": "650dp",
                "id": "flxRoleDetailsContainer",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "17%",
                "isModalContainer": false,
                "right": "17%",
                "skin": "slFbox0j9f841cc563e4e",
                "zIndex": 1
            }, {}, {});
            flxRoleDetailsContainer.setDefaultUnit(kony.flex.DP);
            var flxRolePopupTopColor = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "10px",
                "id": "flxRolePopupTopColor",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0px",
                "isModalContainer": false,
                "skin": "sknFlxInfoToastBg357C9ENoRadius",
                "top": "0dp",
                "width": "100%",
                "zIndex": 1
            }, {}, {});
            flxRolePopupTopColor.setDefaultUnit(kony.flex.DP);
            flxRolePopupTopColor.add();
            var flxRolePopupBody = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                "bottom": "0dp",
                "clipBounds": true,
                "id": "flxRolePopupBody",
                "isVisible": true,
                "layoutType": kony.flex.FLOW_VERTICAL,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "10px",
                "width": "100%",
                "zIndex": 1
            }, {}, {});
            flxRolePopupBody.setDefaultUnit(kony.flex.DP);
            var flxRoleClosePopup = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "25px",
                "id": "flxRoleClosePopup",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "isModalContainer": false,
                "right": "13dp",
                "skin": "sknCursor",
                "top": "10dp",
                "width": "25px",
                "zIndex": 20
            }, {}, {
                "hoverSkin": "sknCursor"
            });
            flxRoleClosePopup.setDefaultUnit(kony.flex.DP);
            var lblIconClosePopup = new kony.ui.Label({
                "centerX": "51%",
                "centerY": "50.00%",
                "height": "15dp",
                "id": "lblIconClosePopup",
                "isVisible": true,
                "left": "17dp",
                "skin": "sknFontIconSearchCross16px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmAlertsManagement.lblAlertPreviewClose\")",
                "width": "15dp",
                "zIndex": 2
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxRoleClosePopup.add(lblIconClosePopup);
            var flxRoleName = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                "clipBounds": true,
                "id": "flxRoleName",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "-5dp",
                "width": "100%"
            }, {}, {});
            flxRoleName.setDefaultUnit(kony.flex.DP);
            var lblFeatureName = new kony.ui.Label({
                "id": "lblFeatureName",
                "isVisible": true,
                "left": "20dp",
                "skin": "sknLbl192b45LatoReg16px",
                "text": "Administartor",
                "top": 0,
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var lblSeperator = new kony.ui.Label({
                "height": "1px",
                "id": "lblSeperator",
                "isVisible": true,
                "left": "20dp",
                "right": "20dp",
                "skin": "sknlblSeperator",
                "text": ".",
                "top": "30dp",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxRoleName.add(lblFeatureName, lblSeperator);
            var flxViewRoleWrapper = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                "bottom": "0dp",
                "clipBounds": true,
                "id": "flxViewRoleWrapper",
                "isVisible": true,
                "layoutType": kony.flex.FLOW_VERTICAL,
                "left": "0px",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "20px",
                "width": "100%",
                "zIndex": 1
            }, {}, {});
            flxViewRoleWrapper.setDefaultUnit(kony.flex.DP);
            var flxDescriptionViewRole = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                "clipBounds": true,
                "id": "flxDescriptionViewRole",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0dp",
                "width": "100%"
            }, {}, {});
            flxDescriptionViewRole.setDefaultUnit(kony.flex.DP);
            var lblViewRoleDescriptionHeading = new kony.ui.Label({
                "id": "lblViewRoleDescriptionHeading",
                "isVisible": true,
                "left": "20dp",
                "skin": "sknLbl78818A11px",
                "text": "DESCRIPTION",
                "top": "0dp",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var lblViewRoleDescriptionValue = new kony.ui.Label({
                "id": "lblViewRoleDescriptionValue",
                "isVisible": true,
                "left": "20dp",
                "right": "20dp",
                "skin": "sknLbl485C75LatoRegular13Px",
                "text": "This terms & conditions will be used in the footer link of retail banking",
                "top": "25dp",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxDescriptionViewRole.add(lblViewRoleDescriptionHeading, lblViewRoleDescriptionValue);
            var flxFeaturesList = new kony.ui.FlexScrollContainer({
                "allowHorizontalBounce": false,
                "allowVerticalBounce": true,
                "bottom": "0px",
                "bounces": true,
                "clipBounds": true,
                "enableScrolling": true,
                "height": "470dp",
                "horizontalScrollIndicator": true,
                "id": "flxFeaturesList",
                "isVisible": true,
                "layoutType": kony.flex.FLOW_VERTICAL,
                "left": "0dp",
                "pagingEnabled": false,
                "scrollDirection": kony.flex.SCROLL_VERTICAL,
                "skin": "slFSbox",
                "top": "20dp",
                "verticalScrollIndicator": true,
                "width": "100%"
            }, {}, {});
            flxFeaturesList.setDefaultUnit(kony.flex.DP);
            var ViewRoleFeaturesActions = new com.adminConsole.customerRoles.ViewRoleFeaturesActions({
                "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                "id": "ViewRoleFeaturesActions",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "masterType": constants.MASTER_TYPE_DEFAULT,
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0dp",
                "width": "100%",
                "overrides": {
                    "ViewRoleFeaturesActions": {
                        "left": "0dp",
                        "right": "viz.val_cleared",
                        "top": "0dp"
                    },
                    "flxFeatureStatus": {
                        "right": "20dp",
                        "width": "11%",
                        "layoutType": kony.flex.FREE_FORM
                    },
                    "lblActionStatus": {
                        "isVisible": true
                    },
                    "statusIcon": {
                        "left": "0dp",
                        "right": "viz.val_cleared"
                    },
                    "statusValue": {
                        "left": "20dp",
                        "right": "viz.val_cleared"
                    }
                }
            }, {
                "overrides": {}
            }, {
                "overrides": {}
            });
            flxFeaturesList.add(ViewRoleFeaturesActions);
            flxViewRoleWrapper.add(flxDescriptionViewRole, flxFeaturesList);
            flxRolePopupBody.add(flxRoleClosePopup, flxRoleName, flxViewRoleWrapper);
            flxRoleDetailsContainer.add(flxRolePopupTopColor, flxRolePopupBody);
            flxViewRolePopup.add(flxRoleDetailsContainer);
            var flxViewLimitsPopup = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "100%",
                "id": "flxViewLimitsPopup",
                "isVisible": false,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "sknFocus50",
                "top": "0dp",
                "width": "100%",
                "zIndex": 5
            }, {}, {});
            flxViewLimitsPopup.setDefaultUnit(kony.flex.DP);
            var flxViewLimitCont = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "centerX": "50%",
                "centerY": "50%",
                "clipBounds": true,
                "height": "251px",
                "id": "flxViewLimitCont",
                "isVisible": true,
                "layoutType": kony.flex.FLOW_VERTICAL,
                "left": "27%",
                "isModalContainer": false,
                "skin": "slFbox0j9f841cc563e4e",
                "width": "499px",
                "zIndex": 1
            }, {}, {});
            flxViewLimitCont.setDefaultUnit(kony.flex.DP);
            var flxLimitsTopColor = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "10px",
                "id": "flxLimitsTopColor",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0px",
                "isModalContainer": false,
                "skin": "sknFlxInfoToastBg357C9ENoRadius",
                "top": "0dp",
                "width": "100%",
                "zIndex": 1
            }, {}, {});
            flxLimitsTopColor.setDefaultUnit(kony.flex.DP);
            flxLimitsTopColor.add();
            var flxPopupLimitsHeader = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                "clipBounds": true,
                "id": "flxPopupLimitsHeader",
                "isVisible": true,
                "layoutType": kony.flex.FLOW_VERTICAL,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0px",
                "width": "100%",
                "zIndex": 1
            }, {}, {});
            flxPopupLimitsHeader.setDefaultUnit(kony.flex.DP);
            var flxCloseLimits = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "25px",
                "id": "flxCloseLimits",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "isModalContainer": false,
                "right": "20dp",
                "skin": "sknCursor",
                "top": "5dp",
                "width": "25px",
                "zIndex": 20
            }, {}, {
                "hoverSkin": "sknCursor"
            });
            flxCloseLimits.setDefaultUnit(kony.flex.DP);
            var imgClose = new kony.ui.Label({
                "centerX": "51%",
                "centerY": "50.00%",
                "height": "15dp",
                "id": "imgClose",
                "isVisible": true,
                "left": "17dp",
                "skin": "sknFontIconSearchCross16px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmAlertsManagement.lblAlertPreviewClose\")",
                "width": "15dp",
                "zIndex": 2
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxCloseLimits.add(imgClose);
            var flxLimitsHeading = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "40dp",
                "id": "flxLimitsHeading",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0dp",
                "width": "100%",
                "zIndex": 20
            }, {}, {});
            flxLimitsHeading.setDefaultUnit(kony.flex.DP);
            var lblLimitsPopupHeading = new kony.ui.Label({
                "id": "lblLimitsPopupHeading",
                "isVisible": true,
                "left": "20dp",
                "skin": "sknLbl192b45LatoReg16px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmServiceManagement.ViewLimits\")",
                "top": "0dp",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var btnEditActionLimits = new kony.ui.Button({
                "height": "22dp",
                "id": "btnEditActionLimits",
                "isVisible": false,
                "right": "20dp",
                "skin": "sknbtnf7f7faLatoRegular12Px485c75Border1px485C75Radius20px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.permission.Edit\")",
                "top": "0dp",
                "width": "50dp",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_CENTER,
                "displayText": true,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {
                "hoverSkin": "sknbtnffffffLatoRegular12Px485c75Border1px485C75Radius20px"
            });
            var lblLimitsPopupSeperator = new kony.ui.Label({
                "bottom": "0dp",
                "height": "1dp",
                "id": "lblLimitsPopupSeperator",
                "isVisible": true,
                "left": "20dp",
                "right": "20dp",
                "skin": "sknLble1e5edOp100",
                "text": "-",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 2
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxLimitsHeading.add(lblLimitsPopupHeading, btnEditActionLimits, lblLimitsPopupSeperator);
            var flxLimitsContainer = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                "bottom": "30dp",
                "clipBounds": true,
                "id": "flxLimitsContainer",
                "isVisible": true,
                "layoutType": kony.flex.FLOW_VERTICAL,
                "left": "20dp",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0dp",
                "width": "93%",
                "zIndex": 20
            }, {}, {});
            flxLimitsContainer.setDefaultUnit(kony.flex.DP);
            var flxActionLimitsHeaders1 = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                "clipBounds": true,
                "id": "flxActionLimitsHeaders1",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "17dp",
                "width": "100%",
                "zIndex": 1
            }, {}, {});
            flxActionLimitsHeaders1.setDefaultUnit(kony.flex.DP);
            var lblMaxValHeader12 = new kony.ui.Label({
                "id": "lblMaxValHeader12",
                "isVisible": true,
                "left": "0dp",
                "skin": "sknlblLato696c7312px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmServiceManagement.PerTransactionLimitUC\")",
                "top": "0dp",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var lblMaxDailyLimitHeader21 = new kony.ui.Label({
                "id": "lblMaxDailyLimitHeader21",
                "isVisible": true,
                "left": "55%",
                "skin": "sknlblLato696c7312px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmServiceManagement.DailyTransactionLimitUC\")",
                "top": "0dp",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxActionLimitsHeaders1.add(lblMaxValHeader12, lblMaxDailyLimitHeader21);
            var flxActionLimitsValues1 = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                "clipBounds": true,
                "id": "flxActionLimitsValues1",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0dp",
                "width": "100%"
            }, {}, {});
            flxActionLimitsValues1.setDefaultUnit(kony.flex.DP);
            var lblMaxValue12 = new kony.ui.Label({
                "id": "lblMaxValue12",
                "isVisible": true,
                "left": "0dp",
                "skin": "sknLatoSemibold485c7313px",
                "text": "$10",
                "top": "10dp",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var lblMaxDailyLimitValue21 = new kony.ui.Label({
                "id": "lblMaxDailyLimitValue21",
                "isVisible": true,
                "left": "55%",
                "skin": "sknLatoSemibold485c7313px",
                "text": "$10",
                "top": "10dp",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxActionLimitsValues1.add(lblMaxValue12, lblMaxDailyLimitValue21);
            var flxActionLimitsHeaders2 = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                "clipBounds": true,
                "id": "flxActionLimitsHeaders2",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "20dp",
                "width": "100%"
            }, {}, {});
            flxActionLimitsHeaders2.setDefaultUnit(kony.flex.DP);
            var lblWeeklyLimitHeader22 = new kony.ui.Label({
                "id": "lblWeeklyLimitHeader22",
                "isVisible": true,
                "left": "0dp",
                "skin": "sknlblLato696c7312px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmServiceManagement.WeeklyTransLimitUC\")",
                "top": "0dp",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxActionLimitsHeaders2.add(lblWeeklyLimitHeader22);
            var flxActionLimitsValues2 = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                "clipBounds": true,
                "id": "flxActionLimitsValues2",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0dp",
                "width": "100%",
                "zIndex": 1
            }, {}, {});
            flxActionLimitsValues2.setDefaultUnit(kony.flex.DP);
            var lblWeeklyLimitValue22 = new kony.ui.Label({
                "id": "lblWeeklyLimitValue22",
                "isVisible": true,
                "left": "0dp",
                "skin": "sknLatoSemibold485c7313px",
                "text": "$10",
                "top": "10dp",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxActionLimitsValues2.add(lblWeeklyLimitValue22);
            flxLimitsContainer.add(flxActionLimitsHeaders1, flxActionLimitsValues1, flxActionLimitsHeaders2, flxActionLimitsValues2);
            flxPopupLimitsHeader.add(flxCloseLimits, flxLimitsHeading, flxLimitsContainer);
            flxViewLimitCont.add(flxLimitsTopColor, flxPopupLimitsHeader);
            flxViewLimitsPopup.add(flxViewLimitCont);
            flxMain.add(flxLeftPannel, flxRightPannel, flxLoading, flxConfirmGroup, flxErrorPopup, flxViewRolePopup, flxViewLimitsPopup);
            var flxEditCancelConfirmation = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "100%",
                "id": "flxEditCancelConfirmation",
                "isVisible": false,
                "layoutType": kony.flex.FREE_FORM,
                "left": "10dp",
                "masterType": constants.MASTER_TYPE_USERWIDGET,
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "10dp",
                "width": "100%",
                "zIndex": 5
            }, {}, {});
            flxEditCancelConfirmation.setDefaultUnit(kony.flex.DP);
            var popUpCancelEdits = new com.adminConsole.common.popUpCancelEdits({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "height": "100%",
                "id": "popUpCancelEdits",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "masterType": constants.MASTER_TYPE_DEFAULT,
                "isModalContainer": false,
                "skin": "sknflxBg000000Op50",
                "top": "0dp",
                "width": "100%",
                "zIndex": 1,
                "overrides": {
                    "btnPopUpCancel": {
                        "i18n_text": "kony.i18n.getLocalizedString(\"i8n.navigation.cancelButton\")"
                    },
                    "btnPopUpDelete": {
                        "i18n_text": "kony.i18n.getLocalizedString(\"i8n.navigation.okButton\")"
                    },
                    "lblPopUpMainMessage": {
                        "i18n_text": "kony.i18n.getLocalizedString(\"i8n.navigation.cancelChanges\")"
                    },
                    "popUpCancelEdits": {
                        "bottom": "viz.val_cleared",
                        "centerX": "viz.val_cleared",
                        "centerY": "viz.val_cleared",
                        "height": "100%",
                        "left": "0dp",
                        "maxHeight": "viz.val_cleared",
                        "maxWidth": "viz.val_cleared",
                        "minHeight": "viz.val_cleared",
                        "minWidth": "viz.val_cleared",
                        "right": "viz.val_cleared",
                        "top": "0dp",
                        "width": "100%"
                    },
                    "rtxPopUpDisclaimer": {
                        "i18n_text": "kony.i18n.getLocalizedString(\"i8n.navigation.cancelChangesBody\")"
                    }
                }
            }, {
                "overrides": {}
            }, {
                "overrides": {}
            });
            flxEditCancelConfirmation.add(popUpCancelEdits);
            this.add(flxMain, flxEditCancelConfirmation);
        };
        return [{
            "addWidgets": addWidgetsfrmServiceDefinition,
            "enabledForIdleTimeout": true,
            "id": "frmServiceDefinition",
            "layoutType": kony.flex.FREE_FORM,
            "needAppMenu": false,
            "preShow": function(eventobject) {
                controller.AS_Form_d48e26b33df94d23b20e8944b9fb9e29(eventobject);
            },
            "skin": "slForm"
        }, {
            "displayOrientation": constants.FORM_DISPLAY_ORIENTATION_PORTRAIT,
            "layoutType": kony.flex.FREE_FORM,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, {
            "onDeviceBack": controller.AS_Form_f56916922aec4295a8a0e90ad0ada17b,
            "retainScrollPosition": false
        }]
    }
});