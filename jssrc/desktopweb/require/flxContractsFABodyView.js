define("flxContractsFABodyView", function() {
    return function(controller) {
        var flxContractsFABodyView = new kony.ui.FlexContainer({
            "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
            "clipBounds": true,
            "id": "flxContractsFABodyView",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "left": "0dp",
            "isModalContainer": false,
            "skin": "sknFlxFFFFFF1000",
            "top": "0dp",
            "width": "100%"
        }, {}, {});
        flxContractsFABodyView.setDefaultUnit(kony.flex.DP);
        var flxViewActionBody = new kony.ui.FlexContainer({
            "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
            "bottom": "10dp",
            "clipBounds": true,
            "id": "flxViewActionBody",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "left": "57px",
            "isModalContainer": false,
            "right": "20px",
            "skin": "slFbox",
            "top": "0px"
        }, {}, {});
        flxViewActionBody.setDefaultUnit(kony.flex.DP);
        var lblActionName = new kony.ui.Label({
            "bottom": "5dp",
            "id": "lblActionName",
            "isVisible": true,
            "left": "0dp",
            "skin": "sknLbl485C75LatoRegular13Px",
            "text": "View History of International Transfers",
            "top": "11px",
            "width": "25%",
            "zIndex": 1
        }, {
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, {});
        var lblActionDesc = new kony.ui.Label({
            "id": "lblActionDesc",
            "isVisible": true,
            "left": "30%",
            "skin": "sknLbl485C75LatoRegular13Px",
            "text": "Ability to view the list of all transactions made to own accounts within the same FI",
            "top": "11px",
            "width": "55%",
            "zIndex": 1
        }, {
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, {});
        var lblCustom = new kony.ui.Label({
            "id": "lblCustom",
            "isVisible": false,
            "left": "82.50%",
            "skin": "sknLbl485c75LatoRegItalic13px",
            "text": "Custom",
            "top": "11dp",
            "width": "52dp"
        }, {
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, {});
        var flxFeatureStatus = new kony.ui.FlexContainer({
            "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
            "clipBounds": true,
            "id": "flxFeatureStatus",
            "isVisible": true,
            "layoutType": kony.flex.FLOW_HORIZONTAL,
            "reverseLayoutDirection": false,
            "left": "90%",
            "isModalContainer": false,
            "skin": "slFbox",
            "top": "11dp",
            "width": "10%"
        }, {}, {});
        flxFeatureStatus.setDefaultUnit(kony.flex.DP);
        var statusIcon = new kony.ui.Label({
            "id": "statusIcon",
            "isVisible": true,
            "right": "5dp",
            "skin": "sknFontIconActivate",
            "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmAlertsManagement.lblViewAlertStatusKey\")",
            "top": 2,
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, {
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, {});
        var statusValue = new kony.ui.Label({
            "id": "statusValue",
            "isVisible": true,
            "right": "20dp",
            "skin": "sknLbl485C75LatoRegular13Px",
            "i18n_text": "kony.i18n.getLocalizedString(\"i18n.secureimage.Active\")",
            "top": "0px",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, {
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, {});
        flxFeatureStatus.add(statusIcon, statusValue);
        flxViewActionBody.add(lblActionName, lblActionDesc, lblCustom, flxFeatureStatus);
        flxContractsFABodyView.add(flxViewActionBody);
        return flxContractsFABodyView;
    }
})