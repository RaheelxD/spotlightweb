define("MFAModule/frmMFAScenarios", function() {
    return function(controller) {
        function addWidgetsfrmMFAScenarios() {
            this.setDefaultUnit(kony.flex.DP);
            var flxMain = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "100%",
                "id": "flxMain",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0dp",
                "width": "100%",
                "zIndex": 1
            }, {}, {});
            flxMain.setDefaultUnit(kony.flex.DP);
            var flxLeftPanel = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "100%",
                "id": "flxLeftPanel",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0dp",
                "width": "306px",
                "zIndex": 1
            }, {}, {});
            flxLeftPanel.setDefaultUnit(kony.flex.DP);
            var leftMenuNew = new com.adminConsole.navigation.leftMenuNew({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "height": "100%",
                "id": "leftMenuNew",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "masterType": constants.MASTER_TYPE_DEFAULT,
                "isModalContainer": false,
                "skin": "sknFlxBg003E75Op100",
                "top": "0dp",
                "width": "100%",
                "overrides": {
                    "brandLogo": {
                        "src": "infinity_dbx_c360_logo_white_2x.png"
                    },
                    "flxScrollMenu": {
                        "bottom": "40dp",
                        "left": "0dp",
                        "top": "80dp"
                    },
                    "imgBottomLogo": {
                        "src": "logo_white_2x.png"
                    },
                    "leftMenuNew": {
                        "bottom": "viz.val_cleared",
                        "centerX": "viz.val_cleared",
                        "centerY": "viz.val_cleared",
                        "height": "100%",
                        "left": "0dp",
                        "maxHeight": "viz.val_cleared",
                        "maxWidth": "viz.val_cleared",
                        "minHeight": "viz.val_cleared",
                        "minWidth": "viz.val_cleared",
                        "right": "viz.val_cleared",
                        "top": "0dp",
                        "width": "100%"
                    },
                    "segMenu": {
                        "left": "0dp",
                        "top": "0dp"
                    }
                }
            }, {
                "overrides": {}
            }, {
                "overrides": {}
            });
            flxLeftPanel.add(leftMenuNew);
            var flxRightPanel = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "100%",
                "id": "flxRightPanel",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "305px",
                "isModalContainer": false,
                "right": 0,
                "skin": "CopyslFbox0j190b3ed67d04b",
                "top": "0dp",
                "zIndex": 1
            }, {}, {});
            flxRightPanel.setDefaultUnit(kony.flex.DP);
            var flxMainHeader = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "108dp",
                "id": "flxMainHeader",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0dp",
                "width": "100%",
                "zIndex": 1
            }, {}, {});
            flxMainHeader.setDefaultUnit(kony.flex.DP);
            var mainHeader = new com.adminConsole.header.mainHeader({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "height": "107px",
                "id": "mainHeader",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0",
                "masterType": constants.MASTER_TYPE_DEFAULT,
                "isModalContainer": false,
                "skin": "CopyslFbox1",
                "top": "0",
                "width": "100%",
                "overrides": {
                    "btnAddNewOption": {
                        "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmMFAScenarios.CREATE_SCENARIO\")",
                        "right": "0dp"
                    },
                    "btnDropdownList": {
                        "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmMFAScenarios.ManageConfigurationUC\")",
                        "isVisible": true,
                        "right": "185dp"
                    },
                    "flxButtons": {
                        "isVisible": true,
                        "width": "420dp"
                    },
                    "flxHeaderSeperator": {
                        "isVisible": false
                    },
                    "flxMainHeader": {
                        "left": "0dp",
                        "top": "0dp"
                    },
                    "imgLogout": {
                        "src": "img_logout.png"
                    },
                    "lblHeading": {
                        "bottom": "15dp",
                        "centerY": "viz.val_cleared",
                        "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmMFAScenarios.MultifactorAuthentication\")",
                        "width": kony.flex.USE_PREFFERED_SIZE
                    },
                    "mainHeader": {
                        "height": "107px",
                        "left": "0",
                        "top": "0"
                    }
                }
            }, {
                "overrides": {}
            }, {
                "overrides": {}
            });
            flxMainHeader.add(mainHeader);
            var flxHeaderDropdown = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                "clipBounds": true,
                "id": "flxHeaderDropdown",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "isModalContainer": false,
                "right": "35dp",
                "skin": "slFbox",
                "top": "20dp",
                "width": "400dp",
                "zIndex": 8
            }, {}, {});
            flxHeaderDropdown.setDefaultUnit(kony.flex.DP);
            var dropdownMainHeader = new com.adminConsole.common.dropdownMainHeader({
                "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                "id": "dropdownMainHeader",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "masterType": constants.MASTER_TYPE_DEFAULT,
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0dp",
                "width": "100%",
                "overrides": {
                    "flxDropdown": {
                        "isVisible": false
                    },
                    "imgUpArrow": {
                        "src": "uparrow_2x.png"
                    }
                }
            }, {
                "overrides": {}
            }, {
                "overrides": {}
            });
            flxHeaderDropdown.add(dropdownMainHeader);
            var flxBreadcrumb = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "20px",
                "id": "flxBreadcrumb",
                "isVisible": false,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "98dp",
                "width": "100%",
                "zIndex": 10
            }, {}, {});
            flxBreadcrumb.setDefaultUnit(kony.flex.DP);
            var breadcrumbs = new com.adminConsole.common.breadcrumbs({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "height": "20px",
                "id": "breadcrumbs",
                "isVisible": true,
                "layoutType": kony.flex.FLOW_HORIZONTAL,
                "left": "0dp",
                "masterType": constants.MASTER_TYPE_DEFAULT,
                "isModalContainer": false,
                "skin": "sknflxf5f6f8Op100",
                "top": "0dp",
                "width": "100%",
                "overrides": {
                    "breadcrumbs": {
                        "isVisible": true
                    },
                    "btnBackToMain": {
                        "text": "MFA SCENARIO LIST"
                    },
                    "fontIconBreadcrumbsRight": {
                        "left": "12px"
                    },
                    "imgBreadcrumbsDown": {
                        "src": "img_down_arrow.png"
                    },
                    "imgBreadcrumbsRight": {
                        "src": "img_breadcrumb_arrow.png"
                    },
                    "imgBreadcrumbsRight2": {
                        "src": "img_breadcrumb_arrow.png"
                    },
                    "imgBreadcrumbsRight3": {
                        "src": "img_breadcrumb_arrow.png"
                    },
                    "lblCurrentScreen": {
                        "text": "TRANSACTIONAL -P2P TRANSFER"
                    }
                }
            }, {
                "overrides": {}
            }, {
                "overrides": {}
            });
            flxBreadcrumb.add(breadcrumbs);
            var flxScrollMainContent = new kony.ui.FlexContainer({
                "bottom": "20px",
                "clipBounds": true,
                "id": "flxScrollMainContent",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "isModalContainer": false,
                "right": "0dp",
                "skin": "slFbox",
                "top": "107dp",
                "zIndex": 5
            }, {}, {});
            flxScrollMainContent.setDefaultUnit(kony.flex.DP);
            var flxMFANotification = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "40dp",
                "id": "flxMFANotification",
                "isVisible": false,
                "layoutType": kony.flex.FREE_FORM,
                "left": "35dp",
                "isModalContainer": false,
                "right": "35px",
                "skin": "slFbox",
                "top": "0dp",
                "zIndex": 1
            }, {}, {});
            flxMFANotification.setDefaultUnit(kony.flex.DP);
            var NotificationFlagMessage = new com.adminConsole.common.NotificationFlagMessage({
                "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                "id": "NotificationFlagMessage",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "masterType": constants.MASTER_TYPE_DEFAULT,
                "isModalContainer": false,
                "right": "0dp",
                "skin": "slFbox",
                "top": "0dp",
                "overrides": {
                    "NotificationFlagMessage": {
                        "right": "0dp",
                        "width": "viz.val_cleared"
                    },
                    "lblMessage": {
                        "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmMFAScenarios.AllFeaturesConfiguredForAllAppsMsg\")"
                    },
                    "lblNotificationIcon": {
                        "centerX": "45%",
                        "centerY": "45%"
                    }
                }
            }, {
                "overrides": {}
            }, {
                "overrides": {}
            });
            flxMFANotification.add(NotificationFlagMessage);
            var flxMFAScenarios = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "bottom": "0px",
                "clipBounds": true,
                "id": "flxMFAScenarios",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "35px",
                "isModalContainer": false,
                "right": "35px",
                "skin": "sknflxffffffop100Bordercbcdd1Radius3px",
                "top": "0px",
                "zIndex": 2
            }, {}, {});
            flxMFAScenarios.setDefaultUnit(kony.flex.DP);
            var flxSubHeadingContainer = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "55dp",
                "id": "flxSubHeadingContainer",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0dp",
                "width": "100%",
                "zIndex": 50
            }, {}, {});
            flxSubHeadingContainer.setDefaultUnit(kony.flex.DP);
            var lblSubHeading = new kony.ui.Label({
                "centerY": "50%",
                "id": "lblSubHeading",
                "isVisible": true,
                "left": "20dp",
                "skin": "sknLbl192B4518pxLatoReg",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmMFAScenarios.MfaScenarios\")",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var lblSubHeadingSeperator = new kony.ui.Label({
                "bottom": "0dp",
                "height": "1dp",
                "id": "lblSubHeadingSeperator",
                "isVisible": true,
                "left": "0dp",
                "right": "0dp",
                "skin": "sknLblBgE4E6ECSeperator",
                "text": "-",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxSubHeadingContainer.add(lblSubHeading, lblSubHeadingSeperator);
            var flxMainSubHeader = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "70px",
                "id": "flxMainSubHeader",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "55dp",
                "width": "100%",
                "zIndex": 10
            }, {}, {});
            flxMainSubHeader.setDefaultUnit(kony.flex.DP);
            var subHeader = new com.adminConsole.header.subHeader({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "centerY": "50%",
                "height": "70dp",
                "id": "subHeader",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "masterType": constants.MASTER_TYPE_DEFAULT,
                "isModalContainer": false,
                "skin": "CopyslFbox2",
                "width": "100%",
                "overrides": {
                    "flxMenu": {
                        "isVisible": false
                    },
                    "flxSearch": {
                        "right": "20dp",
                        "width": "500px"
                    },
                    "flxSearchContainer": {
                        "centerY": "50%",
                        "height": "40px",
                        "top": "viz.val_cleared",
                        "width": "500px"
                    },
                    "flxSubHeader": {
                        "centerX": "viz.val_cleared",
                        "centerY": "viz.val_cleared",
                        "top": "0dp"
                    },
                    "subHeader": {
                        "centerY": "50%",
                        "height": "70dp",
                        "top": "viz.val_cleared"
                    },
                    "tbxSearchBox": {
                        "placeholder": "Search by Feature/Action"
                    }
                }
            }, {
                "overrides": {}
            }, {
                "overrides": {}
            });
            var flxAppFilterContainer = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                "centerY": "50%",
                "clipBounds": true,
                "id": "flxAppFilterContainer",
                "isVisible": false,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "sknFlxPointer",
                "top": "0%",
                "width": "200dp",
                "zIndex": 2
            }, {}, {});
            flxAppFilterContainer.setDefaultUnit(kony.flex.DP);
            var lblAlertFilter = new kony.ui.Label({
                "centerY": "50%",
                "height": "25dp",
                "id": "lblAlertFilter",
                "isVisible": true,
                "left": "0dp",
                "skin": "sknLblIcomoon20px485c75",
                "text": "",
                "top": "0dp",
                "width": "25dp",
                "zIndex": 5
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_CENTER,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var lblAppFilterTitle = new kony.ui.Label({
                "centerY": "50%",
                "height": "25dp",
                "id": "lblAppFilterTitle",
                "isVisible": true,
                "left": "30dp",
                "skin": "sknlblLatoRegular00000013px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmMFAScenarios.All_Apps\")",
                "top": "0dp",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 5
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_CENTER,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxAppFilterContainer.add(lblAlertFilter, lblAppFilterTitle);
            flxMainSubHeader.add(subHeader, flxAppFilterContainer);
            var flxMFAConfigContainer = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "bottom": "0dp",
                "clipBounds": true,
                "id": "flxMFAConfigContainer",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "isModalContainer": false,
                "right": "0dp",
                "skin": "slFbox",
                "top": "125px",
                "zIndex": 2
            }, {}, {});
            flxMFAConfigContainer.setDefaultUnit(kony.flex.DP);
            var lblSubHeaderSegSeperator = new kony.ui.Label({
                "height": "1dp",
                "id": "lblSubHeaderSegSeperator",
                "isVisible": true,
                "left": "0dp",
                "right": "0dp",
                "skin": "sknLblBgE4E6ECSeperator",
                "text": "-",
                "top": "0dp",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var flxHeaderPermissions = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "50px",
                "id": "flxHeaderPermissions",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "20dp",
                "isModalContainer": false,
                "right": "20dp",
                "skin": "slFbox",
                "top": "0dp"
            }, {}, {});
            flxHeaderPermissions.setDefaultUnit(kony.flex.DP);
            var flxApplicationName = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "centerY": "50%",
                "clipBounds": true,
                "height": "100%",
                "id": "flxApplicationName",
                "isVisible": true,
                "layoutType": kony.flex.FLOW_HORIZONTAL,
                "left": "10px",
                "isModalContainer": false,
                "onClick": controller.AS_FlexContainer_a5084e29a733477596892b84c89d16f9,
                "skin": "slFbox",
                "top": 0,
                "width": "18%",
                "zIndex": 1
            }, {}, {});
            flxApplicationName.setDefaultUnit(kony.flex.DP);
            var lblApplicationHeaderName = new kony.ui.Label({
                "centerY": "50%",
                "id": "lblApplicationHeaderName",
                "isVisible": true,
                "left": "0px",
                "skin": "sknlblLato696c7312px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmmfascenarios.lblApplication\")",
                "top": 0,
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {
                "hoverSkin": "sknlblLatoBold00000012px"
            });
            var fontIconApplicationFilter = new kony.ui.Label({
                "centerY": "50%",
                "id": "fontIconApplicationFilter",
                "isVisible": true,
                "left": "10dp",
                "skin": "sknIcon15px",
                "text": "",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {
                "hoverSkin": "sknlblCursorFont"
            });
            flxApplicationName.add(lblApplicationHeaderName, fontIconApplicationFilter);
            var flxFeatureName = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "centerY": "50%",
                "clipBounds": true,
                "height": "100%",
                "id": "flxFeatureName",
                "isVisible": true,
                "layoutType": kony.flex.FLOW_HORIZONTAL,
                "left": "22%",
                "isModalContainer": false,
                "onClick": controller.AS_FlexContainer_af76450867de4196a1e2508dc8c1eae6,
                "skin": "slFbox",
                "top": 0,
                "width": "16%",
                "zIndex": 1
            }, {}, {});
            flxFeatureName.setDefaultUnit(kony.flex.DP);
            var lblFeatureName = new kony.ui.Label({
                "centerY": "50%",
                "id": "lblFeatureName",
                "isVisible": true,
                "left": "0px",
                "skin": "sknlblLato696c7312px",
                "text": "FEATURE",
                "top": 0,
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {
                "hoverSkin": "sknlblLatoBold00000012px"
            });
            var fontIconSortFeature = new kony.ui.Label({
                "centerY": "50%",
                "id": "fontIconSortFeature",
                "isVisible": true,
                "left": "5dp",
                "skin": "sknIcon12pxBlack",
                "text": "",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {
                "hoverSkin": "sknIcon12pxBlackHover"
            });
            flxFeatureName.add(lblFeatureName, fontIconSortFeature);
            var flxScenarioName = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "centerY": "50%",
                "clipBounds": true,
                "height": "100%",
                "id": "flxScenarioName",
                "isVisible": true,
                "layoutType": kony.flex.FLOW_HORIZONTAL,
                "left": "40%",
                "isModalContainer": false,
                "onClick": controller.AS_FlexContainer_a5084e29a733477596892b84c89d16f9,
                "skin": "slFbox",
                "top": 0,
                "width": "15%",
                "zIndex": 1
            }, {}, {});
            flxScenarioName.setDefaultUnit(kony.flex.DP);
            var lblScenarioHeaderName = new kony.ui.Label({
                "centerY": "50%",
                "id": "lblScenarioHeaderName",
                "isVisible": true,
                "left": "0%",
                "skin": "sknlblLato696c7312px",
                "text": "SCENARIO TYPE",
                "top": 0,
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {
                "hoverSkin": "sknlblLatoBold00000012px"
            });
            var fontIconScenarioTypeFilter = new kony.ui.Label({
                "centerY": "50%",
                "id": "fontIconScenarioTypeFilter",
                "isVisible": true,
                "left": "10dp",
                "skin": "sknIcon15px",
                "text": "",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {
                "hoverSkin": "sknlblCursorFont"
            });
            flxScenarioName.add(lblScenarioHeaderName, fontIconScenarioTypeFilter);
            var flxAction = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "centerY": "50%",
                "clipBounds": true,
                "height": "100%",
                "id": "flxAction",
                "isVisible": true,
                "layoutType": kony.flex.FLOW_HORIZONTAL,
                "left": "57%",
                "isModalContainer": false,
                "onClick": controller.AS_FlexContainer_fbe01558a0a64e0ca4625eeff7f71077,
                "skin": "slFbox",
                "top": 0,
                "width": "28%",
                "zIndex": 1
            }, {}, {});
            flxAction.setDefaultUnit(kony.flex.DP);
            var lblAction = new kony.ui.Label({
                "centerY": "50%",
                "id": "lblAction",
                "isVisible": true,
                "left": "0%",
                "skin": "sknlblLato696c7312px",
                "text": "ACTION",
                "top": 0,
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {
                "hoverSkin": "sknlblLatoBold00000012px"
            });
            var fontIconSortAction = new kony.ui.Label({
                "centerY": "50%",
                "id": "fontIconSortAction",
                "isVisible": true,
                "left": "5dp",
                "skin": "sknIcon15px",
                "text": "",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {
                "hoverSkin": "sknlblCursorFont"
            });
            flxAction.add(lblAction, fontIconSortAction);
            var flxHeaderStatus = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "centerY": "50%",
                "clipBounds": true,
                "height": "100%",
                "id": "flxHeaderStatus",
                "isVisible": true,
                "layoutType": kony.flex.FLOW_HORIZONTAL,
                "left": "85%",
                "isModalContainer": false,
                "onClick": controller.AS_FlexContainer_c62ddb6717304b7199d5a79964e58736,
                "skin": "sknFlxPointer",
                "top": "0dp",
                "width": "12%",
                "zIndex": 1
            }, {}, {});
            flxHeaderStatus.setDefaultUnit(kony.flex.DP);
            var lblStatus = new kony.ui.Label({
                "centerY": "50%",
                "id": "lblStatus",
                "isVisible": true,
                "left": "0px",
                "skin": "sknlblLato696c7312px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.Group.SERVICE\")",
                "top": 0,
                "width": "55px",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {
                "hoverSkin": "sknlblLatoBold00000012px"
            });
            var fontIconFilterStatus = new kony.ui.Label({
                "centerY": "50%",
                "id": "fontIconFilterStatus",
                "isVisible": true,
                "left": "10dp",
                "skin": "sknLblIcomoon20px485c75",
                "text": "",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {
                "hoverSkin": "sknLblIcomoon20px485c75"
            });
            flxHeaderStatus.add(lblStatus, fontIconFilterStatus);
            var lblHeaderSeperator = new kony.ui.Label({
                "bottom": 0,
                "height": "1px",
                "id": "lblHeaderSeperator",
                "isVisible": true,
                "left": "0dp",
                "right": "0dp",
                "skin": "sknLblTableHeaderLine",
                "text": "-",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxHeaderPermissions.add(flxApplicationName, flxFeatureName, flxScenarioName, flxAction, flxHeaderStatus, lblHeaderSeperator);
            var flxSegmentMFAScenarios = new kony.ui.FlexScrollContainer({
                "allowHorizontalBounce": false,
                "allowVerticalBounce": true,
                "bottom": "0dp",
                "bounces": true,
                "clipBounds": true,
                "enableScrolling": true,
                "horizontalScrollIndicator": true,
                "id": "flxSegmentMFAScenarios",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0px",
                "pagingEnabled": false,
                "right": "0dp",
                "scrollDirection": kony.flex.SCROLL_VERTICAL,
                "top": "50px",
                "verticalScrollIndicator": true,
                "zIndex": 1
            }, {}, {});
            flxSegmentMFAScenarios.setDefaultUnit(kony.flex.DP);
            var segMFAScenarios = new kony.ui.SegmentedUI2({
                "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                "data": [{
                    "flxViewEditButton": "kony.i18n.getLocalizedString(\"i18n.permission.Edit\")",
                    "fontIconImgViewDescription": "",
                    "fontIconStatusImg": "",
                    "imgOptions": "",
                    "imgServiceStatus": "",
                    "lblAction": "Create Payments",
                    "lblApplicationName": "Bill Payment",
                    "lblFeatureName": "Bill Payment",
                    "lblIconOptions": "",
                    "lblScenarioType": "Monetary",
                    "lblSeperator": "",
                    "lblServiceStatus": "Active"
                }],
                "groupCells": false,
                "id": "segMFAScenarios",
                "isVisible": true,
                "left": "20dp",
                "needPageIndicator": true,
                "pageOffDotImage": "pageoffdot.png",
                "pageOnDotImage": "pageondot.png",
                "retainSelection": false,
                "right": "20dp",
                "rowFocusSkin": "seg2Focus",
                "rowSkin": "sknSegRowTransparent",
                "rowTemplate": "flxSegMFAConfigs",
                "sectionHeaderSkin": "sliPhoneSegmentHeader",
                "selectionBehavior": constants.SEGUI_DEFAULT_BEHAVIOR,
                "separatorRequired": false,
                "showScrollbars": false,
                "top": "0dp",
                "viewType": constants.SEGUI_VIEW_TYPE_TABLEVIEW,
                "widgetDataMap": {
                    "flxAccordianContainer": "flxAccordianContainer",
                    "flxArrow": "flxArrow",
                    "flxContent": "flxContent",
                    "flxOptions": "flxOptions",
                    "flxSegMFAConfigs": "flxSegMFAConfigs",
                    "flxStatus": "flxStatus",
                    "flxViewEditButton": "flxViewEditButton",
                    "fontIconImgViewDescription": "fontIconImgViewDescription",
                    "fontIconStatusImg": "fontIconStatusImg",
                    "imgOptions": "imgOptions",
                    "imgServiceStatus": "imgServiceStatus",
                    "lblAction": "lblAction",
                    "lblApplicationName": "lblApplicationName",
                    "lblFeatureName": "lblFeatureName",
                    "lblIconOptions": "lblIconOptions",
                    "lblScenarioType": "lblScenarioType",
                    "lblSeperator": "lblSeperator",
                    "lblServiceStatus": "lblServiceStatus"
                },
                "zIndex": 1
            }, {
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var flxSeparator = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "1dp",
                "id": "flxSeparator",
                "isVisible": false,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "CopyslFbox0d26883c7010e40",
                "top": "35px",
                "width": "100%",
                "zIndex": 1
            }, {}, {});
            flxSeparator.setDefaultUnit(kony.flex.DP);
            flxSeparator.add();
            flxSegmentMFAScenarios.add(segMFAScenarios, flxSeparator);
            var flxSelectOptions = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                "clipBounds": false,
                "id": "flxSelectOptions",
                "isVisible": true,
                "layoutType": kony.flex.FLOW_VERTICAL,
                "isModalContainer": false,
                "right": "25px",
                "skin": "slFbox",
                "top": "100px",
                "width": "130px",
                "zIndex": 100
            }, {}, {});
            flxSelectOptions.setDefaultUnit(kony.flex.DP);
            var flxUpArrowImage = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "12px",
                "id": "flxUpArrowImage",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0px",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0px",
                "width": "100%",
                "zIndex": 2
            }, {}, {});
            flxUpArrowImage.setDefaultUnit(kony.flex.DP);
            var imgUpArrow = new kony.ui.Image2({
                "height": "12dp",
                "id": "imgUpArrow",
                "isVisible": true,
                "right": "20dp",
                "skin": "slImage",
                "src": "uparrow_2x.png",
                "top": "0dp",
                "width": "15dp",
                "zIndex": 1
            }, {
                "imageScaleMode": constants.IMAGE_SCALE_MODE_FIT_TO_DIMENSIONS,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxUpArrowImage.add(imgUpArrow);
            var flxOptionsMenuContainer = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                "clipBounds": true,
                "id": "flxOptionsMenuContainer",
                "isVisible": true,
                "layoutType": kony.flex.FLOW_VERTICAL,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "sknflxffffffop100dbdbe6Radius3px",
                "top": "-1dp",
                "width": "100%"
            }, {}, {});
            flxOptionsMenuContainer.setDefaultUnit(kony.flex.DP);
            var flxEdit = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "35dp",
                "id": "flxEdit",
                "isVisible": true,
                "layoutType": kony.flex.FLOW_HORIZONTAL,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "CopyslFbox0i3963a83703242",
                "top": "10dp",
                "width": "100%",
                "zIndex": 1
            }, {}, {
                "hoverSkin": "sknContextualMenuEntryHover"
            });
            flxEdit.setDefaultUnit(kony.flex.DP);
            var fonticonEdit = new kony.ui.Label({
                "centerY": "50%",
                "id": "fonticonEdit",
                "isVisible": true,
                "left": "15px",
                "skin": "sknFontIconOptionMenuRow",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmFAQ.fonticonEdit\")",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var imgOption1 = new kony.ui.Image2({
                "centerY": "50%",
                "height": "15dp",
                "id": "imgOption1",
                "isVisible": false,
                "left": "15dp",
                "skin": "slImage",
                "src": "edit2x.png",
                "width": "15dp",
                "zIndex": 1
            }, {
                "imageScaleMode": constants.IMAGE_SCALE_MODE_MAINTAIN_ASPECT_RATIO,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var lblOption1 = new kony.ui.Label({
                "centerY": "50%",
                "id": "lblOption1",
                "isVisible": true,
                "left": "10dp",
                "skin": "sknlblLatoBold35475f14px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.roles.Edit\")",
                "top": "4dp",
                "width": "70px",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxEdit.add(fonticonEdit, imgOption1, lblOption1);
            var flxDeactivate = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "35dp",
                "id": "flxDeactivate",
                "isVisible": true,
                "layoutType": kony.flex.FLOW_HORIZONTAL,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "CopyslFbox0a35230cb45374d",
                "top": "0dp",
                "width": "100%",
                "zIndex": 1
            }, {}, {
                "hoverSkin": "sknContextualMenuEntryHover"
            });
            flxDeactivate.setDefaultUnit(kony.flex.DP);
            var fonticonDeactive = new kony.ui.Label({
                "centerY": "50%",
                "id": "fonticonDeactive",
                "isVisible": true,
                "left": "15px",
                "skin": "sknFontIconOptionMenuRow",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmFAQ.fonticonDeactive\")",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var imgOption2 = new kony.ui.Image2({
                "centerY": "50%",
                "height": "15dp",
                "id": "imgOption2",
                "isVisible": false,
                "left": "15dp",
                "skin": "slImage",
                "src": "deactive_2x.png",
                "width": "15dp",
                "zIndex": 1
            }, {
                "imageScaleMode": constants.IMAGE_SCALE_MODE_MAINTAIN_ASPECT_RATIO,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var lblOption2 = new kony.ui.Label({
                "centerY": "50%",
                "id": "lblOption2",
                "isVisible": true,
                "left": "10dp",
                "skin": "sknlblLatoBold35475f14px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.permission.Deactivate\")",
                "top": "4dp",
                "width": "70px",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxDeactivate.add(fonticonDeactive, imgOption2, lblOption2);
            var flxDelete = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "bottom": "10dp",
                "clipBounds": true,
                "height": "35dp",
                "id": "flxDelete",
                "isVisible": true,
                "layoutType": kony.flex.FLOW_HORIZONTAL,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "CopyslFbox0a6ed527bebbb44",
                "top": "0dp",
                "width": "100%",
                "zIndex": 1
            }, {}, {
                "hoverSkin": "sknContextualMenuEntryHover"
            });
            flxDelete.setDefaultUnit(kony.flex.DP);
            var fontIconOption3 = new kony.ui.Label({
                "centerY": "50%",
                "id": "fontIconOption3",
                "isVisible": true,
                "left": "15dp",
                "skin": "sknFontIconOptionMenuRow",
                "text": "",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var lblDeleteOption = new kony.ui.Label({
                "centerY": "50%",
                "id": "lblDeleteOption",
                "isVisible": true,
                "left": "10dp",
                "skin": "sknlblLatoBold35475f14px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.SecurityQuestions.Delete\")",
                "top": "4dp",
                "width": "60px",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxDelete.add(fontIconOption3, lblDeleteOption);
            flxOptionsMenuContainer.add(flxEdit, flxDeactivate, flxDelete);
            var flxDownArrowImage = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "12px",
                "id": "flxDownArrowImage",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0px",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "-1px",
                "width": "100%",
                "zIndex": 2
            }, {}, {});
            flxDownArrowImage.setDefaultUnit(kony.flex.DP);
            var imgDownArrow = new kony.ui.Image2({
                "height": "12dp",
                "id": "imgDownArrow",
                "isVisible": true,
                "right": "20dp",
                "skin": "slImage",
                "src": "downarrow_2x.png",
                "top": "0dp",
                "width": "15dp",
                "zIndex": 1
            }, {
                "imageScaleMode": constants.IMAGE_SCALE_MODE_FIT_TO_DIMENSIONS,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxDownArrowImage.add(imgDownArrow);
            flxSelectOptions.add(flxUpArrowImage, flxOptionsMenuContainer, flxDownArrowImage);
            flxMFAConfigContainer.add(lblSubHeaderSegSeperator, flxHeaderPermissions, flxSegmentMFAScenarios, flxSelectOptions);
            var flxNorecordsFound = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "bottom": "0dp",
                "clipBounds": true,
                "height": "90%",
                "id": "flxNorecordsFound",
                "isVisible": false,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "isModalContainer": false,
                "right": "35dp",
                "skin": "sknflxffffffop100Bordercbcdd1Radius3px",
                "top": "125dp",
                "width": "100%",
                "zIndex": 1
            }, {}, {});
            flxNorecordsFound.setDefaultUnit(kony.flex.DP);
            var rtxNoResultsFound = new kony.ui.RichText({
                "centerX": "50%",
                "centerY": "50%",
                "id": "rtxNoResultsFound",
                "isVisible": true,
                "skin": "sknRtxLato84939e12Px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmMFAConfigurations.NoResultsFound\")",
                "width": "100%",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_CENTER,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxNorecordsFound.add(rtxNoResultsFound);
            var statusFilterMenu = new com.adminConsole.common.statusFilterMenu({
                "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                "id": "statusFilterMenu",
                "isVisible": false,
                "layoutType": kony.flex.FREE_FORM,
                "masterType": constants.MASTER_TYPE_DEFAULT,
                "isModalContainer": false,
                "right": "450dp",
                "skin": "slFbox",
                "top": "160dp",
                "width": "140dp",
                "zIndex": 15,
                "overrides": {
                    "imgUpArrow": {
                        "left": "viz.val_cleared",
                        "right": "15dp",
                        "src": "uparrow_2x.png"
                    },
                    "statusFilterMenu": {
                        "isVisible": false,
                        "left": "viz.val_cleared",
                        "right": "450dp",
                        "top": "160dp",
                        "width": "140dp",
                        "zIndex": 15
                    }
                }
            }, {
                "overrides": {}
            }, {
                "overrides": {}
            });
            var flxProductStatusFilter = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                "clipBounds": true,
                "id": "flxProductStatusFilter",
                "isVisible": false,
                "layoutType": kony.flex.FREE_FORM,
                "isModalContainer": false,
                "right": "200dp",
                "skin": "slFbox",
                "top": "155dp",
                "width": "100px",
                "zIndex": 50
            }, {}, {});
            flxProductStatusFilter.setDefaultUnit(kony.flex.DP);
            var statusFilterMenu2 = new com.adminConsole.common.statusFilterMenu({
                "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                "id": "statusFilterMenu2",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "masterType": constants.MASTER_TYPE_DEFAULT,
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "5dp",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 5,
                "overrides": {
                    "imgUpArrow": {
                        "left": "viz.val_cleared",
                        "right": "15dp",
                        "src": "uparrow_2x.png"
                    },
                    "statusFilterMenu": {
                        "width": kony.flex.USE_PREFFERED_SIZE
                    }
                }
            }, {
                "overrides": {}
            }, {
                "overrides": {}
            });
            flxProductStatusFilter.add(statusFilterMenu2);
            var flxApplicationsFilter = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                "clipBounds": true,
                "id": "flxApplicationsFilter",
                "isVisible": false,
                "layoutType": kony.flex.FREE_FORM,
                "isModalContainer": false,
                "right": "250dp",
                "skin": "slFbox",
                "top": "155dp",
                "width": "225px",
                "zIndex": 50
            }, {}, {});
            flxApplicationsFilter.setDefaultUnit(kony.flex.DP);
            var applicationsFilterMenu = new com.adminConsole.common.statusFilterMenu({
                "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                "id": "applicationsFilterMenu",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "masterType": constants.MASTER_TYPE_DEFAULT,
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "5dp",
                "width": "100%",
                "zIndex": 5,
                "overrides": {
                    "imgUpArrow": {
                        "centerX": "50%",
                        "left": "viz.val_cleared",
                        "right": "viz.val_cleared",
                        "src": "uparrow_2x.png"
                    },
                    "statusFilterMenu": {
                        "width": "100%"
                    }
                }
            }, {
                "overrides": {}
            }, {
                "overrides": {}
            });
            flxApplicationsFilter.add(applicationsFilterMenu);
            flxMFAScenarios.add(flxSubHeadingContainer, flxMainSubHeader, flxMFAConfigContainer, flxNorecordsFound, statusFilterMenu, flxProductStatusFilter, flxApplicationsFilter);
            var flxEditMFAScenario = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "bottom": "0dp",
                "clipBounds": true,
                "id": "flxEditMFAScenario",
                "isVisible": false,
                "layoutType": kony.flex.FREE_FORM,
                "left": "35px",
                "isModalContainer": false,
                "right": "35px",
                "skin": "sknflxffffffop100Bordercbcdd1Radius3px",
                "top": "25dp",
                "zIndex": 2
            }, {}, {});
            flxEditMFAScenario.setDefaultUnit(kony.flex.DP);
            var flxEditContent = new kony.ui.FlexScrollContainer({
                "allowHorizontalBounce": false,
                "allowVerticalBounce": true,
                "bottom": "80px",
                "bounces": true,
                "clipBounds": true,
                "enableScrolling": true,
                "horizontalScrollIndicator": true,
                "id": "flxEditContent",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "pagingEnabled": false,
                "scrollDirection": kony.flex.SCROLL_VERTICAL,
                "skin": "slFSbox",
                "top": "0dp",
                "verticalScrollIndicator": true,
                "width": "100%",
                "zIndex": 3
            }, {}, {});
            flxEditContent.setDefaultUnit(kony.flex.DP);
            var flxEditContentMain = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                "clipBounds": true,
                "id": "flxEditContentMain",
                "isVisible": true,
                "layoutType": kony.flex.FLOW_VERTICAL,
                "left": "20px",
                "isModalContainer": false,
                "right": "40px",
                "skin": "slFbox",
                "top": "0dp",
                "zIndex": 1
            }, {}, {});
            flxEditContentMain.setDefaultUnit(kony.flex.DP);
            var flxRowHeader = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                "centerX": "50%",
                "clipBounds": true,
                "id": "flxRowHeader",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0px",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0dp",
                "width": "100%",
                "zIndex": 1
            }, {}, {});
            flxRowHeader.setDefaultUnit(kony.flex.DP);
            var lblMFAScenarioName = new kony.ui.Label({
                "bottom": "20px",
                "id": "lblMFAScenarioName",
                "isVisible": true,
                "left": "0px",
                "skin": "sknLbl192b45LatoReg16px",
                "text": "Retail Banking | Transactional - P2P Transfer",
                "top": "20px",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var lblMFAConfigStatus = new kony.ui.Label({
                "bottom": "20px",
                "id": "lblMFAConfigStatus",
                "isVisible": true,
                "right": "49px",
                "skin": "sknllbl485c75Lato13px",
                "text": "Active ",
                "top": "20px",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var switchMFAConfigStatus = new kony.ui.Switch({
                "bottom": "20px",
                "height": "18px",
                "id": "switchMFAConfigStatus",
                "isVisible": true,
                "leftSideText": "ON",
                "right": "12px",
                "rightSideText": "OFF",
                "selectedIndex": 0,
                "skin": "sknSwitchServiceManagementmfa",
                "top": "20px",
                "width": "27px",
                "zIndex": 1
            }, {
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxRowHeader.add(lblMFAScenarioName, lblMFAConfigStatus, switchMFAConfigStatus);
            var flxApplicationContainer = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "80dp",
                "id": "flxApplicationContainer",
                "isVisible": true,
                "layoutType": kony.flex.FLOW_VERTICAL,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0dp",
                "width": "48%",
                "zIndex": 1
            }, {}, {});
            flxApplicationContainer.setDefaultUnit(kony.flex.DP);
            var lblApplicationHeader = new kony.ui.Label({
                "id": "lblApplicationHeader",
                "isVisible": true,
                "left": "0dp",
                "skin": "sknlblLato485c7514px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmmfascenarios.Application\")",
                "top": "0dp",
                "width": "100%",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var flxApplicationsList = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "40dp",
                "id": "flxApplicationsList",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "5dp",
                "width": "100%"
            }, {}, {});
            flxApplicationsList.setDefaultUnit(kony.flex.DP);
            var lstBoxSelectApplication = new kony.ui.ListBox({
                "focusSkin": "defListBoxFocus",
                "height": "40dp",
                "id": "lstBoxSelectApplication",
                "isVisible": true,
                "left": "0dp",
                "masterData": [
                    ["lb1", "Placeholder One"],
                    ["lb2", "Placeholder Two"],
                    ["lb3", "Placeholder Three"]
                ],
                "right": "2dp",
                "skin": "sknlbxBgffffffBorderc1c9ceRadius3Px",
                "top": "0dp",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [2, 0, 0, 0],
                "paddingInPixel": false
            }, {
                "multiSelect": false
            });
            var flxDisableApplication = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "40dp",
                "id": "flxDisableApplication",
                "isVisible": false,
                "layoutType": kony.flex.FREE_FORM,
                "left": 0,
                "isModalContainer": false,
                "right": "2dp",
                "skin": "skinDisabledf3f3f3bordrad3px",
                "top": "0dp",
                "zIndex": 1
            }, {}, {
                "hoverSkin": "sknCursorDisabled"
            });
            flxDisableApplication.setDefaultUnit(kony.flex.DP);
            flxDisableApplication.add();
            flxApplicationsList.add(lstBoxSelectApplication, flxDisableApplication);
            var flxApplicationError = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                "clipBounds": true,
                "id": "flxApplicationError",
                "isVisible": false,
                "layoutType": kony.flex.FLOW_HORIZONTAL,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": 5,
                "width": "100%",
                "zIndex": 1
            }, {}, {});
            flxApplicationError.setDefaultUnit(kony.flex.DP);
            var lblErrorIconApp = new kony.ui.Label({
                "id": "lblErrorIconApp",
                "isVisible": true,
                "left": "0dp",
                "skin": "sknErrorIcon",
                "text": "",
                "top": "0dp",
                "width": "15dp",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var lblErrorMsgApp = new kony.ui.Label({
                "id": "lblErrorMsgApp",
                "isVisible": true,
                "left": "0dp",
                "skin": "sknlblError",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmMFAScenarios.select_app\")",
                "top": "0dp",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxApplicationError.add(lblErrorIconApp, lblErrorMsgApp);
            flxApplicationContainer.add(lblApplicationHeader, flxApplicationsList, flxApplicationError);
            var flxScenarioContainer = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                "bottom": "1dp",
                "clipBounds": true,
                "id": "flxScenarioContainer",
                "isVisible": true,
                "layoutType": kony.flex.FLOW_VERTICAL,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "sknflxffffffop100Bordercbcdd1Radius3px",
                "top": "10dp",
                "width": "100%",
                "zIndex": 1
            }, {}, {});
            flxScenarioContainer.setDefaultUnit(kony.flex.DP);
            var flxRow1 = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "centerX": "50%",
                "clipBounds": true,
                "height": "90dp",
                "id": "flxRow1",
                "isVisible": true,
                "layoutType": kony.flex.FLOW_HORIZONTAL,
                "left": "35px",
                "isModalContainer": false,
                "right": "35px",
                "skin": "slFbox",
                "top": "25dp",
                "width": "100%",
                "zIndex": 1
            }, {}, {});
            flxRow1.setDefaultUnit(kony.flex.DP);
            var flxColumn11 = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "80dp",
                "id": "flxColumn11",
                "isVisible": true,
                "layoutType": kony.flex.FLOW_VERTICAL,
                "left": "20dp",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0dp",
                "width": "45%",
                "zIndex": 1
            }, {}, {});
            flxColumn11.setDefaultUnit(kony.flex.DP);
            var lblHeader11 = new kony.ui.Label({
                "id": "lblHeader11",
                "isVisible": true,
                "left": "0dp",
                "skin": "sknlblLato485c7514px",
                "text": "Feature",
                "top": "0dp",
                "width": "100%",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var flxFeaturesListbox = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "40dp",
                "id": "flxFeaturesListbox",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "5dp",
                "width": "100%"
            }, {}, {});
            flxFeaturesListbox.setDefaultUnit(kony.flex.DP);
            var lstBoxSelectFeature = new kony.ui.ListBox({
                "focusSkin": "defListBoxFocus",
                "height": "40dp",
                "id": "lstBoxSelectFeature",
                "isVisible": true,
                "left": "0dp",
                "masterData": [
                    ["lb1", "Placeholder One"],
                    ["lb2", "Placeholder Two"],
                    ["lb3", "Placeholder Three"]
                ],
                "right": "2dp",
                "skin": "sknlbxBgffffffBorderc1c9ceRadius3Px",
                "top": "0dp",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [2, 0, 0, 0],
                "paddingInPixel": false
            }, {
                "multiSelect": false
            });
            var flxDisableFeature = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "40dp",
                "id": "flxDisableFeature",
                "isVisible": false,
                "layoutType": kony.flex.FREE_FORM,
                "left": 0,
                "isModalContainer": false,
                "right": "2dp",
                "skin": "skinDisabledf3f3f3bordrad3px",
                "top": "0dp",
                "zIndex": 1
            }, {}, {
                "hoverSkin": "sknCursorDisabled"
            });
            flxDisableFeature.setDefaultUnit(kony.flex.DP);
            flxDisableFeature.add();
            flxFeaturesListbox.add(lstBoxSelectFeature, flxDisableFeature);
            var flxErrorHeader = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                "clipBounds": true,
                "id": "flxErrorHeader",
                "isVisible": false,
                "layoutType": kony.flex.FLOW_HORIZONTAL,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": 5,
                "width": "100%",
                "zIndex": 1
            }, {}, {});
            flxErrorHeader.setDefaultUnit(kony.flex.DP);
            var lblErrorIconFeature = new kony.ui.Label({
                "id": "lblErrorIconFeature",
                "isVisible": true,
                "left": "0dp",
                "skin": "sknErrorIcon",
                "text": "",
                "top": "0dp",
                "width": "15dp",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var lblErrorMsgHeader = new kony.ui.Label({
                "id": "lblErrorMsgHeader",
                "isVisible": true,
                "left": "0dp",
                "skin": "sknlblError",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmMFAScenarios.select_feature\")",
                "top": "0dp",
                "width": "170dp",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxErrorHeader.add(lblErrorIconFeature, lblErrorMsgHeader);
            flxColumn11.add(lblHeader11, flxFeaturesListbox, flxErrorHeader);
            var flxColumn12 = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "100%",
                "id": "flxColumn12",
                "isVisible": true,
                "layoutType": kony.flex.FLOW_VERTICAL,
                "left": "15dp",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0dp",
                "width": "45%"
            }, {}, {});
            flxColumn12.setDefaultUnit(kony.flex.DP);
            var lblHeader12 = new kony.ui.Label({
                "id": "lblHeader12",
                "isVisible": true,
                "left": "0dp",
                "skin": "sknlblLato485c7514px",
                "text": "Scenario Type",
                "top": "0dp",
                "width": "100%",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var flxScenarioTypeSelection = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "40dp",
                "id": "flxScenarioTypeSelection",
                "isVisible": true,
                "layoutType": kony.flex.FLOW_HORIZONTAL,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "5dp",
                "width": "100%",
                "zIndex": 1
            }, {}, {});
            flxScenarioTypeSelection.setDefaultUnit(kony.flex.DP);
            var flxMonetaryContainer = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "100%",
                "id": "flxMonetaryContainer",
                "isVisible": true,
                "layoutType": kony.flex.FLOW_HORIZONTAL,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "sknflxffffffBorderE1E5EERadius3px",
                "top": "0dp",
                "width": "40%"
            }, {}, {
                "hoverSkin": "sknCursor"
            });
            flxMonetaryContainer.setDefaultUnit(kony.flex.DP);
            var flxTransactional = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "centerY": "50%",
                "clipBounds": true,
                "height": "40dp",
                "id": "flxTransactional",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "isModalContainer": false,
                "right": "0dp",
                "skin": "sknFlx006CCA0pxRad",
                "top": "0dp",
                "width": "100%",
                "zIndex": 1
            }, {}, {});
            flxTransactional.setDefaultUnit(kony.flex.DP);
            var flxCircleTransaction = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "centerY": "50%",
                "clipBounds": true,
                "height": "30dp",
                "id": "flxCircleTransaction",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "15px",
                "isModalContainer": false,
                "right": "35px",
                "skin": "sknCircleBlue",
                "top": "0dp",
                "width": "30dp",
                "zIndex": 1
            }, {}, {});
            flxCircleTransaction.setDefaultUnit(kony.flex.DP);
            var imgTick1 = new kony.ui.Image2({
                "centerX": "50%",
                "centerY": "50%",
                "height": "10dp",
                "id": "imgTick1",
                "isVisible": true,
                "left": "11dp",
                "skin": "slImage",
                "src": "check.png",
                "top": "11dp",
                "width": "15dp",
                "zIndex": 1
            }, {
                "imageScaleMode": constants.IMAGE_SCALE_MODE_FIT_TO_DIMENSIONS,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxCircleTransaction.add(imgTick1);
            var lblTransactional = new kony.ui.Label({
                "centerY": "50%",
                "id": "lblTransactional",
                "isVisible": true,
                "left": "55dp",
                "skin": "lblfffffflatoregular14px",
                "text": "Monetary",
                "top": "0dp",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {
                "hoverSkin": "sknLbl485c7513pxHoverCursorNew"
            });
            flxTransactional.add(flxCircleTransaction, lblTransactional);
            flxMonetaryContainer.add(flxTransactional);
            var flxNonMonetaryContainer = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "100%",
                "id": "flxNonMonetaryContainer",
                "isVisible": true,
                "layoutType": kony.flex.FLOW_HORIZONTAL,
                "left": "15dp",
                "isModalContainer": false,
                "skin": "sknflxffffffBorderE1E5EERadius3px",
                "top": "0dp",
                "width": "50%"
            }, {}, {
                "hoverSkin": "sknCursor"
            });
            flxNonMonetaryContainer.setDefaultUnit(kony.flex.DP);
            var flxNonTransactional = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "centerY": "50%",
                "clipBounds": true,
                "height": "40dp",
                "id": "flxNonTransactional",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "isModalContainer": false,
                "right": "0dp",
                "skin": "slFbox",
                "top": "0dp",
                "width": "100%",
                "zIndex": 1
            }, {}, {});
            flxNonTransactional.setDefaultUnit(kony.flex.DP);
            var flxCircleNonTransaction = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "centerY": "50%",
                "clipBounds": true,
                "height": "30dp",
                "id": "flxCircleNonTransaction",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "15px",
                "isModalContainer": false,
                "right": "35px",
                "skin": "sknCircleGrey",
                "top": "0dp",
                "width": "30dp",
                "zIndex": 1
            }, {}, {});
            flxCircleNonTransaction.setDefaultUnit(kony.flex.DP);
            var CopyimgTick0f4b71e6416eb4a = new kony.ui.Image2({
                "centerX": "50%",
                "centerY": "50%",
                "height": "10dp",
                "id": "CopyimgTick0f4b71e6416eb4a",
                "isVisible": true,
                "left": "11dp",
                "skin": "slImage",
                "src": "check.png",
                "top": "11dp",
                "width": "15dp",
                "zIndex": 1
            }, {
                "imageScaleMode": constants.IMAGE_SCALE_MODE_FIT_TO_DIMENSIONS,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxCircleNonTransaction.add(CopyimgTick0f4b71e6416eb4a);
            var lblNonTransactional = new kony.ui.Label({
                "centerY": "50%",
                "id": "lblNonTransactional",
                "isVisible": true,
                "left": "55dp",
                "skin": "sknlblLatoReg485c7513px",
                "text": "Non -Monetary",
                "top": "0dp",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {
                "hoverSkin": "sknlbl485c7513pxHoverCursor"
            });
            flxNonTransactional.add(flxCircleNonTransaction, lblNonTransactional);
            flxNonMonetaryContainer.add(flxNonTransactional);
            flxScenarioTypeSelection.add(flxMonetaryContainer, flxNonMonetaryContainer);
            var flxErrorScenarioType = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                "clipBounds": true,
                "id": "flxErrorScenarioType",
                "isVisible": false,
                "layoutType": kony.flex.FLOW_HORIZONTAL,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": 5,
                "width": "100%",
                "zIndex": 1
            }, {}, {});
            flxErrorScenarioType.setDefaultUnit(kony.flex.DP);
            var lblErrorIconType = new kony.ui.Label({
                "id": "lblErrorIconType",
                "isVisible": true,
                "left": "0dp",
                "skin": "sknErrorIcon",
                "text": "",
                "top": "0dp",
                "width": "15dp",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var lblErrorMsgType = new kony.ui.Label({
                "id": "lblErrorMsgType",
                "isVisible": true,
                "left": "0dp",
                "skin": "sknlblError",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmAlertsManagement.Please_select_type\")",
                "top": "0dp",
                "width": "170dp",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxErrorScenarioType.add(lblErrorIconType, lblErrorMsgType);
            flxColumn12.add(lblHeader12, flxScenarioTypeSelection, flxErrorScenarioType);
            flxRow1.add(flxColumn11, flxColumn12);
            var flxTitleSeperator = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "centerX": "50%",
                "clipBounds": true,
                "height": "1dp",
                "id": "flxTitleSeperator",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "35px",
                "isModalContainer": false,
                "right": "35px",
                "skin": "sknflxd6dbe7",
                "top": "0dp",
                "width": "100%",
                "zIndex": 1
            }, {}, {});
            flxTitleSeperator.setDefaultUnit(kony.flex.DP);
            flxTitleSeperator.add();
            var flxRow2 = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                "centerX": "50%",
                "clipBounds": true,
                "id": "flxRow2",
                "isVisible": true,
                "layoutType": kony.flex.FLOW_HORIZONTAL,
                "left": "0px",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0dp",
                "width": "100%",
                "zIndex": 1
            }, {}, {});
            flxRow2.setDefaultUnit(kony.flex.DP);
            var flxColumn21 = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                "clipBounds": true,
                "id": "flxColumn21",
                "isVisible": true,
                "layoutType": kony.flex.FLOW_VERTICAL,
                "left": "0dp",
                "isModalContainer": false,
                "right": "0dp",
                "skin": "slFbox",
                "top": "0dp",
                "width": "100%",
                "zIndex": 1
            }, {}, {});
            flxColumn21.setDefaultUnit(kony.flex.DP);
            var lblHeader21 = new kony.ui.Label({
                "id": "lblHeader21",
                "isVisible": false,
                "left": "0dp",
                "skin": "sknlblLato485c7514px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmMFAScenarios.Scenario_Type\")",
                "top": "0dp",
                "width": "100%",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var flxScenarioType = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "130dp",
                "id": "flxScenarioType",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "sknf8f9faNoBorder",
                "top": "0dp",
                "width": "100%",
                "zIndex": 1
            }, {}, {});
            flxScenarioType.setDefaultUnit(kony.flex.DP);
            var flxVerticalSeperator = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "centerY": "50%",
                "clipBounds": true,
                "height": "100%",
                "id": "flxVerticalSeperator",
                "isVisible": false,
                "layoutType": kony.flex.FREE_FORM,
                "left": "25%",
                "isModalContainer": false,
                "skin": "sknflxd6dbe7",
                "top": "0dp",
                "width": "1px",
                "zIndex": 1
            }, {}, {});
            flxVerticalSeperator.setDefaultUnit(kony.flex.DP);
            flxVerticalSeperator.add();
            var flxActivityType = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "centerY": "50%",
                "clipBounds": true,
                "height": "70dp",
                "id": "flxActivityType",
                "isVisible": false,
                "layoutType": kony.flex.FREE_FORM,
                "left": "25%",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0dp",
                "width": "25%",
                "zIndex": 1
            }, {}, {});
            flxActivityType.setDefaultUnit(kony.flex.DP);
            var lblTitleActivityType = new kony.ui.Label({
                "id": "lblTitleActivityType",
                "isVisible": true,
                "left": "20px",
                "right": "20px",
                "skin": "sknlblLato485c7514px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmMFAScenarios.Activity_Type\")",
                "top": "5dp",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var lbxActivityType = new kony.ui.ListBox({
                "focusSkin": "defListBoxFocus",
                "height": "40dp",
                "id": "lbxActivityType",
                "isVisible": true,
                "left": "20px",
                "masterData": [
                    ["lb1", "Placeholder One"],
                    ["lb2", "Placeholder Two"],
                    ["lb3", "Placeholder Three"]
                ],
                "right": "20px",
                "skin": "sknlbxBgffffffBorderc1c9ceRadius3Px",
                "top": "25dp",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [3, 0, 0, 0],
                "paddingInPixel": false
            }, {
                "multiSelect": false
            });
            flxActivityType.add(lblTitleActivityType, lbxActivityType);
            var flxActionContainer = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "centerY": "50%",
                "clipBounds": true,
                "height": "70dp",
                "id": "flxActionContainer",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "20dp",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0dp",
                "width": "30%",
                "zIndex": 1
            }, {}, {});
            flxActionContainer.setDefaultUnit(kony.flex.DP);
            var lblTitleAction = new kony.ui.Label({
                "id": "lblTitleAction",
                "isVisible": true,
                "left": "0px",
                "right": "10px",
                "skin": "sknlblLato485c7514px",
                "text": "Action",
                "top": "5dp",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var lbxActionType = new kony.ui.ListBox({
                "focusSkin": "defListBoxFocus",
                "height": "40dp",
                "id": "lbxActionType",
                "isVisible": true,
                "left": "0px",
                "masterData": [
                    ["lb1", "Placeholder One"],
                    ["lb2", "Placeholder Two"],
                    ["lb3", "Placeholder Three"]
                ],
                "right": "2px",
                "skin": "sknlbxBgffffffBorderc1c9ceRadius3Px",
                "top": "25dp",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [3, 0, 0, 0],
                "paddingInPixel": false
            }, {
                "multiSelect": false
            });
            var flxDisableAction = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "40dp",
                "id": "flxDisableAction",
                "isVisible": false,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "isModalContainer": false,
                "right": "2dp",
                "skin": "skinDisabledf3f3f3bordrad3px",
                "top": "25dp",
                "zIndex": 1
            }, {}, {
                "hoverSkin": "sknCursorDisabled"
            });
            flxDisableAction.setDefaultUnit(kony.flex.DP);
            flxDisableAction.add();
            flxActionContainer.add(lblTitleAction, lbxActionType, flxDisableAction);
            var flxFrequency = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "centerY": "50%",
                "clipBounds": true,
                "height": "70dp",
                "id": "flxFrequency",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "35%",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0dp",
                "width": "30%",
                "zIndex": 1
            }, {}, {});
            flxFrequency.setDefaultUnit(kony.flex.DP);
            var lblTitleFrequency = new kony.ui.Label({
                "id": "lblTitleFrequency",
                "isVisible": true,
                "left": "0px",
                "right": "10px",
                "skin": "sknlblLato485c7514px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmMFAScenarios.Frequency\")",
                "top": "5dp",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var lbxFrequency = new kony.ui.ListBox({
                "focusSkin": "defListBoxFocus",
                "height": "40dp",
                "id": "lbxFrequency",
                "isVisible": true,
                "left": "0px",
                "masterData": [
                    ["lb1", "Placeholder One"],
                    ["lb2", "Placeholder Two"],
                    ["lb3", "Placeholder Three"]
                ],
                "right": "2px",
                "skin": "sknlbxBgffffffBorderc1c9ceRadius3Px",
                "top": "25dp",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [3, 0, 0, 0],
                "paddingInPixel": false
            }, {
                "multiSelect": false
            });
            flxFrequency.add(lblTitleFrequency, lbxFrequency);
            var flxValue = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "centerY": "50%",
                "clipBounds": true,
                "height": "70dp",
                "id": "flxValue",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "67%",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0dp",
                "width": "30%",
                "zIndex": 1
            }, {}, {});
            flxValue.setDefaultUnit(kony.flex.DP);
            var textBoxEntry = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                "clipBounds": true,
                "id": "textBoxEntry",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "10px",
                "isModalContainer": false,
                "right": "0px",
                "skin": "slFbox",
                "top": "0dp",
                "zIndex": 1
            }, {}, {});
            textBoxEntry.setDefaultUnit(kony.flex.DP);
            var lblOptional = new kony.ui.Label({
                "id": "lblOptional",
                "isVisible": false,
                "left": "65dp",
                "skin": "slLabel0d4f692dab05249",
                "text": "Label",
                "top": "0dp",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var lblValueTitle = new kony.ui.Label({
                "id": "lblValueTitle",
                "isVisible": true,
                "left": "0dp",
                "skin": "sknlblLato485c7514px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmMFAScenarios.Value_Above\")",
                "top": "0dp",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var lblCount = new kony.ui.Label({
                "id": "lblCount",
                "isVisible": false,
                "right": "0dp",
                "skin": "slLabel0d20174dce8ea42",
                "text": "0/10",
                "top": "0dp",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var flxInlineError = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                "clipBounds": true,
                "id": "flxInlineError",
                "isVisible": false,
                "layoutType": kony.flex.FLOW_HORIZONTAL,
                "left": "10dp",
                "isModalContainer": false,
                "right": "0dp",
                "skin": "slFbox",
                "top": "70dp",
                "width": "95%"
            }, {}, {});
            flxInlineError.setDefaultUnit(kony.flex.DP);
            var lblErrorIcon = new kony.ui.Label({
                "id": "lblErrorIcon",
                "isVisible": true,
                "left": "0dp",
                "skin": "sknFontIconError",
                "text": "",
                "top": "2dp",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var lblErrorText = new kony.ui.Label({
                "id": "lblErrorText",
                "isVisible": true,
                "left": "10dp",
                "skin": "sknlblError",
                "text": "Error",
                "top": "0dp",
                "width": "85%",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxInlineError.add(lblErrorIcon, lblErrorText);
            var flxBtnCheck = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "40dp",
                "id": "flxBtnCheck",
                "isVisible": true,
                "layoutType": kony.flex.FLOW_HORIZONTAL,
                "left": "0dp",
                "isModalContainer": false,
                "right": "0dp",
                "skin": "slFbox",
                "top": "25dp"
            }, {}, {});
            flxBtnCheck.setDefaultUnit(kony.flex.DP);
            var btnCheck = new kony.ui.Button({
                "height": "40dp",
                "id": "btnCheck",
                "isVisible": true,
                "left": "0dp",
                "skin": "sknBtnCurrencyIcon485c7516px",
                "text": "$",
                "top": "0dp",
                "width": "22%",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_CENTER,
                "displayText": true,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var flxSepartor = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "40dp",
                "id": "flxSepartor",
                "isVisible": false,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0%",
                "isModalContainer": false,
                "right": "0dp",
                "skin": "sknSeparatorCsr",
                "top": "0dp",
                "width": "1dp",
                "zIndex": 100
            }, {}, {});
            flxSepartor.setDefaultUnit(kony.flex.DP);
            flxSepartor.add();
            var tbxEnterValue = new kony.ui.TextBox2({
                "autoCapitalize": constants.TEXTBOX_AUTO_CAPITALIZE_NONE,
                "height": "40dp",
                "id": "tbxEnterValue",
                "isVisible": true,
                "keyBoardStyle": constants.TEXTBOX_KEY_BOARD_STYLE_DEFAULT,
                "left": "-2dp",
                "maxTextLength": 12,
                "placeholder": "$1 - $ 9999 9999 9999",
                "secureTextEntry": false,
                "skin": "skntbxLato35475f14px",
                "textInputMode": constants.TEXTBOX_INPUT_MODE_NUMERIC,
                "top": "0dp",
                "width": "76%",
                "zIndex": 1
            }, {
                "containerHeightMode": constants.TEXTBOX_FONT_METRICS_DRIVEN_HEIGHT,
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [5, 0, 0, 0],
                "paddingInPixel": false
            }, {
                "autoCorrect": false,
                "placeholderSkin": "defTextBoxPlaceholder"
            });
            flxBtnCheck.add(btnCheck, flxSepartor, tbxEnterValue);
            textBoxEntry.add(lblOptional, lblValueTitle, lblCount, flxInlineError, flxBtnCheck);
            flxValue.add(textBoxEntry);
            var flxErrorAction = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                "bottom": "10dp",
                "clipBounds": true,
                "id": "flxErrorAction",
                "isVisible": false,
                "layoutType": kony.flex.FLOW_HORIZONTAL,
                "left": "20dp",
                "isModalContainer": false,
                "skin": "slFbox",
                "width": "25%",
                "zIndex": 1
            }, {}, {});
            flxErrorAction.setDefaultUnit(kony.flex.DP);
            var lblErrorIconAction = new kony.ui.Label({
                "id": "lblErrorIconAction",
                "isVisible": true,
                "left": "0",
                "skin": "sknErrorIcon",
                "text": "",
                "top": "0dp",
                "width": "15dp",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var lblErrorMsgHeaderAction = new kony.ui.Label({
                "id": "lblErrorMsgHeaderAction",
                "isVisible": true,
                "left": "0dp",
                "skin": "sknlblError",
                "text": "Please select Action",
                "top": "0dp",
                "width": "170dp",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxErrorAction.add(lblErrorIconAction, lblErrorMsgHeaderAction);
            var flxErrorFrequency = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                "bottom": "10dp",
                "clipBounds": true,
                "id": "flxErrorFrequency",
                "isVisible": false,
                "layoutType": kony.flex.FLOW_HORIZONTAL,
                "left": "35%",
                "isModalContainer": false,
                "skin": "slFbox",
                "width": "25%",
                "zIndex": 1
            }, {}, {});
            flxErrorFrequency.setDefaultUnit(kony.flex.DP);
            var lblErrorIconFrequency = new kony.ui.Label({
                "id": "lblErrorIconFrequency",
                "isVisible": true,
                "left": "0dp",
                "skin": "sknErrorIcon",
                "text": "",
                "top": "0dp",
                "width": "15dp",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var lblErrorMsgFrequency = new kony.ui.Label({
                "id": "lblErrorMsgFrequency",
                "isVisible": true,
                "left": "0dp",
                "skin": "sknlblError",
                "text": "Please select Frequency",
                "top": "0dp",
                "width": "170dp",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxErrorFrequency.add(lblErrorIconFrequency, lblErrorMsgFrequency);
            var flxErrorFrequencyValue = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                "bottom": "10dp",
                "clipBounds": true,
                "id": "flxErrorFrequencyValue",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "67%",
                "isModalContainer": false,
                "skin": "slFbox",
                "width": "30%",
                "zIndex": 1
            }, {}, {});
            flxErrorFrequencyValue.setDefaultUnit(kony.flex.DP);
            var flxErrorContent = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                "clipBounds": true,
                "id": "flxErrorContent",
                "isVisible": true,
                "layoutType": kony.flex.FLOW_HORIZONTAL,
                "left": "10dp",
                "isModalContainer": false,
                "right": "0dp",
                "skin": "slFbox",
                "top": "0"
            }, {}, {});
            flxErrorContent.setDefaultUnit(kony.flex.DP);
            var lblErrorIconFrequencyValue = new kony.ui.Label({
                "id": "lblErrorIconFrequencyValue",
                "isVisible": true,
                "left": "22%",
                "skin": "sknErrorIcon",
                "text": "",
                "top": "0dp",
                "width": "15dp",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var lblErrorMsgFrequencyValue = new kony.ui.Label({
                "id": "lblErrorMsgFrequencyValue",
                "isVisible": true,
                "left": "0dp",
                "skin": "sknlblError",
                "text": "Value cannot be empty",
                "top": "0dp",
                "width": "170dp",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxErrorContent.add(lblErrorIconFrequencyValue, lblErrorMsgFrequencyValue);
            flxErrorFrequencyValue.add(flxErrorContent);
            flxScenarioType.add(flxVerticalSeperator, flxActivityType, flxActionContainer, flxFrequency, flxValue, flxErrorAction, flxErrorFrequency, flxErrorFrequencyValue);
            var flxSelectScenarioType = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "130dp",
                "id": "flxSelectScenarioType",
                "isVisible": false,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "sknf8f9faNoBorder",
                "top": "0dp",
                "width": "100%"
            }, {}, {});
            flxSelectScenarioType.setDefaultUnit(kony.flex.DP);
            var lblSelectScenarioType = new kony.ui.Label({
                "bottom": "20dp",
                "centerX": "50%",
                "id": "lblSelectScenarioType",
                "isVisible": true,
                "skin": "sknlblLato485c7514px",
                "text": "Select appropriate feature and scenario type to display the corresponding action.",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var imgSelectScenario = new kony.ui.Image2({
                "centerX": "50%",
                "height": "70dp",
                "id": "imgSelectScenario",
                "isVisible": true,
                "left": "342dp",
                "skin": "slImage",
                "src": "selectscenario.png",
                "top": "20dp",
                "width": "90dp",
                "zIndex": 1
            }, {
                "imageScaleMode": constants.IMAGE_SCALE_MODE_MAINTAIN_ASPECT_RATIO,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxSelectScenarioType.add(lblSelectScenarioType, imgSelectScenario);
            flxColumn21.add(lblHeader21, flxScenarioType, flxSelectScenarioType);
            flxRow2.add(flxColumn21);
            flxScenarioContainer.add(flxRow1, flxTitleSeperator, flxRow2);
            var flxRow3 = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "120dp",
                "id": "flxRow3",
                "isVisible": true,
                "layoutType": kony.flex.FLOW_HORIZONTAL,
                "left": "0px",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "15dp",
                "width": "100%",
                "zIndex": 1
            }, {}, {});
            flxRow3.setDefaultUnit(kony.flex.DP);
            var flxDescription = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                "clipBounds": true,
                "id": "flxDescription",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0px",
                "isModalContainer": false,
                "right": "0px",
                "skin": "slFbox",
                "top": "0dp",
                "zIndex": 1
            }, {}, {});
            flxDescription.setDefaultUnit(kony.flex.DP);
            var lblScenarioDescription = new kony.ui.Label({
                "id": "lblScenarioDescription",
                "isVisible": true,
                "left": "0px",
                "skin": "sknlbl485c7514px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.permission.Description\")",
                "top": 0,
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var lblScenarioDescriptionSize = new kony.ui.Label({
                "id": "lblScenarioDescriptionSize",
                "isVisible": false,
                "right": "0px",
                "skin": "sknlbl485c7514px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmFAQ.lblMessageSize\")",
                "top": "0px",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var txtScenarioDescription = new kony.ui.TextArea2({
                "autoCapitalize": constants.TEXTAREA_AUTO_CAPITALIZE_NONE,
                "focusSkin": "skntxtAreaLato9ca9ba12Px",
                "height": "80dp",
                "id": "txtScenarioDescription",
                "isVisible": true,
                "keyBoardStyle": constants.TEXTAREA_KEY_BOARD_STYLE_DEFAULT,
                "left": "0dp",
                "maxTextLength": 300,
                "numberOfVisibleLines": 3,
                "placeholder": "MFA Name Or Purpose of Trigger",
                "right": "0dp",
                "skin": "skntxtAreaLato35475f14Px",
                "textInputMode": constants.TEXTAREA_INPUT_MODE_ANY,
                "top": "21dp",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_TOP_LEFT,
                "padding": [1, 1, 1, 1],
                "paddingInPixel": false
            }, {
                "autoCorrect": false
            });
            var flxNoDescriptionError = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "15px",
                "id": "flxNoDescriptionError",
                "isVisible": false,
                "layoutType": kony.flex.FLOW_HORIZONTAL,
                "left": "0dp",
                "isModalContainer": false,
                "right": "0dp",
                "skin": "slFbox",
                "top": "105dp",
                "zIndex": 1
            }, {}, {});
            flxNoDescriptionError.setDefaultUnit(kony.flex.DP);
            var lblNoDescErrorIcon = new kony.ui.Label({
                "height": "15dp",
                "id": "lblNoDescErrorIcon",
                "isVisible": true,
                "left": "0dp",
                "skin": "sknErrorIcon",
                "text": "",
                "top": "0dp",
                "width": "15dp",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var lblNoDescriptionError = new kony.ui.Label({
                "id": "lblNoDescriptionError",
                "isVisible": true,
                "left": "0dp",
                "skin": "sknlblError",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmMFAScenarios.scenario_desc_error\")",
                "top": "0dp",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxNoDescriptionError.add(lblNoDescErrorIcon, lblNoDescriptionError);
            var lblDescOptional = new kony.ui.Label({
                "id": "lblDescOptional",
                "isVisible": false,
                "left": "75px",
                "skin": "slLabel0d4f692dab05249",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmPermissions.lblOptional\")",
                "top": "0px",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxDescription.add(lblScenarioDescription, lblScenarioDescriptionSize, txtScenarioDescription, flxNoDescriptionError, lblDescOptional);
            var flxColumn31 = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                "clipBounds": true,
                "id": "flxColumn31",
                "isVisible": false,
                "layoutType": kony.flex.FLOW_VERTICAL,
                "left": "0dp",
                "isModalContainer": false,
                "right": "0dp",
                "skin": "slFbox",
                "top": "0px",
                "width": "100%",
                "zIndex": 1
            }, {}, {});
            flxColumn31.setDefaultUnit(kony.flex.DP);
            var lblHeader31 = new kony.ui.Label({
                "id": "lblHeader31",
                "isVisible": true,
                "left": "0dp",
                "skin": "sknlblLato485c7514px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmMFAScenarios.Description\")",
                "top": "0dp",
                "width": "100%",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var tbxDescription = new kony.ui.TextBox2({
                "autoCapitalize": constants.TEXTBOX_AUTO_CAPITALIZE_NONE,
                "focusSkin": "skntbxLatof13pxFocus",
                "height": "40dp",
                "id": "tbxDescription",
                "isVisible": true,
                "keyBoardStyle": constants.TEXTBOX_KEY_BOARD_STYLE_DEFAULT,
                "left": "0dp",
                "placeholder": "eg: MFA Name or purpose of trigger",
                "right": "0dp",
                "secureTextEntry": false,
                "skin": "sknTbxFFFFFFBorDEDEDE13pxKA",
                "textInputMode": constants.TEXTBOX_INPUT_MODE_ANY,
                "top": "5dp",
                "width": "100%",
                "zIndex": 1
            }, {
                "containerHeightMode": constants.TEXTBOX_FONT_METRICS_DRIVEN_HEIGHT,
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [1, 0, 0, 0],
                "paddingInPixel": false
            }, {
                "autoCorrect": false,
                "placeholderSkin": "defTextBoxPlaceholder"
            });
            var flxErrorDescription = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                "clipBounds": true,
                "id": "flxErrorDescription",
                "isVisible": false,
                "layoutType": kony.flex.FLOW_HORIZONTAL,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "5dp",
                "width": "100%",
                "zIndex": 1
            }, {}, {});
            flxErrorDescription.setDefaultUnit(kony.flex.DP);
            var lblErrorIconDescription = new kony.ui.Label({
                "id": "lblErrorIconDescription",
                "isVisible": true,
                "left": "0dp",
                "skin": "sknErrorIcon",
                "text": "",
                "top": "0dp",
                "width": "15dp",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var lblErrorMsgDescription = new kony.ui.Label({
                "id": "lblErrorMsgDescription",
                "isVisible": true,
                "left": "0dp",
                "skin": "sknlblError",
                "text": "Description cannot be empty",
                "top": "0dp",
                "width": "170dp",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxErrorDescription.add(lblErrorIconDescription, lblErrorMsgDescription);
            flxColumn31.add(lblHeader31, tbxDescription, flxErrorDescription);
            flxRow3.add(flxDescription, flxColumn31);
            var lblAuthFailureTitle = new kony.ui.Label({
                "centerX": "50%",
                "id": "lblAuthFailureTitle",
                "isVisible": true,
                "left": "35px",
                "skin": "sknlblLatoBold485c7512px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmMFAScenarios.MFA_CHALLENGE_TYPE\")",
                "top": "15dp",
                "width": "100%",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var flxRow4 = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "80dp",
                "id": "flxRow4",
                "isVisible": true,
                "layoutType": kony.flex.FLOW_HORIZONTAL,
                "left": "0px",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "10dp",
                "width": "100%",
                "zIndex": 1
            }, {}, {});
            flxRow4.setDefaultUnit(kony.flex.DP);
            var flxColumn41 = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "80dp",
                "id": "flxColumn41",
                "isVisible": true,
                "layoutType": kony.flex.FLOW_VERTICAL,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0dp",
                "width": "49%",
                "zIndex": 1
            }, {}, {});
            flxColumn41.setDefaultUnit(kony.flex.DP);
            var lblHeader41 = new kony.ui.Label({
                "id": "lblHeader41",
                "isVisible": true,
                "left": "0dp",
                "skin": "sknlblLato485c7514px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmMFAScenarios.Primary\")",
                "top": "0dp",
                "width": "100%",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var lbxChallengePrimary = new kony.ui.ListBox({
                "focusSkin": "defListBoxFocus",
                "height": "40dp",
                "id": "lbxChallengePrimary",
                "isVisible": true,
                "left": "0dp",
                "masterData": [
                    ["lb1", "Placeholder One"],
                    ["lb2", "Placeholder Two"],
                    ["lb3", "Placeholder Three"]
                ],
                "skin": "sknlbxBgffffffBorderc1c9ceRadius3Px",
                "top": "5dp",
                "width": "100%",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [3, 0, 0, 0],
                "paddingInPixel": false
            }, {
                "multiSelect": false
            });
            var flxErrorPrimary = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                "clipBounds": true,
                "id": "flxErrorPrimary",
                "isVisible": false,
                "layoutType": kony.flex.FLOW_HORIZONTAL,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "5dp",
                "width": "100%",
                "zIndex": 1
            }, {}, {});
            flxErrorPrimary.setDefaultUnit(kony.flex.DP);
            var lblErrorIconPrimary = new kony.ui.Label({
                "id": "lblErrorIconPrimary",
                "isVisible": true,
                "left": "0dp",
                "skin": "sknErrorIcon",
                "text": "",
                "top": "0dp",
                "width": "15dp",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var lblErrorMsgPrimary = new kony.ui.Label({
                "id": "lblErrorMsgPrimary",
                "isVisible": true,
                "left": "0dp",
                "skin": "sknlblError",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmMFAScenarios.Select_MFA_Type\")",
                "top": "0dp",
                "width": "170dp",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxErrorPrimary.add(lblErrorIconPrimary, lblErrorMsgPrimary);
            flxColumn41.add(lblHeader41, lbxChallengePrimary, flxErrorPrimary);
            var flxColumn42 = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "80dp",
                "id": "flxColumn42",
                "isVisible": true,
                "layoutType": kony.flex.FLOW_VERTICAL,
                "left": "15dp",
                "isModalContainer": false,
                "right": "0dp",
                "skin": "slFbox",
                "top": "0dp",
                "width": "49%",
                "zIndex": 1
            }, {}, {});
            flxColumn42.setDefaultUnit(kony.flex.DP);
            var lblHeader42 = new kony.ui.Label({
                "id": "lblHeader42",
                "isVisible": true,
                "left": "0dp",
                "skin": "sknlblLato485c7514px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmMFAScenarios.Backup\")",
                "top": "0dp",
                "width": "100%",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var lbxChallengeBackup = new kony.ui.ListBox({
                "focusSkin": "defListBoxFocus",
                "height": "40dp",
                "id": "lbxChallengeBackup",
                "isVisible": true,
                "left": "0dp",
                "masterData": [
                    ["lb1", "Placeholder One"],
                    ["lb2", "Placeholder Two"],
                    ["lb3", "Placeholder Three"]
                ],
                "skin": "sknlbxBgffffffBorderc1c9ceRadius3Px",
                "top": "5dp",
                "width": "100%",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [3, 0, 0, 0],
                "paddingInPixel": false
            }, {
                "multiSelect": false
            });
            var flxErrorBackup = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                "clipBounds": true,
                "id": "flxErrorBackup",
                "isVisible": false,
                "layoutType": kony.flex.FLOW_HORIZONTAL,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "5dp",
                "width": "100%",
                "zIndex": 1
            }, {}, {});
            flxErrorBackup.setDefaultUnit(kony.flex.DP);
            var lblErrorIconBackup = new kony.ui.Label({
                "id": "lblErrorIconBackup",
                "isVisible": true,
                "left": "0dp",
                "skin": "sknErrorIcon",
                "text": "",
                "top": "0dp",
                "width": "15dp",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var lblErrorMsgBackup = new kony.ui.Label({
                "id": "lblErrorMsgBackup",
                "isVisible": true,
                "left": "0dp",
                "skin": "sknlblError",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmMFAScenarios.Select_MFA_Type\")",
                "top": "0dp",
                "width": "170dp",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxErrorBackup.add(lblErrorIconBackup, lblErrorMsgBackup);
            flxColumn42.add(lblHeader42, lbxChallengeBackup, flxErrorBackup);
            flxRow4.add(flxColumn41, flxColumn42);
            var lblMessageContentTemplateTitle = new kony.ui.Label({
                "centerX": "50%",
                "id": "lblMessageContentTemplateTitle",
                "isVisible": true,
                "left": "35px",
                "skin": "sknlblLatoBold485c7512px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmMFAScenarios.MESSAGE_CONTENT_TEMPLATE\")",
                "top": "15dp",
                "width": "100%",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var lblMessageContentTemplateNote = new kony.ui.Label({
                "centerX": "50%",
                "id": "lblMessageContentTemplateNote",
                "isVisible": true,
                "left": "35px",
                "skin": "sknlbllatoRegularB2BDCB13px",
                "text": "This option will appear only after selecting the secure access code.",
                "top": "10dp",
                "width": "100%",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var flxRow5 = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "centerX": "50%",
                "clipBounds": true,
                "height": "60dp",
                "id": "flxRow5",
                "isVisible": false,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "20dp",
                "width": "100%",
                "zIndex": 1
            }, {}, {});
            flxRow5.setDefaultUnit(kony.flex.DP);
            var radioMessageContentTemplate = new kony.ui.RadioButtonGroup({
                "id": "radioMessageContentTemplate",
                "isVisible": true,
                "left": "0dp",
                "masterData": [
                    ["SMS", "kony.i18n.getLocalizedString(\"i18n.frmMFAScenarios.SMS_only\")"],
                    ["EMAIL", "kony.i18n.getLocalizedString(\"i18n.frmMFAScenarios.Email_only\")"],
                    ["SMS_AND_EMAIL", "kony.i18n.getLocalizedString(\"i18n.frmMFAScenarios.Both_SMS_and_Email\")"]
                ],
                "skin": "sknRBGLatoRegular13px485c5KA",
                "top": "10px",
                "width": "100%",
                "zIndex": 1
            }, {
                "itemOrientation": constants.RADIOGROUP_ITEM_ORIENTATION_HORIZONTAL,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var flxErrorMessageTemplate = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                "bottom": "5dp",
                "clipBounds": true,
                "id": "flxErrorMessageTemplate",
                "isVisible": false,
                "layoutType": kony.flex.FLOW_HORIZONTAL,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "slFbox",
                "width": "100%",
                "zIndex": 1
            }, {}, {});
            flxErrorMessageTemplate.setDefaultUnit(kony.flex.DP);
            var lblErrorIconTemplate = new kony.ui.Label({
                "id": "lblErrorIconTemplate",
                "isVisible": true,
                "left": "0dp",
                "skin": "sknErrorIcon",
                "text": "",
                "top": "0dp",
                "width": "15dp",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var lblErrorMsgTemplate = new kony.ui.Label({
                "id": "lblErrorMsgTemplate",
                "isVisible": true,
                "left": "0dp",
                "skin": "sknlblError",
                "text": "Please select template",
                "top": "0dp",
                "width": "170dp",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxErrorMessageTemplate.add(lblErrorIconTemplate, lblErrorMsgTemplate);
            flxRow5.add(radioMessageContentTemplate, flxErrorMessageTemplate);
            var flxRow6 = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                "centerX": "50%",
                "clipBounds": true,
                "id": "flxRow6",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0dp",
                "width": "100%",
                "zIndex": 1
            }, {}, {});
            flxRow6.setDefaultUnit(kony.flex.DP);
            var flxColumn61 = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "bottom": "15dp",
                "clipBounds": true,
                "height": "145dp",
                "id": "flxColumn61",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0px",
                "width": "100%",
                "zIndex": 1
            }, {}, {});
            flxColumn61.setDefaultUnit(kony.flex.DP);
            var flxHeaderSMS = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                "clipBounds": false,
                "id": "flxHeaderSMS",
                "isVisible": true,
                "layoutType": kony.flex.FLOW_HORIZONTAL,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0dp",
                "width": "100%",
                "zIndex": 5
            }, {}, {});
            flxHeaderSMS.setDefaultUnit(kony.flex.DP);
            var lblHeader61 = new kony.ui.Label({
                "id": "lblHeader61",
                "isVisible": true,
                "left": "0dp",
                "skin": "sknlblLato485c7514px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmMFAScenarios.SMS_Content\")",
                "top": "8dp",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var btnSMSVariableReferences = new kony.ui.Button({
                "focusSkin": "contextuallinks",
                "id": "btnSMSVariableReferences",
                "isVisible": true,
                "left": "15dp",
                "skin": "sknBtnLatoReg006CCA13px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmMFAScenarios.Variable_Reference\")",
                "top": "8dp",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_CENTER,
                "displayText": true,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxHeaderSMS.add(lblHeader61, btnSMSVariableReferences);
            var txtSMSContent = new kony.ui.TextArea2({
                "autoCapitalize": constants.TEXTAREA_AUTO_CAPITALIZE_NONE,
                "focusSkin": "defTextAreaFocus",
                "height": "80dp",
                "id": "txtSMSContent",
                "isVisible": true,
                "keyBoardStyle": constants.TEXTAREA_KEY_BOARD_STYLE_DEFAULT,
                "left": "0dp",
                "numberOfVisibleLines": 3,
                "i18n_placeholder": "kony.i18n.getLocalizedString(\"i18n.frmMFAScenarios.Content_to_be_sent_via_SMS\")",
                "skin": "sknTextAreaDescription",
                "textInputMode": constants.TEXTAREA_INPUT_MODE_ANY,
                "top": "33dp",
                "width": "100%",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_TOP_LEFT,
                "padding": [1, 1, 1, 1],
                "paddingInPixel": false
            }, {
                "autoCorrect": false,
                "placeholderSkin": "defTextAreaPlaceholder"
            });
            var flxErrorSMSContent = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                "bottom": "5dp",
                "clipBounds": true,
                "id": "flxErrorSMSContent",
                "isVisible": false,
                "layoutType": kony.flex.FLOW_HORIZONTAL,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "slFbox",
                "width": "100%",
                "zIndex": 1
            }, {}, {});
            flxErrorSMSContent.setDefaultUnit(kony.flex.DP);
            var lblErrorIconSMSContent = new kony.ui.Label({
                "id": "lblErrorIconSMSContent",
                "isVisible": true,
                "left": "0dp",
                "skin": "sknErrorIcon",
                "text": "",
                "top": "0dp",
                "width": "15dp",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var lblErrorMsgSMSContent = new kony.ui.Label({
                "id": "lblErrorMsgSMSContent",
                "isVisible": true,
                "left": "0dp",
                "skin": "sknlblError",
                "text": "SMS content cannot be empty",
                "top": "0dp",
                "width": "170dp",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxErrorSMSContent.add(lblErrorIconSMSContent, lblErrorMsgSMSContent);
            flxColumn61.add(flxHeaderSMS, txtSMSContent, flxErrorSMSContent);
            var smsVariableReferencesMenu = new com.adminConsole.common.variableReferencesMenu({
                "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                "id": "smsVariableReferencesMenu",
                "isVisible": false,
                "layoutType": kony.flex.FREE_FORM,
                "left": "200dp",
                "masterType": constants.MASTER_TYPE_DEFAULT,
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0dp",
                "width": "250dp",
                "zIndex": 5,
                "overrides": {
                    "flxArrowImage": {
                        "width": "14dp"
                    },
                    "flxChechboxOuter": {
                        "left": "13dp"
                    },
                    "imgUpArrow": {
                        "src": "leftarrow_2x.png"
                    },
                    "lblHeading": {
                        "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmAlertsManagement.lblVariableReferenceHeader\")"
                    },
                    "segOptionsDropdown": {
                        "top": "2dp"
                    },
                    "variableReferencesMenu": {
                        "isVisible": false,
                        "left": "200dp",
                        "top": "0dp",
                        "width": "250dp"
                    }
                }
            }, {
                "overrides": {}
            }, {
                "overrides": {}
            });
            flxRow6.add(flxColumn61, smsVariableReferencesMenu);
            var flxRow7 = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                "centerX": "50%",
                "clipBounds": true,
                "id": "flxRow7",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "-20dp",
                "width": "100%",
                "zIndex": 1
            }, {}, {});
            flxRow7.setDefaultUnit(kony.flex.DP);
            var flxColumn71 = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "400dp",
                "id": "flxColumn71",
                "isVisible": true,
                "layoutType": kony.flex.FLOW_VERTICAL,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0px",
                "width": "100%",
                "zIndex": 1
            }, {}, {});
            flxColumn71.setDefaultUnit(kony.flex.DP);
            var flxHeaderEmail = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                "clipBounds": false,
                "id": "flxHeaderEmail",
                "isVisible": true,
                "layoutType": kony.flex.FLOW_HORIZONTAL,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0dp",
                "width": "100%",
                "zIndex": 5
            }, {}, {});
            flxHeaderEmail.setDefaultUnit(kony.flex.DP);
            var lblHeader71 = new kony.ui.Label({
                "id": "lblHeader71",
                "isVisible": true,
                "left": "0dp",
                "skin": "sknlblLato485c7514px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmMFAScenarios.Email_Content\")",
                "top": "8dp",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var btnEmailVariableReferences = new kony.ui.Button({
                "focusSkin": "contextuallinks",
                "id": "btnEmailVariableReferences",
                "isVisible": true,
                "left": "15dp",
                "skin": "sknBtnLatoReg006CCA13px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmMFAScenarios.Variable_Reference\")",
                "top": "8dp",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_CENTER,
                "displayText": true,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxHeaderEmail.add(lblHeader71, btnEmailVariableReferences);
            var flxSubject = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "40dp",
                "id": "flxSubject",
                "isVisible": true,
                "layoutType": kony.flex.FLOW_HORIZONTAL,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "sknflxffffffop100Bordercbcdd1Radius3px",
                "top": "10dp",
                "width": "100%",
                "zIndex": 1
            }, {}, {});
            flxSubject.setDefaultUnit(kony.flex.DP);
            var lblSubject = new kony.ui.Label({
                "centerY": "50%",
                "id": "lblSubject",
                "isVisible": true,
                "left": "0dp",
                "skin": "lbl484B52LatoBold12Px",
                "text": "Subject : ",
                "top": "0dp",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [2, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var txtSubject = new kony.ui.TextBox2({
                "autoCapitalize": constants.TEXTBOX_AUTO_CAPITALIZE_NONE,
                "height": "40dp",
                "id": "txtSubject",
                "isVisible": true,
                "keyBoardStyle": constants.TEXTBOX_KEY_BOARD_STYLE_DEFAULT,
                "left": "0dp",
                "placeholder": "Start Typing Subject",
                "right": "0dp",
                "secureTextEntry": false,
                "skin": "sknMessagetxtbox",
                "textInputMode": constants.TEXTBOX_INPUT_MODE_ANY,
                "top": "0dp",
                "width": "91%",
                "zIndex": 1
            }, {
                "containerHeightMode": constants.TEXTBOX_FONT_METRICS_DRIVEN_HEIGHT,
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [2, 0, 0, 0],
                "paddingInPixel": false
            }, {
                "autoCorrect": false
            });
            flxSubject.add(lblSubject, txtSubject);
            var flxErrorEmailSubject = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                "clipBounds": true,
                "id": "flxErrorEmailSubject",
                "isVisible": false,
                "layoutType": kony.flex.FLOW_HORIZONTAL,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "5dp",
                "width": "100%",
                "zIndex": 1
            }, {}, {});
            flxErrorEmailSubject.setDefaultUnit(kony.flex.DP);
            var lblErrorIconEmailSubject = new kony.ui.Label({
                "id": "lblErrorIconEmailSubject",
                "isVisible": true,
                "left": "0dp",
                "skin": "sknErrorIcon",
                "text": "",
                "top": "0dp",
                "width": "15dp",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var lblErrorMsgEmailSubject = new kony.ui.Label({
                "id": "lblErrorMsgEmailSubject",
                "isVisible": true,
                "left": "0dp",
                "skin": "sknlblError",
                "text": "Subject cannot be empty",
                "top": "0dp",
                "width": "170dp",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxErrorEmailSubject.add(lblErrorIconEmailSubject, lblErrorMsgEmailSubject);
            var flxMailContent = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "250dp",
                "id": "flxMailContent",
                "isVisible": true,
                "layoutType": kony.flex.FLOW_HORIZONTAL,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "sknflxffffffop100Bordercbcdd1Radius3px",
                "top": "10dp",
                "width": "100%",
                "zIndex": 1
            }, {}, {});
            flxMailContent.setDefaultUnit(kony.flex.DP);
            var rtxMessage = new kony.ui.Browser({
                "detectTelNumber": true,
                "enableZoom": false,
                "height": "100%",
                "id": "rtxMessage",
                "isVisible": true,
                "left": "0dp",
                "setAsContent": false,
                "requestURLConfig": {
                    "URL": "richtext.html",
                    "requestMethod": constants.BROWSER_REQUEST_METHOD_GET
                },
                "top": "0dp",
                "width": "100%",
                "zIndex": 1
            }, {}, {});
            flxMailContent.add(rtxMessage);
            var flxErrorEmailContent = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                "clipBounds": true,
                "id": "flxErrorEmailContent",
                "isVisible": false,
                "layoutType": kony.flex.FLOW_HORIZONTAL,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "5dp",
                "width": "100%",
                "zIndex": 1
            }, {}, {});
            flxErrorEmailContent.setDefaultUnit(kony.flex.DP);
            var lblErrorIconEmailBody = new kony.ui.Label({
                "id": "lblErrorIconEmailBody",
                "isVisible": true,
                "left": "0dp",
                "skin": "sknErrorIcon",
                "text": "",
                "top": "0dp",
                "width": "15dp",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var lblErrorMsgEmailBody = new kony.ui.Label({
                "id": "lblErrorMsgEmailBody",
                "isVisible": true,
                "left": "0dp",
                "skin": "sknlblError",
                "text": "Email content cannot be empty",
                "top": "0dp",
                "width": "170dp",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxErrorEmailContent.add(lblErrorIconEmailBody, lblErrorMsgEmailBody);
            flxColumn71.add(flxHeaderEmail, flxSubject, flxErrorEmailSubject, flxMailContent, flxErrorEmailContent);
            var emailVariableReferencesMenu = new com.adminConsole.common.variableReferencesMenu({
                "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                "id": "emailVariableReferencesMenu",
                "isVisible": false,
                "layoutType": kony.flex.FREE_FORM,
                "left": "200dp",
                "masterType": constants.MASTER_TYPE_DEFAULT,
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0dp",
                "width": "250dp",
                "zIndex": 5,
                "overrides": {
                    "flxArrowImage": {
                        "width": "14dp"
                    },
                    "flxChechboxOuter": {
                        "left": "13dp"
                    },
                    "imgUpArrow": {
                        "src": "leftarrow_2x.png"
                    },
                    "lblHeading": {
                        "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmAlertsManagement.lblVariableReferenceHeader\")"
                    },
                    "segOptionsDropdown": {
                        "top": "2dp"
                    },
                    "variableReferencesMenu": {
                        "isVisible": false,
                        "left": "200dp",
                        "top": "0dp",
                        "width": "250dp"
                    }
                }
            }, {
                "overrides": {}
            }, {
                "overrides": {}
            });
            flxRow7.add(flxColumn71, emailVariableReferencesMenu);
            flxEditContentMain.add(flxRowHeader, flxApplicationContainer, flxScenarioContainer, flxRow3, lblAuthFailureTitle, flxRow4, lblMessageContentTemplateTitle, lblMessageContentTemplateNote, flxRow5, flxRow6, flxRow7);
            flxEditContent.add(flxEditContentMain);
            var flxEditFooterButtons = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "bottom": "0dp",
                "clipBounds": true,
                "height": "80px",
                "id": "flxEditFooterButtons",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "slFbox",
                "width": "100%",
                "zIndex": 1
            }, {}, {});
            flxEditFooterButtons.setDefaultUnit(kony.flex.DP);
            var commonButtonsEditMFAConfig = new com.adminConsole.common.commonButtons({
                "height": "80px",
                "id": "commonButtonsEditMFAConfig",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "masterType": constants.MASTER_TYPE_DEFAULT,
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0dp",
                "width": "100%",
                "overrides": {
                    "btnCancel": {
                        "left": "viz.val_cleared",
                        "right": "140px",
                        "zIndex": 100
                    },
                    "btnNext": {
                        "isVisible": false,
                        "left": "viz.val_cleared",
                        "right": "20dp"
                    },
                    "btnSave": {
                        "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmPermissionsController.CREATE\")",
                        "isVisible": true,
                        "right": "20px"
                    }
                }
            }, {
                "overrides": {}
            }, {
                "overrides": {}
            });
            var flxDetailsButtonsSeperator = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "1dp",
                "id": "flxDetailsButtonsSeperator",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "isModalContainer": false,
                "right": "0dp",
                "skin": "sknflxd6dbe7",
                "top": "0dp",
                "zIndex": 1
            }, {}, {});
            flxDetailsButtonsSeperator.setDefaultUnit(kony.flex.DP);
            flxDetailsButtonsSeperator.add();
            flxEditFooterButtons.add(commonButtonsEditMFAConfig, flxDetailsButtonsSeperator);
            flxEditMFAScenario.add(flxEditContent, flxEditFooterButtons);
            var flxViewMFAScenario = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "bottom": 0,
                "clipBounds": true,
                "id": "flxViewMFAScenario",
                "isVisible": false,
                "layoutType": kony.flex.FREE_FORM,
                "left": "35px",
                "isModalContainer": false,
                "right": "35px",
                "skin": "sknflxffffffop100Bordercbcdd1Radius3px",
                "top": "25dp",
                "zIndex": 2
            }, {}, {});
            flxViewMFAScenario.setDefaultUnit(kony.flex.DP);
            var flxViewContent = new kony.ui.FlexScrollContainer({
                "allowHorizontalBounce": false,
                "allowVerticalBounce": true,
                "bottom": "15px",
                "bounces": true,
                "clipBounds": true,
                "enableScrolling": true,
                "horizontalScrollIndicator": true,
                "id": "flxViewContent",
                "isVisible": true,
                "layoutType": kony.flex.FLOW_VERTICAL,
                "left": "0dp",
                "pagingEnabled": false,
                "right": "0dp",
                "scrollDirection": kony.flex.SCROLL_VERTICAL,
                "skin": "slFSbox",
                "top": "0dp",
                "verticalScrollIndicator": true,
                "zIndex": 3
            }, {}, {});
            flxViewContent.setDefaultUnit(kony.flex.DP);
            var flxHeaderView = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "60dp",
                "id": "flxHeaderView",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0px",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0dp",
                "width": "100%",
                "zIndex": 1
            }, {}, {});
            flxHeaderView.setDefaultUnit(kony.flex.DP);
            var lblMFAViewHeading = new kony.ui.Label({
                "bottom": "20px",
                "id": "lblMFAViewHeading",
                "isVisible": true,
                "left": "20px",
                "skin": "sknLbl192b45LatoReg16px",
                "text": "Retail Banking | Transactional - P2P Transfer",
                "top": "20px",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var flxRightOptions = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "100%",
                "id": "flxRightOptions",
                "isVisible": true,
                "layoutType": kony.flex.FLOW_HORIZONTAL,
                "reverseLayoutDirection": true,
                "isModalContainer": false,
                "right": "0px",
                "skin": "slFbox",
                "top": "0dp",
                "width": "20%"
            }, {}, {});
            flxRightOptions.setDefaultUnit(kony.flex.DP);
            var flxOptions = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "centerY": "50%",
                "clipBounds": true,
                "height": "25dp",
                "id": "flxOptions",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "isModalContainer": false,
                "onClick": controller.AS_FlexContainer_a4df6b3c553a41f9b22caa3c8542e575,
                "right": "20px",
                "skin": "slFbox",
                "top": "10dp",
                "width": "25px",
                "zIndex": 2
            }, {}, {
                "hoverSkin": "sknflxffffffop100Border424242Radius100px"
            });
            flxOptions.setDefaultUnit(kony.flex.DP);
            var lblIconOptions = new kony.ui.Label({
                "centerX": "50%",
                "centerY": "50%",
                "height": "15px",
                "id": "lblIconOptions",
                "isVisible": true,
                "left": "14dp",
                "skin": "sknFontIconOptionMenu",
                "text": "",
                "width": "15px",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxOptions.add(lblIconOptions);
            var flxStatus = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "centerY": "50%",
                "clipBounds": true,
                "height": "20px",
                "id": "flxStatus",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "isModalContainer": false,
                "right": "0px",
                "skin": "slFbox",
                "width": "70px",
                "zIndex": 1
            }, {}, {});
            flxStatus.setDefaultUnit(kony.flex.DP);
            var imgServiceStatus = new kony.ui.Label({
                "centerY": "50%",
                "id": "imgServiceStatus",
                "isVisible": false,
                "left": "0dp",
                "skin": "sknFontIconActivate",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.userwidgetmodel.lblFontIconStatus1\")",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var lblServiceStatus = new kony.ui.Label({
                "centerY": "50%",
                "id": "lblServiceStatus",
                "isVisible": true,
                "left": "16dp",
                "right": "20dp",
                "skin": "sknlblLato5bc06cBold14px",
                "text": "Active",
                "width": "50px",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var fontIconStatusImg = new kony.ui.Label({
                "centerY": "50%",
                "id": "fontIconStatusImg",
                "isVisible": true,
                "left": "0dp",
                "skin": "sknFontIconActivate",
                "text": "",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxStatus.add(imgServiceStatus, lblServiceStatus, fontIconStatusImg);
            flxRightOptions.add(flxOptions, flxStatus);
            flxHeaderView.add(lblMFAViewHeading, flxRightOptions);
            var flxTitleSepratr = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "1dp",
                "id": "flxTitleSepratr",
                "isVisible": false,
                "layoutType": kony.flex.FREE_FORM,
                "left": "20px",
                "isModalContainer": false,
                "right": "20px",
                "skin": "sknflxd6dbe7",
                "top": "0dp",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {}, {});
            flxTitleSepratr.setDefaultUnit(kony.flex.DP);
            flxTitleSepratr.add();
            var flxViewRow1 = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                "clipBounds": true,
                "id": "flxViewRow1",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0px",
                "width": "100%",
                "zIndex": 1
            }, {}, {});
            flxViewRow1.setDefaultUnit(kony.flex.DP);
            var details = new com.adminConsole.view.details({
                "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                "id": "details",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "masterType": constants.MASTER_TYPE_DEFAULT,
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0dp",
                "width": "100%",
                "overrides": {
                    "btnLink3": {
                        "centerY": "viz.val_cleared"
                    },
                    "details": {
                        "autogrowMode": kony.flex.AUTOGROW_HEIGHT
                    },
                    "flxColumn1": {
                        "left": "20px"
                    },
                    "flxColumn2": {
                        "left": "34%",
                        "top": "0dp",
                        "width": "30%"
                    },
                    "flxColumn3": {
                        "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                        "left": "66%",
                        "right": "20px"
                    },
                    "flxDataAndImage3": {
                        "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                        "top": "25px"
                    },
                    "imgData1": {
                        "src": "active_circle2x.png"
                    },
                    "imgData2": {
                        "src": "active_circle2x.png"
                    },
                    "imgData3": {
                        "centerY": "viz.val_cleared",
                        "src": "active_circle2x.png"
                    },
                    "lblData3": {
                        "centerY": "viz.val_cleared",
                        "text": "Data for details",
                        "width": "100%"
                    },
                    "lblHeading1": {
                        "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmmfascenarios.lblApplication\")"
                    },
                    "lblHeading2": {
                        "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmMfascenarios.Feature_CAP\")"
                    },
                    "lblHeading3": {
                        "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmMFAScenarios.Scenario_Type_Caps\")"
                    }
                }
            }, {
                "overrides": {}
            }, {
                "overrides": {}
            });
            flxViewRow1.add(details);
            var flxViewRow2 = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                "clipBounds": true,
                "id": "flxViewRow2",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "30px",
                "width": "100%",
                "zIndex": 1
            }, {}, {});
            flxViewRow2.setDefaultUnit(kony.flex.DP);
            var details2 = new com.adminConsole.view.details({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "height": "57px",
                "id": "details2",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "masterType": constants.MASTER_TYPE_DEFAULT,
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0dp",
                "width": "100%",
                "overrides": {
                    "btnLink2": {
                        "centerY": "50%",
                        "top": "0dp"
                    },
                    "details": {
                        "height": "57px",
                        "left": "0dp",
                        "top": "0dp"
                    },
                    "flxColumn1": {
                        "height": "55px",
                        "left": "20px"
                    },
                    "flxColumn2": {
                        "height": "50px",
                        "left": "34%",
                        "width": "30%"
                    },
                    "flxColumn3": {
                        "height": "50px",
                        "isVisible": true,
                        "left": "66%",
                        "right": "20px"
                    },
                    "flxDataAndImage1": {
                        "bottom": "viz.val_cleared",
                        "height": "45px",
                        "top": "12px"
                    },
                    "flxDataAndImage2": {
                        "bottom": "viz.val_cleared",
                        "height": "30px",
                        "top": "12px"
                    },
                    "flxDataAndImage3": {
                        "bottom": "viz.val_cleared",
                        "height": "30px",
                        "top": "12px"
                    },
                    "imgData1": {
                        "src": "active_circle2x.png"
                    },
                    "imgData2": {
                        "centerY": "50%",
                        "src": "active_circle2x.png",
                        "top": "0dp"
                    },
                    "imgData3": {
                        "src": "active_circle2x.png"
                    },
                    "lblData1": {
                        "centerY": "viz.val_cleared",
                        "top": "10px"
                    },
                    "lblData2": {
                        "centerY": "viz.val_cleared",
                        "top": "10px"
                    },
                    "lblData3": {
                        "centerY": "viz.val_cleared",
                        "top": "10px"
                    },
                    "lblHeading1": {
                        "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmMfascenarios.ACTION_CAP\")"
                    },
                    "lblHeading2": {
                        "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmMFAScenarios.Frequency_Caps\")"
                    },
                    "lblHeading3": {
                        "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmMFAScenarios.Value_Above_CAP\")"
                    },
                    "lblIconData2": {
                        "centerY": "50%",
                        "top": "0px"
                    },
                    "lblSignData2": {
                        "centerY": "50%",
                        "top": "0px"
                    }
                }
            }, {
                "overrides": {}
            }, {
                "overrides": {}
            });
            flxViewRow2.add(details2);
            var flxViewRow3 = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                "clipBounds": true,
                "id": "flxViewRow3",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "30px",
                "width": "100%",
                "zIndex": 1
            }, {}, {});
            flxViewRow3.setDefaultUnit(kony.flex.DP);
            var flxDescContent = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                "clipBounds": true,
                "id": "flxDescContent",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "20px",
                "isModalContainer": false,
                "right": "20px",
                "skin": "slFbox",
                "top": "0dp",
                "zIndex": 1
            }, {}, {});
            flxDescContent.setDefaultUnit(kony.flex.DP);
            var lblDesc = new kony.ui.Label({
                "id": "lblDesc",
                "isVisible": true,
                "left": "0dp",
                "skin": "sknlblLato5d6c7f12px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.permission.DESCRIPTION\")",
                "top": "0dp",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var lblDescVal = new kony.ui.Label({
                "id": "lblDescVal",
                "isVisible": true,
                "left": "0dp",
                "skin": "sknlbl485C75LatoSemiBold13px",
                "text": "Lorem ipsum dolor sit amet, consectetur adipiscing elit.  cing elit.Lorem ipsum dolor sit amet, consectetur.Lorem ipsum dolor sit amet, consectetur adipiscing elit.  cing elit.Lorem ipsum dolor sit amet, consectetur.",
                "top": "22px",
                "width": "100%",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxDescContent.add(lblDesc, lblDescVal);
            flxViewRow3.add(flxDescContent);
            var lblMfaChallengeTypeSubHeader = new kony.ui.Label({
                "id": "lblMfaChallengeTypeSubHeader",
                "isVisible": true,
                "left": "20px",
                "skin": "sknLbl192B45LatoReg19Px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmMFAScenarios.MFA_CHALLENGE_TYPE\")",
                "top": "30dp",
                "width": "100%",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var flxViewRow4 = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                "clipBounds": true,
                "id": "flxViewRow4",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "30dp",
                "width": "100%",
                "zIndex": 1
            }, {}, {});
            flxViewRow4.setDefaultUnit(kony.flex.DP);
            var details3 = new com.adminConsole.view.details({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "height": "40px",
                "id": "details3",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "masterType": constants.MASTER_TYPE_DEFAULT,
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0dp",
                "width": "100%",
                "overrides": {
                    "details": {
                        "left": "0dp",
                        "top": "0dp"
                    },
                    "flxColumn1": {
                        "left": "20px"
                    },
                    "flxColumn2": {
                        "left": "34%",
                        "width": "30%"
                    },
                    "flxColumn3": {
                        "height": "40px",
                        "isVisible": false,
                        "left": "66%",
                        "right": "20px"
                    },
                    "imgData1": {
                        "src": "active_circle2x.png"
                    },
                    "imgData2": {
                        "src": "active_circle2x.png"
                    },
                    "imgData3": {
                        "src": "active_circle2x.png"
                    },
                    "lblHeading1": {
                        "text": "PRIMARY OPTION"
                    },
                    "lblHeading2": {
                        "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmMFAScenarios.BackupOption_Caps\")"
                    }
                }
            }, {
                "overrides": {}
            }, {
                "overrides": {}
            });
            flxViewRow4.add(details3);
            var lblMsgChallengeType = new kony.ui.Label({
                "id": "lblMsgChallengeType",
                "isVisible": true,
                "left": "20px",
                "skin": "sknLbl192B45LatoReg19Px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmMFAScenarios.MESSAGE_CONTENT_TEMPLATE\")",
                "top": "30dp",
                "width": "100%",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var lblMessageContentTemplateNoteView = new kony.ui.Label({
                "id": "lblMessageContentTemplateNoteView",
                "isVisible": true,
                "left": "20px",
                "skin": "sknlbllatoRegularB2BDCB13px",
                "text": "This option will appear only after selecting the secure access code.",
                "top": "10dp",
                "width": "90%",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var flxViewRow5 = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                "clipBounds": true,
                "id": "flxViewRow5",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0px",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "20dp",
                "width": "100%",
                "zIndex": 1
            }, {}, {});
            flxViewRow5.setDefaultUnit(kony.flex.DP);
            var lblSmsHeader = new kony.ui.Label({
                "id": "lblSmsHeader",
                "isVisible": true,
                "left": "20px",
                "right": "0dp",
                "skin": "sknlblLato5d6c7f12px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmMFAScenarios.SMS_Content_Caps\")",
                "top": "0dp",
                "width": "100px",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var flxSmsSubj = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                "clipBounds": true,
                "id": "flxSmsSubj",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "20px",
                "isModalContainer": false,
                "right": "20px",
                "skin": "sknFlxBorAndRadius4px",
                "top": "28px",
                "zIndex": 1
            }, {}, {});
            flxSmsSubj.setDefaultUnit(kony.flex.DP);
            var lblSmsContent = new kony.ui.Label({
                "bottom": "10px",
                "id": "lblSmsContent",
                "isVisible": true,
                "left": "10px",
                "right": "10px",
                "skin": "sknlblLatoBold35475f14px",
                "text": "Lorem ipsum dolor sit amet, ",
                "top": "10px",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxSmsSubj.add(lblSmsContent);
            flxViewRow5.add(lblSmsHeader, flxSmsSubj);
            var flxViewRow6 = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                "centerX": "50%",
                "clipBounds": true,
                "id": "flxViewRow6",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "30dp",
                "width": "100%",
                "zIndex": 1
            }, {}, {});
            flxViewRow6.setDefaultUnit(kony.flex.DP);
            var flxEmail = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                "clipBounds": true,
                "id": "flxEmail",
                "isVisible": true,
                "layoutType": kony.flex.FLOW_VERTICAL,
                "left": "20dp",
                "isModalContainer": false,
                "right": "20dp",
                "skin": "slFbox",
                "top": "0px",
                "zIndex": 1
            }, {}, {});
            flxEmail.setDefaultUnit(kony.flex.DP);
            var flxEmailHeader = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                "clipBounds": false,
                "id": "flxEmailHeader",
                "isVisible": true,
                "layoutType": kony.flex.FLOW_HORIZONTAL,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0dp",
                "width": "100%",
                "zIndex": 5
            }, {}, {});
            flxEmailHeader.setDefaultUnit(kony.flex.DP);
            var lblHeaderEmail = new kony.ui.Label({
                "id": "lblHeaderEmail",
                "isVisible": true,
                "left": "0dp",
                "skin": "sknlblLato5d6c7f12px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmMFAScenarios.Email_Content_Caps\")",
                "top": "8dp",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxEmailHeader.add(lblHeaderEmail);
            var flxEmailSubject = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                "clipBounds": true,
                "id": "flxEmailSubject",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "isModalContainer": false,
                "right": "0dp",
                "skin": "sknFlxBorAndRadius4px",
                "top": "15dp",
                "zIndex": 1
            }, {}, {});
            flxEmailSubject.setDefaultUnit(kony.flex.DP);
            var lblSubjectEmailContent = new kony.ui.Label({
                "bottom": "10px",
                "id": "lblSubjectEmailContent",
                "isVisible": true,
                "left": "10px",
                "skin": "sknlblLatoBold35475f14px",
                "text": "Lorem ipsum dolor sit amet, consectetur adipiscing elit.  cing elit.Lorem ipsum dolor sit amet, consectetur.Lorem ipsum dolor sit amet, consectetur adipiscing elit.  cing elit.Lorem ipsum dolor sit amet, consectetur.",
                "top": "10px",
                "width": "100%",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxEmailSubject.add(lblSubjectEmailContent);
            var flxEmailContent = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "136px",
                "id": "flxEmailContent",
                "isVisible": true,
                "layoutType": kony.flex.FLOW_HORIZONTAL,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "sknflxffffffop100Bordercbcdd1Radius3px",
                "top": "10dp",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {}, {});
            flxEmailContent.setDefaultUnit(kony.flex.DP);
            var rtxViewer = new kony.ui.Browser({
                "detectTelNumber": true,
                "enableZoom": false,
                "height": "100%",
                "id": "rtxViewer",
                "isVisible": true,
                "left": "0px",
                "setAsContent": false,
                "requestURLConfig": {
                    "URL": "richtextViewer.html",
                    "requestMethod": constants.BROWSER_REQUEST_METHOD_GET
                },
                "top": "0px",
                "width": "100%",
                "zIndex": 1
            }, {}, {});
            flxEmailContent.add(rtxViewer);
            flxEmail.add(flxEmailHeader, flxEmailSubject, flxEmailContent);
            flxViewRow6.add(flxEmail);
            flxViewContent.add(flxHeaderView, flxTitleSepratr, flxViewRow1, flxViewRow2, flxViewRow3, lblMfaChallengeTypeSubHeader, flxViewRow4, lblMsgChallengeType, lblMessageContentTemplateNoteView, flxViewRow5, flxViewRow6);
            var flxSelectOptionsView = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                "clipBounds": false,
                "id": "flxSelectOptionsView",
                "isVisible": true,
                "layoutType": kony.flex.FLOW_VERTICAL,
                "isModalContainer": false,
                "right": "12px",
                "skin": "slFbox",
                "top": "40px",
                "width": "130px",
                "zIndex": 100
            }, {}, {});
            flxSelectOptionsView.setDefaultUnit(kony.flex.DP);
            var flxUpArrowImageView = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "12px",
                "id": "flxUpArrowImageView",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0px",
                "width": "100%",
                "zIndex": 2
            }, {}, {});
            flxUpArrowImageView.setDefaultUnit(kony.flex.DP);
            var imgUpArrowView = new kony.ui.Image2({
                "height": "12dp",
                "id": "imgUpArrowView",
                "isVisible": true,
                "right": "13dp",
                "skin": "slImage",
                "src": "uparrow_2x.png",
                "top": "0dp",
                "width": "15dp",
                "zIndex": 1
            }, {
                "imageScaleMode": constants.IMAGE_SCALE_MODE_FIT_TO_DIMENSIONS,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxUpArrowImageView.add(imgUpArrowView);
            var flxSelectContainerView = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                "clipBounds": true,
                "id": "flxSelectContainerView",
                "isVisible": true,
                "layoutType": kony.flex.FLOW_VERTICAL,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "sknflxffffffop100dbdbe6Radius3px",
                "top": "-1dp",
                "width": "100%"
            }, {}, {});
            flxSelectContainerView.setDefaultUnit(kony.flex.DP);
            var flxEditView = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "35dp",
                "id": "flxEditView",
                "isVisible": true,
                "layoutType": kony.flex.FLOW_HORIZONTAL,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "sknFlxPointer",
                "top": "9dp",
                "width": "100%",
                "zIndex": 1
            }, {}, {
                "hoverSkin": "sknContextualMenuEntryHover"
            });
            flxEditView.setDefaultUnit(kony.flex.DP);
            var lblfonticonEdit = new kony.ui.Label({
                "centerY": "50%",
                "id": "lblfonticonEdit",
                "isVisible": true,
                "left": "15px",
                "skin": "sknFontIconOptionMenuRow",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmFAQ.fonticonEdit\")",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var lblEditOption = new kony.ui.Label({
                "centerY": "50%",
                "id": "lblEditOption",
                "isVisible": true,
                "left": "15dp",
                "skin": "sknlblLatoBold35475f14px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.roles.Edit\")",
                "top": "4dp",
                "width": "70px",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxEditView.add(lblfonticonEdit, lblEditOption);
            var flxDeactivateView = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "35dp",
                "id": "flxDeactivateView",
                "isVisible": true,
                "layoutType": kony.flex.FLOW_HORIZONTAL,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "sknFlxPointer",
                "top": "0dp",
                "width": "100%",
                "zIndex": 1
            }, {}, {
                "hoverSkin": "sknContextualMenuEntryHover"
            });
            flxDeactivateView.setDefaultUnit(kony.flex.DP);
            var fontIconActOrDeact = new kony.ui.Label({
                "centerY": "50%",
                "id": "fontIconActOrDeact",
                "isVisible": true,
                "left": "15px",
                "skin": "sknFontIconOptionMenuRow",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmFAQ.fonticonDeactive\")",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var lblActOrDeact = new kony.ui.Label({
                "centerY": "50%",
                "id": "lblActOrDeact",
                "isVisible": true,
                "left": "15dp",
                "skin": "sknlblLatoBold35475f14px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.permission.Deactivate\")",
                "top": "4dp",
                "width": "70px",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxDeactivateView.add(fontIconActOrDeact, lblActOrDeact);
            var flxDeleteView = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "bottom": "9dp",
                "clipBounds": true,
                "height": "35dp",
                "id": "flxDeleteView",
                "isVisible": true,
                "layoutType": kony.flex.FLOW_HORIZONTAL,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "sknFlxPointer",
                "top": "0dp",
                "width": "100%",
                "zIndex": 1
            }, {}, {
                "hoverSkin": "sknContextualMenuEntryHover"
            });
            flxDeleteView.setDefaultUnit(kony.flex.DP);
            var fontIconOptionDelete = new kony.ui.Label({
                "centerY": "50%",
                "id": "fontIconOptionDelete",
                "isVisible": true,
                "left": "15dp",
                "skin": "sknFontIconOptionMenuRow",
                "text": "",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var lblDeleteOptions = new kony.ui.Label({
                "centerY": "50%",
                "id": "lblDeleteOptions",
                "isVisible": true,
                "left": "10dp",
                "skin": "sknlblLatoBold35475f14px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.SecurityQuestions.Delete\")",
                "top": "4dp",
                "width": "60px",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxDeleteView.add(fontIconOptionDelete, lblDeleteOptions);
            flxSelectContainerView.add(flxEditView, flxDeactivateView, flxDeleteView);
            flxSelectOptionsView.add(flxUpArrowImageView, flxSelectContainerView);
            flxViewMFAScenario.add(flxViewContent, flxSelectOptionsView);
            flxScrollMainContent.add(flxMFANotification, flxMFAScenarios, flxEditMFAScenario, flxViewMFAScenario);
            var CreateScenarioBtnToolTip = new com.adminConsole.Customers.ToolTip({
                "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                "id": "CreateScenarioBtnToolTip",
                "isVisible": false,
                "layoutType": kony.flex.FREE_FORM,
                "masterType": constants.MASTER_TYPE_DEFAULT,
                "isModalContainer": false,
                "right": "75dp",
                "skin": "slFbox",
                "top": "93dp",
                "width": "181dp",
                "zIndex": 10,
                "overrides": {
                    "ToolTip": {
                        "isVisible": false,
                        "left": "viz.val_cleared",
                        "right": "75dp",
                        "top": "93dp",
                        "width": "181dp",
                        "zIndex": 10
                    },
                    "flxToolTipMessage": {
                        "centerX": "viz.val_cleared",
                        "height": "40px",
                        "top": "8px",
                        "width": kony.flex.USE_PREFFERED_SIZE
                    },
                    "lblDownArrow": {
                        "centerX": "viz.val_cleared",
                        "left": "10px",
                        "top": "55dp"
                    },
                    "lblNoConcentToolTip": {
                        "bottom": "5px",
                        "height": kony.flex.USE_PREFFERED_SIZE,
                        "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmMFAScenarios.AllFeaturesConfiguredForAllAppsMsg\")",
                        "left": "10px",
                        "right": "10px",
                        "top": "3px",
                        "width": "165px"
                    },
                    "lblarrow": {
                        "centerX": "viz.val_cleared",
                        "left": "viz.val_cleared",
                        "right": "10px"
                    }
                }
            }, {
                "overrides": {}
            }, {
                "overrides": {}
            });
            flxRightPanel.add(flxMainHeader, flxHeaderDropdown, flxBreadcrumb, flxScrollMainContent, CreateScenarioBtnToolTip);
            var flxLoading = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "100%",
                "id": "flxLoading",
                "isVisible": false,
                "layoutType": kony.flex.FREE_FORM,
                "left": "305px",
                "isModalContainer": false,
                "right": "0dp",
                "skin": "sknLoadingBlur",
                "top": "0dp",
                "width": "80%",
                "zIndex": 10
            }, {}, {});
            flxLoading.setDefaultUnit(kony.flex.DP);
            var flxImageContainer = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "centerX": "50%",
                "centerY": "50%",
                "clipBounds": true,
                "height": "75px",
                "id": "flxImageContainer",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "isModalContainer": false,
                "skin": "slFbox",
                "width": "75px",
                "zIndex": 1
            }, {}, {});
            flxImageContainer.setDefaultUnit(kony.flex.DP);
            var imgLoading = new kony.ui.Image2({
                "centerX": "50%",
                "centerY": "50%",
                "height": "50px",
                "id": "imgLoading",
                "isVisible": true,
                "skin": "slImage",
                "src": "loadingscreenimage.gif",
                "width": "50px",
                "zIndex": 1
            }, {
                "imageScaleMode": constants.IMAGE_SCALE_MODE_MAINTAIN_ASPECT_RATIO,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxImageContainer.add(imgLoading);
            flxLoading.add(flxImageContainer);
            var flxEditCancelConfirmation = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "100%",
                "id": "flxEditCancelConfirmation",
                "isVisible": false,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "masterType": constants.MASTER_TYPE_USERWIDGET,
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0dp",
                "width": "100%",
                "zIndex": 5
            }, {}, {});
            flxEditCancelConfirmation.setDefaultUnit(kony.flex.DP);
            var popUpCancelEdits = new com.adminConsole.common.popUpCancelEdits({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "centerX": "50%",
                "centerY": "50%",
                "height": "100%",
                "id": "popUpCancelEdits",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "masterType": constants.MASTER_TYPE_DEFAULT,
                "isModalContainer": false,
                "skin": "sknflxBg000000Op50",
                "top": "0dp",
                "width": "100%",
                "zIndex": 1,
                "overrides": {
                    "btnPopUpCancel": {
                        "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmMFAConfigurations.NO_LEAVE_AS_IS\")",
                        "minWidth": "81px",
                        "right": "123px",
                        "width": "81px"
                    },
                    "btnPopUpDelete": {
                        "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmMFAConfigurations.YES_CANCEL\")",
                        "minWidth": "83px",
                        "width": "83px"
                    },
                    "imgPopUpClose": {
                        "src": "close_blue.png"
                    },
                    "lblPopUpMainMessage": {
                        "text": "Cancel changes"
                    },
                    "popUpCancelEdits": {
                        "bottom": "viz.val_cleared",
                        "centerX": "50%",
                        "centerY": "50%",
                        "height": "100%",
                        "left": "0dp",
                        "maxHeight": "viz.val_cleared",
                        "maxWidth": "viz.val_cleared",
                        "minHeight": "viz.val_cleared",
                        "minWidth": "viz.val_cleared",
                        "right": "viz.val_cleared",
                        "top": "0dp",
                        "width": "100%"
                    },
                    "rtxPopUpDisclaimer": {
                        "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmMFAConfigurations.cancel_mfa_configs_message\")"
                    }
                }
            }, {
                "overrides": {}
            }, {
                "overrides": {}
            });
            flxEditCancelConfirmation.add(popUpCancelEdits);
            var flxToastMessage = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "bottom": "0px",
                "clipBounds": true,
                "height": "70px",
                "id": "flxToastMessage",
                "isVisible": false,
                "layoutType": kony.flex.FREE_FORM,
                "left": "305px",
                "isModalContainer": false,
                "right": 0,
                "skin": "slFbox",
                "zIndex": 1
            }, {}, {});
            flxToastMessage.setDefaultUnit(kony.flex.DP);
            var flxToastContainer = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "centerX": "50%",
                "centerY": "50%",
                "clipBounds": true,
                "height": "45px",
                "id": "flxToastContainer",
                "isVisible": false,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0%",
                "isModalContainer": false,
                "skin": "sknflxSuccessToast1F844D",
                "top": "0%",
                "width": "60%",
                "zIndex": 1
            }, {}, {});
            flxToastContainer.setDefaultUnit(kony.flex.DP);
            var imgLeft = new kony.ui.Image2({
                "centerY": "50%",
                "height": "15dp",
                "id": "imgLeft",
                "isVisible": true,
                "left": "15px",
                "skin": "slImage",
                "src": "arrow2x.png",
                "width": "15dp",
                "zIndex": 1
            }, {
                "imageScaleMode": constants.IMAGE_SCALE_MODE_MAINTAIN_ASPECT_RATIO,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var lbltoastMessage = new kony.ui.Label({
                "centerY": "50%",
                "id": "lbltoastMessage",
                "isVisible": true,
                "left": "45px",
                "right": "45px",
                "skin": "lblfffffflatoregular14px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.permission.SuccessfullyDeactivated\")",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var imgRight = new kony.ui.Image2({
                "centerY": "50%",
                "height": "15dp",
                "id": "imgRight",
                "isVisible": true,
                "right": "15px",
                "skin": "slImage",
                "src": "close_small2x.png",
                "width": "15dp",
                "zIndex": 1
            }, {
                "imageScaleMode": constants.IMAGE_SCALE_MODE_MAINTAIN_ASPECT_RATIO,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxToastContainer.add(imgLeft, lbltoastMessage, imgRight);
            var toastMessage = new com.adminConsole.common.toastMessage({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "height": "70px",
                "id": "toastMessage",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "masterType": constants.MASTER_TYPE_DEFAULT,
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0dp",
                "width": "100%",
                "overrides": {}
            }, {
                "overrides": {}
            }, {
                "overrides": {}
            });
            flxToastMessage.add(flxToastContainer, toastMessage);
            flxMain.add(flxLeftPanel, flxRightPanel, flxLoading, flxEditCancelConfirmation, flxToastMessage);
            this.add(flxMain);
        };
        return [{
            "addWidgets": addWidgetsfrmMFAScenarios,
            "enabledForIdleTimeout": true,
            "id": "frmMFAScenarios",
            "init": controller.AS_Form_he4dafed433a4a3f8c7a3a835ffa8873,
            "layoutType": kony.flex.FREE_FORM,
            "needAppMenu": false,
            "preShow": function(eventobject) {
                controller.AS_Form_f1e80b93c463429ab4135a1bd23f7a2b(eventobject);
            },
            "skin": "slForm"
        }, {
            "displayOrientation": constants.FORM_DISPLAY_ORIENTATION_PORTRAIT,
            "layoutType": kony.flex.FREE_FORM,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, {
            "retainScrollPosition": false
        }]
    }
});