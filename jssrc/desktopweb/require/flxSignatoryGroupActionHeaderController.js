define("userflxSignatoryGroupActionHeaderController", {
    //Type your controller code here 
});
define("flxSignatoryGroupActionHeaderControllerActions", {
    /*
        This is an auto generated file and any modifications to it may result in corruption of the action sequence.
    */
});
define("flxSignatoryGroupActionHeaderController", ["userflxSignatoryGroupActionHeaderController", "flxSignatoryGroupActionHeaderControllerActions"], function() {
    var controller = require("userflxSignatoryGroupActionHeaderController");
    var controllerActions = ["flxSignatoryGroupActionHeaderControllerActions"];
    return kony.visualizer.mixinControllerActions(controller, controllerActions);
});
