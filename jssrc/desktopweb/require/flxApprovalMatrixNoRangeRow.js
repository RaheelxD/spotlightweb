define("flxApprovalMatrixNoRangeRow", function() {
    return function(controller) {
        var flxApprovalMatrixNoRangeRow = new kony.ui.FlexContainer({
            "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
            "clipBounds": true,
            "id": "flxApprovalMatrixNoRangeRow",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "left": "0dp",
            "isModalContainer": false,
            "skin": "sknFlxFFFFFF100O",
            "top": "0dp",
            "width": "100%"
        }, {}, {});
        flxApprovalMatrixNoRangeRow.setDefaultUnit(kony.flex.DP);
        var flxNoRangesCont = new kony.ui.FlexContainer({
            "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
            "clipBounds": true,
            "id": "flxNoRangesCont",
            "isVisible": true,
            "layoutType": kony.flex.FLOW_VERTICAL,
            "left": "0",
            "isModalContainer": false,
            "skin": "slFbox",
            "top": "0dp",
            "width": "100%"
        }, {}, {});
        flxNoRangesCont.setDefaultUnit(kony.flex.DP);
        var lblNoApprovalText = new kony.ui.Label({
            "bottom": "15dp",
            "centerX": "50%",
            "id": "lblNoApprovalText",
            "isVisible": true,
            "skin": "sknlblLato696c7313px",
            "text": "There are no approval rules configured for this action",
            "top": "5dp",
            "width": kony.flex.USE_PREFFERED_SIZE
        }, {
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, {});
        var btnAddApproval = new kony.ui.Button({
            "bottom": "15dp",
            "centerX": "50%",
            "height": "22px",
            "id": "btnAddApproval",
            "isVisible": true,
            "skin": "sknbtnf7f7faLatoRegular12Px485c75Border1px485C75Radius20px",
            "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmPermissionsController.Add\")",
            "top": "0dp",
            "width": "52dp",
            "zIndex": 1
        }, {
            "contentAlignment": constants.CONTENT_ALIGN_CENTER,
            "displayText": true,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, {
            "hoverSkin": "sknbtnffffffLatoRegular12Px485c75Border1px485C75Radius20px"
        });
        flxNoRangesCont.add(lblNoApprovalText, btnAddApproval);
        var lblLine = new kony.ui.Label({
            "height": "17dp",
            "id": "lblLine",
            "isVisible": true,
            "right": "95dp",
            "skin": "sknLblSeparatore7e7e7",
            "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmCSR.lblSeperator\")",
            "top": "5dp",
            "width": "1dp",
            "zIndex": 1
        }, {
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, {});
        var flxApprovalEdit = new kony.ui.FlexContainer({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "clipBounds": true,
            "height": "20dp",
            "id": "flxApprovalEdit",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "isModalContainer": false,
            "right": "35dp",
            "skin": "sknCursor",
            "top": "5dp",
            "width": "20dp"
        }, {}, {});
        flxApprovalEdit.setDefaultUnit(kony.flex.DP);
        var lblIconAction = new kony.ui.Label({
            "centerX": "50%",
            "centerY": "50%",
            "id": "lblIconAction",
            "isVisible": true,
            "skin": "sknFontIcon485C7518px",
            "text": "",
            "width": kony.flex.USE_PREFFERED_SIZE
        }, {
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, {});
        flxApprovalEdit.add(lblIconAction);
        flxApprovalMatrixNoRangeRow.add(flxNoRangesCont, lblLine, flxApprovalEdit);
        return flxApprovalMatrixNoRangeRow;
    }
})