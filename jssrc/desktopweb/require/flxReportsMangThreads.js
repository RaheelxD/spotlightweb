define("flxReportsMangThreads", function() {
    return function(controller) {
        var flxReportsMangThreads = new kony.ui.FlexContainer({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "clipBounds": true,
            "height": "50px",
            "id": "flxReportsMangThreads",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "left": "0dp",
            "isModalContainer": false,
            "skin": "slFbox",
            "top": "0dp",
            "width": "100%"
        }, {}, {});
        flxReportsMangThreads.setDefaultUnit(kony.flex.DP);
        var flxMessage = new kony.ui.FlexContainer({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "clipBounds": true,
            "height": "50px",
            "id": "flxMessage",
            "isVisible": true,
            "layoutType": kony.flex.FLOW_HORIZONTAL,
            "left": "20px",
            "isModalContainer": false,
            "onClick": controller.AS_FlexContainer_e553ee3b13fe4973aef70f31c7776c5d,
            "right": "20px",
            "skin": "slFbox",
            "top": "0"
        }, {}, {});
        flxMessage.setDefaultUnit(kony.flex.DP);
        var flxFirstColumn = new kony.ui.FlexContainer({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "clipBounds": true,
            "height": "50px",
            "id": "flxFirstColumn",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "left": "0dp",
            "isModalContainer": false,
            "skin": "slFbox",
            "top": "0dp",
            "width": "65%",
            "zIndex": 10
        }, {}, {});
        flxFirstColumn.setDefaultUnit(kony.flex.DP);
        var lblDescription = new kony.ui.Label({
            "centerY": "49%",
            "id": "lblDescription",
            "isVisible": true,
            "left": "20px",
            "right": 0,
            "skin": "sknlblLatoRegular484b5213px",
            "text": "User enrolled",
            "zIndex": 1
        }, {
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, {});
        flxFirstColumn.add(lblDescription);
        var flxlastColoumn = new kony.ui.FlexContainer({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "centerY": "50%",
            "clipBounds": true,
            "height": "25px",
            "id": "flxlastColoumn",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "isModalContainer": false,
            "onClick": controller.AS_FlexContainer_b841079c89634eada06d07f145523502,
            "skin": "slFbox",
            "width": "35%",
            "zIndex": 10
        }, {}, {});
        flxlastColoumn.setDefaultUnit(kony.flex.DP);
        var lblValue = new kony.ui.Label({
            "centerY": "50%",
            "id": "lblValue",
            "isVisible": true,
            "left": "0",
            "right": "20px",
            "skin": "sknlblLatoRegular484b5213px",
            "text": "5000",
            "zIndex": 1
        }, {
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, {});
        flxlastColoumn.add(lblValue);
        flxMessage.add(flxFirstColumn, flxlastColoumn);
        var lblSeperator = new kony.ui.Label({
            "bottom": "0px",
            "height": "1px",
            "id": "lblSeperator",
            "isVisible": true,
            "left": "20px",
            "right": "20px",
            "skin": "sknlblSeperator",
            "text": ".",
            "zIndex": 1
        }, {
            "contentAlignment": constants.CONTENT_ALIGN_TOP_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, {});
        flxReportsMangThreads.add(flxMessage, lblSeperator);
        return flxReportsMangThreads;
    }
})