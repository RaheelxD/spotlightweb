define("flxRolesServiceDefRow", function() {
    return function(controller) {
        var flxRolesServiceDefRow = new kony.ui.FlexContainer({
            "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
            "clipBounds": true,
            "id": "flxRolesServiceDefRow",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "left": "0dp",
            "isModalContainer": false,
            "skin": "sknFlxFFFFFF100O",
            "top": "0dp",
            "width": "100%"
        }, {}, {});
        flxRolesServiceDefRow.setDefaultUnit(kony.flex.DP);
        var flxServiceDefName = new kony.ui.FlexContainer({
            "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
            "bottom": "5dp",
            "clipBounds": true,
            "id": "flxServiceDefName",
            "isVisible": true,
            "layoutType": kony.flex.FLOW_HORIZONTAL,
            "left": "15dp",
            "isModalContainer": false,
            "skin": "slFbox",
            "top": "7dp",
            "width": "27%"
        }, {}, {
            "hoverSkin": "sknCursor"
        });
        flxServiceDefName.setDefaultUnit(kony.flex.DP);
        var flxCheckbox = new kony.ui.FlexContainer({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "clipBounds": true,
            "height": "15dp",
            "id": "flxCheckbox",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "left": "0dp",
            "isModalContainer": false,
            "skin": "slFbox",
            "top": "0dp",
            "width": "15dp"
        }, {}, {});
        flxCheckbox.setDefaultUnit(kony.flex.DP);
        var imgCheckbox = new kony.ui.Image2({
            "height": "100%",
            "id": "imgCheckbox",
            "isVisible": true,
            "left": "0",
            "skin": "slImage",
            "src": "checkboxnormal.png",
            "top": "0",
            "width": "100%"
        }, {
            "imageScaleMode": constants.IMAGE_SCALE_MODE_MAINTAIN_ASPECT_RATIO,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, {});
        flxCheckbox.add(imgCheckbox);
        var lblServiceDefName = new kony.ui.Label({
            "id": "lblServiceDefName",
            "isVisible": true,
            "left": "10dp",
            "skin": "sknLbl485C75LatoRegular13Px",
            "text": "Retail Basic",
            "top": "0dp",
            "width": "85%"
        }, {
            "contentAlignment": constants.CONTENT_ALIGN_TOP_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, {});
        flxServiceDefName.add(flxCheckbox, lblServiceDefName);
        var flxServiceDefDesc = new kony.ui.FlexContainer({
            "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
            "bottom": "5dp",
            "clipBounds": true,
            "id": "flxServiceDefDesc",
            "isVisible": true,
            "layoutType": kony.flex.FLOW_HORIZONTAL,
            "left": "31%",
            "isModalContainer": false,
            "right": "20dp",
            "skin": "slFbox",
            "top": "10dp"
        }, {}, {
            "hoverSkin": "sknCursor"
        });
        flxServiceDefDesc.setDefaultUnit(kony.flex.DP);
        var lblServiceDefDesc = new kony.ui.Label({
            "id": "lblServiceDefDesc",
            "isVisible": true,
            "left": "0dp",
            "skin": "sknLbl485C75LatoRegular13Px",
            "text": "Ability to view list of all transactions made at FI level",
            "top": "0dp",
            "width": "99%"
        }, {
            "contentAlignment": constants.CONTENT_ALIGN_TOP_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, {});
        flxServiceDefDesc.add(lblServiceDefDesc);
        flxRolesServiceDefRow.add(flxServiceDefName, flxServiceDefDesc);
        return flxRolesServiceDefRow;
    }
})