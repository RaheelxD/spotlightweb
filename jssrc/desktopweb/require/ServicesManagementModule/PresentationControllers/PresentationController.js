define(['ErrorInterceptor', 'ErrorIsNetworkDown', 'Promisify'], function(ErrorInterceptor, isNetworkDown, Promisify) {
    function ServicesManagement_PresentationController() {
        kony.mvc.Presentation.BasePresenter.call(this);
    }
    inheritsFrom(ServicesManagement_PresentationController, kony.mvc.Presentation.BasePresenter);
    ServicesManagement_PresentationController.prototype.initializePresentationController = function() {
        var self = this;
        ErrorInterceptor.wrap(this, 'businessController').match(function(on) {
            return [
                on(isNetworkDown).do(function() {
                    self.presentUserInterface("frmServiceManagement", {
                        NetworkDownMessage: {}
                    });
                })
            ];
        });
    };
    /**
     * @name showFeatures
     * @member ServicesManagementModule.presentationController
     * 
     */
    ServicesManagement_PresentationController.prototype.showFeatures = function() {
        var self = this;
        self.showLoadingScreen();
        var promiseGetAllFeatures = Promisify(this.businessController, 'getAllFeatures');
        var promiseFetchAllLocale = Promisify(this.businessController, 'getAllLocaleLanguagesList');
        Promise.all([
            promiseGetAllFeatures({}),
            promiseFetchAllLocale({})
        ]).then(function(responses) {
            var context = {
                featuresList: responses[0].features,
                localeLanguages: responses[1]
            };
            self.showFeaturesScreen(context);
            self.hideLoadingScreen();
        }).catch(function(error) {
            self.hideLoadingScreen();
            self.showToastMessageFlex(ErrorInterceptor.errorMessage(error), "error");
        });
    };
    /**
     * @name fetchAllFeatures
     * @member ServicesManagementModule.presentationController
     * 
     */
    ServicesManagement_PresentationController.prototype.fetchAllFeatures = function() {
        var self = this;

        function successCallback(response) {
            self.showFeaturesScreen({
                featuresList: response.features
            });
            self.hideLoadingScreen();
        }

        function failureCallback(error) {
            self.showToastMessageFlex(ErrorInterceptor.errorMessage(error), "error");
            self.hideLoadingScreen();
        }
        self.showLoadingScreen();
        self.businessController.getAllFeatures({}, successCallback, failureCallback);
    };
    /*
     * function to call command handler to update a service
     * @param : edited request
     */
    ServicesManagement_PresentationController.prototype.updateFeature = function(editedParamReq) {
        var self = this;

        function successCallback(response) {
            self.fetchAllFeatures();
            self.showToastMessageFlex(kony.i18n.getLocalizedString("i18n.frmServiceManagement.Feature_edited_msg"), "success");
        }

        function failureCallback(response) {
            self.showToastMessageFlex(ErrorInterceptor.errorMessage(response), "error");
            self.hideLoadingScreen();
        }
        self.showLoadingScreen();
        self.businessController.updateService(editedParamReq, successCallback, failureCallback);
    };
    /*
     * function to call command handler to update a service
     * @param : edited request,opt (1-list or 2-details)
     */
    ServicesManagement_PresentationController.prototype.updateStatusOfFeature = function(editedParamReq, opt) {
        var self = this;
        var status = editedParamReq.statusId;

        function successCallback(response) {
            if (status === "SID_FEATURE_ACTIVE") {
                self.showToastMessageFlex(kony.i18n.getLocalizedString("i18n.frmServiceManagement.FeatureActivatedSuccessfully"), "success");
            } else {
                self.showToastMessageFlex(kony.i18n.getLocalizedString("i18n.frmServiceManagement.FeatureDeactivatedSuccessfully"), "success");
            }
            self.fetchAllFeatures();
        }

        function failureCallback(response) {
            self.showToastMessageFlex(ErrorInterceptor.errorMessage(response), "error");
            self.hideLoadingScreen();
        }
        self.showLoadingScreen();
        self.businessController.updateService(editedParamReq, successCallback, failureCallback);
    };
    /*
     * function to call command handler to update an action status
     * @param : edited request,feature id for setting context after update
     */
    ServicesManagement_PresentationController.prototype.updateStatusOfAction = function(editedParamReq, featureId) {
        var self = this;
        var status = editedParamReq.status;

        function successCallback(response) {
            if (status === "SID_ACTION_ACTIVE") {
                self.showToastMessageFlex(kony.i18n.getLocalizedString("i18n.frmServiceManagementController.successMsg.ActivateAction"), "success");
            } else {
                self.showToastMessageFlex(kony.i18n.getLocalizedString("i18n.frmServiceManagementController.successMsg.DeactivateAction"), "success");
            }
            self.getActionsOfFeature({
                "context": "detailsView",
                "requestParam": {
                    "featureId": featureId
                }
            });
        }

        function failureCallback(response) {
            self.showToastMessageFlex(ErrorInterceptor.errorMessage(response), "error");
            self.hideLoadingScreen();
        }
        self.showLoadingScreen();
        self.businessController.updateActionStatus(editedParamReq, successCallback, failureCallback);
    };
    /**
     * @name getFeaturesMasterData
     * @member ServicesManagementModule.presentationController
     * 
     */
    ServicesManagement_PresentationController.prototype.getFeaturesMasterData = function() {
        var self = this;
        self.showLoadingScreen();

        function successCallback(response) {
            self.hideLoadingScreen();
            self.showFeaturesScreen({
                localeLanguages: response
            });
        }

        function failureCallback(response) {
            self.hideLoadingScreen();
        }
        self.businessController.getAllLocaleLanguagesList({}, successCallback, failureCallback);
    };
    /**
     * @name getActionsOfFeature
     * @member ServicesManagementModule.presentationController
     * 
     */
    ServicesManagement_PresentationController.prototype.getActionsOfFeature = function(inputParam) {
        var self = this;
        self.showLoadingScreen();
        var context = inputParam.context;
        var inputReq = inputParam.requestParam;

        function successCallback(response) {
            self.showFeaturesScreen({
                context: context,
                actionsOfFeature: response.actions
            });
            self.hideLoadingScreen();
        }

        function failureCallback(error) {
            self.showToastMessageFlex(ErrorInterceptor.errorMessage(error), "error");
            self.hideLoadingScreen();
        }
        self.businessController.getActionsOfFeature(inputReq, successCallback, failureCallback);
    };
    /**
     * @name getLimitGroups
     * @member ServicesManagementModule.presentationController
     * 
     */
    ServicesManagement_PresentationController.prototype.getLimitGroups = function() {
        var self = this;
        self.showLoadingScreen();

        function successCallback(response) {
            self.showFeaturesScreen({
                limitGroups: response
            });
            self.hideLoadingScreen();
        }

        function failureCallback(error) {
            self.showToastMessageFlex(ErrorInterceptor.errorMessage(error), "error");
            self.hideLoadingScreen();
        }
        self.businessController.getLimitGroups({}, successCallback, failureCallback);
    };
    /**
     * @name editLimitGroup
     * @member ServicesManagementModule.presentationController
     * 
     */
    ServicesManagement_PresentationController.prototype.editLimitGroup = function(inputparam) {
        var self = this;
        self.showLoadingScreen();

        function successCallback(response) {
            self.showToastMessageFlex(kony.i18n.getLocalizedString("i18n.frmServiceManagement.LimitGroupUpdatedSuccessfully"), "success");
            self.getLimitGroups();
        }

        function failureCallback(error) {
            self.showToastMessageFlex(ErrorInterceptor.errorMessage(error), "error");
            self.hideLoadingScreen();
        }
        self.businessController.editLimitGroup(inputparam, successCallback, failureCallback);
    };
    /**
     * @name showFeaturesScreen
     * @member ServicesManagementModule.presentationController
     * @param {servicesList : [{Description : object, DisplayDescription : object, WorkSchedule_Desc : object, IsSMSAlertActivated : object, IsAgreementActive : object, BeneficiarySMSCharge : object, TransactionFee_id : object, Channel_id : object, IsFutureTransaction : object, Name : object, IsOutageMessageActive : object, IsBeneficiarySMSAlertActivated : object, MinTransferLimit : object, MaxTransferLimit : object, createdby : object, DisplayName : object, HasWeekendOperation : object, id : object, TransactionCharges : object, TransactionFees : object, Status : object, Type_Name : object, Status_id : object, Channel : object, IsAuthorizationRequired : object, SMSCharges : object, TransferDenominations : object, Category_Name : object, IsCampaignActive : object, Code : object, WorkSchedule_id : object, Category_Id : object, IsTCActive : object, IsAlertActive : object, TransactionLimit_id : object, Type_id : object, PeriodicLimits : object}]} viewModel
     */
    ServicesManagement_PresentationController.prototype.showFeaturesScreen = function(viewModel) {
        var self = this;
        if (viewModel) {
            self.presentUserInterface("frmServiceManagement", viewModel);
        } else {
            self.presentUserInterface("frmServiceManagement");
        }
    };
    /**
     * @name showLoadingScreen
     * @member ServicesManagementModule.presentationController
     * 
     */
    ServicesManagement_PresentationController.prototype.showLoadingScreen = function() {
        this.showFeaturesScreen({
            LoadingScreen: {
                show: true
            }
        });
    };
    /**
     * @name hideLoadingScreen
     * @member ServicesManagementModule.presentationController
     * 
     */
    ServicesManagement_PresentationController.prototype.hideLoadingScreen = function() {
        this.showFeaturesScreen({
            LoadingScreen: {
                show: false
            }
        });
    };
    /*
     * common function to present user interface with Toast message
     */
    ServicesManagement_PresentationController.prototype.showToastMessageFlex = function(msg, status) {
        var self = this;
        self.presentUserInterface("frmServiceManagement", {
            toast: {
                message: msg,
                status: status
            }
        });
    };
    return ServicesManagement_PresentationController;
});