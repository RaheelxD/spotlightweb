define("ServicesManagementModule/frmServiceManagement", function() {
    return function(controller) {
        function addWidgetsfrmServiceManagement() {
            this.setDefaultUnit(kony.flex.DP);
            var flxServiceManagement = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "624dp",
                "id": "flxServiceManagement",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "slFbox0b7aaa2e3bfa340",
                "top": "0dp",
                "width": "100%",
                "zIndex": 1
            }, {}, {});
            flxServiceManagement.setDefaultUnit(kony.flex.DP);
            var flxLeftPannel = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "100%",
                "id": "flxLeftPannel",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "masterType": constants.MASTER_TYPE_USERWIDGET,
                "isModalContainer": false,
                "skin": "sknLeftPannel0bb79e4bcca8e45",
                "top": "0dp",
                "width": "305px",
                "zIndex": 5
            }, {}, {});
            flxLeftPannel.setDefaultUnit(kony.flex.DP);
            var leftMenuNew = new com.adminConsole.navigation.leftMenuNew({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "height": "100%",
                "id": "leftMenuNew",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "masterType": constants.MASTER_TYPE_DEFAULT,
                "isModalContainer": false,
                "skin": "sknFlxBg003E75Op100",
                "top": "0dp",
                "width": "100%",
                "overrides": {
                    "brandLogo": {
                        "src": "infinity_dbx_c360_logo_white_2x.png"
                    },
                    "imgBottomLogo": {
                        "src": "logo_white_2x.png"
                    },
                    "leftMenuNew": {
                        "bottom": "viz.val_cleared",
                        "centerX": "viz.val_cleared",
                        "centerY": "viz.val_cleared",
                        "height": "100%",
                        "left": "0dp",
                        "maxHeight": "viz.val_cleared",
                        "maxWidth": "viz.val_cleared",
                        "minHeight": "viz.val_cleared",
                        "minWidth": "viz.val_cleared",
                        "right": "viz.val_cleared",
                        "top": "0dp",
                        "width": "100%"
                    },
                    "segMenu": {
                        "left": "0dp",
                        "top": "0dp"
                    }
                }
            }, {
                "overrides": {}
            }, {
                "overrides": {}
            });
            flxLeftPannel.add(leftMenuNew);
            var flxRightPanel = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "100%",
                "id": "flxRightPanel",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "305px",
                "masterType": constants.MASTER_TYPE_USERWIDGET,
                "isModalContainer": false,
                "right": 0,
                "skin": "slFbox0b7aaa2e3bfa340",
                "top": "0px",
                "zIndex": 1
            }, {}, {});
            flxRightPanel.setDefaultUnit(kony.flex.DP);
            var flxMainHeader = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                "clipBounds": true,
                "id": "flxMainHeader",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "isModalContainer": false,
                "right": "0px",
                "skin": "CopyslFbox0c18001ae52ef46",
                "top": "0dp",
                "zIndex": 10
            }, {}, {});
            flxMainHeader.setDefaultUnit(kony.flex.DP);
            var breadcrumbs = new com.adminConsole.common.breadcrumbs({
                "bottom": 0,
                "height": "20px",
                "id": "breadcrumbs",
                "isVisible": false,
                "layoutType": kony.flex.FLOW_HORIZONTAL,
                "left": "0dp",
                "masterType": constants.MASTER_TYPE_DEFAULT,
                "isModalContainer": false,
                "skin": "sknflxf5f6f8Op100",
                "top": "105dp",
                "width": "100%",
                "overrides": {
                    "breadcrumbs": {
                        "bottom": 0,
                        "isVisible": false,
                        "top": "105dp"
                    },
                    "btnBackToMain": {
                        "text": "SERVICES"
                    },
                    "imgBreadcrumbsDown": {
                        "src": "img_down_arrow.png"
                    },
                    "imgBreadcrumbsRight": {
                        "src": "img_breadcrumb_arrow.png"
                    },
                    "imgBreadcrumbsRight2": {
                        "src": "img_breadcrumb_arrow.png"
                    },
                    "imgBreadcrumbsRight3": {
                        "src": "img_breadcrumb_arrow.png"
                    },
                    "lblCurrentScreen": {
                        "text": "ADD SERVICES"
                    }
                }
            }, {
                "overrides": {}
            }, {
                "overrides": {}
            });
            var mainHeader = new com.adminConsole.locations.mainHeader({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "height": "106px",
                "id": "mainHeader",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": 0,
                "masterType": constants.MASTER_TYPE_DEFAULT,
                "isModalContainer": false,
                "skin": "CopyslFbox1",
                "top": 0,
                "width": "100%",
                "overrides": {
                    "btnAddNewOption": {
                        "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmServiceManagement.LimitGroups\")",
                        "isVisible": true
                    },
                    "flxDownloadList": {
                        "isVisible": false
                    },
                    "imgLogout": {
                        "src": "img_logout.png"
                    },
                    "lblHeading": {
                        "text": "Features"
                    }
                }
            }, {
                "overrides": {}
            }, {
                "overrides": {}
            });
            flxMainHeader.add(breadcrumbs, mainHeader);
            var flxHeaderDropdown = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                "clipBounds": true,
                "id": "flxHeaderDropdown",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "isModalContainer": false,
                "right": "35dp",
                "skin": "slFbox",
                "top": "20dp",
                "width": "400dp",
                "zIndex": 11
            }, {}, {});
            flxHeaderDropdown.setDefaultUnit(kony.flex.DP);
            var dropdownMainHeader = new com.adminConsole.common.dropdownMainHeader({
                "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                "id": "dropdownMainHeader",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "masterType": constants.MASTER_TYPE_DEFAULT,
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0dp",
                "width": "100%",
                "overrides": {
                    "flxDropdown": {
                        "isVisible": false
                    },
                    "imgUpArrow": {
                        "src": "uparrow_2x.png"
                    }
                }
            }, {
                "overrides": {}
            }, {
                "overrides": {}
            });
            flxHeaderDropdown.add(dropdownMainHeader);
            var flxMainContent = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "bottom": 0,
                "clipBounds": false,
                "id": "flxMainContent",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0px",
                "isModalContainer": false,
                "skin": "Copysknscrollflxf0ja093a1ef62648",
                "top": "105dp",
                "width": "100%",
                "zIndex": 1
            }, {}, {});
            flxMainContent.setDefaultUnit(kony.flex.DP);
            var flxAddService = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "340dp",
                "id": "flxAddService",
                "isVisible": false,
                "layoutType": kony.flex.FREE_FORM,
                "left": "35px",
                "isModalContainer": false,
                "right": "35px",
                "skin": "sknflxUploadImage0e65d5a93fe0f45",
                "top": "0dp",
                "zIndex": 1
            }, {}, {});
            flxAddService.setDefaultUnit(kony.flex.DP);
            var flxAddServiceMain = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "100%",
                "id": "flxAddServiceMain",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0dp",
                "width": "100%",
                "zIndex": 1
            }, {}, {});
            flxAddServiceMain.setDefaultUnit(kony.flex.DP);
            var lblNoServiceAdded = new kony.ui.Label({
                "centerX": "50%",
                "id": "lblNoServiceAdded",
                "isVisible": true,
                "skin": "sknLblLato",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmServiceManagement.lblNoServiceAdded\")",
                "top": "100dp",
                "width": "100%",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_CENTER,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var lblClickOnAddService = new kony.ui.Label({
                "id": "lblClickOnAddService",
                "isVisible": false,
                "left": 0,
                "skin": "sknlbllatoRegular0f715a58455b840",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmServiceManagement.lblClickOnAddService\")",
                "top": "140dp",
                "width": "100%",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_CENTER,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var btnAddService = new kony.ui.Button({
                "centerX": "50%",
                "focusSkin": "sknBtn003E75LatoRegular13pxFFFFFFRad20px",
                "height": "40dp",
                "id": "btnAddService",
                "isVisible": false,
                "left": 0,
                "skin": "sknBtn003E75LatoRegular13pxFFFFFFRad20px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmServiceManagement.btnAddService\")",
                "top": "180dp",
                "width": "110dp",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_CENTER,
                "displayText": true,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {
                "hoverSkin": "sknBtn005198LatoRegular13pxFFFFFFRad20px"
            });
            flxAddServiceMain.add(lblNoServiceAdded, lblClickOnAddService, btnAddService);
            flxAddService.add(flxAddServiceMain);
            var flxScrollViewServices = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "bottom": "0px",
                "clipBounds": false,
                "id": "flxScrollViewServices",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "35px",
                "isModalContainer": false,
                "right": "0dp",
                "top": "0dp",
                "zIndex": 1
            }, {}, {});
            flxScrollViewServices.setDefaultUnit(kony.flex.DP);
            var flxListingPage = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "100%",
                "id": "flxListingPage",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0px",
                "isModalContainer": false,
                "right": "0px",
                "skin": "slFbox",
                "top": "0px",
                "zIndex": 1
            }, {}, {});
            flxListingPage.setDefaultUnit(kony.flex.DP);
            var flxMainSubHeader = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "70dp",
                "id": "flxMainSubHeader",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": 0,
                "isModalContainer": false,
                "right": "35px",
                "skin": "slFbox",
                "top": "0dp",
                "width": "100%",
                "zIndex": 100
            }, {}, {});
            flxMainSubHeader.setDefaultUnit(kony.flex.DP);
            var subHeader = new com.adminConsole.header.subHeader({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "centerY": "50%",
                "height": "60dp",
                "id": "subHeader",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "masterType": constants.MASTER_TYPE_DEFAULT,
                "isModalContainer": false,
                "skin": "CopyslFbox2",
                "width": "100%",
                "overrides": {
                    "flxClearSearchImage": {
                        "isVisible": false
                    },
                    "flxMenu": {
                        "isVisible": false,
                        "left": "0dp"
                    },
                    "flxSearch": {
                        "right": "35px",
                        "width": "500px"
                    },
                    "flxSearchContainer": {
                        "centerY": "50%",
                        "height": "40px",
                        "left": 0,
                        "right": "viz.val_cleared",
                        "top": "viz.val_cleared",
                        "width": "500px"
                    },
                    "subHeader": {
                        "centerY": "50%",
                        "height": "60dp",
                        "top": "viz.val_cleared"
                    },
                    "tbxSearchBox": {
                        "placeholder": "Search by Feature Name"
                    }
                }
            }, {
                "overrides": {}
            }, {
                "overrides": {}
            });
            flxMainSubHeader.add(subHeader);
            var flxViewServicesWrapper = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "bottom": "0px",
                "clipBounds": true,
                "id": "flxViewServicesWrapper",
                "isVisible": true,
                "layoutType": kony.flex.FLOW_VERTICAL,
                "left": "0px",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "70dp",
                "width": "100%",
                "zIndex": 100
            }, {}, {});
            flxViewServicesWrapper.setDefaultUnit(kony.flex.DP);
            var flxSegmentWrapper = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                "clipBounds": true,
                "id": "flxSegmentWrapper",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "isModalContainer": false,
                "top": "0dp",
                "width": "100%",
                "zIndex": 1
            }, {}, {});
            flxSegmentWrapper.setDefaultUnit(kony.flex.DP);
            var flxHeaderAndSegmentWrapper = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                "bottom": "35px",
                "clipBounds": false,
                "id": "flxHeaderAndSegmentWrapper",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0px",
                "isModalContainer": false,
                "right": "35px",
                "skin": "sknflxffffffop100Bordercbcdd1Radius3px",
                "top": "0dp",
                "zIndex": 1
            }, {}, {});
            flxHeaderAndSegmentWrapper.setDefaultUnit(kony.flex.DP);
            var flxViewServicesHeader = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "60px",
                "id": "flxViewServicesHeader",
                "isVisible": false,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0px",
                "isModalContainer": false,
                "right": "0px",
                "skin": "slFbox",
                "top": "0"
            }, {}, {});
            flxViewServicesHeader.setDefaultUnit(kony.flex.DP);
            var flxViewServicesHeaderName = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "100%",
                "id": "flxViewServicesHeaderName",
                "isVisible": true,
                "layoutType": kony.flex.FLOW_HORIZONTAL,
                "left": "30px",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0dp",
                "width": "60px",
                "zIndex": 1
            }, {}, {});
            flxViewServicesHeaderName.setDefaultUnit(kony.flex.DP);
            var lblHeaderName = new kony.ui.Label({
                "centerY": "50%",
                "id": "lblHeaderName",
                "isVisible": true,
                "left": "0",
                "skin": "sknlblLato696c7312px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.permission.NAME\")",
                "top": 0,
                "width": "38px",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {
                "hoverSkin": "sknlblLato00000011px"
            });
            var lblSortName = new kony.ui.Label({
                "centerY": "50%",
                "height": "15dp",
                "id": "lblSortName",
                "isVisible": true,
                "left": "0dp",
                "skin": "sknIcon15px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmAlertsManagement.lblSortName\")",
                "width": "15dp",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxViewServicesHeaderName.add(lblHeaderName, lblSortName);
            var flxViewServicesHeaderCode = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "100%",
                "id": "flxViewServicesHeaderCode",
                "isVisible": true,
                "layoutType": kony.flex.FLOW_HORIZONTAL,
                "left": "21.98%",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0dp",
                "width": "60px",
                "zIndex": 1
            }, {}, {});
            flxViewServicesHeaderCode.setDefaultUnit(kony.flex.DP);
            var lblViewServicesHeaderCode = new kony.ui.Label({
                "centerY": "50%",
                "id": "lblViewServicesHeaderCode",
                "isVisible": true,
                "left": "0px",
                "skin": "sknlblLato696c7312px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.Group.CODE\")",
                "top": 0,
                "width": "35px",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {
                "hoverSkin": "sknlblLato00000011px"
            });
            var lblSortCode = new kony.ui.Label({
                "centerY": "50%",
                "height": "15dp",
                "id": "lblSortCode",
                "isVisible": true,
                "left": "0",
                "skin": "sknIcon15px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmAlertsManagement.lblSortName\")",
                "width": "15dp",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxViewServicesHeaderCode.add(lblViewServicesHeaderCode, lblSortCode);
            var flxViewServicesHeaderType = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "100%",
                "id": "flxViewServicesHeaderType",
                "isVisible": true,
                "layoutType": kony.flex.FLOW_HORIZONTAL,
                "left": "35%",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0dp",
                "width": "100px",
                "zIndex": 1
            }, {}, {});
            flxViewServicesHeaderType.setDefaultUnit(kony.flex.DP);
            var lblViewServicesHeaderType = new kony.ui.Label({
                "centerY": "50%",
                "id": "lblViewServicesHeaderType",
                "isVisible": true,
                "left": "0px",
                "skin": "sknlblLato696c7312px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.Group.TYPE\")",
                "top": 0,
                "width": "80px",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxViewServicesHeaderType.add(lblViewServicesHeaderType);
            var flxViewServicesHeaderCategory = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "100%",
                "id": "flxViewServicesHeaderCategory",
                "isVisible": true,
                "layoutType": kony.flex.FLOW_HORIZONTAL,
                "left": "47%",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0dp",
                "width": "90px",
                "zIndex": 1
            }, {}, {});
            flxViewServicesHeaderCategory.setDefaultUnit(kony.flex.DP);
            var lblViewServicesHeaderCategory = new kony.ui.Label({
                "centerY": "50%",
                "id": "lblViewServicesHeaderCategory",
                "isVisible": true,
                "left": "0px",
                "skin": "sknlblLato696c7312px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.Group.CATEGORY\")",
                "top": 0,
                "width": "65px",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {
                "hoverSkin": "sknlblLato00000011px"
            });
            var lblSortCategory = new kony.ui.Label({
                "centerY": "50%",
                "height": "15dp",
                "id": "lblSortCategory",
                "isVisible": true,
                "left": "0",
                "skin": "sknIcon15px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmAlertsManagement.lblSortName\")",
                "width": "15dp",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxViewServicesHeaderCategory.add(lblViewServicesHeaderCategory, lblSortCategory);
            var flxLocationsHeaderSC = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "100%",
                "id": "flxLocationsHeaderSC",
                "isVisible": true,
                "layoutType": kony.flex.FLOW_HORIZONTAL,
                "left": "61.73%",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0dp",
                "width": "165px",
                "zIndex": 1
            }, {}, {});
            flxLocationsHeaderSC.setDefaultUnit(kony.flex.DP);
            var lblViewServicesHeaderSC = new kony.ui.Label({
                "centerY": "50%",
                "id": "lblViewServicesHeaderSC",
                "isVisible": true,
                "left": "0px",
                "skin": "sknlblLato696c7312px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.Services.Supportedchannels\")",
                "top": 0,
                "width": "140px",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {
                "hoverSkin": "sknlblLato00000011px"
            });
            var lblFilterSC = new kony.ui.Label({
                "centerY": "50%",
                "height": "15dp",
                "id": "lblFilterSC",
                "isVisible": true,
                "skin": "sknIcon15px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmAlertsManagement.lblSortName\")",
                "width": "15dp",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxLocationsHeaderSC.add(lblViewServicesHeaderSC, lblFilterSC);
            var flxStatus = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "100%",
                "id": "flxStatus",
                "isVisible": true,
                "layoutType": kony.flex.FLOW_HORIZONTAL,
                "left": "84.06%",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0dp",
                "width": "65px",
                "zIndex": 1
            }, {}, {});
            flxStatus.setDefaultUnit(kony.flex.DP);
            var lblStatus = new kony.ui.Label({
                "centerY": "50%",
                "id": "lblStatus",
                "isVisible": true,
                "left": "0px",
                "skin": "sknlblLato696c7312px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.roles.STATUS\")",
                "top": 0,
                "width": "50px",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {
                "hoverSkin": "sknlblLato00000011px"
            });
            var lblSortStatus = new kony.ui.Label({
                "centerY": "50%",
                "height": "15dp",
                "id": "lblSortStatus",
                "isVisible": true,
                "left": "0",
                "skin": "sknIcon15px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmAlertsManagement.lblIconChannels\")",
                "width": "15dp",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {
                "hoverSkin": "sknlblCursorFont"
            });
            flxStatus.add(lblStatus, lblSortStatus);
            var lblViewServicesHeaderSeperator = new kony.ui.Label({
                "bottom": 0,
                "height": "1px",
                "id": "lblViewServicesHeaderSeperator",
                "isVisible": true,
                "left": "20px",
                "right": "35px",
                "skin": "sknLblTableHeaderLine",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmCSR.lblSeperator\")",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxViewServicesHeader.add(flxViewServicesHeaderName, flxViewServicesHeaderCode, flxViewServicesHeaderType, flxViewServicesHeaderCategory, flxLocationsHeaderSC, flxStatus, lblViewServicesHeaderSeperator);
            var flxViewServicesSegment = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "600px",
                "id": "flxViewServicesSegment",
                "isVisible": false,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "isModalContainer": false,
                "right": 0,
                "skin": "slFbox",
                "top": "60dp",
                "width": "100%",
                "zIndex": 1
            }, {}, {});
            flxViewServicesSegment.setDefaultUnit(kony.flex.DP);
            var listingSegmentClient = new com.adminConsole.common.listingSegmentClient({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "height": "100%",
                "id": "listingSegmentClient",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "masterType": constants.MASTER_TYPE_DEFAULT,
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0dp",
                "width": "100%",
                "overrides": {
                    "contextualMenu.imgDownArrow": {
                        "src": "downarrow_2x.png"
                    },
                    "contextualMenu.imgOption1": {
                        "src": "imagedrag.png"
                    },
                    "contextualMenu.imgOption2": {
                        "src": "edit2x.png"
                    },
                    "contextualMenu.imgOption3": {
                        "src": "imagedrag.png"
                    },
                    "contextualMenu.imgOption4": {
                        "src": "deactive_2x.png"
                    },
                    "contextualMenu.imgUpArrow": {
                        "src": "uparrow_2x.png"
                    },
                    "flxContextualMenu": {
                        "right": "33px"
                    },
                    "flxListingSegmentWrapper": {
                        "height": "100%"
                    },
                    "flxPagination": {
                        "isVisible": false
                    },
                    "listingSegmentClient": {
                        "bottom": "viz.val_cleared",
                        "centerX": "viz.val_cleared",
                        "centerY": "viz.val_cleared",
                        "height": "100%",
                        "left": "0dp",
                        "maxHeight": "viz.val_cleared",
                        "maxWidth": "viz.val_cleared",
                        "minHeight": "viz.val_cleared",
                        "minWidth": "viz.val_cleared",
                        "right": "viz.val_cleared",
                        "top": "0dp",
                        "width": "100%"
                    },
                    "segListing": {
                        "height": "100%"
                    }
                }
            }, {
                "overrides": {}
            }, {
                "overrides": {}
            });
            flxViewServicesSegment.add(listingSegmentClient);
            var flxFeatureStatusFilter = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                "clipBounds": true,
                "id": "flxFeatureStatusFilter",
                "isVisible": false,
                "layoutType": kony.flex.FREE_FORM,
                "isModalContainer": false,
                "right": "70dp",
                "skin": "slFbox",
                "top": "35px",
                "width": "130dp",
                "zIndex": 5
            }, {}, {});
            flxFeatureStatusFilter.setDefaultUnit(kony.flex.DP);
            var statusFilterMenu = new com.adminConsole.common.statusFilterMenu({
                "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                "id": "statusFilterMenu",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "masterType": constants.MASTER_TYPE_DEFAULT,
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "5dp",
                "width": "100%",
                "zIndex": 5,
                "overrides": {
                    "flxChechboxOuter": {
                        "zIndex": 1
                    },
                    "imgUpArrow": {
                        "src": "uparrow_2x.png"
                    },
                    "segStatusFilterDropdown": {
                        "left": "0dp",
                        "top": "5dp",
                        "zIndex": 1
                    }
                }
            }, {
                "overrides": {}
            }, {
                "overrides": {}
            });
            flxFeatureStatusFilter.add(statusFilterMenu);
            var flxFeatureTypeFilter = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                "clipBounds": true,
                "id": "flxFeatureTypeFilter",
                "isVisible": false,
                "layoutType": kony.flex.FREE_FORM,
                "isModalContainer": false,
                "right": "150dp",
                "skin": "slFbox",
                "top": "35px",
                "width": "195dp",
                "zIndex": 5
            }, {}, {});
            flxFeatureTypeFilter.setDefaultUnit(kony.flex.DP);
            var TypeFilterMenu = new com.adminConsole.common.statusFilterMenu({
                "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                "id": "TypeFilterMenu",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "masterType": constants.MASTER_TYPE_DEFAULT,
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "5dp",
                "width": "100%",
                "zIndex": 5,
                "overrides": {
                    "flxChechboxOuter": {
                        "zIndex": 1
                    },
                    "imgUpArrow": {
                        "src": "uparrow_2x.png"
                    },
                    "segStatusFilterDropdown": {
                        "left": "0dp",
                        "top": "5dp",
                        "zIndex": 1
                    }
                }
            }, {
                "overrides": {}
            }, {
                "overrides": {}
            });
            flxFeatureTypeFilter.add(TypeFilterMenu);
            var flxHeaderFeatures = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "60px",
                "id": "flxHeaderFeatures",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "20dp",
                "isModalContainer": false,
                "right": "20dp",
                "skin": "slFbox",
                "top": "0dp"
            }, {}, {});
            flxHeaderFeatures.setDefaultUnit(kony.flex.DP);
            var flxFeatureName = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "centerY": "50%",
                "clipBounds": true,
                "height": "100%",
                "id": "flxFeatureName",
                "isVisible": true,
                "layoutType": kony.flex.FLOW_HORIZONTAL,
                "left": "4%",
                "isModalContainer": false,
                "onClick": controller.AS_FlexContainer_af76450867de4196a1e2508dc8c1eae6,
                "skin": "slFbox",
                "top": 0,
                "width": "18%",
                "zIndex": 1
            }, {}, {});
            flxFeatureName.setDefaultUnit(kony.flex.DP);
            var lblFeatureName = new kony.ui.Label({
                "centerY": "50%",
                "id": "lblFeatureName",
                "isVisible": true,
                "left": "0px",
                "skin": "sknlblLato696c7312px",
                "text": "FEATURE NAME",
                "top": 0,
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {
                "hoverSkin": "sknlblLatoBold00000012px"
            });
            var fontIconSortFeature = new kony.ui.Label({
                "centerY": "50%",
                "id": "fontIconSortFeature",
                "isVisible": true,
                "left": "5dp",
                "skin": "sknIcon15px",
                "text": "",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {
                "hoverSkin": "sknIcon12pxBlackHover"
            });
            flxFeatureName.add(lblFeatureName, fontIconSortFeature);
            var flxFeatureCode = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "centerY": "50%",
                "clipBounds": true,
                "height": "100%",
                "id": "flxFeatureCode",
                "isVisible": true,
                "layoutType": kony.flex.FLOW_HORIZONTAL,
                "left": "28%",
                "isModalContainer": false,
                "onClick": controller.AS_FlexContainer_a5084e29a733477596892b84c89d16f9,
                "skin": "slFbox",
                "top": 0,
                "width": "20%",
                "zIndex": 1
            }, {}, {});
            flxFeatureCode.setDefaultUnit(kony.flex.DP);
            var lblHeaderFeatureCode = new kony.ui.Label({
                "centerY": "50%",
                "id": "lblHeaderFeatureCode",
                "isVisible": true,
                "left": "0%",
                "skin": "sknlblLato696c7312px",
                "text": "CODE",
                "top": 0,
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {
                "hoverSkin": "sknlblLatoBold00000012px"
            });
            var fontIconCodeSort = new kony.ui.Label({
                "centerY": "50%",
                "id": "fontIconCodeSort",
                "isVisible": true,
                "left": "5dp",
                "skin": "sknIcon15px",
                "text": "",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {
                "hoverSkin": "sknlblCursorFont"
            });
            flxFeatureCode.add(lblHeaderFeatureCode, fontIconCodeSort);
            var flxFeatureType = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "centerY": "50%",
                "clipBounds": true,
                "height": "100%",
                "id": "flxFeatureType",
                "isVisible": true,
                "layoutType": kony.flex.FLOW_HORIZONTAL,
                "left": "55%",
                "isModalContainer": false,
                "onClick": controller.AS_FlexContainer_fbe01558a0a64e0ca4625eeff7f71077,
                "skin": "slFbox",
                "top": 0,
                "width": "25%",
                "zIndex": 1
            }, {}, {});
            flxFeatureType.setDefaultUnit(kony.flex.DP);
            var lblFeatureType = new kony.ui.Label({
                "centerY": "50%",
                "id": "lblFeatureType",
                "isVisible": true,
                "left": "0%",
                "skin": "sknlblLato696c7312px",
                "text": "FEATURE TYPE",
                "top": 0,
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {
                "hoverSkin": "sknlblLatoBold00000012px"
            });
            var fontIconTypeFilter = new kony.ui.Label({
                "centerY": "50%",
                "id": "fontIconTypeFilter",
                "isVisible": true,
                "left": "10dp",
                "skin": "sknIcon15px",
                "text": "",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {
                "hoverSkin": "sknlblCursorFont"
            });
            flxFeatureType.add(lblFeatureType, fontIconTypeFilter);
            var flxFeatureStatus = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "centerY": "50%",
                "clipBounds": true,
                "height": "100%",
                "id": "flxFeatureStatus",
                "isVisible": true,
                "layoutType": kony.flex.FLOW_HORIZONTAL,
                "left": "82%",
                "isModalContainer": false,
                "onClick": controller.AS_FlexContainer_c62ddb6717304b7199d5a79964e58736,
                "skin": "sknFlxPointer",
                "top": "0dp",
                "width": "14.03%",
                "zIndex": 1
            }, {}, {});
            flxFeatureStatus.setDefaultUnit(kony.flex.DP);
            var lblFeatureStatus = new kony.ui.Label({
                "centerY": "50%",
                "id": "lblFeatureStatus",
                "isVisible": true,
                "left": "0px",
                "skin": "sknlblLato696c7312px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.Group.SERVICE\")",
                "top": 0,
                "width": "50px",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {
                "hoverSkin": "sknlblLatoBold00000012px"
            });
            var fontIconFilterStatus = new kony.ui.Label({
                "centerY": "50%",
                "id": "fontIconFilterStatus",
                "isVisible": true,
                "left": "5dp",
                "skin": "sknIcon15px",
                "text": "",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxFeatureStatus.add(lblFeatureStatus, fontIconFilterStatus);
            var lblHeaderSeperator = new kony.ui.Label({
                "bottom": "0px",
                "height": "1px",
                "id": "lblHeaderSeperator",
                "isVisible": true,
                "left": "0dp",
                "right": "0dp",
                "skin": "sknLblTableHeaderLine",
                "text": "-",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxHeaderFeatures.add(flxFeatureName, flxFeatureCode, flxFeatureType, flxFeatureStatus, lblHeaderSeperator);
            var flxSegmentFeatures = new kony.ui.FlexScrollContainer({
                "allowHorizontalBounce": false,
                "allowVerticalBounce": true,
                "bottom": "0dp",
                "bounces": true,
                "clipBounds": true,
                "enableScrolling": true,
                "horizontalScrollIndicator": true,
                "id": "flxSegmentFeatures",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0px",
                "pagingEnabled": false,
                "right": "0dp",
                "scrollDirection": kony.flex.SCROLL_VERTICAL,
                "top": "60px",
                "verticalScrollIndicator": true,
                "zIndex": 1
            }, {}, {});
            flxSegmentFeatures.setDefaultUnit(kony.flex.DP);
            var segFeatures = new kony.ui.SegmentedUI2({
                "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                "data": [{
                    "fontIconStatusImg": "",
                    "lblFeatureCode": "",
                    "lblFeatureName": "",
                    "lblFeatureStatus": "",
                    "lblFeatureType": "",
                    "lblIconImgOptions": "",
                    "lblSeperator": ""
                }],
                "groupCells": false,
                "id": "segFeatures",
                "isVisible": true,
                "left": "20dp",
                "needPageIndicator": true,
                "pageOffDotImage": "pageoffdot.png",
                "pageOnDotImage": "pageondot.png",
                "retainSelection": false,
                "right": "20dp",
                "rowFocusSkin": "seg2Focus",
                "rowSkin": "sknSegRowTransparent",
                "rowTemplate": "flxFeatures",
                "sectionHeaderSkin": "sliPhoneSegmentHeader",
                "selectionBehavior": constants.SEGUI_DEFAULT_BEHAVIOR,
                "separatorRequired": false,
                "showScrollbars": false,
                "top": "0dp",
                "viewType": constants.SEGUI_VIEW_TYPE_TABLEVIEW,
                "widgetDataMap": {
                    "flxFeatures": "flxFeatures",
                    "flxOptions": "flxOptions",
                    "flxStatus": "flxStatus",
                    "fontIconStatusImg": "fontIconStatusImg",
                    "lblFeatureCode": "lblFeatureCode",
                    "lblFeatureName": "lblFeatureName",
                    "lblFeatureStatus": "lblFeatureStatus",
                    "lblFeatureType": "lblFeatureType",
                    "lblIconImgOptions": "lblIconImgOptions",
                    "lblSeperator": "lblSeperator"
                },
                "zIndex": 1
            }, {
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var flxSeparator = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "1dp",
                "id": "flxSeparator",
                "isVisible": false,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "CopyslFbox0d26883c7010e40",
                "top": "35px",
                "width": "100%",
                "zIndex": 1
            }, {}, {});
            flxSeparator.setDefaultUnit(kony.flex.DP);
            flxSeparator.add();
            var rtxNoResultsFound = new kony.ui.RichText({
                "bottom": "150px",
                "centerX": "50%",
                "id": "rtxNoResultsFound",
                "isVisible": false,
                "skin": "sknRtxLato84939e12Px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmCSR.richtextNoResult\")",
                "top": "150px",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_CENTER,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxSegmentFeatures.add(segFeatures, flxSeparator, rtxNoResultsFound);
            var flxContextualMenu = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                "clipBounds": false,
                "id": "flxContextualMenu",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "810px",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "100px",
                "width": "130px",
                "zIndex": 1
            }, {}, {});
            flxContextualMenu.setDefaultUnit(kony.flex.DP);
            var contextualMenu = new com.adminConsole.common.contextualMenu1({
                "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                "id": "contextualMenu",
                "isVisible": true,
                "layoutType": kony.flex.FLOW_VERTICAL,
                "left": "0px",
                "masterType": constants.MASTER_TYPE_DEFAULT,
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0px",
                "width": "100%",
                "overrides": {
                    "flxOption1": {
                        "top": "10dp"
                    },
                    "flxOption2": {
                        "bottom": "10dp"
                    },
                    "imgDownArrow": {
                        "src": "downarrow_2x.png"
                    },
                    "imgOption1": {
                        "src": "imagedrag.png"
                    },
                    "imgOption2": {
                        "src": "edit2x.png"
                    },
                    "imgOption3": {
                        "src": "imagedrag.png"
                    },
                    "imgOption4": {
                        "src": "deactive_2x.png"
                    },
                    "imgUpArrow": {
                        "src": "uparrow_2x.png"
                    }
                }
            }, {
                "overrides": {}
            }, {
                "overrides": {}
            });
            flxContextualMenu.add(contextualMenu);
            flxHeaderAndSegmentWrapper.add(flxViewServicesHeader, flxViewServicesSegment, flxFeatureStatusFilter, flxFeatureTypeFilter, flxHeaderFeatures, flxSegmentFeatures, flxContextualMenu);
            flxSegmentWrapper.add(flxHeaderAndSegmentWrapper);
            flxViewServicesWrapper.add(flxSegmentWrapper);
            flxListingPage.add(flxMainSubHeader, flxViewServicesWrapper);
            flxScrollViewServices.add(flxListingPage);
            var flxViewServiceData = new kony.ui.FlexScrollContainer({
                "allowHorizontalBounce": false,
                "allowVerticalBounce": true,
                "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                "bottom": "0px",
                "bounces": true,
                "clipBounds": true,
                "enableScrolling": true,
                "horizontalScrollIndicator": true,
                "id": "flxViewServiceData",
                "isVisible": false,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0px",
                "pagingEnabled": false,
                "right": "0px",
                "scrollDirection": kony.flex.SCROLL_VERTICAL,
                "skin": "CopyslFSbox0gd265e68164b42",
                "top": "35dp",
                "verticalScrollIndicator": true,
                "zIndex": 1
            }, {}, {});
            flxViewServiceData.setDefaultUnit(kony.flex.DP);
            var flxLanguages = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                "clipBounds": true,
                "id": "flxLanguages",
                "isVisible": false,
                "layoutType": kony.flex.FREE_FORM,
                "left": "220dp",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "175px",
                "width": "170dp",
                "zIndex": 11
            }, {}, {});
            flxLanguages.setDefaultUnit(kony.flex.DP);
            var featuresLangList = new com.adminConsole.Products.productTypeFilterMenu({
                "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                "id": "featuresLangList",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "masterType": constants.MASTER_TYPE_DEFAULT,
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "5dp",
                "width": "100%",
                "zIndex": 5,
                "overrides": {
                    "flxCheckboxInner": {
                        "height": "120px"
                    },
                    "flxFilterSort": {
                        "isVisible": false
                    },
                    "imgUpArrow": {
                        "src": "uparrow_2x.png"
                    },
                    "lblSeperator1": {
                        "isVisible": false
                    },
                    "segStatusFilterDropdown": {
                        "data": [{
                            "imgCheckBox": "checkbox.png",
                            "lblDescription": "Active"
                        }, {
                            "imgCheckBox": "checkbox.png",
                            "lblDescription": "Archived"
                        }, {
                            "imgCheckBox": "checkbox.png",
                            "lblDescription": "Draft"
                        }]
                    }
                }
            }, {
                "overrides": {}
            }, {
                "overrides": {}
            });
            flxLanguages.add(featuresLangList);
            var flxFeatureDetailsContextualMenu = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                "clipBounds": true,
                "id": "flxFeatureDetailsContextualMenu",
                "isVisible": false,
                "layoutType": kony.flex.FREE_FORM,
                "left": "300dp",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "45dp",
                "width": "150dp",
                "zIndex": 2
            }, {}, {});
            flxFeatureDetailsContextualMenu.setDefaultUnit(kony.flex.DP);
            var contextualMenuFeatureDetails = new com.adminConsole.common.contextualMenu1({
                "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                "id": "contextualMenuFeatureDetails",
                "isVisible": true,
                "layoutType": kony.flex.FLOW_VERTICAL,
                "left": "0px",
                "masterType": constants.MASTER_TYPE_DEFAULT,
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0px",
                "width": "100%",
                "overrides": {
                    "btnLink1": {
                        "isVisible": false
                    },
                    "btnLink2": {
                        "isVisible": false
                    },
                    "flxDownArrowImage": {
                        "isVisible": false
                    },
                    "flxOption1": {
                        "isVisible": false
                    },
                    "flxOption3": {
                        "isVisible": false
                    },
                    "flxOptionsSeperator": {
                        "isVisible": false
                    },
                    "imgDownArrow": {
                        "src": "downarrow_2x.png"
                    },
                    "imgOption1": {
                        "src": "imagedrag.png"
                    },
                    "imgOption2": {
                        "src": "edit2x.png"
                    },
                    "imgOption3": {
                        "src": "imagedrag.png"
                    },
                    "imgOption4": {
                        "src": "deactive_2x.png"
                    },
                    "imgUpArrow": {
                        "src": "uparrow_2x.png"
                    },
                    "lblHeader": {
                        "isVisible": false
                    },
                    "lblIconOption2": {
                        "text": ""
                    }
                }
            }, {
                "overrides": {}
            }, {
                "overrides": {}
            });
            flxFeatureDetailsContextualMenu.add(contextualMenuFeatureDetails);
            var flxContainer = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                "clipBounds": true,
                "id": "flxContainer",
                "isVisible": true,
                "layoutType": kony.flex.FLOW_VERTICAL,
                "left": "0",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0",
                "width": "100%"
            }, {}, {});
            flxContainer.setDefaultUnit(kony.flex.DP);
            var flxDetailContainer = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                "clipBounds": true,
                "id": "flxDetailContainer",
                "isVisible": true,
                "layoutType": kony.flex.FLOW_VERTICAL,
                "left": "35dp",
                "isModalContainer": false,
                "right": "35dp",
                "skin": "sknFlxBgFFFFFFBorE4E6ECsz1pxR3px",
                "top": "0dp",
                "width": "95%"
            }, {}, {});
            flxDetailContainer.setDefaultUnit(kony.flex.DP);
            var flxViewServiceData1 = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                "clipBounds": true,
                "id": "flxViewServiceData1",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0",
                "width": "100%",
                "zIndex": 1
            }, {}, {});
            flxViewServiceData1.setDefaultUnit(kony.flex.DP);
            var flxViewServiceDataHeader = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "60dp",
                "id": "flxViewServiceDataHeader",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "20dp",
                "isModalContainer": false,
                "right": "20dp",
                "skin": "slFbox",
                "top": "0dp",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {}, {});
            flxViewServiceDataHeader.setDefaultUnit(kony.flex.DP);
            var lblFeatureDetailsName = new kony.ui.Label({
                "centerY": "50%",
                "id": "lblFeatureDetailsName",
                "isVisible": true,
                "left": "0dp",
                "skin": "sknLbl192B4518pxLatoReg",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmCustomerCare.lblServiceName\")",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var lblIconViewKey = new kony.ui.Label({
                "centerY": "52%",
                "height": "12dp",
                "id": "lblIconViewKey",
                "isVisible": true,
                "right": "90dp",
                "skin": "sknIcon13pxGreen",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmAlertsManagement.lblViewAlertStatusKey\")",
                "width": "12dp",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var lblFeatureDetailsStatusValue = new kony.ui.Label({
                "centerY": "50%",
                "id": "lblFeatureDetailsStatusValue",
                "isVisible": true,
                "right": "50dp",
                "skin": "sknLbl485C75LatoRegular13Px",
                "text": "Active",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var flxFeatureDetailsOptions = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "centerY": "50%",
                "clipBounds": true,
                "height": "25dp",
                "id": "flxFeatureDetailsOptions",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "isModalContainer": false,
                "right": "20dp",
                "skin": "sknFlxBgFFFFFFRounded",
                "width": "25dp"
            }, {}, {
                "hoverSkin": "sknflxffffffop100Border424242Radius100px"
            });
            flxFeatureDetailsOptions.setDefaultUnit(kony.flex.DP);
            var lblIconOptions = new kony.ui.Label({
                "centerX": "50%",
                "centerY": "50%",
                "id": "lblIconOptions",
                "isVisible": true,
                "skin": "sknFontIconOptionMenu",
                "text": "",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxFeatureDetailsOptions.add(lblIconOptions);
            var lblFeatureDetailsSeperator = new kony.ui.Label({
                "bottom": "0dp",
                "height": "1dp",
                "id": "lblFeatureDetailsSeperator",
                "isVisible": true,
                "left": "0dp",
                "right": "0dp",
                "skin": "sknlblSeperatorD7D9E0",
                "text": "Service Name",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxViewServiceDataHeader.add(lblFeatureDetailsName, lblIconViewKey, lblFeatureDetailsStatusValue, flxFeatureDetailsOptions, lblFeatureDetailsSeperator);
            var flxViewServiceDataContent = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                "bottom": "30dp",
                "clipBounds": true,
                "id": "flxViewServiceDataContent",
                "isVisible": true,
                "layoutType": kony.flex.FLOW_VERTICAL,
                "left": "20dp",
                "isModalContainer": false,
                "right": "20dp",
                "skin": "slFbox",
                "top": "60dp",
                "zIndex": 1
            }, {}, {});
            flxViewServiceDataContent.setDefaultUnit(kony.flex.DP);
            var flxFeatureDetailsWarning = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "bottom": "10dp",
                "clipBounds": true,
                "height": "40px",
                "id": "flxFeatureDetailsWarning",
                "isVisible": false,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0px",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "20px",
                "width": "100%",
                "zIndex": 1
            }, {}, {});
            flxFeatureDetailsWarning.setDefaultUnit(kony.flex.DP);
            var featureDetailsWarningMessage = new com.adminConsole.alertMang.alertMessage({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "height": "40px",
                "id": "featureDetailsWarningMessage",
                "isVisible": true,
                "layoutType": kony.flex.FLOW_HORIZONTAL,
                "left": "0dp",
                "masterType": constants.MASTER_TYPE_DEFAULT,
                "isModalContainer": false,
                "skin": "sknYellowBorder",
                "top": "0dp",
                "width": "100%",
                "overrides": {
                    "alertMessage": {
                        "height": "40px",
                        "top": "0dp"
                    },
                    "flxLeftImage": {
                        "width": "40px"
                    },
                    "lblData": {
                        "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmServiceManagement.FeatureInactiveWarningMessage\")"
                    }
                }
            }, {
                "overrides": {}
            }, {
                "overrides": {}
            });
            flxFeatureDetailsWarning.add(featureDetailsWarningMessage);
            var flxDetails = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                "clipBounds": true,
                "id": "flxDetails",
                "isVisible": true,
                "layoutType": kony.flex.FLOW_VERTICAL,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": 0,
                "width": "100%",
                "zIndex": 1
            }, {}, {});
            flxDetails.setDefaultUnit(kony.flex.DP);
            var flxDetailsContent = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                "clipBounds": true,
                "id": "flxDetailsContent",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "10dp",
                "width": "100%",
                "zIndex": 1
            }, {}, {});
            flxDetailsContent.setDefaultUnit(kony.flex.DP);
            var flxDetailsContent1 = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                "clipBounds": true,
                "id": "flxDetailsContent1",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0dp",
                "width": "100%",
                "zIndex": 1
            }, {}, {});
            flxDetailsContent1.setDefaultUnit(kony.flex.DP);
            var flxFeatureDetailsRow11Header = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "20dp",
                "id": "flxFeatureDetailsRow11Header",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "33%",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "5dp",
                "width": "30%",
                "zIndex": 1
            }, {}, {});
            flxFeatureDetailsRow11Header.setDefaultUnit(kony.flex.DP);
            var lblFeatureDetailsRow11Header = new kony.ui.Label({
                "height": "100%",
                "id": "lblFeatureDetailsRow11Header",
                "isVisible": true,
                "left": "0dp",
                "skin": "sknlblLato696c7312px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmServiceManagement.FeatureTypeUC\")",
                "top": "0dp",
                "width": "100%",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var fontIconFeatureDetailsRow11Header = new kony.ui.Label({
                "id": "fontIconFeatureDetailsRow11Header",
                "isVisible": true,
                "left": "92dp",
                "skin": "sknfontIcon192b4b14px",
                "text": "",
                "textStyle": {},
                "top": "2dp",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_TOP_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxFeatureDetailsRow11Header.add(lblFeatureDetailsRow11Header, fontIconFeatureDetailsRow11Header);
            var flxFeatureDetailsRow11Value = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                "clipBounds": true,
                "id": "flxFeatureDetailsRow11Value",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "33%",
                "isModalContainer": false,
                "skin": "sknServiceDetailsContent",
                "top": "30dp",
                "width": "28%",
                "zIndex": 1
            }, {}, {});
            flxFeatureDetailsRow11Value.setDefaultUnit(kony.flex.DP);
            var lblFeatureDetailsRow11Value = new kony.ui.Label({
                "id": "lblFeatureDetailsRow11Value",
                "isVisible": true,
                "left": "0dp",
                "skin": "sknLatoSemibold485c7313px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmLocations.lblDisplayName\")",
                "top": "0dp",
                "width": "100%",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxFeatureDetailsRow11Value.add(lblFeatureDetailsRow11Value);
            var flxFeatureDetailsRow12Header = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "20dp",
                "id": "flxFeatureDetailsRow12Header",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0%",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "5dp",
                "width": "33%",
                "zIndex": 1
            }, {}, {});
            flxFeatureDetailsRow12Header.setDefaultUnit(kony.flex.DP);
            var lblFeatureDetailsRow12Header = new kony.ui.Label({
                "height": "100%",
                "id": "lblFeatureDetailsRow12Header",
                "isVisible": true,
                "left": "0dp",
                "skin": "sknlblLato696c7312px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.feature.Featurecode\")",
                "top": "0dp",
                "width": "100%",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxFeatureDetailsRow12Header.add(lblFeatureDetailsRow12Header);
            var flxFeatureDetailsRow12Value = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                "clipBounds": true,
                "id": "flxFeatureDetailsRow12Value",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0%",
                "isModalContainer": false,
                "skin": "sknServiceDetailsContent",
                "top": "30dp",
                "width": "31%",
                "zIndex": 1
            }, {}, {});
            flxFeatureDetailsRow12Value.setDefaultUnit(kony.flex.DP);
            var lblFeatureDetailsRow12Value = new kony.ui.Label({
                "id": "lblFeatureDetailsRow12Value",
                "isVisible": true,
                "left": "0dp",
                "skin": "sknLatoSemibold485c7313px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmLocations.lblFullName\")",
                "top": "0dp",
                "width": "100%",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxFeatureDetailsRow12Value.add(lblFeatureDetailsRow12Value);
            var flxFeatureDetailsRow13Header = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "20dp",
                "id": "flxFeatureDetailsRow13Header",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "63%",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "5dp",
                "width": "30%",
                "zIndex": 1
            }, {}, {});
            flxFeatureDetailsRow13Header.setDefaultUnit(kony.flex.DP);
            var lblFeatureDetailsRow13Header = new kony.ui.Label({
                "height": "100%",
                "id": "lblFeatureDetailsRow13Header",
                "isVisible": true,
                "left": "0dp",
                "skin": "sknlblLato696c7312px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmServiceManagement.ServiceFeeUC\")",
                "top": "0dp",
                "width": "100%",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxFeatureDetailsRow13Header.add(lblFeatureDetailsRow13Header);
            var flxFeatureDetailsRow13Value = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                "clipBounds": true,
                "id": "flxFeatureDetailsRow13Value",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "63%",
                "isModalContainer": false,
                "skin": "sknServiceDetailsContent",
                "top": "30dp",
                "width": "30%",
                "zIndex": 1
            }, {}, {});
            flxFeatureDetailsRow13Value.setDefaultUnit(kony.flex.DP);
            var lblFeatureDetailsRow13Value = new kony.ui.Label({
                "id": "lblFeatureDetailsRow13Value",
                "isVisible": true,
                "left": "0dp",
                "skin": "sknLatoSemibold485c7313px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmServiceManagement.lblCategory\")",
                "top": "0dp",
                "width": "100%",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxFeatureDetailsRow13Value.add(lblFeatureDetailsRow13Value);
            flxDetailsContent1.add(flxFeatureDetailsRow11Header, flxFeatureDetailsRow11Value, flxFeatureDetailsRow12Header, flxFeatureDetailsRow12Value, flxFeatureDetailsRow13Header, flxFeatureDetailsRow13Value);
            flxDetailsContent.add(flxDetailsContent1);
            flxDetails.add(flxDetailsContent);
            var flxServiceDescription = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                "clipBounds": true,
                "id": "flxServiceDescription",
                "isVisible": true,
                "layoutType": kony.flex.FLOW_VERTICAL,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "sknFlxBgF8F9FABrE1E5EER3px",
                "top": "25dp",
                "width": "100%",
                "zIndex": 1
            }, {}, {});
            flxServiceDescription.setDefaultUnit(kony.flex.DP);
            var flxServiceDescriptionHeader = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "20dp",
                "id": "flxServiceDescriptionHeader",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "20dp",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "20dp",
                "width": "400dp",
                "zIndex": 1
            }, {}, {
                "hoverSkin": "sknCursor"
            });
            flxServiceDescriptionHeader.setDefaultUnit(kony.flex.DP);
            var lblServiceDescriptionHeader = new kony.ui.Label({
                "centerY": "50%",
                "id": "lblServiceDescriptionHeader",
                "isVisible": true,
                "left": "0dp",
                "skin": "sknLbl192b45LatoReg16px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmServiceManagement.DisplayContent\")",
                "width": "130dp",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var flxDropdownLanguages = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "centerY": "50%",
                "clipBounds": true,
                "height": "25dp",
                "id": "flxDropdownLanguages",
                "isVisible": true,
                "layoutType": kony.flex.FLOW_HORIZONTAL,
                "left": "130dp",
                "isModalContainer": false,
                "skin": "slFbox",
                "width": "180dp",
                "zIndex": 2
            }, {}, {});
            flxDropdownLanguages.setDefaultUnit(kony.flex.DP);
            var lblFeatureDetailsSelectedLang = new kony.ui.Label({
                "centerY": "50%",
                "id": "lblFeatureDetailsSelectedLang",
                "isVisible": true,
                "left": "0dp",
                "skin": "sknLbl485C75LatoRegular13Px",
                "text": "English (United States)",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_TOP_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var lblIconArrowDescription = new kony.ui.Label({
                "centerY": "50%",
                "id": "lblIconArrowDescription",
                "isVisible": true,
                "left": "10dp",
                "skin": "sknicon15pxBlack",
                "text": "",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxDropdownLanguages.add(lblFeatureDetailsSelectedLang, lblIconArrowDescription);
            flxServiceDescriptionHeader.add(lblServiceDescriptionHeader, flxDropdownLanguages);
            var flxServiceDescriptionContent = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                "bottom": "20dp",
                "clipBounds": true,
                "id": "flxServiceDescriptionContent",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "20dp",
                "isModalContainer": false,
                "skin": "sknServiceDetailsContent",
                "top": "15dp",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {}, {});
            flxServiceDescriptionContent.setDefaultUnit(kony.flex.DP);
            var lblFeatureDisplayNameHeader = new kony.ui.Label({
                "id": "lblFeatureDisplayNameHeader",
                "isVisible": true,
                "left": "0dp",
                "skin": "sknlblLato696c7312px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.permission.NAME\")",
                "top": "0dp",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var lblFeatureDisplayNameValue = new kony.ui.Label({
                "id": "lblFeatureDisplayNameValue",
                "isVisible": true,
                "left": "0dp",
                "skin": "sknLatoSemibold485c7313px",
                "text": "ACH colletion",
                "top": "20dp",
                "width": "28%",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_TOP_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var lblFeatureDisplayDescHeader = new kony.ui.Label({
                "id": "lblFeatureDisplayDescHeader",
                "isVisible": true,
                "left": "31%",
                "skin": "sknlblLato696c7312px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.permission.DESCRIPTION\")",
                "top": "0dp",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var lblFeatureDisplayDescValue = new kony.ui.Label({
                "id": "lblFeatureDisplayDescValue",
                "isVisible": true,
                "left": "31%",
                "skin": "sknLatoSemibold485c7313px",
                "text": "ACH colletion",
                "top": "20dp",
                "width": "60%",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_TOP_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxServiceDescriptionContent.add(lblFeatureDisplayNameHeader, lblFeatureDisplayNameValue, lblFeatureDisplayDescHeader, lblFeatureDisplayDescValue);
            var flxFeatureDetailsNoDisplayContent = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "bottom": "20dp",
                "clipBounds": true,
                "height": "50dp",
                "id": "flxFeatureDetailsNoDisplayContent",
                "isVisible": false,
                "layoutType": kony.flex.FREE_FORM,
                "left": "20dp",
                "isModalContainer": false,
                "skin": "sknServiceDetailsContent",
                "top": "15dp",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {}, {});
            flxFeatureDetailsNoDisplayContent.setDefaultUnit(kony.flex.DP);
            var lblFeatureDetailsNoDisplayContent = new kony.ui.Label({
                "centerX": "45%",
                "centerY": "50%",
                "id": "lblFeatureDetailsNoDisplayContent",
                "isVisible": true,
                "left": "0dp",
                "skin": "sknLblLato84939E12px",
                "text": "No display content avaialble.",
                "width": "60%",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_CENTER,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxFeatureDetailsNoDisplayContent.add(lblFeatureDetailsNoDisplayContent);
            flxServiceDescription.add(flxServiceDescriptionHeader, flxServiceDescriptionContent, flxFeatureDetailsNoDisplayContent);
            flxViewServiceDataContent.add(flxFeatureDetailsWarning, flxDetails, flxServiceDescription);
            flxViewServiceData1.add(flxViewServiceDataHeader, flxViewServiceDataContent);
            flxDetailContainer.add(flxViewServiceData1);
            var flxActionsContainer = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                "clipBounds": true,
                "id": "flxActionsContainer",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "35dp",
                "isModalContainer": false,
                "right": "35dp",
                "skin": "sknFlxBgFFFFFFBorE4E6ECsz1pxR3px",
                "top": "10dp",
                "width": "95%"
            }, {}, {});
            flxActionsContainer.setDefaultUnit(kony.flex.DP);
            var flxViewServiceData2 = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                "clipBounds": true,
                "id": "flxViewServiceData2",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "isModalContainer": false,
                "right": "0dp",
                "skin": "slFbox",
                "top": "0dp",
                "width": "100%",
                "zIndex": 1
            }, {}, {});
            flxViewServiceData2.setDefaultUnit(kony.flex.DP);
            var flxViewServiceData2Header = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "50dp",
                "id": "flxViewServiceData2Header",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "20dp",
                "isModalContainer": false,
                "right": "20dp",
                "skin": "slFbox",
                "top": "0dp",
                "zIndex": 1
            }, {}, {});
            flxViewServiceData2Header.setDefaultUnit(kony.flex.DP);
            var flxServiceData2Limits = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "100%",
                "id": "flxServiceData2Limits",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0dp",
                "width": "100dp",
                "zIndex": 1
            }, {}, {});
            flxServiceData2Limits.setDefaultUnit(kony.flex.DP);
            var lblServiceData2Limits = new kony.ui.Label({
                "centerY": "50%",
                "id": "lblServiceData2Limits",
                "isVisible": true,
                "left": 0,
                "skin": "sknLbl192b45LatoReg16px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmOutageMessage.Actions\")",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_CENTER,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxServiceData2Limits.add(lblServiceData2Limits);
            var flxSeparatorServiceData2Header = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "bottom": "0px",
                "clipBounds": true,
                "height": "1dp",
                "id": "flxSeparatorServiceData2Header",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "sknflxd5d9ddop100",
                "width": "100%",
                "zIndex": 1
            }, {}, {});
            flxSeparatorServiceData2Header.setDefaultUnit(kony.flex.DP);
            flxSeparatorServiceData2Header.add();
            flxViewServiceData2Header.add(flxServiceData2Limits, flxSeparatorServiceData2Header);
            var flxActionsList = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                "clipBounds": true,
                "id": "flxActionsList",
                "isVisible": true,
                "layoutType": kony.flex.FLOW_VERTICAL,
                "left": "20dp",
                "isModalContainer": false,
                "right": "20dp",
                "skin": "slFbox",
                "top": "50dp",
                "zIndex": 1
            }, {}, {});
            flxActionsList.setDefaultUnit(kony.flex.DP);
            var flxActionsHeader = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "50dp",
                "id": "flxActionsHeader",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0dp",
                "width": "100%",
                "zIndex": 1
            }, {}, {});
            flxActionsHeader.setDefaultUnit(kony.flex.DP);
            var flxActionNameHeader = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "100%",
                "id": "flxActionNameHeader",
                "isVisible": true,
                "layoutType": kony.flex.FLOW_HORIZONTAL,
                "left": "35dp",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0dp",
                "width": "120dp"
            }, {}, {
                "hoverSkin": "sknCursor"
            });
            flxActionNameHeader.setDefaultUnit(kony.flex.DP);
            var lblActionNameHeader = new kony.ui.Label({
                "centerY": "50%",
                "id": "lblActionNameHeader",
                "isVisible": true,
                "left": "0dp",
                "skin": "sknlblLato696c7312px",
                "text": "DISPLAY NAME",
                "width": "90dp",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var lblIconActionNameSort = new kony.ui.Label({
                "centerY": "50%",
                "id": "lblIconActionNameSort",
                "isVisible": true,
                "left": "0dp",
                "skin": "sknIcon485C7513px",
                "text": "",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxActionNameHeader.add(lblActionNameHeader, lblIconActionNameSort);
            var flxActionCodeHeader = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "100%",
                "id": "flxActionCodeHeader",
                "isVisible": true,
                "layoutType": kony.flex.FLOW_HORIZONTAL,
                "left": "20%",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0dp",
                "width": "120dp"
            }, {}, {
                "hoverSkin": "sknCursor"
            });
            flxActionCodeHeader.setDefaultUnit(kony.flex.DP);
            var lblActionCodeHeader = new kony.ui.Label({
                "centerY": "50%",
                "id": "lblActionCodeHeader",
                "isVisible": true,
                "left": "0dp",
                "skin": "sknlblLato696c7312px",
                "text": "ACTION CODE",
                "width": "90dp",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var lblIconActionCodeSort = new kony.ui.Label({
                "centerY": "50%",
                "id": "lblIconActionCodeSort",
                "isVisible": true,
                "left": "0dp",
                "skin": "sknIcon485C7513px",
                "text": "",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxActionCodeHeader.add(lblActionCodeHeader, lblIconActionCodeSort);
            var flxActionTypeHeader = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "100%",
                "id": "flxActionTypeHeader",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "39%",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0dp",
                "width": "15%"
            }, {}, {});
            flxActionTypeHeader.setDefaultUnit(kony.flex.DP);
            var lblActionTypeHeader = new kony.ui.Label({
                "centerY": "50%",
                "id": "lblActionTypeHeader",
                "isVisible": true,
                "left": "0dp",
                "skin": "sknlblLato696c7312px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmServiceManagement.ActionTypeUC\")",
                "width": "80dp",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var flxActionTypeFilterIcon = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "centerY": "50%",
                "clipBounds": true,
                "height": "20dp",
                "id": "flxActionTypeFilterIcon",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "80dp",
                "isModalContainer": false,
                "skin": "slFbox",
                "width": "20dp"
            }, {}, {
                "hoverSkin": "sknCursor"
            });
            flxActionTypeFilterIcon.setDefaultUnit(kony.flex.DP);
            var lblIconActionTypeFilter = new kony.ui.Label({
                "centerX": "50%",
                "centerY": "50%",
                "id": "lblIconActionTypeFilter",
                "isVisible": true,
                "skin": "sknIcon485C7513px",
                "text": "",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxActionTypeFilterIcon.add(lblIconActionTypeFilter);
            flxActionTypeHeader.add(lblActionTypeHeader, flxActionTypeFilterIcon);
            var flxActionCategoryHeader = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "100%",
                "id": "flxActionCategoryHeader",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "55%",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0dp",
                "width": "15%"
            }, {}, {});
            flxActionCategoryHeader.setDefaultUnit(kony.flex.DP);
            var lblActionCategoryHeader = new kony.ui.Label({
                "centerY": "50%",
                "id": "lblActionCategoryHeader",
                "isVisible": true,
                "left": "0dp",
                "skin": "sknlblLato696c7312px",
                "text": "ACTION CATEGORY",
                "width": "115dp",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var flxActionCategoryFilterIcon = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "centerY": "50%",
                "clipBounds": true,
                "height": "20dp",
                "id": "flxActionCategoryFilterIcon",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "115dp",
                "isModalContainer": false,
                "skin": "slFbox",
                "width": "20dp"
            }, {}, {
                "hoverSkin": "sknCursor"
            });
            flxActionCategoryFilterIcon.setDefaultUnit(kony.flex.DP);
            var lblIconActionCategoryFilter = new kony.ui.Label({
                "centerX": "50%",
                "centerY": "50%",
                "id": "lblIconActionCategoryFilter",
                "isVisible": true,
                "skin": "sknIcon485C7513px",
                "text": "",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxActionCategoryFilterIcon.add(lblIconActionCategoryFilter);
            flxActionCategoryHeader.add(lblActionCategoryHeader, flxActionCategoryFilterIcon);
            var flxActionLimitGroupHeader = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "100%",
                "id": "flxActionLimitGroupHeader",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "75%",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0dp",
                "width": "15%"
            }, {}, {});
            flxActionLimitGroupHeader.setDefaultUnit(kony.flex.DP);
            var lblActionLimitGroupHeader = new kony.ui.Label({
                "centerY": "50%",
                "id": "lblActionLimitGroupHeader",
                "isVisible": true,
                "left": "0dp",
                "skin": "sknlblLato696c7312px",
                "text": "LIMIT GROUP",
                "width": "85dp",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var flxActionLimitGroupFilterIcon = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "centerY": "50%",
                "clipBounds": true,
                "height": "20dp",
                "id": "flxActionLimitGroupFilterIcon",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "85dp",
                "isModalContainer": false,
                "skin": "slFbox",
                "width": "20dp"
            }, {}, {
                "hoverSkin": "sknCursor"
            });
            flxActionLimitGroupFilterIcon.setDefaultUnit(kony.flex.DP);
            var lblIconActionLimitGroupFilter = new kony.ui.Label({
                "centerX": "50%",
                "centerY": "50%",
                "id": "lblIconActionLimitGroupFilter",
                "isVisible": true,
                "skin": "sknIcon485C7513px",
                "text": "",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxActionLimitGroupFilterIcon.add(lblIconActionLimitGroupFilter);
            flxActionLimitGroupHeader.add(lblActionLimitGroupHeader, flxActionLimitGroupFilterIcon);
            var flxActionStatusHeader = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "100%",
                "id": "flxActionStatusHeader",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "89%",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0dp",
                "width": "15%"
            }, {}, {});
            flxActionStatusHeader.setDefaultUnit(kony.flex.DP);
            var lblActionStatusHeader = new kony.ui.Label({
                "centerY": "50%",
                "id": "lblActionStatusHeader",
                "isVisible": true,
                "left": "0dp",
                "skin": "sknlblLato696c7312px",
                "text": "STATUS",
                "width": "50dp",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var flxActionStatusFilterIcon = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "centerY": "50%",
                "clipBounds": true,
                "height": "20dp",
                "id": "flxActionStatusFilterIcon",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "50dp",
                "isModalContainer": false,
                "skin": "slFbox",
                "width": "20dp"
            }, {}, {
                "hoverSkin": "sknCursor"
            });
            flxActionStatusFilterIcon.setDefaultUnit(kony.flex.DP);
            var lblIconActionStatusFilter = new kony.ui.Label({
                "centerX": "50%",
                "centerY": "50%",
                "id": "lblIconActionStatusFilter",
                "isVisible": true,
                "skin": "sknIcon485C7513px",
                "text": "",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxActionStatusFilterIcon.add(lblIconActionStatusFilter);
            flxActionStatusHeader.add(lblActionStatusHeader, flxActionStatusFilterIcon);
            var flxSeparatorTransactionFee2 = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "bottom": "0px",
                "clipBounds": true,
                "height": "1dp",
                "id": "flxSeparatorTransactionFee2",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "sknFlxBg696C73Op100",
                "width": "100%",
                "zIndex": 1
            }, {}, {});
            flxSeparatorTransactionFee2.setDefaultUnit(kony.flex.DP);
            flxSeparatorTransactionFee2.add();
            flxActionsHeader.add(flxActionNameHeader, flxActionCodeHeader, flxActionTypeHeader, flxActionCategoryHeader, flxActionLimitGroupHeader, flxActionStatusHeader, flxSeparatorTransactionFee2);
            var flxActionsSegment = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                "clipBounds": true,
                "id": "flxActionsSegment",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0dp",
                "width": "100%",
                "zIndex": 1
            }, {}, {});
            flxActionsSegment.setDefaultUnit(kony.flex.DP);
            var segActionList = new kony.ui.SegmentedUI2({
                "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                "data": [{
                    "fontIconStatusImg": "",
                    "lblActionCategory": "Non-monetaory",
                    "lblActionCode": "Non-monetaory",
                    "lblActionLimitGroup": "",
                    "lblActionName": "",
                    "lblActionStatus": "Active",
                    "lblActionType": "",
                    "lblArrow": "",
                    "lblDescriptionHeader": "",
                    "lblDescriptionValue": "",
                    "lblIconImgOptions": "",
                    "lblSeperatorLine": ""
                }],
                "groupCells": false,
                "id": "segActionList",
                "isVisible": true,
                "left": "0dp",
                "needPageIndicator": true,
                "pageOffDotImage": "pageoffdot.png",
                "pageOnDotImage": "pageondot.png",
                "retainSelection": false,
                "rowFocusSkin": "seg2Focus",
                "rowSkin": "seg2Normal",
                "rowTemplate": "flxFeatureActionsList",
                "sectionHeaderSkin": "sliPhoneSegmentHeader",
                "selectionBehavior": constants.SEGUI_DEFAULT_BEHAVIOR,
                "separatorRequired": false,
                "showScrollbars": false,
                "top": "0dp",
                "viewType": constants.SEGUI_VIEW_TYPE_TABLEVIEW,
                "widgetDataMap": {
                    "flxActionDescription": "flxActionDescription",
                    "flxActionDetails": "flxActionDetails",
                    "flxArrow": "flxArrow",
                    "flxFeatureActionsList": "flxFeatureActionsList",
                    "flxOptions": "flxOptions",
                    "flxStatus": "flxStatus",
                    "fontIconStatusImg": "fontIconStatusImg",
                    "lblActionCategory": "lblActionCategory",
                    "lblActionCode": "lblActionCode",
                    "lblActionLimitGroup": "lblActionLimitGroup",
                    "lblActionName": "lblActionName",
                    "lblActionStatus": "lblActionStatus",
                    "lblActionType": "lblActionType",
                    "lblArrow": "lblArrow",
                    "lblDescriptionHeader": "lblDescriptionHeader",
                    "lblDescriptionValue": "lblDescriptionValue",
                    "lblIconImgOptions": "lblIconImgOptions",
                    "lblSeperatorLine": "lblSeperatorLine"
                },
                "width": "100%",
                "zIndex": 1
            }, {
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var lblActionsListNoResultsFound = new kony.ui.Label({
                "centerX": "50%",
                "height": "150dp",
                "id": "lblActionsListNoResultsFound",
                "isVisible": false,
                "skin": "sknLbl485c75FontSize12px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmUsersController.No_results_found\")",
                "top": "0dp",
                "width": "500dp",
                "zIndex": 5
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_CENTER,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxActionsSegment.add(segActionList, lblActionsListNoResultsFound);
            flxActionsList.add(flxActionsHeader, flxActionsSegment);
            var flxDetailsActionTypeFilter = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                "clipBounds": true,
                "id": "flxDetailsActionTypeFilter",
                "isVisible": false,
                "layoutType": kony.flex.FREE_FORM,
                "isModalContainer": false,
                "right": "150dp",
                "skin": "slFbox",
                "top": "85px",
                "width": "150dp",
                "zIndex": 11
            }, {}, {});
            flxDetailsActionTypeFilter.setDefaultUnit(kony.flex.DP);
            var actionTypeDetailsFilter = new com.adminConsole.common.statusFilterMenu({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "height": "90dp",
                "id": "actionTypeDetailsFilter",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "masterType": constants.MASTER_TYPE_DEFAULT,
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0dp",
                "width": "100%",
                "zIndex": 5,
                "overrides": {
                    "imgUpArrow": {
                        "src": "uparrow_2x.png"
                    },
                    "segStatusFilterDropdown": {
                        "top": "10dp"
                    },
                    "statusFilterMenu": {
                        "bottom": "viz.val_cleared",
                        "centerX": "viz.val_cleared",
                        "centerY": "viz.val_cleared",
                        "height": "90dp",
                        "left": "0dp",
                        "maxHeight": "viz.val_cleared",
                        "maxWidth": "viz.val_cleared",
                        "minHeight": "viz.val_cleared",
                        "minWidth": "viz.val_cleared",
                        "right": "viz.val_cleared",
                        "top": "0dp",
                        "width": "100%"
                    }
                }
            }, {
                "overrides": {}
            }, {
                "overrides": {}
            });
            flxDetailsActionTypeFilter.add(actionTypeDetailsFilter);
            var flxDetailsActionCategoryFilter = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                "clipBounds": true,
                "id": "flxDetailsActionCategoryFilter",
                "isVisible": false,
                "layoutType": kony.flex.FREE_FORM,
                "isModalContainer": false,
                "right": "315dp",
                "skin": "slFbox",
                "top": "85px",
                "width": "150dp",
                "zIndex": 11
            }, {}, {});
            flxDetailsActionCategoryFilter.setDefaultUnit(kony.flex.DP);
            var actionCategoryDetailsFilter = new com.adminConsole.common.statusFilterMenu({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "height": "90dp",
                "id": "actionCategoryDetailsFilter",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "masterType": constants.MASTER_TYPE_DEFAULT,
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0dp",
                "width": "100%",
                "zIndex": 5,
                "overrides": {
                    "imgUpArrow": {
                        "src": "uparrow_2x.png"
                    },
                    "segStatusFilterDropdown": {
                        "top": "10dp"
                    },
                    "statusFilterMenu": {
                        "bottom": "viz.val_cleared",
                        "centerX": "viz.val_cleared",
                        "centerY": "viz.val_cleared",
                        "height": "90dp",
                        "left": "0dp",
                        "maxHeight": "viz.val_cleared",
                        "maxWidth": "viz.val_cleared",
                        "minHeight": "viz.val_cleared",
                        "minWidth": "viz.val_cleared",
                        "right": "viz.val_cleared",
                        "top": "0dp",
                        "width": "100%"
                    }
                }
            }, {
                "overrides": {}
            }, {
                "overrides": {}
            });
            flxDetailsActionCategoryFilter.add(actionCategoryDetailsFilter);
            var flxContextualMenuAction = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                "clipBounds": false,
                "id": "flxContextualMenuAction",
                "isVisible": false,
                "layoutType": kony.flex.FREE_FORM,
                "left": "865px",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "140px",
                "width": "130px",
                "zIndex": 1
            }, {}, {});
            flxContextualMenuAction.setDefaultUnit(kony.flex.DP);
            var contextualMenuAction = new com.adminConsole.common.contextualMenu1({
                "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                "id": "contextualMenuAction",
                "isVisible": true,
                "layoutType": kony.flex.FLOW_VERTICAL,
                "left": "0px",
                "masterType": constants.MASTER_TYPE_DEFAULT,
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0px",
                "width": "100%",
                "overrides": {
                    "flxOption1": {
                        "top": "10dp"
                    },
                    "flxOption2": {
                        "bottom": "10dp"
                    },
                    "imgDownArrow": {
                        "src": "downarrow_2x.png"
                    },
                    "imgOption1": {
                        "src": "imagedrag.png"
                    },
                    "imgOption2": {
                        "src": "edit2x.png"
                    },
                    "imgOption3": {
                        "src": "imagedrag.png"
                    },
                    "imgOption4": {
                        "src": "deactive_2x.png"
                    },
                    "imgUpArrow": {
                        "src": "uparrow_2x.png"
                    }
                }
            }, {
                "overrides": {}
            }, {
                "overrides": {}
            });
            flxContextualMenuAction.add(contextualMenuAction);
            var flxDetailsActionStatusFilter = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                "clipBounds": true,
                "id": "flxDetailsActionStatusFilter",
                "isVisible": false,
                "layoutType": kony.flex.FREE_FORM,
                "isModalContainer": false,
                "right": "48dp",
                "skin": "slFbox",
                "top": "85px",
                "width": "150dp",
                "zIndex": 11
            }, {}, {});
            flxDetailsActionStatusFilter.setDefaultUnit(kony.flex.DP);
            var actionStatusDetailsFilter = new com.adminConsole.common.statusFilterMenu({
                "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                "id": "actionStatusDetailsFilter",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "masterType": constants.MASTER_TYPE_DEFAULT,
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "5dp",
                "width": "100%",
                "zIndex": 5,
                "overrides": {
                    "flxChechboxOuter": {
                        "zIndex": 1
                    },
                    "imgUpArrow": {
                        "src": "uparrow_2x.png"
                    },
                    "segStatusFilterDropdown": {
                        "left": "0dp",
                        "top": "5dp",
                        "zIndex": 1
                    }
                }
            }, {
                "overrides": {}
            }, {
                "overrides": {}
            });
            flxDetailsActionStatusFilter.add(actionStatusDetailsFilter);
            var flxDetailsActionLimitGroupFilter = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                "clipBounds": true,
                "id": "flxDetailsActionLimitGroupFilter",
                "isVisible": false,
                "layoutType": kony.flex.FREE_FORM,
                "isModalContainer": false,
                "right": "150dp",
                "skin": "slFbox",
                "top": "85px",
                "width": "150dp",
                "zIndex": 11
            }, {}, {});
            flxDetailsActionLimitGroupFilter.setDefaultUnit(kony.flex.DP);
            var actionLimitGroupDetailsFilter = new com.adminConsole.common.statusFilterMenu({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "height": "90dp",
                "id": "actionLimitGroupDetailsFilter",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "masterType": constants.MASTER_TYPE_DEFAULT,
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0dp",
                "width": "100%",
                "zIndex": 5,
                "overrides": {
                    "imgUpArrow": {
                        "src": "uparrow_2x.png"
                    },
                    "segStatusFilterDropdown": {
                        "top": "10dp"
                    },
                    "statusFilterMenu": {
                        "bottom": "viz.val_cleared",
                        "centerX": "viz.val_cleared",
                        "centerY": "viz.val_cleared",
                        "height": "90dp",
                        "left": "0dp",
                        "maxHeight": "viz.val_cleared",
                        "maxWidth": "viz.val_cleared",
                        "minHeight": "viz.val_cleared",
                        "minWidth": "viz.val_cleared",
                        "right": "viz.val_cleared",
                        "top": "0dp",
                        "width": "100%"
                    }
                }
            }, {
                "overrides": {}
            }, {
                "overrides": {}
            });
            flxDetailsActionLimitGroupFilter.add(actionLimitGroupDetailsFilter);
            flxViewServiceData2.add(flxViewServiceData2Header, flxActionsList, flxDetailsActionTypeFilter, flxDetailsActionCategoryFilter, flxContextualMenuAction, flxDetailsActionStatusFilter, flxDetailsActionLimitGroupFilter);
            flxActionsContainer.add(flxViewServiceData2);
            flxContainer.add(flxDetailContainer, flxActionsContainer);
            flxViewServiceData.add(flxLanguages, flxFeatureDetailsContextualMenu, flxContainer);
            var flxViewActionData = new kony.ui.FlexScrollContainer({
                "allowHorizontalBounce": false,
                "allowVerticalBounce": true,
                "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                "bottom": "0px",
                "bounces": true,
                "clipBounds": true,
                "enableScrolling": true,
                "horizontalScrollIndicator": true,
                "id": "flxViewActionData",
                "isVisible": false,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0px",
                "pagingEnabled": false,
                "right": "0px",
                "scrollDirection": kony.flex.SCROLL_VERTICAL,
                "skin": "CopyslFSbox0gd265e68164b42",
                "top": "35dp",
                "verticalScrollIndicator": true,
                "zIndex": 1
            }, {}, {});
            flxViewActionData.setDefaultUnit(kony.flex.DP);
            var flxActionDetailContainer = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                "clipBounds": true,
                "id": "flxActionDetailContainer",
                "isVisible": true,
                "layoutType": kony.flex.FLOW_VERTICAL,
                "left": "35dp",
                "isModalContainer": false,
                "right": "35dp",
                "skin": "sknFlxBgFFFFFFBorE4E6ECsz1pxR3px",
                "top": "0dp"
            }, {}, {});
            flxActionDetailContainer.setDefaultUnit(kony.flex.DP);
            var flxViewActionDetails = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                "clipBounds": true,
                "id": "flxViewActionDetails",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0",
                "width": "100%",
                "zIndex": 1
            }, {}, {});
            flxViewActionDetails.setDefaultUnit(kony.flex.DP);
            var flxViewActionDetailsHeader = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "60dp",
                "id": "flxViewActionDetailsHeader",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "20dp",
                "isModalContainer": false,
                "right": "20dp",
                "skin": "slFbox",
                "top": "0dp",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {}, {});
            flxViewActionDetailsHeader.setDefaultUnit(kony.flex.DP);
            var lblActionDetailsName = new kony.ui.Label({
                "centerY": "50%",
                "id": "lblActionDetailsName",
                "isVisible": true,
                "left": "0dp",
                "skin": "sknLbl192B4518pxLatoReg",
                "text": "Action Name",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var lblIconActionStatus = new kony.ui.Label({
                "centerY": "52%",
                "height": "12dp",
                "id": "lblIconActionStatus",
                "isVisible": true,
                "right": "90dp",
                "skin": "sknIcon13pxGreen",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmAlertsManagement.lblViewAlertStatusKey\")",
                "width": "12dp",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var lblActionStatusValue = new kony.ui.Label({
                "centerY": "50%",
                "id": "lblActionStatusValue",
                "isVisible": true,
                "right": "50dp",
                "skin": "sknLbl485C75LatoRegular13Px",
                "text": "Active",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var flxActionDetailsOptions = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "centerY": "50%",
                "clipBounds": true,
                "height": "25dp",
                "id": "flxActionDetailsOptions",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "isModalContainer": false,
                "right": "20dp",
                "skin": "sknFlxBgFFFFFFRounded",
                "width": "25dp"
            }, {}, {
                "hoverSkin": "sknflxffffffop100Border424242Radius100px"
            });
            flxActionDetailsOptions.setDefaultUnit(kony.flex.DP);
            var lblIconActionOptions = new kony.ui.Label({
                "centerX": "50%",
                "centerY": "50%",
                "id": "lblIconActionOptions",
                "isVisible": true,
                "skin": "sknFontIconOptionMenu",
                "text": "",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxActionDetailsOptions.add(lblIconActionOptions);
            var lblActionDetailsSeparator = new kony.ui.Label({
                "bottom": "0dp",
                "height": "1dp",
                "id": "lblActionDetailsSeparator",
                "isVisible": true,
                "left": "0dp",
                "right": "0dp",
                "skin": "sknlblSeperatorD7D9E0",
                "text": "Service Name",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxViewActionDetailsHeader.add(lblActionDetailsName, lblIconActionStatus, lblActionStatusValue, flxActionDetailsOptions, lblActionDetailsSeparator);
            var flxViewActionDetailsContent = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                "bottom": "30dp",
                "clipBounds": true,
                "id": "flxViewActionDetailsContent",
                "isVisible": true,
                "layoutType": kony.flex.FLOW_VERTICAL,
                "left": "20dp",
                "isModalContainer": false,
                "right": "20dp",
                "skin": "slFbox",
                "top": "60dp",
                "zIndex": 1
            }, {}, {});
            flxViewActionDetailsContent.setDefaultUnit(kony.flex.DP);
            var flxActionDetailsWarning = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "bottom": "10dp",
                "clipBounds": true,
                "height": "40px",
                "id": "flxActionDetailsWarning",
                "isVisible": false,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0px",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "20px",
                "width": "100%",
                "zIndex": 1
            }, {}, {});
            flxActionDetailsWarning.setDefaultUnit(kony.flex.DP);
            var actionDetailsWarningMessage = new com.adminConsole.alertMang.alertMessage({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "height": "40px",
                "id": "actionDetailsWarningMessage",
                "isVisible": true,
                "layoutType": kony.flex.FLOW_HORIZONTAL,
                "left": "0dp",
                "masterType": constants.MASTER_TYPE_DEFAULT,
                "isModalContainer": false,
                "skin": "sknYellowBorder",
                "top": "0dp",
                "width": "100%",
                "overrides": {
                    "alertMessage": {
                        "height": "40px",
                        "top": "0dp"
                    },
                    "flxLeftImage": {
                        "width": "40px"
                    },
                    "lblData": {
                        "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmServiceManagement.ActionInactiveWarningMessage\")"
                    }
                }
            }, {
                "overrides": {}
            }, {
                "overrides": {}
            });
            flxActionDetailsWarning.add(actionDetailsWarningMessage);
            var flxActionDetailsContent = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                "clipBounds": true,
                "id": "flxActionDetailsContent",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": 0,
                "width": "100%",
                "zIndex": 1
            }, {}, {});
            flxActionDetailsContent.setDefaultUnit(kony.flex.DP);
            var flxActionCode = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "60dp",
                "id": "flxActionCode",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0%",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "10dp",
                "width": "33%",
                "zIndex": 1
            }, {}, {});
            flxActionCode.setDefaultUnit(kony.flex.DP);
            var lblActionCode = new kony.ui.Label({
                "height": "20dp",
                "id": "lblActionCode",
                "isVisible": true,
                "left": "0dp",
                "skin": "sknlblLato696c7312px",
                "text": "ACTION CODE",
                "top": "5dp",
                "width": "100%",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var lblActionCodeValue = new kony.ui.Label({
                "id": "lblActionCodeValue",
                "isVisible": true,
                "left": "0dp",
                "skin": "sknLatoSemibold485c7313px",
                "text": "ADD_EXTERNAL",
                "top": "35dp",
                "width": "100%",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxActionCode.add(lblActionCode, lblActionCodeValue);
            var flxActionTypes = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "60dp",
                "id": "flxActionTypes",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "33%",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "10dp",
                "width": "33%",
                "zIndex": 1
            }, {}, {});
            flxActionTypes.setDefaultUnit(kony.flex.DP);
            var lblActionTypes = new kony.ui.Label({
                "height": "20dp",
                "id": "lblActionTypes",
                "isVisible": true,
                "left": "0dp",
                "skin": "sknlblLato696c7312px",
                "text": "ACTION TYPE",
                "top": "5dp",
                "width": "100%",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var lblActionTypesValue = new kony.ui.Label({
                "id": "lblActionTypesValue",
                "isVisible": true,
                "left": "0dp",
                "skin": "sknLatoSemibold485c7313px",
                "text": "Retail Banking",
                "top": "35dp",
                "width": "100%",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var fontIconActionTypes = new kony.ui.Label({
                "id": "fontIconActionTypes",
                "isVisible": true,
                "left": "87dp",
                "skin": "sknfontIcon192b4b14px",
                "text": "",
                "textStyle": {},
                "top": "10dp",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_TOP_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxActionTypes.add(lblActionTypes, lblActionTypesValue, fontIconActionTypes);
            var flxActionCategory = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "60dp",
                "id": "flxActionCategory",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "66%",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "10dp",
                "width": "33%",
                "zIndex": 1
            }, {}, {});
            flxActionCategory.setDefaultUnit(kony.flex.DP);
            var lblActionCategory = new kony.ui.Label({
                "height": "20dp",
                "id": "lblActionCategory",
                "isVisible": true,
                "left": "0dp",
                "skin": "sknlblLato696c7312px",
                "text": "ACTION CATEGORY",
                "top": "5dp",
                "width": "100%",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var lblActionCategoryValue = new kony.ui.Label({
                "id": "lblActionCategoryValue",
                "isVisible": true,
                "left": "0dp",
                "skin": "sknLatoSemibold485c7313px",
                "text": "Monetary",
                "top": "35dp",
                "width": "100%",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var fontIconActionCategory = new kony.ui.Label({
                "id": "fontIconActionCategory",
                "isVisible": true,
                "left": "120dp",
                "skin": "sknfontIcon192b4b14px",
                "text": "",
                "textStyle": {},
                "top": "10dp",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_TOP_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxActionCategory.add(lblActionCategory, lblActionCategoryValue, fontIconActionCategory);
            var flxActionLevel = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "60dp",
                "id": "flxActionLevel",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0%",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "90dp",
                "width": "33%",
                "zIndex": 1
            }, {}, {});
            flxActionLevel.setDefaultUnit(kony.flex.DP);
            var lblActionLevel = new kony.ui.Label({
                "height": "20dp",
                "id": "lblActionLevel",
                "isVisible": true,
                "left": "0dp",
                "skin": "sknlblLato696c7312px",
                "text": "ACTION LEVEL",
                "top": "5dp",
                "width": "100%",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var lblActionLevelValue = new kony.ui.Label({
                "id": "lblActionLevelValue",
                "isVisible": true,
                "left": "0dp",
                "skin": "sknLatoSemibold485c7313px",
                "text": "USER LEVEL",
                "top": "35dp",
                "width": "100%",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxActionLevel.add(lblActionLevel, lblActionLevelValue);
            var flxActionAccessLevel = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "60dp",
                "id": "flxActionAccessLevel",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "33%",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "90dp",
                "width": "33%",
                "zIndex": 1
            }, {}, {});
            flxActionAccessLevel.setDefaultUnit(kony.flex.DP);
            var lblActionAccessLevel = new kony.ui.Label({
                "height": "20dp",
                "id": "lblActionAccessLevel",
                "isVisible": true,
                "left": "0dp",
                "skin": "sknlblLato696c7312px",
                "text": "ACCESS LEVEL",
                "top": "5dp",
                "width": "100%",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var lblActionAccessLevelValue = new kony.ui.Label({
                "id": "lblActionAccessLevelValue",
                "isVisible": true,
                "left": "0dp",
                "skin": "sknLatoSemibold485c7313px",
                "text": "CREATE",
                "top": "35dp",
                "width": "100%",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxActionAccessLevel.add(lblActionAccessLevel, lblActionAccessLevelValue);
            var flxActionMFA = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "60dp",
                "id": "flxActionMFA",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "66%",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "90dp",
                "width": "33%",
                "zIndex": 1
            }, {}, {});
            flxActionMFA.setDefaultUnit(kony.flex.DP);
            var lblActionMFA = new kony.ui.Label({
                "height": "20dp",
                "id": "lblActionMFA",
                "isVisible": true,
                "left": "0dp",
                "skin": "sknlblLato696c7312px",
                "text": "MULTI FACTOR AUTHENTICATION",
                "top": "5dp",
                "width": "100%",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var lblActionMFAValue = new kony.ui.Label({
                "id": "lblActionMFAValue",
                "isVisible": true,
                "left": "0dp",
                "skin": "sknLatoSemibold485c7313px",
                "text": "N/A",
                "top": "35dp",
                "width": "100%",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxActionMFA.add(lblActionMFA, lblActionMFAValue);
            var flxActionTnC = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "60dp",
                "id": "flxActionTnC",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0%",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "170dp",
                "width": "33%",
                "zIndex": 1
            }, {}, {});
            flxActionTnC.setDefaultUnit(kony.flex.DP);
            var lblActionTnC = new kony.ui.Label({
                "height": "20dp",
                "id": "lblActionTnC",
                "isVisible": true,
                "left": "0dp",
                "skin": "sknlblLato696c7312px",
                "text": "TERMS AND CONDITION",
                "top": "5dp",
                "width": "100%",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var lblActionTnCValue = new kony.ui.Label({
                "id": "lblActionTnCValue",
                "isVisible": true,
                "left": "0dp",
                "skin": "sknLblLatoReg117eb013px",
                "text": "View",
                "top": "35dp",
                "width": "100%",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxActionTnC.add(lblActionTnC, lblActionTnCValue);
            var flxActionDependencies = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "60dp",
                "id": "flxActionDependencies",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "33%",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "170dp",
                "width": "33%",
                "zIndex": 1
            }, {}, {});
            flxActionDependencies.setDefaultUnit(kony.flex.DP);
            var lblActionDependencies = new kony.ui.Label({
                "height": "20dp",
                "id": "lblActionDependencies",
                "isVisible": true,
                "left": "0dp",
                "skin": "sknlblLato696c7312px",
                "text": "DEPENDENCIES",
                "top": "5dp",
                "width": "100%",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var lblActionDependenciesValue = new kony.ui.Label({
                "id": "lblActionDependenciesValue",
                "isVisible": true,
                "left": "0dp",
                "skin": "sknLblLatoReg117eb013px",
                "text": "View",
                "top": "35dp",
                "width": "100%",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var fontIconActionDependencies = new kony.ui.Label({
                "id": "fontIconActionDependencies",
                "isVisible": true,
                "left": "100dp",
                "skin": "sknfontIcon192b4b14px",
                "text": "",
                "textStyle": {},
                "top": "8dp",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_TOP_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxActionDependencies.add(lblActionDependencies, lblActionDependenciesValue, fontIconActionDependencies);
            flxActionDetailsContent.add(flxActionCode, flxActionTypes, flxActionCategory, flxActionLevel, flxActionAccessLevel, flxActionMFA, flxActionTnC, flxActionDependencies);
            var flxActionDisplayContent = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                "clipBounds": true,
                "id": "flxActionDisplayContent",
                "isVisible": true,
                "layoutType": kony.flex.FLOW_VERTICAL,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "sknFlxBgF8F9FABrE1E5EER3px",
                "top": "25dp",
                "width": "100%",
                "zIndex": 1
            }, {}, {});
            flxActionDisplayContent.setDefaultUnit(kony.flex.DP);
            var flxActionDescriptionHeader = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "20dp",
                "id": "flxActionDescriptionHeader",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "20dp",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "20dp",
                "width": "400dp",
                "zIndex": 1
            }, {}, {
                "hoverSkin": "sknCursor"
            });
            flxActionDescriptionHeader.setDefaultUnit(kony.flex.DP);
            var lblActionDescriptionHeader = new kony.ui.Label({
                "centerY": "50%",
                "id": "lblActionDescriptionHeader",
                "isVisible": true,
                "left": "0dp",
                "skin": "sknLbl192b45LatoReg16px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmServiceManagement.DisplayContent\")",
                "width": "130dp",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var flxActionDropdownLanguages = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "centerY": "50%",
                "clipBounds": true,
                "height": "25dp",
                "id": "flxActionDropdownLanguages",
                "isVisible": true,
                "layoutType": kony.flex.FLOW_HORIZONTAL,
                "left": "130dp",
                "isModalContainer": false,
                "skin": "slFbox",
                "width": "180dp",
                "zIndex": 2
            }, {}, {});
            flxActionDropdownLanguages.setDefaultUnit(kony.flex.DP);
            var lblActionDetailsSelectedLang = new kony.ui.Label({
                "centerY": "50%",
                "id": "lblActionDetailsSelectedLang",
                "isVisible": true,
                "left": "0dp",
                "skin": "sknLbl485C75LatoRegular13Px",
                "text": "English (United States)",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_TOP_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var lblIconArrowActionDescription = new kony.ui.Label({
                "centerY": "50%",
                "id": "lblIconArrowActionDescription",
                "isVisible": true,
                "left": "10dp",
                "skin": "sknicon15pxBlack",
                "text": "",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxActionDropdownLanguages.add(lblActionDetailsSelectedLang, lblIconArrowActionDescription);
            flxActionDescriptionHeader.add(lblActionDescriptionHeader, flxActionDropdownLanguages);
            var flxActionDescriptionContent = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                "bottom": "20dp",
                "clipBounds": true,
                "id": "flxActionDescriptionContent",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "20dp",
                "isModalContainer": false,
                "skin": "sknServiceDetailsContent",
                "top": "15dp",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {}, {});
            flxActionDescriptionContent.setDefaultUnit(kony.flex.DP);
            var lblActionDisplayNameHeader = new kony.ui.Label({
                "id": "lblActionDisplayNameHeader",
                "isVisible": true,
                "left": "0dp",
                "skin": "sknlblLato696c7312px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.permission.NAME\")",
                "top": "0dp",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var lblActionDisplayNameValue = new kony.ui.Label({
                "id": "lblActionDisplayNameValue",
                "isVisible": true,
                "left": "0dp",
                "skin": "sknLatoSemibold485c7313px",
                "text": "ACH colletion",
                "top": "20dp",
                "width": "28%",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_TOP_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var lblActionDisplayDescHeader = new kony.ui.Label({
                "id": "lblActionDisplayDescHeader",
                "isVisible": true,
                "left": "31%",
                "skin": "sknlblLato696c7312px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.permission.DESCRIPTION\")",
                "top": "0dp",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var lblActionDisplayDescValue = new kony.ui.Label({
                "id": "lblActionDisplayDescValue",
                "isVisible": true,
                "left": "31%",
                "skin": "sknLatoSemibold485c7313px",
                "text": "ACH colletion",
                "top": "20dp",
                "width": "60%",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_TOP_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxActionDescriptionContent.add(lblActionDisplayNameHeader, lblActionDisplayNameValue, lblActionDisplayDescHeader, lblActionDisplayDescValue);
            var flxActionDetailsNoDisplayContent = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "bottom": "20dp",
                "clipBounds": true,
                "height": "50dp",
                "id": "flxActionDetailsNoDisplayContent",
                "isVisible": false,
                "layoutType": kony.flex.FREE_FORM,
                "left": "20dp",
                "isModalContainer": false,
                "skin": "sknServiceDetailsContent",
                "top": "15dp",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {}, {});
            flxActionDetailsNoDisplayContent.setDefaultUnit(kony.flex.DP);
            var lblActionDetailsNoDisplayContent = new kony.ui.Label({
                "centerX": "45%",
                "centerY": "50%",
                "id": "lblActionDetailsNoDisplayContent",
                "isVisible": true,
                "left": "0dp",
                "skin": "sknLblLato84939E12px",
                "text": "No display content avaialble.",
                "width": "60%",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_CENTER,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxActionDetailsNoDisplayContent.add(lblActionDetailsNoDisplayContent);
            flxActionDisplayContent.add(flxActionDescriptionHeader, flxActionDescriptionContent, flxActionDetailsNoDisplayContent);
            flxViewActionDetailsContent.add(flxActionDetailsWarning, flxActionDetailsContent, flxActionDisplayContent);
            flxViewActionDetails.add(flxViewActionDetailsHeader, flxViewActionDetailsContent);
            var flxViewActionLimitDetails = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                "clipBounds": true,
                "id": "flxViewActionLimitDetails",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "isModalContainer": false,
                "right": "0dp",
                "skin": "slFbox",
                "top": "0dp",
                "width": "100%",
                "zIndex": 1
            }, {}, {});
            flxViewActionLimitDetails.setDefaultUnit(kony.flex.DP);
            var flxActionLimitGroup = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "60dp",
                "id": "flxActionLimitGroup",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "20dp",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0dp",
                "width": "33%",
                "zIndex": 1
            }, {}, {});
            flxActionLimitGroup.setDefaultUnit(kony.flex.DP);
            var lblActionLimitGroup = new kony.ui.Label({
                "height": "20dp",
                "id": "lblActionLimitGroup",
                "isVisible": true,
                "left": "0dp",
                "skin": "sknlblLato696c7312px",
                "text": "LIMIT GROUP",
                "top": "5dp",
                "width": "100%",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var lblActionLimitGroupValue = new kony.ui.Label({
                "id": "lblActionLimitGroupValue",
                "isVisible": true,
                "left": "0dp",
                "skin": "sknLatoSemibold485c7313px",
                "text": "Single Payment",
                "top": "35dp",
                "width": "100%",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var fontIconActionLimitGroup = new kony.ui.Label({
                "id": "fontIconActionLimitGroup",
                "isVisible": true,
                "left": "87dp",
                "skin": "sknfontIcon192b4b14px",
                "text": "",
                "textStyle": {},
                "top": "10dp",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_TOP_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxActionLimitGroup.add(lblActionLimitGroup, lblActionLimitGroupValue, fontIconActionLimitGroup);
            var flxLimit = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                "clipBounds": true,
                "id": "flxLimit",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "20dp",
                "isModalContainer": false,
                "right": "20dp",
                "skin": "sknFlxBgFFFFFFBorE4E6ECsz1pxR3px",
                "top": "70dp",
                "zIndex": 1
            }, {}, {});
            flxLimit.setDefaultUnit(kony.flex.DP);
            var flxLimitHeader = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "40dp",
                "id": "flxLimitHeader",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "sknf5f6f8border0px",
                "top": "0dp",
                "width": "100%",
                "zIndex": 1
            }, {}, {});
            flxLimitHeader.setDefaultUnit(kony.flex.DP);
            var lblLimitsHeading = new kony.ui.Label({
                "centerY": "50%",
                "id": "lblLimitsHeading",
                "isVisible": true,
                "left": "20dp",
                "skin": "sknLbl192b45LatoReg16px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmServiceManagement.Limits\")",
                "top": "0dp",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var lblSeparator1 = new kony.ui.Label({
                "height": "1px",
                "id": "lblSeparator1",
                "isVisible": true,
                "left": "0dp",
                "skin": "sknlblSeperator",
                "text": ".",
                "top": "39dp",
                "width": "100%",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_BOTTOM_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxLimitHeader.add(lblLimitsHeading, lblSeparator1);
            var flxLimitDetailsContainer = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                "bottom": "30dp",
                "clipBounds": true,
                "id": "flxLimitDetailsContainer",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "20dp",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "40dp",
                "width": "93%",
                "zIndex": 20
            }, {}, {});
            flxLimitDetailsContainer.setDefaultUnit(kony.flex.DP);
            var flxMinPerTxLimit = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "60dp",
                "id": "flxMinPerTxLimit",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "10dp",
                "width": "33%",
                "zIndex": 1
            }, {}, {});
            flxMinPerTxLimit.setDefaultUnit(kony.flex.DP);
            var lblMinPerTxLimit = new kony.ui.Label({
                "height": "20dp",
                "id": "lblMinPerTxLimit",
                "isVisible": true,
                "left": "0dp",
                "skin": "sknlblLato696c7312px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmServiceManagement.MinValuePerTransactionUC\")",
                "top": "5dp",
                "width": "100%",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var lblMinPerTxLimitValue = new kony.ui.Label({
                "id": "lblMinPerTxLimitValue",
                "isVisible": true,
                "left": "5%",
                "skin": "sknLatoSemibold485c7313px",
                "text": "10.00",
                "top": "35dp",
                "width": "95%",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var lblCurrency1 = new kony.ui.Label({
                "id": "lblCurrency1",
                "isVisible": true,
                "left": "0dp",
                "skin": "sknLatoSemibold485c7313px",
                "text": "$",
                "top": "35dp",
                "width": "5%",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var fontIconMinPerTxLimit = new kony.ui.Label({
                "id": "fontIconMinPerTxLimit",
                "isVisible": true,
                "left": "217dp",
                "skin": "sknfontIcon192b4b14px",
                "text": "",
                "textStyle": {},
                "top": "10dp",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_TOP_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxMinPerTxLimit.add(lblMinPerTxLimit, lblMinPerTxLimitValue, lblCurrency1, fontIconMinPerTxLimit);
            var flxMaxTxLimit = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "60dp",
                "id": "flxMaxTxLimit",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "33%",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "10dp",
                "width": "33%",
                "zIndex": 1
            }, {}, {});
            flxMaxTxLimit.setDefaultUnit(kony.flex.DP);
            var lblMaxTxLimit = new kony.ui.Label({
                "height": "20dp",
                "id": "lblMaxTxLimit",
                "isVisible": true,
                "left": "0dp",
                "skin": "sknlblLato696c7312px",
                "text": "MAXIMUM TRANSACTION LIMIT",
                "top": "5dp",
                "width": "100%",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var lblMaxTxLimitValue = new kony.ui.Label({
                "id": "lblMaxTxLimitValue",
                "isVisible": true,
                "left": "5%",
                "skin": "sknLatoSemibold485c7313px",
                "text": "500.00",
                "top": "35dp",
                "width": "95%",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var fontIconMaxTxLimit = new kony.ui.Label({
                "id": "fontIconMaxTxLimit",
                "isVisible": true,
                "left": "190dp",
                "skin": "sknfontIcon192b4b14px",
                "text": "",
                "textStyle": {},
                "top": "10dp",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_TOP_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var lblCurrency2 = new kony.ui.Label({
                "id": "lblCurrency2",
                "isVisible": true,
                "left": "0dp",
                "skin": "sknLatoSemibold485c7313px",
                "text": "$",
                "top": "35dp",
                "width": "5%",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxMaxTxLimit.add(lblMaxTxLimit, lblMaxTxLimitValue, fontIconMaxTxLimit, lblCurrency2);
            var flxMaxDailyLimit = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "60dp",
                "id": "flxMaxDailyLimit",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "66%",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "10dp",
                "width": "33%",
                "zIndex": 1
            }, {}, {});
            flxMaxDailyLimit.setDefaultUnit(kony.flex.DP);
            var lblMaxDailyLimit = new kony.ui.Label({
                "height": "20dp",
                "id": "lblMaxDailyLimit",
                "isVisible": true,
                "left": "0dp",
                "skin": "sknlblLato696c7312px",
                "text": "MAXIMUM DAILY LIMIT",
                "top": "5dp",
                "width": "100%",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var lblMaxDailyLimitValue = new kony.ui.Label({
                "id": "lblMaxDailyLimitValue",
                "isVisible": true,
                "left": "5%",
                "skin": "sknLatoSemibold485c7313px",
                "text": "1000.00",
                "top": "35dp",
                "width": "95%",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var fontIconMaxDailyLimit = new kony.ui.Label({
                "id": "fontIconMaxDailyLimit",
                "isVisible": true,
                "left": "140dp",
                "skin": "sknfontIcon192b4b14px",
                "text": "",
                "textStyle": {},
                "top": "10dp",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_TOP_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var lblCurrency3 = new kony.ui.Label({
                "id": "lblCurrency3",
                "isVisible": true,
                "left": "0dp",
                "skin": "sknLatoSemibold485c7313px",
                "text": "$",
                "top": "35dp",
                "width": "5%",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxMaxDailyLimit.add(lblMaxDailyLimit, lblMaxDailyLimitValue, fontIconMaxDailyLimit, lblCurrency3);
            var flxMaxWeeklyLimit = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "60dp",
                "id": "flxMaxWeeklyLimit",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "90dp",
                "width": "33%",
                "zIndex": 1
            }, {}, {});
            flxMaxWeeklyLimit.setDefaultUnit(kony.flex.DP);
            var lblMaxWeeklyLimit = new kony.ui.Label({
                "height": "20dp",
                "id": "lblMaxWeeklyLimit",
                "isVisible": true,
                "left": "0dp",
                "skin": "sknlblLato696c7312px",
                "text": "MAXIMUM WEEKLY LIMIT",
                "top": "5dp",
                "width": "100%",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var lblMaxWeeklyLimitValue = new kony.ui.Label({
                "id": "lblMaxWeeklyLimitValue",
                "isVisible": true,
                "left": "5%",
                "skin": "sknLatoSemibold485c7313px",
                "text": "5000.00",
                "top": "35dp",
                "width": "95%",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var fontIconMaxWeeklyLimit = new kony.ui.Label({
                "id": "fontIconMaxWeeklyLimit",
                "isVisible": true,
                "left": "157dp",
                "skin": "sknfontIcon192b4b14px",
                "text": "",
                "textStyle": {},
                "top": "10dp",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_TOP_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var lblCurrency4 = new kony.ui.Label({
                "id": "lblCurrency4",
                "isVisible": true,
                "left": "0dp",
                "skin": "sknLatoSemibold485c7313px",
                "text": "$",
                "top": "35dp",
                "width": "5%",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxMaxWeeklyLimit.add(lblMaxWeeklyLimit, lblMaxWeeklyLimitValue, fontIconMaxWeeklyLimit, lblCurrency4);
            flxLimitDetailsContainer.add(flxMinPerTxLimit, flxMaxTxLimit, flxMaxDailyLimit, flxMaxWeeklyLimit);
            flxLimit.add(flxLimitHeader, flxLimitDetailsContainer);
            flxViewActionLimitDetails.add(flxActionLimitGroup, flxLimit);
            flxActionDetailContainer.add(flxViewActionDetails, flxViewActionLimitDetails);
            var flxActionLanguages = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                "clipBounds": true,
                "id": "flxActionLanguages",
                "isVisible": false,
                "layoutType": kony.flex.FREE_FORM,
                "left": "220dp",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "175px",
                "width": "170dp",
                "zIndex": 11
            }, {}, {});
            flxActionLanguages.setDefaultUnit(kony.flex.DP);
            var actionLangList = new com.adminConsole.Products.productTypeFilterMenu({
                "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                "id": "actionLangList",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "masterType": constants.MASTER_TYPE_DEFAULT,
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "5dp",
                "width": "100%",
                "zIndex": 5,
                "overrides": {
                    "flxCheckboxInner": {
                        "height": "120px"
                    },
                    "flxFilterSort": {
                        "isVisible": false
                    },
                    "flxSearchContainer": {
                        "isVisible": false
                    },
                    "imgUpArrow": {
                        "src": "uparrow_2x.png"
                    },
                    "lblSeperator1": {
                        "isVisible": false
                    },
                    "segStatusFilterDropdown": {
                        "data": [{
                            "imgCheckBox": "checkbox.png",
                            "lblDescription": "Active"
                        }, {
                            "imgCheckBox": "checkbox.png",
                            "lblDescription": "Archived"
                        }, {
                            "imgCheckBox": "checkbox.png",
                            "lblDescription": "Draft"
                        }]
                    }
                }
            }, {
                "overrides": {}
            }, {
                "overrides": {}
            });
            flxActionLanguages.add(actionLangList);
            var flxActionDetailsContextualMenu = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                "clipBounds": true,
                "id": "flxActionDetailsContextualMenu",
                "isVisible": false,
                "layoutType": kony.flex.FREE_FORM,
                "left": "300dp",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "45dp",
                "width": "150dp",
                "zIndex": 1
            }, {}, {});
            flxActionDetailsContextualMenu.setDefaultUnit(kony.flex.DP);
            var contextualMenuActionDetails = new com.adminConsole.common.contextualMenu1({
                "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                "id": "contextualMenuActionDetails",
                "isVisible": true,
                "layoutType": kony.flex.FLOW_VERTICAL,
                "left": "0px",
                "masterType": constants.MASTER_TYPE_DEFAULT,
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0px",
                "width": "100%",
                "overrides": {
                    "btnLink1": {
                        "isVisible": false
                    },
                    "btnLink2": {
                        "isVisible": false
                    },
                    "flxDownArrowImage": {
                        "isVisible": false
                    },
                    "flxOption1": {
                        "isVisible": false
                    },
                    "flxOption3": {
                        "isVisible": false
                    },
                    "flxOptionsSeperator": {
                        "isVisible": false
                    },
                    "imgDownArrow": {
                        "src": "downarrow_2x.png"
                    },
                    "imgOption1": {
                        "src": "imagedrag.png"
                    },
                    "imgOption2": {
                        "src": "edit2x.png"
                    },
                    "imgOption3": {
                        "src": "imagedrag.png"
                    },
                    "imgOption4": {
                        "src": "deactive_2x.png"
                    },
                    "imgUpArrow": {
                        "src": "uparrow_2x.png"
                    },
                    "lblHeader": {
                        "isVisible": false
                    },
                    "lblIconOption2": {
                        "text": ""
                    }
                }
            }, {
                "overrides": {}
            }, {
                "overrides": {}
            });
            flxActionDetailsContextualMenu.add(contextualMenuActionDetails);
            flxViewActionData.add(flxActionDetailContainer, flxActionLanguages, flxActionDetailsContextualMenu);
            var flxViewLimitsGroup = new kony.ui.FlexScrollContainer({
                "allowHorizontalBounce": false,
                "allowVerticalBounce": true,
                "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                "bottom": "0px",
                "bounces": true,
                "clipBounds": true,
                "enableScrolling": true,
                "horizontalScrollIndicator": true,
                "id": "flxViewLimitsGroup",
                "isVisible": false,
                "layoutType": kony.flex.FREE_FORM,
                "left": "35dp",
                "pagingEnabled": false,
                "right": "35dp",
                "scrollDirection": kony.flex.SCROLL_VERTICAL,
                "skin": "sknFlxBgFFFFFFBore4e6ec3Px",
                "top": "35dp",
                "verticalScrollIndicator": true,
                "zIndex": 1
            }, {}, {});
            flxViewLimitsGroup.setDefaultUnit(kony.flex.DP);
            var flxHeaderLimitsGroup = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "60px",
                "id": "flxHeaderLimitsGroup",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "20dp",
                "isModalContainer": false,
                "right": "20dp",
                "skin": "slFbox",
                "top": "0dp"
            }, {}, {});
            flxHeaderLimitsGroup.setDefaultUnit(kony.flex.DP);
            var flxLimitGroupNameHeader = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "centerY": "50%",
                "clipBounds": true,
                "height": "100%",
                "id": "flxLimitGroupNameHeader",
                "isVisible": true,
                "layoutType": kony.flex.FLOW_HORIZONTAL,
                "left": "5%",
                "isModalContainer": false,
                "onClick": controller.AS_FlexContainer_af76450867de4196a1e2508dc8c1eae6,
                "skin": "slFbox",
                "top": 0,
                "width": "18%",
                "zIndex": 1
            }, {}, {});
            flxLimitGroupNameHeader.setDefaultUnit(kony.flex.DP);
            var lblLimitGroupName = new kony.ui.Label({
                "centerY": "50%",
                "id": "lblLimitGroupName",
                "isVisible": true,
                "left": "0px",
                "skin": "sknlblLato696c7312px",
                "text": "LIMIT GROUP NAME",
                "top": 0,
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {
                "hoverSkin": "sknlblLatoBold00000012px"
            });
            var fontIconSortLimitGroup = new kony.ui.Label({
                "centerY": "50%",
                "id": "fontIconSortLimitGroup",
                "isVisible": true,
                "left": "5dp",
                "skin": "sknIcon12pxBlack",
                "text": "",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {
                "hoverSkin": "sknIcon12pxBlackHover"
            });
            flxLimitGroupNameHeader.add(lblLimitGroupName, fontIconSortLimitGroup);
            var lblLimitGroupCodeHeader = new kony.ui.Label({
                "centerY": "50%",
                "id": "lblLimitGroupCodeHeader",
                "isVisible": true,
                "left": "40%",
                "skin": "sknlblLato696c7312px",
                "text": "LIMIT GROUP CODE",
                "top": 0,
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {
                "hoverSkin": "sknlblLatoBold00000012px"
            });
            var lblLimitGroupHeaderSeperator = new kony.ui.Label({
                "bottom": "0px",
                "height": "1px",
                "id": "lblLimitGroupHeaderSeperator",
                "isVisible": true,
                "left": "0dp",
                "right": "0dp",
                "skin": "sknLblTableHeaderLine",
                "text": "-",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxHeaderLimitsGroup.add(flxLimitGroupNameHeader, lblLimitGroupCodeHeader, lblLimitGroupHeaderSeperator);
            var segLimitGroup = new kony.ui.SegmentedUI2({
                "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                "data": [{
                    "lblArrow": "",
                    "lblDescriptionHeader": "DESCRIPTION",
                    "lblDescriptionValue": "Label",
                    "lblIconImgOptions": "kony.i18n.getLocalizedString(\"i18n.userwidgetmodel.lblIconOption1\")",
                    "lblLimitGroupCode": "LIMIT_GROUP_CODE",
                    "lblLimitGroupName": "Label",
                    "lblSeperatorLine": "Label"
                }],
                "groupCells": false,
                "id": "segLimitGroup",
                "isVisible": true,
                "left": "20dp",
                "needPageIndicator": true,
                "pageOffDotImage": "pageoffdot.png",
                "pageOnDotImage": "pageondot.png",
                "retainSelection": false,
                "right": "20dp",
                "rowFocusSkin": "seg2Focus",
                "rowSkin": "sknSegRowTransparent",
                "rowTemplate": "flxLimitGroup",
                "sectionHeaderSkin": "sliPhoneSegmentHeader",
                "selectionBehavior": constants.SEGUI_DEFAULT_BEHAVIOR,
                "separatorRequired": false,
                "showScrollbars": false,
                "top": "60dp",
                "viewType": constants.SEGUI_VIEW_TYPE_TABLEVIEW,
                "widgetDataMap": {
                    "flxArrow": "flxArrow",
                    "flxDescription": "flxDescription",
                    "flxLimitGroup": "flxLimitGroup",
                    "flxLimitGroupDetails": "flxLimitGroupDetails",
                    "flxOptions": "flxOptions",
                    "lblArrow": "lblArrow",
                    "lblDescriptionHeader": "lblDescriptionHeader",
                    "lblDescriptionValue": "lblDescriptionValue",
                    "lblIconImgOptions": "lblIconImgOptions",
                    "lblLimitGroupCode": "lblLimitGroupCode",
                    "lblLimitGroupName": "lblLimitGroupName",
                    "lblSeperatorLine": "lblSeperatorLine"
                },
                "zIndex": 1
            }, {
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxViewLimitsGroup.add(flxHeaderLimitsGroup, segLimitGroup);
            flxMainContent.add(flxAddService, flxScrollViewServices, flxViewServiceData, flxViewActionData, flxViewLimitsGroup);
            flxRightPanel.add(flxMainHeader, flxHeaderDropdown, flxMainContent);
            var flxToastMessage = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "bottom": "0px",
                "clipBounds": true,
                "height": "70px",
                "id": "flxToastMessage",
                "isVisible": false,
                "layoutType": kony.flex.FREE_FORM,
                "left": "305px",
                "isModalContainer": false,
                "skin": "slFbox",
                "width": "78%",
                "zIndex": 1
            }, {}, {});
            flxToastMessage.setDefaultUnit(kony.flex.DP);
            var toastMessage = new com.adminConsole.common.toastMessage({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "id": "toastMessage",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "masterType": constants.MASTER_TYPE_DEFAULT,
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0dp",
                "overrides": {}
            }, {
                "overrides": {}
            }, {
                "overrides": {}
            });
            flxToastMessage.add(toastMessage);
            var flxDeactivateServiceManagement = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "100%",
                "id": "flxDeactivateServiceManagement",
                "isVisible": false,
                "layoutType": kony.flex.FREE_FORM,
                "masterType": constants.MASTER_TYPE_USERWIDGET,
                "isModalContainer": false,
                "skin": "slFbox",
                "width": "100%",
                "zIndex": 10
            }, {}, {});
            flxDeactivateServiceManagement.setDefaultUnit(kony.flex.DP);
            var popUpDeactivate = new com.adminConsole.common.popUp1({
                "height": "100%",
                "id": "popUpDeactivate",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "masterType": constants.MASTER_TYPE_DEFAULT,
                "isModalContainer": false,
                "skin": "sknflxBg000000Op50",
                "top": "0dp",
                "width": "100%",
                "zIndex": 1,
                "overrides": {
                    "btnPopUpCancel": {
                        "i18n_text": "kony.i18n.getLocalizedString(\"i18n.PopUp.NoLeaveAsIS\")",
                        "minWidth": "81px",
                        "right": "20px",
                        "width": kony.flex.USE_PREFFERED_SIZE
                    },
                    "btnPopUpDelete": {
                        "i18n_text": "kony.i18n.getLocalizedString(\"i18n.PopUp.YesDeactivate\")",
                        "minWidth": "83px",
                        "width": kony.flex.USE_PREFFERED_SIZE
                    },
                    "flxPopUpClose": {
                        "top": "10dp"
                    },
                    "lblPopUpMainMessage": {
                        "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmServiceManagementController.Deactivate_Service\")",
                        "top": "-10px"
                    },
                    "rtxPopUpDisclaimer": {
                        "bottom": "30dp",
                        "right": "viz.val_cleared",
                        "text": "Lorem ipsum dolor sit amet, consectetur adipiscing elit. ...Even though using \"lorem ipsum\" often arouses curiosity because of its resemblance to classical Latin.<br><br>Lorem ipsum dolor sit amet, consectetur adipiscing elit. ...Even though using \"lorem ipsum\" often arouses curiosity because of its resemblance to \n",
                        "width": "93%"
                    }
                }
            }, {
                "overrides": {}
            }, {
                "overrides": {}
            });
            flxDeactivateServiceManagement.add(popUpDeactivate);
            var flxLoading = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "100%",
                "id": "flxLoading",
                "isVisible": false,
                "layoutType": kony.flex.FREE_FORM,
                "left": "305px",
                "isModalContainer": false,
                "right": "0px",
                "skin": "sknLoadingBlur",
                "top": "0dp",
                "zIndex": 15
            }, {}, {});
            flxLoading.setDefaultUnit(kony.flex.DP);
            var flxImageContainer = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "centerX": "50%",
                "centerY": "50%",
                "clipBounds": true,
                "height": "80px",
                "id": "flxImageContainer",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "isModalContainer": false,
                "skin": "slFbox",
                "width": "80px",
                "zIndex": 1
            }, {}, {});
            flxImageContainer.setDefaultUnit(kony.flex.DP);
            var imgLoading = new kony.ui.Image2({
                "centerX": "50%",
                "centerY": "50%",
                "height": "60px",
                "id": "imgLoading",
                "isVisible": true,
                "skin": "slImage",
                "src": "loadingscreenimage.gif",
                "width": "60px",
                "zIndex": 1
            }, {
                "imageScaleMode": constants.IMAGE_SCALE_MODE_MAINTAIN_ASPECT_RATIO,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxImageContainer.add(imgLoading);
            flxLoading.add(flxImageContainer);
            var flxViewEditLimitsPopup = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "100%",
                "id": "flxViewEditLimitsPopup",
                "isVisible": false,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "sknFocus50",
                "top": "0dp",
                "width": "100%",
                "zIndex": 5
            }, {}, {});
            flxViewEditLimitsPopup.setDefaultUnit(kony.flex.DP);
            var flxViewEditLimitCont = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                "centerX": "50%",
                "centerY": "50%",
                "clipBounds": true,
                "id": "flxViewEditLimitCont",
                "isVisible": true,
                "layoutType": kony.flex.FLOW_VERTICAL,
                "left": "27%",
                "isModalContainer": false,
                "skin": "slFbox0j9f841cc563e4e",
                "width": "650px",
                "zIndex": 1
            }, {}, {});
            flxViewEditLimitCont.setDefaultUnit(kony.flex.DP);
            var flxLimitsTopColor = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "10px",
                "id": "flxLimitsTopColor",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0px",
                "isModalContainer": false,
                "skin": "sknFlxInfoToastBg357C9ENoRadius",
                "top": "0dp",
                "width": "100%",
                "zIndex": 1
            }, {}, {});
            flxLimitsTopColor.setDefaultUnit(kony.flex.DP);
            flxLimitsTopColor.add();
            var flxPopupHeader = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                "clipBounds": true,
                "id": "flxPopupHeader",
                "isVisible": true,
                "layoutType": kony.flex.FLOW_VERTICAL,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0px",
                "width": "100%",
                "zIndex": 1
            }, {}, {});
            flxPopupHeader.setDefaultUnit(kony.flex.DP);
            var flxCloseLimits = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "25px",
                "id": "flxCloseLimits",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "isModalContainer": false,
                "right": "20dp",
                "skin": "sknCursor",
                "top": "5dp",
                "width": "25px",
                "zIndex": 20
            }, {}, {
                "hoverSkin": "sknCursor"
            });
            flxCloseLimits.setDefaultUnit(kony.flex.DP);
            var fontIconImgCLose = new kony.ui.Label({
                "centerX": "51%",
                "centerY": "50.00%",
                "height": "15dp",
                "id": "fontIconImgCLose",
                "isVisible": true,
                "left": "17dp",
                "skin": "sknFontIconSearchCross16px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmAlertsManagement.lblAlertPreviewClose\")",
                "width": "15dp",
                "zIndex": 2
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxCloseLimits.add(fontIconImgCLose);
            var flxLimitsHeading = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "40dp",
                "id": "flxLimitsHeading",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "10dp",
                "width": "100%",
                "zIndex": 20
            }, {}, {});
            flxLimitsHeading.setDefaultUnit(kony.flex.DP);
            var lblLimitsPopupHeading = new kony.ui.Label({
                "id": "lblLimitsPopupHeading",
                "isVisible": true,
                "left": "20dp",
                "skin": "sknLbl192b45LatoReg16px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmServiceManagement.ViewLimits\")",
                "top": "0dp",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var btnEditActionLimits = new kony.ui.Button({
                "height": "22dp",
                "id": "btnEditActionLimits",
                "isVisible": true,
                "right": "20dp",
                "skin": "sknbtnf7f7faLatoRegular12Px485c75Border1px485C75Radius20px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.permission.Edit\")",
                "top": "0dp",
                "width": "50dp",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_CENTER,
                "displayText": true,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {
                "hoverSkin": "sknbtnffffffLatoRegular12Px485c75Border1px485C75Radius20px"
            });
            var lblLimitsPopupSeperator = new kony.ui.Label({
                "bottom": "0dp",
                "height": "1dp",
                "id": "lblLimitsPopupSeperator",
                "isVisible": true,
                "left": "20dp",
                "right": "20dp",
                "skin": "sknLble1e5edOp100",
                "text": "-",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 2
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxLimitsHeading.add(lblLimitsPopupHeading, btnEditActionLimits, lblLimitsPopupSeperator);
            var flxLimitsContainer = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                "bottom": "30dp",
                "clipBounds": true,
                "id": "flxLimitsContainer",
                "isVisible": true,
                "layoutType": kony.flex.FLOW_VERTICAL,
                "left": "20dp",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0dp",
                "width": "93%",
                "zIndex": 20
            }, {}, {});
            flxLimitsContainer.setDefaultUnit(kony.flex.DP);
            var flxActionLimitsHeaders1 = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                "clipBounds": true,
                "id": "flxActionLimitsHeaders1",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "17dp",
                "width": "100%",
                "zIndex": 1
            }, {}, {});
            flxActionLimitsHeaders1.setDefaultUnit(kony.flex.DP);
            var lblMinValHeader11 = new kony.ui.Label({
                "id": "lblMinValHeader11",
                "isVisible": true,
                "left": "0dp",
                "skin": "sknlblLato696c7312px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmServiceManagement.MinValuePerTransactionUC\")",
                "top": "0dp",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var lblMaxValHeader12 = new kony.ui.Label({
                "id": "lblMaxValHeader12",
                "isVisible": true,
                "left": "55%",
                "skin": "sknlblLato696c7312px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmServiceManagement.PerTransactionLimitUC\")",
                "top": "0dp",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxActionLimitsHeaders1.add(lblMinValHeader11, lblMaxValHeader12);
            var flxActionLimitsValues1 = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                "clipBounds": true,
                "id": "flxActionLimitsValues1",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0dp",
                "width": "100%"
            }, {}, {});
            flxActionLimitsValues1.setDefaultUnit(kony.flex.DP);
            var lblMinValue11 = new kony.ui.Label({
                "id": "lblMinValue11",
                "isVisible": true,
                "left": "0dp",
                "skin": "sknLatoSemibold485c7313px",
                "text": "$10",
                "top": "10dp",
                "width": "40%",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var lblMaxValue12 = new kony.ui.Label({
                "id": "lblMaxValue12",
                "isVisible": true,
                "left": "55%",
                "skin": "sknLatoSemibold485c7313px",
                "text": "$10",
                "top": "10dp",
                "width": "40%",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxActionLimitsValues1.add(lblMinValue11, lblMaxValue12);
            var flxActionLimitsTextbox1 = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                "clipBounds": true,
                "id": "flxActionLimitsTextbox1",
                "isVisible": false,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "15dp",
                "width": "100%"
            }, {}, {});
            flxActionLimitsTextbox1.setDefaultUnit(kony.flex.DP);
            var valueEntryMinValueLimit11 = new com.adminConsole.Features.ValueEntry({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "height": "40dp",
                "id": "valueEntryMinValueLimit11",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "masterType": constants.MASTER_TYPE_DEFAULT,
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0dp",
                "width": "250dp",
                "overrides": {
                    "ValueEntry": {
                        "height": "40dp",
                        "width": "250dp"
                    },
                    "tbxEnterValue": {
                        "centerY": "50%",
                        "top": "viz.val_cleared"
                    }
                }
            }, {
                "overrides": {}
            }, {
                "overrides": {}
            });
            var flxMinValueLimitError11 = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                "clipBounds": true,
                "id": "flxMinValueLimitError11",
                "isVisible": false,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "40dp",
                "width": "260dp",
                "zIndex": 1
            }, {}, {});
            flxMinValueLimitError11.setDefaultUnit(kony.flex.DP);
            var lblErrorIcon11 = new kony.ui.Label({
                "height": "15dp",
                "id": "lblErrorIcon11",
                "isVisible": true,
                "left": "0dp",
                "skin": "sknErrorIcon",
                "text": "",
                "top": "0dp",
                "width": "15dp",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var lblMinValueLimitErrorMsg11 = new kony.ui.Label({
                "height": "15dp",
                "id": "lblMinValueLimitErrorMsg11",
                "isVisible": true,
                "left": "15dp",
                "skin": "sknlblError",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmServiceManagement.lblNoServiceTypeError\")",
                "top": "0dp",
                "width": "100%",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxMinValueLimitError11.add(lblErrorIcon11, lblMinValueLimitErrorMsg11);
            var valueEntryMaxValueLimit12 = new com.adminConsole.Features.ValueEntry({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "height": "40dp",
                "id": "valueEntryMaxValueLimit12",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "55%",
                "masterType": constants.MASTER_TYPE_DEFAULT,
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0dp",
                "width": "250dp",
                "overrides": {
                    "ValueEntry": {
                        "height": "40dp",
                        "left": "55%",
                        "top": "0dp",
                        "width": "250dp"
                    }
                }
            }, {
                "overrides": {}
            }, {
                "overrides": {}
            });
            var flxMaxValueLimitError12 = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                "clipBounds": true,
                "id": "flxMaxValueLimitError12",
                "isVisible": false,
                "layoutType": kony.flex.FREE_FORM,
                "left": "55%",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "40dp",
                "width": "260dp",
                "zIndex": 1
            }, {}, {});
            flxMaxValueLimitError12.setDefaultUnit(kony.flex.DP);
            var lblErrorIcon12 = new kony.ui.Label({
                "height": "15dp",
                "id": "lblErrorIcon12",
                "isVisible": true,
                "left": "0dp",
                "skin": "sknErrorIcon",
                "text": "",
                "top": "0dp",
                "width": "15dp",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var lblMaxValueLimitErrorMsg12 = new kony.ui.Label({
                "height": "15dp",
                "id": "lblMaxValueLimitErrorMsg12",
                "isVisible": true,
                "left": "15dp",
                "skin": "sknlblError",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmServiceManagement.lblNoServiceTypeError\")",
                "top": "0dp",
                "width": "100%",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxMaxValueLimitError12.add(lblErrorIcon12, lblMaxValueLimitErrorMsg12);
            flxActionLimitsTextbox1.add(valueEntryMinValueLimit11, flxMinValueLimitError11, valueEntryMaxValueLimit12, flxMaxValueLimitError12);
            var flxActionLimitsHeaders2 = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                "clipBounds": true,
                "id": "flxActionLimitsHeaders2",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "20dp",
                "width": "100%"
            }, {}, {});
            flxActionLimitsHeaders2.setDefaultUnit(kony.flex.DP);
            var lblMaxDailyLimitHeader21 = new kony.ui.Label({
                "id": "lblMaxDailyLimitHeader21",
                "isVisible": true,
                "left": "0dp",
                "skin": "sknlblLato696c7312px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmServiceManagement.DailyTransactionLimitUC\")",
                "top": "0dp",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var lblWeeklyLimitHeader22 = new kony.ui.Label({
                "id": "lblWeeklyLimitHeader22",
                "isVisible": true,
                "left": "55%",
                "skin": "sknlblLato696c7312px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmServiceManagement.WeeklyTransLimitUC\")",
                "top": "0dp",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxActionLimitsHeaders2.add(lblMaxDailyLimitHeader21, lblWeeklyLimitHeader22);
            var flxActionLimitsValues2 = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                "clipBounds": true,
                "id": "flxActionLimitsValues2",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "15dp",
                "width": "100%",
                "zIndex": 1
            }, {}, {});
            flxActionLimitsValues2.setDefaultUnit(kony.flex.DP);
            var lblMaxDailyLimitValue21 = new kony.ui.Label({
                "id": "lblMaxDailyLimitValue21",
                "isVisible": true,
                "left": "0dp",
                "skin": "sknLatoSemibold485c7313px",
                "text": "$10",
                "top": "0dp",
                "width": "40%",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var lblWeeklyLimitValue22 = new kony.ui.Label({
                "id": "lblWeeklyLimitValue22",
                "isVisible": true,
                "left": "55%",
                "skin": "sknLatoSemibold485c7313px",
                "text": "$10",
                "top": "0dp",
                "width": "40%",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxActionLimitsValues2.add(lblMaxDailyLimitValue21, lblWeeklyLimitValue22);
            var flxActionLimitsTextbox2 = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                "clipBounds": true,
                "id": "flxActionLimitsTextbox2",
                "isVisible": false,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "15dp",
                "width": "100%"
            }, {}, {});
            flxActionLimitsTextbox2.setDefaultUnit(kony.flex.DP);
            var valueEntryMaxDailyValue21 = new com.adminConsole.Features.ValueEntry({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "height": "40dp",
                "id": "valueEntryMaxDailyValue21",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "masterType": constants.MASTER_TYPE_DEFAULT,
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0dp",
                "width": "250dp",
                "overrides": {
                    "ValueEntry": {
                        "height": "40dp",
                        "width": "250dp"
                    }
                }
            }, {
                "overrides": {}
            }, {
                "overrides": {}
            });
            var flxMaxDailyLimitError21 = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                "clipBounds": true,
                "id": "flxMaxDailyLimitError21",
                "isVisible": false,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "40dp",
                "width": "260dp",
                "zIndex": 1
            }, {}, {});
            flxMaxDailyLimitError21.setDefaultUnit(kony.flex.DP);
            var lblErrorIcon21 = new kony.ui.Label({
                "height": "15dp",
                "id": "lblErrorIcon21",
                "isVisible": true,
                "left": "0dp",
                "skin": "sknErrorIcon",
                "text": "",
                "top": "0dp",
                "width": "15dp",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var lblDailyLimitValueErrorMsg21 = new kony.ui.Label({
                "height": "15dp",
                "id": "lblDailyLimitValueErrorMsg21",
                "isVisible": true,
                "left": "15dp",
                "skin": "sknlblError",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmServiceManagement.lblNoServiceTypeError\")",
                "top": "0dp",
                "width": "100%",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxMaxDailyLimitError21.add(lblErrorIcon21, lblDailyLimitValueErrorMsg21);
            var valueEntryWeeklyLimitValue22 = new com.adminConsole.Features.ValueEntry({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "height": "40dp",
                "id": "valueEntryWeeklyLimitValue22",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "55%",
                "masterType": constants.MASTER_TYPE_DEFAULT,
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0dp",
                "width": "250dp",
                "overrides": {
                    "ValueEntry": {
                        "height": "40dp",
                        "left": "55%",
                        "top": "0dp",
                        "width": "250dp"
                    }
                }
            }, {
                "overrides": {}
            }, {
                "overrides": {}
            });
            var flxWeeklyLimitValueError22 = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                "clipBounds": true,
                "id": "flxWeeklyLimitValueError22",
                "isVisible": false,
                "layoutType": kony.flex.FREE_FORM,
                "left": "55%",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "40dp",
                "width": "260dp",
                "zIndex": 1
            }, {}, {});
            flxWeeklyLimitValueError22.setDefaultUnit(kony.flex.DP);
            var lblErrorIcon22 = new kony.ui.Label({
                "height": "15dp",
                "id": "lblErrorIcon22",
                "isVisible": true,
                "left": "0dp",
                "skin": "sknErrorIcon",
                "text": "",
                "top": "0dp",
                "width": "15dp",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var lblWeeklyLimitValueErrorMsg22 = new kony.ui.Label({
                "height": "15dp",
                "id": "lblWeeklyLimitValueErrorMsg22",
                "isVisible": true,
                "left": "15dp",
                "skin": "sknlblError",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmServiceManagement.lblNoServiceTypeError\")",
                "top": "0dp",
                "width": "100%",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxWeeklyLimitValueError22.add(lblErrorIcon22, lblWeeklyLimitValueErrorMsg22);
            flxActionLimitsTextbox2.add(valueEntryMaxDailyValue21, flxMaxDailyLimitError21, valueEntryWeeklyLimitValue22, flxWeeklyLimitValueError22);
            flxLimitsContainer.add(flxActionLimitsHeaders1, flxActionLimitsValues1, flxActionLimitsTextbox1, flxActionLimitsHeaders2, flxActionLimitsValues2, flxActionLimitsTextbox2);
            flxPopupHeader.add(flxCloseLimits, flxLimitsHeading, flxLimitsContainer);
            var flxEditButtons = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "80px",
                "id": "flxEditButtons",
                "isVisible": false,
                "layoutType": kony.flex.FREE_FORM,
                "isModalContainer": false,
                "skin": "sknflxBgE4E6EC",
                "top": "0px",
                "width": "100%",
                "zIndex": 1
            }, {}, {});
            flxEditButtons.setDefaultUnit(kony.flex.DP);
            var flxError = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "centerY": "50%",
                "clipBounds": true,
                "height": "80px",
                "id": "flxError",
                "isVisible": false,
                "layoutType": kony.flex.FREE_FORM,
                "left": "10dp",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0dp",
                "width": "50%",
                "zIndex": 1
            }, {}, {});
            flxError.setDefaultUnit(kony.flex.DP);
            var lblErrorMsg = new kony.ui.Label({
                "centerY": "50%",
                "id": "lblErrorMsg",
                "isVisible": true,
                "left": "40dp",
                "skin": "sknlblError",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmPolicies.lblErrorMsg\")",
                "top": "0dp",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var imgError = new kony.ui.Image2({
                "centerY": "50%",
                "height": "15dp",
                "id": "imgError",
                "isVisible": true,
                "left": "15dp",
                "skin": "slImage",
                "src": "error_1x.png",
                "top": "0dp",
                "width": "15dp",
                "zIndex": 1
            }, {
                "imageScaleMode": constants.IMAGE_SCALE_MODE_MAINTAIN_ASPECT_RATIO,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxError.add(lblErrorMsg, imgError);
            var btnCancel = new kony.ui.Button({
                "centerY": "50%",
                "focusSkin": "sknbtnf7f7faLatoRegular4f555dBorder1px8b96a5",
                "height": "40px",
                "id": "btnCancel",
                "isVisible": true,
                "onClick": controller.AS_Button_fe48adad632c43c5b97a05155a450331,
                "right": "155px",
                "skin": "sknbtnf7f7faLatoRegular4f555dBorder1px8b96a5",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmCustomers.CANCEL\")",
                "top": "0%",
                "width": "120px",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_CENTER,
                "displayText": true,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {
                "hoverSkin": "sknbtnffffffLatoRegular4f555dBorder1px485c75"
            });
            var btnsave = new kony.ui.Button({
                "centerY": "50%",
                "focusSkin": "sknBtnBg003e75R20pxF13pxLatoReg",
                "height": "40dp",
                "id": "btnsave",
                "isVisible": true,
                "onClick": controller.AS_Button_c973a89c85624425bb033af39fa13bd2,
                "right": "20px",
                "skin": "sknBtnBg003e75R20pxF13pxLatoReg",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmAdManagement.UpdateCAPS\")",
                "top": "0%",
                "width": "120dp",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_CENTER,
                "displayText": true,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {
                "hoverSkin": "sknBtnBg005198R20pxF13pxLatoReg"
            });
            flxEditButtons.add(flxError, btnCancel, btnsave);
            flxViewEditLimitCont.add(flxLimitsTopColor, flxPopupHeader, flxEditButtons);
            flxViewEditLimitsPopup.add(flxViewEditLimitCont);
            var flxEditFeaturePopup = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "100%",
                "id": "flxEditFeaturePopup",
                "isVisible": false,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "sknFocus50",
                "top": "0dp",
                "width": "100%",
                "zIndex": 5
            }, {}, {});
            flxEditFeaturePopup.setDefaultUnit(kony.flex.DP);
            var flxEditFeatureContainer = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "centerX": "50%",
                "centerY": "50%",
                "clipBounds": true,
                "height": "480dp",
                "id": "flxEditFeatureContainer",
                "isVisible": true,
                "layoutType": kony.flex.FLOW_VERTICAL,
                "isModalContainer": false,
                "skin": "slFbox0j9f841cc563e4e",
                "width": "950px",
                "zIndex": 1
            }, {}, {});
            flxEditFeatureContainer.setDefaultUnit(kony.flex.DP);
            var flxFeaturesTopColor = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "10px",
                "id": "flxFeaturesTopColor",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0px",
                "isModalContainer": false,
                "skin": "sknFlxInfoToastBg357C9ENoRadius",
                "top": "0dp",
                "width": "100%",
                "zIndex": 1
            }, {}, {});
            flxFeaturesTopColor.setDefaultUnit(kony.flex.DP);
            flxFeaturesTopColor.add();
            var flxFeatureEditContainer = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "82%",
                "id": "flxFeatureEditContainer",
                "isVisible": true,
                "layoutType": kony.flex.FLOW_VERTICAL,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0px",
                "width": "100%",
                "zIndex": 1
            }, {}, {});
            flxFeatureEditContainer.setDefaultUnit(kony.flex.DP);
            var flxCloseFeature = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "25px",
                "id": "flxCloseFeature",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "isModalContainer": false,
                "right": "13dp",
                "skin": "sknCursor",
                "top": "10dp",
                "width": "25px",
                "zIndex": 20
            }, {}, {
                "hoverSkin": "sknCursor"
            });
            flxCloseFeature.setDefaultUnit(kony.flex.DP);
            var fontIconImgCloseFeature = new kony.ui.Label({
                "centerX": "51%",
                "centerY": "50.00%",
                "height": "15dp",
                "id": "fontIconImgCloseFeature",
                "isVisible": true,
                "left": "17dp",
                "skin": "sknFontIconSearchCross16px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmAlertsManagement.lblAlertPreviewClose\")",
                "width": "15dp",
                "zIndex": 2
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxCloseFeature.add(fontIconImgCloseFeature);
            var flxFeaturesHeading = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "30dp",
                "id": "flxFeaturesHeading",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0dp",
                "width": "100%",
                "zIndex": 20
            }, {}, {});
            flxFeaturesHeading.setDefaultUnit(kony.flex.DP);
            var lblFeaturesPopupHeading = new kony.ui.Label({
                "centerY": "48%",
                "id": "lblFeaturesPopupHeading",
                "isVisible": true,
                "left": "20dp",
                "skin": "sknLbl192b45LatoReg16px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmServiceManagement.EditFeature\")",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var lblFeatureNamePopup = new kony.ui.Label({
                "centerY": "48%",
                "id": "lblFeatureNamePopup",
                "isVisible": true,
                "left": "120dp",
                "skin": "sknLbl192b45LatoReg16px",
                "text": "ACH Collection",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var flxFeatureStatusContainer = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                "clipBounds": false,
                "id": "flxFeatureStatusContainer",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "isModalContainer": false,
                "right": "20dp",
                "skin": "slFbox",
                "top": 0,
                "width": "10%",
                "zIndex": 1
            }, {}, {});
            flxFeatureStatusContainer.setDefaultUnit(kony.flex.DP);
            var flxStatusValue = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                "clipBounds": true,
                "id": "flxStatusValue",
                "isVisible": true,
                "layoutType": kony.flex.FLOW_HORIZONTAL,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": 0,
                "width": "100%"
            }, {}, {});
            flxStatusValue.setDefaultUnit(kony.flex.DP);
            var lblActiveText = new kony.ui.Label({
                "height": "20px",
                "id": "lblActiveText",
                "isVisible": true,
                "left": "0px",
                "skin": "sknLbl485C75LatoRegular13Px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmAlertsManagement.Active\")",
                "top": "3px",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var flxEditAlertStatusSwitch = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "25px",
                "id": "flxEditAlertStatusSwitch",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "20px",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0px",
                "width": "38px",
                "zIndex": 1
            }, {}, {});
            flxEditAlertStatusSwitch.setDefaultUnit(kony.flex.DP);
            var featureStatusSwitch = new com.adminConsole.common.customSwitch({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "height": "25dp",
                "id": "featureStatusSwitch",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0",
                "masterType": constants.MASTER_TYPE_DEFAULT,
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0",
                "width": "100%",
                "overrides": {
                    "customSwitch": {
                        "height": "25dp",
                        "isVisible": true,
                        "left": "0",
                        "top": "0"
                    },
                    "switchToggle": {
                        "isVisible": true
                    }
                }
            }, {
                "overrides": {}
            }, {
                "overrides": {}
            });
            flxEditAlertStatusSwitch.add(featureStatusSwitch);
            flxStatusValue.add(lblActiveText, flxEditAlertStatusSwitch);
            flxFeatureStatusContainer.add(flxStatusValue);
            flxFeaturesHeading.add(lblFeaturesPopupHeading, lblFeatureNamePopup, flxFeatureStatusContainer);
            var flxFeatureTabs = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "45dp",
                "id": "flxFeatureTabs",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "sknFlxFFFFFF1000",
                "top": "20dp",
                "width": "100%",
                "zIndex": 20
            }, {}, {});
            flxFeatureTabs.setDefaultUnit(kony.flex.DP);
            var flxTabs = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "40px",
                "id": "flxTabs",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0px",
                "isModalContainer": false,
                "right": "0px",
                "skin": "slFbox",
                "top": "0px",
                "zIndex": 2
            }, {}, {});
            flxTabs.setDefaultUnit(kony.flex.DP);
            var btnFeatures = new kony.ui.Button({
                "height": "43px",
                "id": "btnFeatures",
                "isVisible": true,
                "left": "20px",
                "skin": "sknBtnBgffffffBrD7D9E0Rd3px485B75Bold12px",
                "text": "FEATURE DETAILS",
                "top": "0px",
                "width": "130px",
                "zIndex": 5
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_CENTER,
                "displayText": true,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var btnActions = new kony.ui.Button({
                "height": "43px",
                "id": "btnActions",
                "isVisible": true,
                "left": "155px",
                "skin": "sknBtnBgD2D7E2Rou3pxLato485B7512px",
                "text": "ASSOCIATED ACTIONS",
                "top": "0px",
                "width": "150px",
                "zIndex": 5
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_CENTER,
                "displayText": true,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var lblBottomSeperatorCommonTabs = new kony.ui.Label({
                "height": "1px",
                "id": "lblBottomSeperatorCommonTabs",
                "isVisible": true,
                "left": "0px",
                "right": "0px",
                "skin": "sknlblSeperatorBgD5D9DD",
                "top": "39px",
                "zIndex": 2
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxTabs.add(btnFeatures, btnActions, lblBottomSeperatorCommonTabs);
            flxFeatureTabs.add(flxTabs);
            var flxFeatureContainer = new kony.ui.FlexScrollContainer({
                "allowHorizontalBounce": false,
                "allowVerticalBounce": true,
                "bounces": true,
                "clipBounds": true,
                "enableScrolling": true,
                "height": "290dp",
                "horizontalScrollIndicator": true,
                "id": "flxFeatureContainer",
                "isVisible": true,
                "layoutType": kony.flex.FLOW_VERTICAL,
                "left": "0dp",
                "pagingEnabled": false,
                "scrollDirection": kony.flex.SCROLL_VERTICAL,
                "skin": "slFSbox",
                "top": "-5dp",
                "verticalScrollIndicator": true,
                "width": "100%",
                "zIndex": 20
            }, {}, {});
            flxFeatureContainer.setDefaultUnit(kony.flex.DP);
            var flxFeatureDetails = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                "clipBounds": true,
                "id": "flxFeatureDetails",
                "isVisible": true,
                "layoutType": kony.flex.FLOW_HORIZONTAL,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "20dp",
                "width": "100%"
            }, {}, {});
            flxFeatureDetails.setDefaultUnit(kony.flex.DP);
            var flxColumn1 = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                "clipBounds": true,
                "id": "flxColumn1",
                "isVisible": true,
                "layoutType": kony.flex.FLOW_VERTICAL,
                "left": "20px",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0dp",
                "width": "270dp",
                "zIndex": 1
            }, {}, {});
            flxColumn1.setDefaultUnit(kony.flex.DP);
            var lblHeading1 = new kony.ui.Label({
                "id": "lblHeading1",
                "isVisible": true,
                "left": "0px",
                "skin": "sknlblLato696c7313px",
                "text": "FEATURE CODE",
                "top": "0px",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var lblData1 = new kony.ui.Label({
                "id": "lblData1",
                "isVisible": true,
                "left": "0px",
                "skin": "sknLatoSemibold485c7313px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.userwidgetmodel.lblData1\")",
                "top": "10px",
                "width": "100%",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxColumn1.add(lblHeading1, lblData1);
            var flxColumn2 = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                "clipBounds": true,
                "id": "flxColumn2",
                "isVisible": true,
                "layoutType": kony.flex.FLOW_VERTICAL,
                "left": "25px",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0dp",
                "width": "260dp",
                "zIndex": 1
            }, {}, {});
            flxColumn2.setDefaultUnit(kony.flex.DP);
            var flxHeading2 = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                "clipBounds": true,
                "id": "flxHeading2",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0dp",
                "width": "100%"
            }, {}, {});
            flxHeading2.setDefaultUnit(kony.flex.DP);
            var lblHeading2 = new kony.ui.Label({
                "id": "lblHeading2",
                "isVisible": true,
                "left": "0px",
                "skin": "sknlblLato696c7313px",
                "text": "FEATURE TYPE",
                "top": "0px",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var fontIconData2 = new kony.ui.Label({
                "id": "fontIconData2",
                "isVisible": true,
                "left": "100dp",
                "skin": "sknfontIcon192b4b14px",
                "text": "",
                "textStyle": {},
                "top": 0,
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_TOP_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxHeading2.add(lblHeading2, fontIconData2);
            var lblData2 = new kony.ui.Label({
                "id": "lblData2",
                "isVisible": true,
                "left": "0px",
                "skin": "sknLatoSemibold485c7313px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.userwidgetmodel.lblData1\")",
                "top": "10px",
                "width": "100%",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxColumn2.add(flxHeading2, lblData2);
            var flxColumn3 = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                "clipBounds": true,
                "id": "flxColumn3",
                "isVisible": true,
                "layoutType": kony.flex.FLOW_VERTICAL,
                "left": "25dp",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0dp",
                "width": "270dp",
                "zIndex": 1
            }, {}, {});
            flxColumn3.setDefaultUnit(kony.flex.DP);
            var flxHeading3 = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                "clipBounds": true,
                "id": "flxHeading3",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0dp",
                "width": "100%"
            }, {}, {});
            flxHeading3.setDefaultUnit(kony.flex.DP);
            var lblHeading3 = new kony.ui.Label({
                "id": "lblHeading3",
                "isVisible": true,
                "left": "0px",
                "skin": "sknlblLato696c7313px",
                "text": "SERVICE FEE",
                "top": "0px",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var fontIconData3 = new kony.ui.Label({
                "id": "fontIconData3",
                "isVisible": false,
                "left": "75dp",
                "skin": "sknfontIcon192b4b14px",
                "text": "",
                "textStyle": {},
                "top": 0,
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_TOP_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxHeading3.add(lblHeading3, fontIconData3);
            var ValueEntry = new com.adminConsole.Features.ValueEntry({
                "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                "id": "ValueEntry",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "masterType": constants.MASTER_TYPE_DEFAULT,
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "10dp",
                "width": "100%",
                "overrides": {
                    "ValueEntry": {
                        "top": "10dp"
                    },
                    "tbxEnterValue": {
                        "placeholder": "1 - 9999 9999 9999"
                    }
                }
            }, {
                "overrides": {}
            }, {
                "overrides": {}
            });
            var flxNoServiceFee = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "15px",
                "id": "flxNoServiceFee",
                "isVisible": false,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0px",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "5dp",
                "width": "100%",
                "zIndex": 2
            }, {}, {});
            flxNoServiceFee.setDefaultUnit(kony.flex.DP);
            var lblNoServiceFeeErrorIcon = new kony.ui.Label({
                "height": "15px",
                "id": "lblNoServiceFeeErrorIcon",
                "isVisible": true,
                "left": "0px",
                "skin": "sknErrorIcon",
                "text": "",
                "top": "0px",
                "width": "15px",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var lblNoServiceFeeError = new kony.ui.Label({
                "height": "15px",
                "id": "lblNoServiceFeeError",
                "isVisible": true,
                "left": "15px",
                "skin": "sknlblError",
                "text": "Service Fee cannot be empty",
                "top": "0px",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxNoServiceFee.add(lblNoServiceFeeErrorIcon, lblNoServiceFeeError);
            flxColumn3.add(flxHeading3, ValueEntry, flxNoServiceFee);
            flxFeatureDetails.add(flxColumn1, flxColumn2, flxColumn3);
            var flxDisplayContent = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "335dp",
                "id": "flxDisplayContent",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "20dp",
                "width": "100%"
            }, {}, {});
            flxDisplayContent.setDefaultUnit(kony.flex.DP);
            var flxFeatureDisplayContent = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "300dp",
                "id": "flxFeatureDisplayContent",
                "isVisible": true,
                "layoutType": kony.flex.FLOW_VERTICAL,
                "left": "20px",
                "isModalContainer": false,
                "right": "20px",
                "skin": "sknFlxBgF8F9FABrE1E5EER3px",
                "top": "0px",
                "zIndex": 1
            }, {}, {});
            flxFeatureDisplayContent.setDefaultUnit(kony.flex.DP);
            var flxDisplayContentHeader = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                "clipBounds": true,
                "id": "flxDisplayContentHeader",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0dp",
                "width": "100%"
            }, {}, {});
            flxDisplayContentHeader.setDefaultUnit(kony.flex.DP);
            var lblDisplayContentHeader = new kony.ui.Label({
                "id": "lblDisplayContentHeader",
                "isVisible": true,
                "left": "20dp",
                "skin": "sknLatoRegular485C7518px",
                "text": "Display Content",
                "top": "17dp",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var flxLanguageHeader = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "30dp",
                "id": "flxLanguageHeader",
                "isVisible": true,
                "layoutType": kony.flex.FLOW_HORIZONTAL,
                "left": "165dp",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "14dp",
                "width": "200dp"
            }, {}, {});
            flxLanguageHeader.setDefaultUnit(kony.flex.DP);
            var lblSelectedLanguage = new kony.ui.Label({
                "centerY": "50%",
                "id": "lblSelectedLanguage",
                "isVisible": true,
                "left": "0dp",
                "skin": "sknlblLatoBold35475f14px",
                "text": "English (Default)",
                "top": "0dp",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var lblSelectedLanguageIcon = new kony.ui.Label({
                "centerY": "50%",
                "id": "lblSelectedLanguageIcon",
                "isVisible": true,
                "left": "5dp",
                "skin": "sknIcon12pxBlack",
                "text": "",
                "top": "0dp",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 11
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxLanguageHeader.add(lblSelectedLanguage, lblSelectedLanguageIcon);
            flxDisplayContentHeader.add(lblDisplayContentHeader, flxLanguageHeader);
            var flxNameContainer = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                "clipBounds": true,
                "id": "flxNameContainer",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "20dp",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "10dp",
                "width": "460dp"
            }, {}, {});
            flxNameContainer.setDefaultUnit(kony.flex.DP);
            var lblEditFeatureName = new kony.ui.Label({
                "height": "20px",
                "id": "lblEditFeatureName",
                "isVisible": true,
                "left": "0dp",
                "skin": "sknlblLatoBold35475f14px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmAlertsManagement.lblEditAlertMainName\")",
                "top": "0px",
                "width": "150dp",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var lblFeatureNameCount = new kony.ui.Label({
                "height": "20px",
                "id": "lblFeatureNameCount",
                "isVisible": true,
                "right": "0dp",
                "skin": "sknlblLatoBold35475f14px",
                "text": "0/60",
                "top": "0dp",
                "width": "150dp",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_RIGHT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var tbxEditFeatureName = new kony.ui.TextBox2({
                "autoCapitalize": constants.TEXTBOX_AUTO_CAPITALIZE_NONE,
                "focusSkin": "skntxtbxDetailsFocus14px",
                "height": "40px",
                "id": "tbxEditFeatureName",
                "isVisible": true,
                "keyBoardStyle": constants.TEXTBOX_KEY_BOARD_STYLE_DEFAULT,
                "left": "0px",
                "maxTextLength": 60,
                "placeholder": "Name",
                "secureTextEntry": false,
                "skin": "skntbxLato35475f14px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmAlertsManagement.Default\")",
                "textInputMode": constants.TEXTBOX_INPUT_MODE_ANY,
                "top": "30px",
                "width": "100%",
                "zIndex": 1
            }, {
                "containerHeightMode": constants.TEXTBOX_FONT_METRICS_DRIVEN_HEIGHT,
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [1, 0, 0, 0],
                "paddingInPixel": false
            }, {
                "autoCorrect": false
            });
            var flxNoEditFeatureNameError = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "bottom": "0dp",
                "clipBounds": true,
                "height": "15px",
                "id": "flxNoEditFeatureNameError",
                "isVisible": false,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0px",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "75dp",
                "width": "100%",
                "zIndex": 2
            }, {}, {});
            flxNoEditFeatureNameError.setDefaultUnit(kony.flex.DP);
            var lblNoEditFeatureNameErrorIcon = new kony.ui.Label({
                "height": "15px",
                "id": "lblNoEditFeatureNameErrorIcon",
                "isVisible": true,
                "left": "0px",
                "skin": "sknErrorIcon",
                "text": "",
                "top": "0px",
                "width": "15px",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var lblNoEditFeatureNameError = new kony.ui.Label({
                "height": "15px",
                "id": "lblNoEditFeatureNameError",
                "isVisible": true,
                "left": "15px",
                "skin": "sknlblError",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmAlertsManagement.lblNoEditAlertMainNameError\")",
                "top": "0px",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxNoEditFeatureNameError.add(lblNoEditFeatureNameErrorIcon, lblNoEditFeatureNameError);
            flxNameContainer.add(lblEditFeatureName, lblFeatureNameCount, tbxEditFeatureName, flxNoEditFeatureNameError);
            var flxDescriptionContainer = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                "clipBounds": true,
                "id": "flxDescriptionContainer",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "20dp",
                "isModalContainer": false,
                "right": "20dp",
                "skin": "slFbox",
                "top": "20dp",
                "width": "95%"
            }, {}, {});
            flxDescriptionContainer.setDefaultUnit(kony.flex.DP);
            var lblEditFeatureDescription = new kony.ui.Label({
                "height": "20px",
                "id": "lblEditFeatureDescription",
                "isVisible": true,
                "left": "0px",
                "skin": "sknlblLatoBold35475f14px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmAlertsManagement.lblEditAlertDescription\")",
                "top": "0px",
                "width": "150px",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var lblFeatureDescriptionCount = new kony.ui.Label({
                "height": "20px",
                "id": "lblFeatureDescriptionCount",
                "isVisible": true,
                "right": "0px",
                "skin": "sknlblLatoBold35475f14px",
                "text": "0/100",
                "top": "0px",
                "width": "150dp",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_RIGHT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var txtareaEditFeatureDescription = new kony.ui.TextArea2({
                "autoCapitalize": constants.TEXTAREA_AUTO_CAPITALIZE_NONE,
                "focusSkin": "skntxtAreaLato35475f14Px",
                "height": "85px",
                "id": "txtareaEditFeatureDescription",
                "isVisible": true,
                "keyBoardStyle": constants.TEXTAREA_KEY_BOARD_STYLE_DEFAULT,
                "left": "0px",
                "maxTextLength": 100,
                "numberOfVisibleLines": 3,
                "placeholder": "Description",
                "skin": "skntxtAreaLato35475f14Px",
                "text": "Feature description",
                "textInputMode": constants.TEXTAREA_INPUT_MODE_ANY,
                "top": "30px",
                "width": "100%",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_TOP_LEFT,
                "padding": [1, 1, 0, 1],
                "paddingInPixel": false
            }, {
                "autoCorrect": false
            });
            var flxNoEditFeatureDescriptionError = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "15px",
                "id": "flxNoEditFeatureDescriptionError",
                "isVisible": false,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0px",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "120px",
                "width": "100%",
                "zIndex": 2
            }, {}, {});
            flxNoEditFeatureDescriptionError.setDefaultUnit(kony.flex.DP);
            var lblNoEditFeatureDescriptionErrorIcon = new kony.ui.Label({
                "height": "15px",
                "id": "lblNoEditFeatureDescriptionErrorIcon",
                "isVisible": true,
                "left": "0px",
                "skin": "sknErrorIcon",
                "text": "",
                "top": "0px",
                "width": "15px",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var lblNoEditFeatureDescriptionError = new kony.ui.Label({
                "height": "15px",
                "id": "lblNoEditFeatureDescriptionError",
                "isVisible": true,
                "left": "15px",
                "skin": "sknlblError",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmAlertsManagement.lblNoEditAlertDescriptionError\")",
                "top": "0px",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxNoEditFeatureDescriptionError.add(lblNoEditFeatureDescriptionErrorIcon, lblNoEditFeatureDescriptionError);
            flxDescriptionContainer.add(lblEditFeatureDescription, lblFeatureDescriptionCount, txtareaEditFeatureDescription, flxNoEditFeatureDescriptionError);
            flxFeatureDisplayContent.add(flxDisplayContentHeader, flxNameContainer, flxDescriptionContainer);
            var flxFeatureLanguages = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                "clipBounds": true,
                "id": "flxFeatureLanguages",
                "isVisible": false,
                "layoutType": kony.flex.FREE_FORM,
                "left": "143dp",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "35px",
                "width": "170dp",
                "zIndex": 11
            }, {}, {});
            flxFeatureLanguages.setDefaultUnit(kony.flex.DP);
            var EditFeatureLangList = new com.adminConsole.Products.productTypeFilterMenu({
                "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                "id": "EditFeatureLangList",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "masterType": constants.MASTER_TYPE_DEFAULT,
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "5dp",
                "width": "100%",
                "zIndex": 5,
                "overrides": {
                    "flxChechboxOuter": {
                        "height": "120px"
                    },
                    "flxFilterSort": {
                        "isVisible": false
                    },
                    "imgUpArrow": {
                        "src": "uparrow_2x.png"
                    },
                    "lblSeperator": {
                        "isVisible": false
                    },
                    "segStatusFilterDropdown": {
                        "data": [{
                            "imgCheckBox": "",
                            "lblDescription": "English"
                        }, {
                            "imgCheckBox": "",
                            "lblDescription": "German"
                        }, {
                            "imgCheckBox": "",
                            "lblDescription": "Spanish"
                        }]
                    }
                }
            }, {
                "overrides": {}
            }, {
                "overrides": {}
            });
            flxFeatureLanguages.add(EditFeatureLangList);
            flxDisplayContent.add(flxFeatureDisplayContent, flxFeatureLanguages);
            flxFeatureContainer.add(flxFeatureDetails, flxDisplayContent);
            var flxFeaturedAction = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "260dp",
                "id": "flxFeaturedAction",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "10dp",
                "width": "100%"
            }, {}, {});
            flxFeaturedAction.setDefaultUnit(kony.flex.DP);
            var flxSegmentActions = new kony.ui.FlexContainer({
                "bottom": "0dp",
                "clipBounds": false,
                "id": "flxSegmentActions",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "flxf9f9f9RightBorder1Px",
                "top": "0dp",
                "width": "30%",
                "zIndex": 1
            }, {}, {});
            flxSegmentActions.setDefaultUnit(kony.flex.DP);
            var segFeaturedActions = new kony.ui.SegmentedUI2({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "data": [{
                    "btnOption1": "CREATE BILL PAY",
                    "lblSelected1": "kony.i18n.getLocalizedString(\"i18n.userwidgetmodel.fontIconSelected1\")",
                    "lblSeperator": "."
                }, {
                    "btnOption1": "VIEW BILL PAY",
                    "lblSelected1": "",
                    "lblSeperator": "."
                }, {
                    "btnOption1": "DELETE RECIPIENTS",
                    "lblSelected1": "",
                    "lblSeperator": "."
                }],
                "groupCells": false,
                "height": "100%",
                "id": "segFeaturedActions",
                "isVisible": true,
                "left": "0dp",
                "needPageIndicator": true,
                "onRowClick": controller.AS_Segment_f0fc7c14f03343b3a69ce4f6f0b5a1f3,
                "pageOffDotImage": "pageoffdot.png",
                "pageOnDotImage": "pageondot.png",
                "retainSelection": false,
                "right": "0px",
                "rowFocusSkin": "seg2Focus",
                "rowSkin": "sknseg3pxRadius",
                "rowTemplate": "flxRow",
                "sectionHeaderSkin": "sliPhoneSegmentHeader",
                "selectionBehavior": constants.SEGUI_DEFAULT_BEHAVIOR,
                "separatorRequired": false,
                "showScrollbars": true,
                "top": "0dp",
                "viewType": constants.SEGUI_VIEW_TYPE_TABLEVIEW,
                "widgetDataMap": {
                    "btnOption1": "btnOption1",
                    "flxContentContainer": "flxContentContainer",
                    "flxImgArrow": "flxImgArrow",
                    "flxRow": "flxRow",
                    "lblSelected1": "lblSelected1",
                    "lblSeperator": "lblSeperator"
                },
                "zIndex": 10
            }, {
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxSegmentActions.add(segFeaturedActions);
            var flxActionLimits = new kony.ui.FlexScrollContainer({
                "allowHorizontalBounce": false,
                "allowVerticalBounce": true,
                "bounces": true,
                "clipBounds": true,
                "enableScrolling": true,
                "height": "100%",
                "horizontalScrollIndicator": true,
                "id": "flxActionLimits",
                "isVisible": true,
                "layoutType": kony.flex.FLOW_VERTICAL,
                "left": "30%",
                "pagingEnabled": false,
                "scrollDirection": kony.flex.SCROLL_VERTICAL,
                "skin": "slFSbox",
                "top": "0dp",
                "verticalScrollIndicator": true,
                "width": "70%",
                "zIndex": 20
            }, {}, {});
            flxActionLimits.setDefaultUnit(kony.flex.DP);
            var flxActionDetails1 = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                "clipBounds": true,
                "id": "flxActionDetails1",
                "isVisible": true,
                "layoutType": kony.flex.FLOW_HORIZONTAL,
                "left": "20dp",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0dp",
                "width": "95%"
            }, {}, {});
            flxActionDetails1.setDefaultUnit(kony.flex.DP);
            var flxViewActionCode = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                "clipBounds": true,
                "id": "flxViewActionCode",
                "isVisible": true,
                "layoutType": kony.flex.FLOW_VERTICAL,
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0dp",
                "width": "33%",
                "zIndex": 1
            }, {}, {});
            flxViewActionCode.setDefaultUnit(kony.flex.DP);
            var lblViewActionCode = new kony.ui.Label({
                "id": "lblViewActionCode",
                "isVisible": true,
                "left": "0px",
                "skin": "sknlblLato696c7313px",
                "text": "ACTION CODE",
                "top": "0px",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var lblViewActionCodeValue = new kony.ui.Label({
                "id": "lblViewActionCodeValue",
                "isVisible": true,
                "left": "0px",
                "skin": "sknLatoSemibold485c7313px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.userwidgetmodel.lblData1\")",
                "top": "10px",
                "width": "96%",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxViewActionCode.add(lblViewActionCode, lblViewActionCodeValue);
            var flxActionType = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                "clipBounds": true,
                "id": "flxActionType",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0dp",
                "width": "33%",
                "zIndex": 1
            }, {}, {});
            flxActionType.setDefaultUnit(kony.flex.DP);
            var lblActionType = new kony.ui.Label({
                "id": "lblActionType",
                "isVisible": true,
                "left": "0px",
                "skin": "sknlblLato696c7313px",
                "text": "ACTION TYPE",
                "top": "0px",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var fontIconActionType = new kony.ui.Label({
                "id": "fontIconActionType",
                "isVisible": true,
                "left": "95dp",
                "skin": "sknfontIcon192b4b14px",
                "text": "",
                "textStyle": {},
                "top": 0,
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_TOP_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var lblActionTypeValue = new kony.ui.Label({
                "id": "lblActionTypeValue",
                "isVisible": true,
                "left": "0px",
                "skin": "sknLatoSemibold485c7313px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.userwidgetmodel.lblData1\")",
                "top": "25px",
                "width": "100%",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxActionType.add(lblActionType, fontIconActionType, lblActionTypeValue);
            var flxViewActionCategory = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                "clipBounds": true,
                "id": "flxViewActionCategory",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0dp",
                "width": "33%",
                "zIndex": 1
            }, {}, {});
            flxViewActionCategory.setDefaultUnit(kony.flex.DP);
            var lblViewActionCategory = new kony.ui.Label({
                "id": "lblViewActionCategory",
                "isVisible": true,
                "left": "0px",
                "skin": "sknlblLato696c7313px",
                "text": "ACTION CATEGORY",
                "top": "0px",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var lblViewActionCategoryValue = new kony.ui.Label({
                "id": "lblViewActionCategoryValue",
                "isVisible": true,
                "left": "0px",
                "skin": "sknLatoSemibold485c7313px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.userwidgetmodel.lblData1\")",
                "top": "25px",
                "width": "100%",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var fontIconViewActionCategory = new kony.ui.Label({
                "id": "fontIconViewActionCategory",
                "isVisible": true,
                "left": "130dp",
                "skin": "sknfontIcon192b4b14px",
                "text": "",
                "textStyle": {},
                "top": 0,
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_TOP_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxViewActionCategory.add(lblViewActionCategory, lblViewActionCategoryValue, fontIconViewActionCategory);
            flxActionDetails1.add(flxViewActionCode, flxActionType, flxViewActionCategory);
            var flxActionDetails2 = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                "clipBounds": true,
                "id": "flxActionDetails2",
                "isVisible": true,
                "layoutType": kony.flex.FLOW_HORIZONTAL,
                "left": "20px",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "25dp",
                "width": "95%",
                "zIndex": 1
            }, {}, {});
            flxActionDetails2.setDefaultUnit(kony.flex.DP);
            var flxViewActionLevel = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                "clipBounds": true,
                "id": "flxViewActionLevel",
                "isVisible": true,
                "layoutType": kony.flex.FLOW_VERTICAL,
                "left": "0",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0dp",
                "width": "33%",
                "zIndex": 1
            }, {}, {});
            flxViewActionLevel.setDefaultUnit(kony.flex.DP);
            var lblViewActionLevel = new kony.ui.Label({
                "id": "lblViewActionLevel",
                "isVisible": true,
                "left": "0px",
                "skin": "sknlblLato696c7313px",
                "text": "ACTION LEVEL",
                "top": "0px",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var lblViewActionLevelValue = new kony.ui.Label({
                "id": "lblViewActionLevelValue",
                "isVisible": true,
                "left": "0px",
                "skin": "sknLatoSemibold485c7313px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.userwidgetmodel.lblData1\")",
                "top": "10px",
                "width": "100%",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxViewActionLevel.add(lblViewActionLevel, lblViewActionLevelValue);
            var flxViewAccessLevel = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                "clipBounds": true,
                "id": "flxViewAccessLevel",
                "isVisible": true,
                "layoutType": kony.flex.FLOW_VERTICAL,
                "left": "0",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0dp",
                "width": "33%",
                "zIndex": 1
            }, {}, {});
            flxViewAccessLevel.setDefaultUnit(kony.flex.DP);
            var lblViewAccessLevel = new kony.ui.Label({
                "id": "lblViewAccessLevel",
                "isVisible": true,
                "left": "0px",
                "skin": "sknlblLato696c7313px",
                "text": "ACCESS LEVEL",
                "top": "0px",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var lblViewAccessLevelValue = new kony.ui.Label({
                "id": "lblViewAccessLevelValue",
                "isVisible": true,
                "left": "0px",
                "skin": "sknLatoSemibold485c7313px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.userwidgetmodel.lblData1\")",
                "top": "10px",
                "width": "100%",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxViewAccessLevel.add(lblViewAccessLevel, lblViewAccessLevelValue);
            var flxMFA = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                "clipBounds": true,
                "id": "flxMFA",
                "isVisible": true,
                "layoutType": kony.flex.FLOW_VERTICAL,
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0dp",
                "width": "33%",
                "zIndex": 1
            }, {}, {});
            flxMFA.setDefaultUnit(kony.flex.DP);
            var lblMFA = new kony.ui.Label({
                "id": "lblMFA",
                "isVisible": true,
                "left": "0px",
                "skin": "sknlblLato696c7313px",
                "text": "MULTI FACTOR AUTHENTICATION",
                "top": "0px",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var lblMFAValue = new kony.ui.Label({
                "id": "lblMFAValue",
                "isVisible": true,
                "left": "0px",
                "skin": "sknLatoSemibold485c7313px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.userwidgetmodel.lblData1\")",
                "top": "10px",
                "width": "100%",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxMFA.add(lblMFA, lblMFAValue);
            flxActionDetails2.add(flxViewActionLevel, flxViewAccessLevel, flxMFA);
            var flxActionDetails3 = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                "clipBounds": true,
                "id": "flxActionDetails3",
                "isVisible": true,
                "layoutType": kony.flex.FLOW_HORIZONTAL,
                "left": "20px",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "25dp",
                "width": "95%",
                "zIndex": 1
            }, {}, {});
            flxActionDetails3.setDefaultUnit(kony.flex.DP);
            var flxTAndC = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                "clipBounds": true,
                "id": "flxTAndC",
                "isVisible": true,
                "layoutType": kony.flex.FLOW_VERTICAL,
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0dp",
                "width": "33%",
                "zIndex": 1
            }, {}, {});
            flxTAndC.setDefaultUnit(kony.flex.DP);
            var lblTAndC = new kony.ui.Label({
                "id": "lblTAndC",
                "isVisible": true,
                "left": "0px",
                "skin": "sknlblLato696c7313px",
                "text": "TERMS AND CONDITION",
                "top": "0px",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var lblTAndCView = new kony.ui.Label({
                "id": "lblTAndCView",
                "isVisible": true,
                "left": "0px",
                "skin": "sknLblLato13px117eb0",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.permission.View\")",
                "top": "10px",
                "width": "100%",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {
                "hoverSkin": "sknlbl485c7513pxHoverCursor"
            });
            flxTAndC.add(lblTAndC, lblTAndCView);
            var flxViewDependency = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                "clipBounds": true,
                "id": "flxViewDependency",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0dp",
                "width": "33%",
                "zIndex": 1
            }, {}, {});
            flxViewDependency.setDefaultUnit(kony.flex.DP);
            var lblViewDependency = new kony.ui.Label({
                "id": "lblViewDependency",
                "isVisible": true,
                "left": "0px",
                "skin": "sknlblLato696c7313px",
                "text": "DEPENDENCIES",
                "top": "0px",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var lblViewDependencyValue = new kony.ui.Label({
                "id": "lblViewDependencyValue",
                "isVisible": true,
                "left": "0px",
                "skin": "sknLblLato13px117eb0",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.permission.View\")",
                "top": "25px",
                "width": "100%",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {
                "hoverSkin": "sknlbl485c7513pxHoverCursor"
            });
            var fontIconViewDependency = new kony.ui.Label({
                "id": "fontIconViewDependency",
                "isVisible": true,
                "left": "110dp",
                "skin": "sknfontIcon192b4b14px",
                "text": "",
                "textStyle": {},
                "top": 0,
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_TOP_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxViewDependency.add(lblViewDependency, lblViewDependencyValue, fontIconViewDependency);
            var flxViewStatus = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                "clipBounds": true,
                "id": "flxViewStatus",
                "isVisible": true,
                "layoutType": kony.flex.FLOW_VERTICAL,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0dp",
                "width": "33%"
            }, {}, {});
            flxViewStatus.setDefaultUnit(kony.flex.DP);
            var lblViewStatus = new kony.ui.Label({
                "id": "lblViewStatus",
                "isVisible": true,
                "left": "0px",
                "skin": "sknlblLato696c7313px",
                "text": "STATUS",
                "top": "0px",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var flxActionStatusContainer = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                "clipBounds": false,
                "id": "flxActionStatusContainer",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0px",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "10px",
                "width": "100%",
                "zIndex": 1
            }, {}, {});
            flxActionStatusContainer.setDefaultUnit(kony.flex.DP);
            var flxActionStatusValue = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                "clipBounds": true,
                "id": "flxActionStatusValue",
                "isVisible": true,
                "layoutType": kony.flex.FLOW_HORIZONTAL,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": 0,
                "width": "100%"
            }, {}, {});
            flxActionStatusValue.setDefaultUnit(kony.flex.DP);
            var lblActionActiveText = new kony.ui.Label({
                "height": "20px",
                "id": "lblActionActiveText",
                "isVisible": true,
                "left": "0px",
                "skin": "sknLbl485C75LatoRegular13Px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmAlertsManagement.Active\")",
                "top": "3px",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var flxActionEditAlertStatusSwitch = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "25px",
                "id": "flxActionEditAlertStatusSwitch",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "20px",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0px",
                "width": "38px",
                "zIndex": 1
            }, {}, {});
            flxActionEditAlertStatusSwitch.setDefaultUnit(kony.flex.DP);
            var actionStatusSwitch = new com.adminConsole.common.customSwitch({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "height": "25dp",
                "id": "actionStatusSwitch",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0",
                "masterType": constants.MASTER_TYPE_DEFAULT,
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0",
                "width": "100%",
                "overrides": {
                    "customSwitch": {
                        "height": "25dp",
                        "isVisible": true,
                        "left": "0",
                        "top": "0"
                    },
                    "switchToggle": {
                        "isVisible": true
                    }
                }
            }, {
                "overrides": {}
            }, {
                "overrides": {}
            });
            flxActionEditAlertStatusSwitch.add(actionStatusSwitch);
            flxActionStatusValue.add(lblActionActiveText, flxActionEditAlertStatusSwitch);
            flxActionStatusContainer.add(flxActionStatusValue);
            flxViewStatus.add(lblViewStatus, flxActionStatusContainer);
            flxActionDetails3.add(flxTAndC, flxViewDependency, flxViewStatus);
            var flxDisplayContentEdit = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                "clipBounds": true,
                "id": "flxDisplayContentEdit",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "20dp",
                "width": "100%"
            }, {}, {});
            flxDisplayContentEdit.setDefaultUnit(kony.flex.DP);
            var displayContentEdit = new com.adminConsole.ServiceManagement.displayContentEdit({
                "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                "id": "displayContentEdit",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "masterType": constants.MASTER_TYPE_DEFAULT,
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "5dp",
                "width": "100%",
                "overrides": {
                    "EditLangList.flxSearchContainer": {
                        "isVisible": false
                    },
                    "displayContentEdit": {
                        "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                        "bottom": "viz.val_cleared",
                        "centerX": "viz.val_cleared",
                        "centerY": "viz.val_cleared",
                        "left": "0dp",
                        "maxHeight": "viz.val_cleared",
                        "maxWidth": "viz.val_cleared",
                        "minHeight": "viz.val_cleared",
                        "minWidth": "viz.val_cleared",
                        "right": "viz.val_cleared",
                        "top": "5dp",
                        "width": "100%"
                    }
                }
            }, {
                "overrides": {}
            }, {
                "overrides": {}
            });
            flxDisplayContentEdit.add(displayContentEdit);
            var flxActionDetails4 = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                "clipBounds": true,
                "id": "flxActionDetails4",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "20px",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "25dp",
                "width": "100%",
                "zIndex": 1
            }, {}, {});
            flxActionDetails4.setDefaultUnit(kony.flex.DP);
            var lblViewLimitGroup = new kony.ui.Label({
                "id": "lblViewLimitGroup",
                "isVisible": true,
                "left": "0px",
                "skin": "sknlblLato696c7313px",
                "text": "LIMIT GROUP",
                "top": "0px",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var lblViewLimitGroupValue = new kony.ui.Label({
                "id": "lblViewLimitGroupValue",
                "isVisible": true,
                "left": "0px",
                "skin": "sknLatoSemibold485c7313px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.userwidgetmodel.lblData1\")",
                "top": "25px",
                "width": "100%",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var fontIconViewLimitGroup = new kony.ui.Label({
                "id": "fontIconViewLimitGroup",
                "isVisible": true,
                "left": "95dp",
                "skin": "sknfontIcon192b4b14px",
                "text": "",
                "textStyle": {},
                "top": 0,
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_TOP_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxActionDetails4.add(lblViewLimitGroup, lblViewLimitGroupValue, fontIconViewLimitGroup);
            var flxFeaturedActionLimits = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                "bottom": "5dp",
                "clipBounds": true,
                "id": "flxFeaturedActionLimits",
                "isVisible": true,
                "layoutType": kony.flex.FLOW_VERTICAL,
                "left": "20dp",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "25dp",
                "width": "95%",
                "zIndex": 20
            }, {}, {});
            flxFeaturedActionLimits.setDefaultUnit(kony.flex.DP);
            var lblLimitsHeader = new kony.ui.Label({
                "id": "lblLimitsHeader",
                "isVisible": true,
                "left": "0px",
                "skin": "sknlblLato696c7313px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmServiceManagement.Limits\")",
                "top": "0px",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var flxActionLimitsHeaders = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                "clipBounds": true,
                "id": "flxActionLimitsHeaders",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "17dp",
                "width": "100%",
                "zIndex": 1
            }, {}, {});
            flxActionLimitsHeaders.setDefaultUnit(kony.flex.DP);
            var lblMinValHeader = new kony.ui.Label({
                "id": "lblMinValHeader",
                "isVisible": true,
                "left": "0dp",
                "skin": "sknLbl485c75FontSize12px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmServiceManagement.MinValueperTransactionLC\")",
                "top": "0dp",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var fontIconMinVal = new kony.ui.Label({
                "id": "fontIconMinVal",
                "isVisible": true,
                "left": "170dp",
                "skin": "sknfontIcon192b4b14px",
                "text": "",
                "textStyle": {},
                "top": 0,
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_TOP_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var lblMaxValHeader = new kony.ui.Label({
                "id": "lblMaxValHeader",
                "isVisible": true,
                "left": "33%",
                "skin": "sknLbl485c75FontSize12px",
                "text": "Maximum Transaction Limit",
                "top": "0dp",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var fontIconMaxVal = new kony.ui.Label({
                "id": "fontIconMaxVal",
                "isVisible": true,
                "left": "360dp",
                "skin": "sknfontIcon192b4b14px",
                "text": "",
                "textStyle": {},
                "top": 0,
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_TOP_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var lblMaxDailyLimitHeader = new kony.ui.Label({
                "id": "lblMaxDailyLimitHeader",
                "isVisible": true,
                "left": "66%",
                "skin": "sknLbl485c75FontSize12px",
                "text": "Maximum Daily Limit",
                "top": "0dp",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var fontIconMaxDailyVal = new kony.ui.Label({
                "id": "fontIconMaxDailyVal",
                "isVisible": true,
                "left": "535dp",
                "skin": "sknfontIcon192b4b14px",
                "text": "",
                "textStyle": {},
                "top": 0,
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_TOP_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxActionLimitsHeaders.add(lblMinValHeader, fontIconMinVal, lblMaxValHeader, fontIconMaxVal, lblMaxDailyLimitHeader, fontIconMaxDailyVal);
            var flxActionLimitsTextbox = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                "clipBounds": true,
                "id": "flxActionLimitsTextbox",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "10dp",
                "width": "100%"
            }, {}, {});
            flxActionLimitsTextbox.setDefaultUnit(kony.flex.DP);
            var valueEntryMinValueLimit = new com.adminConsole.Features.ValueEntry({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "height": "40dp",
                "id": "valueEntryMinValueLimit",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "masterType": constants.MASTER_TYPE_DEFAULT,
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0dp",
                "width": "30%",
                "overrides": {
                    "ValueEntry": {
                        "height": "40dp",
                        "width": "30%"
                    },
                    "tbxEnterValue": {
                        "centerY": "50%",
                        "top": "viz.val_cleared"
                    }
                }
            }, {
                "overrides": {}
            }, {
                "overrides": {}
            });
            var flxMinValueError = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                "clipBounds": true,
                "id": "flxMinValueError",
                "isVisible": false,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "40dp",
                "width": "30%",
                "zIndex": 1
            }, {}, {});
            flxMinValueError.setDefaultUnit(kony.flex.DP);
            var lblMinValueErrorIcon = new kony.ui.Label({
                "height": "15dp",
                "id": "lblMinValueErrorIcon",
                "isVisible": true,
                "left": "0dp",
                "skin": "sknErrorIcon",
                "text": "",
                "top": "0dp",
                "width": "15dp",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var lblMinValueLimitErrorMsg = new kony.ui.Label({
                "height": "15dp",
                "id": "lblMinValueLimitErrorMsg",
                "isVisible": true,
                "left": "15dp",
                "skin": "sknlblError",
                "text": "Value cannot be empty",
                "top": "0dp",
                "width": "100%",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxMinValueError.add(lblMinValueErrorIcon, lblMinValueLimitErrorMsg);
            var valueEntryMaxValueLimit = new com.adminConsole.Features.ValueEntry({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "height": "40dp",
                "id": "valueEntryMaxValueLimit",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "33%",
                "masterType": constants.MASTER_TYPE_DEFAULT,
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0dp",
                "width": "30%",
                "overrides": {
                    "ValueEntry": {
                        "height": "40dp",
                        "left": "33%",
                        "top": "0dp",
                        "width": "30%"
                    }
                }
            }, {
                "overrides": {}
            }, {
                "overrides": {}
            });
            var flxMaxValueExceedError = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                "clipBounds": true,
                "id": "flxMaxValueExceedError",
                "isVisible": false,
                "layoutType": kony.flex.FREE_FORM,
                "left": "33%",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "40dp",
                "width": "30%",
                "zIndex": 1
            }, {}, {});
            flxMaxValueExceedError.setDefaultUnit(kony.flex.DP);
            var lblMaxValueLimitErrorIcon = new kony.ui.Label({
                "height": "15dp",
                "id": "lblMaxValueLimitErrorIcon",
                "isVisible": true,
                "left": "0dp",
                "skin": "sknErrorIcon",
                "text": "",
                "top": "0dp",
                "width": "15dp",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var lblMaxValueLimitErrorMsg = new kony.ui.Label({
                "height": "15dp",
                "id": "lblMaxValueLimitErrorMsg",
                "isVisible": true,
                "left": "15dp",
                "skin": "sknlblError",
                "text": "Value cannot be empty",
                "top": "0dp",
                "width": "100%",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxMaxValueExceedError.add(lblMaxValueLimitErrorIcon, lblMaxValueLimitErrorMsg);
            var valueEntryMaxDailyValue = new com.adminConsole.Features.ValueEntry({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "height": "40dp",
                "id": "valueEntryMaxDailyValue",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "66%",
                "masterType": constants.MASTER_TYPE_DEFAULT,
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0dp",
                "width": "33%",
                "overrides": {
                    "ValueEntry": {
                        "height": "40dp",
                        "left": "66%",
                        "width": "33%"
                    }
                }
            }, {
                "overrides": {}
            }, {
                "overrides": {}
            });
            var flxMaxDailyLimitError = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                "clipBounds": true,
                "id": "flxMaxDailyLimitError",
                "isVisible": false,
                "layoutType": kony.flex.FREE_FORM,
                "left": "66%",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "40dp",
                "width": "30%",
                "zIndex": 1
            }, {}, {});
            flxMaxDailyLimitError.setDefaultUnit(kony.flex.DP);
            var lblMaxDailyLimitErrorIcon = new kony.ui.Label({
                "height": "15dp",
                "id": "lblMaxDailyLimitErrorIcon",
                "isVisible": true,
                "left": "0dp",
                "skin": "sknErrorIcon",
                "text": "",
                "top": "0dp",
                "width": "15dp",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var lblDailyLimitValueErrorMsg = new kony.ui.Label({
                "height": "15dp",
                "id": "lblDailyLimitValueErrorMsg",
                "isVisible": true,
                "left": "15dp",
                "skin": "sknlblError",
                "text": "Value cannot be empty",
                "top": "0dp",
                "width": "100%",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxMaxDailyLimitError.add(lblMaxDailyLimitErrorIcon, lblDailyLimitValueErrorMsg);
            flxActionLimitsTextbox.add(valueEntryMinValueLimit, flxMinValueError, valueEntryMaxValueLimit, flxMaxValueExceedError, valueEntryMaxDailyValue, flxMaxDailyLimitError);
            var flxActionLimitsHeaders01 = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                "clipBounds": true,
                "id": "flxActionLimitsHeaders01",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "25dp",
                "width": "100%"
            }, {}, {});
            flxActionLimitsHeaders01.setDefaultUnit(kony.flex.DP);
            var lblWeeklyLimitHeader = new kony.ui.Label({
                "id": "lblWeeklyLimitHeader",
                "isVisible": true,
                "left": "0dp",
                "skin": "sknLbl485c75FontSize12px",
                "text": "Maximum Weekly Limit",
                "top": "0dp",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var fontIconMaxWeeklyVal = new kony.ui.Label({
                "id": "fontIconMaxWeeklyVal",
                "isVisible": true,
                "left": "135dp",
                "skin": "sknfontIcon192b4b14px",
                "text": "",
                "textStyle": {},
                "top": 0,
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_TOP_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxActionLimitsHeaders01.add(lblWeeklyLimitHeader, fontIconMaxWeeklyVal);
            var flxActionLimitsTextbox01 = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                "bottom": "5dp",
                "clipBounds": true,
                "id": "flxActionLimitsTextbox01",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "10dp",
                "width": "100%"
            }, {}, {});
            flxActionLimitsTextbox01.setDefaultUnit(kony.flex.DP);
            var valueEntryWeeklyLimitValue = new com.adminConsole.Features.ValueEntry({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "height": "40dp",
                "id": "valueEntryWeeklyLimitValue",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "masterType": constants.MASTER_TYPE_DEFAULT,
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0dp",
                "width": "30%",
                "overrides": {
                    "ValueEntry": {
                        "height": "40dp",
                        "left": "0dp",
                        "top": "0dp",
                        "width": "30%"
                    }
                }
            }, {
                "overrides": {}
            }, {
                "overrides": {}
            });
            var flxWeeklyLimitValueError = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                "clipBounds": true,
                "id": "flxWeeklyLimitValueError",
                "isVisible": false,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "40dp",
                "width": "30%",
                "zIndex": 1
            }, {}, {});
            flxWeeklyLimitValueError.setDefaultUnit(kony.flex.DP);
            var lblMaxWeeklyLimitErrorIcon = new kony.ui.Label({
                "height": "15dp",
                "id": "lblMaxWeeklyLimitErrorIcon",
                "isVisible": true,
                "left": "0dp",
                "skin": "sknErrorIcon",
                "text": "",
                "top": "0dp",
                "width": "15dp",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var lblWeeklyLimitValueErrorMsg = new kony.ui.Label({
                "height": "15dp",
                "id": "lblWeeklyLimitValueErrorMsg",
                "isVisible": true,
                "left": "15dp",
                "skin": "sknlblError",
                "text": "Value cannot be empty",
                "top": "0dp",
                "width": "100%",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxWeeklyLimitValueError.add(lblMaxWeeklyLimitErrorIcon, lblWeeklyLimitValueErrorMsg);
            flxActionLimitsTextbox01.add(valueEntryWeeklyLimitValue, flxWeeklyLimitValueError);
            flxFeaturedActionLimits.add(lblLimitsHeader, flxActionLimitsHeaders, flxActionLimitsTextbox, flxActionLimitsHeaders01, flxActionLimitsTextbox01);
            flxActionLimits.add(flxActionDetails1, flxActionDetails2, flxActionDetails3, flxDisplayContentEdit, flxActionDetails4, flxFeaturedActionLimits);
            flxFeaturedAction.add(flxSegmentActions, flxActionLimits);
            flxFeatureEditContainer.add(flxCloseFeature, flxFeaturesHeading, flxFeatureTabs, flxFeatureContainer, flxFeaturedAction);
            var flxFeatureEditButtons = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "80px",
                "id": "flxFeatureEditButtons",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "isModalContainer": false,
                "skin": "sknflxBgE4E6EC",
                "top": "0px",
                "width": "100%",
                "zIndex": 1
            }, {}, {});
            flxFeatureEditButtons.setDefaultUnit(kony.flex.DP);
            var btnEditCancel = new kony.ui.Button({
                "centerY": "50%",
                "focusSkin": "sknbtnf7f7faLatoRegular4f555dBorder1px8b96a5",
                "height": "40px",
                "id": "btnEditCancel",
                "isVisible": true,
                "onClick": controller.AS_Button_fe48adad632c43c5b97a05155a450331,
                "right": "155px",
                "skin": "sknbtnf7f7faLatoRegular4f555dBorder1px8b96a5",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmCustomers.CANCEL\")",
                "top": "0%",
                "width": "120px",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_CENTER,
                "displayText": true,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {
                "hoverSkin": "sknbtnffffffLatoRegular4f555dBorder1px485c75"
            });
            var btnUpdate = new kony.ui.Button({
                "centerY": "50%",
                "focusSkin": "sknBtnBg003e75R20pxF13pxLatoReg",
                "height": "40dp",
                "id": "btnUpdate",
                "isVisible": true,
                "onClick": controller.AS_Button_c973a89c85624425bb033af39fa13bd2,
                "right": "20px",
                "skin": "sknBtnBg003e75R20pxF13pxLatoReg",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.ConfigurationBundles.buttonUpdate\")",
                "top": "0%",
                "width": "120dp",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_CENTER,
                "displayText": true,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {
                "hoverSkin": "sknBtnBg005198R20pxF13pxLatoReg"
            });
            flxFeatureEditButtons.add(btnEditCancel, btnUpdate);
            flxEditFeatureContainer.add(flxFeaturesTopColor, flxFeatureEditContainer, flxFeatureEditButtons);
            flxEditFeaturePopup.add(flxEditFeatureContainer);
            var flxViewActionTCPopup = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "100%",
                "id": "flxViewActionTCPopup",
                "isVisible": false,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "sknFocus50",
                "top": "0dp",
                "width": "100%",
                "zIndex": 6
            }, {}, {});
            flxViewActionTCPopup.setDefaultUnit(kony.flex.DP);
            var flxActionTaCContainer = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "centerX": "50%",
                "centerY": "50%",
                "clipBounds": true,
                "height": "70%",
                "id": "flxActionTaCContainer",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "17%",
                "isModalContainer": false,
                "right": "17%",
                "skin": "slFbox0j9f841cc563e4e",
                "zIndex": 1
            }, {}, {});
            flxActionTaCContainer.setDefaultUnit(kony.flex.DP);
            var flxTCPopupTopColor = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "10px",
                "id": "flxTCPopupTopColor",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0px",
                "isModalContainer": false,
                "skin": "sknFlxInfoToastBg357C9ENoRadius",
                "top": "0dp",
                "width": "100%",
                "zIndex": 1
            }, {}, {});
            flxTCPopupTopColor.setDefaultUnit(kony.flex.DP);
            flxTCPopupTopColor.add();
            var flxTCPopupLanguages = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                "clipBounds": true,
                "id": "flxTCPopupLanguages",
                "isVisible": false,
                "layoutType": kony.flex.FREE_FORM,
                "left": "330dp",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "60px",
                "width": "170dp",
                "zIndex": 11
            }, {}, {});
            flxTCPopupLanguages.setDefaultUnit(kony.flex.DP);
            var languagesListTCPopup = new com.adminConsole.Products.productTypeFilterMenu({
                "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                "id": "languagesListTCPopup",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "masterType": constants.MASTER_TYPE_DEFAULT,
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "5dp",
                "width": "100%",
                "zIndex": 5,
                "overrides": {
                    "flxChechboxOuter": {
                        "height": "120px"
                    },
                    "flxFilterSort": {
                        "isVisible": false
                    },
                    "imgUpArrow": {
                        "src": "uparrow_2x.png"
                    },
                    "lblSeperator": {
                        "isVisible": false
                    },
                    "segStatusFilterDropdown": {
                        "data": [{
                            "imgCheckBox": "checkbox.png",
                            "lblDescription": "Active"
                        }, {
                            "imgCheckBox": "checkbox.png",
                            "lblDescription": "Archived"
                        }, {
                            "imgCheckBox": "checkbox.png",
                            "lblDescription": "Draft"
                        }]
                    }
                }
            }, {
                "overrides": {}
            }, {
                "overrides": {}
            });
            flxTCPopupLanguages.add(languagesListTCPopup);
            var flxTCPopupBody = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                "clipBounds": true,
                "id": "flxTCPopupBody",
                "isVisible": true,
                "layoutType": kony.flex.FLOW_VERTICAL,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "10px",
                "width": "100%",
                "zIndex": 1
            }, {}, {});
            flxTCPopupBody.setDefaultUnit(kony.flex.DP);
            var flxActionTCClosePopup = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "25px",
                "id": "flxActionTCClosePopup",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "isModalContainer": false,
                "right": "13dp",
                "skin": "sknCursor",
                "top": "10dp",
                "width": "25px",
                "zIndex": 20
            }, {}, {
                "hoverSkin": "sknCursor"
            });
            flxActionTCClosePopup.setDefaultUnit(kony.flex.DP);
            var lblIconCloseTCPopup = new kony.ui.Label({
                "centerX": "51%",
                "centerY": "50.00%",
                "height": "15dp",
                "id": "lblIconCloseTCPopup",
                "isVisible": true,
                "left": "17dp",
                "skin": "sknFontIconSearchCross16px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmAlertsManagement.lblAlertPreviewClose\")",
                "width": "15dp",
                "zIndex": 2
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxActionTCClosePopup.add(lblIconCloseTCPopup);
            var flxActionTCPopupHeader = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "40dp",
                "id": "flxActionTCPopupHeader",
                "isVisible": true,
                "layoutType": kony.flex.FLOW_HORIZONTAL,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "-5dp",
                "width": "100%",
                "zIndex": 20
            }, {}, {});
            flxActionTCPopupHeader.setDefaultUnit(kony.flex.DP);
            var lblTCPopupHeader = new kony.ui.Label({
                "id": "lblTCPopupHeader",
                "isVisible": true,
                "left": "20dp",
                "maxWidth": "65%",
                "skin": "sknLbl192b45LatoReg16px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.TermsAndConditions.viewTandC\")",
                "top": "0dp",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var flxTcLanguageDropdown = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "25dp",
                "id": "flxTcLanguageDropdown",
                "isVisible": true,
                "layoutType": kony.flex.FLOW_HORIZONTAL,
                "left": "25dp",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0dp",
                "width": "180dp",
                "zIndex": 2
            }, {}, {});
            flxTcLanguageDropdown.setDefaultUnit(kony.flex.DP);
            var lblSelectedLangTCPopup = new kony.ui.Label({
                "centerY": "50%",
                "id": "lblSelectedLangTCPopup",
                "isVisible": true,
                "left": "0dp",
                "skin": "sknLbl485C75LatoRegular13Px",
                "text": "English (United States)",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_TOP_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var lblIconArrowTCPopup = new kony.ui.Label({
                "centerY": "50%",
                "id": "lblIconArrowTCPopup",
                "isVisible": true,
                "left": "10dp",
                "skin": "sknicon15pxBlack",
                "text": "",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxTcLanguageDropdown.add(lblSelectedLangTCPopup, lblIconArrowTCPopup);
            flxActionTCPopupHeader.add(lblTCPopupHeader, flxTcLanguageDropdown);
            var flxActionTCDetails = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                "bottom": "30dp",
                "clipBounds": true,
                "id": "flxActionTCDetails",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "20dp",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0dp",
                "width": "95%",
                "zIndex": 20
            }, {}, {});
            flxActionTCDetails.setDefaultUnit(kony.flex.DP);
            var rtxContentViewer = new kony.ui.Browser({
                "bottom": "10dp",
                "detectTelNumber": true,
                "enableNativeCommunication": false,
                "enableZoom": false,
                "height": "300dp",
                "id": "rtxContentViewer",
                "isVisible": true,
                "left": "0px",
                "setAsContent": false,
                "requestURLConfig": {
                    "URL": "richtextViewer.html",
                    "requestMethod": constants.BROWSER_REQUEST_METHOD_GET
                },
                "top": "20px",
                "width": "100%",
                "zIndex": 1
            }, {}, {});
            var flxNoTermsConditionsAvailable = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "centerX": "50%",
                "clipBounds": true,
                "height": "100dp",
                "id": "flxNoTermsConditionsAvailable",
                "isVisible": false,
                "layoutType": kony.flex.FREE_FORM,
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "100dp",
                "width": "100%",
                "zIndex": 5
            }, {}, {});
            flxNoTermsConditionsAvailable.setDefaultUnit(kony.flex.DP);
            var lblNoTCAvailable = new kony.ui.Label({
                "centerX": "50%",
                "centerY": "50%",
                "id": "lblNoTCAvailable",
                "isVisible": true,
                "skin": "sknLblLato84939E12px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.TermsAndConditions.NoContentAdded\")",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_CENTER,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxNoTermsConditionsAvailable.add(lblNoTCAvailable);
            flxActionTCDetails.add(rtxContentViewer, flxNoTermsConditionsAvailable);
            flxTCPopupBody.add(flxActionTCClosePopup, flxActionTCPopupHeader, flxActionTCDetails);
            flxActionTaCContainer.add(flxTCPopupTopColor, flxTCPopupLanguages, flxTCPopupBody);
            flxViewActionTCPopup.add(flxActionTaCContainer);
            var flxLimitGroupPopup = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "100%",
                "id": "flxLimitGroupPopup",
                "isVisible": false,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "sknFocus50",
                "top": "0dp",
                "width": "100%",
                "zIndex": 5
            }, {}, {});
            flxLimitGroupPopup.setDefaultUnit(kony.flex.DP);
            var flxEditLimitGroupContainer = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                "centerX": "50%",
                "centerY": "50%",
                "clipBounds": true,
                "id": "flxEditLimitGroupContainer",
                "isVisible": true,
                "layoutType": kony.flex.FLOW_VERTICAL,
                "isModalContainer": false,
                "skin": "slFbox0j9f841cc563e4e",
                "width": "60%",
                "zIndex": 1
            }, {}, {});
            flxEditLimitGroupContainer.setDefaultUnit(kony.flex.DP);
            var flxLimitGroupTopColor = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "10px",
                "id": "flxLimitGroupTopColor",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0px",
                "isModalContainer": false,
                "skin": "sknFlxInfoToastBg357C9ENoRadius",
                "top": "0dp",
                "width": "100%",
                "zIndex": 1
            }, {}, {});
            flxLimitGroupTopColor.setDefaultUnit(kony.flex.DP);
            flxLimitGroupTopColor.add();
            var flxLimitGroupEditContainer = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                "clipBounds": true,
                "id": "flxLimitGroupEditContainer",
                "isVisible": true,
                "layoutType": kony.flex.FLOW_VERTICAL,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0px",
                "width": "100%",
                "zIndex": 1
            }, {}, {});
            flxLimitGroupEditContainer.setDefaultUnit(kony.flex.DP);
            var flxCloseLimitGroup = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "25px",
                "id": "flxCloseLimitGroup",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "isModalContainer": false,
                "right": "13dp",
                "skin": "sknCursor",
                "top": "10dp",
                "width": "25px",
                "zIndex": 20
            }, {}, {
                "hoverSkin": "sknCursor"
            });
            flxCloseLimitGroup.setDefaultUnit(kony.flex.DP);
            var fontIconImgCloseLimitGroup = new kony.ui.Label({
                "centerX": "51%",
                "centerY": "50.00%",
                "height": "15dp",
                "id": "fontIconImgCloseLimitGroup",
                "isVisible": true,
                "left": "17dp",
                "skin": "sknFontIconSearchCross16px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmAlertsManagement.lblAlertPreviewClose\")",
                "width": "15dp",
                "zIndex": 2
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxCloseLimitGroup.add(fontIconImgCloseLimitGroup);
            var flxLimitGroupHeading = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "40dp",
                "id": "flxLimitGroupHeading",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0dp",
                "width": "100%",
                "zIndex": 20
            }, {}, {});
            flxLimitGroupHeading.setDefaultUnit(kony.flex.DP);
            var lblEditLimitGroupHeader = new kony.ui.Label({
                "id": "lblEditLimitGroupHeader",
                "isVisible": true,
                "left": "20dp",
                "skin": "sknLbl192b45LatoReg16px",
                "text": "Edit Limit Group",
                "top": 0,
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var btnEditLimitGroup = new kony.ui.Button({
                "height": "22dp",
                "id": "btnEditLimitGroup",
                "isVisible": false,
                "right": "20dp",
                "skin": "sknbtnf7f7faLatoRegular12Px485c75Border1px485C75Radius20px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.permission.Edit\")",
                "top": "15dp",
                "width": "50dp",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_CENTER,
                "displayText": true,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {
                "hoverSkin": "sknbtnffffffLatoRegular12Px485c75Border1px485C75Radius20px"
            });
            flxLimitGroupHeading.add(lblEditLimitGroupHeader, btnEditLimitGroup);
            var lblLimitGroupPopupHeaderSeperator = new kony.ui.Label({
                "height": "1px",
                "id": "lblLimitGroupPopupHeaderSeperator",
                "isVisible": true,
                "left": "20dp",
                "skin": "sknlblSeperator",
                "text": ".",
                "top": "19dp",
                "width": "95%",
                "zIndex": 2
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var flxLimitGroupDetails = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "60dp",
                "id": "flxLimitGroupDetails",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "20dp",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "20dp",
                "width": "33%",
                "zIndex": 1
            }, {}, {});
            flxLimitGroupDetails.setDefaultUnit(kony.flex.DP);
            var lblLimitGroupDetails = new kony.ui.Label({
                "height": "20dp",
                "id": "lblLimitGroupDetails",
                "isVisible": true,
                "left": "0dp",
                "skin": "sknlblLato696c7312px",
                "text": "LIMIT GROUP CODE",
                "top": "5dp",
                "width": "100%",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var lblLimitGroupDetailsValue = new kony.ui.Label({
                "id": "lblLimitGroupDetailsValue",
                "isVisible": true,
                "left": "0dp",
                "skin": "sknLatoSemibold485c7313px",
                "text": "Single Payment",
                "top": "35dp",
                "width": "100%",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxLimitGroupDetails.add(lblLimitGroupDetails, lblLimitGroupDetailsValue);
            var flxDisplayContentLimitGroup = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "335dp",
                "id": "flxDisplayContentLimitGroup",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "20dp",
                "width": "100%"
            }, {}, {});
            flxDisplayContentLimitGroup.setDefaultUnit(kony.flex.DP);
            var displayContentLimitGroupEdit = new com.adminConsole.ServiceManagement.displayContentEdit({
                "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                "id": "displayContentLimitGroupEdit",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "masterType": constants.MASTER_TYPE_DEFAULT,
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "5dp",
                "width": "100%",
                "overrides": {
                    "EditLangList.flxSearchContainer": {
                        "isVisible": false
                    },
                    "displayContentEdit": {
                        "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                        "bottom": "viz.val_cleared",
                        "centerX": "viz.val_cleared",
                        "centerY": "viz.val_cleared",
                        "left": "0dp",
                        "maxHeight": "viz.val_cleared",
                        "maxWidth": "viz.val_cleared",
                        "minHeight": "viz.val_cleared",
                        "minWidth": "viz.val_cleared",
                        "right": "viz.val_cleared",
                        "top": "5dp",
                        "width": "100%"
                    }
                }
            }, {
                "overrides": {}
            }, {
                "overrides": {}
            });
            flxDisplayContentLimitGroup.add(displayContentLimitGroupEdit);
            var flxViewDisplayContentLimitGroup = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                "bottom": "30dp",
                "clipBounds": true,
                "id": "flxViewDisplayContentLimitGroup",
                "isVisible": false,
                "layoutType": kony.flex.FREE_FORM,
                "left": "20dp",
                "isModalContainer": false,
                "right": "20dp",
                "skin": "slFbox",
                "width": "95%"
            }, {}, {});
            flxViewDisplayContentLimitGroup.setDefaultUnit(kony.flex.DP);
            var displayContentView = new com.adminConsole.ServiceManagement.displayContentView({
                "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                "id": "displayContentView",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "masterType": constants.MASTER_TYPE_DEFAULT,
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "5dp",
                "width": "100%",
                "overrides": {
                    "EditLangList.flxSearchContainer": {
                        "isVisible": false
                    },
                    "displayContentView": {
                        "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                        "bottom": "viz.val_cleared",
                        "centerX": "viz.val_cleared",
                        "centerY": "viz.val_cleared",
                        "left": "0dp",
                        "maxHeight": "viz.val_cleared",
                        "maxWidth": "viz.val_cleared",
                        "minHeight": "viz.val_cleared",
                        "minWidth": "viz.val_cleared",
                        "right": "viz.val_cleared",
                        "top": "5dp",
                        "width": "100%"
                    }
                }
            }, {
                "overrides": {}
            }, {
                "overrides": {}
            });
            flxViewDisplayContentLimitGroup.add(displayContentView);
            flxLimitGroupEditContainer.add(flxCloseLimitGroup, flxLimitGroupHeading, lblLimitGroupPopupHeaderSeperator, flxLimitGroupDetails, flxDisplayContentLimitGroup, flxViewDisplayContentLimitGroup);
            var flxLimitGroupEditButtons = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "80px",
                "id": "flxLimitGroupEditButtons",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "isModalContainer": false,
                "skin": "sknflxBgE4E6EC",
                "top": "0px",
                "width": "100%",
                "zIndex": 1
            }, {}, {});
            flxLimitGroupEditButtons.setDefaultUnit(kony.flex.DP);
            var btnCancelLimitGroupPopup = new kony.ui.Button({
                "centerY": "50%",
                "focusSkin": "sknbtnf7f7faLatoRegular4f555dBorder1px8b96a5",
                "height": "40px",
                "id": "btnCancelLimitGroupPopup",
                "isVisible": true,
                "left": "20dp",
                "onClick": controller.AS_Button_fe48adad632c43c5b97a05155a450331,
                "skin": "sknbtnf7f7faLatoRegular4f555dBorder1px8b96a5",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmCustomers.CANCEL\")",
                "top": "0%",
                "width": "120px",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_CENTER,
                "displayText": true,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {
                "hoverSkin": "sknbtnffffffLatoRegular4f555dBorder1px485c75"
            });
            var btnUpdateLimitGroup = new kony.ui.Button({
                "centerY": "50%",
                "focusSkin": "sknBtnBg003e75R20pxF13pxLatoReg",
                "height": "40dp",
                "id": "btnUpdateLimitGroup",
                "isVisible": true,
                "onClick": controller.AS_Button_c973a89c85624425bb033af39fa13bd2,
                "right": "20px",
                "skin": "sknBtnBg003e75R20pxF13pxLatoReg",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.ConfigurationBundles.buttonUpdate\")",
                "top": "0%",
                "width": "120dp",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_CENTER,
                "displayText": true,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {
                "hoverSkin": "sknBtnBg005198R20pxF13pxLatoReg"
            });
            flxLimitGroupEditButtons.add(btnCancelLimitGroupPopup, btnUpdateLimitGroup);
            flxEditLimitGroupContainer.add(flxLimitGroupTopColor, flxLimitGroupEditContainer, flxLimitGroupEditButtons);
            flxLimitGroupPopup.add(flxEditLimitGroupContainer);
            var flxDependencyPopup = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "100%",
                "id": "flxDependencyPopup",
                "isVisible": false,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "sknFocus50",
                "top": "0dp",
                "width": "100%",
                "zIndex": 5
            }, {}, {});
            flxDependencyPopup.setDefaultUnit(kony.flex.DP);
            var flxViewDependencyContainer = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                "centerX": "50%",
                "centerY": "50%",
                "clipBounds": true,
                "id": "flxViewDependencyContainer",
                "isVisible": true,
                "layoutType": kony.flex.FLOW_VERTICAL,
                "isModalContainer": false,
                "skin": "slFbox0j9f841cc563e4e",
                "width": "46%",
                "zIndex": 1
            }, {}, {});
            flxViewDependencyContainer.setDefaultUnit(kony.flex.DP);
            var flxDependencyTopColor = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "10px",
                "id": "flxDependencyTopColor",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0px",
                "isModalContainer": false,
                "skin": "sknFlxInfoToastBg357C9ENoRadius",
                "top": "0dp",
                "width": "100%",
                "zIndex": 1
            }, {}, {});
            flxDependencyTopColor.setDefaultUnit(kony.flex.DP);
            flxDependencyTopColor.add();
            var flxDependencyViewContainer = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                "clipBounds": true,
                "id": "flxDependencyViewContainer",
                "isVisible": true,
                "layoutType": kony.flex.FLOW_VERTICAL,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0px",
                "width": "100%",
                "zIndex": 1
            }, {}, {});
            flxDependencyViewContainer.setDefaultUnit(kony.flex.DP);
            var flxCloseDependency = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "25px",
                "id": "flxCloseDependency",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "isModalContainer": false,
                "right": "13dp",
                "skin": "sknCursor",
                "top": "10dp",
                "width": "25px",
                "zIndex": 20
            }, {}, {
                "hoverSkin": "sknCursor"
            });
            flxCloseDependency.setDefaultUnit(kony.flex.DP);
            var fontIconImgCloseDependency = new kony.ui.Label({
                "centerX": "51%",
                "centerY": "50.00%",
                "height": "15dp",
                "id": "fontIconImgCloseDependency",
                "isVisible": true,
                "left": "17dp",
                "skin": "sknFontIconSearchCross16px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmAlertsManagement.lblAlertPreviewClose\")",
                "width": "15dp",
                "zIndex": 2
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxCloseDependency.add(fontIconImgCloseDependency);
            var flxDependencyHeading = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "20dp",
                "id": "flxDependencyHeading",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0dp",
                "width": "100%",
                "zIndex": 20
            }, {}, {});
            flxDependencyHeading.setDefaultUnit(kony.flex.DP);
            var lblDependencyHeading = new kony.ui.Label({
                "centerY": "48%",
                "id": "lblDependencyHeading",
                "isVisible": true,
                "left": "20dp",
                "skin": "sknLbl192b45LatoReg16px",
                "text": "Dependencies",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxDependencyHeading.add(lblDependencyHeading);
            var flxDependencyList = new kony.ui.FlexScrollContainer({
                "allowHorizontalBounce": false,
                "allowVerticalBounce": true,
                "bottom": "20dp",
                "bounces": true,
                "clipBounds": true,
                "enableScrolling": true,
                "height": "250dp",
                "horizontalScrollIndicator": true,
                "id": "flxDependencyList",
                "isVisible": true,
                "layoutType": kony.flex.FLOW_VERTICAL,
                "left": "20dp",
                "pagingEnabled": false,
                "right": "20dp",
                "scrollDirection": kony.flex.SCROLL_VERTICAL,
                "skin": "sknFlxBgFFFFFFBore4e6ec3Px",
                "top": "20dp",
                "verticalScrollIndicator": true,
                "width": "94%"
            }, {}, {});
            flxDependencyList.setDefaultUnit(kony.flex.DP);
            var flxDependencyHeader = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "40dp",
                "id": "flxDependencyHeader",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "sknf5f6f8border0px",
                "top": "0dp",
                "width": "100%",
                "zIndex": 1
            }, {}, {});
            flxDependencyHeader.setDefaultUnit(kony.flex.DP);
            var lblDependencyHeader = new kony.ui.Label({
                "centerY": "50%",
                "id": "lblDependencyHeader",
                "isVisible": true,
                "left": "20dp",
                "skin": "sknLbl192b45LatoReg16px",
                "text": "Actions(4)",
                "top": "0dp",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var lblDependencyHeaderSeparator = new kony.ui.Label({
                "height": "1px",
                "id": "lblDependencyHeaderSeparator",
                "isVisible": true,
                "left": "0dp",
                "skin": "sknlblSeperator",
                "text": ".",
                "top": "39dp",
                "width": "100%",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_BOTTOM_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxDependencyHeader.add(lblDependencyHeader, lblDependencyHeaderSeparator);
            var flxDependencySegmentHeader = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "50dp",
                "id": "flxDependencySegmentHeader",
                "isVisible": true,
                "layoutType": kony.flex.FLOW_HORIZONTAL,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0dp",
                "width": "100%"
            }, {}, {});
            flxDependencySegmentHeader.setDefaultUnit(kony.flex.DP);
            var flxDependencyFeatureNameHeader = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "100%",
                "id": "flxDependencyFeatureNameHeader",
                "isVisible": true,
                "layoutType": kony.flex.FLOW_HORIZONTAL,
                "left": "5%",
                "isModalContainer": false,
                "onClick": controller.AS_FlexContainer_af76450867de4196a1e2508dc8c1eae6,
                "skin": "slFbox",
                "top": 0,
                "width": "28%",
                "zIndex": 1
            }, {}, {});
            flxDependencyFeatureNameHeader.setDefaultUnit(kony.flex.DP);
            var lblDependencyNameHeader = new kony.ui.Label({
                "centerY": "50%",
                "id": "lblDependencyNameHeader",
                "isVisible": true,
                "left": "0px",
                "skin": "sknlblLato696c7312px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.products.Feature_Name_CAPS\")",
                "top": 0,
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {
                "hoverSkin": "sknlblLatoBold00000012px"
            });
            var fontIconSortDependencyFeatureName = new kony.ui.Label({
                "centerY": "50%",
                "id": "fontIconSortDependencyFeatureName",
                "isVisible": true,
                "left": "5dp",
                "skin": "sknIcon12pxBlack",
                "text": "",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {
                "hoverSkin": "sknIcon12pxBlackHover"
            });
            flxDependencyFeatureNameHeader.add(lblDependencyNameHeader, fontIconSortDependencyFeatureName);
            var flxDependencyActionNameHeader = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "100%",
                "id": "flxDependencyActionNameHeader",
                "isVisible": true,
                "layoutType": kony.flex.FLOW_HORIZONTAL,
                "left": "5%",
                "isModalContainer": false,
                "onClick": controller.AS_FlexContainer_af76450867de4196a1e2508dc8c1eae6,
                "skin": "slFbox",
                "top": 0,
                "width": "28%",
                "zIndex": 1
            }, {}, {});
            flxDependencyActionNameHeader.setDefaultUnit(kony.flex.DP);
            var lblDependencyActionNameHeader = new kony.ui.Label({
                "centerY": "50%",
                "id": "lblDependencyActionNameHeader",
                "isVisible": true,
                "left": "0px",
                "skin": "sknlblLato696c7312px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmServiceManagement.ActionNameUC\")",
                "top": 0,
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {
                "hoverSkin": "sknlblLatoBold00000012px"
            });
            var fontIconSortDependencyActionName = new kony.ui.Label({
                "centerY": "50%",
                "id": "fontIconSortDependencyActionName",
                "isVisible": true,
                "left": "5dp",
                "skin": "sknIcon12pxBlack",
                "text": "",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {
                "hoverSkin": "sknIcon12pxBlackHover"
            });
            flxDependencyActionNameHeader.add(lblDependencyActionNameHeader, fontIconSortDependencyActionName);
            var flxDependencyActionCodeHeader = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "100%",
                "id": "flxDependencyActionCodeHeader",
                "isVisible": true,
                "layoutType": kony.flex.FLOW_HORIZONTAL,
                "left": "5%",
                "isModalContainer": false,
                "onClick": controller.AS_FlexContainer_af76450867de4196a1e2508dc8c1eae6,
                "skin": "slFbox",
                "top": 0,
                "width": "28%",
                "zIndex": 1
            }, {}, {});
            flxDependencyActionCodeHeader.setDefaultUnit(kony.flex.DP);
            var lblDependencyActionCodeHeader = new kony.ui.Label({
                "centerY": "50%",
                "id": "lblDependencyActionCodeHeader",
                "isVisible": true,
                "left": "0px",
                "skin": "sknlblLato696c7312px",
                "text": "ACTION CODE",
                "top": 0,
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {
                "hoverSkin": "sknlblLatoBold00000012px"
            });
            var fontIconSortDependencyActionCode = new kony.ui.Label({
                "centerY": "50%",
                "id": "fontIconSortDependencyActionCode",
                "isVisible": true,
                "left": "5dp",
                "skin": "sknIcon12pxBlack",
                "text": "",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {
                "hoverSkin": "sknIcon12pxBlackHover"
            });
            flxDependencyActionCodeHeader.add(lblDependencyActionCodeHeader, fontIconSortDependencyActionCode);
            flxDependencySegmentHeader.add(flxDependencyFeatureNameHeader, flxDependencyActionNameHeader, flxDependencyActionCodeHeader);
            var lblSeperatorLine = new kony.ui.Label({
                "centerX": "50%",
                "height": "1dp",
                "id": "lblSeperatorLine",
                "isVisible": true,
                "left": "0dp",
                "skin": "sknlblSeperatorBgD5D9DD",
                "text": "Label",
                "top": "0dp",
                "width": "94%",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var flxDependencySegment = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                "clipBounds": true,
                "id": "flxDependencySegment",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0dp",
                "width": "100%",
                "zIndex": 1
            }, {}, {});
            flxDependencySegment.setDefaultUnit(kony.flex.DP);
            var segDependencyList = new kony.ui.SegmentedUI2({
                "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                "data": [{
                    "lblActionCode": "Label",
                    "lblActionName": "Label",
                    "lblFeatureName": "Label",
                    "lblSeperatorLine": "Label"
                }, {
                    "lblActionCode": "Label",
                    "lblActionName": "Label",
                    "lblFeatureName": "Label",
                    "lblSeperatorLine": "Label"
                }],
                "groupCells": false,
                "id": "segDependencyList",
                "isVisible": true,
                "left": "0dp",
                "needPageIndicator": true,
                "pageOffDotImage": "pageoffdot.png",
                "pageOnDotImage": "pageondot.png",
                "retainSelection": false,
                "rowFocusSkin": "seg2Focus",
                "rowSkin": "seg2Normal",
                "rowTemplate": "flxDependency",
                "sectionHeaderSkin": "sliPhoneSegmentHeader",
                "selectionBehavior": constants.SEGUI_DEFAULT_BEHAVIOR,
                "separatorRequired": false,
                "showScrollbars": false,
                "top": "0dp",
                "viewType": constants.SEGUI_VIEW_TYPE_TABLEVIEW,
                "widgetDataMap": {
                    "flxDependency": "flxDependency",
                    "flxDependencyDetails": "flxDependencyDetails",
                    "lblActionCode": "lblActionCode",
                    "lblActionName": "lblActionName",
                    "lblFeatureName": "lblFeatureName",
                    "lblSeperatorLine": "lblSeperatorLine"
                },
                "width": "100%",
                "zIndex": 1
            }, {
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var lblDependencyListNoResultsFound = new kony.ui.Label({
                "centerX": "50%",
                "height": "150dp",
                "id": "lblDependencyListNoResultsFound",
                "isVisible": false,
                "skin": "sknLbl485c75FontSize12px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmUsersController.No_results_found\")",
                "top": "0dp",
                "width": "500dp",
                "zIndex": 5
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_CENTER,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxDependencySegment.add(segDependencyList, lblDependencyListNoResultsFound);
            flxDependencyList.add(flxDependencyHeader, flxDependencySegmentHeader, lblSeperatorLine, flxDependencySegment);
            flxDependencyViewContainer.add(flxCloseDependency, flxDependencyHeading, flxDependencyList);
            flxViewDependencyContainer.add(flxDependencyTopColor, flxDependencyViewContainer);
            flxDependencyPopup.add(flxViewDependencyContainer);
            var flxActivateActionPopup = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "100%",
                "id": "flxActivateActionPopup",
                "isVisible": false,
                "layoutType": kony.flex.FREE_FORM,
                "left": "10dp",
                "masterType": constants.MASTER_TYPE_USERWIDGET,
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "10dp",
                "width": "100%",
                "zIndex": 5
            }, {}, {});
            flxActivateActionPopup.setDefaultUnit(kony.flex.DP);
            var popupError = new com.adminConsole.common.popupError({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "height": "100%",
                "id": "popupError",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "masterType": constants.MASTER_TYPE_DEFAULT,
                "isModalContainer": false,
                "skin": "sknFlxBg222B35Op50PopupBg",
                "top": "0dp",
                "width": "100%",
                "overrides": {
                    "btnPopUpCancel": {
                        "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmErrorLogin.closeCAPS\")",
                        "minWidth": "90px",
                        "right": "20px",
                        "width": "110px"
                    },
                    "btnPopUpDelete": {
                        "i18n_text": "kony.i18n.getLocalizedString(\"i8n.navigation.okButton\")",
                        "minWidth": "83px",
                        "width": "100px"
                    },
                    "lblPopUpMainMessage": {
                        "i18n_text": "kony.i18n.getLocalizedString(\"i8n.navigation.cancelChanges\")"
                    },
                    "popupError": {
                        "bottom": "viz.val_cleared",
                        "centerX": "viz.val_cleared",
                        "centerY": "viz.val_cleared",
                        "height": "100%",
                        "left": "0dp",
                        "maxHeight": "viz.val_cleared",
                        "maxWidth": "viz.val_cleared",
                        "minHeight": "viz.val_cleared",
                        "minWidth": "viz.val_cleared",
                        "right": "viz.val_cleared",
                        "top": "0dp",
                        "width": "100%"
                    },
                    "rtxPopUpDisclaimer": {
                        "i18n_text": "kony.i18n.getLocalizedString(\"i8n.navigation.cancelChangesBody\")"
                    }
                }
            }, {
                "overrides": {}
            }, {
                "overrides": {}
            });
            flxActivateActionPopup.add(popupError);
            var ToolTip = new com.adminConsole.Customers.ToolTip({
                "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                "id": "ToolTip",
                "isVisible": false,
                "layoutType": kony.flex.FREE_FORM,
                "left": "298dp",
                "masterType": constants.MASTER_TYPE_DEFAULT,
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "77dp",
                "width": "230px",
                "zIndex": 10,
                "overrides": {
                    "ToolTip": {
                        "isVisible": false,
                        "left": "298dp",
                        "top": "77dp",
                        "zIndex": 10
                    },
                    "flxToolTipMessage": {
                        "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                        "centerX": "50%",
                        "left": "0dp",
                        "minHeight": "70dp",
                        "top": "9dp",
                        "width": "210dp"
                    },
                    "lblNoConcentToolTip": {
                        "bottom": "10dp",
                        "height": kony.flex.USE_PREFFERED_SIZE,
                        "left": "5dp",
                        "top": "10dp",
                        "width": "195dp"
                    },
                    "lblarrow": {
                        "centerX": "viz.val_cleared",
                        "height": "10dp",
                        "left": "viz.val_cleared",
                        "right": "20dp",
                        "width": "17dp"
                    }
                }
            }, {
                "overrides": {}
            }, {
                "overrides": {}
            });
            flxServiceManagement.add(flxLeftPannel, flxRightPanel, flxToastMessage, flxDeactivateServiceManagement, flxLoading, flxViewEditLimitsPopup, flxEditFeaturePopup, flxViewActionTCPopup, flxLimitGroupPopup, flxDependencyPopup, flxActivateActionPopup, ToolTip);
            var flxEditCancelConfirmation = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "100%",
                "id": "flxEditCancelConfirmation",
                "isVisible": false,
                "layoutType": kony.flex.FREE_FORM,
                "left": "10dp",
                "masterType": constants.MASTER_TYPE_USERWIDGET,
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "10dp",
                "width": "100%",
                "zIndex": 5
            }, {}, {});
            flxEditCancelConfirmation.setDefaultUnit(kony.flex.DP);
            var popUpCancelEdits = new com.adminConsole.common.popUpCancelEdits({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "height": "100%",
                "id": "popUpCancelEdits",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "masterType": constants.MASTER_TYPE_DEFAULT,
                "isModalContainer": false,
                "skin": "sknflxBg000000Op50",
                "top": "0dp",
                "width": "100%",
                "zIndex": 1,
                "overrides": {
                    "btnPopUpCancel": {
                        "i18n_text": "kony.i18n.getLocalizedString(\"i8n.navigation.cancelButton\")",
                        "minWidth": "81px",
                        "right": "123px",
                        "width": "81px"
                    },
                    "btnPopUpDelete": {
                        "i18n_text": "kony.i18n.getLocalizedString(\"i8n.navigation.okButton\")",
                        "minWidth": "83px",
                        "width": "83px"
                    },
                    "lblPopUpMainMessage": {
                        "i18n_text": "kony.i18n.getLocalizedString(\"i8n.navigation.cancelChanges\")"
                    },
                    "popUpCancelEdits": {
                        "bottom": "viz.val_cleared",
                        "centerX": "viz.val_cleared",
                        "centerY": "viz.val_cleared",
                        "height": "100%",
                        "left": "0dp",
                        "maxHeight": "viz.val_cleared",
                        "maxWidth": "viz.val_cleared",
                        "minHeight": "viz.val_cleared",
                        "minWidth": "viz.val_cleared",
                        "right": "viz.val_cleared",
                        "top": "0dp",
                        "width": "100%"
                    },
                    "rtxPopUpDisclaimer": {
                        "i18n_text": "kony.i18n.getLocalizedString(\"i8n.navigation.cancelChangesBody\")"
                    }
                }
            }, {
                "overrides": {}
            }, {
                "overrides": {}
            });
            flxEditCancelConfirmation.add(popUpCancelEdits);
            this.add(flxServiceManagement, flxEditCancelConfirmation);
        };
        return [{
            "addWidgets": addWidgetsfrmServiceManagement,
            "enabledForIdleTimeout": true,
            "id": "frmServiceManagement",
            "layoutType": kony.flex.FREE_FORM,
            "needAppMenu": false,
            "preShow": function(eventobject) {
                controller.AS_Form_g197182751ef45b7ab5605a6f65d849d(eventobject);
            },
            "skin": "slForm"
        }, {
            "displayOrientation": constants.FORM_DISPLAY_ORIENTATION_PORTRAIT,
            "layoutType": kony.flex.FREE_FORM,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, {
            "onDeviceBack": controller.AS_Form_d95f5c87fabe4c4d9a1bb01a1661012d,
            "retainScrollPosition": false
        }]
    }
});