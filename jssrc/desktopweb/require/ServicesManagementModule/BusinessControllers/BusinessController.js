define([], function() {
    function ServicesManagementModule_BusinessController() {
        kony.mvc.Business.Delegator.call(this);
    }
    inheritsFrom(ServicesManagementModule_BusinessController, kony.mvc.Business.Delegator);
    ServicesManagementModule_BusinessController.prototype.initializeBusinessController = function() {};
    /**
     * @name getServices
     * @member ServicesManagementModule.businessController
     * @param {} payload
     * @param (response:{{"features": [{"Service_Fee": "","Status_id": "","displayName": {},"name": "","description": "","id": "","roleTypes": [{"name": "","id": ""}],"Type_id": ""}]})=>any onSuccess
     * @param (...callbackArgs)=>any onError
     */
    ServicesManagementModule_BusinessController.prototype.getAllFeatures = function(payload, onSuccess, onError) {
        kony.mvc.MDAApplication.getSharedInstance().getModuleManager().getModule("FeaturesManager").businessController.getAllFeatures(payload, onSuccess, onError);
    };
    ServicesManagementModule_BusinessController.prototype.updateService = function(editedParamReq, onSuccess, onError) {
        kony.mvc.MDAApplication.getSharedInstance().getModuleManager().getModule("FeaturesManager").businessController.editFeatureAndActionLimits(editedParamReq, onSuccess, onError);
    };
    ServicesManagementModule_BusinessController.prototype.updateActionStatus = function(editedParamReq, onSuccess, onError) {
        kony.mvc.MDAApplication.getSharedInstance().getModuleManager().getModule("FeaturesManager").businessController.manageActionStatus(editedParamReq, onSuccess, onError);
    };
    /**
     * @name getAllLocaleLanguagesList
     * @member  ServicesManagementModule.businessController
     * @param {}=>any onSuccess
     * @param (...callbackArgs)=>any onError
     */
    ServicesManagementModule_BusinessController.prototype.getAllLocaleLanguagesList = function(context, onSuccess, onError) {
        kony.mvc.MDAApplication.getSharedInstance().getModuleManager().getModule("MasterDataManager").businessController.getLocales(context, onSuccess, onError);
    };
    /**
     * @name getActionsOfFeature
     * @member  ServicesManagementModule.businessController
     * @param Payload: {"featureId":""}
     * @param ([{"termandcondition": {"en-US": {"contentType": "TEXT","content": ""}},"actionId": "","description": "","isMFAApplicable": "","limits": [{"type": "","value": ""}],"actionName": "","Type_id": ""}])=>any onSuccess
     * @param (...callbackArgs)=>any onError
     */
    ServicesManagementModule_BusinessController.prototype.getActionsOfFeature = function(context, onSuccess, onError) {
        kony.mvc.MDAApplication.getSharedInstance().getModuleManager().getModule("FeaturesManager").businessController.getActionsOfFeature(context, onSuccess, onError);
    };
    /**
     * @name getLimitGroups
     * @member  ServicesManagementModule.businessController
     * @param Payload: {}
     * @param ({"limitGroupRecords":[{"name":"Account to account","description":"Account to account","id":"ACCOUNT_TO_ACCOUNT"},{"name":"Bulk payment","description":"Payment made for bulk payment","id":"BULK_PAYMENT"},{"name":"payment single","description":"single transaction","id":"SINGLE_PAYMENT"}],"opstatus":0,"httpStatusCode":0})=>any onSuccess
     * @param (...callbackArgs)=>any onError
     */
    ServicesManagementModule_BusinessController.prototype.getLimitGroups = function(context, onSuccess, onError) {
        kony.mvc.MDAApplication.getSharedInstance().getModuleManager().getModule("MasterDataManager").businessController.getLimitGroups(context, onSuccess, onError);
    };
    /**
     * @name editLimitGroup
     * @member  ServicesManagementModule.businessController
     * @param Payload: {}
     * @param ()=>any onSuccess
     * @param (...callbackArgs)=>any onError
     */
    ServicesManagementModule_BusinessController.prototype.editLimitGroup = function(context, onSuccess, onError) {
        kony.mvc.MDAApplication.getSharedInstance().getModuleManager().getModule("MasterDataManager").businessController.editLimitGroup(context, onSuccess, onError);
    };
    ServicesManagementModule_BusinessController.prototype.execute = function(command) {
        kony.mvc.Business.Controller.prototype.execute.call(this, command);
    };
    return ServicesManagementModule_BusinessController;
});