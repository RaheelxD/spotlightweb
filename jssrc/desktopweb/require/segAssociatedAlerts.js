define("segAssociatedAlerts", function() {
    return function(controller) {
        var segAssociatedAlerts = new kony.ui.FlexContainer({
            "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
            "clipBounds": true,
            "id": "segAssociatedAlerts",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "left": "0dp",
            "isModalContainer": false,
            "skin": "sknAlertsAssociatedAlerts",
            "top": "0dp",
            "width": "100%"
        }, {}, {});
        segAssociatedAlerts.setDefaultUnit(kony.flex.DP);
        var lblAssociatedAlert = new kony.ui.Label({
            "bottom": "5px",
            "id": "lblAssociatedAlert",
            "isVisible": true,
            "left": "5px",
            "skin": "sknlblLatoBold35475f14px",
            "text": "Associated alert",
            "top": "5px",
            "width": "85%",
            "zIndex": 1
        }, {
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, {});
        segAssociatedAlerts.add(lblAssociatedAlert);
        return segAssociatedAlerts;
    }
})