define("flxCustSearch", function() {
    return function(controller) {
        var flxCustSearch = new kony.ui.FlexContainer({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "clipBounds": true,
            "height": "50px",
            "id": "flxCustSearch",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "isModalContainer": false,
            "skin": "sknCursor"
        }, {}, {});
        flxCustSearch.setDefaultUnit(kony.flex.DP);
        var flxCustSearchWrapper = new kony.ui.FlexContainer({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "clipBounds": true,
            "height": "50px",
            "id": "flxCustSearchWrapper",
            "isVisible": true,
            "layoutType": kony.flex.FLOW_HORIZONTAL,
            "left": "20px",
            "isModalContainer": false,
            "right": "20px",
            "skin": "sknCursor",
            "top": "0"
        }, {}, {
            "hoverSkin": "sknfbfcfc"
        });
        flxCustSearchWrapper.setDefaultUnit(kony.flex.DP);
        var lblName = new kony.ui.Label({
            "centerY": "50%",
            "id": "lblName",
            "isVisible": true,
            "left": "3%",
            "right": 0,
            "skin": "sknlblLatoRegular484b5213px",
            "text": "Mr. Bryan Victor Nash",
            "top": "0%",
            "width": "20%",
            "zIndex": 1
        }, {
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, {});
        var lblUserName = new kony.ui.Label({
            "centerY": "50%",
            "id": "lblUserName",
            "isVisible": true,
            "left": 0,
            "skin": "sknlblLatoRegular484b5213px",
            "text": "bryanvnash123",
            "width": "20%",
            "zIndex": 1
        }, {
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, {});
        var lblUserId = new kony.ui.Label({
            "centerY": "50%",
            "id": "lblUserId",
            "isVisible": true,
            "left": "0%",
            "skin": "sknlblLatoRegular484b5213px",
            "text": "TD12323",
            "width": "16%",
            "zIndex": 1
        }, {
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, {});
        var lblContactNo = new kony.ui.Label({
            "centerY": "50%",
            "id": "lblContactNo",
            "isVisible": true,
            "left": "0%",
            "skin": "sknlblLatoRegular484b5213px",
            "text": "+91-1234-567-890",
            "width": "19%",
            "zIndex": 1
        }, {
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, {});
        var lblEmailId = new kony.ui.Label({
            "centerY": "50%",
            "id": "lblEmailId",
            "isVisible": true,
            "left": "0",
            "right": "20px",
            "skin": "sknlblLatoRegular484b5213px",
            "text": "johndoe@gmail.com",
            "width": "25%",
            "zIndex": 1
        }, {
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, {});
        flxCustSearchWrapper.add(lblName, lblUserName, lblUserId, lblContactNo, lblEmailId);
        var lblSeperator = new kony.ui.Label({
            "bottom": "0px",
            "height": "1px",
            "id": "lblSeperator",
            "isVisible": true,
            "left": "20px",
            "right": "20px",
            "skin": "sknlblSeperator",
            "text": ".",
            "zIndex": 1
        }, {
            "contentAlignment": constants.CONTENT_ALIGN_TOP_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, {});
        flxCustSearch.add(flxCustSearchWrapper, lblSeperator);
        return flxCustSearch;
    }
})