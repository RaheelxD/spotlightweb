define("userflxRoleBusinessTypesController", {
    //Type your controller code here 
});
define("flxRoleBusinessTypesControllerActions", {
    /*
      This is an auto generated file and any modifications to it may result in corruption of the action sequence.
    */
    /** onClick defined for flxCheckboxSelect **/
    AS_FlexContainer_d8dd5d896cea44d89a64319b408faa2b: function AS_FlexContainer_d8dd5d896cea44d89a64319b408faa2b(eventobject, context) {
        this.toggleCheckbox2();
    },
    /** onClick defined for flxCheckboxDefault **/
    AS_FlexContainer_gfa3e0383dcc4b1d8e5c74f58b9f7219: function AS_FlexContainer_gfa3e0383dcc4b1d8e5c74f58b9f7219(eventobject, context) {
        this.toggleCheckbox2();
    }
});
define("flxRoleBusinessTypesController", ["userflxRoleBusinessTypesController", "flxRoleBusinessTypesControllerActions"], function() {
    var controller = require("userflxRoleBusinessTypesController");
    var controllerActions = ["flxRoleBusinessTypesControllerActions"];
    return kony.visualizer.mixinControllerActions(controller, controllerActions);
});
