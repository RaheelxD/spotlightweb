define("flLimitsCompanies", function() {
    return function(controller) {
        var flLimitsCompanies = new kony.ui.FlexContainer({
            "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
            "clipBounds": true,
            "id": "flLimitsCompanies",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "left": "0dp",
            "isModalContainer": false,
            "skin": "sknflxffffffop100Bordercbcdd1Radius3px",
            "top": "0dp",
            "width": "100%"
        }, {}, {});
        flLimitsCompanies.setDefaultUnit(kony.flex.DP);
        var flxLimitHeader = new kony.ui.FlexContainer({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "clipBounds": true,
            "height": "30dp",
            "id": "flxLimitHeader",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "left": "0dp",
            "isModalContainer": false,
            "skin": "slFbox",
            "top": 0,
            "width": "100%",
            "zIndex": 2
        }, {}, {});
        flxLimitHeader.setDefaultUnit(kony.flex.DP);
        var lblFeature = new kony.ui.Label({
            "centerY": "50%",
            "id": "lblFeature",
            "isVisible": true,
            "left": "10dp",
            "skin": "sknLatoRegular192B4516px",
            "text": "Transfer to account in other FIs",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 2
        }, {
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, {});
        var flxStatus = new kony.ui.FlexContainer({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "clipBounds": true,
            "height": "100%",
            "id": "flxStatus",
            "isVisible": true,
            "layoutType": kony.flex.FLOW_HORIZONTAL,
            "reverseLayoutDirection": true,
            "isModalContainer": false,
            "right": "25dp",
            "skin": "slFbox",
            "top": "0dp",
            "width": "100px",
            "zIndex": 1
        }, {}, {});
        flxStatus.setDefaultUnit(kony.flex.DP);
        var lblUsersStatus = new kony.ui.Label({
            "centerY": "50%",
            "id": "lblUsersStatus",
            "isVisible": true,
            "left": "5dp",
            "right": "0dp",
            "skin": "sknlblLato5bc06cBold14px",
            "text": "Active",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, {
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, {});
        var fontIconStatusImg = new kony.ui.Label({
            "centerY": "50%",
            "id": "fontIconStatusImg",
            "isVisible": true,
            "left": "0dp",
            "skin": "sknFontIconActivate",
            "text": "",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, {
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, {});
        flxStatus.add(lblUsersStatus, fontIconStatusImg);
        flxLimitHeader.add(lblFeature, flxStatus);
        var flxLimitLower = new kony.ui.FlexContainer({
            "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
            "clipBounds": true,
            "id": "flxLimitLower",
            "isVisible": true,
            "layoutType": kony.flex.FLOW_HORIZONTAL,
            "left": "10dp",
            "isModalContainer": false,
            "right": "20dp",
            "skin": "slFbox",
            "top": "45dp",
            "zIndex": 2
        }, {}, {});
        flxLimitLower.setDefaultUnit(kony.flex.DP);
        var flxPerTransactionLimit = new kony.ui.FlexContainer({
            "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
            "clipBounds": true,
            "id": "flxPerTransactionLimit",
            "isVisible": true,
            "layoutType": kony.flex.FLOW_VERTICAL,
            "isModalContainer": false,
            "right": "2%",
            "skin": "slFbox",
            "top": "0dp",
            "width": "32%",
            "zIndex": 1
        }, {}, {});
        flxPerTransactionLimit.setDefaultUnit(kony.flex.DP);
        var lblPerTransactionLimit = new kony.ui.Label({
            "height": "15px",
            "id": "lblPerTransactionLimit",
            "isVisible": true,
            "left": "0px",
            "right": "0px",
            "skin": "sknlbl485c7514px",
            "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmServiceManagement.PerTransactionLimitLC\")",
            "top": 0,
            "width": "130px",
            "zIndex": 1
        }, {
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, {});
        var flxPerMax = new kony.ui.FlexContainer({
            "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
            "clipBounds": true,
            "id": "flxPerMax",
            "isVisible": true,
            "layoutType": kony.flex.FLOW_VERTICAL,
            "left": "130dp",
            "isModalContainer": false,
            "skin": "slFbox",
            "top": "-15dp",
            "width": "15px"
        }, {}, {});
        flxPerMax.setDefaultUnit(kony.flex.DP);
        var lblPerMaxIcon = new kony.ui.Label({
            "id": "lblPerMaxIcon",
            "isVisible": true,
            "left": "0dp",
            "skin": "sknLblIcon485C7515px",
            "text": "",
            "top": "0dp",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 2
        }, {
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, {});
        flxPerMax.add(lblPerMaxIcon);
        var tbxPerTransactionLimitValue = new kony.ui.TextBox2({
            "autoCapitalize": constants.TEXTBOX_AUTO_CAPITALIZE_NONE,
            "bottom": "15dp",
            "focusSkin": "skntbxLato35475f14px",
            "height": "40dp",
            "id": "tbxPerTransactionLimitValue",
            "isVisible": true,
            "keyBoardStyle": constants.TEXTBOX_KEY_BOARD_STYLE_DEFAULT,
            "maxTextLength": 50,
            "i18n_placeholder": "kony.i18n.getLocalizedString(\"i18n.Applications.CompanyName\")",
            "right": "0dp",
            "secureTextEntry": false,
            "skin": "skntbxLato35475f14px",
            "textInputMode": constants.TEXTBOX_INPUT_MODE_ANY,
            "top": "10dp",
            "width": "100%",
            "zIndex": 1
        }, {
            "containerHeightMode": constants.TEXTBOX_FONT_METRICS_DRIVEN_HEIGHT,
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [12, 0, 0, 0],
            "paddingInPixel": false
        }, {
            "autoCorrect": false
        });
        var flxPerCurrency = new kony.ui.FlexContainer({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "bottom": "15dp",
            "clipBounds": true,
            "height": "36dp",
            "id": "flxPerCurrency",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "left": "2dp",
            "isModalContainer": false,
            "top": "-53dp",
            "width": "10%",
            "zIndex": 2
        }, {}, {});
        flxPerCurrency.setDefaultUnit(kony.flex.DP);
        var lblPerCurrency = new kony.ui.Label({
            "centerX": "50%",
            "centerY": "50%",
            "id": "lblPerCurrency",
            "isVisible": true,
            "left": "5dp",
            "skin": "sknIcon485C7513px",
            "text": "$",
            "top": "8dp",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, {
            "contentAlignment": constants.CONTENT_ALIGN_CENTER,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, {});
        flxPerCurrency.add(lblPerCurrency);
        var flxPerTransactionLimitError = new kony.ui.FlexContainer({
            "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
            "bottom": "15dp",
            "clipBounds": true,
            "id": "flxPerTransactionLimitError",
            "isVisible": false,
            "layoutType": kony.flex.FLOW_HORIZONTAL,
            "left": "0dp",
            "isModalContainer": false,
            "skin": "slFbox",
            "top": "0dp",
            "width": "100%",
            "zIndex": 1
        }, {}, {});
        flxPerTransactionLimitError.setDefaultUnit(kony.flex.DP);
        var lblPerTransactionLimitErronIcon = new kony.ui.Label({
            "height": "15dp",
            "id": "lblPerTransactionLimitErronIcon",
            "isVisible": true,
            "left": "0dp",
            "skin": "sknErrorIcon",
            "text": "",
            "top": "0dp",
            "width": "15dp",
            "zIndex": 1
        }, {
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, {});
        var lblPerTransactionLimitError = new kony.ui.Label({
            "id": "lblPerTransactionLimitError",
            "isVisible": true,
            "left": "0dp",
            "skin": "sknlblError",
            "text": "Value cannot be more than $5000",
            "top": "0dp",
            "width": "90%",
            "zIndex": 1
        }, {
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, {});
        flxPerTransactionLimitError.add(lblPerTransactionLimitErronIcon, lblPerTransactionLimitError);
        flxPerTransactionLimit.add(lblPerTransactionLimit, flxPerMax, tbxPerTransactionLimitValue, flxPerCurrency, flxPerTransactionLimitError);
        var flxDailyTransactionLimit = new kony.ui.FlexContainer({
            "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
            "clipBounds": true,
            "id": "flxDailyTransactionLimit",
            "isVisible": true,
            "layoutType": kony.flex.FLOW_VERTICAL,
            "isModalContainer": false,
            "right": "2%",
            "skin": "slFbox",
            "top": "0dp",
            "width": "32%",
            "zIndex": 1
        }, {}, {});
        flxDailyTransactionLimit.setDefaultUnit(kony.flex.DP);
        var lblDailyTransactionLimit = new kony.ui.Label({
            "id": "lblDailyTransactionLimit",
            "isVisible": true,
            "left": "0px",
            "right": "0px",
            "skin": "sknlbl485c7514px",
            "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmServiceManagement.DailyTransactionLimitLC\")",
            "top": 0,
            "width": "130px",
            "zIndex": 1
        }, {
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, {});
        var flxDailyMax = new kony.ui.FlexContainer({
            "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
            "clipBounds": true,
            "id": "flxDailyMax",
            "isVisible": true,
            "layoutType": kony.flex.FLOW_VERTICAL,
            "left": "140dp",
            "isModalContainer": false,
            "skin": "slFbox",
            "top": "-15dp",
            "width": "15px"
        }, {}, {});
        flxDailyMax.setDefaultUnit(kony.flex.DP);
        var lblDailyMaxIcon = new kony.ui.Label({
            "id": "lblDailyMaxIcon",
            "isVisible": true,
            "left": "0dp",
            "skin": "sknLblIcon485C7515px",
            "text": "",
            "top": "0dp",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 2
        }, {
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, {});
        flxDailyMax.add(lblDailyMaxIcon);
        var tbxDailyTransactionLimitValue = new kony.ui.TextBox2({
            "autoCapitalize": constants.TEXTBOX_AUTO_CAPITALIZE_NONE,
            "bottom": "15dp",
            "focusSkin": "skntbxLato35475f14px",
            "height": "40dp",
            "id": "tbxDailyTransactionLimitValue",
            "isVisible": true,
            "keyBoardStyle": constants.TEXTBOX_KEY_BOARD_STYLE_DEFAULT,
            "maxTextLength": 50,
            "i18n_placeholder": "kony.i18n.getLocalizedString(\"i18n.Applications.CompanyName\")",
            "right": "0dp",
            "secureTextEntry": false,
            "skin": "skntbxLato35475f14px",
            "textInputMode": constants.TEXTBOX_INPUT_MODE_ANY,
            "top": "10dp",
            "width": "100%",
            "zIndex": 1
        }, {
            "containerHeightMode": constants.TEXTBOX_FONT_METRICS_DRIVEN_HEIGHT,
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [12, 0, 0, 0],
            "paddingInPixel": false
        }, {
            "autoCorrect": false
        });
        var flxDailyCurrency = new kony.ui.FlexContainer({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "bottom": "15dp",
            "clipBounds": true,
            "height": "36dp",
            "id": "flxDailyCurrency",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "left": "2dp",
            "isModalContainer": false,
            "top": "-53dp",
            "width": "10%",
            "zIndex": 2
        }, {}, {});
        flxDailyCurrency.setDefaultUnit(kony.flex.DP);
        var lblDailyCurrency = new kony.ui.Label({
            "centerX": "50%",
            "centerY": "50%",
            "id": "lblDailyCurrency",
            "isVisible": true,
            "left": "5dp",
            "skin": "sknIcon485C7513px",
            "text": "$",
            "top": "8dp",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, {
            "contentAlignment": constants.CONTENT_ALIGN_CENTER,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, {});
        flxDailyCurrency.add(lblDailyCurrency);
        var flxDailyTransactionLimitError = new kony.ui.FlexContainer({
            "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
            "bottom": "15dp",
            "clipBounds": true,
            "id": "flxDailyTransactionLimitError",
            "isVisible": false,
            "layoutType": kony.flex.FLOW_HORIZONTAL,
            "left": "0dp",
            "isModalContainer": false,
            "skin": "slFbox",
            "top": "0dp",
            "width": "100%",
            "zIndex": 1
        }, {}, {});
        flxDailyTransactionLimitError.setDefaultUnit(kony.flex.DP);
        var lblDailyTransactionLimitErrorIcon = new kony.ui.Label({
            "height": "15dp",
            "id": "lblDailyTransactionLimitErrorIcon",
            "isVisible": true,
            "left": "0dp",
            "skin": "sknErrorIcon",
            "text": "",
            "top": "0dp",
            "width": "15dp",
            "zIndex": 1
        }, {
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, {});
        var lblDailyTransactionLimitError = new kony.ui.Label({
            "id": "lblDailyTransactionLimitError",
            "isVisible": true,
            "left": "0dp",
            "skin": "sknlblError",
            "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmAlertsManagement.lblNoEditAlertMainNameError\")",
            "top": "0dp",
            "width": "90%",
            "zIndex": 1
        }, {
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, {});
        flxDailyTransactionLimitError.add(lblDailyTransactionLimitErrorIcon, lblDailyTransactionLimitError);
        flxDailyTransactionLimit.add(lblDailyTransactionLimit, flxDailyMax, tbxDailyTransactionLimitValue, flxDailyCurrency, flxDailyTransactionLimitError);
        var flxWeeklyTransactionLimit = new kony.ui.FlexContainer({
            "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
            "clipBounds": true,
            "id": "flxWeeklyTransactionLimit",
            "isVisible": true,
            "layoutType": kony.flex.FLOW_VERTICAL,
            "left": "0%",
            "isModalContainer": false,
            "skin": "slFbox",
            "top": "0dp",
            "width": "32%",
            "zIndex": 1
        }, {}, {});
        flxWeeklyTransactionLimit.setDefaultUnit(kony.flex.DP);
        var lblWeeklyTransactionLimit = new kony.ui.Label({
            "id": "lblWeeklyTransactionLimit",
            "isVisible": true,
            "left": "0px",
            "right": "0px",
            "skin": "sknlbl485c7514px",
            "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmServiceManagement.WeeklyTransLimitLC\")",
            "top": 0,
            "width": "145px",
            "zIndex": 1
        }, {
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, {});
        var flxWeeklyMax = new kony.ui.FlexContainer({
            "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
            "clipBounds": true,
            "id": "flxWeeklyMax",
            "isVisible": true,
            "layoutType": kony.flex.FLOW_VERTICAL,
            "left": "155dp",
            "isModalContainer": false,
            "skin": "slFbox",
            "top": "-15dp",
            "width": "15px"
        }, {}, {});
        flxWeeklyMax.setDefaultUnit(kony.flex.DP);
        var lblWeeklyMaxIcon = new kony.ui.Label({
            "id": "lblWeeklyMaxIcon",
            "isVisible": true,
            "left": "0dp",
            "skin": "sknLblIcon485C7515px",
            "text": "",
            "top": "0dp",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 2
        }, {
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, {});
        flxWeeklyMax.add(lblWeeklyMaxIcon);
        var tbxWeeklyTransactionLimitValue = new kony.ui.TextBox2({
            "autoCapitalize": constants.TEXTBOX_AUTO_CAPITALIZE_NONE,
            "bottom": "15dp",
            "focusSkin": "skntbxLato35475f14px",
            "height": "40dp",
            "id": "tbxWeeklyTransactionLimitValue",
            "isVisible": true,
            "keyBoardStyle": constants.TEXTBOX_KEY_BOARD_STYLE_DEFAULT,
            "maxTextLength": 50,
            "i18n_placeholder": "kony.i18n.getLocalizedString(\"i18n.Applications.CompanyName\")",
            "right": "0dp",
            "secureTextEntry": false,
            "skin": "skntbxLato35475f14px",
            "textInputMode": constants.TEXTBOX_INPUT_MODE_ANY,
            "top": "10dp",
            "width": "100%",
            "zIndex": 1
        }, {
            "containerHeightMode": constants.TEXTBOX_FONT_METRICS_DRIVEN_HEIGHT,
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [12, 0, 0, 0],
            "paddingInPixel": false
        }, {
            "autoCorrect": false
        });
        var flxWeeklyCurrency = new kony.ui.FlexContainer({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "bottom": "15dp",
            "clipBounds": true,
            "height": "36dp",
            "id": "flxWeeklyCurrency",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "left": "2dp",
            "isModalContainer": false,
            "top": "-53dp",
            "width": "10%",
            "zIndex": 2
        }, {}, {});
        flxWeeklyCurrency.setDefaultUnit(kony.flex.DP);
        var lblWeeklyCurrency = new kony.ui.Label({
            "centerX": "50%",
            "centerY": "50%",
            "id": "lblWeeklyCurrency",
            "isVisible": true,
            "left": "5dp",
            "skin": "sknIcon485C7513px",
            "text": "$",
            "top": "8dp",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, {
            "contentAlignment": constants.CONTENT_ALIGN_CENTER,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, {});
        flxWeeklyCurrency.add(lblWeeklyCurrency);
        var flxWeeklyTransactionLimitError = new kony.ui.FlexContainer({
            "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
            "bottom": "15dp",
            "clipBounds": true,
            "id": "flxWeeklyTransactionLimitError",
            "isVisible": false,
            "layoutType": kony.flex.FLOW_HORIZONTAL,
            "left": "0dp",
            "isModalContainer": false,
            "skin": "slFbox",
            "top": "0dp",
            "width": "100%",
            "zIndex": 1
        }, {}, {});
        flxWeeklyTransactionLimitError.setDefaultUnit(kony.flex.DP);
        var lblWeeklyTransactionLimitErrorIcon = new kony.ui.Label({
            "height": "15dp",
            "id": "lblWeeklyTransactionLimitErrorIcon",
            "isVisible": true,
            "left": "0dp",
            "skin": "sknErrorIcon",
            "text": "",
            "top": "0dp",
            "width": "15dp",
            "zIndex": 1
        }, {
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, {});
        var lblWeeklyTransactionLimitError = new kony.ui.Label({
            "id": "lblWeeklyTransactionLimitError",
            "isVisible": true,
            "left": "0dp",
            "skin": "sknlblError",
            "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmAlertsManagement.lblNoEditAlertMainNameError\")",
            "top": "0dp",
            "width": "90%",
            "zIndex": 1
        }, {
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, {});
        flxWeeklyTransactionLimitError.add(lblWeeklyTransactionLimitErrorIcon, lblWeeklyTransactionLimitError);
        flxWeeklyTransactionLimit.add(lblWeeklyTransactionLimit, flxWeeklyMax, tbxWeeklyTransactionLimitValue, flxWeeklyCurrency, flxWeeklyTransactionLimitError);
        flxLimitLower.add(flxPerTransactionLimit, flxDailyTransactionLimit, flxWeeklyTransactionLimit);
        flLimitsCompanies.add(flxLimitHeader, flxLimitLower);
        return flLimitsCompanies;
    }
})