define("flxFeeRange", function() {
    return function(controller) {
        var flxFeeRange = new kony.ui.FlexContainer({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "clipBounds": true,
            "height": "75dp",
            "id": "flxFeeRange",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "isModalContainer": false,
            "skin": "slFbox"
        }, {}, {});
        flxFeeRange.setDefaultUnit(kony.flex.DP);
        var flxMain = new kony.ui.FlexContainer({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "centerY": "50%",
            "clipBounds": true,
            "height": "65dp",
            "id": "flxMain",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "left": "20dp",
            "isModalContainer": false,
            "right": "50px",
            "skin": "slFbox",
            "zIndex": 1
        }, {}, {});
        flxMain.setDefaultUnit(kony.flex.DP);
        var flxMinAmount = new kony.ui.FlexContainer({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "clipBounds": true,
            "focusSkin": "skntxtbxhover11abeb",
            "height": "40dp",
            "id": "flxMinAmount",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "left": "0dp",
            "isModalContainer": false,
            "skin": "skntxtbxNormald7d9e0",
            "top": "5dp",
            "width": "32%",
            "zIndex": 1
        }, {}, {});
        flxMinAmount.setDefaultUnit(kony.flex.DP);
        var flx1 = new kony.ui.FlexContainer({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "clipBounds": true,
            "height": "100%",
            "id": "flx1",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "left": "0dp",
            "isModalContainer": false,
            "skin": "sknflxNoneditable",
            "top": "0dp",
            "width": "40px",
            "zIndex": 1
        }, {}, {});
        flx1.setDefaultUnit(kony.flex.DP);
        var lblDollar = new kony.ui.Label({
            "centerX": "50%",
            "centerY": "50%",
            "id": "lblDollar",
            "isVisible": true,
            "skin": "sknlblLatoBold35475f14px",
            "text": "$",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, {
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, {});
        var lblSeparator = new kony.ui.Label({
            "height": "100%",
            "id": "lblSeparator",
            "isVisible": true,
            "left": "39dp",
            "skin": "sknlblSeperator",
            "text": "$",
            "top": "0dp",
            "width": "1dp",
            "zIndex": 1
        }, {
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, {});
        flx1.add(lblDollar, lblSeparator);
        var txtbxAmount = new kony.ui.TextBox2({
            "autoCapitalize": constants.TEXTBOX_AUTO_CAPITALIZE_NONE,
            "height": "100%",
            "id": "txtbxAmount",
            "isVisible": true,
            "keyBoardStyle": constants.TEXTBOX_KEY_BOARD_STYLE_DEFAULT,
            "left": "41dp",
            "placeholder": "Min Transaction Amount",
            "right": "0dp",
            "secureTextEntry": false,
            "skin": "skntxtbxRadius0px0a12b1c5c6fbf4e",
            "textInputMode": constants.TEXTBOX_INPUT_MODE_ANY,
            "top": "0dp",
            "zIndex": 1
        }, {
            "containerHeightMode": constants.TEXTBOX_FONT_METRICS_DRIVEN_HEIGHT,
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [3, 0, 0, 0],
            "paddingInPixel": false
        }, {
            "autoCorrect": false
        });
        flxMinAmount.add(flx1, txtbxAmount);
        var flxMaxAmount = new kony.ui.FlexContainer({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "clipBounds": true,
            "focusSkin": "skntxtbxhover11abeb",
            "height": "40dp",
            "id": "flxMaxAmount",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "left": "34%",
            "isModalContainer": false,
            "skin": "skntxtbxNormald7d9e0",
            "top": "5px",
            "width": "32%",
            "zIndex": 1
        }, {}, {});
        flxMaxAmount.setDefaultUnit(kony.flex.DP);
        var flxMax1 = new kony.ui.FlexContainer({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "clipBounds": true,
            "height": "100%",
            "id": "flxMax1",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "left": "0dp",
            "isModalContainer": false,
            "skin": "sknflxNoneditable",
            "top": "0dp",
            "width": "40px",
            "zIndex": 1
        }, {}, {});
        flxMax1.setDefaultUnit(kony.flex.DP);
        var lblDollar1 = new kony.ui.Label({
            "centerX": "50%",
            "centerY": "50%",
            "id": "lblDollar1",
            "isVisible": true,
            "skin": "sknlblLatoBold35475f14px",
            "text": "$",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, {
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, {});
        var lblSeparator2 = new kony.ui.Label({
            "height": "100%",
            "id": "lblSeparator2",
            "isVisible": true,
            "left": "39dp",
            "skin": "sknlblSeperator",
            "text": "$",
            "top": "0dp",
            "width": "1dp",
            "zIndex": 1
        }, {
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, {});
        flxMax1.add(lblDollar1, lblSeparator2);
        var txtbxMaxAmount = new kony.ui.TextBox2({
            "autoCapitalize": constants.TEXTBOX_AUTO_CAPITALIZE_NONE,
            "height": "100%",
            "id": "txtbxMaxAmount",
            "isVisible": true,
            "keyBoardStyle": constants.TEXTBOX_KEY_BOARD_STYLE_DEFAULT,
            "left": "41dp",
            "placeholder": "Max Transaction Amount",
            "right": "0dp",
            "secureTextEntry": false,
            "skin": "skntxtbxRadius0px0a12b1c5c6fbf4e",
            "textInputMode": constants.TEXTBOX_INPUT_MODE_ANY,
            "top": "0dp",
            "zIndex": 1
        }, {
            "containerHeightMode": constants.TEXTBOX_FONT_METRICS_DRIVEN_HEIGHT,
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [3, 0, 0, 0],
            "paddingInPixel": false
        }, {
            "autoCorrect": false
        });
        flxMaxAmount.add(flxMax1, txtbxMaxAmount);
        var flxFee = new kony.ui.FlexContainer({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "clipBounds": true,
            "focusSkin": "skntxtbxhover11abeb",
            "height": "40dp",
            "id": "flxFee",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "left": "68%",
            "isModalContainer": false,
            "skin": "skntxtbxNormald7d9e0",
            "top": "5px",
            "width": "32%",
            "zIndex": 1
        }, {}, {});
        flxFee.setDefaultUnit(kony.flex.DP);
        var flxFee1 = new kony.ui.FlexContainer({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "clipBounds": true,
            "height": "100%",
            "id": "flxFee1",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "left": "0dp",
            "isModalContainer": false,
            "skin": "sknflxNoneditable",
            "top": "0dp",
            "width": "40px",
            "zIndex": 1
        }, {}, {});
        flxFee1.setDefaultUnit(kony.flex.DP);
        var lblDollar2 = new kony.ui.Label({
            "centerX": "50%",
            "centerY": "50%",
            "id": "lblDollar2",
            "isVisible": true,
            "skin": "sknlblLatoBold35475f14px",
            "text": "$",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, {
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, {});
        var lblSeparator3 = new kony.ui.Label({
            "height": "100%",
            "id": "lblSeparator3",
            "isVisible": true,
            "left": "39dp",
            "skin": "sknlblSeperator",
            "text": "$",
            "top": "0dp",
            "width": "1dp",
            "zIndex": 1
        }, {
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, {});
        flxFee1.add(lblDollar2, lblSeparator3);
        var txtbxFee = new kony.ui.TextBox2({
            "autoCapitalize": constants.TEXTBOX_AUTO_CAPITALIZE_NONE,
            "height": "100%",
            "id": "txtbxFee",
            "isVisible": true,
            "keyBoardStyle": constants.TEXTBOX_KEY_BOARD_STYLE_DEFAULT,
            "left": "41dp",
            "placeholder": "Fee",
            "right": "0dp",
            "secureTextEntry": false,
            "skin": "skntxtbxRadius0px0a12b1c5c6fbf4e",
            "textInputMode": constants.TEXTBOX_INPUT_MODE_ANY,
            "top": "0dp",
            "zIndex": 1
        }, {
            "containerHeightMode": constants.TEXTBOX_FONT_METRICS_DRIVEN_HEIGHT,
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [3, 0, 0, 0],
            "paddingInPixel": false
        }, {
            "autoCorrect": false
        });
        flxFee.add(flxFee1, txtbxFee);
        var lblMinimumTransactionError = new kony.ui.Label({
            "id": "lblMinimumTransactionError",
            "isVisible": false,
            "left": "0dp",
            "skin": "sknlbleb3017Lato12px",
            "text": "Invalid Minimum Transaction",
            "top": "50dp",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, {
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, {});
        var lblMaximumTransactionError = new kony.ui.Label({
            "id": "lblMaximumTransactionError",
            "isVisible": false,
            "left": "34%",
            "skin": "sknlbleb3017Lato12px",
            "text": "Invalid Minimum Transaction",
            "top": "50dp",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, {
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, {});
        var lblFeeError = new kony.ui.Label({
            "id": "lblFeeError",
            "isVisible": false,
            "left": "68%",
            "skin": "sknlbleb3017Lato12px",
            "text": "Invalid Fee Amount",
            "top": "50dp",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, {
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, {});
        flxMain.add(flxMinAmount, flxMaxAmount, flxFee, lblMinimumTransactionError, lblMaximumTransactionError, lblFeeError);
        var flxDelete = new kony.ui.FlexContainer({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "clipBounds": true,
            "height": "25px",
            "id": "flxDelete",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "isModalContainer": false,
            "onClick": controller.AS_FlexContainer_d65549c8a2d347eb8bcbdfecae844e71,
            "right": "15px",
            "skin": "slFbox",
            "top": "17dp",
            "width": "25px",
            "zIndex": 1
        }, {}, {});
        flxDelete.setDefaultUnit(kony.flex.DP);
        var imgDelete = new kony.ui.Image2({
            "centerX": "50%",
            "centerY": "50%",
            "height": "15dp",
            "id": "imgDelete",
            "isVisible": true,
            "skin": "slImage",
            "src": "delete_2x.png",
            "width": "15dp",
            "zIndex": 1
        }, {
            "imageScaleMode": constants.IMAGE_SCALE_MODE_MAINTAIN_ASPECT_RATIO,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, {});
        flxDelete.add(imgDelete);
        flxFeeRange.add(flxMain, flxDelete);
        return flxFeeRange;
    }
})