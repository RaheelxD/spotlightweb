define("flxReportsMangSuggestions", function() {
    return function(controller) {
        var flxReportsMangSuggestions = new kony.ui.FlexContainer({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "clipBounds": true,
            "height": "30px",
            "id": "flxReportsMangSuggestions",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "left": "0dp",
            "isModalContainer": false,
            "skin": "slFbox",
            "top": "0dp",
            "width": "100%"
        }, {}, {});
        flxReportsMangSuggestions.setDefaultUnit(kony.flex.DP);
        var lblName = new kony.ui.Label({
            "centerY": "49%",
            "id": "lblName",
            "isVisible": true,
            "left": "15px",
            "right": "20px",
            "skin": "sknlblLatoRegular484b5213px",
            "text": "Internal user",
            "zIndex": 1
        }, {
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, {});
        flxReportsMangSuggestions.add(lblName);
        return flxReportsMangSuggestions;
    }
})