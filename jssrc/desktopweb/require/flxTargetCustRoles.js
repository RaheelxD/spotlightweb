define("flxTargetCustRoles", function() {
    return function(controller) {
        var flxTargetCustRoles = new kony.ui.FlexContainer({
            "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
            "clipBounds": true,
            "id": "flxTargetCustRoles",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "left": "0dp",
            "isModalContainer": false,
            "skin": "sknBackgroundFFFFFF",
            "top": "0dp",
            "width": "100%"
        }, {}, {});
        flxTargetCustRoles.setDefaultUnit(kony.flex.DP);
        var lblRoleName = new kony.ui.Label({
            "bottom": "15dp",
            "id": "lblRoleName",
            "isVisible": true,
            "left": "32dp",
            "skin": "sknLbl192B45LatoRegular14px",
            "text": "Platinum DBX Customers",
            "top": "15dp",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, {
            "contentAlignment": constants.CONTENT_ALIGN_TOP_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, {});
        var lblRoleDescription = new kony.ui.Label({
            "bottom": "15dp",
            "id": "lblRoleDescription",
            "isVisible": true,
            "left": "24%",
            "skin": "sknLbl192B45LatoSemiBold13px",
            "text": "All customers list with DBX platinum  card holders, Includes new addition of 2018 -2019.",
            "top": "15dp",
            "width": "35%",
            "zIndex": 1
        }, {
            "contentAlignment": constants.CONTENT_ALIGN_TOP_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, {});
        var lblUserCount = new kony.ui.Label({
            "bottom": "15dp",
            "id": "lblUserCount",
            "isVisible": true,
            "left": "66%",
            "skin": "sknLbl192B45LatoSemiBold13px",
            "text": "10283",
            "top": "15dp",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, {
            "contentAlignment": constants.CONTENT_ALIGN_TOP_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, {});
        var flxAttributesContainer = new kony.ui.FlexContainer({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "clipBounds": true,
            "height": "15px",
            "id": "flxAttributesContainer",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "left": "84%",
            "isModalContainer": false,
            "skin": "sknFlxffffffCursorPointer",
            "top": "15dp",
            "width": "35dp"
        }, {}, {});
        flxAttributesContainer.setDefaultUnit(kony.flex.DP);
        var lblAttributes = new kony.ui.Label({
            "height": "100%",
            "id": "lblAttributes",
            "isVisible": true,
            "left": "0%",
            "skin": "sknLblLato13px117eb0",
            "text": "View",
            "top": "0%",
            "width": "100%",
            "zIndex": 1
        }, {
            "contentAlignment": constants.CONTENT_ALIGN_TOP_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, {});
        flxAttributesContainer.add(lblAttributes);
        var flxIconDelete = new kony.ui.FlexContainer({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "clipBounds": true,
            "height": "15px",
            "id": "flxIconDelete",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "left": "97%",
            "isModalContainer": false,
            "skin": "sknCursor",
            "top": "15dp",
            "width": "15dp"
        }, {}, {});
        flxIconDelete.setDefaultUnit(kony.flex.DP);
        var lblDeleteIcon = new kony.ui.Label({
            "id": "lblDeleteIcon",
            "isVisible": true,
            "left": "0px",
            "skin": "sknLblIconMoon16px192b45",
            "text": "",
            "top": "0px",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, {
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, {});
        flxIconDelete.add(lblDeleteIcon);
        var lblSeparator = new kony.ui.Label({
            "bottom": "0px",
            "centerX": "50%",
            "height": "1dp",
            "id": "lblSeparator",
            "isVisible": true,
            "left": "20dp",
            "right": "20dp",
            "skin": "sknlblSeperatorD7D9E0",
            "text": "Label",
            "zIndex": 2
        }, {
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, {});
        flxTargetCustRoles.add(lblRoleName, lblRoleDescription, lblUserCount, flxAttributesContainer, flxIconDelete, lblSeparator);
        return flxTargetCustRoles;
    }
})