define("TermsAndConditionsModule/frmTermsAndConditions", function() {
    return function(controller) {
        function addWidgetsfrmTermsAndConditions() {
            this.setDefaultUnit(kony.flex.DP);
            var flxMain = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "100%",
                "id": "flxMain",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "slFbox0b7aaa2e3bfa340",
                "top": "0dp",
                "width": "100%",
                "zIndex": 1
            }, {}, {});
            flxMain.setDefaultUnit(kony.flex.DP);
            var flxLeftPannel = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "100%",
                "id": "flxLeftPannel",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "masterType": constants.MASTER_TYPE_USERWIDGET,
                "isModalContainer": false,
                "skin": "sknLeftPannel0bb79e4bcca8e45",
                "top": "0dp",
                "width": "305px",
                "zIndex": 5
            }, {}, {});
            flxLeftPannel.setDefaultUnit(kony.flex.DP);
            var leftMenuNew = new com.adminConsole.navigation.leftMenuNew({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "height": "100%",
                "id": "leftMenuNew",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "masterType": constants.MASTER_TYPE_DEFAULT,
                "isModalContainer": false,
                "skin": "sknFlxBg003E75Op100",
                "top": "0dp",
                "width": "100%",
                "overrides": {
                    "brandLogo": {
                        "src": "infinity_dbx_c360_logo_white_2x.png"
                    },
                    "flxScrollMenu": {
                        "bottom": "40dp",
                        "left": "0dp",
                        "top": "80dp"
                    },
                    "imgBottomLogo": {
                        "src": "logo_white_2x.png"
                    },
                    "leftMenuNew": {
                        "bottom": "viz.val_cleared",
                        "centerX": "viz.val_cleared",
                        "centerY": "viz.val_cleared",
                        "height": "100%",
                        "left": "0dp",
                        "maxHeight": "viz.val_cleared",
                        "maxWidth": "viz.val_cleared",
                        "minHeight": "viz.val_cleared",
                        "minWidth": "viz.val_cleared",
                        "right": "viz.val_cleared",
                        "top": "0dp",
                        "width": "100%"
                    }
                }
            }, {
                "overrides": {}
            }, {
                "overrides": {}
            });
            flxLeftPannel.add(leftMenuNew);
            var flxRightPanel = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "100%",
                "id": "flxRightPanel",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "305px",
                "masterType": constants.MASTER_TYPE_USERWIDGET,
                "isModalContainer": false,
                "right": "0dp",
                "skin": "slFbox0b7aaa2e3bfa340",
                "top": "0px",
                "zIndex": 1
            }, {}, {});
            flxRightPanel.setDefaultUnit(kony.flex.DP);
            var flxMainHeader = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "126px",
                "id": "flxMainHeader",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0px",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0px",
                "width": "100%",
                "zIndex": 10
            }, {}, {});
            flxMainHeader.setDefaultUnit(kony.flex.DP);
            var mainHeader = new com.adminConsole.header.mainHeader({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "height": "100px",
                "id": "mainHeader",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": 0,
                "masterType": constants.MASTER_TYPE_DEFAULT,
                "isModalContainer": false,
                "skin": "CopyslFbox1",
                "top": 0,
                "width": "100%",
                "overrides": {
                    "btnAddNewOption": {
                        "right": "207dp"
                    },
                    "flxButtons": {
                        "isVisible": true,
                        "right": "71dp",
                        "width": "400dp"
                    },
                    "flxHeaderSeperator": {
                        "isVisible": true,
                        "left": "0dp",
                        "right": "70dp"
                    },
                    "flxHeaderUserOptions": {
                        "height": "24dp",
                        "right": "70px",
                        "width": "30%"
                    },
                    "flxMainHeader": {
                        "left": "35dp",
                        "top": "0dp"
                    },
                    "imgLogout": {
                        "src": "img_logout.png"
                    },
                    "lblHeading": {
                        "centerY": "viz.val_cleared",
                        "i18n_text": "kony.i18n.getLocalizedString(\"i18n.TermsAndConditions.addTnC\")",
                        "left": "0dp",
                        "top": "54px"
                    },
                    "mainHeader": {
                        "height": "100px"
                    }
                }
            }, {
                "overrides": {}
            }, {
                "overrides": {}
            });
            var flxBreadcrumb = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "20px",
                "id": "flxBreadcrumb",
                "isVisible": false,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "106px",
                "width": "100%",
                "zIndex": 1
            }, {}, {});
            flxBreadcrumb.setDefaultUnit(kony.flex.DP);
            var breadcrumbs = new com.adminConsole.common.breadcrumbs({
                "height": "20px",
                "id": "breadcrumbs",
                "isVisible": true,
                "layoutType": kony.flex.FLOW_HORIZONTAL,
                "left": "0dp",
                "masterType": constants.MASTER_TYPE_DEFAULT,
                "isModalContainer": false,
                "skin": "sknflxf5f6f8Op100",
                "top": "0dp",
                "width": "100%",
                "overrides": {
                    "btnBackToMain": {
                        "text": "TERMS & CONDITIONS LIST"
                    },
                    "imgBreadcrumbsDown": {
                        "src": "img_down_arrow.png"
                    },
                    "imgBreadcrumbsRight": {
                        "src": "img_breadcrumb_arrow.png"
                    },
                    "imgBreadcrumbsRight2": {
                        "src": "img_breadcrumb_arrow.png"
                    },
                    "imgBreadcrumbsRight3": {
                        "src": "img_breadcrumb_arrow.png"
                    }
                }
            }, {
                "overrides": {}
            }, {
                "overrides": {}
            });
            flxBreadcrumb.add(breadcrumbs);
            flxMainHeader.add(mainHeader, flxBreadcrumb);
            var flxMainSubHeader = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "58dp",
                "id": "flxMainSubHeader",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "105dp",
                "width": "100%",
                "zIndex": 7
            }, {}, {});
            flxMainSubHeader.setDefaultUnit(kony.flex.DP);
            var subHeader = new com.adminConsole.header.subHeader({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "centerY": "50%",
                "height": "48dp",
                "id": "subHeader",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "masterType": constants.MASTER_TYPE_DEFAULT,
                "isModalContainer": false,
                "skin": "CopyslFbox2",
                "top": "0dp",
                "width": "100%",
                "overrides": {
                    "flxMenu": {
                        "isVisible": false,
                        "left": "35dp"
                    },
                    "flxSearch": {
                        "top": "0px",
                        "width": "350px"
                    },
                    "flxSearchContainer": {
                        "left": "0dp",
                        "right": "0dp",
                        "top": "5dp"
                    },
                    "flxSubHeader": {
                        "bottom": "0dp",
                        "height": "100%",
                        "top": "0dp"
                    },
                    "subHeader": {
                        "centerY": "50%",
                        "height": "48dp",
                        "top": "0dp"
                    },
                    "tbxSearchBox": {
                        "placeholder": "Search by Code / Title"
                    }
                }
            }, {
                "overrides": {}
            }, {
                "overrides": {}
            });
            flxMainSubHeader.add(subHeader);
            var flxHeaderDropdown = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                "clipBounds": true,
                "id": "flxHeaderDropdown",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "isModalContainer": false,
                "right": "35dp",
                "skin": "slFbox",
                "top": "20dp",
                "width": "400dp",
                "zIndex": 11
            }, {}, {});
            flxHeaderDropdown.setDefaultUnit(kony.flex.DP);
            var dropdownMainHeader = new com.adminConsole.common.dropdownMainHeader({
                "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                "id": "dropdownMainHeader",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "masterType": constants.MASTER_TYPE_DEFAULT,
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0dp",
                "width": "100%",
                "overrides": {
                    "flxDropdown": {
                        "isVisible": false
                    },
                    "imgUpArrow": {
                        "src": "uparrow_2x.png"
                    }
                }
            }, {
                "overrides": {}
            }, {
                "overrides": {}
            });
            flxHeaderDropdown.add(dropdownMainHeader);
            var flxScrollMainContent = new kony.ui.FlexContainer({
                "bottom": 0,
                "clipBounds": false,
                "id": "flxScrollMainContent",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0px",
                "isModalContainer": false,
                "right": "0px",
                "top": "146px",
                "width": "100%",
                "zIndex": 1
            }, {}, {});
            flxScrollMainContent.setDefaultUnit(kony.flex.DP);
            var flxNoTermsAndConditions = new kony.ui.FlexScrollContainer({
                "allowHorizontalBounce": false,
                "allowVerticalBounce": true,
                "bottom": "0px",
                "bounces": true,
                "clipBounds": true,
                "enableScrolling": true,
                "height": "350px",
                "horizontalScrollIndicator": true,
                "id": "flxNoTermsAndConditions",
                "isVisible": false,
                "layoutType": kony.flex.FREE_FORM,
                "left": "35px",
                "pagingEnabled": false,
                "right": "0px",
                "scrollDirection": kony.flex.SCROLL_VERTICAL,
                "skin": "slFSbox",
                "top": "15px",
                "verticalScrollIndicator": true,
                "zIndex": 1
            }, {}, {});
            flxNoTermsAndConditions.setDefaultUnit(kony.flex.DP);
            var noStaticData = new com.adminConsole.staticContent.noStaticData({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "height": "340px",
                "id": "noStaticData",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0px",
                "masterType": constants.MASTER_TYPE_DEFAULT,
                "isModalContainer": false,
                "right": "35px",
                "skin": "slFbox",
                "top": "0dp",
                "zIndex": 1,
                "overrides": {
                    "btnAddStaticContent": {
                        "maxWidth": "218px",
                        "width": "218px"
                    },
                    "noStaticData": {
                        "centerX": "viz.val_cleared",
                        "left": "0px"
                    }
                }
            }, {
                "overrides": {}
            }, {
                "overrides": {}
            });
            flxNoTermsAndConditions.add(noStaticData);
            var flxDetailTermsAndConditions = new kony.ui.FlexContainer({
                "bottom": 0,
                "clipBounds": false,
                "height": "100%",
                "id": "flxDetailTermsAndConditions",
                "isVisible": false,
                "layoutType": kony.flex.FLOW_VERTICAL,
                "left": "0px",
                "isModalContainer": false,
                "right": "35px",
                "skin": "slFbox",
                "top": "-10px",
                "zIndex": 1
            }, {}, {});
            flxDetailTermsAndConditions.setDefaultUnit(kony.flex.DP);
            var flxDetailTermsAndConditionsWrapper = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                "clipBounds": true,
                "id": "flxDetailTermsAndConditionsWrapper",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "35dp",
                "isModalContainer": false,
                "right": "35dp",
                "skin": "CopysknflxffffffOp0e4fae09f31bc4e",
                "top": "5dp",
                "width": "97.50%",
                "zIndex": 1
            }, {}, {});
            flxDetailTermsAndConditionsWrapper.setDefaultUnit(kony.flex.DP);
            var lblTandCHeading = new kony.ui.Label({
                "id": "lblTandCHeading",
                "isVisible": true,
                "left": "20dp",
                "skin": "sknLatoRegular485C7518px",
                "text": "DBX footer terms & conditions",
                "top": "15dp",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var lblTandCHeadingSeperator = new kony.ui.Label({
                "centerX": "50%",
                "height": "1px",
                "id": "lblTandCHeadingSeperator",
                "isVisible": true,
                "left": "20dp",
                "right": "35px",
                "skin": "sknLblSeparator",
                "text": "-",
                "top": "50dp",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var flxViewEditButton = new kony.ui.Button({
                "bottom": "0px",
                "focusSkin": "sknbtnf7f7faLatoRegular12Px485c75Border1px485C75Radius20px",
                "height": "22px",
                "id": "flxViewEditButton",
                "isVisible": true,
                "right": "35px",
                "skin": "sknbtnf7f7faLatoRegular12Px485c75Border1px485C75Radius20px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.roles.Edit\")",
                "top": "17px",
                "width": "50px",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_CENTER,
                "displayText": true,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {
                "hoverSkin": "sknbtnffffffLatoRegular12Px485c75Border1px485C75Radius20px"
            });
            var flxEditBody = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "bottom": "0dp",
                "clipBounds": true,
                "id": "flxEditBody",
                "isVisible": false,
                "layoutType": kony.flex.FLOW_VERTICAL,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "93dp",
                "width": "100%"
            }, {}, {});
            flxEditBody.setDefaultUnit(kony.flex.DP);
            var flxEditBodyWrapper = new kony.ui.FlexScrollContainer({
                "allowHorizontalBounce": false,
                "allowVerticalBounce": true,
                "bounces": true,
                "clipBounds": true,
                "enableScrolling": true,
                "height": "70%",
                "horizontalScrollIndicator": true,
                "id": "flxEditBodyWrapper",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "pagingEnabled": false,
                "scrollDirection": kony.flex.SCROLL_VERTICAL,
                "skin": "slFSbox",
                "top": "0dp",
                "verticalScrollIndicator": true,
                "width": "100%"
            }, {}, {});
            flxEditBodyWrapper.setDefaultUnit(kony.flex.DP);
            var flxFirstRow = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                "clipBounds": true,
                "id": "flxFirstRow",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0dp",
                "width": "100%",
                "zIndex": 5
            }, {}, {});
            flxFirstRow.setDefaultUnit(kony.flex.DP);
            var flxEditTitle = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                "clipBounds": true,
                "id": "flxEditTitle",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0dp",
                "width": "430dp"
            }, {}, {});
            flxEditTitle.setDefaultUnit(kony.flex.DP);
            var lblEditTitleHeading = new kony.ui.Label({
                "id": "lblEditTitleHeading",
                "isVisible": true,
                "left": "20dp",
                "skin": "sknLbl78818A11px",
                "text": "Title",
                "top": "0dp",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var lblHeadingCount = new kony.ui.Label({
                "id": "lblHeadingCount",
                "isVisible": false,
                "right": "5dp",
                "skin": "sknLbl78818A11px",
                "text": "0/50",
                "top": "0dp",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var tbxTitle = new kony.ui.TextBox2({
                "autoCapitalize": constants.TEXTBOX_AUTO_CAPITALIZE_NONE,
                "focusSkin": "defTextBoxFocus",
                "height": "40dp",
                "id": "tbxTitle",
                "isVisible": true,
                "keyBoardStyle": constants.TEXTBOX_KEY_BOARD_STYLE_DEFAULT,
                "left": "20dp",
                "maxTextLength": 50,
                "i18n_placeholder": "kony.i18n.getLocalizedString(\"i18n.decisionManagement.lblEnterHere\")",
                "secureTextEntry": false,
                "skin": "skntxtbx484b5214px",
                "textInputMode": constants.TEXTBOX_INPUT_MODE_ANY,
                "top": "30dp",
                "width": "400dp",
                "zIndex": 1
            }, {
                "containerHeightMode": constants.TEXTBOX_FONT_METRICS_DRIVEN_HEIGHT,
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [3, 0, 0, 0],
                "paddingInPixel": false
            }, {
                "autoCorrect": false,
                "placeholderSkin": "defTextBoxPlaceholder"
            });
            var flxTitleError = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "15px",
                "id": "flxTitleError",
                "isVisible": false,
                "layoutType": kony.flex.FREE_FORM,
                "left": "20px",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "75px",
                "width": "300px",
                "zIndex": 1
            }, {}, {});
            flxTitleError.setDefaultUnit(kony.flex.DP);
            var lblTitleError = new kony.ui.Label({
                "height": "15px",
                "id": "lblTitleError",
                "isVisible": true,
                "left": "0px",
                "skin": "sknErrorIcon",
                "text": "",
                "top": "0px",
                "width": "15px",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var lblTitleErrorDesc = new kony.ui.Label({
                "height": "15px",
                "id": "lblTitleErrorDesc",
                "isVisible": true,
                "left": "15px",
                "skin": "sknlblError",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmTermsAndConditions.titleBlankMessage\")",
                "top": "0px",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxTitleError.add(lblTitleError, lblTitleErrorDesc);
            flxEditTitle.add(lblEditTitleHeading, lblHeadingCount, tbxTitle, flxTitleError);
            var flxEditApplicableApps = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                "clipBounds": true,
                "id": "flxEditApplicableApps",
                "isVisible": false,
                "layoutType": kony.flex.FREE_FORM,
                "isModalContainer": false,
                "right": "40dp",
                "skin": "slFbox",
                "top": "0dp",
                "width": "400dp"
            }, {}, {});
            flxEditApplicableApps.setDefaultUnit(kony.flex.DP);
            var lblEditApplicableAppsEdit = new kony.ui.Label({
                "id": "lblEditApplicableAppsEdit",
                "isVisible": true,
                "left": "20dp",
                "skin": "sknLbl78818A11px",
                "text": "Applicable Apps",
                "top": "0dp",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var customListboxApps = new com.adminConsole.alerts.customListbox({
                "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                "id": "customListboxApps",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "20dp",
                "masterType": constants.MASTER_TYPE_DEFAULT,
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "30dp",
                "width": "400dp",
                "overrides": {
                    "customListbox": {
                        "isVisible": true,
                        "left": "20dp",
                        "top": "30dp",
                        "width": "400dp"
                    },
                    "flxDropdown": {
                        "top": "5dp"
                    },
                    "flxListboxError": {
                        "isVisible": false
                    },
                    "flxSegmentList": {
                        "isVisible": false,
                        "width": "94%",
                        "zIndex": 1
                    },
                    "flxSelectedText": {
                        "width": "94%"
                    },
                    "imgCheckBox": {
                        "src": "checkboxnormal.png"
                    },
                    "segList": {
                        "data": [{
                            "imgCheckBox": "checkboxnormal.png",
                            "lblDescription": "All"
                        }, {
                            "imgCheckBox": "checkboxnormal.png",
                            "lblDescription": "Retail banking native & responsive web"
                        }, {
                            "imgCheckBox": "checkboxnormal.png",
                            "lblDescription": "Micro-business banking"
                        }, {
                            "imgCheckBox": "checkboxnormal.png",
                            "lblDescription": "Small business banking"
                        }],
                        "width": "95%"
                    }
                }
            }, {
                "overrides": {}
            }, {
                "overrides": {}
            });
            flxEditApplicableApps.add(lblEditApplicableAppsEdit, customListboxApps);
            flxFirstRow.add(flxEditTitle, flxEditApplicableApps);
            var flxEditDescription = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "135dp",
                "id": "flxEditDescription",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "isModalContainer": false,
                "right": "0dp",
                "skin": "slFbox",
                "top": "100dp"
            }, {}, {});
            flxEditDescription.setDefaultUnit(kony.flex.DP);
            var lblEditDescriptionEdit = new kony.ui.Label({
                "id": "lblEditDescriptionEdit",
                "isVisible": true,
                "left": "20dp",
                "skin": "sknLbl78818A11px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.ConfigurationBundles.description\")",
                "top": "0dp",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var lblDescriptionCount = new kony.ui.Label({
                "id": "lblDescriptionCount",
                "isVisible": false,
                "right": "20dp",
                "skin": "sknLbl78818A11px",
                "text": "0/250",
                "top": "0dp",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var txtAreaDescription = new kony.ui.TextArea2({
                "autoCapitalize": constants.TEXTAREA_AUTO_CAPITALIZE_NONE,
                "focusSkin": "defTextAreaFocus",
                "height": "100dp",
                "id": "txtAreaDescription",
                "isVisible": true,
                "keyBoardStyle": constants.TEXTAREA_KEY_BOARD_STYLE_DEFAULT,
                "left": "20dp",
                "maxTextLength": 250,
                "numberOfVisibleLines": 3,
                "i18n_placeholder": "kony.i18n.getLocalizedString(\"i18n.decisionManagement.lblEnterHere\")",
                "right": "40dp",
                "skin": "skntxtAreaLato35475f14Px",
                "textInputMode": constants.TEXTAREA_INPUT_MODE_ANY,
                "top": "30dp",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_TOP_LEFT,
                "padding": [1, 2, 2, 2],
                "paddingInPixel": false
            }, {
                "autoCorrect": false,
                "placeholderSkin": "defTextAreaPlaceholder"
            });
            flxEditDescription.add(lblEditDescriptionEdit, lblDescriptionCount, txtAreaDescription);
            var flxEditContent = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "280dp",
                "id": "flxEditContent",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "250dp",
                "width": kony.flex.USE_PREFFERED_SIZE
            }, {}, {});
            flxEditContent.setDefaultUnit(kony.flex.DP);
            var flxEditContentWrapper = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "100%",
                "id": "flxEditContentWrapper",
                "isVisible": true,
                "layoutType": kony.flex.FLOW_VERTICAL,
                "left": "0px",
                "isModalContainer": false,
                "right": "0px",
                "top": "0px",
                "zIndex": 1
            }, {}, {});
            flxEditContentWrapper.setDefaultUnit(kony.flex.DP);
            var lblContentHeading = new kony.ui.Label({
                "id": "lblContentHeading",
                "isVisible": true,
                "left": "20px",
                "skin": "sknLbl78818A11px",
                "text": "Content",
                "top": "10px",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var flxContentType = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "40px",
                "id": "flxContentType",
                "isVisible": false,
                "layoutType": kony.flex.FLOW_HORIZONTAL,
                "left": "20px",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "5px",
                "width": "210px",
                "zIndex": 1
            }, {}, {});
            flxContentType.setDefaultUnit(kony.flex.DP);
            var flxYes = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "centerY": "50%",
                "clipBounds": true,
                "height": "20dp",
                "id": "flxYes",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "slFbox",
                "width": "20px",
                "zIndex": 1
            }, {}, {
                "hoverSkin": "sknCursor"
            });
            flxYes.setDefaultUnit(kony.flex.DP);
            var imgYes = new kony.ui.Image2({
                "centerX": "50%",
                "centerY": "50%",
                "height": "15dp",
                "id": "imgYes",
                "isVisible": true,
                "skin": "slImage",
                "src": "radio_selected.png",
                "width": "15px",
                "zIndex": 1
            }, {
                "imageScaleMode": constants.IMAGE_SCALE_MODE_MAINTAIN_ASPECT_RATIO,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxYes.add(imgYes);
            var lblYes = new kony.ui.Label({
                "centerY": "50%",
                "id": "lblYes",
                "isVisible": true,
                "left": "5dp",
                "skin": "slLabel0d20174dce8ea42",
                "text": "Text",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var flxNo = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "centerY": "50%",
                "clipBounds": true,
                "height": "20dp",
                "id": "flxNo",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "20px",
                "isModalContainer": false,
                "skin": "slFbox",
                "width": "20px",
                "zIndex": 1
            }, {}, {
                "hoverSkin": "sknCursor"
            });
            flxNo.setDefaultUnit(kony.flex.DP);
            var imgNo = new kony.ui.Image2({
                "centerX": "50%",
                "centerY": "50%",
                "height": "15dp",
                "id": "imgNo",
                "isVisible": true,
                "skin": "slImage",
                "src": "radio_notselected.png",
                "width": "15dp",
                "zIndex": 1
            }, {
                "imageScaleMode": constants.IMAGE_SCALE_MODE_MAINTAIN_ASPECT_RATIO,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxNo.add(imgNo);
            var lblNo = new kony.ui.Label({
                "centerY": "50%",
                "id": "lblNo",
                "isVisible": true,
                "left": "5dp",
                "skin": "slLabel0d20174dce8ea42",
                "text": "URL",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxContentType.add(flxYes, lblYes, flxNo, lblNo);
            var flxURLTextBox = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "40dp",
                "id": "flxURLTextBox",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "20dp",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "10dp",
                "width": "100%"
            }, {}, {});
            flxURLTextBox.setDefaultUnit(kony.flex.DP);
            var tbxURLcontent = new kony.ui.TextBox2({
                "autoCapitalize": constants.TEXTBOX_AUTO_CAPITALIZE_NONE,
                "focusSkin": "defTextBoxFocus",
                "height": "40dp",
                "id": "tbxURLcontent",
                "isVisible": true,
                "keyBoardStyle": constants.TEXTBOX_KEY_BOARD_STYLE_DEFAULT,
                "left": "0dp",
                "i18n_placeholder": "kony.i18n.getLocalizedString(\"i18n.decisionManagement.lblEnterHere\")",
                "right": "40dp",
                "secureTextEntry": false,
                "skin": "skntxtbx484b5214px",
                "textInputMode": constants.TEXTBOX_INPUT_MODE_ANY,
                "top": "0dp",
                "zIndex": 1
            }, {
                "containerHeightMode": constants.TEXTBOX_FONT_METRICS_DRIVEN_HEIGHT,
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [3, 0, 0, 0],
                "paddingInPixel": false
            }, {
                "autoCorrect": false,
                "placeholderSkin": "defTextBoxPlaceholder"
            });
            flxURLTextBox.add(tbxURLcontent);
            var flxContentRichTxt = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "180px",
                "id": "flxContentRichTxt",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "20px",
                "isModalContainer": false,
                "skin": "sknflxffffffop100Bordercbcdd1Radius3px",
                "top": "10px",
                "width": "96%",
                "zIndex": 1
            }, {}, {});
            flxContentRichTxt.setDefaultUnit(kony.flex.DP);
            var rtxTnCEdit = new kony.ui.Browser({
                "detectTelNumber": true,
                "enableZoom": false,
                "height": "100%",
                "id": "rtxTnCEdit",
                "isVisible": true,
                "left": "0px",
                "setAsContent": false,
                "requestURLConfig": {
                    "URL": "richtext.html",
                    "requestMethod": constants.BROWSER_REQUEST_METHOD_GET
                },
                "right": "0dp",
                "top": "0px",
                "zIndex": 1
            }, {}, {});
            flxContentRichTxt.add(rtxTnCEdit);
            var flxContentError = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "15px",
                "id": "flxContentError",
                "isVisible": false,
                "layoutType": kony.flex.FREE_FORM,
                "left": "20px",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "7px",
                "width": "300px",
                "zIndex": 1
            }, {}, {});
            flxContentError.setDefaultUnit(kony.flex.DP);
            var lblContextErrorIcon = new kony.ui.Label({
                "height": "15px",
                "id": "lblContextErrorIcon",
                "isVisible": true,
                "left": "0px",
                "skin": "sknErrorIcon",
                "text": "",
                "top": "0px",
                "width": "15px",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var lblContextErrorMessage = new kony.ui.Label({
                "height": "15px",
                "id": "lblContextErrorMessage",
                "isVisible": true,
                "left": "15px",
                "skin": "sknlblError",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmTermsAndConditions.contentBlankMessage\")",
                "top": "0px",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxContentError.add(lblContextErrorIcon, lblContextErrorMessage);
            flxEditContentWrapper.add(lblContentHeading, flxContentType, flxURLTextBox, flxContentRichTxt, flxContentError);
            flxEditContent.add(flxEditContentWrapper);
            flxEditBodyWrapper.add(flxFirstRow, flxEditDescription, flxEditContent);
            var lblButtonSeperator = new kony.ui.Label({
                "centerX": "50%",
                "height": "1px",
                "id": "lblButtonSeperator",
                "isVisible": true,
                "left": "20dp",
                "skin": "sknLblSeparator",
                "text": "-",
                "top": "10dp",
                "width": "95%",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var flxCommonButtons = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "bottom": "0px",
                "clipBounds": true,
                "height": "80px",
                "id": "flxCommonButtons",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0px",
                "isModalContainer": false,
                "right": "0px",
                "skin": "slFbox",
                "width": "100%",
                "zIndex": 1
            }, {}, {});
            flxCommonButtons.setDefaultUnit(kony.flex.DP);
            var flxCommonButtonsInner = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "100%",
                "id": "flxCommonButtonsInner",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "35px",
                "isModalContainer": false,
                "right": "35px",
                "skin": "slFbox",
                "top": "0px",
                "zIndex": 1
            }, {}, {});
            flxCommonButtonsInner.setDefaultUnit(kony.flex.DP);
            var commonButtons = new com.adminConsole.common.commonButtons({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "height": "80px",
                "id": "commonButtons",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "masterType": constants.MASTER_TYPE_DEFAULT,
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0dp",
                "width": "100%",
                "overrides": {
                    "btnNext": {
                        "isVisible": false
                    },
                    "btnSave": {
                        "i18n_text": "kony.i18n.getLocalizedString(\"i18n.ConfigurationBundles.buttonUpdate\")"
                    }
                }
            }, {
                "overrides": {}
            }, {
                "overrides": {}
            });
            flxCommonButtonsInner.add(commonButtons);
            flxCommonButtons.add(flxCommonButtonsInner);
            flxEditBody.add(flxEditBodyWrapper, lblButtonSeperator, flxCommonButtons);
            var flxDetailBody = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                "clipBounds": true,
                "id": "flxDetailBody",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "65dp",
                "width": "100%"
            }, {}, {});
            flxDetailBody.setDefaultUnit(kony.flex.DP);
            var flxTitleDetails = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                "clipBounds": true,
                "id": "flxTitleDetails",
                "isVisible": false,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0dp",
                "width": "25%"
            }, {}, {});
            flxTitleDetails.setDefaultUnit(kony.flex.DP);
            var lblTitleHeading = new kony.ui.Label({
                "id": "lblTitleHeading",
                "isVisible": true,
                "left": "20dp",
                "skin": "sknLbl78818A11px",
                "text": "TITLE",
                "top": "0dp",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var lblTitleValue = new kony.ui.Label({
                "id": "lblTitleValue",
                "isVisible": true,
                "left": "20dp",
                "skin": "sknLbl485C75LatoRegular13Px",
                "text": "DBX footer terms & conditions",
                "top": "31dp",
                "width": "90%",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxTitleDetails.add(lblTitleHeading, lblTitleValue);
            var flxApplicableAppsDetails = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                "clipBounds": true,
                "id": "flxApplicableAppsDetails",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "363dp",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0dp",
                "width": "40%"
            }, {}, {});
            flxApplicableAppsDetails.setDefaultUnit(kony.flex.DP);
            var lblApplicableAppsHeadings = new kony.ui.Label({
                "id": "lblApplicableAppsHeadings",
                "isVisible": true,
                "left": "20dp",
                "skin": "sknlblLato696c7311px",
                "text": "APPLICABLE APPS",
                "top": "0dp",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var lblApplicableAppsValue = new kony.ui.Label({
                "id": "lblApplicableAppsValue",
                "isVisible": true,
                "left": "20dp",
                "skin": "sknlbl485C75LatoSemiBold13px",
                "text": "Retail Banking, Business Banking, Onboarding Applications",
                "top": "21dp",
                "width": "360dp",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxApplicableAppsDetails.add(lblApplicableAppsHeadings, lblApplicableAppsValue);
            var flxDescription = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                "bottom": "30dp",
                "clipBounds": true,
                "id": "flxDescription",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "isModalContainer": false,
                "right": "70dp",
                "skin": "slFbox",
                "top": "65dp",
                "width": kony.flex.USE_PREFFERED_SIZE
            }, {}, {});
            flxDescription.setDefaultUnit(kony.flex.DP);
            var lblDescriptionHeading = new kony.ui.Label({
                "id": "lblDescriptionHeading",
                "isVisible": true,
                "left": "20dp",
                "skin": "sknlblLato696c7311px",
                "text": "DESCRIPTION",
                "top": "0dp",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var lblDescriptionValue = new kony.ui.Label({
                "id": "lblDescriptionValue",
                "isVisible": true,
                "left": "20dp",
                "right": "40dp",
                "skin": "sknlbl485C75LatoSemiBold13px",
                "text": "This terms & conditions will be used in the footer link of retail banking",
                "top": "21dp",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxDescription.add(lblDescriptionHeading, lblDescriptionValue);
            var flxCodeDetails = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                "clipBounds": true,
                "id": "flxCodeDetails",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0dp",
                "width": "35%"
            }, {}, {});
            flxCodeDetails.setDefaultUnit(kony.flex.DP);
            var lblCodeHeading = new kony.ui.Label({
                "id": "lblCodeHeading",
                "isVisible": true,
                "left": "20dp",
                "skin": "sknlblLato696c7311px",
                "text": "CODE",
                "top": "0dp",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var lblCodeValue = new kony.ui.Label({
                "id": "lblCodeValue",
                "isVisible": true,
                "left": "20dp",
                "skin": "sknlbl485C75LatoSemiBold13px",
                "text": "RB_Footer_T&C",
                "top": "21dp",
                "width": "300dp",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxCodeDetails.add(lblCodeHeading, lblCodeValue);
            var flxLastModifiedOn = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                "clipBounds": true,
                "id": "flxLastModifiedOn",
                "isVisible": false,
                "layoutType": kony.flex.FREE_FORM,
                "left": "363dp",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "155dp",
                "width": "20%"
            }, {}, {});
            flxLastModifiedOn.setDefaultUnit(kony.flex.DP);
            var lblLastModifiedOnHeading = new kony.ui.Label({
                "id": "lblLastModifiedOnHeading",
                "isVisible": true,
                "left": "20dp",
                "skin": "sknlblLato696c7311px",
                "text": "LAST MODIFIED ON",
                "top": "0dp",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var lblLastModifiedOnValue = new kony.ui.Label({
                "id": "lblLastModifiedOnValue",
                "isVisible": true,
                "left": "20dp",
                "skin": "sknlbl485C75LatoSemiBold13px",
                "text": "01/08/2019",
                "top": "31dp",
                "width": "177dp",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxLastModifiedOn.add(lblLastModifiedOnHeading, lblLastModifiedOnValue);
            var flxLastModifiedBy = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                "clipBounds": true,
                "id": "flxLastModifiedBy",
                "isVisible": false,
                "layoutType": kony.flex.FREE_FORM,
                "isModalContainer": false,
                "right": "102dp",
                "skin": "slFbox",
                "top": "155dp",
                "width": "20%"
            }, {}, {});
            flxLastModifiedBy.setDefaultUnit(kony.flex.DP);
            var lblLastModifiedByHeading = new kony.ui.Label({
                "id": "lblLastModifiedByHeading",
                "isVisible": true,
                "left": "20dp",
                "skin": "sknlblLato696c7311px",
                "text": "LAST MODIFIED BY",
                "top": "0dp",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var lblLastModifiedByValue = new kony.ui.Label({
                "id": "lblLastModifiedByValue",
                "isVisible": true,
                "left": "20dp",
                "skin": "sknlbl485C75LatoSemiBold13px",
                "text": "Peter Smith William",
                "top": "31dp",
                "width": "177dp",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxLastModifiedBy.add(lblLastModifiedByHeading, lblLastModifiedByValue);
            var flxContent = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                "clipBounds": true,
                "id": "flxContent",
                "isVisible": false,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "232dp",
                "width": kony.flex.USE_PREFFERED_SIZE
            }, {}, {});
            flxContent.setDefaultUnit(kony.flex.DP);
            var lblContent = new kony.ui.Label({
                "id": "lblContent",
                "isVisible": true,
                "left": "20dp",
                "skin": "sknLbl78818A11px",
                "text": "CONTENT",
                "top": "0dp",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var lblUrlContent = new kony.ui.Label({
                "id": "lblUrlContent",
                "isVisible": false,
                "left": "20dp",
                "right": "20dp",
                "skin": "sknLbl485C75LatoRegular13Px",
                "text": "www.google.com",
                "top": "33dp",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var staticData = new com.adminConsole.staticContent.staticData({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "bottom": "50dp",
                "id": "staticData",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "20px",
                "masterType": constants.MASTER_TYPE_DEFAULT,
                "isModalContainer": false,
                "right": "20px",
                "skin": "sknFlxFFFFFFbrdre1e5ed",
                "top": "33px",
                "zIndex": 1,
                "overrides": {
                    "flxDeactivateOption": {
                        "isVisible": false
                    },
                    "flxHeader": {
                        "isVisible": false
                    },
                    "flxSelectOptions": {
                        "isVisible": false,
                        "top": "62px"
                    },
                    "flxStaticContantData": {
                        "bottom": "50px",
                        "left": "0px",
                        "top": "0px",
                        "zIndex": 100,
                        "autogrowMode": kony.flex.AUTOGROW_HEIGHT
                    },
                    "flxStatus": {
                        "width": "60px"
                    },
                    "imgUpArrow": {
                        "src": "uparrow_2x.png"
                    },
                    "lblStaticContentHeader": {
                        "text": "KONY DIGITAL BANKING TERMS AND CONDITIONS"
                    },
                    "rtxViewer": {
                        "height": kony.flex.USE_PREFFERED_SIZE,
                        "right": "0px"
                    },
                    "staticData": {
                        "bottom": "50dp",
                        "height": "viz.val_cleared",
                        "isVisible": true,
                        "left": "20px",
                        "right": "20px",
                        "top": "33px",
                        "width": "viz.val_cleared"
                    }
                }
            }, {
                "overrides": {}
            }, {
                "overrides": {}
            });
            flxContent.add(lblContent, lblUrlContent, staticData);
            flxDetailBody.add(flxTitleDetails, flxApplicableAppsDetails, flxDescription, flxCodeDetails, flxLastModifiedOn, flxLastModifiedBy, flxContent);
            flxDetailTermsAndConditionsWrapper.add(lblTandCHeading, lblTandCHeadingSeperator, flxViewEditButton, flxEditBody, flxDetailBody);
            var flxVersionsList = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "230dp",
                "id": "flxVersionsList",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "35dp",
                "isModalContainer": false,
                "right": "35dp",
                "skin": "CopysknflxffffffOp0e4fae09f31bc4e",
                "top": "20dp",
                "width": "97.50%",
                "zIndex": 1
            }, {}, {});
            flxVersionsList.setDefaultUnit(kony.flex.DP);
            var lblVersionHeading = new kony.ui.Label({
                "id": "lblVersionHeading",
                "isVisible": true,
                "left": "20dp",
                "skin": "sknLatoRegular485C7518px",
                "text": "Content Versions",
                "top": "17dp",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var lblVersionHeadingSeperator = new kony.ui.Label({
                "height": "1px",
                "id": "lblVersionHeadingSeperator",
                "isVisible": true,
                "left": "20dp",
                "right": "20px",
                "skin": "sknLblSeparator",
                "text": "-",
                "top": "52dp",
                "zIndex": 5
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var btnNewVersion = new kony.ui.Button({
                "bottom": "0px",
                "focusSkin": "sknbtnf7f7faLatoRegular12Px485c75Border1px485C75Radius20px",
                "height": "22px",
                "id": "btnNewVersion",
                "isVisible": true,
                "right": "35px",
                "skin": "sknbtnf7f7faLatoRegular12Px485c75Border1px485C75Radius20px",
                "text": "Create New Version",
                "top": "17px",
                "width": "140dp",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_CENTER,
                "displayText": true,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {
                "hoverSkin": "sknbtnffffffLatoRegular12Px485c75Border1px485C75Radius20px"
            });
            var segVersionList = new kony.ui.SegmentedUI2({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "data": [{
                    "fontIconGroupStatus": "",
                    "lblContentType": "Lorem ipsum dolor sit amet, consectetur.Lorem ipsum dolor sit amet, consectetur..Lorem ipsum dolor sit amet, consectetur.",
                    "lblDelete": "kony.i18n.getLocalizedString(\"i18n.frmFAQ.fonticonDelete\")",
                    "lblEdit": "kony.i18n.getLocalizedString(\"i18n.userwidgetmodel.lblIconOption1\")",
                    "lblGroupStatus": "Active",
                    "lblLastModifiedBy": "25",
                    "lblLastModifiedOn": "01/12/2018",
                    "lblSeparator": "Label",
                    "lblVersion": "Platinum Customers"
                }, {
                    "fontIconGroupStatus": "",
                    "lblContentType": "Lorem ipsum dolor sit amet, consectetur.Lorem ipsum dolor sit amet, consectetur..Lorem ipsum dolor sit amet, consectetur.",
                    "lblDelete": "kony.i18n.getLocalizedString(\"i18n.frmFAQ.fonticonDelete\")",
                    "lblEdit": "kony.i18n.getLocalizedString(\"i18n.userwidgetmodel.lblIconOption1\")",
                    "lblGroupStatus": "Active",
                    "lblLastModifiedBy": "25",
                    "lblLastModifiedOn": "01/12/2018",
                    "lblSeparator": "Label",
                    "lblVersion": "Platinum Customers"
                }, {
                    "fontIconGroupStatus": "",
                    "lblContentType": "Lorem ipsum dolor sit amet, consectetur.Lorem ipsum dolor sit amet, consectetur..Lorem ipsum dolor sit amet, consectetur.",
                    "lblDelete": "kony.i18n.getLocalizedString(\"i18n.frmFAQ.fonticonDelete\")",
                    "lblEdit": "kony.i18n.getLocalizedString(\"i18n.userwidgetmodel.lblIconOption1\")",
                    "lblGroupStatus": "Active",
                    "lblLastModifiedBy": "25",
                    "lblLastModifiedOn": "01/12/2018",
                    "lblSeparator": "Label",
                    "lblVersion": "Platinum Customers"
                }, {
                    "fontIconGroupStatus": "",
                    "lblContentType": "Lorem ipsum dolor sit amet, consectetur.Lorem ipsum dolor sit amet, consectetur..Lorem ipsum dolor sit amet, consectetur.",
                    "lblDelete": "kony.i18n.getLocalizedString(\"i18n.frmFAQ.fonticonDelete\")",
                    "lblEdit": "kony.i18n.getLocalizedString(\"i18n.userwidgetmodel.lblIconOption1\")",
                    "lblGroupStatus": "Active",
                    "lblLastModifiedBy": "25",
                    "lblLastModifiedOn": "01/12/2018",
                    "lblSeparator": "Label",
                    "lblVersion": "Platinum Customers"
                }, {
                    "fontIconGroupStatus": "",
                    "lblContentType": "Lorem ipsum dolor sit amet, consectetur.Lorem ipsum dolor sit amet, consectetur..Lorem ipsum dolor sit amet, consectetur.",
                    "lblDelete": "kony.i18n.getLocalizedString(\"i18n.frmFAQ.fonticonDelete\")",
                    "lblEdit": "kony.i18n.getLocalizedString(\"i18n.userwidgetmodel.lblIconOption1\")",
                    "lblGroupStatus": "Active",
                    "lblLastModifiedBy": "25",
                    "lblLastModifiedOn": "01/12/2018",
                    "lblSeparator": "Label",
                    "lblVersion": "Platinum Customers"
                }, {
                    "fontIconGroupStatus": "",
                    "lblContentType": "Lorem ipsum dolor sit amet, consectetur.Lorem ipsum dolor sit amet, consectetur..Lorem ipsum dolor sit amet, consectetur.",
                    "lblDelete": "kony.i18n.getLocalizedString(\"i18n.frmFAQ.fonticonDelete\")",
                    "lblEdit": "kony.i18n.getLocalizedString(\"i18n.userwidgetmodel.lblIconOption1\")",
                    "lblGroupStatus": "Active",
                    "lblLastModifiedBy": "25",
                    "lblLastModifiedOn": "01/12/2018",
                    "lblSeparator": "Label",
                    "lblVersion": "Platinum Customers"
                }, {
                    "fontIconGroupStatus": "",
                    "lblContentType": "Lorem ipsum dolor sit amet, consectetur.Lorem ipsum dolor sit amet, consectetur..Lorem ipsum dolor sit amet, consectetur.",
                    "lblDelete": "kony.i18n.getLocalizedString(\"i18n.frmFAQ.fonticonDelete\")",
                    "lblEdit": "kony.i18n.getLocalizedString(\"i18n.userwidgetmodel.lblIconOption1\")",
                    "lblGroupStatus": "Active",
                    "lblLastModifiedBy": "25",
                    "lblLastModifiedOn": "01/12/2018",
                    "lblSeparator": "Label",
                    "lblVersion": "Platinum Customers"
                }, {
                    "fontIconGroupStatus": "",
                    "lblContentType": "Lorem ipsum dolor sit amet, consectetur.Lorem ipsum dolor sit amet, consectetur..Lorem ipsum dolor sit amet, consectetur.",
                    "lblDelete": "kony.i18n.getLocalizedString(\"i18n.frmFAQ.fonticonDelete\")",
                    "lblEdit": "kony.i18n.getLocalizedString(\"i18n.userwidgetmodel.lblIconOption1\")",
                    "lblGroupStatus": "Active",
                    "lblLastModifiedBy": "25",
                    "lblLastModifiedOn": "01/12/2018",
                    "lblSeparator": "Label",
                    "lblVersion": "Platinum Customers"
                }, {
                    "fontIconGroupStatus": "",
                    "lblContentType": "Lorem ipsum dolor sit amet, consectetur.Lorem ipsum dolor sit amet, consectetur..Lorem ipsum dolor sit amet, consectetur.",
                    "lblDelete": "kony.i18n.getLocalizedString(\"i18n.frmFAQ.fonticonDelete\")",
                    "lblEdit": "kony.i18n.getLocalizedString(\"i18n.userwidgetmodel.lblIconOption1\")",
                    "lblGroupStatus": "Active",
                    "lblLastModifiedBy": "25",
                    "lblLastModifiedOn": "01/12/2018",
                    "lblSeparator": "Label",
                    "lblVersion": "Platinum Customers"
                }, {
                    "fontIconGroupStatus": "",
                    "lblContentType": "Lorem ipsum dolor sit amet, consectetur.Lorem ipsum dolor sit amet, consectetur..Lorem ipsum dolor sit amet, consectetur.",
                    "lblDelete": "kony.i18n.getLocalizedString(\"i18n.frmFAQ.fonticonDelete\")",
                    "lblEdit": "kony.i18n.getLocalizedString(\"i18n.userwidgetmodel.lblIconOption1\")",
                    "lblGroupStatus": "Active",
                    "lblLastModifiedBy": "25",
                    "lblLastModifiedOn": "01/12/2018",
                    "lblSeparator": "Label",
                    "lblVersion": "Platinum Customers"
                }],
                "groupCells": false,
                "height": "120dp",
                "id": "segVersionList",
                "isVisible": false,
                "left": "20dp",
                "needPageIndicator": true,
                "pageOffDotImage": "pageoffdot.png",
                "pageOnDotImage": "pageondot.png",
                "retainSelection": false,
                "right": "20dp",
                "rowFocusSkin": "seg2Focus",
                "rowSkin": "seg2Normal",
                "rowTemplate": "flxTandCVersionList",
                "sectionHeaderSkin": "sliPhoneSegmentHeader",
                "selectionBehavior": constants.SEGUI_DEFAULT_BEHAVIOR,
                "separatorRequired": false,
                "showScrollbars": false,
                "top": "108dp",
                "viewType": constants.SEGUI_VIEW_TYPE_TABLEVIEW,
                "widgetDataMap": {
                    "flxDelete": "flxDelete",
                    "flxEdit": "flxEdit",
                    "flxOptions": "flxOptions",
                    "flxSegMain": "flxSegMain",
                    "flxStatus": "flxStatus",
                    "flxTandCVersionList": "flxTandCVersionList",
                    "flxVersionMain": "flxVersionMain",
                    "fontIconGroupStatus": "fontIconGroupStatus",
                    "lblContentType": "lblContentType",
                    "lblDelete": "lblDelete",
                    "lblEdit": "lblEdit",
                    "lblGroupStatus": "lblGroupStatus",
                    "lblLastModifiedBy": "lblLastModifiedBy",
                    "lblLastModifiedOn": "lblLastModifiedOn",
                    "lblSeparator": "lblSeparator",
                    "lblVersion": "lblVersion"
                },
                "zIndex": 1
            }, {
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var lstBoxSubAlertLanguages = new kony.ui.ListBox({
                "focusSkin": "defListBoxFocus",
                "height": "30dp",
                "id": "lstBoxSubAlertLanguages",
                "isVisible": false,
                "left": "165dp",
                "masterData": [
                    ["lb1", "English-UK"],
                    ["lb2", "German"],
                    ["lb3", "Spanish"]
                ],
                "skin": "sknListBoxNoBorder",
                "top": "14dp",
                "width": "200dp",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [3, 0, 0, 0],
                "paddingInPixel": false
            }, {
                "multiSelect": false
            });
            var flxLanguageHeader = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "30dp",
                "id": "flxLanguageHeader",
                "isVisible": true,
                "layoutType": kony.flex.FLOW_HORIZONTAL,
                "left": "165dp",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "14dp",
                "width": "200dp"
            }, {}, {});
            flxLanguageHeader.setDefaultUnit(kony.flex.DP);
            var lblSelectedLanguage = new kony.ui.Label({
                "centerY": "50%",
                "id": "lblSelectedLanguage",
                "isVisible": true,
                "left": "0dp",
                "skin": "sknlblLatoBold35475f14px",
                "text": "English (Default)",
                "top": "0dp",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var lblSelectedLanguageIcon = new kony.ui.Label({
                "centerY": "50%",
                "id": "lblSelectedLanguageIcon",
                "isVisible": true,
                "left": "5dp",
                "skin": "sknIcon12pxBlack",
                "text": "",
                "top": "0dp",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 11
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxLanguageHeader.add(lblSelectedLanguage, lblSelectedLanguageIcon);
            var flxVersionListHeader = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "56px",
                "id": "flxVersionListHeader",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": 0,
                "isModalContainer": false,
                "skin": "sknflxffffffop100",
                "top": "53dp",
                "width": "100%",
                "zIndex": 10
            }, {}, {});
            flxVersionListHeader.setDefaultUnit(kony.flex.DP);
            var lblVersionHeaderSeperator = new kony.ui.Label({
                "bottom": "1px",
                "height": "1px",
                "id": "lblVersionHeaderSeperator",
                "isVisible": true,
                "left": "20dp",
                "right": "20px",
                "skin": "sknLblTableHeaderLine",
                "text": "-",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var flxVersionListHeaderWrapper = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "100%",
                "id": "flxVersionListHeaderWrapper",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "20dp",
                "isModalContainer": false,
                "right": "20dp",
                "skin": "slFbox",
                "top": "0dp"
            }, {}, {});
            flxVersionListHeaderWrapper.setDefaultUnit(kony.flex.DP);
            var flxVersion = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "centerY": "50%",
                "clipBounds": true,
                "height": "100%",
                "id": "flxVersion",
                "isVisible": true,
                "layoutType": kony.flex.FLOW_HORIZONTAL,
                "left": "10px",
                "isModalContainer": false,
                "onClick": controller.AS_FlexContainer_i87bd852f9f74801ae5ceff0e9ee81d1,
                "skin": "slFbox",
                "top": 0,
                "width": "10%",
                "zIndex": 1
            }, {}, {});
            flxVersion.setDefaultUnit(kony.flex.DP);
            var lblVersion = new kony.ui.Label({
                "centerY": "50%",
                "id": "lblVersion",
                "isVisible": true,
                "left": "0px",
                "skin": "sknlblLato696c7312px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.decisionManagement.lblVersion\")",
                "top": 0,
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {
                "hoverSkin": "sknlblLatoBold00000012px"
            });
            var lblSortVersion = new kony.ui.Label({
                "centerY": "50%",
                "id": "lblSortVersion",
                "isVisible": true,
                "left": "10dp",
                "skin": "sknIcon15px",
                "text": "",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {
                "hoverSkin": "sknlblCursorFont"
            });
            flxVersion.add(lblVersion, lblSortVersion);
            var flxContentTyp = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "centerY": "50%",
                "clipBounds": true,
                "height": "100%",
                "id": "flxContentTyp",
                "isVisible": true,
                "layoutType": kony.flex.FLOW_HORIZONTAL,
                "left": "15%",
                "isModalContainer": false,
                "onClick": controller.AS_FlexContainer_i88e2961592c4b338de61134a3117f51,
                "skin": "slFbox",
                "top": "0dp",
                "width": "14%",
                "zIndex": 1
            }, {}, {});
            flxContentTyp.setDefaultUnit(kony.flex.DP);
            var lblContentType = new kony.ui.Label({
                "centerY": "50%",
                "id": "lblContentType",
                "isVisible": true,
                "left": "0px",
                "skin": "sknlblLato696c7312px",
                "text": "CONTENT TYPE",
                "top": 0,
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {
                "hoverSkin": "sknlblLatoBold00000012px"
            });
            var flxFilterContentType = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "centerY": "50%",
                "clipBounds": true,
                "height": "20px",
                "id": "flxFilterContentType",
                "isVisible": true,
                "layoutType": kony.flex.FLOW_HORIZONTAL,
                "left": "10dp",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0dp",
                "width": "20px"
            }, {}, {});
            flxFilterContentType.setDefaultUnit(kony.flex.DP);
            var fontIconFilterContentType = new kony.ui.Label({
                "centerX": "50%",
                "centerY": "50%",
                "id": "fontIconFilterContentType",
                "isVisible": true,
                "skin": "sknIcon15px",
                "text": "",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {
                "hoverSkin": "sknlblCursorFont"
            });
            flxFilterContentType.add(fontIconFilterContentType);
            flxContentTyp.add(lblContentType, flxFilterContentType);
            var flxPublishedOn = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "centerY": "50%",
                "clipBounds": true,
                "height": "100%",
                "id": "flxPublishedOn",
                "isVisible": true,
                "layoutType": kony.flex.FLOW_HORIZONTAL,
                "left": "33%",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0dp",
                "width": "17%",
                "zIndex": 1
            }, {}, {});
            flxPublishedOn.setDefaultUnit(kony.flex.DP);
            var lblPublishedOn = new kony.ui.Label({
                "centerY": "50%",
                "id": "lblPublishedOn",
                "isVisible": true,
                "left": "0dp",
                "skin": "sknlblLato696c7312px",
                "text": "LAST MODIFIED DATE",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {
                "hoverSkin": "sknlblLatoBold00000012px"
            });
            var lblFontPublishedOn = new kony.ui.Label({
                "centerY": "50%",
                "id": "lblFontPublishedOn",
                "isVisible": true,
                "left": "5dp",
                "skin": "sknIcon15px",
                "text": "",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {
                "hoverSkin": "sknlblCursorFont"
            });
            flxPublishedOn.add(lblPublishedOn, lblFontPublishedOn);
            var flxPublishedBy = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "centerY": "50%",
                "clipBounds": true,
                "height": "100%",
                "id": "flxPublishedBy",
                "isVisible": true,
                "layoutType": kony.flex.FLOW_HORIZONTAL,
                "left": "54%",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0dp",
                "width": "15%",
                "zIndex": 1
            }, {}, {});
            flxPublishedBy.setDefaultUnit(kony.flex.DP);
            var lblPublishedBy = new kony.ui.Label({
                "centerY": "50%",
                "id": "lblPublishedBy",
                "isVisible": true,
                "left": "0dp",
                "skin": "sknlblLato696c7312px",
                "text": "LAST MODIFIED BY",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {
                "hoverSkin": "sknlblLatoBold00000012px"
            });
            var lblFontPublishedBy = new kony.ui.Label({
                "centerY": "50%",
                "id": "lblFontPublishedBy",
                "isVisible": true,
                "left": "10dp",
                "skin": "sknIcon15px",
                "text": "",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {
                "hoverSkin": "sknlblCursorFont"
            });
            flxPublishedBy.add(lblPublishedBy, lblFontPublishedBy);
            var flxStatus = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "centerY": "50%",
                "clipBounds": true,
                "height": "100%",
                "id": "flxStatus",
                "isVisible": true,
                "layoutType": kony.flex.FLOW_HORIZONTAL,
                "left": "75%",
                "isModalContainer": false,
                "onClick": controller.AS_FlexContainer_i88e2961592c4b338de61134a3117f51,
                "skin": "slFbox",
                "top": "0dp",
                "width": "13%",
                "zIndex": 1
            }, {}, {});
            flxStatus.setDefaultUnit(kony.flex.DP);
            var lblStatus = new kony.ui.Label({
                "centerY": "50%",
                "id": "lblStatus",
                "isVisible": true,
                "left": "0px",
                "skin": "sknlblLato696c7312px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.permission.STATUS\")",
                "top": 0,
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {
                "hoverSkin": "sknlblLatoBold00000012px"
            });
            var flxVersionFilterStatus = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "centerY": "50%",
                "clipBounds": true,
                "height": "20px",
                "id": "flxVersionFilterStatus",
                "isVisible": true,
                "layoutType": kony.flex.FLOW_HORIZONTAL,
                "left": "10dp",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0dp",
                "width": "20px"
            }, {}, {});
            flxVersionFilterStatus.setDefaultUnit(kony.flex.DP);
            var fontIconVersionFilterStatus = new kony.ui.Label({
                "centerX": "50%",
                "centerY": "50%",
                "id": "fontIconVersionFilterStatus",
                "isVisible": true,
                "skin": "sknIcon15px",
                "text": "",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {
                "hoverSkin": "sknlblCursorFont"
            });
            flxVersionFilterStatus.add(fontIconVersionFilterStatus);
            flxStatus.add(lblStatus, flxVersionFilterStatus);
            flxVersionListHeaderWrapper.add(flxVersion, flxContentTyp, flxPublishedOn, flxPublishedBy, flxStatus);
            flxVersionListHeader.add(lblVersionHeaderSeperator, flxVersionListHeaderWrapper);
            var flxNoVersionsFound = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "130dp",
                "id": "flxNoVersionsFound",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "sknflxConfigurationBundlesNoBorder",
                "top": "70dp",
                "width": "100%",
                "zIndex": 1
            }, {}, {});
            flxNoVersionsFound.setDefaultUnit(kony.flex.DP);
            var rtxNoVersionsFound = new kony.ui.RichText({
                "centerX": "50%",
                "centerY": "50%",
                "id": "rtxNoVersionsFound",
                "isVisible": true,
                "skin": "sknRtxLato84939e12Px",
                "text": "No Terms & Conditions content available for",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var btnAddNewVersion = new kony.ui.Button({
                "bottom": "0px",
                "focusSkin": "sknbtnf7f7faLatoRegular12Px485c75Border1px485C75Radius20px",
                "height": "22px",
                "id": "btnAddNewVersion",
                "isVisible": true,
                "right": "380px",
                "skin": "sknbtnf7f7faLatoRegular12Px485c75Border1px485C75Radius20px",
                "text": "Create New Version",
                "top": "92px",
                "width": "150dp",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_CENTER,
                "displayText": true,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {
                "hoverSkin": "sknbtnffffffLatoRegular12Px485c75Border1px485C75Radius20px"
            });
            flxNoVersionsFound.add(rtxNoVersionsFound, btnAddNewVersion);
            var flxContentTypeFilter = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                "clipBounds": true,
                "id": "flxContentTypeFilter",
                "isVisible": false,
                "layoutType": kony.flex.FREE_FORM,
                "isModalContainer": false,
                "right": "150dp",
                "skin": "slFbox",
                "top": "88dp",
                "width": "80dp",
                "zIndex": 11
            }, {}, {});
            flxContentTypeFilter.setDefaultUnit(kony.flex.DP);
            var contentTypeFilter = new com.adminConsole.Products.productTypeFilterMenu({
                "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                "id": "contentTypeFilter",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "masterType": constants.MASTER_TYPE_DEFAULT,
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "5dp",
                "width": "100%",
                "zIndex": 5,
                "overrides": {
                    "flxCheckboxInner": {
                        "height": "65dp"
                    },
                    "flxFilterSort": {
                        "isVisible": false
                    },
                    "flxSearchContainer": {
                        "isVisible": false
                    },
                    "imgUpArrow": {
                        "src": "uparrow_2x.png"
                    },
                    "lblSeperator1": {
                        "isVisible": false
                    },
                    "segStatusFilterDropdown": {
                        "data": [{
                            "imgCheckBox": "checkboxselected.png",
                            "lblDescription": "Text"
                        }, {
                            "imgCheckBox": "checkboxselected.png",
                            "lblDescription": "URL"
                        }]
                    }
                }
            }, {
                "overrides": {}
            }, {
                "overrides": {}
            });
            flxContentTypeFilter.add(contentTypeFilter);
            var flxStatusFilter = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                "clipBounds": true,
                "id": "flxStatusFilter",
                "isVisible": false,
                "layoutType": kony.flex.FREE_FORM,
                "isModalContainer": false,
                "right": "150dp",
                "skin": "slFbox",
                "top": "88dp",
                "width": "135dp",
                "zIndex": 11
            }, {}, {});
            flxStatusFilter.setDefaultUnit(kony.flex.DP);
            var statusFilter = new com.adminConsole.Products.productTypeFilterMenu({
                "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                "id": "statusFilter",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "masterType": constants.MASTER_TYPE_DEFAULT,
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "5dp",
                "width": "100%",
                "zIndex": 5,
                "overrides": {
                    "flxCheckboxInner": {
                        "height": "90px"
                    },
                    "flxFilterSort": {
                        "isVisible": false
                    },
                    "flxSearchContainer": {
                        "isVisible": false
                    },
                    "imgUpArrow": {
                        "src": "uparrow_2x.png"
                    },
                    "lblSeperator1": {
                        "isVisible": false
                    },
                    "segStatusFilterDropdown": {
                        "data": [{
                            "imgCheckBox": "checkboxselected.png",
                            "lblDescription": "Active"
                        }, {
                            "imgCheckBox": "checkboxselected.png",
                            "lblDescription": "Draft"
                        }, {
                            "imgCheckBox": "checkboxselected.png",
                            "lblDescription": "Archived"
                        }]
                    }
                }
            }, {
                "overrides": {}
            }, {
                "overrides": {}
            });
            flxStatusFilter.add(statusFilter);
            var flxLanguages = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                "clipBounds": true,
                "id": "flxLanguages",
                "isVisible": false,
                "layoutType": kony.flex.FREE_FORM,
                "isModalContainer": false,
                "right": "100dp",
                "skin": "slFbox",
                "top": "35px",
                "width": "200dp",
                "zIndex": 11
            }, {}, {});
            flxLanguages.setDefaultUnit(kony.flex.DP);
            var languages = new com.adminConsole.Products.productTypeFilterMenu({
                "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                "id": "languages",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "masterType": constants.MASTER_TYPE_DEFAULT,
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "5dp",
                "width": "100%",
                "zIndex": 5,
                "overrides": {
                    "flxCheckboxInner": {
                        "height": "108px"
                    },
                    "flxFilterSort": {
                        "isVisible": false
                    },
                    "flxSearchContainer": {
                        "isVisible": false
                    },
                    "imgUpArrow": {
                        "src": "uparrow_2x.png"
                    },
                    "lblSeperator1": {
                        "isVisible": false
                    },
                    "segStatusFilterDropdown": {
                        "data": [{
                            "imgCheckBox": "checkbox.png",
                            "lblDescription": "Active"
                        }, {
                            "imgCheckBox": "checkbox.png",
                            "lblDescription": "Archived"
                        }, {
                            "imgCheckBox": "checkbox.png",
                            "lblDescription": "Draft"
                        }]
                    }
                }
            }, {
                "overrides": {}
            }, {
                "overrides": {}
            });
            flxLanguages.add(languages);
            flxVersionsList.add(lblVersionHeading, lblVersionHeadingSeperator, btnNewVersion, segVersionList, lstBoxSubAlertLanguages, flxLanguageHeader, flxVersionListHeader, flxNoVersionsFound, flxContentTypeFilter, flxStatusFilter, flxLanguages);
            flxDetailTermsAndConditions.add(flxDetailTermsAndConditionsWrapper, flxVersionsList);
            var flxTermsAndConditionsList = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "bottom": "0dp",
                "clipBounds": true,
                "id": "flxTermsAndConditionsList",
                "isVisible": false,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "isModalContainer": false,
                "right": "0dp",
                "skin": "slFbox",
                "top": "20dp",
                "zIndex": 1
            }, {}, {});
            flxTermsAndConditionsList.setDefaultUnit(kony.flex.DP);
            var flxTermsAndConditionsListWrapper = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "bottom": "0dp",
                "clipBounds": true,
                "id": "flxTermsAndConditionsListWrapper",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "isModalContainer": false,
                "right": 0,
                "skin": "slFbox",
                "top": "0px",
                "zIndex": 1
            }, {}, {});
            flxTermsAndConditionsListWrapper.setDefaultUnit(kony.flex.DP);
            var flxTandCTable = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "bottom": "10px",
                "clipBounds": true,
                "id": "flxTandCTable",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "35px",
                "isModalContainer": false,
                "right": "35px",
                "skin": "slFbox",
                "top": "0dp",
                "zIndex": 1
            }, {}, {});
            flxTandCTable.setDefaultUnit(kony.flex.DP);
            var flxTandCList = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "bottom": "10px",
                "clipBounds": true,
                "id": "flxTandCList",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "isModalContainer": false,
                "right": "0dp",
                "skin": "sknflxffffffop100Bordercbcdd1Radius3px",
                "top": "0dp"
            }, {}, {});
            flxTandCList.setDefaultUnit(kony.flex.DP);
            var flxTandCHeader = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "55px",
                "id": "flxTandCHeader",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "20dp",
                "isModalContainer": false,
                "right": "20dp",
                "skin": "sknflxffffffop100",
                "top": "0dp",
                "zIndex": 10
            }, {}, {});
            flxTandCHeader.setDefaultUnit(kony.flex.DP);
            var flxTitle = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "centerY": "50%",
                "clipBounds": true,
                "height": "100%",
                "id": "flxTitle",
                "isVisible": true,
                "layoutType": kony.flex.FLOW_HORIZONTAL,
                "left": "10px",
                "isModalContainer": false,
                "onClick": controller.AS_FlexContainer_i87bd852f9f74801ae5ceff0e9ee81d1,
                "skin": "slFbox",
                "top": 0,
                "width": "20%",
                "zIndex": 1
            }, {}, {});
            flxTitle.setDefaultUnit(kony.flex.DP);
            var lblTitle = new kony.ui.Label({
                "centerY": "50%",
                "id": "lblTitle",
                "isVisible": true,
                "left": "0px",
                "skin": "sknlblLato696c7312px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.TermsAndConditions.Title\")",
                "top": 0,
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {
                "hoverSkin": "sknlblLatoBold00000012px"
            });
            var lblFontTitle = new kony.ui.Label({
                "centerY": "50%",
                "id": "lblFontTitle",
                "isVisible": true,
                "left": "10dp",
                "skin": "sknIcon15px",
                "text": "",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {
                "hoverSkin": "sknlblCursorFont"
            });
            flxTitle.add(lblTitle, lblFontTitle);
            var flxCode = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "centerY": "50%",
                "clipBounds": true,
                "height": "100%",
                "id": "flxCode",
                "isVisible": true,
                "layoutType": kony.flex.FLOW_HORIZONTAL,
                "left": "34%",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0dp",
                "width": "25%",
                "zIndex": 1
            }, {}, {});
            flxCode.setDefaultUnit(kony.flex.DP);
            var lblCode = new kony.ui.Label({
                "centerY": "50%",
                "id": "lblCode",
                "isVisible": true,
                "left": "0dp",
                "skin": "sknlblLato696c7312px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.Group.CODE\")",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {
                "hoverSkin": "sknlblLatoBold00000012px"
            });
            var lblFontCode = new kony.ui.Label({
                "centerY": "50%",
                "id": "lblFontCode",
                "isVisible": true,
                "left": "10dp",
                "skin": "sknIcon15px",
                "text": "",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {
                "hoverSkin": "sknlblCursorFont"
            });
            flxCode.add(lblCode, lblFontCode);
            var flxApplicableApps = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "centerY": "50%",
                "clipBounds": true,
                "height": "100%",
                "id": "flxApplicableApps",
                "isVisible": true,
                "layoutType": kony.flex.FLOW_HORIZONTAL,
                "left": "65%",
                "isModalContainer": false,
                "onClick": controller.AS_FlexContainer_i88e2961592c4b338de61134a3117f51,
                "skin": "slFbox",
                "top": "0dp",
                "width": "20%",
                "zIndex": 1
            }, {}, {});
            flxApplicableApps.setDefaultUnit(kony.flex.DP);
            var lblApplicableApps = new kony.ui.Label({
                "centerY": "50%",
                "id": "lblApplicableApps",
                "isVisible": true,
                "left": "0px",
                "skin": "sknlblLato696c7312px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.TermsAndConditions.ApplicableApps\")",
                "top": 0,
                "width": "120px",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {
                "hoverSkin": "sknlblLatoBold00000012px"
            });
            var flxFilterStatus = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "centerY": "50%",
                "clipBounds": true,
                "height": "20px",
                "id": "flxFilterStatus",
                "isVisible": true,
                "layoutType": kony.flex.FLOW_HORIZONTAL,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0dp",
                "width": "20px"
            }, {}, {});
            flxFilterStatus.setDefaultUnit(kony.flex.DP);
            var fontIconFilterStatus = new kony.ui.Label({
                "centerX": "50%",
                "centerY": "50%",
                "id": "fontIconFilterStatus",
                "isVisible": true,
                "skin": "sknIcon15px",
                "text": "",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {
                "hoverSkin": "sknlblCursorFont"
            });
            flxFilterStatus.add(fontIconFilterStatus);
            flxApplicableApps.add(lblApplicableApps, flxFilterStatus);
            var lblHeaderSeperator = new kony.ui.Label({
                "bottom": "1px",
                "centerX": "50%",
                "height": "1px",
                "id": "lblHeaderSeperator",
                "isVisible": true,
                "left": "0dp",
                "right": "0px",
                "skin": "sknLblTableHeaderLine",
                "text": "-",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxTandCHeader.add(flxTitle, flxCode, flxApplicableApps, lblHeaderSeperator);
            var segTandC = new kony.ui.SegmentedUI2({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "bottom": "0dp",
                "data": [{
                    "fonticonActive": "",
                    "lblApplicableApps": "",
                    "lblCode": "",
                    "lblSeparator1": "",
                    "lblTitle": ""
                }],
                "groupCells": false,
                "id": "segTandC",
                "isVisible": true,
                "left": "0dp",
                "needPageIndicator": true,
                "pageOffDotImage": "pageoffdot.png",
                "pageOnDotImage": "pageondot.png",
                "retainSelection": false,
                "rowFocusSkin": "seg2Focus",
                "rowSkin": "seg2Normal",
                "rowTemplate": "flxTermsAndConditions",
                "sectionHeaderSkin": "sliPhoneSegmentHeader",
                "selectionBehavior": constants.SEGUI_DEFAULT_BEHAVIOR,
                "separatorRequired": false,
                "showScrollbars": false,
                "top": "54dp",
                "viewType": constants.SEGUI_VIEW_TYPE_TABLEVIEW,
                "widgetDataMap": {
                    "flxTermsAndConditions": "flxTermsAndConditions",
                    "flxTermsAndConditionsWrapper": "flxTermsAndConditionsWrapper",
                    "fonticonActive": "fonticonActive",
                    "lblApplicableApps": "lblApplicableApps",
                    "lblCode": "lblCode",
                    "lblSeparator1": "lblSeparator1",
                    "lblTitle": "lblTitle"
                },
                "width": "100%",
                "zIndex": 11
            }, {
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var flxPagination = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                "bottom": "5px",
                "clipBounds": false,
                "id": "flxPagination",
                "isVisible": false,
                "layoutType": kony.flex.FLOW_HORIZONTAL,
                "isModalContainer": false,
                "right": "35dp",
                "skin": "slFbox0d9c3974835234d",
                "top": 0,
                "width": "222px",
                "zIndex": 1
            }, {}, {});
            flxPagination.setDefaultUnit(kony.flex.DP);
            var flxPaginationWrapper = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "bottom": 20,
                "clipBounds": false,
                "height": "30px",
                "id": "flxPaginationWrapper",
                "isVisible": true,
                "layoutType": kony.flex.FLOW_HORIZONTAL,
                "isModalContainer": false,
                "right": "35dp",
                "skin": "slFbox0d9c3974835234d",
                "top": 20,
                "width": "222px",
                "zIndex": 1
            }, {}, {});
            flxPaginationWrapper.setDefaultUnit(kony.flex.DP);
            var lbxPagination = new kony.ui.ListBox({
                "height": "30dp",
                "id": "lbxPagination",
                "isVisible": true,
                "left": "0dp",
                "masterData": [
                    ["lb1", "Page 1 of 20"],
                    ["lb2", "Page 2 of 20"],
                    ["lb3", "Page 3 of 20"],
                    ["lb4", "Page 4 of 20"]
                ],
                "selectedKey": "lb1",
                "skin": "sknlbxNobgNoBorderPagination",
                "top": "0dp",
                "width": "120dp",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {
                "hoverSkin": "lstboxcursor",
                "multiSelect": false
            });
            var flxPaginationSeperator = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "100%",
                "id": "flxPaginationSeperator",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "15dp",
                "isModalContainer": false,
                "skin": "sknflxd6dbe7",
                "top": "2dp",
                "width": "1px",
                "zIndex": 1
            }, {}, {});
            flxPaginationSeperator.setDefaultUnit(kony.flex.DP);
            flxPaginationSeperator.add();
            var flxPrevious = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "32dp",
                "id": "flxPrevious",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "15dp",
                "isModalContainer": false,
                "right": "3dp",
                "skin": "slFbox",
                "top": "0dp",
                "width": "32px",
                "zIndex": 1
            }, {}, {});
            flxPrevious.setDefaultUnit(kony.flex.DP);
            var lblIconPrevious = new kony.ui.Label({
                "centerX": "50%",
                "centerY": "50%",
                "id": "lblIconPrevious",
                "isVisible": true,
                "skin": "sknFontIconPrevNextDisable",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.userwidgetmodel.fontIconPrevious\")",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxPrevious.add(lblIconPrevious);
            var flxNext = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "32dp",
                "id": "flxNext",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0dp",
                "width": "32px",
                "zIndex": 1
            }, {}, {});
            flxNext.setDefaultUnit(kony.flex.DP);
            var lblIconNext = new kony.ui.Label({
                "centerX": "50%",
                "centerY": "50%",
                "id": "lblIconNext",
                "isVisible": true,
                "skin": "sknFontIconPrevNextDisable",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.userwidgetmodel.fontIconNext\")",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxNext.add(lblIconNext);
            flxPaginationWrapper.add(lbxPagination, flxPaginationSeperator, flxPrevious, flxNext);
            flxPagination.add(flxPaginationWrapper);
            var flxApplicableAppsFilter = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                "clipBounds": true,
                "id": "flxApplicableAppsFilter",
                "isVisible": false,
                "layoutType": kony.flex.FREE_FORM,
                "isModalContainer": false,
                "right": "100dp",
                "skin": "slFbox",
                "top": "40dp",
                "width": "200dp",
                "zIndex": 11
            }, {}, {});
            flxApplicableAppsFilter.setDefaultUnit(kony.flex.DP);
            var applicableAppsFilterMenu = new com.adminConsole.Products.productTypeFilterMenu({
                "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                "id": "applicableAppsFilterMenu",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "masterType": constants.MASTER_TYPE_DEFAULT,
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "5dp",
                "width": "100%",
                "zIndex": 5,
                "overrides": {
                    "flxCheckboxInner": {
                        "height": "120px"
                    },
                    "flxFilterSort": {
                        "isVisible": false
                    },
                    "flxSearchContainer": {
                        "isVisible": false
                    },
                    "imgUpArrow": {
                        "src": "uparrow_2x.png"
                    },
                    "lblSeperator1": {
                        "isVisible": false
                    },
                    "segStatusFilterDropdown": {
                        "data": [{
                            "imgCheckBox": "checkbox.png",
                            "lblDescription": "Retail Banking"
                        }, {
                            "imgCheckBox": "checkbox.png",
                            "lblDescription": "Business Banking"
                        }, {
                            "imgCheckBox": "checkbox.png",
                            "lblDescription": "Onboarding"
                        }]
                    }
                }
            }, {
                "overrides": {}
            }, {
                "overrides": {}
            });
            flxApplicableAppsFilter.add(applicableAppsFilterMenu);
            var flxNoRecordsFound = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "350dp",
                "id": "flxNoRecordsFound",
                "isVisible": false,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "sknflxConfigurationBundlesNoBorder",
                "top": "65dp",
                "width": "100%",
                "zIndex": 1
            }, {}, {});
            flxNoRecordsFound.setDefaultUnit(kony.flex.DP);
            var rtxNoRecords = new kony.ui.RichText({
                "centerX": "50%",
                "centerY": "50%",
                "id": "rtxNoRecords",
                "isVisible": true,
                "skin": "sknRtxLato84939e12Px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.userwidgetmodel.rtxNoRecords\")",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxNoRecordsFound.add(rtxNoRecords);
            flxTandCList.add(flxTandCHeader, segTandC, flxPagination, flxApplicableAppsFilter, flxNoRecordsFound);
            flxTandCTable.add(flxTandCList);
            flxTermsAndConditionsListWrapper.add(flxTandCTable);
            flxTermsAndConditionsList.add(flxTermsAndConditionsListWrapper);
            flxScrollMainContent.add(flxNoTermsAndConditions, flxDetailTermsAndConditions, flxTermsAndConditionsList);
            flxRightPanel.add(flxMainHeader, flxMainSubHeader, flxHeaderDropdown, flxScrollMainContent);
            var flxToastMessage = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "bottom": "0%",
                "clipBounds": true,
                "height": "8%",
                "id": "flxToastMessage",
                "isVisible": false,
                "layoutType": kony.flex.FREE_FORM,
                "left": "305px",
                "isModalContainer": false,
                "skin": "slFbox",
                "width": "78%",
                "zIndex": 1
            }, {}, {});
            flxToastMessage.setDefaultUnit(kony.flex.DP);
            var toastMessage = new com.adminConsole.common.toastMessage({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "id": "toastMessage",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "masterType": constants.MASTER_TYPE_DEFAULT,
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0dp",
                "overrides": {}
            }, {
                "overrides": {}
            }, {
                "overrides": {}
            });
            flxToastMessage.add(toastMessage);
            var flxLoading = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "100%",
                "id": "flxLoading",
                "isVisible": false,
                "layoutType": kony.flex.FREE_FORM,
                "left": "305px",
                "isModalContainer": false,
                "right": "0px",
                "skin": "sknLoadingBlur",
                "top": "0dp",
                "zIndex": 15
            }, {}, {});
            flxLoading.setDefaultUnit(kony.flex.DP);
            var flxImageContainer = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "centerX": "50%",
                "centerY": "50%",
                "clipBounds": true,
                "height": "75px",
                "id": "flxImageContainer",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "isModalContainer": false,
                "skin": "slFbox",
                "width": "75px",
                "zIndex": 1
            }, {}, {});
            flxImageContainer.setDefaultUnit(kony.flex.DP);
            var imgLoading = new kony.ui.Image2({
                "centerX": "50%",
                "centerY": "50%",
                "height": "50px",
                "id": "imgLoading",
                "isVisible": true,
                "skin": "slImage",
                "src": "loadingscreenimage.gif",
                "width": "50px",
                "zIndex": 1
            }, {
                "imageScaleMode": constants.IMAGE_SCALE_MODE_MAINTAIN_ASPECT_RATIO,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxImageContainer.add(imgLoading);
            flxLoading.add(flxImageContainer);
            flxMain.add(flxLeftPannel, flxRightPanel, flxToastMessage, flxLoading);
            var flxTandCPopUp = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "100%",
                "id": "flxTandCPopUp",
                "isVisible": false,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "masterType": constants.MASTER_TYPE_USERWIDGET,
                "isModalContainer": false,
                "top": "0dp",
                "width": "100%",
                "zIndex": 10
            }, {}, {});
            flxTandCPopUp.setDefaultUnit(kony.flex.DP);
            var tandCPopUp = new com.adminConsole.common.popUp1({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "height": "100%",
                "id": "tandCPopUp",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "masterType": constants.MASTER_TYPE_DEFAULT,
                "isModalContainer": false,
                "skin": "sknflxBg000000Op50",
                "top": "0dp",
                "width": "100%",
                "zIndex": 1,
                "overrides": {
                    "btnPopUpCancel": {
                        "i18n_text": "kony.i18n.getLocalizedString(\"i18n.PopUp.NoLeaveAsIS\")",
                        "minWidth": "viz.val_cleared",
                        "right": "20px",
                        "width": kony.flex.USE_PREFFERED_SIZE
                    },
                    "btnPopUpDelete": {
                        "i18n_text": "kony.i18n.getLocalizedString(\"i18n.PopUp.YesProceed\")",
                        "minWidth": "83px"
                    },
                    "lblPopUpMainMessage": {
                        "text": "Unsaved Changes"
                    },
                    "popUp1": {
                        "bottom": "viz.val_cleared",
                        "centerX": "viz.val_cleared",
                        "centerY": "viz.val_cleared",
                        "height": "100%",
                        "left": "0dp",
                        "maxHeight": "viz.val_cleared",
                        "maxWidth": "viz.val_cleared",
                        "minHeight": "viz.val_cleared",
                        "minWidth": "viz.val_cleared",
                        "right": "viz.val_cleared",
                        "top": "0dp",
                        "width": "100%"
                    },
                    "rtxPopUpDisclaimer": {
                        "right": "viz.val_cleared",
                        "text": "Are you sure you want to navigate away from the current page?<br><br>\n\n\n\n\n\nThere are unsaved changes in the current page. By clicking on \"Yes, Proceed\", all the unsaved <br>\nchanges would be lost.",
                        "width": "93%"
                    }
                }
            }, {
                "overrides": {}
            }, {
                "overrides": {}
            });
            flxTandCPopUp.add(tandCPopUp);
            var flxTextToUrlPopup = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "100%",
                "id": "flxTextToUrlPopup",
                "isVisible": false,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0",
                "width": "100%"
            }, {}, {});
            flxTextToUrlPopup.setDefaultUnit(kony.flex.DP);
            var textToUrlPopup = new com.adminConsole.common.popUp1({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "height": "100%",
                "id": "textToUrlPopup",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "masterType": constants.MASTER_TYPE_DEFAULT,
                "isModalContainer": false,
                "skin": "sknflxBg000000Op50",
                "top": "0dp",
                "width": "100%",
                "zIndex": 1,
                "overrides": {
                    "btnPopUpCancel": {
                        "i18n_text": "kony.i18n.getLocalizedString(\"i18n.PopUp.NoLeaveAsIS\")",
                        "minWidth": "viz.val_cleared",
                        "right": "20px",
                        "width": kony.flex.USE_PREFFERED_SIZE
                    },
                    "btnPopUpDelete": {
                        "i18n_text": "kony.i18n.getLocalizedString(\"i18n.PopUp.YesProceed\")",
                        "minWidth": "83px"
                    },
                    "flxPopupHeader": {
                        "width": "100%"
                    },
                    "lblPopUpMainMessage": {
                        "i18n_text": "kony.i18n.getLocalizedString(\"i18n.TermsAndConditions.UnsavedChanges\")"
                    },
                    "popUp1": {
                        "bottom": "viz.val_cleared",
                        "centerX": "viz.val_cleared",
                        "centerY": "viz.val_cleared",
                        "height": "100%",
                        "left": "0dp",
                        "maxHeight": "viz.val_cleared",
                        "maxWidth": "viz.val_cleared",
                        "minHeight": "viz.val_cleared",
                        "minWidth": "viz.val_cleared",
                        "right": "viz.val_cleared",
                        "top": "0dp",
                        "width": "100%"
                    },
                    "rtxPopUpDisclaimer": {
                        "i18n_text": "kony.i18n.getLocalizedString(\"i18n.TermsAndConditions.TextToUrlMessage\")",
                        "left": "20px",
                        "right": "20dp",
                        "width": "92%"
                    }
                }
            }, {
                "overrides": {}
            }, {
                "overrides": {}
            });
            flxTextToUrlPopup.add(textToUrlPopup);
            var flxEditTandCPopup = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "100%",
                "id": "flxEditTandCPopup",
                "isVisible": false,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "sknFocus50",
                "top": "0dp",
                "width": "100%",
                "zIndex": 5
            }, {}, {});
            flxEditTandCPopup.setDefaultUnit(kony.flex.DP);
            var flxEditTandC = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                "centerX": "50%",
                "centerY": "50%",
                "clipBounds": true,
                "id": "flxEditTandC",
                "isVisible": true,
                "layoutType": kony.flex.FLOW_VERTICAL,
                "isModalContainer": false,
                "skin": "slFbox0j9f841cc563e4e",
                "width": "650px",
                "zIndex": 1
            }, {}, {});
            flxEditTandC.setDefaultUnit(kony.flex.DP);
            var flxTopColor = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "10px",
                "id": "flxTopColor",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0px",
                "isModalContainer": false,
                "skin": "sknFlxInfoToastBg357C9ENoRadius",
                "top": "0dp",
                "width": "100%",
                "zIndex": 1
            }, {}, {});
            flxTopColor.setDefaultUnit(kony.flex.DP);
            flxTopColor.add();
            var flxPopupHeader = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                "clipBounds": true,
                "id": "flxPopupHeader",
                "isVisible": true,
                "layoutType": kony.flex.FLOW_VERTICAL,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0px",
                "width": "100%",
                "zIndex": 1
            }, {}, {});
            flxPopupHeader.setDefaultUnit(kony.flex.DP);
            var flxTAndCClose = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "20px",
                "id": "flxTAndCClose",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "isModalContainer": false,
                "right": "13dp",
                "skin": "sknCursor",
                "top": "10dp",
                "width": "25px",
                "zIndex": 20
            }, {}, {
                "hoverSkin": "sknCursor"
            });
            flxTAndCClose.setDefaultUnit(kony.flex.DP);
            var fontIconImgCLose = new kony.ui.Label({
                "centerX": "51%",
                "centerY": "50.00%",
                "height": "15dp",
                "id": "fontIconImgCLose",
                "isVisible": true,
                "left": "17dp",
                "skin": "sknFontIconSearchCross16px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmAlertsManagement.lblAlertPreviewClose\")",
                "width": "15dp",
                "zIndex": 2
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_CENTER,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxTAndCClose.add(fontIconImgCLose);
            var flxheader = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "20dp",
                "id": "flxheader",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "-5dp",
                "width": "100%",
                "zIndex": 20
            }, {}, {});
            flxheader.setDefaultUnit(kony.flex.DP);
            var lblEditTAndCHeader = new kony.ui.Label({
                "centerY": "50%",
                "id": "lblEditTAndCHeader",
                "isVisible": true,
                "left": "20dp",
                "skin": "sknlblLatoBold35475f23px",
                "text": "Edit Terms & Conditions",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_CENTER,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxheader.add(lblEditTAndCHeader);
            var flxTCDetails = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "165dp",
                "id": "flxTCDetails",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "slFbox0afd6fda7b2514b",
                "top": "15dp",
                "width": "100%",
                "zIndex": 1
            }, {}, {});
            flxTCDetails.setDefaultUnit(kony.flex.DP);
            var flxTCTitle = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "95dp",
                "id": "flxTCTitle",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "20dp",
                "isModalContainer": false,
                "right": "20dp",
                "skin": "slFbox",
                "top": "0dp",
                "zIndex": 1
            }, {}, {});
            flxTCTitle.setDefaultUnit(kony.flex.DP);
            var txtbxTCTitle = new kony.ui.TextBox2({
                "autoCapitalize": constants.TEXTBOX_AUTO_CAPITALIZE_NONE,
                "focusSkin": "skntxtbxDetailsFocus14px",
                "height": "40px",
                "id": "txtbxTCTitle",
                "isVisible": true,
                "keyBoardStyle": constants.TEXTBOX_KEY_BOARD_STYLE_DEFAULT,
                "left": "0dp",
                "maxTextLength": 50,
                "placeholder": "Title",
                "right": "0dp",
                "secureTextEntry": false,
                "skin": "skntbxLato35475f14px",
                "textInputMode": constants.TEXTBOX_INPUT_MODE_ANY,
                "top": "28dp",
                "zIndex": 1
            }, {
                "containerHeightMode": constants.TEXTBOX_FONT_METRICS_DRIVEN_HEIGHT,
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [1, 0, 0, 0],
                "paddingInPixel": false
            }, {
                "autoCorrect": false
            });
            var lblTCTitle = new kony.ui.Label({
                "id": "lblTCTitle",
                "isVisible": true,
                "left": "0dp",
                "skin": "sknlbl485c7514px",
                "text": "Title",
                "top": "0dp",
                "width": "150dp",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var lblTCTitleCount = new kony.ui.Label({
                "id": "lblTCTitleCount",
                "isVisible": false,
                "right": "0px",
                "skin": "sknlbl485c7514px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmGroups.lblGroupNameCount\")",
                "top": "0dp",
                "width": "40dp",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_CENTER,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var flxErrorTCTitle = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "15dp",
                "id": "flxErrorTCTitle",
                "isVisible": false,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "70dp",
                "width": "100%",
                "zIndex": 1
            }, {}, {});
            flxErrorTCTitle.setDefaultUnit(kony.flex.DP);
            var lblErrorTCTitleIcon = new kony.ui.Label({
                "height": "15dp",
                "id": "lblErrorTCTitleIcon",
                "isVisible": true,
                "left": "0dp",
                "skin": "sknErrorIcon",
                "text": "",
                "top": "0dp",
                "width": "15dp",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var lblErrorTCTitle = new kony.ui.Label({
                "height": "15dp",
                "id": "lblErrorTCTitle",
                "isVisible": true,
                "left": "15dp",
                "skin": "sknlblError",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmAlertsManagement.AlertNameCannotBeEmpty\")",
                "top": "0dp",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxErrorTCTitle.add(lblErrorTCTitleIcon, lblErrorTCTitle);
            flxTCTitle.add(txtbxTCTitle, lblTCTitle, lblTCTitleCount, flxErrorTCTitle);
            var flxTCCode = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "90px",
                "id": "flxTCCode",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "20dp",
                "isModalContainer": false,
                "skin": "slFbox0afd6fda7b2514b",
                "top": "95dp",
                "width": "320dp",
                "zIndex": 1
            }, {}, {});
            flxTCCode.setDefaultUnit(kony.flex.DP);
            var lblTCCode = new kony.ui.Label({
                "id": "lblTCCode",
                "isVisible": true,
                "left": "0dp",
                "skin": "sknlbl485c7514px",
                "text": "Code",
                "top": "0dp",
                "width": "100%",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var txtbxTCCode = new kony.ui.TextBox2({
                "autoCapitalize": constants.TEXTBOX_AUTO_CAPITALIZE_NONE,
                "focusSkin": "skntxtbxDetailsFocus14px",
                "height": "40px",
                "id": "txtbxTCCode",
                "isVisible": true,
                "keyBoardStyle": constants.TEXTBOX_KEY_BOARD_STYLE_DEFAULT,
                "left": "0dp",
                "maxTextLength": 35,
                "placeholder": "Code",
                "right": "0dp",
                "secureTextEntry": false,
                "skin": "skntxtbx484b5214px",
                "text": "RB_Footer_T&C",
                "textInputMode": constants.TEXTBOX_INPUT_MODE_ANY,
                "top": "25dp",
                "zIndex": 1
            }, {
                "containerHeightMode": constants.TEXTBOX_FONT_METRICS_DRIVEN_HEIGHT,
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [2, 0, 0, 0],
                "paddingInPixel": false
            }, {
                "autoCorrect": false
            });
            var flxDisableTbxCode = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "40dp",
                "id": "flxDisableTbxCode",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "isModalContainer": false,
                "right": "0dp",
                "skin": "sknTbxDisabledf3f3f3",
                "top": "25dp",
                "zIndex": 1
            }, {}, {
                "hoverSkin": "sknCursorDisabled"
            });
            flxDisableTbxCode.setDefaultUnit(kony.flex.DP);
            flxDisableTbxCode.add();
            flxTCCode.add(lblTCCode, txtbxTCCode, flxDisableTbxCode);
            var flxTCApplicableApps = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "85px",
                "id": "flxTCApplicableApps",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "365dp",
                "isModalContainer": false,
                "right": "20dp",
                "skin": "slFbox",
                "top": "95dp"
            }, {}, {});
            flxTCApplicableApps.setDefaultUnit(kony.flex.DP);
            var lblTCApplicableApps = new kony.ui.Label({
                "id": "lblTCApplicableApps",
                "isVisible": true,
                "left": "0dp",
                "skin": "sknlbl485c7514px",
                "text": "Applicable Apps",
                "top": "0dp",
                "width": "150dp",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var lblAppsValue = new kony.ui.Label({
                "id": "lblAppsValue",
                "isVisible": true,
                "left": "0dp",
                "right": "0dp",
                "skin": "sknlbl485c7514px",
                "text": "Retail Banking, Business Banking, Onboarding",
                "top": "25dp",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxTCApplicableApps.add(lblTCApplicableApps, lblAppsValue);
            flxTCDetails.add(flxTCTitle, flxTCCode, flxTCApplicableApps);
            var flxTextArea = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "bottom": "10dp",
                "clipBounds": true,
                "height": "160dp",
                "id": "flxTextArea",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "20dp",
                "isModalContainer": false,
                "right": "35dp",
                "skin": "slFbox",
                "top": "30dp",
                "width": "93%",
                "zIndex": 1
            }, {}, {});
            flxTextArea.setDefaultUnit(kony.flex.DP);
            var lblTCDescription = new kony.ui.Label({
                "id": "lblTCDescription",
                "isVisible": true,
                "left": "0px",
                "skin": "sknlbl485c7514px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmAlertsManagement.AlertDesc\")",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var lblTCDescriptionSize = new kony.ui.Label({
                "id": "lblTCDescriptionSize",
                "isVisible": false,
                "right": "1px",
                "skin": "sknlbl485c7514px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmGroups.lblGroupDescriptionCount\")",
                "top": "0px",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var txtDescription = new kony.ui.TextArea2({
                "autoCapitalize": constants.TEXTAREA_AUTO_CAPITALIZE_NONE,
                "focusSkin": "skntxtAreaLato9ca9ba12Px",
                "height": "130dp",
                "id": "txtDescription",
                "isVisible": true,
                "keyBoardStyle": constants.TEXTAREA_KEY_BOARD_STYLE_DEFAULT,
                "left": "0dp",
                "maxTextLength": 250,
                "numberOfVisibleLines": 3,
                "right": "0dp",
                "skin": "skntxtAreaLato35475f14Px",
                "textInputMode": constants.TEXTAREA_INPUT_MODE_ANY,
                "top": "30dp",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_TOP_LEFT,
                "padding": [2, 2, 2, 2],
                "paddingInPixel": false
            }, {
                "autoCorrect": false
            });
            var lblOptional = new kony.ui.Label({
                "id": "lblOptional",
                "isVisible": true,
                "left": "70px",
                "skin": "slLabel0d4f692dab05249",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmPermissions.lblOptional\")",
                "top": "0dp",
                "width": "60px",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxTextArea.add(lblTCDescription, lblTCDescriptionSize, txtDescription, lblOptional);
            var flxNoDescriptionError = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "bottom": "5px",
                "clipBounds": true,
                "height": "15px",
                "id": "flxNoDescriptionError",
                "isVisible": false,
                "layoutType": kony.flex.FREE_FORM,
                "left": "20px",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "5px",
                "width": "350px",
                "zIndex": 1
            }, {}, {});
            flxNoDescriptionError.setDefaultUnit(kony.flex.DP);
            var lblNoDescriptionErrorIcon = new kony.ui.Label({
                "height": "15px",
                "id": "lblNoDescriptionErrorIcon",
                "isVisible": true,
                "left": "0px",
                "skin": "sknErrorIcon",
                "text": "",
                "top": "0px",
                "width": "15px",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var lblNoDescriptionError = new kony.ui.Label({
                "centerY": "50%",
                "height": "15px",
                "id": "lblNoDescriptionError",
                "isVisible": true,
                "left": "15px",
                "skin": "sknlblError",
                "text": "Description cannot be empty",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxNoDescriptionError.add(lblNoDescriptionErrorIcon, lblNoDescriptionError);
            flxPopupHeader.add(flxTAndCClose, flxheader, flxTCDetails, flxTextArea, flxNoDescriptionError);
            var flxEditButtons = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "80px",
                "id": "flxEditButtons",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "isModalContainer": false,
                "skin": "sknFooter",
                "top": "0px",
                "width": "100%",
                "zIndex": 1
            }, {}, {});
            flxEditButtons.setDefaultUnit(kony.flex.DP);
            var btnCancel = new kony.ui.Button({
                "centerY": "50%",
                "height": "40px",
                "id": "btnCancel",
                "isVisible": true,
                "left": "20dp",
                "onClick": controller.AS_Button_fe48adad632c43c5b97a05155a450331,
                "skin": "sknbtnf7f7faLatoRegular4f555dBorder1px8b96a5",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmCustomers.CANCEL\")",
                "top": "0%",
                "width": "112dp",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_CENTER,
                "displayText": true,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {
                "hoverSkin": "sknBtnLatoRegular8b96a513pxKAhover"
            });
            var btnsave = new kony.ui.Button({
                "centerY": "50%",
                "focusSkin": "sknBtn003E75LatoRegular13pxFFFFFFRad20px",
                "height": "40dp",
                "id": "btnsave",
                "isVisible": true,
                "onClick": controller.AS_Button_c973a89c85624425bb033af39fa13bd2,
                "right": "20px",
                "skin": "sknBtn003E75LatoRegular13pxFFFFFFRad20px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmOutageMessageController.UPDATE\")",
                "top": "0%",
                "width": "112dp",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_CENTER,
                "displayText": true,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {
                "hoverSkin": "sknBtn005198LatoRegular13pxFFFFFFRad20px"
            });
            flxEditButtons.add(btnCancel, btnsave);
            flxEditTandC.add(flxTopColor, flxPopupHeader, flxEditButtons);
            flxEditTandCPopup.add(flxEditTandC);
            var flxContentPopUp = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "100%",
                "id": "flxContentPopUp",
                "isVisible": false,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "sknFocus50",
                "top": "0dp",
                "width": "100%",
                "zIndex": 5
            }, {}, {});
            flxContentPopUp.setDefaultUnit(kony.flex.DP);
            var flxAddEditContentPopUp = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                "centerX": "50%",
                "centerY": "50%",
                "clipBounds": true,
                "id": "flxAddEditContentPopUp",
                "isVisible": true,
                "layoutType": kony.flex.FLOW_VERTICAL,
                "isModalContainer": false,
                "skin": "slFbox0j9f841cc563e4e",
                "width": "850px",
                "zIndex": 1
            }, {}, {});
            flxAddEditContentPopUp.setDefaultUnit(kony.flex.DP);
            var flxBlueTopColor = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "10px",
                "id": "flxBlueTopColor",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0px",
                "isModalContainer": false,
                "skin": "sknFlxInfoToastBg357C9ENoRadius",
                "top": "0dp",
                "width": "100%",
                "zIndex": 1
            }, {}, {});
            flxBlueTopColor.setDefaultUnit(kony.flex.DP);
            flxBlueTopColor.add();
            var flxContentPopupHeader = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                "clipBounds": true,
                "id": "flxContentPopupHeader",
                "isVisible": true,
                "layoutType": kony.flex.FLOW_VERTICAL,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0px",
                "width": "100%",
                "zIndex": 1
            }, {}, {});
            flxContentPopupHeader.setDefaultUnit(kony.flex.DP);
            var flxContentClose = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "25px",
                "id": "flxContentClose",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "isModalContainer": false,
                "right": "13dp",
                "skin": "sknCursor",
                "top": "10dp",
                "width": "25px",
                "zIndex": 20
            }, {}, {
                "hoverSkin": "sknCursor"
            });
            flxContentClose.setDefaultUnit(kony.flex.DP);
            var fontIconCloseImg = new kony.ui.Label({
                "centerX": "51%",
                "centerY": "50.00%",
                "height": "15dp",
                "id": "fontIconCloseImg",
                "isVisible": true,
                "left": "17dp",
                "skin": "sknFontIconSearchCross16px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmAlertsManagement.lblAlertPreviewClose\")",
                "width": "15dp",
                "zIndex": 2
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxContentClose.add(fontIconCloseImg);
            var flxContentHeader = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "20dp",
                "id": "flxContentHeader",
                "isVisible": true,
                "layoutType": kony.flex.FLOW_HORIZONTAL,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "-5dp",
                "width": "100%",
                "zIndex": 20
            }, {}, {});
            flxContentHeader.setDefaultUnit(kony.flex.DP);
            var lblContentHeader1 = new kony.ui.Label({
                "centerY": "50%",
                "id": "lblContentHeader1",
                "isVisible": true,
                "left": "20dp",
                "skin": "sknlblLatoBold35475f23px",
                "text": "Create New Version -",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var lblContentHeader2 = new kony.ui.Label({
                "centerY": "50%",
                "id": "lblContentHeader2",
                "isVisible": true,
                "left": "5dp",
                "skin": "sknlblLatoBold35475f23px",
                "text": "DBX Footer Terms & Conditions -",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var lblContentHeader3 = new kony.ui.Label({
                "centerY": "50%",
                "id": "lblContentHeader3",
                "isVisible": true,
                "left": "5dp",
                "skin": "sknlblLatoBold35475f23px",
                "text": "English (UK)",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxContentHeader.add(lblContentHeader1, lblContentHeader2, lblContentHeader3);
            var flxAddEditContent = new kony.ui.FlexScrollContainer({
                "allowHorizontalBounce": false,
                "allowVerticalBounce": true,
                "bounces": true,
                "clipBounds": true,
                "enableScrolling": true,
                "height": "360dp",
                "horizontalScrollIndicator": true,
                "id": "flxAddEditContent",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "pagingEnabled": false,
                "scrollDirection": kony.flex.SCROLL_VERTICAL,
                "skin": "slFSbox",
                "top": "30dp",
                "verticalScrollIndicator": true,
                "width": kony.flex.USE_PREFFERED_SIZE
            }, {}, {});
            flxAddEditContent.setDefaultUnit(kony.flex.DP);
            var flxAddEditContentWrapper = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                "clipBounds": true,
                "id": "flxAddEditContentWrapper",
                "isVisible": true,
                "layoutType": kony.flex.FLOW_VERTICAL,
                "left": "0px",
                "isModalContainer": false,
                "right": "0px",
                "skin": "slFbox",
                "top": "0px",
                "zIndex": 1
            }, {}, {});
            flxAddEditContentWrapper.setDefaultUnit(kony.flex.DP);
            var flxContextDescription = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "180dp",
                "id": "flxContextDescription",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "20dp",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0dp",
                "width": "810dp",
                "zIndex": 1
            }, {}, {});
            flxContextDescription.setDefaultUnit(kony.flex.DP);
            var lblContentDescription = new kony.ui.Label({
                "id": "lblContentDescription",
                "isVisible": true,
                "left": "0px",
                "skin": "sknLbl78818A11px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmAlertsManagement.AlertDesc\")",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var lblContentDescriptionSize = new kony.ui.Label({
                "id": "lblContentDescriptionSize",
                "isVisible": false,
                "right": "1px",
                "skin": "sknlbl485c7514px",
                "text": "0/250",
                "top": "0px",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var txtContentDescription = new kony.ui.TextArea2({
                "autoCapitalize": constants.TEXTAREA_AUTO_CAPITALIZE_NONE,
                "focusSkin": "skntxtAreaLato9ca9ba12Px",
                "height": "130dp",
                "id": "txtContentDescription",
                "isVisible": true,
                "keyBoardStyle": constants.TEXTAREA_KEY_BOARD_STYLE_DEFAULT,
                "left": "0dp",
                "maxTextLength": 250,
                "numberOfVisibleLines": 3,
                "right": "0dp",
                "skin": "skntxtAreaLato35475f14Px",
                "textInputMode": constants.TEXTAREA_INPUT_MODE_ANY,
                "top": "25dp",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_TOP_LEFT,
                "padding": [2, 2, 2, 2],
                "paddingInPixel": false
            }, {
                "autoCorrect": false
            });
            var flxNoContentDescriptionError = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "bottom": "5px",
                "clipBounds": true,
                "height": "15px",
                "id": "flxNoContentDescriptionError",
                "isVisible": false,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0px",
                "isModalContainer": false,
                "skin": "slFbox",
                "width": "350px",
                "zIndex": 1
            }, {}, {});
            flxNoContentDescriptionError.setDefaultUnit(kony.flex.DP);
            var lblNoContentDescriptionErrorIcon = new kony.ui.Label({
                "height": "15px",
                "id": "lblNoContentDescriptionErrorIcon",
                "isVisible": true,
                "left": "0px",
                "skin": "sknErrorIcon",
                "text": "",
                "top": "0px",
                "width": "15px",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var lblNoContentDescriptionError = new kony.ui.Label({
                "centerY": "50%",
                "height": "15px",
                "id": "lblNoContentDescriptionError",
                "isVisible": true,
                "left": "15px",
                "skin": "sknlblError",
                "text": "Description cannot be empty",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxNoContentDescriptionError.add(lblNoContentDescriptionErrorIcon, lblNoContentDescriptionError);
            flxContextDescription.add(lblContentDescription, lblContentDescriptionSize, txtContentDescription, flxNoContentDescriptionError);
            var lblContentHeader = new kony.ui.Label({
                "id": "lblContentHeader",
                "isVisible": true,
                "left": "20px",
                "skin": "sknLbl78818A11px",
                "text": "Content",
                "top": "5px",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var flxContentTypes = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "20px",
                "id": "flxContentTypes",
                "isVisible": true,
                "layoutType": kony.flex.FLOW_HORIZONTAL,
                "left": "20px",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "13px",
                "width": "210px",
                "zIndex": 1
            }, {}, {});
            flxContentTypes.setDefaultUnit(kony.flex.DP);
            var flxRadioYes = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "centerY": "50%",
                "clipBounds": true,
                "height": "20dp",
                "id": "flxRadioYes",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "slFbox",
                "width": "20px",
                "zIndex": 1
            }, {}, {
                "hoverSkin": "sknCursor"
            });
            flxRadioYes.setDefaultUnit(kony.flex.DP);
            var imgRadioYes = new kony.ui.Image2({
                "centerX": "50%",
                "centerY": "50%",
                "height": "15dp",
                "id": "imgRadioYes",
                "isVisible": true,
                "skin": "slImage",
                "src": "radio_selected.png",
                "width": "15px",
                "zIndex": 1
            }, {
                "imageScaleMode": constants.IMAGE_SCALE_MODE_MAINTAIN_ASPECT_RATIO,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxRadioYes.add(imgRadioYes);
            var lblRadioText = new kony.ui.Label({
                "centerY": "50%",
                "id": "lblRadioText",
                "isVisible": true,
                "left": "5dp",
                "skin": "slLabel0d20174dce8ea42",
                "text": "Text",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var flxRadioNo = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "centerY": "50%",
                "clipBounds": true,
                "height": "20dp",
                "id": "flxRadioNo",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "20px",
                "isModalContainer": false,
                "skin": "slFbox",
                "width": "20px",
                "zIndex": 1
            }, {}, {
                "hoverSkin": "sknCursor"
            });
            flxRadioNo.setDefaultUnit(kony.flex.DP);
            var imgRadioNo = new kony.ui.Image2({
                "centerX": "50%",
                "centerY": "50%",
                "height": "15dp",
                "id": "imgRadioNo",
                "isVisible": true,
                "skin": "slImage",
                "src": "radio_notselected.png",
                "width": "15dp",
                "zIndex": 1
            }, {
                "imageScaleMode": constants.IMAGE_SCALE_MODE_MAINTAIN_ASPECT_RATIO,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxRadioNo.add(imgRadioNo);
            var lblRadioURL = new kony.ui.Label({
                "centerY": "50%",
                "id": "lblRadioURL",
                "isVisible": true,
                "left": "5dp",
                "skin": "slLabel0d20174dce8ea42",
                "text": "URL",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxContentTypes.add(flxRadioYes, lblRadioText, flxRadioNo, lblRadioURL);
            var flxURLTextBoxContainer = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "40dp",
                "id": "flxURLTextBoxContainer",
                "isVisible": false,
                "layoutType": kony.flex.FREE_FORM,
                "left": "20dp",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "10dp",
                "width": "810dp"
            }, {}, {});
            flxURLTextBoxContainer.setDefaultUnit(kony.flex.DP);
            var tbxURLTextbox = new kony.ui.TextBox2({
                "autoCapitalize": constants.TEXTBOX_AUTO_CAPITALIZE_NONE,
                "focusSkin": "defTextBoxFocus",
                "height": "40dp",
                "id": "tbxURLTextbox",
                "isVisible": true,
                "keyBoardStyle": constants.TEXTBOX_KEY_BOARD_STYLE_DEFAULT,
                "left": "0dp",
                "i18n_placeholder": "kony.i18n.getLocalizedString(\"i18n.decisionManagement.lblEnterHere\")",
                "right": "0dp",
                "secureTextEntry": false,
                "skin": "skntxtbx484b5214px",
                "textInputMode": constants.TEXTBOX_INPUT_MODE_ANY,
                "top": "0dp",
                "zIndex": 1
            }, {
                "containerHeightMode": constants.TEXTBOX_FONT_METRICS_DRIVEN_HEIGHT,
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [1, 0, 0, 0],
                "paddingInPixel": false
            }, {
                "autoCorrect": false,
                "placeholderSkin": "defTextBoxPlaceholder"
            });
            flxURLTextBoxContainer.add(tbxURLTextbox);
            var flxContentRichTxtContainer = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "bottom": "10dp",
                "clipBounds": true,
                "height": "260px",
                "id": "flxContentRichTxtContainer",
                "isVisible": false,
                "layoutType": kony.flex.FREE_FORM,
                "left": "20px",
                "isModalContainer": false,
                "skin": "sknflxffffffop100Bordercbcdd1Radius3px",
                "top": "10px",
                "width": "810dp",
                "zIndex": 1
            }, {}, {});
            flxContentRichTxtContainer.setDefaultUnit(kony.flex.DP);
            var rtxTnCAddEdit = new kony.ui.Browser({
                "detectTelNumber": true,
                "enableZoom": false,
                "height": "100%",
                "id": "rtxTnCAddEdit",
                "isVisible": true,
                "left": "0px",
                "setAsContent": false,
                "requestURLConfig": {
                    "URL": "richtext.html",
                    "requestMethod": constants.BROWSER_REQUEST_METHOD_GET
                },
                "right": "0dp",
                "top": "0px",
                "zIndex": 1
            }, {}, {});
            flxContentRichTxtContainer.add(rtxTnCAddEdit);
            var flxContentMissingError = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "15px",
                "id": "flxContentMissingError",
                "isVisible": false,
                "layoutType": kony.flex.FREE_FORM,
                "left": "20px",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "5px",
                "width": "300px",
                "zIndex": 1
            }, {}, {});
            flxContentMissingError.setDefaultUnit(kony.flex.DP);
            var lblContentErrorIcon = new kony.ui.Label({
                "height": "15px",
                "id": "lblContentErrorIcon",
                "isVisible": true,
                "left": "0px",
                "skin": "sknErrorIcon",
                "text": "",
                "top": "0px",
                "width": "15px",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var lblContentErrorMessage = new kony.ui.Label({
                "height": "15px",
                "id": "lblContentErrorMessage",
                "isVisible": true,
                "left": "15px",
                "skin": "sknlblError",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmTermsAndConditions.contentBlankMessage\")",
                "top": "0px",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxContentMissingError.add(lblContentErrorIcon, lblContentErrorMessage);
            flxAddEditContentWrapper.add(flxContextDescription, lblContentHeader, flxContentTypes, flxURLTextBoxContainer, flxContentRichTxtContainer, flxContentMissingError);
            var flxModifyType = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                "clipBounds": true,
                "id": "flxModifyType",
                "isVisible": false,
                "layoutType": kony.flex.FLOW_VERTICAL,
                "left": "15dp",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "227dp",
                "width": "300dp"
            }, {}, {});
            flxModifyType.setDefaultUnit(kony.flex.DP);
            var flxTopArrowImage = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "12dp",
                "id": "flxTopArrowImage",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0dp",
                "width": "100%",
                "zIndex": 101
            }, {}, {});
            flxTopArrowImage.setDefaultUnit(kony.flex.DP);
            var imgUpArrow = new kony.ui.Image2({
                "height": "12dp",
                "id": "imgUpArrow",
                "isVisible": true,
                "left": "76dp",
                "skin": "slImage",
                "src": "uparrow_2x.png",
                "top": "0dp",
                "width": "15dp",
                "zIndex": 1
            }, {
                "imageScaleMode": constants.IMAGE_SCALE_MODE_FIT_TO_DIMENSIONS,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxTopArrowImage.add(imgUpArrow);
            var flxTypeChange = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                "clipBounds": false,
                "id": "flxTypeChange",
                "isVisible": true,
                "layoutType": kony.flex.FLOW_VERTICAL,
                "left": "0px",
                "isModalContainer": false,
                "skin": "sknFlxffffffCustomRadius10px",
                "top": "-1px",
                "width": "300px",
                "zIndex": 100
            }, {}, {});
            flxTypeChange.setDefaultUnit(kony.flex.DP);
            var flxAlert = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                "clipBounds": true,
                "id": "flxAlert",
                "isVisible": true,
                "layoutType": kony.flex.FLOW_HORIZONTAL,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0dp",
                "width": "100%"
            }, {}, {});
            flxAlert.setDefaultUnit(kony.flex.DP);
            var lblContentWarning = new kony.ui.Label({
                "height": "18px",
                "id": "lblContentWarning",
                "isVisible": true,
                "left": "15px",
                "skin": "sknLatowarningYellow18px",
                "text": "",
                "top": "13px",
                "width": "18px",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_CENTER,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var lblAlert = new kony.ui.Label({
                "id": "lblAlert",
                "isVisible": true,
                "left": "5px",
                "skin": "sknlblBlack0a6fa48e6253944",
                "text": "ALERT",
                "top": "15dp",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxAlert.add(lblContentWarning, lblAlert);
            var flxSeperator = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "centerX": "50%",
                "clipBounds": true,
                "height": "1px",
                "id": "flxSeperator",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "isModalContainer": false,
                "skin": "sknflxd6dbe7",
                "top": "15px",
                "width": "90%",
                "zIndex": 1
            }, {}, {});
            flxSeperator.setDefaultUnit(kony.flex.DP);
            flxSeperator.add();
            var rtxPopUpDisclaimer = new kony.ui.RichText({
                "bottom": "10px",
                "id": "rtxPopUpDisclaimer",
                "isVisible": true,
                "left": "15px",
                "skin": "sknlRichTextLatoRegular",
                "text": "Are you sure you want to navigate from Text to URL ?<br><br>By clicking on \"Yes\" all the changes in Text will be lost.",
                "top": "10px",
                "width": "90%",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var flxYesNo = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                "clipBounds": true,
                "id": "flxYesNo",
                "isVisible": true,
                "layoutType": kony.flex.FLOW_HORIZONTAL,
                "reverseLayoutDirection": true,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0dp",
                "width": "100%"
            }, {}, {});
            flxYesNo.setDefaultUnit(kony.flex.DP);
            var btnAssignYes = new kony.ui.Button({
                "bottom": "20px",
                "focusSkin": "sknbtnf7f7faLatoRegular12Px485c75Border1px485C75Radius20px",
                "height": "22px",
                "id": "btnAssignYes",
                "isVisible": true,
                "right": "15px",
                "skin": "sknbtnffffffLatoRegular12Px485c75Border1px485C75Radius20px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmTrackApplication.Yes\")",
                "top": "15px",
                "width": "50px",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_CENTER,
                "displayText": true,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {
                "hoverSkin": "sknbtnf7f7faLatoRegular12Px485c75Border1px485C75Radius20px"
            });
            var btnAssignNo = new kony.ui.Button({
                "bottom": "20px",
                "focusSkin": "sknbtnf7f7faLatoRegular12Px485c75Border1px485C75Radius20px",
                "height": "22px",
                "id": "btnAssignNo",
                "isVisible": true,
                "right": "15px",
                "skin": "sknbtnffffffLatoRegular12Px485c75Border1px485C75Radius20px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmTrackApplication.No\")",
                "top": "15px",
                "width": "50px",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_CENTER,
                "displayText": true,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {
                "hoverSkin": "sknbtnf7f7faLatoRegular12Px485c75Border1px485C75Radius20px"
            });
            flxYesNo.add(btnAssignYes, btnAssignNo);
            flxTypeChange.add(flxAlert, flxSeperator, rtxPopUpDisclaimer, flxYesNo);
            flxModifyType.add(flxTopArrowImage, flxTypeChange);
            flxAddEditContent.add(flxAddEditContentWrapper, flxModifyType);
            flxContentPopupHeader.add(flxContentClose, flxContentHeader, flxAddEditContent);
            var flxButtons = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "80px",
                "id": "flxButtons",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "isModalContainer": false,
                "skin": "sknFooter",
                "top": "5px",
                "width": "100%",
                "zIndex": 1
            }, {}, {});
            flxButtons.setDefaultUnit(kony.flex.DP);
            var btnContentCancel = new kony.ui.Button({
                "centerY": "50%",
                "focusSkin": "sknBtnLatoRegular8b96a513pxKAhover",
                "height": "40px",
                "id": "btnContentCancel",
                "isVisible": true,
                "left": "20dp",
                "onClick": controller.AS_Button_fe48adad632c43c5b97a05155a450331,
                "skin": "sknbtnf7f7faLatoRegular4f555dBorder1px8b96a5",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmCustomers.CANCEL\")",
                "top": "0%",
                "width": "114dp",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_CENTER,
                "displayText": true,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {
                "hoverSkin": "sknBtnLatoRegular8b96a513pxKAhover"
            });
            var btnContentSavePublish = new kony.ui.Button({
                "centerY": "50%",
                "focusSkin": "sknBtn003E75LatoRegular13pxFFFFFFRad20px",
                "height": "40dp",
                "id": "btnContentSavePublish",
                "isVisible": true,
                "onClick": controller.AS_Button_c973a89c85624425bb033af39fa13bd2,
                "right": "20px",
                "skin": "sknBtn003E75LatoRegular13pxFFFFFFRad20px",
                "text": "SAVE & PUBLISH",
                "top": "0%",
                "width": "164dp",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_CENTER,
                "displayText": true,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {
                "hoverSkin": "sknBtn003E75LatoRegular13pxFFFFFFRad20px"
            });
            var btnContentSave = new kony.ui.Button({
                "centerY": "50%",
                "focusSkin": "sknBtn003E75LatoRegular13pxFFFFFFRad20px",
                "height": "40dp",
                "id": "btnContentSave",
                "isVisible": true,
                "onClick": controller.AS_Button_c973a89c85624425bb033af39fa13bd2,
                "right": "200px",
                "skin": "sknBtn003E75LatoRegular13pxFFFFFFRad20px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmAlertsManagement.SAVE\")",
                "top": "0%",
                "width": "96dp",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_CENTER,
                "displayText": true,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {
                "hoverSkin": "sknBtn005198LatoRegular13pxFFFFFFRad20px"
            });
            var flxDisableButtons = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "80px",
                "id": "flxDisableButtons",
                "isVisible": false,
                "layoutType": kony.flex.FREE_FORM,
                "isModalContainer": false,
                "skin": "sknTbxDisabledf3f3f3",
                "top": "0px",
                "width": "100%",
                "zIndex": 1
            }, {}, {});
            flxDisableButtons.setDefaultUnit(kony.flex.DP);
            flxDisableButtons.add();
            flxButtons.add(btnContentCancel, btnContentSavePublish, btnContentSave, flxDisableButtons);
            flxAddEditContentPopUp.add(flxBlueTopColor, flxContentPopupHeader, flxButtons);
            flxContentPopUp.add(flxAddEditContentPopUp);
            var flxViewContent = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "100%",
                "id": "flxViewContent",
                "isVisible": false,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "sknFocus50",
                "top": "0dp",
                "width": "100%",
                "zIndex": 5
            }, {}, {});
            flxViewContent.setDefaultUnit(kony.flex.DP);
            var flxViewContentPopUp = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                "centerX": "50%",
                "centerY": "50%",
                "clipBounds": true,
                "id": "flxViewContentPopUp",
                "isVisible": true,
                "layoutType": kony.flex.FLOW_VERTICAL,
                "left": "27%",
                "isModalContainer": false,
                "skin": "slFbox0j9f841cc563e4e",
                "width": "850px",
                "zIndex": 1
            }, {}, {});
            flxViewContentPopUp.setDefaultUnit(kony.flex.DP);
            var flxBlueTop = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "10px",
                "id": "flxBlueTop",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0px",
                "isModalContainer": false,
                "skin": "sknFlxInfoToastBg357C9ENoRadius",
                "top": "0dp",
                "width": "100%",
                "zIndex": 1
            }, {}, {});
            flxBlueTop.setDefaultUnit(kony.flex.DP);
            flxBlueTop.add();
            var flxViewContentPopupHeader = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                "clipBounds": true,
                "id": "flxViewContentPopupHeader",
                "isVisible": true,
                "layoutType": kony.flex.FLOW_VERTICAL,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0px",
                "width": "100%",
                "zIndex": 1
            }, {}, {});
            flxViewContentPopupHeader.setDefaultUnit(kony.flex.DP);
            var flxViewContentClose = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "25px",
                "id": "flxViewContentClose",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "isModalContainer": false,
                "right": "13dp",
                "skin": "sknCursor",
                "top": "10dp",
                "width": "25px",
                "zIndex": 20
            }, {}, {
                "hoverSkin": "sknCursor"
            });
            flxViewContentClose.setDefaultUnit(kony.flex.DP);
            var lblFontIconClose = new kony.ui.Label({
                "centerX": "51%",
                "centerY": "50.00%",
                "height": "15dp",
                "id": "lblFontIconClose",
                "isVisible": true,
                "left": "17dp",
                "skin": "sknFontIconSearchCross16px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmAlertsManagement.lblAlertPreviewClose\")",
                "width": "15dp",
                "zIndex": 2
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxViewContentClose.add(lblFontIconClose);
            var flxViewContentHeader = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "20dp",
                "id": "flxViewContentHeader",
                "isVisible": true,
                "layoutType": kony.flex.FLOW_HORIZONTAL,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "-5dp",
                "width": "100%",
                "zIndex": 20
            }, {}, {});
            flxViewContentHeader.setDefaultUnit(kony.flex.DP);
            var lblViewContentHeader1 = new kony.ui.Label({
                "centerY": "50%",
                "id": "lblViewContentHeader1",
                "isVisible": true,
                "left": "20dp",
                "skin": "sknlblLatoBold35475f23px",
                "text": "Create New Version -",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var lblViewContentHeader2 = new kony.ui.Label({
                "centerY": "50%",
                "id": "lblViewContentHeader2",
                "isVisible": true,
                "left": "5dp",
                "skin": "sknlblLatoBold35475f23px",
                "text": "DBX Footer Terms & Conditions -",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var lblViewContentHeader3 = new kony.ui.Label({
                "centerY": "50%",
                "id": "lblViewContentHeader3",
                "isVisible": true,
                "left": "5dp",
                "skin": "sknlblLatoBold35475f23px",
                "text": "English (UK)",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxViewContentHeader.add(lblViewContentHeader1, lblViewContentHeader2, lblViewContentHeader3);
            var flxViewContentContainer = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                "clipBounds": true,
                "id": "flxViewContentContainer",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "25dp",
                "width": kony.flex.USE_PREFFERED_SIZE
            }, {}, {});
            flxViewContentContainer.setDefaultUnit(kony.flex.DP);
            var flxViewContentWrapper = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                "bottom": "0dp",
                "clipBounds": true,
                "id": "flxViewContentWrapper",
                "isVisible": true,
                "layoutType": kony.flex.FLOW_VERTICAL,
                "left": "0px",
                "isModalContainer": false,
                "right": "0px",
                "skin": "slFbox",
                "top": "0px",
                "zIndex": 1
            }, {}, {});
            flxViewContentWrapper.setDefaultUnit(kony.flex.DP);
            var flxRow1 = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                "clipBounds": true,
                "id": "flxRow1",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0dp",
                "width": "100%"
            }, {}, {});
            flxRow1.setDefaultUnit(kony.flex.DP);
            var flxViewKeyValue1 = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                "clipBounds": true,
                "id": "flxViewKeyValue1",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "20dp",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0dp",
                "width": "31%",
                "zIndex": 1
            }, {}, {});
            flxViewKeyValue1.setDefaultUnit(kony.flex.DP);
            var fontIconActive = new kony.ui.Label({
                "id": "fontIconActive",
                "isVisible": true,
                "left": "0dp",
                "skin": "sknFontIconActivate",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmAlertsManagement.lblViewAlertStatusKey\")",
                "top": "25dp",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var lblViewKey1 = new kony.ui.Label({
                "id": "lblViewKey1",
                "isVisible": true,
                "left": "0dp",
                "skin": "sknlblLato5d6c7f12px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.permission.STATUS\")",
                "top": "2dp",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var lblViewValue1 = new kony.ui.Label({
                "id": "lblViewValue1",
                "isVisible": true,
                "left": "15px",
                "skin": "sknlblLato5bc06cBold14px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.secureimage.Active\")",
                "top": "23px",
                "width": "100%",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxViewKeyValue1.add(fontIconActive, lblViewKey1, lblViewValue1);
            var flxViewKeyValue2 = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                "clipBounds": true,
                "id": "flxViewKeyValue2",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "33%",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0dp",
                "width": "31%",
                "zIndex": 1
            }, {}, {});
            flxViewKeyValue2.setDefaultUnit(kony.flex.DP);
            var lblViewKey2 = new kony.ui.Label({
                "id": "lblViewKey2",
                "isVisible": true,
                "left": "0dp",
                "skin": "sknlblLato5d6c7f12px",
                "text": "PUBLISHED ON",
                "top": "2dp",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var lblViewValue2 = new kony.ui.Label({
                "id": "lblViewValue2",
                "isVisible": true,
                "left": "0px",
                "skin": "sknlblLatoBold35475f14px",
                "text": "01/08/2019 12:00 AM",
                "top": "23px",
                "width": "100%",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxViewKeyValue2.add(lblViewKey2, lblViewValue2);
            var flxViewKeyValue3 = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                "clipBounds": true,
                "id": "flxViewKeyValue3",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "64%",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0dp",
                "width": "31%",
                "zIndex": 1
            }, {}, {});
            flxViewKeyValue3.setDefaultUnit(kony.flex.DP);
            var lblViewKey3 = new kony.ui.Label({
                "id": "lblViewKey3",
                "isVisible": true,
                "left": "0dp",
                "skin": "sknlblLato5d6c7f12px",
                "text": "PUBLISHED BY",
                "top": "2dp",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var lblViewValue3 = new kony.ui.Label({
                "id": "lblViewValue3",
                "isVisible": true,
                "left": "0px",
                "skin": "sknlblLatoBold35475f14px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmCreateNewUser.lblUser\")",
                "top": "23px",
                "width": "100%",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxViewKeyValue3.add(lblViewKey3, lblViewValue3);
            flxRow1.add(flxViewKeyValue1, flxViewKeyValue2, flxViewKeyValue3);
            var flxRow2 = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                "clipBounds": true,
                "id": "flxRow2",
                "isVisible": true,
                "layoutType": kony.flex.FLOW_VERTICAL,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "30dp",
                "width": "100%"
            }, {}, {});
            flxRow2.setDefaultUnit(kony.flex.DP);
            var flxDescriptionViewContent = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                "clipBounds": true,
                "id": "flxDescriptionViewContent",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "isModalContainer": false,
                "right": "50dp",
                "skin": "slFbox",
                "top": "5dp",
                "width": kony.flex.USE_PREFFERED_SIZE
            }, {}, {});
            flxDescriptionViewContent.setDefaultUnit(kony.flex.DP);
            var lblViewContentDescriptionHeading = new kony.ui.Label({
                "id": "lblViewContentDescriptionHeading",
                "isVisible": true,
                "left": "20dp",
                "skin": "sknLbl78818A11px",
                "text": "DESCRIPTION",
                "top": "0dp",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var lblViewContentDescriptionValue = new kony.ui.Label({
                "id": "lblViewContentDescriptionValue",
                "isVisible": true,
                "left": "20dp",
                "right": "20dp",
                "skin": "sknLbl485C75LatoRegular13Px",
                "text": "This terms & conditions will be used in the footer link of retail banking",
                "top": "23dp",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxDescriptionViewContent.add(lblViewContentDescriptionHeading, lblViewContentDescriptionValue);
            var flxViewContentType = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                "clipBounds": true,
                "id": "flxViewContentType",
                "isVisible": true,
                "layoutType": kony.flex.FLOW_HORIZONTAL,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "20dp",
                "width": "100%"
            }, {}, {});
            flxViewContentType.setDefaultUnit(kony.flex.DP);
            var lblViewContentType = new kony.ui.Label({
                "id": "lblViewContentType",
                "isVisible": true,
                "left": "20dp",
                "skin": "sknlblLato5d6c7f12px",
                "text": "CONTENT :",
                "top": "10dp",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var lblViewContentTypeValue = new kony.ui.Label({
                "id": "lblViewContentTypeValue",
                "isVisible": true,
                "left": "5dp",
                "skin": "sknlblLato5d6c7f12px",
                "text": "TEXT",
                "top": "10dp",
                "width": "50dp",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxViewContentType.add(lblViewContentType, lblViewContentTypeValue);
            var rtxContentViewer = new kony.ui.Browser({
                "bottom": "10dp",
                "detectTelNumber": true,
                "enableNativeCommunication": false,
                "enableZoom": false,
                "height": "300dp",
                "id": "rtxContentViewer",
                "isVisible": true,
                "left": "13px",
                "setAsContent": false,
                "requestURLConfig": {
                    "URL": "richtextViewer.html",
                    "requestMethod": constants.BROWSER_REQUEST_METHOD_GET
                },
                "top": "10px",
                "width": "810dp",
                "zIndex": 1
            }, {}, {});
            var lblURLValue = new kony.ui.Label({
                "bottom": "20px",
                "id": "lblURLValue",
                "isVisible": true,
                "left": "20px",
                "skin": "sknlblLatoBold35475f14px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmCreateNewUser.lblUser\")",
                "top": "10px",
                "width": "810dp",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var richtextURLValue = new kony.ui.RichText({
                "bottom": "20dp",
                "id": "richtextURLValue",
                "isVisible": false,
                "left": "20dp",
                "linkSkin": "slRichText0f825e4c814004c",
                "skin": "slRichText0f825e4c814004c",
                "text": "https://",
                "top": "20dp",
                "width": "810dp",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxRow2.add(flxDescriptionViewContent, flxViewContentType, rtxContentViewer, lblURLValue, richtextURLValue);
            flxViewContentWrapper.add(flxRow1, flxRow2);
            flxViewContentContainer.add(flxViewContentWrapper);
            flxViewContentPopupHeader.add(flxViewContentClose, flxViewContentHeader, flxViewContentContainer);
            flxViewContentPopUp.add(flxBlueTop, flxViewContentPopupHeader);
            flxViewContent.add(flxViewContentPopUp);
            this.add(flxMain, flxTandCPopUp, flxTextToUrlPopup, flxEditTandCPopup, flxContentPopUp, flxViewContent);
        };
        return [{
            "addWidgets": addWidgetsfrmTermsAndConditions,
            "enabledForIdleTimeout": true,
            "id": "frmTermsAndConditions",
            "layoutType": kony.flex.FREE_FORM,
            "needAppMenu": false,
            "preShow": function(eventobject) {
                controller.AS_Form_i84b41a94cd94117916c0bb1a757bdca(eventobject);
            },
            "skin": "slForm"
        }, {
            "displayOrientation": constants.FORM_DISPLAY_ORIENTATION_PORTRAIT,
            "layoutType": kony.flex.FREE_FORM,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, {
            "onDeviceBack": controller.AS_Form_ga73a3b96d7646c9ad76a67a401444b0,
            "retainScrollPosition": false
        }]
    }
});