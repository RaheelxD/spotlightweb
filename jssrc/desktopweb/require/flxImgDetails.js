define("flxImgDetails", function() {
    return function(controller) {
        var flxImgDetails = new kony.ui.FlexContainer({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "clipBounds": true,
            "height": "63px",
            "id": "flxImgDetails",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "left": "0dp",
            "isModalContainer": false,
            "skin": "slFbox",
            "top": "0dp",
            "width": "100%"
        }, {}, {});
        flxImgDetails.setDefaultUnit(kony.flex.DP);
        var flxImgType = new kony.ui.FlexContainer({
            "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
            "clipBounds": true,
            "id": "flxImgType",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "left": "0px",
            "isModalContainer": false,
            "skin": "slFbox0afd6fda7b2514b",
            "top": "0px",
            "width": "305px",
            "zIndex": 1
        }, {}, {});
        flxImgType.setDefaultUnit(kony.flex.DP);
        var lstbxImgType = new kony.ui.ListBox({
            "focusSkin": "sknlbxBgffffffBorder11abebRadius3Px14px",
            "height": "40px",
            "id": "lstbxImgType",
            "isVisible": true,
            "left": "0px",
            "masterData": [
                ["lb1", "Select"]
            ],
            "right": "15px",
            "selectedKey": "lb1",
            "skin": "sknLbxborderd7d9e03pxradius",
            "top": "0px",
            "zIndex": 1
        }, {
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [2, 0, 0, 0],
            "paddingInPixel": false
        }, {
            "hoverSkin": "lstboxcursor",
            "multiSelect": false
        });
        var flxImgTypeError = new kony.ui.FlexContainer({
            "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
            "clipBounds": true,
            "id": "flxImgTypeError",
            "isVisible": false,
            "layoutType": kony.flex.FLOW_HORIZONTAL,
            "left": "0px",
            "isModalContainer": false,
            "skin": "slFbox",
            "top": "47px",
            "width": "100%"
        }, {}, {});
        flxImgTypeError.setDefaultUnit(kony.flex.DP);
        var lblImgTypeErrorIcon = new kony.ui.Label({
            "height": "15dp",
            "id": "lblImgTypeErrorIcon",
            "isVisible": true,
            "left": "0dp",
            "skin": "sknErrorIcon",
            "text": "",
            "top": "0dp",
            "width": "15dp",
            "zIndex": 1
        }, {
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, {});
        var lblImgTypeErrorText = new kony.ui.Label({
            "height": "15dp",
            "id": "lblImgTypeErrorText",
            "isVisible": true,
            "left": "0dp",
            "skin": "sknlblError",
            "text": "Error Message",
            "top": "0dp",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, {
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, {});
        flxImgTypeError.add(lblImgTypeErrorIcon, lblImgTypeErrorText);
        flxImgType.add(lstbxImgType, flxImgTypeError);
        var flxImgURL = new kony.ui.FlexContainer({
            "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
            "clipBounds": true,
            "id": "flxImgURL",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "left": "305px",
            "isModalContainer": false,
            "right": "33px",
            "skin": "slFbox",
            "top": "0px"
        }, {}, {});
        flxImgURL.setDefaultUnit(kony.flex.DP);
        var txtImgURL = new kony.ui.TextBox2({
            "autoCapitalize": constants.TEXTBOX_AUTO_CAPITALIZE_NONE,
            "focusSkin": "skntxtbxDetailsFocus14px",
            "height": "40dp",
            "id": "txtImgURL",
            "isVisible": true,
            "keyBoardStyle": constants.TEXTBOX_KEY_BOARD_STYLE_DEFAULT,
            "left": "0px",
            "placeholder": "Image URL",
            "right": "0px",
            "secureTextEntry": false,
            "skin": "txtD7d9e0",
            "textInputMode": constants.TEXTBOX_INPUT_MODE_ANY,
            "top": "0px",
            "zIndex": 1
        }, {
            "containerHeightMode": constants.TEXTBOX_FONT_METRICS_DRIVEN_HEIGHT,
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [2, 0, 0, 0],
            "paddingInPixel": false
        }, {
            "autoCorrect": false,
            "placeholderSkin": "sknTextGreyb2bdcd"
        });
        var flxImgURLError = new kony.ui.FlexContainer({
            "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
            "clipBounds": true,
            "id": "flxImgURLError",
            "isVisible": false,
            "layoutType": kony.flex.FLOW_HORIZONTAL,
            "left": "0px",
            "isModalContainer": false,
            "skin": "slFbox",
            "top": "47px",
            "width": "100%"
        }, {}, {});
        flxImgURLError.setDefaultUnit(kony.flex.DP);
        var lblImgURLErrorIcon = new kony.ui.Label({
            "height": "15dp",
            "id": "lblImgURLErrorIcon",
            "isVisible": true,
            "left": "0dp",
            "skin": "sknErrorIcon",
            "text": "",
            "top": "0dp",
            "width": "15dp",
            "zIndex": 1
        }, {
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, {});
        var lblImgURLErrorText = new kony.ui.Label({
            "height": "15dp",
            "id": "lblImgURLErrorText",
            "isVisible": true,
            "left": "0dp",
            "skin": "sknlblError",
            "text": "Error Message",
            "top": "0dp",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, {
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, {});
        flxImgURLError.add(lblImgURLErrorIcon, lblImgURLErrorText);
        flxImgURL.add(txtImgURL, flxImgURLError);
        var flxDelete = new kony.ui.FlexContainer({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "clipBounds": true,
            "height": "20dp",
            "id": "flxDelete",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "isModalContainer": false,
            "onClick": controller.AS_FlexContainer_h43e332b32624c87a4361f632e581db3,
            "right": 0,
            "skin": "hoverhandSkin",
            "top": "10dp",
            "width": "20dp"
        }, {}, {});
        flxDelete.setDefaultUnit(kony.flex.DP);
        var lblDelete = new kony.ui.Label({
            "centerX": "50%",
            "centerY": "50%",
            "height": "20dp",
            "id": "lblDelete",
            "isVisible": true,
            "skin": "sknIcon20px",
            "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmFAQ.fonticonDelete\")",
            "width": "20dp",
            "zIndex": 1
        }, {
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, {});
        flxDelete.add(lblDelete);
        flxImgDetails.add(flxImgType, flxImgURL, flxDelete);
        return flxImgDetails;
    }
})