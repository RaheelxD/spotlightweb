define("CompaniesModule/frmEnrollmentRequests", function() {
    return function(controller) {
        function addWidgetsfrmEnrollmentRequests() {
            this.setDefaultUnit(kony.flex.DP);
            var flxMain = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "100%",
                "id": "flxMain",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "sknflxf5f6f8Op100",
                "top": "0dp",
                "width": "100%",
                "zIndex": 1
            }, {}, {});
            flxMain.setDefaultUnit(kony.flex.DP);
            var flxLeftPanel = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "100%",
                "id": "flxLeftPanel",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0dp",
                "width": "305dp",
                "zIndex": 1
            }, {}, {});
            flxLeftPanel.setDefaultUnit(kony.flex.DP);
            var leftMenuNew = new com.adminConsole.navigation.leftMenuNew({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "height": "100%",
                "id": "leftMenuNew",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "masterType": constants.MASTER_TYPE_DEFAULT,
                "isModalContainer": false,
                "skin": "sknFlxBg003E75Op100",
                "top": "0dp",
                "width": "100%",
                "overrides": {
                    "brandLogo": {
                        "src": "infinity_dbx_c360_logo_white_2x.png"
                    },
                    "flxScrollMenu": {
                        "bottom": "40dp",
                        "left": "0dp",
                        "top": "85dp"
                    },
                    "imgBottomLogo": {
                        "src": "logo_white_2x.png"
                    },
                    "leftMenuNew": {
                        "bottom": "viz.val_cleared",
                        "centerX": "viz.val_cleared",
                        "centerY": "viz.val_cleared",
                        "height": "100%",
                        "left": "0dp",
                        "maxHeight": "viz.val_cleared",
                        "maxWidth": "viz.val_cleared",
                        "minHeight": "viz.val_cleared",
                        "minWidth": "viz.val_cleared",
                        "right": "viz.val_cleared",
                        "top": "0dp",
                        "width": "100%"
                    }
                }
            }, {
                "overrides": {}
            }, {
                "overrides": {}
            });
            flxLeftPanel.add(leftMenuNew);
            var flxRightPanel = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "100%",
                "id": "flxRightPanel",
                "isVisible": true,
                "layoutType": kony.flex.FLOW_VERTICAL,
                "left": "305dp",
                "isModalContainer": false,
                "right": "0dp",
                "skin": "slFbox",
                "top": "0dp",
                "zIndex": 1
            }, {}, {});
            flxRightPanel.setDefaultUnit(kony.flex.DP);
            var flxMainHeader = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                "clipBounds": true,
                "id": "flxMainHeader",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0dp",
                "width": "100%",
                "zIndex": 1
            }, {}, {});
            flxMainHeader.setDefaultUnit(kony.flex.DP);
            var mainHeader = new com.adminConsole.header.mainHeader({
                "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                "id": "mainHeader",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": 0,
                "masterType": constants.MASTER_TYPE_DEFAULT,
                "isModalContainer": false,
                "skin": "CopyslFbox1",
                "top": 0,
                "width": "100%",
                "overrides": {
                    "btnAddNewOption": {
                        "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmCompanies.CreateCompany\")",
                        "right": "0dp"
                    },
                    "btnDropdownList": {
                        "i18n_text": "kony.i18n.getLocalizedString(\"i18n.mainHeader.DOWNLOADLIST\")",
                        "isVisible": false
                    },
                    "flxButtons": {
                        "bottom": "viz.val_cleared",
                        "height": "30dp",
                        "isVisible": false,
                        "right": "100dp",
                        "width": "400dp"
                    },
                    "flxHeaderSeperator": {
                        "isVisible": false
                    },
                    "flxHeaderUserOptions": {
                        "isVisible": false
                    },
                    "flxMainHeader": {
                        "autogrowMode": kony.flex.AUTOGROW_HEIGHT
                    },
                    "imgLogout": {
                        "src": "img_logout.png"
                    },
                    "lblHeading": {
                        "centerY": "viz.val_cleared",
                        "height": "30px",
                        "i18n_text": "kony.i18n.getLocalizedString(\"i18n.leftmenu.companies\")",
                        "top": "60px"
                    },
                    "mainHeader": {
                        "autogrowMode": kony.flex.AUTOGROW_HEIGHT
                    }
                }
            }, {
                "overrides": {}
            }, {
                "overrides": {}
            });
            flxMainHeader.add(mainHeader);
            var flxBreadcrumb = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "20px",
                "id": "flxBreadcrumb",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "5px",
                "width": "100%",
                "zIndex": 10
            }, {}, {});
            flxBreadcrumb.setDefaultUnit(kony.flex.DP);
            var breadcrumbs = new com.adminConsole.common.breadcrumbs({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "height": "20px",
                "id": "breadcrumbs",
                "isVisible": true,
                "layoutType": kony.flex.FLOW_HORIZONTAL,
                "left": "0dp",
                "masterType": constants.MASTER_TYPE_DEFAULT,
                "isModalContainer": false,
                "skin": "sknflxf5f6f8Op100",
                "top": "0dp",
                "width": "100%",
                "overrides": {
                    "breadcrumbs": {
                        "height": "20px",
                        "isVisible": true
                    },
                    "btnBackToMain": {
                        "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmCompanies.COMPANIES\")",
                        "isVisible": true
                    },
                    "btnPreviousPage": {
                        "isVisible": false
                    },
                    "imgBreadcrumbsDown": {
                        "src": "img_down_arrow.png"
                    },
                    "imgBreadcrumbsRight": {
                        "src": "img_breadcrumb_arrow.png"
                    },
                    "imgBreadcrumbsRight2": {
                        "isVisible": false,
                        "src": "img_breadcrumb_arrow.png"
                    },
                    "imgBreadcrumbsRight3": {
                        "src": "img_breadcrumb_arrow.png"
                    },
                    "lblCurrentScreen": {
                        "i18n_text": "kony.i18n.getLocalizedString(\"i18n.Companies.Pending_Requests_UC\")"
                    }
                }
            }, {
                "overrides": {}
            }, {
                "overrides": {}
            });
            flxBreadcrumb.add(breadcrumbs);
            var flxMainContent = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "bottom": "0%",
                "clipBounds": true,
                "height": "88.50%",
                "id": "flxMainContent",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "isModalContainer": false,
                "right": "0dp",
                "skin": "slFbox",
                "top": "0px",
                "zIndex": 1
            }, {}, {});
            flxMainContent.setDefaultUnit(kony.flex.DP);
            var flxRequestsListContainer = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "100%",
                "id": "flxRequestsListContainer",
                "isVisible": true,
                "layoutType": kony.flex.FLOW_VERTICAL,
                "left": "35dp",
                "isModalContainer": false,
                "right": "35px",
                "skin": "slFbox",
                "top": "15dp"
            }, {}, {});
            flxRequestsListContainer.setDefaultUnit(kony.flex.DP);
            var flxApprovalNotification = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "bottom": "15dp",
                "clipBounds": true,
                "height": "40dp",
                "id": "flxApprovalNotification",
                "isVisible": false,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "isModalContainer": false,
                "right": "35px",
                "skin": "slFbox",
                "top": "0dp",
                "width": "100%",
                "zIndex": 1
            }, {}, {});
            flxApprovalNotification.setDefaultUnit(kony.flex.DP);
            var NotificationFlagMessage = new com.adminConsole.common.NotificationFlagMessage({
                "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                "id": "NotificationFlagMessage",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "masterType": constants.MASTER_TYPE_DEFAULT,
                "isModalContainer": false,
                "right": "0dp",
                "skin": "slFbox",
                "top": "0dp",
                "overrides": {
                    "NotificationFlagMessage": {
                        "right": "0dp",
                        "width": "viz.val_cleared"
                    },
                    "lblMessage": {
                        "i18n_text": "kony.i18n.getLocalizedString(\"i18n.Companies.AutoApprovalNotiMsg\")"
                    }
                }
            }, {
                "overrides": {}
            }, {
                "overrides": {}
            });
            flxApprovalNotification.add(NotificationFlagMessage);
            var flxRequestsList = new kony.ui.FlexContainer({
                "clipBounds": true,
                "height": "650px",
                "id": "flxRequestsList",
                "isVisible": true,
                "layoutType": kony.flex.FLOW_VERTICAL,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "sknFlxBgFFFFFFBorE4E6ECsz1pxR4px",
                "top": "0dp",
                "width": "100%"
            }, {}, {});
            flxRequestsList.setDefaultUnit(kony.flex.DP);
            var flxRequestTabs = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "45dp",
                "id": "flxRequestTabs",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0dp",
                "width": "100%",
                "zIndex": 1
            }, {}, {});
            flxRequestTabs.setDefaultUnit(kony.flex.DP);
            var HeaderTabs = new com.adminConsole.companies.HeaderTabs({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "height": "45px",
                "id": "HeaderTabs",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "masterType": constants.MASTER_TYPE_DEFAULT,
                "isModalContainer": false,
                "skin": "sknbackGroundffffff100",
                "top": "0dp",
                "width": "100%",
                "overrides": {
                    "HeaderTabs": {
                        "height": "45px"
                    },
                    "lblBottomSeperatorCommonTabs": {
                        "bottom": "0dp"
                    },
                    "lblCount1": {
                        "text": "0"
                    },
                    "lblCount2": {
                        "text": "0"
                    }
                }
            }, {
                "overrides": {}
            }, {
                "overrides": {}
            });
            flxRequestTabs.add(HeaderTabs);
            var flxRequestsSearch = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "70dp",
                "id": "flxRequestsSearch",
                "isVisible": false,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "sknBackgroundFFFFFF",
                "top": "0dp",
                "width": "100%",
                "zIndex": 1
            }, {}, {});
            flxRequestsSearch.setDefaultUnit(kony.flex.DP);
            var flxSearch = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "100%",
                "id": "flxSearch",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "isModalContainer": false,
                "right": 20,
                "skin": "CopyslFbox2",
                "top": "0px",
                "width": "430px",
                "zIndex": 1
            }, {}, {});
            flxSearch.setDefaultUnit(kony.flex.DP);
            var flxSearchContainer = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "centerY": "50%",
                "clipBounds": true,
                "height": "40px",
                "id": "flxSearchContainer",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "isModalContainer": false,
                "right": "0dp",
                "skin": "sknflxd5d9ddop100",
                "width": "430px",
                "zIndex": 1
            }, {}, {});
            flxSearchContainer.setDefaultUnit(kony.flex.DP);
            var fontIconSearchImg = new kony.ui.Label({
                "centerY": "50%",
                "id": "fontIconSearchImg",
                "isVisible": true,
                "left": "12px",
                "skin": "sknFontIconSearchImg20px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.userwidgetmodel.lblSearchIcon\")",
                "top": "0dp",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var tbxSearchBox = new kony.ui.TextBox2({
                "autoCapitalize": constants.TEXTBOX_AUTO_CAPITALIZE_NONE,
                "centerY": "50%",
                "height": "30px",
                "id": "tbxSearchBox",
                "isVisible": true,
                "keyBoardStyle": constants.TEXTBOX_KEY_BOARD_STYLE_DEFAULT,
                "left": "30dp",
                "onTouchStart": controller.AS_TextField_cfab06859f0f43eca99495891ee290d5,
                "placeholder": "Search by Company name",
                "right": "35px",
                "secureTextEntry": false,
                "skin": "skntbxffffffNoBorderlato35475f14px",
                "textInputMode": constants.TEXTBOX_INPUT_MODE_ANY,
                "zIndex": 1
            }, {
                "containerHeightMode": constants.TEXTBOX_FONT_METRICS_DRIVEN_HEIGHT,
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [2, 0, 0, 0],
                "paddingInPixel": false
            }, {
                "autoCorrect": false
            });
            var flxClearSearchImage = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "100%",
                "id": "flxClearSearchImage",
                "isVisible": false,
                "layoutType": kony.flex.FREE_FORM,
                "isModalContainer": false,
                "right": "0px",
                "skin": "sknCursor",
                "top": "0dp",
                "width": "35px",
                "zIndex": 1
            }, {}, {
                "hoverSkin": "sknCursor"
            });
            flxClearSearchImage.setDefaultUnit(kony.flex.DP);
            var fontIconCross = new kony.ui.Label({
                "centerY": "50%",
                "id": "fontIconCross",
                "isVisible": true,
                "left": "5px",
                "skin": "sknFontIconSearchCross16px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.userwidgetmodel.lblClearSearch\")",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxClearSearchImage.add(fontIconCross);
            flxSearchContainer.add(fontIconSearchImg, tbxSearchBox, flxClearSearchImage);
            flxSearch.add(flxSearchContainer);
            var flxSerachSeparator = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "bottom": "0dp",
                "clipBounds": true,
                "height": "1dp",
                "id": "flxSerachSeparator",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "20dp",
                "isModalContainer": false,
                "right": "20dp",
                "skin": "sknflxD5D9DD",
                "zIndex": 1
            }, {}, {});
            flxSerachSeparator.setDefaultUnit(kony.flex.DP);
            flxSerachSeparator.add();
            flxRequestsSearch.add(flxSearch, flxSerachSeparator);
            var flxNoRecordsToShow = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "centerY": "45%",
                "clipBounds": true,
                "height": "20dp",
                "id": "flxNoRecordsToShow",
                "isVisible": false,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "sknBackgroundFFFFFF",
                "width": "100%",
                "zIndex": 1
            }, {}, {});
            flxNoRecordsToShow.setDefaultUnit(kony.flex.DP);
            var lblNoRequestsToShow = new kony.ui.Label({
                "centerX": "50%",
                "centerY": "50%",
                "id": "lblNoRequestsToShow",
                "isVisible": true,
                "left": "0dp",
                "skin": "slLabel0d20174dce8ea42",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmLogs.rtxNoResultsFound\")",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_CENTER,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxNoRecordsToShow.add(lblNoRequestsToShow);
            var flxRequests = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "bottom": "10dp",
                "clipBounds": true,
                "height": "530px",
                "id": "flxRequests",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0",
                "isModalContainer": false,
                "right": "0dp",
                "skin": "sknBackgroundFFFFFF",
                "top": "0dp",
                "width": "100%"
            }, {}, {});
            flxRequests.setDefaultUnit(kony.flex.DP);
            var flxRequestsHeader = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "60px",
                "id": "flxRequestsHeader",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0dp",
                "width": "100%"
            }, {}, {});
            flxRequestsHeader.setDefaultUnit(kony.flex.DP);
            var flxRejectedRequestsHeader = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "60px",
                "id": "flxRejectedRequestsHeader",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0px",
                "isModalContainer": false,
                "right": "0px",
                "skin": "slFbox",
                "top": "0dp"
            }, {}, {});
            flxRejectedRequestsHeader.setDefaultUnit(kony.flex.DP);
            var flxHeaderCompanyName2 = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "100%",
                "id": "flxHeaderCompanyName2",
                "isVisible": true,
                "layoutType": kony.flex.FLOW_HORIZONTAL,
                "left": "30dp",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0dp",
                "width": "15%",
                "zIndex": 1
            }, {}, {});
            flxHeaderCompanyName2.setDefaultUnit(kony.flex.DP);
            var lblHeaderCompanyName2 = new kony.ui.Label({
                "centerY": "50%",
                "id": "lblHeaderCompanyName2",
                "isVisible": true,
                "left": "0",
                "skin": "sknlblLato696c7312px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmCompanies.Contract_ID\")",
                "top": 0,
                "width": "100px",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {
                "hoverSkin": "sknlblLato00000011px"
            });
            var lblSortCompanyName2 = new kony.ui.Label({
                "centerY": "50%",
                "height": "15dp",
                "id": "lblSortCompanyName2",
                "isVisible": true,
                "left": "0dp",
                "skin": "sknIcon15px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmAlertsManagement.lblSortName\")",
                "width": "15dp",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxHeaderCompanyName2.add(lblHeaderCompanyName2, lblSortCompanyName2);
            var flxContractName = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "100%",
                "id": "flxContractName",
                "isVisible": true,
                "layoutType": kony.flex.FLOW_HORIZONTAL,
                "left": "18%",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0dp",
                "width": "18%",
                "zIndex": 1
            }, {}, {});
            flxContractName.setDefaultUnit(kony.flex.DP);
            var lblHdrContractName = new kony.ui.Label({
                "centerY": "50%",
                "id": "lblHdrContractName",
                "isVisible": true,
                "left": "0",
                "skin": "sknlblLato696c7312px",
                "text": "COMPANY NAME",
                "top": 0,
                "width": "120px",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {
                "hoverSkin": "sknlblLato00000011px"
            });
            var lblSortContractName = new kony.ui.Label({
                "centerY": "50%",
                "height": "15dp",
                "id": "lblSortContractName",
                "isVisible": true,
                "left": "0dp",
                "skin": "sknIcon15px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmAlertsManagement.lblSortName\")",
                "width": "15dp",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxContractName.add(lblHdrContractName, lblSortContractName);
            var flxHeaderBusinessTypes2 = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "100%",
                "id": "flxHeaderBusinessTypes2",
                "isVisible": true,
                "layoutType": kony.flex.FLOW_HORIZONTAL,
                "left": "38%",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0dp",
                "width": "12%",
                "zIndex": 1
            }, {}, {
                "hoverSkin": "sknFlxPointer"
            });
            flxHeaderBusinessTypes2.setDefaultUnit(kony.flex.DP);
            var lblBusinessTypesHeader2 = new kony.ui.Label({
                "centerY": "50%",
                "id": "lblBusinessTypesHeader2",
                "isVisible": true,
                "left": "0px",
                "skin": "sknlblLato696c7312px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmServiceManagementController.SERVICES\")",
                "top": 0,
                "width": "70px",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {
                "hoverSkin": "sknlblLato00000011px"
            });
            var lblBusinessTypesFilter2 = new kony.ui.Label({
                "centerY": "50%",
                "height": "15dp",
                "id": "lblBusinessTypesFilter2",
                "isVisible": true,
                "left": "0",
                "skin": "sknIcon15px",
                "text": "",
                "width": "15dp",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {
                "hoverSkin": "sknlblCursorFont"
            });
            flxHeaderBusinessTypes2.add(lblBusinessTypesHeader2, lblBusinessTypesFilter2);
            var flxHeaderSubmittedOn2 = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "100%",
                "id": "flxHeaderSubmittedOn2",
                "isVisible": true,
                "layoutType": kony.flex.FLOW_HORIZONTAL,
                "left": "50%",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0dp",
                "width": "17%",
                "zIndex": 1
            }, {}, {});
            flxHeaderSubmittedOn2.setDefaultUnit(kony.flex.DP);
            var lblSubmittedOnHeader2 = new kony.ui.Label({
                "centerY": "50%",
                "id": "lblSubmittedOnHeader2",
                "isVisible": true,
                "left": "0px",
                "skin": "sknlblLato696c7312px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmLoansDashboard.SubmittedOn\")",
                "top": 0,
                "width": "90px",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var lblSubmittedOnSort2 = new kony.ui.Label({
                "centerY": "50%",
                "height": "15dp",
                "id": "lblSubmittedOnSort2",
                "isVisible": true,
                "left": "0dp",
                "skin": "sknIcon15px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmAlertsManagement.lblSortName\")",
                "width": "15dp",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxHeaderSubmittedOn2.add(lblSubmittedOnHeader2, lblSubmittedOnSort2);
            var flxHeaderRejectedOn = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "100%",
                "id": "flxHeaderRejectedOn",
                "isVisible": true,
                "layoutType": kony.flex.FLOW_HORIZONTAL,
                "left": "67%",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0dp",
                "width": "17%",
                "zIndex": 1
            }, {}, {});
            flxHeaderRejectedOn.setDefaultUnit(kony.flex.DP);
            var lblRejectedOn = new kony.ui.Label({
                "centerY": "50%",
                "id": "lblRejectedOn",
                "isVisible": true,
                "left": "0px",
                "skin": "sknlblLato696c7312px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.Companies.rejectedOn_UC\")",
                "top": 0,
                "width": "90px",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {
                "hoverSkin": "sknlblLato00000011px"
            });
            var lblRejectedOnSort = new kony.ui.Label({
                "centerY": "50%",
                "height": "15dp",
                "id": "lblRejectedOnSort",
                "isVisible": true,
                "left": "0",
                "skin": "sknIcon15px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmAlertsManagement.lblSortName\")",
                "width": "15dp",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxHeaderRejectedOn.add(lblRejectedOn, lblRejectedOnSort);
            var flxHeaderRejectedBy = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "100%",
                "id": "flxHeaderRejectedBy",
                "isVisible": true,
                "layoutType": kony.flex.FLOW_HORIZONTAL,
                "left": "82%",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0dp",
                "width": "15%",
                "zIndex": 1
            }, {}, {});
            flxHeaderRejectedBy.setDefaultUnit(kony.flex.DP);
            var lblRejectedBy = new kony.ui.Label({
                "centerY": "50%",
                "id": "lblRejectedBy",
                "isVisible": true,
                "left": "0px",
                "skin": "sknlblLato696c7312px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.Companies.rejectedBy_UC\")",
                "top": 0,
                "width": "90px",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {
                "hoverSkin": "sknlblLato00000011px"
            });
            var lblRejectedBySort = new kony.ui.Label({
                "centerY": "50%",
                "height": "15dp",
                "id": "lblRejectedBySort",
                "isVisible": true,
                "left": "0",
                "skin": "sknIcon15px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmAlertsManagement.lblSortName\")",
                "width": "15dp",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxHeaderRejectedBy.add(lblRejectedBy, lblRejectedBySort);
            flxRejectedRequestsHeader.add(flxHeaderCompanyName2, flxContractName, flxHeaderBusinessTypes2, flxHeaderSubmittedOn2, flxHeaderRejectedOn, flxHeaderRejectedBy);
            var flxPendingRequestsHeader = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "60px",
                "id": "flxPendingRequestsHeader",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0px",
                "isModalContainer": false,
                "right": "0px",
                "skin": "slFbox",
                "top": "0"
            }, {}, {});
            flxPendingRequestsHeader.setDefaultUnit(kony.flex.DP);
            var flxPendingContrId = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "100%",
                "id": "flxPendingContrId",
                "isVisible": true,
                "layoutType": kony.flex.FLOW_HORIZONTAL,
                "left": "30px",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0dp",
                "width": "18%",
                "zIndex": 1
            }, {}, {});
            flxPendingContrId.setDefaultUnit(kony.flex.DP);
            var lblHdrContractId = new kony.ui.Label({
                "centerY": "50%",
                "id": "lblHdrContractId",
                "isVisible": true,
                "left": "0",
                "skin": "sknlblLato696c7312px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmCompanies.Contract_ID\")",
                "top": 0,
                "width": "100px",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {
                "hoverSkin": "sknlblLato00000011px"
            });
            var lblHdrContractSortIcon = new kony.ui.Label({
                "centerY": "50%",
                "height": "15dp",
                "id": "lblHdrContractSortIcon",
                "isVisible": true,
                "left": "0dp",
                "skin": "sknIcon15px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmAlertsManagement.lblSortName\")",
                "width": "15dp",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxPendingContrId.add(lblHdrContractId, lblHdrContractSortIcon);
            var flxHeaderCompanyName1 = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "100%",
                "id": "flxHeaderCompanyName1",
                "isVisible": true,
                "layoutType": kony.flex.FLOW_HORIZONTAL,
                "left": "22%",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0dp",
                "width": "20%",
                "zIndex": 1
            }, {}, {});
            flxHeaderCompanyName1.setDefaultUnit(kony.flex.DP);
            var lblHeaderCompanyName1 = new kony.ui.Label({
                "centerY": "50%",
                "id": "lblHeaderCompanyName1",
                "isVisible": true,
                "left": "0",
                "skin": "sknlblLato696c7312px",
                "text": "COMPANY NAME",
                "top": 0,
                "width": "120px",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {
                "hoverSkin": "sknlblLato00000011px"
            });
            var lblSortCompanyName1 = new kony.ui.Label({
                "centerY": "50%",
                "height": "15dp",
                "id": "lblSortCompanyName1",
                "isVisible": true,
                "left": "0dp",
                "skin": "sknIcon15px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmAlertsManagement.lblSortName\")",
                "width": "15dp",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxHeaderCompanyName1.add(lblHeaderCompanyName1, lblSortCompanyName1);
            var flxHeaderBusinessTypes1 = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "100%",
                "id": "flxHeaderBusinessTypes1",
                "isVisible": true,
                "layoutType": kony.flex.FLOW_HORIZONTAL,
                "left": "44%",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0dp",
                "width": "16%",
                "zIndex": 1
            }, {}, {});
            flxHeaderBusinessTypes1.setDefaultUnit(kony.flex.DP);
            var lblBusinessTypesHeader1 = new kony.ui.Label({
                "centerY": "50%",
                "id": "lblBusinessTypesHeader1",
                "isVisible": true,
                "left": "0px",
                "skin": "sknlblLato696c7312px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmBusinessTypes.business_types_UC\")",
                "top": 0,
                "width": "100px",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {
                "hoverSkin": "sknlblLato00000011px"
            });
            var lblBusinessTypesFilter1 = new kony.ui.Label({
                "centerY": "50%",
                "height": "15dp",
                "id": "lblBusinessTypesFilter1",
                "isVisible": true,
                "left": "0",
                "skin": "sknIcon15px",
                "text": "",
                "width": "15dp",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {
                "hoverSkin": "sknlblCursorFont"
            });
            flxHeaderBusinessTypes1.add(lblBusinessTypesHeader1, lblBusinessTypesFilter1);
            var flxHeaderSubmittedOn1 = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "100%",
                "id": "flxHeaderSubmittedOn1",
                "isVisible": true,
                "layoutType": kony.flex.FLOW_HORIZONTAL,
                "left": "62%",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0dp",
                "width": "17%",
                "zIndex": 1
            }, {}, {});
            flxHeaderSubmittedOn1.setDefaultUnit(kony.flex.DP);
            var lblSubmittedOnHeader1 = new kony.ui.Label({
                "centerY": "50%",
                "id": "lblSubmittedOnHeader1",
                "isVisible": true,
                "left": "0px",
                "skin": "sknlblLato696c7312px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmLoansDashboard.SubmittedOn\")",
                "top": 0,
                "width": "90px",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var lblSubmittedOnSort1 = new kony.ui.Label({
                "centerY": "50%",
                "height": "15dp",
                "id": "lblSubmittedOnSort1",
                "isVisible": true,
                "left": "0dp",
                "skin": "sknIcon15px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmAlertsManagement.lblSortName\")",
                "width": "15dp",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxHeaderSubmittedOn1.add(lblSubmittedOnHeader1, lblSubmittedOnSort1);
            var flxHeaderSubmittedBy = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "100%",
                "id": "flxHeaderSubmittedBy",
                "isVisible": true,
                "layoutType": kony.flex.FLOW_HORIZONTAL,
                "left": "82%",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0dp",
                "width": "15%",
                "zIndex": 1
            }, {}, {});
            flxHeaderSubmittedBy.setDefaultUnit(kony.flex.DP);
            var lblSubmittedByHeader = new kony.ui.Label({
                "centerY": "50%",
                "id": "lblSubmittedByHeader",
                "isVisible": true,
                "left": "0px",
                "skin": "sknlblLato696c7312px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.Companies.SubmittedBy\")",
                "top": 0,
                "width": "90px",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {
                "hoverSkin": "sknlblLato00000011px"
            });
            var lblSubmittedBySort = new kony.ui.Label({
                "centerY": "50%",
                "height": "15dp",
                "id": "lblSubmittedBySort",
                "isVisible": true,
                "left": "0",
                "skin": "sknIcon15px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmAlertsManagement.lblSortName\")",
                "width": "15dp",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxHeaderSubmittedBy.add(lblSubmittedByHeader, lblSubmittedBySort);
            flxPendingRequestsHeader.add(flxPendingContrId, flxHeaderCompanyName1, flxHeaderBusinessTypes1, flxHeaderSubmittedOn1, flxHeaderSubmittedBy);
            flxRequestsHeader.add(flxRejectedRequestsHeader, flxPendingRequestsHeader);
            var flxSegHeaderSeparator = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "1dp",
                "id": "flxSegHeaderSeparator",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "20dp",
                "isModalContainer": false,
                "right": "20dp",
                "skin": "sknTableHeaderLine",
                "top": "60dp",
                "zIndex": 1
            }, {}, {});
            flxSegHeaderSeparator.setDefaultUnit(kony.flex.DP);
            flxSegHeaderSeparator.add();
            var flxRequestsSegment = new kony.ui.FlexContainer({
                "bottom": "0dp",
                "clipBounds": true,
                "id": "flxRequestsSegment",
                "isVisible": true,
                "layoutType": kony.flex.FLOW_VERTICAL,
                "left": "0dp",
                "isModalContainer": false,
                "right": "0dp",
                "skin": "slFbox",
                "top": "61dp"
            }, {}, {});
            flxRequestsSegment.setDefaultUnit(kony.flex.DP);
            var segRequests = new kony.ui.SegmentedUI2({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "bottom": "0dp",
                "data": [{
                    "lblContractId": "XYZ Company",
                    "lblContractName": "ACH_Collection_1",
                    "lblSeperator": ".",
                    "lblServices": "17/08/2017 10:30PM",
                    "lblSubmittedBy": "Admin1",
                    "lblSubmittedOn": "18/08/2017 12:30PM"
                }, {
                    "lblContractId": "XYZ Company",
                    "lblContractName": "ACH_Collection_1",
                    "lblSeperator": ".",
                    "lblServices": "17/08/2017 10:30PM",
                    "lblSubmittedBy": "Admin1",
                    "lblSubmittedOn": "18/08/2017 12:30PM"
                }, {
                    "lblContractId": "XYZ Company",
                    "lblContractName": "ACH_Collection_1",
                    "lblSeperator": ".",
                    "lblServices": "17/08/2017 10:30PM",
                    "lblSubmittedBy": "Admin1",
                    "lblSubmittedOn": "18/08/2017 12:30PM"
                }, {
                    "lblContractId": "XYZ Company",
                    "lblContractName": "ACH_Collection_1",
                    "lblSeperator": ".",
                    "lblServices": "17/08/2017 10:30PM",
                    "lblSubmittedBy": "Admin1",
                    "lblSubmittedOn": "18/08/2017 12:30PM"
                }, {
                    "lblContractId": "XYZ Company",
                    "lblContractName": "ACH_Collection_1",
                    "lblSeperator": ".",
                    "lblServices": "17/08/2017 10:30PM",
                    "lblSubmittedBy": "Admin1",
                    "lblSubmittedOn": "18/08/2017 12:30PM"
                }, {
                    "lblContractId": "XYZ Company",
                    "lblContractName": "ACH_Collection_1",
                    "lblSeperator": ".",
                    "lblServices": "17/08/2017 10:30PM",
                    "lblSubmittedBy": "Admin1",
                    "lblSubmittedOn": "18/08/2017 12:30PM"
                }, {
                    "lblContractId": "XYZ Company",
                    "lblContractName": "ACH_Collection_1",
                    "lblSeperator": ".",
                    "lblServices": "17/08/2017 10:30PM",
                    "lblSubmittedBy": "Admin1",
                    "lblSubmittedOn": "18/08/2017 12:30PM"
                }, {
                    "lblContractId": "XYZ Company",
                    "lblContractName": "ACH_Collection_1",
                    "lblSeperator": ".",
                    "lblServices": "17/08/2017 10:30PM",
                    "lblSubmittedBy": "Admin1",
                    "lblSubmittedOn": "18/08/2017 12:30PM"
                }, {
                    "lblContractId": "XYZ Company",
                    "lblContractName": "ACH_Collection_1",
                    "lblSeperator": ".",
                    "lblServices": "17/08/2017 10:30PM",
                    "lblSubmittedBy": "Admin1",
                    "lblSubmittedOn": "18/08/2017 12:30PM"
                }, {
                    "lblContractId": "XYZ Company",
                    "lblContractName": "ACH_Collection_1",
                    "lblSeperator": ".",
                    "lblServices": "17/08/2017 10:30PM",
                    "lblSubmittedBy": "Admin1",
                    "lblSubmittedOn": "18/08/2017 12:30PM"
                }],
                "groupCells": false,
                "height": "100%",
                "id": "segRequests",
                "isVisible": true,
                "left": "0",
                "needPageIndicator": true,
                "pageOffDotImage": "pageoffdot.png",
                "pageOnDotImage": "pageondot.png",
                "retainSelection": false,
                "rowFocusSkin": "seg2Focus",
                "rowSkin": "seg2Normal",
                "rowTemplate": "flxPendingRequests",
                "sectionHeaderSkin": "sliPhoneSegmentHeader",
                "selectionBehavior": constants.SEGUI_DEFAULT_BEHAVIOR,
                "separatorRequired": false,
                "showScrollbars": true,
                "top": "0",
                "viewType": constants.SEGUI_VIEW_TYPE_TABLEVIEW,
                "widgetDataMap": {
                    "flxPendingRequests": "flxPendingRequests",
                    "lblContractId": "lblContractId",
                    "lblContractName": "lblContractName",
                    "lblSeperator": "lblSeperator",
                    "lblServices": "lblServices",
                    "lblSubmittedBy": "lblSubmittedBy",
                    "lblSubmittedOn": "lblSubmittedOn"
                },
                "width": "100%"
            }, {
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxRequestsSegment.add(segRequests);
            var flxBusinessTypesFilter = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                "clipBounds": true,
                "id": "flxBusinessTypesFilter",
                "isVisible": false,
                "layoutType": kony.flex.FREE_FORM,
                "isModalContainer": false,
                "right": "63%",
                "skin": "slFbox",
                "top": "37px",
                "width": "150px",
                "zIndex": 5
            }, {}, {});
            flxBusinessTypesFilter.setDefaultUnit(kony.flex.DP);
            var BusinessTypesFilterMenu = new com.adminConsole.common.statusFilterMenu({
                "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                "id": "BusinessTypesFilterMenu",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "masterType": constants.MASTER_TYPE_DEFAULT,
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "5dp",
                "width": "100%",
                "zIndex": 5,
                "overrides": {
                    "imgUpArrow": {
                        "src": "uparrow_2x.png"
                    },
                    "segStatusFilterDropdown": {
                        "left": "0dp",
                        "top": "5dp"
                    }
                }
            }, {
                "overrides": {}
            }, {
                "overrides": {}
            });
            flxBusinessTypesFilter.add(BusinessTypesFilterMenu);
            var flxNoRequestsFound = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                "clipBounds": true,
                "id": "flxNoRequestsFound",
                "isVisible": false,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "isModalContainer": false,
                "right": "0dp",
                "skin": "slFbox",
                "top": "60dp",
                "width": "100%"
            }, {}, {});
            flxNoRequestsFound.setDefaultUnit(kony.flex.DP);
            var lblNoRequestsFound = new kony.ui.Label({
                "centerX": "50%",
                "centerY": "50%",
                "id": "lblNoRequestsFound",
                "isVisible": true,
                "left": "0dp",
                "skin": "slLabel0d20174dce8ea42",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmLogs.rtxNoResultsFound\")",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_CENTER,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxNoRequestsFound.add(lblNoRequestsFound);
            flxRequests.add(flxRequestsHeader, flxSegHeaderSeparator, flxRequestsSegment, flxBusinessTypesFilter, flxNoRequestsFound);
            flxRequestsList.add(flxRequestTabs, flxRequestsSearch, flxNoRecordsToShow, flxRequests);
            flxRequestsListContainer.add(flxApprovalNotification, flxRequestsList);
            var flxRequestDetails = new kony.ui.FlexContainer({
                "bottom": "30dp",
                "clipBounds": true,
                "id": "flxRequestDetails",
                "isVisible": false,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "isModalContainer": false,
                "right": "0dp",
                "skin": "slFbox",
                "top": "15dp",
                "zIndex": 1
            }, {}, {});
            flxRequestDetails.setDefaultUnit(kony.flex.DP);
            var flxDetailContainer = new kony.ui.FlexScrollContainer({
                "allowHorizontalBounce": false,
                "allowVerticalBounce": true,
                "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                "bottom": "80dp",
                "bounces": true,
                "clipBounds": true,
                "enableScrolling": true,
                "horizontalScrollIndicator": true,
                "id": "flxDetailContainer",
                "isVisible": true,
                "layoutType": kony.flex.FLOW_VERTICAL,
                "left": "35dp",
                "pagingEnabled": false,
                "right": "35dp",
                "scrollDirection": kony.flex.SCROLL_VERTICAL,
                "skin": "sknFlxFFFFFF",
                "top": "0dp",
                "verticalScrollIndicator": true
            }, {}, {});
            flxDetailContainer.setDefaultUnit(kony.flex.DP);
            var flxMainDetails = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                "clipBounds": true,
                "id": "flxMainDetails",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0dp",
                "width": "100%",
                "zIndex": 1
            }, {}, {});
            flxMainDetails.setDefaultUnit(kony.flex.DP);
            var flxNameHeader = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "57px",
                "id": "flxNameHeader",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "isModalContainer": false,
                "right": "0dp",
                "skin": "slFbox",
                "top": "0dp"
            }, {}, {});
            flxNameHeader.setDefaultUnit(kony.flex.DP);
            var flxViewRequestNameStatus = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "100%",
                "id": "flxViewRequestNameStatus",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0px",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0dp",
                "width": "100%",
                "zIndex": 1
            }, {}, {});
            flxViewRequestNameStatus.setDefaultUnit(kony.flex.DP);
            var lblCompanyDetailName = new kony.ui.Label({
                "bottom": "15dp",
                "centerY": "50%",
                "id": "lblCompanyDetailName",
                "isVisible": true,
                "left": "20dp",
                "skin": "sknLbl192b45LatoReg22px",
                "text": "Kony",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_TOP_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var flxDetailsStatus = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "100%",
                "id": "flxDetailsStatus",
                "isVisible": true,
                "layoutType": kony.flex.FLOW_HORIZONTAL,
                "reverseLayoutDirection": true,
                "isModalContainer": false,
                "right": "20dp",
                "skin": "slFbox",
                "top": "0dp",
                "width": "150dp"
            }, {}, {});
            flxDetailsStatus.setDefaultUnit(kony.flex.DP);
            var lblViewStatus = new kony.ui.Label({
                "centerY": "50%",
                "id": "lblViewStatus",
                "isVisible": true,
                "right": "20dp",
                "skin": "sknlbl485C75LatoSemiBold13px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.secureimage.Active\")",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_RIGHT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var fontIconViewStatus = new kony.ui.Label({
                "centerY": "50%",
                "id": "fontIconViewStatus",
                "isVisible": true,
                "right": "7dp",
                "skin": "sknFontIconSuspend",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmAlertsManagement.lblViewAlertStatusKey\")",
                "top": "2dp",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxDetailsStatus.add(lblViewStatus, fontIconViewStatus);
            flxViewRequestNameStatus.add(lblCompanyDetailName, flxDetailsStatus);
            var btnCompanyDetailEdit = new kony.ui.Button({
                "height": "22px",
                "id": "btnCompanyDetailEdit",
                "isVisible": false,
                "right": "20px",
                "skin": "sknbtnf7f7faLatoRegular12Px485c75Border1px485C75Radius20px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.roles.Edit\")",
                "top": "20px",
                "width": "50px",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_CENTER,
                "displayText": true,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {
                "hoverSkin": "sknbtnffffffLatoRegular12Px485c75Border1px485C75Radius20px"
            });
            var flxLine = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "bottom": "0dp",
                "clipBounds": true,
                "height": "1dp",
                "id": "flxLine",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "20dp",
                "isModalContainer": false,
                "right": "25dp",
                "skin": "sknFlxD7D9E0Separator",
                "zIndex": 1
            }, {}, {});
            flxLine.setDefaultUnit(kony.flex.DP);
            flxLine.add();
            flxNameHeader.add(flxViewRequestNameStatus, btnCompanyDetailEdit, flxLine);
            var flxCompanyDetailsContainer = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                "clipBounds": true,
                "id": "flxCompanyDetailsContainer",
                "isVisible": true,
                "layoutType": kony.flex.FLOW_VERTICAL,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "57dp",
                "width": "100%"
            }, {}, {});
            flxCompanyDetailsContainer.setDefaultUnit(kony.flex.DP);
            var flxViewContainer1 = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                "clipBounds": true,
                "id": "flxViewContainer1",
                "isVisible": true,
                "layoutType": kony.flex.FLOW_VERTICAL,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0dp",
                "width": "100%",
                "zIndex": 1
            }, {}, {});
            flxViewContainer1.setDefaultUnit(kony.flex.DP);
            var flxRow1 = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                "clipBounds": true,
                "id": "flxRow1",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0dp",
                "width": "100%"
            }, {}, {});
            flxRow1.setDefaultUnit(kony.flex.DP);
            var flxViewBusinessType = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "50dp",
                "id": "flxViewBusinessType",
                "isVisible": false,
                "layoutType": kony.flex.FREE_FORM,
                "left": "20dp",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "15dp",
                "width": "30%",
                "zIndex": 1
            }, {}, {});
            flxViewBusinessType.setDefaultUnit(kony.flex.DP);
            var lblViewBusinessType = new kony.ui.Label({
                "id": "lblViewBusinessType",
                "isVisible": true,
                "left": "0dp",
                "skin": "sknlblLato696c7311px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmCompanies.BusinessType_UC\")",
                "top": "2dp",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var lblViewBusinessTypeVal = new kony.ui.Label({
                "id": "lblViewBusinessTypeVal",
                "isVisible": true,
                "left": "0px",
                "skin": "sknlbl485C75LatoSemiBold13px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmCreateNewUser.lblUser\")",
                "top": "26px",
                "width": "100%",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxViewBusinessType.add(lblViewBusinessType, lblViewBusinessTypeVal);
            var flxViewContractID = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "50dp",
                "id": "flxViewContractID",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "20dp",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "25dp",
                "width": "30%",
                "zIndex": 1
            }, {}, {});
            flxViewContractID.setDefaultUnit(kony.flex.DP);
            var lblViewContractID = new kony.ui.Label({
                "id": "lblViewContractID",
                "isVisible": true,
                "left": "0dp",
                "skin": "sknlblLato696c7311px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmCompanies.Contract_ID\")",
                "top": "2dp",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var lblViewContractIDValue = new kony.ui.Label({
                "id": "lblViewContractIDValue",
                "isVisible": true,
                "left": "0px",
                "skin": "sknlbl485C75LatoSemiBold13px",
                "text": "123456",
                "top": "26px",
                "width": "100%",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxViewContractID.add(lblViewContractID, lblViewContractIDValue);
            var flxViewTIN = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "60dp",
                "id": "flxViewTIN",
                "isVisible": false,
                "layoutType": kony.flex.FREE_FORM,
                "left": "40%",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "15dp",
                "width": "31%",
                "zIndex": 1
            }, {}, {});
            flxViewTIN.setDefaultUnit(kony.flex.DP);
            var lblViewTINKey = new kony.ui.Label({
                "id": "lblViewTINKey",
                "isVisible": true,
                "left": "0dp",
                "skin": "sknlblLato696c7311px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmCustomerManagement.TIN\")",
                "top": "2dp",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var lblViewTINValue = new kony.ui.Label({
                "id": "lblViewTINValue",
                "isVisible": true,
                "left": "0px",
                "skin": "sknlbl485C75LatoSemiBold13px",
                "text": "217261736173",
                "top": "26px",
                "width": "100%",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var lblDetailsMore = new kony.ui.Label({
                "id": "lblDetailsMore",
                "isVisible": false,
                "left": "0dp",
                "skin": "sknLblLatoReg117eb013px",
                "text": "+2 more",
                "top": "42dp",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_TOP_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {
                "hoverSkin": "sknLbl117eb013pxHov"
            });
            flxViewTIN.add(lblViewTINKey, lblViewTINValue, lblDetailsMore);
            var flxViewServiceType = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "60dp",
                "id": "flxViewServiceType",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "40%",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "15dp",
                "width": "31%",
                "zIndex": 1
            }, {}, {});
            flxViewServiceType.setDefaultUnit(kony.flex.DP);
            var lblViewServiceType = new kony.ui.Label({
                "id": "lblViewServiceType",
                "isVisible": true,
                "left": "0dp",
                "skin": "sknlblLato696c7311px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmCompanies.Service_Type\")",
                "top": "2dp",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var lblViewServiceTypeValue = new kony.ui.Label({
                "id": "lblViewServiceTypeValue",
                "isVisible": true,
                "left": "0px",
                "skin": "sknlbl485C75LatoSemiBold13px",
                "text": "Sole Proprietor",
                "top": "26px",
                "width": "100%",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxViewServiceType.add(lblViewServiceType, lblViewServiceTypeValue);
            var flxViewService = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "60dp",
                "id": "flxViewService",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "72%",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "15dp",
                "width": "28%",
                "zIndex": 1
            }, {}, {});
            flxViewService.setDefaultUnit(kony.flex.DP);
            var lblViewService = new kony.ui.Label({
                "id": "lblViewService",
                "isVisible": true,
                "left": "0dp",
                "skin": "sknlblLato696c7311px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmEnrollCustomer.Service_UC\")",
                "top": "2dp",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var lblViewServiceValue = new kony.ui.Label({
                "id": "lblViewServiceValue",
                "isVisible": true,
                "left": "0px",
                "skin": "sknlbl485C75LatoSemiBold13px",
                "text": "Trust",
                "top": "26px",
                "width": "100%",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxViewService.add(lblViewService, lblViewServiceValue);
            var flxSubsidiaryView = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "50dp",
                "id": "flxSubsidiaryView",
                "isVisible": false,
                "layoutType": kony.flex.FREE_FORM,
                "left": "72%",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "15dp",
                "width": "28%",
                "zIndex": 1
            }, {}, {});
            flxSubsidiaryView.setDefaultUnit(kony.flex.DP);
            var lblSubsidaryViewLbl = new kony.ui.Label({
                "id": "lblSubsidaryViewLbl",
                "isVisible": true,
                "left": "0dp",
                "skin": "sknlblLato696c7311px",
                "text": "SUBSIDARY COMPANY",
                "top": "2dp",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var flxViewAllSubsidary = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                "clipBounds": true,
                "id": "flxViewAllSubsidary",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "isModalContainer": false,
                "onClick": controller.AS_FlexContainer_cfd5bb0cf5be40f1a410c7dcaa363ea5,
                "skin": "sknCursor",
                "top": "26px",
                "width": kony.flex.USE_PREFFERED_SIZE
            }, {}, {});
            flxViewAllSubsidary.setDefaultUnit(kony.flex.DP);
            var lblViewSubsidaryCompany = new kony.ui.Label({
                "id": "lblViewSubsidaryCompany",
                "isVisible": true,
                "left": "0px",
                "skin": "sknLblLato13px117eb0",
                "text": "View Subsidary Companies",
                "top": "0px",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxViewAllSubsidary.add(lblViewSubsidaryCompany);
            flxSubsidiaryView.add(lblSubsidaryViewLbl, flxViewAllSubsidary);
            flxRow1.add(flxViewBusinessType, flxViewContractID, flxViewTIN, flxViewServiceType, flxViewService, flxSubsidiaryView);
            var flxRow2 = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                "clipBounds": true,
                "id": "flxRow2",
                "isVisible": false,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0dp",
                "width": "100%"
            }, {}, {});
            flxRow2.setDefaultUnit(kony.flex.DP);
            var flxViewSubmittedOn = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "50dp",
                "id": "flxViewSubmittedOn",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "20dp",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "20dp",
                "width": "28%",
                "zIndex": 1
            }, {}, {});
            flxViewSubmittedOn.setDefaultUnit(kony.flex.DP);
            var lblViewSubmittedOnKey = new kony.ui.Label({
                "id": "lblViewSubmittedOnKey",
                "isVisible": true,
                "left": "0dp",
                "skin": "sknlblLato696c7311px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmLoansDashboard.SubmittedOn\")",
                "top": "2dp",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var lblViewSubmittedOnValue = new kony.ui.Label({
                "id": "lblViewSubmittedOnValue",
                "isVisible": true,
                "left": "0px",
                "skin": "sknlbl485C75LatoSemiBold13px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmUsers.lblViewValue3\")",
                "top": "26px",
                "width": "100%",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxViewSubmittedOn.add(lblViewSubmittedOnKey, lblViewSubmittedOnValue);
            var flxViewRejectedOn = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "50dp",
                "id": "flxViewRejectedOn",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "40%",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "20dp",
                "width": "30%",
                "zIndex": 1
            }, {}, {});
            flxViewRejectedOn.setDefaultUnit(kony.flex.DP);
            var lblViewRejectedOnKey = new kony.ui.Label({
                "id": "lblViewRejectedOnKey",
                "isVisible": true,
                "left": "0dp",
                "skin": "sknlblLato696c7311px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.Companies.rejectedOn_UC\")",
                "top": "2dp",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var lblViewRejectedOnValue = new kony.ui.Label({
                "id": "lblViewRejectedOnValue",
                "isVisible": true,
                "left": "0px",
                "skin": "sknlbl485C75LatoSemiBold13px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmUsers.lblViewValue4\")",
                "top": "26px",
                "width": "100%",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxViewRejectedOn.add(lblViewRejectedOnKey, lblViewRejectedOnValue);
            var flxViewRejectedBy = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                "clipBounds": true,
                "id": "flxViewRejectedBy",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "72%",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "20dp",
                "width": "31%",
                "zIndex": 1
            }, {}, {});
            flxViewRejectedBy.setDefaultUnit(kony.flex.DP);
            var lblViewRejectedByKey = new kony.ui.Label({
                "id": "lblViewRejectedByKey",
                "isVisible": true,
                "left": "0dp",
                "skin": "sknlblLato696c7311px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.Companies.rejectedBy_UC\")",
                "top": "1dp",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var lblViewRejectedByValue = new kony.ui.Label({
                "id": "lblViewRejectedByValue",
                "isVisible": true,
                "left": "0px",
                "skin": "sknlbl485C75LatoSemiBold13px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmUsers.lblViewValue4\")",
                "top": "26px",
                "width": "100%",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxViewRejectedBy.add(lblViewRejectedByKey, lblViewRejectedByValue);
            flxRow2.add(flxViewSubmittedOn, flxViewRejectedOn, flxViewRejectedBy);
            var flxViewRejectedReason = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                "clipBounds": true,
                "id": "flxViewRejectedReason",
                "isVisible": false,
                "layoutType": kony.flex.FREE_FORM,
                "left": "20dp",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "20dp",
                "width": "94%",
                "zIndex": 1
            }, {}, {});
            flxViewRejectedReason.setDefaultUnit(kony.flex.DP);
            var lblRejectedReaonKey = new kony.ui.Label({
                "id": "lblRejectedReaonKey",
                "isVisible": true,
                "left": "0dp",
                "skin": "sknlblLato696c7311px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.Companies.Rejected_Reason_UC\")",
                "top": "0dp",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var lblRejectedReasonValue = new kony.ui.Label({
                "id": "lblRejectedReasonValue",
                "isVisible": true,
                "left": "0dp",
                "skin": "sknlbl485C75LatoSemiBold13px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmUsers.rtxLabellastlogin\")",
                "top": "26dp",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxViewRejectedReason.add(lblRejectedReaonKey, lblRejectedReasonValue);
            flxViewContainer1.add(flxRow1, flxRow2, flxViewRejectedReason);
            var flxCompanyContactContainer = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                "bottom": "0dp",
                "clipBounds": true,
                "id": "flxCompanyContactContainer",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "2dp",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "30dp",
                "width": "100%",
                "zIndex": 1
            }, {}, {});
            flxCompanyContactContainer.setDefaultUnit(kony.flex.DP);
            var flxUserDetailsHeader = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "32dp",
                "id": "flxUserDetailsHeader",
                "isVisible": true,
                "layoutType": kony.flex.FLOW_HORIZONTAL,
                "left": "20dp",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0dp",
                "width": "175dp",
                "zIndex": 1
            }, {}, {
                "hoverSkin": "sknCursor"
            });
            flxUserDetailsHeader.setDefaultUnit(kony.flex.DP);
            var lblUserDetailsHeader = new kony.ui.Label({
                "id": "lblUserDetailsHeader",
                "isVisible": true,
                "left": "0dp",
                "skin": "sknLbl192b45LatoReg16px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.Companies.UserDetails\")",
                "top": "0dp",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var lblArrowIcon = new kony.ui.Label({
                "id": "lblArrowIcon",
                "isVisible": true,
                "left": "10dp",
                "skin": "sknfontIconDescRightArrow14px",
                "text": "",
                "top": "4dp",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxUserDetailsHeader.add(lblUserDetailsHeader, lblArrowIcon);
            var flxViewUserDetails = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                "clipBounds": true,
                "id": "flxViewUserDetails",
                "isVisible": false,
                "layoutType": kony.flex.FLOW_VERTICAL,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "33dp",
                "width": "100%",
                "zIndex": 1
            }, {}, {});
            flxViewUserDetails.setDefaultUnit(kony.flex.DP);
            var flxRow3 = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                "clipBounds": true,
                "id": "flxRow3",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0dp",
                "width": "100%"
            }, {}, {});
            flxRow3.setDefaultUnit(kony.flex.DP);
            var flxViewFirstName = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "50dp",
                "id": "flxViewFirstName",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "20dp",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "15dp",
                "width": "30%",
                "zIndex": 1
            }, {}, {});
            flxViewFirstName.setDefaultUnit(kony.flex.DP);
            var lblViewFirstName = new kony.ui.Label({
                "id": "lblViewFirstName",
                "isVisible": true,
                "left": "0dp",
                "skin": "sknlblLato696c7311px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.Applications.FirstNameCAPS\")",
                "top": "2dp",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var lblViewFirstNameVal = new kony.ui.Label({
                "id": "lblViewFirstNameVal",
                "isVisible": true,
                "left": "0px",
                "skin": "sknlbl485C75LatoSemiBold13px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmCreateNewUser.lblUser\")",
                "top": "26px",
                "width": "100%",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxViewFirstName.add(lblViewFirstName, lblViewFirstNameVal);
            var flxViewLastName = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "50dp",
                "id": "flxViewLastName",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "40%",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "15dp",
                "width": "31%",
                "zIndex": 1
            }, {}, {});
            flxViewLastName.setDefaultUnit(kony.flex.DP);
            var lblViewLastName = new kony.ui.Label({
                "id": "lblViewLastName",
                "isVisible": true,
                "left": "0dp",
                "skin": "sknlblLato696c7311px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.Applications.LastNameCAPS\")",
                "top": "2dp",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var lblViewLastNameVal = new kony.ui.Label({
                "id": "lblViewLastNameVal",
                "isVisible": true,
                "left": "0px",
                "skin": "sknlbl485C75LatoSemiBold13px",
                "text": "217261736173",
                "top": "26px",
                "width": "100%",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxViewLastName.add(lblViewLastName, lblViewLastNameVal);
            var flxCustId = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "50dp",
                "id": "flxCustId",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "72%",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "15dp",
                "width": "28%",
                "zIndex": 1
            }, {}, {});
            flxCustId.setDefaultUnit(kony.flex.DP);
            var lblCustId = new kony.ui.Label({
                "id": "lblCustId",
                "isVisible": true,
                "left": "0dp",
                "skin": "sknlblLato696c7311px",
                "text": "CUSTOMER ID",
                "top": "2dp",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var lblCustIdValue = new kony.ui.Label({
                "id": "lblCustIdValue",
                "isVisible": true,
                "left": "0px",
                "skin": "sknlbl485C75LatoSemiBold13px",
                "text": "217261736173",
                "top": "26px",
                "width": "100%",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxCustId.add(lblCustId, lblCustIdValue);
            flxRow3.add(flxViewFirstName, flxViewLastName, flxCustId);
            var flxRow4 = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                "clipBounds": true,
                "id": "flxRow4",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0",
                "width": "100%"
            }, {}, {});
            flxRow4.setDefaultUnit(kony.flex.DP);
            var flxViewDOB = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                "clipBounds": true,
                "id": "flxViewDOB",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "40%",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "20dp",
                "width": "30%",
                "zIndex": 1
            }, {}, {});
            flxViewDOB.setDefaultUnit(kony.flex.DP);
            var lblViewDOB = new kony.ui.Label({
                "id": "lblViewDOB",
                "isVisible": true,
                "left": "0dp",
                "skin": "sknlblLato696c7311px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmAssistedOnboarding.DATEOFBIRTH\")",
                "top": "0dp",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var lblViewDOBVal = new kony.ui.Label({
                "id": "lblViewDOBVal",
                "isVisible": true,
                "left": "0dp",
                "skin": "sknlbl485C75LatoSemiBold13px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmUsers.rtxLabellastlogin\")",
                "top": "26dp",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxViewDOB.add(lblViewDOB, lblViewDOBVal);
            var flxViewSSN = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "50dp",
                "id": "flxViewSSN",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "20dp",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "15dp",
                "width": "28%",
                "zIndex": 1
            }, {}, {});
            flxViewSSN.setDefaultUnit(kony.flex.DP);
            var lblViewSSN = new kony.ui.Label({
                "id": "lblViewSSN",
                "isVisible": true,
                "left": "0dp",
                "skin": "sknlblLato696c7311px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmCustomerManagementController.RetailCustomerSSN\")",
                "top": "2dp",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var lblViewSSNVal = new kony.ui.Label({
                "id": "lblViewSSNVal",
                "isVisible": true,
                "left": "0px",
                "skin": "sknlbl485C75LatoSemiBold13px",
                "text": "823672647",
                "top": "26px",
                "width": "100%",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxViewSSN.add(lblViewSSN, lblViewSSNVal);
            flxRow4.add(flxViewDOB, flxViewSSN);
            flxViewUserDetails.add(flxRow3, flxRow4);
            flxCompanyContactContainer.add(flxUserDetailsHeader, flxViewUserDetails);
            flxCompanyDetailsContainer.add(flxViewContainer1, flxCompanyContactContainer);
            flxMainDetails.add(flxNameHeader, flxCompanyDetailsContainer);
            var flxViewTabs = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "50dp",
                "id": "flxViewTabs",
                "isVisible": true,
                "layoutType": kony.flex.FLOW_HORIZONTAL,
                "left": "0dp",
                "isModalContainer": false,
                "right": "0dp",
                "skin": "slFbox",
                "top": "10dp",
                "zIndex": 1
            }, {}, {});
            flxViewTabs.setDefaultUnit(kony.flex.DP);
            var flxViewTab1 = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "100%",
                "id": "flxViewTab1",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "20dp",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0dp",
                "width": "68px",
                "zIndex": 1
            }, {}, {});
            flxViewTab1.setDefaultUnit(kony.flex.DP);
            var lblTabName1 = new kony.ui.Label({
                "centerY": "50%",
                "height": "100%",
                "id": "lblTabName1",
                "isVisible": true,
                "left": "0dp",
                "skin": "sknlblLato5d6c7f12px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmCustomerManagementController.ACCOUNTS\")",
                "width": "100%",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_CENTER,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {
                "hoverSkin": "sknlblLato0hd9dd99ab6d14aHover"
            });
            flxViewTab1.add(lblTabName1);
            var flxViewTab2 = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "100%",
                "id": "flxViewTab2",
                "isVisible": false,
                "layoutType": kony.flex.FREE_FORM,
                "left": "20dp",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0dp",
                "width": "64px",
                "zIndex": 1
            }, {}, {});
            flxViewTab2.setDefaultUnit(kony.flex.DP);
            var lblTabName2 = new kony.ui.Label({
                "centerY": "50%",
                "height": "100%",
                "id": "lblTabName2",
                "isVisible": true,
                "left": "0dp",
                "skin": "sknlblLato5d6c7f12px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmCompanies.FEATURES\")",
                "width": "100%",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_CENTER,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {
                "hoverSkin": "sknlblLato0hd9dd99ab6d14aHover"
            });
            flxViewTab2.add(lblTabName2);
            var flxViewTab3 = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "100%",
                "id": "flxViewTab3",
                "isVisible": false,
                "layoutType": kony.flex.FREE_FORM,
                "left": "20dp",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0dp",
                "width": "42px",
                "zIndex": 1
            }, {}, {});
            flxViewTab3.setDefaultUnit(kony.flex.DP);
            var lblTabName3 = new kony.ui.Label({
                "centerY": "50%",
                "height": "100%",
                "id": "lblTabName3",
                "isVisible": true,
                "left": "0dp",
                "skin": "sknlblLato5d6c7f12px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.Group.Limits\")",
                "width": "100%",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_CENTER,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {
                "hoverSkin": "sknlblLato0hd9dd99ab6d14aHover"
            });
            flxViewTab3.add(lblTabName3);
            var flxViewTab4 = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "100%",
                "id": "flxViewTab4",
                "isVisible": false,
                "layoutType": kony.flex.FREE_FORM,
                "left": "20dp",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0dp",
                "width": "100px",
                "zIndex": 1
            }, {}, {});
            flxViewTab4.setDefaultUnit(kony.flex.DP);
            var lblTabName4 = new kony.ui.Label({
                "centerY": "50%",
                "height": "100%",
                "id": "lblTabName4",
                "isVisible": true,
                "left": "0dp",
                "skin": "sknlblLato5d6c7f12px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmCompanies.BUSINESS_USERS\")",
                "width": "100%",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_CENTER,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {
                "hoverSkin": "sknlblLato0hd9dd99ab6d14aHover"
            });
            flxViewTab4.add(lblTabName4);
            var flxViewTab5 = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "100%",
                "id": "flxViewTab5",
                "isVisible": false,
                "layoutType": kony.flex.FREE_FORM,
                "left": "20dp",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0dp",
                "width": "112px",
                "zIndex": 1
            }, {}, {});
            flxViewTab5.setDefaultUnit(kony.flex.DP);
            var lblTabName5 = new kony.ui.Label({
                "centerY": "50%",
                "height": "100%",
                "id": "lblTabName5",
                "isVisible": true,
                "left": "0dp",
                "skin": "sknlblLato5d6c7f12px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmComapnies.APPROVAL_MATRIX\")",
                "width": "100%",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_CENTER,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {
                "hoverSkin": "sknlblLato0hd9dd99ab6d14aHover"
            });
            flxViewTab5.add(lblTabName5);
            flxViewTabs.add(flxViewTab1, flxViewTab2, flxViewTab3, flxViewTab4, flxViewTab5);
            var flxSeparatorintab = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "1px",
                "id": "flxSeparatorintab",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "isModalContainer": false,
                "right": "0dp",
                "skin": "sknflxSepratorE1E5ED",
                "top": "0dp",
                "zIndex": 1
            }, {}, {});
            flxSeparatorintab.setDefaultUnit(kony.flex.DP);
            flxSeparatorintab.add();
            var flxAccountSegDetails = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                "clipBounds": true,
                "id": "flxAccountSegDetails",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0dp",
                "width": "100%"
            }, {}, {});
            flxAccountSegDetails.setDefaultUnit(kony.flex.DP);
            var flxViewSeperator = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "1px",
                "id": "flxViewSeperator",
                "isVisible": false,
                "layoutType": kony.flex.FREE_FORM,
                "left": "20dp",
                "isModalContainer": false,
                "right": "20dp",
                "skin": "sknflxSepratorE1E5ED",
                "top": "0dp",
                "zIndex": 1
            }, {}, {});
            flxViewSeperator.setDefaultUnit(kony.flex.DP);
            flxViewSeperator.add();
            var flxAccountSegment = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                "clipBounds": true,
                "id": "flxAccountSegment",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "20dp",
                "isModalContainer": false,
                "right": "20dp",
                "skin": "slFbox",
                "top": "10dp"
            }, {}, {});
            flxAccountSegment.setDefaultUnit(kony.flex.DP);
            var flxAccountSegsolepro = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                "clipBounds": true,
                "id": "flxAccountSegsolepro",
                "isVisible": false,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0dp",
                "width": "99%"
            }, {}, {});
            flxAccountSegsolepro.setDefaultUnit(kony.flex.DP);
            var flxAccountsSegSubsidaryPart = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                "clipBounds": true,
                "id": "flxAccountsSegSubsidaryPart",
                "isVisible": true,
                "layoutType": kony.flex.FLOW_VERTICAL,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "CopyslFbox",
                "top": "0dp",
                "width": "100%"
            }, {}, {});
            flxAccountsSegSubsidaryPart.setDefaultUnit(kony.flex.DP);
            var flxCompany = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                "clipBounds": true,
                "id": "flxCompany",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "sknflxfffffborderd7d9e0",
                "top": "80dp",
                "width": "100%",
                "zIndex": 1
            }, {}, {});
            flxCompany.setDefaultUnit(kony.flex.DP);
            var segCompanyDetails = new kony.ui.SegmentedUI2({
                "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                "data": [
                    [{
                            "imgArrow": "arrowdown_1x.png",
                            "lblAccountName": "ACCOUNT NAME",
                            "lblAccountNumber": "ACCOUNT NUMBER",
                            "lblAccountType": "ACCOUNT TYPE",
                            "lblCompanyName": "ABC Private Company",
                            "lblCustomerID": "CUSTOMERID",
                            "lblCustomerIDVal": "12345677687",
                            "lblNoOfAccTitle": "NUMBER OF ACCOUNTS",
                            "lblNoOfAccValue": "12345677687",
                            "lblSeperator": ".",
                            "lblStatus": "STATUS",
                            "lblTaxiDTitle": "TAX ID",
                            "lblTaxiDValue": "12345677687"
                        },
                        [{
                            "labelOne": "Label",
                            "lblAccountName": "Gold saving",
                            "lblAccountNumber": "1234567890123456",
                            "lblAccountType": "Savings",
                            "lblPageCount": "Label",
                            "lblPageGo": "1",
                            "lblSeperator": ".",
                            "lblShowing": "Label",
                            "lblStatus": "Active",
                            "lblStatusicon": "",
                            "lblarrowNext": "",
                            "lblarrowNextLast": "",
                            "lblarrowprevfirst": "Label",
                            "lblarrowprevious": "",
                            "lblsegId": "0",
                            "textpageNo": {
                                "placeholder": "",
                                "text": "01"
                            },
                            "txtPageGo": {
                                "placeholder": "",
                                "text": "01"
                            }
                        }, {
                            "labelOne": "Label",
                            "lblAccountName": "Gold saving",
                            "lblAccountNumber": "1234567890123456",
                            "lblAccountType": "Savings",
                            "lblPageCount": "Label",
                            "lblPageGo": "1",
                            "lblSeperator": ".",
                            "lblShowing": "Label",
                            "lblStatus": "Active",
                            "lblStatusicon": "",
                            "lblarrowNext": "",
                            "lblarrowNextLast": "",
                            "lblarrowprevfirst": "Label",
                            "lblarrowprevious": "",
                            "lblsegId": "0",
                            "textpageNo": {
                                "placeholder": "",
                                "text": "01"
                            },
                            "txtPageGo": {
                                "placeholder": "",
                                "text": "01"
                            }
                        }, {
                            "labelOne": "Label",
                            "lblAccountName": "Gold saving",
                            "lblAccountNumber": "1234567890123456",
                            "lblAccountType": "Savings",
                            "lblPageCount": "Label",
                            "lblPageGo": "1",
                            "lblSeperator": ".",
                            "lblShowing": "Label",
                            "lblStatus": "Active",
                            "lblStatusicon": "",
                            "lblarrowNext": "",
                            "lblarrowNextLast": "",
                            "lblarrowprevfirst": "Label",
                            "lblarrowprevious": "",
                            "lblsegId": "0",
                            "textpageNo": {
                                "placeholder": "",
                                "text": "01"
                            },
                            "txtPageGo": {
                                "placeholder": "",
                                "text": "01"
                            }
                        }, {
                            "labelOne": "1",
                            "lblAccountName": "Gold saving",
                            "lblAccountNumber": "1234567890123456",
                            "lblAccountType": "Savings",
                            "lblPageCount": "Label",
                            "lblPageGo": "1",
                            "lblSeperator": ".",
                            "lblShowing": "Showing  1-10 of 95",
                            "lblStatus": "Active",
                            "lblStatusicon": "",
                            "lblarrowNext": "",
                            "lblarrowNextLast": "",
                            "lblarrowprevfirst": "Label",
                            "lblarrowprevious": "",
                            "lblsegId": "0",
                            "textpageNo": {
                                "placeholder": "01",
                                "text": "01"
                            },
                            "txtPageGo": {
                                "placeholder": "GO",
                                "text": "GO"
                            }
                        }]
                    ]
                ],
                "groupCells": false,
                "id": "segCompanyDetails",
                "isVisible": true,
                "left": "0dp",
                "needPageIndicator": true,
                "pageOffDotImage": "pageoffdot.png",
                "pageOnDotImage": "pageondot.png",
                "retainSelection": false,
                "right": "0dp",
                "rowFocusSkin": "seg2Focus",
                "rowSkin": "seg2Focus",
                "rowTemplate": "flxAccountHeader",
                "sectionHeaderTemplate": "flxCompanydetails",
                "selectionBehavior": constants.SEGUI_DEFAULT_BEHAVIOR,
                "separatorRequired": false,
                "showScrollbars": false,
                "top": "0dp",
                "viewType": constants.SEGUI_VIEW_TYPE_TABLEVIEW,
                "widgetDataMap": {
                    "flxAccountHeader": "flxAccountHeader",
                    "flxArrow": "flxArrow",
                    "flxCompany": "flxCompany",
                    "flxCompanyAccounts": "flxCompanyAccounts",
                    "flxCompanyDetailsAccounts": "flxCompanyDetailsAccounts",
                    "flxCompanyDetailsView": "flxCompanyDetailsView",
                    "flxCompanydetails": "flxCompanydetails",
                    "flxCompanyname": "flxCompanyname",
                    "flxCustomerID": "flxCustomerID",
                    "flxNoOfAccounts": "flxNoOfAccounts",
                    "flxNumber": "flxNumber",
                    "flxPagingContainer": "flxPagingContainer",
                    "flxPagingNumber": "flxPagingNumber",
                    "flxPrevious": "flxPrevious",
                    "flxPreviousfirst": "flxPreviousfirst",
                    "flxSectionHeader": "flxSectionHeader",
                    "flxSeparator": "flxSeparator",
                    "flxStatus": "flxStatus",
                    "flxTaxID": "flxTaxID",
                    "flxnext": "flxnext",
                    "flxnextLast": "flxnextLast",
                    "flxpageGoContainer": "flxpageGoContainer",
                    "flxpageNocontainer": "flxpageNocontainer",
                    "imgArrow": "imgArrow",
                    "labelOne": "labelOne",
                    "lblAccountName": "lblAccountName",
                    "lblAccountNumber": "lblAccountNumber",
                    "lblAccountType": "lblAccountType",
                    "lblCompanyName": "lblCompanyName",
                    "lblCustomerID": "lblCustomerID",
                    "lblCustomerIDVal": "lblCustomerIDVal",
                    "lblNoOfAccTitle": "lblNoOfAccTitle",
                    "lblNoOfAccValue": "lblNoOfAccValue",
                    "lblPageCount": "lblPageCount",
                    "lblPageGo": "lblPageGo",
                    "lblSeperator": "lblSeperator",
                    "lblShowing": "lblShowing",
                    "lblStatus": "lblStatus",
                    "lblStatusicon": "lblStatusicon",
                    "lblTaxiDTitle": "lblTaxiDTitle",
                    "lblTaxiDValue": "lblTaxiDValue",
                    "lblarrowNext": "lblarrowNext",
                    "lblarrowNextLast": "lblarrowNextLast",
                    "lblarrowprevfirst": "lblarrowprevfirst",
                    "lblarrowprevious": "lblarrowprevious",
                    "lblsegId": "lblsegId",
                    "textpageNo": "textpageNo",
                    "txtPageGo": "txtPageGo"
                },
                "width": "100%",
                "zIndex": 1
            }, {
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxCompany.add(segCompanyDetails);
            var flxAccountSep = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "1px",
                "id": "flxAccountSep",
                "isVisible": false,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "isModalContainer": false,
                "right": "0dp",
                "skin": "sknTableHeaderLine",
                "top": "64dp",
                "zIndex": 1
            }, {}, {});
            flxAccountSep.setDefaultUnit(kony.flex.DP);
            flxAccountSep.add();
            flxAccountsSegSubsidaryPart.add(flxCompany, flxAccountSep);
            var flxRequestsdetSearch = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "70dp",
                "id": "flxRequestsdetSearch",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "CopyslFbox",
                "top": "0dp",
                "width": "100%",
                "zIndex": 1
            }, {}, {});
            flxRequestsdetSearch.setDefaultUnit(kony.flex.DP);
            var flxSearchdet = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "100%",
                "id": "flxSearchdet",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "isModalContainer": false,
                "right": "0dp",
                "skin": "CopyslFbox2",
                "top": "0px",
                "width": "470px",
                "zIndex": 1
            }, {}, {});
            flxSearchdet.setDefaultUnit(kony.flex.DP);
            var flxSearchContainerdet = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "centerY": "50%",
                "clipBounds": true,
                "height": "40px",
                "id": "flxSearchContainerdet",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "isModalContainer": false,
                "right": "0dp",
                "skin": "sknflxd5d9ddop100",
                "width": "470px",
                "zIndex": 1
            }, {}, {});
            flxSearchContainerdet.setDefaultUnit(kony.flex.DP);
            var flxClearSearchImagedet = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "100%",
                "id": "flxClearSearchImagedet",
                "isVisible": false,
                "layoutType": kony.flex.FREE_FORM,
                "isModalContainer": false,
                "right": "45px",
                "skin": "sknCursor",
                "top": "0dp",
                "width": "35px",
                "zIndex": 1
            }, {}, {
                "hoverSkin": "sknCursor"
            });
            flxClearSearchImagedet.setDefaultUnit(kony.flex.DP);
            var fontIconCrossdet = new kony.ui.Label({
                "centerY": "50%",
                "id": "fontIconCrossdet",
                "isVisible": true,
                "left": "5px",
                "skin": "sknFontIconSearchCross16px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.userwidgetmodel.lblClearSearch\")",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxClearSearchImagedet.add(fontIconCrossdet);
            var tbxSearchBoxdet = new kony.ui.TextBox2({
                "autoCapitalize": constants.TEXTBOX_AUTO_CAPITALIZE_NONE,
                "centerY": "50%",
                "height": "30px",
                "id": "tbxSearchBoxdet",
                "isVisible": true,
                "keyBoardStyle": constants.TEXTBOX_KEY_BOARD_STYLE_DEFAULT,
                "left": "12dp",
                "onTouchStart": controller.AS_TextField_cfab06859f0f43eca99495891ee290d5,
                "i18n_placeholder": "kony.i18n.getLocalizedString(\"i18n.frmCompanies.searchByCont\")",
                "right": "35px",
                "secureTextEntry": false,
                "skin": "skntbxffffffNoBorderlato35475f14px",
                "textInputMode": constants.TEXTBOX_INPUT_MODE_ANY,
                "width": "380dp",
                "zIndex": 1
            }, {
                "containerHeightMode": constants.TEXTBOX_FONT_METRICS_DRIVEN_HEIGHT,
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [2, 0, 0, 0],
                "paddingInPixel": false
            }, {
                "autoCorrect": false,
                "placeholderSkin": "skntbxffffffNoBorderlatoRegB2BDCB"
            });
            var flxsep = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "bottom": "7dp",
                "clipBounds": true,
                "height": "26dp",
                "id": "flxsep",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "isModalContainer": false,
                "right": "42px",
                "skin": "sknFlxD7D9E0Separator",
                "top": "7dp",
                "width": "2dp",
                "zIndex": 1
            }, {}, {});
            flxsep.setDefaultUnit(kony.flex.DP);
            flxsep.add();
            var flxsearchimg = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "bottom": "7dp",
                "clipBounds": true,
                "height": "26dp",
                "id": "flxsearchimg",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "isModalContainer": false,
                "right": "7px",
                "skin": "sknflxroundedborder003e75",
                "top": "6dp",
                "width": "26dp",
                "zIndex": 1
            }, {}, {});
            flxsearchimg.setDefaultUnit(kony.flex.DP);
            var fontIconSearchImgdet = new kony.ui.Label({
                "centerX": "51%",
                "centerY": "50%",
                "height": "16dp",
                "id": "fontIconSearchImgdet",
                "isVisible": true,
                "skin": "sknFontIconSearchImgfffff",
                "text": "",
                "top": "0dp",
                "width": "16dp",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxsearchimg.add(fontIconSearchImgdet);
            flxSearchContainerdet.add(flxClearSearchImagedet, tbxSearchBoxdet, flxsep, flxsearchimg);
            flxSearchdet.add(flxSearchContainerdet);
            flxRequestsdetSearch.add(flxSearchdet);
            flxAccountSegsolepro.add(flxAccountsSegSubsidaryPart, flxRequestsdetSearch);
            var flxAccountsSegmentPart = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                "clipBounds": true,
                "id": "flxAccountsSegmentPart",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "20dp",
                "width": "100%"
            }, {}, {});
            flxAccountsSegmentPart.setDefaultUnit(kony.flex.DP);
            var flxAccountsHeader = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "65dp",
                "id": "flxAccountsHeader",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "isModalContainer": false,
                "right": "0dp",
                "skin": "slFbox",
                "top": "0dp",
                "zIndex": 1
            }, {}, {});
            flxAccountsHeader.setDefaultUnit(kony.flex.DP);
            var flxAccountType = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "100%",
                "id": "flxAccountType",
                "isVisible": true,
                "layoutType": kony.flex.FLOW_HORIZONTAL,
                "left": "10px",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0dp",
                "width": "25%",
                "zIndex": 1
            }, {}, {
                "hoverSkin": "sknCursor"
            });
            flxAccountType.setDefaultUnit(kony.flex.DP);
            var lblAccountType = new kony.ui.Label({
                "centerY": "50%",
                "id": "lblAccountType",
                "isVisible": true,
                "left": "0px",
                "skin": "sknlblLato696c7312px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmCompanies.ACCOUNTTYPE\")",
                "top": 0,
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {
                "hoverSkin": "sknlblLatoBold00000012px"
            });
            var lblAccountTypeSortIcon = new kony.ui.Label({
                "centerY": "50%",
                "id": "lblAccountTypeSortIcon",
                "isVisible": true,
                "left": "10dp",
                "skin": "sknIcon15px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmAlertsManagement.lblSortName\")",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxAccountType.add(lblAccountType, lblAccountTypeSortIcon);
            var flxAccountName = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "100%",
                "id": "flxAccountName",
                "isVisible": true,
                "layoutType": kony.flex.FLOW_HORIZONTAL,
                "left": "27%",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0dp",
                "width": "24.50%",
                "zIndex": 1
            }, {}, {
                "hoverSkin": "sknCursor"
            });
            flxAccountName.setDefaultUnit(kony.flex.DP);
            var lblAccountName = new kony.ui.Label({
                "centerY": "50%",
                "id": "lblAccountName",
                "isVisible": true,
                "left": "0px",
                "skin": "sknlblLato696c7312px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmCompanies.ACCOUNTNAME\")",
                "top": 0,
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {
                "hoverSkin": "sknlblLatoBold00000012px"
            });
            var lblAccountNameSortIcon = new kony.ui.Label({
                "centerY": "50%",
                "id": "lblAccountNameSortIcon",
                "isVisible": true,
                "left": "10dp",
                "skin": "sknIcon15px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmAlertsManagement.lblSortName\")",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxAccountName.add(lblAccountName, lblAccountNameSortIcon);
            var flxAccountNumber = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "100%",
                "id": "flxAccountNumber",
                "isVisible": true,
                "layoutType": kony.flex.FLOW_HORIZONTAL,
                "left": "53%",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0dp",
                "width": "25%",
                "zIndex": 1
            }, {}, {
                "hoverSkin": "sknCursor"
            });
            flxAccountNumber.setDefaultUnit(kony.flex.DP);
            var lblAccountNumber = new kony.ui.Label({
                "centerY": "50%",
                "id": "lblAccountNumber",
                "isVisible": true,
                "left": "0px",
                "skin": "sknlblLato696c7312px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmCompanies.ACCOUNTNUMBER\")",
                "top": 0,
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {
                "hoverSkin": "sknlblLatoBold00000012px"
            });
            var lblAccountNumberSortIcon = new kony.ui.Label({
                "centerY": "50%",
                "id": "lblAccountNumberSortIcon",
                "isVisible": true,
                "left": "10dp",
                "skin": "sknIcon15px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmAlertsManagement.lblSortName\")",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxAccountNumber.add(lblAccountNumber, lblAccountNumberSortIcon);
            var flxAccountTin = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "100%",
                "id": "flxAccountTin",
                "isVisible": false,
                "layoutType": kony.flex.FLOW_HORIZONTAL,
                "left": "76%",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0dp",
                "width": "19.50%",
                "zIndex": 1
            }, {}, {});
            flxAccountTin.setDefaultUnit(kony.flex.DP);
            var lblAccountTin = new kony.ui.Label({
                "centerY": "50%",
                "id": "lblAccountTin",
                "isVisible": true,
                "left": "0px",
                "skin": "sknlblLato696c7312px",
                "text": "TAX NUMBER",
                "top": 0,
                "width": "100px",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {
                "hoverSkin": "sknlblLatoBold00000012px"
            });
            flxAccountTin.add(lblAccountTin);
            var flxStatus = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "100%",
                "id": "flxStatus",
                "isVisible": true,
                "layoutType": kony.flex.FLOW_HORIZONTAL,
                "left": "80%",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0dp",
                "width": "10%",
                "zIndex": 1
            }, {}, {
                "hoverSkin": "sknCursor"
            });
            flxStatus.setDefaultUnit(kony.flex.DP);
            var lblStatus = new kony.ui.Label({
                "centerY": "50%",
                "id": "lblStatus",
                "isVisible": true,
                "left": "0px",
                "skin": "sknlblLato696c7312px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.permission.STATUS\")",
                "top": 0,
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {
                "hoverSkin": "sknlblLatoBold00000012px"
            });
            var lblStatusSortIcon = new kony.ui.Label({
                "centerY": "50%",
                "id": "lblStatusSortIcon",
                "isVisible": true,
                "left": "10dp",
                "skin": "sknIcon15px",
                "text": "",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxStatus.add(lblStatus, lblStatusSortIcon);
            flxAccountsHeader.add(flxAccountType, flxAccountName, flxAccountNumber, flxAccountTin, flxStatus);
            var flxSegDetails = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                "clipBounds": true,
                "id": "flxSegDetails",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "65dp",
                "width": "100%"
            }, {}, {});
            flxSegDetails.setDefaultUnit(kony.flex.DP);
            var segCompanyDetailAccount = new kony.ui.SegmentedUI2({
                "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                "data": [{
                    "flblUnlink": "kony.i18n.getLocalizedString(\"i18n.frmFAQ.fonticonEdit\")",
                    "fontIconStatus": "",
                    "lblAccountName": "Gold saving",
                    "lblAccountNumber": "1234567890123456",
                    "lblAccountType": "Savings",
                    "lblSeperator": ".",
                    "lblStatus": "Active"
                }, {
                    "flblUnlink": "kony.i18n.getLocalizedString(\"i18n.frmFAQ.fonticonEdit\")",
                    "fontIconStatus": "",
                    "lblAccountName": "Gold saving",
                    "lblAccountNumber": "1234567890123456",
                    "lblAccountType": "Savings",
                    "lblSeperator": ".",
                    "lblStatus": "Active"
                }],
                "groupCells": false,
                "id": "segCompanyDetailAccount",
                "isVisible": true,
                "left": "0dp",
                "needPageIndicator": true,
                "pageOffDotImage": "pageoffdot.png",
                "pageOnDotImage": "pageondot.png",
                "retainSelection": false,
                "right": "0dp",
                "rowFocusSkin": "seg2Focus",
                "rowSkin": "seg2Normal",
                "rowTemplate": "flxCompanyDetailsAccounts",
                "sectionHeaderSkin": "sliPhoneSegmentHeader",
                "selectionBehavior": constants.SEGUI_DEFAULT_BEHAVIOR,
                "separatorRequired": false,
                "showScrollbars": false,
                "top": "0dp",
                "viewType": constants.SEGUI_VIEW_TYPE_TABLEVIEW,
                "widgetDataMap": {
                    "flblUnlink": "flblUnlink",
                    "flxCompanyAccounts": "flxCompanyAccounts",
                    "flxCompanyDetailsAccounts": "flxCompanyDetailsAccounts",
                    "flxStatus": "flxStatus",
                    "flxUnlink": "flxUnlink",
                    "fontIconStatus": "fontIconStatus",
                    "lblAccountName": "lblAccountName",
                    "lblAccountNumber": "lblAccountNumber",
                    "lblAccountType": "lblAccountType",
                    "lblSeperator": "lblSeperator",
                    "lblStatus": "lblStatus"
                },
                "zIndex": 1
            }, {
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxSegDetails.add(segCompanyDetailAccount);
            var flxAccountsHeaderSepartor = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "1px",
                "id": "flxAccountsHeaderSepartor",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "isModalContainer": false,
                "right": "0dp",
                "skin": "sknTableHeaderLine",
                "top": "64dp",
                "zIndex": 1
            }, {}, {});
            flxAccountsHeaderSepartor.setDefaultUnit(kony.flex.DP);
            flxAccountsHeaderSepartor.add();
            flxAccountsSegmentPart.add(flxAccountsHeader, flxSegDetails, flxAccountsHeaderSepartor);
            var flxCustomerIdPart = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                "clipBounds": true,
                "id": "flxCustomerIdPart",
                "isVisible": false,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0dp",
                "width": "18%",
                "zIndex": 1
            }, {}, {});
            flxCustomerIdPart.setDefaultUnit(kony.flex.DP);
            var flxAccCustomerIdHeader = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "65dp",
                "id": "flxAccCustomerIdHeader",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0px",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0dp",
                "width": "100%",
                "zIndex": 1
            }, {}, {
                "hoverSkin": "sknCursor"
            });
            flxAccCustomerIdHeader.setDefaultUnit(kony.flex.DP);
            var lblAccountsCustHeading = new kony.ui.Label({
                "centerY": "50%",
                "id": "lblAccountsCustHeading",
                "isVisible": true,
                "left": "10px",
                "skin": "sknlblLato696c7312px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmCustomerManagementController.CUSTOMER_ID\")",
                "top": 0,
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {
                "hoverSkin": "sknlblLatoBold00000012px"
            });
            var flxAccountsCustLine = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "bottom": "0dp",
                "clipBounds": true,
                "height": "1px",
                "id": "flxAccountsCustLine",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "isModalContainer": false,
                "right": "0dp",
                "skin": "sknTableHeaderLine",
                "zIndex": 1
            }, {}, {});
            flxAccountsCustLine.setDefaultUnit(kony.flex.DP);
            flxAccountsCustLine.add();
            flxAccCustomerIdHeader.add(lblAccountsCustHeading, flxAccountsCustLine);
            var flxAccountsCustRow = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "50dp",
                "id": "flxAccountsCustRow",
                "isVisible": false,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "75dp",
                "width": "100%",
                "zIndex": 1
            }, {}, {});
            flxAccountsCustRow.setDefaultUnit(kony.flex.DP);
            var lblCustomerId = new kony.ui.Label({
                "centerY": "50%",
                "id": "lblCustomerId",
                "isVisible": true,
                "left": "10dp",
                "skin": "sknLbl485C75LatoRegular13Px",
                "text": "1234678",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var lblSeperatorLine = new kony.ui.Label({
                "bottom": "0dp",
                "height": "1dp",
                "id": "lblSeperatorLine",
                "isVisible": true,
                "left": "0dp",
                "right": "0dp",
                "skin": "sknLblD5D9DD1000",
                "text": "Label",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxAccountsCustRow.add(lblCustomerId, lblSeperatorLine);
            var flxAccountCustVerticalLine = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                "bottom": "0dp",
                "clipBounds": true,
                "id": "flxAccountCustVerticalLine",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "isModalContainer": false,
                "right": "0%",
                "skin": "sknflxd5d9ddop100",
                "top": "65dp",
                "width": "1dp",
                "zIndex": 1
            }, {}, {});
            flxAccountCustVerticalLine.setDefaultUnit(kony.flex.DP);
            flxAccountCustVerticalLine.add();
            var flxAccCustomerIdColumn = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                "clipBounds": true,
                "id": "flxAccCustomerIdColumn",
                "isVisible": true,
                "layoutType": kony.flex.FLOW_VERTICAL,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "65dp",
                "width": "100%",
                "zIndex": 1
            }, {}, {});
            flxAccCustomerIdColumn.setDefaultUnit(kony.flex.DP);
            flxAccCustomerIdColumn.add();
            flxCustomerIdPart.add(flxAccCustomerIdHeader, flxAccountsCustRow, flxAccountCustVerticalLine, flxAccCustomerIdColumn);
            var flxRequestsdetSearchAcc = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "70dp",
                "id": "flxRequestsdetSearchAcc",
                "isVisible": false,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "CopyslFbox",
                "top": "10dp",
                "width": "100%",
                "zIndex": 1
            }, {}, {});
            flxRequestsdetSearchAcc.setDefaultUnit(kony.flex.DP);
            var flxSearchdetAcc = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "100%",
                "id": "flxSearchdetAcc",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "isModalContainer": false,
                "right": "5dp",
                "skin": "CopyslFbox2",
                "top": "0px",
                "width": "470px",
                "zIndex": 1
            }, {}, {});
            flxSearchdetAcc.setDefaultUnit(kony.flex.DP);
            var flxSearchContainerdetAcc = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "centerY": "50%",
                "clipBounds": true,
                "height": "40px",
                "id": "flxSearchContainerdetAcc",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "isModalContainer": false,
                "right": "0dp",
                "skin": "sknflxd5d9ddop100",
                "width": "470px",
                "zIndex": 1
            }, {}, {});
            flxSearchContainerdetAcc.setDefaultUnit(kony.flex.DP);
            var flxClearSearchImagedetAcc = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "100%",
                "id": "flxClearSearchImagedetAcc",
                "isVisible": false,
                "layoutType": kony.flex.FREE_FORM,
                "isModalContainer": false,
                "right": "0px",
                "skin": "sknCursor",
                "top": "0dp",
                "width": "35px",
                "zIndex": 1
            }, {}, {
                "hoverSkin": "sknCursor"
            });
            flxClearSearchImagedetAcc.setDefaultUnit(kony.flex.DP);
            var fontIconCrossdetAcc = new kony.ui.Label({
                "centerY": "50%",
                "id": "fontIconCrossdetAcc",
                "isVisible": true,
                "left": "5px",
                "skin": "sknFontIconSearchCross16px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.userwidgetmodel.lblClearSearch\")",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxClearSearchImagedetAcc.add(fontIconCrossdetAcc);
            var tbxSearchBoxdetAcc = new kony.ui.TextBox2({
                "autoCapitalize": constants.TEXTBOX_AUTO_CAPITALIZE_NONE,
                "centerY": "50%",
                "height": "30px",
                "id": "tbxSearchBoxdetAcc",
                "isVisible": true,
                "keyBoardStyle": constants.TEXTBOX_KEY_BOARD_STYLE_DEFAULT,
                "onTouchStart": controller.AS_TextField_cfab06859f0f43eca99495891ee290d5,
                "placeholder": "Search by Account Number, Account Name",
                "right": "30px",
                "secureTextEntry": false,
                "skin": "skntbxffffffNoBorderlato35475f14px",
                "textInputMode": constants.TEXTBOX_INPUT_MODE_ANY,
                "width": "405dp",
                "zIndex": 1
            }, {
                "containerHeightMode": constants.TEXTBOX_FONT_METRICS_DRIVEN_HEIGHT,
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [2, 0, 0, 0],
                "paddingInPixel": false
            }, {
                "autoCorrect": false,
                "placeholderSkin": "skntbxffffffNoBorderlatoRegB2BDCB"
            });
            var fontIconSearchImgdetAcc = new kony.ui.Label({
                "centerY": "50%",
                "id": "fontIconSearchImgdetAcc",
                "isVisible": true,
                "left": "15px",
                "skin": "sknFontIconSearchImg20px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.userwidgetmodel.lblSearchIcon\")",
                "top": "0dp",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxSearchContainerdetAcc.add(flxClearSearchImagedetAcc, tbxSearchBoxdetAcc, fontIconSearchImgdetAcc);
            flxSearchdetAcc.add(flxSearchContainerdetAcc);
            flxRequestsdetSearchAcc.add(flxSearchdetAcc);
            var flxNoAccountResults = new kony.ui.FlexContainer({
                "clipBounds": true,
                "height": "220dp",
                "id": "flxNoAccountResults",
                "isVisible": false,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "65dp",
                "width": "100%",
                "zIndex": 1
            }, {}, {});
            flxNoAccountResults.setDefaultUnit(kony.flex.DP);
            var lblNoAccountResults = new kony.ui.Label({
                "centerX": "505dp",
                "centerY": "50%",
                "id": "lblNoAccountResults",
                "isVisible": true,
                "skin": "sknLblLato84939E12px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmLogs.rtxNoResultsFound\")",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_CENTER,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxNoAccountResults.add(lblNoAccountResults);
            var flxAccountStatusFilter = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                "clipBounds": true,
                "id": "flxAccountStatusFilter",
                "isVisible": false,
                "layoutType": kony.flex.FREE_FORM,
                "left": "590dp",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "37px",
                "width": "120px",
                "zIndex": 5
            }, {}, {});
            flxAccountStatusFilter.setDefaultUnit(kony.flex.DP);
            var accountStatusFilterMenu = new com.adminConsole.common.statusFilterMenu({
                "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                "id": "accountStatusFilterMenu",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "masterType": constants.MASTER_TYPE_DEFAULT,
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "5dp",
                "width": "100%",
                "zIndex": 5,
                "overrides": {
                    "imgUpArrow": {
                        "src": "uparrow_2x.png"
                    },
                    "segStatusFilterDropdown": {
                        "left": "0dp",
                        "top": "5dp"
                    }
                }
            }, {
                "overrides": {}
            }, {
                "overrides": {}
            });
            flxAccountStatusFilter.add(accountStatusFilterMenu);
            flxAccountSegment.add(flxAccountSegsolepro, flxAccountsSegmentPart, flxCustomerIdPart, flxRequestsdetSearchAcc, flxNoAccountResults, flxAccountStatusFilter);
            flxAccountSegDetails.add(flxViewSeperator, flxAccountSegment);
            var flxCompanyDetailsFeaturesContainer = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                "bottom": "0dp",
                "clipBounds": true,
                "id": "flxCompanyDetailsFeaturesContainer",
                "isVisible": false,
                "layoutType": kony.flex.FLOW_VERTICAL,
                "left": "0dp",
                "isModalContainer": false,
                "right": "0dp",
                "skin": "slFbox",
                "top": "0",
                "width": "100%"
            }, {}, {});
            flxCompanyDetailsFeaturesContainer.setDefaultUnit(kony.flex.DP);
            var flxFeaturesTabs = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "50px",
                "id": "flxFeaturesTabs",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0px",
                "width": "100%",
                "zIndex": 1
            }, {}, {});
            flxFeaturesTabs.setDefaultUnit(kony.flex.DP);
            var flxFeaturesTabWrapper = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "50px",
                "id": "flxFeaturesTabWrapper",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0px",
                "isModalContainer": false,
                "right": "0px",
                "skin": "sknBgE1E5EDLeftRightCorner3px",
                "top": "0dp",
                "zIndex": 1
            }, {}, {});
            flxFeaturesTabWrapper.setDefaultUnit(kony.flex.DP);
            var btnMandatoryFeatures = new kony.ui.Button({
                "bottom": "0dp",
                "height": "40px",
                "id": "btnMandatoryFeatures",
                "isVisible": true,
                "left": "20px",
                "skin": "sknbtnBgffffffLato485c75Radius3Px12Px",
                "text": "MANDATORY FEATURES",
                "width": "170px",
                "zIndex": 2
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_CENTER,
                "displayText": true,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var btnAdditionalFeatures = new kony.ui.Button({
                "bottom": "0dp",
                "height": "40px",
                "id": "btnAdditionalFeatures",
                "isVisible": true,
                "left": "200px",
                "skin": "sknbtnBgf5f6f8Lato485c75Radius3Px12Px",
                "text": "ADDITIONAL FEATURES",
                "width": "170px",
                "zIndex": 2
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_CENTER,
                "displayText": true,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxFeaturesTabWrapper.add(btnMandatoryFeatures, btnAdditionalFeatures);
            flxFeaturesTabs.add(flxFeaturesTabWrapper);
            var flxViewAdditionalFeature = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                "clipBounds": true,
                "id": "flxViewAdditionalFeature",
                "isVisible": true,
                "layoutType": kony.flex.FLOW_VERTICAL,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0dp",
                "width": "100%"
            }, {}, {});
            flxViewAdditionalFeature.setDefaultUnit(kony.flex.DP);
            var btnShowSuspendFeature = new kony.ui.Button({
                "height": "22px",
                "id": "btnShowSuspendFeature",
                "isVisible": true,
                "right": "20px",
                "skin": "sknbtnf7f7faLatoRegular12Px485c75Border1px485C75Radius20px",
                "text": "Suspend Features",
                "top": "15px",
                "width": "120px",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_CENTER,
                "displayText": true,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {
                "hoverSkin": "sknbtnffffffLatoRegular12Px485c75Border1px485C75Radius20px"
            });
            var segCompanyFeatures = new kony.ui.SegmentedUI2({
                "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                "data": [{
                    "lblFeatureName": "Transfer to account in other FIs",
                    "lblViewDetails": "kony.i18n.getLocalizedString(\"i18n.frmGroupsController.viewDetails\")",
                    "statusIcon": "kony.i18n.getLocalizedString(\"i18n.frmAlertsManagement.lblViewAlertStatusKey\")",
                    "statusValue": "kony.i18n.getLocalizedString(\"i18n.secureimage.Active\")"
                }, {
                    "lblFeatureName": "Transfer to account in other FIs",
                    "lblViewDetails": "kony.i18n.getLocalizedString(\"i18n.frmGroupsController.viewDetails\")",
                    "statusIcon": "kony.i18n.getLocalizedString(\"i18n.frmAlertsManagement.lblViewAlertStatusKey\")",
                    "statusValue": "kony.i18n.getLocalizedString(\"i18n.secureimage.Active\")"
                }, {
                    "lblFeatureName": "Transfer to account in other FIs",
                    "lblViewDetails": "kony.i18n.getLocalizedString(\"i18n.frmGroupsController.viewDetails\")",
                    "statusIcon": "kony.i18n.getLocalizedString(\"i18n.frmAlertsManagement.lblViewAlertStatusKey\")",
                    "statusValue": "kony.i18n.getLocalizedString(\"i18n.secureimage.Active\")"
                }, {
                    "lblFeatureName": "Transfer to account in other FIs",
                    "lblViewDetails": "kony.i18n.getLocalizedString(\"i18n.frmGroupsController.viewDetails\")",
                    "statusIcon": "kony.i18n.getLocalizedString(\"i18n.frmAlertsManagement.lblViewAlertStatusKey\")",
                    "statusValue": "kony.i18n.getLocalizedString(\"i18n.secureimage.Active\")"
                }, {
                    "lblFeatureName": "Transfer to account in other FIs",
                    "lblViewDetails": "kony.i18n.getLocalizedString(\"i18n.frmGroupsController.viewDetails\")",
                    "statusIcon": "kony.i18n.getLocalizedString(\"i18n.frmAlertsManagement.lblViewAlertStatusKey\")",
                    "statusValue": "kony.i18n.getLocalizedString(\"i18n.secureimage.Active\")"
                }, {
                    "lblFeatureName": "Transfer to account in other FIs",
                    "lblViewDetails": "kony.i18n.getLocalizedString(\"i18n.frmGroupsController.viewDetails\")",
                    "statusIcon": "kony.i18n.getLocalizedString(\"i18n.frmAlertsManagement.lblViewAlertStatusKey\")",
                    "statusValue": "kony.i18n.getLocalizedString(\"i18n.secureimage.Active\")"
                }, {
                    "lblFeatureName": "Transfer to account in other FIs",
                    "lblViewDetails": "kony.i18n.getLocalizedString(\"i18n.frmGroupsController.viewDetails\")",
                    "statusIcon": "kony.i18n.getLocalizedString(\"i18n.frmAlertsManagement.lblViewAlertStatusKey\")",
                    "statusValue": "kony.i18n.getLocalizedString(\"i18n.secureimage.Active\")"
                }, {
                    "lblFeatureName": "Transfer to account in other FIs",
                    "lblViewDetails": "kony.i18n.getLocalizedString(\"i18n.frmGroupsController.viewDetails\")",
                    "statusIcon": "kony.i18n.getLocalizedString(\"i18n.frmAlertsManagement.lblViewAlertStatusKey\")",
                    "statusValue": "kony.i18n.getLocalizedString(\"i18n.secureimage.Active\")"
                }, {
                    "lblFeatureName": "Transfer to account in other FIs",
                    "lblViewDetails": "kony.i18n.getLocalizedString(\"i18n.frmGroupsController.viewDetails\")",
                    "statusIcon": "kony.i18n.getLocalizedString(\"i18n.frmAlertsManagement.lblViewAlertStatusKey\")",
                    "statusValue": "kony.i18n.getLocalizedString(\"i18n.secureimage.Active\")"
                }, {
                    "lblFeatureName": "Transfer to account in other FIs",
                    "lblViewDetails": "kony.i18n.getLocalizedString(\"i18n.frmGroupsController.viewDetails\")",
                    "statusIcon": "kony.i18n.getLocalizedString(\"i18n.frmAlertsManagement.lblViewAlertStatusKey\")",
                    "statusValue": "kony.i18n.getLocalizedString(\"i18n.secureimage.Active\")"
                }],
                "groupCells": false,
                "id": "segCompanyFeatures",
                "isVisible": true,
                "left": "0",
                "needPageIndicator": true,
                "pageOffDotImage": "pageoffdot.png",
                "pageOnDotImage": "pageondot.png",
                "retainSelection": false,
                "rowFocusSkin": "seg2Focus",
                "rowSkin": "seg2Normal",
                "rowTemplate": "flxCompanyFeatures",
                "sectionHeaderSkin": "sliPhoneSegmentHeader",
                "selectionBehavior": constants.SEGUI_DEFAULT_BEHAVIOR,
                "separatorRequired": false,
                "showScrollbars": false,
                "top": 15,
                "viewType": constants.SEGUI_VIEW_TYPE_TABLEVIEW,
                "widgetDataMap": {
                    "flxCompanyFeatures": "flxCompanyFeatures",
                    "flxHeader": "flxHeader",
                    "flxLeft": "flxLeft",
                    "flxRight": "flxRight",
                    "flxTopRow1": "flxTopRow1",
                    "flxViewDetails": "flxViewDetails",
                    "lblFeatureName": "lblFeatureName",
                    "lblViewDetails": "lblViewDetails",
                    "statusIcon": "statusIcon",
                    "statusValue": "statusValue"
                },
                "width": "100%"
            }, {
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var flxNoAdditionalFeatures = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "bottom": "0dp",
                "clipBounds": true,
                "height": "220px",
                "id": "flxNoAdditionalFeatures",
                "isVisible": false,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0",
                "isModalContainer": false,
                "skin": "slFbox",
                "width": "100%",
                "zIndex": 2
            }, {}, {});
            flxNoAdditionalFeatures.setDefaultUnit(kony.flex.DP);
            var lblNoAdditionalFeatures = new kony.ui.Label({
                "centerX": "50%",
                "centerY": "50%",
                "id": "lblNoAdditionalFeatures",
                "isVisible": true,
                "skin": "sknIcon485C7513px",
                "text": "There are no additional features associated to the company",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxNoAdditionalFeatures.add(lblNoAdditionalFeatures);
            flxViewAdditionalFeature.add(btnShowSuspendFeature, segCompanyFeatures, flxNoAdditionalFeatures);
            var flxViewMandatoryFeature = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                "clipBounds": true,
                "id": "flxViewMandatoryFeature",
                "isVisible": false,
                "layoutType": kony.flex.FLOW_VERTICAL,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0dp",
                "width": "100%"
            }, {}, {});
            flxViewMandatoryFeature.setDefaultUnit(kony.flex.DP);
            var segCompanyMandatoryFeatures = new kony.ui.SegmentedUI2({
                "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                "data": [{
                    "lblFeatureName": "Transfer to account in other FIs",
                    "lblViewDetails": "kony.i18n.getLocalizedString(\"i18n.frmGroupsController.viewDetails\")",
                    "statusIcon": "kony.i18n.getLocalizedString(\"i18n.frmAlertsManagement.lblViewAlertStatusKey\")",
                    "statusValue": "kony.i18n.getLocalizedString(\"i18n.secureimage.Active\")"
                }, {
                    "lblFeatureName": "Transfer to account in other FIs",
                    "lblViewDetails": "kony.i18n.getLocalizedString(\"i18n.frmGroupsController.viewDetails\")",
                    "statusIcon": "kony.i18n.getLocalizedString(\"i18n.frmAlertsManagement.lblViewAlertStatusKey\")",
                    "statusValue": "kony.i18n.getLocalizedString(\"i18n.secureimage.Active\")"
                }, {
                    "lblFeatureName": "Transfer to account in other FIs",
                    "lblViewDetails": "kony.i18n.getLocalizedString(\"i18n.frmGroupsController.viewDetails\")",
                    "statusIcon": "kony.i18n.getLocalizedString(\"i18n.frmAlertsManagement.lblViewAlertStatusKey\")",
                    "statusValue": "kony.i18n.getLocalizedString(\"i18n.secureimage.Active\")"
                }, {
                    "lblFeatureName": "Transfer to account in other FIs",
                    "lblViewDetails": "kony.i18n.getLocalizedString(\"i18n.frmGroupsController.viewDetails\")",
                    "statusIcon": "kony.i18n.getLocalizedString(\"i18n.frmAlertsManagement.lblViewAlertStatusKey\")",
                    "statusValue": "kony.i18n.getLocalizedString(\"i18n.secureimage.Active\")"
                }, {
                    "lblFeatureName": "Transfer to account in other FIs",
                    "lblViewDetails": "kony.i18n.getLocalizedString(\"i18n.frmGroupsController.viewDetails\")",
                    "statusIcon": "kony.i18n.getLocalizedString(\"i18n.frmAlertsManagement.lblViewAlertStatusKey\")",
                    "statusValue": "kony.i18n.getLocalizedString(\"i18n.secureimage.Active\")"
                }, {
                    "lblFeatureName": "Transfer to account in other FIs",
                    "lblViewDetails": "kony.i18n.getLocalizedString(\"i18n.frmGroupsController.viewDetails\")",
                    "statusIcon": "kony.i18n.getLocalizedString(\"i18n.frmAlertsManagement.lblViewAlertStatusKey\")",
                    "statusValue": "kony.i18n.getLocalizedString(\"i18n.secureimage.Active\")"
                }, {
                    "lblFeatureName": "Transfer to account in other FIs",
                    "lblViewDetails": "kony.i18n.getLocalizedString(\"i18n.frmGroupsController.viewDetails\")",
                    "statusIcon": "kony.i18n.getLocalizedString(\"i18n.frmAlertsManagement.lblViewAlertStatusKey\")",
                    "statusValue": "kony.i18n.getLocalizedString(\"i18n.secureimage.Active\")"
                }, {
                    "lblFeatureName": "Transfer to account in other FIs",
                    "lblViewDetails": "kony.i18n.getLocalizedString(\"i18n.frmGroupsController.viewDetails\")",
                    "statusIcon": "kony.i18n.getLocalizedString(\"i18n.frmAlertsManagement.lblViewAlertStatusKey\")",
                    "statusValue": "kony.i18n.getLocalizedString(\"i18n.secureimage.Active\")"
                }, {
                    "lblFeatureName": "Transfer to account in other FIs",
                    "lblViewDetails": "kony.i18n.getLocalizedString(\"i18n.frmGroupsController.viewDetails\")",
                    "statusIcon": "kony.i18n.getLocalizedString(\"i18n.frmAlertsManagement.lblViewAlertStatusKey\")",
                    "statusValue": "kony.i18n.getLocalizedString(\"i18n.secureimage.Active\")"
                }, {
                    "lblFeatureName": "Transfer to account in other FIs",
                    "lblViewDetails": "kony.i18n.getLocalizedString(\"i18n.frmGroupsController.viewDetails\")",
                    "statusIcon": "kony.i18n.getLocalizedString(\"i18n.frmAlertsManagement.lblViewAlertStatusKey\")",
                    "statusValue": "kony.i18n.getLocalizedString(\"i18n.secureimage.Active\")"
                }],
                "groupCells": false,
                "id": "segCompanyMandatoryFeatures",
                "isVisible": true,
                "left": "0",
                "needPageIndicator": true,
                "pageOffDotImage": "pageoffdot.png",
                "pageOnDotImage": "pageondot.png",
                "retainSelection": false,
                "rowFocusSkin": "seg2Focus",
                "rowSkin": "seg2Normal",
                "rowTemplate": "flxCompanyFeatures",
                "sectionHeaderSkin": "sliPhoneSegmentHeader",
                "selectionBehavior": constants.SEGUI_DEFAULT_BEHAVIOR,
                "separatorRequired": false,
                "showScrollbars": false,
                "top": "15dp",
                "viewType": constants.SEGUI_VIEW_TYPE_TABLEVIEW,
                "widgetDataMap": {
                    "flxCompanyFeatures": "flxCompanyFeatures",
                    "flxHeader": "flxHeader",
                    "flxLeft": "flxLeft",
                    "flxRight": "flxRight",
                    "flxTopRow1": "flxTopRow1",
                    "flxViewDetails": "flxViewDetails",
                    "lblFeatureName": "lblFeatureName",
                    "lblViewDetails": "lblViewDetails",
                    "statusIcon": "statusIcon",
                    "statusValue": "statusValue"
                },
                "width": "100%"
            }, {
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxViewMandatoryFeature.add(segCompanyMandatoryFeatures);
            var flxFeatureSuspensionContainer = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                "bottom": "0dp",
                "clipBounds": true,
                "id": "flxFeatureSuspensionContainer",
                "isVisible": false,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0",
                "width": "100%",
                "zIndex": 2
            }, {}, {});
            flxFeatureSuspensionContainer.setDefaultUnit(kony.flex.DP);
            var flxFeatureSuspensionHeader = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "96px",
                "id": "flxFeatureSuspensionHeader",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "20dp",
                "isModalContainer": false,
                "right": "20dp",
                "skin": "slFbox",
                "top": "0dp",
                "zIndex": 1
            }, {}, {});
            flxFeatureSuspensionHeader.setDefaultUnit(kony.flex.DP);
            var backToFeautresList = new com.adminConsole.customerMang.backToPageHeader({
                "height": "20px",
                "id": "backToFeautresList",
                "isVisible": true,
                "layoutType": kony.flex.FLOW_HORIZONTAL,
                "left": "0dp",
                "masterType": constants.MASTER_TYPE_DEFAULT,
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "10dp",
                "width": "100%",
                "overrides": {
                    "backToPageHeader": {
                        "top": "10dp"
                    },
                    "btnBack": {
                        "text": "Back"
                    },
                    "flxBack": {
                        "left": "0px"
                    }
                }
            }, {
                "overrides": {}
            }, {
                "overrides": {}
            });
            var lblSuspendFeatures = new kony.ui.Label({
                "height": "20px",
                "id": "lblSuspendFeatures",
                "isVisible": true,
                "left": "0dp",
                "skin": "sknLatoRegular192B4518px",
                "text": "Suspend Features",
                "top": "45dp",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var flxSuspendedFeatureContainer = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                "clipBounds": true,
                "id": "flxSuspendedFeatureContainer",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "80dp",
                "width": "100%",
                "zIndex": 1
            }, {}, {});
            flxSuspendedFeatureContainer.setDefaultUnit(kony.flex.DP);
            var lblSuspendedFeatures = new kony.ui.Label({
                "height": "15px",
                "id": "lblSuspendedFeatures",
                "isVisible": true,
                "left": "0dp",
                "skin": "sknLbl485C75LatoRegular13Px",
                "text": "Suspended:",
                "top": "0dp",
                "width": "68px",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var lblSuspendedFeaturesSelectedCount = new kony.ui.Label({
                "height": "15px",
                "id": "lblSuspendedFeaturesSelectedCount",
                "isVisible": true,
                "left": "70dp",
                "skin": "sknLbl192b45LatoBold13px",
                "text": "02",
                "top": "0dp",
                "width": "16px",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var lblSuspendedFeaturesTotalCount = new kony.ui.Label({
                "height": "15px",
                "id": "lblSuspendedFeaturesTotalCount",
                "isVisible": true,
                "left": "88dp",
                "skin": "sknLbl485C75LatoRegular13Px",
                "text": "of 05",
                "top": "0dp",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxSuspendedFeatureContainer.add(lblSuspendedFeatures, lblSuspendedFeaturesSelectedCount, lblSuspendedFeaturesTotalCount);
            flxFeatureSuspensionHeader.add(backToFeautresList, lblSuspendFeatures, flxSuspendedFeatureContainer);
            var flxSegSuspendedContainer = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "bottom": "65dp",
                "clipBounds": true,
                "height": "230dp",
                "id": "flxSegSuspendedContainer",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "20dp",
                "isModalContainer": false,
                "right": "20dp",
                "skin": "slFbox",
                "top": "111dp",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {}, {});
            flxSegSuspendedContainer.setDefaultUnit(kony.flex.DP);
            var segSuspendedFeature = new kony.ui.SegmentedUI2({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "data": [{
                    "lblFeature": "Transfer to account in other FIs",
                    "lblViewFeatureDetails": "View Details"
                }, {
                    "lblFeature": "Transfer to account in other FIs",
                    "lblViewFeatureDetails": "View Details"
                }, {
                    "lblFeature": "Transfer to account in other FIs",
                    "lblViewFeatureDetails": "View Details"
                }, {
                    "lblFeature": "Transfer to account in other FIs",
                    "lblViewFeatureDetails": "View Details"
                }, {
                    "lblFeature": "Transfer to account in other FIs",
                    "lblViewFeatureDetails": "View Details"
                }, {
                    "lblFeature": "Transfer to account in other FIs",
                    "lblViewFeatureDetails": "View Details"
                }, {
                    "lblFeature": "Transfer to account in other FIs",
                    "lblViewFeatureDetails": "View Details"
                }, {
                    "lblFeature": "Transfer to account in other FIs",
                    "lblViewFeatureDetails": "View Details"
                }, {
                    "lblFeature": "Transfer to account in other FIs",
                    "lblViewFeatureDetails": "View Details"
                }, {
                    "lblFeature": "Transfer to account in other FIs",
                    "lblViewFeatureDetails": "View Details"
                }],
                "groupCells": false,
                "height": "100%",
                "id": "segSuspendedFeature",
                "isVisible": true,
                "left": "0dp",
                "needPageIndicator": true,
                "pageOffDotImage": "pageoffdot.png",
                "pageOnDotImage": "pageondot.png",
                "retainSelection": false,
                "rowFocusSkin": "seg2Focus",
                "rowSkin": "seg2Normal",
                "rowTemplate": "flxSuspendFeature",
                "sectionHeaderSkin": "sliPhoneSegmentHeader",
                "selectionBehavior": constants.SEGUI_DEFAULT_BEHAVIOR,
                "separatorColor": "ffffff00",
                "separatorRequired": true,
                "separatorThickness": 5,
                "showScrollbars": false,
                "top": "0dp",
                "viewType": constants.SEGUI_VIEW_TYPE_TABLEVIEW,
                "widgetDataMap": {
                    "flxFeatureUpper": "flxFeatureUpper",
                    "flxFeaturesContainer": "flxFeaturesContainer",
                    "flxFeaturesLower": "flxFeaturesLower",
                    "flxSuspendFeature": "flxSuspendFeature",
                    "flxViewDetails": "flxViewDetails",
                    "lblFeature": "lblFeature",
                    "lblViewFeatureDetails": "lblViewFeatureDetails"
                },
                "width": "100%",
                "zIndex": 1
            }, {
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxSegSuspendedContainer.add(segSuspendedFeature);
            var flxCommonButtonsFeatureSuspend = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "bottom": "-15dp",
                "clipBounds": true,
                "height": "80dp",
                "id": "flxCommonButtonsFeatureSuspend",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "sknFlxFFFFFF100O",
                "width": "100%",
                "zIndex": 1
            }, {}, {});
            flxCommonButtonsFeatureSuspend.setDefaultUnit(kony.flex.DP);
            var flxCommonButtonsFeatureSuspendSeprator = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "1dp",
                "id": "flxCommonButtonsFeatureSuspendSeprator",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "isModalContainer": false,
                "right": "0dp",
                "skin": "sknflxe1e5edop100",
                "top": "0dp",
                "zIndex": 1
            }, {}, {});
            flxCommonButtonsFeatureSuspendSeprator.setDefaultUnit(kony.flex.DP);
            flxCommonButtonsFeatureSuspendSeprator.add();
            var commonButtonsFeatureSuspend = new com.adminConsole.common.commonButtons({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "centerY": "50%",
                "height": "80px",
                "id": "commonButtonsFeatureSuspend",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "masterType": constants.MASTER_TYPE_DEFAULT,
                "isModalContainer": false,
                "skin": "slFbox",
                "width": "100%",
                "overrides": {
                    "btnCancel": {
                        "left": "viz.val_cleared",
                        "right": "140px"
                    },
                    "btnNext": {
                        "isVisible": false,
                        "left": "viz.val_cleared",
                        "right": "20dp",
                        "width": "100dp"
                    },
                    "btnSave": {
                        "i18n_text": "kony.i18n.getLocalizedString(\"i18n.permission.SAVE\")",
                        "isVisible": true,
                        "right": "20dp"
                    },
                    "commonButtons": {
                        "bottom": "viz.val_cleared",
                        "centerY": "50%",
                        "top": "viz.val_cleared"
                    },
                    "flxRightButtons": {
                        "width": "140px"
                    }
                }
            }, {
                "overrides": {}
            }, {
                "overrides": {}
            });
            flxCommonButtonsFeatureSuspend.add(flxCommonButtonsFeatureSuspendSeprator, commonButtonsFeatureSuspend);
            flxFeatureSuspensionContainer.add(flxFeatureSuspensionHeader, flxSegSuspendedContainer, flxCommonButtonsFeatureSuspend);
            flxCompanyDetailsFeaturesContainer.add(flxFeaturesTabs, flxViewAdditionalFeature, flxViewMandatoryFeature, flxFeatureSuspensionContainer);
            flxDetailContainer.add(flxMainDetails, flxViewTabs, flxSeparatorintab, flxAccountSegDetails, flxCompanyDetailsFeaturesContainer);
            var flxTaxIdTooltip = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                "clipBounds": true,
                "id": "flxTaxIdTooltip",
                "isVisible": false,
                "layoutType": kony.flex.FLOW_VERTICAL,
                "left": "33%",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "135px",
                "width": "150px",
                "zIndex": 10
            }, {}, {});
            flxTaxIdTooltip.setDefaultUnit(kony.flex.DP);
            var flxUpArrowImage = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "12px",
                "id": "flxUpArrowImage",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0px",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0px",
                "width": "100%",
                "zIndex": 2
            }, {}, {});
            flxUpArrowImage.setDefaultUnit(kony.flex.DP);
            var imgUpArrow = new kony.ui.Image2({
                "height": "12dp",
                "id": "imgUpArrow",
                "isVisible": true,
                "left": "10dp",
                "skin": "slImage",
                "src": "uparrow_2x.png",
                "top": "0dp",
                "width": "15dp",
                "zIndex": 1
            }, {
                "imageScaleMode": constants.IMAGE_SCALE_MODE_FIT_TO_DIMENSIONS,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxUpArrowImage.add(imgUpArrow);
            var flxTooltipContainer = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                "clipBounds": true,
                "id": "flxTooltipContainer",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "sknflxffffffop100dbdbe6Radius3px",
                "top": "-1dp",
                "width": "100%",
                "zIndex": 1
            }, {}, {});
            flxTooltipContainer.setDefaultUnit(kony.flex.DP);
            var segTaxId = new kony.ui.SegmentedUI2({
                "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                "bottom": 10,
                "data": [{
                    "lblName": "Granville Street, Vancover, BC, Canada"
                }, {
                    "lblName": "Granville Street, Vancover, BC, Canada"
                }, {
                    "lblName": "Granville Street, Vancover, BC, Canada"
                }],
                "groupCells": false,
                "id": "segTaxId",
                "isVisible": true,
                "left": "15dp",
                "needPageIndicator": true,
                "pageOffDotImage": "pageoffdot.png",
                "pageOnDotImage": "pageondot.png",
                "retainSelection": false,
                "right": "15px",
                "rowFocusSkin": "seg2Focus",
                "rowSkin": "seg2Normal",
                "rowTemplate": "flxMore",
                "sectionHeaderSkin": "sliPhoneSegmentHeader",
                "selectionBehavior": constants.SEGUI_DEFAULT_BEHAVIOR,
                "separatorColor": "aaaaaa00",
                "separatorRequired": false,
                "separatorThickness": 0,
                "showScrollbars": false,
                "top": "0dp",
                "viewType": constants.SEGUI_VIEW_TYPE_TABLEVIEW,
                "widgetDataMap": {
                    "flxMore": "flxMore",
                    "lblName": "lblName"
                },
                "zIndex": 1
            }, {
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxTooltipContainer.add(segTaxId);
            flxTaxIdTooltip.add(flxUpArrowImage, flxTooltipContainer);
            var flxButtons = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "bottom": "1dp",
                "clipBounds": true,
                "height": "80px",
                "id": "flxButtons",
                "isVisible": false,
                "layoutType": kony.flex.FREE_FORM,
                "left": "35dp",
                "isModalContainer": false,
                "right": "35dp",
                "skin": "sknFlxBgFFFFFFBorE4E6ECsz1pxR1px",
                "zIndex": 1
            }, {}, {});
            flxButtons.setDefaultUnit(kony.flex.DP);
            var btnPopUpCancel = new kony.ui.Button({
                "centerY": "50%",
                "focusSkin": "sknBtnLatoRegulara5abc413pxKA",
                "height": "40px",
                "id": "btnPopUpCancel",
                "isVisible": true,
                "right": "160px",
                "skin": "sknbtnf7f7faLatoReg13px485c75Rad20px",
                "text": "REJECT",
                "top": "0%",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 20
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_CENTER,
                "displayText": true,
                "padding": [3, 0, 3, 0],
                "paddingInPixel": false
            }, {
                "hoverSkin": "sknbtnffffffLatoBolda5abc413Pxd7d8e01pxBorderRadius20px"
            });
            var btnPopUpDelete = new kony.ui.Button({
                "centerY": "50%",
                "focusSkin": "sknBtn003E75LatoRegular13pxFFFFFFRad20px",
                "height": "40px",
                "id": "btnPopUpDelete",
                "isVisible": true,
                "minWidth": "100px",
                "right": "20px",
                "skin": "sknBtn003E75LatoRegular13pxFFFFFFRad20px",
                "text": "APPROVE",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 20
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_CENTER,
                "displayText": true,
                "padding": [3, 0, 3, 0],
                "paddingInPixel": false
            }, {
                "hoverSkin": "sknBtn005198LatoRegular13pxFFFFFFRad20px"
            });
            flxButtons.add(btnPopUpCancel, btnPopUpDelete);
            flxRequestDetails.add(flxDetailContainer, flxTaxIdTooltip, flxButtons);
            flxMainContent.add(flxRequestsListContainer, flxRequestDetails);
            flxRightPanel.add(flxMainHeader, flxBreadcrumb, flxMainContent);
            var flxToastMessage = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "bottom": "60dp",
                "clipBounds": true,
                "height": "8%",
                "id": "flxToastMessage",
                "isVisible": false,
                "layoutType": kony.flex.FREE_FORM,
                "left": "305px",
                "isModalContainer": false,
                "skin": "slFbox",
                "width": "78%",
                "zIndex": 11
            }, {}, {});
            flxToastMessage.setDefaultUnit(kony.flex.DP);
            var toastMessage = new com.adminConsole.common.toastMessage({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "id": "toastMessage",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "masterType": constants.MASTER_TYPE_DEFAULT,
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0dp",
                "overrides": {}
            }, {
                "overrides": {}
            }, {
                "overrides": {}
            });
            flxToastMessage.add(toastMessage);
            var flxLoading = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "100%",
                "id": "flxLoading",
                "isVisible": false,
                "layoutType": kony.flex.FREE_FORM,
                "left": "305px",
                "isModalContainer": false,
                "right": "0px",
                "skin": "sknLoadingBlur",
                "top": "0dp",
                "zIndex": 10
            }, {}, {});
            flxLoading.setDefaultUnit(kony.flex.DP);
            var flxImageContainer = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "centerX": "50%",
                "centerY": "50%",
                "clipBounds": true,
                "height": "75px",
                "id": "flxImageContainer",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "isModalContainer": false,
                "skin": "slFbox",
                "width": "75px",
                "zIndex": 1
            }, {}, {});
            flxImageContainer.setDefaultUnit(kony.flex.DP);
            var imgLoading = new kony.ui.Image2({
                "centerX": "50%",
                "centerY": "50%",
                "height": "50px",
                "id": "imgLoading",
                "isVisible": true,
                "skin": "slImage",
                "src": "loadingscreenimage.gif",
                "width": "50px",
                "zIndex": 1
            }, {
                "imageScaleMode": constants.IMAGE_SCALE_MODE_MAINTAIN_ASPECT_RATIO,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxImageContainer.add(imgLoading);
            flxLoading.add(flxImageContainer);
            var flxRejectPopUp = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "100%",
                "id": "flxRejectPopUp",
                "isVisible": false,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "sknFocus50",
                "top": "0dp",
                "width": "100%",
                "zIndex": 6
            }, {}, {});
            flxRejectPopUp.setDefaultUnit(kony.flex.DP);
            var flxRejectPopupContainer = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "centerX": "50%",
                "centerY": "50%",
                "clipBounds": true,
                "height": "345px",
                "id": "flxRejectPopupContainer",
                "isVisible": true,
                "layoutType": kony.flex.FLOW_VERTICAL,
                "left": "27%",
                "isModalContainer": false,
                "skin": "slFbox0j9f841cc563e4e",
                "width": "520px",
                "zIndex": 1
            }, {}, {});
            flxRejectPopupContainer.setDefaultUnit(kony.flex.DP);
            var flxTopColor = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "10px",
                "id": "flxTopColor",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0px",
                "isModalContainer": false,
                "skin": "sknFlxTopColor4A77A0",
                "top": "0dp",
                "width": "100%",
                "zIndex": 1
            }, {}, {});
            flxTopColor.setDefaultUnit(kony.flex.DP);
            flxTopColor.add();
            var flxPopupHeader = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "255px",
                "id": "flxPopupHeader",
                "isVisible": true,
                "layoutType": kony.flex.FLOW_VERTICAL,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0px",
                "width": "100%",
                "zIndex": 1
            }, {}, {});
            flxPopupHeader.setDefaultUnit(kony.flex.DP);
            var flxClose = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "25px",
                "id": "flxClose",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "isModalContainer": false,
                "right": "13dp",
                "skin": "sknCursor",
                "top": "15dp",
                "width": "25px",
                "zIndex": 20
            }, {}, {
                "hoverSkin": "sknCursor"
            });
            flxClose.setDefaultUnit(kony.flex.DP);
            var fontIconImgCLose = new kony.ui.Label({
                "centerX": "51%",
                "centerY": "50.00%",
                "height": "15dp",
                "id": "fontIconImgCLose",
                "isVisible": true,
                "left": "17dp",
                "skin": "sknFontIconSearchCross16px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmAlertsManagement.lblAlertPreviewClose\")",
                "width": "15dp",
                "zIndex": 2
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxClose.add(fontIconImgCLose);
            var flxheader = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "25dp",
                "id": "flxheader",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "-5dp",
                "width": "100%",
                "zIndex": 20
            }, {}, {});
            flxheader.setDefaultUnit(kony.flex.DP);
            var lblAddSubHeader = new kony.ui.Label({
                "id": "lblAddSubHeader",
                "isVisible": true,
                "left": "20dp",
                "skin": "sknlblLatoBold35475f23px",
                "text": "Reason to Reject",
                "top": "0dp",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_TOP_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxheader.add(lblAddSubHeader);
            var lblMsg = new kony.ui.Label({
                "id": "lblMsg",
                "isVisible": true,
                "left": "20dp",
                "skin": "sknlbl485c7514px",
                "text": "Specify the reason to reject this enrollment request",
                "top": "5dp",
                "width": "450dp",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var flxTextArea = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "bottom": "10dp",
                "clipBounds": true,
                "height": "120dp",
                "id": "flxTextArea",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "20dp",
                "isModalContainer": false,
                "right": "35dp",
                "skin": "slFbox",
                "top": "22dp",
                "width": "93%",
                "zIndex": 1
            }, {}, {});
            flxTextArea.setDefaultUnit(kony.flex.DP);
            var lblReason = new kony.ui.Label({
                "id": "lblReason",
                "isVisible": true,
                "left": "0px",
                "skin": "sknlbl485c7514px",
                "text": "Reason",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var lblReasonSize = new kony.ui.Label({
                "id": "lblReasonSize",
                "isVisible": false,
                "right": "0px",
                "skin": "sknlbl485c7514px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmCSR.lblCharCount2\")",
                "top": "0px",
                "width": "100px",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_RIGHT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var txtReason = new kony.ui.TextArea2({
                "autoCapitalize": constants.TEXTAREA_AUTO_CAPITALIZE_NONE,
                "focusSkin": "skntxtAreaLato9ca9ba12Px",
                "height": "90dp",
                "id": "txtReason",
                "isVisible": true,
                "keyBoardStyle": constants.TEXTAREA_KEY_BOARD_STYLE_DEFAULT,
                "left": "0dp",
                "maxTextLength": 300,
                "numberOfVisibleLines": 3,
                "right": "0dp",
                "skin": "skntxtAreaLato35475f14Px",
                "textInputMode": constants.TEXTAREA_INPUT_MODE_ANY,
                "top": "30dp",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_TOP_LEFT,
                "padding": [2, 2, 2, 2],
                "paddingInPixel": false
            }, {
                "autoCorrect": false
            });
            flxTextArea.add(lblReason, lblReasonSize, txtReason);
            var flxNoReasonError = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "bottom": "5dp",
                "clipBounds": true,
                "height": "15px",
                "id": "flxNoReasonError",
                "isVisible": false,
                "layoutType": kony.flex.FREE_FORM,
                "left": "20px",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0px",
                "width": "350px",
                "zIndex": 1
            }, {}, {});
            flxNoReasonError.setDefaultUnit(kony.flex.DP);
            var lblNoReasonErrorIcon = new kony.ui.Label({
                "height": "15px",
                "id": "lblNoReasonErrorIcon",
                "isVisible": true,
                "left": "0px",
                "skin": "sknErrorIcon",
                "text": "",
                "top": "0px",
                "width": "15px",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var lblNoReasonError = new kony.ui.Label({
                "centerY": "50%",
                "height": "15px",
                "id": "lblNoReasonError",
                "isVisible": true,
                "left": "15px",
                "skin": "sknlblError",
                "text": "Reject Reason cannot be empty",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxNoReasonError.add(lblNoReasonErrorIcon, lblNoReasonError);
            flxPopupHeader.add(flxClose, flxheader, lblMsg, flxTextArea, flxNoReasonError);
            var flxRejectButtons = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "80px",
                "id": "flxRejectButtons",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "isModalContainer": false,
                "skin": "sknFooter",
                "top": "0px",
                "width": "100%",
                "zIndex": 1
            }, {}, {});
            flxRejectButtons.setDefaultUnit(kony.flex.DP);
            var btnCancel = new kony.ui.Button({
                "centerY": "50%",
                "focusSkin": "sknBtnLatoRegulara5abc413pxKA",
                "height": "40px",
                "id": "btnCancel",
                "isVisible": true,
                "onClick": controller.AS_Button_fe48adad632c43c5b97a05155a450331,
                "right": "140px",
                "skin": "sknbtnf7f7faLatoReg13px485c75Rad20px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmCustomers.CANCEL\")",
                "top": "0%",
                "width": "110px",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_CENTER,
                "displayText": true,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {
                "hoverSkin": "sknbtnffffffLatoBolda5abc413Pxd7d8e01pxBorderRadius20px"
            });
            var btnsave = new kony.ui.Button({
                "centerY": "50%",
                "focusSkin": "sknBtn003E75LatoRegular13pxFFFFFFRad20px",
                "height": "40dp",
                "id": "btnsave",
                "isVisible": true,
                "onClick": controller.AS_Button_c973a89c85624425bb033af39fa13bd2,
                "right": "20px",
                "skin": "sknBtn003E75LatoRegular13pxFFFFFFRad20px",
                "text": "REJECT",
                "top": "0%",
                "width": "100dp",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_CENTER,
                "displayText": true,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {
                "hoverSkin": "sknBtn005198LatoRegular13pxFFFFFFRad20px"
            });
            flxRejectButtons.add(btnCancel, btnsave);
            flxRejectPopupContainer.add(flxTopColor, flxPopupHeader, flxRejectButtons);
            flxRejectPopUp.add(flxRejectPopupContainer);
            var flxHeaderDropdown = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                "clipBounds": true,
                "id": "flxHeaderDropdown",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "isModalContainer": false,
                "right": "35dp",
                "skin": "slFbox",
                "top": "20dp",
                "width": "400dp",
                "zIndex": 5
            }, {}, {});
            flxHeaderDropdown.setDefaultUnit(kony.flex.DP);
            var dropdownMainHeader = new com.adminConsole.common.dropdownMainHeader({
                "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                "id": "dropdownMainHeader",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "masterType": constants.MASTER_TYPE_DEFAULT,
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0dp",
                "width": "100%",
                "overrides": {
                    "flxDropdown": {
                        "isVisible": false
                    },
                    "imgUpArrow": {
                        "src": "uparrow_2x.png"
                    }
                }
            }, {
                "overrides": {}
            }, {
                "overrides": {}
            });
            flxHeaderDropdown.add(dropdownMainHeader);
            var flxViewSubsidiary = new kony.ui.FlexContainer({
                "clipBounds": true,
                "height": "100%",
                "id": "flxViewSubsidiary",
                "isVisible": false,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "isModalContainer": false,
                "top": "0dp",
                "width": "100%",
                "zIndex": 11
            }, {}, {});
            flxViewSubsidiary.setDefaultUnit(kony.flex.DP);
            var flxViewPopup = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "centerX": "50%",
                "centerY": "50%",
                "clipBounds": true,
                "height": "539dp",
                "id": "flxViewPopup",
                "isVisible": true,
                "layoutType": kony.flex.FLOW_VERTICAL,
                "isModalContainer": false,
                "width": "986dp",
                "zIndex": 1
            }, {}, {});
            flxViewPopup.setDefaultUnit(kony.flex.DP);
            var flxBorderblue = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "10dp",
                "id": "flxBorderblue",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0px",
                "isModalContainer": false,
                "skin": "sknFlxTopColor4A77A0",
                "top": "0px",
                "width": "986dp",
                "zIndex": 1
            }, {}, {});
            flxBorderblue.setDefaultUnit(kony.flex.DP);
            flxBorderblue.add();
            var flxCloseIcn = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "15dp",
                "id": "flxCloseIcn",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "921dp",
                "isModalContainer": false,
                "onClick": controller.AS_FlexContainer_f94a1ea7cf714c1eba67701216231f4c,
                "right": "20dp",
                "skin": "slFbox",
                "top": "25dp",
                "width": "15dp",
                "zIndex": 1
            }, {}, {});
            flxCloseIcn.setDefaultUnit(kony.flex.DP);
            var lblClosePopup = new kony.ui.Label({
                "height": "15dp",
                "id": "lblClosePopup",
                "isVisible": true,
                "left": "0dp",
                "skin": "sknIcnCloseFntSize15px",
                "text": "",
                "top": "0dp",
                "width": "15dp",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxCloseIcn.add(lblClosePopup);
            var flxLblsub = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "19dp",
                "id": "flxLblsub",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "20dp",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "30dp",
                "width": "197dp"
            }, {}, {});
            flxLblsub.setDefaultUnit(kony.flex.DP);
            var lblListofSubsdiary = new kony.ui.Label({
                "height": "100%",
                "id": "lblListofSubsdiary",
                "isVisible": true,
                "left": 0,
                "skin": "sknLatoRegular192B4516px",
                "text": "List of Subsdiary Company",
                "top": 0,
                "width": "100%",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxLblsub.add(lblListofSubsdiary);
            var flxseparatorSub = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "1px",
                "id": "flxseparatorSub",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "20px",
                "isModalContainer": false,
                "skin": "sknflxD5D9DD",
                "top": "19px",
                "width": "946px",
                "zIndex": 1
            }, {}, {});
            flxseparatorSub.setDefaultUnit(kony.flex.DP);
            flxseparatorSub.add();
            var flxSearchsubview = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "40px",
                "id": "flxSearchsubview",
                "isVisible": true,
                "layoutType": kony.flex.FLOW_HORIZONTAL,
                "left": "539px",
                "isModalContainer": false,
                "skin": "sknFlxBgFFFFFFborD7D9E0rad19",
                "top": "14px",
                "width": "427px",
                "zIndex": 1
            }, {}, {});
            flxSearchsubview.setDefaultUnit(kony.flex.DP);
            var lblSubsidiarysearch = new kony.ui.Label({
                "bottom": "12px",
                "height": "16px",
                "id": "lblSubsidiarysearch",
                "isVisible": true,
                "left": "15px",
                "skin": "sknLblSerachIcommon16px",
                "text": "",
                "top": "12px",
                "width": "13px",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var flxSearchSub = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "40px",
                "id": "flxSearchSub",
                "isVisible": true,
                "layoutType": kony.flex.FLOW_HORIZONTAL,
                "left": "10px",
                "isModalContainer": false,
                "skin": "slFbox",
                "width": "340px"
            }, {}, {});
            flxSearchSub.setDefaultUnit(kony.flex.DP);
            var txtSearchbox = new kony.ui.TextBox2({
                "autoCapitalize": constants.TEXTBOX_AUTO_CAPITALIZE_NONE,
                "height": "40px",
                "id": "txtSearchbox",
                "isVisible": true,
                "keyBoardStyle": constants.TEXTBOX_KEY_BOARD_STYLE_DEFAULT,
                "placeholder": "Search by Company name",
                "secureTextEntry": false,
                "skin": "sknTxtFntB2BDCBBorNone",
                "textInputMode": constants.TEXTBOX_INPUT_MODE_ANY,
                "width": "350px",
                "zIndex": 1
            }, {
                "containerHeightMode": constants.TEXTBOX_FONT_METRICS_DRIVEN_HEIGHT,
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {
                "autoCorrect": false,
                "placeholderSkin": "defTextBoxPlaceholder"
            });
            flxSearchSub.add(txtSearchbox);
            var flxClearSearchImageSub = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "100%",
                "id": "flxClearSearchImageSub",
                "isVisible": false,
                "layoutType": kony.flex.FREE_FORM,
                "isModalContainer": false,
                "right": "0px",
                "skin": "sknCursor",
                "top": "0dp",
                "width": "35px",
                "zIndex": 1
            }, {}, {
                "hoverSkin": "sknCursor"
            });
            flxClearSearchImageSub.setDefaultUnit(kony.flex.DP);
            var fontIconCrossSub = new kony.ui.Label({
                "centerY": "50%",
                "id": "fontIconCrossSub",
                "isVisible": true,
                "left": "5px",
                "skin": "sknFontIconSearchCross16px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.userwidgetmodel.lblClearSearch\")",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxClearSearchImageSub.add(fontIconCrossSub);
            flxSearchsubview.add(lblSubsidiarysearch, flxSearchSub, flxClearSearchImageSub);
            var flxSepartorSubView = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "1px",
                "id": "flxSepartorSubView",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "20px",
                "isModalContainer": false,
                "skin": "sknflxD5D9DD",
                "top": "14px",
                "width": "946px",
                "zIndex": 1
            }, {}, {});
            flxSepartorSubView.setDefaultUnit(kony.flex.DP);
            flxSepartorSubView.add();
            var flxViewHeaderSubsidiary = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "10%",
                "id": "flxViewHeaderSubsidiary",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "25px",
                "isModalContainer": false,
                "right": "0px",
                "skin": "slFbox",
                "top": "0dp",
                "width": "946px"
            }, {}, {});
            flxViewHeaderSubsidiary.setDefaultUnit(kony.flex.DP);
            var flxCompanynameheader = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                "centerY": "50%",
                "clipBounds": true,
                "id": "flxCompanynameheader",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "20dp",
                "isModalContainer": false,
                "skin": "slFbox",
                "width": "20%"
            }, {}, {});
            flxCompanynameheader.setDefaultUnit(kony.flex.DP);
            var lblCompanynameheader = new kony.ui.Label({
                "id": "lblCompanynameheader",
                "isVisible": true,
                "left": "0",
                "skin": "sknlblLato696c7311px",
                "text": "COMPANY NAME",
                "top": "0",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxCompanynameheader.add(lblCompanynameheader);
            var flxCustomerId = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                "centerY": "50%",
                "clipBounds": true,
                "id": "flxCustomerId",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "30%",
                "isModalContainer": false,
                "skin": "slFbox",
                "width": "20%",
                "zIndex": 1
            }, {}, {
                "hoverSkin": "sknFlxPointer"
            });
            flxCustomerId.setDefaultUnit(kony.flex.DP);
            var lblCustomerIdSub = new kony.ui.Label({
                "id": "lblCustomerIdSub",
                "isVisible": true,
                "skin": "sknlblLato696c7311px",
                "text": "CUSTOMER ID",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxCustomerId.add(lblCustomerIdSub);
            var flxTax = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                "centerY": "50%",
                "clipBounds": true,
                "id": "flxTax",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "55%",
                "isModalContainer": false,
                "skin": "slFbox",
                "width": "20%",
                "zIndex": 1
            }, {}, {});
            flxTax.setDefaultUnit(kony.flex.DP);
            var lblTaxID = new kony.ui.Label({
                "id": "lblTaxID",
                "isVisible": true,
                "skin": "sknlblLato696c7311px",
                "text": "TAX ID",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxTax.add(lblTaxID);
            var flxNumberOfAccount = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                "centerY": "50%",
                "clipBounds": true,
                "id": "flxNumberOfAccount",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "80%",
                "isModalContainer": false,
                "skin": "slFbox",
                "width": "20%",
                "zIndex": 1
            }, {}, {});
            flxNumberOfAccount.setDefaultUnit(kony.flex.DP);
            var lblNumberOfAccountSub = new kony.ui.Label({
                "id": "lblNumberOfAccountSub",
                "isVisible": true,
                "skin": "sknlblLato696c7311px",
                "text": "NUMBER OF ACCOUNTS",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxNumberOfAccount.add(lblNumberOfAccountSub);
            flxViewHeaderSubsidiary.add(flxCompanynameheader, flxCustomerId, flxTax, flxNumberOfAccount);
            var flxSeparatorSubViewHeader = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "1px",
                "id": "flxSeparatorSubViewHeader",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "20px",
                "isModalContainer": false,
                "skin": "sknFlx1pxBorder696C73",
                "width": "946px",
                "zIndex": 1
            }, {}, {});
            flxSeparatorSubViewHeader.setDefaultUnit(kony.flex.DP);
            flxSeparatorSubViewHeader.add();
            var flxSegSubsView = new kony.ui.FlexScrollContainer({
                "allowHorizontalBounce": false,
                "allowVerticalBounce": true,
                "bounces": true,
                "clipBounds": true,
                "enableScrolling": true,
                "height": "215px",
                "horizontalScrollIndicator": true,
                "id": "flxSegSubsView",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "25px",
                "pagingEnabled": false,
                "scrollDirection": kony.flex.SCROLL_VERTICAL,
                "skin": "slFSbox",
                "top": "15px",
                "verticalScrollIndicator": true,
                "width": "946px",
                "zIndex": 1
            }, {}, {});
            flxSegSubsView.setDefaultUnit(kony.flex.DP);
            var segSubViews = new kony.ui.SegmentedUI2({
                "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                "data": [{
                    "lblBusinessTypes": "ACH_Collection_1",
                    "lblCompanyName": "XYZ Company",
                    "lblSeperator": ".",
                    "lblSubmittedBy": "JOHN PETER",
                    "lblSubmittedOn": "17/08/2017 10:30PM"
                }, {
                    "lblBusinessTypes": "ACH_Collection_1",
                    "lblCompanyName": "XYZ Company",
                    "lblSeperator": ".",
                    "lblSubmittedBy": "JOHN PETER",
                    "lblSubmittedOn": "17/08/2017 10:30PM"
                }, {
                    "lblBusinessTypes": "ACH_Collection_1",
                    "lblCompanyName": "XYZ Company",
                    "lblSeperator": ".",
                    "lblSubmittedBy": "JOHN PETER",
                    "lblSubmittedOn": "17/08/2017 10:30PM"
                }, {
                    "lblBusinessTypes": "ACH_Collection_1",
                    "lblCompanyName": "XYZ Company",
                    "lblSeperator": ".",
                    "lblSubmittedBy": "JOHN PETER",
                    "lblSubmittedOn": "17/08/2017 10:30PM"
                }, {
                    "lblBusinessTypes": "ACH_Collection_1",
                    "lblCompanyName": "XYZ Company",
                    "lblSeperator": ".",
                    "lblSubmittedBy": "JOHN PETER",
                    "lblSubmittedOn": "17/08/2017 10:30PM"
                }, {
                    "lblBusinessTypes": "ACH_Collection_1",
                    "lblCompanyName": "XYZ Company",
                    "lblSeperator": ".",
                    "lblSubmittedBy": "JOHN PETER",
                    "lblSubmittedOn": "17/08/2017 10:30PM"
                }, {
                    "lblBusinessTypes": "ACH_Collection_1",
                    "lblCompanyName": "XYZ Company",
                    "lblSeperator": ".",
                    "lblSubmittedBy": "JOHN PETER",
                    "lblSubmittedOn": "17/08/2017 10:30PM"
                }, {
                    "lblBusinessTypes": "ACH_Collection_1",
                    "lblCompanyName": "XYZ Company",
                    "lblSeperator": ".",
                    "lblSubmittedBy": "JOHN PETER",
                    "lblSubmittedOn": "17/08/2017 10:30PM"
                }, {
                    "lblBusinessTypes": "ACH_Collection_1",
                    "lblCompanyName": "XYZ Company",
                    "lblSeperator": ".",
                    "lblSubmittedBy": "JOHN PETER",
                    "lblSubmittedOn": "17/08/2017 10:30PM"
                }, {
                    "lblBusinessTypes": "ACH_Collection_1",
                    "lblCompanyName": "XYZ Company",
                    "lblSeperator": ".",
                    "lblSubmittedBy": "JOHN PETER",
                    "lblSubmittedOn": "17/08/2017 10:30PM"
                }],
                "groupCells": false,
                "id": "segSubViews",
                "isVisible": true,
                "left": 0,
                "needPageIndicator": true,
                "pageOffDotImage": "pageoffdot.png",
                "pageOnDotImage": "pageondot.png",
                "retainSelection": false,
                "rowFocusSkin": "seg2Focus",
                "rowSkin": "seg2Normal",
                "rowTemplate": "flxViewSubsidiary",
                "sectionHeaderSkin": "sliPhoneSegmentHeader",
                "selectionBehavior": constants.SEGUI_DEFAULT_BEHAVIOR,
                "separatorColor": "aaaaaa00",
                "separatorRequired": true,
                "separatorThickness": 1,
                "showScrollbars": false,
                "top": 0,
                "viewType": constants.SEGUI_VIEW_TYPE_TABLEVIEW,
                "widgetDataMap": {
                    "flxViewSubsidiary": "flxViewSubsidiary",
                    "lblBusinessTypes": "lblBusinessTypes",
                    "lblCompanyName": "lblCompanyName",
                    "lblSeperator": "lblSeperator",
                    "lblSubmittedBy": "lblSubmittedBy",
                    "lblSubmittedOn": "lblSubmittedOn"
                },
                "width": "100%",
                "zIndex": 1
            }, {
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxSegSubsView.add(segSubViews);
            flxViewPopup.add(flxBorderblue, flxCloseIcn, flxLblsub, flxseparatorSub, flxSearchsubview, flxSepartorSubView, flxViewHeaderSubsidiary, flxSeparatorSubViewHeader, flxSegSubsView);
            flxViewSubsidiary.add(flxViewPopup);
            var flxFeatureDetails = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "100%",
                "id": "flxFeatureDetails",
                "isVisible": false,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "sknFocus50",
                "top": "0dp",
                "width": "100%",
                "zIndex": 5
            }, {}, {});
            flxFeatureDetails.setDefaultUnit(kony.flex.DP);
            var flxFeatureDetailsPopUp = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "centerX": "50%",
                "centerY": "50%",
                "clipBounds": true,
                "height": "80%",
                "id": "flxFeatureDetailsPopUp",
                "isVisible": true,
                "layoutType": kony.flex.FLOW_VERTICAL,
                "isModalContainer": false,
                "skin": "slFbox0j9f841cc563e4e",
                "width": "70%",
                "zIndex": 1
            }, {}, {});
            flxFeatureDetailsPopUp.setDefaultUnit(kony.flex.DP);
            var flxBlueTop = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "10px",
                "id": "flxBlueTop",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0px",
                "isModalContainer": false,
                "skin": "sknFlxTopColor4A77A0",
                "top": "0dp",
                "width": "100%",
                "zIndex": 1
            }, {}, {});
            flxBlueTop.setDefaultUnit(kony.flex.DP);
            flxBlueTop.add();
            var flxFeatureDetailsContainer = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "98.50%",
                "id": "flxFeatureDetailsContainer",
                "isVisible": true,
                "layoutType": kony.flex.FLOW_VERTICAL,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0px",
                "width": "100%",
                "zIndex": 1
            }, {}, {});
            flxFeatureDetailsContainer.setDefaultUnit(kony.flex.DP);
            var flxFeatureDetailsClose = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "25px",
                "id": "flxFeatureDetailsClose",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "isModalContainer": false,
                "right": "13dp",
                "skin": "sknCursor",
                "top": "10dp",
                "width": "25px",
                "zIndex": 20
            }, {}, {
                "hoverSkin": "sknCursor"
            });
            flxFeatureDetailsClose.setDefaultUnit(kony.flex.DP);
            var lblFontIconClose = new kony.ui.Label({
                "centerX": "51%",
                "centerY": "50.00%",
                "height": "15dp",
                "id": "lblFontIconClose",
                "isVisible": true,
                "left": "17dp",
                "skin": "sknFontIconSearchCross16px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmAlertsManagement.lblAlertPreviewClose\")",
                "width": "15dp",
                "zIndex": 2
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxFeatureDetailsClose.add(lblFontIconClose);
            var flxFeatureDetailsHeader = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "20dp",
                "id": "flxFeatureDetailsHeader",
                "isVisible": true,
                "layoutType": kony.flex.FLOW_HORIZONTAL,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "-5dp",
                "width": "100%",
                "zIndex": 20
            }, {}, {});
            flxFeatureDetailsHeader.setDefaultUnit(kony.flex.DP);
            var lblFeatureDetailsHeader1 = new kony.ui.Label({
                "centerY": "50%",
                "id": "lblFeatureDetailsHeader1",
                "isVisible": true,
                "left": "20dp",
                "skin": "sknlblLatoBold35475f23px",
                "text": "Feature Details -",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var lblFeatureDetailsHeader2 = new kony.ui.Label({
                "centerY": "50%",
                "id": "lblFeatureDetailsHeader2",
                "isVisible": true,
                "left": "5dp",
                "skin": "sknlblLatoBold35475f23px",
                "text": "Bill Payments",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxFeatureDetailsHeader.add(lblFeatureDetailsHeader1, lblFeatureDetailsHeader2);
            var flxFeatureStatus = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                "clipBounds": true,
                "id": "flxFeatureStatus",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "20dp",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "10dp",
                "width": "31%",
                "zIndex": 1
            }, {}, {});
            flxFeatureStatus.setDefaultUnit(kony.flex.DP);
            var fontIconActive = new kony.ui.Label({
                "id": "fontIconActive",
                "isVisible": true,
                "left": "0dp",
                "skin": "sknFontIconActivate",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmAlertsManagement.lblViewAlertStatusKey\")",
                "top": "2dp",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var lblFeatureStatus = new kony.ui.Label({
                "id": "lblFeatureStatus",
                "isVisible": true,
                "left": "15px",
                "skin": "sknlblLato5bc06cBold14px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.secureimage.Active\")",
                "top": "0px",
                "width": "100%",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxFeatureStatus.add(fontIconActive, lblFeatureStatus);
            var lblSeparator = new kony.ui.Label({
                "bottom": "15px",
                "height": "1px",
                "id": "lblSeparator",
                "isVisible": true,
                "left": "20dp",
                "right": "20dp",
                "skin": "sknlblSeperator",
                "text": "-",
                "top": "15px",
                "zIndex": 2
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_TOP_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var flxFeatureDescription = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                "clipBounds": true,
                "id": "flxFeatureDescription",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "isModalContainer": false,
                "right": "50dp",
                "skin": "slFbox",
                "top": "5dp",
                "width": kony.flex.USE_PREFFERED_SIZE
            }, {}, {});
            flxFeatureDescription.setDefaultUnit(kony.flex.DP);
            var lblFeatureDescriptionHeading = new kony.ui.Label({
                "id": "lblFeatureDescriptionHeading",
                "isVisible": true,
                "left": "20dp",
                "skin": "sknLbl78818A11px",
                "text": "DESCRIPTION",
                "top": "0dp",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var lblFeatureDescriptionValue = new kony.ui.Label({
                "id": "lblFeatureDescriptionValue",
                "isVisible": true,
                "left": "20dp",
                "right": "20dp",
                "skin": "sknLatoSemibold485c7313px",
                "text": "This terms & conditions will be used in the footer link of retail banking",
                "top": "25dp",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxFeatureDescription.add(lblFeatureDescriptionHeading, lblFeatureDescriptionValue);
            var flxActionsList = new kony.ui.FlexScrollContainer({
                "allowHorizontalBounce": false,
                "allowVerticalBounce": true,
                "bounces": true,
                "clipBounds": true,
                "enableScrolling": true,
                "height": "63%",
                "horizontalScrollIndicator": true,
                "id": "flxActionsList",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "pagingEnabled": false,
                "right": "0dp",
                "scrollDirection": kony.flex.SCROLL_VERTICAL,
                "skin": "slFSbox",
                "top": "20dp",
                "verticalScrollIndicator": true
            }, {}, {});
            flxActionsList.setDefaultUnit(kony.flex.DP);
            var flxActionsHeader = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "45px",
                "id": "flxActionsHeader",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": 0,
                "isModalContainer": false,
                "right": "0dp",
                "skin": "sknflxffffffop100",
                "top": "0",
                "zIndex": 10
            }, {}, {});
            flxActionsHeader.setDefaultUnit(kony.flex.DP);
            var lblHeaderSeperator1 = new kony.ui.Label({
                "centerX": "50%",
                "height": "1px",
                "id": "lblHeaderSeperator1",
                "isVisible": true,
                "left": "20dp",
                "right": "20px",
                "skin": "sknlblSeperator",
                "text": "-",
                "top": "0dp",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var lblActionName = new kony.ui.Label({
                "centerY": "50%",
                "id": "lblActionName",
                "isVisible": true,
                "left": "30dp",
                "skin": "sknlblLato696c7312px",
                "text": "ACTION",
                "width": "30%",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {
                "hoverSkin": "sknlblLatoBold00000012px"
            });
            var lblDescriptionHeader = new kony.ui.Label({
                "centerY": "50%",
                "id": "lblDescriptionHeader",
                "isVisible": true,
                "left": "40%",
                "skin": "sknlblLato696c7312px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.View.DESCRIPTION\")",
                "top": 0,
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {
                "hoverSkin": "sknlblLatoBold00000012px"
            });
            var lblHeaderSeperator2 = new kony.ui.Label({
                "bottom": "1px",
                "centerX": "50%",
                "height": "1px",
                "id": "lblHeaderSeperator2",
                "isVisible": true,
                "left": "20dp",
                "right": "20px",
                "skin": "sknLblTableHeaderLine",
                "text": "-",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxActionsHeader.add(lblHeaderSeperator1, lblActionName, lblDescriptionHeader, lblHeaderSeperator2);
            var segActions = new kony.ui.SegmentedUI2({
                "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                "data": [{
                    "lblActionDescription": "Handle your business Payables  efficiently",
                    "lblActionName": "Mobile App",
                    "lblSeparator": "Label"
                }, {
                    "lblActionDescription": "Handle your business Payables  efficiently",
                    "lblActionName": "Mobile App",
                    "lblSeparator": "Label"
                }, {
                    "lblActionDescription": "Handle your business Payables  efficiently",
                    "lblActionName": "Mobile App",
                    "lblSeparator": "Label"
                }, {
                    "lblActionDescription": "Handle your business Payables  efficiently",
                    "lblActionName": "Mobile App",
                    "lblSeparator": "Label"
                }, {
                    "lblActionDescription": "Handle your business Payables  efficiently",
                    "lblActionName": "Mobile App",
                    "lblSeparator": "Label"
                }, {
                    "lblActionDescription": "Handle your business Payables  efficiently",
                    "lblActionName": "Mobile App",
                    "lblSeparator": "Label"
                }, {
                    "lblActionDescription": "Handle your business Payables  efficiently",
                    "lblActionName": "Mobile App",
                    "lblSeparator": "Label"
                }, {
                    "lblActionDescription": "Handle your business Payables  efficiently",
                    "lblActionName": "Mobile App",
                    "lblSeparator": "Label"
                }, {
                    "lblActionDescription": "Handle your business Payables  efficiently",
                    "lblActionName": "Mobile App",
                    "lblSeparator": "Label"
                }, {
                    "lblActionDescription": "Handle your business Payables  efficiently",
                    "lblActionName": "Mobile App",
                    "lblSeparator": "Label"
                }],
                "groupCells": false,
                "id": "segActions",
                "isVisible": true,
                "left": "0px",
                "needPageIndicator": true,
                "pageOffDotImage": "pageoffdot.png",
                "pageOnDotImage": "pageondot.png",
                "retainSelection": false,
                "right": "0dp",
                "rowFocusSkin": "seg2Focus",
                "rowSkin": "seg2Normal",
                "rowTemplate": "flxFeatureDetailsActions",
                "sectionHeaderSkin": "sliPhoneSegmentHeader",
                "selectionBehavior": constants.SEGUI_DEFAULT_BEHAVIOR,
                "separatorRequired": false,
                "showScrollbars": false,
                "top": "45dp",
                "viewType": constants.SEGUI_VIEW_TYPE_TABLEVIEW,
                "widgetDataMap": {
                    "flxFeatureDetailsActions": "flxFeatureDetailsActions",
                    "lblActionDescription": "lblActionDescription",
                    "lblActionName": "lblActionName",
                    "lblSeparator": "lblSeparator"
                },
                "zIndex": 10
            }, {
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var flxNoRecordsFound = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "200dp",
                "id": "flxNoRecordsFound",
                "isVisible": false,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0dp",
                "width": "100%",
                "zIndex": 1
            }, {}, {});
            flxNoRecordsFound.setDefaultUnit(kony.flex.DP);
            var rtxNoRecords = new kony.ui.RichText({
                "centerX": "50%",
                "centerY": "50%",
                "id": "rtxNoRecords",
                "isVisible": true,
                "skin": "sknRtxLato84939e12Px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.userwidgetmodel.rtxNoRecords\")",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxNoRecordsFound.add(rtxNoRecords);
            flxActionsList.add(flxActionsHeader, segActions, flxNoRecordsFound);
            flxFeatureDetailsContainer.add(flxFeatureDetailsClose, flxFeatureDetailsHeader, flxFeatureStatus, lblSeparator, flxFeatureDescription, flxActionsList);
            flxFeatureDetailsPopUp.add(flxBlueTop, flxFeatureDetailsContainer);
            flxFeatureDetails.add(flxFeatureDetailsPopUp);
            var flxSuspendFeaturePopup = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "100%",
                "id": "flxSuspendFeaturePopup",
                "isVisible": false,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "masterType": constants.MASTER_TYPE_USERWIDGET,
                "isModalContainer": false,
                "skin": "flxPopupTrans0ba858b5d5d304e",
                "top": "0dp",
                "width": "100%",
                "zIndex": 10
            }, {}, {});
            flxSuspendFeaturePopup.setDefaultUnit(kony.flex.DP);
            var suspendFeaturePopup = new com.adminConsole.common.popUp({
                "height": "100%",
                "id": "suspendFeaturePopup",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "masterType": constants.MASTER_TYPE_DEFAULT,
                "isModalContainer": false,
                "skin": "sknflxBg000000Op50",
                "top": "0dp",
                "width": "100%",
                "zIndex": 1,
                "overrides": {
                    "btnPopUpCancel": {
                        "i18n_text": "kony.i18n.getLocalizedString(\"i18n.PopUp.NoLeaveAsIS\")",
                        "right": "20dp"
                    },
                    "btnPopUpDelete": {
                        "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmLocationsController.YES__OFFLINE\")",
                        "minWidth": "viz.val_cleared"
                    },
                    "lblPopUpMainMessage": {
                        "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmCompaniesPopup.confirmation\")",
                        "isVisible": true
                    },
                    "rtxPopUpDisclaimer": {
                        "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmCompanies.suspendFeatureDetail\")",
                        "width": "90%"
                    }
                }
            }, {
                "overrides": {}
            }, {
                "overrides": {}
            });
            flxSuspendFeaturePopup.add(suspendFeaturePopup);
            flxMain.add(flxLeftPanel, flxRightPanel, flxToastMessage, flxLoading, flxRejectPopUp, flxHeaderDropdown, flxViewSubsidiary, flxFeatureDetails, flxSuspendFeaturePopup);
            this.add(flxMain);
        };
        return [{
            "addWidgets": addWidgetsfrmEnrollmentRequests,
            "enabledForIdleTimeout": true,
            "id": "frmEnrollmentRequests",
            "layoutType": kony.flex.FREE_FORM,
            "needAppMenu": false,
            "preShow": function(eventobject) {
                controller.AS_Form_ab23dc4a2d5a4a35b0440952bae5eee3(eventobject);
            },
            "skin": "slForm"
        }, {
            "displayOrientation": constants.FORM_DISPLAY_ORIENTATION_PORTRAIT,
            "layoutType": kony.flex.FREE_FORM,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, {
            "retainScrollPosition": false
        }]
    }
});