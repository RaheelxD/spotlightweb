define("CompaniesModule/frmBusinessTypes", function() {
    return function(controller) {
        function addWidgetsfrmBusinessTypes() {
            this.setDefaultUnit(kony.flex.DP);
            var flxMain = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "100%",
                "id": "flxMain",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0",
                "isModalContainer": false,
                "skin": "sknflxf5f6f8Op100",
                "top": "0",
                "width": "100%"
            }, {}, {});
            flxMain.setDefaultUnit(kony.flex.DP);
            var flxLeftPanel = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "100%",
                "id": "flxLeftPanel",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0dp",
                "width": "305dp",
                "zIndex": 1
            }, {}, {});
            flxLeftPanel.setDefaultUnit(kony.flex.DP);
            var leftMenuNew = new com.adminConsole.navigation.leftMenuNew({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "height": "100%",
                "id": "leftMenuNew",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "masterType": constants.MASTER_TYPE_DEFAULT,
                "isModalContainer": false,
                "skin": "sknFlxBg003E75Op100",
                "top": "0dp",
                "width": "100%",
                "overrides": {
                    "brandLogo": {
                        "src": "infinity_dbx_c360_logo_white_2x.png"
                    },
                    "flxScrollMenu": {
                        "bottom": "40dp",
                        "left": "0dp",
                        "top": "85dp"
                    },
                    "imgBottomLogo": {
                        "src": "logo_white_2x.png"
                    },
                    "leftMenuNew": {
                        "bottom": "viz.val_cleared",
                        "centerX": "viz.val_cleared",
                        "centerY": "viz.val_cleared",
                        "height": "100%",
                        "left": "0dp",
                        "maxHeight": "viz.val_cleared",
                        "maxWidth": "viz.val_cleared",
                        "minHeight": "viz.val_cleared",
                        "minWidth": "viz.val_cleared",
                        "right": "viz.val_cleared",
                        "top": "0dp",
                        "width": "100%"
                    }
                }
            }, {
                "overrides": {}
            }, {
                "overrides": {}
            });
            flxLeftPanel.add(leftMenuNew);
            var flxRightPanel = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "bottom": "0dp",
                "clipBounds": true,
                "id": "flxRightPanel",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "305dp",
                "isModalContainer": false,
                "right": "0dp",
                "skin": "slFbox",
                "top": "0dp"
            }, {}, {});
            flxRightPanel.setDefaultUnit(kony.flex.DP);
            var flxMainHeader = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "106dp",
                "id": "flxMainHeader",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0dp",
                "width": "100%",
                "zIndex": 1
            }, {}, {});
            flxMainHeader.setDefaultUnit(kony.flex.DP);
            var mainHeader = new com.adminConsole.header.mainHeader({
                "height": "106px",
                "id": "mainHeader",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": 0,
                "masterType": constants.MASTER_TYPE_DEFAULT,
                "isModalContainer": false,
                "skin": "CopyslFbox1",
                "top": 0,
                "width": "100%",
                "overrides": {
                    "btnAddNewOption": {
                        "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmBusinessTypes.add_business_type_UC\")",
                        "right": "0dp"
                    },
                    "btnDropdownList": {
                        "i18n_text": "kony.i18n.getLocalizedString(\"i18n.mainHeader.DOWNLOADLIST\")",
                        "isVisible": false
                    },
                    "flxButtons": {
                        "right": "35dp",
                        "width": "400dp"
                    },
                    "flxHeaderSeperator": {
                        "bottom": "1dp",
                        "isVisible": false,
                        "top": "viz.val_cleared"
                    },
                    "imgLogout": {
                        "src": "img_logout.png"
                    },
                    "lblHeading": {
                        "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmBusinessTypes.BusinessTypes\")"
                    }
                }
            }, {
                "overrides": {}
            }, {
                "overrides": {}
            });
            flxMainHeader.add(mainHeader);
            var flxBreadcrumb = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "31dp",
                "id": "flxBreadcrumb",
                "isVisible": true,
                "layoutType": kony.flex.FLOW_VERTICAL,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "106px",
                "width": "100%",
                "zIndex": 10
            }, {}, {});
            flxBreadcrumb.setDefaultUnit(kony.flex.DP);
            var breadcrumbs = new com.adminConsole.common.breadcrumbs({
                "height": "20px",
                "id": "breadcrumbs",
                "isVisible": true,
                "layoutType": kony.flex.FLOW_HORIZONTAL,
                "left": "0dp",
                "masterType": constants.MASTER_TYPE_DEFAULT,
                "isModalContainer": false,
                "skin": "sknflxf5f6f8Op100",
                "top": "0dp",
                "width": "100%",
                "overrides": {
                    "breadcrumbs": {
                        "isVisible": true
                    },
                    "btnBackToMain": {
                        "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmCompanies.COMPANIES\")",
                        "isVisible": true
                    },
                    "btnPreviousPage": {
                        "isVisible": false
                    },
                    "fontIconBreadcrumbsRight": {
                        "left": "8dp",
                        "right": "8dp"
                    },
                    "fontIconBreadcrumbsRight2": {
                        "isVisible": false,
                        "left": "8dp",
                        "right": "8dp"
                    },
                    "imgBreadcrumbsDown": {
                        "src": "img_down_arrow.png"
                    },
                    "imgBreadcrumbsRight": {
                        "src": "img_breadcrumb_arrow.png"
                    },
                    "imgBreadcrumbsRight2": {
                        "isVisible": false,
                        "src": "img_breadcrumb_arrow.png"
                    },
                    "imgBreadcrumbsRight3": {
                        "src": "img_breadcrumb_arrow.png"
                    },
                    "lblCurrentScreen": {
                        "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmBusinessTypes.Business_Type_List_UC\")"
                    }
                }
            }, {
                "overrides": {}
            }, {
                "overrides": {}
            });
            flxBreadcrumb.add(breadcrumbs);
            var flxMainContent = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "bottom": "0dp",
                "clipBounds": true,
                "id": "flxMainContent",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "137dp",
                "width": "100%"
            }, {}, {});
            flxMainContent.setDefaultUnit(kony.flex.DP);
            var flxBusinessTypesList = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "100%",
                "id": "flxBusinessTypesList",
                "isVisible": false,
                "layoutType": kony.flex.FREE_FORM,
                "left": "35dp",
                "isModalContainer": false,
                "right": "35dp",
                "skin": "slFbox",
                "top": "0dp"
            }, {}, {});
            flxBusinessTypesList.setDefaultUnit(kony.flex.DP);
            var flxSeparator = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                "clipBounds": true,
                "id": "flxSeparator",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0dp",
                "width": "100%"
            }, {}, {});
            flxSeparator.setDefaultUnit(kony.flex.DP);
            var flxSeparatorVal = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "bottom": "1dp",
                "clipBounds": true,
                "height": "1dp",
                "id": "flxSeparatorVal",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "isModalContainer": false,
                "right": "0dp",
                "skin": "sknflx115678",
                "zIndex": 1
            }, {}, {});
            flxSeparatorVal.setDefaultUnit(kony.flex.DP);
            flxSeparatorVal.add();
            flxSeparator.add(flxSeparatorVal);
            var flxBusinessTypeSearch = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "60dp",
                "id": "flxBusinessTypeSearch",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0dp",
                "width": "100%",
                "zIndex": 1
            }, {}, {});
            flxBusinessTypeSearch.setDefaultUnit(kony.flex.DP);
            var subHeader = new com.adminConsole.header.subHeader({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "centerY": "50%",
                "height": "53dp",
                "id": "subHeader",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "masterType": constants.MASTER_TYPE_DEFAULT,
                "isModalContainer": false,
                "skin": "CopyslFbox2",
                "top": "0dp",
                "width": "100%",
                "overrides": {
                    "flxMenu": {
                        "isVisible": false,
                        "left": "0dp"
                    },
                    "flxSearch": {
                        "right": 0
                    },
                    "flxSearchContainer": {
                        "height": "40px"
                    },
                    "subHeader": {
                        "centerY": "50%",
                        "height": "53dp"
                    },
                    "tbxSearchBox": {
                        "i18n_placeholder": "kony.i18n.getLocalizedString(\"i18n.frmBusinessTypes.search_by_business_type\")"
                    }
                }
            }, {
                "overrides": {}
            }, {
                "overrides": {}
            });
            flxBusinessTypeSearch.add(subHeader);
            var flxSegBusinessTypes = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                "bottom": "10dp",
                "clipBounds": true,
                "id": "flxSegBusinessTypes",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "isModalContainer": false,
                "right": "0dp",
                "skin": "sknflxffffffOp100BorderE1E5Ed",
                "top": "60dp",
                "width": "100%"
            }, {}, {});
            flxSegBusinessTypes.setDefaultUnit(kony.flex.DP);
            var flxHeader = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "60dp",
                "id": "flxHeader",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "30dp",
                "isModalContainer": false,
                "right": "25dp",
                "skin": "slFbox",
                "top": "0"
            }, {}, {});
            flxHeader.setDefaultUnit(kony.flex.DP);
            var flxBusinessType = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "100%",
                "id": "flxBusinessType",
                "isVisible": true,
                "layoutType": kony.flex.FLOW_HORIZONTAL,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0",
                "width": "35%"
            }, {}, {
                "hoverSkin": "sknFlxPointer"
            });
            flxBusinessType.setDefaultUnit(kony.flex.DP);
            var lblBusinessType = new kony.ui.Label({
                "centerY": "50%",
                "id": "lblBusinessType",
                "isVisible": true,
                "left": "0",
                "skin": "sknlblLato696c7311px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmBusinessTypes.business_types_UC\")",
                "top": "0",
                "width": kony.flex.USE_PREFFERED_SIZE
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var fontIconSortBusinessType = new kony.ui.Label({
                "centerY": "50%",
                "id": "fontIconSortBusinessType",
                "isVisible": true,
                "left": "3dp",
                "skin": "sknIcon15px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmAlertsManagement.lblSortName\")",
                "top": "0dp",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxBusinessType.add(lblBusinessType, fontIconSortBusinessType);
            var flxAuthorizedSignatories = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "100%",
                "id": "flxAuthorizedSignatories",
                "isVisible": true,
                "layoutType": kony.flex.FLOW_HORIZONTAL,
                "left": "37%",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0",
                "width": "35%"
            }, {}, {
                "hoverSkin": "sknFlxPointer"
            });
            flxAuthorizedSignatories.setDefaultUnit(kony.flex.DP);
            var lblAuthorizedSignatories = new kony.ui.Label({
                "centerY": "50%",
                "id": "lblAuthorizedSignatories",
                "isVisible": true,
                "left": "0",
                "skin": "sknlblLato696c7311px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmBusinessTypes.authorized_signatories_UC\")",
                "top": "0",
                "width": kony.flex.USE_PREFFERED_SIZE
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var fontIconSortAuthorizedSignatories = new kony.ui.Label({
                "centerY": "50%",
                "id": "fontIconSortAuthorizedSignatories",
                "isVisible": true,
                "left": "3dp",
                "skin": "sknIcon15px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmAlertsManagement.lblSortName\")",
                "top": "0dp",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxAuthorizedSignatories.add(lblAuthorizedSignatories, fontIconSortAuthorizedSignatories);
            var flxNoOfRoles = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "100%",
                "id": "flxNoOfRoles",
                "isVisible": true,
                "layoutType": kony.flex.FLOW_HORIZONTAL,
                "left": "74%",
                "isModalContainer": false,
                "skin": "sknFlxPointer",
                "top": "0",
                "width": "20%"
            }, {}, {});
            flxNoOfRoles.setDefaultUnit(kony.flex.DP);
            var lblNoOfRoles = new kony.ui.Label({
                "centerY": "50%",
                "id": "lblNoOfRoles",
                "isVisible": true,
                "left": "0",
                "skin": "sknlblLato696c7311px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmBusinessTypes.no_of_roles_UC\")",
                "top": "0",
                "width": kony.flex.USE_PREFFERED_SIZE
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var fontIconSortNoOfRoles = new kony.ui.Label({
                "centerY": "50%",
                "id": "fontIconSortNoOfRoles",
                "isVisible": true,
                "left": "3dp",
                "skin": "sknIcon15px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmAlertsManagement.lblSortName\")",
                "top": "0dp",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxNoOfRoles.add(lblNoOfRoles, fontIconSortNoOfRoles);
            flxHeader.add(flxBusinessType, flxAuthorizedSignatories, flxNoOfRoles);
            var flxSegHeaderSeparator = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "1dp",
                "id": "flxSegHeaderSeparator",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "20dp",
                "isModalContainer": false,
                "right": "35dp",
                "skin": "sknTableHeaderLine",
                "top": "60dp",
                "zIndex": 1
            }, {}, {});
            flxSegHeaderSeparator.setDefaultUnit(kony.flex.DP);
            flxSegHeaderSeparator.add();
            var flxBusinessTypesSegment = new kony.ui.FlexScrollContainer({
                "allowHorizontalBounce": false,
                "allowVerticalBounce": true,
                "bounces": true,
                "clipBounds": true,
                "enableScrolling": true,
                "height": "240dp",
                "horizontalScrollIndicator": true,
                "id": "flxBusinessTypesSegment",
                "isVisible": true,
                "layoutType": kony.flex.FLOW_VERTICAL,
                "left": "20dp",
                "pagingEnabled": false,
                "right": "0dp",
                "scrollDirection": kony.flex.SCROLL_VERTICAL,
                "skin": "slFSbox",
                "top": "61dp",
                "verticalScrollIndicator": true
            }, {}, {});
            flxBusinessTypesSegment.setDefaultUnit(kony.flex.DP);
            var segBusinessTypes = new kony.ui.SegmentedUI2({
                "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                "data": [{
                    "lblAuthorizedSignatories": "Owner",
                    "lblBusinessTypes": "Sole Properietor",
                    "lblIconOptions": "",
                    "lblMore": "+4 more",
                    "lblNoOfRoles": "07",
                    "lblSeperator": "."
                }, {
                    "lblAuthorizedSignatories": "Owner",
                    "lblBusinessTypes": "Sole Properietor",
                    "lblIconOptions": "",
                    "lblMore": "+4 more",
                    "lblNoOfRoles": "07",
                    "lblSeperator": "."
                }, {
                    "lblAuthorizedSignatories": "Owner",
                    "lblBusinessTypes": "Sole Properietor",
                    "lblIconOptions": "",
                    "lblMore": "+4 more",
                    "lblNoOfRoles": "07",
                    "lblSeperator": "."
                }, {
                    "lblAuthorizedSignatories": "Owner",
                    "lblBusinessTypes": "Sole Properietor",
                    "lblIconOptions": "",
                    "lblMore": "+4 more",
                    "lblNoOfRoles": "07",
                    "lblSeperator": "."
                }, {
                    "lblAuthorizedSignatories": "Owner",
                    "lblBusinessTypes": "Sole Properietor",
                    "lblIconOptions": "",
                    "lblMore": "+4 more",
                    "lblNoOfRoles": "07",
                    "lblSeperator": "."
                }, {
                    "lblAuthorizedSignatories": "Owner",
                    "lblBusinessTypes": "Sole Properietor",
                    "lblIconOptions": "",
                    "lblMore": "+4 more",
                    "lblNoOfRoles": "07",
                    "lblSeperator": "."
                }, {
                    "lblAuthorizedSignatories": "Owner",
                    "lblBusinessTypes": "Sole Properietor",
                    "lblIconOptions": "",
                    "lblMore": "+4 more",
                    "lblNoOfRoles": "07",
                    "lblSeperator": "."
                }, {
                    "lblAuthorizedSignatories": "Owner",
                    "lblBusinessTypes": "Sole Properietor",
                    "lblIconOptions": "",
                    "lblMore": "+4 more",
                    "lblNoOfRoles": "07",
                    "lblSeperator": "."
                }, {
                    "lblAuthorizedSignatories": "Owner",
                    "lblBusinessTypes": "Sole Properietor",
                    "lblIconOptions": "",
                    "lblMore": "+4 more",
                    "lblNoOfRoles": "07",
                    "lblSeperator": "."
                }, {
                    "lblAuthorizedSignatories": "Owner",
                    "lblBusinessTypes": "Sole Properietor",
                    "lblIconOptions": "",
                    "lblMore": "+4 more",
                    "lblNoOfRoles": "07",
                    "lblSeperator": "."
                }],
                "groupCells": false,
                "id": "segBusinessTypes",
                "isVisible": true,
                "left": "0",
                "needPageIndicator": true,
                "pageOffDotImage": "pageoffdot.png",
                "pageOnDotImage": "pageondot.png",
                "retainSelection": false,
                "right": "0dp",
                "rowFocusSkin": "seg2Focus",
                "rowSkin": "seg2Normal",
                "rowTemplate": "flxBusinessTypes",
                "sectionHeaderSkin": "sliPhoneSegmentHeader",
                "selectionBehavior": constants.SEGUI_DEFAULT_BEHAVIOR,
                "separatorRequired": false,
                "showScrollbars": false,
                "top": "0",
                "viewType": constants.SEGUI_VIEW_TYPE_TABLEVIEW,
                "widgetDataMap": {
                    "flxAuthSignatoryNames": "flxAuthSignatoryNames",
                    "flxBusinessTypes": "flxBusinessTypes",
                    "flxContent": "flxContent",
                    "flxMore": "flxMore",
                    "flxOptions": "flxOptions",
                    "flxRow": "flxRow",
                    "lblAuthorizedSignatories": "lblAuthorizedSignatories",
                    "lblBusinessTypes": "lblBusinessTypes",
                    "lblIconOptions": "lblIconOptions",
                    "lblMore": "lblMore",
                    "lblNoOfRoles": "lblNoOfRoles",
                    "lblSeperator": "lblSeperator"
                },
                "width": "100%"
            }, {
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxBusinessTypesSegment.add(segBusinessTypes);
            var flxSelectOptions = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                "clipBounds": false,
                "id": "flxSelectOptions",
                "isVisible": false,
                "layoutType": kony.flex.FLOW_VERTICAL,
                "isModalContainer": false,
                "right": "20px",
                "skin": "slFbox",
                "top": "100px",
                "width": "108px",
                "zIndex": 100
            }, {}, {});
            flxSelectOptions.setDefaultUnit(kony.flex.DP);
            var flxUpArrowImage = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "12px",
                "id": "flxUpArrowImage",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0px",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0px",
                "width": "100%",
                "zIndex": 2
            }, {}, {});
            flxUpArrowImage.setDefaultUnit(kony.flex.DP);
            var imgUpArrow = new kony.ui.Image2({
                "height": "12dp",
                "id": "imgUpArrow",
                "isVisible": true,
                "right": "20dp",
                "skin": "slImage",
                "src": "uparrow_2x.png",
                "top": "0dp",
                "width": "15dp",
                "zIndex": 1
            }, {
                "imageScaleMode": constants.IMAGE_SCALE_MODE_FIT_TO_DIMENSIONS,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxUpArrowImage.add(imgUpArrow);
            var flxSelectOptionsInner = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                "clipBounds": true,
                "id": "flxSelectOptionsInner",
                "isVisible": true,
                "layoutType": kony.flex.FLOW_VERTICAL,
                "left": "0px",
                "isModalContainer": false,
                "skin": "sknflxffffffop100dbdbe6Radius3px",
                "top": "0px",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {}, {});
            flxSelectOptionsInner.setDefaultUnit(kony.flex.DP);
            var flxEdit = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "30dp",
                "id": "flxEdit",
                "isVisible": true,
                "layoutType": kony.flex.FLOW_HORIZONTAL,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "CopyslFbox0ca7ba3b92f9949",
                "top": "13dp",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {}, {
                "hoverSkin": "sknContextualMenuEntryHover2"
            });
            flxEdit.setDefaultUnit(kony.flex.DP);
            var lblIconOption2 = new kony.ui.Label({
                "centerY": "50%",
                "height": "20px",
                "id": "lblIconOption2",
                "isVisible": true,
                "left": "20px",
                "skin": "sknIcon20px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmFAQ.fonticonEdit\")",
                "top": "0dp",
                "width": "20px",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var lblOption1 = new kony.ui.Label({
                "centerY": "50%",
                "id": "lblOption1",
                "isVisible": true,
                "left": "10dp",
                "skin": "sknlblLatoBold35475f14px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.roles.Edit\")",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxEdit.add(lblIconOption2, lblOption1);
            var flxDelete = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "bottom": "13dp",
                "clipBounds": true,
                "height": "30dp",
                "id": "flxDelete",
                "isVisible": true,
                "layoutType": kony.flex.FLOW_HORIZONTAL,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "CopyslFbox0b2deaad0a9f24f",
                "top": "5dp",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {}, {
                "hoverSkin": "sknContextualMenuEntryHover2"
            });
            flxDelete.setDefaultUnit(kony.flex.DP);
            var lblIconOption3 = new kony.ui.Label({
                "centerY": "50%",
                "id": "lblIconOption3",
                "isVisible": true,
                "left": "20px",
                "skin": "sknIcon20px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmFAQ.fonticonDelete\")",
                "top": "0dp",
                "width": "20px",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var lblOption3 = new kony.ui.Label({
                "centerY": "50%",
                "id": "lblOption3",
                "isVisible": true,
                "left": "10dp",
                "skin": "sknlblLatoBold35475f14px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.SecurityQuestions.Delete\")",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxDelete.add(lblIconOption3, lblOption3);
            flxSelectOptionsInner.add(flxEdit, flxDelete);
            var flxDownArrowImage = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "12px",
                "id": "flxDownArrowImage",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0px",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "-1px",
                "width": "100%",
                "zIndex": 2
            }, {}, {});
            flxDownArrowImage.setDefaultUnit(kony.flex.DP);
            var imgDownArrow = new kony.ui.Image2({
                "height": "12dp",
                "id": "imgDownArrow",
                "isVisible": true,
                "right": "20dp",
                "skin": "slImage",
                "src": "downarrow_2x.png",
                "top": "0dp",
                "width": "15dp",
                "zIndex": 1
            }, {
                "imageScaleMode": constants.IMAGE_SCALE_MODE_FIT_TO_DIMENSIONS,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxDownArrowImage.add(imgDownArrow);
            flxSelectOptions.add(flxUpArrowImage, flxSelectOptionsInner, flxDownArrowImage);
            var flxAdjustSegMore = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                "clipBounds": true,
                "id": "flxAdjustSegMore",
                "isVisible": false,
                "layoutType": kony.flex.FREE_FORM,
                "left": "30dp",
                "isModalContainer": false,
                "right": "25dp",
                "skin": "slFbox",
                "top": "190dp"
            }, {}, {});
            flxAdjustSegMore.setDefaultUnit(kony.flex.DP);
            var flxSegMore = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                "clipBounds": true,
                "id": "flxSegMore",
                "isVisible": true,
                "layoutType": kony.flex.FLOW_VERTICAL,
                "left": "37%",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0px",
                "width": "250px",
                "zIndex": 5
            }, {}, {});
            flxSegMore.setDefaultUnit(kony.flex.DP);
            var flxUp = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "12px",
                "id": "flxUp",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0px",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0px",
                "width": "100%",
                "zIndex": 2
            }, {}, {});
            flxUp.setDefaultUnit(kony.flex.DP);
            var imgUp = new kony.ui.Image2({
                "height": "12dp",
                "id": "imgUp",
                "isVisible": true,
                "left": "10dp",
                "skin": "slImage",
                "src": "uparrow_2x.png",
                "top": "0dp",
                "width": "15dp",
                "zIndex": 1
            }, {
                "imageScaleMode": constants.IMAGE_SCALE_MODE_FIT_TO_DIMENSIONS,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxUp.add(imgUp);
            var flxOuter = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                "clipBounds": true,
                "id": "flxOuter",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "sknflxffffffop100dbdbe6Radius3px",
                "top": "-1dp",
                "width": "100%",
                "zIndex": 1
            }, {}, {});
            flxOuter.setDefaultUnit(kony.flex.DP);
            var segMore = new kony.ui.SegmentedUI2({
                "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                "bottom": 10,
                "data": [{
                    "lblName": "Granville Street, Vancover, BC, Canada"
                }, {
                    "lblName": "Granville Street, Vancover, BC, Canada"
                }, {
                    "lblName": "Granville Street, Vancover, BC, Canada"
                }],
                "groupCells": false,
                "id": "segMore",
                "isVisible": true,
                "left": "15dp",
                "needPageIndicator": true,
                "pageOffDotImage": "pageoffdot.png",
                "pageOnDotImage": "pageondot.png",
                "retainSelection": false,
                "right": "15px",
                "rowFocusSkin": "seg2Focus",
                "rowSkin": "seg2Normal",
                "rowTemplate": "flxMore",
                "sectionHeaderSkin": "sliPhoneSegmentHeader",
                "selectionBehavior": constants.SEGUI_DEFAULT_BEHAVIOR,
                "separatorColor": "aaaaaa00",
                "separatorRequired": false,
                "separatorThickness": 0,
                "showScrollbars": false,
                "top": "0dp",
                "viewType": constants.SEGUI_VIEW_TYPE_TABLEVIEW,
                "widgetDataMap": {
                    "flxMore": "flxMore",
                    "lblName": "lblName"
                },
                "zIndex": 1
            }, {
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxOuter.add(segMore);
            var flxDown = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "12px",
                "id": "flxDown",
                "isVisible": false,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0px",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "-1px",
                "width": "100%",
                "zIndex": 2
            }, {}, {});
            flxDown.setDefaultUnit(kony.flex.DP);
            var imgDown = new kony.ui.Image2({
                "height": "12dp",
                "id": "imgDown",
                "isVisible": true,
                "left": "10dp",
                "skin": "slImage",
                "src": "downarrow_2x.png",
                "top": "0dp",
                "width": "15dp",
                "zIndex": 1
            }, {
                "imageScaleMode": constants.IMAGE_SCALE_MODE_FIT_TO_DIMENSIONS,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxDown.add(imgDown);
            flxSegMore.add(flxUp, flxOuter, flxDown);
            flxAdjustSegMore.add(flxSegMore);
            flxSegBusinessTypes.add(flxHeader, flxSegHeaderSeparator, flxBusinessTypesSegment, flxSelectOptions, flxAdjustSegMore);
            var flxNoBusinessTypesFound = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                "bottom": "25dp",
                "clipBounds": true,
                "id": "flxNoBusinessTypesFound",
                "isVisible": false,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "isModalContainer": false,
                "right": "0dp",
                "skin": "sknflxffffffOp100BorderE1E5Ed",
                "top": "60dp",
                "width": "100%"
            }, {}, {});
            flxNoBusinessTypesFound.setDefaultUnit(kony.flex.DP);
            var lblNoBusinessTypesFound = new kony.ui.Label({
                "centerX": "50%",
                "centerY": "50%",
                "id": "lblNoBusinessTypesFound",
                "isVisible": true,
                "left": "0dp",
                "skin": "slLabel0d20174dce8ea42",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmLogs.rtxNoResultsFound\")",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_CENTER,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxNoBusinessTypesFound.add(lblNoBusinessTypesFound);
            flxBusinessTypesList.add(flxSeparator, flxBusinessTypeSearch, flxSegBusinessTypes, flxNoBusinessTypesFound);
            var flxViewEditBusinessTypes = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "bottom": "10dp",
                "clipBounds": true,
                "id": "flxViewEditBusinessTypes",
                "isVisible": false,
                "layoutType": kony.flex.FREE_FORM,
                "reverseLayoutDirection": false,
                "left": 35,
                "isModalContainer": false,
                "right": "35dp",
                "skin": "sknflxffffffOp100BorderE1E5Ed",
                "top": "0"
            }, {}, {});
            flxViewEditBusinessTypes.setDefaultUnit(kony.flex.DP);
            var flxBusinessTypeViewEdit = new kony.ui.FlexScrollContainer({
                "allowHorizontalBounce": false,
                "allowVerticalBounce": true,
                "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                "bottom": "80px",
                "bounces": true,
                "clipBounds": true,
                "enableScrolling": true,
                "horizontalScrollIndicator": true,
                "id": "flxBusinessTypeViewEdit",
                "isVisible": true,
                "layoutType": kony.flex.FLOW_VERTICAL,
                "left": "0dp",
                "pagingEnabled": false,
                "right": "0px",
                "scrollDirection": kony.flex.SCROLL_VERTICAL,
                "skin": "slFSbox",
                "top": "0dp",
                "verticalScrollIndicator": true,
                "zIndex": 1
            }, {}, {});
            flxBusinessTypeViewEdit.setDefaultUnit(kony.flex.DP);
            var flxBusinessTypeViewDetails = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                "clipBounds": false,
                "id": "flxBusinessTypeViewDetails",
                "isVisible": true,
                "layoutType": kony.flex.FLOW_VERTICAL,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0dp",
                "width": "100%",
                "zIndex": 2
            }, {}, {});
            flxBusinessTypeViewDetails.setDefaultUnit(kony.flex.DP);
            var flxViewContainer = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                "clipBounds": false,
                "id": "flxViewContainer",
                "isVisible": true,
                "layoutType": kony.flex.FLOW_VERTICAL,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0dp",
                "width": "100%",
                "zIndex": 1
            }, {}, {});
            flxViewContainer.setDefaultUnit(kony.flex.DP);
            var flxViewHeader = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": false,
                "height": "60dp",
                "id": "flxViewHeader",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0px",
                "isModalContainer": false,
                "right": "35px",
                "skin": "slFbox",
                "top": "0px",
                "zIndex": 2
            }, {}, {});
            flxViewHeader.setDefaultUnit(kony.flex.DP);
            var flxBusinessTypeNameHeader = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                "centerY": "50%",
                "clipBounds": true,
                "id": "flxBusinessTypeNameHeader",
                "isVisible": true,
                "layoutType": kony.flex.FLOW_HORIZONTAL,
                "left": "20dp",
                "isModalContainer": false,
                "right": "50dp",
                "skin": "slFbox",
                "width": kony.flex.USE_PREFFERED_SIZE
            }, {}, {});
            flxBusinessTypeNameHeader.setDefaultUnit(kony.flex.DP);
            var lblName = new kony.ui.Label({
                "id": "lblName",
                "isVisible": true,
                "left": "0dp",
                "skin": "sknLbl192B4518pxLatoReg",
                "text": "Sole Properietor",
                "top": "0dp",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxBusinessTypeNameHeader.add(lblName);
            var flxOptions = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "centerY": "50%",
                "clipBounds": true,
                "height": "24dp",
                "id": "flxOptions",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "isModalContainer": false,
                "right": "20px",
                "skin": "sknFlxBorffffff1pxRound",
                "top": "0px",
                "width": "24px",
                "zIndex": 3
            }, {}, {
                "hoverSkin": "sknflxffffffop100Border424242Radius100px"
            });
            flxOptions.setDefaultUnit(kony.flex.DP);
            var lblIconOptions = new kony.ui.Label({
                "centerX": "50%",
                "centerY": "50%",
                "id": "lblIconOptions",
                "isVisible": true,
                "skin": "sknFontIconOptionMenu",
                "text": "",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxOptions.add(lblIconOptions);
            var flxSelectOptionsView = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                "clipBounds": false,
                "id": "flxSelectOptionsView",
                "isVisible": false,
                "layoutType": kony.flex.FLOW_VERTICAL,
                "isModalContainer": false,
                "right": "4px",
                "skin": "slFbox",
                "top": "40px",
                "width": "108px",
                "zIndex": 100
            }, {}, {});
            flxSelectOptionsView.setDefaultUnit(kony.flex.DP);
            var flxUpArrowImageView = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "12px",
                "id": "flxUpArrowImageView",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0px",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0px",
                "width": "100%",
                "zIndex": 2
            }, {}, {});
            flxUpArrowImageView.setDefaultUnit(kony.flex.DP);
            var imgUpArrowView = new kony.ui.Image2({
                "height": "12dp",
                "id": "imgUpArrowView",
                "isVisible": true,
                "right": "20dp",
                "skin": "slImage",
                "src": "uparrow_2x.png",
                "top": "0dp",
                "width": "15dp",
                "zIndex": 1
            }, {
                "imageScaleMode": constants.IMAGE_SCALE_MODE_FIT_TO_DIMENSIONS,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxUpArrowImageView.add(imgUpArrowView);
            var flxSelectOptionsInnerView = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                "clipBounds": true,
                "id": "flxSelectOptionsInnerView",
                "isVisible": true,
                "layoutType": kony.flex.FLOW_VERTICAL,
                "left": "0px",
                "isModalContainer": false,
                "skin": "sknflxffffffop100dbdbe6Radius3px",
                "top": "0px",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {}, {});
            flxSelectOptionsInnerView.setDefaultUnit(kony.flex.DP);
            var flxEditView = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "30dp",
                "id": "flxEditView",
                "isVisible": true,
                "layoutType": kony.flex.FLOW_HORIZONTAL,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "CopyslFbox0ca7ba3b92f9949",
                "top": "13dp",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {}, {
                "hoverSkin": "sknContextualMenuEntryHover2"
            });
            flxEditView.setDefaultUnit(kony.flex.DP);
            var lblIconOptionEdit = new kony.ui.Label({
                "centerY": "50%",
                "height": "20px",
                "id": "lblIconOptionEdit",
                "isVisible": true,
                "left": "20px",
                "skin": "sknIcon20px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmFAQ.fonticonEdit\")",
                "top": "0dp",
                "width": "20px",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var lblOptionEdit = new kony.ui.Label({
                "centerY": "50%",
                "id": "lblOptionEdit",
                "isVisible": true,
                "left": "10dp",
                "skin": "sknlblLatoBold35475f14px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.roles.Edit\")",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxEditView.add(lblIconOptionEdit, lblOptionEdit);
            var flxDeleteView = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "bottom": "13dp",
                "clipBounds": true,
                "height": "30dp",
                "id": "flxDeleteView",
                "isVisible": true,
                "layoutType": kony.flex.FLOW_HORIZONTAL,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "CopyslFbox0b2deaad0a9f24f",
                "top": "5dp",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {}, {
                "hoverSkin": "sknContextualMenuEntryHover2"
            });
            flxDeleteView.setDefaultUnit(kony.flex.DP);
            var lblIconOptionDelete = new kony.ui.Label({
                "centerY": "50%",
                "id": "lblIconOptionDelete",
                "isVisible": true,
                "left": "20px",
                "skin": "sknIcon20px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmFAQ.fonticonDelete\")",
                "top": "0dp",
                "width": "20px",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var lblOptionDelete = new kony.ui.Label({
                "centerY": "50%",
                "id": "lblOptionDelete",
                "isVisible": true,
                "left": "10dp",
                "skin": "sknlblLatoBold35475f14px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.SecurityQuestions.Delete\")",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxDeleteView.add(lblIconOptionDelete, lblOptionDelete);
            flxSelectOptionsInnerView.add(flxEditView, flxDeleteView);
            flxSelectOptionsView.add(flxUpArrowImageView, flxSelectOptionsInnerView);
            var flxViewSeparator1 = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                "bottom": "0dp",
                "clipBounds": true,
                "id": "flxViewSeparator1",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "slFbox",
                "width": "100%",
                "zIndex": 1
            }, {}, {});
            flxViewSeparator1.setDefaultUnit(kony.flex.DP);
            var flxViewSeparatorVal1 = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "bottom": "0dp",
                "clipBounds": true,
                "height": "1dp",
                "id": "flxViewSeparatorVal1",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "20dp",
                "isModalContainer": false,
                "right": "20dp",
                "skin": "sknflxffffffOp100BorderE1E5EdNoRadius",
                "zIndex": 1
            }, {}, {});
            flxViewSeparatorVal1.setDefaultUnit(kony.flex.DP);
            flxViewSeparatorVal1.add();
            flxViewSeparator1.add(flxViewSeparatorVal1);
            flxViewHeader.add(flxBusinessTypeNameHeader, flxOptions, flxSelectOptionsView, flxViewSeparator1);
            var flxViewContent = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                "bottom": "20dp",
                "clipBounds": true,
                "id": "flxViewContent",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "isModalContainer": false,
                "right": "20dp",
                "skin": "slFbox",
                "top": "0dp",
                "width": "100%"
            }, {}, {});
            flxViewContent.setDefaultUnit(kony.flex.DP);
            var flxEditDisableSkin = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "25dp",
                "id": "flxEditDisableSkin",
                "isVisible": false,
                "layoutType": kony.flex.FREE_FORM,
                "isModalContainer": false,
                "right": "35dp",
                "skin": "sknflxbgffffffOp50Per",
                "top": "15dp",
                "width": "60dp",
                "zIndex": 2
            }, {}, {
                "hoverSkin": "sknCursorDisabled"
            });
            flxEditDisableSkin.setDefaultUnit(kony.flex.DP);
            flxEditDisableSkin.add();
            var flxAuthSIgnatoriesViewName = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                "clipBounds": true,
                "id": "flxAuthSIgnatoriesViewName",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "20dp",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "15dp",
                "width": "31%",
                "zIndex": 1
            }, {}, {});
            flxAuthSIgnatoriesViewName.setDefaultUnit(kony.flex.DP);
            var lblAuthSignatoriesName = new kony.ui.Label({
                "id": "lblAuthSignatoriesName",
                "isVisible": true,
                "left": "0dp",
                "skin": "sknlblLato696c7311px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmBusinessTypes.authorized_signatories_UC\")",
                "top": "1dp",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var flxAuthSignatoriesMore = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                "clipBounds": true,
                "id": "flxAuthSignatoriesMore",
                "isVisible": true,
                "layoutType": kony.flex.FLOW_VERTICAL,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0dp",
                "width": "100%"
            }, {}, {});
            flxAuthSignatoriesMore.setDefaultUnit(kony.flex.DP);
            var lblAuthSignatoriesValue = new kony.ui.Label({
                "id": "lblAuthSignatoriesValue",
                "isVisible": true,
                "left": "0px",
                "skin": "sknlbl485C75LatoSemiBold13px",
                "text": "Admin Role",
                "top": "25px",
                "width": "100%",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var flxMoreView = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "15px",
                "id": "flxMoreView",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "sknCursor",
                "top": "2dp",
                "width": "60dp",
                "zIndex": 10
            }, {}, {
                "hoverSkin": "hoverhandSkin2"
            });
            flxMoreView.setDefaultUnit(kony.flex.DP);
            var lblMore = new kony.ui.Label({
                "centerY": "50%",
                "id": "lblMore",
                "isVisible": true,
                "left": "0dp",
                "skin": "sknLblLato13px117eb0",
                "text": "+4 more",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_CENTER,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxMoreView.add(lblMore);
            flxAuthSignatoriesMore.add(lblAuthSignatoriesValue, flxMoreView);
            flxAuthSIgnatoriesViewName.add(lblAuthSignatoriesName, flxAuthSignatoriesMore);
            var flxMinAuthSIgnatories = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "50dp",
                "id": "flxMinAuthSIgnatories",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "35%",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "15dp",
                "width": "31%",
                "zIndex": 1
            }, {}, {});
            flxMinAuthSIgnatories.setDefaultUnit(kony.flex.DP);
            var lblMinAuthSIgnatories = new kony.ui.Label({
                "id": "lblMinAuthSIgnatories",
                "isVisible": true,
                "left": "0dp",
                "skin": "sknlblLato696c7311px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmBusinessTypes.min_no_ofauthorized_signatories_UC\")",
                "top": "1dp",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var lblMinAuthSIgnatoriesValue = new kony.ui.Label({
                "id": "lblMinAuthSIgnatoriesValue",
                "isVisible": true,
                "left": "0px",
                "skin": "sknlbl485C75LatoSemiBold13px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmGroups.lblViewNameValue\")",
                "top": "25px",
                "width": "100%",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxMinAuthSIgnatories.add(lblMinAuthSIgnatories, lblMinAuthSIgnatoriesValue);
            var flxMaxAuthSIgnatories = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "50dp",
                "id": "flxMaxAuthSIgnatories",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "68%",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "15dp",
                "width": "31%",
                "zIndex": 1
            }, {}, {});
            flxMaxAuthSIgnatories.setDefaultUnit(kony.flex.DP);
            var lblMaxAuthSIgnatories = new kony.ui.Label({
                "id": "lblMaxAuthSIgnatories",
                "isVisible": true,
                "left": "0dp",
                "skin": "sknlblLato696c7311px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmBusinessTypes.max_no_ofauthorized_signatories_UC\")",
                "top": "1dp",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var lblMaxAuthSIgnatoriesValue = new kony.ui.Label({
                "id": "lblMaxAuthSIgnatoriesValue",
                "isVisible": true,
                "left": "0px",
                "skin": "sknlbl485C75LatoSemiBold13px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmGroups.lblViewNameValue\")",
                "top": "25px",
                "width": "100%",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxMaxAuthSIgnatories.add(lblMaxAuthSIgnatories, lblMaxAuthSIgnatoriesValue);
            flxViewContent.add(flxEditDisableSkin, flxAuthSIgnatoriesViewName, flxMinAuthSIgnatories, flxMaxAuthSIgnatories);
            flxViewContainer.add(flxViewHeader, flxViewContent);
            flxBusinessTypeViewDetails.add(flxViewContainer);
            var flxBusinessTypeEditDetails = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                "clipBounds": true,
                "id": "flxBusinessTypeEditDetails",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0dp",
                "width": "100%"
            }, {}, {});
            flxBusinessTypeEditDetails.setDefaultUnit(kony.flex.DP);
            var flxBusinessTypeEditDetailsContainer = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                "clipBounds": false,
                "id": "flxBusinessTypeEditDetailsContainer",
                "isVisible": true,
                "layoutType": kony.flex.FLOW_VERTICAL,
                "left": "20dp",
                "isModalContainer": false,
                "right": "20dp",
                "skin": "slFbox",
                "top": "20dp",
                "zIndex": 2
            }, {}, {});
            flxBusinessTypeEditDetailsContainer.setDefaultUnit(kony.flex.DP);
            var flxEditRow1 = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "95dp",
                "id": "flxEditRow1",
                "isVisible": true,
                "layoutType": kony.flex.FLOW_HORIZONTAL,
                "left": "0dp",
                "isModalContainer": false,
                "right": "20dp",
                "skin": "slFbox",
                "top": "0dp",
                "width": "100%",
                "zIndex": 1
            }, {}, {});
            flxEditRow1.setDefaultUnit(kony.flex.DP);
            var flxEditRow11 = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "100%",
                "id": "flxEditRow11",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "isModalContainer": false,
                "right": "0dp",
                "skin": "slFbox",
                "top": "0dp",
                "width": "49%",
                "zIndex": 1
            }, {}, {});
            flxEditRow11.setDefaultUnit(kony.flex.DP);
            var flxEditBusinessTypeName = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                "clipBounds": true,
                "id": "flxEditBusinessTypeName",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0dp",
                "width": "100%"
            }, {}, {});
            flxEditBusinessTypeName.setDefaultUnit(kony.flex.DP);
            var lblBusinessTypeNameEdit = new kony.ui.Label({
                "id": "lblBusinessTypeNameEdit",
                "isVisible": true,
                "left": "0dp",
                "skin": "sknLbl485C75LatoRegular13Px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmBusinessTypes.BusinessType\")",
                "top": "0dp",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var lblBusinessTypeNameCount = new kony.ui.Label({
                "id": "lblBusinessTypeNameCount",
                "isVisible": false,
                "right": "2dp",
                "skin": "slLabel0d20174dce8ea42",
                "text": "0/50",
                "top": "0dp",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxEditBusinessTypeName.add(lblBusinessTypeNameEdit, lblBusinessTypeNameCount);
            var tbxEditBusinessTypeName = new kony.ui.TextBox2({
                "autoCapitalize": constants.TEXTBOX_AUTO_CAPITALIZE_NONE,
                "focusSkin": "skntbxLato35475f14px",
                "height": "40dp",
                "id": "tbxEditBusinessTypeName",
                "isVisible": true,
                "keyBoardStyle": constants.TEXTBOX_KEY_BOARD_STYLE_DEFAULT,
                "left": "0dp",
                "maxTextLength": 50,
                "i18n_placeholder": "kony.i18n.getLocalizedString(\"i18n.frmBusinessTypes.BusinessType\")",
                "secureTextEntry": false,
                "skin": "skntbxLato35475f14px",
                "textInputMode": constants.TEXTBOX_INPUT_MODE_ANY,
                "top": "25dp",
                "width": "100%",
                "zIndex": 1
            }, {
                "containerHeightMode": constants.TEXTBOX_FONT_METRICS_DRIVEN_HEIGHT,
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [2, 2, 2, 2],
                "paddingInPixel": false
            }, {
                "autoCorrect": false
            });
            var flxEditErrorRow11 = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                "clipBounds": true,
                "id": "flxEditErrorRow11",
                "isVisible": false,
                "layoutType": kony.flex.FLOW_HORIZONTAL,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "68dp",
                "width": "100%",
                "zIndex": 1
            }, {}, {});
            flxEditErrorRow11.setDefaultUnit(kony.flex.DP);
            var lblIconEditError11 = new kony.ui.Label({
                "id": "lblIconEditError11",
                "isVisible": true,
                "left": "0dp",
                "skin": "sknErrorIcon",
                "text": "",
                "top": "0dp",
                "width": "15dp",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var lblEditErrorRow11 = new kony.ui.Label({
                "id": "lblEditErrorRow11",
                "isVisible": true,
                "left": "0dp",
                "skin": "sknlblError",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmAlertsManagement.Value_cannot_be_empty\")",
                "top": "0dp",
                "width": "170dp",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxEditErrorRow11.add(lblIconEditError11, lblEditErrorRow11);
            flxEditRow11.add(flxEditBusinessTypeName, tbxEditBusinessTypeName, flxEditErrorRow11);
            var flxEditRow12 = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "100%",
                "id": "flxEditRow12",
                "isVisible": false,
                "layoutType": kony.flex.FREE_FORM,
                "left": "10dp",
                "isModalContainer": false,
                "right": "0dp",
                "skin": "slFbox",
                "top": "0dp",
                "width": "49%",
                "zIndex": 1
            }, {}, {});
            flxEditRow12.setDefaultUnit(kony.flex.DP);
            var flxAuthorizedSIgnatoriesEdit = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                "clipBounds": true,
                "id": "flxAuthorizedSIgnatoriesEdit",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0dp",
                "width": "100%"
            }, {}, {});
            flxAuthorizedSIgnatoriesEdit.setDefaultUnit(kony.flex.DP);
            var lblAuthSignatoriesEdit = new kony.ui.Label({
                "id": "lblAuthSignatoriesEdit",
                "isVisible": true,
                "left": "0dp",
                "skin": "sknLbl485C75LatoRegular13Px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmBusinessTypes.authorized_signatories\")",
                "top": "0dp",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var lblAuthSignatoriesEditCount = new kony.ui.Label({
                "id": "lblAuthSignatoriesEditCount",
                "isVisible": false,
                "right": "2dp",
                "skin": "slLabel0d20174dce8ea42",
                "text": "0/50",
                "top": "0dp",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxAuthorizedSIgnatoriesEdit.add(lblAuthSignatoriesEdit, lblAuthSignatoriesEditCount);
            var tbxAuthSignatoriesEdit = new kony.ui.TextBox2({
                "autoCapitalize": constants.TEXTBOX_AUTO_CAPITALIZE_NONE,
                "focusSkin": "skntbxLato35475f14px",
                "id": "tbxAuthSignatoriesEdit",
                "isVisible": true,
                "keyBoardStyle": constants.TEXTBOX_KEY_BOARD_STYLE_DEFAULT,
                "left": "0dp",
                "maxTextLength": 50,
                "i18n_placeholder": "kony.i18n.getLocalizedString(\"i18n.frmBusinessTypes.authorized_signatories\")",
                "secureTextEntry": false,
                "skin": "skntbxLato35475f14px",
                "textInputMode": constants.TEXTBOX_INPUT_MODE_ANY,
                "top": "25dp",
                "width": "100%",
                "zIndex": 1
            }, {
                "containerHeightMode": constants.TEXTBOX_FONT_METRICS_DRIVEN_HEIGHT,
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [2, 2, 2, 2],
                "paddingInPixel": false
            }, {
                "autoCorrect": false
            });
            var flxErrorRowEdit12 = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                "clipBounds": true,
                "id": "flxErrorRowEdit12",
                "isVisible": false,
                "layoutType": kony.flex.FLOW_HORIZONTAL,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "68dp",
                "width": "100%",
                "zIndex": 1
            }, {}, {});
            flxErrorRowEdit12.setDefaultUnit(kony.flex.DP);
            var lblEditErrorRow12 = new kony.ui.Label({
                "id": "lblEditErrorRow12",
                "isVisible": true,
                "left": "0dp",
                "skin": "sknErrorIcon",
                "text": "",
                "top": "0dp",
                "width": "15dp",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var lblEditErrorRow = new kony.ui.Label({
                "id": "lblEditErrorRow",
                "isVisible": true,
                "left": "0dp",
                "skin": "sknlblError",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmAlertsManagement.Value_cannot_be_empty\")",
                "top": "0dp",
                "width": "170dp",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxErrorRowEdit12.add(lblEditErrorRow12, lblEditErrorRow);
            flxEditRow12.add(flxAuthorizedSIgnatoriesEdit, tbxAuthSignatoriesEdit, flxErrorRowEdit12);
            flxEditRow1.add(flxEditRow11, flxEditRow12);
            var flxAddHeader = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                "clipBounds": true,
                "id": "flxAddHeader",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "isModalContainer": false,
                "right": "0dp",
                "skin": "slFbox",
                "top": "0dp",
                "width": "100%"
            }, {}, {});
            flxAddHeader.setDefaultUnit(kony.flex.DP);
            var lblAuthSignatoriesHeader = new kony.ui.Label({
                "id": "lblAuthSignatoriesHeader",
                "isVisible": true,
                "left": "0dp",
                "skin": "sknLbl192B45LatoRegular14px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmBusinessTypes.authorized_signatories\")",
                "top": "0dp",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var addButton = new kony.ui.Button({
                "focusSkin": "sknBtnLato117eb013Px",
                "id": "addButton",
                "isVisible": true,
                "right": "0dp",
                "skin": "sknBtnLato117eb013Px",
                "text": "+ Add",
                "top": "0dp",
                "width": kony.flex.USE_PREFFERED_SIZE
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_CENTER,
                "displayText": true,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxAddHeader.add(lblAuthSignatoriesHeader, addButton);
            var flxDynamicAuthSignatories = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                "clipBounds": true,
                "id": "flxDynamicAuthSignatories",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0",
                "isModalContainer": false,
                "skin": "sknFlxBgF8F9FABrE1E5EER3px",
                "top": "15dp",
                "width": "100%"
            }, {}, {});
            flxDynamicAuthSignatories.setDefaultUnit(kony.flex.DP);
            var flxAddSignatories = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                "bottom": "0dp",
                "clipBounds": true,
                "id": "flxAddSignatories",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0dp",
                "width": "100%"
            }, {}, {});
            flxAddSignatories.setDefaultUnit(kony.flex.DP);
            var addBusinessTypes = new com.adminConsole.businessTypes.addBusinessTypes({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "height": "70dp",
                "id": "addBusinessTypes",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "masterType": constants.MASTER_TYPE_DEFAULT,
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0dp",
                "width": "100%",
                "overrides": {
                    "addBusinessTypes": {
                        "height": "70dp"
                    }
                }
            }, {
                "overrides": {}
            }, {
                "overrides": {}
            });
            var addBusinessTypes1 = new com.adminConsole.businessTypes.addBusinessTypes({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "height": "80dp",
                "id": "addBusinessTypes1",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "masterType": constants.MASTER_TYPE_DEFAULT,
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0dp",
                "width": "100%",
                "overrides": {}
            }, {
                "overrides": {}
            }, {
                "overrides": {}
            });
            flxAddSignatories.add(addBusinessTypes, addBusinessTypes1);
            flxDynamicAuthSignatories.add(flxAddSignatories);
            var flxEditRow2 = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                "bottom": "40dp",
                "clipBounds": true,
                "id": "flxEditRow2",
                "isVisible": true,
                "layoutType": kony.flex.FLOW_VERTICAL,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "35dp",
                "width": "100%"
            }, {}, {});
            flxEditRow2.setDefaultUnit(kony.flex.DP);
            var lblNoOfAuthSignatoriesEdit = new kony.ui.Label({
                "id": "lblNoOfAuthSignatoriesEdit",
                "isVisible": true,
                "left": "0px",
                "skin": "sknLbl192B45LatoRegular14px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmBusinessTypes.no_ofauthorized_signatories\")",
                "top": "0dp",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var flxMinMaxEdit = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                "clipBounds": true,
                "id": "flxMinMaxEdit",
                "isVisible": true,
                "layoutType": kony.flex.FLOW_HORIZONTAL,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "15dp",
                "width": "100%"
            }, {}, {});
            flxMinMaxEdit.setDefaultUnit(kony.flex.DP);
            var flxMinEdit = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                "clipBounds": true,
                "id": "flxMinEdit",
                "isVisible": true,
                "layoutType": kony.flex.FLOW_VERTICAL,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0dp",
                "width": "184dp"
            }, {}, {});
            flxMinEdit.setDefaultUnit(kony.flex.DP);
            var lblMinEdit = new kony.ui.Label({
                "id": "lblMinEdit",
                "isVisible": true,
                "left": "0dp",
                "skin": "sknLbl485C75LatoRegular13Px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmBusinessTypes.label.Min\")",
                "top": "0dp",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var flxMin = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                "clipBounds": true,
                "id": "flxMin",
                "isVisible": true,
                "layoutType": kony.flex.FLOW_VERTICAL,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "14dp",
                "width": "181dp"
            }, {}, {});
            flxMin.setDefaultUnit(kony.flex.DP);
            var MinEdit = new com.customer360.common.CharactersplusMinus({
                "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                "id": "MinEdit",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "masterType": constants.MASTER_TYPE_DEFAULT,
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0dp",
                "width": "181dp",
                "overrides": {
                    "CharactersplusMinus": {
                        "top": "0dp",
                        "width": "181dp"
                    },
                    "flxMinus": {
                        "bottom": "0dp",
                        "height": "40dp",
                        "right": "0dp",
                        "top": "0dp",
                        "width": "54dp"
                    },
                    "flxMinusDisable": {
                        "bottom": 2,
                        "height": "38dp",
                        "right": "1dp",
                        "top": "1dp",
                        "width": "53dp"
                    },
                    "flxPlus": {
                        "bottom": "2dp",
                        "height": "40dp",
                        "left": "0dp",
                        "top": "0dp",
                        "width": "54dp",
                        "layoutType": kony.flex.FREE_FORM
                    },
                    "flxPlusDisable": {
                        "bottom": "2dp",
                        "height": "38dp",
                        "left": "1dp",
                        "top": "1dp",
                        "width": "53dp"
                    },
                    "flxSeperator1": {
                        "width": "1dp"
                    },
                    "flxSeperator2": {
                        "width": "1dp"
                    },
                    "lblMinus": {
                        "height": "36dp",
                        "width": "50dp"
                    },
                    "lblPlus": {
                        "height": "36dp",
                        "right": "viz.val_cleared",
                        "text": "-",
                        "width": "50dp"
                    },
                    "tbxcharcterSize": {
                        "text": "8"
                    }
                }
            }, {
                "overrides": {}
            }, {
                "overrides": {}
            });
            flxMin.add(MinEdit);
            flxMinEdit.add(lblMinEdit, flxMin);
            var flxMaxEdit = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                "clipBounds": true,
                "id": "flxMaxEdit",
                "isVisible": true,
                "layoutType": kony.flex.FLOW_VERTICAL,
                "left": "20dp",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0dp",
                "width": "184dp"
            }, {}, {});
            flxMaxEdit.setDefaultUnit(kony.flex.DP);
            var flxMaxLabel = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                "clipBounds": true,
                "id": "flxMaxLabel",
                "isVisible": true,
                "layoutType": kony.flex.FLOW_HORIZONTAL,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0dp",
                "width": "100%"
            }, {}, {});
            flxMaxLabel.setDefaultUnit(kony.flex.DP);
            var lblMaxEdit = new kony.ui.Label({
                "id": "lblMaxEdit",
                "isVisible": true,
                "left": "0dp",
                "skin": "sknLbl485C75LatoRegular13Px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmBusinessTypes.label.Max\")",
                "top": "0dp",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var lblOptionalEdit = new kony.ui.Label({
                "id": "lblOptionalEdit",
                "isVisible": true,
                "left": "4dp",
                "skin": "sknLbl9ca8b0fs13px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmPermissions.lblOptional\")",
                "top": "0dp",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxMaxLabel.add(lblMaxEdit, lblOptionalEdit);
            var flxMax = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                "clipBounds": true,
                "id": "flxMax",
                "isVisible": true,
                "layoutType": kony.flex.FLOW_VERTICAL,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "14dp",
                "width": "181dp"
            }, {}, {});
            flxMax.setDefaultUnit(kony.flex.DP);
            var MaxEdit = new com.customer360.common.CharactersplusMinus({
                "height": "40dp",
                "id": "MaxEdit",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "masterType": constants.MASTER_TYPE_DEFAULT,
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0dp",
                "width": "181dp",
                "overrides": {
                    "CharactersplusMinus": {
                        "height": "40dp",
                        "top": "0dp",
                        "width": "181dp"
                    },
                    "flxMinus": {
                        "bottom": "0dp",
                        "height": "40dp",
                        "isVisible": true,
                        "right": "0dp",
                        "top": "0dp",
                        "width": "54dp"
                    },
                    "flxMinusDisable": {
                        "bottom": 2,
                        "height": "38dp",
                        "right": "1dp",
                        "top": "1dp",
                        "width": "53dp"
                    },
                    "flxPlus": {
                        "bottom": "0dp",
                        "height": "40dp",
                        "left": "0dp",
                        "top": "0dp",
                        "width": "54dp",
                        "layoutType": kony.flex.FREE_FORM
                    },
                    "flxPlusDisable": {
                        "bottom": "2dp",
                        "height": "38dp",
                        "left": "1dp",
                        "top": "1dp",
                        "width": "53dp"
                    },
                    "flxSeperator1": {
                        "width": "1dp"
                    },
                    "flxSeperator2": {
                        "width": "1dp"
                    },
                    "lblMinus": {
                        "centerY": "50%",
                        "height": "36dp",
                        "width": "50dp"
                    },
                    "lblPlus": {
                        "height": "36dp",
                        "width": "50dp"
                    },
                    "tbxcharcterSize": {
                        "height": "40dp",
                        "text": "64"
                    }
                }
            }, {
                "overrides": {}
            }, {
                "overrides": {}
            });
            flxMax.add(MaxEdit);
            flxMaxEdit.add(flxMaxLabel, flxMax);
            flxMinMaxEdit.add(flxMinEdit, flxMaxEdit);
            var flxErrorRowEdit2 = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "bottom": "0px",
                "clipBounds": true,
                "height": "20px",
                "id": "flxErrorRowEdit2",
                "isVisible": false,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0px",
                "isModalContainer": false,
                "right": "0px",
                "skin": "slFbox",
                "top": "5px",
                "width": "100%",
                "zIndex": 1
            }, {}, {});
            flxErrorRowEdit2.setDefaultUnit(kony.flex.DP);
            var lblIconOptionsMinEdit = new kony.ui.Label({
                "centerY": "50%",
                "height": "15px",
                "id": "lblIconOptionsMinEdit",
                "isVisible": true,
                "left": 0,
                "skin": "sknErrorIcon",
                "text": "",
                "width": "15px",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var lblErrormsgMinEdit = new kony.ui.Label({
                "centerY": "50%",
                "id": "lblErrormsgMinEdit",
                "isVisible": true,
                "left": "16px",
                "skin": "sknlblError",
                "text": "Range is between 8 and 64",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxErrorRowEdit2.add(lblIconOptionsMinEdit, lblErrormsgMinEdit);
            flxEditRow2.add(lblNoOfAuthSignatoriesEdit, flxMinMaxEdit, flxErrorRowEdit2);
            flxBusinessTypeEditDetailsContainer.add(flxEditRow1, flxAddHeader, flxDynamicAuthSignatories, flxEditRow2);
            flxBusinessTypeEditDetails.add(flxBusinessTypeEditDetailsContainer);
            var flxAssociatedRoles = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                "clipBounds": true,
                "id": "flxAssociatedRoles",
                "isVisible": true,
                "layoutType": kony.flex.FLOW_VERTICAL,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0dp",
                "width": "100%"
            }, {}, {});
            flxAssociatedRoles.setDefaultUnit(kony.flex.DP);
            var flxSeparator2 = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                "clipBounds": true,
                "id": "flxSeparator2",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0dp",
                "width": "100%"
            }, {}, {});
            flxSeparator2.setDefaultUnit(kony.flex.DP);
            var flxViewSeparator2 = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "bottom": "0dp",
                "clipBounds": true,
                "height": "1dp",
                "id": "flxViewSeparator2",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "isModalContainer": false,
                "right": "0dp",
                "skin": "sknflxffffffOp100BorderE1E5EdNoRadius",
                "zIndex": 1
            }, {}, {});
            flxViewSeparator2.setDefaultUnit(kony.flex.DP);
            flxViewSeparator2.add();
            flxSeparator2.add(flxViewSeparator2);
            var flxRolesContent = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                "clipBounds": true,
                "id": "flxRolesContent",
                "isVisible": true,
                "layoutType": kony.flex.FLOW_VERTICAL,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0dp",
                "width": "100%"
            }, {}, {});
            flxRolesContent.setDefaultUnit(kony.flex.DP);
            var flxAssociateRoleHeader = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                "clipBounds": true,
                "id": "flxAssociateRoleHeader",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0dp",
                "width": "100%"
            }, {}, {});
            flxAssociateRoleHeader.setDefaultUnit(kony.flex.DP);
            var flxLblAssociatedRole = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "52dp",
                "id": "flxLblAssociatedRole",
                "isVisible": true,
                "layoutType": kony.flex.FLOW_HORIZONTAL,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0dp",
                "width": "100%"
            }, {}, {});
            flxLblAssociatedRole.setDefaultUnit(kony.flex.DP);
            var lblAssociatedRole = new kony.ui.Label({
                "centerY": "50%",
                "id": "lblAssociatedRole",
                "isVisible": true,
                "left": "20dp",
                "skin": "sknLatoRegular192B4518px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmBusinessTypes.associated_role\")",
                "top": "20dp",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_TOP_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var lblRolesCount = new kony.ui.Label({
                "centerY": "50%",
                "id": "lblRolesCount",
                "isVisible": true,
                "left": "5dp",
                "skin": "sknLatoRegular192B4518px",
                "text": "(07)",
                "top": "20dp",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_TOP_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxLblAssociatedRole.add(lblAssociatedRole, lblRolesCount);
            var flxChangeDefault = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "22dp",
                "id": "flxChangeDefault",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "isModalContainer": false,
                "right": "20dp",
                "skin": "slFbox",
                "top": "14dp",
                "width": "140dp"
            }, {}, {});
            flxChangeDefault.setDefaultUnit(kony.flex.DP);
            var flxChangeDefaultRoleButton = new kony.ui.Button({
                "focusSkin": "sknbtnf7f7faLatoRegular12Px485c75Border1px485C75Radius20px",
                "height": "22px",
                "id": "flxChangeDefaultRoleButton",
                "isVisible": true,
                "right": "0dp",
                "skin": "sknbtnf7f7faLatoRegular12Px485c75Border1px485C75Radius20px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmBusinessTypes.label.defaultRoleChange\")",
                "top": "0px",
                "width": "140dp",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_CENTER,
                "displayText": true,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {
                "hoverSkin": "sknbtnffffffLatoRegular12Px485c75Border1px485C75Radius20px"
            });
            flxChangeDefault.add(flxChangeDefaultRoleButton);
            flxAssociateRoleHeader.add(flxLblAssociatedRole, flxChangeDefault);
            var flxViewSeparator = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                "clipBounds": true,
                "id": "flxViewSeparator",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0dp",
                "width": "100%"
            }, {}, {});
            flxViewSeparator.setDefaultUnit(kony.flex.DP);
            var flxViewSeparatorVal = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "bottom": "0dp",
                "clipBounds": true,
                "height": "1dp",
                "id": "flxViewSeparatorVal",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "20dp",
                "isModalContainer": false,
                "right": "20dp",
                "skin": "sknflxffffffOp100BorderE1E5EdNoRadius",
                "zIndex": 1
            }, {}, {});
            flxViewSeparatorVal.setDefaultUnit(kony.flex.DP);
            flxViewSeparatorVal.add();
            flxViewSeparator.add(flxViewSeparatorVal);
            var flxAssignRolesContainer = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                "bottom": "20dp",
                "clipBounds": true,
                "id": "flxAssignRolesContainer",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "isModalContainer": false,
                "right": "0dp",
                "skin": "slFbox",
                "top": "0dp"
            }, {}, {});
            flxAssignRolesContainer.setDefaultUnit(kony.flex.DP);
            var flxNoResultsFound = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "centerX": "50%",
                "clipBounds": true,
                "height": "200dp",
                "id": "flxNoResultsFound",
                "isVisible": false,
                "layoutType": kony.flex.FLOW_HORIZONTAL,
                "isModalContainer": false,
                "skin": "slFbox",
                "top": 00,
                "width": "100%",
                "zIndex": 1
            }, {}, {});
            flxNoResultsFound.setDefaultUnit(kony.flex.DP);
            var lblNoResults = new kony.ui.Label({
                "centerX": "47%",
                "centerY": "50%",
                "id": "lblNoResults",
                "isVisible": true,
                "left": "0dp",
                "skin": "slLabel0d20174dce8ea42",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmLogs.rtxNoResultsFound\")",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_CENTER,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxNoResultsFound.add(lblNoResults);
            var flxResults = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                "bottom": "0dp",
                "clipBounds": true,
                "id": "flxResults",
                "isVisible": true,
                "layoutType": kony.flex.FLOW_VERTICAL,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0dp",
                "width": "100%"
            }, {}, {});
            flxResults.setDefaultUnit(kony.flex.DP);
            var lblChangeDefaultRole = new kony.ui.Label({
                "id": "lblChangeDefaultRole",
                "isVisible": false,
                "left": "20px",
                "skin": "sknLbl192B45LatoRegular14px",
                "text": "Change Default Role",
                "top": "14dp",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var flxRoleUndoHeader = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "65px",
                "id": "flxRoleUndoHeader",
                "isVisible": false,
                "layoutType": kony.flex.FREE_FORM,
                "left": "20dp",
                "isModalContainer": false,
                "right": "20dp",
                "skin": "slFbox",
                "top": "0dp",
                "zIndex": 1
            }, {}, {});
            flxRoleUndoHeader.setDefaultUnit(kony.flex.DP);
            var backToRolesList = new com.adminConsole.customerMang.backToPageHeader({
                "height": "20px",
                "id": "backToRolesList",
                "isVisible": true,
                "layoutType": kony.flex.FLOW_HORIZONTAL,
                "left": "0dp",
                "masterType": constants.MASTER_TYPE_DEFAULT,
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "10dp",
                "width": "100%",
                "overrides": {
                    "backToPageHeader": {
                        "top": "10dp"
                    },
                    "btnBack": {
                        "text": "Back"
                    },
                    "flxBack": {
                        "left": "0px"
                    }
                }
            }, {
                "overrides": {}
            }, {
                "overrides": {}
            });
            var lblDefaultRole = new kony.ui.Label({
                "height": "20px",
                "id": "lblDefaultRole",
                "isVisible": true,
                "left": "0dp",
                "skin": "sknLatoRegular192B4518px",
                "text": "Default Role",
                "top": "45dp",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxRoleUndoHeader.add(backToRolesList, lblDefaultRole);
            var segCustomerRolesEdit = new kony.ui.SegmentedUI2({
                "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                "data": [{
                    "btnViewDetails": "kony.i18n.getLocalizedString(\"i18n.frmGroupsController.viewDetails\")",
                    "flxDefaultRoleButton": "Change Default Role",
                    "imgRoleCheckbox": "checkboxnormal.png",
                    "imgRoleRadio": "checkboxnormal.png",
                    "lblRoleDesc": "kony.i18n.getLocalizedString(\"i18n.frmAlertsManagement.LoremIpsumA\")",
                    "lblRoleName": "Bill Payments"
                }],
                "groupCells": false,
                "id": "segCustomerRolesEdit",
                "isVisible": true,
                "left": "0dp",
                "needPageIndicator": true,
                "pageOffDotImage": "pageoffdot.png",
                "pageOnDotImage": "pageondot.png",
                "retainSelection": false,
                "rowFocusSkin": "seg2Focus",
                "rowSkin": "seg2Normal",
                "rowTemplate": "flxCustomerProfileRoles",
                "sectionHeaderSkin": "sliPhoneSegmentHeader",
                "selectionBehavior": constants.SEGUI_SINGLE_SELECT_BEHAVIOR,
                "separatorRequired": false,
                "showScrollbars": false,
                "top": "4dp",
                "viewType": constants.SEGUI_VIEW_TYPE_TABLEVIEW,
                "widgetDataMap": {
                    "btnViewDetails": "btnViewDetails",
                    "flxCustomerProfileRoles": "flxCustomerProfileRoles",
                    "flxDefaultRoleButton": "flxDefaultRoleButton",
                    "flxRoleCheckbox": "flxRoleCheckbox",
                    "flxRoleInfo": "flxRoleInfo",
                    "flxRoleName": "flxRoleName",
                    "flxRoleNameContainer": "flxRoleNameContainer",
                    "flxRoleRadio": "flxRoleRadio",
                    "imgRoleCheckbox": "imgRoleCheckbox",
                    "imgRoleRadio": "imgRoleRadio",
                    "lblRoleDesc": "lblRoleDesc",
                    "lblRoleName": "lblRoleName"
                },
                "width": "100%",
                "zIndex": 1
            }, {
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var flxHeightAdjust = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "100dp",
                "id": "flxHeightAdjust",
                "isVisible": false,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0",
                "width": "100%"
            }, {}, {});
            flxHeightAdjust.setDefaultUnit(kony.flex.DP);
            flxHeightAdjust.add();
            flxResults.add(lblChangeDefaultRole, flxRoleUndoHeader, segCustomerRolesEdit, flxHeightAdjust);
            flxAssignRolesContainer.add(flxNoResultsFound, flxResults);
            flxRolesContent.add(flxAssociateRoleHeader, flxViewSeparator, flxAssignRolesContainer);
            flxAssociatedRoles.add(flxSeparator2, flxRolesContent);
            flxBusinessTypeViewEdit.add(flxBusinessTypeViewDetails, flxBusinessTypeEditDetails, flxAssociatedRoles);
            var flxCommonButtonsRolesSave = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "bottom": "0dp",
                "clipBounds": true,
                "height": "80dp",
                "id": "flxCommonButtonsRolesSave",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "sknFlxFFFFFF100O",
                "width": "100%",
                "zIndex": 1
            }, {}, {});
            flxCommonButtonsRolesSave.setDefaultUnit(kony.flex.DP);
            var flxCommonButtonsRolesSeparator = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "1dp",
                "id": "flxCommonButtonsRolesSeparator",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "isModalContainer": false,
                "right": "0dp",
                "skin": "sknflxe1e5edop100",
                "top": "0dp",
                "zIndex": 1
            }, {}, {});
            flxCommonButtonsRolesSeparator.setDefaultUnit(kony.flex.DP);
            flxCommonButtonsRolesSeparator.add();
            var commonButtonsRoles = new com.adminConsole.common.commonButtons({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "centerY": "50%",
                "height": "80px",
                "id": "commonButtonsRoles",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "masterType": constants.MASTER_TYPE_DEFAULT,
                "isModalContainer": false,
                "skin": "slFbox",
                "width": "100%",
                "overrides": {
                    "btnCancel": {
                        "left": "viz.val_cleared",
                        "right": "140px"
                    },
                    "btnNext": {
                        "isVisible": false,
                        "left": "viz.val_cleared",
                        "right": "20dp",
                        "width": "100dp"
                    },
                    "btnSave": {
                        "i18n_text": "kony.i18n.getLocalizedString(\"i18n.permission.SAVE\")",
                        "isVisible": true,
                        "right": "20dp"
                    },
                    "commonButtons": {
                        "bottom": "viz.val_cleared",
                        "centerY": "50%",
                        "isVisible": true,
                        "top": "viz.val_cleared"
                    },
                    "flxRightButtons": {
                        "width": "140px"
                    }
                }
            }, {
                "overrides": {}
            }, {
                "overrides": {}
            });
            flxCommonButtonsRolesSave.add(flxCommonButtonsRolesSeparator, commonButtonsRoles);
            var flxAddEditButtons = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "bottom": "0dp",
                "clipBounds": true,
                "height": "80dp",
                "id": "flxAddEditButtons",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "slFbox",
                "width": "100%",
                "zIndex": 1
            }, {}, {});
            flxAddEditButtons.setDefaultUnit(kony.flex.DP);
            var flxAddEditButtonsSeparator = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "1dp",
                "id": "flxAddEditButtonsSeparator",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "isModalContainer": false,
                "right": "0dp",
                "skin": "sknflxe1e5edop100",
                "top": "0dp",
                "zIndex": 1
            }, {}, {});
            flxAddEditButtonsSeparator.setDefaultUnit(kony.flex.DP);
            flxAddEditButtonsSeparator.add();
            var commonButtonsAddEdit = new com.adminConsole.common.commonButtons({
                "height": "80px",
                "id": "commonButtonsAddEdit",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "masterType": constants.MASTER_TYPE_DEFAULT,
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0dp",
                "width": "100%",
                "overrides": {
                    "btnCancel": {
                        "left": "20px"
                    },
                    "btnNext": {
                        "isVisible": false,
                        "left": "viz.val_cleared",
                        "right": "145dp",
                        "width": "100px"
                    },
                    "btnSave": {
                        "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmAdManagement.CreateCAPS\")",
                        "right": "20px",
                        "top": "0%",
                        "width": "125px"
                    },
                    "commonButtons": {
                        "left": "0dp",
                        "right": "viz.val_cleared",
                        "top": "0dp",
                        "width": "100%"
                    },
                    "flxRightButtons": {
                        "width": "250px"
                    }
                }
            }, {
                "overrides": {}
            }, {
                "overrides": {}
            });
            flxAddEditButtons.add(flxAddEditButtonsSeparator, commonButtonsAddEdit);
            var flxSegViewMore = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                "clipBounds": true,
                "id": "flxSegViewMore",
                "isVisible": false,
                "layoutType": kony.flex.FLOW_VERTICAL,
                "left": "20px",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "135px",
                "width": "250px",
                "zIndex": 5
            }, {}, {});
            flxSegViewMore.setDefaultUnit(kony.flex.DP);
            var flxMoreUpArrow = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "12px",
                "id": "flxMoreUpArrow",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0px",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0px",
                "width": "100%",
                "zIndex": 2
            }, {}, {});
            flxMoreUpArrow.setDefaultUnit(kony.flex.DP);
            var imgUpArrowMore = new kony.ui.Image2({
                "height": "12dp",
                "id": "imgUpArrowMore",
                "isVisible": true,
                "left": "10dp",
                "right": 0,
                "skin": "slImage",
                "src": "uparrow_2x.png",
                "top": "0dp",
                "width": "15dp",
                "zIndex": 1
            }, {
                "imageScaleMode": constants.IMAGE_SCALE_MODE_FIT_TO_DIMENSIONS,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxMoreUpArrow.add(imgUpArrowMore);
            var flxViewMore = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                "clipBounds": true,
                "id": "flxViewMore",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "sknflxffffffop100dbdbe6Radius3px",
                "top": "-1dp",
                "width": "100%",
                "zIndex": 1
            }, {}, {});
            flxViewMore.setDefaultUnit(kony.flex.DP);
            var segViewMore = new kony.ui.SegmentedUI2({
                "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                "bottom": 10,
                "data": [{
                    "lblName": "Granville Street, Vancover, BC, Canada"
                }, {
                    "lblName": "Granville Street, Vancover, BC, Canada"
                }, {
                    "lblName": "Granville Street, Vancover, BC, Canada"
                }],
                "groupCells": false,
                "id": "segViewMore",
                "isVisible": true,
                "left": "15dp",
                "needPageIndicator": true,
                "pageOffDotImage": "pageoffdot.png",
                "pageOnDotImage": "pageondot.png",
                "retainSelection": false,
                "right": "15px",
                "rowFocusSkin": "seg2Focus",
                "rowSkin": "seg2Normal",
                "rowTemplate": "flxMore",
                "sectionHeaderSkin": "sliPhoneSegmentHeader",
                "selectionBehavior": constants.SEGUI_DEFAULT_BEHAVIOR,
                "separatorColor": "aaaaaa00",
                "separatorRequired": false,
                "separatorThickness": 0,
                "showScrollbars": false,
                "top": "0dp",
                "viewType": constants.SEGUI_VIEW_TYPE_TABLEVIEW,
                "widgetDataMap": {
                    "flxMore": "flxMore",
                    "lblName": "lblName"
                },
                "zIndex": 1
            }, {
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxViewMore.add(segViewMore);
            flxSegViewMore.add(flxMoreUpArrow, flxViewMore);
            flxViewEditBusinessTypes.add(flxBusinessTypeViewEdit, flxCommonButtonsRolesSave, flxAddEditButtons, flxSegViewMore);
            flxMainContent.add(flxBusinessTypesList, flxViewEditBusinessTypes);
            flxRightPanel.add(flxMainHeader, flxBreadcrumb, flxMainContent);
            var flxHeaderDropdown = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                "clipBounds": true,
                "id": "flxHeaderDropdown",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "isModalContainer": false,
                "right": "35dp",
                "skin": "slFbox",
                "top": "20dp",
                "width": "400dp",
                "zIndex": 5
            }, {}, {});
            flxHeaderDropdown.setDefaultUnit(kony.flex.DP);
            var dropdownMainHeader = new com.adminConsole.common.dropdownMainHeader({
                "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                "id": "dropdownMainHeader",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "masterType": constants.MASTER_TYPE_DEFAULT,
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0dp",
                "width": "100%",
                "overrides": {
                    "flxDropdown": {
                        "isVisible": false
                    },
                    "imgUpArrow": {
                        "src": "uparrow_2x.png"
                    }
                }
            }, {
                "overrides": {}
            }, {
                "overrides": {}
            });
            flxHeaderDropdown.add(dropdownMainHeader);
            var flxToastMessage = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "bottom": "0dp",
                "clipBounds": true,
                "height": "70dp",
                "id": "flxToastMessage",
                "isVisible": false,
                "layoutType": kony.flex.FREE_FORM,
                "left": "305px",
                "isModalContainer": false,
                "skin": "slFbox",
                "width": "78%",
                "zIndex": 1
            }, {}, {});
            flxToastMessage.setDefaultUnit(kony.flex.DP);
            var toastMessage = new com.adminConsole.common.toastMessage({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "id": "toastMessage",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "masterType": constants.MASTER_TYPE_DEFAULT,
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0dp",
                "overrides": {}
            }, {
                "overrides": {}
            }, {
                "overrides": {}
            });
            flxToastMessage.add(toastMessage);
            var flxLoading = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "100%",
                "id": "flxLoading",
                "isVisible": false,
                "layoutType": kony.flex.FREE_FORM,
                "left": "305px",
                "isModalContainer": false,
                "right": "0px",
                "skin": "sknLoadingBlur",
                "top": "0dp",
                "zIndex": 10
            }, {}, {});
            flxLoading.setDefaultUnit(kony.flex.DP);
            var flxImageContainer = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "centerX": "50%",
                "centerY": "50%",
                "clipBounds": true,
                "height": "75px",
                "id": "flxImageContainer",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "isModalContainer": false,
                "skin": "slFbox",
                "width": "75px",
                "zIndex": 1
            }, {}, {});
            flxImageContainer.setDefaultUnit(kony.flex.DP);
            var imgLoading = new kony.ui.Image2({
                "centerX": "50%",
                "centerY": "50%",
                "height": "50px",
                "id": "imgLoading",
                "isVisible": true,
                "skin": "slImage",
                "src": "loadingscreenimage.gif",
                "width": "50px",
                "zIndex": 1
            }, {
                "imageScaleMode": constants.IMAGE_SCALE_MODE_MAINTAIN_ASPECT_RATIO,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxImageContainer.add(imgLoading);
            flxLoading.add(flxImageContainer);
            var flxViewRolePopup = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "100%",
                "id": "flxViewRolePopup",
                "isVisible": false,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "sknFocus50",
                "top": "0dp",
                "width": "100%",
                "zIndex": 6
            }, {}, {});
            flxViewRolePopup.setDefaultUnit(kony.flex.DP);
            var flxRoleDetailsContainer = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "centerX": "50%",
                "centerY": "50%",
                "clipBounds": true,
                "height": "540dp",
                "id": "flxRoleDetailsContainer",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "17%",
                "isModalContainer": false,
                "right": "17%",
                "skin": "slFbox0j9f841cc563e4e",
                "zIndex": 1
            }, {}, {});
            flxRoleDetailsContainer.setDefaultUnit(kony.flex.DP);
            var flxRolePopupTopColor = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "10px",
                "id": "flxRolePopupTopColor",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0px",
                "isModalContainer": false,
                "skin": "sknFlxInfoToastBg357C9ENoRadius",
                "top": "0dp",
                "width": "100%",
                "zIndex": 1
            }, {}, {});
            flxRolePopupTopColor.setDefaultUnit(kony.flex.DP);
            flxRolePopupTopColor.add();
            var flxRolePopupBody = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                "bottom": "0dp",
                "clipBounds": true,
                "id": "flxRolePopupBody",
                "isVisible": true,
                "layoutType": kony.flex.FLOW_VERTICAL,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "10px",
                "width": "100%",
                "zIndex": 1
            }, {}, {});
            flxRolePopupBody.setDefaultUnit(kony.flex.DP);
            var flxRoleClosePopup = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "25px",
                "id": "flxRoleClosePopup",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "isModalContainer": false,
                "right": "13dp",
                "skin": "sknCursor",
                "top": "10dp",
                "width": "25px",
                "zIndex": 20
            }, {}, {
                "hoverSkin": "sknCursor"
            });
            flxRoleClosePopup.setDefaultUnit(kony.flex.DP);
            var lblIconClosePopup = new kony.ui.Label({
                "centerX": "51%",
                "centerY": "50.00%",
                "height": "15dp",
                "id": "lblIconClosePopup",
                "isVisible": true,
                "left": "17dp",
                "skin": "sknFontIconSearchCross16px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmAlertsManagement.lblAlertPreviewClose\")",
                "width": "15dp",
                "zIndex": 2
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxRoleClosePopup.add(lblIconClosePopup);
            var flxRoleName = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                "clipBounds": true,
                "id": "flxRoleName",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "-5dp",
                "width": "100%"
            }, {}, {});
            flxRoleName.setDefaultUnit(kony.flex.DP);
            var lblFeatureName = new kony.ui.Label({
                "id": "lblFeatureName",
                "isVisible": true,
                "left": "20dp",
                "skin": "sknLbl192b45LatoReg16px",
                "text": "Administartor",
                "top": 0,
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var lblSeperator = new kony.ui.Label({
                "height": "1px",
                "id": "lblSeperator",
                "isVisible": true,
                "left": "20dp",
                "right": "20dp",
                "skin": "sknlblSeperator",
                "text": ".",
                "top": "40dp",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxRoleName.add(lblFeatureName, lblSeperator);
            var flxViewRoleWrapper = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                "bottom": "0dp",
                "clipBounds": true,
                "id": "flxViewRoleWrapper",
                "isVisible": true,
                "layoutType": kony.flex.FLOW_VERTICAL,
                "left": "0px",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "20px",
                "width": "100%",
                "zIndex": 1
            }, {}, {});
            flxViewRoleWrapper.setDefaultUnit(kony.flex.DP);
            var flxDescriptionViewRole = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                "clipBounds": true,
                "id": "flxDescriptionViewRole",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0dp",
                "width": "100%"
            }, {}, {});
            flxDescriptionViewRole.setDefaultUnit(kony.flex.DP);
            var lblViewRoleDescriptionHeading = new kony.ui.Label({
                "id": "lblViewRoleDescriptionHeading",
                "isVisible": true,
                "left": "20dp",
                "skin": "sknLbl78818A11px",
                "text": "DESCRIPTION",
                "top": "0dp",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var lblViewRoleDescriptionValue = new kony.ui.Label({
                "id": "lblViewRoleDescriptionValue",
                "isVisible": true,
                "left": "20dp",
                "right": "20dp",
                "skin": "sknLbl485C75LatoRegular13Px",
                "text": "This terms & conditions will be used in the footer link of retail banking",
                "top": "25dp",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxDescriptionViewRole.add(lblViewRoleDescriptionHeading, lblViewRoleDescriptionValue);
            var flxFeaturesList = new kony.ui.FlexScrollContainer({
                "allowHorizontalBounce": false,
                "allowVerticalBounce": true,
                "bottom": "0px",
                "bounces": true,
                "clipBounds": true,
                "enableScrolling": true,
                "height": "350dp",
                "horizontalScrollIndicator": true,
                "id": "flxFeaturesList",
                "isVisible": true,
                "layoutType": kony.flex.FLOW_VERTICAL,
                "left": "0dp",
                "pagingEnabled": false,
                "scrollDirection": kony.flex.SCROLL_VERTICAL,
                "skin": "slFSbox",
                "top": "0dp",
                "verticalScrollIndicator": true,
                "width": "100%"
            }, {}, {});
            flxFeaturesList.setDefaultUnit(kony.flex.DP);
            var ViewRoleFeaturesActions = new com.adminConsole.customerRoles.ViewRoleFeaturesActions({
                "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                "id": "ViewRoleFeaturesActions",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "masterType": constants.MASTER_TYPE_DEFAULT,
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0dp",
                "width": "100%",
                "overrides": {
                    "ViewRoleFeaturesActions": {
                        "left": "0dp",
                        "right": "viz.val_cleared",
                        "top": "0dp"
                    }
                }
            }, {
                "overrides": {}
            }, {
                "overrides": {}
            });
            flxFeaturesList.add(ViewRoleFeaturesActions);
            flxViewRoleWrapper.add(flxDescriptionViewRole, flxFeaturesList);
            flxRolePopupBody.add(flxRoleClosePopup, flxRoleName, flxViewRoleWrapper);
            flxRoleDetailsContainer.add(flxRolePopupTopColor, flxRolePopupBody);
            flxViewRolePopup.add(flxRoleDetailsContainer);
            var flxActionPopup = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "100%",
                "id": "flxActionPopup",
                "isVisible": false,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "masterType": constants.MASTER_TYPE_USERWIDGET,
                "isModalContainer": false,
                "skin": "flxPopupTrans0ba858b5d5d304e",
                "top": "0dp",
                "width": "100%",
                "zIndex": 10
            }, {}, {});
            flxActionPopup.setDefaultUnit(kony.flex.DP);
            var popUp = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "100%",
                "id": "popUp",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0dp",
                "width": "100%"
            }, {}, {});
            popUp.setDefaultUnit(kony.flex.DP);
            var flxPopUp = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                "centerX": "50%",
                "clipBounds": false,
                "id": "flxPopUp",
                "isVisible": true,
                "layoutType": kony.flex.FLOW_VERTICAL,
                "isModalContainer": false,
                "skin": "slFbox0j9f841cc563e4e",
                "top": "250px",
                "width": "600px",
                "zIndex": 10
            }, {}, {});
            flxPopUp.setDefaultUnit(kony.flex.DP);
            var flxPopUpTopColor = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "10px",
                "id": "flxPopUpTopColor",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "sknflxebb54cOp100",
                "top": "0dp",
                "width": "100%",
                "zIndex": 1
            }, {}, {});
            flxPopUpTopColor.setDefaultUnit(kony.flex.DP);
            flxPopUpTopColor.add();
            var flxPopupHeader = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                "clipBounds": true,
                "id": "flxPopupHeader",
                "isVisible": true,
                "layoutType": kony.flex.FLOW_VERTICAL,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "slFbox0d9c3974835234d",
                "top": "0px",
                "width": "100%",
                "zIndex": 1
            }, {}, {});
            flxPopupHeader.setDefaultUnit(kony.flex.DP);
            var flxPopUpClose = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "25px",
                "id": "flxPopUpClose",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "isModalContainer": false,
                "right": 15,
                "skin": "CopyslFbox0efc73ba91cad4b",
                "top": "15dp",
                "width": "25px",
                "zIndex": 20
            }, {}, {});
            flxPopUpClose.setDefaultUnit(kony.flex.DP);
            var lblPopUpClose = new kony.ui.Label({
                "centerY": "50%",
                "id": "lblPopUpClose",
                "isVisible": true,
                "left": "4dp",
                "skin": "sknIcon18pxGray",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.userwidgetmodel.lblClearSearch\")",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var fontIconImgCLose = new kony.ui.Label({
                "centerX": "50%",
                "centerY": "50.00%",
                "height": "15dp",
                "id": "fontIconImgCLose",
                "isVisible": true,
                "skin": "sknFontIconSearchCross16px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.userwidgetmodel.lblClearSearch\")",
                "width": "15dp",
                "zIndex": 2
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxPopUpClose.add(lblPopUpClose, fontIconImgCLose);
            var lblPopUpMainMessage = new kony.ui.Label({
                "id": "lblPopUpMainMessage",
                "isVisible": true,
                "left": "20px",
                "skin": "sknlblLatoBold35475f23px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.ConfigurationBundles.delete\")",
                "top": "-10px",
                "width": "80%",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var rtxPopUpDisclaimer = new kony.ui.RichText({
                "bottom": 30,
                "id": "rtxPopUpDisclaimer",
                "isVisible": true,
                "left": "20px",
                "right": 20,
                "skin": "sknrtxLato0df8337c414274d",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.ConfigurationBundles.deleteConfigurationBody\")",
                "top": "30px",
                "width": "560dp",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxPopupHeader.add(flxPopUpClose, lblPopUpMainMessage, rtxPopUpDisclaimer);
            var flxPopUpButtons = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "80px",
                "id": "flxPopUpButtons",
                "isVisible": true,
                "layoutType": kony.flex.FLOW_HORIZONTAL,
                "reverseLayoutDirection": true,
                "left": "0%",
                "isModalContainer": false,
                "skin": "slFbox0dc900c4572aa40",
                "top": "0px",
                "width": "100%",
                "zIndex": 1
            }, {}, {});
            flxPopUpButtons.setDefaultUnit(kony.flex.DP);
            var btnPopUpProceed = new kony.ui.Button({
                "centerY": "50%",
                "focusSkin": "sknBtn003E75LatoRegular13pxFFFFFFRad20px",
                "height": "40px",
                "id": "btnPopUpProceed",
                "isVisible": true,
                "right": "20px",
                "skin": "sknBtn003E75LatoRegular13pxFFFFFFRad20px",
                "text": "YES, PROCEED",
                "width": "148dp",
                "zIndex": 20
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_CENTER,
                "displayText": true,
                "padding": [3, 0, 3, 0],
                "paddingInPixel": false
            }, {
                "hoverSkin": "sknBtn005198LatoRegular13pxFFFFFFRad20px"
            });
            var btnPopUpCancel = new kony.ui.Button({
                "centerY": "50.00%",
                "focusSkin": "sknbtnf7f7faLatoRegular4f555dBorder1px8b96a5",
                "height": "40px",
                "id": "btnPopUpCancel",
                "isVisible": true,
                "right": "20px",
                "skin": "sknbtnf7f7faLatoRegular4f555dBorder1px8b96a5",
                "text": "NO, LEAVE AS IS",
                "top": "0%",
                "width": "157dp",
                "zIndex": 20
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_CENTER,
                "displayText": true,
                "padding": [3, 0, 3, 0],
                "paddingInPixel": false
            }, {
                "hoverSkin": "sknbtnffffffLatoRegular4f555dBorder1px485c75"
            });
            flxPopUpButtons.add(btnPopUpProceed, btnPopUpCancel);
            flxPopUp.add(flxPopUpTopColor, flxPopupHeader, flxPopUpButtons);
            popUp.add(flxPopUp);
            flxActionPopup.add(popUp);
            var flxConflictPopup = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "100%",
                "id": "flxConflictPopup",
                "isVisible": false,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "masterType": constants.MASTER_TYPE_USERWIDGET,
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0dp",
                "width": "100%",
                "zIndex": 10
            }, {}, {});
            flxConflictPopup.setDefaultUnit(kony.flex.DP);
            var conflictPopup = new com.adminConsole.common.popUp1({
                "height": "100%",
                "id": "conflictPopup",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "masterType": constants.MASTER_TYPE_DEFAULT,
                "isModalContainer": false,
                "skin": "sknflxBg000000Op50",
                "top": "0dp",
                "width": "100%",
                "zIndex": 1,
                "overrides": {
                    "btnPopUpCancel": {
                        "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmCustomerManagement.close_popup\")",
                        "right": "20px"
                    },
                    "btnPopUpDelete": {
                        "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmOutageMessageController.UPDATE\")",
                        "isVisible": false
                    },
                    "flxPopUpTopColor": {
                        "isVisible": true
                    },
                    "lblPopUpMainMessage": {
                        "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmBusinessTypes.deleteBusinessType\")",
                        "top": "-10px"
                    },
                    "popUp1": {
                        "left": "0dp",
                        "top": "0dp"
                    },
                    "rtxPopUpDisclaimer": {
                        "bottom": "30dp",
                        "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmBusinessTypes.warning.deleteBusinessType\")",
                        "right": "viz.val_cleared",
                        "width": "93%"
                    }
                }
            }, {
                "overrides": {}
            }, {
                "overrides": {}
            });
            flxConflictPopup.add(conflictPopup);
            flxMain.add(flxLeftPanel, flxRightPanel, flxHeaderDropdown, flxToastMessage, flxLoading, flxViewRolePopup, flxActionPopup, flxConflictPopup);
            this.add(flxMain);
        };
        return [{
            "addWidgets": addWidgetsfrmBusinessTypes,
            "enabledForIdleTimeout": false,
            "id": "frmBusinessTypes",
            "layoutType": kony.flex.FREE_FORM,
            "needAppMenu": false,
            "preShow": function(eventobject) {
                controller.AS_Form_a45a6f80c7dd4237b8136a547ea30e8a(eventobject);
            },
            "skin": "slForm"
        }, {
            "displayOrientation": constants.FORM_DISPLAY_ORIENTATION_PORTRAIT,
            "layoutType": kony.flex.FREE_FORM,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, {
            "retainScrollPosition": false
        }]
    }
});