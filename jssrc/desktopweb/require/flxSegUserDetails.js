define("flxSegUserDetails", function() {
    return function(controller) {
        var flxSegUserDetails = new kony.ui.FlexContainer({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "clipBounds": true,
            "height": "50px",
            "id": "flxSegUserDetails",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "isModalContainer": false,
            "skin": "sknflxffffffop100"
        }, {}, {
            "hoverSkin": "sknfbfcfc"
        });
        flxSegUserDetails.setDefaultUnit(kony.flex.DP);
        var flxAssignedCustomers = new kony.ui.FlexContainer({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "clipBounds": true,
            "height": "50px",
            "id": "flxAssignedCustomers",
            "isVisible": true,
            "layoutType": kony.flex.FLOW_HORIZONTAL,
            "left": "0",
            "isModalContainer": false,
            "skin": "slFbox",
            "top": "0",
            "width": "100%"
        }, {}, {});
        flxAssignedCustomers.setDefaultUnit(kony.flex.DP);
        var lblName = new kony.ui.Label({
            "centerY": "50%",
            "id": "lblName",
            "isVisible": true,
            "left": "7.50%",
            "skin": "sknlblLatoRegular484b5213px",
            "text": "Name",
            "width": "14%",
            "zIndex": 1
        }, {
            "contentAlignment": constants.CONTENT_ALIGN_TOP_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, {});
        var lblUserID = new kony.ui.Label({
            "centerY": "50%",
            "id": "lblUserID",
            "isVisible": true,
            "left": "2.50%",
            "skin": "sknlblLatoRegular484b5213px",
            "text": "User ID",
            "width": "15.50%",
            "zIndex": 1
        }, {
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, {});
        var lblrole = new kony.ui.Label({
            "centerY": "50%",
            "id": "lblrole",
            "isVisible": true,
            "left": "0%",
            "skin": "sknlblLatoRegular484b5213px",
            "text": "Role",
            "width": "22%",
            "zIndex": 1
        }, {
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, {});
        var lblView = new kony.ui.Label({
            "centerY": "50%",
            "id": "lblView",
            "isVisible": true,
            "left": "1.50%",
            "skin": "sknLbl117EB0LatoReg14px",
            "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmAdManagement.View\")",
            "width": "8%",
            "zIndex": 1
        }, {
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, {});
        var lblIconRemove = new kony.ui.Label({
            "centerY": "50%",
            "id": "lblIconRemove",
            "isVisible": true,
            "left": "22%",
            "right": "30dp",
            "skin": "sknIcon00000018px",
            "text": "",
            "top": "0",
            "width": kony.flex.USE_PREFFERED_SIZE
        }, {
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, {});
        flxAssignedCustomers.add(lblName, lblUserID, lblrole, lblView, lblIconRemove);
        var lblSeperator = new kony.ui.Label({
            "bottom": 0,
            "height": "1px",
            "id": "lblSeperator",
            "isVisible": true,
            "left": "20dp",
            "right": "20dp",
            "skin": "sknlblSeperatorD7D9E0",
            "text": ".",
            "width": "95.91%",
            "zIndex": 1
        }, {
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, {});
        flxSegUserDetails.add(flxAssignedCustomers, lblSeperator);
        return flxSegUserDetails;
    }
})