define("flxAttributeRow", function() {
    return function(controller) {
        var flxAttributeRow = new kony.ui.FlexContainer({
            "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
            "clipBounds": true,
            "id": "flxAttributeRow",
            "isVisible": true,
            "layoutType": kony.flex.FLOW_HORIZONTAL,
            "left": "0dp",
            "isModalContainer": false,
            "skin": "slFbox",
            "top": "0dp",
            "width": "100%"
        }, {}, {});
        flxAttributeRow.setDefaultUnit(kony.flex.DP);
        var flxRow = new kony.ui.FlexContainer({
            "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
            "clipBounds": true,
            "id": "flxRow",
            "isVisible": true,
            "layoutType": kony.flex.FLOW_HORIZONTAL,
            "left": "0dp",
            "minHeight": "40dp",
            "isModalContainer": false,
            "skin": "slFbox",
            "top": "0dp",
            "width": "100%"
        }, {}, {});
        flxRow.setDefaultUnit(kony.flex.DP);
        var lblDataset = new kony.ui.Label({
            "bottom": "10dp",
            "id": "lblDataset",
            "isVisible": true,
            "left": "0",
            "skin": "sknLbl485C75LatoRegular13Px",
            "text": "DATASET",
            "top": "10dp",
            "width": "24%",
            "zIndex": 1
        }, {
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, {});
        var lblAttribute = new kony.ui.Label({
            "bottom": "10dp",
            "id": "lblAttribute",
            "isVisible": true,
            "left": "1%",
            "skin": "sknLbl485C75LatoRegular13Px",
            "text": "Attribute",
            "top": "10dp",
            "width": "24%",
            "zIndex": 1
        }, {
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, {});
        var lblCriteria = new kony.ui.Label({
            "bottom": "10dp",
            "id": "lblCriteria",
            "isVisible": true,
            "left": "1%",
            "skin": "sknLbl485C75LatoRegular13Px",
            "text": "Criteria",
            "top": "10dp",
            "width": "24%",
            "zIndex": 1
        }, {
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, {});
        var lblValue = new kony.ui.Label({
            "bottom": "10dp",
            "id": "lblValue",
            "isVisible": true,
            "left": "1%",
            "skin": "sknLbl485C75LatoRegular13Px",
            "text": "Value",
            "top": "10dp",
            "width": "24%",
            "zIndex": 1
        }, {
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, {});
        flxRow.add(lblDataset, lblAttribute, lblCriteria, lblValue);
        flxAttributeRow.add(flxRow);
        return flxAttributeRow;
    }
})