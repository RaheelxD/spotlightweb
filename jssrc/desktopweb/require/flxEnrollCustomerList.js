define("flxEnrollCustomerList", function() {
    return function(controller) {
        var flxEnrollCustomerList = new kony.ui.FlexContainer({
            "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
            "clipBounds": true,
            "id": "flxEnrollCustomerList",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "left": "0dp",
            "isModalContainer": false,
            "skin": "sknFlxFFFFFF100O",
            "top": "0dp",
            "width": "100%"
        }, {}, {});
        flxEnrollCustomerList.setDefaultUnit(kony.flex.DP);
        var flxCustomerIdContainer = new kony.ui.FlexContainer({
            "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
            "bottom": "30dp",
            "clipBounds": true,
            "id": "flxCustomerIdContainer",
            "isVisible": true,
            "layoutType": kony.flex.FLOW_HORIZONTAL,
            "left": "10dp",
            "isModalContainer": false,
            "skin": "slFbox",
            "top": "30dp",
            "width": "22%",
            "zIndex": 1
        }, {}, {});
        flxCustomerIdContainer.setDefaultUnit(kony.flex.DP);
        var lblCustomerId = new kony.ui.Label({
            "id": "lblCustomerId",
            "isVisible": true,
            "left": "0dp",
            "skin": "sknLbl192b45LatoReg16px",
            "text": "Label",
            "top": "0dp",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, {
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, {});
        var flxPrimary = new kony.ui.FlexContainer({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "clipBounds": true,
            "height": "20dp",
            "id": "flxPrimary",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "left": "10dp",
            "isModalContainer": false,
            "skin": "sknflx1F844DRadius20px",
            "top": "0dp",
            "width": "61dp",
            "zIndex": 1
        }, {}, {});
        flxPrimary.setDefaultUnit(kony.flex.DP);
        var lblPrimary = new kony.ui.Label({
            "centerY": "50%",
            "id": "lblPrimary",
            "isVisible": true,
            "left": "10dp",
            "skin": "sknlblffffffLato12px",
            "text": "Primary",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, {
            "contentAlignment": constants.CONTENT_ALIGN_TOP_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, {});
        flxPrimary.add(lblPrimary);
        flxCustomerIdContainer.add(lblCustomerId, flxPrimary);
        var lblCustomerName = new kony.ui.Label({
            "bottom": "30dp",
            "id": "lblCustomerName",
            "isVisible": true,
            "left": "25%",
            "skin": "sknLbl192b45LatoReg16px",
            "text": "Label",
            "top": "30dp",
            "width": "23%",
            "zIndex": 1
        }, {
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, {});
        var lstBoxService = new kony.ui.ListBox({
            "bottom": "20dp",
            "focusSkin": "defListBoxFocus",
            "height": "40dp",
            "id": "lstBoxService",
            "isVisible": true,
            "left": "50%",
            "masterData": [
                ["lb1", "Placeholder One"],
                ["lb2", "Placeholder Two"],
                ["lb3", "Placeholder Three"]
            ],
            "skin": "sknLbxborderd7d9e03pxradius",
            "top": "20dp",
            "width": "19%",
            "zIndex": 1
        }, {
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [1, 0, 0, 0],
            "paddingInPixel": false
        }, {
            "multiSelect": false
        });
        var flxServiceError = new kony.ui.FlexContainer({
            "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
            "clipBounds": true,
            "id": "flxServiceError",
            "isVisible": false,
            "layoutType": kony.flex.FREE_FORM,
            "left": "50%",
            "isModalContainer": false,
            "skin": "slFbox",
            "top": "61dp",
            "width": "20%",
            "zIndex": 1
        }, {}, {});
        flxServiceError.setDefaultUnit(kony.flex.DP);
        var lblIconServiceError = new kony.ui.Label({
            "height": "15dp",
            "id": "lblIconServiceError",
            "isVisible": true,
            "left": "0dp",
            "skin": "sknErrorIcon",
            "text": "",
            "top": "0dp",
            "width": "15dp",
            "zIndex": 1
        }, {
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, {});
        var lblServiceErrorMsg = new kony.ui.Label({
            "id": "lblServiceErrorMsg",
            "isVisible": true,
            "left": "17dp",
            "skin": "sknlblError",
            "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmServiceManagement.lblNoServiceTypeError\")",
            "top": "0dp",
            "width": "80%",
            "zIndex": 1
        }, {
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, {});
        flxServiceError.add(lblIconServiceError, lblServiceErrorMsg);
        var lstBoxRole = new kony.ui.ListBox({
            "bottom": "20dp",
            "focusSkin": "defListBoxFocus",
            "height": "40dp",
            "id": "lstBoxRole",
            "isVisible": true,
            "left": "71%",
            "masterData": [
                ["lb1", "Placeholder One"],
                ["lb2", "Placeholder Two"],
                ["lb3", "Placeholder Three"]
            ],
            "skin": "sknLbxborderd7d9e03pxradius",
            "top": "20dp",
            "width": "19%",
            "zIndex": 1
        }, {
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [1, 0, 0, 0],
            "paddingInPixel": false
        }, {
            "multiSelect": false
        });
        var flxRoleError = new kony.ui.FlexContainer({
            "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
            "clipBounds": true,
            "id": "flxRoleError",
            "isVisible": false,
            "layoutType": kony.flex.FREE_FORM,
            "left": "71%",
            "isModalContainer": false,
            "skin": "slFbox",
            "top": "61dp",
            "width": "21%",
            "zIndex": 1
        }, {}, {});
        flxRoleError.setDefaultUnit(kony.flex.DP);
        var lblIconRoleError = new kony.ui.Label({
            "height": "15dp",
            "id": "lblIconRoleError",
            "isVisible": true,
            "left": "0dp",
            "skin": "sknErrorIcon",
            "text": "",
            "top": "0dp",
            "width": "15dp",
            "zIndex": 1
        }, {
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, {});
        var lblRoleErrorMsg = new kony.ui.Label({
            "id": "lblRoleErrorMsg",
            "isVisible": true,
            "left": "17dp",
            "skin": "sknlblError",
            "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmServiceManagement.lblNoServiceTypeError\")",
            "top": "0dp",
            "width": "80%",
            "zIndex": 1
        }, {
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, {});
        flxRoleError.add(lblIconRoleError, lblRoleErrorMsg);
        var flxOptions = new kony.ui.FlexContainer({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "clipBounds": true,
            "height": "27dp",
            "id": "flxOptions",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "isModalContainer": false,
            "right": "15dp",
            "skin": "sknFlxBorffffff1pxRound",
            "top": "27dp",
            "width": "27px",
            "zIndex": 2
        }, {}, {
            "hoverSkin": "sknflxffffffop100Border424242Radius100px"
        });
        flxOptions.setDefaultUnit(kony.flex.DP);
        var lblOptions = new kony.ui.Label({
            "centerX": "50%",
            "centerY": "50%",
            "height": "15dp",
            "id": "lblOptions",
            "isVisible": true,
            "skin": "sknFontIconOptionMenu",
            "text": "",
            "width": "15dp",
            "zIndex": 1
        }, {
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, {});
        flxOptions.add(lblOptions);
        var lblRemoved = new kony.ui.Label({
            "id": "lblRemoved",
            "isVisible": false,
            "right": "10dp",
            "skin": "sknLbl485c75LatoRegItalic13px",
            "text": "Removed",
            "top": "30dp",
            "width": "60dp"
        }, {
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, {});
        var lblSeperator = new kony.ui.Label({
            "bottom": "0dp",
            "height": "1dp",
            "id": "lblSeperator",
            "isVisible": true,
            "left": "0dp",
            "right": "0dp",
            "skin": "sknlblSeperatorD7D9E0",
            "text": "Label",
            "width": kony.flex.USE_PREFFERED_SIZE
        }, {
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, {});
        flxEnrollCustomerList.add(flxCustomerIdContainer, lblCustomerName, lstBoxService, flxServiceError, lstBoxRole, flxRoleError, flxOptions, lblRemoved, lblSeperator);
        return flxEnrollCustomerList;
    }
})