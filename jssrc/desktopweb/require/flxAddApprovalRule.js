define("flxAddApprovalRule", function() {
    return function(controller) {
        var flxAddApprovalRule = new kony.ui.FlexContainer({
            "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
            "clipBounds": true,
            "id": "flxAddApprovalRule",
            "isVisible": true,
            "layoutType": kony.flex.FLOW_VERTICAL,
            "left": "0dp",
            "isModalContainer": false,
            "skin": "sknFlxFFFFFF100O",
            "top": "0dp",
            "width": "100%"
        }, {}, {});
        flxAddApprovalRule.setDefaultUnit(kony.flex.DP);
        var flxAddApprovalCont = new kony.ui.FlexContainer({
            "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
            "clipBounds": true,
            "id": "flxAddApprovalCont",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "left": "0dp",
            "isModalContainer": false,
            "skin": "slFbox",
            "top": "15dp",
            "width": "100%"
        }, {}, {});
        flxAddApprovalCont.setDefaultUnit(kony.flex.DP);
        var lstBoxRangeType = new kony.ui.ListBox({
            "focusSkin": "defListBoxFocus",
            "height": "40dp",
            "id": "lstBoxRangeType",
            "isVisible": true,
            "left": "20dp",
            "masterData": [
                ["lb1", "Placeholder One"],
                ["lb2", "Placeholder Two"],
                ["lb3", "Placeholder Three"]
            ],
            "skin": "sknLbxborderd7d9e03pxradius",
            "top": "0",
            "width": "17%"
        }, {
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [1, 0, 0, 0],
            "paddingInPixel": false
        }, {
            "multiSelect": false
        });
        var flxRangeSingleValue = new kony.ui.FlexContainer({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "clipBounds": true,
            "height": "40dp",
            "id": "flxRangeSingleValue",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "left": "22%",
            "isModalContainer": false,
            "skin": "slFbox",
            "top": "0dp",
            "width": "35%",
            "zIndex": 2
        }, {}, {});
        flxRangeSingleValue.setDefaultUnit(kony.flex.DP);
        var lblIconCurrencySingle = new kony.ui.Label({
            "centerY": "50%",
            "id": "lblIconCurrencySingle",
            "isVisible": true,
            "left": "5dp",
            "skin": "sknIcomoonCurrencySymbol",
            "text": "",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 2
        }, {
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, {});
        var tbxSingleRangeValue = new kony.ui.TextBox2({
            "autoCapitalize": constants.TEXTBOX_AUTO_CAPITALIZE_NONE,
            "focusSkin": "defTextBoxFocus",
            "height": "40dp",
            "id": "tbxSingleRangeValue",
            "isVisible": true,
            "keyBoardStyle": constants.TEXTBOX_KEY_BOARD_STYLE_DEFAULT,
            "left": "0",
            "placeholder": "Placeholder",
            "right": "0dp",
            "secureTextEntry": false,
            "skin": "sknTbxBgFFFFFFBrD7D9E01pxR3px",
            "textInputMode": constants.TEXTBOX_INPUT_MODE_ANY,
            "top": "0"
        }, {
            "containerHeightMode": constants.TEXTBOX_FONT_METRICS_DRIVEN_HEIGHT,
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [7, 0, 0, 0],
            "paddingInPixel": false
        }, {
            "autoCorrect": false,
            "placeholderSkin": "defTextBoxPlaceholder"
        });
        flxRangeSingleValue.add(lblIconCurrencySingle, tbxSingleRangeValue);
        var flxSingleValError = new kony.ui.FlexContainer({
            "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
            "clipBounds": true,
            "id": "flxSingleValError",
            "isVisible": false,
            "layoutType": kony.flex.FLOW_HORIZONTAL,
            "left": "22%",
            "isModalContainer": false,
            "skin": "slFbox",
            "top": "40dp",
            "width": "35%"
        }, {}, {});
        flxSingleValError.setDefaultUnit(kony.flex.DP);
        var lblIconError1 = new kony.ui.Label({
            "id": "lblIconError1",
            "isVisible": true,
            "left": "0dp",
            "skin": "sknFontIconError",
            "text": "",
            "top": "2dp",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, {
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, {});
        var lblErrorText1 = new kony.ui.Label({
            "id": "lblErrorText1",
            "isVisible": true,
            "left": "10dp",
            "skin": "sknlblError",
            "text": "Error",
            "top": "0dp",
            "width": "85%",
            "zIndex": 1
        }, {
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, {});
        flxSingleValError.add(lblIconError1, lblErrorText1);
        var flxRangeMultiValue = new kony.ui.FlexContainer({
            "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
            "clipBounds": true,
            "id": "flxRangeMultiValue",
            "isVisible": false,
            "layoutType": kony.flex.FREE_FORM,
            "left": "22%",
            "isModalContainer": false,
            "skin": "slFbox",
            "top": "0dp",
            "width": "35%",
            "zIndex": 2
        }, {}, {});
        flxRangeMultiValue.setDefaultUnit(kony.flex.DP);
        var flxFromRangeCont = new kony.ui.FlexContainer({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "clipBounds": true,
            "height": "40dp",
            "id": "flxFromRangeCont",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "left": "0dp",
            "isModalContainer": false,
            "skin": "slFbox",
            "top": "0dp",
            "width": "50%"
        }, {}, {});
        flxFromRangeCont.setDefaultUnit(kony.flex.DP);
        var lblIconCurrencyFrom = new kony.ui.Label({
            "centerY": "50%",
            "id": "lblIconCurrencyFrom",
            "isVisible": true,
            "left": "3dp",
            "skin": "sknIcomoonCurrencySymbol",
            "text": "",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 2
        }, {
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, {});
        var tbxFromRangeValue = new kony.ui.TextBox2({
            "autoCapitalize": constants.TEXTBOX_AUTO_CAPITALIZE_NONE,
            "focusSkin": "defTextBoxFocus",
            "height": "40dp",
            "id": "tbxFromRangeValue",
            "isVisible": true,
            "keyBoardStyle": constants.TEXTBOX_KEY_BOARD_STYLE_DEFAULT,
            "left": "0",
            "placeholder": "Placeholder",
            "right": "10dp",
            "secureTextEntry": false,
            "skin": "sknTbxBgFFFFFFBrD7D9E01pxR3px",
            "textInputMode": constants.TEXTBOX_INPUT_MODE_ANY,
            "top": "0",
            "width": "100%"
        }, {
            "containerHeightMode": constants.TEXTBOX_FONT_METRICS_DRIVEN_HEIGHT,
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [14, 0, 0, 0],
            "paddingInPixel": false
        }, {
            "autoCorrect": false,
            "placeholderSkin": "defTextBoxPlaceholder"
        });
        flxFromRangeCont.add(lblIconCurrencyFrom, tbxFromRangeValue);
        var flxToRangeCont = new kony.ui.FlexContainer({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "clipBounds": true,
            "height": "40dp",
            "id": "flxToRangeCont",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "left": "50%",
            "isModalContainer": false,
            "skin": "slFbox",
            "top": "0dp",
            "width": "50%"
        }, {}, {});
        flxToRangeCont.setDefaultUnit(kony.flex.DP);
        var lblIconCurrencyTo = new kony.ui.Label({
            "centerY": "50%",
            "id": "lblIconCurrencyTo",
            "isVisible": true,
            "left": "13dp",
            "skin": "sknIcomoonCurrencySymbol",
            "text": "",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 2
        }, {
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, {});
        var tbxToRangeValue = new kony.ui.TextBox2({
            "autoCapitalize": constants.TEXTBOX_AUTO_CAPITALIZE_NONE,
            "focusSkin": "defTextBoxFocus",
            "height": "40dp",
            "id": "tbxToRangeValue",
            "isVisible": true,
            "keyBoardStyle": constants.TEXTBOX_KEY_BOARD_STYLE_DEFAULT,
            "left": "10dp",
            "placeholder": "Placeholder",
            "right": "2dp",
            "secureTextEntry": false,
            "skin": "sknTbxBgFFFFFFBrD7D9E01pxR3px",
            "textInputMode": constants.TEXTBOX_INPUT_MODE_ANY,
            "top": "0dp"
        }, {
            "containerHeightMode": constants.TEXTBOX_FONT_METRICS_DRIVEN_HEIGHT,
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [14, 0, 0, 0],
            "paddingInPixel": false
        }, {
            "autoCorrect": false,
            "placeholderSkin": "defTextBoxPlaceholder"
        });
        flxToRangeCont.add(lblIconCurrencyTo, tbxToRangeValue);
        var flxFromValError = new kony.ui.FlexContainer({
            "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
            "clipBounds": true,
            "id": "flxFromValError",
            "isVisible": false,
            "layoutType": kony.flex.FLOW_HORIZONTAL,
            "left": "0dp",
            "isModalContainer": false,
            "skin": "slFbox",
            "top": "40dp",
            "width": "50%"
        }, {}, {});
        flxFromValError.setDefaultUnit(kony.flex.DP);
        var lblIconError2 = new kony.ui.Label({
            "id": "lblIconError2",
            "isVisible": true,
            "left": "0dp",
            "skin": "sknFontIconError",
            "text": "",
            "top": "2dp",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, {
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, {});
        var lblErrorText2 = new kony.ui.Label({
            "id": "lblErrorText2",
            "isVisible": true,
            "left": "10dp",
            "skin": "sknlblError",
            "text": "Error",
            "top": "0dp",
            "width": "85%",
            "zIndex": 1
        }, {
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, {});
        flxFromValError.add(lblIconError2, lblErrorText2);
        var flxToValError = new kony.ui.FlexContainer({
            "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
            "clipBounds": true,
            "id": "flxToValError",
            "isVisible": false,
            "layoutType": kony.flex.FLOW_HORIZONTAL,
            "left": "52%",
            "isModalContainer": false,
            "right": "0dp",
            "skin": "slFbox",
            "top": "40dp"
        }, {}, {});
        flxToValError.setDefaultUnit(kony.flex.DP);
        var lblIconError3 = new kony.ui.Label({
            "id": "lblIconError3",
            "isVisible": true,
            "left": "0dp",
            "skin": "sknFontIconError",
            "text": "",
            "top": "2dp",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, {
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, {});
        var lblErrorText3 = new kony.ui.Label({
            "id": "lblErrorText3",
            "isVisible": true,
            "left": "10dp",
            "skin": "sknlblError",
            "text": "Error",
            "top": "0dp",
            "width": "85%",
            "zIndex": 1
        }, {
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, {});
        flxToValError.add(lblIconError3, lblErrorText3);
        flxRangeMultiValue.add(flxFromRangeCont, flxToRangeCont, flxFromValError, flxToValError);
        var flxRequiredCheckbox = new kony.ui.FlexContainer({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "clipBounds": true,
            "height": "40dp",
            "id": "flxRequiredCheckbox",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "left": "60%",
            "isModalContainer": false,
            "skin": "slFbox",
            "top": "0dp",
            "width": "15%"
        }, {}, {});
        flxRequiredCheckbox.setDefaultUnit(kony.flex.DP);
        var flxApprovalCheckbox = new kony.ui.FlexContainer({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "centerY": "50%",
            "clipBounds": false,
            "height": "20dp",
            "id": "flxApprovalCheckbox",
            "isVisible": true,
            "layoutType": kony.flex.FLOW_HORIZONTAL,
            "left": "0dp",
            "isModalContainer": false,
            "skin": "slFbox",
            "width": "20px",
            "zIndex": 1
        }, {}, {});
        flxApprovalCheckbox.setDefaultUnit(kony.flex.DP);
        var imgCheckbox = new kony.ui.Image2({
            "centerY": "50%",
            "height": "15dp",
            "id": "imgCheckbox",
            "isVisible": true,
            "left": "0",
            "skin": "slImage",
            "src": "checkboxnormal.png",
            "width": "15dp"
        }, {
            "imageScaleMode": constants.IMAGE_SCALE_MODE_MAINTAIN_ASPECT_RATIO,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, {});
        flxApprovalCheckbox.add(imgCheckbox);
        var lblApprovalRequired = new kony.ui.Label({
            "id": "lblApprovalRequired",
            "isVisible": true,
            "left": "30dp",
            "skin": "sknLatoSemibold485c7313px",
            "text": "Label",
            "top": "12dp",
            "width": kony.flex.USE_PREFFERED_SIZE
        }, {
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, {});
        flxRequiredCheckbox.add(flxApprovalCheckbox, lblApprovalRequired);
        var lblAddApprovalCondition = new kony.ui.Label({
            "id": "lblAddApprovalCondition",
            "isVisible": true,
            "left": "80%",
            "skin": "sknLblLatoReg117eb013px",
            "text": "Add Condition",
            "top": "12dp",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, {
            "contentAlignment": constants.CONTENT_ALIGN_TOP_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, {
            "hoverSkin": "sknLblLato13px117eb0Cursor"
        });
        var lblApprovalCondNA = new kony.ui.Label({
            "id": "lblApprovalCondNA",
            "isVisible": false,
            "left": "60%",
            "skin": "sknLatoSemibold485c7313px",
            "text": "N/A",
            "top": "13dp",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, {
            "contentAlignment": constants.CONTENT_ALIGN_TOP_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, {});
        var flxDeleteRule = new kony.ui.FlexContainer({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "clipBounds": true,
            "height": "25dp",
            "id": "flxDeleteRule",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "isModalContainer": false,
            "right": "40dp",
            "skin": "slFbox",
            "top": "7dp",
            "width": "25dp"
        }, {}, {});
        flxDeleteRule.setDefaultUnit(kony.flex.DP);
        var lblIconDeleteRule = new kony.ui.Label({
            "centerY": "50%",
            "id": "lblIconDeleteRule",
            "isVisible": true,
            "left": "5dp",
            "skin": "sknFontIcon485C7518px",
            "text": "",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 2
        }, {
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, {});
        flxDeleteRule.add(lblIconDeleteRule);
        var flxAddRuleOption = new kony.ui.FlexContainer({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "clipBounds": true,
            "height": "25dp",
            "id": "flxAddRuleOption",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "isModalContainer": false,
            "right": "80dp",
            "skin": "slFbox",
            "top": "7dp",
            "width": "25dp"
        }, {}, {});
        flxAddRuleOption.setDefaultUnit(kony.flex.DP);
        var lblIconAdd = new kony.ui.Label({
            "centerY": "50%",
            "id": "lblIconAdd",
            "isVisible": true,
            "left": "5dp",
            "skin": "sknFontIcon485C7518px",
            "text": "",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 2
        }, {
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, {});
        flxAddRuleOption.add(lblIconAdd);
        flxAddApprovalCont.add(lstBoxRangeType, flxRangeSingleValue, flxSingleValError, flxRangeMultiValue, flxRequiredCheckbox, lblAddApprovalCondition, lblApprovalCondNA, flxDeleteRule, flxAddRuleOption);
        var lblSeperator = new kony.ui.Label({
            "height": "1dp",
            "id": "lblSeperator",
            "isVisible": true,
            "left": "20dp",
            "right": "30dp",
            "skin": "sknLblSeparatore7e7e7",
            "text": "Label",
            "top": "15dp",
            "width": kony.flex.USE_PREFFERED_SIZE
        }, {
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, {});
        flxAddApprovalRule.add(flxAddApprovalCont, lblSeperator);
        return flxAddApprovalRule;
    }
})