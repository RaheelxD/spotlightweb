define("InternalUserModule/frmUsers", function() {
    return function(controller) {
        function addWidgetsfrmUsers() {
            this.setDefaultUnit(kony.flex.DP);
            var tbxDummy = new kony.ui.TextBox2({
                "autoCapitalize": constants.TEXTBOX_AUTO_CAPITALIZE_NONE,
                "focusSkin": "defTextBoxFocus",
                "height": "40dp",
                "id": "tbxDummy",
                "isVisible": false,
                "keyBoardStyle": constants.TEXTBOX_KEY_BOARD_STYLE_DEFAULT,
                "left": "456dp",
                "placeholder": "Placeholder",
                "secureTextEntry": true,
                "skin": "slTextBox",
                "textInputMode": constants.TEXTBOX_INPUT_MODE_ANY,
                "top": "4dp",
                "width": "300dp",
                "zIndex": 5
            }, {
                "containerHeightMode": constants.TEXTBOX_FONT_METRICS_DRIVEN_HEIGHT,
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [3, 0, 0, 0],
                "paddingInPixel": false
            }, {
                "autoCorrect": false,
                "placeholderSkin": "defTextBoxPlaceholder"
            });
            var flxMain = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "900dp",
                "id": "flxMain",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "slFbox0b7aaa2e3bfa340",
                "top": "0dp",
                "width": "100%",
                "zIndex": 1
            }, {}, {});
            flxMain.setDefaultUnit(kony.flex.DP);
            var flxLeftPannel = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "100%",
                "id": "flxLeftPannel",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "masterType": constants.MASTER_TYPE_USERWIDGET,
                "isModalContainer": false,
                "skin": "sknLeftPannel0bb79e4bcca8e45",
                "top": "0dp",
                "width": "305px",
                "zIndex": 5
            }, {}, {});
            flxLeftPannel.setDefaultUnit(kony.flex.DP);
            var leftMenuNew = new com.adminConsole.navigation.leftMenuNew({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "height": "100%",
                "id": "leftMenuNew",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "masterType": constants.MASTER_TYPE_DEFAULT,
                "isModalContainer": false,
                "skin": "sknFlxBg003E75Op100",
                "top": "0dp",
                "width": "100%",
                "overrides": {
                    "leftMenuNew": {
                        "bottom": "viz.val_cleared",
                        "centerX": "viz.val_cleared",
                        "centerY": "viz.val_cleared",
                        "height": "100%",
                        "left": "0dp",
                        "maxHeight": "viz.val_cleared",
                        "maxWidth": "viz.val_cleared",
                        "minHeight": "viz.val_cleared",
                        "minWidth": "viz.val_cleared",
                        "right": "viz.val_cleared",
                        "top": "0dp",
                        "width": "100%"
                    }
                }
            }, {
                "overrides": {}
            }, {
                "overrides": {}
            });
            flxLeftPannel.add(leftMenuNew);
            var flxRightPanel = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "100%",
                "id": "flxRightPanel",
                "isVisible": true,
                "layoutType": kony.flex.FLOW_VERTICAL,
                "left": "305px",
                "masterType": constants.MASTER_TYPE_USERWIDGET,
                "isModalContainer": false,
                "right": 0,
                "skin": "slFbox0b7aaa2e3bfa340",
                "top": "0.00%",
                "zIndex": 1
            }, {}, {});
            flxRightPanel.setDefaultUnit(kony.flex.DP);
            var flxMainHeader = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "106dp",
                "id": "flxMainHeader",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0dp",
                "width": "100%",
                "zIndex": 100
            }, {}, {});
            flxMainHeader.setDefaultUnit(kony.flex.DP);
            var mainHeader = new com.adminConsole.header.mainHeader({
                "height": "106px",
                "id": "mainHeader",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": 0,
                "masterType": constants.MASTER_TYPE_DEFAULT,
                "isModalContainer": false,
                "skin": "CopyslFbox1",
                "top": 0,
                "width": "100%",
                "overrides": {
                    "btnAddNewOption": {
                        "i18n_text": "kony.i18n.getLocalizedString(\"i18n.users.ADDNEWUSER\")"
                    },
                    "btnDropdownList": {
                        "i18n_text": "kony.i18n.getLocalizedString(\"i18n.mainHeader.DOWNLOADLIST\")"
                    },
                    "flxButtons": {
                        "right": "35dp",
                        "width": "400dp"
                    },
                    "imgLogout": {
                        "src": "img_logout.png"
                    }
                }
            }, {
                "overrides": {}
            }, {
                "overrides": {}
            });
            flxMainHeader.add(mainHeader);
            var flxUsersBreadCrumb = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "bottom": "20px",
                "clipBounds": true,
                "height": "20px",
                "id": "flxUsersBreadCrumb",
                "isVisible": false,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0px",
                "isModalContainer": false,
                "right": "0px",
                "skin": "slFbox",
                "top": "-10px",
                "width": "100%",
                "zIndex": 105
            }, {}, {});
            flxUsersBreadCrumb.setDefaultUnit(kony.flex.DP);
            var breadcrumbs = new com.adminConsole.common.breadcrumbs({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "bottom": "0px",
                "height": "20px",
                "id": "breadcrumbs",
                "isVisible": true,
                "layoutType": kony.flex.FLOW_HORIZONTAL,
                "left": "35px",
                "masterType": constants.MASTER_TYPE_DEFAULT,
                "isModalContainer": false,
                "right": "35px",
                "skin": "sknflxf5f6f8Op100",
                "top": "0px",
                "zIndex": 1,
                "overrides": {
                    "breadcrumbs": {
                        "bottom": "0px",
                        "left": "35px",
                        "right": "35px",
                        "top": "0px",
                        "width": "viz.val_cleared",
                        "zIndex": 1
                    },
                    "btnBackToMain": {
                        "left": "0px",
                        "text": "ALERTS"
                    },
                    "btnPreviousPage": {
                        "isVisible": true
                    },
                    "fontIconBreadcrumbsRight2": {
                        "isVisible": true
                    },
                    "imgBreadcrumbsRight2": {
                        "isVisible": false
                    },
                    "lblCurrentScreen": {
                        "isVisible": true,
                        "width": kony.flex.USE_PREFFERED_SIZE
                    }
                }
            }, {
                "overrides": {}
            }, {
                "overrides": {}
            });
            flxUsersBreadCrumb.add(breadcrumbs);
            var flxMainSubHeader = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "63dp",
                "id": "flxMainSubHeader",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0dp",
                "width": "100%",
                "zIndex": 100
            }, {}, {});
            flxMainSubHeader.setDefaultUnit(kony.flex.DP);
            var subHeader = new com.adminConsole.header.subHeader({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "centerY": "50%",
                "height": "48dp",
                "id": "subHeader",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "masterType": constants.MASTER_TYPE_DEFAULT,
                "isModalContainer": false,
                "skin": "CopyslFbox2",
                "top": "0dp",
                "width": "100%",
                "overrides": {
                    "flxMenu": {
                        "isVisible": false,
                        "left": "35dp"
                    },
                    "flxSearch": {
                        "top": "0px",
                        "width": "350px"
                    },
                    "flxSearchContainer": {
                        "left": "0dp",
                        "right": "0dp",
                        "top": "5dp"
                    },
                    "flxSubHeader": {
                        "bottom": "0dp",
                        "height": "100%",
                        "top": "0dp"
                    },
                    "subHeader": {
                        "centerY": "50%",
                        "height": "48dp",
                        "top": "0dp"
                    }
                }
            }, {
                "overrides": {}
            }, {
                "overrides": {}
            });
            flxMainSubHeader.add(subHeader);
            var flxScrollMainContent = new kony.ui.FlexContainer({
                "bottom": 0,
                "clipBounds": false,
                "height": "100%",
                "id": "flxScrollMainContent",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0px",
                "isModalContainer": false,
                "right": "0px",
                "top": "0px",
                "zIndex": 1
            }, {}, {});
            flxScrollMainContent.setDefaultUnit(kony.flex.DP);
            var flxPermissions = new kony.ui.FlexContainer({
                "clipBounds": false,
                "height": "100%",
                "id": "flxPermissions",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0px",
                "isModalContainer": false,
                "right": "0px",
                "top": "0px",
                "zIndex": 1
            }, {}, {});
            flxPermissions.setDefaultUnit(kony.flex.DP);
            var flxUsersContainer = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "bottom": "10dp",
                "clipBounds": false,
                "id": "flxUsersContainer",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "35dp",
                "isModalContainer": false,
                "right": "35dp",
                "skin": "sknflxffffffoptemplateop3px",
                "top": "0dp",
                "zIndex": 1
            }, {}, {});
            flxUsersContainer.setDefaultUnit(kony.flex.DP);
            var flxUsersHeader = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "65px",
                "id": "flxUsersHeader",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "20dp",
                "isModalContainer": false,
                "right": "20dp",
                "skin": "sknflxffffffop100",
                "top": "0",
                "zIndex": 11
            }, {}, {});
            flxUsersHeader.setDefaultUnit(kony.flex.DP);
            var flxUsersHeaderFullName = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "100%",
                "id": "flxUsersHeaderFullName",
                "isVisible": true,
                "layoutType": kony.flex.FLOW_HORIZONTAL,
                "left": "10px",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0dp",
                "width": "120px",
                "zIndex": 1
            }, {}, {});
            flxUsersHeaderFullName.setDefaultUnit(kony.flex.DP);
            var lblUsersHeaderName = new kony.ui.Label({
                "centerY": "50%",
                "id": "lblUsersHeaderName",
                "isVisible": true,
                "skin": "sknlblLato696c7312px",
                "text": "FULL NAME",
                "top": 0,
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {
                "hoverSkin": "sknlblLatoBold00000012px"
            });
            var lblSortName = new kony.ui.Label({
                "centerY": "50%",
                "id": "lblSortName",
                "isVisible": true,
                "left": "7dp",
                "skin": "sknIcon15px",
                "text": "",
                "top": "0dp",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {
                "hoverSkin": "sknlblCursorFont"
            });
            flxUsersHeaderFullName.add(lblUsersHeaderName, lblSortName);
            var flxUsersHeaderUsername = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "100%",
                "id": "flxUsersHeaderUsername",
                "isVisible": true,
                "layoutType": kony.flex.FLOW_HORIZONTAL,
                "left": "19.70%",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0dp",
                "width": "20%",
                "zIndex": 1
            }, {}, {});
            flxUsersHeaderUsername.setDefaultUnit(kony.flex.DP);
            var lblUsersHeaderUsername = new kony.ui.Label({
                "centerY": "50%",
                "id": "lblUsersHeaderUsername",
                "isVisible": true,
                "left": "0px",
                "skin": "sknlblLato696c7312px",
                "text": "USERNAME",
                "top": 0,
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {
                "hoverSkin": "sknlblLatoBold00000012px"
            });
            var lblSortUsername = new kony.ui.Label({
                "centerY": "50%",
                "id": "lblSortUsername",
                "isVisible": true,
                "left": "7dp",
                "skin": "sknIcon15px",
                "text": "",
                "top": "0dp",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {
                "hoverSkin": "sknlblCursorFont"
            });
            flxUsersHeaderUsername.add(lblUsersHeaderUsername, lblSortUsername);
            var flxUsersHeaderEmailId = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "100%",
                "id": "flxUsersHeaderEmailId",
                "isVisible": true,
                "layoutType": kony.flex.FLOW_HORIZONTAL,
                "left": "40%",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0dp",
                "width": "29%",
                "zIndex": 1
            }, {}, {});
            flxUsersHeaderEmailId.setDefaultUnit(kony.flex.DP);
            var lblUsersHeaderEmail = new kony.ui.Label({
                "centerY": "50%",
                "id": "lblUsersHeaderEmail",
                "isVisible": true,
                "left": "0px",
                "skin": "sknlblLato696c7312px",
                "text": "EMAIL",
                "top": 0,
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {
                "hoverSkin": "sknlblLatoBold00000012px"
            });
            var lblSortEmail = new kony.ui.Label({
                "centerY": "50%",
                "id": "lblSortEmail",
                "isVisible": true,
                "left": "7dp",
                "skin": "sknIcon15px",
                "text": "",
                "top": "0dp",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {
                "hoverSkin": "sknlblCursorFont"
            });
            flxUsersHeaderEmailId.add(lblUsersHeaderEmail, lblSortEmail);
            var flxUsersHeaderRole = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "100%",
                "id": "flxUsersHeaderRole",
                "isVisible": true,
                "layoutType": kony.flex.FLOW_HORIZONTAL,
                "left": "69%",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0dp",
                "width": "100px",
                "zIndex": 1
            }, {}, {});
            flxUsersHeaderRole.setDefaultUnit(kony.flex.DP);
            var lblUsersHeaderRole = new kony.ui.Label({
                "centerY": "50%",
                "id": "lblUsersHeaderRole",
                "isVisible": true,
                "left": "0px",
                "skin": "sknlblLato696c7312px",
                "text": "ROLE",
                "top": 0,
                "width": "35px",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {
                "hoverSkin": "sknlblLatoBold00000012px"
            });
            var lblSortRole = new kony.ui.Label({
                "centerY": "50%",
                "id": "lblSortRole",
                "isVisible": true,
                "left": "7dp",
                "skin": "sknIcon15px",
                "text": "",
                "top": "0dp",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {
                "hoverSkin": "sknlblCursorFont"
            });
            flxUsersHeaderRole.add(lblUsersHeaderRole, lblSortRole);
            var flxUsersHeaderPermissions = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "100%",
                "id": "flxUsersHeaderPermissions",
                "isVisible": false,
                "layoutType": kony.flex.FLOW_HORIZONTAL,
                "left": "69%",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0dp",
                "width": "120px",
                "zIndex": 1
            }, {}, {});
            flxUsersHeaderPermissions.setDefaultUnit(kony.flex.DP);
            var lblUsersHeaderPermissions = new kony.ui.Label({
                "centerY": "50%",
                "id": "lblUsersHeaderPermissions",
                "isVisible": true,
                "left": "0px",
                "skin": "sknlblLato696c7312px",
                "text": "PERMISSIONS",
                "top": 0,
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {
                "hoverSkin": "sknlblLatoBold00000012px"
            });
            var lblSortPermissions = new kony.ui.Label({
                "centerY": "50%",
                "id": "lblSortPermissions",
                "isVisible": true,
                "left": "7dp",
                "skin": "sknIcon15px",
                "text": "",
                "top": "0dp",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {
                "hoverSkin": "sknlblCursorFont"
            });
            flxUsersHeaderPermissions.add(lblUsersHeaderPermissions, lblSortPermissions);
            var flxUsersHeaderStatus = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "100%",
                "id": "flxUsersHeaderStatus",
                "isVisible": true,
                "layoutType": kony.flex.FLOW_HORIZONTAL,
                "left": "82%",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0dp",
                "width": "100px",
                "zIndex": 1
            }, {}, {});
            flxUsersHeaderStatus.setDefaultUnit(kony.flex.DP);
            var lblUsersHeaderStatus = new kony.ui.Label({
                "centerY": "50%",
                "id": "lblUsersHeaderStatus",
                "isVisible": true,
                "left": "0px",
                "skin": "sknlblLato696c7312px",
                "text": "STATUS",
                "top": 0,
                "width": "45px",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {
                "hoverSkin": "sknlblLatoBold00000012px"
            });
            var lblFilterStatus = new kony.ui.Label({
                "centerY": "50%",
                "id": "lblFilterStatus",
                "isVisible": true,
                "left": "7dp",
                "skin": "sknIcon15px",
                "text": "",
                "top": "0dp",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {
                "hoverSkin": "sknlblCursorFont"
            });
            flxUsersHeaderStatus.add(lblUsersHeaderStatus, lblFilterStatus);
            var lblUsersHeaderSeperator = new kony.ui.Label({
                "bottom": 0,
                "height": "1px",
                "id": "lblUsersHeaderSeperator",
                "isVisible": true,
                "left": "0dp",
                "right": "0dp",
                "skin": "sknLblTableHeaderLine",
                "text": ".",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxUsersHeader.add(flxUsersHeaderFullName, flxUsersHeaderUsername, flxUsersHeaderEmailId, flxUsersHeaderRole, flxUsersHeaderPermissions, flxUsersHeaderStatus, lblUsersHeaderSeperator);
            var flxUserStatusFilter = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                "clipBounds": true,
                "id": "flxUserStatusFilter",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "isModalContainer": false,
                "right": "20dp",
                "skin": "slFbox",
                "top": "40dp",
                "width": "120px",
                "zIndex": 11
            }, {}, {});
            flxUserStatusFilter.setDefaultUnit(kony.flex.DP);
            var statusFilterMenu = new com.adminConsole.common.statusFilterMenu({
                "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                "id": "statusFilterMenu",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "masterType": constants.MASTER_TYPE_DEFAULT,
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "5dp",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 5,
                "overrides": {
                    "imgUpArrow": {
                        "left": "viz.val_cleared",
                        "right": "15dp",
                        "src": "uparrow_2x.png"
                    },
                    "statusFilterMenu": {
                        "width": kony.flex.USE_PREFFERED_SIZE
                    }
                }
            }, {
                "overrides": {}
            }, {
                "overrides": {}
            });
            flxUserStatusFilter.add(statusFilterMenu);
            var flxSegmentUsers = new kony.ui.FlexContainer({
                "bottom": "10dp",
                "clipBounds": false,
                "id": "flxSegmentUsers",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "isModalContainer": false,
                "right": "0dp",
                "skin": "slFbox",
                "top": "65dp",
                "zIndex": 1
            }, {}, {});
            flxSegmentUsers.setDefaultUnit(kony.flex.DP);
            var segUsers = new kony.ui.SegmentedUI2({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "data": [{
                    "fontIconStatusImg": "",
                    "lblEmailId": "",
                    "lblFontIconOptions": "",
                    "lblFullName": "",
                    "lblPermissions": "",
                    "lblRole": "",
                    "lblSeperator": "",
                    "lblUsername": "",
                    "lblUsersStatus": ""
                }],
                "groupCells": false,
                "height": "250px",
                "id": "segUsers",
                "isVisible": true,
                "left": "0dp",
                "needPageIndicator": true,
                "onRowClick": controller.AS_Segment_dafbdcf29f124135accc4cbaaeac51e8,
                "pageOffDotImage": "pageoffdot.png",
                "pageOnDotImage": "pageondot.png",
                "retainSelection": false,
                "right": "2px",
                "rowFocusSkin": "seg2Focus",
                "rowSkin": "sknseg3pxRadius",
                "rowTemplate": "flxUsers",
                "sectionHeaderSkin": "sliPhoneSegmentHeader",
                "selectionBehavior": constants.SEGUI_DEFAULT_BEHAVIOR,
                "separatorRequired": false,
                "showScrollbars": true,
                "top": "0dp",
                "viewType": constants.SEGUI_VIEW_TYPE_TABLEVIEW,
                "widgetDataMap": {
                    "flxOptions": "flxOptions",
                    "flxStatus": "flxStatus",
                    "flxUsers": "flxUsers",
                    "flxUsersContainer": "flxUsersContainer",
                    "fontIconStatusImg": "fontIconStatusImg",
                    "lblEmailId": "lblEmailId",
                    "lblFontIconOptions": "lblFontIconOptions",
                    "lblFullName": "lblFullName",
                    "lblPermissions": "lblPermissions",
                    "lblRole": "lblRole",
                    "lblSeperator": "lblSeperator",
                    "lblUsername": "lblUsername",
                    "lblUsersStatus": "lblUsersStatus"
                },
                "zIndex": 10
            }, {
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxSegmentUsers.add(segUsers);
            var flxNoFilterResults = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                "clipBounds": true,
                "id": "flxNoFilterResults",
                "isVisible": true,
                "layoutType": kony.flex.FLOW_VERTICAL,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0dp",
                "width": "100%"
            }, {}, {});
            flxNoFilterResults.setDefaultUnit(kony.flex.DP);
            var flxAdvancedSearch = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": false,
                "height": "96dp",
                "id": "flxAdvancedSearch",
                "isVisible": false,
                "layoutType": kony.flex.FLOW_VERTICAL,
                "left": "0dp",
                "isModalContainer": false,
                "right": "0dp",
                "skin": "slFbox",
                "top": "0dp",
                "width": "100%",
                "zIndex": 1
            }, {}, {});
            flxAdvancedSearch.setDefaultUnit(kony.flex.DP);
            var flxAdvSearchWrapper = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "95px",
                "id": "flxAdvSearchWrapper",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0dp",
                "width": "100%",
                "zIndex": 1
            }, {}, {});
            flxAdvSearchWrapper.setDefaultUnit(kony.flex.DP);
            var lblAdvSearchParam1 = new kony.ui.Label({
                "id": "lblAdvSearchParam1",
                "isVisible": true,
                "left": "35dp",
                "skin": "sknlblLatoRegular484b5213px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmPermissions.btnRoles\")",
                "top": "18dp",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var flxRoleDropDown = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "33dp",
                "id": "flxRoleDropDown",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "35dp",
                "isModalContainer": false,
                "skin": "sknflxffffffoptemplateop3px",
                "top": "44dp",
                "width": "134px",
                "zIndex": 1
            }, {}, {
                "hoverSkin": "sknCursor"
            });
            flxRoleDropDown.setDefaultUnit(kony.flex.DP);
            var lblSelectRole = new kony.ui.Label({
                "centerY": "50%",
                "id": "lblSelectRole",
                "isVisible": true,
                "left": "13dp",
                "skin": "sknlblLato5d6c7f12px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmUsers.lblSelectRole\")",
                "top": "10dp",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var flxDropDown1 = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "centerY": "50%",
                "clipBounds": true,
                "height": "10px",
                "id": "flxDropDown1",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "isModalContainer": false,
                "right": "10px",
                "skin": "slFbox",
                "width": "12px",
                "zIndex": 1
            }, {}, {
                "hoverSkin": "sknCursor"
            });
            flxDropDown1.setDefaultUnit(kony.flex.DP);
            var lblDropDown1 = new kony.ui.Label({
                "centerY": "50%",
                "id": "lblDropDown1",
                "isVisible": true,
                "right": "0dp",
                "skin": "sknfontIconDescDownArrow12px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmAlertsManagement.lblIconDescription\")",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxDropDown1.add(lblDropDown1);
            flxRoleDropDown.add(lblSelectRole, flxDropDown1);
            var lblAdvSearchParam2 = new kony.ui.Label({
                "id": "lblAdvSearchParam2",
                "isVisible": true,
                "left": "188dp",
                "skin": "sknlblLatoRegular484b5213px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmAlertsManagement.lblEditAlertStatus\")",
                "top": "18dp",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var flxStatusDropDown = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "33dp",
                "id": "flxStatusDropDown",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "188dp",
                "isModalContainer": false,
                "skin": "sknflxffffffoptemplateop3px",
                "top": "44dp",
                "width": "134px",
                "zIndex": 1
            }, {}, {
                "hoverSkin": "sknCursor"
            });
            flxStatusDropDown.setDefaultUnit(kony.flex.DP);
            var lblSelectStatus = new kony.ui.Label({
                "centerY": "50%",
                "id": "lblSelectStatus",
                "isVisible": true,
                "left": "13dp",
                "skin": "sknlblLato5d6c7f12px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmCSRController.Select_Status\")",
                "top": "10dp",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var flxDropDown2 = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "centerY": "50%",
                "clipBounds": true,
                "height": "10px",
                "id": "flxDropDown2",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "isModalContainer": false,
                "right": "10px",
                "skin": "slFbox",
                "width": "12px",
                "zIndex": 1
            }, {}, {
                "hoverSkin": "sknCursor"
            });
            flxDropDown2.setDefaultUnit(kony.flex.DP);
            var lblDropDown2 = new kony.ui.Label({
                "centerY": "50%",
                "id": "lblDropDown2",
                "isVisible": true,
                "right": "0dp",
                "skin": "sknfontIconDescDownArrow12px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmAlertsManagement.lblIconDescription\")",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxDropDown2.add(lblDropDown2);
            flxStatusDropDown.add(lblSelectStatus, flxDropDown2);
            var lblAdvSearchParam3 = new kony.ui.Label({
                "id": "lblAdvSearchParam3",
                "isVisible": true,
                "left": "340dp",
                "skin": "sknlblLatoRegular484b5213px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmUsersController.Created_Date\")",
                "top": "18dp",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var flxCreatedRangePicker = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "33px",
                "id": "flxCreatedRangePicker",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "342px",
                "isModalContainer": false,
                "skin": "sknflxffffffoptemplateop3px",
                "top": "44px",
                "width": "200px",
                "zIndex": 1
            }, {}, {});
            flxCreatedRangePicker.setDefaultUnit(kony.flex.DP);
            var customCalCreatedDate = new kony.ui.CustomWidget({
                "id": "customCalCreatedDate",
                "isVisible": true,
                "left": "1dp",
                "right": "3px",
                "bottom": "1px",
                "top": "5dp",
                "width": "170px",
                "zIndex": 1,
                "clipBounds": true
            }, {
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {
                "widgetName": "dateRangePicker",
                "drops": "down",
                "opens": "center",
                "rangeType": "",
                "resetData": "",
                "type": "list",
                "value": ""
            });
            var flxCloseCal1 = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "centerY": "50%",
                "clipBounds": true,
                "height": "10px",
                "id": "flxCloseCal1",
                "isVisible": false,
                "layoutType": kony.flex.FREE_FORM,
                "isModalContainer": false,
                "right": "10px",
                "skin": "slFbox",
                "width": "12px",
                "zIndex": 1
            }, {}, {
                "hoverSkin": "sknCursor"
            });
            flxCloseCal1.setDefaultUnit(kony.flex.DP);
            var lblCloseCal1 = new kony.ui.Label({
                "centerY": "50%",
                "id": "lblCloseCal1",
                "isVisible": true,
                "right": "0dp",
                "skin": "sknfontIconDescDownArrow12px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmAlertsManagement.lblAlertPreviewClose\")",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxCloseCal1.add(lblCloseCal1);
            flxCreatedRangePicker.add(customCalCreatedDate, flxCloseCal1);
            var lblAdvSearchParam4 = new kony.ui.Label({
                "id": "lblAdvSearchParam4",
                "isVisible": true,
                "left": "560dp",
                "skin": "sknlblLatoRegular484b5213px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmUsersController.Updated_Date\")",
                "top": "18dp",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var flxUpdatedRangePicker = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "33px",
                "id": "flxUpdatedRangePicker",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "560px",
                "isModalContainer": false,
                "skin": "sknflxffffffoptemplateop3px",
                "top": "44px",
                "width": "200px",
                "zIndex": 1
            }, {}, {});
            flxUpdatedRangePicker.setDefaultUnit(kony.flex.DP);
            var customCalUpdatedDate = new kony.ui.CustomWidget({
                "id": "customCalUpdatedDate",
                "isVisible": true,
                "left": "1dp",
                "right": "3px",
                "bottom": "1px",
                "top": "5dp",
                "width": "170px",
                "zIndex": 1,
                "clipBounds": true
            }, {
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {
                "widgetName": "dateRangePicker",
                "drops": "down",
                "opens": "left",
                "rangeType": "",
                "resetData": "",
                "type": "list",
                "value": ""
            });
            var flxCloseCal2 = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "centerY": "50%",
                "clipBounds": true,
                "height": "10px",
                "id": "flxCloseCal2",
                "isVisible": false,
                "layoutType": kony.flex.FREE_FORM,
                "isModalContainer": false,
                "right": "10px",
                "skin": "slFbox",
                "width": "12px",
                "zIndex": 1
            }, {}, {
                "hoverSkin": "sknCursor"
            });
            flxCloseCal2.setDefaultUnit(kony.flex.DP);
            var lblCloseCal2 = new kony.ui.Label({
                "centerY": "50%",
                "id": "lblCloseCal2",
                "isVisible": true,
                "right": "0dp",
                "skin": "sknfontIconDescDownArrow12px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmAlertsManagement.lblAlertPreviewClose\")",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxCloseCal2.add(lblCloseCal2);
            flxUpdatedRangePicker.add(customCalUpdatedDate, flxCloseCal2);
            var btnApply = new kony.ui.Button({
                "focusSkin": "sknbtnf7f7faLatoRegular12Px485c75Border1px485C75Radius20px",
                "height": "22px",
                "id": "btnApply",
                "isVisible": true,
                "left": "780px",
                "skin": "sknbtnf7f7faLatoRegular12Px485c75Border1px485C75Radius20px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmCustomerManagement.btnApply\")",
                "top": "50px",
                "width": "60px",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_CENTER,
                "displayText": true,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {
                "hoverSkin": "sknbtnffffffLatoRegular12Px485c75Border1px485C75Radius20px"
            });
            flxAdvSearchWrapper.add(lblAdvSearchParam1, flxRoleDropDown, lblAdvSearchParam2, flxStatusDropDown, lblAdvSearchParam3, flxCreatedRangePicker, lblAdvSearchParam4, flxUpdatedRangePicker, btnApply);
            var flxAdvSearchSeperator = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "bottom": 0,
                "clipBounds": true,
                "height": "1px",
                "id": "flxAdvSearchSeperator",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "sknflxd6dbe7",
                "width": "100%",
                "zIndex": 1
            }, {}, {});
            flxAdvSearchSeperator.setDefaultUnit(kony.flex.DP);
            flxAdvSearchSeperator.add();
            flxAdvancedSearch.add(flxAdvSearchWrapper, flxAdvSearchSeperator);
            var flxRtxNoResults = new kony.ui.FlexContainer({
                "clipBounds": true,
                "height": "220dp",
                "id": "flxRtxNoResults",
                "isVisible": false,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0dp",
                "width": "100%",
                "zIndex": 1
            }, {}, {});
            flxRtxNoResults.setDefaultUnit(kony.flex.DP);
            var rtxAvailableOptionsMessage = new kony.ui.RichText({
                "centerX": "50%",
                "centerY": "50%",
                "id": "rtxAvailableOptionsMessage",
                "isVisible": true,
                "skin": "sknRtxLato84939e12Px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmCSR.richtextNoResult\")",
                "width": "80%",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_CENTER,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxRtxNoResults.add(rtxAvailableOptionsMessage);
            flxNoFilterResults.add(flxAdvancedSearch, flxRtxNoResults);
            flxUsersContainer.add(flxUsersHeader, flxUserStatusFilter, flxSegmentUsers, flxNoFilterResults);
            var flxFilterDropdown = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                "clipBounds": true,
                "id": "flxFilterDropdown",
                "isVisible": false,
                "layoutType": kony.flex.FREE_FORM,
                "left": "225dp",
                "isModalContainer": false,
                "skin": "sknCursorDropDown",
                "top": "78dp",
                "width": "140px",
                "zIndex": 10
            }, {}, {});
            flxFilterDropdown.setDefaultUnit(kony.flex.DP);
            var segDropDown = new kony.ui.SegmentedUI2({
                "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                "data": [{
                    "imgCheckBox": "checkboxnormal.png",
                    "lblDescription": "KL098234 - Westheimer"
                }, {
                    "imgCheckBox": "checkboxnormal.png",
                    "lblDescription": "KL098234 - Westheimer"
                }, {
                    "imgCheckBox": "checkboxnormal.png",
                    "lblDescription": "KL098234 - Westheimer"
                }, {
                    "imgCheckBox": "checkboxnormal.png",
                    "lblDescription": "KL098234 - Westheimer"
                }, {
                    "imgCheckBox": "checkboxnormal.png",
                    "lblDescription": "KL098234 - Westheimer"
                }, {
                    "imgCheckBox": "checkboxnormal.png",
                    "lblDescription": "KL098234 - Westheimer"
                }],
                "groupCells": false,
                "id": "segDropDown",
                "isVisible": true,
                "left": "0dp",
                "needPageIndicator": true,
                "pageOffDotImage": "pageoffdot.png",
                "pageOnDotImage": "pageondot.png",
                "retainSelection": false,
                "right": "0px",
                "rowFocusSkin": "seg2Focus",
                "rowTemplate": "flxSearchDropDown",
                "sectionHeaderSkin": "sliPhoneSegmentHeader",
                "selectionBehavior": constants.SEGUI_MULTI_SELECT_BEHAVIOR,
                "selectionBehaviorConfig": {
                    "imageIdentifier": "imgCheckBox",
                    "selectedStateImage": "checkboxselected.png",
                    "unselectedStateImage": "checkbox.png"
                },
                "separatorRequired": false,
                "showScrollbars": false,
                "top": "10dp",
                "viewType": constants.SEGUI_VIEW_TYPE_TABLEVIEW,
                "widgetDataMap": {
                    "flxCheckBox": "flxCheckBox",
                    "flxSearchDropDown": "flxSearchDropDown",
                    "imgCheckBox": "imgCheckBox",
                    "lblDescription": "lblDescription"
                },
                "zIndex": 1
            }, {
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxFilterDropdown.add(segDropDown);
            var flxStatusFilterDropdown = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                "clipBounds": true,
                "id": "flxStatusFilterDropdown",
                "isVisible": false,
                "layoutType": kony.flex.FREE_FORM,
                "left": "215dp",
                "isModalContainer": false,
                "skin": "sknCursorDropDown",
                "top": "78dp",
                "width": "134px",
                "zIndex": 10
            }, {}, {});
            flxStatusFilterDropdown.setDefaultUnit(kony.flex.DP);
            var segStatusDropDown = new kony.ui.SegmentedUI2({
                "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                "data": [{
                    "imgCheckBox": "checkboxnormal.png",
                    "lblDescription": "KL098234 - Westheimer"
                }, {
                    "imgCheckBox": "checkboxnormal.png",
                    "lblDescription": "KL098234 - Westheimer"
                }, {
                    "imgCheckBox": "checkboxnormal.png",
                    "lblDescription": "KL098234 - Westheimer"
                }, {
                    "imgCheckBox": "checkboxnormal.png",
                    "lblDescription": "KL098234 - Westheimer"
                }, {
                    "imgCheckBox": "checkboxnormal.png",
                    "lblDescription": "KL098234 - Westheimer"
                }, {
                    "imgCheckBox": "checkboxnormal.png",
                    "lblDescription": "KL098234 - Westheimer"
                }],
                "groupCells": false,
                "id": "segStatusDropDown",
                "isVisible": true,
                "left": "0dp",
                "needPageIndicator": true,
                "pageOffDotImage": "pageoffdot.png",
                "pageOnDotImage": "pageondot.png",
                "retainSelection": false,
                "right": "0px",
                "rowFocusSkin": "seg2Focus",
                "rowTemplate": "flxSearchDropDown",
                "sectionHeaderSkin": "sliPhoneSegmentHeader",
                "selectionBehavior": constants.SEGUI_MULTI_SELECT_BEHAVIOR,
                "selectionBehaviorConfig": {
                    "imageIdentifier": "imgCheckBox",
                    "selectedStateImage": "checkboxselected.png",
                    "unselectedStateImage": "checkbox.png"
                },
                "separatorRequired": false,
                "showScrollbars": false,
                "top": "10dp",
                "viewType": constants.SEGUI_VIEW_TYPE_TABLEVIEW,
                "widgetDataMap": {
                    "flxCheckBox": "flxCheckBox",
                    "flxSearchDropDown": "flxSearchDropDown",
                    "imgCheckBox": "imgCheckBox",
                    "lblDescription": "lblDescription"
                },
                "zIndex": 1
            }, {
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxStatusFilterDropdown.add(segStatusDropDown);
            var flxSelectOptions = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                "clipBounds": true,
                "id": "flxSelectOptions",
                "isVisible": false,
                "layoutType": kony.flex.FLOW_VERTICAL,
                "left": "810dp",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "100dp",
                "width": "200dp"
            }, {}, {});
            flxSelectOptions.setDefaultUnit(kony.flex.DP);
            var flxTopArrowImage = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "12dp",
                "id": "flxTopArrowImage",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0dp",
                "width": "100%",
                "zIndex": 101
            }, {}, {});
            flxTopArrowImage.setDefaultUnit(kony.flex.DP);
            var imgUpArrow = new kony.ui.Image2({
                "height": "12dp",
                "id": "imgUpArrow",
                "isVisible": true,
                "right": "15dp",
                "skin": "slImage",
                "src": "uparrow_2x.png",
                "top": "0dp",
                "width": "15dp",
                "zIndex": 1
            }, {
                "imageScaleMode": constants.IMAGE_SCALE_MODE_FIT_TO_DIMENSIONS,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxTopArrowImage.add(imgUpArrow);
            var flxMenuOptions = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                "clipBounds": false,
                "id": "flxMenuOptions",
                "isVisible": true,
                "layoutType": kony.flex.FLOW_VERTICAL,
                "left": "0px",
                "isModalContainer": false,
                "skin": "sknflxffffffop100dbdbe6Radius3px",
                "top": "-1px",
                "width": "200px",
                "zIndex": 100
            }, {}, {});
            flxMenuOptions.setDefaultUnit(kony.flex.DP);
            var lblDescription = new kony.ui.Label({
                "id": "lblDescription",
                "isVisible": true,
                "left": "15px",
                "skin": "sknlblLatoBold9ca9ba14px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.roles.ASSIGN\")",
                "top": "20dp",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var btnRoles = new kony.ui.Button({
                "id": "btnRoles",
                "isVisible": true,
                "left": "16dp",
                "skin": "contextuallinks",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmPermissions.btnRoles\")",
                "top": "20dp",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_CENTER,
                "displayText": true,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var btnPermissions = new kony.ui.Button({
                "bottom": "15dp",
                "id": "btnPermissions",
                "isVisible": true,
                "left": "16dp",
                "skin": "contextuallinks",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.users.Permissions\")",
                "top": "15dp",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_CENTER,
                "displayText": true,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var flxSeperator = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "centerX": "50%",
                "clipBounds": true,
                "height": "1px",
                "id": "flxSeperator",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "isModalContainer": false,
                "skin": "sknflxd6dbe7",
                "top": "0px",
                "width": "100%",
                "zIndex": 1
            }, {}, {});
            flxSeperator.setDefaultUnit(kony.flex.DP);
            flxSeperator.add();
            var flxOption1 = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "35dp",
                "id": "flxOption1",
                "isVisible": false,
                "layoutType": kony.flex.FLOW_HORIZONTAL,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "CopyslFbox0aa28ddf20b1140",
                "top": "0dp",
                "width": "100%",
                "zIndex": 1
            }, {}, {
                "hoverSkin": "sknContextualMenuEntryHover"
            });
            flxOption1.setDefaultUnit(kony.flex.DP);
            var imgOption1 = new kony.ui.Image2({
                "centerY": "50%",
                "height": "15dp",
                "id": "imgOption1",
                "isVisible": true,
                "left": "15dp",
                "skin": "slImage",
                "src": "imagedrag.png",
                "width": "15dp",
                "zIndex": 1
            }, {
                "imageScaleMode": constants.IMAGE_SCALE_MODE_MAINTAIN_ASPECT_RATIO,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var lblOption1 = new kony.ui.Label({
                "centerY": "50%",
                "id": "lblOption1",
                "isVisible": true,
                "left": "15dp",
                "skin": "sknlblLatoBold35475f14px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.permission.View\")",
                "top": "4dp",
                "width": "40px",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxOption1.add(imgOption1, lblOption1);
            var flxOption2 = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "35dp",
                "id": "flxOption2",
                "isVisible": true,
                "layoutType": kony.flex.FLOW_HORIZONTAL,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "CopyslFbox0f0ce435306514e",
                "top": "0dp",
                "width": "100%",
                "zIndex": 1
            }, {}, {
                "hoverSkin": "sknContextualMenuEntryHover"
            });
            flxOption2.setDefaultUnit(kony.flex.DP);
            var fontIconOptionEdit = new kony.ui.Label({
                "centerY": "50%",
                "id": "fontIconOptionEdit",
                "isVisible": true,
                "left": "15dp",
                "skin": "sknFontIconOptionMenuRow",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmFAQ.fonticonEdit\")",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var lblOption2 = new kony.ui.Label({
                "centerY": "50%",
                "id": "lblOption2",
                "isVisible": true,
                "left": "15dp",
                "skin": "sknlblLatoRegular484b5213px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.permission.Edit\")",
                "top": "4dp",
                "width": "40px",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxOption2.add(fontIconOptionEdit, lblOption2);
            var flxOption3 = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "35dp",
                "id": "flxOption3",
                "isVisible": true,
                "layoutType": kony.flex.FLOW_HORIZONTAL,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "CopyslFbox0g7a6974b2e1c4b",
                "top": "0dp",
                "width": "100%",
                "zIndex": 1
            }, {}, {
                "hoverSkin": "sknContextualMenuEntryHover"
            });
            flxOption3.setDefaultUnit(kony.flex.DP);
            var fontIconSuspend = new kony.ui.Label({
                "centerY": "50%",
                "id": "fontIconSuspend",
                "isVisible": true,
                "left": "15dp",
                "skin": "sknFontIconOptionMenuRow",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmPermissions.fontIconOption3\")",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var lblOption3 = new kony.ui.Label({
                "centerY": "50%",
                "id": "lblOption3",
                "isVisible": true,
                "left": "15dp",
                "skin": "sknlblLatoRegular484b5213px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.permission.Suspend\")",
                "top": "4dp",
                "width": "60px",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxOption3.add(fontIconSuspend, lblOption3);
            var flxOption4 = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "bottom": "10dp",
                "clipBounds": true,
                "height": "35dp",
                "id": "flxOption4",
                "isVisible": true,
                "layoutType": kony.flex.FLOW_HORIZONTAL,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "CopyslFbox0jdb97e2f150046",
                "top": "0dp",
                "width": "100%",
                "zIndex": 1
            }, {}, {
                "hoverSkin": "sknContextualMenuEntryHover"
            });
            flxOption4.setDefaultUnit(kony.flex.DP);
            var fontIconDeactivate = new kony.ui.Label({
                "centerY": "50%",
                "id": "fontIconDeactivate",
                "isVisible": true,
                "left": "15dp",
                "skin": "sknFontIconOptionMenuRow",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmFAQ.fonticonDeactive\")",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var lblOption4 = new kony.ui.Label({
                "centerY": "50%",
                "id": "lblOption4",
                "isVisible": true,
                "left": "15dp",
                "skin": "sknlblLatoRegular484b5213px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.permission.Deactivate\")",
                "top": "4dp",
                "width": "70px",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxOption4.add(fontIconDeactivate, lblOption4);
            flxMenuOptions.add(lblDescription, btnRoles, btnPermissions, flxSeperator, flxOption1, flxOption2, flxOption3, flxOption4);
            var flxDownArrowImage = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "12dp",
                "id": "flxDownArrowImage",
                "isVisible": false,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "-1dp",
                "width": "100%",
                "zIndex": 101
            }, {}, {});
            flxDownArrowImage.setDefaultUnit(kony.flex.DP);
            var imgDownArrow = new kony.ui.Image2({
                "height": "12dp",
                "id": "imgDownArrow",
                "isVisible": true,
                "right": "15dp",
                "skin": "slImage",
                "src": "downarrow_2x.png",
                "top": "0dp",
                "width": "15dp",
                "zIndex": 1
            }, {
                "imageScaleMode": constants.IMAGE_SCALE_MODE_FIT_TO_DIMENSIONS,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxDownArrowImage.add(imgDownArrow);
            flxSelectOptions.add(flxTopArrowImage, flxMenuOptions, flxDownArrowImage);
            flxPermissions.add(flxUsersContainer, flxFilterDropdown, flxStatusFilterDropdown, flxSelectOptions);
            var flxViews = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                "clipBounds": true,
                "id": "flxViews",
                "isVisible": false,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "masterType": constants.MASTER_TYPE_USERWIDGET,
                "isModalContainer": false,
                "skin": "sknflxf5f6f8Op100",
                "top": "0dp",
                "width": "100%",
                "zIndex": 1
            }, {}, {});
            flxViews.setDefaultUnit(kony.flex.DP);
            var flxAddMainContainer = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "bottom": 35,
                "clipBounds": true,
                "height": "480px",
                "id": "flxAddMainContainer",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "35dp",
                "masterType": constants.MASTER_TYPE_USERWIDGET,
                "isModalContainer": false,
                "right": 35,
                "skin": "sknflxf5f6f8Op100BorderE1E5Ed",
                "top": "0px",
                "zIndex": 1
            }, {}, {});
            flxAddMainContainer.setDefaultUnit(kony.flex.DP);
            var flxOptions = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "bottom": "0dp",
                "clipBounds": true,
                "id": "flxOptions",
                "isVisible": true,
                "layoutType": kony.flex.FLOW_VERTICAL,
                "left": "0px",
                "isModalContainer": false,
                "skin": "sknflxf9f9f9NoBorder",
                "top": "0%",
                "width": "230px",
                "zIndex": 1
            }, {}, {});
            flxOptions.setDefaultUnit(kony.flex.DP);
            var flxOptionDetails = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                "clipBounds": true,
                "id": "flxOptionDetails",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0dp",
                "width": "100%",
                "zIndex": 1
            }, {}, {});
            flxOptionDetails.setDefaultUnit(kony.flex.DP);
            var btnOptionDetails = new kony.ui.Button({
                "bottom": "15px",
                "id": "btnOptionDetails",
                "isVisible": true,
                "left": "20px",
                "skin": "Btn000000font14px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.users.USERDETAILS\")",
                "top": "20px",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_CENTER,
                "displayText": true,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var flxoptionSeperator1 = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "bottom": 0,
                "centerX": "50%",
                "clipBounds": true,
                "height": "1px",
                "id": "flxoptionSeperator1",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "20px",
                "isModalContainer": false,
                "right": "20px",
                "skin": "sknflxe1e5edop100",
                "zIndex": 1
            }, {}, {});
            flxoptionSeperator1.setDefaultUnit(kony.flex.DP);
            flxoptionSeperator1.add();
            var fontIconRightArrow2 = new kony.ui.Label({
                "bottom": "15px",
                "id": "fontIconRightArrow2",
                "isVisible": true,
                "right": "20dp",
                "skin": "sknFontIconRightArrow16px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmPermissions.fontIconRightArrow1\")",
                "top": "20px",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxOptionDetails.add(btnOptionDetails, flxoptionSeperator1, fontIconRightArrow2);
            var flxAddRoles = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                "clipBounds": true,
                "id": "flxAddRoles",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0dp",
                "width": "100%",
                "zIndex": 1
            }, {}, {});
            flxAddRoles.setDefaultUnit(kony.flex.DP);
            var btnAddRoles = new kony.ui.Button({
                "bottom": "37px",
                "id": "btnAddRoles",
                "isVisible": true,
                "left": "20px",
                "skin": "Btna1acbafont12pxKA",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmUsers.btnAddRoles\")",
                "top": "15dp",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_CENTER,
                "displayText": true,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var lblAddOptional3 = new kony.ui.Label({
                "bottom": "15px",
                "id": "lblAddOptional3",
                "isVisible": true,
                "left": "20px",
                "skin": "slLabel0d4f692dab05249",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.permission.Optional\")",
                "top": "38px",
                "width": "93px",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var flxAddSeperator4 = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "bottom": 0,
                "centerX": "50%",
                "clipBounds": true,
                "height": "1px",
                "id": "flxAddSeperator4",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "20px",
                "isModalContainer": false,
                "right": "20px",
                "skin": "sknflxe1e5edop100",
                "zIndex": 1
            }, {}, {});
            flxAddSeperator4.setDefaultUnit(kony.flex.DP);
            flxAddSeperator4.add();
            var fontIconRightArrow3 = new kony.ui.Label({
                "id": "fontIconRightArrow3",
                "isVisible": true,
                "right": "20dp",
                "skin": "sknFontIconRightArrow16px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmPermissions.fontIconRightArrow1\")",
                "top": "15dp",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxAddRoles.add(btnAddRoles, lblAddOptional3, flxAddSeperator4, fontIconRightArrow3);
            var flxAddPermissions = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                "clipBounds": true,
                "id": "flxAddPermissions",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0dp",
                "width": "100%",
                "zIndex": 1
            }, {}, {});
            flxAddPermissions.setDefaultUnit(kony.flex.DP);
            var btnAddPermissions = new kony.ui.Button({
                "bottom": "37px",
                "id": "btnAddPermissions",
                "isVisible": true,
                "left": "20px",
                "skin": "Btna1acbafont12pxKA",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.roles.ASSIGNPERMISSIONS\")",
                "top": "15dp",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_CENTER,
                "displayText": true,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var lblAddOptional1 = new kony.ui.Label({
                "bottom": "15px",
                "id": "lblAddOptional1",
                "isVisible": true,
                "left": "20px",
                "skin": "slLabel0d4f692dab05249",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.permission.Optional\")",
                "top": "38px",
                "width": "93px",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var flxAddSeperator2 = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "bottom": 0,
                "centerX": "50%",
                "clipBounds": true,
                "height": "1px",
                "id": "flxAddSeperator2",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "20px",
                "isModalContainer": false,
                "right": "20px",
                "skin": "sknflxe1e5edop100",
                "zIndex": 1
            }, {}, {});
            flxAddSeperator2.setDefaultUnit(kony.flex.DP);
            flxAddSeperator2.add();
            var fontIconRightArrow4 = new kony.ui.Label({
                "id": "fontIconRightArrow4",
                "isVisible": true,
                "right": "20dp",
                "skin": "sknFontIconRightArrow16px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmPermissions.fontIconRightArrow1\")",
                "top": "15dp",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxAddPermissions.add(btnAddPermissions, lblAddOptional1, flxAddSeperator2, fontIconRightArrow4);
            flxOptions.add(flxOptionDetails, flxAddRoles, flxAddPermissions);
            var flxAddOptionsSection = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "bottom": "0dp",
                "clipBounds": true,
                "id": "flxAddOptionsSection",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "230px",
                "isModalContainer": false,
                "right": "0px",
                "skin": "sknflxffffffop100",
                "top": "0px",
                "zIndex": 1
            }, {}, {});
            flxAddOptionsSection.setDefaultUnit(kony.flex.DP);
            var lblAddOptionsHeading = new kony.ui.Label({
                "id": "lblAddOptionsHeading",
                "isVisible": false,
                "left": "30px",
                "skin": "sknlblLato35475f14px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmPermissionsController.ADD_USERS\")",
                "top": 30,
                "width": "70%",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var flxAddOptionsContainer = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "bottom": "0px",
                "clipBounds": true,
                "id": "flxAddOptionsContainer",
                "isVisible": false,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "isModalContainer": false,
                "right": "0px",
                "skin": "slFbox",
                "top": "20dp",
                "zIndex": 1
            }, {}, {});
            flxAddOptionsContainer.setDefaultUnit(kony.flex.DP);
            var flxAvailableOptionsSection = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "bottom": "100dp",
                "clipBounds": true,
                "id": "flxAvailableOptionsSection",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "20px",
                "isModalContainer": false,
                "skin": "slFbox0c11dce22bfe447",
                "top": "0px",
                "width": "53%",
                "zIndex": 1
            }, {}, {});
            flxAvailableOptionsSection.setDefaultUnit(kony.flex.DP);
            var lblAvailableOptionsHeading = new kony.ui.Label({
                "id": "lblAvailableOptionsHeading",
                "isVisible": true,
                "left": "0px",
                "skin": "sknlblLatoRegular485c7512px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.permission.AvailableUsers\")",
                "top": 0,
                "width": "60%",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var btnAddAll = new kony.ui.Button({
                "id": "btnAddAll",
                "isVisible": true,
                "right": 0,
                "skin": "sknBtn11abeb12px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmGroupsController.Add_All\")",
                "top": 0,
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "displayText": true,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var flxAvailableOptSearchSeg = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "bottom": "0dp",
                "clipBounds": true,
                "id": "flxAvailableOptSearchSeg",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "sknflxffffffBordere1e5ed4Px",
                "top": "30dp",
                "width": "100%"
            }, {}, {});
            flxAvailableOptSearchSeg.setDefaultUnit(kony.flex.DP);
            var flxSearch = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "55px",
                "id": "flxSearch",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": 0,
                "isModalContainer": false,
                "right": 0,
                "skin": "sknflxf5f6f8Op100",
                "top": "0px",
                "zIndex": 1
            }, {}, {});
            flxSearch.setDefaultUnit(kony.flex.DP);
            var flxSearchContainer = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "centerY": "50%",
                "clipBounds": true,
                "focusSkin": "slFbox0ebc847fa67a243Search",
                "height": "35px",
                "id": "flxSearchContainer",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "10dp",
                "isModalContainer": false,
                "right": 10,
                "skin": "sknflxd5d9ddop100",
                "zIndex": 1
            }, {}, {});
            flxSearchContainer.setDefaultUnit(kony.flex.DP);
            var tbxSearchBox = new kony.ui.TextBox2({
                "autoCapitalize": constants.TEXTBOX_AUTO_CAPITALIZE_NONE,
                "centerY": "50%",
                "height": "100%",
                "id": "tbxSearchBox",
                "isVisible": true,
                "keyBoardStyle": constants.TEXTBOX_KEY_BOARD_STYLE_DEFAULT,
                "left": "33dp",
                "onTouchStart": controller.AS_TextField_f138c9e0665245388949c737ff9374da,
                "i18n_placeholder": "kony.i18n.getLocalizedString(\"i18n.permission.search\")",
                "secureTextEntry": false,
                "skin": "skntbxffffffNoBorderlato35475f14px",
                "textInputMode": constants.TEXTBOX_INPUT_MODE_ANY,
                "width": "88%",
                "zIndex": 1
            }, {
                "containerHeightMode": constants.TEXTBOX_FONT_METRICS_DRIVEN_HEIGHT,
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [3, 0, 0, 0],
                "paddingInPixel": false
            }, {
                "autoCorrect": false
            });
            var fontIconSearch = new kony.ui.Label({
                "centerY": "50%",
                "id": "fontIconSearch",
                "isVisible": true,
                "left": "15dp",
                "skin": "sknFontIconSearchImg20px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmGroups.fontIconSearchimg\")",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var flxSearchCrossImg = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "bottom": "0dp",
                "clipBounds": true,
                "height": "35px",
                "id": "flxSearchCrossImg",
                "isVisible": false,
                "layoutType": kony.flex.FREE_FORM,
                "isModalContainer": false,
                "right": "0px",
                "skin": "slFbox",
                "top": "0dp",
                "width": "35px",
                "zIndex": 2
            }, {}, {
                "hoverSkin": "sknCursor"
            });
            flxSearchCrossImg.setDefaultUnit(kony.flex.DP);
            var fontIconCross = new kony.ui.Label({
                "centerY": "49%",
                "id": "fontIconCross",
                "isVisible": true,
                "left": "5px",
                "skin": "sknFontIconSearchCross16px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmAlertsManagement.lblAlertPreviewClose\")",
                "top": "10px",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxSearchCrossImg.add(fontIconCross);
            flxSearchContainer.add(tbxSearchBox, fontIconSearch, flxSearchCrossImg);
            flxSearch.add(flxSearchContainer);
            var flxAddOptionsSegment = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "bottom": 0,
                "clipBounds": true,
                "id": "flxAddOptionsSegment",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "sknFlxFFFFFF100O",
                "top": "55dp",
                "width": "100%",
                "zIndex": 1
            }, {}, {});
            flxAddOptionsSegment.setDefaultUnit(kony.flex.DP);
            var segAddOptions = new kony.ui.SegmentedUI2({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "bottom": 0,
                "data": [{
                    "btnAdd": "ADD",
                    "lblFullName": "John Doe",
                    "lblUserIdValue": "TDU34256987",
                    "lblUsername": "USERNAME"
                }, {
                    "btnAdd": "ADD",
                    "lblFullName": "John Doe",
                    "lblUserIdValue": "TDU34256987",
                    "lblUsername": "USERNAME"
                }, {
                    "btnAdd": "ADD",
                    "lblFullName": "John Doe",
                    "lblUserIdValue": "TDU34256987",
                    "lblUsername": "USERNAME"
                }, {
                    "btnAdd": "ADD",
                    "lblFullName": "John Doe",
                    "lblUserIdValue": "TDU34256987",
                    "lblUsername": "USERNAME"
                }, {
                    "btnAdd": "ADD",
                    "lblFullName": "John Doe",
                    "lblUserIdValue": "TDU34256987",
                    "lblUsername": "USERNAME"
                }, {
                    "btnAdd": "ADD",
                    "lblFullName": "John Doe",
                    "lblUserIdValue": "TDU34256987",
                    "lblUsername": "USERNAME"
                }, {
                    "btnAdd": "ADD",
                    "lblFullName": "John Doe",
                    "lblUserIdValue": "TDU34256987",
                    "lblUsername": "USERNAME"
                }, {
                    "btnAdd": "ADD",
                    "lblFullName": "John Doe",
                    "lblUserIdValue": "TDU34256987",
                    "lblUsername": "USERNAME"
                }, {
                    "btnAdd": "ADD",
                    "lblFullName": "John Doe",
                    "lblUserIdValue": "TDU34256987",
                    "lblUsername": "USERNAME"
                }],
                "groupCells": false,
                "height": "100%",
                "id": "segAddOptions",
                "isVisible": true,
                "left": "0dp",
                "needPageIndicator": true,
                "pageOffDotImage": "pageoffdot.png",
                "pageOnDotImage": "pageondot.png",
                "retainSelection": false,
                "rowFocusSkin": "seg2Focus",
                "rowSkin": "sknsegffffffOp100Bordere1e5ed",
                "rowTemplate": "flxAddUsers",
                "sectionHeaderSkin": "sliPhoneSegmentHeader",
                "selectionBehavior": constants.SEGUI_DEFAULT_BEHAVIOR,
                "separatorRequired": false,
                "showScrollbars": true,
                "top": "0dp",
                "viewType": constants.SEGUI_VIEW_TYPE_TABLEVIEW,
                "widgetDataMap": {
                    "btnAdd": "btnAdd",
                    "flxAddUsers": "flxAddUsers",
                    "flxAddUsersWrapper": "flxAddUsersWrapper",
                    "flxxUsernameWrapper": "flxxUsernameWrapper",
                    "lblFullName": "lblFullName",
                    "lblUserIdValue": "lblUserIdValue",
                    "lblUsername": "lblUsername"
                },
                "width": "100%",
                "zIndex": 1
            }, {
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var rtxAvailableOptionsMessage1 = new kony.ui.RichText({
                "centerX": "50%",
                "centerY": "50%",
                "id": "rtxAvailableOptionsMessage1",
                "isVisible": false,
                "skin": "sknRtxLatoReg84939e13px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmCSR.richtextNoResult\")",
                "width": "80%",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_CENTER,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxAddOptionsSegment.add(segAddOptions, rtxAvailableOptionsMessage1);
            flxAvailableOptSearchSeg.add(flxSearch, flxAddOptionsSegment);
            flxAvailableOptionsSection.add(lblAvailableOptionsHeading, btnAddAll, flxAvailableOptSearchSeg);
            var flxSelectedOptions = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "bottom": "100dp",
                "clipBounds": true,
                "id": "flxSelectedOptions",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "isModalContainer": false,
                "right": "20dp",
                "skin": "slFbox0c11dce22bfe447",
                "top": "0px",
                "width": "39%",
                "zIndex": 1
            }, {}, {});
            flxSelectedOptions.setDefaultUnit(kony.flex.DP);
            var lblSelectedOption = new kony.ui.Label({
                "id": "lblSelectedOption",
                "isVisible": true,
                "left": "0px",
                "skin": "sknlblLatoRegular485c7512px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.permission.SelectedUsers\")",
                "top": 0,
                "width": "60%",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var flxSegSelectedOptions = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "bottom": 0,
                "clipBounds": true,
                "id": "flxSegSelectedOptions",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "sknflxf9f9f9Border1pxRad4px",
                "top": "30dp",
                "width": "100%",
                "zIndex": 1
            }, {}, {});
            flxSegSelectedOptions.setDefaultUnit(kony.flex.DP);
            var segSelectedOptions = new kony.ui.SegmentedUI2({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "data": [{
                    "imgClose": "imagedrag.png",
                    "lblOption": "John Doe"
                }, {
                    "imgClose": "imagedrag.png",
                    "lblOption": "John Doe"
                }],
                "groupCells": false,
                "height": "100%",
                "id": "segSelectedOptions",
                "isVisible": false,
                "left": "0px",
                "needPageIndicator": true,
                "pageOffDotImage": "pageoffdot.png",
                "pageOnDotImage": "pageondot.png",
                "retainSelection": false,
                "rowSkin": "sknsegf9f9f9Op100",
                "rowTemplate": "flxOptionAdded",
                "sectionHeaderSkin": "sliPhoneSegmentHeader0h3448bdd9d8941",
                "selectionBehavior": constants.SEGUI_DEFAULT_BEHAVIOR,
                "separatorColor": "64646400",
                "separatorRequired": true,
                "separatorThickness": 0,
                "showScrollbars": true,
                "top": "0px",
                "viewType": constants.SEGUI_VIEW_TYPE_TABLEVIEW,
                "widgetDataMap": {
                    "flxAddOptionWrapper": "flxAddOptionWrapper",
                    "flxClose": "flxClose",
                    "flxOptionAdded": "flxOptionAdded",
                    "imgClose": "imgClose",
                    "lblOption": "lblOption"
                },
                "width": "100%",
                "zIndex": 1
            }, {
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var rtxSelectedOptionsMessage = new kony.ui.RichText({
                "centerX": "50%",
                "centerY": "50%",
                "id": "rtxSelectedOptionsMessage",
                "isVisible": true,
                "skin": "sknRtxlatoregular84939e14px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmPermissionsController.Click_Add_to_select_a_user\")",
                "width": "80%",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_CENTER,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxSegSelectedOptions.add(segSelectedOptions, rtxSelectedOptionsMessage);
            var btnRemoveAll = new kony.ui.Button({
                "id": "btnRemoveAll",
                "isVisible": true,
                "right": 2,
                "skin": "sknBtn11abeb12px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.Common.button.Reset\")",
                "top": 0,
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "displayText": true,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxSelectedOptions.add(lblSelectedOption, flxSegSelectedOptions, btnRemoveAll);
            var flxButtons = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "bottom": "20dp",
                "clipBounds": true,
                "height": "40px",
                "id": "flxButtons",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "20dp",
                "isModalContainer": false,
                "right": "20dp",
                "skin": "slFbox",
                "zIndex": 1
            }, {}, {});
            flxButtons.setDefaultUnit(kony.flex.DP);
            var btnCancel = new kony.ui.Button({
                "centerY": "50%",
                "focusSkin": "sknBtnLatoRegulara5abc413pxKA",
                "height": "40dp",
                "id": "btnCancel",
                "isVisible": true,
                "left": 0,
                "right": "29.50%",
                "skin": "sknbtnf7f7faLatoRegular4f555dBorder1px8b96a5",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.permission.CANCEL\")",
                "top": "0%",
                "width": "100px",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_CENTER,
                "displayText": true,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {
                "hoverSkin": "sknbtnffffffLatoRegular4f555dBorder1px485c75"
            });
            var flxRightButtons = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "centerY": "50%",
                "clipBounds": true,
                "height": "100%",
                "id": "flxRightButtons",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "isModalContainer": false,
                "right": 0,
                "skin": "slFbox",
                "width": "220px",
                "zIndex": 1
            }, {}, {});
            flxRightButtons.setDefaultUnit(kony.flex.DP);
            var btnNext = new kony.ui.Button({
                "centerY": "50%",
                "focusSkin": "sknBtn4A77A0LatoRegular13pxFFFFFFRad20px",
                "height": "40dp",
                "id": "btnNext",
                "isVisible": true,
                "left": 0,
                "skin": "sknBtn4A77A0LatoRegular13pxFFFFFFRad20px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.permission.NEXT\")",
                "top": "0%",
                "width": "100dp",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_CENTER,
                "displayText": true,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {
                "hoverSkin": "sknBtn2D5982LatoRegular13pxFFFFFFRad20px"
            });
            var btnSave = new kony.ui.Button({
                "centerY": "50%",
                "focusSkin": "sknBtn003E75LatoRegular13pxFFFFFFRad20px",
                "height": "40dp",
                "id": "btnSave",
                "isVisible": true,
                "right": "0px",
                "skin": "sknBtn003E75LatoRegular13pxFFFFFFRad20px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.permission.SAVE\")",
                "top": "0%",
                "width": "100px",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_CENTER,
                "displayText": true,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {
                "hoverSkin": "sknBtn005198LatoRegular13pxFFFFFFRad20px"
            });
            flxRightButtons.add(btnNext, btnSave);
            flxButtons.add(btnCancel, flxRightButtons);
            var flxBtnSeperatorUsers = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "bottom": "80dp",
                "clipBounds": true,
                "height": "1px",
                "id": "flxBtnSeperatorUsers",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "isModalContainer": false,
                "right": "0dp",
                "skin": "sknflxe1e5edop100",
                "zIndex": 5
            }, {}, {});
            flxBtnSeperatorUsers.setDefaultUnit(kony.flex.DP);
            flxBtnSeperatorUsers.add();
            flxAddOptionsContainer.add(flxAvailableOptionsSection, flxSelectedOptions, flxButtons, flxBtnSeperatorUsers);
            var flxAddUsersDetails = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "bottom": "0dp",
                "clipBounds": true,
                "id": "flxAddUsersDetails",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "isModalContainer": false,
                "right": 0,
                "skin": "slFbox",
                "top": "0dp",
                "zIndex": 1
            }, {}, {});
            flxAddUsersDetails.setDefaultUnit(kony.flex.DP);
            var flxUserDetailsData = new kony.ui.FlexScrollContainer({
                "allowHorizontalBounce": false,
                "allowVerticalBounce": true,
                "bounces": true,
                "clipBounds": true,
                "enableScrolling": true,
                "height": "400px",
                "horizontalScrollIndicator": true,
                "id": "flxUserDetailsData",
                "isVisible": true,
                "layoutType": kony.flex.FLOW_VERTICAL,
                "left": "20dp",
                "pagingEnabled": false,
                "right": "0dp",
                "scrollDirection": kony.flex.SCROLL_VERTICAL,
                "top": "0dp",
                "verticalScrollIndicator": true,
                "zIndex": 1
            }, {}, {});
            flxUserDetailsData.setDefaultUnit(kony.flex.DP);
            var flxPersonalInfo = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "240dp",
                "id": "flxPersonalInfo",
                "isVisible": true,
                "layoutType": kony.flex.FLOW_VERTICAL,
                "left": "0dp",
                "masterType": constants.MASTER_TYPE_USERWIDGET,
                "isModalContainer": false,
                "skin": "slFbox0afd6fda7b2514b",
                "top": "22dp",
                "width": "100%",
                "zIndex": 1
            }, {}, {});
            flxPersonalInfo.setDefaultUnit(kony.flex.DP);
            var lblSubTitle = new kony.ui.Label({
                "id": "lblSubTitle",
                "isVisible": true,
                "left": "0dp",
                "skin": "sknlblSubHeader0b73b6af9628b4e",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmCreateNewUser.lblSubTitle\")",
                "top": "0%",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var lblLineUser = new kony.ui.Label({
                "bottom": "19px",
                "height": "1px",
                "id": "lblLineUser",
                "isVisible": true,
                "left": "0dp",
                "skin": "slnLnBackground0e865c9f7aca941",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmAlertsManagement.lblAlertTypeDescriptionCount\")",
                "top": "18px",
                "width": "100%",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var flxFullName = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "85dp",
                "id": "flxFullName",
                "isVisible": true,
                "layoutType": kony.flex.FLOW_HORIZONTAL,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "slFbox0afd6fda7b2514b",
                "top": "0dp",
                "width": "100%",
                "zIndex": 1
            }, {}, {});
            flxFullName.setDefaultUnit(kony.flex.DP);
            var flxFirstNameUser = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "85dp",
                "id": "flxFirstNameUser",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0dp",
                "width": "30%",
                "zIndex": 1
            }, {}, {});
            flxFirstNameUser.setDefaultUnit(kony.flex.DP);
            var txtbxFirstNameUser = new kony.ui.TextBox2({
                "autoCapitalize": constants.TEXTBOX_AUTO_CAPITALIZE_NONE,
                "focusSkin": "skntxtbxDetailsFocus14px",
                "height": "40px",
                "id": "txtbxFirstNameUser",
                "isVisible": true,
                "keyBoardStyle": constants.TEXTBOX_KEY_BOARD_STYLE_DEFAULT,
                "left": "0dp",
                "maxTextLength": 35,
                "placeholder": "First Name",
                "secureTextEntry": false,
                "skin": "skntxtbx484b5214px",
                "textInputMode": constants.TEXTBOX_INPUT_MODE_ANY,
                "top": "25dp",
                "width": "100%",
                "zIndex": 1
            }, {
                "containerHeightMode": constants.TEXTBOX_FONT_METRICS_DRIVEN_HEIGHT,
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [4, 0, 0, 0],
                "paddingInPixel": false
            }, {
                "autoCorrect": false
            });
            var lblFirstName = new kony.ui.Label({
                "id": "lblFirstName",
                "isVisible": true,
                "left": "0dp",
                "skin": "sknlbl485c7514px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmUsers.lblFirstName\")",
                "top": "0dp",
                "width": "150dp",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var lblFirstNameCount = new kony.ui.Label({
                "id": "lblFirstNameCount",
                "isVisible": true,
                "right": "0px",
                "skin": "sknlbl485c7514px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmUsers.lblFirstNameCount\")",
                "top": "0dp",
                "width": "40dp",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_CENTER,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var flxErrorFirstName = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "15dp",
                "id": "flxErrorFirstName",
                "isVisible": false,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "70dp",
                "width": "100%",
                "zIndex": 1
            }, {}, {});
            flxErrorFirstName.setDefaultUnit(kony.flex.DP);
            var lblErrorFirstNameIcon = new kony.ui.Label({
                "height": "15dp",
                "id": "lblErrorFirstNameIcon",
                "isVisible": true,
                "left": "0dp",
                "skin": "sknErrorIcon",
                "text": "",
                "top": "0dp",
                "width": "15dp",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var lblErrorFirstName = new kony.ui.Label({
                "height": "15dp",
                "id": "lblErrorFirstName",
                "isVisible": true,
                "left": "15dp",
                "skin": "sknlblError",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmUsers.lblErrorFirstName\")",
                "top": "0dp",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxErrorFirstName.add(lblErrorFirstNameIcon, lblErrorFirstName);
            flxFirstNameUser.add(txtbxFirstNameUser, lblFirstName, lblFirstNameCount, flxErrorFirstName);
            var flxMiddleNameUser = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "85dp",
                "id": "flxMiddleNameUser",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "10dp",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0dp",
                "width": "30%",
                "zIndex": 1
            }, {}, {});
            flxMiddleNameUser.setDefaultUnit(kony.flex.DP);
            var lblMiddleName = new kony.ui.Label({
                "id": "lblMiddleName",
                "isVisible": true,
                "left": "0dp",
                "skin": "sknlbl485c7514px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmUsers.lblMiddleName\")",
                "top": "0dp",
                "width": "85dp",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var lblMiddleNameCount = new kony.ui.Label({
                "id": "lblMiddleNameCount",
                "isVisible": true,
                "right": "0dp",
                "skin": "sknlbl485c7514px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmUsers.lblFirstNameCount\")",
                "top": "0dp",
                "width": "40dp",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_CENTER,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var txtbxMiddleNameUser = new kony.ui.TextBox2({
                "autoCapitalize": constants.TEXTBOX_AUTO_CAPITALIZE_NONE,
                "focusSkin": "skntxtbxDetailsFocus14px",
                "height": "40px",
                "id": "txtbxMiddleNameUser",
                "isVisible": true,
                "keyBoardStyle": constants.TEXTBOX_KEY_BOARD_STYLE_DEFAULT,
                "left": "0dp",
                "maxTextLength": 35,
                "placeholder": "Middle Name",
                "secureTextEntry": false,
                "skin": "skntxtbx484b5214px",
                "textInputMode": constants.TEXTBOX_INPUT_MODE_ANY,
                "top": "25dp",
                "width": "100%",
                "zIndex": 1
            }, {
                "containerHeightMode": constants.TEXTBOX_FONT_METRICS_DRIVEN_HEIGHT,
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [4, 0, 0, 0],
                "paddingInPixel": false
            }, {
                "autoCorrect": false
            });
            var lblOptional = new kony.ui.Label({
                "id": "lblOptional",
                "isVisible": true,
                "left": "85dp",
                "skin": "slLabel0d4f692dab05249",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmPermissions.lblOptional\")",
                "top": "1dp",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxMiddleNameUser.add(lblMiddleName, lblMiddleNameCount, txtbxMiddleNameUser, lblOptional);
            var flxLastNameUser = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "85dp",
                "id": "flxLastNameUser",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "10dp",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0dp",
                "width": "30%",
                "zIndex": 1
            }, {}, {});
            flxLastNameUser.setDefaultUnit(kony.flex.DP);
            var lblLastName = new kony.ui.Label({
                "id": "lblLastName",
                "isVisible": true,
                "left": "0dp",
                "skin": "sknlbl485c7514px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmUsers.lblLastName\")",
                "top": "0dp",
                "width": "150dp",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var lblLastNameCount = new kony.ui.Label({
                "id": "lblLastNameCount",
                "isVisible": true,
                "right": "0dp",
                "skin": "sknlbl485c7514px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmUsers.lblFirstNameCount\")",
                "top": "0dp",
                "width": "40dp",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_CENTER,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var txtbxLastNameUser = new kony.ui.TextBox2({
                "autoCapitalize": constants.TEXTBOX_AUTO_CAPITALIZE_NONE,
                "focusSkin": "skntxtbxDetailsFocus14px",
                "height": "40px",
                "id": "txtbxLastNameUser",
                "isVisible": true,
                "keyBoardStyle": constants.TEXTBOX_KEY_BOARD_STYLE_DEFAULT,
                "left": "0dp",
                "maxTextLength": 35,
                "placeholder": "Last Name",
                "secureTextEntry": false,
                "skin": "skntxtbx484b5214px",
                "textInputMode": constants.TEXTBOX_INPUT_MODE_ANY,
                "top": "25dp",
                "width": "100%",
                "zIndex": 1
            }, {
                "containerHeightMode": constants.TEXTBOX_FONT_METRICS_DRIVEN_HEIGHT,
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [4, 0, 0, 0],
                "paddingInPixel": false
            }, {
                "autoCorrect": false
            });
            var flxErrorLastName = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "15dp",
                "id": "flxErrorLastName",
                "isVisible": false,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "70dp",
                "width": "100%",
                "zIndex": 1
            }, {}, {});
            flxErrorLastName.setDefaultUnit(kony.flex.DP);
            var lblErrorLastNameIcon = new kony.ui.Label({
                "height": "15dp",
                "id": "lblErrorLastNameIcon",
                "isVisible": true,
                "left": "0dp",
                "skin": "sknErrorIcon",
                "text": "",
                "top": "0dp",
                "width": "15dp",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var lblErrorLastName = new kony.ui.Label({
                "height": "15dp",
                "id": "lblErrorLastName",
                "isVisible": true,
                "left": "15dp",
                "skin": "sknlblError",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmUsers.lblErrorLastName\")",
                "top": "0dp",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxErrorLastName.add(lblErrorLastNameIcon, lblErrorLastName);
            flxLastNameUser.add(lblLastName, lblLastNameCount, txtbxLastNameUser, flxErrorLastName);
            flxFullName.add(flxFirstNameUser, flxMiddleNameUser, flxLastNameUser);
            var flxEmailID = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "85dp",
                "id": "flxEmailID",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "slFbox0afd6fda7b2514b",
                "top": "5dp",
                "width": "30%",
                "zIndex": 1
            }, {}, {});
            flxEmailID.setDefaultUnit(kony.flex.DP);
            var lblEmailIDUser = new kony.ui.Label({
                "id": "lblEmailIDUser",
                "isVisible": true,
                "left": "0dp",
                "skin": "sknlbl485c7514px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmCustomerCareController.Email_ID\")",
                "top": "3dp",
                "width": "100%",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var txtbxEmailIDUser = new kony.ui.TextBox2({
                "autoCapitalize": constants.TEXTBOX_AUTO_CAPITALIZE_NONE,
                "focusSkin": "skntxtbxDetailsFocus14px",
                "height": "40px",
                "id": "txtbxEmailIDUser",
                "isVisible": true,
                "keyBoardStyle": constants.TEXTBOX_KEY_BOARD_STYLE_EMAIL,
                "left": "0dp",
                "maxTextLength": 70,
                "placeholder": "Email ID",
                "secureTextEntry": false,
                "skin": "skntxtbx484b5214px",
                "textInputMode": constants.TEXTBOX_INPUT_MODE_ANY,
                "top": "25dp",
                "width": "100%",
                "zIndex": 1
            }, {
                "containerHeightMode": constants.TEXTBOX_FONT_METRICS_DRIVEN_HEIGHT,
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [4, 0, 0, 0],
                "paddingInPixel": false
            }, {
                "autoCorrect": false
            });
            var lblEmailIdCount = new kony.ui.Label({
                "id": "lblEmailIdCount",
                "isVisible": true,
                "right": "0dp",
                "skin": "sknlbl485c7514px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmUsers.lblEmailIdCount\")",
                "top": "0dp",
                "width": "40dp",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_CENTER,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var flxErrorEmailId = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "15dp",
                "id": "flxErrorEmailId",
                "isVisible": false,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "70dp",
                "width": "100%",
                "zIndex": 1
            }, {}, {});
            flxErrorEmailId.setDefaultUnit(kony.flex.DP);
            var lblErrorEmailIdcon = new kony.ui.Label({
                "height": "15dp",
                "id": "lblErrorEmailIdcon",
                "isVisible": true,
                "left": "0dp",
                "skin": "sknErrorIcon",
                "text": "",
                "top": "0dp",
                "width": "15dp",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var lblErrorEmailId = new kony.ui.Label({
                "height": "15dp",
                "id": "lblErrorEmailId",
                "isVisible": true,
                "left": "15dp",
                "skin": "sknlblError",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmUsers.lblErrorEmailId\")",
                "top": "0dp",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxErrorEmailId.add(lblErrorEmailIdcon, lblErrorEmailId);
            flxEmailID.add(lblEmailIDUser, txtbxEmailIDUser, lblEmailIdCount, flxErrorEmailId);
            flxPersonalInfo.add(lblSubTitle, lblLineUser, flxFullName, flxEmailID);
            var flxLoginCreationUser = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "160dp",
                "id": "flxLoginCreationUser",
                "isVisible": true,
                "layoutType": kony.flex.FLOW_VERTICAL,
                "left": "0dp",
                "masterType": constants.MASTER_TYPE_USERWIDGET,
                "isModalContainer": false,
                "skin": "slFbox0afd6fda7b2514b",
                "top": "8dp",
                "width": "100%",
                "zIndex": 1
            }, {}, {});
            flxLoginCreationUser.setDefaultUnit(kony.flex.DP);
            var lblLoginCreationUser = new kony.ui.Label({
                "id": "lblLoginCreationUser",
                "isVisible": true,
                "left": "0dp",
                "skin": "sknlblSubHeader0b73b6af9628b4e",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmCreateNewUser.lblLoginCreationUser\")",
                "top": "0%",
                "width": "100%",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var lblLineUser2 = new kony.ui.Label({
                "bottom": "15px",
                "height": "1dp",
                "id": "lblLineUser2",
                "isVisible": true,
                "left": "0dp",
                "skin": "slnLnBackground0e865c9f7aca941",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmAlertsManagement.lblAlertTypeDescriptionCount\")",
                "top": "18px",
                "width": "100%",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var flxLoginFields = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "90px",
                "id": "flxLoginFields",
                "isVisible": true,
                "layoutType": kony.flex.FLOW_HORIZONTAL,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "slFbox0afd6fda7b2514b",
                "top": "0dp",
                "width": "100%",
                "zIndex": 1
            }, {}, {});
            flxLoginFields.setDefaultUnit(kony.flex.DP);
            var flxUserNameUser = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "90px",
                "id": "flxUserNameUser",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "slFbox0afd6fda7b2514b",
                "top": "0%",
                "width": "30%",
                "zIndex": 1
            }, {}, {});
            flxUserNameUser.setDefaultUnit(kony.flex.DP);
            var lblUserNameUser = new kony.ui.Label({
                "height": "25dp",
                "id": "lblUserNameUser",
                "isVisible": true,
                "left": "0dp",
                "skin": "sknlbl485c7514px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmGroupsController.Username\")",
                "top": "0dp",
                "width": "100%",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var txtbxUserNameUsr = new kony.ui.TextBox2({
                "autoCapitalize": constants.TEXTBOX_AUTO_CAPITALIZE_NONE,
                "focusSkin": "skntxtbxDetailsFocus14px",
                "height": "40px",
                "id": "txtbxUserNameUsr",
                "isVisible": false,
                "keyBoardStyle": constants.TEXTBOX_KEY_BOARD_STYLE_DEFAULT,
                "left": "10dp",
                "maxTextLength": 35,
                "placeholder": "Username",
                "secureTextEntry": false,
                "skin": "skntxtbx484b5214px",
                "textInputMode": constants.TEXTBOX_INPUT_MODE_ANY,
                "top": "35dp",
                "width": "200dp",
                "zIndex": 1
            }, {
                "containerHeightMode": constants.TEXTBOX_FONT_METRICS_DRIVEN_HEIGHT,
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [4, 0, 0, 0],
                "paddingInPixel": false
            }, {
                "autoCorrect": false
            });
            var txtbxUserNameUser = new kony.ui.TextBox2({
                "autoCapitalize": constants.TEXTBOX_AUTO_CAPITALIZE_NONE,
                "focusSkin": "skntxtbxDetailsFocus14px",
                "height": "40px",
                "id": "txtbxUserNameUser",
                "isVisible": true,
                "keyBoardStyle": constants.TEXTBOX_KEY_BOARD_STYLE_DEFAULT,
                "left": "0dp",
                "maxTextLength": 35,
                "placeholder": "Username",
                "secureTextEntry": false,
                "skin": "skntxtbx484b5214px",
                "textInputMode": constants.TEXTBOX_INPUT_MODE_NUMERIC,
                "top": "25dp",
                "width": "100%",
                "zIndex": 1
            }, {
                "containerHeightMode": constants.TEXTBOX_FONT_METRICS_DRIVEN_HEIGHT,
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [4, 0, 0, 0],
                "paddingInPixel": false
            }, {
                "autoCorrect": false
            });
            var flxDisableTbxUserName = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "40dp",
                "id": "flxDisableTbxUserName",
                "isVisible": false,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "sknTbxDisabledf3f3f3",
                "top": "25dp",
                "width": "100%",
                "zIndex": 1
            }, {}, {
                "hoverSkin": "sknCursorDisabled"
            });
            flxDisableTbxUserName.setDefaultUnit(kony.flex.DP);
            flxDisableTbxUserName.add();
            var lblUserNameCount = new kony.ui.Label({
                "id": "lblUserNameCount",
                "isVisible": true,
                "right": "0dp",
                "skin": "sknlbl485c7514px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmUsers.lblFirstNameCount\")",
                "top": "3dp",
                "width": "40dp",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_CENTER,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var flxUserNameNA = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "bottom": "2px",
                "clipBounds": true,
                "height": "15dp",
                "id": "flxUserNameNA",
                "isVisible": false,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "70dp",
                "width": "100%",
                "zIndex": 1
            }, {}, {});
            flxUserNameNA.setDefaultUnit(kony.flex.DP);
            var lblUserNameNAIcon = new kony.ui.Label({
                "height": "15dp",
                "id": "lblUserNameNAIcon",
                "isVisible": true,
                "left": "0dp",
                "skin": "sknErrorIcon",
                "text": "",
                "top": "0dp",
                "width": "15dp",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var lblUserNameNA = new kony.ui.Label({
                "height": "15dp",
                "id": "lblUserNameNA",
                "isVisible": true,
                "left": "15dp",
                "skin": "sknlblError",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmUsers.CopylblUserNameNA0ded79a49f56f43\")",
                "top": "0dp",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxUserNameNA.add(lblUserNameNAIcon, lblUserNameNA);
            var flxUserNameAvailable = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "bottom": "2px",
                "clipBounds": true,
                "height": "15dp",
                "id": "flxUserNameAvailable",
                "isVisible": false,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "70dp",
                "width": "100%",
                "zIndex": 1
            }, {}, {});
            flxUserNameAvailable.setDefaultUnit(kony.flex.DP);
            var lblUserNameAvailable = new kony.ui.Label({
                "height": "15dp",
                "id": "lblUserNameAvailable",
                "isVisible": true,
                "left": "0dp",
                "skin": "sknlblError",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmUsers.CopylblUserNameNA0ded79a49f56f43\")",
                "top": "0dp",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxUserNameAvailable.add(lblUserNameAvailable);
            flxUserNameUser.add(lblUserNameUser, txtbxUserNameUsr, txtbxUserNameUser, flxDisableTbxUserName, lblUserNameCount, flxUserNameNA, flxUserNameAvailable);
            var flxPasswordUser = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "80dp",
                "id": "flxPasswordUser",
                "isVisible": false,
                "layoutType": kony.flex.FREE_FORM,
                "left": "10dp",
                "isModalContainer": false,
                "skin": "slFbox0afd6fda7b2514b",
                "top": "2%",
                "width": "30%",
                "zIndex": 1
            }, {}, {});
            flxPasswordUser.setDefaultUnit(kony.flex.DP);
            var lblPasswordUser = new kony.ui.Label({
                "id": "lblPasswordUser",
                "isVisible": true,
                "left": "0dp",
                "skin": "sknlbl485c7514px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmCreateNewUser.lblPasswordUser\")",
                "top": "3dp",
                "width": "100%",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var txtbxPasswordUser = new kony.ui.TextBox2({
                "autoCapitalize": constants.TEXTBOX_AUTO_CAPITALIZE_NONE,
                "focusSkin": "skntxtbxDetailsFocus14px",
                "height": "40px",
                "id": "txtbxPasswordUser",
                "isVisible": true,
                "keyBoardStyle": constants.TEXTBOX_KEY_BOARD_STYLE_DEFAULT,
                "left": "0dp",
                "placeholder": "Password",
                "secureTextEntry": true,
                "skin": "skntxtbx484b5214px",
                "textInputMode": constants.TEXTBOX_INPUT_MODE_ANY,
                "top": "25dp",
                "width": "100%",
                "zIndex": 1
            }, {
                "containerHeightMode": constants.TEXTBOX_FONT_METRICS_DRIVEN_HEIGHT,
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [4, 0, 0, 0],
                "paddingInPixel": false
            }, {
                "autoCorrect": false
            });
            var imgHelp = new kony.ui.Image2({
                "height": "22dp",
                "id": "imgHelp",
                "isVisible": false,
                "left": "7%",
                "skin": "slImage0b846bb54ca704f",
                "src": "imagedrag.png",
                "top": "0dp",
                "width": "26dp",
                "zIndex": 1
            }, {
                "imageScaleMode": constants.IMAGE_SCALE_MODE_MAINTAIN_ASPECT_RATIO,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var lblCheckAvailabilityPwd = new kony.ui.Label({
                "bottom": "1%",
                "id": "lblCheckAvailabilityPwd",
                "isVisible": true,
                "left": 0,
                "skin": "lblCheckAvailSknNormal0h0ae95eb2f5a41",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmAlertsManagement.lblAlertTypeDescriptionCount\")",
                "width": "89%",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxPasswordUser.add(lblPasswordUser, txtbxPasswordUser, imgHelp, lblCheckAvailabilityPwd);
            var flxReenterPasswordUser = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "85px",
                "id": "flxReenterPasswordUser",
                "isVisible": false,
                "layoutType": kony.flex.FREE_FORM,
                "left": "10dp",
                "isModalContainer": false,
                "skin": "slFbox0afd6fda7b2514b",
                "top": "2%",
                "width": "30%",
                "zIndex": 1
            }, {}, {});
            flxReenterPasswordUser.setDefaultUnit(kony.flex.DP);
            var lblReenterPasswordUser = new kony.ui.Label({
                "id": "lblReenterPasswordUser",
                "isVisible": true,
                "left": "0dp",
                "skin": "sknlbl485c7514px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmCreateNewUser.lblReenterPasswordUser\")",
                "top": "3dp",
                "width": "100%",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var txtbxReenterPasswordUser = new kony.ui.TextBox2({
                "autoCapitalize": constants.TEXTBOX_AUTO_CAPITALIZE_NONE,
                "focusSkin": "skntxtbxDetailsFocus14px",
                "height": "40px",
                "id": "txtbxReenterPasswordUser",
                "isVisible": true,
                "keyBoardStyle": constants.TEXTBOX_KEY_BOARD_STYLE_DEFAULT,
                "left": "0dp",
                "placeholder": "Re-enter Password",
                "secureTextEntry": true,
                "skin": "skntxtbx484b5214px",
                "textInputMode": constants.TEXTBOX_INPUT_MODE_ANY,
                "top": "25dp",
                "width": "100%",
                "zIndex": 1
            }, {
                "containerHeightMode": constants.TEXTBOX_FONT_METRICS_DRIVEN_HEIGHT,
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [4, 0, 0, 0],
                "paddingInPixel": false
            }, {
                "autoCorrect": false
            });
            var CopylblLineUser0d53d486f5ead4f = new kony.ui.Label({
                "bottom": "2%",
                "height": "2dp",
                "id": "CopylblLineUser0d53d486f5ead4f",
                "isVisible": false,
                "left": 0,
                "skin": "slnLnBackground0e865c9f7aca941",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmAlertsManagement.lblAlertTypeDescriptionCount\")",
                "width": "100%",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var lblCheckPasswordMatching = new kony.ui.Label({
                "bottom": "0px",
                "id": "lblCheckPasswordMatching",
                "isVisible": true,
                "left": "1dp",
                "skin": "lblCheckAvailSknNormal0h0ae95eb2f5a41",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmUsers.lblCheckPasswordMatching\")",
                "width": "100%",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var CopylblUserNameNA0ded79a49f56f43 = new kony.ui.Label({
                "bottom": "3dp",
                "id": "CopylblUserNameNA0ded79a49f56f43",
                "isVisible": false,
                "left": "0dp",
                "skin": "lblCheckAvailSknNormal0h0ae95eb2f5a41",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmUsers.CopylblUserNameNA0ded79a49f56f43\")",
                "width": "100%",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxReenterPasswordUser.add(lblReenterPasswordUser, txtbxReenterPasswordUser, CopylblLineUser0d53d486f5ead4f, lblCheckPasswordMatching, CopylblUserNameNA0ded79a49f56f43);
            var btnResetPassword = new kony.ui.Button({
                "id": "btnResetPassword",
                "isVisible": true,
                "left": "10dp",
                "onClick": controller.AS_Button_f4fe3eca5cea4880b9b4a8563a94bec0,
                "skin": "sknBtnLatoRegular11abeb14px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.users.ResetPassword\")",
                "top": "35px",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_CENTER,
                "displayText": true,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {
                "hoverSkin": "sknBtnLatoRegular1682b213pxHover"
            });
            var btnCheckAvailabilityUser = new kony.ui.Button({
                "id": "btnCheckAvailabilityUser",
                "isVisible": false,
                "left": "10dp",
                "skin": "sknBtnLatoRegular11abeb14px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmCreateNewUser.btnCheckAvailabilityUser\")",
                "top": "35dp",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "displayText": true,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {
                "hoverSkin": "sknBtnLatoRegular1682b213pxHover"
            });
            flxLoginFields.add(flxUserNameUser, flxPasswordUser, flxReenterPasswordUser, btnResetPassword, btnCheckAvailabilityUser);
            flxLoginCreationUser.add(lblLoginCreationUser, lblLineUser2, flxLoginFields);
            var flxHomeAddressUser = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "420dp",
                "id": "flxHomeAddressUser",
                "isVisible": true,
                "layoutType": kony.flex.FLOW_VERTICAL,
                "left": "0dp",
                "masterType": constants.MASTER_TYPE_USERWIDGET,
                "isModalContainer": false,
                "skin": "slFbox0afd6fda7b2514b",
                "top": "-2dp",
                "width": "100%",
                "zIndex": 1
            }, {}, {});
            flxHomeAddressUser.setDefaultUnit(kony.flex.DP);
            var lblHomeAddressUser = new kony.ui.Label({
                "id": "lblHomeAddressUser",
                "isVisible": true,
                "left": "0dp",
                "skin": "sknlblSubHeader0b73b6af9628b4e",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.users.HOMEADDRESS\")",
                "top": 0,
                "width": "100%",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var CopylblLineUser0jf96173620274f = new kony.ui.Label({
                "bottom": "18px",
                "height": "1dp",
                "id": "CopylblLineUser0jf96173620274f",
                "isVisible": true,
                "left": "0dp",
                "skin": "slnLnBackground0e865c9f7aca941",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmAlertsManagement.lblAlertTypeDescriptionCount\")",
                "top": "18px",
                "width": "100%",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var flxAddrLine1User = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "85dp",
                "id": "flxAddrLine1User",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "slFbox0afd6fda7b2514b",
                "top": "0dp",
                "width": "94%",
                "zIndex": 1
            }, {}, {});
            flxAddrLine1User.setDefaultUnit(kony.flex.DP);
            var lblAddressLine1User = new kony.ui.Label({
                "id": "lblAddressLine1User",
                "isVisible": true,
                "left": "0dp",
                "skin": "sknlbl485c7514px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmCreateNewUser.lblAddressLine1User\")",
                "top": "3dp",
                "width": "150dp",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var txtbxAddressLine1User = new kony.ui.TextBox2({
                "autoCapitalize": constants.TEXTBOX_AUTO_CAPITALIZE_NONE,
                "focusSkin": "skntxtbxDetailsFocus14px",
                "height": "40px",
                "id": "txtbxAddressLine1User",
                "isVisible": true,
                "keyBoardStyle": constants.TEXTBOX_KEY_BOARD_STYLE_DEFAULT,
                "left": "0dp",
                "maxTextLength": 40,
                "placeholder": "Address",
                "secureTextEntry": false,
                "skin": "skntxtbx484b5214px",
                "textInputMode": constants.TEXTBOX_INPUT_MODE_NUMERIC,
                "top": "25dp",
                "width": "100%",
                "zIndex": 1
            }, {
                "containerHeightMode": constants.TEXTBOX_FONT_METRICS_DRIVEN_HEIGHT,
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [1, 0, 0, 0],
                "paddingInPixel": false
            }, {
                "autoCorrect": false
            });
            var lblAddrLine1Count = new kony.ui.Label({
                "id": "lblAddrLine1Count",
                "isVisible": true,
                "right": "0dp",
                "skin": "sknlbl485c7514px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmUsers.lblAddrLine1Count\")",
                "top": "3dp",
                "width": "40dp",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_CENTER,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var flxErrorAddress = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "15dp",
                "id": "flxErrorAddress",
                "isVisible": false,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "70dp",
                "width": "100%",
                "zIndex": 1
            }, {}, {});
            flxErrorAddress.setDefaultUnit(kony.flex.DP);
            var lblErrorAddressIcon = new kony.ui.Label({
                "height": "15dp",
                "id": "lblErrorAddressIcon",
                "isVisible": true,
                "left": "0dp",
                "skin": "sknErrorIcon",
                "text": "",
                "top": "0dp",
                "width": "15dp",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var lblErrorAddress = new kony.ui.Label({
                "height": "15dp",
                "id": "lblErrorAddress",
                "isVisible": true,
                "left": "15dp",
                "skin": "sknlblError",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmLocationsController.Enter_Address\")",
                "top": "0dp",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxErrorAddress.add(lblErrorAddressIcon, lblErrorAddress);
            flxAddrLine1User.add(lblAddressLine1User, txtbxAddressLine1User, lblAddrLine1Count, flxErrorAddress);
            var flxAddrLine2User = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "80dp",
                "id": "flxAddrLine2User",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "slFbox0afd6fda7b2514b",
                "top": "2%",
                "width": "94%",
                "zIndex": 1
            }, {}, {});
            flxAddrLine2User.setDefaultUnit(kony.flex.DP);
            var lblAddressLine2User = new kony.ui.Label({
                "id": "lblAddressLine2User",
                "isVisible": true,
                "left": "0dp",
                "skin": "sknlbl485c7514px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmCreateNewUser.lblAddressLine2User\")",
                "top": "3dp",
                "width": "150dp",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var txtbxAddressLine2User = new kony.ui.TextBox2({
                "autoCapitalize": constants.TEXTBOX_AUTO_CAPITALIZE_NONE,
                "focusSkin": "skntxtbxDetailsFocus14px",
                "height": "40px",
                "id": "txtbxAddressLine2User",
                "isVisible": true,
                "keyBoardStyle": constants.TEXTBOX_KEY_BOARD_STYLE_DEFAULT,
                "left": "0dp",
                "maxTextLength": 40,
                "placeholder": "Address",
                "secureTextEntry": false,
                "skin": "skntxtbx484b5214px",
                "textInputMode": constants.TEXTBOX_INPUT_MODE_NUMERIC,
                "top": "25dp",
                "width": "100%",
                "zIndex": 1
            }, {
                "containerHeightMode": constants.TEXTBOX_FONT_METRICS_DRIVEN_HEIGHT,
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [1, 0, 0, 0],
                "paddingInPixel": false
            }, {
                "autoCorrect": false
            });
            var lblAddrLine2Count = new kony.ui.Label({
                "id": "lblAddrLine2Count",
                "isVisible": true,
                "right": "0dp",
                "skin": "sknlbl485c7514px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmUsers.lblAddrLine1Count\")",
                "top": "3dp",
                "width": "40dp",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_CENTER,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var lblAddressOptional = new kony.ui.Label({
                "id": "lblAddressOptional",
                "isVisible": true,
                "left": "90dp",
                "skin": "slLabel0d4f692dab05249",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmPermissions.lblOptional\")",
                "top": "3dp",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxAddrLine2User.add(lblAddressLine2User, txtbxAddressLine2User, lblAddrLine2Count, lblAddressOptional);
            var flxCityUser = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "87dp",
                "id": "flxCityUser",
                "isVisible": true,
                "layoutType": kony.flex.FLOW_HORIZONTAL,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "slFbox0afd6fda7b2514b",
                "top": "2%",
                "width": "100%",
                "zIndex": 1
            }, {}, {});
            flxCityUser.setDefaultUnit(kony.flex.DP);
            var flxCountryUser = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "100%",
                "id": "flxCountryUser",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "slFbox0afd6fda7b2514b",
                "top": "0dp",
                "width": "30%",
                "zIndex": 1
            }, {}, {});
            flxCountryUser.setDefaultUnit(kony.flex.DP);
            var lblCountryUser = new kony.ui.Label({
                "id": "lblCountryUser",
                "isVisible": true,
                "left": "0dp",
                "skin": "sknlbl485c7514px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmCreateNewUser.lblCountryUser\")",
                "top": "0dp",
                "width": "100%",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var lstbxCountryUser = new kony.ui.ListBox({
                "focusSkin": "sknlbxBgffffffBorder11abebRadius3Px14px",
                "height": "40px",
                "id": "lstbxCountryUser",
                "isVisible": true,
                "left": "0dp",
                "masterData": [
                    ["lb1", "Select"],
                    ["lb2", "India"],
                    ["lb3", "USA"]
                ],
                "selectedKey": "lb1",
                "skin": "sknlstbx485c7513px",
                "top": "25dp",
                "width": "100%",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [2, 0, 0, 0],
                "paddingInPixel": false
            }, {
                "hoverSkin": "lstboxcursor",
                "multiSelect": false
            });
            var flxErrorCountry = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "15dp",
                "id": "flxErrorCountry",
                "isVisible": false,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "70dp",
                "width": "100%",
                "zIndex": 1
            }, {}, {});
            flxErrorCountry.setDefaultUnit(kony.flex.DP);
            var lblErrorCountryIcon = new kony.ui.Label({
                "height": "15dp",
                "id": "lblErrorCountryIcon",
                "isVisible": true,
                "left": "0dp",
                "skin": "sknErrorIcon",
                "text": "",
                "top": "0dp",
                "width": "15dp",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var lblErrorCountry = new kony.ui.Label({
                "height": "15dp",
                "id": "lblErrorCountry",
                "isVisible": true,
                "left": "15dp",
                "skin": "sknlblError",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmLocations.lblNoCountryError\")",
                "top": "0dp",
                "width": "100%",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxErrorCountry.add(lblErrorCountryIcon, lblErrorCountry);
            flxCountryUser.add(lblCountryUser, lstbxCountryUser, flxErrorCountry);
            var flxStateUser = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "100%",
                "id": "flxStateUser",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "10dp",
                "isModalContainer": false,
                "skin": "slFbox0afd6fda7b2514b",
                "top": "0dp",
                "width": "30%",
                "zIndex": 1
            }, {}, {});
            flxStateUser.setDefaultUnit(kony.flex.DP);
            var lblStateUser = new kony.ui.Label({
                "id": "lblStateUser",
                "isVisible": true,
                "left": "0dp",
                "skin": "sknlbl485c7514px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmCreateNewUser.lblStateUser\")",
                "top": "0dp",
                "width": "90%",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var lstbxStateUser = new kony.ui.ListBox({
                "focusSkin": "sknlbxBgffffffBorder11abebRadius3Px14px",
                "height": "40px",
                "id": "lstbxStateUser",
                "isVisible": true,
                "left": "0dp",
                "masterData": [
                    ["lb1", "Select"],
                    ["lb2", "Telengana"],
                    ["lb3", "Maharastra"]
                ],
                "selectedKey": "lb1",
                "skin": "sknlstbx485c7513px",
                "top": "25dp",
                "width": "100%",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [2, 0, 0, 0],
                "paddingInPixel": false
            }, {
                "hoverSkin": "lstboxcursor",
                "multiSelect": false
            });
            var flxErrorState = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "15dp",
                "id": "flxErrorState",
                "isVisible": false,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "70dp",
                "width": "100%",
                "zIndex": 1
            }, {}, {});
            flxErrorState.setDefaultUnit(kony.flex.DP);
            var lblErrorStateIcon = new kony.ui.Label({
                "height": "15dp",
                "id": "lblErrorStateIcon",
                "isVisible": true,
                "left": "0dp",
                "skin": "sknErrorIcon",
                "text": "",
                "top": "0dp",
                "width": "15dp",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var lblErrorState = new kony.ui.Label({
                "height": "15dp",
                "id": "lblErrorState",
                "isVisible": true,
                "left": "15dp",
                "skin": "sknlblError",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmLocations.lblNoStateError\")",
                "top": "0dp",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxErrorState.add(lblErrorStateIcon, lblErrorState);
            flxStateUser.add(lblStateUser, lstbxStateUser, flxErrorState);
            var flxCityUser1 = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "100%",
                "id": "flxCityUser1",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "10dp",
                "isModalContainer": false,
                "skin": "slFbox0afd6fda7b2514b",
                "top": "0dp",
                "width": "30%",
                "zIndex": 1
            }, {}, {});
            flxCityUser1.setDefaultUnit(kony.flex.DP);
            var lblCityUser = new kony.ui.Label({
                "id": "lblCityUser",
                "isVisible": true,
                "left": "0dp",
                "skin": "sknlbl485c7514px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmCreateNewUser.lblCityUser\")",
                "top": "0dp",
                "width": "90%",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var flxCityUserData = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "30px",
                "id": "flxCityUserData",
                "isVisible": false,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "flxRoundBordr0jc424c639cff4c",
                "top": "25dp",
                "width": "200dp",
                "zIndex": 1
            }, {}, {});
            flxCityUserData.setDefaultUnit(kony.flex.DP);
            var imgArrow1 = new kony.ui.Image2({
                "height": "25dp",
                "id": "imgArrow1",
                "imageWhenFailed": "downrow.png",
                "imageWhileDownloading": "downrow.png",
                "isVisible": false,
                "right": "5dp",
                "skin": "CopyslImage0db63f9eba6e04b",
                "src": "downrow.png",
                "top": "1dp",
                "width": "25dp",
                "zIndex": 10
            }, {
                "imageScaleMode": constants.IMAGE_SCALE_MODE_MAINTAIN_ASPECT_RATIO,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var FlexContainer0a6bdcaf99ecc4a = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "focusSkin": "flxLineSknNormal0a4d63b626aec47",
                "height": "220%",
                "id": "FlexContainer0a6bdcaf99ecc4a",
                "isVisible": false,
                "layoutType": kony.flex.FREE_FORM,
                "isModalContainer": false,
                "right": "30dp",
                "skin": "flxLineSknNormal0a4d63b626aec47",
                "top": "0dp",
                "width": "1dp",
                "zIndex": 10
            }, {}, {});
            FlexContainer0a6bdcaf99ecc4a.setDefaultUnit(kony.flex.DP);
            FlexContainer0a6bdcaf99ecc4a.add();
            flxCityUserData.add(imgArrow1, FlexContainer0a6bdcaf99ecc4a);
            var lstbxCityUser = new kony.ui.ListBox({
                "focusSkin": "sknlbxBgffffffBorder11abebRadius3Px14px",
                "height": "40px",
                "id": "lstbxCityUser",
                "isVisible": false,
                "left": "0dp",
                "masterData": [
                    ["lb1", "Select"],
                    ["lb2", "Mumbai"],
                    ["lb3", "Delhi"],
                    ["lb4", "Hyderabad"]
                ],
                "selectedKey": "lb1",
                "skin": "sknlstbx485c7513px",
                "top": "25dp",
                "width": "100%",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [2, 0, 0, 0],
                "paddingInPixel": false
            }, {
                "multiSelect": false
            });
            var txtbxCityUser = new kony.ui.TextBox2({
                "autoCapitalize": constants.TEXTBOX_AUTO_CAPITALIZE_NONE,
                "focusSkin": "defTextBoxFocus",
                "height": "40px",
                "id": "txtbxCityUser",
                "isVisible": true,
                "keyBoardStyle": constants.TEXTBOX_KEY_BOARD_STYLE_DEFAULT,
                "left": "0dp",
                "i18n_placeholder": "kony.i18n.getLocalizedString(\"i18n.frmUsersController.Enter_City\")",
                "secureTextEntry": false,
                "skin": "sknTxtBox485c75Rad3pxbr1px",
                "textInputMode": constants.TEXTBOX_INPUT_MODE_ANY,
                "top": "25dp",
                "width": "100%",
                "zIndex": 1
            }, {
                "containerHeightMode": constants.TEXTBOX_FONT_METRICS_DRIVEN_HEIGHT,
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [3, 0, 0, 0],
                "paddingInPixel": false
            }, {
                "autoCorrect": false,
                "placeholderSkin": "defTextBoxPlaceholder"
            });
            var flxErrorCity = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "15dp",
                "id": "flxErrorCity",
                "isVisible": false,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "70dp",
                "width": "100%",
                "zIndex": 1
            }, {}, {});
            flxErrorCity.setDefaultUnit(kony.flex.DP);
            var lblErrorCityIcon = new kony.ui.Label({
                "height": "15dp",
                "id": "lblErrorCityIcon",
                "isVisible": true,
                "left": "0dp",
                "skin": "sknErrorIcon",
                "text": "",
                "top": "0dp",
                "width": "15dp",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var lblErrorCity = new kony.ui.Label({
                "height": "15dp",
                "id": "lblErrorCity",
                "isVisible": true,
                "left": "15dp",
                "skin": "sknlblError",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmLocations.lblNoCityError\")",
                "top": "0dp",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxErrorCity.add(lblErrorCityIcon, lblErrorCity);
            flxCityUser1.add(lblCityUser, flxCityUserData, lstbxCityUser, txtbxCityUser, flxErrorCity);
            flxCityUser.add(flxCountryUser, flxStateUser, flxCityUser1);
            var flxCountryDetailsUser = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "90dp",
                "id": "flxCountryDetailsUser",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "slFbox0afd6fda7b2514b",
                "top": "2%",
                "width": "100%",
                "zIndex": 1
            }, {}, {});
            flxCountryDetailsUser.setDefaultUnit(kony.flex.DP);
            var flxPostalCodeUser = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "100%",
                "id": "flxPostalCodeUser",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "slFbox0afd6fda7b2514b",
                "top": "0dp",
                "width": "30%",
                "zIndex": 1
            }, {}, {});
            flxPostalCodeUser.setDefaultUnit(kony.flex.DP);
            var lblPostalCodeUser = new kony.ui.Label({
                "id": "lblPostalCodeUser",
                "isVisible": true,
                "left": "0dp",
                "skin": "sknlbl485c7514px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmLocations.lblPostalCodeUser\")",
                "top": "0dp",
                "width": "90%",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var txtbxPostalCodeUser = new kony.ui.TextBox2({
                "autoCapitalize": constants.TEXTBOX_AUTO_CAPITALIZE_NONE,
                "focusSkin": "skntxtbxDetailsFocus14px",
                "height": "40px",
                "id": "txtbxPostalCodeUser",
                "isVisible": true,
                "keyBoardStyle": constants.TEXTBOX_KEY_BOARD_STYLE_DEFAULT,
                "left": "0dp",
                "i18n_placeholder": "kony.i18n.getLocalizedString(\"i18n.frmLocations.lblPostalCodeUser\")",
                "secureTextEntry": false,
                "skin": "skntxtbx484b5214px",
                "textInputMode": constants.TEXTBOX_INPUT_MODE_ANY,
                "top": "25dp",
                "width": "100%",
                "zIndex": 1
            }, {
                "containerHeightMode": constants.TEXTBOX_FONT_METRICS_DRIVEN_HEIGHT,
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [3, 0, 0, 0],
                "paddingInPixel": false
            }, {
                "autoCorrect": false
            });
            var flxErrorZipCode = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "15dp",
                "id": "flxErrorZipCode",
                "isVisible": false,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "70dp",
                "width": "100%",
                "zIndex": 1
            }, {}, {});
            flxErrorZipCode.setDefaultUnit(kony.flex.DP);
            var lblErrorZipCodeIcon = new kony.ui.Label({
                "height": "15dp",
                "id": "lblErrorZipCodeIcon",
                "isVisible": true,
                "left": "0dp",
                "skin": "sknErrorIcon",
                "text": "",
                "top": "0dp",
                "width": "15dp",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var lblErrorZipCode = new kony.ui.Label({
                "height": "15dp",
                "id": "lblErrorZipCode",
                "isVisible": true,
                "left": "15dp",
                "skin": "sknlblError",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmLocations.lblNoZipCodeError\")",
                "top": "0dp",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxErrorZipCode.add(lblErrorZipCodeIcon, lblErrorZipCode);
            flxPostalCodeUser.add(lblPostalCodeUser, txtbxPostalCodeUser, flxErrorZipCode);
            flxCountryDetailsUser.add(flxPostalCodeUser);
            flxHomeAddressUser.add(lblHomeAddressUser, CopylblLineUser0jf96173620274f, flxAddrLine1User, flxAddrLine2User, flxCityUser, flxCountryDetailsUser);
            var flxWorkAddressUser = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "bottom": "10px",
                "clipBounds": true,
                "height": "220px",
                "id": "flxWorkAddressUser",
                "isVisible": true,
                "layoutType": kony.flex.FLOW_VERTICAL,
                "left": "0dp",
                "masterType": constants.MASTER_TYPE_USERWIDGET,
                "isModalContainer": false,
                "skin": "slFbox0afd6fda7b2514b",
                "top": "8dp",
                "width": "100%",
                "zIndex": 1
            }, {}, {});
            flxWorkAddressUser.setDefaultUnit(kony.flex.DP);
            var lblWorkAddressUser = new kony.ui.Label({
                "id": "lblWorkAddressUser",
                "isVisible": true,
                "left": "0dp",
                "skin": "sknlblSubHeader0b73b6af9628b4e",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.users.WORKADDRESS\")",
                "width": "100%",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var lblLineUserWork = new kony.ui.Label({
                "bottom": "18px",
                "height": "1dp",
                "id": "lblLineUserWork",
                "isVisible": true,
                "left": "0dp",
                "skin": "slnLnBackground0e865c9f7aca941",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmAlertsManagement.lblAlertTypeDescriptionCount\")",
                "top": "18px",
                "width": "100%",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var flxWorkLocation = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "85dp",
                "id": "flxWorkLocation",
                "isVisible": true,
                "layoutType": kony.flex.FLOW_HORIZONTAL,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "slFbox0afd6fda7b2514b",
                "top": "0px",
                "width": "100%",
                "zIndex": 1
            }, {}, {});
            flxWorkLocation.setDefaultUnit(kony.flex.DP);
            var flxOfficeLocation = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "100%",
                "id": "flxOfficeLocation",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0dp",
                "width": "30%"
            }, {}, {});
            flxOfficeLocation.setDefaultUnit(kony.flex.DP);
            var lblChooseWorkLocation = new kony.ui.Label({
                "id": "lblChooseWorkLocation",
                "isVisible": true,
                "left": "0dp",
                "skin": "sknlbl485c7514px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmUsers.lblChooseWorkLocation\")",
                "top": "3dp",
                "width": "200px",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var lstbxWorkLocationUser = new kony.ui.ListBox({
                "focusSkin": "sknlbxBgffffffBorder11abebRadius3Px14px",
                "height": "40px",
                "id": "lstbxWorkLocationUser",
                "isVisible": true,
                "left": "0dp",
                "masterData": [
                    ["lb1", "Select an Office location"],
                    ["lb2", "New York"],
                    ["lb3", "London"],
                    ["lb4", "Paris"],
                    ["lb5", "New Delhi"],
                    ["lb6", "Tokyo"]
                ],
                "selectedKey": "lb1",
                "skin": "sknlstbx485c7513px",
                "top": "25dp",
                "width": "100%",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [1, 0, 0, 0],
                "paddingInPixel": false
            }, {
                "multiSelect": false
            });
            var flxErrorLocation = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "15dp",
                "id": "flxErrorLocation",
                "isVisible": false,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "70dp",
                "width": "200dp",
                "zIndex": 1
            }, {}, {});
            flxErrorLocation.setDefaultUnit(kony.flex.DP);
            var lblErrorLocationIcon = new kony.ui.Label({
                "height": "15dp",
                "id": "lblErrorLocationIcon",
                "isVisible": true,
                "left": "0dp",
                "skin": "sknErrorIcon",
                "text": "",
                "top": "0dp",
                "width": "15dp",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var lblErrorLocation = new kony.ui.Label({
                "height": "15dp",
                "id": "lblErrorLocation",
                "isVisible": true,
                "left": "15dp",
                "skin": "sknlblError",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmUsers.lblErrorBranch\")",
                "top": "0dp",
                "width": "100%",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxErrorLocation.add(lblErrorLocationIcon, lblErrorLocation);
            flxOfficeLocation.add(lblChooseWorkLocation, lstbxWorkLocationUser, flxErrorLocation);
            var flxWorkBranch = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "100%",
                "id": "flxWorkBranch",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "10dp",
                "isModalContainer": false,
                "right": "20dp",
                "skin": "slFbox",
                "top": "0dp",
                "width": "62%",
                "zIndex": 1
            }, {}, {});
            flxWorkBranch.setDefaultUnit(kony.flex.DP);
            var lblChooseBranch = new kony.ui.Label({
                "id": "lblChooseBranch",
                "isVisible": true,
                "left": "0dp",
                "skin": "sknlbl485c7514px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmUsers.lblChooseBranch\")",
                "top": "3dp",
                "width": "200px",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var lbxWorkbranch = new kony.ui.ListBox({
                "focusSkin": "sknlbxBgffffffBorder11abebRadius3Px14px",
                "height": "40px",
                "id": "lbxWorkbranch",
                "isVisible": true,
                "left": "0dp",
                "masterData": [
                    ["lb1", "Select an office location"],
                    ["lb2", "Lorem Ipsum"],
                    ["lb3", "Lorem Ipsum"],
                    ["lb4", "Lorem Ipsum"],
                    ["lb5", "Lorem Ipsum"]
                ],
                "selectedKey": "lb1",
                "skin": "sknlstbx485c7513px",
                "top": "25dp",
                "width": "100%",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [2, 0, 0, 0],
                "paddingInPixel": false
            }, {
                "multiSelect": false
            });
            var flxErrorBranch = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "15dp",
                "id": "flxErrorBranch",
                "isVisible": false,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "70dp",
                "width": "200dp",
                "zIndex": 1
            }, {}, {});
            flxErrorBranch.setDefaultUnit(kony.flex.DP);
            var lblErrorBranchIcon = new kony.ui.Label({
                "height": "15dp",
                "id": "lblErrorBranchIcon",
                "isVisible": true,
                "left": "0dp",
                "skin": "sknErrorIcon",
                "text": "",
                "top": "0dp",
                "width": "15dp",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var lblErrorBranch = new kony.ui.Label({
                "height": "15dp",
                "id": "lblErrorBranch",
                "isVisible": true,
                "left": "15dp",
                "skin": "sknlblError",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmUsers.lblErrorBranch\")",
                "top": "0dp",
                "width": "100%",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxErrorBranch.add(lblErrorBranchIcon, lblErrorBranch);
            flxWorkBranch.add(lblChooseBranch, lbxWorkbranch, flxErrorBranch);
            flxWorkLocation.add(flxOfficeLocation, flxWorkBranch);
            var rtxWorkAddress = new kony.ui.RichText({
                "id": "rtxWorkAddress",
                "isVisible": true,
                "left": "0dp",
                "skin": "sknrtxLato485c7514px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmUsersController.Select_City_and_Branch\")",
                "top": "5dp",
                "width": "100%",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_TOP_LEFT,
                "padding": [1, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxWorkAddressUser.add(lblWorkAddressUser, lblLineUserWork, flxWorkLocation, rtxWorkAddress);
            flxUserDetailsData.add(flxPersonalInfo, flxLoginCreationUser, flxHomeAddressUser, flxWorkAddressUser);
            var flxRightButtonsSeperator = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "bottom": "80dp",
                "clipBounds": true,
                "height": "1px",
                "id": "flxRightButtonsSeperator",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "isModalContainer": false,
                "right": "0dp",
                "skin": "sknflxe1e5edop100",
                "zIndex": 1
            }, {}, {});
            flxRightButtonsSeperator.setDefaultUnit(kony.flex.DP);
            flxRightButtonsSeperator.add();
            var flxAddUsersButtons = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "bottom": "0px",
                "clipBounds": true,
                "height": "80px",
                "id": "flxAddUsersButtons",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "20dp",
                "isModalContainer": false,
                "right": "0dp",
                "skin": "slFbox",
                "zIndex": 1
            }, {}, {});
            flxAddUsersButtons.setDefaultUnit(kony.flex.DP);
            var btnAddUsersCancel = new kony.ui.Button({
                "centerY": "50%",
                "focusSkin": "sknBtnLatoRegulara5abc413pxKA",
                "height": "40dp",
                "id": "btnAddUsersCancel",
                "isVisible": true,
                "left": 0,
                "right": "29.50%",
                "skin": "sknbtnf7f7faLatoRegular4f555dBorder1px8b96a5",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.permission.CANCEL\")",
                "top": "0%",
                "width": "100px",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_CENTER,
                "displayText": true,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {
                "hoverSkin": "sknbtnffffffLatoRegular4f555dBorder1px485c75"
            });
            var flxAddPermissionRightButtons = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "centerY": "50%",
                "clipBounds": true,
                "height": "100%",
                "id": "flxAddPermissionRightButtons",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "isModalContainer": false,
                "right": 15,
                "skin": "slFbox",
                "width": "220px",
                "zIndex": 1
            }, {}, {});
            flxAddPermissionRightButtons.setDefaultUnit(kony.flex.DP);
            var btnAddUsersNext = new kony.ui.Button({
                "centerY": "50%",
                "focusSkin": "sknBtn4A77A0LatoRegular13pxFFFFFFRad20px",
                "height": "40dp",
                "id": "btnAddUsersNext",
                "isVisible": true,
                "left": 0,
                "skin": "sknBtn4A77A0LatoRegular13pxFFFFFFRad20px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.permission.NEXT\")",
                "top": "0%",
                "width": "100dp",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_CENTER,
                "displayText": true,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {
                "hoverSkin": "sknBtn2D5982LatoRegular13pxFFFFFFRad20px"
            });
            var btnAddUsersSave = new kony.ui.Button({
                "centerY": "50%",
                "focusSkin": "sknBtn003E75LatoRegular13pxFFFFFFRad20px",
                "height": "40dp",
                "id": "btnAddUsersSave",
                "isVisible": true,
                "right": "10px",
                "skin": "sknBtn003E75LatoRegular13pxFFFFFFRad20px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.permission.SAVE\")",
                "top": "0%",
                "width": "100px",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_CENTER,
                "displayText": true,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {
                "hoverSkin": "sknBtn005198LatoRegular13pxFFFFFFRad20px"
            });
            flxAddPermissionRightButtons.add(btnAddUsersNext, btnAddUsersSave);
            flxAddUsersButtons.add(btnAddUsersCancel, flxAddPermissionRightButtons);
            flxAddUsersDetails.add(flxUserDetailsData, flxRightButtonsSeperator, flxAddUsersButtons);
            flxAddOptionsSection.add(lblAddOptionsHeading, flxAddOptionsContainer, flxAddUsersDetails);
            var tbxDummyPassword2 = new kony.ui.TextBox2({
                "autoCapitalize": constants.TEXTBOX_AUTO_CAPITALIZE_NONE,
                "focusSkin": "defTextBoxFocus",
                "height": "40dp",
                "id": "tbxDummyPassword2",
                "isVisible": false,
                "keyBoardStyle": constants.TEXTBOX_KEY_BOARD_STYLE_DEFAULT,
                "left": "21dp",
                "placeholder": "Placeholder",
                "secureTextEntry": true,
                "skin": "slTextBox",
                "textInputMode": constants.TEXTBOX_INPUT_MODE_ANY,
                "top": "173dp",
                "width": "300dp",
                "zIndex": 100
            }, {
                "containerHeightMode": constants.TEXTBOX_FONT_METRICS_DRIVEN_HEIGHT,
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [3, 0, 0, 0],
                "paddingInPixel": false
            }, {
                "autoCorrect": false,
                "placeholderSkin": "defTextBoxPlaceholder"
            });
            flxAddMainContainer.add(flxOptions, flxAddOptionsSection, tbxDummyPassword2);
            var flxViewUsers = new kony.ui.FlexScrollContainer({
                "allowHorizontalBounce": false,
                "allowVerticalBounce": true,
                "bounces": true,
                "clipBounds": true,
                "enableScrolling": true,
                "height": "480px",
                "horizontalScrollIndicator": true,
                "id": "flxViewUsers",
                "isVisible": false,
                "layoutType": kony.flex.FLOW_VERTICAL,
                "left": "35dp",
                "pagingEnabled": false,
                "right": 35,
                "scrollDirection": kony.flex.SCROLL_VERTICAL,
                "skin": "sknscrollflxFFFFFFBorder1Px",
                "top": "0dp",
                "verticalScrollIndicator": true,
                "zIndex": 1
            }, {}, {});
            flxViewUsers.setDefaultUnit(kony.flex.DP);
            var flxViewContainer1 = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                "clipBounds": true,
                "id": "flxViewContainer1",
                "isVisible": true,
                "layoutType": kony.flex.FLOW_VERTICAL,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "5dp",
                "width": "100%",
                "zIndex": 1
            }, {}, {});
            flxViewContainer1.setDefaultUnit(kony.flex.DP);
            var flxRow1 = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                "clipBounds": true,
                "id": "flxRow1",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0dp",
                "width": "100%"
            }, {}, {});
            flxRow1.setDefaultUnit(kony.flex.DP);
            var flxViewKeyValue1 = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "50dp",
                "id": "flxViewKeyValue1",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "20dp",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "20dp",
                "width": "31%",
                "zIndex": 1
            }, {}, {});
            flxViewKeyValue1.setDefaultUnit(kony.flex.DP);
            var fontIconFullName = new kony.ui.Label({
                "id": "fontIconFullName",
                "isVisible": false,
                "left": "0dp",
                "skin": "sknfontIconViewScreen",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmGroups.fontIconViewName\")",
                "top": "1dp",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var lblViewKey1 = new kony.ui.Label({
                "id": "lblViewKey1",
                "isVisible": true,
                "left": "0dp",
                "skin": "sknlblLato696c7311px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.users.FULLNAME\")",
                "top": "2dp",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var lblViewValue1 = new kony.ui.Label({
                "id": "lblViewValue1",
                "isVisible": true,
                "left": "0px",
                "skin": "sknlbl485C75LatoSemiBold13px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmCreateNewUser.lblUser\")",
                "top": "26px",
                "width": "100%",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxViewKeyValue1.add(fontIconFullName, lblViewKey1, lblViewValue1);
            var flxViewKeyValue2 = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "50dp",
                "id": "flxViewKeyValue2",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "33%",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "20dp",
                "width": "31%",
                "zIndex": 1
            }, {}, {});
            flxViewKeyValue2.setDefaultUnit(kony.flex.DP);
            var fontIconActive = new kony.ui.Label({
                "id": "fontIconActive",
                "isVisible": true,
                "left": "0dp",
                "skin": "sknFontIconActivate",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmAlertsManagement.lblViewAlertStatusKey\")",
                "top": "28dp",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var lblViewKey2 = new kony.ui.Label({
                "id": "lblViewKey2",
                "isVisible": true,
                "left": "0dp",
                "skin": "sknlblLato696c7311px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.permission.STATUS\")",
                "top": "2dp",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var lblViewValue2 = new kony.ui.Label({
                "id": "lblViewValue2",
                "isVisible": true,
                "left": "15px",
                "skin": "sknlbl485C75LatoSemiBold13px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.secureimage.Active\")",
                "top": "26px",
                "width": "100%",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var fontIconStatus = new kony.ui.Label({
                "id": "fontIconStatus",
                "isVisible": false,
                "left": "0dp",
                "skin": "sknfontIconViewScreen",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmGroups.fontIconViewStatus\")",
                "top": "1dp",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxViewKeyValue2.add(fontIconActive, lblViewKey2, lblViewValue2, fontIconStatus);
            var flxViewKeyValue3 = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "50dp",
                "id": "flxViewKeyValue3",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "64%",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "20dp",
                "width": "31%",
                "zIndex": 1
            }, {}, {});
            flxViewKeyValue3.setDefaultUnit(kony.flex.DP);
            var fontIconEmail = new kony.ui.Label({
                "id": "fontIconEmail",
                "isVisible": false,
                "left": "0dp",
                "skin": "sknfontIconViewScreen",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmUsers.fontIconEmail\")",
                "top": "1dp",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var lblViewKey3 = new kony.ui.Label({
                "id": "lblViewKey3",
                "isVisible": true,
                "left": "0dp",
                "skin": "sknlblLato696c7311px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.View.EMAILID\")",
                "top": "2dp",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var lblViewValue3 = new kony.ui.Label({
                "id": "lblViewValue3",
                "isVisible": true,
                "left": "0px",
                "skin": "sknlbl485C75LatoSemiBold13px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmUsers.lblViewValue3\")",
                "top": "26px",
                "width": "100%",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxViewKeyValue3.add(fontIconEmail, lblViewKey3, lblViewValue3);
            var flxViewEditButton = new kony.ui.Button({
                "focusSkin": "sknbtnf7f7faLatoRegular12Px485c75Border1px485C75Radius20px",
                "height": "22px",
                "id": "flxViewEditButton",
                "isVisible": true,
                "left": "92%",
                "skin": "sknbtnf7f7faLatoRegular12Px485c75Border1px485C75Radius20px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.permission.Edit\")",
                "top": "20px",
                "width": "50px",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_CENTER,
                "displayText": true,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {
                "hoverSkin": "sknbtnffffffLatoRegular12Px485c75Border1px485C75Radius20px"
            });
            flxRow1.add(flxViewKeyValue1, flxViewKeyValue2, flxViewKeyValue3, flxViewEditButton);
            var flxRow2 = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                "clipBounds": true,
                "id": "flxRow2",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0dp",
                "width": "100%"
            }, {}, {});
            flxRow2.setDefaultUnit(kony.flex.DP);
            var flxViewKeyValue4 = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "50dp",
                "id": "flxViewKeyValue4",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "20px",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "15dp",
                "width": "31%",
                "zIndex": 1
            }, {}, {});
            flxViewKeyValue4.setDefaultUnit(kony.flex.DP);
            var fontIconUsername = new kony.ui.Label({
                "id": "fontIconUsername",
                "isVisible": false,
                "left": "0dp",
                "skin": "sknfontIconViewScreen",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmGroups.fontIconViewName\")",
                "top": "1dp",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var lblViewKey4 = new kony.ui.Label({
                "id": "lblViewKey4",
                "isVisible": true,
                "left": "0dp",
                "skin": "sknlblLato696c7311px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.permission.USERNAME\")",
                "top": "2dp",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var lblViewValue4 = new kony.ui.Label({
                "id": "lblViewValue4",
                "isVisible": true,
                "left": "0px",
                "skin": "sknlbl485C75LatoSemiBold13px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmUsers.lblViewValue4\")",
                "top": "26px",
                "width": "100%",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxViewKeyValue4.add(fontIconUsername, lblViewKey4, lblViewValue4);
            var flxViewDescription = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "bottom": 20,
                "clipBounds": true,
                "height": "20dp",
                "id": "flxViewDescription",
                "isVisible": false,
                "layoutType": kony.flex.FLOW_HORIZONTAL,
                "left": "35px",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "15dp",
                "width": "120px",
                "zIndex": 1
            }, {}, {});
            flxViewDescription.setDefaultUnit(kony.flex.DP);
            var lblViewDescription = new kony.ui.Label({
                "id": "lblViewDescription",
                "isVisible": true,
                "left": "0dp",
                "skin": "sknlblLato5d6c7f12px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.permission.DESCRIPTION\")",
                "top": "1dp",
                "width": "70px",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var imgViewDescription = new kony.ui.Image2({
                "height": "12dp",
                "id": "imgViewDescription",
                "isVisible": false,
                "left": "8px",
                "skin": "slImage",
                "src": "img_desc_arrow.png",
                "top": "1dp",
                "width": "12dp",
                "zIndex": 1
            }, {
                "imageScaleMode": constants.IMAGE_SCALE_MODE_MAINTAIN_ASPECT_RATIO,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxViewDescription.add(lblViewDescription, imgViewDescription);
            var flxViewWorkAddress = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                "clipBounds": true,
                "id": "flxViewWorkAddress",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "33%",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "15dp",
                "width": "31%",
                "zIndex": 1
            }, {}, {});
            flxViewWorkAddress.setDefaultUnit(kony.flex.DP);
            var lblViewWorkAddress = new kony.ui.Label({
                "id": "lblViewWorkAddress",
                "isVisible": true,
                "left": "0dp",
                "skin": "sknlblLato696c7311px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.users.WORKADDRESS\")",
                "top": "1dp",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var rtxViewWorkAddress = new kony.ui.Label({
                "id": "rtxViewWorkAddress",
                "isVisible": true,
                "left": "0px",
                "skin": "sknlbl485C75LatoSemiBold13px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmUsers.lblViewValue4\")",
                "top": "26px",
                "width": "100%",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var fontIconWorkAddr = new kony.ui.Label({
                "id": "fontIconWorkAddr",
                "isVisible": false,
                "left": "0dp",
                "skin": "sknfontIconViewScreen",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmUsers.fontIconHomeAddr\")",
                "top": "0dp",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxViewWorkAddress.add(lblViewWorkAddress, rtxViewWorkAddress, fontIconWorkAddr);
            var flxViewHomeAddress = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                "bottom": "10px",
                "clipBounds": true,
                "id": "flxViewHomeAddress",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "64%",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "15dp",
                "width": "31%",
                "zIndex": 1
            }, {}, {});
            flxViewHomeAddress.setDefaultUnit(kony.flex.DP);
            var lblViewHomeAddress = new kony.ui.Label({
                "id": "lblViewHomeAddress",
                "isVisible": true,
                "left": "0dp",
                "skin": "sknlblLato696c7311px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.users.HOMEADDRESS\")",
                "top": "1dp",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var rtxViewHomeAddress = new kony.ui.Label({
                "id": "rtxViewHomeAddress",
                "isVisible": true,
                "left": "0px",
                "skin": "sknlbl485C75LatoSemiBold13px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmUsers.lblViewValue4\")",
                "top": "26px",
                "width": "100%",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var fontIconHomeAddr = new kony.ui.Label({
                "id": "fontIconHomeAddr",
                "isVisible": false,
                "left": "0dp",
                "skin": "sknfontIconViewScreen",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmUsers.fontIconHomeAddr\")",
                "top": "0dp",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxViewHomeAddress.add(lblViewHomeAddress, rtxViewHomeAddress, fontIconHomeAddr);
            flxRow2.add(flxViewKeyValue4, flxViewDescription, flxViewWorkAddress, flxViewHomeAddress);
            var flxViewLastlogin = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                "bottom": 20,
                "clipBounds": true,
                "id": "flxViewLastlogin",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "20dp",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "15dp",
                "width": "31%",
                "zIndex": 1
            }, {}, {});
            flxViewLastlogin.setDefaultUnit(kony.flex.DP);
            var lblLastlogin = new kony.ui.Label({
                "id": "lblLastlogin",
                "isVisible": true,
                "left": "0dp",
                "skin": "sknlblLato696c7311px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmUsers.lblLastlogin\")",
                "top": "0dp",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var rtxLabellastlogin = new kony.ui.Label({
                "id": "rtxLabellastlogin",
                "isVisible": true,
                "left": "0dp",
                "skin": "sknlbl485C75LatoSemiBold13px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmUsers.rtxLabellastlogin\")",
                "top": "26dp",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var fontIconLastLogged = new kony.ui.Label({
                "id": "fontIconLastLogged",
                "isVisible": false,
                "left": "0dp",
                "skin": "sknfontIconViewScreen",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmUsers.fontIconLastLogged\")",
                "top": "0dp",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxViewLastlogin.add(lblLastlogin, rtxLabellastlogin, fontIconLastLogged);
            flxViewContainer1.add(flxRow1, flxRow2, flxViewLastlogin);
            var flxViewContainer2 = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                "bottom": 50,
                "clipBounds": true,
                "id": "flxViewContainer2",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0dp",
                "width": "100%",
                "zIndex": 1
            }, {}, {});
            flxViewContainer2.setDefaultUnit(kony.flex.DP);
            var flxViewTabs = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "50dp",
                "id": "flxViewTabs",
                "isVisible": true,
                "layoutType": kony.flex.FLOW_HORIZONTAL,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "15dp",
                "width": "100%",
                "zIndex": 1
            }, {}, {});
            flxViewTabs.setDefaultUnit(kony.flex.DP);
            var flxViewTab1 = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "100%",
                "id": "flxViewTab1",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "20dp",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0dp",
                "width": "35px",
                "zIndex": 1
            }, {}, {});
            flxViewTab1.setDefaultUnit(kony.flex.DP);
            var lblTabName1 = new kony.ui.Label({
                "bottom": "0%",
                "centerX": "50%",
                "height": "100%",
                "id": "lblTabName1",
                "isVisible": true,
                "skin": "sknlblLato5d6c7f12px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.roles.ROLE\")",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_CENTER,
                "padding": [3, 0, 3, 0],
                "paddingInPixel": false
            }, {
                "hoverSkin": "sknlblLato0hd9dd99ab6d14aHover"
            });
            var flxTabUnderline1 = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "3px",
                "id": "flxTabUnderline1",
                "isVisible": false,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "sknflx11abebRadius3px",
                "top": "45dp",
                "width": "32px",
                "zIndex": 1
            }, {}, {});
            flxTabUnderline1.setDefaultUnit(kony.flex.DP);
            flxTabUnderline1.add();
            flxViewTab1.add(lblTabName1, flxTabUnderline1);
            var flxViewTab2 = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "100%",
                "id": "flxViewTab2",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "20dp",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0dp",
                "width": "80px",
                "zIndex": 1
            }, {}, {});
            flxViewTab2.setDefaultUnit(kony.flex.DP);
            var lblTabName2 = new kony.ui.Label({
                "bottom": "0%",
                "centerX": "50%",
                "height": "100%",
                "id": "lblTabName2",
                "isVisible": true,
                "skin": "sknlblLato5d6c7f12px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.permission.PERMISSIONS\")",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_CENTER,
                "padding": [3, 0, 3, 0],
                "paddingInPixel": false
            }, {
                "hoverSkin": "sknlblLato0hd9dd99ab6d14aHover"
            });
            var flxTabUnderline2 = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "3dp",
                "id": "flxTabUnderline2",
                "isVisible": false,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "sknflx11abebRadius3px",
                "top": "45dp",
                "width": "80px",
                "zIndex": 1
            }, {}, {});
            flxTabUnderline2.setDefaultUnit(kony.flex.DP);
            flxTabUnderline2.add();
            flxViewTab2.add(lblTabName2, flxTabUnderline2);
            flxViewTabs.add(flxViewTab1, flxViewTab2);
            var flxViewSeperator = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "1px",
                "id": "flxViewSeperator",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "sknflxSepratorE1E5ED",
                "top": "66dp",
                "width": "100%",
                "zIndex": 1
            }, {}, {});
            flxViewSeperator.setDefaultUnit(kony.flex.DP);
            flxViewSeperator.add();
            var flxViewSegmentAndHeaders = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                "clipBounds": true,
                "id": "flxViewSegmentAndHeaders",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "20dp",
                "isModalContainer": false,
                "right": 20,
                "skin": "slFbox",
                "top": "67dp",
                "zIndex": 1
            }, {}, {});
            flxViewSegmentAndHeaders.setDefaultUnit(kony.flex.DP);
            var flxPermissionsHeader = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "60px",
                "id": "flxPermissionsHeader",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0",
                "isModalContainer": false,
                "skin": "sknflxffffffop100",
                "top": "0",
                "width": "100%"
            }, {}, {});
            flxPermissionsHeader.setDefaultUnit(kony.flex.DP);
            var flxViewPermissionName = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "100%",
                "id": "flxViewPermissionName",
                "isVisible": true,
                "layoutType": kony.flex.FLOW_HORIZONTAL,
                "left": "10px",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0dp",
                "width": "100px",
                "zIndex": 1
            }, {}, {});
            flxViewPermissionName.setDefaultUnit(kony.flex.DP);
            var lblViewPermissionName = new kony.ui.Label({
                "centerY": "50%",
                "id": "lblViewPermissionName",
                "isVisible": true,
                "left": "0px",
                "skin": "sknlblLato696c7312px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.permission.NAME\")",
                "width": "37px",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {
                "hoverSkin": "sknlblLato00000011px"
            });
            var lblViewPermissionNameSort = new kony.ui.Label({
                "centerY": "50%",
                "id": "lblViewPermissionNameSort",
                "isVisible": true,
                "left": "7dp",
                "skin": "sknIcon15px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmAlertsManagement.lblSortName\")",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {
                "hoverSkin": "sknlblcursor"
            });
            flxViewPermissionName.add(lblViewPermissionName, lblViewPermissionNameSort);
            var flxViewPermissionDescription = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "100%",
                "id": "flxViewPermissionDescription",
                "isVisible": true,
                "layoutType": kony.flex.FLOW_HORIZONTAL,
                "left": "30%",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0dp",
                "width": "100px",
                "zIndex": 1
            }, {}, {});
            flxViewPermissionDescription.setDefaultUnit(kony.flex.DP);
            var lblViewPermissionDescription = new kony.ui.Label({
                "centerY": "50%",
                "id": "lblViewPermissionDescription",
                "isVisible": true,
                "left": "0px",
                "skin": "sknlblLato696c7312px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.permission.DESCRIPTION\")",
                "top": 0,
                "width": "90px",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxViewPermissionDescription.add(lblViewPermissionDescription);
            var flxViewPermissionSeperator = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "bottom": 0,
                "clipBounds": true,
                "height": "1px",
                "id": "flxViewPermissionSeperator",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "sknTableHeaderLine",
                "width": "100%",
                "zIndex": 1
            }, {}, {});
            flxViewPermissionSeperator.setDefaultUnit(kony.flex.DP);
            flxViewPermissionSeperator.add();
            flxPermissionsHeader.add(flxViewPermissionName, flxViewPermissionDescription, flxViewPermissionSeperator);
            var flxRolesHeader = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "60px",
                "id": "flxRolesHeader",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "sknflxffffffop100",
                "top": "0dp",
                "width": "100%"
            }, {}, {});
            flxRolesHeader.setDefaultUnit(kony.flex.DP);
            var flxViewRolesName = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "100%",
                "id": "flxViewRolesName",
                "isVisible": true,
                "layoutType": kony.flex.FLOW_HORIZONTAL,
                "left": "10px",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0dp",
                "width": "100px",
                "zIndex": 1
            }, {}, {});
            flxViewRolesName.setDefaultUnit(kony.flex.DP);
            var lblViewRolesName = new kony.ui.Label({
                "centerY": "50%",
                "id": "lblViewRolesName",
                "isVisible": true,
                "left": "0px",
                "skin": "sknlblLato696c7312px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.permission.NAME\")",
                "width": "37px",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var lblViewRolesNameSort = new kony.ui.Label({
                "centerY": "50%",
                "id": "lblViewRolesNameSort",
                "isVisible": true,
                "left": "7dp",
                "skin": "sknIcon15px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmAlertsManagement.lblSortName\")",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {
                "hoverSkin": "sknlblcursor"
            });
            flxViewRolesName.add(lblViewRolesName, lblViewRolesNameSort);
            var flxViewRolesDescription = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "100%",
                "id": "flxViewRolesDescription",
                "isVisible": true,
                "layoutType": kony.flex.FLOW_HORIZONTAL,
                "left": "26.27%",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0dp",
                "width": "100px",
                "zIndex": 1
            }, {}, {});
            flxViewRolesDescription.setDefaultUnit(kony.flex.DP);
            var lblViewRolesDescription = new kony.ui.Label({
                "centerY": "50%",
                "id": "lblViewRolesDescription",
                "isVisible": true,
                "left": "0px",
                "skin": "sknlblLato696c7312px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.permission.DESCRIPTION\")",
                "width": "90px",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxViewRolesDescription.add(lblViewRolesDescription);
            var flxViewRolesSeperator = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "bottom": 0,
                "clipBounds": true,
                "height": "1px",
                "id": "flxViewRolesSeperator",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "sknTableHeaderLine",
                "width": "100%",
                "zIndex": 1
            }, {}, {});
            flxViewRolesSeperator.setDefaultUnit(kony.flex.DP);
            flxViewRolesSeperator.add();
            flxRolesHeader.add(flxViewRolesName, flxViewRolesDescription, flxViewRolesSeperator);
            var flxViewSegment = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                "clipBounds": true,
                "id": "flxViewSegment",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "61dp",
                "width": "100%",
                "zIndex": 1
            }, {}, {});
            flxViewSegment.setDefaultUnit(kony.flex.DP);
            var segViewSegment = new kony.ui.SegmentedUI2({
                "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                "bottom": "10px",
                "data": [{
                    "lblDescription": "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do",
                    "lblIconAction": "Label",
                    "lblPermissionName": "Admin Role",
                    "lblSeperator": "."
                }, {
                    "lblDescription": "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do",
                    "lblIconAction": "Label",
                    "lblPermissionName": "Admin Role",
                    "lblSeperator": "."
                }, {
                    "lblDescription": "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do",
                    "lblIconAction": "Label",
                    "lblPermissionName": "Admin Role",
                    "lblSeperator": "."
                }, {
                    "lblDescription": "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do",
                    "lblIconAction": "Label",
                    "lblPermissionName": "Admin Role",
                    "lblSeperator": "."
                }, {
                    "lblDescription": "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do",
                    "lblIconAction": "Label",
                    "lblPermissionName": "Admin Role",
                    "lblSeperator": "."
                }, {
                    "lblDescription": "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do",
                    "lblIconAction": "Label",
                    "lblPermissionName": "Admin Role",
                    "lblSeperator": "."
                }, {
                    "lblDescription": "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do",
                    "lblIconAction": "Label",
                    "lblPermissionName": "Admin Role",
                    "lblSeperator": "."
                }, {
                    "lblDescription": "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do",
                    "lblIconAction": "Label",
                    "lblPermissionName": "Admin Role",
                    "lblSeperator": "."
                }, {
                    "lblDescription": "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do",
                    "lblIconAction": "Label",
                    "lblPermissionName": "Admin Role",
                    "lblSeperator": "."
                }],
                "groupCells": false,
                "id": "segViewSegment",
                "isVisible": true,
                "left": "0dp",
                "needPageIndicator": true,
                "pageOffDotImage": "pageoffdot.png",
                "pageOnDotImage": "pageondot.png",
                "retainSelection": false,
                "rowFocusSkin": "seg2Focus",
                "rowSkin": "seg2Normal",
                "rowTemplate": "flxViewPermissions",
                "sectionHeaderSkin": "sliPhoneSegmentHeader",
                "selectionBehavior": constants.SEGUI_DEFAULT_BEHAVIOR,
                "separatorRequired": false,
                "showScrollbars": false,
                "viewType": constants.SEGUI_VIEW_TYPE_TABLEVIEW,
                "widgetDataMap": {
                    "flxActionIcon": "flxActionIcon",
                    "flxViewPermissions": "flxViewPermissions",
                    "lblDescription": "lblDescription",
                    "lblIconAction": "lblIconAction",
                    "lblPermissionName": "lblPermissionName",
                    "lblSeperator": "lblSeperator"
                },
                "width": "100%",
                "zIndex": 1
            }, {
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxViewSegment.add(segViewSegment);
            var rtxAvailabletxt = new kony.ui.RichText({
                "bottom": "50px",
                "centerX": "50%",
                "id": "rtxAvailabletxt",
                "isVisible": false,
                "skin": "sknRtxLato84939e12Px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmUsers.rtxAvailabletxt\")",
                "top": "50px",
                "width": "80%",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_CENTER,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxViewSegmentAndHeaders.add(flxPermissionsHeader, flxRolesHeader, flxViewSegment, rtxAvailabletxt);
            var flxViewConfigureCSRPermissions = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                "clipBounds": true,
                "id": "flxViewConfigureCSRPermissions",
                "isVisible": false,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "67dp",
                "width": "100%",
                "zIndex": 1
            }, {}, {});
            flxViewConfigureCSRPermissions.setDefaultUnit(kony.flex.DP);
            var viewConfigureCSRAssist = new com.adminConsole.Permissions.viewConfigureCSRAssist({
                "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                "id": "viewConfigureCSRAssist",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "masterType": constants.MASTER_TYPE_DEFAULT,
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0dp",
                "width": "100%",
                "overrides": {}
            }, {
                "overrides": {}
            }, {
                "overrides": {}
            });
            flxViewConfigureCSRPermissions.add(viewConfigureCSRAssist);
            var flxPagination1 = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "bottom": 20,
                "clipBounds": true,
                "height": "30px",
                "id": "flxPagination1",
                "isVisible": false,
                "layoutType": kony.flex.FLOW_HORIZONTAL,
                "isModalContainer": false,
                "right": "35dp",
                "skin": "slFbox0d9c3974835234d",
                "top": "90px",
                "width": "222px",
                "zIndex": 1
            }, {}, {});
            flxPagination1.setDefaultUnit(kony.flex.DP);
            var lbxPaginationKA = new kony.ui.ListBox({
                "height": "30dp",
                "id": "lbxPaginationKA",
                "isVisible": true,
                "left": "0dp",
                "masterData": [
                    ["lb1", "Page 1 of 20"],
                    ["lb2", "Page 2 of 20"],
                    ["lb3", "Page 3 of 20"],
                    ["lb4", "Page 4 of 20"]
                ],
                "selectedKey": "lb1",
                "skin": "sknlbxNobgNoBorderPagination",
                "top": "0dp",
                "width": "120dp",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {
                "multiSelect": false
            });
            var flxPaginationSeperatorKA = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "100%",
                "id": "flxPaginationSeperatorKA",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "15dp",
                "isModalContainer": false,
                "skin": "sknflxd6dbe7",
                "top": "2dp",
                "width": "1px",
                "zIndex": 1
            }, {}, {});
            flxPaginationSeperatorKA.setDefaultUnit(kony.flex.DP);
            flxPaginationSeperatorKA.add();
            var flxPrevious2 = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "30dp",
                "id": "flxPrevious2",
                "isVisible": false,
                "layoutType": kony.flex.FREE_FORM,
                "left": "15dp",
                "isModalContainer": false,
                "skin": "sknflxffffffop100Bordercbcdd1Radius100px",
                "top": "0dp",
                "width": "30px",
                "zIndex": 1
            }, {}, {});
            flxPrevious2.setDefaultUnit(kony.flex.DP);
            var imgPrevious2 = new kony.ui.Image2({
                "centerX": "50%",
                "centerY": "50%",
                "height": "15dp",
                "id": "imgPrevious2",
                "isVisible": true,
                "skin": "slImage",
                "src": "rightarrow_circel2x.png",
                "width": "15dp",
                "zIndex": 1
            }, {
                "imageScaleMode": constants.IMAGE_SCALE_MODE_MAINTAIN_ASPECT_RATIO,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxPrevious2.add(imgPrevious2);
            var flxNext2 = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "30dp",
                "id": "flxNext2",
                "isVisible": false,
                "layoutType": kony.flex.FREE_FORM,
                "left": "10dp",
                "isModalContainer": false,
                "skin": "sknflxffffffop100Bordercbcdd1Radius100px",
                "top": "0dp",
                "width": "30px",
                "zIndex": 1
            }, {}, {});
            flxNext2.setDefaultUnit(kony.flex.DP);
            var imgNext2 = new kony.ui.Image2({
                "centerX": "50%",
                "centerY": "50%",
                "height": "15dp",
                "id": "imgNext2",
                "isVisible": true,
                "skin": "slImage",
                "src": "left_cricle.png",
                "width": "15dp",
                "zIndex": 1
            }, {
                "imageScaleMode": constants.IMAGE_SCALE_MODE_MAINTAIN_ASPECT_RATIO,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxNext2.add(imgNext2);
            var imgPreviousArrow1 = new kony.ui.Image2({
                "height": "30px",
                "id": "imgPreviousArrow1",
                "isVisible": true,
                "left": "15px",
                "skin": "slImage",
                "src": "rightarrow_circel2x.png",
                "top": "0px",
                "width": "30dp",
                "zIndex": 1
            }, {
                "imageScaleMode": constants.IMAGE_SCALE_MODE_MAINTAIN_ASPECT_RATIO,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var imgNextArrow1 = new kony.ui.Image2({
                "height": "30dp",
                "id": "imgNextArrow1",
                "isVisible": true,
                "left": "10dp",
                "skin": "slImage",
                "src": "left_cricle.png",
                "top": "0dp",
                "width": "30dp",
                "zIndex": 1
            }, {
                "imageScaleMode": constants.IMAGE_SCALE_MODE_MAINTAIN_ASPECT_RATIO,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxPagination1.add(lbxPaginationKA, flxPaginationSeperatorKA, flxPrevious2, flxNext2, imgPreviousArrow1, imgNextArrow1);
            flxViewContainer2.add(flxViewTabs, flxViewSeperator, flxViewSegmentAndHeaders, flxViewConfigureCSRPermissions, flxPagination1);
            flxViewUsers.add(flxViewContainer1, flxViewContainer2);
            flxViews.add(flxAddMainContainer, flxViewUsers);
            var flxAdvansedSearch = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                "clipBounds": true,
                "id": "flxAdvansedSearch",
                "isVisible": false,
                "layoutType": kony.flex.FREE_FORM,
                "left": "35px",
                "isModalContainer": false,
                "right": "35px",
                "skin": "CopyslFbox0c37f1533930c4d",
                "top": "0px",
                "zIndex": 1
            }, {}, {});
            flxAdvansedSearch.setDefaultUnit(kony.flex.DP);
            var flxSearch1 = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                "bottom": "35px",
                "clipBounds": true,
                "id": "flxSearch1",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "CopyslFbox0ddacfb6d79684f",
                "top": "0dp",
                "width": "100%",
                "zIndex": 1
            }, {}, {});
            flxSearch1.setDefaultUnit(kony.flex.DP);
            var flxSearchHeading = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                "clipBounds": true,
                "id": "flxSearchHeading",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0%",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0px",
                "width": "100%",
                "zIndex": 1
            }, {}, {});
            flxSearchHeading.setDefaultUnit(kony.flex.DP);
            var advancedSearch = new com.adminConsole.search.advancedSearch({
                "height": "85px",
                "id": "advancedSearch",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "masterType": constants.MASTER_TYPE_DEFAULT,
                "isModalContainer": false,
                "skin": "slFbox0hfd18814fd664dCM",
                "top": "0dp",
                "width": "100%",
                "overrides": {}
            }, {
                "overrides": {}
            }, {
                "overrides": {}
            });
            var flxSearchResult = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "60px",
                "id": "flxSearchResult",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "85dp",
                "width": "100%",
                "zIndex": 1
            }, {}, {});
            flxSearchResult.setDefaultUnit(kony.flex.DP);
            var flxCsr = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "25px",
                "id": "flxCsr",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "35dp",
                "isModalContainer": false,
                "skin": "CopyslFbox0ica8ce013fdc40",
                "top": "16dp",
                "width": "95px",
                "zIndex": 1
            }, {}, {});
            flxCsr.setDefaultUnit(kony.flex.DP);
            var lblCsrAssist = new kony.ui.Label({
                "centerY": "50%",
                "id": "lblCsrAssist",
                "isVisible": true,
                "left": "5dp",
                "skin": "CopyslLabel0d2920a8c52fc41",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmLogsController.CSR_Assist\")",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var imgCross = new kony.ui.Image2({
                "centerY": "50%",
                "height": "10dp",
                "id": "imgCross",
                "isVisible": true,
                "left": "75dp",
                "skin": "slImage",
                "src": "close_blue.png",
                "width": "10dp",
                "zIndex": 1
            }, {
                "imageScaleMode": constants.IMAGE_SCALE_MODE_MAINTAIN_ASPECT_RATIO,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxCsr.add(lblCsrAssist, imgCross);
            var flxAdmin = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "25px",
                "id": "flxAdmin",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "140dp",
                "isModalContainer": false,
                "skin": "CopyslFbox0ica8ce013fdc40",
                "top": "16dp",
                "width": "95px",
                "zIndex": 1
            }, {}, {});
            flxAdmin.setDefaultUnit(kony.flex.DP);
            var lblAdmin = new kony.ui.Label({
                "centerY": "50%",
                "id": "lblAdmin",
                "isVisible": true,
                "left": "25dp",
                "skin": "CopyslLabel0d2920a8c52fc41",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmLogsController.Admin\")",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var imgCross1 = new kony.ui.Image2({
                "centerY": "50%",
                "height": "10dp",
                "id": "imgCross1",
                "isVisible": true,
                "left": "75dp",
                "skin": "slImage",
                "src": "close_blue.png",
                "width": "10dp",
                "zIndex": 1
            }, {
                "imageScaleMode": constants.IMAGE_SCALE_MODE_MAINTAIN_ASPECT_RATIO,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxAdmin.add(lblAdmin, imgCross1);
            var flxSuperAdmin = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "25px",
                "id": "flxSuperAdmin",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "245dp",
                "isModalContainer": false,
                "skin": "CopyslFbox0ica8ce013fdc40",
                "top": "16dp",
                "width": "95px",
                "zIndex": 1
            }, {}, {});
            flxSuperAdmin.setDefaultUnit(kony.flex.DP);
            var lblSuperAdmin = new kony.ui.Label({
                "centerY": "50%",
                "id": "lblSuperAdmin",
                "isVisible": true,
                "left": "5dp",
                "skin": "CopyslLabel0d2920a8c52fc41",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmLogsController.Super_Admin\")",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var imgCross2 = new kony.ui.Image2({
                "centerY": "50%",
                "height": "10dp",
                "id": "imgCross2",
                "isVisible": true,
                "left": "75dp",
                "skin": "slImage",
                "src": "close_blue.png",
                "width": "10dp",
                "zIndex": 1
            }, {
                "imageScaleMode": constants.IMAGE_SCALE_MODE_MAINTAIN_ASPECT_RATIO,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxSuperAdmin.add(lblSuperAdmin, imgCross2);
            var flxSeparetor = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "centerY": "50%",
                "clipBounds": true,
                "height": "25px",
                "id": "flxSeparetor",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "356dp",
                "isModalContainer": false,
                "skin": "CopyslFbox0b600ae3ddf634f",
                "width": "1px",
                "zIndex": 1
            }, {}, {});
            flxSeparetor.setDefaultUnit(kony.flex.DP);
            flxSeparetor.add();
            var lblClearAll = new kony.ui.Label({
                "centerY": "50%",
                "id": "lblClearAll",
                "isVisible": true,
                "left": "372dp",
                "skin": "CopyslLabel0d6e029a5d8e647",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmCustomerCare.lblClearAll\")",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var flxUnderline2 = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "bottom": "0dp",
                "clipBounds": true,
                "height": "1px",
                "id": "flxUnderline2",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "CopyslFbox0g9535637f9db4c",
                "width": "100%",
                "zIndex": 1
            }, {}, {});
            flxUnderline2.setDefaultUnit(kony.flex.DP);
            flxUnderline2.add();
            flxSearchResult.add(flxCsr, flxAdmin, flxSuperAdmin, flxSeparetor, lblClearAll, flxUnderline2);
            flxSearchHeading.add(advancedSearch, flxSearchResult);
            var flxSegmentUsers1 = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                "clipBounds": false,
                "id": "flxSegmentUsers1",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0%",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "146px",
                "width": "100%",
                "zIndex": 1
            }, {}, {});
            flxSegmentUsers1.setDefaultUnit(kony.flex.DP);
            var segusers1 = new kony.ui.SegmentedUI2({
                "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                "data": [
                    [{
                            "lblFilterStatus": "Label",
                            "lblSortEmail": "",
                            "lblSortName": "Label",
                            "lblSortPermissions": "",
                            "lblSortRole": "",
                            "lblSortUsername": "",
                            "lblUsersHeaderEmail": "EMAIL",
                            "lblUsersHeaderName": "FULL NAME",
                            "lblUsersHeaderPermissions": "PERMISSIONS",
                            "lblUsersHeaderRole": "ROLE",
                            "lblUsersHeaderSeperator": ".",
                            "lblUsersHeaderStatus": "STATUS",
                            "lblUsersHeaderUsername": "USERNAME"
                        },
                        [{
                            "fontIconStatusImg": "",
                            "lblEmailId": "john.doe@kony.com",
                            "lblFontIconOptions": "",
                            "lblFullName": "John Doe",
                            "lblPermissions": "25",
                            "lblRole": "20",
                            "lblSeperator": ".",
                            "lblUsername": "john.doe",
                            "lblUsersStatus": "Active"
                        }, {
                            "fontIconStatusImg": "",
                            "lblEmailId": "john.doe@kony.com",
                            "lblFontIconOptions": "",
                            "lblFullName": "John Doe",
                            "lblPermissions": "25",
                            "lblRole": "20",
                            "lblSeperator": ".",
                            "lblUsername": "john.doe",
                            "lblUsersStatus": "Active"
                        }, {
                            "fontIconStatusImg": "",
                            "lblEmailId": "john.doe@kony.com",
                            "lblFontIconOptions": "",
                            "lblFullName": "John Doe",
                            "lblPermissions": "25",
                            "lblRole": "20",
                            "lblSeperator": ".",
                            "lblUsername": "john.doe",
                            "lblUsersStatus": "Active"
                        }, {
                            "fontIconStatusImg": "",
                            "lblEmailId": "john.doe@kony.com",
                            "lblFontIconOptions": "",
                            "lblFullName": "John Doe",
                            "lblPermissions": "25",
                            "lblRole": "20",
                            "lblSeperator": ".",
                            "lblUsername": "john.doe",
                            "lblUsersStatus": "Active"
                        }, {
                            "fontIconStatusImg": "",
                            "lblEmailId": "john.doe@kony.com",
                            "lblFontIconOptions": "",
                            "lblFullName": "John Doe",
                            "lblPermissions": "25",
                            "lblRole": "20",
                            "lblSeperator": ".",
                            "lblUsername": "john.doe",
                            "lblUsersStatus": "Active"
                        }]
                    ]
                ],
                "groupCells": false,
                "id": "segusers1",
                "isVisible": true,
                "left": "35dp",
                "needPageIndicator": true,
                "pageOffDotImage": "pageoffdot.png",
                "pageOnDotImage": "pageondot.png",
                "retainSelection": false,
                "right": 35,
                "rowFocusSkin": "seg2Focus",
                "rowSkin": "sknseg3pxRadius",
                "rowTemplate": "flxUsers",
                "sectionHeaderSkin": "sliPhoneSegmentHeader",
                "sectionHeaderTemplate": "flxUsersHeader",
                "selectionBehavior": constants.SEGUI_DEFAULT_BEHAVIOR,
                "separatorRequired": false,
                "showScrollbars": false,
                "top": "0dp",
                "viewType": constants.SEGUI_VIEW_TYPE_TABLEVIEW,
                "widgetDataMap": {
                    "flxOptions": "flxOptions",
                    "flxStatus": "flxStatus",
                    "flxUsers": "flxUsers",
                    "flxUsersContainer": "flxUsersContainer",
                    "flxUsersHeader": "flxUsersHeader",
                    "flxUsersHeaderEmailId": "flxUsersHeaderEmailId",
                    "flxUsersHeaderFullName": "flxUsersHeaderFullName",
                    "flxUsersHeaderPermissions": "flxUsersHeaderPermissions",
                    "flxUsersHeaderRole": "flxUsersHeaderRole",
                    "flxUsersHeaderStatus": "flxUsersHeaderStatus",
                    "flxUsersHeaderUsername": "flxUsersHeaderUsername",
                    "fontIconStatusImg": "fontIconStatusImg",
                    "lblEmailId": "lblEmailId",
                    "lblFilterStatus": "lblFilterStatus",
                    "lblFontIconOptions": "lblFontIconOptions",
                    "lblFullName": "lblFullName",
                    "lblPermissions": "lblPermissions",
                    "lblRole": "lblRole",
                    "lblSeperator": "lblSeperator",
                    "lblSortEmail": "lblSortEmail",
                    "lblSortName": "lblSortName",
                    "lblSortPermissions": "lblSortPermissions",
                    "lblSortRole": "lblSortRole",
                    "lblSortUsername": "lblSortUsername",
                    "lblUsername": "lblUsername",
                    "lblUsersHeaderEmail": "lblUsersHeaderEmail",
                    "lblUsersHeaderName": "lblUsersHeaderName",
                    "lblUsersHeaderPermissions": "lblUsersHeaderPermissions",
                    "lblUsersHeaderRole": "lblUsersHeaderRole",
                    "lblUsersHeaderSeperator": "lblUsersHeaderSeperator",
                    "lblUsersHeaderStatus": "lblUsersHeaderStatus",
                    "lblUsersHeaderUsername": "lblUsersHeaderUsername",
                    "lblUsersStatus": "lblUsersStatus"
                },
                "zIndex": 10
            }, {
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var flexOptions = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                "clipBounds": false,
                "id": "flexOptions",
                "isVisible": false,
                "layoutType": kony.flex.FLOW_VERTICAL,
                "isModalContainer": false,
                "right": "2%",
                "skin": "sknflxffffffop100dbdbe6Radius3px",
                "top": "100px",
                "width": "18.39%",
                "zIndex": 100
            }, {}, {});
            flexOptions.setDefaultUnit(kony.flex.DP);
            var CopylblDescription0a252259b5c5b47 = new kony.ui.Label({
                "height": "40px",
                "id": "CopylblDescription0a252259b5c5b47",
                "isVisible": true,
                "left": "15px",
                "skin": "sknlblLato5d6c7f12px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.roles.ASSIGN\")",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 6, 0, 0],
                "paddingInPixel": false
            }, {});
            var CopylblRoles0f4ed925d516743 = new kony.ui.Label({
                "id": "CopylblRoles0f4ed925d516743",
                "isVisible": true,
                "left": "15dp",
                "skin": "sknlbllato00a3d412px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.permission.Users\")",
                "top": "0dp",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var CopylblPremissions0f8ff66e806ad46 = new kony.ui.Label({
                "bottom": 7,
                "id": "CopylblPremissions0f8ff66e806ad46",
                "isVisible": true,
                "left": "15dp",
                "skin": "sknlbllato00a3d412px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmCustomerCare.CopylblPremissions0f8ff66e806ad46\")",
                "top": "5dp",
                "width": kony.flex.USE_PREFFERED_SIZE,
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var CopyflxSeperator0f47be42d7a5845 = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "centerX": "50%",
                "clipBounds": true,
                "height": "1px",
                "id": "CopyflxSeperator0f47be42d7a5845",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "isModalContainer": false,
                "skin": "sknflxd6dbe7",
                "top": "0px",
                "width": "100%",
                "zIndex": 1
            }, {}, {});
            CopyflxSeperator0f47be42d7a5845.setDefaultUnit(kony.flex.DP);
            CopyflxSeperator0f47be42d7a5845.add();
            var CopyflxOption0f482b0cd04b74b = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "35dp",
                "id": "CopyflxOption0f482b0cd04b74b",
                "isVisible": false,
                "layoutType": kony.flex.FLOW_HORIZONTAL,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0dp",
                "width": "100%",
                "zIndex": 1
            }, {}, {});
            CopyflxOption0f482b0cd04b74b.setDefaultUnit(kony.flex.DP);
            var CopyimgOption0cd1138e9eecd41 = new kony.ui.Image2({
                "centerY": "50%",
                "height": "15dp",
                "id": "CopyimgOption0cd1138e9eecd41",
                "isVisible": true,
                "left": "15dp",
                "skin": "slImage",
                "src": "imagedrag.png",
                "width": "15dp",
                "zIndex": 1
            }, {
                "imageScaleMode": constants.IMAGE_SCALE_MODE_MAINTAIN_ASPECT_RATIO,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var CopylblOption0h45b31f2ff7d41 = new kony.ui.Label({
                "centerY": "50%",
                "id": "CopylblOption0h45b31f2ff7d41",
                "isVisible": true,
                "left": "15dp",
                "skin": "sknlblLatoBold35475f14px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.permission.View\")",
                "top": "4dp",
                "width": "40px",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            CopyflxOption0f482b0cd04b74b.add(CopyimgOption0cd1138e9eecd41, CopylblOption0h45b31f2ff7d41);
            var CopyflxOption0bc20c8a8030749 = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "35dp",
                "id": "CopyflxOption0bc20c8a8030749",
                "isVisible": true,
                "layoutType": kony.flex.FLOW_HORIZONTAL,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0dp",
                "width": "100%",
                "zIndex": 1
            }, {}, {});
            CopyflxOption0bc20c8a8030749.setDefaultUnit(kony.flex.DP);
            var CopyimgOption0b58a89219f314f = new kony.ui.Image2({
                "centerY": "50%",
                "height": "15dp",
                "id": "CopyimgOption0b58a89219f314f",
                "isVisible": true,
                "left": "15dp",
                "skin": "slImage",
                "src": "edit2x.png",
                "width": "15dp",
                "zIndex": 1
            }, {
                "imageScaleMode": constants.IMAGE_SCALE_MODE_MAINTAIN_ASPECT_RATIO,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var CopylblOption0b3d5b94677c44e = new kony.ui.Label({
                "centerY": "50%",
                "id": "CopylblOption0b3d5b94677c44e",
                "isVisible": true,
                "left": "15dp",
                "skin": "sknlblLatoBold35475f14px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.permission.Edit\")",
                "top": "4dp",
                "width": "40px",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            CopyflxOption0bc20c8a8030749.add(CopyimgOption0b58a89219f314f, CopylblOption0b3d5b94677c44e);
            var CopyflxOption0a3abb187c4704d = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "35dp",
                "id": "CopyflxOption0a3abb187c4704d",
                "isVisible": false,
                "layoutType": kony.flex.FLOW_HORIZONTAL,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0dp",
                "width": "100%",
                "zIndex": 1
            }, {}, {});
            CopyflxOption0a3abb187c4704d.setDefaultUnit(kony.flex.DP);
            var CopyimgOption0g7346328c2894d = new kony.ui.Image2({
                "centerY": "50%",
                "height": "15dp",
                "id": "CopyimgOption0g7346328c2894d",
                "isVisible": true,
                "left": "15dp",
                "skin": "slImage",
                "src": "imagedrag.png",
                "width": "15dp",
                "zIndex": 1
            }, {
                "imageScaleMode": constants.IMAGE_SCALE_MODE_MAINTAIN_ASPECT_RATIO,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var CopylblOption0e4aa6e320a224b = new kony.ui.Label({
                "centerY": "50%",
                "id": "CopylblOption0e4aa6e320a224b",
                "isVisible": true,
                "left": "15dp",
                "skin": "sknlblLatoBold35475f14px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.permission.Suspend\")",
                "top": "4dp",
                "width": "60px",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            CopyflxOption0a3abb187c4704d.add(CopyimgOption0g7346328c2894d, CopylblOption0e4aa6e320a224b);
            var CopyflxOption0e8d2e0a8b51446 = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "35dp",
                "id": "CopyflxOption0e8d2e0a8b51446",
                "isVisible": true,
                "layoutType": kony.flex.FLOW_HORIZONTAL,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0dp",
                "width": "100%",
                "zIndex": 1
            }, {}, {});
            CopyflxOption0e8d2e0a8b51446.setDefaultUnit(kony.flex.DP);
            var CopyimgOption0j2dc3599ac654d = new kony.ui.Image2({
                "centerY": "50%",
                "height": "15dp",
                "id": "CopyimgOption0j2dc3599ac654d",
                "isVisible": true,
                "left": "15dp",
                "skin": "slImage",
                "src": "deactive_2x.png",
                "width": "15dp",
                "zIndex": 1
            }, {
                "imageScaleMode": constants.IMAGE_SCALE_MODE_MAINTAIN_ASPECT_RATIO,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var CopylblOption0d9aafa510e7f4c = new kony.ui.Label({
                "centerY": "50%",
                "id": "CopylblOption0d9aafa510e7f4c",
                "isVisible": true,
                "left": "15dp",
                "skin": "sknlblLatoBold35475f14px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.permission.Deactivate\")",
                "top": "4dp",
                "width": "70px",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            CopyflxOption0e8d2e0a8b51446.add(CopyimgOption0j2dc3599ac654d, CopylblOption0d9aafa510e7f4c);
            flexOptions.add(CopylblDescription0a252259b5c5b47, CopylblRoles0f4ed925d516743, CopylblPremissions0f8ff66e806ad46, CopyflxSeperator0f47be42d7a5845, CopyflxOption0f482b0cd04b74b, CopyflxOption0bc20c8a8030749, CopyflxOption0a3abb187c4704d, CopyflxOption0e8d2e0a8b51446);
            flxSegmentUsers1.add(segusers1, flexOptions);
            flxSearch1.add(flxSearchHeading, flxSegmentUsers1);
            flxAdvansedSearch.add(flxSearch1);
            flxScrollMainContent.add(flxPermissions, flxViews, flxAdvansedSearch);
            flxRightPanel.add(flxMainHeader, flxUsersBreadCrumb, flxMainSubHeader, flxScrollMainContent);
            var flxHeaderDropdown = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                "clipBounds": true,
                "id": "flxHeaderDropdown",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "isModalContainer": false,
                "right": "35dp",
                "skin": "slFbox",
                "top": "20dp",
                "width": "400dp",
                "zIndex": 5
            }, {}, {});
            flxHeaderDropdown.setDefaultUnit(kony.flex.DP);
            var dropdownMainHeader = new com.adminConsole.common.dropdownMainHeader({
                "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
                "id": "dropdownMainHeader",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "masterType": constants.MASTER_TYPE_DEFAULT,
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0dp",
                "width": "100%",
                "overrides": {
                    "flxDropdown": {
                        "isVisible": false
                    },
                    "imgUpArrow": {
                        "src": "uparrow_2x.png"
                    }
                }
            }, {
                "overrides": {}
            }, {
                "overrides": {}
            });
            flxHeaderDropdown.add(dropdownMainHeader);
            var flxToastMessage = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "bottom": "0px",
                "clipBounds": true,
                "height": "70px",
                "id": "flxToastMessage",
                "isVisible": false,
                "layoutType": kony.flex.FREE_FORM,
                "left": "305px",
                "isModalContainer": false,
                "right": "0px",
                "skin": "slFbox",
                "zIndex": 1
            }, {}, {});
            flxToastMessage.setDefaultUnit(kony.flex.DP);
            var flxToastContainer = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "centerX": "50%",
                "centerY": "50%",
                "clipBounds": true,
                "height": "45px",
                "id": "flxToastContainer",
                "isVisible": false,
                "layoutType": kony.flex.FREE_FORM,
                "isModalContainer": false,
                "skin": "sknflxSuccessToast1F844D",
                "width": "60%",
                "zIndex": 1
            }, {}, {});
            flxToastContainer.setDefaultUnit(kony.flex.DP);
            var imgLeft = new kony.ui.Image2({
                "centerY": "50%",
                "height": "15px",
                "id": "imgLeft",
                "isVisible": true,
                "left": "15px",
                "skin": "slImage",
                "src": "arrow2x.png",
                "width": "15dp",
                "zIndex": 1
            }, {
                "imageScaleMode": constants.IMAGE_SCALE_MODE_MAINTAIN_ASPECT_RATIO,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var lbltoastMessage = new kony.ui.Label({
                "centerY": "50%",
                "id": "lbltoastMessage",
                "isVisible": true,
                "left": "45px",
                "right": 45,
                "skin": "lblfffffflatoregular14px",
                "i18n_text": "kony.i18n.getLocalizedString(\"i18n.permission.SuccessfullyDeactivated\")",
                "zIndex": 1
            }, {
                "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            var imgRight = new kony.ui.Image2({
                "centerY": "50%",
                "height": "15px",
                "id": "imgRight",
                "isVisible": true,
                "right": "15px",
                "skin": "slImage",
                "src": "close_small2x.png",
                "width": "15px",
                "zIndex": 1
            }, {
                "imageScaleMode": constants.IMAGE_SCALE_MODE_MAINTAIN_ASPECT_RATIO,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxToastContainer.add(imgLeft, lbltoastMessage, imgRight);
            var toastMessage = new com.adminConsole.common.toastMessage({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "height": "70px",
                "id": "toastMessage",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "masterType": constants.MASTER_TYPE_DEFAULT,
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0dp",
                "width": "100%",
                "overrides": {}
            }, {
                "overrides": {}
            }, {
                "overrides": {}
            });
            flxToastMessage.add(flxToastContainer, toastMessage);
            var flxUpdatedDateRangePicker = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "100%",
                "id": "flxUpdatedDateRangePicker",
                "isVisible": false,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0dp",
                "width": "100%",
                "zIndex": 5
            }, {}, {});
            flxUpdatedDateRangePicker.setDefaultUnit(kony.flex.DP);
            var updatedDateRangePicker = new com.adminConsole.advanceSearch.dateRangePicker({
                "id": "updatedDateRangePicker",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "masterType": constants.MASTER_TYPE_DEFAULT,
                "isModalContainer": false,
                "skin": "sknflxBg000000Op50",
                "top": "0dp",
                "zIndex": 1,
                "overrides": {}
            }, {
                "overrides": {}
            }, {
                "overrides": {}
            });
            flxUpdatedDateRangePicker.add(updatedDateRangePicker);
            var flxCreatedDateRangePicker = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "100%",
                "id": "flxCreatedDateRangePicker",
                "isVisible": false,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0dp",
                "width": "100%",
                "zIndex": 5
            }, {}, {});
            flxCreatedDateRangePicker.setDefaultUnit(kony.flex.DP);
            var createdDateRangePicker = new com.adminConsole.advanceSearch.dateRangePicker({
                "id": "createdDateRangePicker",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "masterType": constants.MASTER_TYPE_DEFAULT,
                "isModalContainer": false,
                "skin": "sknflxBg000000Op50",
                "top": "0dp",
                "zIndex": 1,
                "overrides": {}
            }, {
                "overrides": {}
            }, {
                "overrides": {}
            });
            flxCreatedDateRangePicker.add(createdDateRangePicker);
            var flxLoading = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "100%",
                "id": "flxLoading",
                "isVisible": false,
                "layoutType": kony.flex.FREE_FORM,
                "left": "305px",
                "isModalContainer": false,
                "right": "0px",
                "skin": "sknLoadingBlur",
                "top": "0dp",
                "zIndex": 10
            }, {}, {});
            flxLoading.setDefaultUnit(kony.flex.DP);
            var flxImageContainer = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "centerX": "50%",
                "centerY": "50%",
                "clipBounds": true,
                "height": "75px",
                "id": "flxImageContainer",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "isModalContainer": false,
                "skin": "slFbox",
                "width": "75px",
                "zIndex": 1
            }, {}, {});
            flxImageContainer.setDefaultUnit(kony.flex.DP);
            var imgLoading = new kony.ui.Image2({
                "centerX": "50%",
                "centerY": "50%",
                "height": "50px",
                "id": "imgLoading",
                "isVisible": true,
                "skin": "slImage",
                "src": "loadingscreenimage.gif",
                "width": "50px",
                "zIndex": 1
            }, {
                "imageScaleMode": constants.IMAGE_SCALE_MODE_MAINTAIN_ASPECT_RATIO,
                "padding": [0, 0, 0, 0],
                "paddingInPixel": false
            }, {});
            flxImageContainer.add(imgLoading);
            flxLoading.add(flxImageContainer);
            flxMain.add(flxLeftPannel, flxRightPanel, flxHeaderDropdown, flxToastMessage, flxUpdatedDateRangePicker, flxCreatedDateRangePicker, flxLoading);
            var flxDeactivateUser = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "100%",
                "id": "flxDeactivateUser",
                "isVisible": false,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "masterType": constants.MASTER_TYPE_USERWIDGET,
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "0dp",
                "width": "100%",
                "zIndex": 5
            }, {}, {});
            flxDeactivateUser.setDefaultUnit(kony.flex.DP);
            var popUp = new com.adminConsole.common.popUp1({
                "height": "100%",
                "id": "popUp",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "masterType": constants.MASTER_TYPE_DEFAULT,
                "isModalContainer": false,
                "skin": "sknflxBg000000Op50",
                "top": "0dp",
                "width": "100%",
                "zIndex": 1,
                "overrides": {
                    "btnPopUpCancel": {
                        "i18n_text": "kony.i18n.getLocalizedString(\"i18n.PopUp.NoLeaveAsIS\")",
                        "minWidth": 81,
                        "right": "20px",
                        "top": "viz.val_cleared"
                    },
                    "btnPopUpDelete": {
                        "i18n_text": "kony.i18n.getLocalizedString(\"i18n.PopUp.YesDeactivate\")",
                        "minWidth": "83px"
                    },
                    "lblPopUpMainMessage": {
                        "text": "Deactivate User"
                    },
                    "rtxPopUpDisclaimer": {
                        "right": "viz.val_cleared",
                        "text": "Are you sure to deactivate the user \"John Doe\"?<br><br>\nThe User has been assigned to a role. Deactivating this may impact the user’s ability to perform certain actions",
                        "width": "93%"
                    }
                }
            }, {
                "overrides": {}
            }, {
                "overrides": {}
            });
            flxDeactivateUser.add(popUp);
            var flxEditCancelConfirmation = new kony.ui.FlexContainer({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "clipBounds": true,
                "height": "100%",
                "id": "flxEditCancelConfirmation",
                "isVisible": false,
                "layoutType": kony.flex.FREE_FORM,
                "left": "10dp",
                "masterType": constants.MASTER_TYPE_USERWIDGET,
                "isModalContainer": false,
                "skin": "slFbox",
                "top": "10dp",
                "width": "100%",
                "zIndex": 5
            }, {}, {});
            flxEditCancelConfirmation.setDefaultUnit(kony.flex.DP);
            var popUpCancelEdits = new com.adminConsole.common.popUpCancelEdits({
                "autogrowMode": kony.flex.AUTOGROW_NONE,
                "height": "100%",
                "id": "popUpCancelEdits",
                "isVisible": true,
                "layoutType": kony.flex.FREE_FORM,
                "left": "0dp",
                "masterType": constants.MASTER_TYPE_DEFAULT,
                "isModalContainer": false,
                "skin": "sknflxBg000000Op50",
                "top": "0dp",
                "width": "100%",
                "zIndex": 1,
                "overrides": {
                    "btnPopUpCancel": {
                        "i18n_text": "kony.i18n.getLocalizedString(\"i8n.navigation.cancelButton\")"
                    },
                    "btnPopUpDelete": {
                        "i18n_text": "kony.i18n.getLocalizedString(\"i8n.navigation.okButton\")"
                    },
                    "lblPopUpMainMessage": {
                        "i18n_text": "kony.i18n.getLocalizedString(\"i8n.navigation.cancelChanges\")"
                    },
                    "popUpCancelEdits": {
                        "bottom": "viz.val_cleared",
                        "centerX": "viz.val_cleared",
                        "centerY": "viz.val_cleared",
                        "height": "100%",
                        "left": "0dp",
                        "maxHeight": "viz.val_cleared",
                        "maxWidth": "viz.val_cleared",
                        "minHeight": "viz.val_cleared",
                        "minWidth": "viz.val_cleared",
                        "right": "viz.val_cleared",
                        "top": "0dp",
                        "width": "100%"
                    },
                    "rtxPopUpDisclaimer": {
                        "i18n_text": "kony.i18n.getLocalizedString(\"i8n.navigation.cancelChangesBody\")"
                    }
                }
            }, {
                "overrides": {}
            }, {
                "overrides": {}
            });
            flxEditCancelConfirmation.add(popUpCancelEdits);
            this.add(tbxDummy, flxMain, flxDeactivateUser, flxEditCancelConfirmation);
        };
        return [{
            "addWidgets": addWidgetsfrmUsers,
            "enabledForIdleTimeout": true,
            "id": "frmUsers",
            "layoutType": kony.flex.FREE_FORM,
            "needAppMenu": false,
            "postShow": controller.AS_Form_eb75f3eb470547b08e93b3222dde2f8a,
            "preShow": function(eventobject) {
                controller.AS_Form_c4f980b971b640f3a662511ecd6172c4(eventobject);
            },
            "skin": "slForm"
        }, {
            "displayOrientation": constants.FORM_DISPLAY_ORIENTATION_PORTRAIT,
            "layoutType": kony.flex.FREE_FORM,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, {
            "onDeviceBack": controller.AS_Form_b2402e4f26d848488405ed143b3b2afc,
            "retainScrollPosition": false
        }]
    }
});