define("flxDetailsDefaultURLs", function() {
    return function(controller) {
        var flxDetailsDefaultURLs = new kony.ui.FlexContainer({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "clipBounds": true,
            "height": "46px",
            "id": "flxDetailsDefaultURLs",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "left": "0dp",
            "isModalContainer": false,
            "skin": "sknBackgroundFFFFFF",
            "top": "0dp",
            "width": "100%"
        }, {}, {});
        flxDetailsDefaultURLs.setDefaultUnit(kony.flex.DP);
        var flxDetailsURLsParent = new kony.ui.FlexContainer({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "clipBounds": true,
            "height": "100%",
            "id": "flxDetailsURLsParent",
            "isVisible": true,
            "layoutType": kony.flex.FLOW_HORIZONTAL,
            "left": "20px",
            "isModalContainer": false,
            "right": "20px",
            "skin": "slFbox",
            "top": "0dp"
        }, {}, {});
        flxDetailsURLsParent.setDefaultUnit(kony.flex.DP);
        var flxResolution = new kony.ui.FlexContainer({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "clipBounds": true,
            "height": "16px",
            "id": "flxResolution",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "left": "0%",
            "isModalContainer": false,
            "skin": "slFbox",
            "top": "17px",
            "width": "18%",
            "zIndex": 1
        }, {}, {});
        flxResolution.setDefaultUnit(kony.flex.DP);
        var lblResolution = new kony.ui.Label({
            "height": "100%",
            "id": "lblResolution",
            "isVisible": true,
            "left": "0px",
            "skin": "sknlbl333333LatoSemiBold13px",
            "text": "Label",
            "top": "0px",
            "width": "100%",
            "zIndex": 1
        }, {
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, {});
        flxResolution.add(lblResolution);
        var flxImageContainerNameScale = new kony.ui.FlexContainer({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "clipBounds": true,
            "height": "16px",
            "id": "flxImageContainerNameScale",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "left": "2%",
            "isModalContainer": false,
            "skin": "slFbox",
            "top": "17px",
            "width": "20%",
            "zIndex": 1
        }, {}, {});
        flxImageContainerNameScale.setDefaultUnit(kony.flex.DP);
        var lblImageContainerNameScale = new kony.ui.Label({
            "height": "100%",
            "id": "lblImageContainerNameScale",
            "isVisible": true,
            "left": "0px",
            "skin": "sknlbl333333LatoSemiBold13px",
            "text": "Label",
            "top": "0px",
            "width": "100%",
            "zIndex": 1
        }, {
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, {});
        flxImageContainerNameScale.add(lblImageContainerNameScale);
        var flxImgSourceURL = new kony.ui.FlexContainer({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "clipBounds": true,
            "height": "16px",
            "id": "flxImgSourceURL",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "left": "2%",
            "isModalContainer": false,
            "skin": "sknFlxffffffCursorPointer",
            "top": "17px",
            "width": "28%",
            "zIndex": 1
        }, {}, {});
        flxImgSourceURL.setDefaultUnit(kony.flex.DP);
        var lblImgSourceURL = new kony.ui.Label({
            "height": "100%",
            "id": "lblImgSourceURL",
            "isVisible": true,
            "left": "0px",
            "skin": "sknLblLato13px117eb0",
            "text": "Label",
            "top": "0px",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, {
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, {});
        flxImgSourceURL.add(lblImgSourceURL);
        var flxImgTargetURL = new kony.ui.FlexContainer({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "clipBounds": true,
            "height": "16px",
            "id": "flxImgTargetURL",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "left": "2%",
            "isModalContainer": false,
            "skin": "sknFlxffffffCursorPointer",
            "top": "17px",
            "width": "28%",
            "zIndex": 1
        }, {}, {});
        flxImgTargetURL.setDefaultUnit(kony.flex.DP);
        var lblTargetURL = new kony.ui.Label({
            "height": "100%",
            "id": "lblTargetURL",
            "isVisible": true,
            "left": "0px",
            "skin": "sknLblLato13px117eb0",
            "text": "Label",
            "top": "0px",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, {
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, {});
        flxImgTargetURL.add(lblTargetURL);
        flxDetailsURLsParent.add(flxResolution, flxImageContainerNameScale, flxImgSourceURL, flxImgTargetURL);
        flxDetailsDefaultURLs.add(flxDetailsURLsParent);
        return flxDetailsDefaultURLs;
    }
})