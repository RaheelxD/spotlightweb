define("flxApprovalMatrixActionHeader", function() {
    return function(controller) {
        var flxApprovalMatrixActionHeader = new kony.ui.FlexContainer({
            "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
            "clipBounds": true,
            "id": "flxApprovalMatrixActionHeader",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "left": "0dp",
            "isModalContainer": false,
            "skin": "sknFlxFFFFFF1000",
            "top": "0dp",
            "width": "100%"
        }, {}, {});
        flxApprovalMatrixActionHeader.setDefaultUnit(kony.flex.DP);
        var lblFASeperatorTop = new kony.ui.Label({
            "height": "1px",
            "id": "lblFASeperatorTop",
            "isVisible": true,
            "left": "0px",
            "right": "0dp",
            "skin": "sknLblSeparatore7e7e7",
            "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmCSR.lblSeperator\")",
            "top": "0dp",
            "zIndex": 3
        }, {
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, {});
        var flxActionName = new kony.ui.FlexContainer({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "clipBounds": true,
            "height": "40px",
            "id": "flxActionName",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "left": "0dp",
            "isModalContainer": false,
            "skin": "sknflxf5f6f8Op100",
            "top": "0dp",
            "width": kony.flex.USE_PREFFERED_SIZE
        }, {}, {});
        flxActionName.setDefaultUnit(kony.flex.DP);
        var lblIconAction = new kony.ui.Label({
            "centerY": "50%",
            "height": "15dp",
            "id": "lblIconAction",
            "isVisible": true,
            "left": "57dp",
            "skin": "sknFontIcon485C7518px",
            "text": "",
            "width": "18dp"
        }, {
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, {});
        var lblActionName = new kony.ui.Label({
            "centerY": "50%",
            "id": "lblActionName",
            "isVisible": true,
            "left": "80dp",
            "skin": "sknLbl192B45LatoRegular14px",
            "text": "Create Bill Pay",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, {
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, {});
        var lbxTransactionType = new kony.ui.ListBox({
            "centerY": "50%",
            "height": "20dp",
            "id": "lbxTransactionType",
            "isVisible": true,
            "masterData": [
                ["lb1", "Placeholder One"],
                ["lb2", "Placeholder Two"],
                ["lb3", "Placeholder Three"]
            ],
            "right": "10dp",
            "skin": "sknlbx0069cd12px",
            "width": "125dp",
            "zIndex": 1
        }, {
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [1, 0, 0, 0],
            "paddingInPixel": false
        }, {
            "multiSelect": false
        });
        flxActionName.add(lblIconAction, lblActionName, lbxTransactionType);
        var flxViewApprovalsHeader = new kony.ui.FlexContainer({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "clipBounds": true,
            "height": "55dp",
            "id": "flxViewApprovalsHeader",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "left": "20px",
            "isModalContainer": false,
            "right": "20px",
            "skin": "slFbox",
            "top": "40px"
        }, {}, {});
        flxViewApprovalsHeader.setDefaultUnit(kony.flex.DP);
        var lblRangeHeader = new kony.ui.Label({
            "id": "lblRangeHeader",
            "isVisible": true,
            "left": "40dp",
            "skin": "sknlblLato696c7312px",
            "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmComapnies.RANGE\")",
            "top": "15dp",
            "width": "10%",
            "zIndex": 1
        }, {
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, {
            "hoverSkin": "sknlblLato00000011px"
        });
        var lblApprovalRequiredHeader = new kony.ui.Label({
            "id": "lblApprovalRequiredHeader",
            "isVisible": true,
            "left": "40%",
            "skin": "sknlblLato696c7312px",
            "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmComapnies.APPROVAL_REQUIRED\")",
            "top": "15dp",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, {
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, {});
        var lblApproversHeader = new kony.ui.Label({
            "id": "lblApproversHeader",
            "isVisible": true,
            "left": "75%",
            "skin": "sknlblLato696c7312px",
            "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmComapnies.APPROVERS\")",
            "top": "15dp",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, {
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, {});
        var lblFASeperator2 = new kony.ui.Label({
            "bottom": "10dp",
            "height": "1px",
            "id": "lblFASeperator2",
            "isVisible": true,
            "left": "0px",
            "skin": "sknLblSeparatore7e7e7",
            "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmCSR.lblSeperator\")",
            "width": "100%",
            "zIndex": 1
        }, {
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, {});
        flxViewApprovalsHeader.add(lblRangeHeader, lblApprovalRequiredHeader, lblApproversHeader, lblFASeperator2);
        var lblFASeperator1 = new kony.ui.Label({
            "height": "1px",
            "id": "lblFASeperator1",
            "isVisible": true,
            "left": "0px",
            "right": "0dp",
            "skin": "sknLblSeparatore7e7e7",
            "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmCSR.lblSeperator\")",
            "top": "40dp",
            "zIndex": 1
        }, {
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, {});
        flxApprovalMatrixActionHeader.add(lblFASeperatorTop, flxActionName, flxViewApprovalsHeader, lblFASeperator1);
        return flxApprovalMatrixActionHeader;
    }
})