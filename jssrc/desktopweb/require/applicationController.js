define({
    /*
        This is an auto generated file and any modifications to it may result in corruption of the action sequence.
    */
    AS_AppEvents_d9f275df03bb4f2c9c87f0aca8dac598: function AS_AppEvents_d9f275df03bb4f2c9c87f0aca8dac598(eventobject) {
        var self = this;
        var qpExists = window.location.href.indexOf("qp=");
        if (qpExists !== -1) {
            return "frmErrorLogin";
        } else {
            return "frmLogin";
        }
    },
    AS_AppEvents_hf8e5e479c1348dc9f9ed8e64ecde98e: function AS_AppEvents_hf8e5e479c1348dc9f9ed8e64ecde98e(eventobject) {
        var self = this;
        loadCommonFiles();
        var authModule = kony.mvc.MDAApplication.getSharedInstance().getModuleManager().getModule("AuthModule");
        clearCache();
    },
    appInit: function(params) {
        skinsInit();
        kony.mvc.registry.add("com.adminConsole.adManagement.attribute", "attribute", "attributeController");
        kony.application.registerMaster({
            "namespace": "com.adminConsole.adManagement",
            "classname": "attribute",
            "name": "com.adminConsole.adManagement.attribute"
        });
        kony.mvc.registry.add("com.adminConsole.adManagement.attributesContainer", "attributesContainer", "attributesContainerController");
        kony.application.registerMaster({
            "namespace": "com.adminConsole.adManagement",
            "classname": "attributesContainer",
            "name": "com.adminConsole.adManagement.attributesContainer"
        });
        kony.mvc.registry.add("com.adminConsole.adManagement.campaignPopupMobile", "campaignPopupMobile", "campaignPopupMobileController");
        kony.application.registerMaster({
            "namespace": "com.adminConsole.adManagement",
            "classname": "campaignPopupMobile",
            "name": "com.adminConsole.adManagement.campaignPopupMobile"
        });
        kony.mvc.registry.add("com.adminConsole.adManagement.campaignPopupWeb", "campaignPopupWeb", "campaignPopupWebController");
        kony.application.registerMaster({
            "namespace": "com.adminConsole.adManagement",
            "classname": "campaignPopupWeb",
            "name": "com.adminConsole.adManagement.campaignPopupWeb"
        });
        kony.mvc.registry.add("com.adminConsole.adManagement.channelScreen", "channelScreen", "channelScreenController");
        kony.application.registerMaster({
            "namespace": "com.adminConsole.adManagement",
            "classname": "channelScreen",
            "name": "com.adminConsole.adManagement.channelScreen"
        });
        kony.mvc.registry.add("com.adminConsole.adManagement.channelTemplate", "channelTemplate", "channelTemplateController");
        kony.application.registerMaster({
            "namespace": "com.adminConsole.adManagement",
            "classname": "channelTemplate",
            "name": "com.adminConsole.adManagement.channelTemplate"
        });
        kony.mvc.registry.add("com.adminConsole.adManagement.dataset", "dataset", "datasetController");
        kony.application.registerMaster({
            "namespace": "com.adminConsole.adManagement",
            "classname": "dataset",
            "name": "com.adminConsole.adManagement.dataset"
        });
        kony.mvc.registry.add("com.adminConsole.adManagement.defaultImageCount", "defaultImageCount", "defaultImageCountController");
        kony.application.registerMaster({
            "namespace": "com.adminConsole.adManagement",
            "classname": "defaultImageCount",
            "name": "com.adminConsole.adManagement.defaultImageCount"
        });
        kony.mvc.registry.add("com.adminConsole.adManagement.postLoginSettingspopup", "postLoginSettingspopup", "postLoginSettingspopupController");
        kony.application.registerMaster({
            "namespace": "com.adminConsole.adManagement",
            "classname": "postLoginSettingspopup",
            "name": "com.adminConsole.adManagement.postLoginSettingspopup"
        });
        kony.mvc.registry.add("com.adminConsole.adManagement.selectOptions", "selectOptions", "selectOptionsController");
        kony.application.registerMaster({
            "namespace": "com.adminConsole.adManagement",
            "classname": "selectOptions",
            "name": "com.adminConsole.adManagement.selectOptions"
        });
        kony.mvc.registry.add("com.adminConsole.adManagement.verticalTabs", "verticalTabs", "verticalTabsController");
        kony.application.registerMaster({
            "namespace": "com.adminConsole.adManagement",
            "classname": "verticalTabs",
            "name": "com.adminConsole.adManagement.verticalTabs"
        });
        kony.mvc.registry.add("com.adminConsole.adManagement.viewInternalExternalEvents", "viewInternalExternalEvents", "viewInternalExternalEventsController");
        kony.application.registerMaster({
            "namespace": "com.adminConsole.adManagement",
            "classname": "viewInternalExternalEvents",
            "name": "com.adminConsole.adManagement.viewInternalExternalEvents"
        });
        kony.mvc.registry.add("com.adminConsole.adManagement.viewOfflineChannelDetails", "viewOfflineChannelDetails", "viewOfflineChannelDetailsController");
        kony.application.registerMaster({
            "namespace": "com.adminConsole.adManagement",
            "classname": "viewOfflineChannelDetails",
            "name": "com.adminConsole.adManagement.viewOfflineChannelDetails"
        });
        kony.mvc.registry.add("com.adminConsole.adManagement.viewOnlineChannelDetails", "viewOnlineChannelDetails", "viewOnlineChannelDetailsController");
        kony.application.registerMaster({
            "namespace": "com.adminConsole.adManagement",
            "classname": "viewOnlineChannelDetails",
            "name": "com.adminConsole.adManagement.viewOnlineChannelDetails"
        });
        kony.mvc.registry.add("com.adminConsole.adManagement.viewPopupAdDetails", "viewPopupAdDetails", "viewPopupAdDetailsController");
        kony.application.registerMaster({
            "namespace": "com.adminConsole.adManagement",
            "classname": "viewPopupAdDetails",
            "name": "com.adminConsole.adManagement.viewPopupAdDetails"
        });
        kony.mvc.registry.add("com.adminConsole.adManagement.viewPopupEvents", "viewPopupEvents", "viewPopupEventsController");
        kony.application.registerMaster({
            "namespace": "com.adminConsole.adManagement",
            "classname": "viewPopupEvents",
            "name": "com.adminConsole.adManagement.viewPopupEvents"
        });
        kony.mvc.registry.add("com.adminConsole.advanceSearch.dateRangePicker", "dateRangePicker", "dateRangePickerController");
        kony.application.registerMaster({
            "namespace": "com.adminConsole.advanceSearch",
            "classname": "dateRangePicker",
            "name": "com.adminConsole.advanceSearch.dateRangePicker"
        });
        kony.mvc.registry.add("com.adminConsole.alertMang.alertMessage", "alertMessage", "alertMessageController");
        kony.application.registerMaster({
            "namespace": "com.adminConsole.alertMang",
            "classname": "alertMessage",
            "name": "com.adminConsole.alertMang.alertMessage"
        });
        kony.mvc.registry.add("com.adminConsole.alerts.alertCategoryCard", "alertCategoryCard", "alertCategoryCardController");
        kony.application.registerMaster({
            "namespace": "com.adminConsole.alerts",
            "classname": "alertCategoryCard",
            "name": "com.adminConsole.alerts.alertCategoryCard"
        });
        kony.mvc.registry.add("com.adminConsole.alerts.customListbox", "customListbox", "customListboxController");
        kony.application.registerMaster({
            "namespace": "com.adminConsole.alerts",
            "classname": "customListbox",
            "name": "com.adminConsole.alerts.customListbox"
        });
        kony.mvc.registry.add("com.adminConsole.alerts.supportedChannel", "supportedChannel", "supportedChannelController");
        kony.application.registerMaster({
            "namespace": "com.adminConsole.alerts",
            "classname": "supportedChannel",
            "name": "com.adminConsole.alerts.supportedChannel"
        });
        kony.mvc.registry.add("com.adminConsole.alerts.ViewTemplate", "ViewTemplate", "ViewTemplateController");
        kony.application.registerMaster({
            "namespace": "com.adminConsole.alerts",
            "classname": "ViewTemplate",
            "name": "com.adminConsole.alerts.ViewTemplate"
        });
        kony.mvc.registry.add("com.adminConsole.authorizedSignatories.accountCentricAccounts", "accountCentricAccounts", "accountCentricAccountsController");
        kony.application.registerMaster({
            "namespace": "com.adminConsole.authorizedSignatories",
            "classname": "accountCentricAccounts",
            "name": "com.adminConsole.authorizedSignatories.accountCentricAccounts"
        });
        kony.mvc.registry.add("com.adminConsole.BusinessConfigurations.configLeftMenu", "configLeftMenu", "configLeftMenuController");
        kony.application.registerMaster({
            "namespace": "com.adminConsole.BusinessConfigurations",
            "classname": "configLeftMenu",
            "name": "com.adminConsole.BusinessConfigurations.configLeftMenu"
        });
        kony.mvc.registry.add("com.adminConsole.BusinessConfigurations.criteriaList", "criteriaList", "criteriaListController");
        kony.application.registerMaster({
            "namespace": "com.adminConsole.BusinessConfigurations",
            "classname": "criteriaList",
            "name": "com.adminConsole.BusinessConfigurations.criteriaList"
        });
        kony.mvc.registry.add("com.adminConsole.businessTypes.addBusinessTypes", "addBusinessTypes", "addBusinessTypesController");
        kony.application.registerMaster({
            "namespace": "com.adminConsole.businessTypes",
            "classname": "addBusinessTypes",
            "name": "com.adminConsole.businessTypes.addBusinessTypes"
        });
        kony.mvc.registry.add("com.adminConsole.cdd.infoIconComponent", "infoIconComponent", "infoIconComponentController");
        kony.application.registerMaster({
            "namespace": "com.adminConsole.cdd",
            "classname": "infoIconComponent",
            "name": "com.adminConsole.cdd.infoIconComponent"
        });
        kony.mvc.registry.add("com.adminConsole.common.addAndRemoveOptions", "addAndRemoveOptions", "addAndRemoveOptionsController");
        kony.application.registerMaster({
            "namespace": "com.adminConsole.common",
            "classname": "addAndRemoveOptions",
            "name": "com.adminConsole.common.addAndRemoveOptions"
        });
        kony.mvc.registry.add("com.adminConsole.common.addAndRemoveOptions1", "addAndRemoveOptions1", "addAndRemoveOptions1Controller");
        kony.application.registerMaster({
            "namespace": "com.adminConsole.common",
            "classname": "addAndRemoveOptions1",
            "name": "com.adminConsole.common.addAndRemoveOptions1"
        });
        kony.mvc.registry.add("com.adminConsole.common.addNewRow", "addNewRow", "addNewRowController");
        kony.application.registerMaster({
            "namespace": "com.adminConsole.common",
            "classname": "addNewRow",
            "name": "com.adminConsole.common.addNewRow"
        });
        kony.mvc.registry.add("com.adminConsole.common.assignLimitsContainer", "assignLimitsContainer", "assignLimitsContainerController");
        kony.application.registerMaster({
            "namespace": "com.adminConsole.common",
            "classname": "assignLimitsContainer",
            "name": "com.adminConsole.common.assignLimitsContainer"
        });
        kony.mvc.registry.add("com.adminConsole.common.breadcrumbs", "breadcrumbs", "breadcrumbsController");
        kony.application.registerMaster({
            "namespace": "com.adminConsole.common",
            "classname": "breadcrumbs",
            "name": "com.adminConsole.common.breadcrumbs"
        });
        kony.mvc.registry.add("com.adminConsole.common.breadcrumbs1", "breadcrumbs1", "breadcrumbs1Controller");
        kony.application.registerMaster({
            "namespace": "com.adminConsole.common",
            "classname": "breadcrumbs1",
            "name": "com.adminConsole.common.breadcrumbs1"
        });
        kony.mvc.registry.add("com.adminConsole.common.commonButtons", "commonButtons", "commonButtonsController");
        kony.application.registerMaster({
            "namespace": "com.adminConsole.common",
            "classname": "commonButtons",
            "name": "com.adminConsole.common.commonButtons"
        });
        kony.mvc.registry.add("com.adminConsole.common.contactNumber", "contactNumber", "contactNumberController");
        kony.application.registerMaster({
            "namespace": "com.adminConsole.common",
            "classname": "contactNumber",
            "name": "com.adminConsole.common.contactNumber"
        });
        kony.mvc.registry.add("com.adminConsole.common.contextualMenu", "contextualMenu", "contextualMenuController");
        kony.application.registerMaster({
            "namespace": "com.adminConsole.common",
            "classname": "contextualMenu",
            "name": "com.adminConsole.common.contextualMenu"
        });
        kony.mvc.registry.add("com.adminConsole.common.contextualMenu1", "contextualMenu1", "contextualMenu1Controller");
        kony.application.registerMaster({
            "namespace": "com.adminConsole.common",
            "classname": "contextualMenu1",
            "name": "com.adminConsole.common.contextualMenu1"
        });
        kony.mvc.registry.add("com.adminConsole.common.customRadioButtonGroup", "customRadioButtonGroup", "customRadioButtonGroupController");
        kony.application.registerMaster({
            "namespace": "com.adminConsole.common",
            "classname": "customRadioButtonGroup",
            "name": "com.adminConsole.common.customRadioButtonGroup"
        });
        kony.mvc.registry.add("com.adminConsole.common.customSwitch", "customSwitch", "customSwitchController");
        kony.application.registerMaster({
            "namespace": "com.adminConsole.common",
            "classname": "customSwitch",
            "name": "com.adminConsole.common.customSwitch"
        });
        kony.mvc.registry.add("com.adminConsole.common.dataEntry", "dataEntry", "dataEntryController");
        kony.application.registerMaster({
            "namespace": "com.adminConsole.common",
            "classname": "dataEntry",
            "name": "com.adminConsole.common.dataEntry"
        });
        kony.mvc.registry.add("com.adminConsole.common.dropdownMainHeader", "dropdownMainHeader", "dropdownMainHeaderController");
        kony.application.registerMaster({
            "namespace": "com.adminConsole.common",
            "classname": "dropdownMainHeader",
            "name": "com.adminConsole.common.dropdownMainHeader"
        });
        kony.mvc.registry.add("com.adminConsole.common.errorFlagMessage", "errorFlagMessage", "errorFlagMessageController");
        kony.application.registerMaster({
            "namespace": "com.adminConsole.common",
            "classname": "errorFlagMessage",
            "name": "com.adminConsole.common.errorFlagMessage"
        });
        kony.mvc.registry.add("com.adminConsole.common.HeaderGuest", "HeaderGuest", "HeaderGuestController");
        kony.application.registerMaster({
            "namespace": "com.adminConsole.common",
            "classname": "HeaderGuest",
            "name": "com.adminConsole.common.HeaderGuest"
        });
        kony.mvc.registry.add("com.adminConsole.common.HeaderTabs", "HeaderTabs", "HeaderTabsController");
        kony.application.registerMaster({
            "namespace": "com.adminConsole.common",
            "classname": "HeaderTabs",
            "name": "com.adminConsole.common.HeaderTabs"
        });
        kony.mvc.registry.add("com.adminConsole.common.leadContextualMenu", "leadContextualMenu", "leadContextualMenuController");
        kony.application.registerMaster({
            "namespace": "com.adminConsole.common",
            "classname": "leadContextualMenu",
            "name": "com.adminConsole.common.leadContextualMenu"
        });
        kony.mvc.registry.add("com.adminConsole.common.loadingIndicator", "loadingIndicator", "loadingIndicatorController");
        kony.application.registerMaster({
            "namespace": "com.adminConsole.common",
            "classname": "loadingIndicator",
            "name": "com.adminConsole.common.loadingIndicator"
        });
        kony.mvc.registry.add("com.adminConsole.common.Login", "Login", "LoginController");
        kony.application.registerMaster({
            "namespace": "com.adminConsole.common",
            "classname": "Login",
            "name": "com.adminConsole.common.Login"
        });
        kony.mvc.registry.add("com.adminConsole.common.noResultsWithButton", "noResultsWithButton", "noResultsWithButtonController");
        kony.application.registerMaster({
            "namespace": "com.adminConsole.common",
            "classname": "noResultsWithButton",
            "name": "com.adminConsole.common.noResultsWithButton"
        });
        kony.mvc.registry.add("com.adminConsole.common.NotificationFlagMessage", "NotificationFlagMessage", "NotificationFlagMessageController");
        kony.application.registerMaster({
            "namespace": "com.adminConsole.common",
            "classname": "NotificationFlagMessage",
            "name": "com.adminConsole.common.NotificationFlagMessage"
        });
        kony.mvc.registry.add("com.adminConsole.common.Numbers", "Numbers", "NumbersController");
        kony.application.registerMaster({
            "namespace": "com.adminConsole.common",
            "classname": "Numbers",
            "name": "com.adminConsole.common.Numbers"
        });
        kony.mvc.registry.add("com.adminConsole.common.pagination1", "pagination1", "pagination1Controller");
        kony.application.registerMaster({
            "namespace": "com.adminConsole.common",
            "classname": "pagination1",
            "name": "com.adminConsole.common.pagination1"
        });
        kony.mvc.registry.add("com.adminConsole.common.popUp", "popUp", "popUpController");
        kony.application.registerMaster({
            "namespace": "com.adminConsole.common",
            "classname": "popUp",
            "name": "com.adminConsole.common.popUp"
        });
        kony.mvc.registry.add("com.adminConsole.common.popUp1", "popUp1", "popUp1Controller");
        kony.application.registerMaster({
            "namespace": "com.adminConsole.common",
            "classname": "popUp1",
            "name": "com.adminConsole.common.popUp1"
        });
        kony.mvc.registry.add("com.adminConsole.common.popUpCancelEdits", "popUpCancelEdits", "popUpCancelEditsController");
        kony.application.registerMaster({
            "namespace": "com.adminConsole.common",
            "classname": "popUpCancelEdits",
            "name": "com.adminConsole.common.popUpCancelEdits"
        });
        kony.mvc.registry.add("com.adminConsole.common.popupError", "popupError", "popupErrorController");
        kony.application.registerMaster({
            "namespace": "com.adminConsole.common",
            "classname": "popupError",
            "name": "com.adminConsole.common.popupError"
        });
        kony.mvc.registry.add("com.adminConsole.common.productHeader", "productHeader", "productHeaderController");
        kony.application.registerMaster({
            "namespace": "com.adminConsole.common",
            "classname": "productHeader",
            "name": "com.adminConsole.common.productHeader"
        });
        kony.mvc.registry.add("com.adminConsole.common.RangeResults", "RangeResults", "RangeResultsController");
        kony.application.registerMaster({
            "namespace": "com.adminConsole.common",
            "classname": "RangeResults",
            "name": "com.adminConsole.common.RangeResults"
        });
        kony.mvc.registry.add("com.adminConsole.common.searchBox", "searchBox", "searchBoxController");
        kony.application.registerMaster({
            "namespace": "com.adminConsole.common",
            "classname": "searchBox",
            "name": "com.adminConsole.common.searchBox"
        });
        kony.mvc.registry.add("com.adminConsole.common.statusFilterMenu", "statusFilterMenu", "statusFilterMenuController");
        kony.application.registerMaster({
            "namespace": "com.adminConsole.common",
            "classname": "statusFilterMenu",
            "name": "com.adminConsole.common.statusFilterMenu"
        });
        kony.mvc.registry.add("com.adminConsole.common.tabs", "tabs", "tabsController");
        kony.application.registerMaster({
            "namespace": "com.adminConsole.common",
            "classname": "tabs",
            "name": "com.adminConsole.common.tabs"
        });
        kony.mvc.registry.add("com.adminConsole.common.textBoxEntry", "textBoxEntry", "textBoxEntryController");
        kony.application.registerMaster({
            "namespace": "com.adminConsole.common",
            "classname": "textBoxEntry",
            "name": "com.adminConsole.common.textBoxEntry"
        });
        kony.mvc.registry.add("com.adminConsole.common.textBoxEntryAsst", "textBoxEntryAsst", "textBoxEntryAsstController");
        kony.application.registerMaster({
            "namespace": "com.adminConsole.common",
            "classname": "textBoxEntryAsst",
            "name": "com.adminConsole.common.textBoxEntryAsst"
        });
        kony.mvc.registry.add("com.adminConsole.common.timePicker", "timePicker", "timePickerController");
        kony.application.registerMaster({
            "namespace": "com.adminConsole.common",
            "classname": "timePicker",
            "name": "com.adminConsole.common.timePicker"
        });
        kony.mvc.registry.add("com.adminConsole.common.toastMessage", "toastMessage", "toastMessageController");
        kony.application.registerMaster({
            "namespace": "com.adminConsole.common",
            "classname": "toastMessage",
            "name": "com.adminConsole.common.toastMessage"
        });
        kony.mvc.registry.add("com.adminConsole.common.toastMessageWithLink", "toastMessageWithLink", "toastMessageWithLinkController");
        kony.application.registerMaster({
            "namespace": "com.adminConsole.common",
            "classname": "toastMessageWithLink",
            "name": "com.adminConsole.common.toastMessageWithLink"
        });
        kony.mvc.registry.add("com.adminConsole.common.toastMessageWithWarning", "toastMessageWithWarning", "toastMessageWithWarningController");
        kony.application.registerMaster({
            "namespace": "com.adminConsole.common",
            "classname": "toastMessageWithWarning",
            "name": "com.adminConsole.common.toastMessageWithWarning"
        });
        kony.mvc.registry.add("com.adminConsole.common.toggleButtons", "toggleButtons", "toggleButtonsController");
        kony.application.registerMaster({
            "namespace": "com.adminConsole.common",
            "classname": "toggleButtons",
            "name": "com.adminConsole.common.toggleButtons"
        });
        kony.mvc.registry.add("com.adminConsole.common.tooltip", "tooltip", "tooltipController");
        kony.application.registerMaster({
            "namespace": "com.adminConsole.common",
            "classname": "tooltip",
            "name": "com.adminConsole.common.tooltip"
        });
        kony.mvc.registry.add("com.adminConsole.common.typeHead", "typeHead", "typeHeadController");
        kony.application.registerMaster({
            "namespace": "com.adminConsole.common",
            "classname": "typeHead",
            "name": "com.adminConsole.common.typeHead"
        });
        kony.mvc.registry.add("com.adminConsole.common.valueSelector", "valueSelector", "valueSelectorController");
        kony.application.registerMaster({
            "namespace": "com.adminConsole.common",
            "classname": "valueSelector",
            "name": "com.adminConsole.common.valueSelector"
        });
        kony.mvc.registry.add("com.adminConsole.common.variableReferencesMenu", "variableReferencesMenu", "variableReferencesMenuController");
        kony.application.registerMaster({
            "namespace": "com.adminConsole.common",
            "classname": "variableReferencesMenu",
            "name": "com.adminConsole.common.variableReferencesMenu"
        });
        kony.mvc.registry.add("com.adminConsole.common.verticalTabs", "verticalTabs", "verticalTabsController");
        kony.application.registerMaster({
            "namespace": "com.adminConsole.common",
            "classname": "verticalTabs",
            "name": "com.adminConsole.common.verticalTabs"
        });
        kony.mvc.registry.add("com.adminConsole.common.verticalTabs1", "verticalTabs1", "verticalTabs1Controller");
        kony.application.registerMaster({
            "namespace": "com.adminConsole.common",
            "classname": "verticalTabs1",
            "name": "com.adminConsole.common.verticalTabs1"
        });
        kony.mvc.registry.add("com.adminConsole.commonTabsDashboard.dashboardCommonTab", "dashboardCommonTab", "dashboardCommonTabController");
        kony.application.registerMaster({
            "namespace": "com.adminConsole.commonTabsDashboard",
            "classname": "dashboardCommonTab",
            "name": "com.adminConsole.commonTabsDashboard.dashboardCommonTab"
        });
        kony.mvc.registry.add("com.adminConsole.companies.featureLimitsTemplate", "featureLimitsTemplate", "featureLimitsTemplateController");
        kony.application.registerMaster({
            "namespace": "com.adminConsole.companies",
            "classname": "featureLimitsTemplate",
            "name": "com.adminConsole.companies.featureLimitsTemplate"
        });
        kony.mvc.registry.add("com.adminConsole.companies.featuresTemplate", "featuresTemplate", "featuresTemplateController");
        kony.application.registerMaster({
            "namespace": "com.adminConsole.companies",
            "classname": "featuresTemplate",
            "name": "com.adminConsole.companies.featuresTemplate"
        });
        kony.mvc.registry.add("com.adminConsole.companies.HeaderTabs", "HeaderTabs", "HeaderTabsController");
        kony.application.registerMaster({
            "namespace": "com.adminConsole.companies",
            "classname": "HeaderTabs",
            "name": "com.adminConsole.companies.HeaderTabs"
        });
        kony.mvc.registry.add("com.adminConsole.configurationBundle.bundleData", "bundleData", "bundleDataController");
        kony.application.registerMaster({
            "namespace": "com.adminConsole.configurationBundle",
            "classname": "bundleData",
            "name": "com.adminConsole.configurationBundle.bundleData"
        });
        kony.mvc.registry.add("com.adminConsole.configurationBundle.configurationData", "configurationData", "configurationDataController");
        kony.application.registerMaster({
            "namespace": "com.adminConsole.configurationBundle",
            "classname": "configurationData",
            "name": "com.adminConsole.configurationBundle.configurationData"
        });
        kony.mvc.registry.add("com.adminConsole.contracts.addApprovalCondition", "addApprovalCondition", "addApprovalConditionController");
        kony.application.registerMaster({
            "namespace": "com.adminConsole.contracts",
            "classname": "addApprovalCondition",
            "name": "com.adminConsole.contracts.addApprovalCondition"
        });
        kony.mvc.registry.add("com.adminConsole.contracts.approvalMatrixCard", "approvalMatrixCard", "approvalMatrixCardController");
        kony.application.registerMaster({
            "namespace": "com.adminConsole.contracts",
            "classname": "approvalMatrixCard",
            "name": "com.adminConsole.contracts.approvalMatrixCard"
        });
        kony.mvc.registry.add("com.adminConsole.contracts.approvalMatrixFeatureCard", "approvalMatrixFeatureCard", "approvalMatrixFeatureCardController");
        kony.application.registerMaster({
            "namespace": "com.adminConsole.contracts",
            "classname": "approvalMatrixFeatureCard",
            "name": "com.adminConsole.contracts.approvalMatrixFeatureCard"
        });
        kony.mvc.registry.add("com.adminConsole.contracts.customersDropdown", "customersDropdown", "customersDropdownController");
        kony.application.registerMaster({
            "namespace": "com.adminConsole.contracts",
            "classname": "customersDropdown",
            "name": "com.adminConsole.contracts.customersDropdown"
        });
        kony.mvc.registry.add("com.adminConsole.contracts.serviceCard", "serviceCard", "serviceCardController");
        kony.application.registerMaster({
            "namespace": "com.adminConsole.contracts",
            "classname": "serviceCard",
            "name": "com.adminConsole.contracts.serviceCard"
        });
        kony.mvc.registry.add("com.adminConsole.CSR.detailHeader", "detailHeader", "detailHeaderController");
        kony.application.registerMaster({
            "namespace": "com.adminConsole.CSR",
            "classname": "detailHeader",
            "name": "com.adminConsole.CSR.detailHeader"
        });
        kony.mvc.registry.add("com.adminConsole.CSR.TemplateMessage", "TemplateMessage", "TemplateMessageController");
        kony.application.registerMaster({
            "namespace": "com.adminConsole.CSR",
            "classname": "TemplateMessage",
            "name": "com.adminConsole.CSR.TemplateMessage"
        });
        kony.mvc.registry.add("com.adminConsole.custMang.searchBox", "searchBox", "searchBoxController");
        kony.application.registerMaster({
            "namespace": "com.adminConsole.custMang",
            "classname": "searchBox",
            "name": "com.adminConsole.custMang.searchBox"
        });
        kony.mvc.registry.add("com.adminConsole.CustomerManagement.CustomerSearchandResults", "CustomerSearchandResults", "CustomerSearchandResultsController");
        kony.application.registerMaster({
            "namespace": "com.adminConsole.CustomerManagement",
            "classname": "CustomerSearchandResults",
            "name": "com.adminConsole.CustomerManagement.CustomerSearchandResults"
        });
        kony.mvc.registry.add("com.adminConsole.customerMang.Address", "Address", "AddressController");
        kony.application.registerMaster({
            "namespace": "com.adminConsole.customerMang",
            "classname": "Address",
            "name": "com.adminConsole.customerMang.Address"
        });
        kony.mvc.registry.add("com.adminConsole.customerMang.alertMessage", "alertMessage", "alertMessageController");
        kony.application.registerMaster({
            "namespace": "com.adminConsole.customerMang",
            "classname": "alertMessage",
            "name": "com.adminConsole.customerMang.alertMessage"
        });
        kony.mvc.registry.add("com.adminConsole.customerMang.backToPageHeader", "backToPageHeader", "backToPageHeaderController");
        kony.application.registerMaster({
            "namespace": "com.adminConsole.customerMang",
            "classname": "backToPageHeader",
            "name": "com.adminConsole.customerMang.backToPageHeader"
        });
        kony.mvc.registry.add("com.adminConsole.customerMang.ContactNum", "ContactNum", "ContactNumController");
        kony.application.registerMaster({
            "namespace": "com.adminConsole.customerMang",
            "classname": "ContactNum",
            "name": "com.adminConsole.customerMang.ContactNum"
        });
        kony.mvc.registry.add("com.adminConsole.customerMang.CSRAssist", "CSRAssist", "CSRAssistController");
        kony.application.registerMaster({
            "namespace": "com.adminConsole.customerMang",
            "classname": "CSRAssist",
            "name": "com.adminConsole.customerMang.CSRAssist"
        });
        kony.mvc.registry.add("com.adminConsole.customerMang.deleteRow.deleteRow", "deleteRow", "deleteRowController");
        kony.application.registerMaster({
            "namespace": "com.adminConsole.customerMang.deleteRow",
            "classname": "deleteRow",
            "name": "com.adminConsole.customerMang.deleteRow.deleteRow"
        });
        kony.mvc.registry.add("com.adminConsole.customerMang.deviceInfo", "deviceInfo", "deviceInfoController");
        kony.application.registerMaster({
            "namespace": "com.adminConsole.customerMang",
            "classname": "deviceInfo",
            "name": "com.adminConsole.customerMang.deviceInfo"
        });
        kony.mvc.registry.add("com.adminConsole.customerMang.EditAddress", "EditAddress", "EditAddressController");
        kony.application.registerMaster({
            "namespace": "com.adminConsole.customerMang",
            "classname": "EditAddress",
            "name": "com.adminConsole.customerMang.EditAddress"
        });
        kony.mvc.registry.add("com.adminConsole.customerMang.generalInfoHeader", "generalInfoHeader", "generalInfoHeaderController");
        kony.application.registerMaster({
            "namespace": "com.adminConsole.customerMang",
            "classname": "generalInfoHeader",
            "name": "com.adminConsole.customerMang.generalInfoHeader"
        });
        kony.mvc.registry.add("com.adminConsole.customerMang.Llimits.Limits", "Limits", "LimitsController");
        kony.application.registerMaster({
            "namespace": "com.adminConsole.customerMang.Llimits",
            "classname": "Limits",
            "name": "com.adminConsole.customerMang.Llimits.Limits"
        });
        kony.mvc.registry.add("com.adminConsole.customerMang.Notes", "Notes", "NotesController");
        kony.application.registerMaster({
            "namespace": "com.adminConsole.customerMang",
            "classname": "Notes",
            "name": "com.adminConsole.customerMang.Notes"
        });
        kony.mvc.registry.add("com.adminConsole.customerMang.productDetails", "productDetails", "productDetailsController");
        kony.application.registerMaster({
            "namespace": "com.adminConsole.customerMang",
            "classname": "productDetails",
            "name": "com.adminConsole.customerMang.productDetails"
        });
        kony.mvc.registry.add("com.adminConsole.customerMang.selectOptions", "selectOptions", "selectOptionsController");
        kony.application.registerMaster({
            "namespace": "com.adminConsole.customerMang",
            "classname": "selectOptions",
            "name": "com.adminConsole.customerMang.selectOptions"
        });
        kony.mvc.registry.add("com.adminConsole.customerMang.tabs", "tabs", "tabsController");
        kony.application.registerMaster({
            "namespace": "com.adminConsole.customerMang",
            "classname": "tabs",
            "name": "com.adminConsole.customerMang.tabs"
        });
        kony.mvc.registry.add("com.adminConsole.customerRoles.collapsibleSegment", "collapsibleSegment", "collapsibleSegmentController");
        kony.application.registerMaster({
            "namespace": "com.adminConsole.customerRoles",
            "classname": "collapsibleSegment",
            "name": "com.adminConsole.customerRoles.collapsibleSegment"
        });
        kony.mvc.registry.add("com.adminConsole.customerRoles.ViewRoleFeaturesActions", "ViewRoleFeaturesActions", "ViewRoleFeaturesActionsController");
        kony.application.registerMaster({
            "namespace": "com.adminConsole.customerRoles",
            "classname": "ViewRoleFeaturesActions",
            "name": "com.adminConsole.customerRoles.ViewRoleFeaturesActions"
        });
        kony.mvc.registry.add("com.adminConsole.Customers.ToolTip", "ToolTip", "ToolTipController");
        kony.application.registerMaster({
            "namespace": "com.adminConsole.Customers",
            "classname": "ToolTip",
            "name": "com.adminConsole.Customers.ToolTip"
        });
        kony.mvc.registry.add("com.adminConsole.dashBoard.dashBoardTab", "dashBoardTab", "dashBoardTabController");
        kony.application.registerMaster({
            "namespace": "com.adminConsole.dashBoard",
            "classname": "dashBoardTab",
            "name": "com.adminConsole.dashBoard.dashBoardTab"
        });
        kony.mvc.registry.add("com.adminConsole.decisionManagement.DecisionFolder", "DecisionFolder", "DecisionFolderController");
        kony.application.registerMaster({
            "namespace": "com.adminConsole.decisionManagement",
            "classname": "DecisionFolder",
            "name": "com.adminConsole.decisionManagement.DecisionFolder"
        });
        kony.mvc.registry.add("com.adminConsole.dueDiligence.addTemplate.addTemplate", "addTemplate", "addTemplateController");
        kony.application.registerMaster({
            "namespace": "com.adminConsole.dueDiligence.addTemplate",
            "classname": "addTemplate",
            "name": "com.adminConsole.dueDiligence.addTemplate.addTemplate"
        });
        kony.mvc.registry.add("com.adminConsole.dueDiligence.customListbox", "customListbox", "customListboxController");
        kony.application.registerMaster({
            "namespace": "com.adminConsole.dueDiligence",
            "classname": "customListbox",
            "name": "com.adminConsole.dueDiligence.customListbox"
        });
        kony.mvc.registry.add("com.adminConsole.dueDiligence.verticalTabs.verticalTabs", "verticalTabs", "verticalTabsController");
        kony.application.registerMaster({
            "namespace": "com.adminConsole.dueDiligence.verticalTabs",
            "classname": "verticalTabs",
            "name": "com.adminConsole.dueDiligence.verticalTabs.verticalTabs"
        });
        kony.mvc.registry.add("com.adminConsole.enrollCustomer.contractsList", "contractsList", "contractsListController");
        kony.application.registerMaster({
            "namespace": "com.adminConsole.enrollCustomer",
            "classname": "contractsList",
            "name": "com.adminConsole.enrollCustomer.contractsList"
        });
        kony.mvc.registry.add("com.adminConsole.Features.ValueEntry", "ValueEntry", "ValueEntryController");
        kony.application.registerMaster({
            "namespace": "com.adminConsole.Features",
            "classname": "ValueEntry",
            "name": "com.adminConsole.Features.ValueEntry"
        });
        kony.mvc.registry.add("com.adminConsole.Groups.AdvancedSearchDropDown", "AdvancedSearchDropDown", "AdvancedSearchDropDownController");
        kony.application.registerMaster({
            "namespace": "com.adminConsole.Groups",
            "classname": "AdvancedSearchDropDown",
            "name": "com.adminConsole.Groups.AdvancedSearchDropDown"
        });
        kony.mvc.registry.add("com.adminConsole.Groups.SelectScheduler", "SelectScheduler", "SelectSchedulerController");
        kony.application.registerMaster({
            "namespace": "com.adminConsole.Groups",
            "classname": "SelectScheduler",
            "name": "com.adminConsole.Groups.SelectScheduler"
        });
        kony.mvc.registry.add("com.adminConsole.header.mainHeader", "mainHeader", "mainHeaderController");
        kony.application.registerMaster({
            "namespace": "com.adminConsole.header",
            "classname": "mainHeader",
            "name": "com.adminConsole.header.mainHeader"
        });
        kony.mvc.registry.add("com.adminConsole.header.search", "search", "searchController");
        kony.application.registerMaster({
            "namespace": "com.adminConsole.header",
            "classname": "search",
            "name": "com.adminConsole.header.search"
        });
        kony.mvc.registry.add("com.adminConsole.header.subHeader", "subHeader", "subHeaderController");
        kony.application.registerMaster({
            "namespace": "com.adminConsole.header",
            "classname": "subHeader",
            "name": "com.adminConsole.header.subHeader"
        });
        kony.mvc.registry.add("com.adminConsole.history.search", "search", "searchController");
        kony.application.registerMaster({
            "namespace": "com.adminConsole.history",
            "classname": "search",
            "name": "com.adminConsole.history.search"
        });
        kony.mvc.registry.add("com.adminConsole.LeadManagement.viewLeads", "viewLeads", "viewLeadsController");
        kony.application.registerMaster({
            "namespace": "com.adminConsole.LeadManagement",
            "classname": "viewLeads",
            "name": "com.adminConsole.LeadManagement.viewLeads"
        });
        kony.mvc.registry.add("com.adminConsole.LeadManagment.countTabs", "countTabs", "countTabsController");
        kony.application.registerMaster({
            "namespace": "com.adminConsole.LeadManagment",
            "classname": "countTabs",
            "name": "com.adminConsole.LeadManagment.countTabs"
        });
        kony.mvc.registry.add("com.adminConsole.loans.applications.consent", "consent", "consentController");
        kony.application.registerMaster({
            "namespace": "com.adminConsole.loans.applications",
            "classname": "consent",
            "name": "com.adminConsole.loans.applications.consent"
        });
        kony.mvc.registry.add("com.adminConsole.loans.applications.errorMsg", "errorMsg", "errorMsgController");
        kony.application.registerMaster({
            "namespace": "com.adminConsole.loans.applications",
            "classname": "errorMsg",
            "name": "com.adminConsole.loans.applications.errorMsg"
        });
        kony.mvc.registry.add("com.adminConsole.loans.applications.incomeGroup", "incomeGroup", "incomeGroupController");
        kony.application.registerMaster({
            "namespace": "com.adminConsole.loans.applications",
            "classname": "incomeGroup",
            "name": "com.adminConsole.loans.applications.incomeGroup"
        });
        kony.mvc.registry.add("com.adminConsole.loans.applications.leftMenu", "leftMenu", "leftMenuController");
        kony.application.registerMaster({
            "namespace": "com.adminConsole.loans.applications",
            "classname": "leftMenu",
            "name": "com.adminConsole.loans.applications.leftMenu"
        });
        kony.mvc.registry.add("com.adminConsole.loans.applications.loanApplicationDetails", "loanApplicationDetails", "loanApplicationDetailsController");
        kony.application.registerMaster({
            "namespace": "com.adminConsole.loans.applications",
            "classname": "loanApplicationDetails",
            "name": "com.adminConsole.loans.applications.loanApplicationDetails"
        });
        kony.mvc.registry.add("com.adminConsole.loans.applications.loansSectionHeader", "loansSectionHeader", "loansSectionHeaderController");
        kony.application.registerMaster({
            "namespace": "com.adminConsole.loans.applications",
            "classname": "loansSectionHeader",
            "name": "com.adminConsole.loans.applications.loansSectionHeader"
        });
        kony.mvc.registry.add("com.adminConsole.loans.applications.loantype.loaninfo", "loaninfo", "loaninfoController");
        kony.application.registerMaster({
            "namespace": "com.adminConsole.loans.applications.loantype",
            "classname": "loaninfo",
            "name": "com.adminConsole.loans.applications.loantype.loaninfo"
        });
        kony.mvc.registry.add("com.adminConsole.loans.applications.simulationTemplate.simulationTemplate", "simulationTemplate", "simulationTemplateController");
        kony.application.registerMaster({
            "namespace": "com.adminConsole.loans.applications.simulationTemplate",
            "classname": "simulationTemplate",
            "name": "com.adminConsole.loans.applications.simulationTemplate.simulationTemplate"
        });
        kony.mvc.registry.add("com.adminConsole.location.supportedCurrency", "supportedCurrency", "supportedCurrencyController");
        kony.application.registerMaster({
            "namespace": "com.adminConsole.location",
            "classname": "supportedCurrency",
            "name": "com.adminConsole.location.supportedCurrency"
        });
        kony.mvc.registry.add("com.adminConsole.locations.mainHeader", "mainHeader", "mainHeaderController");
        kony.application.registerMaster({
            "namespace": "com.adminConsole.locations",
            "classname": "mainHeader",
            "name": "com.adminConsole.locations.mainHeader"
        });
        kony.mvc.registry.add("com.adminConsole.logs.LogCustomTabs", "LogCustomTabs", "LogCustomTabsController");
        kony.application.registerMaster({
            "namespace": "com.adminConsole.logs",
            "classname": "LogCustomTabs",
            "name": "com.adminConsole.logs.LogCustomTabs"
        });
        kony.mvc.registry.add("com.adminConsole.logs.LogDefaultTabs", "LogDefaultTabs", "LogDefaultTabsController");
        kony.application.registerMaster({
            "namespace": "com.adminConsole.logs",
            "classname": "LogDefaultTabs",
            "name": "com.adminConsole.logs.LogDefaultTabs"
        });
        kony.mvc.registry.add("com.adminConsole.logs.popUpSaveFilter", "popUpSaveFilter", "popUpSaveFilterController");
        kony.application.registerMaster({
            "namespace": "com.adminConsole.logs",
            "classname": "popUpSaveFilter",
            "name": "com.adminConsole.logs.popUpSaveFilter"
        });
        kony.mvc.registry.add("com.adminConsole.logs.searchCustomer", "searchCustomer", "searchCustomerController");
        kony.application.registerMaster({
            "namespace": "com.adminConsole.logs",
            "classname": "searchCustomer",
            "name": "com.adminConsole.logs.searchCustomer"
        });
        kony.mvc.registry.add("com.adminConsole.logs.selectAmount", "selectAmount", "selectAmountController");
        kony.application.registerMaster({
            "namespace": "com.adminConsole.logs",
            "classname": "selectAmount",
            "name": "com.adminConsole.logs.selectAmount"
        });
        kony.mvc.registry.add("com.adminConsole.MFA.backToPageRight", "backToPageRight", "backToPageRightController");
        kony.application.registerMaster({
            "namespace": "com.adminConsole.MFA",
            "classname": "backToPageRight",
            "name": "com.adminConsole.MFA.backToPageRight"
        });
        kony.mvc.registry.add("com.adminConsole.navigation.leftMenuNew", "leftMenuNew", "leftMenuNewController");
        kony.application.registerMaster({
            "namespace": "com.adminConsole.navigation",
            "classname": "leftMenuNew",
            "name": "com.adminConsole.navigation.leftMenuNew"
        });
        kony.mvc.registry.add("com.adminConsole.onBoarding.popUp", "popUp", "popUpController");
        kony.application.registerMaster({
            "namespace": "com.adminConsole.onBoarding",
            "classname": "popUp",
            "name": "com.adminConsole.onBoarding.popUp"
        });
        kony.mvc.registry.add("com.adminConsole.Products.productDetailsView", "productDetailsView", "productDetailsViewController");
        kony.application.registerMaster({
            "namespace": "com.adminConsole.Products",
            "classname": "productDetailsView",
            "name": "com.adminConsole.Products.productDetailsView"
        });
        kony.mvc.registry.add("com.adminConsole.Products.productDetailsViewNew", "productDetailsViewNew", "productDetailsViewNewController");
        kony.application.registerMaster({
            "namespace": "com.adminConsole.Products",
            "classname": "productDetailsViewNew",
            "name": "com.adminConsole.Products.productDetailsViewNew"
        });
        kony.mvc.registry.add("com.adminConsole.Products.productTypeFilterMenu", "productTypeFilterMenu", "productTypeFilterMenuController");
        kony.application.registerMaster({
            "namespace": "com.adminConsole.Products",
            "classname": "productTypeFilterMenu",
            "name": "com.adminConsole.Products.productTypeFilterMenu"
        });
        kony.mvc.registry.add("com.adminConsole.Products.ViewProductFeatureDetails", "ViewProductFeatureDetails", "ViewProductFeatureDetailsController");
        kony.application.registerMaster({
            "namespace": "com.adminConsole.Products",
            "classname": "ViewProductFeatureDetails",
            "name": "com.adminConsole.Products.ViewProductFeatureDetails"
        });
        kony.mvc.registry.add("com.adminConsole.reports.reportPagination", "reportPagination", "reportPaginationController");
        kony.application.registerMaster({
            "namespace": "com.adminConsole.reports",
            "classname": "reportPagination",
            "name": "com.adminConsole.reports.reportPagination"
        });
        kony.mvc.registry.add("com.adminConsole.reports.search", "search", "searchController");
        kony.application.registerMaster({
            "namespace": "com.adminConsole.reports",
            "classname": "search",
            "name": "com.adminConsole.reports.search"
        });
        kony.mvc.registry.add("com.adminConsole.search.advancedSearch", "advancedSearch", "advancedSearchController");
        kony.application.registerMaster({
            "namespace": "com.adminConsole.search",
            "classname": "advancedSearch",
            "name": "com.adminConsole.search.advancedSearch"
        });
        kony.mvc.registry.add("com.adminConsole.search.modifySearch", "modifySearch", "modifySearchController");
        kony.application.registerMaster({
            "namespace": "com.adminConsole.search",
            "classname": "modifySearch",
            "name": "com.adminConsole.search.modifySearch"
        });
        kony.mvc.registry.add("com.adminConsole.selectDate.selectDate", "selectDate", "selectDateController");
        kony.application.registerMaster({
            "namespace": "com.adminConsole.selectDate",
            "classname": "selectDate",
            "name": "com.adminConsole.selectDate.selectDate"
        });
        kony.mvc.registry.add("com.adminConsole.serviceDefinition.advanceSelection", "advanceSelection", "advanceSelectionController");
        kony.application.registerMaster({
            "namespace": "com.adminConsole.serviceDefinition",
            "classname": "advanceSelection",
            "name": "com.adminConsole.serviceDefinition.advanceSelection"
        });
        kony.mvc.registry.add("com.adminConsole.serviceDefinition.advanceSelectionComponent", "advanceSelectionComponent", "advanceSelectionComponentController");
        kony.application.registerMaster({
            "namespace": "com.adminConsole.serviceDefinition",
            "classname": "advanceSelectionComponent",
            "name": "com.adminConsole.serviceDefinition.advanceSelectionComponent"
        });
        kony.mvc.registry.add("com.adminConsole.serviceDefinition.viewFeatureAction", "viewFeatureAction", "viewFeatureActionController");
        kony.application.registerMaster({
            "namespace": "com.adminConsole.serviceDefinition",
            "classname": "viewFeatureAction",
            "name": "com.adminConsole.serviceDefinition.viewFeatureAction"
        });
        kony.mvc.registry.add("com.adminConsole.staticContent.addStaticData", "addStaticData", "addStaticDataController");
        kony.application.registerMaster({
            "namespace": "com.adminConsole.staticContent",
            "classname": "addStaticData",
            "name": "com.adminConsole.staticContent.addStaticData"
        });
        kony.mvc.registry.add("com.adminConsole.staticContent.noStaticData", "noStaticData", "noStaticDataController");
        kony.application.registerMaster({
            "namespace": "com.adminConsole.staticContent",
            "classname": "noStaticData",
            "name": "com.adminConsole.staticContent.noStaticData"
        });
        kony.mvc.registry.add("com.adminConsole.staticContent.staticData", "staticData", "staticDataController");
        kony.application.registerMaster({
            "namespace": "com.adminConsole.staticContent",
            "classname": "staticData",
            "name": "com.adminConsole.staticContent.staticData"
        });
        kony.mvc.registry.add("com.adminConsole.StaticContentManagement.faq.tableView", "tableView", "tableViewController");
        kony.application.registerMaster({
            "namespace": "com.adminConsole.StaticContentManagement.faq",
            "classname": "tableView",
            "name": "com.adminConsole.StaticContentManagement.faq.tableView"
        });
        kony.mvc.registry.add("com.adminConsole.StaticContentManagement.tableView", "tableView", "tableViewController");
        kony.application.registerMaster({
            "namespace": "com.adminConsole.StaticContentManagement",
            "classname": "tableView",
            "name": "com.adminConsole.StaticContentManagement.tableView"
        });
        kony.mvc.registry.add("com.adminConsole.templateMessage.Message", "Message", "MessageController");
        kony.application.registerMaster({
            "namespace": "com.adminConsole.templateMessage",
            "classname": "Message",
            "name": "com.adminConsole.templateMessage.Message"
        });
        kony.mvc.registry.add("com.adminConsole.view.Description", "Description", "DescriptionController");
        kony.application.registerMaster({
            "namespace": "com.adminConsole.view",
            "classname": "Description",
            "name": "com.adminConsole.view.Description"
        });
        kony.mvc.registry.add("com.adminConsole.view.Description1", "Description1", "Description1Controller");
        kony.application.registerMaster({
            "namespace": "com.adminConsole.view",
            "classname": "Description1",
            "name": "com.adminConsole.view.Description1"
        });
        kony.mvc.registry.add("com.adminConsole.view.details", "details", "detailsController");
        kony.application.registerMaster({
            "namespace": "com.adminConsole.view",
            "classname": "details",
            "name": "com.adminConsole.view.details"
        });
        kony.mvc.registry.add("com.adminConsole.view.details1", "details1", "details1Controller");
        kony.application.registerMaster({
            "namespace": "com.adminConsole.view",
            "classname": "details1",
            "name": "com.adminConsole.view.details1"
        });
        kony.mvc.registry.add("com.adminConsole.view.detailsMultiLine", "detailsMultiLine", "detailsMultiLineController");
        kony.application.registerMaster({
            "namespace": "com.adminConsole.view",
            "classname": "detailsMultiLine",
            "name": "com.adminConsole.view.detailsMultiLine"
        });
        kony.mvc.registry.add("com.adminConsole.view.detailsWithLink", "detailsWithLink", "detailsWithLinkController");
        kony.application.registerMaster({
            "namespace": "com.adminConsole.view",
            "classname": "detailsWithLink",
            "name": "com.adminConsole.view.detailsWithLink"
        });
        kony.mvc.registry.add("com.c360.common.inputNumber", "inputNumber", "inputNumberController");
        kony.application.registerMaster({
            "namespace": "com.c360.common",
            "classname": "inputNumber",
            "name": "com.c360.common.inputNumber"
        });
        kony.mvc.registry.add("com.customer360.common.CharactersplusMinus", "CharactersplusMinus", "CharactersplusMinusController");
        kony.application.registerMaster({
            "namespace": "com.customer360.common",
            "classname": "CharactersplusMinus",
            "name": "com.customer360.common.CharactersplusMinus"
        });
        kony.mvc.registry.add("com.customer360.common.policies", "policies", "policiesController");
        kony.application.registerMaster({
            "namespace": "com.customer360.common",
            "classname": "policies",
            "name": "com.customer360.common.policies"
        });
        kony.mvc.registry.add("com.customer360.common.policiesRulesView", "policiesRulesView", "policiesRulesViewController");
        kony.application.registerMaster({
            "namespace": "com.customer360.common",
            "classname": "policiesRulesView",
            "name": "com.customer360.common.policiesRulesView"
        });
        kony.mvc.registry.add("com.konymp.map1", "map1", "map1Controller");
        kony.application.registerMaster({
            "namespace": "com.konymp",
            "classname": "map1",
            "name": "com.konymp.map1"
        });
        kony.mvc.registry.add("com.adminConsole.adManagement.attributeSelection", "attributeSelection", "attributeSelectionController");
        kony.application.registerMaster({
            "namespace": "com.adminConsole.adManagement",
            "classname": "attributeSelection",
            "name": "com.adminConsole.adManagement.attributeSelection"
        });
        kony.mvc.registry.add("com.adminConsole.adManagement.imageContainer", "imageContainer", "imageContainerController");
        kony.application.registerMaster({
            "namespace": "com.adminConsole.adManagement",
            "classname": "imageContainer",
            "name": "com.adminConsole.adManagement.imageContainer"
        });
        kony.mvc.registry.add("com.adminConsole.adManagement.imageSourceURL", "imageSourceURL", "imageSourceURLController");
        kony.application.registerMaster({
            "namespace": "com.adminConsole.adManagement",
            "classname": "imageSourceURL",
            "name": "com.adminConsole.adManagement.imageSourceURL"
        });
        kony.mvc.registry.add("com.adminConsole.adManagement.popupDetails", "popupDetails", "popupDetailsController");
        kony.application.registerMaster({
            "namespace": "com.adminConsole.adManagement",
            "classname": "popupDetails",
            "name": "com.adminConsole.adManagement.popupDetails"
        });
        kony.mvc.registry.add("com.adminConsole.alerts.alertGroupLayout", "alertGroupLayout", "alertGroupLayoutController");
        kony.application.registerMaster({
            "namespace": "com.adminConsole.alerts",
            "classname": "alertGroupLayout",
            "name": "com.adminConsole.alerts.alertGroupLayout"
        });
        kony.mvc.registry.add("com.adminConsole.alerts.frequencySelection", "frequencySelection", "frequencySelectionController");
        kony.application.registerMaster({
            "namespace": "com.adminConsole.alerts",
            "classname": "frequencySelection",
            "name": "com.adminConsole.alerts.frequencySelection"
        });
        kony.mvc.registry.add("com.adminConsole.businessBanking.linkProfilesPopup", "linkProfilesPopup", "linkProfilesPopupController");
        kony.application.registerMaster({
            "namespace": "com.adminConsole.businessBanking",
            "classname": "linkProfilesPopup",
            "name": "com.adminConsole.businessBanking.linkProfilesPopup"
        });
        kony.mvc.registry.add("com.adminConsole.common.assignLimits", "assignLimits", "assignLimitsController");
        kony.application.registerMaster({
            "namespace": "com.adminConsole.common",
            "classname": "assignLimits",
            "name": "com.adminConsole.common.assignLimits"
        });
        kony.mvc.registry.add("com.adminConsole.common.expandableDetails", "expandableDetails", "expandableDetailsController");
        kony.application.registerMaster({
            "namespace": "com.adminConsole.common",
            "classname": "expandableDetails",
            "name": "com.adminConsole.common.expandableDetails"
        });
        kony.mvc.registry.add("com.adminConsole.common.listingSegmentClient", "listingSegmentClient", "listingSegmentClientController");
        kony.application.registerMaster({
            "namespace": "com.adminConsole.common",
            "classname": "listingSegmentClient",
            "name": "com.adminConsole.common.listingSegmentClient"
        });
        kony.mvc.registry.add("com.adminConsole.common.listingSegmentLogs", "listingSegmentLogs", "listingSegmentLogsController");
        kony.application.registerMaster({
            "namespace": "com.adminConsole.common",
            "classname": "listingSegmentLogs",
            "name": "com.adminConsole.common.listingSegmentLogs"
        });
        kony.mvc.registry.add("com.adminConsole.contracts.accountsFeaturesCard", "accountsFeaturesCard", "accountsFeaturesCardController");
        kony.application.registerMaster({
            "namespace": "com.adminConsole.contracts",
            "classname": "accountsFeaturesCard",
            "name": "com.adminConsole.contracts.accountsFeaturesCard"
        });
        kony.mvc.registry.add("com.adminConsole.contracts.bulkUpdateAddNewFeatureRow", "bulkUpdateAddNewFeatureRow", "bulkUpdateAddNewFeatureRowController");
        kony.application.registerMaster({
            "namespace": "com.adminConsole.contracts",
            "classname": "bulkUpdateAddNewFeatureRow",
            "name": "com.adminConsole.contracts.bulkUpdateAddNewFeatureRow"
        });
        kony.mvc.registry.add("com.adminConsole.contracts.contractDetailsPopup", "contractDetailsPopup", "contractDetailsPopupController");
        kony.application.registerMaster({
            "namespace": "com.adminConsole.contracts",
            "classname": "contractDetailsPopup",
            "name": "com.adminConsole.contracts.contractDetailsPopup"
        });
        kony.mvc.registry.add("com.adminConsole.CSR.MessageFilter", "MessageFilter", "MessageFilterController");
        kony.application.registerMaster({
            "namespace": "com.adminConsole.CSR",
            "classname": "MessageFilter",
            "name": "com.adminConsole.CSR.MessageFilter"
        });
        kony.mvc.registry.add("com.adminConsole.csrEdit.editMessages", "editMessages", "editMessagesController");
        kony.application.registerMaster({
            "namespace": "com.adminConsole.csrEdit",
            "classname": "editMessages",
            "name": "com.adminConsole.csrEdit.editMessages"
        });
        kony.mvc.registry.add("com.adminConsole.customer.alerts", "alerts", "alertsController");
        kony.application.registerMaster({
            "namespace": "com.adminConsole.customer",
            "classname": "alerts",
            "name": "com.adminConsole.customer.alerts"
        });
        kony.mvc.registry.add("com.adminConsole.customerMang.delinkProfilePopup", "delinkProfilePopup", "delinkProfilePopupController");
        kony.application.registerMaster({
            "namespace": "com.adminConsole.customerMang",
            "classname": "delinkProfilePopup",
            "name": "com.adminConsole.customerMang.delinkProfilePopup"
        });
        kony.mvc.registry.add("com.adminConsole.customerMang.DeviceMang", "DeviceMang", "DeviceMangController");
        kony.application.registerMaster({
            "namespace": "com.adminConsole.customerMang",
            "classname": "DeviceMang",
            "name": "com.adminConsole.customerMang.DeviceMang"
        });
        kony.mvc.registry.add("com.adminConsole.customerMang.EditContact", "EditContact", "EditContactController");
        kony.application.registerMaster({
            "namespace": "com.adminConsole.customerMang",
            "classname": "EditContact",
            "name": "com.adminConsole.customerMang.EditContact"
        });
        kony.mvc.registry.add("com.adminConsole.customerMang.EditGeneralInfo", "EditGeneralInfo", "EditGeneralInfoController");
        kony.application.registerMaster({
            "namespace": "com.adminConsole.customerMang",
            "classname": "EditGeneralInfo",
            "name": "com.adminConsole.customerMang.EditGeneralInfo"
        });
        kony.mvc.registry.add("com.adminConsole.customerMang.RequestsHelpCenter", "RequestsHelpCenter", "RequestsHelpCenterController");
        kony.application.registerMaster({
            "namespace": "com.adminConsole.customerMang",
            "classname": "RequestsHelpCenter",
            "name": "com.adminConsole.customerMang.RequestsHelpCenter"
        });
        kony.mvc.registry.add("com.adminConsole.customerRoles.addFeaturesAndActions", "addFeaturesAndActions", "addFeaturesAndActionsController");
        kony.application.registerMaster({
            "namespace": "com.adminConsole.customerRoles",
            "classname": "addFeaturesAndActions",
            "name": "com.adminConsole.customerRoles.addFeaturesAndActions"
        });
        kony.mvc.registry.add("com.adminConsole.DeviceManagement.deviceAuthenticator", "deviceAuthenticator", "deviceAuthenticatorController");
        kony.application.registerMaster({
            "namespace": "com.adminConsole.DeviceManagement",
            "classname": "deviceAuthenticator",
            "name": "com.adminConsole.DeviceManagement.deviceAuthenticator"
        });
        kony.mvc.registry.add("com.adminConsole.Groups.AdvancedSearch", "AdvancedSearch", "AdvancedSearchController");
        kony.application.registerMaster({
            "namespace": "com.adminConsole.Groups",
            "classname": "AdvancedSearch",
            "name": "com.adminConsole.Groups.AdvancedSearch"
        });
        kony.mvc.registry.add("com.adminConsole.LeadManagment.createUpdate", "createUpdate", "createUpdateController");
        kony.application.registerMaster({
            "namespace": "com.adminConsole.LeadManagment",
            "classname": "createUpdate",
            "name": "com.adminConsole.LeadManagment.createUpdate"
        });
        kony.mvc.registry.add("com.adminConsole.loans.applications.editAdditionalIncome", "editAdditionalIncome", "editAdditionalIncomeController");
        kony.application.registerMaster({
            "namespace": "com.adminConsole.loans.applications",
            "classname": "editAdditionalIncome",
            "name": "com.adminConsole.loans.applications.editAdditionalIncome"
        });
        kony.mvc.registry.add("com.adminConsole.loans.applications.editAddressInfo", "editAddressInfo", "editAddressInfoController");
        kony.application.registerMaster({
            "namespace": "com.adminConsole.loans.applications",
            "classname": "editAddressInfo",
            "name": "com.adminConsole.loans.applications.editAddressInfo"
        });
        kony.mvc.registry.add("com.adminConsole.loans.applications.editAddressInfoAsPrevious", "editAddressInfoAsPrevious", "editAddressInfoAsPreviousController");
        kony.application.registerMaster({
            "namespace": "com.adminConsole.loans.applications",
            "classname": "editAddressInfoAsPrevious",
            "name": "com.adminConsole.loans.applications.editAddressInfoAsPrevious"
        });
        kony.mvc.registry.add("com.adminConsole.loans.applications.editConsent", "editConsent", "editConsentController");
        kony.application.registerMaster({
            "namespace": "com.adminConsole.loans.applications",
            "classname": "editConsent",
            "name": "com.adminConsole.loans.applications.editConsent"
        });
        kony.mvc.registry.add("com.adminConsole.loans.applications.editContactInfo", "editContactInfo", "editContactInfoController");
        kony.application.registerMaster({
            "namespace": "com.adminConsole.loans.applications",
            "classname": "editContactInfo",
            "name": "com.adminConsole.loans.applications.editContactInfo"
        });
        kony.mvc.registry.add("com.adminConsole.loans.applications.editEmployementInfo", "editEmployementInfo", "editEmployementInfoController");
        kony.application.registerMaster({
            "namespace": "com.adminConsole.loans.applications",
            "classname": "editEmployementInfo",
            "name": "com.adminConsole.loans.applications.editEmployementInfo"
        });
        kony.mvc.registry.add("com.adminConsole.loans.applications.editExpenditureInfo", "editExpenditureInfo", "editExpenditureInfoController");
        kony.application.registerMaster({
            "namespace": "com.adminConsole.loans.applications",
            "classname": "editExpenditureInfo",
            "name": "com.adminConsole.loans.applications.editExpenditureInfo"
        });
        kony.mvc.registry.add("com.adminConsole.loans.applications.editLoaninformation", "editLoaninformation", "editLoaninformationController");
        kony.application.registerMaster({
            "namespace": "com.adminConsole.loans.applications",
            "classname": "editLoaninformation",
            "name": "com.adminConsole.loans.applications.editLoaninformation"
        });
        kony.mvc.registry.add("com.adminConsole.loans.applications.editPersonalInfo", "editPersonalInfo", "editPersonalInfoController");
        kony.application.registerMaster({
            "namespace": "com.adminConsole.loans.applications",
            "classname": "editPersonalInfo",
            "name": "com.adminConsole.loans.applications.editPersonalInfo"
        });
        kony.mvc.registry.add("com.adminConsole.loans.applications.viewAdditionalIncome", "viewAdditionalIncome", "viewAdditionalIncomeController");
        kony.application.registerMaster({
            "namespace": "com.adminConsole.loans.applications",
            "classname": "viewAdditionalIncome",
            "name": "com.adminConsole.loans.applications.viewAdditionalIncome"
        });
        kony.mvc.registry.add("com.adminConsole.loans.applications.viewAddressInfo", "viewAddressInfo", "viewAddressInfoController");
        kony.application.registerMaster({
            "namespace": "com.adminConsole.loans.applications",
            "classname": "viewAddressInfo",
            "name": "com.adminConsole.loans.applications.viewAddressInfo"
        });
        kony.mvc.registry.add("com.adminConsole.loans.applications.viewConsent", "viewConsent", "viewConsentController");
        kony.application.registerMaster({
            "namespace": "com.adminConsole.loans.applications",
            "classname": "viewConsent",
            "name": "com.adminConsole.loans.applications.viewConsent"
        });
        kony.mvc.registry.add("com.adminConsole.loans.applications.viewContactInfo", "viewContactInfo", "viewContactInfoController");
        kony.application.registerMaster({
            "namespace": "com.adminConsole.loans.applications",
            "classname": "viewContactInfo",
            "name": "com.adminConsole.loans.applications.viewContactInfo"
        });
        kony.mvc.registry.add("com.adminConsole.loans.applications.viewEmployerDetails", "viewEmployerDetails", "viewEmployerDetailsController");
        kony.application.registerMaster({
            "namespace": "com.adminConsole.loans.applications",
            "classname": "viewEmployerDetails",
            "name": "com.adminConsole.loans.applications.viewEmployerDetails"
        });
        kony.mvc.registry.add("com.adminConsole.loans.applications.viewExpenditureInfo", "viewExpenditureInfo", "viewExpenditureInfoController");
        kony.application.registerMaster({
            "namespace": "com.adminConsole.loans.applications",
            "classname": "viewExpenditureInfo",
            "name": "com.adminConsole.loans.applications.viewExpenditureInfo"
        });
        kony.mvc.registry.add("com.adminConsole.loans.applications.viewIncomeSource", "viewIncomeSource", "viewIncomeSourceController");
        kony.application.registerMaster({
            "namespace": "com.adminConsole.loans.applications",
            "classname": "viewIncomeSource",
            "name": "com.adminConsole.loans.applications.viewIncomeSource"
        });
        kony.mvc.registry.add("com.adminConsole.loans.applications.viewLoaninformation", "viewLoaninformation", "viewLoaninformationController");
        kony.application.registerMaster({
            "namespace": "com.adminConsole.loans.applications",
            "classname": "viewLoaninformation",
            "name": "com.adminConsole.loans.applications.viewLoaninformation"
        });
        kony.mvc.registry.add("com.adminConsole.loans.applications.viewPersonalInfo", "viewPersonalInfo", "viewPersonalInfoController");
        kony.application.registerMaster({
            "namespace": "com.adminConsole.loans.applications",
            "classname": "viewPersonalInfo",
            "name": "com.adminConsole.loans.applications.viewPersonalInfo"
        });
        kony.mvc.registry.add("com.adminConsole.locations.uploadfilePopup", "uploadfilePopup", "uploadfilePopupController");
        kony.application.registerMaster({
            "namespace": "com.adminConsole.locations",
            "classname": "uploadfilePopup",
            "name": "com.adminConsole.locations.uploadfilePopup"
        });
        kony.mvc.registry.add("com.adminConsole.Permissions.viewConfigureCSRAssist", "viewConfigureCSRAssist", "viewConfigureCSRAssistController");
        kony.application.registerMaster({
            "namespace": "com.adminConsole.Permissions",
            "classname": "viewConfigureCSRAssist",
            "name": "com.adminConsole.Permissions.viewConfigureCSRAssist"
        });
        kony.mvc.registry.add("com.adminConsole.productManagement.manageProducts", "manageProducts", "manageProductsController");
        kony.application.registerMaster({
            "namespace": "com.adminConsole.productManagement",
            "classname": "manageProducts",
            "name": "com.adminConsole.productManagement.manageProducts"
        });
        kony.mvc.registry.add("com.adminConsole.productManagement.viewProducts", "viewProducts", "viewProductsController");
        kony.application.registerMaster({
            "namespace": "com.adminConsole.productManagement",
            "classname": "viewProducts",
            "name": "com.adminConsole.productManagement.viewProducts"
        });
        kony.mvc.registry.add("com.adminConsole.Products.addProduct", "addProduct", "addProductController");
        kony.application.registerMaster({
            "namespace": "com.adminConsole.Products",
            "classname": "addProduct",
            "name": "com.adminConsole.Products.addProduct"
        });
        kony.mvc.registry.add("com.adminConsole.reports.dynamicParameters", "dynamicParameters", "dynamicParametersController");
        kony.application.registerMaster({
            "namespace": "com.adminConsole.reports",
            "classname": "dynamicParameters",
            "name": "com.adminConsole.reports.dynamicParameters"
        });
        kony.mvc.registry.add("com.adminConsole.serviceDefinition.associatedRoles", "associatedRoles", "associatedRolesController");
        kony.application.registerMaster({
            "namespace": "com.adminConsole.serviceDefinition",
            "classname": "associatedRoles",
            "name": "com.adminConsole.serviceDefinition.associatedRoles"
        });
        kony.mvc.registry.add("com.adminConsole.serviceDefinition.featureAndActions", "featureAndActions", "featureAndActionsController");
        kony.application.registerMaster({
            "namespace": "com.adminConsole.serviceDefinition",
            "classname": "featureAndActions",
            "name": "com.adminConsole.serviceDefinition.featureAndActions"
        });
        kony.mvc.registry.add("com.adminConsole.ServiceManagement.displayContentEdit", "displayContentEdit", "displayContentEditController");
        kony.application.registerMaster({
            "namespace": "com.adminConsole.ServiceManagement",
            "classname": "displayContentEdit",
            "name": "com.adminConsole.ServiceManagement.displayContentEdit"
        });
        kony.mvc.registry.add("com.adminConsole.ServiceManagement.displayContentView", "displayContentView", "displayContentViewController");
        kony.application.registerMaster({
            "namespace": "com.adminConsole.ServiceManagement",
            "classname": "displayContentView",
            "name": "com.adminConsole.ServiceManagement.displayContentView"
        });
        kony.mvc.registry.add("com.adminConsole.contracts.bulkUpdateFeaturesLimitsPopup", "bulkUpdateFeaturesLimitsPopup", "bulkUpdateFeaturesLimitsPopupController");
        kony.application.registerMaster({
            "namespace": "com.adminConsole.contracts",
            "classname": "bulkUpdateFeaturesLimitsPopup",
            "name": "com.adminConsole.contracts.bulkUpdateFeaturesLimitsPopup"
        });
        kony.mvc.registry.add("com.adminConsole.CustomerManagement.ProfileGeneralInfo", "ProfileGeneralInfo", "ProfileGeneralInfoController");
        kony.application.registerMaster({
            "namespace": "com.adminConsole.CustomerManagement",
            "classname": "ProfileGeneralInfo",
            "name": "com.adminConsole.CustomerManagement.ProfileGeneralInfo"
        });
        kony.mvc.registry.add("com.adminConsole.loans.applications.editApplicantDetails", "editApplicantDetails", "editApplicantDetailsController");
        kony.application.registerMaster({
            "namespace": "com.adminConsole.loans.applications",
            "classname": "editApplicantDetails",
            "name": "com.adminConsole.loans.applications.editApplicantDetails"
        });
        kony.mvc.registry.add("com.adminConsole.loans.applications.editAUDetails", "editAUDetails", "editAUDetailsController");
        kony.application.registerMaster({
            "namespace": "com.adminConsole.loans.applications",
            "classname": "editAUDetails",
            "name": "com.adminConsole.loans.applications.editAUDetails"
        });
        kony.mvc.registry.add("com.adminConsole.loans.applications.viewApplicantDetails", "viewApplicantDetails", "viewApplicantDetailsController");
        kony.application.registerMaster({
            "namespace": "com.adminConsole.loans.applications",
            "classname": "viewApplicantDetails",
            "name": "com.adminConsole.loans.applications.viewApplicantDetails"
        });
        kony.mvc.registry.add("com.adminConsole.loans.applications.viewAUDetails", "viewAUDetails", "viewAUDetailsController");
        kony.application.registerMaster({
            "namespace": "com.adminConsole.loans.applications",
            "classname": "viewAUDetails",
            "name": "com.adminConsole.loans.applications.viewAUDetails"
        });
        kony.mvc.registry.add("flxAccountAlertPreferTab", "flxAccountAlertPreferTab", "flxAccountAlertPreferTabController");
        kony.mvc.registry.add("flxAccountAlertsCategoryDetails", "flxAccountAlertsCategoryDetails", "flxAccountAlertsCategoryDetailsController");
        kony.mvc.registry.add("flxAccountCentricDetails", "flxAccountCentricDetails", "flxAccountCentricDetailsController");
        kony.mvc.registry.add("flxAccountHeader", "flxAccountHeader", "flxAccountHeaderController");
        kony.mvc.registry.add("flxActionsLimits", "flxActionsLimits", "flxActionsLimitsController");
        kony.mvc.registry.add("flxActionsList", "flxActionsList", "flxActionsListController");
        kony.mvc.registry.add("flxActionStatus", "flxActionStatus", "flxActionStatusController");
        kony.mvc.registry.add("flxAddApprovalRule", "flxAddApprovalRule", "flxAddApprovalRuleController");
        kony.mvc.registry.add("flxSegAddAttribute", "flxSegAddAttribute", "flxSegAddAttributeController");
        kony.mvc.registry.add("flxImgDetails", "flxImgDetails", "flxImgDetailsController");
        kony.mvc.registry.add("flxAddOption", "flxAddOption", "flxAddOptionController");
        kony.mvc.registry.add("flxAddPermissions", "flxAddPermissions", "flxAddPermissionsController");
        kony.mvc.registry.add("flxAddRole", "flxAddRole", "flxAddRoleController");
        kony.mvc.registry.add("flxAddSecurityQuestion", "flxAddSecurityQuestion", "flxAddSecurityQuestionController");
        kony.mvc.registry.add("flxAddUsers", "flxAddUsers", "flxAddUsersController");
        kony.mvc.registry.add("flxDetailsURLs", "flxDetailsURLs", "flxDetailsURLsController");
        kony.mvc.registry.add("flxAdminConsoleLogList", "flxAdminConsoleLogList", "flxAdminConsoleLogListController");
        kony.mvc.registry.add("flxAdminConsoleLogListMain", "flxAdminConsoleLogListMain", "flxAdminConsoleLogListMainController");
        kony.mvc.registry.add("flxAlertCategory", "flxAlertCategory", "flxAlertCategoryController");
        kony.mvc.registry.add("flxAlertGroupCardList", "flxAlertGroupCardList", "flxAlertGroupCardListController");
        kony.mvc.registry.add("flxAlertGroupManagement", "flxAlertGroupManagement", "flxAlertGroupManagementController");
        kony.mvc.registry.add("flxAlertPreferAccountsList", "flxAlertPreferAccountsList", "flxAlertPreferAccountsListController");
        kony.mvc.registry.add("segAssociatedAlerts", "segAssociatedAlerts", "segAssociatedAlertsController");
        kony.mvc.registry.add("flxAlertsCategoryLanguageData", "flxAlertsCategoryLanguageData", "flxAlertsCategoryLanguageDataController");
        kony.mvc.registry.add("flxAlertSequenceMap", "flxAlertSequenceMap", "flxAlertSequenceMapController");
        kony.mvc.registry.add("flxAlertsLanguageData", "flxAlertsLanguageData", "flxAlertsLanguageDataController");
        kony.mvc.registry.add("segVariableReference", "segVariableReference", "segVariableReferenceController");
        kony.mvc.registry.add("flxApprovalMatrixActionHeader", "flxApprovalMatrixActionHeader", "flxApprovalMatrixActionHeaderController");
        kony.mvc.registry.add("flxApprovalMatrixNoRangeRow", "flxApprovalMatrixNoRangeRow", "flxApprovalMatrixNoRangeRowController");
        kony.mvc.registry.add("flxApprovalMatrixRangeRow", "flxApprovalMatrixRangeRow", "flxApprovalMatrixRangeRowController");
        kony.mvc.registry.add("flxSegApprovePermission", "flxSegApprovePermission", "flxSegApprovePermissionController");
        kony.mvc.registry.add("flxAssignLimits", "flxAssignLimits", "flxAssignLimitsController");
        kony.mvc.registry.add("flxAssignUsers", "flxAssignUsers", "flxAssignUsersController");
        kony.mvc.registry.add("flxTAndC", "flxTAndC", "flxTAndCController");
        kony.mvc.registry.add("flxSegAttributeList", "flxSegAttributeList", "flxSegAttributeListController");
        kony.mvc.registry.add("flxAvailableAccounts", "flxAvailableAccounts", "flxAvailableAccountsController");
        kony.mvc.registry.add("flxBusinessTypes", "flxBusinessTypes", "flxBusinessTypesController");
        kony.mvc.registry.add("flxCampaignAddCustomer", "flxCampaignAddCustomer", "flxCampaignAddCustomerController");
        kony.mvc.registry.add("flxCampaignOptionsSelected", "flxCampaignOptionsSelected", "flxCampaignOptionsSelectedController");
        kony.mvc.registry.add("flxCampaigns", "flxCampaigns", "flxCampaignsController");
        kony.mvc.registry.add("flxCampaignsDetails", "flxCampaignsDetails", "flxCampaignsDetailsController");
        kony.mvc.registry.add("flxCompanies", "flxCompanies", "flxCompaniesController");
        kony.mvc.registry.add("flxCompanyAccounts", "flxCompanyAccounts", "flxCompanyAccountsController");
        kony.mvc.registry.add("flxCompanyCustomer", "flxCompanyCustomer", "flxCompanyCustomerController");
        kony.mvc.registry.add("flxCompanyDetailsAccounts", "flxCompanyDetailsAccounts", "flxCompanyDetailsAccountsController");
        kony.mvc.registry.add("flxCompanyFeatures", "flxCompanyFeatures", "flxCompanyFeaturesController");
        kony.mvc.registry.add("flxCompanyLimits", "flxCompanyLimits", "flxCompanyLimitsController");
        kony.mvc.registry.add("flxCompanySelectedAccountsRow", "flxCompanySelectedAccountsRow", "flxCompanySelectedAccountsRowController");
        kony.mvc.registry.add("flxConfiguration", "flxConfiguration", "flxConfigurationController");
        kony.mvc.registry.add("flxConfigurationWithDescription", "flxConfigurationWithDescription", "flxConfigurationWithDescriptionController");
        kony.mvc.registry.add("flxContractEnrollAccountsEditRow", "flxContractEnrollAccountsEditRow", "flxContractEnrollAccountsEditRowController");
        kony.mvc.registry.add("flxContractEnrollAccountsEditSection", "flxContractEnrollAccountsEditSection", "flxContractEnrollAccountsEditSectionController");
        kony.mvc.registry.add("flxContractEnrollFeaturesEditRow", "flxContractEnrollFeaturesEditRow", "flxContractEnrollFeaturesEditRowController");
        kony.mvc.registry.add("flxContractEnrollFeaturesEditSection", "flxContractEnrollFeaturesEditSection", "flxContractEnrollFeaturesEditSectionController");
        kony.mvc.registry.add("flxContractsAccountHeaderView", "flxContractsAccountHeaderView", "flxContractsAccountHeaderViewController");
        kony.mvc.registry.add("flxContractsAccountView", "flxContractsAccountView", "flxContractsAccountViewController");
        kony.mvc.registry.add("flxContractsFABodyView", "flxContractsFABodyView", "flxContractsFABodyViewController");
        kony.mvc.registry.add("flxContractsFAHeaderView", "flxContractsFAHeaderView", "flxContractsFAHeaderViewController");
        kony.mvc.registry.add("flxContractsLimitsBodyCreate", "flxContractsLimitsBodyCreate", "flxContractsLimitsBodyCreateController");
        kony.mvc.registry.add("flxContractsLimitsBodyView", "flxContractsLimitsBodyView", "flxContractsLimitsBodyViewController");
        kony.mvc.registry.add("flxContractsLimitsHeaderCreate", "flxContractsLimitsHeaderCreate", "flxContractsLimitsHeaderCreateController");
        kony.mvc.registry.add("flxContractsLimitsHeaderView", "flxContractsLimitsHeaderView", "flxContractsLimitsHeaderViewController");
        kony.mvc.registry.add("flxCriterias", "flxCriterias", "flxCriteriasController");
        kony.mvc.registry.add("flxCSRMessageTemplate", "flxCSRMessageTemplate", "flxCSRMessageTemplateController");
        kony.mvc.registry.add("flxCSRMsgList", "flxCSRMsgList", "flxCSRMsgListController");
        kony.mvc.registry.add("flxCSRQueueList", "flxCSRQueueList", "flxCSRQueueListController");
        kony.mvc.registry.add("flxSelected", "flxSelected", "flxSelectedController");
        kony.mvc.registry.add("flxCustMangActivityHistory", "flxCustMangActivityHistory", "flxCustMangActivityHistoryController");
        kony.mvc.registry.add("flxCustMangActivityHistoryDetails", "flxCustMangActivityHistoryDetails", "flxCustMangActivityHistoryDetailsController");
        kony.mvc.registry.add("flxCustMangAlertHistory", "flxCustMangAlertHistory", "flxCustMangAlertHistoryController");
        kony.mvc.registry.add("flxCustMangContracts", "flxCustMangContracts", "flxCustMangContractsController");
        kony.mvc.registry.add("flxCustMangNotification", "flxCustMangNotification", "flxCustMangNotificationController");
        kony.mvc.registry.add("flxCustMangNotificationSelected", "flxCustMangNotificationSelected", "flxCustMangNotificationSelectedController");
        kony.mvc.registry.add("flxCustMangProductInfo", "flxCustMangProductInfo", "flxCustMangProductInfoController");
        kony.mvc.registry.add("flxCustMangRequest", "flxCustMangRequest", "flxCustMangRequestController");
        kony.mvc.registry.add("flxCustMangRequestSelected", "flxCustMangRequestSelected", "flxCustMangRequestSelectedController");
        kony.mvc.registry.add("flxCustMangSearch", "flxCustMangSearch", "flxCustMangSearchController");
        kony.mvc.registry.add("flxCustMangTransactionHistoryCompanies", "flxCustMangTransactionHistoryCompanies", "flxCustMangTransactionHistoryCompaniesController");
        kony.mvc.registry.add("flxCustMangTransctionHistory", "flxCustMangTransctionHistory", "flxCustMangTransctionHistoryController");
        kony.mvc.registry.add("flxCustomerActivityLog", "flxCustomerActivityLog", "flxCustomerActivityLogController");
        kony.mvc.registry.add("flxCustomerActivityLogAccounts", "flxCustomerActivityLogAccounts", "flxCustomerActivityLogAccountsController");
        kony.mvc.registry.add("flxCustomerAdminLog", "flxCustomerAdminLog", "flxCustomerAdminLogController");
        kony.mvc.registry.add("flxCustomerCardsList", "flxCustomerCardsList", "flxCustomerCardsListController");
        kony.mvc.registry.add("flxCustomerCare", "flxCustomerCare", "flxCustomerCareController");
        kony.mvc.registry.add("flxCustomerCareSelected", "flxCustomerCareSelected", "flxCustomerCareSelectedController");
        kony.mvc.registry.add("flxCustomerCentricDetails", "flxCustomerCentricDetails", "flxCustomerCentricDetailsController");
        kony.mvc.registry.add("flxCustomerInformation", "flxCustomerInformation", "flxCustomerInformationController");
        kony.mvc.registry.add("flxCustomerMangEntitlements", "flxCustomerMangEntitlements", "flxCustomerMangEntitlementsController");
        kony.mvc.registry.add("flxCustomerMangGroup", "flxCustomerMangGroup", "flxCustomerMangGroupController");
        kony.mvc.registry.add("flxCustomerName", "flxCustomerName", "flxCustomerNameController");
        kony.mvc.registry.add("flxCustomerProfileRoles", "flxCustomerProfileRoles", "flxCustomerProfileRolesController");
        kony.mvc.registry.add("flxCustomerResults", "flxCustomerResults", "flxCustomerResultsController");
        kony.mvc.registry.add("flxsegCustomersList", "flxsegCustomersList", "flxsegCustomersListController");
        kony.mvc.registry.add("flxCustomerSubAlerts", "flxCustomerSubAlerts", "flxCustomerSubAlertsController");
        kony.mvc.registry.add("flxCustRolesFeatures", "flxCustRolesFeatures", "flxCustRolesFeaturesController");
        kony.mvc.registry.add("flxCustRolesSelectedActions", "flxCustRolesSelectedActions", "flxCustRolesSelectedActionsController");
        kony.mvc.registry.add("flxsegCustomerGroup", "flxsegCustomerGroup", "flxsegCustomerGroupController");
        kony.mvc.registry.add("flxDashBoardAlert", "flxDashBoardAlert", "flxDashBoardAlertController");
        kony.mvc.registry.add("flxDataSources", "flxDataSources", "flxDataSourcesController");
        kony.mvc.registry.add("flxDecisionComments", "flxDecisionComments", "flxDecisionCommentsController");
        kony.mvc.registry.add("flxDecisionFiles", "flxDecisionFiles", "flxDecisionFilesController");
        kony.mvc.registry.add("flxDetailsDefaultURLs", "flxDetailsDefaultURLs", "flxDetailsDefaultURLsController");
        kony.mvc.registry.add("flxDefaultTargetCustRoles", "flxDefaultTargetCustRoles", "flxDefaultTargetCustRolesController");
        kony.mvc.registry.add("flxDependency", "flxDependency", "flxDependencyController");
        kony.mvc.registry.add("flxDepositApplicationsStarted", "flxDepositApplicationsStarted", "flxDepositApplicationsStartedController");
        kony.mvc.registry.add("flxDepositApplicationsSubmitted", "flxDepositApplicationsSubmitted", "flxDepositApplicationsSubmittedController");
        kony.mvc.registry.add("flxDepositsLeads", "flxDepositsLeads", "flxDepositsLeadsController");
        kony.mvc.registry.add("flxDeviceAuthenticator", "flxDeviceAuthenticator", "flxDeviceAuthenticatorController");
        kony.mvc.registry.add("flxDeviceManagement", "flxDeviceManagement", "flxDeviceManagementController");
        kony.mvc.registry.add("flxMain", "flxMain", "flxMainController");
        kony.mvc.registry.add("flxDropDown", "flxDropDown", "flxDropDownController");
        kony.mvc.registry.add("flxDueDiligence", "flxDueDiligence", "flxDueDiligenceController");
        kony.mvc.registry.add("flxDynamicLogs", "flxDynamicLogs", "flxDynamicLogsController");
        kony.mvc.registry.add("flxEmailMain", "flxEmailMain", "flxEmailMainController");
        kony.mvc.registry.add("flxEmailMainLeftPadding", "flxEmailMainLeftPadding", "flxEmailMainLeftPaddingController");
        kony.mvc.registry.add("flxEnrollCustomerContractList", "flxEnrollCustomerContractList", "flxEnrollCustomerContractListController");
        kony.mvc.registry.add("flxEnrollCustomerList", "flxEnrollCustomerList", "flxEnrollCustomerListController");
        kony.mvc.registry.add("flxEnrollCustomerSearchResult", "flxEnrollCustomerSearchResult", "flxEnrollCustomerSearchResultController");
        kony.mvc.registry.add("flxEnrollLimits", "flxEnrollLimits", "flxEnrollLimitsController");
        kony.mvc.registry.add("flxEnrollSelectedAccountsSec", "flxEnrollSelectedAccountsSec", "flxEnrollSelectedAccountsSecController");
        kony.mvc.registry.add("flxEventsDataSet", "flxEventsDataSet", "flxEventsDataSetController");
        kony.mvc.registry.add("flxEventsView", "flxEventsView", "flxEventsViewController");
        kony.mvc.registry.add("flxFAQ", "flxFAQ", "flxFAQController");
        kony.mvc.registry.add("flxFeatureActions", "flxFeatureActions", "flxFeatureActionsController");
        kony.mvc.registry.add("flxFeatureActionsList", "flxFeatureActionsList", "flxFeatureActionsListController");
        kony.mvc.registry.add("flxFeatureDetailsActions", "flxFeatureDetailsActions", "flxFeatureDetailsActionsController");
        kony.mvc.registry.add("flxFeatureLimits", "flxFeatureLimits", "flxFeatureLimitsController");
        kony.mvc.registry.add("flxFeatures", "flxFeatures", "flxFeaturesController");
        kony.mvc.registry.add("flxFeaturesList", "flxFeaturesList", "flxFeaturesListController");
        kony.mvc.registry.add("flxFeaturesListSelected", "flxFeaturesListSelected", "flxFeaturesListSelectedController");
        kony.mvc.registry.add("flxFeeRange", "flxFeeRange", "flxFeeRangeController");
        kony.mvc.registry.add("flxGroupseg", "flxGroupseg", "flxGroupsegController");
        kony.mvc.registry.add("flxGroupsAdvSearch", "flxGroupsAdvSearch", "flxGroupsAdvSearchController");
        kony.mvc.registry.add("flxGroupsAssignedCustomers", "flxGroupsAssignedCustomers", "flxGroupsAssignedCustomersController");
        kony.mvc.registry.add("flxGroupsListSelected", "flxGroupsListSelected", "flxGroupsListSelectedController");
        kony.mvc.registry.add("flxGrpAddOption", "flxGrpAddOption", "flxGrpAddOptionController");
        kony.mvc.registry.add("flxHeaderDashBoardAlert", "flxHeaderDashBoardAlert", "flxHeaderDashBoardAlertController");
        kony.mvc.registry.add("flxIdResults", "flxIdResults", "flxIdResultsController");
        kony.mvc.registry.add("flxSegImageDetails", "flxSegImageDetails", "flxSegImageDetailsController");
        kony.mvc.registry.add("segUploadedImageListKA", "segUploadedImageListKA", "segUploadedImageListKAController");
        kony.mvc.registry.add("flxImageUploadListHeader", "flxImageUploadListHeader", "flxImageUploadListHeaderController");
        kony.mvc.registry.add("flxJobs", "flxJobs", "flxJobsController");
        kony.mvc.registry.add("flxLeads", "flxLeads", "flxLeadsController");
        kony.mvc.registry.add("flxLeftMenuItem", "flxLeftMenuItem", "flxLeftMenuItemController");
        kony.mvc.registry.add("flxLeftMenuSectionHeader", "flxLeftMenuSectionHeader", "flxLeftMenuSectionHeaderController");
        kony.mvc.registry.add("flxLeftMenuSectionItem", "flxLeftMenuSectionItem", "flxLeftMenuSectionItemController");
        kony.mvc.registry.add("flxLimitGroup", "flxLimitGroup", "flxLimitGroupController");
        kony.mvc.registry.add("flxLimitsCustomer", "flxLimitsCustomer", "flxLimitsCustomerController");
        kony.mvc.registry.add("flxListOfApplicationsSubmittedHeader", "flxListOfApplicationsSubmittedHeader", "flxListOfApplicationsSubmittedHeaderController");
        kony.mvc.registry.add("flxListOfApplications", "flxListOfApplications", "flxListOfApplicationsController");
        kony.mvc.registry.add("flxListOfApplicationsHeader", "flxListOfApplicationsHeader", "flxListOfApplicationsHeaderController");
        kony.mvc.registry.add("flxListOfApplicationsSubmitted", "flxListOfApplicationsSubmitted", "flxListOfApplicationsSubmittedController");
        kony.mvc.registry.add("flxListOfLeads", "flxListOfLeads", "flxListOfLeadsController");
        kony.mvc.registry.add("flxLocationFacilites", "flxLocationFacilites", "flxLocationFacilitesController");
        kony.mvc.registry.add("flxsegLocations", "flxsegLocations", "flxsegLocationsController");
        kony.mvc.registry.add("flxCustSearch", "flxCustSearch", "flxCustSearchController");
        kony.mvc.registry.add("flxMasterList", "flxMasterList", "flxMasterListController");
        kony.mvc.registry.add("flxCompanydetails", "flxCompanydetails", "flxCompanydetailsController");
        kony.mvc.registry.add("flxSegMFAConfigs", "flxSegMFAConfigs", "flxSegMFAConfigsController");
        kony.mvc.registry.add("flxSegMFAConfigsSelected", "flxSegMFAConfigsSelected", "flxSegMFAConfigsSelectedController");
        kony.mvc.registry.add("flxSegMFAScenarioSelected", "flxSegMFAScenarioSelected", "flxSegMFAScenarioSelectedController");
        kony.mvc.registry.add("flxMore", "flxMore", "flxMoreController");
        kony.mvc.registry.add("flxCustomerMangNotes", "flxCustomerMangNotes", "flxCustomerMangNotesController");
        kony.mvc.registry.add("segNotes", "segNotes", "segNotesController");
        kony.mvc.registry.add("flxNotificationsOuter", "flxNotificationsOuter", "flxNotificationsOuterController");
        kony.mvc.registry.add("flxOptionAdded", "flxOptionAdded", "flxOptionAddedController");
        kony.mvc.registry.add("flxOptionList", "flxOptionList", "flxOptionListController");
        kony.mvc.registry.add("flxOptionValue", "flxOptionValue", "flxOptionValueController");
        kony.mvc.registry.add("flxOutage", "flxOutage", "flxOutageController");
        kony.mvc.registry.add("flxOutageMessages", "flxOutageMessages", "flxOutageMessagesController");
        kony.mvc.registry.add("flxPendingRequests", "flxPendingRequests", "flxPendingRequestsController");
        kony.mvc.registry.add("flxPermissions", "flxPermissions", "flxPermissionsController");
        kony.mvc.registry.add("flxPermissionsHeader", "flxPermissionsHeader", "flxPermissionsHeaderController");
        kony.mvc.registry.add("flxPolicies", "flxPolicies", "flxPoliciesController");
        kony.mvc.registry.add("flxPolicyDescription", "flxPolicyDescription", "flxPolicyDescriptionController");
        kony.mvc.registry.add("flxProductAddedFeatures", "flxProductAddedFeatures", "flxProductAddedFeaturesController");
        kony.mvc.registry.add("flxProductFeature", "flxProductFeature", "flxProductFeatureController");
        kony.mvc.registry.add("flxSegProductFeatureOption", "flxSegProductFeatureOption", "flxSegProductFeatureOptionController");
        kony.mvc.registry.add("flxProductGroup", "flxProductGroup", "flxProductGroupController");
        kony.mvc.registry.add("flxProductLine", "flxProductLine", "flxProductLineController");
        kony.mvc.registry.add("flxProducts", "flxProducts", "flxProductsController");
        kony.mvc.registry.add("flxProfiles", "flxProfiles", "flxProfilesController");
        kony.mvc.registry.add("flxRejectedRequests", "flxRejectedRequests", "flxRejectedRequestsController");
        kony.mvc.registry.add("flxRelatedCustomerList", "flxRelatedCustomerList", "flxRelatedCustomerListController");
        kony.mvc.registry.add("flxReportsListing", "flxReportsListing", "flxReportsListingController");
        kony.mvc.registry.add("flxReportsMangMessages", "flxReportsMangMessages", "flxReportsMangMessagesController");
        kony.mvc.registry.add("flxReportsMangSuggestions", "flxReportsMangSuggestions", "flxReportsMangSuggestionsController");
        kony.mvc.registry.add("flxReportsMangThreads", "flxReportsMangThreads", "flxReportsMangThreadsController");
        kony.mvc.registry.add("flxTransactionReports", "flxTransactionReports", "flxTransactionReportsController");
        kony.mvc.registry.add("flxUsersReport", "flxUsersReport", "flxUsersReportController");
        kony.mvc.registry.add("flxRequestsOuter", "flxRequestsOuter", "flxRequestsOuterController");
        kony.mvc.registry.add("flxRoleBusinessTypes", "flxRoleBusinessTypes", "flxRoleBusinessTypesController");
        kony.mvc.registry.add("flxRoleDetailsActions", "flxRoleDetailsActions", "flxRoleDetailsActionsController");
        kony.mvc.registry.add("flxRoles", "flxRoles", "flxRolesController");
        kony.mvc.registry.add("flxRolesHeader", "flxRolesHeader", "flxRolesHeaderController");
        kony.mvc.registry.add("flxRolesServiceDefRow", "flxRolesServiceDefRow", "flxRolesServiceDefRowController");
        kony.mvc.registry.add("flxSearchCompanyMap", "flxSearchCompanyMap", "flxSearchCompanyMapController");
        kony.mvc.registry.add("flxSearchDropDown", "flxSearchDropDown", "flxSearchDropDownController");
        kony.mvc.registry.add("flxSearchedUserDetails", "flxSearchedUserDetails", "flxSearchedUserDetailsController");
        kony.mvc.registry.add("flxSecurityQuestions", "flxSecurityQuestions", "flxSecurityQuestionsController");
        kony.mvc.registry.add("flxSelectReport", "flxSelectReport", "flxSelectReportController");
        kony.mvc.registry.add("flxSelectRoles", "flxSelectRoles", "flxSelectRolesController");
        kony.mvc.registry.add("flxServicesAndFaq", "flxServicesAndFaq", "flxServicesAndFaqController");
        kony.mvc.registry.add("flxServices2", "flxServices2", "flxServices2Controller");
        kony.mvc.registry.add("flxServicesAndFaqSelected", "flxServicesAndFaqSelected", "flxServicesAndFaqSelectedController");
        kony.mvc.registry.add("flxSegDefineFeeRange", "flxSegDefineFeeRange", "flxSegDefineFeeRangeController");
        kony.mvc.registry.add("flxServicesList", "flxServicesList", "flxServicesListController");
        kony.mvc.registry.add("flxServicesListSelected", "flxServicesListSelected", "flxServicesListSelectedController");
        kony.mvc.registry.add("flxServicesScheduleMaster", "flxServicesScheduleMaster", "flxServicesScheduleMasterController");
        kony.mvc.registry.add("flxSignatoryGroupActionBody", "flxSignatoryGroupActionBody", "flxSignatoryGroupActionBodyController");
        kony.mvc.registry.add("flxSignatoryGroupActionHeader", "flxSignatoryGroupActionHeader", "flxSignatoryGroupActionHeaderController");
        kony.mvc.registry.add("flxSegSignatoryGroupContent", "flxSegSignatoryGroupContent", "flxSegSignatoryGroupContentController");
        kony.mvc.registry.add("flxSegSignatoryGroupHeader", "flxSegSignatoryGroupHeader", "flxSegSignatoryGroupHeaderController");
        kony.mvc.registry.add("flxSubAlerts", "flxSubAlerts", "flxSubAlertsController");
        kony.mvc.registry.add("flxTandCVersionList", "flxTandCVersionList", "flxTandCVersionListController");
        kony.mvc.registry.add("flxTargetCustRoles", "flxTargetCustRoles", "flxTargetCustRolesController");
        kony.mvc.registry.add("flxTargetCustRolesAdDetails", "flxTargetCustRolesAdDetails", "flxTargetCustRolesAdDetailsController");
        kony.mvc.registry.add("flxTermsAndConditions", "flxTermsAndConditions", "flxTermsAndConditionsController");
        kony.mvc.registry.add("flxTransactionLogs", "flxTransactionLogs", "flxTransactionLogsController");
        kony.mvc.registry.add("flxSegUserDetails", "flxSegUserDetails", "flxSegUserDetailsController");
        kony.mvc.registry.add("flxUsers", "flxUsers", "flxUsersController");
        kony.mvc.registry.add("flxUsersDropDown", "flxUsersDropDown", "flxUsersDropDownController");
        kony.mvc.registry.add("flxUsersHeader", "flxUsersHeader", "flxUsersHeaderController");
        kony.mvc.registry.add("flxSegVariableOptions", "flxSegVariableOptions", "flxSegVariableOptionsController");
        kony.mvc.registry.add("flxRow", "flxRow", "flxRowController");
        kony.mvc.registry.add("flxVerticalTabsRolesAccounts", "flxVerticalTabsRolesAccounts", "flxVerticalTabsRolesAccountsController");
        kony.mvc.registry.add("flxViewAllLanguagePopup", "flxViewAllLanguagePopup", "flxViewAllLanguagePopupController");
        kony.mvc.registry.add("flxViewConfigureCSR", "flxViewConfigureCSR", "flxViewConfigureCSRController");
        kony.mvc.registry.add("flxVewCustomerRoles", "flxVewCustomerRoles", "flxVewCustomerRolesController");
        kony.mvc.registry.add("flxViewEntitlements", "flxViewEntitlements", "flxViewEntitlementsController");
        kony.mvc.registry.add("flxSegViewFeatureOptions", "flxSegViewFeatureOptions", "flxSegViewFeatureOptionsController");
        kony.mvc.registry.add("flxViewImgDetails", "flxViewImgDetails", "flxViewImgDetailsController");
        kony.mvc.registry.add("flxViewPermissions", "flxViewPermissions", "flxViewPermissionsController");
        kony.mvc.registry.add("flxViewRoles", "flxViewRoles", "flxViewRolesController");
        kony.mvc.registry.add("flxViewServiceDetailsTF", "flxViewServiceDetailsTF", "flxViewServiceDetailsTFController");
        kony.mvc.registry.add("flxViewSubsidiary", "flxViewSubsidiary", "flxViewSubsidiaryController");
        kony.mvc.registry.add("flxViewUsers", "flxViewUsers", "flxViewUsersController");
        kony.mvc.registry.add("flxTempFeatures", "flxTempFeatures", "flxTempFeaturesController");
        kony.mvc.registry.add("flLimitsCompanies", "flLimitsCompanies", "flLimitsCompaniesController");
        kony.mvc.registry.add("flxMandatoryFeatures", "flxMandatoryFeatures", "flxMandatoryFeaturesController");
        kony.mvc.registry.add("flxSegRelatedCustList", "flxSegRelatedCustList", "flxSegRelatedCustListController");
        kony.mvc.registry.add("flxSuspendFeature", "flxSuspendFeature", "flxSuspendFeatureController");
        kony.mvc.registry.add("flxAttributeRow", "flxAttributeRow", "flxAttributeRowController");
        kony.mvc.registry.add("flxCampaignPriorities", "flxCampaignPriorities", "flxCampaignPrioritiesController");
        kony.mvc.registry.add("flxGuestSimulationResults", "flxGuestSimulationResults", "flxGuestSimulationResultsController");
        kony.mvc.registry.add("flxSimulationResults", "flxSimulationResults", "flxSimulationResultsController");
        kony.mvc.registry.add("frmAlertsManagement", "AlertsManagementModule/frmAlertsManagement", {
            "controllerName": "AlertsManagementModule/frmAlertsManagementController",
            "controllerExtName": ["Pagination_FormExtn", "Sorting_FormExtn", "Navigation_Form_Extn", "WidgetPermission_Checker_FormExtns", "TabUtil_FormExtn", "ErrorInterceptor", "AdminConsoleCommonUtilities", "DateTimeUtils_FormExtn", "CurrencyUtils_FormExtn"],
            "controllerType": "kony.mvc.MDAFormController"
        });
        kony.mvc.registry.add("frmErrorLogin", "AuthModule/frmErrorLogin", {
            "controllerName": "AuthModule/frmErrorLoginController",
            "controllerExtName": ["Navigation_Form_Extn"],
            "controllerType": "kony.mvc.MDAFormController"
        });
        kony.mvc.registry.add("frmLogin", "AuthModule/frmLogin", {
            "controllerName": "AuthModule/frmLoginController",
            "controllerExtName": [],
            "controllerType": "kony.mvc.MDAFormController"
        });
        kony.mvc.registry.add("CampaignModule/frmAdDetails", "CampaignModule/frmAdDetails", {
            "controllerName": "CampaignModule/frmAdDetailsController",
            "controllerExtName": ["Sorting_FormExtn", "WidgetPermission_Checker_FormExtns", "Navigation_Form_Extn", "DateTimeUtils_FormExtn", "AdminConsoleCommonUtilities", "TabUtil_FormExtn"],
            "controllerType": "kony.mvc.MDAFormController"
        });
        kony.mvc.registry.add("CampaignModule/frmAdManagement", "CampaignModule/frmAdManagement", {
            "controllerName": "CampaignModule/frmAdManagementController",
            "controllerExtName": ["Sorting_FormExtn", "WidgetPermission_Checker_FormExtns", "Navigation_Form_Extn", "DateTimeUtils_FormExtn", "TabUtil_FormExtn", "AdminConsoleCommonUtilities"],
            "controllerType": "kony.mvc.MDAFormController"
        });
        kony.mvc.registry.add("frmBusinessTypes", "CompaniesModule/frmBusinessTypes", {
            "controllerName": "CompaniesModule/frmBusinessTypesController",
            "controllerExtName": ["Navigation_Form_Extn", "TabUtil_FormExtn", "DateTimeUtils_FormExtn", "CurrencyUtils_FormExtn", "Sorting_FormExtn", "AdminConsoleCommonUtilities", "downloadCSV_Extn", "ErrorInterceptor", "WidgetPermission_Checker_FormExtns"],
            "controllerType": "kony.mvc.MDAFormController"
        });
        kony.mvc.registry.add("frmCompanies", "CompaniesModule/frmCompanies", {
            "controllerName": "CompaniesModule/frmCompaniesController",
            "controllerExtName": ["Navigation_Form_Extn", "TabUtil_FormExtn", "AdminConsoleCommonUtilities", "DateTimeUtils_FormExtn", "CurrencyUtils_FormExtn", "Sorting_FormExtn", "CustomerManagement_Search_Extn", "AdminConsoleCommonUtilities", "downloadCSV_Extn", "ErrorInterceptor", "googleApiKeys", "WidgetPermission_Checker_FormExtns"],
            "controllerType": "kony.mvc.MDAFormController"
        });
        kony.mvc.registry.add("frmEnrollmentRequests", "CompaniesModule/frmEnrollmentRequests", {
            "controllerName": "CompaniesModule/frmEnrollmentRequestsController",
            "controllerExtName": ["Navigation_Form_Extn", "AdminConsoleCommonUtilities", "DateTimeUtils_FormExtn", "CurrencyUtils_FormExtn", "Sorting_FormExtn", "ErrorInterceptor", "WidgetPermission_Checker_FormExtns"],
            "controllerType": "kony.mvc.MDAFormController"
        });
        kony.mvc.registry.add("frmBusinessConfigurations", "ConfigurationsModule/frmBusinessConfigurations", {
            "controllerName": "ConfigurationsModule/frmBusinessConfigurationsController",
            "controllerExtName": ["AdminConsoleCommonUtilities", "Pagination_FormExtn", "Sorting_FormExtn", "WidgetPermission_Checker_FormExtns", "Navigation_Form_Extn", "TabUtil_FormExtn"],
            "controllerType": "kony.mvc.MDAFormController"
        });
        kony.mvc.registry.add("frmConfigurationBundles", "ConfigurationsModule/frmConfigurationBundles", {
            "controllerName": "ConfigurationsModule/frmConfigurationBundlesController",
            "controllerExtName": ["AdminConsoleCommonUtilities", "Pagination_FormExtn", "Sorting_FormExtn", "WidgetPermission_Checker_FormExtns", "Navigation_Form_Extn", "TabUtil_FormExtn"],
            "controllerType": "kony.mvc.MDAFormController"
        });
        kony.mvc.registry.add("ConfigurationsModule/frmServiceDefinition", "ConfigurationsModule/frmServiceDefinition", {
            "controllerName": "ConfigurationsModule/frmServiceDefinitionController",
            "controllerExtName": ["Pagination_FormExtn", "Sorting_FormExtn", "downloadCSV_Extn", "WidgetPermission_Checker_FormExtns", "Navigation_Form_Extn", "TabUtil_FormExtn", "DateTimeUtils_FormExtn", "AdminConsoleCommonUtilities", "CurrencyUtils_FormExtn"],
            "controllerType": "kony.mvc.MDAFormController"
        });
        kony.mvc.registry.add("frmCSR", "CSRModule/frmCSR", {
            "controllerName": "CSRModule/frmCSRController",
            "controllerExtName": ["AdminConsoleCommonUtilities", "WidgetPermission_Checker_FormExtns", "downloadCSV_Extn", "Pagination_FormExtn", "Sorting_FormExtn", "Navigation_Form_Extn", "DateTimeUtils_FormExtn"],
            "controllerType": "kony.mvc.MDAFormController"
        });
        kony.mvc.registry.add("frmCustomerCreate", "CustomerCreateModule/frmCustomerCreate", {
            "controllerName": "CustomerCreateModule/frmCustomerCreateController",
            "controllerExtName": ["Navigation_Form_Extn", "TabUtil_FormExtn", "AdminConsoleCommonUtilities", "DateTimeUtils_FormExtn", "Sorting_FormExtn"],
            "controllerType": "kony.mvc.MDAFormController"
        });
        kony.mvc.registry.add("frmGroups", "CustomerGroupsModule/frmGroups", {
            "controllerName": "CustomerGroupsModule/frmGroupsController",
            "controllerExtName": ["Pagination_FormExtn", "Sorting_FormExtn", "downloadCSV_Extn", "WidgetPermission_Checker_FormExtns", "Navigation_Form_Extn", "TabUtil_FormExtn", "DateTimeUtils_FormExtn", "AdminConsoleCommonUtilities", "CurrencyUtils_FormExtn"],
            "controllerType": "kony.mvc.MDAFormController"
        });
        kony.mvc.registry.add("CustomerManagementModule/frmAssistedOnboarding", "CustomerManagementModule/frmAssistedOnboarding", {
            "controllerName": "CustomerManagementModule/frmAssistedOnboardingController",
            "controllerExtName": ["AdminConsoleCommonUtilities", "AdminConsoleDateTimeUtilities", "WidgetPermission_Checker_FormExtns", "Navigation_Form_Extn", "DateTimeUtils_FormExtn", "TabUtil_FormExtn", "googleApiKeys"],
            "controllerType": "kony.mvc.MDAFormController"
        });
        kony.mvc.registry.add("frmCustomerManagement", "CustomerManagementModule/frmCustomerManagement", {
            "controllerName": "CustomerManagementModule/frmCustomerManagementController",
            "controllerExtName": ["AdminConsoleCommonUtilities", "Pagination_FormExtn", "Sorting_FormExtn", "downloadCSV_Extn", "CustomerManagement_Search_Extn", "WidgetPermission_Checker_FormExtns", "Navigation_Form_Extn", "DateTimeUtils_FormExtn", "CurrencyUtils_FormExtn", "TabUtil_FormExtn", "googleApiKeys"],
            "controllerType": "kony.mvc.MDAFormController"
        });
        kony.mvc.registry.add("CustomerManagementModule/frmCustomerProfileAccounts", "CustomerManagementModule/frmCustomerProfileAccounts", {
            "controllerName": "CustomerManagementModule/frmCustomerProfileAccountsController",
            "controllerExtName": ["AdminConsoleCommonUtilities", "AdminConsoleDateTimeUtilities", "Pagination_FormExtn", "Sorting_FormExtn", "downloadCSV_Extn", "CustomerManagement_Search_Extn", "WidgetPermission_Checker_FormExtns", "Navigation_Form_Extn", "DateTimeUtils_FormExtn", "CurrencyUtils_FormExtn", "TabUtil_FormExtn", "googleApiKeys"],
            "controllerType": "kony.mvc.MDAFormController"
        });
        kony.mvc.registry.add("frmCustomerProfileActivityHistory", "CustomerManagementModule/frmCustomerProfileActivityHistory", {
            "controllerName": "CustomerManagementModule/frmCustomerProfileActivityHistoryController",
            "controllerExtName": ["AdminConsoleCommonUtilities", "AdminConsoleDateTimeUtilities", "Pagination_FormExtn", "Sorting_FormExtn", "downloadCSV_Extn", "CustomerManagement_Search_Extn", "WidgetPermission_Checker_FormExtns", "Navigation_Form_Extn", "DateTimeUtils_FormExtn", "CurrencyUtils_FormExtn", "TabUtil_FormExtn", "googleApiKeys"],
            "controllerType": "kony.mvc.MDAFormController"
        });
        kony.mvc.registry.add("CustomerManagementModule/frmCustomerProfileAlerts", "CustomerManagementModule/frmCustomerProfileAlerts", {
            "controllerName": "CustomerManagementModule/frmCustomerProfileAlertsController",
            "controllerExtName": ["AdminConsoleCommonUtilities", "AdminConsoleDateTimeUtilities", "Pagination_FormExtn", "Sorting_FormExtn", "downloadCSV_Extn", "CustomerManagement_Search_Extn", "WidgetPermission_Checker_FormExtns", "Navigation_Form_Extn", "DateTimeUtils_FormExtn", "CurrencyUtils_FormExtn", "TabUtil_FormExtn", "googleApiKeys"],
            "controllerType": "kony.mvc.MDAFormController"
        });
        kony.mvc.registry.add("CustomerManagementModule/frmCustomerProfileContacts", "CustomerManagementModule/frmCustomerProfileContacts", {
            "controllerName": "CustomerManagementModule/frmCustomerProfileContactsController",
            "controllerExtName": ["AdminConsoleCommonUtilities", "AdminConsoleDateTimeUtilities", "Pagination_FormExtn", "Sorting_FormExtn", "downloadCSV_Extn", "CustomerManagement_Search_Extn", "WidgetPermission_Checker_FormExtns", "Navigation_Form_Extn", "DateTimeUtils_FormExtn", "CurrencyUtils_FormExtn", "TabUtil_FormExtn", "googleApiKeys"],
            "controllerType": "kony.mvc.MDAFormController"
        });
        kony.mvc.registry.add("frmCustomerProfileContracts", "CustomerManagementModule/frmCustomerProfileContracts", {
            "controllerName": "CustomerManagementModule/frmCustomerProfileContractsController",
            "controllerExtName": ["AdminConsoleCommonUtilities", "Pagination_FormExtn", "Sorting_FormExtn", "downloadCSV_Extn", "CustomerManagement_Search_Extn", "WidgetPermission_Checker_FormExtns", "Navigation_Form_Extn", "DateTimeUtils_FormExtn", "CurrencyUtils_FormExtn", "TabUtil_FormExtn", "googleApiKeys"],
            "controllerType": "kony.mvc.MDAFormController"
        });
        kony.mvc.registry.add("frmCustomerProfileDeviceInfo", "CustomerManagementModule/frmCustomerProfileDeviceInfo", {
            "controllerName": "CustomerManagementModule/frmCustomerProfileDeviceInfoController",
            "controllerExtName": ["AdminConsoleCommonUtilities", "AdminConsoleDateTimeUtilities", "Pagination_FormExtn", "Sorting_FormExtn", "downloadCSV_Extn", "CustomerManagement_Search_Extn", "WidgetPermission_Checker_FormExtns", "Navigation_Form_Extn", "DateTimeUtils_FormExtn", "CurrencyUtils_FormExtn", "TabUtil_FormExtn", "googleApiKeys"],
            "controllerType": "kony.mvc.MDAFormController"
        });
        kony.mvc.registry.add("frmCustomerProfileDueDiligence", "CustomerManagementModule/frmCustomerProfileDueDiligence", {
            "controllerName": "CustomerManagementModule/frmCustomerProfileDueDiligenceController",
            "controllerExtName": ["AdminConsoleCommonUtilities", "AdminConsoleDateTimeUtilities", "Pagination_FormExtn", "Sorting_FormExtn", "downloadCSV_Extn", "CustomerManagement_Search_Extn", "WidgetPermission_Checker_FormExtns", "Navigation_Form_Extn", "DateTimeUtils_FormExtn", "CurrencyUtils_FormExtn", "TabUtil_FormExtn", "googleApiKeys"],
            "controllerType": "kony.mvc.MDAFormController"
        });
        kony.mvc.registry.add("CustomerManagementModule/frmCustomerProfileEntitlements", "CustomerManagementModule/frmCustomerProfileEntitlements", {
            "controllerName": "CustomerManagementModule/frmCustomerProfileEntitlementsController",
            "controllerExtName": ["AdminConsoleCommonUtilities", "AdminConsoleDateTimeUtilities", "Pagination_FormExtn", "Sorting_FormExtn", "downloadCSV_Extn", "CustomerManagement_Search_Extn", "WidgetPermission_Checker_FormExtns", "Navigation_Form_Extn", "DateTimeUtils_FormExtn", "CurrencyUtils_FormExtn", "TabUtil_FormExtn", "googleApiKeys"],
            "controllerType": "kony.mvc.MDAFormController"
        });
        kony.mvc.registry.add("CustomerManagementModule/frmCustomerProfileHelpCenter", "CustomerManagementModule/frmCustomerProfileHelpCenter", {
            "controllerName": "CustomerManagementModule/frmCustomerProfileHelpCenterController",
            "controllerExtName": ["AdminConsoleCommonUtilities", "AdminConsoleDateTimeUtilities", "Pagination_FormExtn", "Sorting_FormExtn", "downloadCSV_Extn", "CustomerManagement_Search_Extn", "WidgetPermission_Checker_FormExtns", "Navigation_Form_Extn", "DateTimeUtils_FormExtn", "CurrencyUtils_FormExtn", "TabUtil_FormExtn", "googleApiKeys"],
            "controllerType": "kony.mvc.MDAFormController"
        });
        kony.mvc.registry.add("frmCustomerProfileLimits", "CustomerManagementModule/frmCustomerProfileLimits", {
            "controllerName": "CustomerManagementModule/frmCustomerProfileLimitsController",
            "controllerExtName": ["AdminConsoleCommonUtilities", "Pagination_FormExtn", "Sorting_FormExtn", "downloadCSV_Extn", "CustomerManagement_Search_Extn", "WidgetPermission_Checker_FormExtns", "Navigation_Form_Extn", "DateTimeUtils_FormExtn", "CurrencyUtils_FormExtn", "TabUtil_FormExtn", "googleApiKeys"],
            "controllerType": "kony.mvc.MDAFormController"
        });
        kony.mvc.registry.add("CustomerManagementModule/frmCustomerProfileRoles", "CustomerManagementModule/frmCustomerProfileRoles", {
            "controllerName": "CustomerManagementModule/frmCustomerProfileRolesController",
            "controllerExtName": ["AdminConsoleCommonUtilities", "AdminConsoleDateTimeUtilities", "Pagination_FormExtn", "Sorting_FormExtn", "downloadCSV_Extn", "CustomerManagement_Search_Extn", "WidgetPermission_Checker_FormExtns", "Navigation_Form_Extn", "DateTimeUtils_FormExtn", "CurrencyUtils_FormExtn", "TabUtil_FormExtn", "googleApiKeys"],
            "controllerType": "kony.mvc.MDAFormController"
        });
        kony.mvc.registry.add("frmDepositsDashboard", "CustomerManagementModule/frmDepositsDashboard", {
            "controllerName": "CustomerManagementModule/frmDepositsDashboardController",
            "controllerExtName": ["AdminConsoleCommonUtilities", "AdminConsoleDateTimeUtilities", "Pagination_FormExtn", "Sorting_FormExtn", "downloadCSV_Extn", "Navigation_Form_Extn", "DateTimeUtils_FormExtn", "CurrencyUtils_FormExtn", "TabUtil_FormExtn", "Navigation_Form_Extn", "WidgetPermission_Checker_FormExtns"],
            "controllerType": "kony.mvc.MDAFormController"
        });
        kony.mvc.registry.add("frmEnrollCustomer", "CustomerManagementModule/frmEnrollCustomer", {
            "controllerName": "CustomerManagementModule/frmEnrollCustomerController",
            "controllerExtName": ["AdminConsoleCommonUtilities", "Pagination_FormExtn", "Sorting_FormExtn", "downloadCSV_Extn", "CustomerManagement_Search_Extn", "WidgetPermission_Checker_FormExtns", "Navigation_Form_Extn", "DateTimeUtils_FormExtn", "CurrencyUtils_FormExtn", "TabUtil_FormExtn", "googleApiKeys"],
            "controllerType": "kony.mvc.MDAFormController"
        });
        kony.mvc.registry.add("frmLoansDashboard", "CustomerManagementModule/frmLoansDashboard", {
            "controllerName": "CustomerManagementModule/frmLoansDashboardController",
            "controllerExtName": ["AdminConsoleCommonUtilities", "WidgetPermission_Checker_FormExtns", "Navigation_Form_Extn", "DateTimeUtils_FormExtn", "AdminConsoleDateTimeUtilities", "TabUtil_FormExtn", "Sorting_FormExtn", "googleApiKeys"],
            "controllerType": "kony.mvc.MDAFormController"
        });
        kony.mvc.registry.add("CustomerManagementModule/frmNewCustomer", "CustomerManagementModule/frmNewCustomer", {
            "controllerName": "CustomerManagementModule/frmNewCustomerController",
            "controllerExtName": ["AdminConsoleCommonUtilities", "Pagination_FormExtn", "Sorting_FormExtn", "downloadCSV_Extn", "CustomerManagement_Search_Extn", "WidgetPermission_Checker_FormExtns", "Navigation_Form_Extn", "DateTimeUtils_FormExtn", "CurrencyUtils_FormExtn", "TabUtil_FormExtn", "googleApiKeys"],
            "controllerType": "kony.mvc.MDAFormController"
        });
        kony.mvc.registry.add("CustomerManagementModule/frmTrackApplication", "CustomerManagementModule/frmTrackApplication", {
            "controllerName": "CustomerManagementModule/frmTrackApplicationController",
            "controllerExtName": ["AdminConsoleCommonUtilities", "WidgetPermission_Checker_FormExtns", "Navigation_Form_Extn", "DateTimeUtils_FormExtn", "AdminConsoleDateTimeUtilities", "TabUtil_FormExtn", "googleApiKeys"],
            "controllerType": "kony.mvc.MDAFormController"
        });
        kony.mvc.registry.add("CustomerManagementModule/frmUpgradeUser", "CustomerManagementModule/frmUpgradeUser", {
            "controllerName": "CustomerManagementModule/frmUpgradeUserController",
            "controllerExtName": ["AdminConsoleCommonUtilities", "Pagination_FormExtn", "Sorting_FormExtn", "downloadCSV_Extn", "CustomerManagement_Search_Extn", "WidgetPermission_Checker_FormExtns", "Navigation_Form_Extn", "DateTimeUtils_FormExtn", "CurrencyUtils_FormExtn", "TabUtil_FormExtn", "googleApiKeys"],
            "controllerType": "kony.mvc.MDAFormController"
        });
        kony.mvc.registry.add("frmDashboard", "DashboardModule/frmDashboard", {
            "controllerName": "DashboardModule/frmDashboardController",
            "controllerExtName": ["Navigation_Form_Extn", "WidgetPermission_Checker_FormExtns", "DateTimeUtils_FormExtn"],
            "controllerType": "kony.mvc.MDAFormController"
        });
        kony.mvc.registry.add("DecisionManagementModule/frmDecisionManagement", "DecisionManagementModule/frmDecisionManagement", {
            "controllerName": "DecisionManagementModule/frmDecisionManagementController",
            "controllerExtName": ["Navigation_Form_Extn", "Sorting_FormExtn", "WidgetPermission_Checker_FormExtns", "DateTimeUtils_FormExtn"],
            "controllerType": "kony.mvc.MDAFormController"
        });
        kony.mvc.registry.add("frmGuestDashboard", "DetailsModule/frmGuestDashboard", {
            "controllerName": "DetailsModule/frmGuestDashboardController",
            "controllerExtName": ["AdminConsoleCommonUtilities", "WidgetPermission_Checker_FormExtns", "Navigation_Form_Extn"],
            "controllerType": "kony.mvc.MDAFormController"
        });
        kony.mvc.registry.add("frmFAQ", "FAQsModule/frmFAQ", {
            "controllerName": "FAQsModule/frmFAQController",
            "controllerExtName": ["Sorting_FormExtn", "WidgetPermission_Checker_FormExtns", "Navigation_Form_Extn", "Pagination_FormExtn", "ErrorInterceptor", "AdminConsoleCommonUtilities"],
            "controllerType": "kony.mvc.MDAFormController"
        });
        kony.mvc.registry.add("frmUsers", "InternalUserModule/frmUsers", {
            "controllerName": "InternalUserModule/frmUsersController",
            "controllerExtName": ["AdminConsoleCommonUtilities", "Pagination_FormExtn", "Sorting_FormExtn", "WidgetPermission_Checker_FormExtns", "Navigation_Form_Extn", "DateTimeUtils_FormExtn", "TabUtil_FormExtn"],
            "controllerType": "kony.mvc.MDAFormController"
        });
        kony.mvc.registry.add("LeadManagementModule/frmLeadManagement", "LeadManagementModule/frmLeadManagement", {
            "controllerName": "LeadManagementModule/frmLeadManagementController",
            "controllerExtName": ["AdminConsoleCommonUtilities", "Pagination_FormExtn", "Sorting_FormExtn", "WidgetPermission_Checker_FormExtns", "Navigation_Form_Extn", "TabUtil_FormExtn", "DateTimeUtils_FormExtn", "Regexes"],
            "controllerType": "kony.mvc.MDAFormController"
        });
        kony.mvc.registry.add("frmLogs", "LogsModule/frmLogs", {
            "controllerName": "LogsModule/frmLogsController",
            "controllerExtName": ["AdminConsoleCommonUtilities", "DateTimeUtils_FormExtn", "Pagination_FormExtn", "Sorting_FormExtn", "Navigation_Form_Extn", "WidgetPermission_Checker_FormExtns", "TabUtil_FormExtn", "CurrencyUtils_FormExtn"],
            "controllerType": "kony.mvc.MDAFormController"
        });
        kony.mvc.registry.add("frmCustomerCare", "MasterDataModule/frmCustomerCare", {
            "controllerName": "MasterDataModule/frmCustomerCareController",
            "controllerExtName": ["Sorting_FormExtn", "WidgetPermission_Checker_FormExtns", "Navigation_Form_Extn", "DateTimeUtils_FormExtn", "AdminConsoleCommonUtilities"],
            "controllerType": "kony.mvc.MDAFormController"
        });
        kony.mvc.registry.add("frmLocations", "MasterDataModule/frmLocations", {
            "controllerName": "MasterDataModule/frmLocationsController",
            "controllerExtName": ["Pagination_FormExtn", "Sorting_FormExtn", "downloadCSV_Extn", "Navigation_Form_Extn", "WidgetPermission_Checker_FormExtns", "DateTimeUtils_FormExtn", "TabUtil_FormExtn", "AdminConsoleCommonUtilities"],
            "controllerType": "kony.mvc.MDAFormController"
        });
        kony.mvc.registry.add("MasterDataModule/frmProduct", "MasterDataModule/frmProduct", {
            "controllerName": "MasterDataModule/frmProductController",
            "controllerExtName": ["Pagination_FormExtn", "Sorting_FormExtn", "downloadCSV_Extn", "Navigation_Form_Extn", "WidgetPermission_Checker_FormExtns", "DateTimeUtils_FormExtn", "AdminConsoleCommonUtilities"],
            "controllerType": "kony.mvc.MDAFormController"
        });
        kony.mvc.registry.add("frmMFAConfigurations", "MFAModule/frmMFAConfigurations", {
            "controllerName": "MFAModule/frmMFAConfigurationsController",
            "controllerExtName": ["WidgetPermission_Checker_FormExtns", "Navigation_Form_Extn", "Sorting_FormExtn", "AdminConsoleCommonUtilities", "CurrencyUtils_FormExtn"],
            "controllerType": "kony.mvc.MDAFormController"
        });
        kony.mvc.registry.add("frmMFAScenarios", "MFAModule/frmMFAScenarios", {
            "controllerName": "MFAModule/frmMFAScenariosController",
            "controllerExtName": ["WidgetPermission_Checker_FormExtns", "Navigation_Form_Extn", "Sorting_FormExtn", "AdminConsoleCommonUtilities", "CurrencyUtils_FormExtn"],
            "controllerType": "kony.mvc.MDAFormController"
        });
        kony.mvc.registry.add("frmOutageMessage", "OutageMessageModule/frmOutageMessage", {
            "controllerName": "OutageMessageModule/frmOutageMessageController",
            "controllerExtName": ["WidgetPermission_Checker_FormExtns", "Navigation_Form_Extn", "Sorting_FormExtn", "DateTimeUtils_FormExtn", "AdminConsoleCommonUtilities", "TabUtil_FormExtn", "AdminConsoleDateTimeUtilities", "Pagination_FormExtn"],
            "controllerType": "kony.mvc.MDAFormController"
        });
        kony.mvc.registry.add("frmPasswordAgeAndLockoutSettings", "PasswordAgeAndLockoutModule/frmPasswordAgeAndLockoutSettings", "PasswordAgeAndLockoutModule/frmPasswordAgeAndLockoutSettingsController");
        kony.mvc.registry.add("frmPermissions", "PermissionsModule/frmPermissions", {
            "controllerName": "PermissionsModule/frmPermissionsController",
            "controllerExtName": ["Pagination_FormExtn", "Sorting_FormExtn", "WidgetPermission_Checker_FormExtns", "Navigation_Form_Extn", "AdminConsoleCommonUtilities", "TabUtil_FormExtn", "DateTimeUtils_FormExtn", "AdminConsoleCommonUtilities"],
            "controllerType": "kony.mvc.MDAFormController"
        });
        kony.mvc.registry.add("frmPolicies", "PolicyModule/frmPolicies", {
            "controllerName": "PolicyModule/frmPoliciesController",
            "controllerExtName": ["WidgetPermission_Checker_FormExtns", "Navigation_Form_Extn"],
            "controllerType": "kony.mvc.MDAFormController"
        });
        kony.mvc.registry.add("frmPrivacyPolicy", "PrivacyPolicyModule/frmPrivacyPolicy", {
            "controllerName": "PrivacyPolicyModule/frmPrivacyPolicyController",
            "controllerExtName": ["WidgetPermission_Checker_FormExtns", "Navigation_Form_Extn", "ErrorInterceptor"],
            "controllerType": "kony.mvc.MDAFormController"
        });
        kony.mvc.registry.add("ProfileManagement/frmCreateProfile", "ProfileManagement/frmCreateProfile", "ProfileManagement/frmCreateProfileController");
        kony.mvc.registry.add("ProfileManagement/frmProfiles", "ProfileManagement/frmProfiles", "ProfileManagement/frmProfilesController");
        kony.mvc.registry.add("frmReportsManagement", "ReportsManagementModule/frmReportsManagement", {
            "controllerName": "ReportsManagementModule/frmReportsManagementController",
            "controllerExtName": ["DateTimeUtils_FormExtn", "AdminConsoleCommonUtilities", "Pagination_FormExtn", "Sorting_FormExtn", "downloadCSV_Extn", "CustomerManagement_Search_Extn", "WidgetPermission_Checker_FormExtns", "Navigation_Form_Extn", "CurrencyUtils_FormExtn", "TabUtil_FormExtn"],
            "controllerType": "kony.mvc.MDAFormController"
        });
        kony.mvc.registry.add("frmRoles", "RoleModule/frmRoles", {
            "controllerName": "RoleModule/frmRolesController",
            "controllerExtName": ["Pagination_FormExtn", "Sorting_FormExtn", "WidgetPermission_Checker_FormExtns", "Navigation_Form_Extn", "TabUtil_FormExtn", "DateTimeUtils_FormExtn", "AdminConsoleCommonUtilities"],
            "controllerType": "kony.mvc.MDAFormController"
        });
        kony.mvc.registry.add("frmMoneyMovementScheduling", "SchedulingModule/frmMoneyMovementScheduling", {
            "controllerName": "SchedulingModule/frmMoneyMovementSchedulingController",
            "controllerExtName": ["WidgetPermission_Checker_FormExtns", "Navigation_Form_Extn", "Sorting_FormExtn", "DateTimeUtils_FormExtn"],
            "controllerType": "kony.mvc.MDAFormController"
        });
        kony.mvc.registry.add("frmSecureImage", "SecurityModule/frmSecureImage", {
            "controllerName": "SecurityModule/frmSecureImageController",
            "controllerExtName": ["WidgetPermission_Checker_FormExtns", "Sorting_FormExtn", "Navigation_Form_Extn"],
            "controllerType": "kony.mvc.MDAFormController"
        });
        kony.mvc.registry.add("frmSecurityQuestions", "SecurityModule/frmSecurityQuestions", {
            "controllerName": "SecurityModule/frmSecurityQuestionsController",
            "controllerExtName": ["WidgetPermission_Checker_FormExtns", "Sorting_FormExtn", "Navigation_Form_Extn", "AdminConsoleCommonUtilities"],
            "controllerType": "kony.mvc.MDAFormController"
        });
        kony.mvc.registry.add("frmServiceManagement", "ServicesManagementModule/frmServiceManagement", {
            "controllerName": "ServicesManagementModule/frmServiceManagementController",
            "controllerExtName": ["AdminConsoleCommonUtilities", "WidgetPermission_Checker_FormExtns", "Navigation_Form_Extn", "downloadCSV_Extn", "Pagination_FormExtn", "Sorting_FormExtn", "CurrencyUtils_FormExtn", "TabUtil_FormExtn"],
            "controllerType": "kony.mvc.MDAFormController"
        });
        kony.mvc.registry.add("frmTermsAndConditions", "TermsAndConditionsModule/frmTermsAndConditions", {
            "controllerName": "TermsAndConditionsModule/frmTermsAndConditionsController",
            "controllerExtName": ["WidgetPermission_Checker_FormExtns", "Navigation_Form_Extn", "Sorting_FormExtn", "DateTimeUtils_FormExtn", "AdminConsoleCommonUtilities"],
            "controllerType": "kony.mvc.MDAFormController"
        });
        setAppBehaviors();
    },
    postAppInitCallBack: function(eventObj) {},
    appmenuseq: function() {
        var startupModule = kony.mvc.MDAApplication.getSharedInstance().moduleManager.getModule("AuthModule");
        startupModule.presentationController.presentUserInterface("frmLogin");
    }
});