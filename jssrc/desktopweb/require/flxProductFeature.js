define("flxProductFeature", function() {
    return function(controller) {
        var flxProductFeature = new kony.ui.FlexContainer({
            "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
            "clipBounds": true,
            "id": "flxProductFeature",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "left": "0dp",
            "isModalContainer": false,
            "skin": "sknBackgroundFFFFFF",
            "top": "0dp",
            "width": "100%"
        }, {}, {});
        flxProductFeature.setDefaultUnit(kony.flex.DP);
        var lblName = new kony.ui.Label({
            "bottom": "15px",
            "id": "lblName",
            "isVisible": true,
            "left": "10px",
            "skin": "sknLbl485C75LatoRegular13Px",
            "text": "Product Group",
            "top": "15px",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, {
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, {});
        var lblGroup = new kony.ui.Label({
            "bottom": "15px",
            "id": "lblGroup",
            "isVisible": true,
            "left": "30%",
            "skin": "sknLbl485C75LatoRegular13Px",
            "text": "Group Reference",
            "top": "15px",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, {
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, {});
        var lblType = new kony.ui.Label({
            "bottom": "15px",
            "id": "lblType",
            "isVisible": true,
            "left": "66%",
            "skin": "sknLbl485C75LatoRegular13Px",
            "text": "Product Line",
            "top": "15px",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, {
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, {});
        var flxEdit = new kony.ui.FlexContainer({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "clipBounds": true,
            "height": "20px",
            "id": "flxEdit",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "isModalContainer": false,
            "onClick": controller.AS_FlexContainer_h43e332b32624c87a4361f632e581db3,
            "right": "12px",
            "skin": "hoverhandSkin",
            "top": "15px",
            "width": "20px"
        }, {}, {});
        flxEdit.setDefaultUnit(kony.flex.DP);
        var lblEdit = new kony.ui.Label({
            "centerX": "50%",
            "centerY": "50%",
            "height": "20px",
            "id": "lblEdit",
            "isVisible": true,
            "skin": "sknIcon20px",
            "text": "",
            "width": "20px",
            "zIndex": 1
        }, {
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, {});
        flxEdit.add(lblEdit);
        var lblSeparator = new kony.ui.Label({
            "bottom": "0px",
            "height": "1px",
            "id": "lblSeparator",
            "isVisible": true,
            "left": "0px",
            "right": "0px",
            "skin": "sknlblSeperatorD7D9E0",
            "text": "Label",
            "zIndex": 2
        }, {
            "contentAlignment": constants.CONTENT_ALIGN_TOP_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, {});
        flxProductFeature.add(lblName, lblGroup, lblType, flxEdit, lblSeparator);
        return flxProductFeature;
    }
})