define("flxContractsLimitsHeaderView", function() {
    return function(controller) {
        var flxContractsLimitsHeaderView = new kony.ui.FlexContainer({
            "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
            "clipBounds": true,
            "id": "flxContractsLimitsHeaderView",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "left": "0dp",
            "isModalContainer": false,
            "skin": "sknFlxFFFFFF1000",
            "top": "0dp",
            "width": "100%"
        }, {}, {});
        flxContractsLimitsHeaderView.setDefaultUnit(kony.flex.DP);
        var flxHeader = new kony.ui.FlexContainer({
            "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
            "clipBounds": true,
            "id": "flxHeader",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "left": "30px",
            "isModalContainer": false,
            "right": "20px",
            "skin": "slFbox",
            "top": "0px",
            "zIndex": 1
        }, {}, {});
        flxHeader.setDefaultUnit(kony.flex.DP);
        var flxActionDetails = new kony.ui.FlexContainer({
            "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
            "clipBounds": true,
            "id": "flxActionDetails",
            "isVisible": true,
            "layoutType": kony.flex.FLOW_VERTICAL,
            "left": "30dp",
            "isModalContainer": false,
            "skin": "slFbox",
            "top": "0dp",
            "width": "100%"
        }, {}, {});
        flxActionDetails.setDefaultUnit(kony.flex.DP);
        var flxRow1 = new kony.ui.FlexContainer({
            "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
            "centerX": "50%",
            "clipBounds": true,
            "id": "flxRow1",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "left": "0dp",
            "isModalContainer": false,
            "skin": "slFbox",
            "top": "12dp",
            "width": "100%"
        }, {}, {});
        flxRow1.setDefaultUnit(kony.flex.DP);
        var lblActionName = new kony.ui.Label({
            "id": "lblActionName",
            "isVisible": true,
            "left": "0px",
            "skin": "sknLatoRegular192B4516px",
            "text": "International Transfers",
            "top": "0",
            "width": "70%",
            "zIndex": 1
        }, {
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, {});
        var flxActionStatus = new kony.ui.FlexContainer({
            "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
            "clipBounds": true,
            "id": "flxActionStatus",
            "isVisible": true,
            "layoutType": kony.flex.FLOW_HORIZONTAL,
            "reverseLayoutDirection": false,
            "isModalContainer": false,
            "right": "20dp",
            "skin": "slFbox",
            "top": "0dp",
            "width": "10%"
        }, {}, {});
        flxActionStatus.setDefaultUnit(kony.flex.DP);
        var statusIcon = new kony.ui.Label({
            "id": "statusIcon",
            "isVisible": true,
            "right": "5dp",
            "skin": "sknFontIconActivate",
            "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmAlertsManagement.lblViewAlertStatusKey\")",
            "top": 2,
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, {
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, {});
        var statusValue = new kony.ui.Label({
            "id": "statusValue",
            "isVisible": true,
            "right": "20dp",
            "skin": "sknlblLato5bc06cBold14px",
            "i18n_text": "kony.i18n.getLocalizedString(\"i18n.secureimage.Active\")",
            "top": "0px",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, {
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, {});
        flxActionStatus.add(statusIcon, statusValue);
        flxRow1.add(lblActionName, flxActionStatus);
        var flxSelectedActions = new kony.ui.FlexContainer({
            "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
            "bottom": "10dp",
            "clipBounds": true,
            "id": "flxSelectedActions",
            "isVisible": true,
            "layoutType": kony.flex.FLOW_HORIZONTAL,
            "left": "0dp",
            "isModalContainer": false,
            "skin": "slFbox",
            "top": "10dp",
            "width": "50%"
        }, {}, {});
        flxSelectedActions.setDefaultUnit(kony.flex.DP);
        var lblAvailableActions = new kony.ui.Label({
            "id": "lblAvailableActions",
            "isVisible": true,
            "left": "0dp",
            "skin": "sknLbl485C75LatoRegular13Px",
            "text": "Selected Actions:",
            "top": "0dp",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, {
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, {});
        var lblCountActions = new kony.ui.Label({
            "id": "lblCountActions",
            "isVisible": true,
            "left": "10dp",
            "skin": "sknLbl192B45LatoSemiBold13px",
            "text": "2",
            "top": "0dp",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, {
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, {});
        var lblTotalActions = new kony.ui.Label({
            "id": "lblTotalActions",
            "isVisible": false,
            "left": "3dp",
            "skin": "sknLbl485C75LatoRegular13Px",
            "text": "of 2",
            "top": "0dp",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, {
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, {});
        flxSelectedActions.add(lblAvailableActions, lblCountActions, lblTotalActions);
        flxActionDetails.add(flxRow1, flxSelectedActions);
        var flxArrow = new kony.ui.FlexContainer({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "clipBounds": true,
            "height": "20dp",
            "id": "flxArrow",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "left": "5dp",
            "isModalContainer": false,
            "skin": "sknCursor",
            "top": "12dp",
            "width": "20dp"
        }, {}, {
            "hoverSkin": "sknCursor"
        });
        flxArrow.setDefaultUnit(kony.flex.DP);
        var lblArrow = new kony.ui.Label({
            "centerY": "50%",
            "id": "lblArrow",
            "isVisible": true,
            "left": "0",
            "skin": "sknIcon00000015px",
            "text": "",
            "width": kony.flex.USE_PREFFERED_SIZE
        }, {
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, {});
        flxArrow.add(lblArrow);
        flxHeader.add(flxActionDetails, flxArrow);
        var flxViewLimitsHeader = new kony.ui.FlexContainer({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "clipBounds": true,
            "height": "47px",
            "id": "flxViewLimitsHeader",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "left": "57px",
            "isModalContainer": false,
            "right": "20px",
            "skin": "slFbox",
            "top": "60px"
        }, {}, {});
        flxViewLimitsHeader.setDefaultUnit(kony.flex.DP);
        var lblActionHeader = new kony.ui.Label({
            "bottom": "15dp",
            "id": "lblActionHeader",
            "isVisible": true,
            "left": "0dp",
            "skin": "sknlblLatoReg485c7512px",
            "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmMfascenarios.ACTION_CAP\")",
            "top": "15dp",
            "width": "10%",
            "zIndex": 1
        }, {
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, {});
        var flxPerLimitHeader = new kony.ui.FlexContainer({
            "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
            "centerY": "50%",
            "clipBounds": true,
            "id": "flxPerLimitHeader",
            "isVisible": true,
            "layoutType": kony.flex.FLOW_HORIZONTAL,
            "left": "25%",
            "isModalContainer": false,
            "skin": "slFbox",
            "width": "20%"
        }, {}, {});
        flxPerLimitHeader.setDefaultUnit(kony.flex.DP);
        var lblPerLimitHeader = new kony.ui.Label({
            "bottom": "15dp",
            "id": "lblPerLimitHeader",
            "isVisible": true,
            "left": "0dp",
            "skin": "sknlblLatoReg485c7512px",
            "text": "PER TRANSACTION",
            "top": "15dp",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, {
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, {});
        var flxLimitInfo1 = new kony.ui.FlexContainer({
            "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
            "bottom": "15dp",
            "clipBounds": true,
            "id": "flxLimitInfo1",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "left": "10dp",
            "isModalContainer": false,
            "onClick": controller.AS_FlexContainer_aeb8168ea3624740bf06d583316c5596,
            "skin": "sknCursor",
            "top": "15dp",
            "width": "17dp",
            "zIndex": 2
        }, {}, {});
        flxLimitInfo1.setDefaultUnit(kony.flex.DP);
        var fontIconInfo1 = new kony.ui.Label({
            "id": "fontIconInfo1",
            "isVisible": true,
            "left": "0dp",
            "skin": "sknIcon18px",
            "text": "",
            "top": "0dp",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, {
            "contentAlignment": constants.CONTENT_ALIGN_TOP_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, {});
        flxLimitInfo1.add(fontIconInfo1);
        flxPerLimitHeader.add(lblPerLimitHeader, flxLimitInfo1);
        var flxDailyLimitHeader = new kony.ui.FlexContainer({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "centerY": "50%",
            "clipBounds": true,
            "height": "100%",
            "id": "flxDailyLimitHeader",
            "isVisible": true,
            "layoutType": kony.flex.FLOW_HORIZONTAL,
            "left": "53%",
            "isModalContainer": false,
            "skin": "slFbox",
            "width": "20%"
        }, {}, {});
        flxDailyLimitHeader.setDefaultUnit(kony.flex.DP);
        var lblDailyLimitHeader = new kony.ui.Label({
            "bottom": "15dp",
            "id": "lblDailyLimitHeader",
            "isVisible": true,
            "left": "0dp",
            "skin": "sknlblLatoReg485c7512px",
            "text": "DAILY TRANSACTION",
            "top": "15dp",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, {
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, {});
        var flxLimitInfo2 = new kony.ui.FlexContainer({
            "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
            "bottom": "15dp",
            "clipBounds": true,
            "id": "flxLimitInfo2",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "left": "10dp",
            "isModalContainer": false,
            "onClick": controller.AS_FlexContainer_aeb8168ea3624740bf06d583316c5596,
            "skin": "sknCursor",
            "top": "15dp",
            "width": "17dp",
            "zIndex": 2
        }, {}, {});
        flxLimitInfo2.setDefaultUnit(kony.flex.DP);
        var fontIconInfo2 = new kony.ui.Label({
            "id": "fontIconInfo2",
            "isVisible": true,
            "left": "0dp",
            "skin": "sknIcon18px",
            "text": "",
            "top": "0dp",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, {
            "contentAlignment": constants.CONTENT_ALIGN_TOP_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, {});
        flxLimitInfo2.add(fontIconInfo2);
        flxDailyLimitHeader.add(lblDailyLimitHeader, flxLimitInfo2);
        var flxWeeklyLimitHeader = new kony.ui.FlexContainer({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "centerY": "50%",
            "clipBounds": true,
            "height": "100%",
            "id": "flxWeeklyLimitHeader",
            "isVisible": true,
            "layoutType": kony.flex.FLOW_HORIZONTAL,
            "left": "78%",
            "isModalContainer": false,
            "skin": "slFbox",
            "width": "22%"
        }, {}, {});
        flxWeeklyLimitHeader.setDefaultUnit(kony.flex.DP);
        var lblWeeklyLimitHeader = new kony.ui.Label({
            "bottom": "15dp",
            "id": "lblWeeklyLimitHeader",
            "isVisible": true,
            "left": "0dp",
            "skin": "sknlblLatoReg485c7512px",
            "text": "WEEKLY TRANSACTION",
            "top": "15dp",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, {
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, {});
        var flxLimitInfo3 = new kony.ui.FlexContainer({
            "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
            "bottom": "15dp",
            "clipBounds": true,
            "id": "flxLimitInfo3",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "left": "10dp",
            "isModalContainer": false,
            "onClick": controller.AS_FlexContainer_aeb8168ea3624740bf06d583316c5596,
            "skin": "sknCursor",
            "top": "15dp",
            "width": "17dp",
            "zIndex": 2
        }, {}, {});
        flxLimitInfo3.setDefaultUnit(kony.flex.DP);
        var fontIconInfo3 = new kony.ui.Label({
            "id": "fontIconInfo3",
            "isVisible": true,
            "left": "0dp",
            "skin": "sknIcon18px",
            "text": "",
            "top": "0dp",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, {
            "contentAlignment": constants.CONTENT_ALIGN_TOP_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, {});
        flxLimitInfo3.add(fontIconInfo3);
        flxWeeklyLimitHeader.add(lblWeeklyLimitHeader, flxLimitInfo3);
        var lblFASeperator2 = new kony.ui.Label({
            "bottom": 0,
            "height": "1px",
            "id": "lblFASeperator2",
            "isVisible": true,
            "left": "0px",
            "right": "0px",
            "skin": "sknLblSeparatore7e7e7",
            "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmCSR.lblSeperator\")",
            "zIndex": 1
        }, {
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, {});
        flxViewLimitsHeader.add(lblActionHeader, flxPerLimitHeader, flxDailyLimitHeader, flxWeeklyLimitHeader, lblFASeperator2);
        var lblFASeperator1 = new kony.ui.Label({
            "height": "1px",
            "id": "lblFASeperator1",
            "isVisible": true,
            "left": "0px",
            "right": "0dp",
            "skin": "sknlblSeperatorD7D9E0",
            "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmCSR.lblSeperator\")",
            "top": "61dp",
            "zIndex": 1
        }, {
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, {});
        var lblLimitsSeperator3 = new kony.ui.Label({
            "height": "1px",
            "id": "lblLimitsSeperator3",
            "isVisible": true,
            "left": "0px",
            "right": "0dp",
            "skin": "sknlblSeperatorD7D9E0",
            "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmCSR.lblSeperator\")",
            "top": "0dp",
            "zIndex": 1
        }, {
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, {});
        flxContractsLimitsHeaderView.add(flxHeader, flxViewLimitsHeader, lblFASeperator1, lblLimitsSeperator3);
        return flxContractsLimitsHeaderView;
    }
})