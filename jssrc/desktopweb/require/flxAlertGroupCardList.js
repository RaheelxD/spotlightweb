define("flxAlertGroupCardList", function() {
    return function(controller) {
        var flxAlertGroupCardList = new kony.ui.FlexContainer({
            "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
            "clipBounds": true,
            "id": "flxAlertGroupCardList",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "left": "0dp",
            "isModalContainer": false,
            "skin": "sknFlxFFFFFF100O",
            "top": "0dp",
            "width": "100%"
        }, {}, {});
        flxAlertGroupCardList.setDefaultUnit(kony.flex.DP);
        var flxGroupCardListHoverContainer = new kony.ui.FlexContainer({
            "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
            "clipBounds": true,
            "id": "flxGroupCardListHoverContainer",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "left": "10dp",
            "isModalContainer": false,
            "right": "10dp",
            "skin": "slFbox",
            "top": "0dp"
        }, {}, {
            "hoverSkin": "sknfbfcfc"
        });
        flxGroupCardListHoverContainer.setDefaultUnit(kony.flex.DP);
        var lblAlertName = new kony.ui.Label({
            "bottom": "25dp",
            "id": "lblAlertName",
            "isVisible": true,
            "left": "30px",
            "skin": "sknLbl485C75LatoRegular13Px",
            "text": "Admin Role",
            "top": "15dp",
            "width": "17%",
            "zIndex": 1
        }, {
            "contentAlignment": constants.CONTENT_ALIGN_TOP_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, {});
        var lblAlertCode = new kony.ui.Label({
            "bottom": "15dp",
            "id": "lblAlertCode",
            "isVisible": true,
            "left": "21%",
            "skin": "sknLbl485C75LatoRegular13Px",
            "text": "Lorem ipsum dolor sit amet",
            "top": "15dp",
            "width": "18%",
            "zIndex": 1
        }, {
            "contentAlignment": constants.CONTENT_ALIGN_TOP_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, {});
        var lblFreqEnabled = new kony.ui.Label({
            "bottom": "15dp",
            "id": "lblFreqEnabled",
            "isVisible": true,
            "left": "70%",
            "skin": "sknLbl485C75LatoRegular13Px",
            "text": "Yes",
            "top": "15dp",
            "width": "10%",
            "zIndex": 1
        }, {
            "contentAlignment": constants.CONTENT_ALIGN_TOP_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, {});
        var flxStatus = new kony.ui.FlexContainer({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "bottom": "10dp",
            "clipBounds": true,
            "height": "20px",
            "id": "flxStatus",
            "isVisible": true,
            "layoutType": kony.flex.FLOW_HORIZONTAL,
            "reverseLayoutDirection": false,
            "left": "90%",
            "isModalContainer": false,
            "skin": "slFbox",
            "top": "12dp",
            "width": "100px",
            "zIndex": 1
        }, {}, {});
        flxStatus.setDefaultUnit(kony.flex.DP);
        var lblIconStatus = new kony.ui.Label({
            "centerY": "51%",
            "id": "lblIconStatus",
            "isVisible": true,
            "right": "5dp",
            "skin": "sknFontIconActivate",
            "text": "",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 2
        }, {
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, {});
        var lblAlertStatus = new kony.ui.Label({
            "centerY": "50%",
            "id": "lblAlertStatus",
            "isVisible": true,
            "right": "0dp",
            "skin": "sknlblLato5bc06cBold14px",
            "text": "Active",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, {
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, {});
        flxStatus.add(lblIconStatus, lblAlertStatus);
        var lblSeperator = new kony.ui.Label({
            "height": "1px",
            "id": "lblSeperator",
            "isVisible": true,
            "left": "0dp",
            "right": "0dp",
            "skin": "sknlblSeperator",
            "text": ".",
            "top": "0dp",
            "zIndex": 1
        }, {
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, {});
        var lblIcon = new kony.ui.Label({
            "bottom": "15px",
            "height": "20px",
            "id": "lblIcon",
            "isVisible": true,
            "left": "50%",
            "skin": "sknLblIcomoon20px485c75",
            "text": "",
            "top": "15px",
            "width": "20px",
            "zIndex": 1
        }, {
            "contentAlignment": constants.CONTENT_ALIGN_TOP_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, {});
        var flxTagsGroup = new kony.ui.FlexContainer({
            "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
            "bottom": "5dp",
            "clipBounds": true,
            "id": "flxTagsGroup",
            "isVisible": true,
            "layoutType": kony.flex.FLOW_HORIZONTAL,
            "left": "0dp",
            "isModalContainer": false,
            "skin": "slFbox",
            "width": "100%"
        }, {}, {});
        flxTagsGroup.setDefaultUnit(kony.flex.DP);
        var btnExt = new kony.ui.Button({
            "bottom": "0dp",
            "id": "btnExt",
            "isVisible": false,
            "left": 30,
            "skin": "sknbtnffffffLatoRegular12PxA94240",
            "text": "Button",
            "width": kony.flex.USE_PREFFERED_SIZE
        }, {
            "contentAlignment": constants.CONTENT_ALIGN_CENTER,
            "displayText": true,
            "padding": [1, 0, 1, 0],
            "paddingInPixel": false
        }, {});
        var btnAutoSubscribed = new kony.ui.Button({
            "bottom": "0dp",
            "id": "btnAutoSubscribed",
            "isVisible": false,
            "left": "15dp",
            "skin": "sknbtnffffffLatoRegularLightGreen",
            "text": "Auto Subscribed",
            "width": kony.flex.USE_PREFFERED_SIZE
        }, {
            "contentAlignment": constants.CONTENT_ALIGN_CENTER,
            "displayText": true,
            "padding": [1, 0, 1, 0],
            "paddingInPixel": false
        }, {});
        flxTagsGroup.add(btnExt, btnAutoSubscribed);
        flxGroupCardListHoverContainer.add(lblAlertName, lblAlertCode, lblFreqEnabled, flxStatus, lblSeperator, lblIcon, flxTagsGroup);
        flxAlertGroupCardList.add(flxGroupCardListHoverContainer);
        return flxAlertGroupCardList;
    }
})