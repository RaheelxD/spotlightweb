define("flxCustomerResults", function() {
    return function(controller) {
        var flxCustomerResults = new kony.ui.FlexContainer({
            "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
            "clipBounds": true,
            "id": "flxCustomerResults",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "left": "0dp",
            "isModalContainer": false,
            "skin": "slFbox",
            "top": "0dp",
            "width": "100%"
        }, {}, {});
        flxCustomerResults.setDefaultUnit(kony.flex.DP);
        var flxInnerContainer = new kony.ui.FlexContainer({
            "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
            "bottom": "15px",
            "clipBounds": true,
            "id": "flxInnerContainer",
            "isVisible": true,
            "layoutType": kony.flex.FLOW_VERTICAL,
            "left": "20dp",
            "isModalContainer": false,
            "right": "20dp",
            "skin": "sknFlxbgFFFFFFbdrD7D9E0rd3px",
            "top": "15px",
            "zIndex": 1
        }, {}, {
            "hoverSkin": "sknCustomerResultsSegrowHover"
        });
        flxInnerContainer.setDefaultUnit(kony.flex.DP);
        var flxUpperContainer = new kony.ui.FlexContainer({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "clipBounds": true,
            "height": "60dp",
            "id": "flxUpperContainer",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "left": "0dp",
            "isModalContainer": false,
            "skin": "slFbox",
            "top": "0dp",
            "width": "100%",
            "zIndex": 1
        }, {}, {});
        flxUpperContainer.setDefaultUnit(kony.flex.DP);
        var flxLeftContainer = new kony.ui.FlexContainer({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "clipBounds": true,
            "height": "100%",
            "id": "flxLeftContainer",
            "isVisible": true,
            "layoutType": kony.flex.FLOW_HORIZONTAL,
            "left": "20dp",
            "isModalContainer": false,
            "skin": "slFbox",
            "top": "0dp",
            "width": "67%",
            "zIndex": 1
        }, {}, {});
        flxLeftContainer.setDefaultUnit(kony.flex.DP);
        var flxRadioButton = new kony.ui.FlexContainer({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "centerY": "50%",
            "clipBounds": true,
            "height": "20px",
            "id": "flxRadioButton",
            "isVisible": true,
            "layoutType": kony.flex.FLOW_HORIZONTAL,
            "left": "0px",
            "isModalContainer": false,
            "skin": "slFbox",
            "width": "20px"
        }, {}, {});
        flxRadioButton.setDefaultUnit(kony.flex.DP);
        var imgRadioBtn = new kony.ui.Image2({
            "centerY": "50%",
            "height": "15dp",
            "id": "imgRadioBtn",
            "isVisible": true,
            "left": "0dp",
            "skin": "slImage",
            "src": "radio_notselected.png",
            "width": "15dp",
            "zIndex": 1
        }, {
            "imageScaleMode": constants.IMAGE_SCALE_MODE_MAINTAIN_ASPECT_RATIO,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, {});
        flxRadioButton.add(imgRadioBtn);
        var lblCustomerName = new kony.ui.Label({
            "centerY": "50%",
            "id": "lblCustomerName",
            "isVisible": true,
            "left": "4dp",
            "skin": "sknLatoRegular192B4518px",
            "text": "Bryann Nash",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, {
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, {});
        var flxRetailTag = new kony.ui.FlexContainer({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "centerY": "50%",
            "clipBounds": true,
            "height": "20px",
            "id": "flxRetailTag",
            "isVisible": true,
            "layoutType": kony.flex.FLOW_HORIZONTAL,
            "left": "20px",
            "isModalContainer": false,
            "skin": "sknflxCustomertagRed",
            "top": "0dp",
            "width": "77px"
        }, {}, {});
        flxRetailTag.setDefaultUnit(kony.flex.DP);
        var fontIconCircle1 = new kony.ui.Label({
            "centerY": "48%",
            "id": "fontIconCircle1",
            "isVisible": true,
            "left": "6px",
            "skin": "sknCircleCustomerTag",
            "i18n_text": "kony.i18n.getLocalizedString(\"i18n.userwidgetmodel.lblFontIconStatus1\")",
            "top": "0dp",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 2
        }, {
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, {});
        var lblContent1 = new kony.ui.Label({
            "centerY": "50%",
            "id": "lblContent1",
            "isVisible": true,
            "left": "3px",
            "skin": "sknCustomerTypeTag",
            "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmCustomerManagementController.RetailUser\")",
            "top": "0dp",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, {
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, {});
        flxRetailTag.add(fontIconCircle1, lblContent1);
        var flxApplicantTag = new kony.ui.FlexContainer({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "centerY": "50%",
            "clipBounds": true,
            "height": "20px",
            "id": "flxApplicantTag",
            "isVisible": true,
            "layoutType": kony.flex.FLOW_HORIZONTAL,
            "left": "20px",
            "isModalContainer": false,
            "skin": "sknflxCustomertagBlue",
            "top": "0dp",
            "width": "70px"
        }, {}, {});
        flxApplicantTag.setDefaultUnit(kony.flex.DP);
        var fontIconCircle2 = new kony.ui.Label({
            "centerY": "48%",
            "id": "fontIconCircle2",
            "isVisible": true,
            "left": "6px",
            "skin": "sknCircleCustomerTag",
            "i18n_text": "kony.i18n.getLocalizedString(\"i18n.userwidgetmodel.lblFontIconStatus1\")",
            "top": "0dp",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 2
        }, {
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, {});
        var lblContent2 = new kony.ui.Label({
            "centerY": "50%",
            "id": "lblContent2",
            "isVisible": true,
            "left": "3px",
            "skin": "sknCustomerTypeTag",
            "text": "Applicant",
            "top": "0dp",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, {
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, {});
        flxApplicantTag.add(fontIconCircle2, lblContent2);
        var flxSmallBusinessTag = new kony.ui.FlexContainer({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "centerY": "50%",
            "clipBounds": true,
            "height": "20px",
            "id": "flxSmallBusinessTag",
            "isVisible": false,
            "layoutType": kony.flex.FLOW_HORIZONTAL,
            "left": "20px",
            "isModalContainer": false,
            "skin": "sknflxCustomertagLiteGreen",
            "top": "0dp",
            "width": "125px"
        }, {}, {});
        flxSmallBusinessTag.setDefaultUnit(kony.flex.DP);
        var fontIconCircle3 = new kony.ui.Label({
            "centerY": "48%",
            "id": "fontIconCircle3",
            "isVisible": true,
            "left": "6px",
            "skin": "sknCircleCustomerTag",
            "i18n_text": "kony.i18n.getLocalizedString(\"i18n.userwidgetmodel.lblFontIconStatus1\")",
            "top": "0dp",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 2
        }, {
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, {});
        var lblContent3 = new kony.ui.Label({
            "centerY": "50%",
            "id": "lblContent3",
            "isVisible": true,
            "left": "3px",
            "skin": "sknCustomerTypeTag",
            "text": "Small Business User",
            "top": "0dp",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, {
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, {});
        flxSmallBusinessTag.add(fontIconCircle3, lblContent3);
        var flxMicroBusinessTag = new kony.ui.FlexContainer({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "centerY": "50%",
            "clipBounds": true,
            "height": "20px",
            "id": "flxMicroBusinessTag",
            "isVisible": true,
            "layoutType": kony.flex.FLOW_HORIZONTAL,
            "left": "20px",
            "isModalContainer": false,
            "skin": "sknflxCustomertagPurple",
            "top": "0dp",
            "width": "97px"
        }, {}, {});
        flxMicroBusinessTag.setDefaultUnit(kony.flex.DP);
        var fontIconCircle4 = new kony.ui.Label({
            "centerY": "48%",
            "id": "fontIconCircle4",
            "isVisible": true,
            "left": "6px",
            "skin": "sknCircleCustomerTag",
            "i18n_text": "kony.i18n.getLocalizedString(\"i18n.userwidgetmodel.lblFontIconStatus1\")",
            "top": "0dp",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 2
        }, {
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, {});
        var lblContent4 = new kony.ui.Label({
            "centerY": "50%",
            "id": "lblContent4",
            "isVisible": true,
            "left": "3px",
            "skin": "sknCustomerTypeTag",
            "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmCustomerProfile.BusinessUser\")",
            "top": "0dp",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, {
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, {});
        flxMicroBusinessTag.add(fontIconCircle4, lblContent4);
        var flxLeadTag = new kony.ui.FlexContainer({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "centerY": "50%",
            "clipBounds": true,
            "height": "20px",
            "id": "flxLeadTag",
            "isVisible": true,
            "layoutType": kony.flex.FLOW_HORIZONTAL,
            "left": "20px",
            "isModalContainer": false,
            "skin": "sknflxCustomertagDarkBlue",
            "top": "0dp",
            "width": "50px"
        }, {}, {});
        flxLeadTag.setDefaultUnit(kony.flex.DP);
        var fontIconCircle5 = new kony.ui.Label({
            "centerY": "48%",
            "id": "fontIconCircle5",
            "isVisible": true,
            "left": "6px",
            "skin": "sknCircleCustomerTag",
            "i18n_text": "kony.i18n.getLocalizedString(\"i18n.userwidgetmodel.lblFontIconStatus1\")",
            "top": "0dp",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 2
        }, {
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, {});
        var lblContent5 = new kony.ui.Label({
            "centerY": "50%",
            "id": "lblContent5",
            "isVisible": true,
            "left": "3px",
            "skin": "sknCustomerTypeTag",
            "text": "Lead",
            "top": "0dp",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, {
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, {});
        flxLeadTag.add(fontIconCircle5, lblContent5);
        flxLeftContainer.add(flxRadioButton, lblCustomerName, flxRetailTag, flxApplicantTag, flxSmallBusinessTag, flxMicroBusinessTag, flxLeadTag);
        var flxRightContainer = new kony.ui.FlexContainer({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "clipBounds": true,
            "height": "100%",
            "id": "flxRightContainer",
            "isVisible": true,
            "layoutType": kony.flex.FLOW_HORIZONTAL,
            "reverseLayoutDirection": true,
            "isModalContainer": false,
            "right": "0dp",
            "skin": "slFbox",
            "top": "0dp",
            "width": "30%",
            "zIndex": 1
        }, {}, {});
        flxRightContainer.setDefaultUnit(kony.flex.DP);
        var lblStatusValue = new kony.ui.Label({
            "centerY": "50%",
            "id": "lblStatusValue",
            "isVisible": true,
            "right": "20px",
            "skin": "sknLbl485C75LatoRegular13Px",
            "text": "Active",
            "top": "0px",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, {
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, {});
        var lblIconStatus = new kony.ui.Label({
            "bottom": "1px",
            "centerY": "50%",
            "height": "15px",
            "id": "lblIconStatus",
            "isVisible": true,
            "right": "5dp",
            "skin": "sknFontIconActivate",
            "text": "",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, {
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, {});
        flxRightContainer.add(lblStatusValue, lblIconStatus);
        flxUpperContainer.add(flxLeftContainer, flxRightContainer);
        var lblRowSeparator = new kony.ui.Label({
            "bottom": "0px",
            "height": "1px",
            "id": "lblRowSeparator",
            "isVisible": false,
            "left": 0,
            "right": 0,
            "skin": "sknConfigSeparator",
            "i18n_text": "kony.i18n.getLocalizedString(\"i18n.userwidgetmodel.lblHeaderSeparator\")",
            "width": "100%",
            "zIndex": 1
        }, {
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, {});
        var flxLowerContainer = new kony.ui.FlexContainer({
            "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
            "clipBounds": true,
            "id": "flxLowerContainer",
            "isVisible": true,
            "layoutType": kony.flex.FLOW_VERTICAL,
            "left": "0dp",
            "isModalContainer": false,
            "skin": "sknflxf5f6f8Op100",
            "top": "0dp",
            "width": "100%",
            "zIndex": 1
        }, {}, {});
        flxLowerContainer.setDefaultUnit(kony.flex.DP);
        var flxRow1 = new kony.ui.FlexContainer({
            "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
            "clipBounds": true,
            "id": "flxRow1",
            "isVisible": true,
            "layoutType": kony.flex.FLOW_HORIZONTAL,
            "left": "0dp",
            "isModalContainer": false,
            "skin": "slFbox",
            "top": "0dp",
            "width": "100%"
        }, {}, {});
        flxRow1.setDefaultUnit(kony.flex.DP);
        var flxDataContainer1 = new kony.ui.FlexContainer({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "clipBounds": true,
            "height": "50dp",
            "id": "flxDataContainer1",
            "isVisible": true,
            "layoutType": kony.flex.FLOW_VERTICAL,
            "left": "20dp",
            "isModalContainer": false,
            "skin": "slFbox",
            "top": "0dp",
            "width": "23%"
        }, {}, {});
        flxDataContainer1.setDefaultUnit(kony.flex.DP);
        var lblHeading1 = new kony.ui.Label({
            "id": "lblHeading1",
            "isVisible": true,
            "left": "0px",
            "right": "0px",
            "skin": "sknlblLato696c7311px",
            "i18n_text": "kony.i18n.getLocalizedString(\"i18n.permission.USERNAME\")",
            "top": "15px",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, {
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, {});
        var lblData1 = new kony.ui.Label({
            "id": "lblData1",
            "isVisible": true,
            "left": 0,
            "skin": "sknlbl485C75LatoSemiBold13px",
            "text": "TRN232422",
            "top": "5px",
            "width": "99%",
            "zIndex": 1
        }, {
            "contentAlignment": constants.CONTENT_ALIGN_TOP_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, {});
        flxDataContainer1.add(lblHeading1, lblData1);
        var flxDataContainer2 = new kony.ui.FlexContainer({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "clipBounds": true,
            "height": "50dp",
            "id": "flxDataContainer2",
            "isVisible": true,
            "layoutType": kony.flex.FLOW_VERTICAL,
            "left": "10px",
            "isModalContainer": false,
            "skin": "slFbox",
            "top": "0dp",
            "width": "23%"
        }, {}, {});
        flxDataContainer2.setDefaultUnit(kony.flex.DP);
        var lblHeading2 = new kony.ui.Label({
            "id": "lblHeading2",
            "isVisible": true,
            "left": "0px",
            "right": "0px",
            "skin": "sknlblLato696c7311px",
            "text": "SSN",
            "top": "15px",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, {
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, {});
        var lblData2 = new kony.ui.Label({
            "id": "lblData2",
            "isVisible": true,
            "left": 0,
            "skin": "sknlbl485C75LatoSemiBold13px",
            "text": "*****2212",
            "top": "5px",
            "width": "99%",
            "zIndex": 1
        }, {
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, {});
        flxDataContainer2.add(lblHeading2, lblData2);
        var flxDataContainer3 = new kony.ui.FlexContainer({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "clipBounds": true,
            "height": "50dp",
            "id": "flxDataContainer3",
            "isVisible": true,
            "layoutType": kony.flex.FLOW_VERTICAL,
            "left": "10px",
            "isModalContainer": false,
            "skin": "slFbox",
            "top": "0dp",
            "width": "23%"
        }, {}, {});
        flxDataContainer3.setDefaultUnit(kony.flex.DP);
        var lblHeading3 = new kony.ui.Label({
            "id": "lblHeading3",
            "isVisible": true,
            "left": "0px",
            "right": "0px",
            "skin": "sknlblLato696c7311px",
            "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmCustomerManagementController.CUSTOMER_ID\")",
            "top": "15px",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, {
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, {});
        var lblData3 = new kony.ui.Label({
            "id": "lblData3",
            "isVisible": true,
            "left": 0,
            "skin": "sknlbl485C75LatoSemiBold13px",
            "text": "02/12/1995",
            "top": "5dp",
            "width": "99%",
            "zIndex": 1
        }, {
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, {});
        flxDataContainer3.add(lblHeading3, lblData3);
        var flxDataContainer4 = new kony.ui.FlexContainer({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "clipBounds": true,
            "height": "50dp",
            "id": "flxDataContainer4",
            "isVisible": true,
            "layoutType": kony.flex.FLOW_VERTICAL,
            "left": "10px",
            "isModalContainer": false,
            "skin": "slFbox",
            "top": "0dp",
            "width": "23%"
        }, {}, {});
        flxDataContainer4.setDefaultUnit(kony.flex.DP);
        var lblHeading4 = new kony.ui.Label({
            "id": "lblHeading4",
            "isVisible": true,
            "left": "0px",
            "right": "0px",
            "skin": "sknlblLato696c7311px",
            "i18n_text": "kony.i18n.getLocalizedString(\"i18n.Applications.MobileNumberCAPS\")",
            "top": "15px",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, {
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, {});
        var lblData4 = new kony.ui.Label({
            "id": "lblData4",
            "isVisible": true,
            "left": 0,
            "skin": "sknlbl485C75LatoSemiBold13px",
            "text": "Active",
            "top": "5dp",
            "width": "99%",
            "zIndex": 1
        }, {
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, {});
        flxDataContainer4.add(lblHeading4, lblData4);
        flxRow1.add(flxDataContainer1, flxDataContainer2, flxDataContainer3, flxDataContainer4);
        var flxRow2 = new kony.ui.FlexContainer({
            "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
            "bottom": "10dp",
            "clipBounds": true,
            "id": "flxRow2",
            "isVisible": true,
            "layoutType": kony.flex.FLOW_HORIZONTAL,
            "left": "0dp",
            "isModalContainer": false,
            "skin": "slFbox",
            "top": "5dp",
            "width": "100%"
        }, {}, {});
        flxRow2.setDefaultUnit(kony.flex.DP);
        var flxDataContainer5 = new kony.ui.FlexContainer({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "clipBounds": true,
            "height": "50dp",
            "id": "flxDataContainer5",
            "isVisible": true,
            "layoutType": kony.flex.FLOW_VERTICAL,
            "left": "20dp",
            "isModalContainer": false,
            "skin": "slFbox",
            "top": "0dp",
            "width": "23%"
        }, {}, {});
        flxDataContainer5.setDefaultUnit(kony.flex.DP);
        var lblHeading5 = new kony.ui.Label({
            "id": "lblHeading5",
            "isVisible": true,
            "left": "0px",
            "right": "0px",
            "skin": "sknlblLato696c7311px",
            "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmCompanies.DOB\")",
            "top": "15px",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, {
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, {});
        var lblData5 = new kony.ui.Label({
            "id": "lblData5",
            "isVisible": true,
            "left": 0,
            "skin": "sknlbl485C75LatoSemiBold13px",
            "text": "Active",
            "top": "5px",
            "width": "99%",
            "zIndex": 1
        }, {
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, {});
        flxDataContainer5.add(lblHeading5, lblData5);
        var flxDataContainer6 = new kony.ui.FlexContainer({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "clipBounds": true,
            "height": "50dp",
            "id": "flxDataContainer6",
            "isVisible": true,
            "layoutType": kony.flex.FLOW_VERTICAL,
            "left": "10px",
            "isModalContainer": false,
            "skin": "slFbox",
            "top": "0dp",
            "width": "23%"
        }, {}, {});
        flxDataContainer6.setDefaultUnit(kony.flex.DP);
        var lblHeading6 = new kony.ui.Label({
            "id": "lblHeading6",
            "isVisible": true,
            "left": "0px",
            "right": "0px",
            "skin": "sknlblLato696c7311px",
            "i18n_text": "kony.i18n.getLocalizedString(\"i18n.permission.EMAILID\")",
            "top": "15px",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, {
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, {});
        var lblData6 = new kony.ui.Label({
            "id": "lblData6",
            "isVisible": true,
            "left": 0,
            "skin": "sknlbl485C75LatoSemiBold13px",
            "text": "Active",
            "top": "5px",
            "width": "99%",
            "zIndex": 1
        }, {
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, {});
        flxDataContainer6.add(lblHeading6, lblData6);
        var flxDataContainer7 = new kony.ui.FlexContainer({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "clipBounds": true,
            "height": "50dp",
            "id": "flxDataContainer7",
            "isVisible": true,
            "layoutType": kony.flex.FLOW_VERTICAL,
            "left": "10px",
            "isModalContainer": false,
            "skin": "slFbox",
            "top": "0dp",
            "width": "23%"
        }, {}, {});
        flxDataContainer7.setDefaultUnit(kony.flex.DP);
        var lblHeading7 = new kony.ui.Label({
            "id": "lblHeading7",
            "isVisible": true,
            "left": "0px",
            "right": "0px",
            "skin": "sknlblLato696c7311px",
            "i18n_text": "kony.i18n.getLocalizedString(\"i18n.permission.EMAILID\")",
            "top": "15px",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, {
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, {});
        var lblData7 = new kony.ui.Label({
            "id": "lblData7",
            "isVisible": true,
            "left": 0,
            "skin": "sknlbl485C75LatoSemiBold13px",
            "text": "Active",
            "top": "5px",
            "width": "99%",
            "zIndex": 1
        }, {
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, {});
        flxDataContainer7.add(lblHeading7, lblData7);
        var flxDataContainer8 = new kony.ui.FlexContainer({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "clipBounds": true,
            "height": "50dp",
            "id": "flxDataContainer8",
            "isVisible": false,
            "layoutType": kony.flex.FLOW_VERTICAL,
            "left": "20px",
            "isModalContainer": false,
            "skin": "slFbox",
            "top": "10dp",
            "width": "20%"
        }, {}, {});
        flxDataContainer8.setDefaultUnit(kony.flex.DP);
        var lblHeading8 = new kony.ui.Label({
            "id": "lblHeading8",
            "isVisible": true,
            "left": "0px",
            "right": "0px",
            "skin": "sknlblLato696c7311px",
            "i18n_text": "kony.i18n.getLocalizedString(\"i18n.permission.EMAILID\")",
            "top": "15px",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, {
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, {});
        var lblData8 = new kony.ui.Label({
            "id": "lblData8",
            "isVisible": true,
            "left": 0,
            "right": "0px",
            "skin": "sknlbl485C75LatoSemiBold13px",
            "text": "Active",
            "top": "3px",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, {
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, {});
        flxDataContainer8.add(lblHeading8, lblData8);
        flxRow2.add(flxDataContainer5, flxDataContainer6, flxDataContainer7, flxDataContainer8);
        flxLowerContainer.add(flxRow1, flxRow2);
        flxInnerContainer.add(flxUpperContainer, lblRowSeparator, flxLowerContainer);
        flxCustomerResults.add(flxInnerContainer);
        return flxCustomerResults;
    }
})