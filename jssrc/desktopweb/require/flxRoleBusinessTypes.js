define("flxRoleBusinessTypes", function() {
    return function(controller) {
        var flxRoleBusinessTypes = new kony.ui.FlexContainer({
            "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
            "clipBounds": true,
            "focusSkin": "sknflxffffffop100",
            "id": "flxRoleBusinessTypes",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "isModalContainer": false,
            "skin": "sknflxffffffop100"
        }, {}, {});
        flxRoleBusinessTypes.setDefaultUnit(kony.flex.DP);
        var lblDescription = new kony.ui.Label({
            "id": "lblDescription",
            "isVisible": true,
            "left": "20px",
            "skin": "sknLbl192B45LatoRegular14px",
            "text": "Trust",
            "top": "15dp",
            "width": "30%",
            "zIndex": 1
        }, {
            "contentAlignment": constants.CONTENT_ALIGN_TOP_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, {});
        var flxCheckboxSelect = new kony.ui.FlexContainer({
            "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
            "clipBounds": true,
            "id": "flxCheckboxSelect",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "left": "38%",
            "isModalContainer": false,
            "skin": "sknCursor",
            "top": "15dp",
            "width": "20px",
            "zIndex": 2
        }, {}, {});
        flxCheckboxSelect.setDefaultUnit(kony.flex.DP);
        var imgCheckBoxSelect = new kony.ui.Image2({
            "height": "12px",
            "id": "imgCheckBoxSelect",
            "isVisible": true,
            "left": "4px",
            "skin": "slImage",
            "src": "checkbox.png",
            "top": "0dp",
            "width": "12px",
            "zIndex": 2
        }, {
            "imageScaleMode": constants.IMAGE_SCALE_MODE_MAINTAIN_ASPECT_RATIO,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, {});
        flxCheckboxSelect.add(imgCheckBoxSelect);
        var flxCheckboxDefault = new kony.ui.FlexContainer({
            "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
            "clipBounds": true,
            "id": "flxCheckboxDefault",
            "isVisible": false,
            "layoutType": kony.flex.FREE_FORM,
            "left": "68%",
            "isModalContainer": false,
            "skin": "sknCursor",
            "top": "15dp",
            "width": "30px",
            "zIndex": 2
        }, {}, {});
        flxCheckboxDefault.setDefaultUnit(kony.flex.DP);
        var imgCheckBoxDefault = new kony.ui.Image2({
            "height": "12px",
            "id": "imgCheckBoxDefault",
            "isVisible": true,
            "left": "0px",
            "skin": "slImage",
            "src": "checkbox.png",
            "top": "0dp",
            "width": "12px",
            "zIndex": 2
        }, {
            "imageScaleMode": constants.IMAGE_SCALE_MODE_MAINTAIN_ASPECT_RATIO,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, {});
        flxCheckboxDefault.add(imgCheckBoxDefault);
        flxRoleBusinessTypes.add(lblDescription, flxCheckboxSelect, flxCheckboxDefault);
        return flxRoleBusinessTypes;
    }
})