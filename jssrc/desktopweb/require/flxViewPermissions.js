define("flxViewPermissions", function() {
    return function(controller) {
        var flxViewPermissions = new kony.ui.FlexContainer({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "clipBounds": true,
            "height": "50px",
            "id": "flxViewPermissions",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "isModalContainer": false,
            "skin": "sknflxffffffop100"
        }, {}, {
            "hoverSkin": "sknfbfcfc"
        });
        flxViewPermissions.setDefaultUnit(kony.flex.DP);
        var lblPermissionName = new kony.ui.Label({
            "centerY": "50%",
            "id": "lblPermissionName",
            "isVisible": true,
            "left": "10px",
            "skin": "sknlblLatoBold35475f14px",
            "text": "Admin Role",
            "width": "23%",
            "zIndex": 1
        }, {
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, {});
        var lblDescription = new kony.ui.Label({
            "centerY": "50%",
            "id": "lblDescription",
            "isVisible": true,
            "left": "30%",
            "skin": "sknlblLatoBold35475f14px",
            "text": "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do",
            "width": "63%",
            "zIndex": 1
        }, {
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, {});
        var lblSeperator = new kony.ui.Label({
            "height": "1px",
            "id": "lblSeperator",
            "isVisible": true,
            "left": "0dp",
            "skin": "sknlblSeperator",
            "text": ".",
            "top": "0dp",
            "width": "100%",
            "zIndex": 1
        }, {
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, {});
        var flxActionIcon = new kony.ui.FlexContainer({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "centerY": "50%",
            "clipBounds": true,
            "height": "40dp",
            "id": "flxActionIcon",
            "isVisible": false,
            "layoutType": kony.flex.FREE_FORM,
            "isModalContainer": false,
            "right": "15dp",
            "skin": "slFbox",
            "width": "40dp",
            "zIndex": 1
        }, {}, {
            "hoverSkin": "sknCursor"
        });
        flxActionIcon.setDefaultUnit(kony.flex.DP);
        var lblIconAction = new kony.ui.Label({
            "centerX": "50%",
            "centerY": "50%",
            "id": "lblIconAction",
            "isVisible": true,
            "skin": "sknIcon20px",
            "text": "",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, {
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, {});
        flxActionIcon.add(lblIconAction);
        flxViewPermissions.add(lblPermissionName, lblDescription, lblSeperator, flxActionIcon);
        return flxViewPermissions;
    }
})