define("flxDeviceAuthenticator", function() {
    return function(controller) {
        var flxDeviceAuthenticator = new kony.ui.FlexContainer({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "clipBounds": true,
            "height": "50px",
            "id": "flxDeviceAuthenticator",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "left": "0dp",
            "isModalContainer": false,
            "skin": "sknBackgroundFFFFFF",
            "top": "0dp",
            "width": "100%"
        }, {}, {});
        flxDeviceAuthenticator.setDefaultUnit(kony.flex.DP);
        var lblName = new kony.ui.Label({
            "bottom": "15px",
            "id": "lblName",
            "isVisible": true,
            "left": "10px",
            "skin": "sknLbl485C75LatoRegular13Px",
            "text": "Name",
            "top": "15px",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, {
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, {});
        var flxDeviceId = new kony.ui.FlexContainer({
            "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
            "bottom": "15px",
            "clipBounds": true,
            "id": "flxDeviceId",
            "isVisible": true,
            "layoutType": kony.flex.FLOW_HORIZONTAL,
            "left": "41%",
            "isModalContainer": false,
            "right": "0px",
            "skin": "slFbox",
            "top": "15px"
        }, {}, {});
        flxDeviceId.setDefaultUnit(kony.flex.DP);
        var lblDeviceId = new kony.ui.Label({
            "id": "lblDeviceId",
            "isVisible": true,
            "left": "0px",
            "skin": "sknLbl485C75LatoRegular13Px",
            "text": "Device Id",
            "top": "30%",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, {
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, {});
        var flxCopy = new kony.ui.FlexContainer({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "clipBounds": true,
            "height": "20px",
            "id": "flxCopy",
            "isVisible": false,
            "layoutType": kony.flex.FREE_FORM,
            "left": "10px",
            "isModalContainer": false,
            "onClick": controller.AS_FlexContainer_h43e332b32624c87a4361f632e581db3,
            "skin": "hoverhandSkin",
            "top": "10px",
            "width": "20px"
        }, {}, {});
        flxCopy.setDefaultUnit(kony.flex.DP);
        var lblCopy = new kony.ui.Label({
            "centerX": "50%",
            "centerY": "50%",
            "height": "20px",
            "id": "lblCopy",
            "isVisible": true,
            "skin": "sknIcon20px",
            "text": "",
            "width": "20px",
            "zIndex": 1
        }, {
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, {});
        flxCopy.add(lblCopy);
        flxDeviceId.add(lblDeviceId, flxCopy);
        var lblStatus = new kony.ui.Label({
            "height": "100%",
            "id": "lblStatus",
            "isVisible": true,
            "left": "80%",
            "skin": "sknlblLatoBold35475f14px",
            "i18n_text": "kony.i18n.getLocalizedString(\"i18n.roles.STATUS\")",
            "top": "0",
            "width": "9%",
            "zIndex": 1
        }, {
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, {});
        var fontIconStatusInfo = new kony.ui.Label({
            "centerY": "50%",
            "id": "fontIconStatusInfo",
            "isVisible": true,
            "left": "77%",
            "skin": "sknfontIcon13px",
            "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmFAQ.fonticonActiveSearch\")",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, {
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, {});
        var lblSeparator = new kony.ui.Label({
            "bottom": "0px",
            "height": "1px",
            "id": "lblSeparator",
            "isVisible": true,
            "left": "0px",
            "right": "0px",
            "skin": "sknlblSeperatorD7D9E0",
            "text": "Label",
            "zIndex": 2
        }, {
            "contentAlignment": constants.CONTENT_ALIGN_TOP_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, {});
        var flxOptions = new kony.ui.FlexContainer({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "centerY": "50%",
            "clipBounds": true,
            "height": "27dp",
            "id": "flxOptions",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "isModalContainer": false,
            "right": "15dp",
            "skin": "sknFlxBorffffff1pxRound",
            "top": "25dp",
            "width": "27px",
            "zIndex": 2
        }, {}, {
            "hoverSkin": "sknflxffffffop100Border424242Radius100px"
        });
        flxOptions.setDefaultUnit(kony.flex.DP);
        var lblOptions = new kony.ui.Label({
            "centerX": "50%",
            "centerY": "50%",
            "height": "15dp",
            "id": "lblOptions",
            "isVisible": true,
            "skin": "sknFontIconOptionMenu",
            "text": "",
            "width": "15dp",
            "zIndex": 1
        }, {
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, {});
        flxOptions.add(lblOptions);
        flxDeviceAuthenticator.add(lblName, flxDeviceId, lblStatus, fontIconStatusInfo, lblSeparator, flxOptions);
        return flxDeviceAuthenticator;
    }
})