define("flxSegRelatedCustList", function() {
    return function(controller) {
        var flxSegRelatedCustList = new kony.ui.FlexContainer({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "clipBounds": true,
            "height": "60dp",
            "id": "flxSegRelatedCustList",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "left": "0dp",
            "isModalContainer": false,
            "skin": "slFbox",
            "top": "0dp",
            "width": "100%"
        }, {}, {});
        flxSegRelatedCustList.setDefaultUnit(kony.flex.DP);
        var flxExistingUserListHeader = new kony.ui.FlexContainer({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "clipBounds": true,
            "height": "100%",
            "id": "flxExistingUserListHeader",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "left": "0dp",
            "isModalContainer": false,
            "skin": "sknflxffffffop100",
            "top": "0dp",
            "width": "100%",
            "zIndex": 1
        }, {}, {});
        flxExistingUserListHeader.setDefaultUnit(kony.flex.DP);
        var lblCustomRadioBox = new kony.ui.Label({
            "centerY": "50%",
            "height": "15dp",
            "id": "lblCustomRadioBox",
            "isVisible": false,
            "left": "25dp",
            "skin": "sknIcoMoon003E7513px",
            "width": "15dp",
            "zIndex": 2
        }, {
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, {});
        var imgRadioSelectionBox = new kony.ui.Image2({
            "centerY": "50%",
            "height": "15dp",
            "id": "imgRadioSelectionBox",
            "isVisible": true,
            "left": "25dp",
            "skin": "slImage",
            "src": "radio_notselected.png",
            "width": "15dp",
            "zIndex": 2
        }, {
            "imageScaleMode": constants.IMAGE_SCALE_MODE_MAINTAIN_ASPECT_RATIO,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, {});
        var flxExistingUserHeaderListInner = new kony.ui.FlexContainer({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "clipBounds": true,
            "height": "100%",
            "id": "flxExistingUserHeaderListInner",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "left": "50dp",
            "isModalContainer": false,
            "right": "20dp",
            "skin": "sknflxffffffop100",
            "top": "0dp",
            "zIndex": 1
        }, {}, {});
        flxExistingUserHeaderListInner.setDefaultUnit(kony.flex.DP);
        var flxExistingUserName = new kony.ui.FlexContainer({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "centerY": "50%",
            "clipBounds": true,
            "height": "60px",
            "id": "flxExistingUserName",
            "isVisible": true,
            "layoutType": kony.flex.FLOW_HORIZONTAL,
            "left": "0dp",
            "isModalContainer": false,
            "right": "60px",
            "skin": "slFbox",
            "width": "13%",
            "zIndex": 1
        }, {}, {});
        flxExistingUserName.setDefaultUnit(kony.flex.DP);
        var lblExistingUserName = new kony.ui.Label({
            "centerY": "50%",
            "id": "lblExistingUserName",
            "isVisible": true,
            "left": "0px",
            "skin": "sknLblLato485c7513px",
            "text": "John Bailey",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, {
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, {});
        flxExistingUserName.add(lblExistingUserName);
        var flxExistingUserCustId = new kony.ui.FlexContainer({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "centerY": "50%",
            "clipBounds": true,
            "height": "60px",
            "id": "flxExistingUserCustId",
            "isVisible": true,
            "layoutType": kony.flex.FLOW_HORIZONTAL,
            "left": "13%",
            "isModalContainer": false,
            "right": "60px",
            "skin": "slFbox",
            "width": "13%",
            "zIndex": 1
        }, {}, {});
        flxExistingUserCustId.setDefaultUnit(kony.flex.DP);
        var lblExistingUserCustId = new kony.ui.Label({
            "centerY": "50%",
            "id": "lblExistingUserCustId",
            "isVisible": true,
            "left": "0px",
            "skin": "sknLblLato485c7513px",
            "text": "1503",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, {
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, {});
        flxExistingUserCustId.add(lblExistingUserCustId);
        var flxExistingUserTaxId = new kony.ui.FlexContainer({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "centerY": "50%",
            "clipBounds": true,
            "height": "60px",
            "id": "flxExistingUserTaxId",
            "isVisible": true,
            "layoutType": kony.flex.FLOW_HORIZONTAL,
            "left": "26%",
            "isModalContainer": false,
            "right": "60px",
            "skin": "slFbox",
            "width": "13%",
            "zIndex": 1
        }, {}, {});
        flxExistingUserTaxId.setDefaultUnit(kony.flex.DP);
        var lblExistingUserTaxId = new kony.ui.Label({
            "centerY": "50%",
            "id": "lblExistingUserTaxId",
            "isVisible": true,
            "left": "0px",
            "skin": "sknLblLato485c7513px",
            "text": "233-05-6546",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, {
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, {});
        flxExistingUserTaxId.add(lblExistingUserTaxId);
        var flxExistingUserDOB = new kony.ui.FlexContainer({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "centerY": "50%",
            "clipBounds": true,
            "height": "60px",
            "id": "flxExistingUserDOB",
            "isVisible": true,
            "layoutType": kony.flex.FLOW_HORIZONTAL,
            "left": "39%",
            "isModalContainer": false,
            "right": "60px",
            "skin": "slFbox",
            "width": "13%",
            "zIndex": 1
        }, {}, {});
        flxExistingUserDOB.setDefaultUnit(kony.flex.DP);
        var lblExistingUserDOB = new kony.ui.Label({
            "centerY": "50%",
            "id": "lblExistingUserDOB",
            "isVisible": true,
            "left": "0px",
            "skin": "sknLblLato485c7513px",
            "text": "12-09-1990",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, {
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, {});
        flxExistingUserDOB.add(lblExistingUserDOB);
        var flxExistingUserPhoneNumber = new kony.ui.FlexContainer({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "centerY": "50%",
            "clipBounds": true,
            "height": "60px",
            "id": "flxExistingUserPhoneNumber",
            "isVisible": true,
            "layoutType": kony.flex.FLOW_HORIZONTAL,
            "left": "52%",
            "isModalContainer": false,
            "right": "60px",
            "skin": "slFbox",
            "width": "15%",
            "zIndex": 1
        }, {}, {});
        flxExistingUserPhoneNumber.setDefaultUnit(kony.flex.DP);
        var lblExistingUserPhoneNumber = new kony.ui.Label({
            "centerY": "50%",
            "id": "lblExistingUserPhoneNumber",
            "isVisible": true,
            "left": "0px",
            "skin": "sknLblLato485c7513px",
            "text": "+1 505-709-6785",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, {
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, {});
        flxExistingUserPhoneNumber.add(lblExistingUserPhoneNumber);
        var flxExistingUserEmailId = new kony.ui.FlexContainer({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "centerY": "50%",
            "clipBounds": true,
            "height": "60px",
            "id": "flxExistingUserEmailId",
            "isVisible": true,
            "layoutType": kony.flex.FLOW_HORIZONTAL,
            "left": "67%",
            "isModalContainer": false,
            "right": "60px",
            "skin": "slFbox",
            "width": "25%",
            "zIndex": 1
        }, {}, {});
        flxExistingUserEmailId.setDefaultUnit(kony.flex.DP);
        var lblExistingUserEmailId = new kony.ui.Label({
            "centerY": "50%",
            "id": "lblExistingUserEmailId",
            "isVisible": true,
            "left": "0px",
            "skin": "sknLblLato485c7513px",
            "text": "Johndoe@gmail.com",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, {
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, {});
        flxExistingUserEmailId.add(lblExistingUserEmailId);
        var lblReasonToDisable = new kony.ui.Label({
            "centerY": "50%",
            "height": "15dp",
            "id": "lblReasonToDisable",
            "isVisible": true,
            "right": "20dp",
            "skin": "sknIcomoon20px",
            "text": "",
            "width": "15dp",
            "zIndex": 2
        }, {
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, {});
        flxExistingUserHeaderListInner.add(flxExistingUserName, flxExistingUserCustId, flxExistingUserTaxId, flxExistingUserDOB, flxExistingUserPhoneNumber, flxExistingUserEmailId, lblReasonToDisable);
        var lblHeaderSeparator = new kony.ui.Label({
            "bottom": "0px",
            "centerX": "50%",
            "height": "1dp",
            "id": "lblHeaderSeparator",
            "isVisible": true,
            "left": "20dp",
            "right": "20dp",
            "skin": "sknlblSeperatorBgD5D9DD",
            "i18n_text": "kony.i18n.getLocalizedString(\"i18n.userwidgetmodel.lblHeaderSeparator\")",
            "zIndex": 2
        }, {
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, {});
        flxExistingUserListHeader.add(lblCustomRadioBox, imgRadioSelectionBox, flxExistingUserHeaderListInner, lblHeaderSeparator);
        flxSegRelatedCustList.add(flxExistingUserListHeader);
        return flxSegRelatedCustList;
    }
})