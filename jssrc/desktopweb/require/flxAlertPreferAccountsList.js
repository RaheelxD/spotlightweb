define("flxAlertPreferAccountsList", function() {
    return function(controller) {
        var flxAlertPreferAccountsList = new kony.ui.FlexContainer({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "clipBounds": true,
            "height": "45dp",
            "id": "flxAlertPreferAccountsList",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "left": "0dp",
            "isModalContainer": false,
            "skin": "sknFlxFFFFFF100O",
            "top": "0dp",
            "width": "100%"
        }, {}, {
            "hoverSkin": "sknFlxffffffCursorPointer"
        });
        flxAlertPreferAccountsList.setDefaultUnit(kony.flex.DP);
        var lblAccountNumValue = new kony.ui.Label({
            "centerY": "50%",
            "id": "lblAccountNumValue",
            "isVisible": true,
            "left": "0dp",
            "skin": "sknLatoRegular485c7514px",
            "text": "ACC010101084928323",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, {
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, {});
        var flxAccountStatus = new kony.ui.FlexContainer({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "clipBounds": true,
            "height": "45dp",
            "id": "flxAccountStatus",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "left": "40%",
            "isModalContainer": false,
            "skin": "slFbox",
            "top": "0dp",
            "width": "100dp",
            "zIndex": 1
        }, {}, {});
        flxAccountStatus.setDefaultUnit(kony.flex.DP);
        var lblIconEnabled = new kony.ui.Label({
            "centerY": "50%",
            "id": "lblIconEnabled",
            "isVisible": true,
            "left": "0dp",
            "skin": "sknFontIconActivate",
            "text": "",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, {
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, {});
        var lblEnabledValue = new kony.ui.Label({
            "centerY": "50%",
            "id": "lblEnabledValue",
            "isVisible": true,
            "left": "20dp",
            "skin": "sknLatoRegular485c7514px",
            "text": "Enabled",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, {
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, {});
        flxAccountStatus.add(lblIconEnabled, lblEnabledValue);
        var lblSeperator = new kony.ui.Label({
            "bottom": "0dp",
            "height": "1dp",
            "id": "lblSeperator",
            "isVisible": true,
            "left": "0dp",
            "right": "0dp",
            "skin": "sknlblSeperator",
            "text": "-",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, {
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, {});
        flxAlertPreferAccountsList.add(lblAccountNumValue, flxAccountStatus, lblSeperator);
        return flxAlertPreferAccountsList;
    }
})