define("flxDueDiligence", function() {
    return function(controller) {
        var flxDueDiligence = new kony.ui.FlexContainer({
            "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
            "clipBounds": true,
            "id": "flxDueDiligence",
            "isVisible": true,
            "layoutType": kony.flex.FLOW_VERTICAL,
            "left": "0dp",
            "isModalContainer": false,
            "skin": "sknFlxFFFFFF1000",
            "top": "0dp",
            "width": "100%"
        }, {}, {
            "hoverSkin": "sknfbfcfc"
        });
        flxDueDiligence.setDefaultUnit(kony.flex.DP);
        var flxSegHeader = new kony.ui.FlexContainer({
            "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
            "bottom": "10dp",
            "clipBounds": true,
            "focusSkin": "sknfbfcfc",
            "id": "flxSegHeader",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "left": "0dp",
            "isModalContainer": false,
            "skin": "slFbox",
            "top": "0dp",
            "width": "100%",
            "zIndex": 1
        }, {}, {});
        flxSegHeader.setDefaultUnit(kony.flex.DP);
        var lblCountry = new kony.ui.Label({
            "id": "lblCountry",
            "isVisible": true,
            "left": "10dp",
            "skin": "sknlblLatoRegular484b5213px",
            "text": "Antigua andBarboda",
            "top": "15dp",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, {
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, {});
        var lblCitizenship = new kony.ui.Label({
            "id": "lblCitizenship",
            "isVisible": true,
            "left": "24%",
            "skin": "sknlblLatoRegular484b5213px",
            "text": "CITIZENSHIP",
            "top": "15dp",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, {
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, {});
        var lblResidency = new kony.ui.Label({
            "id": "lblResidency",
            "isVisible": true,
            "left": "43%",
            "skin": "sknlblLatoRegular484b5213px",
            "text": "RESIDENCY",
            "top": "15dp",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, {
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, {});
        var lblStatutoryPeriod = new kony.ui.Label({
            "id": "lblStatutoryPeriod",
            "isVisible": true,
            "left": "70%",
            "skin": "sknlblLatoRegular484b5213px",
            "text": "RESIDENCY",
            "top": "15dp",
            "width": "14%",
            "zIndex": 1
        }, {
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, {});
        var flxOptions = new kony.ui.FlexContainer({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "clipBounds": true,
            "height": "27dp",
            "id": "flxOptions",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "left": "92%",
            "isModalContainer": false,
            "skin": "sknFlxBorffffff1pxRound",
            "top": "10dp",
            "width": "27px",
            "zIndex": 2
        }, {}, {
            "hoverSkin": "sknflxffffffop100Border424242Radius100px"
        });
        flxOptions.setDefaultUnit(kony.flex.DP);
        var imgOptions = new kony.ui.Image2({
            "centerX": "50%",
            "centerY": "50%",
            "height": "12dp",
            "id": "imgOptions",
            "isVisible": false,
            "left": 14,
            "skin": "slImage",
            "src": "img_edit_btn.png",
            "width": "6dp",
            "zIndex": 1
        }, {
            "imageScaleMode": constants.IMAGE_SCALE_MODE_MAINTAIN_ASPECT_RATIO,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, {});
        var lblOptions = new kony.ui.Label({
            "centerY": "50%",
            "id": "lblOptions",
            "isVisible": true,
            "left": "5dp",
            "skin": "sknFontIconOptionMenuRow",
            "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmFAQ.fonticonEdit\")",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, {
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, {});
        flxOptions.add(imgOptions, lblOptions);
        flxSegHeader.add(lblCountry, lblCitizenship, lblResidency, lblStatutoryPeriod, flxOptions);
        var lblSeperator = new kony.ui.Label({
            "bottom": "0px",
            "height": "1px",
            "id": "lblSeperator",
            "isVisible": true,
            "left": "0dp",
            "skin": "sknlblSeperator",
            "width": "100%",
            "zIndex": 1
        }, {
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, {});
        flxDueDiligence.add(flxSegHeader, lblSeperator);
        return flxDueDiligence;
    }
})