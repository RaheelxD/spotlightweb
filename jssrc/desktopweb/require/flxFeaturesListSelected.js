define("flxFeaturesListSelected", function() {
    return function(controller) {
        var flxFeaturesListSelected = new kony.ui.FlexContainer({
            "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
            "clipBounds": true,
            "id": "flxFeaturesListSelected",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "left": "0dp",
            "isModalContainer": false,
            "skin": "slFbox",
            "top": "0dp",
            "width": "100%"
        }, {}, {});
        flxFeaturesListSelected.setDefaultUnit(kony.flex.DP);
        var flxFeatureNameContainer = new kony.ui.FlexContainer({
            "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
            "clipBounds": true,
            "id": "flxFeatureNameContainer",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "left": "0dp",
            "isModalContainer": false,
            "right": "29px",
            "skin": "slFbox",
            "top": "0dp",
            "width": "100%",
            "zIndex": 3
        }, {}, {});
        flxFeatureNameContainer.setDefaultUnit(kony.flex.DP);
        var flxGrouping2 = new kony.ui.FlexContainer({
            "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
            "clipBounds": true,
            "id": "flxGrouping2",
            "isVisible": true,
            "layoutType": kony.flex.FLOW_HORIZONTAL,
            "left": "0dp",
            "isModalContainer": false,
            "skin": "slFbox",
            "top": "0dp",
            "width": "100%"
        }, {}, {});
        flxGrouping2.setDefaultUnit(kony.flex.DP);
        var flxFeatureCheckbox = new kony.ui.FlexContainer({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "clipBounds": true,
            "height": "15dp",
            "id": "flxFeatureCheckbox",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "left": "10dp",
            "isModalContainer": false,
            "skin": "slFbox",
            "top": "15dp",
            "width": "15dp",
            "zIndex": 1
        }, {}, {});
        flxFeatureCheckbox.setDefaultUnit(kony.flex.DP);
        var imgCheckBox = new kony.ui.Image2({
            "centerX": "50%",
            "centerY": "50%",
            "height": "100%",
            "id": "imgCheckBox",
            "isVisible": true,
            "skin": "slImage",
            "src": "checkboxnormal.png",
            "width": "100%",
            "zIndex": 1
        }, {
            "imageScaleMode": constants.IMAGE_SCALE_MODE_MAINTAIN_ASPECT_RATIO,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, {});
        flxFeatureCheckbox.add(imgCheckBox);
        var flxGrouping1 = new kony.ui.FlexContainer({
            "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
            "clipBounds": true,
            "id": "flxGrouping1",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "left": "15dp",
            "isModalContainer": false,
            "skin": "slFbox",
            "top": "0dp",
            "width": "100%"
        }, {}, {});
        flxGrouping1.setDefaultUnit(kony.flex.DP);
        var flxMain1 = new kony.ui.FlexContainer({
            "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
            "bottom": "15dp",
            "clipBounds": true,
            "id": "flxMain1",
            "isVisible": true,
            "layoutType": kony.flex.FLOW_HORIZONTAL,
            "left": "0dp",
            "isModalContainer": false,
            "skin": "slFbox",
            "top": "40dp",
            "width": "97%",
            "zIndex": 1
        }, {}, {});
        flxMain1.setDefaultUnit(kony.flex.DP);
        var flxAction1 = new kony.ui.FlexContainer({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "clipBounds": true,
            "height": "20dp",
            "id": "flxAction1",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "left": "0dp",
            "isModalContainer": false,
            "skin": "sknFlxbgFFFFFFbdrD7D9E0rd3px",
            "top": "0dp",
            "width": "110dp",
            "zIndex": 1
        }, {}, {});
        flxAction1.setDefaultUnit(kony.flex.DP);
        var lblAction1 = new kony.ui.Label({
            "centerY": "45%",
            "id": "lblAction1",
            "isVisible": true,
            "left": "17dp",
            "skin": "sknlblLatoReg12px485C75",
            "text": "View",
            "top": "0dp",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, {
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, {});
        var fontIconCross1 = new kony.ui.Label({
            "centerY": "50%",
            "id": "fontIconCross1",
            "isVisible": true,
            "right": "5dp",
            "skin": "sknFontIconSearchCross16px",
            "i18n_text": "kony.i18n.getLocalizedString(\"i18n.userwidgetmodel.lblClearSearch\")",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, {
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, {});
        var lblIcon1 = new kony.ui.Label({
            "centerY": "52%",
            "height": "12dp",
            "id": "lblIcon1",
            "isVisible": true,
            "left": "2dp",
            "skin": "sknFontIconActivate",
            "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmAlertsManagement.lblViewAlertStatusKey\")",
            "width": "12dp",
            "zIndex": 1
        }, {
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, {});
        flxAction1.add(lblAction1, fontIconCross1, lblIcon1);
        var flxAction2 = new kony.ui.FlexContainer({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "clipBounds": true,
            "height": "20dp",
            "id": "flxAction2",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "left": "10dp",
            "isModalContainer": false,
            "skin": "sknFlxbgFFFFFFbdrD7D9E0rd3px",
            "top": "0dp",
            "width": "110dp",
            "zIndex": 1
        }, {}, {});
        flxAction2.setDefaultUnit(kony.flex.DP);
        var lblAction2 = new kony.ui.Label({
            "centerY": "45%",
            "id": "lblAction2",
            "isVisible": true,
            "left": "17dp",
            "skin": "sknlblLatoReg12px485C75",
            "text": "Create",
            "top": "0dp",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, {
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, {});
        var fontIconCross2 = new kony.ui.Label({
            "centerY": "50%",
            "id": "fontIconCross2",
            "isVisible": true,
            "right": "5dp",
            "skin": "sknFontIconSearchCross16px",
            "i18n_text": "kony.i18n.getLocalizedString(\"i18n.userwidgetmodel.lblClearSearch\")",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, {
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, {});
        var lblIcon2 = new kony.ui.Label({
            "centerY": "52%",
            "height": "12dp",
            "id": "lblIcon2",
            "isVisible": true,
            "left": "2dp",
            "skin": "sknFontIconActivate",
            "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmAlertsManagement.lblViewAlertStatusKey\")",
            "width": "12dp",
            "zIndex": 1
        }, {
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, {});
        flxAction2.add(lblAction2, fontIconCross2, lblIcon2);
        var flxAction3 = new kony.ui.FlexContainer({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "clipBounds": true,
            "height": "20dp",
            "id": "flxAction3",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "left": "10dp",
            "isModalContainer": false,
            "skin": "sknFlxbgFFFFFFbdrD7D9E0rd3px",
            "top": "0dp",
            "width": "110dp",
            "zIndex": 1
        }, {}, {});
        flxAction3.setDefaultUnit(kony.flex.DP);
        var lblAction3 = new kony.ui.Label({
            "centerY": "45%",
            "id": "lblAction3",
            "isVisible": true,
            "left": "17dp",
            "skin": "sknlblLatoReg12px485C75",
            "text": "Approve",
            "top": "0dp",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, {
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, {});
        var fontIconCross3 = new kony.ui.Label({
            "centerY": "50%",
            "id": "fontIconCross3",
            "isVisible": true,
            "right": "5dp",
            "skin": "sknFontIconSearchCross16px",
            "i18n_text": "kony.i18n.getLocalizedString(\"i18n.userwidgetmodel.lblClearSearch\")",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, {
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, {});
        var lblIcon3 = new kony.ui.Label({
            "centerY": "52%",
            "height": "12dp",
            "id": "lblIcon3",
            "isVisible": true,
            "left": "2dp",
            "skin": "sknFontIconActivate",
            "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmAlertsManagement.lblViewAlertStatusKey\")",
            "width": "12dp",
            "zIndex": 1
        }, {
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, {});
        flxAction3.add(lblAction3, fontIconCross3, lblIcon3);
        var flxAction4 = new kony.ui.FlexContainer({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "clipBounds": true,
            "height": "20dp",
            "id": "flxAction4",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "left": "10dp",
            "isModalContainer": false,
            "skin": "sknFlxbgFFFFFFbdrD7D9E0rd3px",
            "top": "0dp",
            "width": "110dp",
            "zIndex": 1
        }, {}, {});
        flxAction4.setDefaultUnit(kony.flex.DP);
        var lblAction4 = new kony.ui.Label({
            "centerY": "45%",
            "id": "lblAction4",
            "isVisible": true,
            "left": "17dp",
            "skin": "sknlblLatoReg12px485C75",
            "text": "Delete",
            "top": "0dp",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, {
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, {});
        var fontIconCross4 = new kony.ui.Label({
            "centerY": "50%",
            "id": "fontIconCross4",
            "isVisible": true,
            "right": "5dp",
            "skin": "sknFontIconSearchCross16px",
            "i18n_text": "kony.i18n.getLocalizedString(\"i18n.userwidgetmodel.lblClearSearch\")",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, {
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, {});
        var lblIcon4 = new kony.ui.Label({
            "centerY": "52%",
            "height": "12dp",
            "id": "lblIcon4",
            "isVisible": true,
            "left": "2dp",
            "skin": "sknFontIconActivate",
            "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmAlertsManagement.lblViewAlertStatusKey\")",
            "width": "12dp",
            "zIndex": 1
        }, {
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, {});
        flxAction4.add(lblAction4, fontIconCross4, lblIcon4);
        var flxAction5 = new kony.ui.FlexContainer({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "clipBounds": true,
            "height": "20dp",
            "id": "flxAction5",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "left": "10dp",
            "isModalContainer": false,
            "skin": "sknFlxbgFFFFFFbdrD7D9E0rd3px",
            "top": "0dp",
            "width": "110dp",
            "zIndex": 1
        }, {}, {});
        flxAction5.setDefaultUnit(kony.flex.DP);
        var lblAction5 = new kony.ui.Label({
            "centerY": "45%",
            "id": "lblAction5",
            "isVisible": true,
            "left": "17dp",
            "skin": "sknlblLatoReg12px485C75",
            "text": "Delete",
            "top": "0dp",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, {
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, {});
        var fontIconCross5 = new kony.ui.Label({
            "centerY": "50%",
            "id": "fontIconCross5",
            "isVisible": true,
            "right": "5dp",
            "skin": "sknFontIconSearchCross16px",
            "i18n_text": "kony.i18n.getLocalizedString(\"i18n.userwidgetmodel.lblClearSearch\")",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, {
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, {});
        var lblIcon5 = new kony.ui.Label({
            "centerY": "52%",
            "height": "12dp",
            "id": "lblIcon5",
            "isVisible": true,
            "left": "2dp",
            "skin": "sknFontIconActivate",
            "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmAlertsManagement.lblViewAlertStatusKey\")",
            "width": "12dp",
            "zIndex": 1
        }, {
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, {});
        flxAction5.add(lblAction5, fontIconCross5, lblIcon5);
        var flxRemove1 = new kony.ui.FlexContainer({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "clipBounds": true,
            "height": "20dp",
            "id": "flxRemove1",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "left": "10dp",
            "isModalContainer": false,
            "skin": "sknFlxbgFFFFFFbdrD7D9E0rd3px",
            "top": "0dp",
            "width": "90dp",
            "zIndex": 1
        }, {}, {});
        flxRemove1.setDefaultUnit(kony.flex.DP);
        var lblRemove1 = new kony.ui.Label({
            "centerY": "50%",
            "id": "lblRemove1",
            "isVisible": true,
            "left": "17dp",
            "skin": "sknlblLatoReg12px485C75",
            "text": "Remove All",
            "top": "0dp",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, {
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, {});
        var lblIconRemove1 = new kony.ui.Label({
            "centerY": "52%",
            "height": "12dp",
            "id": "lblIconRemove1",
            "isVisible": true,
            "left": "2dp",
            "skin": "sknFontIconActivate",
            "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmAlertsManagement.lblViewAlertStatusKey\")",
            "width": "12dp",
            "zIndex": 1
        }, {
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, {});
        flxRemove1.add(lblRemove1, lblIconRemove1);
        var btnView1 = new kony.ui.Button({
            "height": "20dp",
            "id": "btnView1",
            "isVisible": false,
            "left": "11dp",
            "skin": "sknBtn12pxfnt117EB0",
            "text": "View More",
            "top": "0dp",
            "width": "100dp",
            "zIndex": 1
        }, {
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "displayText": true,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, {});
        flxMain1.add(flxAction1, flxAction2, flxAction3, flxAction4, flxAction5, flxRemove1, btnView1);
        var lblFeatureName = new kony.ui.Label({
            "id": "lblFeatureName",
            "isVisible": true,
            "left": "0dp",
            "skin": "sknLbl192B45LatoRegular14px",
            "text": "Bill Payments",
            "top": "15dp",
            "width": "75%",
            "zIndex": 1
        }, {
            "contentAlignment": constants.CONTENT_ALIGN_TOP_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, {});
        var flxMain2 = new kony.ui.FlexContainer({
            "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
            "bottom": "15dp",
            "clipBounds": true,
            "id": "flxMain2",
            "isVisible": false,
            "layoutType": kony.flex.FLOW_HORIZONTAL,
            "left": "0dp",
            "isModalContainer": false,
            "skin": "slFbox",
            "top": "65dp",
            "width": "97%",
            "zIndex": 1
        }, {}, {});
        flxMain2.setDefaultUnit(kony.flex.DP);
        var flxAction6 = new kony.ui.FlexContainer({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "clipBounds": true,
            "height": "20dp",
            "id": "flxAction6",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "left": "0dp",
            "isModalContainer": false,
            "skin": "sknFlxbgFFFFFFbdrD7D9E0rd3px",
            "top": "0dp",
            "width": "110dp",
            "zIndex": 1
        }, {}, {});
        flxAction6.setDefaultUnit(kony.flex.DP);
        var lblAction6 = new kony.ui.Label({
            "centerY": "45%",
            "id": "lblAction6",
            "isVisible": true,
            "left": "17dp",
            "skin": "sknlblLatoReg12px485C75",
            "text": "Bulk- Approve",
            "top": "0dp",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, {
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, {});
        var fontIconCross6 = new kony.ui.Label({
            "centerY": "50%",
            "id": "fontIconCross6",
            "isVisible": true,
            "right": "5dp",
            "skin": "sknFontIconSearchCross16px",
            "i18n_text": "kony.i18n.getLocalizedString(\"i18n.userwidgetmodel.lblClearSearch\")",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, {
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, {});
        var lblIcon6 = new kony.ui.Label({
            "centerY": "52%",
            "height": "12dp",
            "id": "lblIcon6",
            "isVisible": true,
            "left": "2dp",
            "skin": "sknFontIconActivate",
            "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmAlertsManagement.lblViewAlertStatusKey\")",
            "width": "12dp",
            "zIndex": 1
        }, {
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, {});
        flxAction6.add(lblAction6, fontIconCross6, lblIcon6);
        var flxAction7 = new kony.ui.FlexContainer({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "clipBounds": true,
            "height": "20dp",
            "id": "flxAction7",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "left": "10dp",
            "isModalContainer": false,
            "skin": "sknFlxbgFFFFFFbdrD7D9E0rd3px",
            "top": "0dp",
            "width": "110dp",
            "zIndex": 1
        }, {}, {});
        flxAction7.setDefaultUnit(kony.flex.DP);
        var lblAction7 = new kony.ui.Label({
            "centerY": "45%",
            "id": "lblAction7",
            "isVisible": true,
            "left": "17dp",
            "skin": "sknlblLatoReg12px485C75",
            "text": "Create",
            "top": "0dp",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, {
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, {});
        var fontIconCross7 = new kony.ui.Label({
            "centerY": "50%",
            "id": "fontIconCross7",
            "isVisible": true,
            "right": "5dp",
            "skin": "sknFontIconSearchCross16px",
            "i18n_text": "kony.i18n.getLocalizedString(\"i18n.userwidgetmodel.lblClearSearch\")",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, {
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, {});
        var lblIcon7 = new kony.ui.Label({
            "centerY": "52%",
            "height": "12dp",
            "id": "lblIcon7",
            "isVisible": true,
            "left": "2dp",
            "skin": "sknFontIconActivate",
            "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmAlertsManagement.lblViewAlertStatusKey\")",
            "width": "12dp",
            "zIndex": 1
        }, {
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, {});
        flxAction7.add(lblAction7, fontIconCross7, lblIcon7);
        var flxAction8 = new kony.ui.FlexContainer({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "clipBounds": true,
            "height": "20dp",
            "id": "flxAction8",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "left": "10dp",
            "isModalContainer": false,
            "skin": "sknFlxbgFFFFFFbdrD7D9E0rd3px",
            "top": "0dp",
            "width": "110dp",
            "zIndex": 1
        }, {}, {});
        flxAction8.setDefaultUnit(kony.flex.DP);
        var lblAction8 = new kony.ui.Label({
            "centerY": "45%",
            "id": "lblAction8",
            "isVisible": true,
            "left": "17dp",
            "skin": "sknlblLatoReg12px485C75",
            "text": "Approve",
            "top": "0dp",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, {
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, {});
        var fontIconCross8 = new kony.ui.Label({
            "centerY": "50%",
            "id": "fontIconCross8",
            "isVisible": true,
            "right": "5dp",
            "skin": "sknFontIconSearchCross16px",
            "i18n_text": "kony.i18n.getLocalizedString(\"i18n.userwidgetmodel.lblClearSearch\")",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, {
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, {});
        var lblIcon8 = new kony.ui.Label({
            "centerY": "52%",
            "height": "12dp",
            "id": "lblIcon8",
            "isVisible": true,
            "left": "2dp",
            "skin": "sknFontIconActivate",
            "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmAlertsManagement.lblViewAlertStatusKey\")",
            "width": "12dp",
            "zIndex": 1
        }, {
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, {});
        flxAction8.add(lblAction8, fontIconCross8, lblIcon8);
        var flxAction9 = new kony.ui.FlexContainer({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "clipBounds": true,
            "height": "20dp",
            "id": "flxAction9",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "left": "10dp",
            "isModalContainer": false,
            "skin": "sknFlxbgFFFFFFbdrD7D9E0rd3px",
            "top": "0dp",
            "width": "110dp",
            "zIndex": 1
        }, {}, {});
        flxAction9.setDefaultUnit(kony.flex.DP);
        var lblAction9 = new kony.ui.Label({
            "centerY": "45%",
            "id": "lblAction9",
            "isVisible": true,
            "left": "17dp",
            "skin": "sknlblLatoReg12px485C75",
            "text": "Delete",
            "top": "0dp",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, {
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, {});
        var fontIconCross9 = new kony.ui.Label({
            "centerY": "50%",
            "id": "fontIconCross9",
            "isVisible": true,
            "right": "5dp",
            "skin": "sknFontIconSearchCross16px",
            "i18n_text": "kony.i18n.getLocalizedString(\"i18n.userwidgetmodel.lblClearSearch\")",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, {
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, {});
        var lblIcon9 = new kony.ui.Label({
            "centerY": "52%",
            "height": "12dp",
            "id": "lblIcon9",
            "isVisible": true,
            "left": "2dp",
            "skin": "sknFontIconActivate",
            "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmAlertsManagement.lblViewAlertStatusKey\")",
            "width": "12dp",
            "zIndex": 1
        }, {
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, {});
        flxAction9.add(lblAction9, fontIconCross9, lblIcon9);
        var flxAction10 = new kony.ui.FlexContainer({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "clipBounds": true,
            "height": "20dp",
            "id": "flxAction10",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "left": "10dp",
            "isModalContainer": false,
            "skin": "sknFlxbgFFFFFFbdrD7D9E0rd3px",
            "top": "0dp",
            "width": "110dp",
            "zIndex": 1
        }, {}, {});
        flxAction10.setDefaultUnit(kony.flex.DP);
        var lblAction10 = new kony.ui.Label({
            "centerY": "45%",
            "id": "lblAction10",
            "isVisible": true,
            "left": "17dp",
            "skin": "sknlblLatoReg12px485C75",
            "text": "Delete",
            "top": "0dp",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, {
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, {});
        var fontIconCross10 = new kony.ui.Label({
            "centerY": "50%",
            "id": "fontIconCross10",
            "isVisible": true,
            "right": "5dp",
            "skin": "sknFontIconSearchCross16px",
            "i18n_text": "kony.i18n.getLocalizedString(\"i18n.userwidgetmodel.lblClearSearch\")",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, {
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, {});
        var lblIcon10 = new kony.ui.Label({
            "centerY": "52%",
            "height": "12dp",
            "id": "lblIcon10",
            "isVisible": true,
            "left": "2dp",
            "skin": "sknFontIconActivate",
            "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmAlertsManagement.lblViewAlertStatusKey\")",
            "width": "12dp",
            "zIndex": 1
        }, {
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, {});
        flxAction10.add(lblAction10, fontIconCross10, lblIcon10);
        var flxRemove2 = new kony.ui.FlexContainer({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "clipBounds": true,
            "height": "20dp",
            "id": "flxRemove2",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "left": "10dp",
            "isModalContainer": false,
            "skin": "sknFlxbgFFFFFFbdrD7D9E0rd3px",
            "top": "0dp",
            "width": "90dp",
            "zIndex": 1
        }, {}, {});
        flxRemove2.setDefaultUnit(kony.flex.DP);
        var lblRemove2 = new kony.ui.Label({
            "centerY": "50%",
            "id": "lblRemove2",
            "isVisible": true,
            "left": "17dp",
            "skin": "sknlblLatoReg12px485C75",
            "text": "Remove All",
            "top": "0dp",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, {
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, {});
        var lblIconRemove2 = new kony.ui.Label({
            "centerY": "52%",
            "height": "12dp",
            "id": "lblIconRemove2",
            "isVisible": true,
            "left": "2dp",
            "skin": "sknFontIconActivate",
            "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmAlertsManagement.lblViewAlertStatusKey\")",
            "width": "12dp",
            "zIndex": 1
        }, {
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, {});
        flxRemove2.add(lblRemove2, lblIconRemove2);
        var btnView2 = new kony.ui.Button({
            "height": "20dp",
            "id": "btnView2",
            "isVisible": false,
            "left": "11dp",
            "skin": "sknBtn12pxfnt117EB0",
            "text": "View Less",
            "top": "0dp",
            "width": "100dp",
            "zIndex": 1
        }, {
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "displayText": true,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, {});
        flxMain2.add(flxAction6, flxAction7, flxAction8, flxAction9, flxAction10, flxRemove2, btnView2);
        var flxMain3 = new kony.ui.FlexContainer({
            "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
            "bottom": "15dp",
            "clipBounds": true,
            "id": "flxMain3",
            "isVisible": false,
            "layoutType": kony.flex.FLOW_HORIZONTAL,
            "left": "0dp",
            "isModalContainer": false,
            "skin": "slFbox",
            "top": "90dp",
            "width": "97%",
            "zIndex": 1
        }, {}, {});
        flxMain3.setDefaultUnit(kony.flex.DP);
        var flxAction11 = new kony.ui.FlexContainer({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "clipBounds": true,
            "height": "20dp",
            "id": "flxAction11",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "left": "0dp",
            "isModalContainer": false,
            "skin": "sknFlxbgFFFFFFbdrD7D9E0rd3px",
            "top": "0dp",
            "width": "110dp",
            "zIndex": 1
        }, {}, {});
        flxAction11.setDefaultUnit(kony.flex.DP);
        var lblAction11 = new kony.ui.Label({
            "centerY": "45%",
            "id": "lblAction11",
            "isVisible": true,
            "left": "17dp",
            "skin": "sknlblLatoReg12px485C75",
            "text": "Bulk- Approve",
            "top": "0dp",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, {
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, {});
        var fontIconCross11 = new kony.ui.Label({
            "centerY": "50%",
            "id": "fontIconCross11",
            "isVisible": true,
            "right": "5dp",
            "skin": "sknFontIconSearchCross16px",
            "i18n_text": "kony.i18n.getLocalizedString(\"i18n.userwidgetmodel.lblClearSearch\")",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, {
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, {});
        var lblIcon11 = new kony.ui.Label({
            "centerY": "52%",
            "height": "12dp",
            "id": "lblIcon11",
            "isVisible": true,
            "left": "2dp",
            "skin": "sknFontIconActivate",
            "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmAlertsManagement.lblViewAlertStatusKey\")",
            "width": "12dp",
            "zIndex": 1
        }, {
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, {});
        flxAction11.add(lblAction11, fontIconCross11, lblIcon11);
        var flxAction12 = new kony.ui.FlexContainer({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "clipBounds": true,
            "height": "20dp",
            "id": "flxAction12",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "left": "10dp",
            "isModalContainer": false,
            "skin": "sknFlxbgFFFFFFbdrD7D9E0rd3px",
            "top": "0dp",
            "width": "110dp",
            "zIndex": 1
        }, {}, {});
        flxAction12.setDefaultUnit(kony.flex.DP);
        var lblAction12 = new kony.ui.Label({
            "centerY": "45%",
            "id": "lblAction12",
            "isVisible": true,
            "left": "17dp",
            "skin": "sknlblLatoReg12px485C75",
            "text": "Create",
            "top": "0dp",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, {
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, {});
        var fontIconCross12 = new kony.ui.Label({
            "centerY": "50%",
            "id": "fontIconCross12",
            "isVisible": true,
            "right": "5dp",
            "skin": "sknFontIconSearchCross16px",
            "i18n_text": "kony.i18n.getLocalizedString(\"i18n.userwidgetmodel.lblClearSearch\")",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, {
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, {});
        var lblIcon12 = new kony.ui.Label({
            "centerY": "52%",
            "height": "12dp",
            "id": "lblIcon12",
            "isVisible": true,
            "left": "2dp",
            "skin": "sknFontIconActivate",
            "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmAlertsManagement.lblViewAlertStatusKey\")",
            "width": "12dp",
            "zIndex": 1
        }, {
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, {});
        flxAction12.add(lblAction12, fontIconCross12, lblIcon12);
        var flxAction13 = new kony.ui.FlexContainer({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "clipBounds": true,
            "height": "20dp",
            "id": "flxAction13",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "left": "10dp",
            "isModalContainer": false,
            "skin": "sknFlxbgFFFFFFbdrD7D9E0rd3px",
            "top": "0dp",
            "width": "110dp",
            "zIndex": 1
        }, {}, {});
        flxAction13.setDefaultUnit(kony.flex.DP);
        var lblAction13 = new kony.ui.Label({
            "centerY": "45%",
            "id": "lblAction13",
            "isVisible": true,
            "left": "17dp",
            "skin": "sknlblLatoReg12px485C75",
            "text": "Approve",
            "top": "0dp",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, {
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, {});
        var fontIconCross13 = new kony.ui.Label({
            "centerY": "50%",
            "id": "fontIconCross13",
            "isVisible": true,
            "right": "5dp",
            "skin": "sknFontIconSearchCross16px",
            "i18n_text": "kony.i18n.getLocalizedString(\"i18n.userwidgetmodel.lblClearSearch\")",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, {
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, {});
        var lblIcon13 = new kony.ui.Label({
            "centerY": "52%",
            "height": "12dp",
            "id": "lblIcon13",
            "isVisible": true,
            "left": "2dp",
            "skin": "sknFontIconActivate",
            "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmAlertsManagement.lblViewAlertStatusKey\")",
            "width": "12dp",
            "zIndex": 1
        }, {
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, {});
        flxAction13.add(lblAction13, fontIconCross13, lblIcon13);
        var flxAction14 = new kony.ui.FlexContainer({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "clipBounds": true,
            "height": "20dp",
            "id": "flxAction14",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "left": "10dp",
            "isModalContainer": false,
            "skin": "sknFlxbgFFFFFFbdrD7D9E0rd3px",
            "top": "0dp",
            "width": "110dp",
            "zIndex": 1
        }, {}, {});
        flxAction14.setDefaultUnit(kony.flex.DP);
        var lblAction14 = new kony.ui.Label({
            "centerY": "45%",
            "id": "lblAction14",
            "isVisible": true,
            "left": "17dp",
            "skin": "sknlblLatoReg12px485C75",
            "text": "Delete",
            "top": "0dp",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, {
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, {});
        var fontIconCross14 = new kony.ui.Label({
            "centerY": "50%",
            "id": "fontIconCross14",
            "isVisible": true,
            "right": "5dp",
            "skin": "sknFontIconSearchCross16px",
            "i18n_text": "kony.i18n.getLocalizedString(\"i18n.userwidgetmodel.lblClearSearch\")",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, {
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, {});
        var lblIcon14 = new kony.ui.Label({
            "centerY": "52%",
            "height": "12dp",
            "id": "lblIcon14",
            "isVisible": true,
            "left": "2dp",
            "skin": "sknFontIconActivate",
            "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmAlertsManagement.lblViewAlertStatusKey\")",
            "width": "12dp",
            "zIndex": 1
        }, {
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, {});
        flxAction14.add(lblAction14, fontIconCross14, lblIcon14);
        var flxAction15 = new kony.ui.FlexContainer({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "clipBounds": true,
            "height": "20dp",
            "id": "flxAction15",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "left": "10dp",
            "isModalContainer": false,
            "skin": "sknFlxbgFFFFFFbdrD7D9E0rd3px",
            "top": "0dp",
            "width": "110dp",
            "zIndex": 1
        }, {}, {});
        flxAction15.setDefaultUnit(kony.flex.DP);
        var lblAction15 = new kony.ui.Label({
            "centerY": "45%",
            "id": "lblAction15",
            "isVisible": true,
            "left": "17dp",
            "skin": "sknlblLatoReg12px485C75",
            "text": "Delete",
            "top": "0dp",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, {
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, {});
        var fontIconCross15 = new kony.ui.Label({
            "centerY": "50%",
            "id": "fontIconCross15",
            "isVisible": true,
            "right": "5dp",
            "skin": "sknFontIconSearchCross16px",
            "i18n_text": "kony.i18n.getLocalizedString(\"i18n.userwidgetmodel.lblClearSearch\")",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, {
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, {});
        var lblIcon15 = new kony.ui.Label({
            "centerY": "52%",
            "height": "12dp",
            "id": "lblIcon15",
            "isVisible": true,
            "left": "2dp",
            "skin": "sknFontIconActivate",
            "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmAlertsManagement.lblViewAlertStatusKey\")",
            "width": "12dp",
            "zIndex": 1
        }, {
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, {});
        flxAction15.add(lblAction15, fontIconCross15, lblIcon15);
        var flxRemove3 = new kony.ui.FlexContainer({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "clipBounds": true,
            "height": "20dp",
            "id": "flxRemove3",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "left": "10dp",
            "isModalContainer": false,
            "skin": "sknFlxbgFFFFFFbdrD7D9E0rd3px",
            "top": "0dp",
            "width": "90dp",
            "zIndex": 1
        }, {}, {});
        flxRemove3.setDefaultUnit(kony.flex.DP);
        var lblRemove3 = new kony.ui.Label({
            "centerY": "50%",
            "id": "lblRemove3",
            "isVisible": true,
            "left": "17dp",
            "skin": "sknlblLatoReg12px485C75",
            "text": "Remove All",
            "top": "0dp",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, {
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, {});
        var lblIconRemove3 = new kony.ui.Label({
            "centerY": "52%",
            "height": "12dp",
            "id": "lblIconRemove3",
            "isVisible": true,
            "left": "2dp",
            "skin": "sknFontIconActivate",
            "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmAlertsManagement.lblViewAlertStatusKey\")",
            "width": "12dp",
            "zIndex": 1
        }, {
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, {});
        flxRemove3.add(lblRemove3, lblIconRemove3);
        var btnView3 = new kony.ui.Button({
            "height": "20dp",
            "id": "btnView3",
            "isVisible": false,
            "left": "11dp",
            "skin": "sknBtn12pxfnt117EB0",
            "text": "View Less",
            "top": "0dp",
            "width": "100dp",
            "zIndex": 1
        }, {
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "displayText": true,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, {});
        flxMain3.add(flxAction11, flxAction12, flxAction13, flxAction14, flxAction15, flxRemove3, btnView3);
        var flxMain4 = new kony.ui.FlexContainer({
            "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
            "bottom": "15dp",
            "clipBounds": true,
            "id": "flxMain4",
            "isVisible": false,
            "layoutType": kony.flex.FLOW_HORIZONTAL,
            "left": "0dp",
            "isModalContainer": false,
            "skin": "slFbox",
            "top": "115dp",
            "width": "97%",
            "zIndex": 1
        }, {}, {});
        flxMain4.setDefaultUnit(kony.flex.DP);
        var flxAction16 = new kony.ui.FlexContainer({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "clipBounds": true,
            "height": "20dp",
            "id": "flxAction16",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "left": "0dp",
            "isModalContainer": false,
            "skin": "sknFlxbgFFFFFFbdrD7D9E0rd3px",
            "top": "0dp",
            "width": "110dp",
            "zIndex": 1
        }, {}, {});
        flxAction16.setDefaultUnit(kony.flex.DP);
        var lblAction16 = new kony.ui.Label({
            "centerY": "45%",
            "id": "lblAction16",
            "isVisible": true,
            "left": "17dp",
            "skin": "sknlblLatoReg12px485C75",
            "text": "Bulk- Approve",
            "top": "0dp",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, {
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, {});
        var fontIconCross16 = new kony.ui.Label({
            "centerY": "50%",
            "id": "fontIconCross16",
            "isVisible": true,
            "right": "5dp",
            "skin": "sknFontIconSearchCross16px",
            "i18n_text": "kony.i18n.getLocalizedString(\"i18n.userwidgetmodel.lblClearSearch\")",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, {
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, {});
        var lblIcon16 = new kony.ui.Label({
            "centerY": "52%",
            "height": "12dp",
            "id": "lblIcon16",
            "isVisible": true,
            "left": "2dp",
            "skin": "sknFontIconActivate",
            "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmAlertsManagement.lblViewAlertStatusKey\")",
            "width": "12dp",
            "zIndex": 1
        }, {
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, {});
        flxAction16.add(lblAction16, fontIconCross16, lblIcon16);
        var flxAction17 = new kony.ui.FlexContainer({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "clipBounds": true,
            "height": "20dp",
            "id": "flxAction17",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "left": "10dp",
            "isModalContainer": false,
            "skin": "sknFlxbgFFFFFFbdrD7D9E0rd3px",
            "top": "0dp",
            "width": "110dp",
            "zIndex": 1
        }, {}, {});
        flxAction17.setDefaultUnit(kony.flex.DP);
        var lblAction17 = new kony.ui.Label({
            "centerY": "45%",
            "id": "lblAction17",
            "isVisible": true,
            "left": "17dp",
            "skin": "sknlblLatoReg12px485C75",
            "text": "Create",
            "top": "0dp",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, {
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, {});
        var fontIconCross17 = new kony.ui.Label({
            "centerY": "50%",
            "id": "fontIconCross17",
            "isVisible": true,
            "right": "5dp",
            "skin": "sknFontIconSearchCross16px",
            "i18n_text": "kony.i18n.getLocalizedString(\"i18n.userwidgetmodel.lblClearSearch\")",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, {
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, {});
        var lblIcon17 = new kony.ui.Label({
            "centerY": "52%",
            "height": "12dp",
            "id": "lblIcon17",
            "isVisible": true,
            "left": "2dp",
            "skin": "sknFontIconActivate",
            "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmAlertsManagement.lblViewAlertStatusKey\")",
            "width": "12dp",
            "zIndex": 1
        }, {
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, {});
        flxAction17.add(lblAction17, fontIconCross17, lblIcon17);
        var flxAction18 = new kony.ui.FlexContainer({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "clipBounds": true,
            "height": "20dp",
            "id": "flxAction18",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "left": "10dp",
            "isModalContainer": false,
            "skin": "sknFlxbgFFFFFFbdrD7D9E0rd3px",
            "top": "0dp",
            "width": "110dp",
            "zIndex": 1
        }, {}, {});
        flxAction18.setDefaultUnit(kony.flex.DP);
        var lblAction18 = new kony.ui.Label({
            "centerY": "45%",
            "id": "lblAction18",
            "isVisible": true,
            "left": "17dp",
            "skin": "sknlblLatoReg12px485C75",
            "text": "Approve",
            "top": "0dp",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, {
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, {});
        var fontIconCross18 = new kony.ui.Label({
            "centerY": "50%",
            "id": "fontIconCross18",
            "isVisible": true,
            "right": "5dp",
            "skin": "sknFontIconSearchCross16px",
            "i18n_text": "kony.i18n.getLocalizedString(\"i18n.userwidgetmodel.lblClearSearch\")",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, {
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, {});
        var lblIcon18 = new kony.ui.Label({
            "centerY": "52%",
            "height": "12dp",
            "id": "lblIcon18",
            "isVisible": true,
            "left": "2dp",
            "skin": "sknFontIconActivate",
            "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmAlertsManagement.lblViewAlertStatusKey\")",
            "width": "12dp",
            "zIndex": 1
        }, {
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, {});
        flxAction18.add(lblAction18, fontIconCross18, lblIcon18);
        var flxAction19 = new kony.ui.FlexContainer({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "clipBounds": true,
            "height": "20dp",
            "id": "flxAction19",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "left": "10dp",
            "isModalContainer": false,
            "skin": "sknFlxbgFFFFFFbdrD7D9E0rd3px",
            "top": "0dp",
            "width": "110dp",
            "zIndex": 1
        }, {}, {});
        flxAction19.setDefaultUnit(kony.flex.DP);
        var lblAction19 = new kony.ui.Label({
            "centerY": "45%",
            "id": "lblAction19",
            "isVisible": true,
            "left": "17dp",
            "skin": "sknlblLatoReg12px485C75",
            "text": "Delete",
            "top": "0dp",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, {
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, {});
        var fontIconCross19 = new kony.ui.Label({
            "centerY": "50%",
            "id": "fontIconCross19",
            "isVisible": true,
            "right": "5dp",
            "skin": "sknFontIconSearchCross16px",
            "i18n_text": "kony.i18n.getLocalizedString(\"i18n.userwidgetmodel.lblClearSearch\")",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, {
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, {});
        var lblIcon19 = new kony.ui.Label({
            "centerY": "52%",
            "height": "12dp",
            "id": "lblIcon19",
            "isVisible": true,
            "left": "2dp",
            "skin": "sknFontIconActivate",
            "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmAlertsManagement.lblViewAlertStatusKey\")",
            "width": "12dp",
            "zIndex": 1
        }, {
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, {});
        flxAction19.add(lblAction19, fontIconCross19, lblIcon19);
        var flxAction20 = new kony.ui.FlexContainer({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "clipBounds": true,
            "height": "20dp",
            "id": "flxAction20",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "left": "10dp",
            "isModalContainer": false,
            "skin": "sknFlxbgFFFFFFbdrD7D9E0rd3px",
            "top": "0dp",
            "width": "110dp",
            "zIndex": 1
        }, {}, {});
        flxAction20.setDefaultUnit(kony.flex.DP);
        var lblAction20 = new kony.ui.Label({
            "centerY": "45%",
            "id": "lblAction20",
            "isVisible": true,
            "left": "17dp",
            "skin": "sknlblLatoReg12px485C75",
            "text": "Delete",
            "top": "0dp",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, {
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, {});
        var fontIconCross20 = new kony.ui.Label({
            "centerY": "50%",
            "id": "fontIconCross20",
            "isVisible": true,
            "right": "5dp",
            "skin": "sknFontIconSearchCross16px",
            "i18n_text": "kony.i18n.getLocalizedString(\"i18n.userwidgetmodel.lblClearSearch\")",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, {
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, {});
        var lblIcon20 = new kony.ui.Label({
            "centerY": "52%",
            "height": "12dp",
            "id": "lblIcon20",
            "isVisible": true,
            "left": "2dp",
            "skin": "sknFontIconActivate",
            "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmAlertsManagement.lblViewAlertStatusKey\")",
            "width": "12dp",
            "zIndex": 1
        }, {
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, {});
        flxAction20.add(lblAction20, fontIconCross20, lblIcon20);
        var flxRemove4 = new kony.ui.FlexContainer({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "clipBounds": true,
            "height": "20dp",
            "id": "flxRemove4",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "left": "10dp",
            "isModalContainer": false,
            "skin": "sknFlxbgFFFFFFbdrD7D9E0rd3px",
            "top": "0dp",
            "width": "90dp",
            "zIndex": 1
        }, {}, {});
        flxRemove4.setDefaultUnit(kony.flex.DP);
        var lblRemove4 = new kony.ui.Label({
            "centerY": "50%",
            "id": "lblRemove4",
            "isVisible": true,
            "left": "17dp",
            "skin": "sknlblLatoReg12px485C75",
            "text": "Remove All",
            "top": "0dp",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, {
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, {});
        var lblIconRemove4 = new kony.ui.Label({
            "centerY": "52%",
            "height": "12dp",
            "id": "lblIconRemove4",
            "isVisible": true,
            "left": "2dp",
            "skin": "sknFontIconActivate",
            "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmAlertsManagement.lblViewAlertStatusKey\")",
            "width": "12dp",
            "zIndex": 1
        }, {
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, {});
        flxRemove4.add(lblRemove4, lblIconRemove4);
        var btnView4 = new kony.ui.Button({
            "height": "20dp",
            "id": "btnView4",
            "isVisible": false,
            "left": "11dp",
            "skin": "sknBtn12pxfnt117EB0",
            "text": "View Less",
            "top": "0dp",
            "width": "100dp",
            "zIndex": 1
        }, {
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "displayText": true,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, {});
        flxMain4.add(flxAction16, flxAction17, flxAction18, flxAction19, flxAction20, flxRemove4, btnView4);
        flxGrouping1.add(flxMain1, lblFeatureName, flxMain2, flxMain3, flxMain4);
        flxGrouping2.add(flxFeatureCheckbox, flxGrouping1);
        var lblSeparator = new kony.ui.Label({
            "bottom": "0dp",
            "height": "1px",
            "id": "lblSeparator",
            "isVisible": true,
            "left": "0dp",
            "skin": "sknlblSeperator",
            "text": "Label",
            "width": "100%",
            "zIndex": 2
        }, {
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, {});
        var flxFeatureStatus = new kony.ui.FlexContainer({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "clipBounds": true,
            "height": "20dp",
            "id": "flxFeatureStatus",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "isModalContainer": false,
            "right": "0dp",
            "skin": "slFbox",
            "top": "15dp",
            "width": "80dp"
        }, {}, {});
        flxFeatureStatus.setDefaultUnit(kony.flex.DP);
        var lblIconStatus = new kony.ui.Label({
            "centerY": "52%",
            "height": "12dp",
            "id": "lblIconStatus",
            "isVisible": true,
            "left": "0dp",
            "skin": "sknFontIconActivate",
            "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmAlertsManagement.lblViewAlertStatusKey\")",
            "width": "12dp",
            "zIndex": 1
        }, {
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, {});
        var lblFeatureStatusValue = new kony.ui.Label({
            "centerY": "50%",
            "id": "lblFeatureStatusValue",
            "isVisible": true,
            "left": 15,
            "skin": "sknLbl485C75LatoRegular13Px",
            "text": "Active",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, {
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, {});
        flxFeatureStatus.add(lblIconStatus, lblFeatureStatusValue);
        flxFeatureNameContainer.add(flxGrouping2, lblSeparator, flxFeatureStatus);
        flxFeaturesListSelected.add(flxFeatureNameContainer);
        return flxFeaturesListSelected;
    }
})