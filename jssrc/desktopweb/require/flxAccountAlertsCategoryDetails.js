define("flxAccountAlertsCategoryDetails", function() {
    return function(controller) {
        var flxAccountAlertsCategoryDetails = new kony.ui.FlexContainer({
            "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
            "clipBounds": true,
            "id": "flxAccountAlertsCategoryDetails",
            "isVisible": true,
            "layoutType": kony.flex.FLOW_VERTICAL,
            "left": "0dp",
            "isModalContainer": false,
            "skin": "slFbox",
            "top": "0dp",
            "width": "100%"
        }, {}, {});
        flxAccountAlertsCategoryDetails.setDefaultUnit(kony.flex.DP);
        var flxAccountAlertCategoryContainer = new kony.ui.FlexContainer({
            "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
            "clipBounds": true,
            "id": "flxAccountAlertCategoryContainer",
            "isVisible": true,
            "layoutType": kony.flex.FLOW_VERTICAL,
            "left": "0dp",
            "isModalContainer": false,
            "skin": "slFbox",
            "top": "0dp",
            "width": "100%",
            "zIndex": 2
        }, {}, {});
        flxAccountAlertCategoryContainer.setDefaultUnit(kony.flex.DP);
        var flxAlertsEnabledStatus = new kony.ui.FlexContainer({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "clipBounds": true,
            "height": "25dp",
            "id": "flxAlertsEnabledStatus",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "left": "0dp",
            "masterType": constants.MASTER_TYPE_USERWIDGET,
            "isModalContainer": false,
            "top": "16dp",
            "width": "100%",
            "zIndex": 1
        }, {}, {});
        flxAlertsEnabledStatus.setDefaultUnit(kony.flex.DP);
        var flxEnabledIcon = new kony.ui.FlexContainer({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "centerY": "50%",
            "clipBounds": true,
            "height": "15dp",
            "id": "flxEnabledIcon",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "left": "0dp",
            "isModalContainer": false,
            "top": 0,
            "width": "15dp",
            "zIndex": 1
        }, {}, {});
        flxEnabledIcon.setDefaultUnit(kony.flex.DP);
        var imgEnabledCheckBox = new kony.ui.Image2({
            "height": "15dp",
            "id": "imgEnabledCheckBox",
            "isVisible": true,
            "left": "0px",
            "skin": "slImage",
            "src": "checkboxselected.png",
            "top": "0px",
            "width": "15dp",
            "zIndex": 1
        }, {
            "imageScaleMode": constants.IMAGE_SCALE_MODE_MAINTAIN_ASPECT_RATIO,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, {});
        var lblEnabledIcon = new kony.ui.Label({
            "centerX": "60%",
            "height": "15dp",
            "id": "lblEnabledIcon",
            "isVisible": true,
            "left": "0px",
            "skin": "sknFontIconActivate",
            "text": "",
            "top": "0px",
            "width": "15dp",
            "zIndex": 1
        }, {
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, {});
        flxEnabledIcon.add(imgEnabledCheckBox, lblEnabledIcon);
        var lblAlertDescription = new kony.ui.Label({
            "centerY": "50%",
            "id": "lblAlertDescription",
            "isVisible": true,
            "left": "25dp",
            "right": "30dp",
            "skin": "sknlblLatoRegular484b5213px",
            "text": "Label",
            "zIndex": 1
        }, {
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, {});
        flxAlertsEnabledStatus.add(flxEnabledIcon, lblAlertDescription);
        var flxAccBalanceInBetween = new kony.ui.FlexContainer({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "clipBounds": true,
            "height": "25dp",
            "id": "flxAccBalanceInBetween",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "left": "25dp",
            "masterType": constants.MASTER_TYPE_USERWIDGET,
            "isModalContainer": false,
            "top": "5dp",
            "width": "90%",
            "zIndex": 1
        }, {}, {});
        flxAccBalanceInBetween.setDefaultUnit(kony.flex.DP);
        var tbxMinLimit = new kony.ui.TextBox2({
            "autoCapitalize": constants.TEXTBOX_AUTO_CAPITALIZE_NONE,
            "height": "25dp",
            "id": "tbxMinLimit",
            "isVisible": true,
            "keyBoardStyle": constants.TEXTBOX_KEY_BOARD_STYLE_DEFAULT,
            "left": "0dp",
            "secureTextEntry": false,
            "skin": "skntbxLato35475f14px",
            "textInputMode": constants.TEXTBOX_INPUT_MODE_NUMERIC,
            "top": "0dp",
            "width": "135dp",
            "zIndex": 1
        }, {
            "containerHeightMode": constants.TEXTBOX_FONT_METRICS_DRIVEN_HEIGHT,
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [1, 0, 0, 0],
            "paddingInPixel": false
        }, {
            "autoCorrect": false
        });
        var tbxMaxLimit = new kony.ui.TextBox2({
            "autoCapitalize": constants.TEXTBOX_AUTO_CAPITALIZE_NONE,
            "height": "25dp",
            "id": "tbxMaxLimit",
            "isVisible": true,
            "keyBoardStyle": constants.TEXTBOX_KEY_BOARD_STYLE_DEFAULT,
            "left": "165dp",
            "secureTextEntry": false,
            "skin": "skntbxLato35475f14px",
            "textInputMode": constants.TEXTBOX_INPUT_MODE_NUMERIC,
            "top": "0dp",
            "width": "135dp",
            "zIndex": 1
        }, {
            "containerHeightMode": constants.TEXTBOX_FONT_METRICS_DRIVEN_HEIGHT,
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [1, 0, 0, 0],
            "paddingInPixel": false
        }, {
            "autoCorrect": false
        });
        var flxHorSeparator = new kony.ui.FlexContainer({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "clipBounds": true,
            "height": "2dp",
            "id": "flxHorSeparator",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "left": "145dp",
            "isModalContainer": false,
            "top": "13dp",
            "width": "10dp",
            "zIndex": 1
        }, {}, {});
        flxHorSeparator.setDefaultUnit(kony.flex.DP);
        var lblHorSeparator = new kony.ui.Label({
            "id": "lblHorSeparator",
            "isVisible": true,
            "left": "100%",
            "top": "0dp",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, {
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, {});
        flxHorSeparator.add(lblHorSeparator);
        flxAccBalanceInBetween.add(tbxMinLimit, tbxMaxLimit, flxHorSeparator);
        var flxAccBalanceCreditedDebited = new kony.ui.FlexContainer({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "clipBounds": true,
            "height": "25dp",
            "id": "flxAccBalanceCreditedDebited",
            "isVisible": false,
            "layoutType": kony.flex.FREE_FORM,
            "left": "25dp",
            "masterType": constants.MASTER_TYPE_USERWIDGET,
            "isModalContainer": false,
            "top": "0dp",
            "width": "90%",
            "zIndex": 1
        }, {}, {});
        flxAccBalanceCreditedDebited.setDefaultUnit(kony.flex.DP);
        var tbxAmount = new kony.ui.TextBox2({
            "autoCapitalize": constants.TEXTBOX_AUTO_CAPITALIZE_NONE,
            "height": "25dp",
            "id": "tbxAmount",
            "isVisible": true,
            "keyBoardStyle": constants.TEXTBOX_KEY_BOARD_STYLE_DEFAULT,
            "left": "0dp",
            "secureTextEntry": false,
            "skin": "skntbxLato35475f14px",
            "textInputMode": constants.TEXTBOX_INPUT_MODE_NUMERIC,
            "top": "0dp",
            "width": "135dp",
            "zIndex": 1
        }, {
            "containerHeightMode": constants.TEXTBOX_FONT_METRICS_DRIVEN_HEIGHT,
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [1, 0, 0, 0],
            "paddingInPixel": false
        }, {
            "autoCorrect": false
        });
        flxAccBalanceCreditedDebited.add(tbxAmount);
        var flxNotifyBalance = new kony.ui.FlexContainer({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "clipBounds": true,
            "height": "25dp",
            "id": "flxNotifyBalance",
            "isVisible": false,
            "layoutType": kony.flex.FREE_FORM,
            "left": "25dp",
            "masterType": constants.MASTER_TYPE_USERWIDGET,
            "isModalContainer": false,
            "top": "0dp",
            "width": "90%",
            "zIndex": 1
        }, {}, {});
        flxNotifyBalance.setDefaultUnit(kony.flex.DP);
        var lstNotify = new kony.ui.ListBox({
            "height": "20dp",
            "id": "lstNotify",
            "isVisible": true,
            "left": "15dp",
            "masterData": [
                ["lb1", "Placeholder One"],
                ["lb2", "Placeholder Two"],
                ["lb3", "Placeholder Three"]
            ],
            "top": "5dp",
            "width": "300dp",
            "zIndex": 1
        }, {
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [3, 0, 0, 0],
            "paddingInPixel": false
        }, {
            "multiSelect": false
        });
        flxNotifyBalance.add(lstNotify);
        var lblSeperator = new kony.ui.Label({
            "bottom": "0dp",
            "height": "1dp",
            "id": "lblSeperator",
            "isVisible": true,
            "left": "0dp",
            "skin": "sknlblSeperator",
            "text": "-",
            "top": "15dp",
            "width": "100%",
            "zIndex": 2
        }, {
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, {});
        flxAccountAlertCategoryContainer.add(flxAlertsEnabledStatus, flxAccBalanceInBetween, flxAccBalanceCreditedDebited, flxNotifyBalance, lblSeperator);
        flxAccountAlertsCategoryDetails.add(flxAccountAlertCategoryContainer);
        return flxAccountAlertsCategoryDetails;
    }
})