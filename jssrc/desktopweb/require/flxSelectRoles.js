define("flxSelectRoles", function() {
    return function(controller) {
        var flxSelectRoles = new kony.ui.FlexContainer({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "clipBounds": true,
            "height": "39dp",
            "id": "flxSelectRoles",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "left": "0dp",
            "isModalContainer": false,
            "skin": "sknflxBgF9F9F9",
            "top": "0dp",
            "width": "100%"
        }, {}, {});
        flxSelectRoles.setDefaultUnit(kony.flex.DP);
        var flxSelectRolesInner = new kony.ui.FlexContainer({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "clipBounds": true,
            "height": "35dp",
            "id": "flxSelectRolesInner",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "left": "9dp",
            "isModalContainer": false,
            "right": "9dp",
            "skin": "sknflxffffffBorderE1E5EERadius3px",
            "top": "2dp"
        }, {}, {
            "hoverSkin": "sknflxEAF3FBBorder006CCARadius3px"
        });
        flxSelectRolesInner.setDefaultUnit(kony.flex.DP);
        var lblRoleName = new kony.ui.Label({
            "id": "lblRoleName",
            "isVisible": true,
            "left": "44dp",
            "skin": "sknLbl485C75LatoRegular13Px",
            "text": "KL098234 - Westheimer",
            "top": "9dp",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, {
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, {});
        var flxCheckBox = new kony.ui.FlexContainer({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "centerY": "50%",
            "clipBounds": true,
            "height": "22dp",
            "id": "flxCheckBox",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "left": "12dp",
            "isModalContainer": false,
            "skin": "sknflxBgE1E5EERadiusCircular",
            "width": "22dp"
        }, {}, {});
        flxCheckBox.setDefaultUnit(kony.flex.DP);
        var fonticonCheckBox = new kony.ui.Label({
            "centerX": "50%",
            "centerY": "50%",
            "height": "20dp",
            "id": "fonticonCheckBox",
            "isVisible": true,
            "skin": "sknIcon20pxWhite",
            "text": "",
            "width": "20dp",
            "zIndex": 2
        }, {
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, {});
        flxCheckBox.add(fonticonCheckBox);
        flxSelectRolesInner.add(lblRoleName, flxCheckBox);
        flxSelectRoles.add(flxSelectRolesInner);
        return flxSelectRoles;
    }
})