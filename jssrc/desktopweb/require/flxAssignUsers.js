define("flxAssignUsers", function() {
    return function(controller) {
        var flxAssignUsers = new kony.ui.FlexContainer({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "clipBounds": true,
            "height": "30px",
            "id": "flxAssignUsers",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "isModalContainer": false,
            "skin": "sknflxffffffop100"
        }, {}, {});
        flxAssignUsers.setDefaultUnit(kony.flex.DP);
        var lblViewFullName = new kony.ui.Label({
            "centerY": "50%",
            "id": "lblViewFullName",
            "isVisible": true,
            "left": "20dp",
            "skin": "sknlblLatoRegular484b5213px",
            "text": "Edward",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, {
            "contentAlignment": constants.CONTENT_ALIGN_CENTER,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, {});
        flxAssignUsers.add(lblViewFullName);
        return flxAssignUsers;
    }
})