define("flxAlertSequenceMap", function() {
    return function(controller) {
        var flxAlertSequenceMap = new kony.ui.FlexContainer({
            "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
            "clipBounds": true,
            "id": "flxAlertSequenceMap",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "left": "0dp",
            "isModalContainer": false,
            "skin": "sknbackGroundffffff100",
            "top": "0dp",
            "width": "100%"
        }, {}, {
            "hoverSkin": "sknbackGroundffffffPointer"
        });
        flxAlertSequenceMap.setDefaultUnit(kony.flex.DP);
        var lblSeqNum = new kony.ui.Label({
            "height": "20dp",
            "id": "lblSeqNum",
            "isVisible": false,
            "left": "10dp",
            "skin": "sknIcomoon20px",
            "top": "10px",
            "width": "20dp",
            "zIndex": 1
        }, {
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, {});
        var lblName = new kony.ui.Label({
            "bottom": "10dp",
            "id": "lblName",
            "isVisible": true,
            "left": "20dp",
            "skin": "sknlbl485c7514px",
            "text": "Profile Management",
            "top": "10dp",
            "width": "80%",
            "zIndex": 1
        }, {
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, {});
        flxAlertSequenceMap.add(lblSeqNum, lblName);
        return flxAlertSequenceMap;
    }
})