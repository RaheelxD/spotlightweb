define("flxContractsLimitsBodyView", function() {
    return function(controller) {
        var flxContractsLimitsBodyView = new kony.ui.FlexContainer({
            "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
            "clipBounds": true,
            "id": "flxContractsLimitsBodyView",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "left": "0dp",
            "isModalContainer": false,
            "skin": "sknFlxFFFFFF1000",
            "top": "0dp",
            "width": "100%"
        }, {}, {});
        flxContractsLimitsBodyView.setDefaultUnit(kony.flex.DP);
        var flxViewLimits = new kony.ui.FlexContainer({
            "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
            "clipBounds": true,
            "id": "flxViewLimits",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "left": "30px",
            "isModalContainer": false,
            "right": "20px",
            "skin": "slFbox",
            "top": "0px"
        }, {}, {});
        flxViewLimits.setDefaultUnit(kony.flex.DP);
        var lblAction = new kony.ui.Label({
            "bottom": "20dp",
            "id": "lblAction",
            "isVisible": true,
            "left": "0dp",
            "skin": "sknlblLatoReg485c7513px",
            "text": "Create Bill Payment",
            "top": "16dp",
            "width": "15%",
            "zIndex": 1
        }, {
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, {});
        var flxPerLimitTextBox = new kony.ui.FlexContainer({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "clipBounds": true,
            "height": "40dp",
            "id": "flxPerLimitTextBox",
            "isVisible": false,
            "layoutType": kony.flex.FREE_FORM,
            "left": "25%",
            "isModalContainer": false,
            "skin": "sknFlxbgFFFFFFbdrD7D9E0rd3px",
            "top": "0dp",
            "width": "20%"
        }, {}, {});
        flxPerLimitTextBox.setDefaultUnit(kony.flex.DP);
        var tbxPerValue = new kony.ui.TextBox2({
            "autoCapitalize": constants.TEXTBOX_AUTO_CAPITALIZE_NONE,
            "centerY": "50%",
            "height": "38dp",
            "id": "tbxPerValue",
            "isVisible": true,
            "keyBoardStyle": constants.TEXTBOX_KEY_BOARD_STYLE_DEFAULT,
            "left": "30px",
            "right": "0dp",
            "secureTextEntry": false,
            "skin": "sknTbxBgFFFFFFf485c7513pxNoBorder",
            "textInputMode": constants.TEXTBOX_INPUT_MODE_NUMERIC,
            "zIndex": 1
        }, {
            "containerHeightMode": constants.TEXTBOX_FONT_METRICS_DRIVEN_HEIGHT,
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [2, 0, 0, 0],
            "paddingInPixel": false
        }, {
            "autoCorrect": false,
            "placeholderSkin": "defTextBoxPlaceholder"
        });
        var lblCurrencySymbol1 = new kony.ui.Label({
            "centerY": "50%",
            "id": "lblCurrencySymbol1",
            "isVisible": true,
            "left": "5dp",
            "skin": "sknFontIcon485c7520px",
            "text": "",
            "width": "20dp",
            "zIndex": 1
        }, {
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, {});
        flxPerLimitTextBox.add(tbxPerValue, lblCurrencySymbol1);
        var flxDailyLimitTextBox = new kony.ui.FlexContainer({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "clipBounds": true,
            "height": "40dp",
            "id": "flxDailyLimitTextBox",
            "isVisible": false,
            "layoutType": kony.flex.FREE_FORM,
            "left": "53%",
            "isModalContainer": false,
            "skin": "sknFlxbgFFFFFFbdrD7D9E0rd3px",
            "top": "0dp",
            "width": "20%"
        }, {}, {});
        flxDailyLimitTextBox.setDefaultUnit(kony.flex.DP);
        var tbxDailyValue = new kony.ui.TextBox2({
            "autoCapitalize": constants.TEXTBOX_AUTO_CAPITALIZE_NONE,
            "centerY": "50%",
            "height": "38dp",
            "id": "tbxDailyValue",
            "isVisible": true,
            "keyBoardStyle": constants.TEXTBOX_KEY_BOARD_STYLE_DEFAULT,
            "left": "30px",
            "right": "0dp",
            "secureTextEntry": false,
            "skin": "sknTbxBgFFFFFFf485c7513pxNoBorder",
            "textInputMode": constants.TEXTBOX_INPUT_MODE_NUMERIC,
            "zIndex": 1
        }, {
            "containerHeightMode": constants.TEXTBOX_FONT_METRICS_DRIVEN_HEIGHT,
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [2, 0, 0, 0],
            "paddingInPixel": false
        }, {
            "autoCorrect": false,
            "placeholderSkin": "defTextBoxPlaceholder"
        });
        var lblCurrencySymbol2 = new kony.ui.Label({
            "centerY": "50%",
            "id": "lblCurrencySymbol2",
            "isVisible": true,
            "left": "5dp",
            "skin": "sknFontIcon485c7520px",
            "text": "",
            "width": "20dp",
            "zIndex": 1
        }, {
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, {});
        flxDailyLimitTextBox.add(tbxDailyValue, lblCurrencySymbol2);
        var flxWeeklyLimitTextBox = new kony.ui.FlexContainer({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "clipBounds": true,
            "height": "40dp",
            "id": "flxWeeklyLimitTextBox",
            "isVisible": false,
            "layoutType": kony.flex.FREE_FORM,
            "left": "80%",
            "isModalContainer": false,
            "skin": "sknFlxbgFFFFFFbdrD7D9E0rd3px",
            "top": "0dp",
            "width": "20%"
        }, {}, {});
        flxWeeklyLimitTextBox.setDefaultUnit(kony.flex.DP);
        var tbxWeeklyValue = new kony.ui.TextBox2({
            "autoCapitalize": constants.TEXTBOX_AUTO_CAPITALIZE_NONE,
            "centerY": "50%",
            "height": "38dp",
            "id": "tbxWeeklyValue",
            "isVisible": true,
            "keyBoardStyle": constants.TEXTBOX_KEY_BOARD_STYLE_DEFAULT,
            "left": "30px",
            "right": "0dp",
            "secureTextEntry": false,
            "skin": "sknTbxBgFFFFFFf485c7513pxNoBorder",
            "textInputMode": constants.TEXTBOX_INPUT_MODE_NUMERIC,
            "zIndex": 1
        }, {
            "containerHeightMode": constants.TEXTBOX_FONT_METRICS_DRIVEN_HEIGHT,
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [2, 0, 0, 0],
            "paddingInPixel": false
        }, {
            "autoCorrect": false,
            "placeholderSkin": "defTextBoxPlaceholder"
        });
        var lblCurrencySymbol3 = new kony.ui.Label({
            "centerY": "50%",
            "id": "lblCurrencySymbol3",
            "isVisible": true,
            "left": "5dp",
            "skin": "sknFontIcon485c7520px",
            "text": "",
            "width": "20dp",
            "zIndex": 1
        }, {
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, {});
        flxWeeklyLimitTextBox.add(tbxWeeklyValue, lblCurrencySymbol3);
        var flxPerLimit = new kony.ui.FlexContainer({
            "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
            "clipBounds": true,
            "id": "flxPerLimit",
            "isVisible": true,
            "layoutType": kony.flex.FLOW_HORIZONTAL,
            "left": "25%",
            "isModalContainer": false,
            "skin": "slFbox",
            "top": "15dp",
            "width": "20%"
        }, {}, {});
        flxPerLimit.setDefaultUnit(kony.flex.DP);
        var lblCurrencyPer = new kony.ui.Label({
            "id": "lblCurrencyPer",
            "isVisible": true,
            "left": "0dp",
            "skin": "sknFontIcon485c7520px",
            "text": "",
            "top": "0dp",
            "width": "20dp",
            "zIndex": 1
        }, {
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, {});
        var lblPerTransactionLimit = new kony.ui.Label({
            "bottom": "15dp",
            "id": "lblPerTransactionLimit",
            "isVisible": true,
            "left": "0dp",
            "skin": "sknlblLatoReg485c7513px",
            "text": "Create Bill Payment",
            "top": "2dp",
            "width": "100%",
            "zIndex": 1
        }, {
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, {});
        flxPerLimit.add(lblCurrencyPer, lblPerTransactionLimit);
        var flxDailyLimit = new kony.ui.FlexContainer({
            "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
            "clipBounds": true,
            "id": "flxDailyLimit",
            "isVisible": true,
            "layoutType": kony.flex.FLOW_HORIZONTAL,
            "left": "53%",
            "isModalContainer": false,
            "skin": "slFbox",
            "top": "15dp",
            "width": "20%"
        }, {}, {});
        flxDailyLimit.setDefaultUnit(kony.flex.DP);
        var lblCurrencyDaily = new kony.ui.Label({
            "id": "lblCurrencyDaily",
            "isVisible": true,
            "left": "0dp",
            "skin": "sknFontIcon485c7520px",
            "text": "",
            "top": "0dp",
            "width": "20dp",
            "zIndex": 1
        }, {
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, {});
        var lblDailyTransactionLimit = new kony.ui.Label({
            "bottom": "15dp",
            "id": "lblDailyTransactionLimit",
            "isVisible": true,
            "left": "0dp",
            "skin": "sknlblLatoReg485c7513px",
            "text": "Create Bill Payment",
            "top": "2dp",
            "width": "100%",
            "zIndex": 1
        }, {
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, {});
        flxDailyLimit.add(lblCurrencyDaily, lblDailyTransactionLimit);
        var flxWeeklyLimit = new kony.ui.FlexContainer({
            "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
            "clipBounds": true,
            "id": "flxWeeklyLimit",
            "isVisible": true,
            "layoutType": kony.flex.FLOW_HORIZONTAL,
            "left": "78%",
            "isModalContainer": false,
            "skin": "slFbox",
            "top": "15dp",
            "width": "22%"
        }, {}, {});
        flxWeeklyLimit.setDefaultUnit(kony.flex.DP);
        var lblCurrencyWeekly = new kony.ui.Label({
            "id": "lblCurrencyWeekly",
            "isVisible": true,
            "left": "0dp",
            "skin": "sknFontIcon485c7520px",
            "text": "",
            "top": "0dp",
            "width": "20dp",
            "zIndex": 1
        }, {
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, {});
        var lblWeeklyTransactionLimit = new kony.ui.Label({
            "bottom": "15dp",
            "id": "lblWeeklyTransactionLimit",
            "isVisible": true,
            "left": "0dp",
            "skin": "sknlblLatoReg485c7513px",
            "text": "Create Bill Payment",
            "top": "2dp",
            "width": "100%",
            "zIndex": 1
        }, {
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, {});
        flxWeeklyLimit.add(lblCurrencyWeekly, lblWeeklyTransactionLimit);
        flxViewLimits.add(lblAction, flxPerLimitTextBox, flxDailyLimitTextBox, flxWeeklyLimitTextBox, flxPerLimit, flxDailyLimit, flxWeeklyLimit);
        var lblLimitsSeperator = new kony.ui.Label({
            "height": "1px",
            "id": "lblLimitsSeperator",
            "isVisible": true,
            "left": "20px",
            "right": "20px",
            "skin": "sknlblSeperatorD7D9E0",
            "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmCSR.lblSeperator\")",
            "top": "0dp",
            "zIndex": 1
        }, {
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, {});
        flxContractsLimitsBodyView.add(flxViewLimits, lblLimitsSeperator);
        return flxContractsLimitsBodyView;
    }
})