define("flxRoles", function() {
    return function(controller) {
        var flxRoles = new kony.ui.FlexContainer({
            "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
            "clipBounds": true,
            "id": "flxRoles",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "isModalContainer": false,
            "skin": "sknflxffffffop100"
        }, {}, {});
        flxRoles.setDefaultUnit(kony.flex.DP);
        var flxRolesContainer = new kony.ui.FlexContainer({
            "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
            "clipBounds": true,
            "id": "flxRolesContainer",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "left": "20dp",
            "isModalContainer": false,
            "right": "20dp",
            "skin": "slFbox",
            "top": "0dp"
        }, {}, {
            "hoverSkin": "sknfbfcfc"
        });
        flxRolesContainer.setDefaultUnit(kony.flex.DP);
        var lblRoleName = new kony.ui.Label({
            "bottom": "15dp",
            "id": "lblRoleName",
            "isVisible": true,
            "left": "10px",
            "right": "86.96%",
            "skin": "sknlblLatoBold35475f14px",
            "text": "Admin Role",
            "top": "18px",
            "zIndex": 100
        }, {
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, {});
        var lblDescription = new kony.ui.Label({
            "bottom": "18px",
            "id": "lblDescription",
            "isVisible": true,
            "left": "18%",
            "skin": "sknlblLatoBold35475f14px",
            "text": "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do",
            "top": "18px",
            "width": "24%",
            "zIndex": 1
        }, {
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, {});
        var lblValidTillDate = new kony.ui.Label({
            "id": "lblValidTillDate",
            "isVisible": false,
            "left": "49%",
            "skin": "sknlblLatoBold35475f14px",
            "text": "01/12/2018",
            "top": "18px",
            "width": "12%",
            "zIndex": 1
        }, {
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, {});
        var lblNoOfUsers = new kony.ui.Label({
            "id": "lblNoOfUsers",
            "isVisible": true,
            "left": "61%",
            "skin": "sknlblLatoBold35475f14px",
            "text": "20",
            "top": "18px",
            "width": "8%",
            "zIndex": 1
        }, {
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, {});
        var lblPermissions = new kony.ui.Label({
            "id": "lblPermissions",
            "isVisible": true,
            "left": "71%",
            "skin": "sknlblLatoBold35475f14px",
            "text": "25",
            "top": "18px",
            "width": "10%",
            "zIndex": 1
        }, {
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, {});
        var flxStatus = new kony.ui.FlexContainer({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "clipBounds": true,
            "height": "50px",
            "id": "flxStatus",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "left": "86%",
            "isModalContainer": false,
            "skin": "slFbox",
            "top": "0dp",
            "width": "8%",
            "zIndex": 1
        }, {}, {});
        flxStatus.setDefaultUnit(kony.flex.DP);
        var lblRoleStatus = new kony.ui.Label({
            "centerY": "50%",
            "id": "lblRoleStatus",
            "isVisible": true,
            "left": "16px",
            "skin": "sknlblLato5bc06cBold14px",
            "text": "Active",
            "width": "45px",
            "zIndex": 1
        }, {
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, {});
        var fontIconStatusImg = new kony.ui.Label({
            "centerY": "50%",
            "id": "fontIconStatusImg",
            "isVisible": true,
            "left": "0dp",
            "skin": "sknFontIconActivate",
            "text": "",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, {
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, {});
        flxStatus.add(lblRoleStatus, fontIconStatusImg);
        var lblSeperator = new kony.ui.Label({
            "bottom": 0,
            "height": "1px",
            "id": "lblSeperator",
            "isVisible": true,
            "left": "0dp",
            "skin": "sknlblSeperator",
            "text": ".",
            "width": "100%",
            "zIndex": 1
        }, {
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, {});
        var flxOptions = new kony.ui.FlexContainer({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "clipBounds": true,
            "height": "25dp",
            "id": "flxOptions",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "isModalContainer": false,
            "onClick": controller.AS_FlexContainer_baa61be0f35e4cf8b2f0420fe113e29c,
            "right": "10px",
            "skin": "slFbox",
            "top": "11dp",
            "width": "25px",
            "zIndex": 1
        }, {}, {
            "hoverSkin": "sknflxffffffop100Border424242Radius100px"
        });
        flxOptions.setDefaultUnit(kony.flex.DP);
        var lblIconImgOptions = new kony.ui.Label({
            "centerX": "50%",
            "centerY": "50%",
            "id": "lblIconImgOptions",
            "isVisible": true,
            "skin": "sknFontIconOptionMenu",
            "text": "",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, {
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, {});
        flxOptions.add(lblIconImgOptions);
        flxRolesContainer.add(lblRoleName, lblDescription, lblValidTillDate, lblNoOfUsers, lblPermissions, flxStatus, lblSeperator, flxOptions);
        flxRoles.add(flxRolesContainer);
        return flxRoles;
    }
})