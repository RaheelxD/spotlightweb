define("flxSegMFAConfigsSelected", function() {
    return function(controller) {
        var flxSegMFAConfigsSelected = new kony.ui.FlexContainer({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "clipBounds": true,
            "height": "210px",
            "id": "flxSegMFAConfigsSelected",
            "isVisible": true,
            "layoutType": kony.flex.FLOW_VERTICAL,
            "left": "0dp",
            "isModalContainer": false,
            "onClick": controller.AS_FlexContainer_d66ca5614ee24985a5981069c9a80c7e,
            "skin": "slFbox",
            "top": "0dp",
            "width": "100%"
        }, {}, {});
        flxSegMFAConfigsSelected.setDefaultUnit(kony.flex.DP);
        var flxContent = new kony.ui.FlexContainer({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "clipBounds": true,
            "height": "210px",
            "id": "flxContent",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "left": "0dp",
            "isModalContainer": false,
            "skin": "sknFlxFFFFFF1000",
            "top": "0px",
            "width": "100%",
            "zIndex": 1
        }, {}, {
            "hoverSkin": "sknfbfcfc"
        });
        flxContent.setDefaultUnit(kony.flex.DP);
        var flxMFAConfigsRow = new kony.ui.FlexContainer({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "clipBounds": true,
            "height": "50dp",
            "id": "flxMFAConfigsRow",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "left": "0",
            "isModalContainer": false,
            "skin": "slFbox",
            "top": "0dp",
            "width": "100%"
        }, {}, {});
        flxMFAConfigsRow.setDefaultUnit(kony.flex.DP);
        var flxOptions = new kony.ui.FlexContainer({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "centerY": "50%",
            "clipBounds": true,
            "height": "25dp",
            "id": "flxOptions",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "left": "94.50%",
            "isModalContainer": false,
            "onClick": controller.AS_FlexContainer_c544a2b25fc845458df85ba63e8216c5,
            "skin": "slFbox",
            "top": "10dp",
            "width": "25px",
            "zIndex": 2
        }, {}, {
            "hoverSkin": "sknflxffffffop100Border424242Radius100px"
        });
        flxOptions.setDefaultUnit(kony.flex.DP);
        var imgOptions = new kony.ui.Image2({
            "centerX": "50%",
            "centerY": "50%",
            "height": "12dp",
            "id": "imgOptions",
            "isVisible": false,
            "left": 14,
            "skin": "slImage",
            "src": "dots3x.png",
            "width": "6dp",
            "zIndex": 1
        }, {
            "imageScaleMode": constants.IMAGE_SCALE_MODE_MAINTAIN_ASPECT_RATIO,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, {});
        var lblIconOptions = new kony.ui.Label({
            "centerX": "50%",
            "centerY": "50%",
            "height": "15px",
            "id": "lblIconOptions",
            "isVisible": true,
            "left": "14dp",
            "skin": "sknFontIconOptionMenu",
            "text": "",
            "width": "15px",
            "zIndex": 1
        }, {
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, {});
        flxOptions.add(imgOptions, lblIconOptions);
        var flxAccordianContainer = new kony.ui.FlexContainer({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "clipBounds": true,
            "height": "50dp",
            "id": "flxAccordianContainer",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "left": "0dp",
            "isModalContainer": false,
            "onClick": controller.AS_FlexContainer_b51f6487d542463d900b0cddde953d43,
            "right": "115px",
            "skin": "slFbox",
            "top": "0dp",
            "zIndex": 1
        }, {}, {});
        flxAccordianContainer.setDefaultUnit(kony.flex.DP);
        var flxArrow = new kony.ui.FlexContainer({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "centerY": "50%",
            "clipBounds": true,
            "height": "15dp",
            "id": "flxArrow",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "left": "15dp",
            "isModalContainer": false,
            "skin": "slFbox",
            "top": "0dp",
            "width": "15px",
            "zIndex": 1
        }, {}, {});
        flxArrow.setDefaultUnit(kony.flex.DP);
        var fontIconImgViewDescription = new kony.ui.Label({
            "centerX": "50%",
            "centerY": "50%",
            "height": "15px",
            "id": "fontIconImgViewDescription",
            "isVisible": true,
            "left": "10dp",
            "skin": "sknfontIcon33333316px",
            "text": "",
            "width": "15px",
            "zIndex": 1
        }, {
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, {});
        flxArrow.add(fontIconImgViewDescription);
        var lblMFAConfigName = new kony.ui.Label({
            "centerY": "50%",
            "height": "100%",
            "id": "lblMFAConfigName",
            "isVisible": true,
            "left": "35px",
            "right": "0px",
            "skin": "sknLbl192b45LatoReg16px",
            "text": "Secure Access Code",
            "zIndex": 1
        }, {
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, {});
        flxAccordianContainer.add(flxArrow, lblMFAConfigName);
        var flxStatus = new kony.ui.FlexContainer({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "centerY": "50%",
            "clipBounds": true,
            "height": "20px",
            "id": "flxStatus",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "left": "86%",
            "isModalContainer": false,
            "skin": "slFbox",
            "width": "8%",
            "zIndex": 1
        }, {}, {});
        flxStatus.setDefaultUnit(kony.flex.DP);
        var lblServiceStatus = new kony.ui.Label({
            "centerY": "50%",
            "id": "lblServiceStatus",
            "isVisible": true,
            "left": "20px",
            "skin": "sknlblLato5bc06cBold14px",
            "text": "Active",
            "width": "45px",
            "zIndex": 1
        }, {
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, {});
        var imgServiceStatus = new kony.ui.Image2({
            "centerY": "50%",
            "height": "10dp",
            "id": "imgServiceStatus",
            "isVisible": true,
            "left": "0dp",
            "skin": "slImage",
            "src": "active_circle2x.png",
            "width": "10dp",
            "zIndex": 1
        }, {
            "imageScaleMode": constants.IMAGE_SCALE_MODE_MAINTAIN_ASPECT_RATIO,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, {});
        flxStatus.add(lblServiceStatus, imgServiceStatus);
        var flxViewEditButton = new kony.ui.Button({
            "centerY": "50%",
            "focusSkin": "sknbtnf7f7faLatoRegular12Px485c75Border1px485C75Radius20px",
            "height": "22px",
            "id": "flxViewEditButton",
            "isVisible": false,
            "right": "35px",
            "skin": "sknbtnf7f7faLatoRegular12Px485c75Border1px485C75Radius20px",
            "i18n_text": "kony.i18n.getLocalizedString(\"i18n.permission.Edit\")",
            "top": "15px",
            "width": "50px",
            "zIndex": 1
        }, {
            "contentAlignment": constants.CONTENT_ALIGN_CENTER,
            "displayText": true,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, {
            "hoverSkin": "sknbtnffffffLatoRegular12Px485c75Border1px485C75Radius20px"
        });
        flxMFAConfigsRow.add(flxOptions, flxAccordianContainer, flxStatus, flxViewEditButton);
        var flxMFAConfigDescription = new kony.ui.FlexContainer({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "clipBounds": true,
            "height": "150px",
            "id": "flxMFAConfigDescription",
            "isVisible": true,
            "layoutType": kony.flex.FLOW_VERTICAL,
            "isModalContainer": false,
            "skin": "slFbox",
            "top": "60dp",
            "width": "100%",
            "zIndex": 1
        }, {}, {});
        flxMFAConfigDescription.setDefaultUnit(kony.flex.DP);
        var flxRow1 = new kony.ui.FlexContainer({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "clipBounds": true,
            "height": "40px",
            "id": "flxRow1",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "left": "0dp",
            "isModalContainer": false,
            "skin": "slFbox",
            "top": "0dp",
            "width": "100%",
            "zIndex": 1
        }, {}, {});
        flxRow1.setDefaultUnit(kony.flex.DP);
        var flxColumn11 = new kony.ui.FlexContainer({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "clipBounds": true,
            "height": "40px",
            "id": "flxColumn11",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "left": "35px",
            "isModalContainer": false,
            "skin": "slFbox",
            "top": "0dp",
            "width": "30%",
            "zIndex": 1
        }, {}, {});
        flxColumn11.setDefaultUnit(kony.flex.DP);
        var lblHeading11 = new kony.ui.Label({
            "id": "lblHeading11",
            "isVisible": true,
            "left": "0px",
            "skin": "sknlblLato5d6c7f12px",
            "i18n_text": "kony.i18n.getLocalizedString(\"i18n.userwidgetmodel.lblHeading1\")",
            "top": "0px",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, {
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, {});
        var flxDataAndImage1 = new kony.ui.FlexContainer({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "bottom": "0px",
            "clipBounds": true,
            "height": "20px",
            "id": "flxDataAndImage1",
            "isVisible": true,
            "layoutType": kony.flex.FLOW_HORIZONTAL,
            "left": "0dp",
            "isModalContainer": false,
            "skin": "slFbox",
            "width": "100%",
            "zIndex": 1
        }, {}, {});
        flxDataAndImage1.setDefaultUnit(kony.flex.DP);
        var imgData1 = new kony.ui.Image2({
            "centerY": "50%",
            "height": "12px",
            "id": "imgData1",
            "isVisible": false,
            "left": "0dp",
            "right": "5px",
            "skin": "slImage",
            "src": "active_circle2x.png",
            "top": "0dp",
            "width": "12px",
            "zIndex": 1
        }, {
            "imageScaleMode": constants.IMAGE_SCALE_MODE_MAINTAIN_ASPECT_RATIO,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, {});
        var lblData11 = new kony.ui.Label({
            "centerY": "50%",
            "id": "lblData11",
            "isVisible": true,
            "left": "0px",
            "skin": "sknlblLatoBold35475f14px",
            "i18n_text": "kony.i18n.getLocalizedString(\"i18n.userwidgetmodel.lblData1\")",
            "top": "0px",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, {
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, {});
        flxDataAndImage1.add(imgData1, lblData11);
        flxColumn11.add(lblHeading11, flxDataAndImage1);
        var flxColumn12 = new kony.ui.FlexContainer({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "clipBounds": true,
            "height": "40px",
            "id": "flxColumn12",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "left": "40%",
            "isModalContainer": false,
            "skin": "slFbox",
            "top": "0dp",
            "width": "30%",
            "zIndex": 1
        }, {}, {});
        flxColumn12.setDefaultUnit(kony.flex.DP);
        var lblHeading12 = new kony.ui.Label({
            "id": "lblHeading12",
            "isVisible": true,
            "left": "0px",
            "skin": "sknlblLato5d6c7f12px",
            "i18n_text": "kony.i18n.getLocalizedString(\"i18n.userwidgetmodel.lblHeading1\")",
            "top": "0px",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, {
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, {});
        var flxDataAndImage2 = new kony.ui.FlexContainer({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "bottom": "0px",
            "clipBounds": true,
            "height": "20px",
            "id": "flxDataAndImage2",
            "isVisible": true,
            "layoutType": kony.flex.FLOW_HORIZONTAL,
            "left": "0dp",
            "isModalContainer": false,
            "skin": "slFbox",
            "width": "100%",
            "zIndex": 1
        }, {}, {});
        flxDataAndImage2.setDefaultUnit(kony.flex.DP);
        var imgData2 = new kony.ui.Image2({
            "centerY": "50%",
            "height": "12px",
            "id": "imgData2",
            "isVisible": false,
            "left": "0dp",
            "right": "5px",
            "skin": "slImage",
            "src": "active_circle2x.png",
            "top": "0dp",
            "width": "12px",
            "zIndex": 1
        }, {
            "imageScaleMode": constants.IMAGE_SCALE_MODE_MAINTAIN_ASPECT_RATIO,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, {});
        var lblData12 = new kony.ui.Label({
            "centerY": "50%",
            "id": "lblData12",
            "isVisible": true,
            "left": "0px",
            "skin": "sknlblLatoBold35475f14px",
            "i18n_text": "kony.i18n.getLocalizedString(\"i18n.userwidgetmodel.lblData1\")",
            "top": "0px",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, {
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, {});
        flxDataAndImage2.add(imgData2, lblData12);
        flxColumn12.add(lblHeading12, flxDataAndImage2);
        var flxColumn13 = new kony.ui.FlexContainer({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "clipBounds": true,
            "height": "40px",
            "id": "flxColumn13",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "left": "72%",
            "isModalContainer": false,
            "skin": "slFbox",
            "top": "0dp",
            "width": "30%",
            "zIndex": 1
        }, {}, {});
        flxColumn13.setDefaultUnit(kony.flex.DP);
        var lblHeading13 = new kony.ui.Label({
            "id": "lblHeading13",
            "isVisible": true,
            "left": "0px",
            "skin": "sknlblLato5d6c7f12px",
            "i18n_text": "kony.i18n.getLocalizedString(\"i18n.userwidgetmodel.lblHeading1\")",
            "top": "0px",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, {
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, {});
        var flxDataAndImage3 = new kony.ui.FlexContainer({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "bottom": "0px",
            "clipBounds": true,
            "height": "20px",
            "id": "flxDataAndImage3",
            "isVisible": true,
            "layoutType": kony.flex.FLOW_HORIZONTAL,
            "left": "0dp",
            "isModalContainer": false,
            "skin": "slFbox",
            "width": "100%",
            "zIndex": 1
        }, {}, {});
        flxDataAndImage3.setDefaultUnit(kony.flex.DP);
        var imgData3 = new kony.ui.Image2({
            "centerY": "50%",
            "height": "12px",
            "id": "imgData3",
            "isVisible": false,
            "left": "0dp",
            "right": "5px",
            "skin": "slImage",
            "src": "active_circle2x.png",
            "top": "0dp",
            "width": "12px",
            "zIndex": 1
        }, {
            "imageScaleMode": constants.IMAGE_SCALE_MODE_MAINTAIN_ASPECT_RATIO,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, {});
        var lblData13 = new kony.ui.Label({
            "centerY": "50%",
            "id": "lblData13",
            "isVisible": true,
            "left": "0px",
            "skin": "sknlblLatoBold35475f14px",
            "i18n_text": "kony.i18n.getLocalizedString(\"i18n.userwidgetmodel.lblData1\")",
            "top": "0px",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, {
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, {});
        flxDataAndImage3.add(imgData3, lblData13);
        flxColumn13.add(lblHeading13, flxDataAndImage3);
        flxRow1.add(flxColumn11, flxColumn12, flxColumn13);
        var lblSubHeadeing = new kony.ui.Label({
            "id": "lblSubHeadeing",
            "isVisible": true,
            "left": "35px",
            "skin": "sknLbl9ca9baFontSize12px",
            "text": "Authentication Failure Settings ",
            "top": "25px",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, {
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, {});
        var flxRow2 = new kony.ui.FlexContainer({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "clipBounds": true,
            "height": "40px",
            "id": "flxRow2",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "left": "0dp",
            "isModalContainer": false,
            "skin": "slFbox",
            "top": "15px",
            "width": "100%",
            "zIndex": 1
        }, {}, {});
        flxRow2.setDefaultUnit(kony.flex.DP);
        var flxColumn21 = new kony.ui.FlexContainer({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "clipBounds": true,
            "height": "40px",
            "id": "flxColumn21",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "left": "35px",
            "isModalContainer": false,
            "skin": "slFbox",
            "top": "0dp",
            "width": "30%",
            "zIndex": 1
        }, {}, {});
        flxColumn21.setDefaultUnit(kony.flex.DP);
        var lblHeading21 = new kony.ui.Label({
            "id": "lblHeading21",
            "isVisible": true,
            "left": "0px",
            "skin": "sknlblLato5d6c7f12px",
            "i18n_text": "kony.i18n.getLocalizedString(\"i18n.userwidgetmodel.lblHeading1\")",
            "top": "0px",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, {
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, {});
        var flxDataAndImage21 = new kony.ui.FlexContainer({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "bottom": "0px",
            "clipBounds": true,
            "height": "20px",
            "id": "flxDataAndImage21",
            "isVisible": true,
            "layoutType": kony.flex.FLOW_HORIZONTAL,
            "left": "0dp",
            "isModalContainer": false,
            "skin": "slFbox",
            "width": "100%",
            "zIndex": 1
        }, {}, {});
        flxDataAndImage21.setDefaultUnit(kony.flex.DP);
        var CopyimgData0f5f2cb6ee81d48 = new kony.ui.Image2({
            "centerY": "50%",
            "height": "12px",
            "id": "CopyimgData0f5f2cb6ee81d48",
            "isVisible": false,
            "left": "0dp",
            "right": "5px",
            "skin": "slImage",
            "src": "active_circle2x.png",
            "top": "0dp",
            "width": "12px",
            "zIndex": 1
        }, {
            "imageScaleMode": constants.IMAGE_SCALE_MODE_MAINTAIN_ASPECT_RATIO,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, {});
        var lblData21 = new kony.ui.Label({
            "centerY": "50%",
            "id": "lblData21",
            "isVisible": true,
            "left": "0px",
            "skin": "sknlblLatoBold35475f14px",
            "i18n_text": "kony.i18n.getLocalizedString(\"i18n.userwidgetmodel.lblData1\")",
            "top": "0px",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, {
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, {});
        flxDataAndImage21.add(CopyimgData0f5f2cb6ee81d48, lblData21);
        flxColumn21.add(lblHeading21, flxDataAndImage21);
        var flxColumn22 = new kony.ui.FlexContainer({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "clipBounds": true,
            "height": "40px",
            "id": "flxColumn22",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "left": "40%",
            "isModalContainer": false,
            "skin": "slFbox",
            "top": "0dp",
            "width": "30%",
            "zIndex": 1
        }, {}, {});
        flxColumn22.setDefaultUnit(kony.flex.DP);
        var lblHeading22 = new kony.ui.Label({
            "id": "lblHeading22",
            "isVisible": true,
            "left": "0px",
            "skin": "sknlblLato5d6c7f12px",
            "i18n_text": "kony.i18n.getLocalizedString(\"i18n.userwidgetmodel.lblHeading1\")",
            "top": "0px",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, {
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, {});
        var flxDataAndImage22 = new kony.ui.FlexContainer({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "bottom": "0px",
            "clipBounds": true,
            "height": "20px",
            "id": "flxDataAndImage22",
            "isVisible": true,
            "layoutType": kony.flex.FLOW_HORIZONTAL,
            "left": "0dp",
            "isModalContainer": false,
            "skin": "slFbox",
            "width": "100%",
            "zIndex": 1
        }, {}, {});
        flxDataAndImage22.setDefaultUnit(kony.flex.DP);
        var CopyimgData0b21f7c7979eb4b = new kony.ui.Image2({
            "centerY": "50%",
            "height": "12px",
            "id": "CopyimgData0b21f7c7979eb4b",
            "isVisible": false,
            "left": "0dp",
            "right": "5px",
            "skin": "slImage",
            "src": "active_circle2x.png",
            "top": "0dp",
            "width": "12px",
            "zIndex": 1
        }, {
            "imageScaleMode": constants.IMAGE_SCALE_MODE_MAINTAIN_ASPECT_RATIO,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, {});
        var lblData22 = new kony.ui.Label({
            "centerY": "50%",
            "id": "lblData22",
            "isVisible": true,
            "left": "0px",
            "skin": "sknlblLatoBold35475f14px",
            "i18n_text": "kony.i18n.getLocalizedString(\"i18n.userwidgetmodel.lblData1\")",
            "top": "0px",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, {
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, {});
        flxDataAndImage22.add(CopyimgData0b21f7c7979eb4b, lblData22);
        flxColumn22.add(lblHeading22, flxDataAndImage22);
        var flxColumn23 = new kony.ui.FlexContainer({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "clipBounds": true,
            "height": "40px",
            "id": "flxColumn23",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "left": "72%",
            "isModalContainer": false,
            "skin": "slFbox",
            "top": "0dp",
            "width": "30%",
            "zIndex": 1
        }, {}, {});
        flxColumn23.setDefaultUnit(kony.flex.DP);
        var lblHeading23 = new kony.ui.Label({
            "id": "lblHeading23",
            "isVisible": true,
            "left": "0px",
            "skin": "sknlblLato5d6c7f12px",
            "i18n_text": "kony.i18n.getLocalizedString(\"i18n.userwidgetmodel.lblHeading1\")",
            "top": "0px",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, {
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, {});
        var flxDataAndImage23 = new kony.ui.FlexContainer({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "bottom": "0px",
            "clipBounds": true,
            "height": "20px",
            "id": "flxDataAndImage23",
            "isVisible": true,
            "layoutType": kony.flex.FLOW_HORIZONTAL,
            "left": "0dp",
            "isModalContainer": false,
            "skin": "slFbox",
            "width": "100%",
            "zIndex": 1
        }, {}, {});
        flxDataAndImage23.setDefaultUnit(kony.flex.DP);
        var CopyimgData0ff2d3d10abd541 = new kony.ui.Image2({
            "centerY": "50%",
            "height": "12px",
            "id": "CopyimgData0ff2d3d10abd541",
            "isVisible": false,
            "left": "0dp",
            "right": "5px",
            "skin": "slImage",
            "src": "active_circle2x.png",
            "top": "0dp",
            "width": "12px",
            "zIndex": 1
        }, {
            "imageScaleMode": constants.IMAGE_SCALE_MODE_MAINTAIN_ASPECT_RATIO,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, {});
        var lblData23 = new kony.ui.Label({
            "centerY": "50%",
            "id": "lblData23",
            "isVisible": true,
            "left": "0px",
            "skin": "sknlblLatoBold35475f14px",
            "i18n_text": "kony.i18n.getLocalizedString(\"i18n.userwidgetmodel.lblData1\")",
            "top": "0px",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, {
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, {});
        flxDataAndImage23.add(CopyimgData0ff2d3d10abd541, lblData23);
        flxColumn23.add(lblHeading23, flxDataAndImage23);
        flxRow2.add(flxColumn21, flxColumn22, flxColumn23);
        flxMFAConfigDescription.add(flxRow1, lblSubHeadeing, flxRow2);
        var lblSeperator = new kony.ui.Label({
            "bottom": 0,
            "height": "1px",
            "id": "lblSeperator",
            "isVisible": true,
            "left": "0dp",
            "right": "0dp",
            "skin": "sknlblSeperator",
            "text": ".",
            "zIndex": 1
        }, {
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, {});
        flxContent.add(flxMFAConfigsRow, flxMFAConfigDescription, lblSeperator);
        flxSegMFAConfigsSelected.add(flxContent);
        return flxSegMFAConfigsSelected;
    }
})