define("flxTAndC", function() {
    return function(controller) {
        var flxTAndC = new kony.ui.FlexContainer({
            "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
            "clipBounds": true,
            "id": "flxTAndC",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "left": "0dp",
            "isModalContainer": false,
            "skin": "slFbox",
            "top": "0dp",
            "width": "100%"
        }, {}, {});
        flxTAndC.setDefaultUnit(kony.flex.DP);
        var flxTAndCHeader = new kony.ui.FlexContainer({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "clipBounds": true,
            "height": "41dp",
            "id": "flxTAndCHeader",
            "isVisible": true,
            "layoutType": kony.flex.FLOW_VERTICAL,
            "left": "0dp",
            "isModalContainer": false,
            "skin": "sknflxffffffop100",
            "top": "0dp",
            "width": "100%"
        }, {}, {});
        flxTAndCHeader.setDefaultUnit(kony.flex.DP);
        var lblSubHeading = new kony.ui.Label({
            "height": "40dp",
            "id": "lblSubHeading",
            "isVisible": true,
            "left": "0dp",
            "skin": "sknlblLato5d6c7f13px",
            "text": "SUB - HEADING 1",
            "top": "0dp",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, {
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, {});
        var lblSeperator = new kony.ui.Label({
            "bottom": "5px",
            "height": "1px",
            "id": "lblSeperator",
            "isVisible": true,
            "left": "0px",
            "skin": "sknlblSeperator",
            "text": ".",
            "width": "100%",
            "zIndex": 1
        }, {
            "contentAlignment": constants.CONTENT_ALIGN_TOP_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, {});
        flxTAndCHeader.add(lblSubHeading, lblSeperator);
        var rtxTermsAndCond = new kony.ui.RichText({
            "bottom": "10dp",
            "id": "rtxTermsAndCond",
            "isVisible": true,
            "left": "0dp",
            "linkSkin": "defRichTextLink",
            "right": "10px",
            "skin": "sknlRichTextLatoRegular",
            "text": "You authorize, XYZ Bank, to:\\n\n\n1. Pull a credit report on you and co-applicant (if any) in order to make a credit decision.\\n\n\n2. Use the information on this application and any other information we or our affiliates have about you to determine your ability to pay, as required by federal law.\\n\n\n3. Verify, at our option and for your benefit, your application information.\\n\n\n4. Contact you or receive a detailed message or messages at the telephone, email or direct mail contact data you provided above for purposes of fulfilling this inquiry about automobile loan financing even if you have previously registered on a Do No Call registry or requested XYZ Bank not to send marketing information to you by email and/or direct mail.\\n",
            "top": "55dp",
            "zIndex": 1
        }, {
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, {});
        flxTAndC.add(flxTAndCHeader, rtxTermsAndCond);
        return flxTAndC;
    }
})