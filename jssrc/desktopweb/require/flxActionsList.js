define("flxActionsList", function() {
    return function(controller) {
        var flxActionsList = new kony.ui.FlexContainer({
            "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
            "clipBounds": true,
            "id": "flxActionsList",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "left": "0dp",
            "isModalContainer": false,
            "skin": "sknflxffffffop100",
            "top": "0dp",
            "width": "100%"
        }, {}, {});
        flxActionsList.setDefaultUnit(kony.flex.DP);
        var flxCheckBox = new kony.ui.FlexContainer({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "clipBounds": true,
            "height": "12px",
            "id": "flxCheckBox",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "left": "1.20%",
            "isModalContainer": false,
            "skin": "slFbox",
            "top": "7dp",
            "width": "12px",
            "zIndex": 2
        }, {}, {});
        flxCheckBox.setDefaultUnit(kony.flex.DP);
        var imgCheckBox = new kony.ui.Image2({
            "centerY": "50%",
            "height": "100%",
            "id": "imgCheckBox",
            "isVisible": true,
            "left": "0%",
            "skin": "slImage",
            "src": "checkboxnormal.png",
            "width": "100%",
            "zIndex": 2
        }, {
            "imageScaleMode": constants.IMAGE_SCALE_MODE_MAINTAIN_ASPECT_RATIO,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, {});
        flxCheckBox.add(imgCheckBox);
        var lblDescription = new kony.ui.Label({
            "bottom": "5dp",
            "id": "lblDescription",
            "isVisible": true,
            "left": "4%",
            "skin": "sknlblLatoBold35475f14px",
            "text": "View Recepient",
            "top": "5dp",
            "width": "80%",
            "zIndex": 1
        }, {
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, {});
        var flxFeatureStatus = new kony.ui.FlexContainer({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "bottom": "5dp",
            "clipBounds": true,
            "height": "15dp",
            "id": "flxFeatureStatus",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "left": "85%",
            "isModalContainer": false,
            "skin": "slFbox",
            "top": "5dp",
            "width": "100dp"
        }, {}, {});
        flxFeatureStatus.setDefaultUnit(kony.flex.DP);
        var lblIconStatus = new kony.ui.Label({
            "centerY": "52%",
            "height": "12dp",
            "id": "lblIconStatus",
            "isVisible": true,
            "left": "0dp",
            "skin": "sknFontIconActivate",
            "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmAlertsManagement.lblViewAlertStatusKey\")",
            "width": "12dp",
            "zIndex": 1
        }, {
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, {});
        var lblFeatureStatusValue = new kony.ui.Label({
            "centerY": "50%",
            "id": "lblFeatureStatusValue",
            "isVisible": true,
            "left": 15,
            "skin": "sknLbl485C75LatoRegular13Px",
            "text": "Active",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, {
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, {});
        flxFeatureStatus.add(lblIconStatus, lblFeatureStatusValue);
        flxActionsList.add(flxCheckBox, lblDescription, flxFeatureStatus);
        return flxActionsList;
    }
})