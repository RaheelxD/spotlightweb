define("userflxSearchDropDownController", {
    hide: function() {
        this.executeOnParent("toggleVisibility");
    }
});
define("flxSearchDropDownControllerActions", {
    /*
      This is an auto generated file and any modifications to it may result in corruption of the action sequence.
    */
});
define("flxSearchDropDownController", ["userflxSearchDropDownController", "flxSearchDropDownControllerActions"], function() {
    var controller = require("userflxSearchDropDownController");
    var controllerActions = ["flxSearchDropDownControllerActions"];
    return kony.visualizer.mixinControllerActions(controller, controllerActions);
});
