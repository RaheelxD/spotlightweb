define("userflxAddPermissionsController", {
    addPermission: function() {
        var index = kony.application.getCurrentForm().segAddOptions.selectedIndex;
        var rowIndex = index[0];
        var data = kony.application.getCurrentForm().segAddOptions.data;
        var lblOptionText = data[rowIndex].lblPermissionsName;
        var toAdd = {
            "imgClose": "close_blue.png",
            "lblOption": "" + lblOptionText
        };
        var data2 = kony.application.getCurrentForm().segSelectedOptions.data;
        data2.push(toAdd);
        kony.application.getCurrentForm().segSelectedOptions.setData(data2);
        kony.application.getCurrentForm().forceLayout();
    },
});
define("flxAddPermissionsControllerActions", {
    /*
        This is an auto generated file and any modifications to it may result in corruption of the action sequence.
    */
    AS_Button_c900685a8a2046e9b7341cf39934bdec: function AS_Button_c900685a8a2046e9b7341cf39934bdec(eventobject, context) {
        var self = this;
        this.executeOnParent("addPermissionstoRole");
    }
});
define("flxAddPermissionsController", ["userflxAddPermissionsController", "flxAddPermissionsControllerActions"], function() {
    var controller = require("userflxAddPermissionsController");
    var controllerActions = ["flxAddPermissionsControllerActions"];
    return kony.visualizer.mixinControllerActions(controller, controllerActions);
});
