define("flxDepositApplicationsStarted", function() {
    return function(controller) {
        var flxDepositApplicationsStarted = new kony.ui.FlexContainer({
            "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
            "clipBounds": true,
            "id": "flxDepositApplicationsStarted",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "left": "0dp",
            "isModalContainer": false,
            "skin": "slFbox",
            "top": "0dp",
            "width": "100%"
        }, {}, {});
        flxDepositApplicationsStarted.setDefaultUnit(kony.flex.DP);
        var flxStartedApplications = new kony.ui.FlexContainer({
            "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
            "bottom": "15dp",
            "clipBounds": true,
            "id": "flxStartedApplications",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "left": "0dp",
            "isModalContainer": false,
            "skin": "slFbox",
            "top": "0dp",
            "width": "100%"
        }, {}, {});
        flxStartedApplications.setDefaultUnit(kony.flex.DP);
        var lblDepositApplIdStarted = new kony.ui.Label({
            "id": "lblDepositApplIdStarted",
            "isVisible": true,
            "left": "35px",
            "skin": "sknlblLatoBold35475f14px",
            "text": "95236853443",
            "top": "15dp",
            "width": "23%",
            "zIndex": 1
        }, {
            "contentAlignment": constants.CONTENT_ALIGN_TOP_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, {});
        var lblDepositApplProductStarted = new kony.ui.Label({
            "id": "lblDepositApplProductStarted",
            "isVisible": true,
            "left": "27%",
            "skin": "sknlblLatoBold35475f14px",
            "text": "12 Month Term CD",
            "top": "15dp",
            "width": "25%",
            "zIndex": 1
        }, {
            "contentAlignment": constants.CONTENT_ALIGN_TOP_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, {});
        var lblDepositApplCreatedStarted = new kony.ui.Label({
            "id": "lblDepositApplCreatedStarted",
            "isVisible": true,
            "left": "55%",
            "skin": "sknlblLatoBold35475f14px",
            "text": "14-06-2019",
            "top": "15dp",
            "width": "20%",
            "zIndex": 1
        }, {
            "contentAlignment": constants.CONTENT_ALIGN_TOP_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, {});
        var lblDepositApplModifiedStarted = new kony.ui.Label({
            "id": "lblDepositApplModifiedStarted",
            "isVisible": true,
            "right": "35dp",
            "skin": "sknlblLatoBold35475f14px",
            "text": "17-06-2019",
            "top": "15dp",
            "width": "20%",
            "zIndex": 1
        }, {
            "contentAlignment": constants.CONTENT_ALIGN_TOP_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, {});
        var flxResumeApplication = new kony.ui.FlexContainer({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "clipBounds": true,
            "height": "20dp",
            "id": "flxResumeApplication",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "isModalContainer": false,
            "right": "35dp",
            "skin": "hoverhandSkin",
            "top": "10dp",
            "width": "20dp"
        }, {}, {});
        flxResumeApplication.setDefaultUnit(kony.flex.DP);
        var lblIconResume = new kony.ui.Label({
            "height": "20px",
            "id": "lblIconResume",
            "isVisible": true,
            "left": "0dp",
            "skin": "sknIcon20px",
            "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmAdManagement.ResumeIcon\")",
            "top": "0px",
            "width": "20px",
            "zIndex": 1
        }, {
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, {});
        flxResumeApplication.add(lblIconResume);
        flxStartedApplications.add(lblDepositApplIdStarted, lblDepositApplProductStarted, lblDepositApplCreatedStarted, lblDepositApplModifiedStarted, flxResumeApplication);
        var lblStartedSeperator = new kony.ui.Label({
            "bottom": "0dp",
            "height": "1dp",
            "id": "lblStartedSeperator",
            "isVisible": true,
            "left": "20dp",
            "right": 20,
            "skin": "sknlblSeperator",
            "text": "Label",
            "zIndex": 1
        }, {
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, {});
        flxDepositApplicationsStarted.add(flxStartedApplications, lblStartedSeperator);
        return flxDepositApplicationsStarted;
    }
})