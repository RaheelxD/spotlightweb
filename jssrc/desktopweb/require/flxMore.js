define("flxMore", function() {
    return function(controller) {
        var flxMore = new kony.ui.FlexContainer({
            "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
            "clipBounds": true,
            "id": "flxMore",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "left": "0dp",
            "isModalContainer": false,
            "skin": "sknbackGroundffffff100",
            "top": "0dp",
            "width": "100%"
        }, {}, {
            "hoverSkin": "sknFlxffffffCursorPointer"
        });
        flxMore.setDefaultUnit(kony.flex.DP);
        var lblName = new kony.ui.Label({
            "id": "lblName",
            "isVisible": true,
            "left": "0dp",
            "skin": "sknlbl485C75LatoSemiBold13px",
            "text": "Granville Street, Vancover, BC, Canada",
            "top": "10dp",
            "width": "100%",
            "zIndex": 1
        }, {
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, {});
        flxMore.add(lblName);
        return flxMore;
    }
})