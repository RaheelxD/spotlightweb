define("flxContractsFAHeaderView", function() {
    return function(controller) {
        var flxContractsFAHeaderView = new kony.ui.FlexContainer({
            "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
            "clipBounds": true,
            "id": "flxContractsFAHeaderView",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "left": "0dp",
            "isModalContainer": false,
            "skin": "sknFlxFFFFFF1000",
            "top": "0dp",
            "width": "100%"
        }, {}, {});
        flxContractsFAHeaderView.setDefaultUnit(kony.flex.DP);
        var flxHeader = new kony.ui.FlexContainer({
            "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
            "clipBounds": true,
            "id": "flxHeader",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "left": "30px",
            "isModalContainer": false,
            "right": "20px",
            "skin": "slFbox",
            "top": "0px",
            "zIndex": 1
        }, {}, {});
        flxHeader.setDefaultUnit(kony.flex.DP);
        var flxFeatureDetails = new kony.ui.FlexContainer({
            "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
            "clipBounds": true,
            "id": "flxFeatureDetails",
            "isVisible": true,
            "layoutType": kony.flex.FLOW_VERTICAL,
            "left": "27dp",
            "isModalContainer": false,
            "right": "0dp",
            "skin": "slFbox",
            "top": "0dp"
        }, {}, {});
        flxFeatureDetails.setDefaultUnit(kony.flex.DP);
        var flxRow1 = new kony.ui.FlexContainer({
            "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
            "clipBounds": true,
            "id": "flxRow1",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "left": "0dp",
            "isModalContainer": false,
            "right": "0dp",
            "skin": "slFbox",
            "top": "12dp",
            "width": "100%"
        }, {}, {});
        flxRow1.setDefaultUnit(kony.flex.DP);
        var lblFeatureName = new kony.ui.Label({
            "id": "lblFeatureName",
            "isVisible": true,
            "left": "0px",
            "skin": "sknLatoRegular192B4516px",
            "text": "International Transfers",
            "top": "0",
            "width": "470dp",
            "zIndex": 1
        }, {
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, {});
        var flxFeatureStatus = new kony.ui.FlexContainer({
            "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
            "clipBounds": true,
            "id": "flxFeatureStatus",
            "isVisible": true,
            "layoutType": kony.flex.FLOW_HORIZONTAL,
            "reverseLayoutDirection": false,
            "left": "90%",
            "isModalContainer": false,
            "skin": "slFbox",
            "top": "0dp",
            "width": "60dp"
        }, {}, {});
        flxFeatureStatus.setDefaultUnit(kony.flex.DP);
        var statusIcon = new kony.ui.Label({
            "id": "statusIcon",
            "isVisible": true,
            "right": "5dp",
            "skin": "sknFontIconActivate",
            "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmAlertsManagement.lblViewAlertStatusKey\")",
            "top": 2,
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, {
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, {});
        var statusValue = new kony.ui.Label({
            "id": "statusValue",
            "isVisible": true,
            "skin": "sknlblLato5bc06cBold14px",
            "i18n_text": "kony.i18n.getLocalizedString(\"i18n.secureimage.Active\")",
            "top": "0px",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, {
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, {});
        flxFeatureStatus.add(statusIcon, statusValue);
        var lblCustom = new kony.ui.Label({
            "id": "lblCustom",
            "isVisible": false,
            "left": "82.50%",
            "skin": "sknLbl485c75LatoRegItalic13px",
            "text": "Custom",
            "top": "0dp",
            "width": "52dp"
        }, {
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, {});
        flxRow1.add(lblFeatureName, flxFeatureStatus, lblCustom);
        var flxSelectedActions = new kony.ui.FlexContainer({
            "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
            "bottom": "10dp",
            "clipBounds": true,
            "id": "flxSelectedActions",
            "isVisible": true,
            "layoutType": kony.flex.FLOW_HORIZONTAL,
            "left": "0dp",
            "isModalContainer": false,
            "skin": "slFbox",
            "top": "10dp",
            "width": "250dp"
        }, {}, {});
        flxSelectedActions.setDefaultUnit(kony.flex.DP);
        var lblAvailableActions = new kony.ui.Label({
            "id": "lblAvailableActions",
            "isVisible": true,
            "left": "0dp",
            "skin": "sknLbl485C75LatoRegular13Px",
            "text": "Selected Actions:",
            "top": "0dp",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, {
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, {});
        var lblCountActions = new kony.ui.Label({
            "id": "lblCountActions",
            "isVisible": true,
            "left": "10dp",
            "skin": "sknLbl192B45LatoSemiBold13px",
            "text": "2",
            "top": "0dp",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, {
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, {});
        var lblTotalActions = new kony.ui.Label({
            "id": "lblTotalActions",
            "isVisible": true,
            "left": "3dp",
            "skin": "sknLbl485C75LatoRegular13Px",
            "text": "of 2",
            "top": "0dp",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, {
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, {});
        flxSelectedActions.add(lblAvailableActions, lblCountActions, lblTotalActions);
        flxFeatureDetails.add(flxRow1, flxSelectedActions);
        var flxArrow = new kony.ui.FlexContainer({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "clipBounds": true,
            "height": "20dp",
            "id": "flxArrow",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "left": "5dp",
            "isModalContainer": false,
            "skin": "sknCursor",
            "top": "12dp",
            "width": "20dp"
        }, {}, {
            "hoverSkin": "sknCursor"
        });
        flxArrow.setDefaultUnit(kony.flex.DP);
        var lblArrow = new kony.ui.Label({
            "centerY": "50%",
            "id": "lblArrow",
            "isVisible": true,
            "left": "0",
            "skin": "sknIcon00000015px",
            "text": "",
            "width": kony.flex.USE_PREFFERED_SIZE
        }, {
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, {});
        flxArrow.add(lblArrow);
        flxHeader.add(flxFeatureDetails, flxArrow);
        var flxViewActionHeader = new kony.ui.FlexContainer({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "clipBounds": true,
            "height": "45px",
            "id": "flxViewActionHeader",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "left": "57px",
            "isModalContainer": false,
            "right": "20px",
            "skin": "slFbox",
            "top": "64px"
        }, {}, {});
        flxViewActionHeader.setDefaultUnit(kony.flex.DP);
        var lblActionHeader = new kony.ui.Label({
            "centerY": "50%",
            "id": "lblActionHeader",
            "isVisible": true,
            "left": "0dp",
            "skin": "sknlblLato696c7312px",
            "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmMfascenarios.ACTION_CAP\")",
            "top": 0,
            "width": "10%",
            "zIndex": 1
        }, {
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, {});
        var lblActionDescHeader = new kony.ui.Label({
            "centerY": "50%",
            "id": "lblActionDescHeader",
            "isVisible": true,
            "left": "30%",
            "skin": "sknlblLato696c7312px",
            "i18n_text": "kony.i18n.getLocalizedString(\"i18n.products.Description_CAPS\")",
            "top": 0,
            "width": "10%",
            "zIndex": 1
        }, {
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, {});
        var lblActionStatusHeader = new kony.ui.Label({
            "centerY": "50%",
            "id": "lblActionStatusHeader",
            "isVisible": true,
            "left": "90%",
            "skin": "sknlblLato696c7312px",
            "i18n_text": "kony.i18n.getLocalizedString(\"i18n.roles.STATUS\")",
            "top": 0,
            "width": "50px",
            "zIndex": 1
        }, {
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, {});
        var lblFASeperator2 = new kony.ui.Label({
            "bottom": 0,
            "height": "1px",
            "id": "lblFASeperator2",
            "isVisible": true,
            "left": "0px",
            "right": "0px",
            "skin": "sknlblSeperatorD7D9E0",
            "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmCSR.lblSeperator\")",
            "zIndex": 1
        }, {
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, {});
        flxViewActionHeader.add(lblActionHeader, lblActionDescHeader, lblActionStatusHeader, lblFASeperator2);
        var lblFASeperator1 = new kony.ui.Label({
            "height": "1px",
            "id": "lblFASeperator1",
            "isVisible": true,
            "left": "0px",
            "right": "0dp",
            "skin": "sknlblSeperatorD7D9E0",
            "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmCSR.lblSeperator\")",
            "top": "64dp",
            "zIndex": 1
        }, {
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, {});
        var lblFASeperator3 = new kony.ui.Label({
            "height": "1px",
            "id": "lblFASeperator3",
            "isVisible": true,
            "left": "0px",
            "right": "0dp",
            "skin": "sknlblSeperatorD7D9E0",
            "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmCSR.lblSeperator\")",
            "top": "0dp",
            "zIndex": 1
        }, {
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, {});
        flxContractsFAHeaderView.add(flxHeader, flxViewActionHeader, lblFASeperator1, lblFASeperator3);
        return flxContractsFAHeaderView;
    }
})