define("flxRejectedRequests", function() {
    return function(controller) {
        var flxRejectedRequests = new kony.ui.FlexContainer({
            "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
            "clipBounds": true,
            "id": "flxRejectedRequests",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "isModalContainer": false,
            "skin": "sknflxffffffop100"
        }, {}, {
            "hoverSkin": "sknfbfcfc"
        });
        flxRejectedRequests.setDefaultUnit(kony.flex.DP);
        var lblContractId = new kony.ui.Label({
            "bottom": "15px",
            "id": "lblContractId",
            "isVisible": true,
            "left": "30px",
            "skin": "sknLbl485C75LatoRegular13Px",
            "text": "XYZ Company",
            "top": "15px",
            "width": "15%",
            "zIndex": 100
        }, {
            "contentAlignment": constants.CONTENT_ALIGN_TOP_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, {});
        var lblContractName = new kony.ui.Label({
            "bottom": "5px",
            "id": "lblContractName",
            "isVisible": true,
            "left": "18%",
            "skin": "sknLbl485C75LatoRegular13Px",
            "text": "XYZ Company",
            "top": "15px",
            "width": "18%",
            "zIndex": 100
        }, {
            "contentAlignment": constants.CONTENT_ALIGN_TOP_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, {});
        var lblServices = new kony.ui.Label({
            "bottom": "15px",
            "id": "lblServices",
            "isVisible": true,
            "left": "38%",
            "skin": "sknLbl485C75LatoRegular13Px",
            "text": "ACH_Collection_1",
            "top": "15px",
            "width": "12%",
            "zIndex": 1
        }, {
            "contentAlignment": constants.CONTENT_ALIGN_TOP_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, {});
        var lblSubmittedOn = new kony.ui.Label({
            "bottom": "5px",
            "id": "lblSubmittedOn",
            "isVisible": true,
            "left": "50%",
            "skin": "sknLbl485C75LatoRegular13Px",
            "text": "ACH_Collection_1",
            "top": "15px",
            "width": "17%",
            "zIndex": 1
        }, {
            "contentAlignment": constants.CONTENT_ALIGN_TOP_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, {});
        var lblRejectedOn = new kony.ui.Label({
            "id": "lblRejectedOn",
            "isVisible": true,
            "left": "67%",
            "skin": "sknLbl485C75LatoRegular13Px",
            "text": "17/08/2017 10:30PM",
            "top": "15px",
            "width": "17%",
            "zIndex": 1
        }, {
            "contentAlignment": constants.CONTENT_ALIGN_TOP_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, {});
        var lblRejectedBy = new kony.ui.Label({
            "id": "lblRejectedBy",
            "isVisible": true,
            "left": "84%",
            "skin": "sknLbl485C75LatoRegular13Px",
            "text": "Admin",
            "top": "15px",
            "width": "16%",
            "zIndex": 1
        }, {
            "contentAlignment": constants.CONTENT_ALIGN_TOP_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, {});
        var lblSeperator = new kony.ui.Label({
            "bottom": "0dp",
            "height": "1px",
            "id": "lblSeperator",
            "isVisible": true,
            "left": "20dp",
            "right": "20dp",
            "skin": "sknlblSeperator",
            "text": ".",
            "zIndex": 1
        }, {
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, {});
        flxRejectedRequests.add(lblContractId, lblContractName, lblServices, lblSubmittedOn, lblRejectedOn, lblRejectedBy, lblSeperator);
        return flxRejectedRequests;
    }
})