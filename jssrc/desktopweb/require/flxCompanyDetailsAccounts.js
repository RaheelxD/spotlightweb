define("flxCompanyDetailsAccounts", function() {
    return function(controller) {
        var flxCompanyDetailsAccounts = new kony.ui.FlexContainer({
            "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
            "clipBounds": true,
            "id": "flxCompanyDetailsAccounts",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "left": "0dp",
            "isModalContainer": false,
            "skin": "sknFlxFFFFFF100O",
            "top": "0dp",
            "width": "100%"
        }, {}, {
            "hoverSkin": "sknfbfcfc"
        });
        flxCompanyDetailsAccounts.setDefaultUnit(kony.flex.DP);
        var flxCompanyAccounts = new kony.ui.FlexContainer({
            "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
            "clipBounds": true,
            "id": "flxCompanyAccounts",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "left": "0dp",
            "isModalContainer": false,
            "skin": "slFbox",
            "top": "0dp",
            "width": "100%",
            "zIndex": 1
        }, {}, {});
        flxCompanyAccounts.setDefaultUnit(kony.flex.DP);
        var lblAccountType = new kony.ui.Label({
            "bottom": "15dp",
            "id": "lblAccountType",
            "isVisible": true,
            "left": "10dp",
            "right": "0px",
            "skin": "sknlblLatoBold35475f14px",
            "text": "Savings",
            "top": "15dp",
            "width": "25%",
            "zIndex": 1
        }, {
            "contentAlignment": constants.CONTENT_ALIGN_TOP_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, {});
        var lblAccountName = new kony.ui.Label({
            "bottom": "15dp",
            "id": "lblAccountName",
            "isVisible": true,
            "left": "27%",
            "skin": "sknlblLatoBold35475f14px",
            "text": "Gold saving",
            "top": "15dp",
            "width": "24.50%",
            "zIndex": 1
        }, {
            "contentAlignment": constants.CONTENT_ALIGN_TOP_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, {});
        var lblAccountNumber = new kony.ui.Label({
            "bottom": "15dp",
            "id": "lblAccountNumber",
            "isVisible": true,
            "left": "53%",
            "skin": "sknlblLatoBold35475f14px",
            "text": "1234567890123456",
            "top": "15dp",
            "width": "25%",
            "zIndex": 1
        }, {
            "contentAlignment": constants.CONTENT_ALIGN_TOP_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, {});
        var flxStatus = new kony.ui.FlexContainer({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "clipBounds": true,
            "height": "15dp",
            "id": "flxStatus",
            "isVisible": true,
            "layoutType": kony.flex.FLOW_HORIZONTAL,
            "left": "80%",
            "isModalContainer": false,
            "skin": "slFbox",
            "top": "14dp",
            "width": "9%",
            "zIndex": 1
        }, {}, {});
        flxStatus.setDefaultUnit(kony.flex.DP);
        var fontIconStatus = new kony.ui.Label({
            "centerY": "50%",
            "id": "fontIconStatus",
            "isVisible": true,
            "left": "0px",
            "skin": "sknFontIconActivate",
            "text": "",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, {
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, {});
        var lblStatus = new kony.ui.Label({
            "centerY": "50%",
            "id": "lblStatus",
            "isVisible": true,
            "left": "5px",
            "skin": "sknlblLato5bc06cBold14px",
            "text": "Active",
            "top": "0dp",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, {
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, {});
        flxStatus.add(fontIconStatus, lblStatus);
        var flxUnlink = new kony.ui.FlexContainer({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "clipBounds": true,
            "height": "45dp",
            "id": "flxUnlink",
            "isVisible": false,
            "layoutType": kony.flex.FREE_FORM,
            "isModalContainer": false,
            "right": "25dp",
            "skin": "slFbox",
            "top": "0dp",
            "width": "2%",
            "zIndex": 1
        }, {}, {});
        flxUnlink.setDefaultUnit(kony.flex.DP);
        var flblUnlink = new kony.ui.Label({
            "centerY": "50%",
            "height": "20dp",
            "id": "flblUnlink",
            "isVisible": true,
            "right": "0dp",
            "skin": "sknIcomoon20px",
            "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmFAQ.fonticonEdit\")",
            "top": "20dp",
            "width": "20dp",
            "zIndex": 1
        }, {
            "contentAlignment": constants.CONTENT_ALIGN_CENTER,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, {});
        flxUnlink.add(flblUnlink);
        flxCompanyAccounts.add(lblAccountType, lblAccountName, lblAccountNumber, flxStatus, flxUnlink);
        var lblSeperator = new kony.ui.Label({
            "bottom": "0dp",
            "height": "1px",
            "id": "lblSeperator",
            "isVisible": true,
            "left": "0px",
            "right": "0px",
            "skin": "sknLblD5D9DD1000",
            "text": ".",
            "zIndex": 1
        }, {
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, {});
        flxCompanyDetailsAccounts.add(flxCompanyAccounts, lblSeperator);
        return flxCompanyDetailsAccounts;
    }
})