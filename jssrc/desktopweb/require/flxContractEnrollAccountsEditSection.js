define("flxContractEnrollAccountsEditSection", function() {
    return function(controller) {
        var flxContractEnrollAccountsEditSection = new kony.ui.FlexContainer({
            "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
            "clipBounds": true,
            "id": "flxContractEnrollAccountsEditSection",
            "isVisible": true,
            "layoutType": kony.flex.FLOW_VERTICAL,
            "left": "0dp",
            "isModalContainer": false,
            "skin": "sknFlxFFFFFF100O",
            "top": "0dp",
            "width": "100%"
        }, {}, {});
        flxContractEnrollAccountsEditSection.setDefaultUnit(kony.flex.DP);
        var flxAccountSelectionError = new kony.ui.FlexContainer({
            "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
            "clipBounds": true,
            "id": "flxAccountSelectionError",
            "isVisible": false,
            "layoutType": kony.flex.FREE_FORM,
            "left": "0",
            "isModalContainer": false,
            "skin": "slFbox",
            "top": "0",
            "width": "100%"
        }, {}, {});
        flxAccountSelectionError.setDefaultUnit(kony.flex.DP);
        var flxErrorContainer = new kony.ui.FlexContainer({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "clipBounds": true,
            "height": "40dp",
            "id": "flxErrorContainer",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "left": "15dp",
            "isModalContainer": false,
            "right": "15dp",
            "skin": "sknFlxBorder1pxe61919",
            "top": "15dp",
            "zIndex": 1
        }, {}, {});
        flxErrorContainer.setDefaultUnit(kony.flex.DP);
        var flxErrorIconContainer = new kony.ui.FlexContainer({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "clipBounds": true,
            "height": "40dp",
            "id": "flxErrorIconContainer",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "left": "0dp",
            "isModalContainer": false,
            "skin": "sknFlxBge61919",
            "top": "0dp",
            "width": "40dp",
            "zIndex": 1
        }, {}, {});
        flxErrorIconContainer.setDefaultUnit(kony.flex.DP);
        var lblAccountErrorIcon = new kony.ui.Label({
            "centerX": "50%",
            "centerY": "50%",
            "height": "11px",
            "id": "lblAccountErrorIcon",
            "isVisible": true,
            "skin": "sknErrorWhiteIcon",
            "text": "",
            "width": "15px",
            "zIndex": 1
        }, {
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, {});
        flxErrorIconContainer.add(lblAccountErrorIcon);
        var lblErrorValue = new kony.ui.Label({
            "centerY": "50%",
            "id": "lblErrorValue",
            "isVisible": true,
            "left": "50dp",
            "skin": "sknLbl485C75LatoRegular13Px",
            "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmCompanies.Select_atleast_one_account_to_proceed\")",
            "width": kony.flex.USE_PREFFERED_SIZE,
            "zIndex": 1
        }, {
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, {});
        flxErrorContainer.add(flxErrorIconContainer, lblErrorValue);
        flxAccountSelectionError.add(flxErrorContainer);
        var flxHeaderContainer = new kony.ui.FlexContainer({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "clipBounds": true,
            "height": "52dp",
            "id": "flxHeaderContainer",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "left": "0dp",
            "isModalContainer": false,
            "skin": "slFbox",
            "top": "0dp",
            "width": "100%"
        }, {}, {});
        flxHeaderContainer.setDefaultUnit(kony.flex.DP);
        var flxAccountNumCont = new kony.ui.FlexContainer({
            "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
            "clipBounds": true,
            "id": "flxAccountNumCont",
            "isVisible": true,
            "layoutType": kony.flex.FLOW_HORIZONTAL,
            "left": "15dp",
            "isModalContainer": false,
            "skin": "slFbox",
            "top": "25dp",
            "width": "26%"
        }, {}, {});
        flxAccountNumCont.setDefaultUnit(kony.flex.DP);
        var flxCheckbox = new kony.ui.FlexContainer({
            "autogrowMode": kony.flex.AUTOGROW_NONE,
            "clipBounds": true,
            "height": "15dp",
            "id": "flxCheckbox",
            "isVisible": true,
            "layoutType": kony.flex.FREE_FORM,
            "left": "0dp",
            "isModalContainer": false,
            "skin": "sknFlxPointer",
            "top": "0dp",
            "width": "15dp"
        }, {}, {});
        flxCheckbox.setDefaultUnit(kony.flex.DP);
        var imgSectionCheckbox = new kony.ui.Image2({
            "height": "100%",
            "id": "imgSectionCheckbox",
            "isVisible": true,
            "left": "0",
            "skin": "slImage",
            "src": "checkboxnormal.png",
            "top": "0",
            "width": "100%"
        }, {
            "imageScaleMode": constants.IMAGE_SCALE_MODE_MAINTAIN_ASPECT_RATIO,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, {});
        flxCheckbox.add(imgSectionCheckbox);
        var lblAccountNumber = new kony.ui.Label({
            "id": "lblAccountNumber",
            "isVisible": true,
            "left": "10dp",
            "skin": "sknlblLato696c7311px",
            "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmCompanies.ACCOUNTNUMBER\")",
            "top": "0dp",
            "width": kony.flex.USE_PREFFERED_SIZE
        }, {
            "contentAlignment": constants.CONTENT_ALIGN_TOP_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, {});
        var lblIconSortAccName = new kony.ui.Label({
            "id": "lblIconSortAccName",
            "isVisible": true,
            "left": "5dp",
            "skin": "sknIcon485C7513px",
            "text": "",
            "top": "0dp",
            "width": kony.flex.USE_PREFFERED_SIZE
        }, {
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, {
            "hoverSkin": "sknIcon485C7513pxHover"
        });
        flxAccountNumCont.add(flxCheckbox, lblAccountNumber, lblIconSortAccName);
        var flxAccountType = new kony.ui.FlexContainer({
            "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
            "clipBounds": true,
            "id": "flxAccountType",
            "isVisible": true,
            "layoutType": kony.flex.FLOW_HORIZONTAL,
            "left": "29%",
            "isModalContainer": false,
            "skin": "slFbox",
            "top": "25dp",
            "width": "20%"
        }, {}, {});
        flxAccountType.setDefaultUnit(kony.flex.DP);
        var lblAccountType = new kony.ui.Label({
            "id": "lblAccountType",
            "isVisible": true,
            "left": "0%",
            "skin": "sknlblLato696c7311px",
            "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmCompanies.ACCOUNTTYPE\")",
            "top": "0dp",
            "width": kony.flex.USE_PREFFERED_SIZE
        }, {
            "contentAlignment": constants.CONTENT_ALIGN_TOP_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, {});
        var lblIconFilterAccType = new kony.ui.Label({
            "id": "lblIconFilterAccType",
            "isVisible": true,
            "left": "5dp",
            "skin": "sknIcon485C7513px",
            "text": "",
            "top": "0dp",
            "width": kony.flex.USE_PREFFERED_SIZE
        }, {
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, {
            "hoverSkin": "sknIcon485C7513pxHover"
        });
        flxAccountType.add(lblAccountType, lblIconFilterAccType);
        var flxAccountName = new kony.ui.FlexContainer({
            "clipBounds": true,
            "height": "100%",
            "id": "flxAccountName",
            "isVisible": true,
            "layoutType": kony.flex.FLOW_HORIZONTAL,
            "left": "50%",
            "isModalContainer": false,
            "skin": "slFbox",
            "top": "25dp",
            "width": "22%"
        }, {}, {});
        flxAccountName.setDefaultUnit(kony.flex.DP);
        var lblAccountName = new kony.ui.Label({
            "id": "lblAccountName",
            "isVisible": true,
            "left": "0%",
            "skin": "sknlblLato696c7311px",
            "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmCompanies.ACCOUNTNAME\")",
            "top": "0dp",
            "width": kony.flex.USE_PREFFERED_SIZE
        }, {
            "contentAlignment": constants.CONTENT_ALIGN_TOP_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, {});
        var lblIconAccNameSort = new kony.ui.Label({
            "id": "lblIconAccNameSort",
            "isVisible": true,
            "left": "5dp",
            "skin": "sknIcon485C7513px",
            "text": "",
            "top": "0dp",
            "width": kony.flex.USE_PREFFERED_SIZE
        }, {
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, {
            "hoverSkin": "sknIcon485C7513pxHover"
        });
        flxAccountName.add(lblAccountName, lblIconAccNameSort);
        var flxAccountHolder = new kony.ui.FlexContainer({
            "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
            "clipBounds": true,
            "id": "flxAccountHolder",
            "isVisible": true,
            "layoutType": kony.flex.FLOW_HORIZONTAL,
            "left": "73%",
            "isModalContainer": false,
            "right": "100dp",
            "skin": "slFbox",
            "top": "25dp"
        }, {}, {});
        flxAccountHolder.setDefaultUnit(kony.flex.DP);
        var lblAccountHolder = new kony.ui.Label({
            "id": "lblAccountHolder",
            "isVisible": true,
            "left": "0%",
            "skin": "sknlblLato696c7311px",
            "i18n_text": "kony.i18n.getLocalizedString(\"i18n.frmEnrollCustomer.OwnershipType_UC\")",
            "top": "0dp",
            "width": kony.flex.USE_PREFFERED_SIZE
        }, {
            "contentAlignment": constants.CONTENT_ALIGN_TOP_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, {});
        var lblIconSortAccHolder = new kony.ui.Label({
            "id": "lblIconSortAccHolder",
            "isVisible": true,
            "left": "5dp",
            "skin": "sknIcon485C7513px",
            "text": "",
            "top": "0dp",
            "width": kony.flex.USE_PREFFERED_SIZE
        }, {
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, {
            "hoverSkin": "sknIcon485C7513pxHover"
        });
        flxAccountHolder.add(lblAccountHolder, lblIconSortAccHolder);
        var lblSeperator = new kony.ui.Label({
            "bottom": "0dp",
            "height": "1dp",
            "id": "lblSeperator",
            "isVisible": true,
            "left": "0dp",
            "skin": "sknLblSeparator696C73",
            "text": "Label",
            "width": "100%"
        }, {
            "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
            "padding": [0, 0, 0, 0],
            "paddingInPixel": false
        }, {});
        flxHeaderContainer.add(flxAccountNumCont, flxAccountType, flxAccountName, flxAccountHolder, lblSeperator);
        flxContractEnrollAccountsEditSection.add(flxAccountSelectionError, flxHeaderContainer);
        return flxContractEnrollAccountsEditSection;
    }
})